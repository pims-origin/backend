<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Models\Setting;
use Seldat\Wms2\Models\SystemMeasurement;

class AsnHdrModel extends AbstractModel
{
    const DEFAULT_MEASUREMENT = 'IN';
    const MEASUREMENT_SETTING_KEY = 'MS';
    const DAS_LIMIT = 2;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnHdr();
    }

    /**
     * @return mixed
     */
    public function getLatestAsnNumber()
    {
        return $this->model->where('asn_type', 'ASN')->orderBy('asn_hdr_num', 'desc')->take(1)->get()->first();
    }

    public function search($attributes, array $with = [], $limit = 15, $export = false)
    {
        $query = $this->make($with);
        $query->select([
            'asn_hdr.*',
            DB::raw('MIN(gr_hdr.created_at) AS gr_hdr_created_at'),
            DB::raw('MAX(gr_hdr.gr_hdr_act_dt) AS gr_hdr_act_dt'),
            DB::raw('gr_hdr.ctnr_id AS gr_hdr_ctnr_id'),
            DB::raw('gr_hdr.ctnr_num AS gr_hdr_ctnr_num'),
        ])->leftJoin('gr_hdr',function($query){
            $query->on('gr_hdr.asn_hdr_id','=','asn_hdr.asn_hdr_id')
                ->where('gr_hdr.created_from','=',DB::raw('GUN'));
        });
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        // search asn po
        if (isset($attributes['asn_dtl_po'])) {
            $query->whereHas('asnDtl', function ($query) use ($attributes) {
                $query->where('asn_dtl_po', 'like', "%" . SelStr::escapeLike($attributes['asn_dtl_po']) . "%");
            });
        }

        if (isset($attributes['sku'])) {
            $query->whereHas('asnDtl', function ($query) use ($attributes) {
                $query->where('asn_dtl_sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
            });
        }

        // search whs id
        if (isset($attributes['whs_id'])) {
            $query->where('asn_hdr.whs_id', (int)$attributes['whs_id']);
        }

        if (isset($attributes['asn_type'])) {
            $query->where('asn_hdr.asn_type', $attributes['asn_type']);
        }

        // search asn number
        if (isset($attributes['asn_hdr_num'])) {
            $query->where('asn_hdr.asn_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['asn_hdr_num']) . "%");
        }

        // search customer
        if (isset($attributes['cus_id'])) {
            $query->where('asn_hdr.cus_id', (int)$attributes['cus_id']);
        }

        // search asn status
        if (isset($attributes['asn_sts']) && count($attributes['asn_sts'])) {
            $query->where(function($query) use ($attributes){
                foreach($attributes['asn_sts'] as $index => $value){
                    if ($index === 0){
                        $query->where('asn_sts', $value);
                    }else{
                        $query->orWhere('asn_sts', $value);
                    }
                }
            });
        }

        // Search by expected date
        if (isset($attributes['asn_hdr_ept_dt'])) {
            $query->where(
                DB::raw('DATE_FORMAT(FROM_UNIXTIME(asn_hdr.asn_hdr_ept_dt), "%m/%d/%Y")'),
                $attributes['asn_hdr_ept_dt']
            );
        }
        // search container
        if (isset($attributes['ctnr_num'])) {
            $query->whereHas('asnDtl.container', function ($query) use ($attributes) {
                $query->where('ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
            });
        }

        // search item
        if (isset($attributes['dtl_sku'])) {
            $query->whereHas('asnDtl.item', function ($query) use ($attributes) {
                $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['dtl_sku']) . "%");
            });
        }

        //search dashboard alert
        if(!empty($attributes['asn_late'])){
            $query->whereRaw('DATE(FROM_UNIXTIME(asn_hdr.asn_hdr_ept_dt)) < DATE(NOW())')
                ->whereNotIn('asn_hdr.asn_sts', ['RE', 'CC']);
        }

        //search dashboard alert
        if(!empty($attributes['asn_late'])){
            $query->whereRaw('DATE(FROM_UNIXTIME(asn_hdr.asn_hdr_ept_dt)) < DATE(NOW())')
                ->whereNotIn('asn_hdr.asn_sts', ['RE', 'CC']);
        }

        // Get X-doc
        if (!empty($attributes['xdoc']) && !empty($attributes['xdoc']) === true) {
            $query->select(DB::raw('concat(asn_hdr_id, "-", ctnr_id) as asn_ctnr, sum(asn_dtl_crs_doc) as xdoc'));
            $query->groupBy(['asn_hdr_id', 'ctnr_id']);
        } else {
            $query->groupBy('asn_hdr.asn_hdr_id');
        }
        $this->sortBuilder($query, $attributes);
        if (isset($attributes['day'])) {
            $day = $attributes['day'];
            $limitday = (!empty($day) && is_numeric($day)) ? $day : 0;
            $query->where('updated_at', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $limitday . ' DAY)'));
        }
        $this->model->filterData($query, true);

        if ($export) {
            return $query->get();
        } else {
            return $query->paginate($limit);
        }
    }

    /**
     * @return mixed
     */
    public function dashboard($input = [])
    {
        $asnSts = [
            'RG' => [Status::getByKey("ASN_STATUS", 'RECEIVING')],
            'RE' => [Status::getByKey("ASN_STATUS", 'RECEIVED')],
            'NW' => [Status::getByKey("ASN_STATUS", 'NEW')],
            'CO' => [Status::getByKey("ASN_STATUS", 'RECEIVING_COMPLETE')]
        ];

        foreach ($asnSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $asnSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis->getCustomersByWhs($currentWH);

        $query = DB::table('asn_hdr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['NW']})
                    THEN 1 ELSE 0 END), 0) AS newAsn,
	            COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['RE']})
	                THEN 1 ELSE 0 END), 0) AS received,
	            COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['RG']})
	                THEN 1 ELSE 0 END), 0) AS receiving
            "))
            ->where('deleted', 0)
            ->where('updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->whereIn('cus_id', $cus_ids)
            ->where('whs_id', $currentWH)
            ->where(function ($query) use ($userId) {
                $query->where('created_by', $userId)
                    ->orWhere('updated_by', $userId);
            });

        $this->model->filterData($query, true);

        $model = $query->first();

        return [
            'newAsn'    => !empty($model['newAsn']) ? $model['newAsn'] : 0,
            'received'  => !empty($model['received']) ? $model['received'] : 0,
            'receiving' => !empty($model['receiving']) ? $model['receiving'] : 0
        ];
    }

    /**
     * Get default measurement
     *
     * @return String
     */
    public function getDefaultMeasurement() {
        $setting = Setting::where([
            'setting_key'=>self::MEASUREMENT_SETTING_KEY
        ])->first();
        $value = str_replace('"', '', $setting->setting_value);

        if(!$value) {
            return self::DEFAULT_MEASUREMENT;
        }

        $measure = SystemMeasurement::where([
            'sys_mea_name' => $value
        ])->first();

        if($measure) {
            return $measure->sys_mea_code;
        }

        return self::DEFAULT_MEASUREMENT;
    }

    public function updateAsnHdrReceived($asnId){
        $sql = "(SELECT COUNT(asn_dtl.asn_dtl_id) FROM asn_dtl WHERE asn_dtl.asn_hdr_id = asn_hdr.asn_hdr_id AND asn_dtl.`asn_dtl_sts` NOT IN ('RE', 'CC') AND asn_dtl.deleted = 0) = 0";
        $result = $this->model
            ->where(['asn_hdr_id' => $asnId])
            ->whereRaw($sql)
            ->update([
                'asn_hdr_ept_dt' => time(),
                'asn_sts' => 'RE'
            ]);
        return $result;
    }

    public function getAsnHdrById($asnId){
        $result = $this->model
            ->where(['asn_hdr_id' => $asnId])
            ->first();
        return $result;
    }

    public function updateAsnHdrToReceived($asnId)
    {
        return $this->model
            ->where(['asn_hdr_id' => $asnId])
            ->update(['asn_sts' => 'RE']);
    }

    public function generateAsnNumAndSeq($prefix = 'ASN')
    {
        $currentYearMonth = date('ym');
        $asn = AsnHdr::where('asn_hdr_num', 'LIKE', '%-' . $currentYearMonth . '-%')
                        ->orderBy('asn_hdr_seq', 'DESC')
                        ->first();

        if (!$asn){
            return collect([
                'asn_num' => $prefix . '-' . $currentYearMonth . '-' . '00001',
                'asn_seq' => 1
            ]);
        }

        $asn->asn_hdr_seq++;
        return collect([
            'asn_num' => $prefix . '-' . $currentYearMonth . '-' . sprintf('%05d', $asn->asn_hdr_seq),
            'asn_seq' => $asn->asn_hdr_seq
        ]);
    }

    public function getAnsListByAsnIds(array $asnIds, array $with = [])
    {
        $query = $this->make($with)->join('cus_meta', 'asn_hdr.cus_id', '=', 'cus_meta.cus_id')
            ->whereIn('asn_hdr.asn_hdr_id', $asnIds)
            ->where('cus_meta.qualifier', 'EEX')
            ->selectRaw("asn_hdr.*, cus_meta.value->>'$.email' as ae_email");

        return $query->get();
    }
}
