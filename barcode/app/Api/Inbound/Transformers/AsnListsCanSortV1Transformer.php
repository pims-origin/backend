<?php

namespace App\Api\Inbound\Transformers;

use App\Api\Inbound\Models\AsnDtlModel;
use App\Api\Inbound\Models\AsnHdrModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\Inbound\Models\ContainerModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\AsnStatusModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;


class AsnListsCanSortV1Transformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(AsnDtl $asnDtl)
    {
        $asnDetail = new AsnDtlModel();
        //$vtlCtn = new VirtualCartonModel();
        $item = $asnDetail->getListItem($asnDtl->asn_hdr_id, $asnDtl->ctnr_id);
        $data = $asnDtl->asnHdr()->first();
        $item_ = [];
        $it_ct = [];
        foreach ($item as $it) {
            $it_ct['sku'] = $it['sku'];
            $it_ct['size'] = $it['size'];
            $it_ct['color'] = $it['color'];
            $it_ct['pack'] = $it['pack'];
            $it_ct['asn_dtl_ctn_ttl'] = $it['asn_dtl_ctn_ttl'];
            $it_ct['asn_dtl_pack'] = $it['asn_dtl_pack'];
            $it_ct['asn_dtl_sts'] = $asnDetail->formatStatus('default', $it['asn_dtl_sts']);
            $it_ct['asn_sts_name'] = $it['asn_sts_name'];
            $it_ct['item_id'] = $it['item_id'];
            $it_ct['asn_dtl_id'] = $it['asn_dtl_id'];
            $it_ct['asn_dtl_lot'] = $it['asn_dtl_lot'];
            $it_ct['uom_code'] = $it['uom_code'];

            $total_ = $it['asn_dtl_qty_ttl'] - $it['asn_dtl_crs_doc'];
            if ($it['uom_code'] === 'KG') {
                $actCtns = $it['asn_dtl_ctn_ttl'] ?? 'NA';
                $it_ct['total_pieces'] = $total_ . '/0';
                $it_ct['act_carton'] = $actCtns . '/0';
            } else {
                $actCtns = $total_ / $it['asn_dtl_pack'];
                $it_ct['total_pieces'] = $total_ . '/0';
                $it_ct['act_carton'] = $actCtns . '/0';
            }


            
            $item_[] = $it_ct;
        }

        $asnHdr = new AsnHdrModel();
        $asnHdr = $asnHdr->getFullASN($asnDtl->asn_hdr_id);

        $expDate = object_get($asnHdr, 'asn_hdr_ept_dt', '');
        $expDate = ($expDate) ? date('m/d/Y', $expDate) : '';

        $ctnr = new ContainerModel();
        $ctnr = $ctnr->getFirstBy('ctnr_id', $asnDtl->ctnr_id);

        return [
            'asn_hdr_id'     => $asnHdr->asn_hdr_id,
            'asn_hdr_num'    => $asnHdr->asn_hdr_num,
            'asn_hdr_ref'    => $asnHdr->asn_hdr_ref,
            "asn_date"       => $asnHdr->created_at->format('m/d/Y'),
            'cus_name'       => $asnHdr->cus_name,
            'whs_id'         => $asnHdr->whs_id,
            'cus_id'         => $asnHdr->cus_id,
            'ctnr_id'        => $ctnr->ctnr_id,
            'ctnr_num'       => $ctnr->ctnr_num,
            'asn_hdr_ept_dt' => $expDate,
            'asn_sts'        => $asnDetail->formatStatus('default', $asnHdr->asn_sts),
            'asn_sts_name'   => $asnHdr->asn_sts_name,
            'cus_code'       => $asnDtl->cus_code,
            'total-sku'      => count($item),
            'items'          => $item_,
        ];

    }

}
