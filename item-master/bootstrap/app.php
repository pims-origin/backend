<?php

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__ . '/../')
);

$app->withFacades();

$app->withEloquent();
Seldat\Wms2\Utils\Profiler::start();
$app->configure('swagger-lume');
$app->configure('constants');
$app->configure('mail');
$app->configure('filesystems');

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

// for JWT
$app->singleton(
    Illuminate\Cache\CacheManager::class,
    function ($app) {
        return $app->make('cache');
    }
);

// for JWT
$app->singleton(
    Illuminate\Auth\AuthManager::class,
    function ($app) {
        return $app->make('auth');
    }
);

// for filesystem
$app->singleton('filesystem', function ($app) {
    return $app->loadComponent('filesystems', 'Illuminate\Filesystem\FilesystemServiceProvider', 'filesystem');
});
/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

$app->routeMiddleware([
    'verifySecret' => App\Http\Middleware\VerifySecret::class,
    'trimInput'    => App\Http\Middleware\TrimInput::class,
    'authorize'    => Seldat\Wms2\Http\Middleware\Authorize::class,
    'setWarehouseTimezone'    => Seldat\Wms2\Http\Middleware\SetWarehouseTimezone::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

$app->register(Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class);

$app->register(Dingo\Api\Provider\LumenServiceProvider::class);

$app->register(Illuminate\Mail\MailServiceProvider::class);

$app['Dingo\Api\Exception\Handler']->setErrorFormat([
    'errors' => [
        'message'     => ':message',
        'errors'      => ':errors',
        'code'        => ':code',
        'status_code' => ':status_code',
        'debug'       => ':debug'
    ]
]);

$app['Dingo\Api\Transformer\Factory']->setAdapter(function ($app) {
    return new Dingo\Api\Transformer\Adapter\Fractal(new League\Fractal\Manager, 'include', ',');
});

$app['Dingo\Api\Auth\Auth']->extend('jwt', function ($app) {
    return new Dingo\Api\Auth\Provider\JWT($app['Tymon\JWTAuth\JWTAuth']);
});

// for Swagger
$app->register(\SwaggerLume\ServiceProvider::class);

$app->register(\Maatwebsite\Excel\ExcelServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Custom The Application Monolog
|--------------------------------------------------------------------------
|
| Configuration daily monolog.
|
*/

$app->configureMonologUsing(function (\Monolog\Logger $logger) {
    $maxFiles = env('APP_MAX_LOG_FILE');
    $filename = storage_path('logs/lumen.log');
    $handler = new \Monolog\Handler\RotatingFileHandler($filename, $maxFiles);
    $handler->setFilenameFormat('{filename}-{date}', 'Y-m-d');
    $formatter = new \Monolog\Formatter\LineFormatter(null, null, true, true);
    $handler->setFormatter($formatter);
    $logger->pushHandler($handler);

    return $logger;
});

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

require __DIR__ . '/../app/Api/routes.php';

return $app;
