<?php

namespace App\Modules\Asns\Validators;

use Validator;
use App\Modules\Asns\Models\AbstractModel;

class UpdateValidator
{
    public function rules()
    {
        return [
            // Validate asnHdr
            'cus_id'            => 'required',
            'ctnr_ept_dt'       => 'required|date',

            // Validate asnDtl
            'ctnr_num'          => 'required',
            'items.*.po'        => 'required|string',
            'items.*.ept_qty'   => 'required|integer|min:1',
            'items.*.x_dock'    => 'nullable|integer|min:0',
            'items.*.sku'       => 'required',
            'items.*.length'    => 'required|numeric|between:0,999.99',
            'items.*.width'     => 'required|numeric|between:0,999.99',
            'items.*.height'    => 'required|numeric|between:0,999.99',
            'items.*.weight'    => 'required|numeric|between:0,999.99',
            'items.*.po_dt'     => 'nullable|date',
            'items.*.ept_dtl_dt' => 'nullable|date',
        ];
    }

    public function validate($input)
    {
        $validator = Validator::make($input, $this->rules(), $this->messages());

        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                }
            }
        }

        return $validator;
    }

    public function messages()
    {
        return [
            // Validate asnHdr
            'cus_id.required'           => 'Customer Name is required.',
            'ctnr_ept_dt.required'      => 'Container Expected Date is required.',
            'ctnr_ept_dt.date'          => 'Invalid Container Expected Date format.',

            // Validate asnDtl
            'ctnr_num.required'         => 'Container is required.',
            'items.*.po.required'       => 'PO is required.',
            'items.*.po.string.'        => 'PO must be string',
            'items.*.ept_qty.required'  => 'Exp cart is required.',
            'items.*.ept_qty.integer'   => 'Exp cart must be number',
            'items.*.ept_qty.min'       => 'Exp cart is invalid ( Min value is 1 )',
            'items.*.x_dock.integer'    => 'X-DOCK qty must be is integer',
            'items.*.x_dock.min'        => 'X-DOCK qty is invalid ( Min value is 0 )',
            'items.*.sku'               => 'SKU is required.',
            'items.*.length.required'   => 'Length is required',
            'items.*.length.numeric'    => 'Length must be numeric',
            'items.*.length.between'    => 'Length is invalid ( MAX VALUE IS 999.99 )',
            'items.*.width.required'    => 'Width is required',
            'items.*.width.numeric'     => 'Width must be numeric',
            'items.*.width.between'     => 'Width is invalid ( MAX VALUE IS 999.99 )',
            'items.*.height.required'   => 'Height is required',
            'items.*.height.numeric'    => 'Height must be numeric',
            'items.*.height.between'    => 'Height is invalid ( MAX VALUE IS 999.99 )',
            'items.*.weight.required'   => 'Weight is required',
            'items.*.weight.numeric'    => 'Weight must be numeric',
            'items.*.weight.between'    => 'Weight is invalid ( MAX VALUE IS 999.99 )',
            'items.*.po_dt.date'        => 'Invalid PO date format.',
            'items.*.ept_dtl_dt.date'   => 'Invalid Expected detail date format.',
        ];
    }

    public function checkUpdateAsn($asn)
    {
        if(empty($asn))
        {
            throw new \Exception(json_encode(['title' =>'Update asn unsuccessfully', 'detail' => 'Please try']));
        }
    }
}