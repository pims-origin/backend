<?php
namespace Seldat\Wms2\Models;

class OtbdOrder extends BaseSoftModel
{
    
    protected $table = 'odr_hdr';
    
    protected $primaryKey = 'odr_id';
    
    protected $fillable = [
        'ship_id',
        'cus_id',
        'whs_id',
        'so_id',
        'odr_num',
        'cus_odr_num',
        'ref_cod',
        'cus_po',
        'odr_type',
        'odr_sts',
        'ship_to_name',
        'ship_to_add_1',
        'ship_to_add_2',
        'ship_to_city',
        'ship_to_state',
        'ship_to_zip',
        'ship_to_country',
        'carrier',
        'odr_req_dt',
        'act_cmpl_dt',
        'req_cmpl_dt',
        'ship_by_dt',
        'cancel_by_dt',
        'act_cancel_dt',
        'ship_after_dt',
        'shipped_dt',
        'cus_pick_num',
        'ship_method',
        'cus_dept_ref',
        'csr',
        'rush_odr',
        'cus_notes',
        'in_notes',
        'hold_sts',
        'back_odr_seq',
        'back_odr',
        'org_odr_id',
        'sku_ttl',
        'audit_sts',
        'wv_num',
        'is_ecom'
    ];
    
    public $dateFields = [
        'odr_req_dt',
        'req_cmpl_dt',
        'act_cmpl_dt',
        'ship_by_dt',
        'cancel_by_dt',
        'act_cancel_dt',
        'ship_after_dt',
        'shipped_dt',
    ];
    
    public function Customer()
    {
        return $this->hasOne(__NAMESPACE__. '\Customer', 'cus_id', 'cus_id');
    }
    
    public function Warehouse()
    {
        return $this->hasOne(__NAMESPACE__. '\Warehouse', 'whs_id', 'whs_id');
    }
    
    public function OrderDtl()
    {
        return $this->belongsTo(__NAMESPACE__. '\OrderDtl', 'odr_id', 'odr_id');
    }
    
    public function UserOC()
    {
        return $this->belongsTo(__NAMESPACE__. '\User', 'created_by', 'user_id');
    }
    
    public function UserOU()
    {
        return $this->belongsTo(__NAMESPACE__. '\User', 'updated_by', 'user_id');
    }
    
    public function OrderFlowMaster()
    {
        return $this->belongsTo(__NAMESPACE__. '\OrderFlowMaster', 'odr_sts', 'odr_sts');
    }
    
    public function InvoiceHeader()
    {
        return $this->belongsTo(__NAMESPACE__. '\InvoiceHeader', 'cus_id', 'cus_id');
    }
    
    public function ShippingOrder()
    {
        return $this->belongsTo(__NAMESPACE__. '\ShippingOrder', 'cus_odr_num', 'cus_odr_num');
    }
    
    public function OrderFlowMasterR()
    {
        return $this->belongsTo(__NAMESPACE__. '\OrderFlowMaster', 'so_sts', 'odr_sts');
    }
    
    public function WavepickHdr()
    {
        return $this->belongsTo(__NAMESPACE__. '\WavepickHdr', 'wv_id', 'wv_id');
    }
    
    public function UserUW()
    {
        return $this->belongsTo(__NAMESPACE__. '\User', 'picker', 'user_id');
    }
    
    public function OrderHdr()
    {
        return $this->belongsTo(__NAMESPACE__. '\OrderHdr', 'odr_id', 'odr_id');
    }
    
}