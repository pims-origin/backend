<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\ReportConfigs;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Models\Setting;
use Seldat\Wms2\Models\SystemMeasurement;

class ReportConfigsModel extends AbstractModel
{
    /**
     * ReportConfigsModel constructor.
     *
     * @param ReportConfigs|null $model
     */
    public function __construct(ReportConfigs $model = null)
    {
        $this->model = ($model) ?: new ReportConfigs();
    }

    /**
     * @return mixed
     */
    public function getInfoReportTables()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $result = [];

        $tables = $this->getReportTables();

        foreach ( $tables as $table) {
            $info               = new \stdClass();
            $info->table        = $table;
            $info->fields       = [];

            $columns            = $this->getTableColumns($table);
            foreach ($columns as $column) {
                $type = $this->formatColumnTypeFromSql($column['Type']);

                $info->fields[] = [
                    'name'              => $column['Field'],
                    'type'              => $type->type,
                    'length'            => $type->length,
                    'basic_data_type'   => $this->getBasicType($type->type, $type->length)
                ];
            }

            $result[] = $info;
        }

        return $result;
    }

    protected function getReportTables($prefix = 'rpt')
    {
        $tables = DB::select("SHOW TABLES LIKE '" . $prefix . "_%'");

        foreach ($tables as $table) {
            foreach ($table as $key => $value) {
                $arrItems[] = [
                    'table_name' => $value,
                ];
            }
        }

        return array_column($arrItems, 'table_name');
    }

    protected function getTableColumns(string $tableName)
    {
        try {
            $columns = DB::select( DB::raw('SHOW COLUMNS FROM ' . $tableName) );
        } catch (\Exception $e) {
            $columns = [];
        }

        return $columns;
    }

    protected function formatColumnTypeFromSql($type)
    {
        $result = new \stdClass();
        $result->type   = null;
        $result->length = null;

        if ( !preg_match('/([A-za-z]+)\((\d+)\)/', $type, $matches) &&
             !preg_match('/([A-za-z]+)/', $type, $matches)
        ) {
            return $result;
        }

        $result->type   = $matches[1];


        if ( isset($matches[2]) ) {
            $result->length = (int)$matches[2];
        }

        return $result;
    }

    protected function getBasicType($type, $length)
    {
        $string     = ['varchar', 'char', 'text', 'tinytext', 'mediumtext', 'longtext', 'enum'];
        $number     = ['int', 'decimal', 'tinyint', 'smallint', 'mediumint', 'bigint', 'float', 'double'];
        $okTypes    = ['date', 'datetime'];

        if ( in_array($type, $string) ) {
            return 'string';
        }

        if ( in_array($type, $number) ) {
            if ( $length === 10 ) {
                return 'date';
            }
            return 'number';
        }

        if ( in_array($type, $okTypes) ) {
            return $type;
        }

        return 'unknown';
    }
}
