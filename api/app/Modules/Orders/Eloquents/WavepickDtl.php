<?php


namespace App\Modules\Orders\Eloquents;


class WavepickDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_dtl';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_dtl_id';
    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'wv_id',
        'item_id',
        'uom_id',
        'wv_num',
        'color',
        'sku',
        'size',
        'pack_size',
        'lot',
        'ctn_qty',
        'piece_qty',
        'act_piece_qty',
        'primary_loc_id',
        'primary_loc',
        'bu_loc_1_id',
        'bu_loc_1',
        'bu_loc_2_id',
        'bu_loc_2',
        'bu_loc_3_id',
        'bu_loc_3',
        'bu_loc_4_id',
        'bu_loc_4',
        'bu_loc_5_id',
        'bu_loc_5',
        'wv_dtl_sts',
        'act_loc_id',
        'act_loc',
        'cus_upc',
    ];

    public function waveHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\WavepickHdr', 'wv_id', 'wv_id');
    }
}
