<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class MakePaymentTransformer extends TransformerAbstract
{
    
    /**
     * @param $data
     * @return array
     */
    public function transform($data)
    {
        return [
            'inv_id' => $data->inv_id,
            'inv_num' => $data->inv_num,
            'inv_paid_dt' => $data->inv_paid_dt,
            'inv_paid_typ' => $data->inv_paid_typ,
            'inv_paid_ref' => $data->inv_paid_ref
        ];
    }
}
