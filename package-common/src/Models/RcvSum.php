<?php

namespace Seldat\Wms2\Models;


class RcvSum extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rcv_sum';

    /**
     * The primary key for the model.
     *
     * @var string
     */
     protected $primaryKey = 'id';
     
    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'event_id',
        'gr_hdr_id',
        'dt',
        'cntr_val',
        'pcs_val',
        'ctn_val',
        'plt_val',
        'vol_val'
    ];

    public function goodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'ctnr_id', 'ctnr_id');
    }
    
    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
    
    public function eventTracking()
    {
        return $this->belongsTo(__NAMESPACE__ . '\EventTracking', 'event_id', 'id');
    }
}