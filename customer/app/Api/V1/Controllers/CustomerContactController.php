<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MenuFavoriteModel;
use App\Api\V1\Models\CustomerContactModel;
use App\Api\V1\Models\MenuModel;
use App\Api\V1\Transformers\CustomerContactTransformer;
use App\Api\V1\Validators\CustomerContactValidator;
use App\Api\V1\Validators\DeleteMassContactValidator;
use App\Api\V1\Validators\SaveMassContactValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;

class CustomerContactController extends AbstractController
{
    /*
     * @var $customerContactModel
     * */
    protected $customerContactModel;
    /*
     * @var $customerContactTransformer
     * */
    protected $customerContactTransformer;
    /*
     * @var $customerContactValidator
     * */
    protected $customerContactValidator;
    /*
     * @var $delMassZoneValidator
     * */
    protected $delMassZoneValidator;
    /*
     * @var $saveMassContactValidator
     * */
    protected $saveMassContactValidator;

    public function __construct()
    {
        parent::__construct();

        $this->customerContactModel = new CustomerContactModel();
        $this->customerContactTransformer = new CustomerContactTransformer();
        $this->customerContactValidator = new CustomerContactValidator();
        $this->delMassZoneValidator = new DeleteMassContactValidator();
        $this->saveMassContactValidator = new SaveMassContactValidator();
    }

    public function index($customerId)
    {
        $customerContacts = $this->customerContactModel->getByCustomerId($customerId);

        return $this->response->collection($customerContacts, $this->customerContactTransformer);
    }

    public function show($customerId, $contactId)
    {
        try {
            $customerContact = $this->customerContactModel->getContactByCustomer($customerId, $contactId);

            return $this->response->item($customerContact, $this->customerContactTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function store(
        $customerId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->customerContactValidator->validate($input);

        if ($this->customerContactModel->countContact($customerId) < 5) {
            $params = [
                'cus_ctt_fname'      => $input['cus_ctt_fname'],
                'cus_ctt_lname'      => array_get($input, 'cus_ctt_lname', ''),
                'cus_ctt_email'      => $input['cus_ctt_email'],
                'cus_ctt_phone'      => $input['cus_ctt_phone'],
                'cus_ctt_mobile'     => $input['cus_ctt_mobile'],
                'cus_ctt_ext'        => $input['cus_ctt_ext'],
                'cus_ctt_position'   => $input['cus_ctt_position'],
                'cus_ctt_cus_id'     => $customerId,
                'cus_ctt_department' => $input['cus_ctt_department'],
            ];

            try {
                if ($customerContact = $this->customerContactModel->create($params)) {
                    return $this->response->item($customerContact,
                        $this->customerContactTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
                }
            } catch (\PDOException $e) {
                return  $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        } else {
            return $this->response->errorBadRequest(Message::get("VR061", "Number of contact", "5"));
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function update(
        $customerId,
        $contactId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->customerContactValidator->validate($input);

        $params = [
            'cus_ctt_id'         => $contactId,
            'cus_ctt_lname'      => $input['cus_ctt_lname'],
            'cus_ctt_fname'      => $input['cus_ctt_fname'],
            'cus_ctt_email'      => $input['cus_ctt_email'],
            'cus_ctt_phone'      => $input['cus_ctt_phone'],
            'cus_ctt_mobile'     => $input['cus_ctt_mobile'],
            'cus_ctt_ext'        => $input['cus_ctt_ext'],
            'cus_ctt_position'   => $input['cus_ctt_position'],
            'cus_ctt_cus_id'     => $customerId,
            'cus_ctt_department' => $input['cus_ctt_department'],
        ];

        try {
            if ($customerContact = $this->customerContactModel->update($params)) {
                return $this->response->item($customerContact, $this->customerContactTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function destroy($customerId, $contactId)
    {
        try {
            if ($this->customerContactModel->deleteCustomerContact($customerId, $contactId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function deleteMass($customerId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['cus_ctt_id'] = is_array($input['cus_ctt_id']) ? $input['cus_ctt_id'] : [$input['cus_ctt_id']];

        // validation
        $this->delMassZoneValidator->validate($input);

        try {
            $this->customerContactModel->deleteMassContact($customerId, $input['cus_ctt_id']);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }

    public function saveMassAndDeleteUnused(
        $customerId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->saveMassContactValidator->validate($input);
        if (count($input['cus_ctt_id']) <= 5) {
            $contacts = $this->customerContactModel->getByCustomerId($customerId);

            $contacts = array_pluck($contacts, 'cus_ctt_lname', 'cus_ctt_id');

            foreach ($input['cus_ctt_fname'] as $key => $val) {
                if (isset($contacts[$input['cus_ctt_id'][$key]])) {
                    $action = 'update';
                    unset($contacts[$input['cus_ctt_id'][$key]]);
                } else {
                    $action = 'create';
                }

                $params = [
                    'cus_ctt_id'         => array_get($input, 'cus_ctt_id.' . $key, null),
                    'cus_ctt_fname'      => array_get($input, 'cus_ctt_fname.' . $key, ''),
                    'cus_ctt_lname'      => array_get($input, 'cus_ctt_lname.' . $key, ''),
                    'cus_ctt_email'      => array_get($input, 'cus_ctt_email.' . $key, ''),
                    'cus_ctt_phone'      => array_get($input, 'cus_ctt_phone.' . $key, ''),
                    'cus_ctt_mobile'     => array_get($input, 'cus_ctt_mobile.' . $key, ''),
                    'cus_ctt_ext'        => array_get($input, 'cus_ctt_ext.' . $key, ''),
                    'cus_ctt_position'   => array_get($input, 'cus_ctt_position.' . $key, ''),
                    'cus_ctt_cus_id'     => $customerId,
                    'cus_ctt_department' => array_get($input, 'cus_ctt_department.' . $key, ''),
                ];
                try {
                    $this->customerContactModel->refreshModel();
                    $this->customerContactModel->{$action}($params);
                } catch (ValidationHttpException $e) {
                    return $this->response->error(implode("\n", $e->getErrors()->all()),
                        IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
                } catch (\PDOException $e) {
                    return  $this->response->errorBadRequest(
                        SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
                    );
                } catch (\Exception $e) {
                    return $this->response->errorBadRequest($e->getMessage());
                }
            }

            /**
             * delete unused
             */
            if ($contacts) {
                try {
                    $this->customerContactModel->refreshModel();
                    $this->customerContactModel->deleteCustomerContact($customerId, array_keys($contacts));
                } catch (\PDOException $e) {
                    return  $this->response->errorBadRequest(
                        SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
                    );
                } catch (\Exception $e) {
                    return $this->response->errorBadRequest($e->getMessage());
                }
            }
        } else {
            return $this->response->errorBadRequest(Message::get("VR061", "Number of contact", "5"));
        }

        return $this->response->noContent();
    }
}
