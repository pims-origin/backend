<?php
/**
 * Created by PhpStorm.
 * User: vinhpham
 * Date: 11/6/18
 * Time: 5:14 PM
 */

namespace App\Modules;


use Illuminate\Support\ServiceProvider;

class ModulesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // For each of the registered modules, include their routes and Views
        $modules = config("module.modules");

        foreach ($modules as $key => $values) {

            while (list(,$module) = each($values)) {
                $route = __DIR__. "/" . $key . "/" . $module . '/routes.php';
                if(file_exists($route)) {
                    include $route;
                }
            }

        }

    }

    public function register()
    {
    }
}