<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EmailConfigurationModel;
use App\Api\V1\Transformers\EmailConfigurationTransformer;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class EmailConfigurationController extends AbstractController
{
    protected $transactionModel;

    protected $transformer;

    /**
     * Init.
     *
     */
    public function __construct()
    {
        $this->emailConfigurationModel = new EmailConfigurationModel();
        $this->transformer = new EmailConfigurationTransformer();
    }

    /**
     * @SWG\Get(
     *     path="/email/{whsId}",
     *     tags={"SMTP Configuration"},
     *     summary="Get SMTP Configuration",
     *     description="Get SMTP Configuration",
     *     operationId="getSMTPConfig",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="whsId",
     *         in="path",
     *         description="ID of Warehouse",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return ar",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */  
    /**
     * Display a listing of the resource.
     * @param IRequest $request
     * @return mixed
     */
    public function show($whsId)
    {
        try {
            // get email smtp info
            $smtpInfo = $this->emailConfigurationModel->getSmtpInfo($whsId);

            return $this->response->item($smtpInfo, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Put(
     *     path="/email/{whsId}",
     *     tags={"SMTP Configuration"},
     *     summary="Update SMTP Configuration",
     *     description="Update SMTP Configuration",
     *     operationId="updateSMTPConfig",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="whsId",
     *         in="path",
     *         description="ID of Warehouse",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return ar",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */  
    /**
     * Store a newly created data in whs meta.
     *
     * @param  IRequest $request
     * @return Response
     */
    public function update(IRequest $request, $whsId)
    {
        $input = $request->getParsedBody();
        try {
            // Store email smtp info
            $this->emailConfigurationModel->add($input, $whsId);

            return $this->response->created(null, '{}');
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
