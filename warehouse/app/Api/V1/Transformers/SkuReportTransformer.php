<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\Status;

class SkuReportTransformer extends TransformerAbstract
{
    /**
     * @param Carton $carton
     *
     * @return array
     */
    public function transform($carton)
    {
        $packSize = object_get($carton, 'ctn_pack_size', '');
        $pieceTtl = object_get($carton, 'piece_remain_ttl', '');
        return [
            'item_id'       => object_get($carton, 'item_id', 0),
            'sku'           => object_get($carton, 'sku', ''),
            'size'          => object_get($carton, 'size', ''),
            'color'         => object_get($carton, 'color', ''),
            'cus_id'        => object_get($carton, 'customer.cus_id', ''),
            'cus_name'      => object_get($carton, 'customer.cus_name', ''),
            'cus_code'      => object_get($carton, 'customer.cus_code', ''),
            'avail'         => object_get($carton, 'avail', ''),
            'allocated_qty' => object_get($carton, 'allocated_qty', ''),
            'picked_qty'    => object_get($carton, 'picked_qty', ''),
            'dmg_qty'       => object_get($carton, 'dmg_qty', ''),
            'crs_doc_qty'   => object_get($carton, 'crs_doc_qty', ''),
            'ttl'           => object_get($carton, 'ttl', ''),
            'upc'           => object_get($carton, 'upc', ''),
            'uom'           => object_get($carton, 'uom', ''),
            'length'   => round(object_get($carton, 'length', ''), 2),
            'width'    => round(object_get($carton, 'width', ''), 2),
            'height'   => round(object_get($carton, 'height', ''), 2),
            'weight'   => round(object_get($carton, 'weight', ''), 1),
            'whs_name' => object_get($carton, 'whs_name', ''),

            'ctn_pack_size'    => $packSize,
            'piece_remain_ttl' => $pieceTtl,
//            'carton_ttl'       => object_get($carton, 'carton_ttl', ''),
            'carton_ttl'       => round($pieceTtl/$packSize),
            'cube'             => round(object_get($carton, 'cube', '')),
            'volume'           => round(object_get($carton, 'volume', '')),
            'status'           => Status::getByValue($carton->ctn_sts, 'CTN_STATUS'),

        ];
    }
}
