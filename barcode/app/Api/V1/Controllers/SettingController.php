<?php
/**
 * Created by PhpStorm.
 * User: Xuan Nguyen
 * Date: 12/28/2018
 * Time: 9:37 AM
 */
namespace App\Api\V1\Controllers;
use App\Api\V1\Models\SettingModel;
use Dingo\Api\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;

class SettingController extends AbstractController
{
    protected $model;
    public function __construct(SettingModel $_model)
    {
        $this->model = $_model;
    }
    public function getSettingByKey($whsId,$cusId){
        $res['data'] = [];
        $respone= $this->model->getSettingByKey("PRINTER");
        if(!empty($respone)){
            $settingVal = $respone['setting_value'];
            $data = json_decode($settingVal,true);
            foreach($data as $item){
                $whs = $item['whs_id']??"";
                $cus = $item['cus_id']??"";
                $printerIp = $item['printer_ip']??"";
                $name = $item['name']??"";
                if($whs == $whsId && $cus == $cusId){
                    $res['data'][]=[
                        "printer_ip"=>$printerIp,
                        "name"=>$name,
                        "cus_id"=>$cus,
                        "whs_id"=>$whs
                    ];
                }
            }
        }
        return $res;
    }
    public function upsertPrinter(Request $request,$whsId,$cusId){
        $data = array_get($request,"data");
        try{
            if(empty($data)){
                return $this->response->errorBadRequest("No Data Setting");
            }
            else{
                $data = json_decode($data,true);
                for($i=0; $i<count($data); $i++){
                    $data[$i]["whs_id"] = $whsId;
                    $data[$i]["cus_id"] = $cusId;
                }
                $res = $this->model->upsertPrinter('PRINTER',$data);
                return $this->response->noContent()
                    ->setContent(['data' => ['message' => 'Update Printer successfully',"result"=>$res]])
                    ->setStatusCode(IlluminateResponse::HTTP_OK);
            }
        }
        catch (\Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}