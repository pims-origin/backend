<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 29-Jun-16
 * Time: 10:18
 */

namespace App\Api\V1\Transformers;

use App\SystemMeasurement;
use League\Fractal\TransformerAbstract;

class SystemMeasurementTransformer extends TransformerAbstract
{
    public function transform(SystemMeasurement $systemMeasurement)
    {
        return [
            'sys_mea_code' => $systemMeasurement->sys_mea_code,
            'sys_mea_name' => $systemMeasurement->sys_mea_name,
            'sys_mea_desc' => $systemMeasurement->sys_mea_desc,
        ];
    }
}
