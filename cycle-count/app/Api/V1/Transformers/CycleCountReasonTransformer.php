<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CycleHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;

class CycleCountReasonTransformer extends TransformerAbstract
{
    public function transform(Reason $reason)
    {
        return [
            'r_name'        => $reason->r_name,
            'reason_desc'   => $reason->reason_desc,            
            'created_at'    => $reason->created_at->format("Y-m-d"),
        ];
    }

}
