<?php

namespace backend\models;

use common\models\UserMeta;
use yii\db\Query;
use yii;


class UserMetaProcess
{
    public function get($userId, $qualifier)
    {
		//TODO: DUYNQK
		if($qualifier=='wid')
		{
			return 1;
		}

        $query = new Query();
        $value = $query->from('user_meta')
            ->where(['user_id' => $userId, 'qualifier' => $qualifier])
            ->select('value')
            ->one();
		
        return empty($value['value']) ? null : $value['value'];
    }

    public function set($userId, $qualifier, $value)
    {
        $userMeta = UserMeta::find()
            ->where(['user_id' => $userId, 'qualifier' => $qualifier])
            ->one();

        if (!$userMeta) {
            $userMeta = new UserMeta();
            $userMeta->user_id = $userId;
            $userMeta->qualifier = strtoupper($qualifier);
        }

        $userMeta->value = $value;
        $userMeta->save();

        return true;
    }
}
