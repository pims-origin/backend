<?php

namespace App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Controllers;

use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\CartonModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\CustomerConfigModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\EventTrackingModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\ItemModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\LocationModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\OrderCartonModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\OrderDtlModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\OrderHdrModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\PackDtlModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\PackHdrModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\PalletModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\WaveDtlLocModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\WaveDtlModel;
use App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models\WaveHdrModel;

use App\Api\V1\Validators\WaveValidator;

use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;
use GuzzleHttp\Client;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\JWTUtil;

use App\Jobs\AutoAssignBarcodesJob;

class WavepickController extends AbstractController
{

    const STATUS_ALLOCATE_LOCATION       = 'AL';

    /**
     * @var int|mixed
     */
    protected $userId;

    /**
     * @var WaveHdrModel
     */
    protected $waveHdrModel;

    /**
     * @var WaveDtlModel
     */
    protected $waveDtlModel;

    /**
     * @var WaveDtlLocModel
     */
    protected $waveDtlLocModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var WaveDtlLocModel
     */
    protected $wavePickDtlLocModel;

    /**
     * @var CartonModel
     */
    protected $carton;

    public function __construct(
        WaveHdrModel $waveHdrModel,
        WaveDtlModel $waveDtlModel,
        WaveDtlLocModel $waveDtlLocModel,
        CartonModel $cartonModel,
        PackDtlModel $packDtlModel,
        PackHdrModel $packHdrModel,
        ItemModel $itemModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->waveHdrModel = $waveHdrModel;
        $this->waveDtlModel = $waveDtlModel;
        $this->waveDtlLocModel = $waveDtlLocModel;
        $this->cartonModel = $cartonModel;
        $this->palletModel = new PalletModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->locationModel = new LocationModel();
        $this->packHdrModel = $packHdrModel;
        $this->packDtlModel = $packDtlModel;
        $this->itemModel = $itemModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->customerConfigModel = new CustomerConfigModel();
        $this->wavePickDtlLocModel = new WaveDtlLocModel();
        $this->carton = new CartonModel();

        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function updateWavePick($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/wave-without-rfid/update";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'WWR',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Update Wavepick Without Rfid'
        ]);

        /*
         * end logs
         */

        $wvDtlId  = array_get($input, 'wv_dtl_id');
        $locId    = array_get($input, 'loc_id');
        $pickQty  = array_get($input, 'qty');
        $typePick = array_get($input, 'type_pick'); // (1: cartons | 2: pieces )

        if (!$wvDtlId || !$locId || !$pickQty || !$typePick) {
            $msg = "Wave pick ID, Location ID, pick Qty and type pick are required.";

            return $this->_responseErrorMessage($msg);
        }

        if (!is_int($pickQty) || $pickQty < 1) {
            $msg = "The number Qty must be integer and greater than 0.";
            return $this->_responseErrorMessage($msg);
        }

        $wvDtlObj = $this->waveDtlModel->getFirstWhere([
                        'whs_id'    => $whsId,
                        'wv_dtl_id' => $wvDtlId,
                    ]);

        if (!$wvDtlObj) {
            $msg = sprintf("The Wave pick ID %s doesn't existed.", $wvDtlId);
            $wvDtlObj = $this->waveDtlModel->getFirstWhere([
                        'wv_dtl_id' => $wvDtlId,
                    ]);
            if ($wvDtlObj) {
                $msg = sprintf("The Wave pick number %s doesn't belong to current warehouse.", object_get($wvDtlObj, 'wv_num'));
            }
            return $this->_responseErrorMessage($msg);
        }

        $locationObj = $this->locationModel->getFirstWhere([
                            'loc_whs_id' => $whsId,
                            'loc_id'     => $locId,
                        ]);

        if (!$locationObj) {
            $msg = sprintf("The Location ID %s doesn't existed.", $locId);
            $locationObj = $this->locationModel->getFirstWhere([
                            'loc_id' => $locId,
                        ]);
            if ($locationObj) {
                $msg = sprintf("The Location code %s doesn't belong to current warehouse.", object_get($locationObj, 'loc_code'));
            }
            return $this->_responseErrorMessage($msg);
        }

        $locCode = object_get($locationObj, 'loc_code');

        $cartonChk = $this->carton->getModel()
                        ->where('whs_id', $whsId)
                        ->where('loc_id', $locId)
                        ->where('ctn_sts', '!=', 'AJ')
                        ->whereNotNull('rfid')
                        ->first();
        if ($cartonChk) {
            $msg = sprintf("The Location code %s exist carton which is RFID.", object_get($locationObj, 'loc_code'));
            return $this->_responseErrorMessage($msg);
        }

        try {

            // Pick type is 1 : pick cartons
            if ($typePick == 1) {
                $pickQty = $pickQty * $wvDtlObj->pack_size;
            }

            $data = [
                'wv_dtl_id' => $wvDtlId,
                'loc_code'  => $locCode,
                'act_qty'   => $pickQty,
            ];

            $errors = $this->_updateWavePickNoEcom($whsId, $data);
            if ($errors) {
                return $errors;
            }

            // WAP-625 - [Outbound - Wave Pick] Auto assign cartons to Order if 1 Wave Pick just have only 1 Order (Case Barcode)
            $isAutoAssign = $this->_isAutoAssignToOrder($wvDtlObj);
            if ($isAutoAssign) {
                dispatch(new AutoAssignBarcodesJob($whsId, $wvDtlId, $pickQty, $request));
            }

            $wvDtlObj = $this->waveDtlModel->getFirstWhere([
                        'whs_id'    => $whsId,
                        'wv_dtl_id' => $wvDtlId,
                    ]);
            $qtyPieces = $wvDtlObj->act_piece_qty;

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Update Wavepick successfully!"
            ]];
            $msg['data']   = [[
                'qty_pieces' => $qtyPieces
            ]];

            return $msg;

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _updateWavePickNoEcom($whs_id, $input)
    {

        $wvDtlId = $input['wv_dtl_id'];
        $locCode = $input['loc_code'];
        $actQty = $input['act_qty'];

        DB::beginTransaction();

        //get wv_dtl
        $wvDtl = WavepickDtl::where([
            'wv_dtl_id' => $wvDtlId,
            'whs_id'    => $whs_id
        ])->first();

        if (!in_array($wvDtl['wv_dtl_sts'], ['NW', 'PK']) || ($wvDtl['piece_qty'] == $wvDtl['act_piece_qty'])) {
            $msg = "Wavepick detail Picked already!";
            return $this->_responseErrorMessage($msg);
        }

        $wvHdr = $this->waveHdrModel->findWhere(['wv_id' => $wvDtl->wv_id])->first();

        if ($wvDtl['picker_id'] != Data::getCurrentUserId()) {
            $msg = "This wave pick item doesn't belong to current user";
            return $this->_responseErrorMessage($msg);
        }

        if (!$wvDtl || !$wvHdr) {
            $msg = "Wave pick is not existed";
            return $this->_responseErrorMessage($msg);
        }

        if (!in_array($wvHdr['wv_sts'], ['NW', 'PK'])) {
            $msg = "Wave pick Completed already!";
            return $this->_responseErrorMessage($msg);
        }

        //  Pick Pallet
        if ($wvDtl->pick_pallet == 1) {

            $query = DB::table('odr_dtl_allocation')
                ->select('qty', 'loc_code', 'plt_rfid')
                ->where('wv_dtl_id', $wvDtlId);

            if ($this->isPalletCode($locCode)) {
                $query->where('plt_rfid', trim($locCode));
            } else {
                $query->where('loc_code', trim($locCode));
            }

            $orderDetailAllocation = $query->first();

            if (empty($orderDetailAllocation['plt_rfid'])) {
                $msg = "Wavepick detail is not pick full pallet";
                return $this->_responseErrorMessage($msg);
            }

            $locCode = $orderDetailAllocation['plt_rfid'];
            $actQty = $orderDetailAllocation['qty'];

        }

        // if ($this->isPalletCode($locCode)) {
        //     return $this->updateWavePickPallet($locCode, $whs_id, $wvDtl, $actQty);
        // }

        //get location
        $loc = Location::where([
            'loc_code'   => $locCode,
            'loc_whs_id' => $whs_id
        ])->first();
        if (!$loc) {
            $msg = "Location " . $locCode . " is not existed";
            return $this->_responseErrorMessage($msg);
        }

        /*
        * Check $loc status = IN, LK ko cho phep tiep tuc pick
        */
        if ($loc->loc_sts_code == 'IA') {
            $msg = "Location " . $locCode . " is inactive";
            return $this->_responseErrorMessage($msg);
        }

        if ($loc->loc_sts_code == 'LK') {
            $msg = "Location " . $locCode . " is locked";
            return $this->_responseErrorMessage($msg);
        }

        if ($loc->loc_sts_code == self::STATUS_ALLOCATE_LOCATION) {
            $userId = Data::getCurrentUserId();
            $whsMeta = DB::table('whs_meta')
                ->where('whs_id', $whs_id)
                ->where('whs_qualifier', 'wle')
                ->where('deleted', 0)
                ->first();
            $minuteExpired = array_get($whsMeta, 'whs_meta_value', 0);

            $timeReleased = array_get($loc->toArray(), 'updated_at', 0) + $minuteExpired * 60;
            if ($timeReleased > time() && object_get($loc, 'updated_by') != $userId) {
                $msg = "Location " . $locCode . " is allocated.";
            }
        }

        //validate qty
        if ($wvDtl['piece_qty'] < $actQty) {
            $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s picked Qty QTY %d is greater than Allocated Qty %d',
                $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['lot'], $actQty, $wvDtl['piece_qty']);
            return $this->_responseErrorMessage($msg);
        }

        //get algorithm
        $algorithm = $this->customerConfigModel->getPickingAlgorithm($wvDtl->whs_id, $wvDtl->cus_id);

        $qty = $this->cartonModel->getAvailableQuantity($wvDtl['item_id'], $wvDtl['lot'], $loc->loc_id,
            $loc->loc_whs_id);
        if ($qty < $actQty) {
            $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s available QTY %d is less than Picked Qty %d at Location %s',
                $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['lot'], $qty, $actQty, $locCode);
            // $checkLocRfid = $this->locationModel->getModel()
            //                         ->where('loc_code', $locCode)
                                       // ->where('ctn_sts', '!=', 'AJ')
            //                         ->whereNotNull('rfid')
            //                         ->first();
            // if($checkLocRfid)
            // {
            //     $msg = sprintf("Unable update wave pick from location %s which exist RFID",$locCode);
            // }
            return $this->_responseErrorMessage($msg);
        }

        $actLoc = [
            'picked_qty' => $actQty,
            'act_loc_id' => $loc->loc_id,
            'act_loc'    => $loc->loc_code,
            'loc_id'     => $loc->loc_id,
            'loc_code'   => $loc->loc_code,
            'avail_qty'  => $qty
        ];


        $cartons = $this->cartonModel->getAllCartonsByWvDtl($wvDtl, $algorithm, [$loc->loc_id], [$actLoc]);

        if(! $cartons ){
            $msg = 'There is no carton to pick or duplicated submit';
            return $this->_responseErrorMessage($msg);
        }

        try {

            $wvDtlSts = 'PK';
            if ($wvDtl->act_piece_qty + $actQty > $wvDtl->piece_qty) {
                $msg = 'Pick qty greater than remaining qty of wavepick';
                return $this->_responseErrorMessage($msg);
            }

            if ($wvDtl->act_piece_qty + $actQty == $wvDtl->piece_qty) {
                $wvDtlSts = 'PD';

                // WAP-707 - [Outbound - Wavepick] Modify API update wavepick
                $this->_updateReleasedLocation([$loc->loc_id]);
            }
            $this->waveDtlModel->updateWaveDtl($wvDtlId, $actQty, $wvDtlSts, $this->userId);
            $data = $this->groupCartonsByLot($cartons);
            $odrCartons = $this->cartonModel->updateCartonByLocs($data['cartons']);

            $this->updateInventoryByLot($actQty, $wvDtl);

            $this->wavePickDtlLocModel->updateWaveDtlLoc($actLoc, $wvDtl->wv_id, $wvDtlId);

            $data = $this->orderDtlModel->insertOrderCartons($wvDtl, $odrCartons);

            $skuInfo = '%s-%s-%s-%s-%s';
            $skuInfo = sprintf($skuInfo, $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['pack_size'], $wvDtl['lot']);
            $eventDatas = [
                'evt_code'   => 'UWP',
                'info'       => sprintf('WAP - %d Carton(s) Barcode, belonged to %s, were picked in Location %s',ceil($actQty / $wvDtl['pack_size']), $skuInfo, $locCode),
                'owner'      => $wvDtl['wv_num'],
                'trans_num'  => $wvDtl['wv_num'],
                'whs_id'     => $whs_id,
                'cus_id'     => $wvDtl->cus_id,
                'created_at' => time(),
                'created_by' => JWTUtil::getPayloadValue('jti') ?: 1
            ];
            if(ceil($actQty / $wvDtl['pack_size']) > 0){
                $this->eventTrackingModel->create($eventDatas);
            }

            $this->palletModel->updatePalletCtnTtl([$loc->loc_id]);
            $this->palletModel->updateZeroPallet([$loc->loc_id]);
            //[Kien vu] Doesn't implement this feature on WMS360 yet
            //$this->palletModel->removeLocDynZone($loc->loc_id);

            //update sts
            // $autoPack = $this->orderHdrModel->updateOrderPickedByWv($wvDtl->wv_id);

            $this->waveHdrModel->updateWvPicking($wvDtl->wv_id, $this->userId);

            // $this->waveHdrModel->updateWvComplete($wvDtl->wv_id);

            DB::commit();
            // $this->dispatch(new AutoPackJob($wvDtl->wv_id, $request));

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    private function updateInventoryByLot($pickedQty, $cond)
    {
        $chkInvt = DB::table('invt_smr')
            ->where([
                'whs_id'  => $cond['whs_id'],
                'item_id' => $cond['item_id'],
                'lot'     => $cond['lot']
            ])
            ->first();
        $currentQty = array_get($chkInvt, 'allocated_qty');
        // if ($currentQty< $pickedQty) {
        //     $msg = sprintf("Not enough allocated Qty to pick. Current allocated Qty: %s; Picked Qty: %s", $currentQty, $pickedQty) ;
        //     throw new \Exception($msg);
        // }
        $sqlPickQty = sprintf('`picked_qty` + %d', $pickedQty);
        $sqlAvailQty = sprintf('IF(`allocated_qty` < %d, 0, `allocated_qty` - %d)', $pickedQty, $pickedQty);
        $res = DB::table('invt_smr')
            ->where([
                'whs_id'  => $cond['whs_id'],
                'item_id' => $cond['item_id'],
                'lot'     => $cond['lot']
            ])
            ->update([
                    'picked_qty'    => DB::raw($sqlPickQty),
                    'allocated_qty' => DB::raw($sqlAvailQty),
                ]
            );

        return $res;
    }

    private function groupCartonsByLot($locs)
    {
        $lots = [];
        $cartons = [];
        foreach ($locs as $carton) {
            if (!isset($lots[$carton['lot']])) {
                $lots[$carton['lot']] = 0;
            }
            $lots[$carton['lot']] += $carton['picked_qty'];
            $cartons[] = $carton;
        }

        return [
            'cartons' => $cartons,
            'lots'    => $lots
        ];
    }

    private function _isAutoAssignToOrder($wvDtlObj)
    {
        $countOdr = $this->orderHdrModel->getModel()
            ->where('wv_id', $wvDtlObj->wv_id)
            ->whereNull('org_odr_id')
            ->count();

        if ($countOdr <= 1) {
            return true;
        }
        return false;
    }

    private function _updateReleasedLocation($locIds)
    {
        return $this->locationModel->getModel()
            ->whereIn('loc_id', $locIds)
            // ->where('loc_sts_code', self::STATUS_ALLOCATE_LOCATION)
            ->update([
                // 'loc_sts_code' => 'AC',
                'reserved_at'  => 0
            ]);
    }
}

