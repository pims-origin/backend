<?php

namespace Seldat\Wms2\Models;

class BOLOrderDetail extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bol_odr_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_id';

    public $timestamps = false;
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'odr_id',
        'ship_id',
        'cus_po',
        'ctn_qty',
        'piece_qty',
        'weight',
        'plt_qty',
        'cus_dept',
        'cus_ticket',
        'cus_odr',
        'add_shipper_info',
        'odr_hdr_num',
        'cube_qty_ttl',
        'vol_qty_ttl'

    ];

    public function shipment()
    {
        return $this->hasOne(__NAMESPACE__ . '\Shipment', 'ship_id', 'ship_id');
    }
}
