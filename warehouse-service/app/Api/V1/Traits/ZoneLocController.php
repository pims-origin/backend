<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/10/2016
 * Time: 11:16 AM
 */

namespace App\Api\V1\Traits;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use GuzzleHttp\Psr7\Response;

trait ZoneLocController
{
    public function index()
    {
        //same with get location by warehose
        return [];
    }

    public function show($warehouseId, $id)
    {
        $result = $this->service->getDetail($warehouseId, $id);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function store(IRequest $request, $warehouseId)
    {
        $input = $request->getParsedBody();
        $result = $this->service->create($input, $warehouseId);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }
}