<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\OrderHdr;

use League\Fractal\TransformerAbstract;

class OpenOrdersTransformer extends TransformerAbstract
{
    public $today = 0;

    public function transform(OrderHdr $order)
    {
        $info = $order->toArray();

        $sku_count = $plt_count = $ctn_ttl = $pcs_ttl = 0;

        if ($info) {

            $detailData = reset($info['details']);

            $sku_count = array_get($detailData, 'sku_count', 0);
            $ctn_ttl = array_get($detailData, 'ctn_ttl', 0);
            $pcs_ttl = array_get($detailData, 'pcs_ttl', 0);

            if ($detailData['order_hdr']) {

                $palletData = reset($detailData['order_hdr']['pack_hdr']);

                $plt_count = array_get($palletData, 'plt_count', 0);
            }
        }

        $updateDate = object_get($order, 'openOrder.created_at', '');

        return [
            'ord_id' => $order->odr_id,
            'whs_name' => object_get($order, 'warehouse.whs_name', ''),
            'cus_name' => object_get($order, 'customer.cus_name', ''),
            'odr_num' => $order->odr_num,
            'cus_odr_num' => $order->cus_odr_num,
            'upd_sts' => object_get($order, 'openOrder.sts', 'Click to edit'),
            'upd_user' => trim(object_get($order, 'openOrder.csrUser.first_name', NULL)
                    . ' ' . object_get($order, 'openOrder.csrUser.last_name', NULL)),
            'upd_date' => $updateDate ? date('m/d/Y', $updateDate) : NULL,
            'odr_type' => $order->odr_type,
            'open_ord_sts' => $order->odr_type,
            'user_name' => trim(object_get($order, 'csrUser.first_name', NULL)
                    . ' ' . object_get($order, 'csrUser.last_name', NULL)),
            'created_at' => date('m/d/Y', strtotime($order->created_at)),
            'sku_count' => $sku_count,
            'plt_count' => $plt_count,
            'ctn_ttl' => $ctn_ttl,
            'pcs_ttl' => $pcs_ttl,
            'cancel_by_dt' => date('m/d/Y', max($order->cancel_by_dt, $this->today)),
        ];
    }

    /*
    ***************************************************************************
    */

}
