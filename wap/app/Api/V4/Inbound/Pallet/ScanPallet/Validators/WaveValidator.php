<?php

namespace App\Api\V4\Inbound\Pallet\ScanPallet\Validators;


class WaveValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'whs_id' => 'required|integer|exists:warehouse,whs_id'
        ];

    }


}
