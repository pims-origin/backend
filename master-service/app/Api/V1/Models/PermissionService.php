<?php
namespace App\Api\V1\Models;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use GuzzleHttp\Exception\RequestException;

class PermissionService extends BaseService
{
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_AUTHORIZATION');
        parent::__construct($request, $api);
    }

    public function getPermissionListByQueryString($queryStr)
    {
        $uri = 'permissions';
        $uri = sprintf('%s?%s', $uri, $queryStr);

        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }
    
    public function getPermissionByUser($id)
    {
        $uri = 'users/' . $id . '/permissions';
    
        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }
    
    public function getGrpList($userId)
    {
        
        $uri = 'users/' . $userId . '/groups';

        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function getUserByPermission($permission)
    {
        $uri = 'permission/' . $permission . '/users';

        try {
            $response = $this->client->get($uri);
            
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }
}
