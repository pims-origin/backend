<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\VirtualCartonSumModel;
use App\Api\V1\Transformers\ReceivingGateTransformer;
use Swagger\Annotations as SWG;

class VirtualCartonSumController extends AbstractController
{
    protected $vtlCtnSum;
    protected $receivingGateTrans;
    public function __construct()
    {
        $this->vtlCtnSum = new VirtualCartonSumModel();
        $this->receivingGateTrans = new ReceivingGateTransformer();
    }

    /**
     * @param $whsId
     * @return mixed
     * author:cuongnguyen
     */
    public function getReceivingGates($whsId) {
        $data = $this->vtlCtnSum->getAllReceivingGates($whsId);
        return $data;
    }
}
