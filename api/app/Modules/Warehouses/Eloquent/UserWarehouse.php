<?php
namespace App\Modules\Warehouses\Eloquent;

use Illuminate\Database\Eloquent\Model;

class UserWarehouse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_whs';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'whs_id',
    ];
}
