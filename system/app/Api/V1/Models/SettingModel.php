<?php

namespace App\Api\V1\Models;

use App\Setting;
use App\Api\V1\Validators\MenuValidator;

class SettingModel extends AbstractModel
{
    /**
     * @var string
     */
    private $keyColumn = 'setting_key';

    /**
     * @var QueryBuilder
     */
    protected $model;

    /**
     * @param Menu $model
     */
    public function __construct(Setting $model = null)
    {
        $this->model = ($model) ?: new Setting();
    }

    public function getSettingByKey($value, array $with = [])
    {
        return $this->getFirstBy($this->keyColumn, $value, $with);
    }

    public function updateSettingByKey($key, $data)
    {
        $json = json_encode($data);
        $setting = $this->getSettingByKey($key);
        if (null === $setting) {
            return null;
        }
        $setting->setting_value = $json;
        $setting->save();

        return $setting;
    }
}
