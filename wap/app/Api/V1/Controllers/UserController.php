<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\Log;
use App\Api\V1\Models\UserModel;
use Dingo\Api\Http\Response;
use App\MyHelper;
use Psr\Http\Message\ServerRequestInterface as Request;

class UserController extends AbstractController
{

    protected $userModel;
    /**
     * WavePickController constructor.
     */
    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    public function getUsersByWarehouseID ($whsId, Request $request) {

        /*
        * start logs
        */
        $url = "/$whsId/users";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'UWI',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get user by warehouse id'
        ]);
        /*
        * end logs
        */
            
        $userObj = $this->userModel->getListUserOfWH($whsId);

        if (empty($userObj)) {
            $msg = sprintf('This warehouse has no user.');

            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }
        $data = [];
        foreach ($userObj as $item) {
            $tmp = [];
            $tmp['first_name'] =$item->first_name;
            $tmp['last_name'] =$item->last_name;
            $tmp['email'] =$item->email;
            $tmp['username'] =$item->username;
            array_push($data, $tmp);
        }
        $data = [
            'data' => $data,
            'status' => true,
        ];
        return new Response($data, 200, [], null);
    }
}
