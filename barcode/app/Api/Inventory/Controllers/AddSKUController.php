<?php

namespace App\Api\Inventory\Controllers;

use App\Api\Inventory\Models\AddSKUModel;
use App\Api\Inventory\Validators\AddSKUValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\Inventory\Transformers\CycleDtlTransformer;
use Swagger\Annotations as SWG;
use App\Api\Inventory\Models\EventTrackingModel;

class AddSKUController extends AbstractController
{
    protected $model;
    protected $validator;
    protected $request;
    protected $eventTrackingModel;
    public $errors = [];

    /**
     * AddSKUController constructor.
     * @param AddSKUModel $model
     * @param AddSKUValidator $validator
     * @param Request $request
     * @param EventTrackingModel $eventTrackingModel
     */

    public function __construct(
        AddSKUModel $model,
        AddSKUValidator $validator,
        Request $request,
        EventTrackingModel $eventTrackingModel
    )
    {
        $this->model = $model;
        $this->validator = $validator;
        $this->request = $request;
        $this->eventTrackingModel = $eventTrackingModel;
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response|void
     */

    public function store(Request $request, $cycle_id)
    {
        $input = $request->getParsedBody();
        $input['cycle_hdr_id'] = $cycle_id;

        $this->validator->validate($input);

        $data = $this->model->processAddSKU($input);

        if (! $data) {
            return $this->response->errorBadRequest(implode($this->model->errors));
        }

        //event tracking
        $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingTransNum(
            $cycle_id,
            EventTrackingModel::CYCLE_ADD_SKU
        );
        $this->eventTrackingModel->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => isset($data['cus_id']) ? $data['cus_id'] : null,
            'owner'     => $cycleNumAndSeq['cycle_num'],
            'evt_code'  => EventTrackingModel::CYCLE_ADD_SKU,
            'trans_num' => $cycleNumAndSeq['trans_num'],
            'info'      => EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_ADD_SKU] . ' ' .
                $cycleNumAndSeq['trans_num'] . ' for ' . $cycleNumAndSeq['cycle_num']

        ]);

        return $this->response->item($data, new CycleDtlTransformer());
    }
}