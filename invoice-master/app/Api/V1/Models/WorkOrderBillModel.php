<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\BillableHdrModel;
use App\Api\V1\Models\BillableDtlModel;
use App\VasHdr;
use App\VasDtl;
use App\Api\V1\Models\CustomerSacModel;
use Illuminate\Support\Facades\DB;

class WorkOrderBillModel extends AbstractModel
{
    protected $cusId;
    protected $whsId;
    protected $type;
    protected $quantity;
    protected $amount;
    protected $billableHdr;
    protected $billableDtl;
    protected $vasHdr;
    protected $vasDtl;

    public function __construct(VasHdr $vasHdr)
    {
        $this->cusId = $vasHdr->cus_id;
        $this->whsId = $vasHdr->whs_id;
        $this->refNum = $vasHdr->vas_num;
        $this->type = 'WO'; // Work Order
        $this->vasHdr = $vasHdr;
        $this->vasDtl = $vasHdr->vasDtl()->get();
        $this->billableHdr = new BillableHdrModel();
        $this->billableDtl = new BillableDtlModel();
    }

    public function prepareDetailData()
    {        
        $detail[1] = [
            'ba_id' => '',
            'cus_id' => $this->cusId,
            'whs_id' => $this->whsId,
            'ba_type' => $this->type,
            'sac_id' => 0,
            'sac' => '',
            'sac_name' => '',
            'ref_num' => $this->refNum,
            'item' => '',
            'description' => '',
            'qty' => 1,
            'unit_price' => $this->vasHdr->amount_ttl,
            'discount' => 0,
            'amount' => $this->vasHdr->amount_ttl
        ];
        $this->quantity += $detail[1]['qty'];
        $this->amount += $detail[1]['amount'];

        return $detail;
    }

    public function prepareHeaderData()
    {
        # billable header
        return [
            'cus_id' => $this->cusId,
            'whs_id' => $this->whsId,
            'ref_num' => $this->refNum, // vas_num
            'ba_from' => time(),
            'ba_to' => time(),
            'ba_ttl' => $this->quantity,
            'amount_ttl' => $this->amount,
            'ba_type' => $this->type,
            'ba_period' => 'DAY',
        ];
    }
    
    public function saveBillableItem()
    {
        $detail = $this->prepareDetailData();
        if (!empty($detail)) {
            $headerData = $this->prepareHeaderData();
            return DB::transaction(function () use ($detail, $headerData) {
                $header = $this->billableHdr->create($headerData);
                $header->detail()->createMany($detail);
                return $header;
            });
        }
    }
}
