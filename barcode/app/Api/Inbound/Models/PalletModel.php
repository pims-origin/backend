<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\Inbound\Models;

use Dingo\Api\Exception\UnknownVersionException;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class PalletModel extends AbstractModel
{

    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
    }

    public function validatePLTFormat($plt_num){
        return (bool)preg_match("/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num);
    }

    public function validateToTeFormat($plt_num){
        return (bool)preg_match("/^T-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num);
    }

    public function validateVirtualFormat($plt_num){
        return (bool)preg_match("/^V-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num);
    }
}
