<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemState;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class SystemStateModel extends AbstractModel
{
    public function __construct(SystemState $model = null)
    {
        $this->model = ($model) ?: new SystemState();
    }

    public function search($attributes = [], $limit = PAGING_LIMIT)
    {
        $query = $this->getModel()->leftJoin('system_country', 'system_state.sys_state_country_id', 
            '=', 'system_country.sys_country_id');
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'sys_state_name' || $key === 'sys_state_code' || $key === 'sys_state_desc') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                } else if ($key === 'sys_state_id' || $key === 'sys_state_country_id') {
                    $query->where($key, $value);
                }
            }
        }

        //Implement sort builder for api
        //usage: ?sort[name]=desc&sort[title]=asc
        $this->sortBuilder($query, $attributes);
        if (isset($attributes['sort']['sys_country_name'])) {
            $query->orderBy('sys_country_name', $attributes['sort']['sys_country_name']);
        }
        
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    public function getStateInCountryByCode($code, $countryId)
    {
        return $this->model
            ->where('sys_state_country_id', $countryId)
            ->where('sys_state_code', $code)
            ->first();
    }

    public function getStateInCountryByName($name, $countryId)
    {
        return $this->model
            ->where('sys_state_country_id', $countryId)
            ->where('sys_state_name', $name)
            ->first();
    }
}
