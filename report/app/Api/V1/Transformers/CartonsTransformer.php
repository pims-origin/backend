<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Inventory;
use Seldat\Wms2\Models\Carton;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Utils\Status;

class CartonsTransformer extends TransformerAbstract
{
    public function transform(Carton $carton)
    {
        $expiredDt = '';
        $expired_dt = $carton->expired_dt;
        if ($expired_dt != 0 && $expired_dt != null)
        {
            $expiredDt = date("m/d/Y", $expired_dt);
        }

        $location = \DB::table('location')
            ->where('loc_whs_id', object_get($carton, 'whs_id'))
            ->where('is_block_stack', 1)
            ->whereNull('parent_id')
            ->where('parent_id', object_get($carton, 'loc_id'))
            ->first();

        return [
            'ctn_id'        => $carton->ctn_id,
            'cus_name'      => object_get($carton, 'customer.cus_name', ''),
            'cus_code'      => object_get($carton, 'customer.cus_code', ''),
            'ctnr_num'      => $carton->ctnr_num,
            'gr_hdr_num'    => object_get($carton, 'goodReceiptDtl.goodsReceipt.gr_hdr_num', ''),
            'piece_remain'  => $carton->piece_remain,
            'sys_mea_name'  => object_get($carton, 'AsnDtl.AsnHdr.SystemMeasurement.sys_mea_name', ''),
            'upc'           => $carton->upc,
            'sku'           => $carton->sku,
            'size'          => $carton->size,
            'color'         => $carton->color,
            'po'            => $carton->po,
            'lot'           => $carton->lot,
            'length'        => round($carton->length, 2),
            'width'         => round($carton->width, 2),
            'height'        => round($carton->height, 2),
            'weight'        => round($carton->weight, 1),
            'ctn_pack_size' => $carton->ctn_pack_size,
            'ctn_num'       => $carton->ctn_num,
            'rfid'          => $carton->rfid,
            'plt_num'       => object_get($carton, 'pallet.plt_num', ''),
            'plt_rfid'      => object_get($carton, 'pallet.rfid', ''),
            'loc_name'      => empty($location) ? object_get($carton, 'loc_name') : $location['loc_alternative_name'],
            'loc_code'      => empty($location) ? object_get($carton, 'loc_code') : $location['loc_code'],
            'ctn_sts'       => Status::getByValue($carton->ctn_sts, 'CTN_STATUS'),
            'created_at'    => $carton->created_at->format('m/d/Y H:i:s'),
            'expired_dt'    => $expiredDt,
            'cube'          => round($carton->cube, 2),
            'volume'        => round($carton->volume, 2),
            'row'           => object_get($carton, 'row', ''),
            'level'         => object_get($carton, 'level', ''),
            'aisle'         => object_get($carton, 'aisle', '')
        ];
    }

    /*
    ****************************************************************************
    */

}
