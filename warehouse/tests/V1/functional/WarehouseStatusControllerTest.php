<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 30-May-16
 * Time: 09:08
 */

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\WarehouseModel;
use Illuminate\Http\Response as IlluminateResponse;

class WarehouseStatusControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $endpointWarehouse;

    /**
     * WarehouseControllerCest constructor.
     */
    public function __construct()
    {
        $this->endpointWarehouseStatus = '/v1/warehouses/statuses';
    }

    public function testSearch_NoParam_Ok()
    {
        $response = $this->call('GET', $this->endpointWarehouseStatus);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is not null
        $this->assertNotEmpty($responseData['data']);

        // Case 3: Data has 3 records.
        //$this->assertCount(3, $responseData['data']);
    }

    public function testSearch_WithParam_Ok()
    {
        $this->call('GET', $this->endpointWarehouseStatus, [
            'whs_sts_code' => 'code',
            'whs_sts_desc' => 'desc',
            'sort' => [
                'whs_sts_code' => 'desc',
                'whs_sts_desc' => 'asc'
            ],
        ]);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }
}