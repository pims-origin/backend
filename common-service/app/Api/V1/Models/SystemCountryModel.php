<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemCountry;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class SystemCountryModel extends AbstractModel
{
    public function __construct(SystemCountry $model = null)
    {
        $this->model = ($model) ?: new SystemCountry();
    }

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'sys_country_name' || $key === 'sys_country_code') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                } else if ($key === 'sys_country_id') {
                    $query->where($key, $value);
                }
            }
        }

        //Implement sort builder for api
        //usage: ?sort[name]=desc&sort[title]=asc
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }
}
