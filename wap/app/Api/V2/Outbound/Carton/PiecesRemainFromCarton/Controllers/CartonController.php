<?php

namespace App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Controllers;

use App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Models\CartonModel;
use App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Models\OrderDtlModel;
use App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Models\WavepickDtlModel;
use App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Models\OrderCartonModel;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Models\CustomerWarehouse;
use App\Api\V1\Models\Log;
use GuzzleHttp\Client;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\OrderDtl;

class CartonController extends AbstractController
{
    private $carton;
    private $orderCarton;
    private $odrDtl;
    private $wavepickDetail;
    private $wavepickHdr;
    private $wavepickDtl;
    private $orderHdr;
    private $orderDtl;
    /**
     * CartonController constructor.
     * @param CartonModel $ctn
     */
    public function __construct(CartonModel $ctn, OrderCartonModel $odrCtn, OrderDtlModel $odrDtl, WavepickDtlModel $wavepickDetail, WavepickHdr $wavepickHdr, WavepickDtl $wavepickDtl, OrderHdr $orderHdr, OrderDtl $orderDtl)
    {
        $this->carton = $ctn;
        $this->orderCarton = $odrCtn;
        $this->odrDtl = $odrDtl;
        $this->wavepickDetail = $wavepickDetail;
        $this->wavepickHdr = ($wavepickHdr) ?: new WavepickHdr();
        $this->wavepickDtl = ($wavepickDtl) ?: new WavepickDtl();
        $this->orderHdr = ($orderHdr) ?: new OrderHdr();
        $this->orderDtl = ($orderDtl) ?: new OrderDtl();

    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    /**
     * @param $whsId
     * @param $waveDetailId
     * @param $cartonRfid
     * @param Request $request
     * @return array|string
     */
    public function getPiecesRemain($whsId, $waveDetailId, $cartonRfid, Request $request)
    {
        $owner = $transaction = "";
        $urlEndpoint = "/v2/whs/{$whsId}/wave-detail/{$waveDetailId}/{$cartonRfid}";
        $apiName = 'Get piece remain from carton';
        Log:: info($request, $whsId, [
            'evt_code'     => 'GPR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $urlEndpoint,
            'message'      => $apiName
        ]);

        // Check exit carton and belong warehouse
        $isBelong = $this->carton->getFirstWhere([
            'whs_id'   => $whsId,
            'rfid'     => $cartonRfid
        ]);
        if (!$isBelong) {
            $msg = "Carton doesn't exist";
            $isExist = $this->carton->getFirstWhere(['rfid' => $cartonRfid]);
            if ($isExist) {
                $msg = "Carton doesn't belong to warehouse";
            }
            return $this->_responseErrorMessage($msg);
        }

        // Check exit wavepick detail
        $wvIsExist = $this->wavepickDetail->getFirstWhere(['wv_dtl_id' => $waveDetailId]);
        if (!$wvIsExist) {
            $msg = "Wavepick detail doesn't exist";
            return $this->_responseErrorMessage($msg);
        }

        // Check carton belong to wavepich
        $ctnIsBelongWv = DB::table('odr_cartons')
            ->where('deleted', 0)
            ->where('deleted_at', 915148800)
            ->where('wv_dtl_id', $wvIsExist['wv_dtl_id'])
            ->where('ctn_id', $isBelong['ctn_id'])->first();
        if ($ctnIsBelongWv) {
            $msg = "Carton doesn't belong to wavepick";
            return $this->_responseErrorMessage($msg);
        }

        // Get
        $pieceRemain = DB::table('cartons')
            ->join('wv_dtl', function($join)
            {
                $join->on('cartons.item_id', '=', 'wv_dtl.item_id')
                     ->on('cartons.sku', '=', 'wv_dtl.sku');

            })
            ->where('cartons.deleted', 0)
            ->where('cartons.deleted_at', 915148800)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.rfid', $cartonRfid)
            ->where('wv_dtl.wv_dtl_id', $waveDetailId)->first();
        $data = array_get($pieceRemain, 'piece_remain', 0);

        // Return data
        $msg = [];
        $msg['status'] = true;
        $msg['iat'] = time();
        $msg['messages'] = [[
            'status_code' => 1,
            'msg' => sprintf("Successfully!")
        ]];
        $msg['data'] = [
            'piece_remain' => $data
        ];

        return $msg;
    }

    public function splitCartonTag($whsId, $wvDtlId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $owner = $transaction = "";
        $urlEndpoint = "/v2/whs/{$whsId}/carton/split-carton-tag";
        $apiName = 'Set piece remain Carton';
        Log:: info($request, $whsId, [
            'evt_code'     => 'SPR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $urlEndpoint,
            'message'      => $apiName
        ]);

        $ctnRfid = array_get($input, 'ctn_rfid', null);
        $pieceRemain = array_get($input, 'piece_remain', 0);

        // Check exit carton and belong warehouse
        $isBelong = $this->carton->getFirstWhere([
            'whs_id'   => $whsId,
            'rfid'     => $ctnRfid
        ]);
        if (!$isBelong) {
            $msg = "Carton doesn't exist";
            $isExist = $this->carton->getFirstWhere(['rfid' => $ctnRfid]);
            if ($isExist) {
                $msg = "Carton doesn't belong to warehouse";
            }
            return $this->_responseErrorMessage($msg);
        }

        // Check carton pack size >= piece remain
        $ctnExist = $this->carton->getFirstWhere([
            'rfid'     => $ctnRfid
        ]);
        if ($ctnExist) {
            if($ctnExist['ctn_pack_size'] < $pieceRemain){
                $msg = "Not allow piece remain bigger more than carton pack size.";
                return $this->_responseErrorMessage($msg);
            }
        }

        $wvDtl = WavepickDtl::find($wvDtlId);
        if (!$wvDtl){
            $msg = "Wavepick doesn't exits.";
            return $this->_responseErrorMessage($msg);
        }

        //Get Info about carton from wvdtl
        $odrDtl = DB::table('odr_dtl')->where([
            'wv_id'     => $wvDtl->wv_id,
            'item_id'   => $ctnExist->item_id,
            'deleted'   => 0
        ])->first();
        if (!$odrDtl){
            $msg = "Order detail doesn't exits.";
            return $this->_responseErrorMessage($msg);
        }

        // Process
        try {
            DB::beginTransaction();
            $this->carton->updateWhere(
                [
                    'piece_remain' => $ctnExist->piece_remain - $pieceRemain
                ],
                [
                    'rfid' => $ctnRfid
                ]
            );

            // create new carton with pice_remain = pice_remain
            $newCarton = $this->carton->cloneCarton($ctnExist);
            $newCarton->piece_remain = $pieceRemain;
            $newCarton->save();

            // assign new carton to order
            $newOrderCarton = [
                'odr_hdr_id' => array_get($odrDtl, 'odr_id', ''),
                'odr_dtl_id' => array_get($odrDtl, 'odr_dtl_id', ''),
                'wv_hdr_id' => array_get($odrDtl, 'wv_id', ''),
                'wv_dtl_id' => $wvDtlId,
                'piece_qty' => $newCarton->piece_remain,
                'ctn_id' => $newCarton->ctn_id,
                'item_id' => $newCarton->item_id,
                'sku' => $newCarton->sku,
                'size' => $newCarton->size,
                'color' => $newCarton->color,
                'lot' => $newCarton->lot,
                'upc' => $newCarton->upc,
                'pack' => $newCarton->ctn_pack_size,
                'uom_id' => $newCarton->ctn_uom_id,
                'uom_code' => $newCarton->uom_code,
                'uom_name' => $newCarton->uom_name,
                'loc_id' => $newCarton->loc_id,
                'loc_code' => $newCarton->loc_code,
                'length' => $newCarton->length,
                'width' => $newCarton->width,
                'height' => $newCarton->height,
                'weight' => $newCarton->weight,
                'volume' => $newCarton->volume,
                'plt_id' => $newCarton->plt_id,
                'updated_by' => Data::getCurrentUserId(),
                'updated_at' => time(),
                'created_by' => Data::getCurrentUserId(),
                'created_at' => time(),
                'deleted_at' => 915148800,
                'deleted' => 0,
                'ctn_num' => $newCarton->ctn_num . '-1',
                'ctn_sts'    => 'PD',
                'sts'        => 'I'
            ];
            DB::table('odr_cartons')->insert($newOrderCarton);

            // WMS2-6335
            $newCarton->ctn_sts = 'PD';
            $newCarton->loc_id = null;
            $newCarton->loc_code = null;
            $newCarton->save();

            // act_piece_qty
            $wvDtlSts = 'PK';
            if ($wvDtl->act_piece_qty + $pieceRemain == $wvDtl->piece_qty) {
                $wvDtlSts = 'PD';
            }
            $sqlUpdatePieceWv = sprintf('act_piece_qty + %d', $pieceRemain);
            $this->wavepickDetail->updateWhere(
                [
                    'act_piece_qty' => DB::raw($sqlUpdatePieceWv),
                    'wv_dtl_sts'    => $wvDtlSts ,
                    'updated_by'    => Data::getCurrentUserId(),
                    'updated_at'    => time()
                ],
                [
                    'wv_dtl_id' => $wvDtlId
                ]
            );

            // picked_qty
            $odr_dtl_id = array_get($odrDtl, 'odr_dtl_id', '');
            $odr_hdr_id = array_get($odrDtl, 'odr_id', '');
            $sqlUpdatePieceOdrDtl = sprintf('picked_qty + %d', $pieceRemain);
            $orderDtlStatus = 'PK';
            if(array_get($odrDtl, 'alloc_qty', 0) == array_get($odrDtl, 'picked_qty', 0) + $pieceRemain){
                if(array_get($odrDtl, 'alloc_qty', 0) != 0){
                    $orderDtlStatus = 'PD';
                }
            }
            $this->odrDtl->updateWhere(
                [
                    'picked_qty' => DB::raw($sqlUpdatePieceOdrDtl),
                    'itm_sts'     => $orderDtlStatus,
                    'updated_by' => Data::getCurrentUserId(),
                    'updated_at' => time()
                ],
                [
                    'odr_dtl_id' => $odr_dtl_id
                ]
            );

            // Update wv_hdr status
            $this->wavepickHdr
                ->where('wv_id', array_get($odrDtl, 'wv_id', ''))
                ->update([
                    'wv_sts'     => 'PK',
                    'updated_by' => Data::getCurrentUserId(),
                    'updated_at' => time(),
                ]);

            // Update Odr_hdr
            $orderHdrStatus = 'PK';
            $this->orderHdr
                ->where('odr_id', $odr_hdr_id)
                ->update([
                    'odr_sts'     => $orderHdrStatus,
                    'updated_by' => Data::getCurrentUserId(),
                    'updated_at' => time(),
                ]);
            $sql = "(SELECT COUNT(1) FROM odr_dtl WHERE odr_dtl.odr_id = odr_hdr.odr_id AND odr_dtl.itm_sts != 'PD' AND deleted =0  AND alloc_qty > 0) = 0";
            $this->orderHdr
                ->where('odr_id', $odr_hdr_id)
                ->where('odr_sts', 'PK')
                ->whereRaw(DB::raw($sql))->update(["odr_sts" => 'PD']);

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => sprintf('Successfully!')
            ]];
            $msg['data'] = [];

            return $msg;

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $urlEndpoint,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return $this->_responseErrorMessage($this->getResponseData());
        }
    }
}
