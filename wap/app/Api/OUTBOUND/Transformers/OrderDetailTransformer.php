<?php

namespace App\Api\OUTBOUND\Transformers;

use App\Api\OUTBOUND\Models\OrderHdrModel;
use App\Api\OUTBOUND\Models\OrderCartonModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\Status;

class OrderDetailTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(OrderDtl $orderDtl)
    {
        $ordHdr = new OrderHdrModel();
        $item = $ordHdr->getFirstWhere(['odr_id' => $orderDtl->odr_id]);
        $ordCtn = new OrderCartonModel();

        $sts = $orderDtl->itm_sts;
        if ($sts == null) {
            $sts = 'NW';
        }
        //get available carton piece_qty where odr_dtl_id = null and ctn_sts = Picking
        $cartonOrd = $ordCtn->getOrderCT($orderDtl->odr_dtl_id);
        $cartonOrd = $cartonOrd->toArray();
        $ctnOrd = [];
        $dtCtn = [];
        foreach ($cartonOrd as $dt) {
            $dtCtn['ctn_id'] = $dt['ctn_id'];
            $dtCtn['ctn_rfid'] = $dt['ctn_rfid'];
            $dtCtn['ctn_num'] = $dt['ctn_num'];

            //calculator total piece, actual piece

            $act = $ordCtn->findWhere([
                'odr_dtl_id' => $dt['odr_dtl_id'],
                'ctn_id'     => $dt['ctn_id'],
                'ctn_sts'    => Status::getByValue('Picked', 'ORDER-STATUS')
            ]);
            $numPD = 0;
            $numPK = 0;
            if (!empty($act)) {
                $act = $act->toArray();
                foreach ($act as $t) {
                    $numPD = $numPD + $t['piece_qty'];
                }
            }

            $notCtnAs = $ordCtn->findWhere([
                'ctn_id'  => $dt['ctn_id'],
                'ctn_sts' => Status::getByValue('Picking', 'ORDER-STATUS')
            ]);
            if (!empty($notCtnAs)) {
                $notCtnAs = $notCtnAs->toArray();
                foreach ($notCtnAs as $n) {
                    $numPK = $numPK + $n['piece_qty'];
                }
            }

            $dtCtn['piece_picking'] = $numPK;
            $dtCtn['piece_picked'] = $numPD;
            $dtCtn['piece_qty'] = ($numPK + $numPD) . '/' . $numPD;

            $sts = Status::getByValue('Picking', 'ORDER-STATUS');
            if ($numPK == 0) {
                $sts = Status::getByValue('Picked', 'ORDER-STATUS');
            }

            $dtCtn['ctn_sts'] = $ordCtn->formatStatus('order', $sts);
            $ctnOrd[] = $dtCtn;
            //$dtCtn['ctn_sts'] = $dt['ctn_id'];
        }


        return [
            'odr_id'       => $orderDtl->odr_id,
            'odr_num'      => $item->odr_num,
            'wv_id'        => $item->wv_id,
            'wv_num'       => $item->wv_num,
            'whs_id'       => $item->whs_id,
            'ord_dtl_id'   => $orderDtl->odr_dtl_id,
            'sku'          => $orderDtl->sku,
            'size'         => $orderDtl->size,
            'color'        => $orderDtl->color,
            'pack'         => $orderDtl->pack,
            'itm_sts'      => $sts,
            'itm_sts_name' => Status::getByKey('ORDER-STATUS', $sts),
            'item_id'      => $orderDtl->item_id,
            'piece_qty'    => $orderDtl->piece_qty,
            'alloc_qty'    => $orderDtl->alloc_qty,
            'ctn_detail'   => $ctnOrd
        ];

    }
}
