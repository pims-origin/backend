<?php

namespace App\Api\Ecom\Controllers;

use App\Api\Ecom\Models\PackDtlModel;
use App\Api\Ecom\Models\ToteModel;
use App\Api\Ecom\Transformers\Serializers\CustomSerializer;
use App\Api\Ecom\Transformers\ToteInfo\ToteInfoTransformer;
use App\Api\Ecom\Transformers\ToteTransformer;
use Illuminate\Http\Request;

class ToteController extends AbstractController
{
    public function __construct()
    {
        $this->toteModel = new ToteModel();
    }


    public function scan($whsId, Request $request)
    {
        try{
            $this->toteModel->validateToteNum($whsId, $request->input('tote_num'));

            $tote = $this->toteModel->scan($whsId, $request);

            return $this->response->item($tote, new ToteTransformer());
        }catch(\Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getInfo($whsId, $toteNum){
        try{
            $this->toteModel->validateToteNum($whsId, $toteNum);

            $tote = $this->toteModel->getFirst(['whs_id' => $whsId, 'tote_num' => $toteNum], ['odrHdrs.details', 'odrHdrs.waveHdr', 'odrHdrs.packHdr']);

            if (!$tote){
                return ['data' => []];
            }

            return $this->response->item($tote, new ToteInfoTransformer(), ['key' => 'data'], function($resource, $fractal){
                $fractal->setSerializer(new CustomSerializer());
                $fractal->parseIncludes(['odrHdrs.details']);
            });
        }catch(\Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}

