<?php

namespace App\Api\V1\Models;



use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WarehouseMeta;

class WarehouseMetaModel extends AbstractModel
{
    protected $model;

    public function __construct(WarehouseMeta $model = null)
    {
        $this->model = ($model) ?: new WarehouseMeta();
    }

    /**
     * @param int $warehouseId
     * @param string|array $qualifier
     *
     * @return mixed
     */
    public function deleteWarehouseMeta($warehouseId, $qualifier)
    {
        $qualifier = is_array($qualifier) ? $qualifier : [$qualifier];

        return $this->model
            ->where('whs_id', $warehouseId)
            ->whereIn('whs_qualifier', $qualifier)
            ->delete();
    }

    public function getWarehouseMeta($warehouseId, $qualifier)
    {
        $query = $this->model->where('whs_id', $warehouseId)
            ->where('whs_qualifier', $qualifier);
        //$this->model->filterData($query);
        return $query->first();
    }

    /**
     * @param $warehouseId
     * @param $qualifier
     *
     * @return mixed
     */
    public function getWarehouseConfigMeta($warehouseId, $qualifier)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('whs_meta')
            ->select([
                'whs_id',
                'whs_qualifier',
                'whs_meta_value'
            ])
            ->where('whs_id', $warehouseId);
          if(is_array($qualifier)) {
              $query->whereIn('whs_qualifier', $qualifier);
          }
          else {
              $query->where('whs_qualifier', $qualifier);
          }
          $query->where('deleted', 0);

        return $query->get();
    }

    public function updateWithoutReturnedModel(array $data)
    {
        return $this->model
            ->where('whs_qualifier', $data['whs_qualifier'])
            ->where('whs_id', $data['whs_id'])
            ->update(['whs_meta_value' => $data['whs_meta_value']]);
    }

    public function update(array $data)
    {
        $this->model
            ->where('whs_qualifier', $data['whs_qualifier'])
            ->where('whs_id', $data['whs_id'])
            ->update(['whs_meta_value' => $data['whs_meta_value']]);

        return $this->getWarehouseMeta($data['whs_id'], $data['whs_qualifier']);
    }

    public function getByWarehouseId($warehouseId)
    {
        return $this->allBy('whs_id', $warehouseId);
    }
}
