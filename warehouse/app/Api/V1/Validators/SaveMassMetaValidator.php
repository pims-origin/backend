<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 05/05/2016
 * Time: 15:21
 */

namespace App\Api\V1\Validators;


class SaveMassMetaValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_qualifier'   => 'required',
            'whs_qualifier.*' => 'required|exists:whs_qualifier,qualifier'

        ];
    }
}
