<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 22/July/2016
 * Time: 11:05
 */

namespace App\Api\V1\Validators;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\PalletModel;
use Seldat\Wms2\Utils\Message;

class ConsolidationPltValidator extends AbstractValidator
{
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var $consolidationLocValidator
     */
    protected $consolidationLocValidator;

    /**
     * ConsolidationPltValidator constructor.
     */
    public function __construct(
        PalletModel $palletModel,
        LocationModel $locationModel,
        ConsolidationLocValidator $consolidationLocValidator,
        CartonModel $cartonModel
    ) {
        $this->palletModel = $palletModel;
        $this->locationModel = $locationModel;
        $this->cartonModel = $cartonModel;
        $this->consolidationLocValidator = $consolidationLocValidator;
    }

    protected function rules()
    {
        return [
            'from_plt_id' => 'required',
            'to_plt_id'   => 'required',
        ];
    }

    public function check($toPltId, $ctn_ids)
    {
        // check exist of new pallet
        $palletInfo2 = $this->palletModel->getFirstWhere(['plt_id' => $toPltId]);
        if (empty($palletInfo2)) {
            throw new \Exception(Message::get("BM017", "New Pallet"));
        }

        // Load location (new location id) to get name..
        $toLocId = object_get($palletInfo2, 'loc_id', 0);
        $newLocationInfo = $this->locationModel->getFirstWhere(['loc_id' => $toLocId]);

        if (empty($newLocationInfo)) {
            throw new \Exception(Message::get("BM028"));
        }

        //check New location status
        $this->consolidationLocValidator->checkPalletStatus(object_get($newLocationInfo, 'loc_sts_code', null),
            "New Location");

        // check exist of carton
        $checkCartons = $this->cartonModel->checkExistedCartons($ctn_ids);
        if ($checkCartons == false) {
            throw new \Exception(Message::get("BM017", "carton(s)"));
        }

        return $newLocationInfo;
    }

    public function checkCurrentLoc($fromPltId)
    {
        // check exist of pallet
        $palletInfo = $this->palletModel->getFirstWhere(['plt_id' => $fromPltId]);
        if (empty($palletInfo)) {
            throw new \Exception(Message::get("BM017", "Current Pallet"));
        }

        // Load location (current location id) to get name..
        $fromLocId = object_get($palletInfo, 'loc_id', 0);
        $currentLocationInfo = $this->locationModel->getFirstWhere(['loc_id' => $fromLocId]);

        $result = [$currentLocationInfo, $palletInfo];

        return $result;
    }


}
