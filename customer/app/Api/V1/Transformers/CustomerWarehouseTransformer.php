<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerWarehouse;
use League\Fractal\TransformerAbstract;

class CustomerWarehouseTransformer extends TransformerAbstract
{
    public function transform(CustomerWarehouse $customerWarehouse)
    {
        return [
            'cus_id'              => $customerWarehouse->cus_id,
            'whs_id'              => $customerWarehouse->whs_id,
            'cus_code'            => object_get($customerWarehouse, 'customer.cus_code', null),
            'cus_billing_account' => object_get($customerWarehouse, 'customer.cus_billing_account', null),
            'cus_name'            => object_get($customerWarehouse, 'customer.cus_name', null),
            'cus_city_name'       => object_get($customerWarehouse, 'customer.cus_city_name', null),
            'cus_state_name'      => !empty($customerWarehouse->customer->systemState->sys_state_name)
                ? $customerWarehouse->customer->systemState->sys_state_name
                : null,
            'cus_postal_code'     => object_get($customerWarehouse, 'customer.cus_postal_code', null),
            'contact'             => object_get($customerWarehouse, 'customer.customerContact', null),
            'status'              => object_get($customerWarehouse, 'customer.customerStatus', null)
        ];
    }
}