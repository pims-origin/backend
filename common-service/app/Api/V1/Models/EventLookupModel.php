<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\EventLookup;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class EventLookupModel extends AbstractModel
{
    public function __construct(EventLookup $model = null)
    {
        $this->model = ($model) ?: new EventLookup();
    }

    /**
     * @param $attributes
     * @param $whs_id
     *
     * @return mixed
     */
    public function evtCodeDropDown($attributes, $whs_id)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        
        $query = DB::table('logs as l')
                ->leftJoin('evt_lookup as el', 'el.evt_code', '=', 'l.evt_code')
                ->select([
                    DB::raw('DISTINCT l.evt_code'),
                    'el.trans',
                    DB::raw('IF (el.des is null, l.evt_code, el.des) as des'),
                    'l.type',
                    ]);
        $query->orderBy('des');
        $query->where('l.whs_id', $whs_id);
        
        if(isset($attributes['type'])) {
            $query->where('l.type', $attributes['type']);
        } else {
            $query->whereIn('l.type', ['info', 'log', 'error']);
        }

        $row = $query->get();

        return $row;
    }


}
