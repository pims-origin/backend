<?php
/**
 * Created by PhpStorm.
 * User: PhucTran
 * Date: 3/17/2017
 * Time: 3:26 PM
 */

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VerifyController extends AbstractController
{
    protected $_request;

    /**
     * FileController constructor.
     */
    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function postVerify(){

        return response()->json([]);
    }
}