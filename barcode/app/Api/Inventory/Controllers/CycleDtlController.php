<?php

namespace App\Api\Inventory\Controllers;

use App\Api\Inventory\Models\CycleDtlModel;
use App\Api\Inventory\Models\CycleHdrModel;
use App\Api\Inventory\Models\LocationModel;
use App\Api\Inventory\Transformers\CycleDtlTransformer;
use App\Api\Inventory\Validators\CycleDtlValidator;
use App\Api\Inventory\Validators\UpdateActualCycleDtlValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\Inventory\Validators\CycleHdrValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\CycleHdr;
use App\Api\Inventory\Transformers\CycleHdrTransformer;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class CycleDtlController extends AbstractController
{

    /**
     * @var CycleDtlModel
     */
    protected $cycleDtlModel;

    /**
     * @var CycleDtlValidator
     */
    protected $validator;

    /**
     * @var CycleDtlTransformer
     */
    protected $cycleDtlTransform;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var
     */
    protected $error;

    /**
     * CycleDtlController constructor.
     *
     * @param CycleDtlModel $model
     * @param CycleDtlValidator $validator
     * @param Request $request
     */
    public function __construct(CycleDtlModel $model, CycleDtlValidator $validator, Request $request)
    {
        $this->cycleDtlModel = $model;
        $this->validator = $validator;
        $this->cycleDtlTransform = new CycleDtlTransformer();
        $this->request = $request;
    }

    /**
     * @param $cycleHdrId
     *
     * @return \Dingo\Api\Http\Response|string
     */
    public function listDtl($whs_id, $cycleHdrId)
    {
        $input = $this->request->getQueryParams();

        try {
            $data = $this->cycleDtlModel->search($whs_id, $cycleHdrId, null, $input, [], array_get($input, 'limit', 20),
                array_get($input, 'status', ''));

            $return = $this->response->paginator($data, $this->cycleDtlTransform);
            $meta = DB::table('cycle_hdr')->where('cycle_hdr_id',
                $cycleHdrId)->value('cycle_name');
            $return->meta('cycle_hdr_name', $meta);
            return $return;
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $id
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($whs_id, $id)
    {
        $this->validator = new UpdateActualCycleDtlValidator();
        $input = $this->request->getParsedBody();

        $this->validator->validate($input);

        $cycleDtl = $this->cycleDtlModel->getCycleDtlById($id);

        if (!$cycleDtl) {
            return $this->response->error(Message::get('BM067', 'Cycle detail'),
                IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }
        $spc_hdl_code = DB::table("location")->where('loc_id', $cycleDtl->sys_loc_id)->value('spc_hdl_code');
        if($spc_hdl_code == 'SHE' && $input['act_qty']> 25) {
            $errorMsg = 'Location SHE must be less than 25';
            return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        } else if($spc_hdl_code == 'BIN' && $input['act_qty']> 50){
            $errorMsg = 'Location BIN must be less than 50';
            return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }
        $validate = $this->cycleDtlModel->validateUpdateData($cycleDtl, $input);

        if (isset($validate['errors'])) {
            $errorMsg = implode('<br>', $validate['errors']);

            return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }

        $result = $this->cycleDtlModel->updateCycleDtl($cycleDtl, $input);

        $checkCycleSave = $this->cycleDtlModel->getModel()
            ->where('cycle_hdr_id', $cycleDtl->cycle_hdr_id)
            ->where('cycle_dtl_sts', '!=', 'OP')
            ->count();

        if (!$checkCycleSave) {
            (new CycleHdrController())->saveCycled($cycleDtl->cycle_hdr_id);
        }

        if ($result) {
            return $this->response->accepted(null, ['message' => Message::get('BM072'), 'data' => 'success']);
        } else {
            return $this->response->error(Message::get('BM073'), IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }
    }

    /**
     * @param $whsId
     * @param $locCode
     *
     * @return array
     */
    private function checkLocation($whsId, $locCode)
    {
        $result = ['data' => [], 'error' => []];
        $condition = [
            'loc_whs_id'   => $whsId,
            'loc_code'     => $locCode,
            'loc_sts_code' => 'AC',
        ];
        $location = LocationModel::filterLocation($condition)->first();
        if ($location) {
            $result['data']['act_loc_id'] = $location->loc_id;
            $result['data']['act_loc_name'] = $location->loc_code;
        } else {
            $result['error']['act_loc_name'] = Message::get('BM017', 'Location');
        }

        return $result;
    }

    /**
     * @param $cycleHdrId
     * @param $cycleDtlId
     *
     * @return \Dingo\Api\Http\Response|string
     */
    public function view($whs_id, $cycleHdrId, $cycleDtlId)
    {
        $input = $this->request->getQueryParams();

        try {

            $data = $this->cycleDtlModel->search($whs_id, $cycleHdrId, $cycleDtlId, $input, ['systemLocation.cartons']);
            return $this->response->paginator($data, $this->cycleDtlTransform);

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    public function rfgunView($whs_id, $cycHdrId, $cycDtlId)
    {
        $rs = $this->cycleDtlModel->getModel()
            ->select([
                'cycle_dtl.cycle_dtl_id',
                'cycle_dtl.cycle_hdr_id',
                'cycle_dtl.sku',
                'cycle_dtl.lot',
                'cycle_dtl.sys_loc_id',
                'cycle_dtl.sys_loc_name',
                'cycle_dtl.sys_qty',
                'cartons.ucc128',
                'cartons.upc'
            ])
            ->leftJoin('cartons', function ($join) {
                $join->on('cartons.item_id', '=', 'cycle_dtl.item_id');
                $join->on('cartons.lot', '=', 'cycle_dtl.lot');
            })
            ->where([
                'cycle_hdr_id' => $cycHdrId,
                'cycle_dtl_id' => $cycDtlId
            ])
            ->first()
            ->toArray();

        return $rs;
    }
}
