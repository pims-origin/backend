<?php

namespace App\Api\V1\Validators;

class SaveMassContactValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_con_fname.*'    => 'required',
            'whs_con_lname.*'    => 'required',
            'whs_con_email.*'    => 'required|email',
            'whs_con_phone.*'    => 'required',
            'whs_con_position'   => '',
            'whs_con_department' => '',
        ];
    }
}
