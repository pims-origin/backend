<?php
namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderHdr;

class OrderHdrModel extends AbstractModel
{

    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }

    public function updatePKOdrHdr($wvHdrId)
    {
        $result = DB::table('odr_hdr as oh')
            ->where('oh.wv_id', $wvHdrId)
            ->update(['oh.odr_sts' => 'PK']);

        return $result;
    }

    public function updatePDOdrHdr($wvHdrId)
    {
        $result = DB::table('odr_hdr as oh')
            ->where('oh.wv_id', $wvHdrId)
            ->where('oh.odr_sts', 'PK')
            ->where(
                DB::raw("(SELECT COUNT(1) FROM `odr_dtl` as od WHERE od.`odr_id` = oh.odr_id AND od.`itm_sts` = 'PD')"),
                DB::raw("(SELECT COUNT(1) FROM `odr_dtl` as od WHERE od.`odr_id` = oh.odr_id)")
            )
            ->update(['oh.odr_sts' => 'PD']);

        return $result;
    }

    public function updatePAOdrHdr($odrHdrIds)
    {
        $result = DB::table('odr_hdr')
            ->where('odr_id', $odrHdrIds)
            ->where('odr_sts', 'PD')
            ->update(['odr_sts' => 'PA']);

        return $result;
    }

}
