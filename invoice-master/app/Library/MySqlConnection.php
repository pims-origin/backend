<?php

namespace App\Library;

use Illuminate\Database\MySqlConnection as origCon;

class MySqlConnection extends origCon
{
    public function setFetchMode($fetchMode)
    {
        $this->fetchMode = $fetchMode;
    }
}