<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Utils\SelArr;

class GoodsReceiptCartonModel extends AbstractModel
{
    public function __construct()
    {

    }

    public function search($goodsReceipt, $attributes, $with, $limit = 20)
    {
    	if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query = $goodsReceipt->cartons()
                            ->whereIn('ctn_sts', ['LK', 'AC', 'RG'])
                            ->with($with);

        if (isset($attributes['sku'])){
            $query->where('sku', 'LIKE', '%' . $attributes['sku'] . '%');
        }

        if (isset($attributes['ctn_rfid'])){
            $query->where('rfid', 'LIKE', '%' . $attributes['ctn_rfid'] . '%');
        }

        if (isset($attributes['is_gateway'])){
            if ( $attributes['is_gateway'] == 'Y' ){
                $query->has('pallet');
            }
            if ( $attributes['is_gateway'] == 'N' ){
                $query->has('pallet', '<', 1);
            }
        }

        return $query->paginate($limit);
    }
}
