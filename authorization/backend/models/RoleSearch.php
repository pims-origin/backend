<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/30/2016
 * Time: 5:14 PM
 */

namespace backend\models;

use yii\db\Query;
use yii\data\Pagination;
use yii;
use yii\helpers\VarDumper;

class RoleSearch
{
    public static function getList(Form $form)
    {
        $query = (new Query());
        $query->from('authority');
        $params = $form->makeUserCriteria();

        $form->makeOrder();

        foreach ($params as $field => $detail) {
            $query->andWhere(
                [
                    $detail['opr'],
                    $field,
                    $detail['value']
                ]
            );
        }
        $count = $query->count();
        if (! $count) {
            return [];
        }
        // Pagination
        $pages = new Pagination(
            [
                'totalCount' => $count,
                'pageSize'   => $form->limit,
                'page'       => $form->page - 1,
            ]
        );

        if (! empty($form->sort)) {
            $query->orderBy($form->sort);
        }

        $data = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->select(RoleSearchForm::ALLOW_SELECT_FIELDS)
        ->all();

        // Meta
        $meta['totalCount'] = $pages->totalCount;
        $meta['pageCount'] = $pages->pageCount;
        $meta['currentPage'] = $pages->page + 1;
        $meta['perPage'] = $pages->limit;

        $links = self::makePaginationLinks($pages->getLinks(true));

        return [
            'data' => $data,
            'link' => $links,
            'meta' => $meta,
        ];
    }

    public static function makePaginationLinks(array $links)
    {
        $result = [];
        foreach ($links as $key => $link) {
            $result[$key] = [
                'href' => $link,
            ];
        }
        return $result;
    }

    public function getRolesByUserId($id, $options = [])
    {
        $auth = Yii::$app->authManager;
        // Get list roles by user
        $roles = $auth->getRolesByUserId($id);
        
        //  Have roles
        if ( $roles ) {
            //  Page
            $page = 1;
            $limit = 20;
            $error = [];
            if (isset($options['page'])) {
                //  Page must be integer and greater than 0
                if (is_numeric($options['page']) && $options['page'] > 0) {
                    $page = $options['page'];
                } else {
                    $error['page'] = ['Page must be integer and greater than 0.'];
                }
            }
            //  Limit
            if (isset($options['limit'])) {
                //  Limit must be integer and greater than or equal 5
                if (is_numeric($options['limit']) && $options['limit'] > 5) {
                    $limit = $options['limit'];
                } else {
                    $error['limit'] = ['Limit must be integer greater than or equal to 5'];
                }
            }

            //  Have error
            if (count($error) > 0) {
                return ['error' => $error];
            }

            //  Total count
            $totalCount = count($roles);
            //  Total count greater than 0
            if ($totalCount > 0) {
                $arrRoles = [];
                $i = 0;
                foreach ($roles as $role) {
                    $arrRoles[$i / $limit][$i] = $role;
                    $i++;
                }
                if (isset($arrRoles)) {
                    return [
                        'data' => isset($arrRoles[$page - 1]) ? array_values($arrRoles[$page - 1]) : [],
                        'meta' => [
                            'totalCount' => $totalCount,
                            'pageCount' => count($arrRoles),
                            'currentPage' => $page,
                            'perPage' => $limit
                        ]
                    ];
                }
            }
        }
        return [];
    }
}
