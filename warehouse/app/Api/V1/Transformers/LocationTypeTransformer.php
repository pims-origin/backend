<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\LocationType;


class LocationTypeTransformer extends TransformerAbstract
{
    /**
     * @param LocationType $locationType
     *
     * @return array
     */
    public function transform(locationType $locationType)
    {
        return [
            'loc_type_id'   => $locationType->loc_type_id,
            'loc_type_name' => $locationType->loc_type_name,
            'loc_type_code' => $locationType->loc_type_code,
            'loc_type_desc' => $locationType->loc_type_desc
        ];
    }
}
