<?php

namespace App\Api\V2\PackDetail\Models;

use Seldat\Wms2\Models\ItemChild;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ItemChildModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new ItemChild();
    }

    public function insertBatch($data)
    {
        $this->model->insert($data);
    }
}
