<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\PackRef;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class PackRefModel extends AbstractModel
{
    /**
     * PackRefModel constructor.
     */
    public function __construct()
    {
        $this->model = new PackRef();
    }

    public function search($attributes = [], $with = [])
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                $query = $query->where($key, SelStr::escapeLike($value));
            }
        }

        $models = $query->get();

        return $models;
    }
}
