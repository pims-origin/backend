<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\Ecom\Models;

use Dingo\Api\Exception\UnknownVersionException;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class PalletModel extends AbstractModel
{

    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
    }

    public function updatePallet($pltIds)
    {
        // Get Pallet info
        $pltInfos = $this->model
            ->whereIn('plt_id', $pltIds)
            ->get();
        foreach ($pltInfos as $pltInfo) {
            $created_at = array_get($pltInfo, 'created_at', 0);
            $zeroDt = array_get($pltInfo, 'zero_date', 0);

            // Calculate storage_duration
            $date1 = date("Y-m-d", is_int($created_at) ?: $created_at->timestamp );
            $date2 = date("Y-m-d", is_int($zeroDt) ?: $created_at->timestamp );

            $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }

            $this->model
                ->where('plt_id', $pltInfo['plt_id'])
                ->update([
                    'storage_duration' => $storageDuration
                ]);
        }
    }
}
