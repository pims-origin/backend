<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Seldat\Wms2\Models\XferDtl;
use Seldat\Wms2\Models\XferHdr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\CLI;

define('LOC_TYPE_RACK_ID', 3);

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // For command vendor:publish
        \Laravelista\LumenVendorPublish\VendorPublishCommand::class,

        // Location Commands
        Commands\LocationCommands::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            echo CLI::setTextColor("\nPlease Wait...\n", CLI::GREEN, CLI::BLACK);

            // Get all refill
            $allRefill = DB::table('refill')->where('deleted_at', 915148800)->where('deleted', 0)->get();

            foreach ($allRefill as $refill) {
                // Each Refill
                $refillLocation = DB::table('location')->where([
                    'loc_id'       => $refill['loc_id'],
                    'loc_sts_code' => 'AC'
                ])->first();
                if (empty($refillLocation)) {
                    continue;
                }
                $itemExcept = DB::table('xfer_hdr')
                    ->select('item_id')
                    ->where('xfer_sts', '!=', 'CO')
                    ->where('deleted_at', 915148800)
                    ->where('whs_id', $refillLocation['loc_whs_id'])
                    ->where('deleted', 0)
                    ->get();
                $itemExcept = array_pluck($itemExcept, 'item_id', 'item_id');
                if (!empty($itemExcept[$refill['item_id']])) {
                    continue;
                }

                $ecomCarton = DB::table('cartons')
                    ->select([
                        DB::raw("sum(piece_remain) as piece_remain_ttl"),
                        "whs_id",
                        "cartons.loc_id",
                        "item_id",
                        "cus_id"
                    ])
                    ->where('loc_id', $refill['loc_id'])
                    ->where('whs_id', $refillLocation['loc_whs_id'])
                    ->where('sku', $refill['sku'])
                    ->where('size', $refill['size'])
                    ->where('color', $refill['color'])
                    ->where('lot', 'ECO')
                    ->where('is_ecom', 1)
                    ->where('cus_id', $refill['cus_id'])
                    ->groupBy('item_id')
                    ->first();

                $piece_remain_ttl = array_get($ecomCarton, 'piece_remain_ttl', 0);

                $a = $refill['sku'].'-'.$refill['size'].'-'.$refill['color'];
                echo $a.': '.$piece_remain_ttl.', min: '.$refill['min'].PHP_EOL;

                if (empty($ecomCarton) || $piece_remain_ttl < $refill['min']) {
                    $rackCartons = DB::table('cartons')
                        ->select([
                            DB::raw("sum(piece_remain) as piece_remain_ttl"),
                            'cartons.loc_id',
                            'cartons.loc_code',
                            'cartons.item_id',
                            'cartons.lot',
                            'cartons.whs_id',
                            'cartons.cus_id',
                            'cartons.created_by',
                        ])
                        ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
                        ->where('location.loc_type_id', LOC_TYPE_RACK_ID)
                        ->where('location.loc_whs_id', $refillLocation['loc_whs_id'])
                        ->where('location.loc_sts_code', 'AC')
                        ->whereNotNull('cartons.loc_id')
                        ->where('cartons.ctn_sts', 'AC')
                        ->where('sku', $refill['sku'])
                        ->where('size', $refill['size'])
                        ->where('color', $refill['color'])
                        ->where('is_ecom', 0)
                        ->where('cus_id', $refill['cus_id'])
                        ->groupBy('loc_id', 'item_id', 'lot')
                        ->orderBy('piece_remain_ttl', 'desc')
                        ->get();

                    // The Piece need to get.
                    $needMore = $refill['max'] - $ecomCarton['piece_remain_ttl'];
                    $piece_ttl = 0;
                    $itemIds = [];
                    $i = 0;
                    while ($needMore > $piece_ttl) {
                        if (empty($rackCartons[$i])) {
                            break;
                        }
                        $piece_ttl += $rackCartons[$i]['piece_remain_ttl'];
                        $itemIds[] = [
                            'item_id'       => $rackCartons[$i]['item_id'],
                            'lot'           => $rackCartons[$i]['lot'],
                            'whs_id'        => $rackCartons[$i]['whs_id'],
                            'cus_id'        => $rackCartons[$i]['cus_id'],
                            'allocated_qty' => $rackCartons[$i]['piece_remain_ttl'] < $needMore ?
                                $rackCartons[$i]['piece_remain_ttl'] : $needMore,
                            'req_loc_id'    => $rackCartons[$i]['loc_id'],
                            'req_loc'       => $rackCartons[$i]['loc_code'],
                            'mez_loc'       => $refill['loc_code'],
                            'mez_loc_id'    => $refill['loc_id'],
                            'created_by'       => $rackCartons[$i]['created_by'],
                        ];

                        $i++;
                    }

                    $needMore = $piece_ttl < $needMore ? $piece_ttl : $needMore;

                    // Create Xfer
                    if (!empty($itemIds)) {
                        $this->createXferHdr($itemIds, $needMore);
                    } else {
                        echo PHP_EOL .
                            CLI::setTextColor("LOG:", CLI::DARK_GRAY_FOR_TEXT, CLI::YELLOW) .
                            CLI::setTextColor("loc_type_id: ", CLI::GREEN) .
                            CLI::setTextColor(LOC_TYPE_RACK_ID . " (RACK)", CLI::PURPLE_FOR_TEXT) .
                            CLI::setTextColor(" - whs_id: ", CLI::GREEN) .
                            CLI::setTextColor($refillLocation['loc_whs_id'], CLI::PURPLE_FOR_TEXT) .
                            CLI::setTextColor(" - item_id: ", CLI::GREEN) .
                            CLI::setTextColor($refill['item_id'], CLI::PURPLE_FOR_TEXT) .
                            CLI::setTextColor(" - cus_id: ", CLI::GREEN) .
                            CLI::setTextColor($refill['cus_id'], CLI::PURPLE_FOR_TEXT) .
                            CLI::setTextColor(" not exist in table Cartons!", CLI::GREEN);
                    }

                }
            }

            if (!isset($needMore)) {
                echo PHP_EOL . PHP_EOL . CLI::setTextColor("   Nothing Change!   ", CLI::WHITE_FOR_TEXT,
                        CLI::GREEN) . PHP_EOL;
            } else {
                echo PHP_EOL . PHP_EOL . CLI::setTextColor("   Done!  ", CLI::WHITE_FOR_TEXT, CLI::GREEN) . PHP_EOL;
            }


        })->everyMinute();
    }

    private function createXferHdr($itemIds, $needMore)
    {
        $first = reset($itemIds);

        $item = DB::table('item')->select(['sku', 'size', 'color', 'cus_upc'])
            ->where('item_id', $first['item_id'])->first();
        $xferNum = $this->getXferNum();

        $xferHdrParam = [
            'xfer_hdr_num' => $xferNum,
            'whs_id'       => $first['whs_id'],
            'cus_id'       => $first['cus_id'],
            'item_id'      => $first['item_id'],
            'sku'          => $item['sku'],
            'size'         => $item['size'],
            'color'        => $item['color'],
            'lot'          => $first['lot'],
            'cus_upc'      => $item['cus_upc'],
            'piece_qty'    => $needMore,
            'xfer_sts'     => 'PD',
            'sts'          => 'i',
            'created_at'   => time(),
            'created_by'   => $first['created_by'],
            'updated_at'   => time(),
            'updated_by'   => $first['created_by'],
            'deleted'      => 0,
            'deleted_at'   => 915148800,
        ];

        DB::beginTransaction();
        // Insert Xfer Header
        XferHdr::insert($xferHdrParam);
        echo "\n\nXfer {$xferHdrParam['xfer_hdr_num']} created!";

        // Insert Xfer Detail
        echo "\nXfer {$xferHdrParam['xfer_hdr_num']}'s detail is creating!";
        $this->createXferDtl($itemIds, $item);
        echo "\nDetails created!\n";
        DB::commit();
    }

    private function createXferDtl($itemIds, $item)
    {
        $xferId = DB::getPdo()->lastInsertId();
        foreach ($itemIds as $itemId) {
            $xferDtlParam = [
                'xfer_hdr_id'   => $xferId,
                'whs_id'        => $itemId['whs_id'],
                'cus_id'        => $itemId['cus_id'],
                'item_id'       => $itemId['item_id'],
                'sku'           => $item['sku'],
                'size'          => $item['size'],
                'color'         => $item['color'],
                'lot'           => $itemId['lot'],
                'cus_upc'       => $item['cus_upc'],
                'allocated_qty' => $itemId['allocated_qty'],
                'req_loc_id'    => $itemId['req_loc_id'],
                'req_loc'       => $itemId['req_loc'],
                'mez_loc'       => $itemId['mez_loc'],
                'mez_loc_id'    => $itemId['mez_loc_id'],
                'xfer_dtl_sts'  => 'PD',
                'sts'           => 'i',
                'created_at'    => time(),
                'created_by'    => $itemId['created_by'],
                'updated_at'    => time(),
                'updated_by'    => $itemId['created_by'],
                'deleted'       => 0,
                'deleted_at'    => 915148800,
            ];
            XferDtl::insert($xferDtlParam);
        }

    }

    private function getXferNum()
    {
        $xfer = DB::table('xfer_hdr')->select('xfer_hdr_num')->orderBy('xfer_hdr_id', 'desc')->first();

        return SelStr::getHdrNum(array_get($xfer, 'xfer_hdr_num'), 'XFE');
    }
}
