<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Transformers\ItemTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class ItemController extends AbstractController
{
    protected $invt_smr_model;

    /**
     * ItemController constructor.
     */
    public function __construct()
    {
        $this->invt_smr_model = new InventorySummaryModel();
    }

    /**
     * @param Request $request
     * @param ItemTransformer $itemTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function itemInfo(Request $request, ItemTransformer $itemTransformer)
    {
        $input = $request->getQueryParams();

        try {
            $wvTfs = $this->invt_smr_model->searchAutoComplete($input, array_get($input, 'limit', 20));

            return $this->response->collection($wvTfs, $itemTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
