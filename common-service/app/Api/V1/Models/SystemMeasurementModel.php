<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 29-Jun-16
 * Time: 10:15
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemMeasurement;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class SystemMeasurementModel extends AbstractModel
{
    public function __construct(SystemMeasurement $model = null)
    {
        $this->model = ($model) ?: new SystemMeasurement();
    }

    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'sys_mea_name' || $key === 'sys_mea_code' || $key === 'sys_mea_desc') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        //Implement sort builder for api
        //usage: ?sort[name]=desc&sort[title]=asc
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }
}