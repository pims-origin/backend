<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;

class RMAOrderTransformer extends TransformerAbstract
{
    public function transform($order)
    {
        return [
            'odr_num'       => object_get($order, 'odr_num'),
            'cus_odr_num'   => object_get($order, 'cus_odr_num'),
            'cus_po'        => object_get($order, 'cus_po')
        ];
    }
}
