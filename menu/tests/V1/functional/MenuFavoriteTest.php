<?php

/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 25-May-16
 * Time: 15:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class MenuFavoriteTest extends TestCase
{
    use DatabaseTransactions;

    protected $url;

    protected $menuId;

    /**
     * MenuFavoriteTest constructor.
     */
    public function __construct()
    {
        $this->url = "/v1/favorites";
    }

    public function setUp()
    {
        parent::setUp();

        $menu = factory(App\Menu::class)->create();
        $this->menuId = $menu->menu_id;
    }

    /**
     * Test Create Menu Favorite
     *
     * No Input
     * Return OK
     */
    public function testCreate_Ok()
    {
        $user = factory(\App\User::class)->create();
        $userId = $user->user_id;
        $menuId = $this->menuId;

        $response = $this->call('POST', $this->url . "/" . $userId, [
            'menu_id'       => $menuId,
            'display_order' => 5,
        ]);

        $this->assertEquals(201, $response->status());

        $response = $this->call('DELETE', "{$this->url}/$userId/$menuId");

        $this->assertEquals(204, $response->status());
    }

    /**
     * Test Create new Menu Favorite with exist Menu Id in DB
     *
     * Input: Name, Description
     *
     * Output: Error
     */
    public function testCreate_MenuIdExist_Error()
    {
        $userId = 1;
        $menuId = $this->menuId;

        $this->call('POST', "$this->url/$userId", [
            'menu_id'       => $menuId,
            'display_order' => 5,
        ]);

        $response = $this->call('POST', "$this->url/$userId", [
            'menu_id'       => $menuId,
            'display_order' => 5,
        ]);

        $this->assertEquals(400, $response->status());

        $this->call('DELETE', "{$this->url}/$userId/$menuId");
    }

    /**
     * Test Delete Menu Favorite
     *
     * AssertEqual code 200
     */
    public function testDelete_OkAndFail()
    {
        $userId = 1;
        $menuId = $this->menuId;

        $this->call('POST', "$this->url/$userId", [
            'menu_id'       => $menuId,
            'display_order' => 5,
        ]);

        $this->call('DELETE', "{$this->url}/$userId/$menuId");

        $response = $this->call('DELETE', "{$this->url}/$userId/$menuId");

        $this->assertEquals(400, $response->status());
    }

    /**
     * Test List Menu Favorite
     *
     * No Input
     * AssertEqual
     */
    public function testList()
    {
        $user_id = 1;
        $response = $this->call('GET', "{$this->url}/$user_id");

        $this->assertEquals(200, $response->status());
    }
}
