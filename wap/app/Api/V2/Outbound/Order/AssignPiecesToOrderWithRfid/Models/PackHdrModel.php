<?php

namespace App\Api\V2\Outbound\Order\AssignPiecesToOrderWithRfid\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackHdr;

/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class PackHdrModel extends AbstractModel
{
    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
    }

    public function getCartonsByOutPltId($out_plt_id, $whsId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
            ->where('out_plt_id', $out_plt_id)
            ->where('whs_id', $whsId)
            ->get();
    }

    public function getCartonsByOrderId($odrid, $whsId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
            ->where('odr_hdr_id', $odrid)
            ->where('whs_id', $whsId)
            ->get()
            ->toArray();
    }

    /**
     * @param $whsId
     * @param $odrId
     *
     * @return mixed
     */
    public function packTotal($whsId, $odrId)
    {
        return $this->model
                        ->where('whs_id', $whsId)
                        ->where('odr_hdr_id', $odrId)
                        ->count();
    }

    /**
     * @param $whsId
     * @param $outPltId
     *
     * @return mixed
     */
    public function scannedByPalletTotal($whsId, $outPltId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
                        ->where('whs_id', $whsId)
                        ->where('out_plt_id', $outPltId)
                        ->get()
                        ->toArray();
    }

    public function scannedByOrderTotal($whsId, $odrId)
    {
        return $this->model
            ->where('whs_id', $whsId)
            ->where('odr_hdr_id', $odrId)
            ->whereNotNull('out_plt_id')
            ->count();
    }
}
