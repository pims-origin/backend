<?php

namespace backend\models;

use yii\db\Query;
use backend\forms\UserSearchForm;
use Yii;

class BaseUser
{
    protected $_isSuperAdmin = false;

    protected function __construct()
    {
        $this->_isSuperAdmin = (Yii::$app->user->getId() === 1);
    }

    public static function getUserIdByToken($token)
    {
        $query = (new Query());
        return $query->from('reset_password')
            ->where(['token' => $token])
            ->select('user_id')
            ->column();

    }

    public static function getFullUserIdByToken($token)
    {
        $query = (new Query());
        return $query->from('reset_password')
            ->where(['token' => $token])
            ->select(['user_id', 'expired_at', 'created_at', 'updated_at'])
            ->one();

    }

    public static function getUser($id, $fields = UserSearchForm::ALLOW_SELECT_FIELDS)
    {
        $query = (new Query());
        $user = $query->from('users')
            ->where(['user_id' => $id])
            ->select($fields)
            ->one();
        if (null === $user) {
            return false;
        }

        return $user;
    }

    public static function populate($rows, $indexName, $indexColumns)
    {
        $result = [];
        foreach ($rows as $row) {
            foreach ($row as $colName => $value) {
                if (in_array($colName, $indexColumns)) {
                    unset($row[$colName]);
                    $row[$indexName][$colName] = $value;
                }
            }
            $result[] = $row;
        }

        return $result;
    }

    protected function _getUserInfo($userId)
    {
        $userFields = [
            "usr_dpm_id",
            "user_id",
            "username",
            "emp_code",
            "email",
            "first_name",
            "last_name",
            "off_loc",
            "dept",
            "phone",
            "phone_extend",
            "status",
            "mobile"
        ];

        $user = self::getUser($userId, $userFields);
        $user['last_login'] = Yii::$app->user->getIdentity()->last_login; //add last login info

        if (null === $user) {
            return false;
        }

        if ($userId !== 1) {
            $user = array_merge($user, $this->getAssignUserInfo($userId));
        } else {
            $user = array_merge($user, $this->getAssignUserInfo());
        }

        return $user;
    }

    /**
     * Get all or follow by userId data of: warehouses, customers, roles
     *
     * @param bool $userId
     *
     * @return array
     */
    public function getAssignUserInfo($userId = false)
    {
        $result = [];

        $queryRoles = (new Query());
        $queryWhs = (new Query());
        $queryCus = (new Query());

        //query for get all or by $userId
        if (!$userId) {
            //get all roles
            $queryRoles->from('authority a')
                ->where('a.type=1');

            //all warehouse
            $queryWhs->from('warehouse w')
                ->where('w.deleted = 0');

            //all customer
            $queryCus->from("customer c")
                ->join('LEFT JOIN', 'customer_warehouse ciu', 'c.cus_id=ciu.cus_id AND ciu.deleted=0')
                ->where('c.deleted = 0');

        } else {
            //roles
            $queryRoles->from('user_role ur')
                ->join('LEFT JOIN', 'authority a', 'ur.item_name=name')
                ->where('a.type=1 AND ur.user_id = :user_id', ['user_id' => $userId]);

            //warehouse
            $queryWhs->from('user_whs us')
                ->join('LEFT JOIN', 'warehouse w', 'us.whs_id=w.whs_id')
                ->where('us.user_id = :user_id AND w.deleted = 0', ['user_id' => $userId]);

            //customer
            $queryCus->from("cus_in_user ciu")
                ->join('LEFT JOIN', 'customer c', 'ciu.cus_id=c.cus_id')
                ->where('ciu.user_id = :user_id AND c.deleted = 0', ['user_id' => $userId]);
        }

        //update roles query
        $queryRoles
            ->select('a.name, a.code, a.description');

        //update warehouse query
        $queryWhs
            ->join('LEFT JOIN', 'whs_contact wc', 'w.whs_id=wc.whs_con_whs_id')
            ->select("
                w.whs_id, w.whs_name, w.whs_code,
                wc.whs_con_fname, wc.whs_con_lname, wc.whs_con_phone
            ")
            ->andWhere("w.whs_status = 'AC'")
            ->groupBy('w.whs_id');

        //update customer query
        $queryCus
            ->join('LEFT JOIN', 'cus_contact cc', 'c.cus_id=cc.cus_ctt_cus_id')
            ->join('LEFT JOIN', 'warehouse w', 'ciu.whs_id=w.whs_id')
            ->select([
                'c.cus_id',
                'c.cus_name',
                'c.cus_des',
                'c.cus_code',
                'CONCAT(cc.cus_ctt_fname, " ",cc.cus_ctt_lname) AS cus_ctt_full_name',
                'cc.cus_ctt_email',
                'cc.cus_ctt_phone',
                'w.whs_id',
                'w.whs_code'
            ])
            ->andWhere("c.cus_status = 'AC'")
            ->groupBy('c.cus_id,w.whs_id');


        $warehouses = $queryWhs->all();
        $warehouses = self::populate(
            $warehouses,
            'whs_contact',
            ['whs_con_fname', 'whs_con_lname', 'whs_con_phone']
        );

        $result['user_warehouses'] = $warehouses;
        $result['user_customers'] = $queryCus->all();
        $result['user_roles'] = $queryRoles->all();

        return $result;
    }

    public function getUserPermissions()
    {
        if ($this->_isSuperAdmin) {
            $query = (new Query())
                ->select('name')
                ->from('authority')
                ->where('type=2');
            $perms = $query->column();
        } else {
            //get user permission from authorization
            $url = Yii::$app->params['authorization_api'] . 'users/' . $this->_userId . '/permissions';
            $resp = Yii::$app->httpclient->get($url, [], [], true);
            if ($resp->getStatusCode() !== 200) {
                throw new \Exception("Can't get permission");
            }
            $perms = \GuzzleHttp\json_decode($resp->getBody(), true)['data'];
            $perms = array_column($perms, 'name');
        }

        return $perms;
    }
}