<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\GoodReceiptStatusModel;
use App\Api\V1\Transformers\GoodReceiptStatusTransformer;
use App\Api\V1\Validators;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class GoodReceiptStatusController extends AbstractController
{

    public function __construct()
    {
    }

    public function index(
        GoodReceiptStatusModel $goodReceiptStatusModel,
        GoodReceiptStatusTransformer $goodReceiptStatusTransformer,
        Request $request
    ) {
        $input =  $request->getQueryParams();
        $inputType = array_get($input, 'type', 'gr');

        try {
            // get list good reciept status
            $statuses = $goodReceiptStatusModel->all();
            $statuses = $statuses->keyBy('gr_sts_code');

            $userId = Data::getCurrentUserId();
            $userMeta = DB::table('user_meta')
                            ->where('user_id', $userId)
                            ->where('qualifier', config('constants.qualifier.GR_STATUS'))
                            ->first();
            $userValue = collect(json_decode($userMeta['value']));
            $userValue = $userValue->get($inputType, []);
            if($userValue){
                $userValue = collect($userValue)->keyBy('key');
            }

            foreach($userValue as $value){
                $status = $statuses[$value->key];
                $status->checked = true;
            }

            return $this->response->collection($statuses, $goodReceiptStatusTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
