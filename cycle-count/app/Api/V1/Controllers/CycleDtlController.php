<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Transformers\CycleDtlTransformer;
use App\Api\V1\Validators\CycleDtlValidator;
use App\Api\V1\Validators\UpdateActualCycleDtlValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V1\Validators\CycleHdrValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\CycleHdr;
use App\Api\V1\Transformers\CycleHdrTransformer;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class CycleDtlController extends AbstractController
{

    /**
     * @var CycleDtlModel
     */
    protected $cycleDtlModel;

    /**
     * @var CycleDtlValidator
     */
    protected $validator;

    /**
     * @var CycleDtlTransformer
     */
    protected $cycleDtlTransform;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var
     */
    protected $error;

    /**
     * CycleDtlController constructor.
     * @param CycleDtlModel $model
     * @param CycleDtlValidator $validator
     * @param Request $request
     */
    public function __construct(CycleDtlModel $model, CycleDtlValidator $validator, Request $request)
    {
        $this->cycleDtlModel = $model;
        $this->validator = $validator;
        $this->cycleDtlTransform = new CycleDtlTransformer();
        $this->request = $request;
    }

    /**
     * @param $cycleHdrId
     * @return \Dingo\Api\Http\Response|string
     */
    public function listDtl($cycleHdrId)
    {
        $input = $this->request->getQueryParams();
        try {
            $data = $this->cycleDtlModel->search($cycleHdrId, null, $input, [], array_get($input, 'limit', 20), array_get($input, 'status', ''));
            $customer_ids=[];
            $warehouse_ids=[];
            foreach ($data as $v){
                $customer_ids[$v->cus_id]=$v->cus_id;
                $warehouse_ids[$v->whs_id]=$v->whs_id;
            }
            $statusData=$this->cycleDtlModel->getItemInProcess([
                'customer_ids'=>$customer_ids,
                'warehouse_ids'=>$warehouse_ids,
            ]);
            $this->cycleDtlTransform->setAvailableIncludes($statusData);
            return $this->response->paginator($data, $this->cycleDtlTransform);
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $id
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        $this->validator = new UpdateActualCycleDtlValidator();
        $input = $this->request->getParsedBody();

        $this->validator->validate($input);

        $cycleDtl = $this->cycleDtlModel->getCycleDtlById($id);

        if (! $cycleDtl) {
            return $this->response->error(Message::get('BM067', 'Cycle detail'), IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }

        $validate = $this->cycleDtlModel->validateUpdateData($cycleDtl, $input);

        if (isset($validate['errors'])) {
            $errorMsg = implode('<br>', $validate['errors']);
            return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }

        $result = $this->cycleDtlModel->updateCycleDtl($cycleDtl, $input);

        if ($result) {
            return $this->response->accepted(null, ['message' => Message::get('BM072'), 'data' => 'success']);
        } else {
            return $this->response->error(Message::get('BM073'), IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }
    }

    /**
     * @param $whsId
     * @param $locCode
     * @return array
     */
    private function checkLocation($whsId, $locCode)
    {
        $result = ['data' => [], 'error' => []];
        $condition = [
            'loc_whs_id' => $whsId,
            'loc_code' => $locCode,
            'loc_sts_code' => 'AC',
        ];
        $location = LocationModel::filterLocation($condition)->first();
        if ($location) {
            $result['data']['act_loc_id'] = $location->loc_id;
            $result['data']['act_loc_name'] = $location->loc_code;
        } else {
            $result['error']['act_loc_name'] = Message::get('BM017', 'Location');
        }
        return $result;
    }

    /**
     * @param $cycleHdrId
     * @param $cycleDtlId
     * @return \Dingo\Api\Http\Response|string
     */
    public function view($cycleHdrId, $cycleDtlId)
    {
        $input = $this->request->getQueryParams();

        try {

            $data = $this->cycleDtlModel->search($cycleHdrId, $cycleDtlId, $input);

            return $this->response->paginator($data, $this->cycleDtlTransform);

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    public function rfgunView($cycHdrId, $cycDtlId)
    {
        $rs = $this->cycleDtlModel->getModel()
            ->select([
                'cycle_dtl.sku',
                'cycle_dtl.lot',
                'cycle_dtl.sys_loc_id',
                'cycle_dtl.sys_loc_name',
                'cycle_dtl.sys_qty',
                'cartons.ucc128'
            ])
            ->leftJoin('cartons', function($join) {
                $join->on('cartons.item_id', '=', 'cycle_dtl.item_id');
                $join->on('cartons.lot', '=', 'cycle_dtl.lot');
            })
            ->where([
                'cycle_hdr_id' => $cycHdrId,
                'cycle_dtl_id' => $cycDtlId
            ])
            ->first()
            ->toArray()
        ;
        
        return $rs;
    }
}
