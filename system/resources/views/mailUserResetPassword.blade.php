<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" style="background-color: #F1F1F1">
    <tr>
        <td align="center">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
                <tr>
                    <td class="email-masthead" style="padding-bottom: 30px; padding-top: 30px; text-align: center;">
                        <a class="email-masthead_name"><img src="http://seldatinc.com/wp-content/themes/seldatinc_vvt/images/logo.png" alt=""/></a>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell" style="background-color: white; padding: 27px; border-radius: 5px;">
                                    <h2 style="text-align: left">Hi {{$name}},</h2>
                                    <p> Your password to log in the WMS has been changed. Please contact administrator for more information.</p>
                                    <p>Thanks,<br>{{$author}} Team</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-cell">
                                    <p class="sub center">&copy; {{date('Y')}} Seldat Inc. All rights reserved.</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>