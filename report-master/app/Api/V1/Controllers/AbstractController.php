<?php

namespace App\Api\V1\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Wms2\UserInfo\Data;

/**
 * Class BaseController
 *
 * @package App\Api\V1\Controllers
 *
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="",
 *     basePath="/v1",
 *     definitions={},
 *     @SWG\Info(
 *         version="WMS 2.0.0",
 *         title="Goods receipt API",
 *         description="There are some API function to view, create, update, delete data for Goods receipt",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="dang.dung@seldatinc.com"
 *         ),
 *         @SWG\License(
 *             name="WMS2.0",
 *             url="seldatinc.com"
 *         )
 *     ),
 *     @SWG\Definition(
 *          definition="ErrorModel",
 *          @SWG\Property(
 *              property="errors",
 *              required={"message","status_code"},
 *              @SWG\Property(property="message", type="string", description="Message error"),
 *              @SWG\Property(property="status_code", type="integer", format="int32", description="Status code error")
 *          )
 *     ),
 *     @SWG\Definition(
 *         definition="InputAsnsData",
 *         required={"whs_id", "cus_id", "asn_ref", "exp_date", "measurement_code", "ctnr_num"},
 *         @SWG\Property(property="whs_id", type="integer", format="int32", description="Warehouse id", default=1),
 *         @SWG\Property(property="cus_id", type="integer", format="int32", description="Customer Id", default=1),
 *         @SWG\Property(property="asn_ref", type="string", description="asn_hdr_ref", default="abc"),
 *         @SWG\Property(property="exp_date", type="string", format="date", description="Expiration Date",
 *              default="2016-07-20"),
 *         @SWG\Property(property="measurement_code", type="string", description="Measurement Code", default="CM"),
 *         @SWG\Property(property="ctnr_num",  type="string", description="Container Number", default="CT123"),
 *         @SWG\Property(
 *              property="details",
 *              type="array",
 *              @SWG\Items(ref="#/definitions/AsnsDetail")
 *          )
 *     ),
 *     @SWG\Definition(
 *         definition="AsnsDetail",
 *         required={"dtl_item_code", "dtl_sku", "dtl_color", "dtl_length", "dtl_width", "dtl_height", "dtl_weight",
 *              "dtl_pack", "dtl_uom_id", "dtl_po", "dtl_ctn_ttl", "dtl_crs_doc"},
 *         @SWG\Property(property="dtl_itm_id", type="integer", format="int32", description="Detail Item Id",
 *              default=1),
 *         @SWG\Property(property="dtl_item_code", type="string", description="Detail Item Code", default="abc123"),
 *         @SWG\Property(property="dtl_des", type="string", description="Description", default="abc123"),
 *         @SWG\Property(property="dtl_suffix", type="string", description="Suffix", default="SF"),
 *         @SWG\Property(property="dtl_sku", type="string", description="Detail Item SKU", default="SKU001"),
 *         @SWG\Property(property="dtl_size", type="string", description="Detail Item Size", default="XXL"),
 *         @SWG\Property(property="dtl_color", type="string", description="Detail Item Color", default="BLA"),
 *         @SWG\Property(property="dtl_volume", type="number", format="float", description="Detail Item Volume",
 *              default=12.00),
 *         @SWG\Property(property="dtl_width", type="number", format="float", description="Detail Item Width",
 *              default=12.00),
 *         @SWG\Property(property="dtl_length", type="number", format="float", description="Detail Item Length",
 *              default=12.00),
 *         @SWG\Property(property="dtl_height", type="number", format="float", description="Detail Item Height",
 *              default=12.00),
 *         @SWG\Property(property="dtl_weight", type="number", format="float", description="Detail Item Weight",
 *              default=12.00),
 *         @SWG\Property(property="dtl_pack", type="integer", format="int32", description="Detail Item Pack",
 *              default=12),
 *         @SWG\Property(property="dtl_uom_id", type="integer", format="int32", description="Detail Item UOM Id",
 *              default=1),
 *         @SWG\Property(property="dtl_po", type="string", description="Detail Item PO", default="abc123"),
 *         @SWG\Property(property="dtl_po_date", type="string", format="date", description="Detail Item PO Date",
 *              default="2016-07-20"),
 *         @SWG\Property(property="dtl_ctn_ttl", type="integer", format="int32",
 *              description="Detail Item Carton Total", default=13),
 *         @SWG\Property(property="dtl_crs_doc", type="integer", format="int32", description="Detail Item Dock Total",
 *              default=12)
 *     ),
 *     @SWG\Definition(
 *         definition="AsnsData",
 *         @SWG\Property(
 *              property="data",
 *              @SWG\Property(property="asn_hdr_id", type="integer", format="int32", description="ASN Header Id"),
 *              @SWG\Property(property="asn_hdr_num", type="string", description="ASN header number"),
 *              @SWG\Property(property="cus_id", type="integer", format="int32", description="Customer Id"),
 *              @SWG\Property(property="asn_hdr_ref", type="string", description="ASN header ref"),
 *              @SWG\Property(property="asn_hdr_ept_dt", type="integer", format="int32",
 *                  description="ASN Header ept dt"),
 *              @SWG\Property(property="ctnr_id", type="integer", format="int32", description="Container Id")
 *          )
 *     ),
 *     @SWG\Definition(
 *         definition="Containers",
 *         @SWG\Property(property="ctnr_id", type="integer", format="int32", description="Container Id"),
 *         @SWG\Property(property="ctnr_num", type="string", description="Container number"),
 *         @SWG\Property(property="ctnr_note", type="string", description="Container note"),
 *         @SWG\Property(property="created_at", type="string", description="Created at date"),
 *         @SWG\Property(property="updated_at", type="string", description="Updated at date"),
 *         @SWG\Property(property="created_by", type="integer", format="int32", description="created by user id"),
 *         @SWG\Property(property="updated_by", type="integer", format="int32", description="updated by user id"),
 *         @SWG\Property(property="deleted_at", type="integer", format="int32", description="Deleted_at at date"),
 *         @SWG\Property(property="deleted", type="integer", format="int32", description="Deleted status")
 *     ),
 *     @SWG\Definition(
 *         definition="AsnsHeader",
 *         @SWG\Property(property="asn_hdr_id", type="integer", format="int32", description="ASN header Id"),
 *         @SWG\Property(property="asn_hdr_num", type="string", description="ASN header number"),
 *         @SWG\Property(property="asn_hdr_ref", type="string", description="ASN header ref"),
 *         @SWG\Property(property="asn_date", type="string", format="date", description="ASN header ref"),
 *         @SWG\Property(property="cus_name", type="string", description="Customer name"),
 *         @SWG\Property(property="ctnr_id", type="integer", format="int32", description="Container Id"),
 *         @SWG\Property(property="ctnr_num", type="string", description="Container Number"),
 *         @SWG\Property(property="dtl_crs_doc", type="integer", format="int32", description="Detail Item Dock total"),
 *         @SWG\Property(property="asn_hdr_ept_dt", type="string", format="date", description="ASN header ept dt"),
 *         @SWG\Property(property="dtl_po", type="string", description="Detail Item PO"),
 *         @SWG\Property(property="dtl_po_date", type="string", description="Detail Item PO date"),
 *         @SWG\Property(property="asn_sts", type="string", description="ASN status"),
 *         @SWG\Property(property="asn_sts_name", type="string", description="ASN status name"),
 *         @SWG\Property(property="asn_sts_des", type="string", description="ASN status description"),
 *         @SWG\Property(property="user", type="string", description="User"),
 *         @SWG\Property(property="containers", type="array",
 *              @SWG\Items(ref="#/definitions/Containers")
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="AsnsHeaderResponse",
 *         @SWG\Property(
 *              property="data",
 *              @SWG\Property(property="asn_hdr_id", type="integer", format="int32", description="ASN header Id"),
 *              @SWG\Property(property="asn_hdr_num", type="string", description="ASN header number"),
 *              @SWG\Property(property="asn_hdr_ref", type="string", description="ASN header ref"),
 *              @SWG\Property(property="asn_hdr_ept_dt", type="string", format="date",
 *                  description="ASN header ept dt"),
 *              @SWG\Property(property="asn_hdr_ctn_ttl", type="integer", format="int32",
 *                  description="ASN header container ttl"),
 *              @SWG\Property(property="asn_hdr_itm_ttl", type="integer", format="int32",
 *                  description="ASN header item ttl"),
 *              @SWG\Property(property="sys_measurement_code", type="string", description="System measurement code"),
 *              @SWG\Property(property="asn_sts", type="string", description="ASN status"),
 *              @SWG\Property(property="asn_sts_name", type="string", description="ASN status name"),
 *              @SWG\Property(property="asn_sts_des", type="string", description="ASN status description"),
 *              @SWG\Property(property="whs_id", type="integer", format="int32", description="Warehouse id"),
 *              @SWG\Property(property="whs_code", type="string", description="Warehouse code"),
 *              @SWG\Property(property="whs_name", type="string", description="Warehouse name"),
 *              @SWG\Property(property="cus_id", type="integer", format="int32", description="Customer Id"),
 *              @SWG\Property(property="cus_code", type="string", description="Customer code"),
 *              @SWG\Property(property="cus_name", type="string", description="Customer name"),
 *              @SWG\Property(property="asn_details", type="array",
 *                  @SWG\Items(ref="#/definitions/AsnsDetailResponse")
 *              ),
 *              @SWG\Property(
 *                  property="created_at",
 *                  @SWG\Property(property="date", type="string", format="date-time", description="Date"),
 *                  @SWG\Property(property="timezone_type", type="integer", format="int32",
 *                      description="Timezone Type"),
 *                  @SWG\Property(property="timezone", type="string", description="Timezone")
 *              ),
 *              @SWG\Property(
 *                  property="updated_at",
 *                  @SWG\Property(property="date", type="string", format="date-time", description="Date"),
 *                  @SWG\Property(property="timezone_type", type="integer", format="int32",
 *                      description="Timezone Type"),
 *                  @SWG\Property(property="timezone", type="string", description="Timezone")
 *              )
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="AsnsDetailResponse",
 *         @SWG\Property(property="asn_dtl_id", type="integer", format="int32", description="ASN detail Id"),
 *         @SWG\Property(property="asn_hdr_id", type="integer", format="int32", description="ASN Header Id"),
 *         @SWG\Property(property="ctnr_id", type="integer", format="int32", description="Container Id"),
 *         @SWG\Property(property="ctnr_num", type="string", description="Container number"),
 *         @SWG\Property(property="dtl_itm_id", type="integer", format="int32", description="Detail Item Id"),
 *         @SWG\Property(property="dtl_item_code", type="string", description="Detail Item code"),
 *         @SWG\Property(property="dtl_suffix", type="string", description="Suffix"),
 *         @SWG\Property(property="dtl_sku", type="string", description="Detail Item SKU"),
 *         @SWG\Property(property="dtl_size", type="string", description="Detail Item size"),
 *         @SWG\Property(property="dtl_color", type="string", description="Detail Item color"),
 *         @SWG\Property(property="dtl_uom_id", type="integer", format="int32", description="Detail Item UOM Id"),
 *         @SWG\Property(property="dtl_uom_code", type="string", description="Detail Item UOM code"),
 *         @SWG\Property(property="dtl_uom_name", type="string", description="Detail Item UOM name"),
 *         @SWG\Property(property="dtl_po", type="string", description="Detail Item PO"),
 *         @SWG\Property(property="dtl_po_date", type="string", format="date", description="Detail Item PO Date"),
 *         @SWG\Property(property="dtl_ctn_ttl", type="integer", format="int32",
 *              description="Detail Item Carton Total"),
 *         @SWG\Property(property="gr_hdr_num", type="string", description="Goods receipt header number"),
 *         @SWG\Property(property="dtl_gr_dtl_act_ctn_ttl", type="integer", format="int32"),
 *         @SWG\Property(property="gr_dtl_plt_ttl", type="integer", format="int32"),
 *         @SWG\Property(property="dtl_gr_dtl_is_dmg", type="integer", format="int32"),
 *         @SWG\Property(property="dtl_gr_dtl_disc", type="integer", format="int32"),
 *         @SWG\Property(property="dtl_crs_doc", type="integer", format="int32"),
 *         @SWG\Property(property="dtl_des", type="string", description="Description"),
 *         @SWG\Property(property="dtl_length", type="number", format="float", description="Detail Item Length"),
 *         @SWG\Property(property="dtl_width", type="number", format="float", description="Detail Item Width"),
 *         @SWG\Property(property="dtl_height", type="number", format="float", description="Detail Item Height"),
 *         @SWG\Property(property="dtl_weight", type="number", format="float", description="Detail Item Weight"),
 *         @SWG\Property(property="dtl_pack", type="integer", format="int32", description="Detail Item Pack")
 *     ),
 *     @SWG\Definition(
 *         definition="ListContainers",
 *         @SWG\Property(property="ctnr_id", type="integer", format="int32", description="Container Id"),
 *         @SWG\Property(property="ctnr_num", type="string", description="Container number")
 *     ),
 *     @SWG\Definition(
 *         definition="AsnStatuses",
 *         @SWG\Property(property="asn_sts", type="string", description="Asn status"),
 *         @SWG\Property(property="asn_sts_name", type="string", description="Asn status name"),
 *         @SWG\Property(property="asn_sts_des", type="string", description="Asn status description")
 *     ),
 *     @SWG\Definition(
 *         definition="GoodsReceiptResponse",
 *         @SWG\Property(
 *              property="data",
 *              @SWG\Property(property="asn_hdr_num", type="string", description="ASN header number"),
 *              @SWG\Property(property="asn_hdr_ref", type="string", description="ASN header ref"),
 *              @SWG\Property(property="sys_measurement_code", type="string", description="System measurement code"),
 *              @SWG\Property(property="gr_hdr_id", type="integer", format="int32",
 *                  description="Goods receipt header id"),
 *              @SWG\Property(property="ctnr_id", type="integer", format="int32", description="Container Id"),
 *              @SWG\Property(property="ctnr_num",  type="string", description="Container Number"),
 *              @SWG\Property(property="ctnr_note", type="string", description="Container note"),
 *              @SWG\Property(property="gr_hdr_ept_dt", type="string", description="Goods receipt header ept dt"),
 *              @SWG\Property(property="gr_hdr_num", type="string", description="Goods receipt header number"),
 *              @SWG\Property(property="whs_id", type="integer", format="int32", description="Warehouse id"),
 *              @SWG\Property(property="cus_id", type="integer", format="int32", description="Customer Id"),
 *              @SWG\Property(property="cus_code", type="string", description="Customer code"),
 *              @SWG\Property(property="cus_name", type="string", description="Customer name"),
 *              @SWG\Property(property="gr_in_note", type="string", description="Goods receipt in note"),
 *              @SWG\Property(property="gr_ex_note", type="string", description="Goods receipt ex note"),
 *              @SWG\Property(property="gr_sts_code", type="string", description="Goods receipt status code"),
 *              @SWG\Property(property="gr_sts_name", type="string", description="Goods receipt status name"),
 *              @SWG\Property(property="gr_sts_desc", type="string", description="Goods receipt status description"),
 *              @SWG\Property(property="asn_details", type="array",
 *                  @SWG\Items(ref="#/definitions/GoodsReceiptDetailResponse")
 *              )
 *         ),
 *     ),
 *     @SWG\Definition(
 *         definition="GoodsReceiptDetailResponse",
 *         @SWG\Property(property="dtl_gr_dtl_id", type="integer", format="int32",
 *              description="Goods receipt detail Id"),
 *         @SWG\Property(property="dtl_itm_id", type="integer", format="int32", description="Detail Item Id"),
 *         @SWG\Property(property="dtl_item_code", type="string", description="Detail Item code"),
 *         @SWG\Property(property="dtl_sku", type="string", description="Detail Item SKU"),
 *         @SWG\Property(property="dtl_suffix", type="string", description="Suffix"),
 *         @SWG\Property(property="dtl_color", type="string", description="Detail Item color"),
 *         @SWG\Property(property="dtl_size", type="string", description="Detail Item size"),
 *         @SWG\Property(property="dtl_length", type="number", format="float", description="Detail Item Length"),
 *         @SWG\Property(property="dtl_width", type="number", format="float", description="Detail Item Width"),
 *         @SWG\Property(property="dtl_height", type="number", format="float", description="Detail Item Height"),
 *         @SWG\Property(property="dtl_weight", type="number", format="float", description="Detail Item Weight"),
 *         @SWG\Property(property="dtl_gr_dtl_ept_ctn_ttl", type="integer", format="int32",
 *              description="Goods receipt detail ept container ttl"),
 *         @SWG\Property(property="dtl_gr_dtl_act_ctn_ttl", type="integer", format="int32",
 *              description="Goods receipt detail act container ttl"),
 *         @SWG\Property(property="dtl_gr_dtl_plt_ttl", type="integer", format="int32",
 *              description="Goods receipt detail plt ttl"),
 *         @SWG\Property(property="dtl_asn_dtl_crs_doc", type="integer", format="int32",
 *              description="ASN detail crs doc"),
 *         @SWG\Property(property="dtl_gr_dtl_disc", type="integer", format="int32",
 *              description="Goods receipt detail disc"),
 *         @SWG\Property(property="dtl_gr_dtl_is_dmg", type="integer", format="int32",
 *              description="Goods receipt detail is dmg"),
 *         @SWG\Property(property="dtl_asn_dtl_po", type="string", description="ASN detail po"),
 *         @SWG\Property(property="dtl_asn_dtl_po_dt", type="string", description="ASN detail po dl"),
 *         @SWG\Property(property="dtl_uom_id", type="integer", format="int32", description="Detail Item UOM Id"),
 *         @SWG\Property(property="dtl_uom_name", type="string", description="Detail Item UOM name")
 *     ),
 *     @SWG\Definition(
 *         definition="InputGoodsReceiptsData",
 *         @SWG\Property(property="gr_in_note", type="string", description="Goods receipt in note", default="in note"),
 *         @SWG\Property(property="gr_ex_note", type="string", description="Goods receipt ex note", default="ex note"),
 *         @SWG\Property(property="gr_received_pending", type="integer", format="int32",
 *           description="0: is for submit button 1: is for Rcvd-Pending", default=1),
 *         @SWG\Property(property="details", type="array",
 *              @SWG\Items(ref="#/definitions/GoodsReceiptDetailInput")
 *          )
 *      ),
 *      @SWG\Definition(
 *         definition="GoodsReceiptDetailInput",
 *         @SWG\Property(property="gr_dtl_id", type="integer", format="int32", description="Goods receipt detail Id",
 *              default=1),
 *         @SWG\Property(property="gr_dtl_act_ctn_ttl", type="integer", format="int32",
 *              description="Goods receipt detail act container ttl", default=1),
 *         @SWG\Property(property="gr_dtl_plt_ttl", type="integer", format="int32",
 *              description="Goods receipt detail plt ttl", default=1),
 *         @SWG\Property(property="gr_dtl_is_dmg", type="integer", format="int32",
 *              description="Goods receipt detail is dmg", default=1)
 *     ),
 *     @SWG\Definition(
 *         definition="GoodsReceiptsData",
 *         @SWG\Property(
 *              property="data",
 *              @SWG\Property(property="asn_hdr_num", type="string", description="ASN header number"),
 *              @SWG\Property(property="asn_hdr_ref", type="string", description="ASN header ref"),
 *              @SWG\Property(property="sys_measurement_code", type="string", description="System measurement code"),
 *              @SWG\Property(property="gr_hdr_id", type="integer", format="int32",
 *                  description="Goods receipt header Id"),
 *          )
 *     ),
 *     @SWG\Definition(
 *         definition="GoodsReceiptsList",
 *         @SWG\Property(property="asn_hdr_num", type="string", description="ASN header number"),
 *         @SWG\Property(property="asn_hdr_ref", type="string", description="ASN header ref"),
 *         @SWG\Property(property="gr_hdr_id", type="integer", format="int32",
 *                  description="Goods receipt header id"),
 *         @SWG\Property(property="ctnr_num", type="string", description="Container Number"),
 *         @SWG\Property(property="gr_hdr_num", type="string", description="Goods receipt header number"),
 *         @SWG\Property(property="gr_sts_name", type="string", description="Goods receipt status name"),
 *         @SWG\Property(property="whs_id", type="integer", format="int32", description="Warehouse id"),
 *         @SWG\Property(property="cus_id", type="integer", format="int32", description="Customer Id"),
 *         @SWG\Property(property="cus_name", type="string", description="Customer name"),
 *         @SWG\Property(property="gr_ctn_ttl", type="string", description="Goods receipt ctn ttl"),
 *         @SWG\Property(property="gr_act_ctn_ttl", type="string", description="Goods receipt act ctn ttl"),
 *         @SWG\Property(property="gr_crs_doc", type="integer", format="int32",
 *                  description="Goods receipt crs dock total"),
 *         @SWG\Property(property="gr_dtl_is_dmg", type="integer", format="int32",
 *                  description="Goods receipt dtl  is dmg")
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */
abstract class AbstractController extends Controller
{
    use Helpers;

    private  static $userInfo;

    final public static function getCurrentUserInfo()
    {
        $userInfo = new Data();
        if(! self::$userInfo){
            self::$userInfo =  $userInfo->getUserInfo();
        }
        return self::$userInfo;
    }

    final public static function getCurrentUserId()
    {
        $userInfo = self::getCurrentUserInfo();

        return $userInfo['user_id'];
    }

    final public static function getCurrentWhsId()
    {
        $userInfo = self::getCurrentUserInfo();

        return $userInfo['current_whs'];
    }
    /**
     * Refresh model to be clean
     */
    public function refreshModel()
    {
        $this->model = $this->model->newInstance();
    }


}
