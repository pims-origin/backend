<?php

use App\Api\V1\Models\AsnDtlModel;

trait TraitTest
{

    protected function setUpData()
    {
        
    }

    protected function getAsnInfo()
    {
        $asnModel = new AsnDtlModel();
        $input['whs_id'] = $this->whsId;
        $return = $asnModel->getListASN(
                $input, null
        );
        $obj = $return->getCollection()->first();

        return $obj;
    }

    protected function APIScanCartons($whsId, $data, $vtlCtnOnPallet)
    {
        $asnHdrId = $data['asn_hdr_id'];
        //check them item co hay ko
        if (!isset($data['items'])) {
            $this->markTestIncomplete("[INCO] No data to test\n");
        }
        $asnDtlId = $data['items'][0]['asn_dtl_id'];
        $itemId = $data['items'][0]['item_id'];
        $ctnrId = $data['ctnr_id'];
        $cusId = $data['cus_id'];

        $params = [
            'asn_hdr_id' => $asnHdrId,
            'asn_dtl_id' => $asnDtlId,
            'ctn_rfid.0' => '',
            'ctn_rfid' => "",
            'ctns_rfid' => $vtlCtnOnPallet,
            'gate_code' => '',
            'type' => 1,
            'item_id' => $itemId,
            'ctnr_id' => $ctnrId,
                //'discrepancy' => '',
        ];

        $url = "/inbound/whs/{$whsId}/cus/{$cusId}/add-virtual-cartons";
        echo "[RURL] 1.{$url}\n";
        $this->refreshApplication();
        $response = $this->call("POST", $url, $params, [], []
                , ['HTTP_Authorization' => $this->_token]
        );

        return $response;
    }

    protected function APIScanPallet($whsId, $param)
    {
        $url = "/inbound/whs/{$this->whsId}/scan-pallet/";
        echo "[RURL] 2.{$url}\n";
        echo "[DATA] 2." . json_encode($param) . "\n";
        $this->refreshApplication();
        $res = $this->call('PUT', $url, $param);

        return $res;
    }

    /**
     * Call Put Away API
     * 
     * @param integer $whsId
     * @param string $palletRfid
     * @param string $locRfid
     * 
     * @return response
     */
    protected function APIPutAway($whsId, $palletRfid, $locRfid)
    {
        $url = "/inbound/whs/{$whsId}/location/rack/put-pallet";
        echo "[RURL] {$url}\n";
        $param = [
            'pallet-rfid' => $palletRfid,
            'loc-rfid' => $locRfid
        ];

        $this->refreshApplication();
        $result = $this->call("PUT", $url, $param, [], [], ['HTTP_Authorization' => $this->_token]);

        return $result;
    }

    protected function APIScanPalletPUT($whsId, $vtlCtnRfid, $newPltRfid)
    {
        $param = [
            'pallet.pallet-rfid' => $newPltRfid,
            'pallet.ctn-rfid' => [$vtlCtnRfid]
        ];
        $url = "/inbound/whs/{$whsId}/scan-pallet/";
        echo "[MESS] Put scan pallet plt_rfid:{$newPltRfid}, ctn_rfid: {$vtlCtnRfid}\n";
        echo "[CALL] {$url}\n";
        echo "[MESS] Data submit " . json_encode($param) . "\n";
        $this->refreshApplication();
        
        return $this->call("PUT", $url, $param);
    }

}
