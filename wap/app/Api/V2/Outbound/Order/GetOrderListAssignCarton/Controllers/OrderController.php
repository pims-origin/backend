<?php

namespace App\Api\V2\Outbound\Order\GetOrderListAssignCarton\Controllers;

use App\Api\V2\Outbound\Order\GetOrderListAssignCarton\Models\OrderHdrModel;
use App\libraries\RFIDValidate;

use App\Api\V2\Outbound\Order\GetOrderListAssignCarton\Transformers\OrderListTransformer;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\SelArr;

class OrderController extends AbstractController
{

    protected $orderHdrModel;

    public function __construct()
    {
        $this->orderHdrModel = new OrderHdrModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function getListOrder($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/v2/{$whsId}/order/list-assign-carton";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GLO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get List Order'
        ]);
        /*
         * end logs
         */
        try {

            $sku    = array_get($input, 'sku', '');
            $odrSts = array_get($input, 'odr_sts', '');
            $odrNum = array_get($input, 'odr_num', '');

            $odrStsArr = explode(',', $odrSts);
            $odrStsArr = SelArr::removeNullOrEmptyString($odrStsArr);

            $orders = $this->orderHdrModel->getPackingOrders($whsId, $sku, array_get($input, 'limit', 20), $odrStsArr, $odrNum);

            $orderListTransformer = new OrderListTransformer(['sku'=>$sku]);

            foreach($orders as $order){
                $order->is_assigned_label = $this->_isAssignedLabel($order->odr_id, $order->odr_sts);
                // $order->is_assigned_ctn2pallet = $this->_isAssignedPallet($order->odr_id, $order->odr_sts);
            }

            return $this->response->paginator($orders, $orderListTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _isAssignedLabel($odrId, $odrSts)
    {
        if (!in_array($odrSts, ['PN', 'PA'])) {
            return false;
        }

        $isCompleteLable = DB::table('odr_hdr_meta')->where('odr_id', $odrId)
                                ->where('qualifier', 'LBL')
                                ->first();

        if (count($isCompleteLable) > 0) {
            return false;
        }

        $isFinalWorkOrder = DB::table('vas_hdr')->where('odr_hdr_id', $odrId)
                                ->where('deleted', 0)
                                // ->where('vas_sts', 'FN')
                                ->first();

        if (!count($isFinalWorkOrder) || array_get($isFinalWorkOrder, 'vas_sts') == "FN") {
            return false;
        }

        return true;
    }

    private function _isAssignedPallet($odrId, $odrSts)
    {
        if (!in_array($odrSts, ['PN', 'PA'])) {
            return false;
        }

        return true;
    }
}