<?php

namespace App\Api\V1\Validators;

class DeleteMassZoneTypeValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'zone_type_id'   => 'required',
            'zone_type_id.*' => 'required|exists:zone_type,zone_type_id'
        ];
    }
}