<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Refill;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class RefillModel extends AbstractModel
{

    /**
     * @param Refill $model
     */
    public function __construct(Refill $model = null)
    {
        parent::__construct($model);
    }

    /**
     * @param $refillId
     *
     * @return mixed
     */
    public function deleteRefill($refillId)
    {
        return $this->model
            ->where('rf_id', $refillId)
            ->delete();
    }

    /**
     * @param $refillIds
     *
     * @return mixed
     */
    public function deleteRefills($refillIds)
    {
        return $this->model
            ->whereIn('rf_id', $refillIds)
            ->delete();
    }

    /**
     * @param int $itemId
     * @param int $locationId
     *
     * @return mixed
     */
    public function checkByItemIdAndLocId($itemId ,$locationId)
    {
        return $this->model
            ->where('item_id', $itemId)
            ->where('loc_id','!=', $locationId)
            ->where('deleted', 0)
            ->count();
    }

}
