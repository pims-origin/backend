<?php

namespace App\Api\V2\Outbound\RemoveCartons\Order\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class WavePickHdrModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickHdr($data){
        return $this->model->create($data);
    }

    /**
     * @return mixed
     */
    public function getLatestWavePickNumber()
    {
        return $this->model->orderBy('wv_num', 'desc')->take(1)->first();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getWvHdrInfo($wv_id){
        return $this->model
            ->where('wv_hdr.wv_id','=',$wv_id)
            ->leftJoin('odr_hdr','odr_hdr.wv_id','=','wv_hdr.wv_id')
            ->leftJoin('customer','customer.cus_id','=','odr_hdr.cus_id')
            ->select(
                'wv_hdr.wv_id',
                'wv_hdr.wv_num',
                'odr_hdr.cus_id',
                'customer.cus_name',
                'wv_hdr.wv_sts',
                'wv_hdr.whs_id'
            )
            ->first();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['wv_num','wv_sts'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }
        $this->model->filterData($query);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function updateStatusWvHdr($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param string $wvId
     *
     * @return mixed
     */
    public function updatePicking($wvId)
    {
        $sqlRaw = "(
        (SELECT COUNT(1) FROM odr_dtl WHERE odr_dtl.odr_id = odr_hdr.odr_id AND odr_dtl.itm_sts IN ('NW', 'PK') AND deleted =0) != 0
        or
        (SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty != wv_dtl.piece_qty AND deleted =0) != 0
        )";

        return \DB::table('wv_hdr')
            ->join('odr_hdr', 'odr_hdr.wv_num', '=', 'wv_hdr.wv_num')
            ->where('wv_hdr.wv_id', $wvId)
            ->where('wv_hdr.deleted', 0)
            ->where('odr_hdr.deleted', 0)
            ->whereRaw(DB::Raw($sqlRaw))
            ->update([
                'wv_hdr.wv_sts'     => Status::getByValue("Picking", "WAVEPICK-STATUS"),
                'wv_hdr.updated_at' => time(),
                'wv_hdr.updated_by' => Data::getCurrentUserId(),
            ]);
    }

    public function updatePickingAfterDeleteCartons($wvHdrId) {
        return $this->getModel()->where([
                            'wv_id' => $wvHdrId
                        ])
                        ->update([
                            'wv_sts' => Status::getByValue("Picking", "WAVEPICK-STATUS")
        ]);
    }



    /**
     * @param string $wvId
     *
     * @return mixed
     */
    public function updatePickedV2($wvId)
    {
        $sqlOdrPicked = sprintf("(SELECT COUNT(1) FROM odr_dtl WHERE odr_dtl.wv_id = %s AND odr_dtl.itm_sts IN ('NW', 'PK') AND deleted =0 AND alloc_qty > 0 and odr_hdr.odr_id = odr_dtl.odr_id) = 0", $wvId);
        $sqlWvPickedStr = "(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.wv_dtl_sts IN ('NW', 'PK') AND deleted =0) = 0";
        return \DB::table('wv_hdr')
            ->join('odr_hdr', 'odr_hdr.wv_num', '=', 'wv_hdr.wv_num')
            ->where('wv_hdr.wv_id', $wvId)
            ->where('wv_hdr.deleted', 0)
            ->where('odr_hdr.deleted', 0)
            ->whereRaw(DB::Raw($sqlWvPickedStr))
            ->whereRaw(DB::Raw($sqlOdrPicked))
            ->update([
                'wv_hdr.wv_sts'     => Status::getByValue("Completed", "WAVEPICK-STATUS"),
                'wv_hdr.updated_at' => time(),
                'wv_hdr.updated_by' => Data::getCurrentUserId(),
            ]);
    }


}
