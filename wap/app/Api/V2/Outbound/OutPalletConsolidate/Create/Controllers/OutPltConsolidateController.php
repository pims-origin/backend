<?php

namespace App\Api\V2\Outbound\OutPalletConsolidate\Create\Controllers;

use App\Api\V2\Outbound\OutPalletConsolidate\Create\Models\OutPalletModel;
use App\Api\V2\Outbound\OutPalletConsolidate\Create\Validators\OutPalletValidator;
use App\Api\V2\Outbound\OutPalletConsolidate\Create\Validators\MasterOutPalletValidator;

use App\Api\OUTBOUND\Transformers\OrderListShippingTransformer;
use App\Api\OUTBOUND\Transformers\OrderListTransformer;
use App\Api\OUTBOUND\Transformers\OrderDetailTransformer;
use App\Api\OUTBOUND\Transformers\OrderByOdrIDTransformer;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OutPltConsolidateController extends AbstractController
{

    protected $outPalletModel;

    public function __construct()
    {
        $this->outPalletModel = new OutPalletModel();
    }

    const PREFIX_MASTER_PALLET = "MPL";

    /**
     * @param Request $request
     *
     * @return Response|void
     */
    public function validationPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/out-plt-consolidate/validate-pallet";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'VOC',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Validate Out pallet consolidate'
        ]);

        /*
         * end logs
         */

        $pltNum = array_get($input, 'mpl_num');
        if (!$pltNum || $pltNum == "") {
            $msg = "The Master out pallet number is required!";
            return $this->_responseErrorMessage($msg);
        }

        $codeArr     = explode('-', $pltNum);
        $first       = array_shift($codeArr);
        $isMasterPlt = false;
        if ($first == self::PREFIX_MASTER_PALLET) {
            $isMasterPlt = true;
            $masterOutPalletValidator = new MasterOutPalletValidator();
            $pltNumValid = $masterOutPalletValidator->validate($pltNum, $whsId);
            if (!$pltNumValid) {
                return $this->_responseErrorMessage($masterOutPalletValidator->getError());
            }
        } else {
            // only validate for master out pallet
            $mplCorrect = sprintf("Your master out pallet's number %s is invalid.+ Please check Example: + First 3 %s: Pallet Pattern; + Next 2 columns: Belong to BOL Number + Last digits: Sequence Pallet Number (Decimal)",$pltNum, MasterOutPalletValidator::PRE_FIX);
            return $this->_responseErrorMessage($mplCorrect);
            // $pltNumValid = OutPalletValidator::validate($pltNum);
            // if (!$pltNumValid) {
            //     return $this->_responseErrorMessage(OutPalletValidator::ERROR_MSG);
            // }
        }

        try {

            $checkPltExist = $this->outPalletModel->getFirstWhere([
                    'plt_num' => $pltNum,
                    'whs_id'  => $whsId,
                ]);

            $data = [[
                    'mpl_num'  => $pltNum,
                    'plt_nums' => [],
                ]];

            if ($isMasterPlt && $checkPltExist) {
                $pltNums = $this->outPalletModel->getModel()
                    ->where('parent', $checkPltExist->plt_id)
                    ->where('plt_id', "!=", $checkPltExist->plt_id)
                    ->pluck('plt_num');

                $data = [[
                    'mpl_num'  => $pltNum,
                    'plt_nums' => $pltNums,
                ]];
            }

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data']   = $data;

            return $msg;


        } catch (\Exception $e) {
            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function store($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/out-plt-consolidate/create";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'OPC',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Out pallet consolidate'
        ]);

        /*
         * end logs
         */

        $input['whs_id'] = $whsId;
        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        $pltNum = array_get($input, 'plt_num');
        $mplNum = array_get($input, 'mpl_num');

        $pallet = $this->outPalletModel->getFirstWhere([
            'whs_id'  => $whsId,
            'plt_num' => $pltNum,
        ]);

        if (!$pallet) {
            $msg = "The Pallet number doesn't existed.";
            $pallet = $this->outPalletModel->getFirstWhere([
                'plt_num' => $pltNum,
            ]);
            if ($pallet) {
                $msg = "The Pallet number doesn't belong to current warehouse.";
            }
            return $this->_responseErrorMessage($msg);
        }

        $masterPallet = $this->outPalletModel->getFirstWhere([
            'plt_num' => $mplNum,
        ]);

        if ($masterPallet && $masterPallet->whs_id != $whsId) {
            $msg = "The master pallet number doesn't belong to current warehouse.";
            return $this->_responseErrorMessage($msg);
        }

        //Cannot Consolidate when Finalize BOL
        if ($bolIdPlt = object_get($pallet, 'bol_id')) {
            $shipmentObj = DB::table('shipment')
                ->where('ship_id' , $bolIdPlt)
                ->where('deleted' , 0)
                ->first();

            $sts = array_get($shipmentObj, 'ship_sts');
            if ($sts == "FN") {
                $msg = sprintf("Unable to consolidate out pallet when BOL already finalized.", $pltNum);
                return $this->_responseErrorMessage($msg);
            }

            // check master pallet belong to BOL number
            $codeArr     = explode('-', $mplNum);
            $bolNumCheck = "BOL-{$codeArr[1]}-{$codeArr[2]}";
            $realBoNum   = array_get($shipmentObj, 'bo_num');
            if ($bolNumCheck != $realBoNum) {
                $msg = sprintf("Master out pallet %s doesn't belong to Bill of Ladings number %s.", $mplNum, $realBoNum);
                return $this->_responseErrorMessage($msg);
            }

        } else {
            $msg = sprintf("Unable to consolidate out pallet when BOL isn't created yet.", $pltNum);
            return $this->_responseErrorMessage($msg);
        }

        if ($pallet->plt_id != $pallet->parent) {
            $otherMasterPallet = $this->outPalletModel->getFirstWhere([
                'plt_id' => $pallet->parent,
            ]);
            $msg = sprintf("Out pallet number %s belonged to Master out pallet %s.", $pltNum, $otherMasterPallet->plt_num);
            return $this->_responseErrorMessage($msg);
        }

        try {
            // start transaction
            DB::beginTransaction();

            if (!$masterPallet) {
                // create new master pallet
                $dataMpl = [
                    'whs_id'      => $whsId,
                    'cus_id'      => $pallet->cus_id,
                    'plt_num'     => $mplNum,
                    'ctn_ttl'     => 0,
                    'out_plt_sts' => 'AC',
                    'is_movement' => 0,
                    'bol_id'      => $pallet->bol_id,
                ];
                $masterPallet = $this->outPalletModel->create($dataMpl);
            }

            if ($masterPallet->plt_id == $pallet->parent) {
                \DB::rollback();
                $msg = sprintf("Out pallet number %s and Master out pallet %s already consolidated.", $pltNum, $mplNum);
                return $this->_responseErrorMessage($msg);
            }

            if ($masterPallet->bol_id && $masterPallet->bol_id != $pallet->bol_id) {
                \DB::rollback();
                $msg = sprintf("Out pallet number %s and Master out pallet %s are not the same a Bill of Ladings.", $pltNum, $mplNum);
                return $this->_responseErrorMessage($msg);
            }

            $pallet->parent = $masterPallet->plt_id;
            $pallet->save();

            $masterPallet->ctn_ttl += $pallet->ctn_ttl;
            $masterPallet->parent = $masterPallet->plt_id;
            $masterPallet->save();

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Assign Pallet consolidation successfully!"
            ]];
            $msg['data']   = [];

            return $msg;

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _validateParams($attributes, $whsId)
    {
        $mplNum = array_get($attributes, 'mpl_num');
        $pltNum = array_get($attributes, 'plt_num');

        $names = [
            'mpl_num' => 'Master pallet number',
            'plt_num' => 'Pallet number',
        ];

        // Check Required
        $requireds = [
            'mpl_num' => $mplNum,
            'plt_num' => $pltNum,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet number is string
        if (!is_string($mplNum) || !is_string($pltNum)) {
            $msg = "Master outpallet and pallet number must be string type";
            return $this->_responseErrorMessage($msg);
        }

        //validate pallet number
        $masterOutPalletValidator = new MasterOutPalletValidator();
        $mplNumValid = $masterOutPalletValidator->validate($mplNum, $whsId);
        if (!$mplNumValid) {
            return $this->_responseErrorMessage($masterOutPalletValidator->getError());
        }

        //validate pallet num
        $pltNumValid = OutPalletValidator::validate($pltNum);
        if (!$pltNumValid) {
            return $this->_responseErrorMessage(OutPalletValidator::ERROR_MSG);
        }

    }
}