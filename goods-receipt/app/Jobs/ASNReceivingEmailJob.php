<?php

namespace App\Jobs;

use DB;
use Illuminate\Support\Facades\Mail;

class ASNReceivingEmailJob extends Job
{
    protected $data;
    
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        $sendToDB = DB::table('cus_meta')->where('cus_id', $this->data['asnHrd']['cus_id'])->where('qualifier', 'EEX')->where('deleted', 0)->value('value');

        if(!empty($sendToDB)) {

            $sendToInfo = json_decode($sendToDB);

            $emailDB = DB::table('settings')->where('setting_key', 'EC')->value('setting_value');

            if(empty($emailDB)) {
                $this->writeLogment('ASN Receiving Email', "System Email is not configurate.");
                return false;
            }

            $emailConfig = json_decode($emailDB);

            config([
                'mail.host'         => $emailConfig->host,
                'mail.port'         => $emailConfig->port,
                'mail.encryption'   => $emailConfig->encryption,
                'mail.username'     => $emailConfig->username,
                'mail.password'     => $emailConfig->password,
                'mail.from.name'    => $emailConfig->name,
                'mail.from.address' => 'seldat-support@gmail.com'
            ]);

            $emailInfo = [
                'toAddress' =>  $sendToInfo->email,
                'toName'    =>  null,
                'subject'   =>  'ASN Wait Approve Items'
            ];

            try {
                $data = $this->data;
                $data['editLink'] = env('ITEM_MASTER_EDIT_URL', null);

                if(empty($data['editLink'])) {
                    $this->writeLogment('ASN Receiving Email', "Config ITEM_MASTER_EDIT_URL is not exist");
                    return false;
                }

                Mail::send('email.ASNCompleteReceivingEmailTemplate', $data, function($message) use ($emailInfo){
                    $message->to($emailInfo['toAddress'], $emailInfo['toName'])->subject($emailInfo['subject']);
                });

                $this->writeLogment('ASN Receiving Email', "Wait-Approve Items list has been sent!");
            } catch (\Exception $e) {
                $this->writeLogment('ASN Receiving Email', $e->getMessage());
            }
        }
    }
}
