<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 30-May-16
 * Time: 09:08
 */

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Models\WarehouseAddressModel;
use Illuminate\Http\Response as IlluminateResponse;

class WarehouseAddressControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $warehouseId;

    protected $warehouseParams = [
        'whs_name'       => 'warehouse from functional test',
        'whs_status'     => 'IA',
        'whs_short_name' => 'phone from functional test',
        'created_at'     => 1465961195,
        'updated_at'     => 1465961195,
        'deleted_at'     => 915148800,
        'deleted_at'     => 0,
    ];

    protected $warehouseAddressParams = [
        'whs_add_line_1'       => 'line1 functional test',
        'whs_add_line_2'       => 'add line 2 functional test',
        'whs_add_country_code' => 'country code from functional test',
        'whs_add_city_name'    => 'city name from functional test',
        'whs_add_state_id'     => 123,
        'whs_add_postal_code'  => 'postal code from functional test',
        'whs_add_whs_id'       => 1,
        'whs_add_type'         => 'type from functional test',
    ];

    /**
     * WarehouseAddressControllerCest constructor.
     */
    public function __construct()
    {

    }

    public function setUp()
    {
        parent::setUp();

        $warehouse = factory(App\Warehouse::class)->create();
        $this->warehouseId = $warehouse->whs_id;
        $this->warehouseContactParams['whs_con_whs_id'] = $this->warehouseId;
    }

    private function getEndPoint($warehouseAddressId = null)
    {
        return "/v1/warehouses/{$this->warehouseId}/addresses" . ($warehouseAddressId ? '/' . $warehouseAddressId : '');
    }

    public function testList_Ok()
    {
        // make data
        factory(App\WarehouseAddress::class, 3)->create(['whs_add_whs_id' => $this->warehouseId]);

        $uri = $this->getEndPoint();
        $response = $this->call('GET', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
        $this->assertCount(3, $responseData['data']);
    }

    public function testShow_Ok()
    {
        // make data
        $address = factory(App\WarehouseAddress::class)->create();
        $warehouseAddressId = $address->whs_add_id;

        $uri = $this->getEndPoint($warehouseAddressId);

        $this->call('GET', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $uri = $this->getEndPoint();

        $response = $this->call('POST', $uri, $this->warehouseAddressParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testUpdate_Ok()
    {
        // make data
        $address = factory(App\WarehouseAddress::class)->create();
        $warehouseAddressId = $address->whs_add_id;

        $uri = $this->getEndPoint($warehouseAddressId);

        $response = $this->call('PUT', $uri, $this->warehouseAddressParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testUpdate_WithZero_Fail()
    {
        $uri = $this->getEndPoint(99999);

        $this->call('PUT', $uri, $this->warehouseAddressParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDelete_Ok()
    {
        // make data
        $address = factory(App\WarehouseAddress::class)->create(['whs_add_whs_id' => $this->warehouseId]);
        $warehouseAddressId = $address->whs_add_id;

        $uri = $this->getEndPoint($warehouseAddressId);

        $this->call('DELETE', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        $this->seeInDatabase((new App\WarehouseAddress())->getTable(),
            ['whs_add_id' => $address->whs_add_id, 'deleted' => 1]);
    }

    public function testDelete_NotExit_Fail()
    {
        $uri = $this->getEndPoint(99999);

        $this->call('DELETE', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }
}