<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\OutPalletModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Transformers\PackDtlTransformer;
use App\Api\V1\Transformers\PackHdrTransformer;
use App\Api\V1\Transformers\ViewAssignPalletTransformer;
use App\Api\V1\Validators\PackHdrAutoValidator;
use App\Api\V1\Validators\PackHdrValidator;
use App\Jobs\BOLJob;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Models\Warehouse;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Profiler;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use TCPDF;
use yii\helpers\VarDumper;
use mPDF;
use Wms2\UserInfo\Data;

/**
 * Class PackHdrController
 *
 * @package App\Api\V1\Controllers
 */
class PackHdrController extends AbstractController
{
    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var OutPalletModel
     */
    protected $outPalletModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var WavePickDtlModel
     */
    protected $wavePickDtlModel;
    protected $orderHdrMetaModel;

    /**
     * @param PackHdrModel $packHdrModel
     * @param PackDtlModel $packDtlModel
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param OutPalletModel $outPalletModel
     */
    public function __construct(
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel,
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        OutPalletModel $outPalletModel,
        EventTrackingModel $eventTrackingModel,
        WavePickDtlModel $wavePickDtlModel
    ) {
        $this->packHdrModel = $packHdrModel;
        $this->packDtlModel = $packDtlModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->outPalletModel = $outPalletModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->wavePickDtlModel = $wavePickDtlModel;
        $this->orderHdrMetaModel = new OrderHdrMetaModel();
    }


    /**
     * @param $orderId
     * @param PackDtlTransformer $packDtlTransformer
     *
     * @return Response|void
     */
    public function show($orderId, PackDtlTransformer $packDtlTransformer)
    {
        try {
            // Load Packing Info
            $packHdr = $this->packHdrModel->allBy("odr_hdr_id", $orderId, ['systemUom'])->toArray();

            $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId, ['packHdr.details', 'packHdr.packRef', 'packHdr.carton']);
            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            $orderHdr->piece_ttl = !empty($packHdr) ? array_sum(array_pluck($packHdr, "piece_ttl")) : 0;
            $orderHdr->ctn_ttl = count($packHdr);

            // Load Items
            $orderDtl = $this->orderDtlModel
                ->allBy('odr_id', $orderHdr->odr_id, ['inventorySummary.item', 'systemUom'])
                ->toArray();

            $items = [];
            $skuTotal = 0;
            if (!empty($orderDtl)) {
                foreach ($orderDtl as $detail) {
                    $remain = $detail['alloc_qty'] - (int)$this->packDtlModel->sumPieceQty($detail['lot'],
                            $detail['item_id'], $detail['odr_id']);
                    if ($remain < 0) {
                        $remain = 0;
                    }

                    if ($detail['alloc_qty'] <= 0) {
                        continue;
                    }
                    $items[] = [
                        'itm_id'        => $detail['item_id'],
                        'sku'           => $detail['sku'],
                        'color'         => $detail['color'],
                        'size'          => $detail['size'],
                        'lot'           => $detail['lot'],
                        'alloc_qty'     => $detail['alloc_qty'],//$allocQTY,
                        'remain_qty'    => $remain,
                        'ship_track_id' => $detail['ship_track_id'],
                        'odr_dtl_id'    => $detail['odr_dtl_id']
                    ];
                    if (empty($remain)) {
                        $skuTotal++;
                    }
                }
            }
            $orderHdr->sku_ttl_com = $skuTotal;

            $orderHdr->items = $items;

            $packHdr = $orderHdr->packHdr->toArray();

            $orderHdr->cartons = array_map(function ($value) {
                return [
                    'pack_hdr_id'   => $value['pack_hdr_id'],
                    'odr_hdr_id'    => $value['odr_hdr_id'],
                    'pack_hdr_num'  => $value['pack_hdr_num'],
                    'sku_ttl'       => $value['sku_ttl'],
                    'piece_ttl'     => $value['piece_ttl'],
                    'pack_type'     => array_get($value, 'pack_ref.pack_type', null),
                    'seq'           => $value['seq'],
                    'carrier_name'  => $value['carrier_name'],
                    'pack_sts'      => $value['pack_sts'],
                    'pack_sts_name' => Status::getByKey("Pack-Status", $value['pack_sts']),
                    'ship_to_name'  => $value['ship_to_name'],
                    'length'        => array_get($value, 'pack_ref.length', null),
                    'width'         => array_get($value, 'pack_ref.width', null),
                    'height'        => array_get($value, 'pack_ref.height', null),
                    'dimension'     => array_get($value, 'pack_ref.dimension', null),
                    'is_print'      => (int)$value['is_print'],
                    'sku'           => $value['sku'],
                    'has_rfid'      => array_get($value, 'carton.rfid', null) ? true : false,
                ];
            }, $packHdr);

            return $this->response->item($orderHdr, $packDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param PackHdrTransformer $packHdrTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request, PackHdrTransformer $packHdrTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $packHdr = $this->packHdrModel->search($input, [

            ], array_get($input, 'limit'));

            return $this->response->paginator($packHdr, $packHdrTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderHdrId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function printCartonLabel($orderHdrId, Request $request)
    {
        $input = $request->getQueryParams();

        $userInfo = new \Wms2\UserInfo\Data();
        $userInfo = $userInfo->getUserInfo();
        $firstName = $userInfo['first_name'];
        $lastName = $userInfo['last_name'];

        if (empty($input['pack_hdr_id'])) {
            throw new \Exception(Message::get("VR029", "Pack Header Ids"));
        }

        $packHdrIds = explode(",", $input['pack_hdr_id']);
        if (!is_numeric($packHdrIds[0])) {
            throw new \Exception(Message::get("VR029", "Pack Header Ids"));
        }

        try {
            $packHdr = $this->packHdrModel->getPackById($packHdrIds, $orderHdrId,
                ['customer', 'details', 'orderHdr'])->toArray();

            if (empty($packHdr)) {
                return $this->response->noContent();
            }

            $odrInfo = $this->orderHdrModel->getFirstWhere(
                ['odr_id' => $orderHdrId],
                ['customer']
            );
            $odrNum = array_get($odrInfo, 'odr_num', '');
            $createdAt = array_get($odrInfo, 'created_at', '');
            $cusName = array_get($odrInfo, 'customer.cus_name', '');

            //change is_print to 1
            foreach ($packHdrIds as $packHdrId) {
                $park_hdr_params = [
                    'pack_hdr_id' => (int)$packHdrId,
                    'is_print'    => 1,

                ];
                DB::table('pack_hdr')->where('pack_hdr_id',
                    $park_hdr_params['pack_hdr_id'])->update($park_hdr_params);

            }

            $this->createPdfFile($packHdr, $odrNum, $firstName, $lastName, $createdAt, $cusName);

            return $this->response->noContent()->setContent(['status' => 'OK'])->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $packHdr
     * @param $odrNum
     * @param $firstName
     * @param $lastName
     *
     * @throws \MpdfException
     */
    private function createPdfFile($packHdr, $odrNum, $firstName, $lastName, $createdAt, $cusName)
    {
        $pdf = new \Mpdf\Mpdf();
        $html = (string)view('PackedCartonListPrintoutTemplate', [
            'packHdr'   => $packHdr,
            'odrNum'    => $odrNum,
            'firstName' => $firstName,
            'lastName'  => $lastName,
            'createdAt' => $createdAt,
            'cusName'   => $cusName,
        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        $pdf->Output("$odrNum.pdf", "D");
    }


    /**
     * @param $packHdr
     *
     * @deprecated not use this function
     */
    private function createPdfFileBK($packHdr)
    {
        $pdf = new TCPDF("P", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set font
        $pdf->SetFont('helvetica', '', 20);


        foreach ($packHdr as $header) {
            // add a page
            $pdf->AddPage();
            $createdAt = date("m/d/Y", $header['created_at']);
            $packHdrNum = $header['pack_hdr_num'];
            $customer = array_get($header, 'customer.cus_name', null);
            $odrHdrId = array_get($header, 'order_hdr.odr_num', null);

            $items = "$createdAt\n$packHdrNum\n";
            $pdf->Write(0, $items, '', 0, 'C', true, 0, false, false, 0);
            $pdf->write1DBarcode($packHdrNum, 'C128', 10, 30, '', 50, 4);

            $items = "\n\n\n\n\n\n$customer\n$odrHdrId\n";
            $pdf->Write(0, $items, '', 0, 'C', true, 0, false, false, 0);
            $pdf->write1DBarcode($odrHdrId, 'C128', 10, 100, '', 50, 4);
            $items = "\n\n\n\n\n\n";
            foreach ($header['details'] as $detail) {
                $items .= $detail['cus_upc'] . "/" . $detail['sku'] . "(" . $detail['piece_qty'] . ")\n";
            }

            $items .= "\n_________________________________________\n\n\n";

            $pdf->Write(0, $items, '', 0, 'C', true, 0, false, false, 0);
            $this->packHdrModel->updateWhere(['is_print' => 1], ['pack_hdr_id' => $header['pack_hdr_id']]);
        }
        //Close and output PDF document
        $pdf->Output("Carton_Label.pdf", 'D');
    }

    /**
     * @param Request $request
     *
     * @return Response|mixed|void
     */
    public function assignPallet(
        $print,
        Request $request
    ) {
        try {
            // get data from HTTP
            $input = $request->getParsedBody();

            \DB::beginTransaction();

            $orderIds = [];
            $whsId = 0;
            foreach ($input['data'] as $data) {
                //Check order have not been packed yet...
                $odr_hdr_id = array_get($data, 'odr_id', 0);
                $orderIds[] = $odr_hdr_id;
                $packInfo = $this->packHdrModel->checkWhere(
                    [
                        'odr_hdr_id' => $odr_hdr_id
                    ]);
                if (empty($packInfo)) {
                    throw new \Exception(Message::get('BM130', 'This order', 'packed'));
                }

                //Create Out_Pallet

                $orderInfo = $this->orderHdrModel->getOrderById($odr_hdr_id)->first()->toArray();
                $whsId = $orderInfo['whs_id'];
                foreach ($data['cartons'] as $cartons) {
                    $check_sum = array_get($cartons, 'pack_dt_checksum', "checksum");

                    foreach ($cartons['pallet_carton'] as $palletCarton) {
                        $pallet_num = array_get($palletCarton, 'pallet_num', 0);

                        for ($i = 0; $i < $pallet_num; $i++) {
                            $paramOutPallet = [
                                // 'plt_num' => 'LPN' . str_replace_first('ORD', '', $orderInfo['odr_num']) . "-" .
                                //     str_pad($i + 1, 4, '0', STR_PAD_LEFT),
                                'plt_num' => $this->outPalletModel->generateLPNNumByOdrNum($orderInfo['odr_num']),
                                'cus_id'  => $orderInfo['cus_id'],
                                'whs_id'  => $orderInfo['whs_id'],
                                'ctn_ttl' => array_get($palletCarton, 'carton_num', 0),
                                'volume'  => 0,
                                'item_id' => array_get($cartons, 'items.0.item_id', ''),
                                'sku'     => array_get($cartons, 'items.0.sku', ''),
                                'size'    => array_get($cartons, 'items.0.size', ''),
                                'color'   => array_get($cartons, 'items.0.color', ''),
                                'cus_upc' => array_get($cartons, 'items.0.cus_upc', ''),
                            ];

                            $this->outPalletModel->refreshModel();

                            //Create Out_Pallet
                            $outPallet = $this->outPalletModel->create($paramOutPallet);

                            //Update outPalletID to PackHdr
                            $ctn_ttl = $paramOutPallet['ctn_ttl'];
                            $plt_id = object_get($outPallet, 'plt_id', 0);
                            $volume = 0;
                            for ($j = 0; $j < $ctn_ttl; $j++) {
                                $pack = $this->packHdrModel->getFistPackNotAssignByCheckSum($check_sum, $odr_hdr_id);
                                if ($pack) {
                                    // $volume += ($pack['width'] * $pack['height'] * $pack['length']) / 1728;
                                    $volume += ($pack['width'] * $pack['height'] * $pack['length']);
                                    $this->packHdrModel->updateWhere(
                                        ['out_plt_id' => $plt_id],
                                        ['pack_hdr_id' => object_get($pack, 'pack_hdr_id')]
                                    );
                                }

                            }
                            //update out_pallet volume
                            $outPallet->volume = round($volume, 2);
                            $outPallet->save();
                        }
                    }
                }

                //Update status for orders
                $paramOrderUpdate = [
                    'odr_id'  => $orderInfo['odr_id'],
                    'odr_sts' => Status::getByValue("Palletized", "ORDER-STATUS")
                ];
                $this->orderHdrModel->update($paramOrderUpdate);

                // Create Event Tracking
                $evtCode = 'ORDER-PALLETIZED';
                $info = 'ORDER-PALLETIZED';
                $this->eventTrackingModel->refreshModel();

                $this->eventTrackingModel->create([
                    'whs_id'    => $orderInfo['whs_id'],
                    'cus_id'    => $orderInfo['cus_id'],
                    'owner'     => $orderInfo['odr_num'],
                    'evt_code'  => Status::getByKey("Event", $evtCode),
                    'trans_num' => $orderInfo['odr_num'],
                    'info'      => sprintf(Status::getByKey("Event-Info", $info), $orderInfo['odr_num']),
                ]);
            }

            \DB::commit();

            if ($print == 1) {
                $this->printPalletLabels($orderIds);
            }


            if ($outPallet) {

                foreach ($orderIds as $odrId) {
                    dispatch(new BOLJob($odrId, $whsId, $request));
                }

                return $this->response->noContent()
                    ->setContent(['data' => ['message' => Message::get('BM129', 'Pallets assigned')]])
                    ->setStatusCode(Response::HTTP_CREATED);
            }


        } catch (\PDOException $e) {
            \DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            \DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param ViewAssignPalletTransformer $viewAssignPalletTransformer
     *
     * @return Response
     * @throws \Exception
     */
    public function showAssignPallet(
        Request $request,
        ViewAssignPalletTransformer $viewAssignPalletTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        if (empty($input['odr_hdr_ids'])) {
            throw new \Exception(Message::get("BM017", "Order Header"));
        }

        $odrHdrIds = explode(",", $input['odr_hdr_ids']);
        $orderHdrs = $this->orderHdrModel->getOrderById($odrHdrIds);

        return $this->response->collection($orderHdrs, $viewAssignPalletTransformer);
    }

    /**
     * @param Request $request
     * @param ViewAssignPalletTransformer $viewAssignPalletTransformer
     *
     * @throws \Exception
     */
    public function printLpns(
        Request $request,
        ViewAssignPalletTransformer $viewAssignPalletTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        if (empty($input['odr_hdr_ids'])) {
            throw new \Exception(Message::get("BM017", "Order Header"));
        }
        $this->printPalletLabels(array_filter(explode(',', $input['odr_hdr_ids'])));

    }

    /**
     * @param array $orderIds
     *
     * @throws \Exception
     */
    private function printPalletLabels($orderIds = [])
    {
        if (empty($orderIds)) {
            return false;
        }
        $packInfo = $this->packDtlModel->checkWhere(
            [
                'odr_hdr_id' => implode(',', $orderIds)
            ]);
        if (empty($packInfo)) {
            throw new \Exception(Message::get('BM131', 'This order', 'pack'));
        }

        $orderHdrs = $this->orderHdrModel->findWhereIn(
            $orderIds,
            ['packHdr', 'packDtl.packHdr']
        );

        if (empty($orderHdrs)) {
            throw new \Exception(Message::get("BM017", "Orders"));
        }

        $packHdr = [];
        foreach ($orderHdrs->toArray() as $odrHdr) {
            if ($odrHdr['odr_sts'] != 'PTD') {
                throw new \Exception('Only palletized orders can be print LPN!');
            }
            foreach ($odrHdr['pack_dtl'] as $detail) {
                $out_plt_id = array_get($detail, 'pack_hdr.out_plt_id', null);

                $packHdr[$out_plt_id] = [
                    'odr_num'      => array_get($odrHdr, 'odr_num', null),
                    'ship_to_name' => array_get($odrHdr, 'ship_to_name', null),
                    'cus_po'       => array_get($odrHdr, 'cus_po', null),
                    'ship_by_dt'   => array_get($odrHdr, 'ship_by_dt', null),
                    'cus_id'       => array_get($detail, 'cus_id', null),
                    'created_by'   => array_get($detail, 'created_by', null),
                    'plt_id'       => $out_plt_id
                ];

            }
        }

        if (empty($packHdr)) {
            throw new \Exception(Message::get('BM131', 'This order', 'out pallet'));
        }

        $admins = array_pluck(
            User::select(DB::raw('CONCAT(first_name, " ", last_name) AS user_name'), 'user_id')
                ->whereIn('user_id', array_pluck($packHdr, 'created_by'))
                ->get(), 'user_name', 'user_id');

        $clientNames = array_pluck(
            Customer::select('cus_name', 'cus_id')
                ->whereIn('cus_id', array_pluck($packHdr, 'cus_id'))
                ->get(), 'cus_name', 'cus_id');

        $licensePlates = array_pluck(OutPallet::select('plt_num', 'plt_id')
            ->whereIn('plt_id', array_pluck($packHdr, 'plt_id'))
            ->get(), 'plt_num', 'plt_id');

        // Sort by key ASC
        ksort($packHdr);

        $this->createLPNPdfFile($packHdr, $admins, $clientNames, $licensePlates);
    }

    /**
     * @param $packHdr
     * @param $admins
     * @param $clientNames
     * @param $licensePlates
     *
     * @throws \MpdfException
     */
    private function createLPNPdfFile($packHdr, $admins, $clientNames, $licensePlates)
    {
        $pdf = new \Mpdf\Mpdf();
        $html = (string)view('LPNListPrintoutTemplate', [
            'packHdr'       => $packHdr,
            'admins'        => $admins,
            'clientNames'   => $clientNames,
            'licensePlates' => $licensePlates
        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        //$pdf->Output("$odrNum.pdf", "D");
        $pdf->Output();
    }

    public function delete($packId)
    {
        $packSts = ['PN', 'PPA'];
        $packObj = $this->packHdrModel->byId($packId);
        $orders = $packObj->orderHdr()->get();

        foreach ($orders as $order) {
            if (!in_array($order->odr_sts, $packSts)) {
                return $this->response->errorBadRequest('This pack has pallet assignment');
            }
        }
        $packObj->delete();

        return ['data' => "Packed " . $packObj->pack_hdr_num . " is deleted"];
    }

    /**
     * @param $odr_hdr_id
     * @return mixed
     * @throws \Exception
     */
    public function undoPackingPackedOrder($odr_hdr_id)
    {
        $whs_id = Data::getCurrentWhsId();

        $odrObj = $this->orderHdrModel->getFirstWhere([
            'odr_id' => $odr_hdr_id,
            'whs_id' => $whs_id
        ]);
        $odrSts = object_get($odrObj, 'odr_sts');

        if($odrSts != 'PN' && $odrSts != 'PA'){
            throw new \Exception("The order not Paking/Packed.");
        }

        try {
            DB::beginTransaction();

            // Check Status Packing and Packed
            if($odrSts == 'PN' || $odrSts == 'PA'){
                // Delete PackDtl
                $this->packDtlModel->forceDeletePackDtl($odr_hdr_id, $whs_id);

                // Delete PackHdr
                $this->packHdrModel->forceDeletePackHdr($odr_hdr_id, $whs_id);

                // Update OdrHdr
                $this->orderHdrModel->undoOdrHdrStatusToPicked($odr_hdr_id);

                // Update OdrDtl
                $this->orderDtlModel->undoOdrDtlStatusToPicked($odr_hdr_id);
            }

            // Insert Event Tracking
            $eventTrackingInfo = [];
            if (!empty($eventTrackingInfo)) {
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $whs_id,
                    'cus_id'    => $odrObj->cus_id,
                    'owner'     => $odrObj->odr_num,
                    'evt_code'  => Status::getByKey("event", "ORDER-UNDO-PACK"),
                    'trans_num' => $odrObj->odr_num,
                    'info'      => 'Order ' . $odrObj->odr_num . ' is undo Packing/Packed.'
                ]);
            }

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data' => "Order ' . $odrObj->odr_num . ' is undo Packing/Packed."
            ])->setStatusCode
            (Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            SystemBug::writeSysBugs($e->getMessage(), SysBug::API_ORDER, __FUNCTION__);
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function autoStore(
        $orderHdrId,
        Request $request,
        PackHdrModel $packHdrModel,
        PackHdrAutoValidator $packHdrAutoValidator
    ) {
        $input = $request->getParsedBody();
        $packHdrAutoValidator->checkOdrHdrExit($orderHdrId);
        $odrDtlIds = array_pluck($input, 'odr_dtl_id');

        $packHdrAutoValidator->validate($input);
        //$packHdrAutoValidator->checkCanAutoPack($odrDtlIds);
        try {
            DB::beginTransaction();

            $packHdrModel->autoPack($odrDtlIds, $orderHdrId);

            $packHdrModel->updatePackedStatus($orderHdrId);

            $order = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderHdrId]);

            $this->_updateOrderFlow($orderHdrId, object_get($order, 'whs_id', 0));

            DB::commit();

            dispatch(new BOLJob($orderHdrId, object_get($order, 'whs_id', 0), $request));

            return $this->response->noContent()->setContent([
                'status' => 'OK'
            ])->setStatusCode
            (Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            SystemBug::writeSysBugs($e->getMessage(), SysBug::API_ORDER, __FUNCTION__);
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _updateOrderFlow($odrId, $whsId)
    {
        $odrObj = $this->orderHdrModel->getFirstWhere(['odr_id' => $odrId]);
        $odrSts = object_get($odrObj, 'odr_sts');

        if (in_array($odrSts, ['PA', 'PTD'])) {
            $orderFlow = $this->orderHdrMetaModel->getOrderFlow($odrId);
            $isAutoPAM = $this->orderHdrMetaModel->getFlow($orderFlow, 'PAM');
            // $isAutoBOL = $this->orderHdrMetaModel->getFlow($orderFlow, 'BOL');
            $isAutoARS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ARS');
            // $isAutoASS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ASS');

            $flagStatus = $odrObj->odr_sts;
            if (array_get($isAutoPAM, 'usage', -1) == 1) {
                $flagStatus = 'PTD';
            }

            if ($flagStatus == 'PTD' &&
                array_get($isAutoARS, 'usage', -1) == 1) {
                $flagStatus = 'RS';
            }

            if ($odrObj->odr_sts != $flagStatus) {
                $odrObj->odr_sts = $flagStatus;
                $odrObj->save();
            }
        }

    }

}
