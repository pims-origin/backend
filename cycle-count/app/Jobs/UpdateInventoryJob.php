<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SystemBug;

class UpdateInventoryJob extends Job
{
    protected $whsId;

    public function __construct($data)
    {
        $this->whsId = array_get($data, 'whs_id');
    }

    public function handle()
    {
        $this->correctInventory();
    }

    public function correctInventory()
    {
        try {
            DB::statement('UPDATE inventory i
                LEFT JOIN (
                    SELECT
                        t.whs_id,
                        t.item_id,
                        SUM(t.total_qty) AS total_qty,
                        SUM(t.in_pick_qty) AS in_pick_qty
                    FROM
                        (
                            (
                                SELECT
                                    d.whs_id,
                                    d.item_id,
                                    0 AS total_qty,
                                    SUM(
                                        IF (
                                            d.alloc_qty < d.picked_qty,
                                            0,
                                            d.alloc_qty - d.picked_qty
                                        )
                                    ) AS in_pick_qty
                                FROM
                                    odr_dtl d
                                JOIN odr_hdr h ON h.odr_id = d.odr_id
                                WHERE
                                    h.odr_sts IN ("AL", "PK") AND d.whs_id = ' . $this->whsId . '
                                    AND d.alloc_qty > 0
                                    AND d.deleted = 0
                                GROUP BY
                                    d.whs_id,
                                    d.item_id
                            )
                            UNION
                            (
                                SELECT
                                    c.whs_id,
                                    c.item_id,
                                    SUM(c.piece_remain) AS total_qty,
                                    SUM(
                                        IF (
                                            c.ctn_sts = "PD",
                                            c.piece_remain,
                                            0
                                        )
                                    ) AS in_pick_qty
                                FROM
                                    cartons c
                                WHERE
                                    c.ctn_sts IN ("AC", "LK", "PD", "TF") and c.whs_id = ' . $this->whsId . '
                                    AND c.deleted = 0
                                GROUP BY
                                    c.whs_id,
                                    c.item_id
                            )
                        ) AS t
                    GROUP BY
                        t.whs_id,
                        t.item_id
                ) AS d ON d.whs_id = i.whs_id
                AND d.item_id = i.item_id
            SET i.in_hand_qty = GREATEST(
                IFNULL(
                    d.total_qty - d.in_pick_qty,
                    0
                ),
                0
            ),
            i.in_pick_qty = IFNULL(d.in_pick_qty, 0) -
            IF (
                d.total_qty - d.in_pick_qty < 0,
                ABS(d.total_qty - d.in_pick_qty),
                0
            )
            WHERE
                i.type = "I" 
                AND i.whs_id = ' . $this->whsId . '
                AND (
                    (
                        d.total_qty IS NULL
                        AND i.total_qty > 0
                    )
                OR i.total_qty != d.total_qty
                OR i.in_pick_qty != IFNULL(d.in_pick_qty, 0) -
                IF (
                    d.total_qty - d.in_pick_qty < 0,
                    ABS(d.total_qty - d.in_pick_qty),
                    0
                )
            );');
        } catch (\PDOException $e) {
            SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }
}