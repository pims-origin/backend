<?php

namespace App\Api\V1\Models;


use App\KittingPutaway;
use Seldat\Wms2\Utils\JWTUtil;

class KittingPutawayModel extends AbstractModel
{
    protected $model;

    /**
     */
    public function __construct()
    {
        $this->model = new KittingPutaway();
    }

    public function createKittingPutaway($kittingItems, $orderHdrId) {
        $arrData = [];

        foreach ($kittingItems as $kittingItem) {
            $arrData[] = $this->addTime([
                'kitting_id'        => $orderHdrId,
                'kitting_dtl_id'    => $kittingItem['odr_dtl_id'],
                'plt_id'            => $kittingItem['plt_id'],
                'loc_id'            => $kittingItem['loc_id'],
                'loc_code'          => $kittingItem['loc_code'],
                'loc_name'          => $kittingItem['loc_name'],
                'item_id'           => $kittingItem['item_id'],
                'sku'               => $kittingItem['sku'],
                'size'              => $kittingItem['size'],
                'color'             => $kittingItem['color'],
                'ctn_qty'           => $kittingItem['picked_ctns'],
            ]);
        }
        $this->model->insert($arrData);
    }

    /**
     * @param array $cartonParams
     *
     * @return array
     */
    private function addTime(array $cartonParams)
    {
        $userId = JWTUtil::getPayloadValue('jti') ?: 0;

        $deleted_at = getDefaultDatetimeDeletedAt();
        $created_at = time();
        $time = [
            'created_at' => $created_at,
            'updated_at' => $created_at,
            'deleted_at' => $deleted_at,
            'created_by' => $userId,
            'updated_by' => $userId,
            'deleted'    => 0
        ];

        return array_merge($cartonParams, $time);
    }

    public function getByKittingId($id) {
        return $this->model
            ->select([
                'kitting_putaway.*',
                'pallet.plt_num'
            ])
            ->join('pallet', 'kitting_putaway.plt_id', '=', 'pallet.plt_id')
            ->where('kitting_putaway.kitting_id', $id)
            ->groupBy('kitting_putaway.sku', 'kitting_putaway.plt_id')
            ->get();
    }
}
