<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCycleCountEvtCodeToEvtLookup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('evt_lookup')) {
            DB::table('evt_lookup')->insert([
                ['evt_code'=>'CRA', 'trans'=>'CYC', 'des' => 'Cycle Count Created & Assigned'],
                ['evt_code'=>'CYD', 'trans'=>'CYC', 'des' => 'Cycle Count Cycled'],
                ['evt_code'=>'CYR', 'trans'=>'CYC', 'des' => 'Cycle Count Recount'],
                ['evt_code'=>'CYS', 'trans'=>'CYC', 'des' => 'Cycle Count Add SKU'],
                ['evt_code'=>'CYC', 'trans'=>'CYC', 'des' => 'Cycle Count Completed'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // to do
    }
}
