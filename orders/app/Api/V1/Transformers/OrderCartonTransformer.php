<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderHdr;
use League\Fractal\TransformerAbstract;
use App\Api\V1\Models\OrderHdrModel;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderCartonTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        return [
            'odr_id'      => object_get($orderHdr, 'odr_id', ''),
            'odr_num'     => object_get($orderHdr, 'odr_num', ''),
            'cus_id'      => object_get($orderHdr, 'customer.cus_id', ''),
            'cus_name'    => object_get($orderHdr, 'customer.cus_name', ''),
            'ship_to'     => object_get($orderHdr, 'ship_to_name', ''),
            'num_sku'     => $orderHdr->sku_ttl,
            'return_qty'  => OrderCarton::where('piece_qty', '>', 0)
                ->where('odr_hdr_id', $orderHdr->odr_id)->sum('piece_qty'),
            'cancel_date' => object_get($orderHdr, 'updated_at', '')->format('Y-m-d'),
            'cancel_by'   => trim(object_get($orderHdr, "updatedBy.first_name", null) . " " .
                object_get($orderHdr, "updatedBy.last_name", null)),
            'odr_type'     => object_get($orderHdr, 'odr_type', ''),
        ];
    }
}
