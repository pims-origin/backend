<?php

namespace App\Api\V4\Outbound\OutPallet\AssignCartons\Controllers;

use App\Api\V4\Outbound\OutPallet\AssignCartons\Models\OrderHdrModel;
use App\Api\V4\Outbound\OutPallet\AssignCartons\Models\EventTrackingModel;
use App\Api\V4\Outbound\OutPallet\AssignCartons\Models\OutPalletModel;
use App\Api\V4\Outbound\OutPallet\AssignCartons\Models\PackHdrModel;
use App\Api\V4\Outbound\OutPallet\AssignCartons\Models\CartonModel;
use App\Api\V4\Outbound\OutPallet\AssignCartons\Models\OrderCartonModel;
use App\Api\V4\Outbound\OutPallet\AssignCartons\Validators\OutPalletValidator;
use App\libraries\RFIDValidate;
use App\Api\V4\Outbound\OutPallet\AssignCartons\Models\OrderHdrMetaModel;

use App\Api\V1\Models\Log;

use App\Jobs\BOLJob;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OutPalletController extends AbstractController
{

    const PREFIX_CARTON_RFID_OVERPICK = 'CCTCO';

    protected $orderHdrModel;
    protected $eventTrackingModel;
    protected $outPalletModel;
    protected $packHdrModel;
    protected $cartonModel;
    protected $orderCartonModel;
    protected $orderHdrMetaModel;

    public function __construct()
    {
        $this->orderHdrModel      = new OrderHdrModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->outPalletModel     = new OutPalletModel();
        $this->packHdrModel       = new PackHdrModel();
        $this->cartonModel        = new CartonModel();
        $this->orderCartonModel   = new OrderCartonModel();
        $this->orderHdrMetaModel  = new OrderHdrMetaModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function assignCartonToPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $pltNum = array_get($input, 'pallet', null);
        $skus   = array_get($input, 'skus', null);

        /*
         * start logs
         */

        $url = "v4/{$whsId}/outpallet/assign-cartons";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'ACP',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Assign Packed Cartons to Out pallet'
        ]);

        /*
         * end logs
         */

        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        // Check exists Order hdl num
        $pltNumArr = explode('-', $pltNum);
        if (count($pltNumArr) < 4) {
            $msg = sprintf("The pallet num %s is invalid", $pltNum);
            return $this->_responseErrorMessage($msg);
        }

        $odrNum = "ORD-" . $pltNumArr[1] . "-" . $pltNumArr[2];
        if (count($pltNumArr) == 5) {
            $odrNum = "ORD-" . $pltNumArr[1] . "-" . $pltNumArr[2] . "-" . $pltNumArr[3];
        }
        $order = $this->orderHdrModel->getFirstWhere(['odr_num' => $odrNum]);
        $odrId = object_get($order, 'odr_id');
        if (!$order) {
            $msg = sprintf("No order has been found for the pallet number %s", $pltNum);
            return $this->_responseErrorMessage($msg);
        } else {
            $odrSts = object_get($order, 'odr_sts', '');
            if (!in_array($odrSts, ['PA', 'PTG'])) {
                $msg = sprintf('Unable to assign cartons to out pallet for Order is %s!', Status::getByKey('ORDER-STATUS', $odrSts));
                return $this->_responseErrorMessage($msg);
            }
        }

        // check pallet num
        $pallet = $this->outPalletModel->getFirstWhere(['plt_num' => $pltNum]);

        if ($pallet && $pallet->whs_id != $whsId) {
            $msg = sprintf("The pallet num %s does not belong to current warehouse", $pltNum, $pallet->out_plt_sts);
            return $this->_responseErrorMessage($msg);
        }

        if ($pallet && $pallet->out_plt_sts != 'AC') {
            $msg = sprintf("The pallet num %s is %s and not active", $pltNum, $pallet->out_plt_sts);
            return $this->_responseErrorMessage($msg);
        }

        try {
            // start transaction
            DB::beginTransaction();
            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $numAssigned = 0;
            foreach ($skus as $sku) {
                $packs  = array_get($sku, 'packs');
                $itemId = array_get($sku, 'item_id');
                $itemArr = DB::table('item')
                    ->where('item_id', $itemId)
                    ->where('deleted', 0)
                    ->first();
                if (!count($itemArr)) {
                    $msg = sprintf("Item Id %s does not exist.", $itemId);
                    return $this->_responseErrorMessage($msg);
                }
                // WAP-772 - [Outbound - Assign Ctn to Pallet] Create API submit Assign Carton to Pallet with both option (NO/YES)
                // Option tag is YES
                if (count($packs)) {
                    // Check duplicate Pack hdl num
                    $uniquePacks = array_unique($packs);
                    if (count($uniquePacks) != count($packs)) {
                        $duplicatePack = array_unique(array_diff_assoc($packs, $uniquePacks));
                        $msg = 'Duplicate Pack: ' . implode(', ', $duplicatePack);
                        return $this->_responseErrorMessage($msg);
                    }

                    $ctnRfidInValid = [];
                    $errorMsg = '';
                    $isAssignRfid = 0; //0: is assigned with barcode, 1: is assigned with rfid
                    foreach ($packs as $key => $pack) {
                        //check invalid carton rfid when auto packs
                        $checkRfid = substr($pack, 0, 3);
                        if (!in_array($checkRfid, ["CTN", "CYC"])) {
                            $isAssignRfid = 1;
                            $ctnRFIDValid = new RFIDValidate($pack, RFIDValidate::TYPE_CARTON, $whsId);
                            if (!$ctnRFIDValid->validate()) {
                                $ctnRfidInValid[] = $pack;
                                if ('' == $errorMsg) {
                                    $errorMsg = $ctnRFIDValid->error;
                                }
                            }

                            $ctnObj = $this->cartonModel->getFirstWhere(['rfid' => $pack]);
                            if (!$ctnObj) {
                                $msg = sprintf("The Carton Rfid %s doesn't exist.", $pack);
                                return $this->_responseErrorMessage($msg);
                            }
                            if ($ctnObj && $ctnObj->ctn_sts == 'AJ') {
                                $this->_processCartonsCCTC($ctnObj, $odrId);
                            }
                            $ctnId = object_get($ctnObj, 'ctn_id', null);
                            $packHdr = $this->packHdrModel->getFirstWhere([
                                'cnt_id'     => $ctnId,
                                'odr_hdr_id' => $odrId,
                            ]);

                            $packs[$key] = object_get($packHdr, 'pack_hdr_num', null);
                        }
                    }

                    //if has errors, return all error data and message
                    if ($ctnRfidInValid) {
                        return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
                    }

                    foreach ((array)$packs as $packCode) {
                        $packHdr = $this->packHdrModel->getFirstWhere([
                            'pack_hdr_num' => $packCode,
                            'whs_id'       => $whsId,
                        ]);

                        if (!$packHdr) {
                            $msg = sprintf("Packed Carton %s is not found!", $packCode);
                            return $this->_responseErrorMessage($msg);
                        }

                        if ($packHdr->out_plt_id) {
                            $pltId = object_get($pallet, 'plt_id', -1);
                            if ($packHdr->out_plt_id != $pltId) {
                                $msg = sprintf("The carton %s has been assigned to other pallet already!", $packCode);
                                return $this->_responseErrorMessage($msg);
                            } else {
                                $msg = sprintf("The carton %s has been assigned to this pallet already!", $packCode);
                                return $this->_responseErrorMessage($msg);
                            }
                        }

                        if ($odrId != $packHdr->odr_hdr_id) {
                            $msg = sprintf("Please scan cartons the same as order", $odrNum);
                            return $this->_responseErrorMessage($msg);
                        }
                        if (!$pallet) {
                            $pallet = $this->outPalletModel->createNewPallet($packHdr, $pltNum, 0);
                        }

                        //Update Pack Status after asigned Pallet
                        $packHdr->out_plt_id = $pallet->plt_id;
                        $packHdr->pack_sts = 'AS';
                        $packHdr->is_assigned_rfid = $isAssignRfid;
                        $packHdr->save();
                        $numAssigned ++;
                    }
                }

                // WAP-772 - [Outbound - Assign Ctn to Pallet] Create API submit Assign Carton to Pallet with both option (NO/YES)
                // Option tag is No
                $noTagQty = array_get($sku, 'no_tag');
                if ($noTagQty > 0) {
                    $packNotAssigneds = $this->_getPackNotAssigned($odrId, $itemId);
                    $countNotAssigned = count($packNotAssigneds);
                    if ($noTagQty > $countNotAssigned) {

                        $msg = sprintf("Assigned QTY (%s) must not greater than Pack QTY remaining (%s) of SKU-SIZE-COLOR-PACK %s-%s-%s-%s", $noTagQty, $countNotAssigned, $itemArr['sku'],$itemArr['size'],$itemArr['color'],$itemArr['pack']);
                        return $this->_responseErrorMessage($msg);
                    }
                    foreach ($packNotAssigneds as $packNotAssigned) {
                        if ($noTagQty <= 0) {
                            break;
                        }

                        if (!$pallet) {
                            $pallet = $this->outPalletModel->createNewPallet($packNotAssigned, $pltNum, 0);
                        }

                        $packHdrId = object_get($packNotAssigned, 'pack_hdr_id');
                        //Update Pack Status after asigned Pallet
                        $this->packHdrModel->getModel()
                            ->where('pack_hdr_id', $packHdrId)
                            ->update([
                                'out_plt_id' => $pallet->plt_id,
                                'pack_sts'   => 'AS',
                            ]);
                        $noTagQty --;
                        $numAssigned ++;
                    }
                }
            }

            // Update total pack carton
            $this->outPalletModel->updatePalletCtnTtl($pallet->plt_id);

            $this->orderHdrModel->updateOrderPalletizing($odrId);
            $this->orderHdrModel->updateOrderPalletized($odrId);

            if ($odrId) {
                $odrObj = $this->orderHdrModel->getFirstWhere(
                    [
                        'odr_id' => $odrId
                    ]
                );
                $this->eventTrackingModel->eventTracking([
                    'whs_id'    => $whsId,
                    'cus_id'    => object_get($odrObj, 'cus_id', null),
                    'owner'     => object_get($odrObj, 'odr_num', null),
                    'evt_code'  => 'WOP',
                    'trans_num' => object_get($odrObj, 'odr_num', null),
                    'info'      => "WAP - Assigning Carton(s) to Pallet"
                ]);
            }

            $packRemain = $this->packHdrModel->getModel()
                ->where('odr_hdr_id', $odrId)
                ->whereNull('out_plt_id')
                ->count();

            $this->_updateOrderFlow($odrId, $whsId);

            DB::commit();

            dispatch(new BOLJob($odrId, $whsId, $request));

            $msg = sprintf("%d carton(s) assigned to pallet %s successfully!", $numAssigned,
                $pltNum);

            return [
                "iat"     => time(),
                "status"  => true,
                "messages" => [[
                    "msg"         => $msg,
                    "status_code" => 1,
                ]],
                "data"    => [[
                    'pack_remain' => $packRemain,
                ]]
            ];

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }


    private function _validateParams($attributes, $whsId)
    {
        // $odrNum = array_get($attributes, 'odr_num');
        $pltNum = array_get($attributes, 'pallet');
        $skus   = array_get($attributes, 'skus');

        $names = [
            // 'odr_num' => 'Order number',
            'plt_num' => 'Pallet Code',
            'skus'    => 'Sku array',
            'item_id' => 'Item Id',
            'no_tag'  => 'No tag',
            'packs'   => 'Tags or Pack code',
        ];

        // Check Required
        $requireds = [
            // 'odr_num' => $odrNum,
            'plt_num' => $pltNum,
            'skus'    => $skus,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // skus is array
        if (!is_array($skus) || !count($skus)) {
            $msg = $names['skus'] . " must be array type and not be empty.";
            return $this->_responseErrorMessage($msg);
        }

        foreach ($skus as $sku) {
            // Check int type and greater than 0
            $itemId = (int)array_get($sku, 'item_id');
            $intGreaterThan0BySub = [
                'item_id'   => $itemId,
            ];
            foreach ($intGreaterThan0BySub as $key => $field) {
                if (!is_int($field) || $field < 1) {
                    $errorDetail = "{$names[$key]} must be integer type and greater than 0.";
                    return $this->_responseErrorMessage($errorDetail);
                }
            }

            $noTagQty = array_get($sku, 'no_tag');
            $packs = array_get($sku, 'packs');
            if ($noTagQty <= 0 && !count($packs)) {
                $errorDetail = "Wrong input tag qty.";
                return $this->_responseErrorMessage($errorDetail);
            }

        }

        /**
         * WMS2-3891: [WAP API - OUTBOUND] Able to assign packed cartons to invalid out-pallet
         */
        if (!OutPalletValidator::validate($pltNum)) {
            $msg = OutPalletValidator::ERROR_MSG;
            return $this->_responseErrorMessage($msg);
        }
    }

    private function _getPackNotAssigned($odrId, $itemId)
    {
        return $this->packHdrModel->getModel()
            ->where('odr_hdr_id', $odrId)
            ->where('item_id', $itemId)
            ->whereNull('out_plt_id')
            ->get();
    }

    private function _updateOrderFlow($odrId, $whsId)
    {
        $odrObj = $this->orderHdrModel->getFirstWhere(['odr_id' => $odrId]);
        $odrSts = object_get($odrObj, 'odr_sts');

        if ($odrSts == 'PTD') {
            $orderFlow = $this->orderHdrMetaModel->getOrderFlow($odrId);
            $isAutoARS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ARS');
            // $isAutoBOL = $this->orderHdrMetaModel->getFlow($orderFlow, 'BOL');
            // $isAutoASS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ASS');

            if (array_get($isAutoARS, 'usage', -1) == 1) {
                $odrObj->odr_sts = 'RS';
                $odrObj->save();
            }
        }
    }

    private function _processCartonsCCTC($ctnObj, $odrId)
    {

        $itemId = object_get($ctnObj, 'item_id');
        $lot = object_get($ctnObj, 'lot');
        $rfidReal = object_get($ctnObj, 'rfid');
        $odrCarton = $this->orderCartonModel->getModel()
            ->where('item_id', $itemId)
            ->where('lot', $lot)
            ->where('ctn_rfid', 'like', 'CCTC%')
            ->where('odr_hdr_id', $odrId)
            ->first();
        if ($odrCarton) {
            $rfidFake = object_get($odrCarton, 'ctn_rfid');
            $ctnIdFake = object_get($odrCarton, 'ctn_id');
            $cartonFake = $this->cartonModel->getFirstWhere(['rfid' => $rfidFake]);
            $cartonFake->rfid = null;
            $cartonFake->save();
            $ctnObj->rfid = $rfidFake;
            $ctnObj->deleted_at = time();
            $ctnObj->deleted = 1;
            $ctnObj->save();
            $cartonFake->rfid = $rfidReal;
            $cartonFake->save();
            $odrCarton->ctn_id = $cartonFake->ctn_id;
            $odrCarton->ctn_rfid = $rfidReal;
            $odrCarton->save();

            $packHdr = $this->packHdrModel->updateWhere(
                    ['cnt_id'     => $ctnObj->ctn_id],
                    [
                                'cnt_id'     => $cartonFake->ctn_id,
                                'odr_hdr_id' => $odrId,
                            ]
                        );

        }
    }
}