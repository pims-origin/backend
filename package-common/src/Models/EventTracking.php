<?php

namespace Seldat\Wms2\Models;

use Seldat\Wms2\Utils\JWTUtil;
use Wms2\UserInfo\Data;

class EventTracking extends BaseModel
{
    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        $userId = JWTUtil::getPayloadValue('jti') ?? Data::getCurrentUserId() ?? 1;

        // create a event to happen on saving
        static::saving(function ($table) use ($userId) {
            $table->created_by = $table->created_by ?: $userId;
        });

    }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evt_tracking';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'evt_code',
        'owner',
        'trans_num',
        'info',
        'whs_id',
        'cus_id'

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(__NAMESPACE__ . '\EventLookup', 'evt_code', 'evt_code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }


}
