<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Transformers\RFGunDetailTransformer;
use App\Api\V1\Transformers\RFGunTransformer;
use App\Api\V1\Validators\ConsolidationPltValidator;
use App\Api\V1\Validators\RFGunValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;


/**
 * Class PalletController
 *
 * @package App\Api\V1\Controllers
 */
class PalletController extends AbstractController
{

    protected $userId;
    /**
     * @var PalletSuggestLocationModel
     */
    protected $palletSuggestLocationModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    protected $cartonModel;

    protected $locationModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var GoodReceiptHdrModel
     */
    protected $goodReceiptHdrModel;

    /**
     * PalletController constructor.
     *
     * @param PalletModel $palletModel
     * @param PalletSuggestLocationModel $palletSuggestLocationModel
     */
    public function __construct(
        PalletModel $palletModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        LocationModel $locationModel,
        CartonModel $cartonModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->palletModel = $palletModel;
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->locationModel = $locationModel;
        $this->cartonModel = $cartonModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->goodReceiptHdrModel = new GoodsReceiptModel();

        $this->userId = JWTUtil::getPayloadValue('jti') ? JWTUtil::getPayloadValue('jti') : 0;
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param RFGunTransformer $rfGunTransformer
     * @param RFGunValidator $rfGunValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search($whsId, Request $request, RFGunTransformer $rfGunTransformer, RFGunValidator $rfGunValidator)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['putter'] = $this->userId;

        $rfGunValidator->validate($input);

        try {
            $allPallets = $this->palletSuggestLocationModel->show($input)->toArray();
            $all = [];
            if (!empty($allPallets)) {
                foreach ($allPallets as $allPallet) {

                    if (!empty($allPallet['actual'])) {
                        if (empty($all[$allPallet['gr_hdr_id']]['complete'])) {
                            $all[$allPallet['gr_hdr_id']]['complete'] = 0;
                        }
                        $all[$allPallet['gr_hdr_id']]['complete']++;
                    } else {
                        if (empty($all[$allPallet['gr_hdr_id']]['not-complete'])) {
                            $all[$allPallet['gr_hdr_id']]['not-complete'] = 0;
                        }
                        $all[$allPallet['gr_hdr_id']]['not-complete']++;
                    }
                }
            }

            $pallets = $this->palletSuggestLocationModel->search($input, ['pallet']);

            if (!empty($pallets)) {
                foreach ($pallets as $key => $pallet) {
                    $pallet->total = array_get($all, "{$pallet->gr_hdr_id}.complete", 0) +
                        array_get($all, "{$pallet->gr_hdr_id}.not-complete", 0);
                    $pallet->actual = array_get($all, "{$pallet->gr_hdr_id}.complete", 0);

                    $pallets[$key] = $pallet;
                }
            }

            return $this->response->collection($pallets, $rfGunTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show($whsId, $grHdrId, RFGunDetailTransformer $rfGunDetailTransformer)
    {
        try {
            $pallets = $this->palletSuggestLocationModel->show([
                'gr_hdr_id' => $grHdrId,
                'whs_id'    => $whsId,
                'putter'    => $this->userId
            ]);

            return $this->response->collection($pallets, $rfGunDetailTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function isBlockStackPallet($pid)
    {
        return preg_match('/^([A-Z]{3})-PL([F,B,G]{1})-([0-9]{6})$/', $pid);
    }

    public function updatePalletToLocation($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        $code = array_get($input, 'codes', null);

        if (empty($code)) {
            return $this->response->errorBadRequest("Please input codes values");
        }

        if (count($code) % 3 != 0) {
            return $this->response->errorBadRequest("Format codes input is incorrect.");
        }
        $arrSlip = array_chunk($code, 3);

        $arrUpdate = [];
        //validate data
        foreach ($arrSlip as $item) {
            $item = array_map(function ($loc) {
                return trim($loc);
            }, $item);
            if ($item[0] != $item[2]) {
                $msg = sprintf("Location start %s is not same with %s", $item[0], $item[2]);

                return $this->response->errorBadRequest($msg);
            }

            $locCode = $item[0];
            $pid = $item[1];


            //check rfid num
            $chk = $this->palletModel->checkPalletByPalletNum($item[1], $whsId);
            if ($chk == 0) {
                $msg = sprintf("Pallet %s is empty", $item[1]);

                return $this->response->errorBadRequest($msg);
            }

            $objPlt = $this->palletModel->getPalletByPalletNum($item[1], $whsId);

            if (is_null($objPlt)) {
                $msg = sprintf("%s is not existed.", $item[1]);

                return $this->response->errorBadRequest($msg);
            }

            $objLoc = null;
            list($whsCode, $pltType) = explode('-', $pid);
            $isFull = $objPlt['is_full'] && $objPlt['init_ctn_ttl'] == $objPlt['ctn_ttl'];
            if ($blockStack = $this->isBlockStackPallet($pid)) {
                $objLoc = $this->locationModel->getLocationByLocCode($isFull, $locCode, $whsId, $pltType, $blockStack);
            } else {
                $objLoc = $this->locationModel->getLocationByLocCode($isFull, $locCode, $whsId, $pltType);
            }

            if (is_null($objLoc)) {
                $msg = sprintf("%s is not existed.", $locCode);
                if ($blockStack) {
                    $msg = sprintf("%s child is not available.", $locCode);
                }

                return $this->response->errorBadRequest($msg);
            }

            if ($objLoc->loc_sts_code != 'AC') {
                $msg = sprintf("Location %s is not active.", $item[0]);

                return $this->response->errorBadRequest($msg);
            }

            if ($objLoc->rfid) {
                $msg = sprintf("The existed Pallet %s on Location code %s already.", $objLoc->rfid, $item[0]);

                return $this->response->errorBadRequest($msg);
            }


//            if ($objPlt->spc_hdl_code != $objLoc->spc_hdl_code) {
//                $msg = sprintf("%s isn't same temperature with %s", $item[1], $objLoc->loc_code);
//
//                return $this->response->errorBadRequest($msg);
//            }

            if ($objLoc->cus_id && $objPlt->cus_id != $objLoc->cus_id) {
                $msg = sprintf("%s is not belonged to this customer.", $item[0]);

                return $this->response->errorBadRequest($msg);
            }

            //push data to array Update
            $arrUpdate[] = [
                'plt_num'   => $objPlt->plt_num,
                'whs_id'    => $objPlt->whs_id,
                'cus_id'    => $objPlt->cus_id,
                'plt_id'    => $objPlt->plt_id,
                'loc_id'    => $objLoc->loc_id,
                'loc_code'  => $objLoc->loc_code,
                'loc_name'  => $objLoc->loc_alternative_name,
                'ctn_ttl'   => $objPlt->ctn_ttl,
                'item_id'   => $objPlt->item_id,
                'sku'       => $objPlt->sku,
                'size'      => $objPlt->size,
                'color'     => $objPlt->color,
                'lot'       => $objPlt->lot,
                'whs_id'    => Data::getCurrentWhsId(),
                'put_sts'   => 'NW',
                'gr_dtl_id' => $objPlt->gr_dtl_id
            ];
        }

        //update loc id to pallet
        try {
            // start transaction
            DB::beginTransaction();

            if (is_null($objLoc->cus_id)) {
                //Add to dynamic Zone
                if ($objLoc->loc_zone_id) {
                    $msg = sprintf("%s is assigned a Zone but not belonged to any customer.", $item[0]);

                    return $this->response->errorBadRequest($msg);
                }

                $zoneId = $this->locationModel->getDynZone($whsId, $objPlt->cus_id);
                if (!$zoneId) {
                    return $this->response->errorBadRequest("This customer don't support dynamic zone.");
                }

                $objLoc->loc_zone_id = $zoneId;
                $objLoc->save();
            }

            foreach ($arrUpdate as $item) {
                if (!empty($objPlt->loc_id)) {
                    //check and remove dynamic zone
                    DB::table('location')->join('zone', 'location.loc_zone_id', '=', 'zone.zone_id')
                        ->where([
                            'zone.dynamic'    => 1,
                            'location.loc_id' => $objPlt->loc_id
                        ])
                        ->update([
                            'location.loc_zone_id' => null,
                            'zone.zone_num_of_loc' => DB::raw('zone.zone_num_of_loc - 1')
                        ]);
                }

                //update location sts = RG
                $objLoc->loc_sts_code = 'RG';
                $objLoc->save();

                $this->palletModel->updateWhere(
                    [
                        'loc_id'   => $item['loc_id'],
                        'loc_code' => $item['loc_code'],
                        'loc_name' => $item['loc_name'],
//                        'rfid'     => null,
                        'plt_sts'  => 'RG',
                    ],
                    [
                        'plt_id' => $item['plt_id'],
                    ]);

                $this->cartonModel->updateWhere(
                    [
                        'loc_id'        => $item['loc_id'],
                        'loc_code'      => $item['loc_code'],
                        'loc_type_code' => 'RAC',
                        'loc_name'      => $item['loc_name'],
                    ],
                    [
                        'plt_id' => $item['plt_id'],
                    ]);

                $grHdr = $this->goodReceiptHdrModel->getFirstWhere(['gr_hdr_id' => $objPlt->gr_hdr_id]);
                if (!$grHdr) {
                    return $this->response->errorBadRequest('Pallet has not goods receipt');
                }

                //add pallet suggest location
                $this->palletSuggestLocationModel->upsert($item, $grHdr);

                $event = [
                    'whs_id'    => $item['whs_id'],
                    'cus_id'    => $item['cus_id'],
                    'owner'     => $grHdr->gr_hdr_num,
                    'evt_code'  => 'RFP',
                    'trans_num' => $item['plt_num'],
                    'info'      => sprintf("GUN - Update Pallet %s on location %s", $item['plt_num'],
                        $item['loc_code']),
                ];

                //  Evt tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create($event);
            }

            if ($grHdr->gr_sts == 'RE') {
                GoodsReceipt::where([
                    'gr_hdr_id' => $grHdr->gr_hdr_id
                ])
                    ->whereRaw("
                        (
                            SELECT COUNT(pal_sug_loc.act_loc_id) 
                            FROM pal_sug_loc 
                            JOIN gr_dtl ON pal_sug_loc.`gr_dtl_id` = gr_dtl.`gr_dtl_id`
                            WHERE pal_sug_loc.act_loc_id IS NOT NULL
                                AND pal_sug_loc.gr_hdr_id = gr_hdr.gr_hdr_id
                                AND gr_dtl.`gr_dtl_sts` = 'RG'
                        ) = 
                        (
                            SELECT SUM(gr_dtl.gr_dtl_plt_ttl) FROM gr_dtl
                            WHERE gr_dtl.gr_hdr_id = gr_hdr.gr_hdr_id
                            AND gr_dtl.`gr_dtl_sts` = 'RG'
                        )
                    ")
                    ->update(['gr_hdr.putaway' => 1]);
            } else {
                GoodsReceipt::where([
                    'gr_hdr_id' => $grHdr->gr_hdr_id,
                    'created_from' => 'GUN'
                ])
                    ->whereRaw("
                        (
                            SELECT COUNT(pal_sug_loc.act_loc_id) 
                            FROM pal_sug_loc 
                            JOIN gr_dtl ON pal_sug_loc.`gr_dtl_id` = gr_dtl.`gr_dtl_id`
                            WHERE pal_sug_loc.act_loc_id IS NOT NULL
                                AND pal_sug_loc.gr_hdr_id = gr_hdr.gr_hdr_id
                                AND gr_dtl.`gr_dtl_sts` = 'RG'
                        ) = 
                        (
                            SELECT SUM(gr_dtl.gr_dtl_plt_ttl) FROM gr_dtl
                            WHERE gr_dtl.gr_hdr_id = gr_hdr.gr_hdr_id
                            AND gr_dtl.`gr_dtl_sts` = 'RG'
                        )
                    ")
                    ->update(['gr_hdr.putaway' => 1]);
            }


            DB::commit();

            //respond
            $msg = sprintf("%d pallets has been put on Rack successfully!", count($arrUpdate));

            return $this->response->noContent()
                ->setContent(['data' => ['message' => $msg]])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollBack();
            //return $this->response->errorBadRequest($e->getMessage());
            throw $e;
        }
    }

    public function getSuggestLocation($whsId, $grDtlId)
    {
        //update loc id to pallet
        try {
            $hdrData = $this->palletSuggestLocationModel->getHdrPutAwayByHdrDtlID($grDtlId);

            $dtlData = $this->palletSuggestLocationModel->getDtlPutAwayByHdrDtlID($grDtlId);

            $returnData = [];
            foreach ($dtlData as $item) {
                $returnData[] = $item->loc_code;
                $returnData[] = $item->plt_num;
                $returnData[] = $item->loc_code;
            }

            $sku_size_color = $hdrData[0]['sku'];
            $size = !empty($hdrData[0]['size']) ? strtoupper($hdrData[0]['size']) : '';
            if ($size != 'NA') {
                $sku_size_color .= '-' . $hdrData[0]['size'];
            }
            $color = !empty($hdrData[0]['color']) ? strtoupper($hdrData[0]['color']) : '';
            if ($color != 'NA') {
                $sku_size_color .= '-' . $hdrData[0]['color'];
            }
            $hdrData[0]['sku'] = $sku_size_color;

            $data = [
                'hdr' => $hdrData,
                'dtl' => $returnData,
            ];

            return $data;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function putawayDashboard($whsId, Request $request)
    {
        $input = $request->getQueryParams();
        $day = $input['day'];
        $input['whs_id'] = $whsId;
        $input['putter'] = $this->userId;
        try {

            $result = $this->goodReceiptHdrModel->grDashBoard($whsId, $this->userId, $day);


            /*$allPallets = $this->palletSuggestLocationModel->show($input, [], $day)->toArray();
            $all = [];
            if (!empty($allPallets)) {
                foreach ($allPallets as $allPallet) {

                    if (!empty($allPallet['actual'])) {
                        if (empty($all[$allPallet['gr_hdr_id']]['complete'])) {
                            $all[$allPallet['gr_hdr_id']]['complete'] = 0;
                        }
                        $all[$allPallet['gr_hdr_id']]['complete']++;
                    } else {
                        if (empty($all[$allPallet['gr_hdr_id']]['not-complete'])) {
                            $all[$allPallet['gr_hdr_id']]['not-complete'] = 0;
                        }
                        $all[$allPallet['gr_hdr_id']]['not-complete']++;
                    }
                }
            }

            $pallets = $this->palletSuggestLocationModel->search($input, ['pallet']);
            $result = ["putaway_total" => 0, "putting" => 0, "putReady" => 0];
            if (!empty($pallets)) {
                foreach ($pallets as $key => $pallet) {
                    $total = array_get($all, "{$pallet->gr_hdr_id}.complete", 0) +
                        array_get($all, "{$pallet->gr_hdr_id}.not-complete", 0);
                    $actual = array_get($all, "{$pallet->gr_hdr_id}.complete", 0);
                    $result["putaway_total"] = $result["putaway_total"] + 1;
                    $result["putting"] = $result["putting"] + ($total > $actual) ? 1 : 0;
                    $result['putReady'] = $result['putReady'] + ($total == $actual) ? 1 : 0;

                }
            }*/

            return $this->response->array($result);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function checkPallet(\Illuminate\Http\Request $request, $whsId)
    {
        $palletCode = trim($request->get('pallet'));
        $pallet = Pallet::where('pallet.plt_num', $palletCode)
            ->where('whs_id', $whsId)
            ->whereIn('plt_sts', ['NW','RG'])
            ->where('ctn_ttl', '>', 0)
            ->select([
                'plt_id',
                'loc_id',
                'loc_name',
                'loc_code',
                'gr_dtl_id',
                'gr_hdr_id',
            ])
            ->first();

        if (!$pallet) {
            return $this->response()->errorNotFound("Pallet " . $palletCode . ' is not existed or on rack');
        }

        //if ($pallet->gr_hdr_id != $idGrHdr) {
        //    return $this->response()->errorBadRequest("Pallet is not belonged to this GR");
        //}

        if (!is_null($pallet->loc_id)) {
            return $this->response()->errorBadRequest("Pallet has been put away already in location {$pallet->loc_code}");
        }

        return response()->json([
            'message' => "Pallet is OK"
        ]);
    }

    public function checkLocationPallet(\Illuminate\Http\Request $request, $idWH)
    {
        $palletCode = trim($request->get('pallet'));
        $locationCode = trim($request->get('location'));

        $tableGrDtlName = (new GoodsReceiptDetail)->getTable();
        $tablePalletName = (new Pallet)->getTable();

        $pallet = Pallet::where('pallet.rfid', $palletCode)
            ->where('whs_id', $idWH)
            ->where('pallet.ctn_ttl', '>', 0)
            ->where('pallet.plt_sts', "NW")
            ->leftJoin($tableGrDtlName, $tableGrDtlName . '.gr_dtl_id', '=', $tablePalletName . '.gr_dtl_id')
            ->select([
                $tablePalletName . '.plt_id',
                $tablePalletName . '.loc_id',
                $tableGrDtlName . '.spc_hdl_code'
            ])
            ->first();


        if (!$pallet) {
            return $this->response()->errorBadRequest(
                "Pallet is not existed or not belonged to this GR."
            );
        }

        $chk = explode('-', $locationCode);
        $location = null;
        if (count($chk) == 3 && !preg_match('/^([A,C]{1})([0-9]{2})/', $chk[0])) {
            $location = Location::where('loc_code', 'LIKE', $locationCode . "%")
                ->where('loc_whs_id', $idWH)
                ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
                ->where('loc_type.loc_type_code', 'RAC')
                ->select([
                    'loc_id',
                    'loc_code',
                    'spc_hdl_code',
                    'spc_hdl_name'
                ])
                ->first();
        } else {
            $location = Location::where('loc_code', $locationCode)
                ->where('loc_whs_id', $idWH)
                ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
                ->where('loc_type.loc_type_code', 'RAC')
                ->select([
                    'loc_id',
                    'loc_code',
                    'spc_hdl_code',
                    'spc_hdl_name'
                ])
                ->first();
        }


        if (!$location) {
            return $this->response()->errorBadRequest(
                "Location is not existed."
            );
        }

//        if ($pallet->spc_hdl_code != $location->spc_hdl_code) {
//            return $this->response()->errorBadRequest(
//                "Location and pallet are not the same temperature {$pallet->spc_hdl_name}"
//            );
//        }

        return response()->json([
            'message' => "Location and Pallet is OK"
        ]);
    }

    public function getBarcode($idGrDtl)
    {
        $locationUnavailable = Pallet::where('whs_id', 1)
            ->whereNotNull('loc_id')
            ->select([
                'loc_id',
                'loc_code'
            ])
            ->get();
        //dd($locationUnavailable->pluck('loc_id')->toArray());
        $location = Location::where('loc_whs_id', 1)
            ->whereNotIn('loc_id', $locationUnavailable->pluck('loc_id'))
            ->first();

        echo $location->loc_code . "<br><br>";

        $generator = new \Picqer\Barcode\BarcodeGeneratorHTML();
        echo $generator->getBarcode($location->loc_code, $generator::TYPE_CODE_128);
        echo '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';

        $pallet = null;
        if (is_null($location->spc_hdl_code)) {
            $pallet = Pallet::where('whs_id', 1)
                ->whereNull('loc_id')
                ->where('gr_dtl_id', $idGrDtl)
                ->first();
        } else {
            $pallet = Pallet::where('whs_id', 1)
                ->where('spc_hdl_code', $location->spc_hdl_code)
                ->whereNull('loc_id')
                ->where('gr_dtl_id', $idGrDtl)
                ->first();
        }

        echo $pallet->plt_num . "<br><br>";
        echo $pallet->plt_id . "<br><br>";
        echo $pallet->gr_dtl_id . "<br><br>";
        echo $generator->getBarcode($pallet->plt_num, $generator::TYPE_CODE_128);
        echo "<br><br><br>";
    }

    public function searchPutAwayInfo(Request $request, $whsId) {
        try {
            $input = $request->getQueryParams();
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $itemModel = new Item();
            $query = $itemModel->getModel()
                ->join('cartons', 'cartons.item_id', '=', 'item.item_id')
                ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
                ->where('cartons.whs_id', '=', $whsId);

            if(isset($input['sku_upc_des'])) {
                $sku_upc_des = $input['sku_upc_des'];
                $query = $query->where(function($query) use ($sku_upc_des){
                    $query->where('item.sku', '=', $sku_upc_des);
                    $query->orWhere('item.cus_upc', '=', $sku_upc_des);
                    $query->orWhere('item.description', '=', $sku_upc_des);
                });
            }
            if(isset($input['cus_id'])) {
                $cus_id = $input['cus_id'];
                $query = $query->where(function($query) use ($cus_id){
                    $query->where('item.cus_id', '=', $cus_id);
                });
            }
            $data = $query->groupBy('cartons.item_id')
                ->select(DB::raw("sum(cartons.piece_remain) as pieces"), 'item.description', 'item.cus_id', 'item.sku', 'item.cus_upc', 'cartons.lpn_carton as plt_num', 'location.loc_code')
                ->orderBy('cartons.ctn_id', 'desc')
                ->get();
            return response()->json([
                'data' => $data
            ]);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function consolidation(
        Request $request,
        ConsolidationPltValidator $consolidationPltValidator
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        // get data from HTTP
        $input = $request->getParsedBody();
        $whsId = Data::getCurrentWhsId();

        // Validate 1 lpn 1 sku
        $this->validateLpnSku($input['lpn_from'], $input['lpn_to']);
        if(strpos($input['lpn_from'], "P-") === false){
            $listCartonFrom = $this->cartonModel->getModel()->where("loc_code", $input['lpn_from'])->where("whs_id", $whsId)->get()->toArray();
        } else {
            $listCartonFrom = $this->cartonModel->getModel()->where("lpn_carton", $input['lpn_from'])->where("whs_id", $whsId)->get()->toArray();
        }

        if (empty($listCartonFrom)) {
            throw new \Exception("LPN/Location From does not exsit!");
        }
        $is_location = false;
        if(strpos($input['lpn_to'], "P-") === false){
            $is_location = true;
            $listCartonTo = $this->cartonModel->getModel()->where("loc_code", $input['lpn_to'])->where("whs_id", $whsId)->get()->toArray();
            if(!empty($listCartonTo)){
                $cartonTo = $listCartonTo[0];
            }
        } else {
            $listCartonTo = $this->cartonModel->getModel()->where("lpn_carton", $input['lpn_to'])->where("whs_id", $whsId)->get()->toArray();
            if(!empty($listCartonTo)){
                $cartonTo = $listCartonTo[0];
            }
        }
        $fromPltId = array_get($listCartonFrom, "0.plt_id");
        $fromLocId = array_get($listCartonFrom, "0.loc_id");

        $ctnIds = array_column($listCartonFrom, 'ctn_id');
        $ctnNums = array_column($listCartonFrom, 'ctn_num');
        // validation
        $consolidationPltValidator->validate($input);
        $currentLocationAndPalletInfo = $consolidationPltValidator->checkCurrentLoc($fromPltId);

        $toPltId = $fromPltId;
        $is_location_empty = false;
        if(!empty($cartonTo)) {
            $toPltId = array_get($cartonTo, "plt_id");
            $newLocationInfo = $consolidationPltValidator->check($toPltId, $ctnIds);
        } else {
            $msg = sprintf("Location is empty!");
            throw new \Exception($msg);
            /*$is_location_empty = true;
            $newLocationInfo = $this->locationModel->getModel()->where("loc_code", $input['lpn_to'])->where("loc_whs_id", $whsId)->first();
            if (empty($newLocationInfo->loc_zone_id)) {
                $msg = sprintf("This Location %s is not exited",$input['lpn_to']);
                throw new \Exception($msg);
            }*/
        }
        $palletFrom = $currentLocationAndPalletInfo[1];
        /*$palletTo = $this->palletModel->checkPltSameCustomer(
            $palletFrom->whs_id, $palletFrom->cus_id, $toPltId
        );*/
        $palletTo = $this->palletModel->checkPalletByPltId(
            $palletFrom->whs_id, $toPltId
        );
//        $itemIdsFrom = array_unique(array_column($listCartonFrom, 'item_id'));
//        $itemIdsTo = array_unique(array_column($listCartonTo, 'item_id'));
//        if(!empty(array_diff($itemIdsFrom,$itemIdsTo))){
//            $msg = sprintf("From Item does not contain in To Item ");
//            throw new \Exception($msg);
//        }
        $cartonsFrom = object_get($palletFrom, 'carton');
        $itemIdFrom =array_get($cartonsFrom, '0.item_id');
        $cartonsTo = object_get($palletTo, 'carton');
        $itemIdTo =array_get($cartonsTo, '0.item_id');
        $itemFrom = DB::table('item')->where('item_id', $itemIdFrom)->first();
        $itemTo = DB::table('item')->where('item_id', $itemIdTo)->first();

        if(empty($cartonTo)) {
            throw new \Exception("Location is empty!");
        }

        /*if(($itemFrom['spc_hdl_code'] == "BIN" || $itemFrom['spc_hdl_code'] == "SHE") &&  $is_location == false) {
            $msg = 'Special handling code ' . $itemFrom['spc_hdl_code'] . " must scans Location";
            throw new \Exception($msg);
        }*/


//        if (empty($palletTo)) {
//            $msg = 'Source and destination must be belonged to a same customer';
//            throw new \Exception($msg);
//        }
        if(empty($itemFrom)) {
            $msg = 'Current LPN not have item';
            throw new \Exception($msg);
        }
        if(empty($itemTo)) {
            $msg = 'To LPN not have item';
            throw new \Exception($msg);
        }

        if ($palletTo->plt_sts == "RG" || $palletFrom->plt_sts == "RG") {
            $msg = 'Can not consolidate pallet with status Receiving';
            throw new \Exception($msg);
        }
        $toLocId = object_get($newLocationInfo, 'loc_id', 0);
        if($toLocId == $fromLocId) {
            $msg = 'Can not consolidate same location';
            throw new \Exception($msg);  
        }
        $toLocName = object_get($newLocationInfo, 'loc_alternative_name', '');
        $toLocCode = object_get($newLocationInfo, 'loc_code', '');
        $lpn_carton = array_get($cartonTo, 'lpn_carton', '');
        $fromLoc = DB::table("location")->where("loc_id",$fromLocId)->first();
//        if(object_get($newLocationInfo, 'spc_hdl_code', '') !== array_get($fromLoc, "spc_hdl_code","")) {
//            $msg = 'Current LPN and New LPN do not belong to the same Special Handling';
//            throw new \Exception($msg);
//        }
        try {
            //update carton to new location
            $data_rpt_pallet = [
                'loc_id'   => $toLocId,
                'loc_name' => $toLocName,
                'plt_id'   => $toPltId,
                'loc_code' => $toLocCode,
            ];
            if($is_location == true) {
                $data = [
                    'loc_id'   => $toLocId,
                    'loc_name' => $toLocName,
                    'plt_id'   => $toPltId,
                    'loc_code' => $toLocCode,
                ];
            } else {
                $data = [
                    'loc_id'   => $toLocId,
                    'loc_name' => $toLocName,
                    'plt_id'   => $toPltId,
                    'loc_code' => $toLocCode,
                    'lpn_carton' => $lpn_carton
                ];
            }

            DB::beginTransaction();
            $this->cartonModel->updateWhereIn($data, $ctnIds, 'ctn_id');
            DB::table('rpt_carton')->whereIn('ctn_id', $ctnIds)->update(
                $data_rpt_pallet
            );
            $fromLocCode = object_get($currentLocationAndPalletInfo[0], 'loc_code', '');
            $CurrentWhsId = object_get($currentLocationAndPalletInfo[1], 'whs_id', 0);
            $CurrentCusId = object_get($currentLocationAndPalletInfo[1], 'cus_id', 0);

            //event tracking carton location
            foreach ($ctnNums as $ctn_num) {
                // tracking carton location
                $evt_owner = $this->palletModel->generateEventOwner('CON');
                $this->eventTrackingModel->refreshModel();

                $this->eventTrackingModel->create([
                    'whs_id'    => $CurrentWhsId,
                    'cus_id'    => $CurrentCusId,
                    'owner'     => $evt_owner,
                    'evt_code'  => config('constants.event.CON-CTN-LOC'),
                    'trans_num' => $evt_owner,
                    'info'      => sprintf(config('constants.event-info.CON-CTN-LOC'), $ctn_num, $fromLocCode,
                        $toLocCode)

                ]);

                $evt_owner = $this->palletModel->generateEventOwner('CON');
                $this->eventTrackingModel->refreshModel();

                $this->eventTrackingModel->create([
                    'whs_id'    => $CurrentWhsId,
                    'cus_id'    => $CurrentCusId,
                    'owner'     => $evt_owner,
                    'evt_code'  => config('constants.event.CON-LP-LOC'),
                    'trans_num' => $evt_owner,
                    'info'      => sprintf(config('constants.event-info.CON-LP-LOC'), $ctn_num,
                        $palletTo->plt_num, $palletTo->plt_num)
                ]);
            }

            $evt_owner = $this->palletModel->generateEventOwner('CON');
            //event tracking carton location complete
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $CurrentWhsId,
                'cus_id'    => $CurrentCusId,
                'owner'     => $evt_owner,
                'evt_code'  => config('constants.event.COMPLETE-CON'),
                'trans_num' => $evt_owner,
                'info'      => sprintf(config('constants.event-info.COMPLETE-CON'), $evt_owner)
            ]);

            //Remove loc_id from pallet when pallet is empty
            $fromLocId = $currentLocationAndPalletInfo[0]->loc_id;
            $arr_loc_id = [$fromLocId];



            // update total carton pallet
            $this->palletModel->updateWhere(
                ["ctn_ttl" => $this->cartonModel->checkWhere(['plt_id' => $toPltId])],
                ['plt_id' => $toPltId]
            );
            $this->palletModel->updateWhere(
                ["ctn_ttl" => $this->cartonModel->checkWhere(['plt_id' => $fromPltId])],
                ['plt_id' => $fromPltId]
            );


            if (empty($this->palletModel->getFirstWhere(['loc_id' => $fromLocId])->ctn_ttl)
            ) {
                $this->palletModel->removeWhereInLocation($arr_loc_id);
                DB::table("location")
                    ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
                    ->join('zone_type', 'zone_type.zone_type_id', '=', 'zone.zone_type_id')
                    ->where("location.loc_id", $fromLocId)
                    ->where("zone_type.zone_type_code", "DZ")
                    ->update(['location.loc_zone_id' => null]);
            }
            /*$loc_zone_id = DB::table("location")->where("location.loc_id", $fromLocId)->where("location.loc_whs_id", $whsId)->value("loc_zone_id");
            DB::table("location")->where("loc_id", $toLocId)->update(["loc_zone_id" =>$loc_zone_id]);*/

            /*DB::table("pal_sug_loc")
                ->where("plt_id", $palletFrom->plt_id)
                ->where("loc_id", $palletFrom->loc_id)
                ->update([
                    'plt_id' => $toPltId,
                    'loc_id' => $toLocId,
                    'data' => $toLocCode,
                    'act_loc_id' => $toLocId,
                    'act_loc_code' => $toLocCode
                ]);*/

            DB::table('rpt_pallet')->where('plt_id', $toPltId)->update(
                ["ctn_ttl" =>  $this->cartonModel->checkWhere(['plt_id' => $toPltId])]
            );
            $this->palletModel->updatePalletZeroDateAndDurationDaysAuto();
            DB::commit();
            return response()->json([
                'message' => "Consolidate Successfully!"
            ]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function validateLpnSku($lpn_from, $lpn_to) {

        if($this->validateLPNFormat($lpn_from)) {
            $carton_from = DB::table("cartons")
                ->where("lpn_carton", $lpn_from)->where("deleted", 0)
                ->select("item_id")
                ->groupBy("item_id")->get();
            if(count($carton_from) < 1) {
                $msg = sprintf('The LPN %s do not use for any items.', $lpn_from);
                throw new \Exception($msg);
            }
        }else {
            $location = DB::table("location")
                ->where("loc_code", $lpn_from)->where("deleted", 0)->get();
            if(empty($location)){
                $msg = sprintf('The Location %s does not exist.', $lpn_from);
                throw new \Exception($msg);
            }
            $carton_from = DB::table("cartons")
                ->where("loc_code", $lpn_from)->where("deleted", 0)->select("item_id")
                ->groupBy("item_id")->get();

            if(count($carton_from) < 1) {
                $msg = sprintf('The Location %s does not empty.', $lpn_from);
                throw new \Exception($msg);
            }
        }
        $item_ids = array_pluck($carton_from,"item_id");

        if(empty($carton_from)) {
            $msg = sprintf('The LPN %s is not valid.', $lpn_from);
            throw new \Exception($msg);
        }

        /*if($this->validateLPNFormat($lpn_to)) {
            $carton_to = DB::table("cartons")->where("lpn_carton", $lpn_to)
                ->whereIn("item_id", $item_ids)
                ->where("deleted", 0)->select("item_id")->first();
            if(!empty($carton_to)) {
                $msg = sprintf('The LPN %s is used for another SKU.', $lpn_to);
                throw new \Exception($msg);
            }
            return 0;
        }*/
        if ($this->validateLPNFormat($lpn_to)) {

            $carton_to = DB::table("cartons")
                ->where(function ($query) use ($lpn_to) {
                    $query->where('loc_code', $lpn_to);
                    $query->orwhere('lpn_carton', $lpn_to);
                })
                ->where("deleted", 0)->select("item_id")->groupBy("item_id")->get();
            $item_ids_to = array_pluck($carton_to,"item_id");

            $result=array_diff($item_ids,$item_ids_to);
            if (!empty($result)) {
                $msg = sprintf('The Location/LPN %s do not have any cartons of this SKU.', $lpn_to);
                throw new \Exception($msg);
            }
        }


        return 0;
    }

    public function validateLPNFormat($plt_num){
        if((bool)preg_match("/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }
        if((bool)preg_match("/^T-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }

        if((bool)preg_match("/^V-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }
        return false;
    }

    /**
     * @param Request $request
     * @param ConsolidationPltValidator $consolidationPltValidator
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function consolidate(Request $request, ConsolidationPltValidator $consolidationPltValidator)
    {
        $request = $request->getParsedBody();
        $consolidationPltValidator->validate($request);

        $whsId = Data::getCurrentWhsId();
//        $itemIds = explode(',', array_get($request, 'item_ids'));
//        if (empty($itemIds)) {
//            return $this->response()->errorBadRequest('Please select one or more SKU');
//        }

        $currentLocOrLPN = array_get($request, 'lpn_from');
        $toLocOrLPN = array_get($request, 'lpn_to');
        $curLPNCarton = null;
        $toLPNCarton = null;

        if ($currentLocOrLPN === $toLocOrLPN) {
            return $this->response()->errorBadRequest('Cannot consolidate to original LPN or Location');
        }

        $curCartons = $this->cartonModel->getModel()
            ->where('ctn_sts', 'AC')
            ->where('whs_id', $whsId)
//            ->whereIn('item_id', $itemIds)
            ->select(['plt_id', 'whs_id', 'cus_id', 'loc_id', 'loc_code', 'loc_name', 'lpn_carton']);

        if ($this->isLPN($currentLocOrLPN)) {
            $curLPNCarton = $currentLocOrLPN;
            $curCartons = $curCartons->where('lpn_carton', $currentLocOrLPN);
            $currentLocOrLPN = $this->cartonModel->getModel()
                ->where('lpn_carton', '=', $currentLocOrLPN)
                ->where('whs_id', '=', $whsId)
                ->value('loc_code');
        } else {
            $curCartons = $curCartons->where('loc_code', $currentLocOrLPN);
        }

        $curCartons = $curCartons->groupBy('plt_id', 'cus_id', 'loc_id')->get()->toArray();
        if (empty($curCartons)) {
            return $this->response()->errorBadRequest(sprintf('LPN Or Location %s dose not have any cartons', $currentLocOrLPN));
        }
        $curCusIds = array_unique(array_pluck($curCartons, 'cus_id'));

        if ($this->isLPN($toLocOrLPN)) {
            return $this->response()->errorBadRequest('You can only consolidate to location');
        } else {
            $checkLocation = $this->locationModel->getModel()->where([
                'loc_code' => $toLocOrLPN,
                'loc_whs_id' => $whsId,
                'parent_id' => null,
                'loc_sts_code' => 'AC'
            ])->value('loc_sts_code');
            if (! $checkLocation) {
                return $this->response()->errorBadRequest(sprintf('Location %s is not active', $toLocOrLPN));
            }
        }

        try {
            DB::beginTransaction();

            if (! empty($curCusIds)) {
                foreach ($curCusIds as $cusId) {
                    $toLocation = $this->locationModel->getFirstWhere([
                        'loc_code' => $toLocOrLPN,
                        'loc_whs_id' => $whsId,
                        'parent_id' => null,
                        'loc_sts_code' => 'AC'
                    ]);

                    $toPallet = $this->palletModel->getFirstWhere([
                        'whs_id' => $whsId,
                        'cus_id' => $cusId,
                        'loc_code' => $toLocOrLPN,
                        //'plt_sts' => 'AC'
                    ]);

                    if (empty($toPallet)) {
                        // create pallet
                        $palletNum = $this->palletModel->generatePltNum();
                        $this->palletModel->refreshModel();
                        $toPallet = $this->palletModel->create([
                            'plt_num' => $palletNum,
                            'loc_id' => object_get($toLocation, 'loc_id'),
                            'loc_code' => object_get($toLocation, 'loc_code'),
                            'loc_name' => object_get($toLocation, 'loc_code'),
                            'whs_id' => object_get($toLocation, 'loc_whs_id'),
                            'cus_id' => $cusId,
                            'plt_sts' => 'AC',
                            'created_at' => time(),
                            'created_by' => Data::getCurrentUserId(),
                            'updated_at' => time(),
                            'updated_by' => Data::getCurrentUserId(),
                            'ctn_ttl' => 0
                        ]);
                    }

                    // update cartons
                    $cartonModel = [
                        'plt_id' => $toPallet->plt_id,
                        'loc_id' => $toPallet->loc_id,
                        'loc_code' => $toPallet->loc_code,
                        'loc_name' => $toPallet->loc_code,
                        'updated_at' => time(),
                        'updated_by' => Data::getCurrentUserId()
                    ];

                    if ($toLPNCarton) {
                        $cartonModel['lpn_carton'] = $toLPNCarton;
                    }

                    $whereCarton = [
                        'whs_id' => $whsId,
                        'cus_id' => $cusId,
                        'ctn_sts' => 'AC'
                    ];

                    if ($curLPNCarton) {
                        $whereCarton['lpn_carton'] = $curLPNCarton;
                    } else {
                        $whereCarton['loc_code'] = $currentLocOrLPN;
                    }

                    // consolidate
                    $this->cartonModel->refreshModel();
                    //$this->cartonModel->getModel()->where($whereCarton)->whereIn('item_id', $itemIds)->update($cartonModel);
                    $this->cartonModel->getModel()->where($whereCarton)->update($cartonModel);

                    // update to pallet
                    $this->updateCartonTotalInPallet($toPallet);

                    // update current pallet
                    $currentPallet = $this->palletModel->getFirstWhere([
                        'whs_id' => $whsId,
                        'cus_id' => $cusId,
                        'loc_code' => $currentLocOrLPN,
                        //'plt_sts' => 'AC'
                    ]);

                    if (! empty($currentPallet)) {
                        $this->updateCartonTotalInPallet($currentPallet);
                    }
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'Consolidate Successfully'
            ]);

        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response()->errorBadRequest(SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $lpnCarton
     * @return bool|void
     */
    public function isLPN($lpnCarton)
    {
        if (strpos($lpnCarton, 'P-') === false) {
            return false;
        }
        if (! $this->validateLPNFormat($lpnCarton)) {
            return $this->response()->errorBadRequest(sprintf('LPN %s is not valid', $lpnCarton));
        }
        return true;
    }

    /**
     * @param $pallet
     */
    public function updateCartonTotalInPallet($pallet)
    {
        $cartonTotal = $this->cartonModel->getModel()
            ->where('whs_id', '=', $pallet->whs_id)
            ->where('cus_id', '=', $pallet->cus_id)
            ->where('plt_id', '=', $pallet->plt_id)
            ->count();

        $this->palletModel->refreshModel();
        $pallet->ctn_ttl = $cartonTotal;
        $pallet->updated_at = time();
        $pallet->updated_by = Data::getCurrentUserId();

        if ($cartonTotal === 0) {
            $pallet->loc_id = null;
            $pallet->loc_code = null;
        }

        $pallet->save();
    }

}

