<?php

namespace Seldat\Wms2\Models;

class ReceivingContainerReport extends BaseSoftModel
{
    protected $table = 'gr_hdr';
    
    protected $primaryKey = 'gr_hdr_id';
    
    protected $fillable = [
        'ctnr_id',
        'asn_hdr_id',
        'gr_hdr_seq',
        'gr_hdr_ept_dt',
        'gr_hdr_num',
        'whs_id',
        'cus_id',
        'gr_in_note',
        'gr_ex_note',
        'gr_sts',
        'putter',
        'ctnr_num',
        'ref_code',
        'ctnr_num',
    ];

    public function Customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
    
    public function Warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }
    
    public function User()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'user_id');
    }
    
    public function GoodsReceiptDetail()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptDetail', 'gr_dtl_id', 'gr_dtl_id');
    }
    
    public function GoodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'gr_hdr_id', 'gr_hdr_id');
    }
    
    public function InvoiceHeader()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceHeader', 'cus_id', 'cus_id');
    }
    
    public function Carton()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Carton', 'gr_dtl_id', 'gr_dtl_id');
    }
    
    public function LocationType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\LocationType', 'loc_type_code', 'loc_type_code');
    }
}
