<?php

namespace App\Api\V1\Validators;

use App\WarehouseQualifier;

class WarehouseMetaValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_meta_value' => '',
            'whs_qualifier'  => 'required|exists:whs_qualifier,qualifier'
        ];
    }
}
