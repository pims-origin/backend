<?php

namespace App\Api\V2\Inbound\Pallet2\GetInfoByRfid\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Logs;
use Psr\Http\Message\ServerRequestInterface as Request;

class Log extends AbstractModel
{

    protected $asnHdr;
    /*
     * @var Log
     */
    protected static $instance = null;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new Logs();
    }

    public static function getInstance(){
        if(! self::$instance){
            self::$instance = new self();
        }
        return self::$instance ;
    }

    public static function error(Request $request, $whsId, $logsData=[]){
        $instance = self::getInstance();
        return $instance->insertError($request, $whsId, $logsData);
    }

    public static function info(Request $request, $whsId, $logsData=[]){
        $instance = self::getInstance();
        return $instance->insertInfo($request, $whsId, $logsData);
    }

    public static function respond(Request $request, $whsId, $logsData=[]){
        $instance = self::getInstance();
        return $instance->insertRespondData($request, $whsId, $logsData);
    }

    public static function debug(Request $request, $whsId, $logsData=[]){
        $instance = self::getInstance();
        return $instance->insertDebug($request, $whsId, $logsData);
    }


    public function insertError(Request $request, $whsId, $logsData=[]){

        $resData = array_merge($request->getQueryParams(), $request->getParsedBody());
        $headerData =  $request->getHeaders();

        // Logs Model
        $this->refreshModel();
        $response = array_get($logsData, 'response_data', 'NA');

        $this->create([
            'whs_id' => $whsId,
            'type' => 'error',
            'evt_code' => array_get($logsData, 'evt_code', 'NA'),
            'owner' => array_get($logsData, 'owner', 'NA'),
            'transaction' => array_get($logsData, 'transaction', 'NA'),
            'url_endpoint' => array_get($logsData, 'url_endpoint', 'NA'),
            'message' => array_get($logsData, 'message', 'NA'),
            'header' => is_array($headerData) ? \GuzzleHttp\json_encode($headerData) : $headerData,
            'request_data' => is_array($resData) ? \GuzzleHttp\json_encode($resData) : $resData,
            'response_data' => is_array($response) ? \GuzzleHttp\json_encode($response) : $response
        ]);
    }

    public function insertInfo(Request $request, $whsId, $logsData=[]){

        if (!env('API_LOG_INFO')) {
            return false;
        }
        $resData = array_merge($request->getQueryParams(), $request->getParsedBody());
        $headerData =  $request->getHeaders();

        // Logs Model
        $this->refreshModel();
        $response = array_get($logsData, 'response_data', 'NA');

        return $this->create([
            'whs_id' => $whsId,
            'type' => 'info',
            'evt_code' => array_get($logsData, 'evt_code', 'NA'),
            'owner' => array_get($logsData, 'owner', 'NA'),
            'transaction' => array_get($logsData, 'transaction', 'NA'),
            'url_endpoint' => array_get($logsData, 'url_endpoint', 'NA'),
            'message' => array_get($logsData, 'message', 'NA'),
            'header' => is_array($headerData) ? \GuzzleHttp\json_encode($headerData) : $headerData,
            'request_data' => is_array($resData) ? \GuzzleHttp\json_encode($resData) : $resData,
            'response_data' => is_array($response) ? \GuzzleHttp\json_encode($response) : $response
        ]);

    }

    public function insertRespondData(Request $request, $whsId, $logsData=[]){

        if (!env('API_LOG_INFO')) {
            return false;
        }
        $resData = array_merge($request->getQueryParams(), $request->getParsedBody());
        $headerData =  $request->getHeaders();

        // Logs Model
        $this->refreshModel();
        $response = array_get($logsData, 'response_data', 'NA');

        return $this->create([
            'whs_id' => $whsId,
            'type' => array_get($logsData, 'log_type', 'log'),
            'evt_code' => array_get($logsData, 'evt_code', 'NA'),
            'owner' => array_get($logsData, 'owner', 'NA'),
            'transaction' => array_get($logsData, 'transaction', 'NA'),
            'url_endpoint' => array_get($logsData, 'url_endpoint', 'NA'),
            'message' => array_get($logsData, 'message', 'Successfully'),
            'header' => is_array($headerData) ? \GuzzleHttp\json_encode($headerData) : $headerData,
            'request_data' => is_array($resData) ? \GuzzleHttp\json_encode($resData) : $resData,
            'response_data' => is_array($response) ? \GuzzleHttp\json_encode($response) : $response
        ]);

    }

    public function insertDebug(Request $request, $whsId, $logsData=[]){

        if (!env('API_DEBUG')) {
            return false;
        }
        $resData = array_merge($request->getQueryParams(), $request->getParsedBody());
        $headerData =  $request->getHeaders();

        // Logs Model
        $this->refreshModel();
        $response = array_get($logsData, 'response_data', 'NA');

        return $this->create([
            'whs_id' => $whsId,
            'type' => 'debug',
            'evt_code' => array_get($logsData, 'evt_code', 'NA'),
            'owner' => array_get($logsData, 'owner', 'NA'),
            'transaction' => array_get($logsData, 'transaction', 'NA'),
            'url_endpoint' => array_get($logsData, 'url_endpoint', 'NA'),
            'message' => array_get($logsData, 'message', 'NA'),
            'header' => is_array($headerData) ? \GuzzleHttp\json_encode($headerData) : $headerData,
            'request_data' => is_array($resData) ? \GuzzleHttp\json_encode($resData) : $resData,
            'response_data' => is_array($response) ? \GuzzleHttp\json_encode($response) : $response
        ]);

    }
}
