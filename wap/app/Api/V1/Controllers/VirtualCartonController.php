<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\Log;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\VirtualCartonSumModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Transformers\AsnscanStatusTransformer;
use App\Api\V1\Transformers\ListAllVirtualCartonTransformer;
use App\Api\V1\Validators\InsertVirtualCartonValidator;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Models\CustomerWarehouse;

class VirtualCartonController extends AbstractController
{
    /**
     * @var VirtualCartonModel
     */
    private $vtlCtn;

    /**
     * @var VirtualCartonSumModel
     */
    private $vtlCtnSum;

    /**
     * @var AsnDtlModel
     */
    private $asnDtlModel;

    /**
     * @var AsnHdrModel
     */
    protected $asnHdrModel;

    /**
     * @var InsertVirtualCartonValidator
     */
    protected $insertVtlCtn;
    protected $itemModel;
    protected $containerModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var EventTrackingModel
     */
    protected $grHdrModel;
    protected $cartonModel;
    protected $grDtlModel;
    protected $pltModel;
    /**
     * VirtualCartonController constructor.
     *
     * @param VirtualCartonModel $vtlCtn
     * @param VirtualCartonSumModel $vtlCtnSum
     * @param AsnDtlModel $asnDtlModel
     */
    public function __construct(VirtualCartonModel $vtlCtn, VirtualCartonSumModel $vtlCtnSum, AsnDtlModel $asnDtlModel)
    {
        $this->vtlCtn = $vtlCtn;
        $this->vtlCtnSum = $vtlCtnSum;
        $this->asnDtlModel = $asnDtlModel;
        $this->insertVtlCtn = new InsertVirtualCartonValidator();
        $this->asnHdrModel = new AsnHdrModel();
        $this->itemModel = new ItemModel();
        $this->containerModel = new ContainerModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->grHdrModel = new GoodsReceiptModel();
        $this->cartonModel = new CartonModel();
        $this->grDtlModel = new GoodsReceiptDetailModel();
        $this->pltModel = new PalletModel();
    }

    /**
     * @param Request $request
     * @param $whsId
     * @param $cusId
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */

    public function verifyCartonRfids(Request $request, $whsId, $cusId)
    {
        $input = $request->getParsedBody();
        //$this->insertVtlCtn->validate($input);
        $requireFieldsValidate = $this->insertVtlCtn->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        $asnDtlId = array_get($input, 'asn_dtl_id', null);
        $arrCtnRFID = array_get($input, 'ctns_rfid', null);
        $rcv = [];
        $notRcv = [];
        foreach ($arrCtnRFID as $key => $ctn) {
            $check = $this->vtlCtn->getFirstWhere([
                'asn_dtl_id' => $asnDtlId,
                'ctn_rfid'   => $ctn
            ]);

            if (!empty($check)) {
                $rcv[] = $ctn;
            } else {
                $notRcv[] = $ctn;
            }
        }

        return $this->response->noContent()
            ->setContent([
                'data' => [
                    'status'       => true,
                    'received'     => $rcv,
                    'not_received' => $notRcv
                ]
            ])
            ->setStatusCode(IlluminateResponse::HTTP_CREATED);

    }

    /**
     * @param Request $request
     * @param $whsId
     * @param $cusId
     *
     * @return Response
     */
    public function scanCartons(Request $request, $whsId, $cusId)
    {
        $input = $request->getParsedBody();

        /**
         * Log
         */
        $url = "inbound/whs/{$whsId}/cus/{$cusId}/add-virtual-cartons";
        $transaction = $owner = '';
        Log::info($request, $whsId, [
            'evt_code'     => 'ACC',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Scan Cartons Start'
        ]);

        $requireFieldsValidate = $this->insertVtlCtn->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        $asnHdrId = array_get($input, 'asn_hdr_id', 0);
        $asnDtlId = array_get($input, 'asn_dtl_id', 0);
        $itemId = array_get($input, 'item_id', 0);
        $ctnrId = array_get($input, 'ctnr_id', 0);

        $ctnRfidsRequired = array_get($input, 'ctns_rfid', '');
        if (count($ctnRfidsRequired) == 0) {
            $msg = "Ctns_rfid is required!";
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        // Check duplicate cartons rfid
        $uniqueRFIDs = array_unique($ctnRfidsRequired);
        if (count($uniqueRFIDs) != count($ctnRfidsRequired)) {
            $duplicateRFID = array_unique(array_diff_assoc($ctnRfidsRequired, $uniqueRFIDs));
            $msg = 'Duplicate Carton: ' . implode(', ', $duplicateRFID);
            $data = [
                'data'    => null,
                'message' => $msg,
                'code'    => 'WAP002',
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $ansHdr = $this->asnHdrModel->getFirstWhere([
                                                        'asn_hdr_id' => $asnHdrId,
                                                        'whs_id'     => $whsId,
                                                        'cus_id'     => $cusId,
                                                    ]);
        if(!$ansHdr){
            $msg = "The ASN doesn't exist ";
            $ansHdr = $this->asnHdrModel->getFirstWhere([
                                                        'asn_hdr_id' => $asnHdrId,
                                                    ]);
            if ($ansHdr) {
                $msg = sprintf("The ASN doesn't belong to warehouse %s and customer %s", $whsId, $cusId);
            }
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }
        else {
            $asnSts = object_get($ansHdr, 'asn_sts', '');
            if ($asnSts == 'RE') {
                $msg = sprintf('Unable to scan new cartons for Received ASN!');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            if ($asnSts == 'CO') {
                $msg = sprintf('Unable to scan new cartons for Completed ASN!');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }

        $grHdr = $this->grHdrModel->getFirstWhere(['asn_hdr_id' => $asnHdrId , 'ctnr_id' => $ctnrId]);

        $grSts = object_get($grHdr, 'gr_sts', '');
        if($grSts == 'RE') {
            $msg = sprintf('Unable to scan new cartons for completed Good Receipt!');
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $asnDtlObj = $this->asnDtlModel->getFirstBy('asn_dtl_id', $asnDtlId);
        if (!$asnDtlObj) {
            $msg = "ASN detail doesn't exist!";
            $dataValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataValidate, 200, [], null);
        }

        if ($asnDtlObj && $asnDtlObj->asn_dtl_sts == "CC") {
            $msg = "ASN detail already canceled!";
            $dataValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataValidate, 200, [], null);
        }

        if (empty($this->itemModel->checkWhere(['item_id' => $itemId]))) {
            $msg = "Item doesn't exist!";
            $dataValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataValidate, 200, [], null);
        }

        if (empty($this->containerModel->checkWhere(['ctnr_id' => $ctnrId]))) {
            $msg = "Container doesn't exist!";
            $dataValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataValidate, 200, [], null);
        }

        if ($asnHdrId != object_get($asnDtlObj, 'asn_hdr_id')) {
            $msg = "ASN detail doesn't belong to ASN!";
            $dataValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataValidate, 200, [], null);
        }

        if ($ctnrId != object_get($asnDtlObj, 'ctnr_id')) {
            $msg = "ASN detail doesn't belong to Container!";
            $dataValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataValidate, 200, [], null);
        }

        if ($itemId != object_get($asnDtlObj, 'item_id')) {
            $msg = "Item doesn't belong to ASN detail!";
            $dataValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataValidate, 200, [], null);
        }

        $ctn_rfid = array_get($input, 'ctns_rfid.0', '');
        $arrCtnRFID = array_get($input, 'ctns_rfid', null);
        $gateCode = array_get($input, 'gate_code', '');
        $type = array_get($input, 'type', false);

        $ansHdr = $this->asnHdrModel->getFirstWhere(['asn_hdr_id' => $asnHdrId]);
        $owner = array_get($ansHdr, 'asn_hdr_num', '');

        $transaction = $ctnrId;
        try {
            /**
             * Log
             */
            // $url = "inbound/whs/{$whsId}/cus/{$cusId}/add-virtual-cartons";
            Log::info($request, $whsId, [
                'evt_code'     => 'ACC',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Scan Cartons Processing'
            ]);

            //validate carton RFID
            $ctnRfidInValid = [];
            $errorMsg = '';
            foreach ($arrCtnRFID as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }

            //if has errors, return all error data and message
            if ($ctnRfidInValid) {
                $data = [
                    'data'    => $ctnRfidInValid,
                    'message' => $errorMsg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            if (!$type) {
                $msg = MessageCode::get('WAP001');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'code'    => 'WAP001',
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            // check carton if type = 2 return check carton
            // if type = 1 for insert carton

            if ($type == 2) {
                return $this->verifyCartonRfids($request, $whsId, $cusId);
            }

            $vtlCtnDataCommon = [
                'asn_hdr_id'  => $asnHdrId,
                'asn_dtl_id'  => $asnDtlId,
                'item_id'     => array_get($input, 'item_id', null),
                'ctnr_id'     => array_get($input, 'ctnr_id', null),
                'cus_id'      => $cusId,
                'whs_id'      => $whsId,
                'scanned'     => '1',
                'is_damaged'  => array_get($input, 'is_damaged', 0),
                'vtl_ctn_sts' => Status::getByValue('NEW', 'VIRTUAL-CARTON-STATUS'),
            ];

            $dataVtlCtnDtl = [];

            $dataVtlCtnSum = [
                'asn_hdr_id'      => array_get($input, 'asn_hdr_id', null),
                'asn_dtl_id'      => array_get($input, 'asn_dtl_id', null),
                'cus_id'          => $cusId,
                'whs_id'          => $whsId,
                'item_id'         => array_get($input, 'item_id', null),
                'ctnr_id'         => array_get($input, 'ctnr_id', null),
                'discrepancy'     => array_get($input, 'discrepancy', null),
                'vtl_ctn_sum_sts' => Status::getByValue('RECEIVING', 'VIRTUAL-CARTON-SUMMARY-STATUS'),
                //'gate_code'       => $gateCode,
            ];

            //get asn request number of carton
            $asnDtl = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $dataVtlCtnSum['asn_dtl_id']]);

            $vtlCtnSum = $this->vtlCtnSum->getFirstWhere(['asn_dtl_id' => $dataVtlCtnSum['asn_dtl_id']]);

            $vtlCtnSumSts = object_get($vtlCtnSum, 'vtl_ctn_sum_sts', '');

            if (trim($vtlCtnSumSts) == Status::getByValue('RECEIVED', 'VIRTUAL-CARTON-SUMMARY-STATUS')) {
                $msg = sprintf(MessageCode::get('WAP005'), $asnDtlId);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'code'    => 'WAP005',
                    'status'  => false,
                ];

                $this->setData($data);
                $this->setMessage($msg);
                Log::error($request, $whsId, [
                    'evt_code'      => 'EAN',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);
                throw new \Exception($this->getMessage());

            }

            // start transaction
            DB::beginTransaction();

            // Check input cartons.ctn_status = AJ
            $cartonsTemps = DB::table('cartons')
                ->whereIn('rfid', $ctnRfidsRequired)
                ->whereIn('ctn_sts', ['AJ', 'IA'])
                ->where('deleted', 0)
                ->get();
            // Update vtl_ctn and cartons when cartons.ctn_status = AJ
            foreach ($cartonsTemps as $cTemp) {
                // update vtl_ctn
                $this->vtlCtn->updateWhere(
                    [
                        'deleted' => 1,
                        'deleted_at' => time(),
                    ],
                    [
                        'ctn_rfid' => $cTemp['rfid']
                    ]
                );
                // update cartons
                DB::table('cartons')->where('rfid', $cTemp['rfid'])
                    ->where('deleted', 0)
                    ->update([
                        'deleted' => 1,
                        'deleted_at' => time(),
                    ]);
            }

            if ($ansHdr->asn_type === 'RMA'){
                $this->cartonModel->deletePutBackAndShippedCartons($ctnRfidsRequired);
            }


            //get vtl ctn by ans hdr id
            $existAsnHdr = $this->vtlCtnSum->getFirstWhere(
                [
                    'asn_hdr_id' => $asnHdrId,
                    'asn_dtl_id' => $asnDtlId,
                ]
            );

            $insertVtlCtnSumId = null;
            $stsIns = false;
            if (empty($existAsnHdr)) {
                //insert virtual carton summary
                $insertVtlCtnSumId = $this->vtlCtnSum->create($dataVtlCtnSum)->vtl_ctn_sum_id;
            } else {
                $insertVtlCtnSumId = object_get($existAsnHdr, 'vtl_ctn_sum_id', null);
            }

            //get vtl ctn by vtl ctn ctn_rfid
            $existVtlCtn = $this->vtlCtn->getFirstWhere(
                [
                    'ctn_rfid' => array_get($input, 'ctn_rfid', '')
                ]
            );

            if (!empty($existVtlCtn)) {
                //insert virtual carton summary
                throw new \Exception(Message::get('BM097', 'This virtual carton\'s RFID'));
            }

            $user = (new Data())->getUserInfo();
            foreach ($arrCtnRFID as $key => $ctn) {
                $vtlCtnDataCommon['vtl_ctn_sum_id'] = $insertVtlCtnSumId;
                $vtlCtnDataCommon['ctn_rfid'] = $ctn;
                $vtlCtnDataCommon['created_at'] = time();
                $vtlCtnDataCommon['updated_at'] = time();
                $vtlCtnDataCommon['created_by'] = $user['user_id'];
                $vtlCtnDataCommon['updated_by'] = $user['user_id'];
                $vtlCtnDataCommon['deleted_at'] = 915148800;
                $vtlCtnDataCommon['deleted'] = 0;

                //init data array
                $dataVtlCtnDtl[] = $vtlCtnDataCommon;
            }

            //check carton is existed
            $duplicateCtn = $this->checkExistedCarton($dataVtlCtnDtl);
            if (!empty($duplicateCtn)) {
                // update vtl_ctn
//                $this->vtlCtn->getModel()
//                    ->whereIn('ctn_rfid', $duplicateCtn)
//                    ->update([
//                        'deleted' => 1,
//                        'deleted_at' => time(),
//
//                    ]);
                //insert virtual carton summary
                $this->setData(['ctns_rfid' => $duplicateCtn]);
                $msg = 'There are cartons existed.';
                $this->setMessage($msg);

                Log::error($request, $whsId, [
                    'evt_code'      => 'EDC',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);

                return $this->response->noContent()
                    ->setContent([
                        'data' => [
                            'status'  => false,
                            'message' => $msg
                        ]
                    ])
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);

            }

            try {
                //insert virtual carton
                $insertVtlCtn = DB::table('vtl_ctn')->insert($dataVtlCtnDtl);
            } catch (\Exception $e) {
                $msg = MessageCode::get('WAP002');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'code'    => 'WAP002',
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            if ($insertVtlCtn) {
                //get asn hdr num by asn hdr id for EVT
                $asnHdrObj = $this->asnHdrModel->getASNHdrById($asnHdrId);

                //check ANS Status is Receiving or not, if not go to update ASN Status
                if (!empty($asnHdrObj) && $asnHdrObj->asn_sts != Status::getByKey('ASN_STATUS', 'RECEIVING')) {

                    //update ASN detail status
                    $this->updateASNReceiving($asnHdrId);

                    //params for ASN event tracking receiving
                    $evtASNReceiving = [
                        'whs_id'    => $whsId,
                        'cus_id'    => $cusId,
                        'owner'     => $asnHdrObj->asn_hdr_num,
                        'evt_code'  => Status::getByKey('EVENT', 'ASN-RECEIVING'),
                        'trans_num' => $asnHdrObj->asn_hdr_num,
                        'info'      => sprintf(Status::getByKey('EVENT-INFO', 'ASN-RECEIVING'),
                            $asnHdrObj->asn_hdr_num),
                    ];

                    //call save event tracking for ASN receiving
                    $this->eventTracking($evtASNReceiving);
                }

                $this->updateASNDtlReceiving($asnDtlId);

                DB::commit();
                $msg = sprintf("Add new cartons has been scanned successfully!");
                $stsIns = true;

                return $this->response->noContent()
                    ->setContent([
                        'data' => [
                            'status'  => $stsIns,
                            'message' => $msg
                        ]
                    ])
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => "inbound/whs/{$whsId}/cus/{$cusId}/add-virtual-cartons",
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    private function checkExistedCarton($dataVtlCtnDtl)
    {
        $duplicate = [];
        $inputCtn = [];
        foreach ($dataVtlCtnDtl as $ctn) {
            $inputCtn[] = $ctn['ctn_rfid'];
        }
        $vtlCtnObj = $this->vtlCtn->getModel()->whereIn('ctn_rfid', $inputCtn)->get();
        if ($vtlCtnObj) {
            $existedVtlCtn = array_pluck($vtlCtnObj, 'ctn_rfid', null);
            $duplicate = array_diff($inputCtn, array_diff($inputCtn, $existedVtlCtn));
        }

        return $duplicate;
    }

    /**
     * @param $ansDtlId
     *
     * @return mixed
     * @deprecated not in use
     */
    private function updateVtlCtnSumCompleted($ansDtlId)
    {
        return $this->vtlCtnSum->updateWhere(
            [
                'vtl_ctn_sum_sts' => Status::getByValue('RECEIVED', 'VIRTUAL-CARTON-SUMMARY-STATUS')
            ],
            ['asn_dtl_id' => $ansDtlId]);
    }

    /**
     * @param $ansDtlId
     *
     * @return mixed
     */
    private function updateAsnDtlReceived($ansDtlId)
    {
        $status = Status::getByKey('ASN_DTL_STS', 'RECEIVED');

        return $this->updateASNDtl($ansDtlId, $status);
    }

    /**
     * @param $ansDtlId
     *
     * @return mixed
     */
    private function updateAsnDtlNew($ansDtlId)
    {
        $status = Status::getByKey('ASN_DTL_STS', 'NEW');

        return $this->updateASNDtl($ansDtlId, $status);
    }

    /**
     * @param $ansDtlId
     *
     * @return mixed
     */
    private function updateASNDtlReceiving($ansDtlId)
    {
        $status = Status::getByKey('ASN_DTL_STS', 'RECEIVING');

        return $this->updateASNDtl($ansDtlId, $status);
    }

    /**
     * @param $asnHdrId
     */
    private function updateASNReceiving($asnHdrId)
    {
        $this->asnHdrModel->updateWhere(
            [
                'asn_sts' => Status::getByKey('ASN_STATUS', 'RECEIVING')
            ],
            ['asn_hdr_id' => $asnHdrId]
        );
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    private function eventTracking($params)
    {
        $this->eventTrackingModel->refreshModel();

        return $this->eventTrackingModel->create([
            'whs_id'    => $params['whs_id'],
            'cus_id'    => $params['cus_id'],
            'owner'     => $params['owner'],
            'evt_code'  => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info'      => $params['info'],
        ]);
    }

    /**
     * @param $asnHdrId
     */
    private function updateASNNew($asnHdrId)
    {
        $this->asnHdrModel->updateWhere(
            [
                'asn_sts' => Status::getByKey('ASN_STATUS', 'NEW')
            ],
            ['asn_hdr_id' => $asnHdrId]
        );
    }

    /**
     * @param $ansDtlId
     * @param $status
     *
     * @return mixed
     */
    private function updateASNDtl($ansDtlId, $status)
    {
        return $this->asnDtlModel->updateWhere(
            [
                'asn_dtl_sts' => $status
            ],
            [
                'asn_dtl_id' => $ansDtlId
            ]
        );
    }

    /**
     * @param $asnHdrId
     */
    private function updateAsnNewByDeleteLastOne($asnHdrId)
    {
        $countAsnDtlNotNew = $this->asnDtlModel->countAsnDtlNotNew($asnHdrId);

        if (0 == $countAsnDtlNotNew) {
            $this->updateASNNew($asnHdrId);
        }
    }

    /**
     * @param $ctnRFID
     *
     * @return mixed
     */
    private function updateVtlCtnStsToADByRfid($ctnRFID)
    {
        return $this->vtlCtn->updateWhere(
            [
                'deleted'     => '1',
                'deleted_at'  => time(),
                'vtl_ctn_sts' => Status::getByValue('ADJUSTED', 'VIRTUAL-CARTON-STATUS')
            ],
            [
                'ctn_rfid' => $ctnRFID
            ]
        );
    }

    /**
     * @param $asnDtlId
     * @param $sts
     *
     * @return mixed
     */
    private function updateVtlCtnSumSts($asnDtlId, $sts)
    {
        return $this->vtlCtnSum->updateWhere(
            [
                'vtl_ctn_sum_sts' => $sts
            ],
            [
                'asn_dtl_id' => $asnDtlId
            ]
        );
    }


    /**
     * @param $whsId
     * @param $cusId
     * @param $asnId
     * @param $ctnrId
     * @param ListAllVirtualCartonTransformer $listAllVirtualCartonTransformer
     *
     * @return Response|void
     */
    public function listVirtualCarton(
        $whsId,
        $cusId,
        $asnId,
        $ctnrId,
        ListAllVirtualCartonTransformer $listAllVirtualCartonTransformer,
        Request $request
    ) {
        set_time_limit(0);
        // get data from HTTP
        $input = [
            'whsId'      => $whsId,
            'cusId'      => $cusId,
            'asn_hdr_id' => $asnId,
            'ctnr_id'    => $ctnrId,
        ];

        try {

            /*
             * start logs
             */
            $url = "/whs/$whsId/cus/$cusId/asns/$asnId/containers/$ctnrId/carton/list-virtual-carton";
            $owner = "";
            $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'LVC',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'List virtual cartons'
            ]);
            /*
             * end logs
             */


            $vtlCtn = $this->vtlCtn->listAllVirtualCarton($input, []);

            return $this->response->collection($vtlCtn, $listAllVirtualCartonTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param $whsId
     * @param $cusId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function setDamageCarton($whsId, $cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $params = [
            'whsId'    => $whsId,
            'cusId'    => $cusId,
            'ctn_rfid' => array_get($input, 'ctn_rfid', null)
        ];

        $owner = $transaction = "";
        $urlEndpoint = "/whs/$whsId/cus/$cusId/carton/set-damage-carton";
        $apiName = 'Set damage Carton';
        Log:: info($request, $whsId, [
            'evt_code'     => 'UPD',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $urlEndpoint,
            'message'      => $apiName
        ]);

        $ctnRfid = $params['ctn_rfid'];

        //validate carton RFID
        $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
        if (!$ctnRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $ctnRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //WMS2-3964
        $this->checkCusBelongWhs($whsId, $cusId);
        $isBelong = $this->vtlCtn->getFirstWhere([
            'cus_id'   => $cusId,
            'whs_id'   => $whsId,
            'ctn_rfid' => $ctnRfid
        ]);

        if (!$isBelong) {
            $msg = "Virtual carton doesn't exist";
            $isExist = $this->vtlCtn->getFirstWhere(['ctn_rfid' => $ctnRfid]);
            if ($isExist) {
                $msg = "Virtual carton doesn't belong to warehouse and customer";
            }
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //check this vtl ctn can delete
        $vtlCtnSts = $this->vtlCtn->getVtlCtnStatus($ctnRfid);

        if (empty($vtlCtnSts) || count($vtlCtnSts) == 0 || is_null($vtlCtnSts)) {
            $msg = sprintf("Can not find carton has RFID: %s .", $ctnRfid);

            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $vtlCtnSts = object_get($vtlCtnSts, 'vtl_ctn_sts', null);
        if ($vtlCtnSts == 'GC') {
            $data = [
                'data'    => null,
                'message' => "Can't set damage cartons which have been assigned to pallet!",
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            $this->vtlCtn->updateWhere(
                [
                    'is_damaged' => '1'
                ],
                [
                    'ctn_rfid' => $ctnRfid
                ]
            );

            $msg = sprintf("Set damage carton has RFID %s successfully!", $ctnRfid);

            return $this->response
                ->noContent()
                ->setContent([
                    'data'    => [],
                    'status'  => true,
                    'message' => $msg
                ])
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $urlEndpoint,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }


    /**
     * @param $whsId
     * @param $cusId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function deleteVirtualCarton($whsId, $cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $params = [
            'whsId'    => $whsId,
            'cusId'    => $cusId,
            'ctn_rfid' => array_get($input, 'ctn_rfid', null)
        ];

        $ctnRfid = $params['ctn_rfid'];

        //validate carton RFID
        $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
        if (!$ctnRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $ctnRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //check this vtl ctn can delete
        $vtlCtnSts = $this->vtlCtn->getVtlCtnStatus($ctnRfid);

        if (empty($vtlCtnSts) || count($vtlCtnSts) == 0 || is_null($vtlCtnSts)) {
            $msg = sprintf("Can not find carton has RFID: %s .", $ctnRfid);

            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
                'empty'   => true
            ];

            return new Response($data, 200, [], null);
        }

        //only delete vtl ctn with status NEW
        if ($vtlCtnSts->vtl_ctn_sts != Status::getByValue('NEW', 'VIRTUAL-CARTON-STATUS')) {
            $msg = sprintf("Can not delete carton has RFID: %s .", $ctnRfid);

            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {

            /*
             * start logs
             */
            $url = "/whs/$whsId/cus/$cusId/carton/delete-virtual-carton";
            $owner = $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'DVC',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Delete virtual cartons'
            ]);
            /*
             * end logs
             */

            DB::beginTransaction();

            //get vtl ctn sum by ctl ctn id
            $vtlCtn = $this->vtlCtn->getFirstWhere(['ctn_rfid' => $ctnRfid]);

            if (empty($vtlCtn) || count($vtlCtn) == 0 || is_null($vtlCtn)) {
                $msg = "Can not find virtual carton summary of this carton.";
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                // return new Response($data, 200, [], null);

                $msg = sprintf("Deleted virtual carton has RFID %s successfully!", $ctnRfid);

                return $this->response
                    ->noContent()
                    ->setContent([
                        'data'    => [],
                        'status'  => true,
                        'message' => $msg
                    ])
                    ->setStatusCode(IlluminateResponse::HTTP_OK);
            }

            $asnDtlId = $vtlCtn->asn_dtl_id;
            //update virtual carton sum from CO to AD
            $this->updateVtlCtnStsToADByRfid($ctnRfid);

            $countVtlCtn = $this->vtlCtn->countCtnByASNDtlIdNew($vtlCtn->asn_dtl_id);

            $status = '';
            if ($countVtlCtn == 0) {
                $status = Status::getByValue('NEW', 'VIRTUAL-CARTON-SUMMARY-STATUS');
                $this->updateASNDtlNew($asnDtlId);
            } else {
                $status = Status::getByValue('RECEIVING', 'VIRTUAL-CARTON-SUMMARY-STATUS');
                $this->updateASNDtlReceiving($asnDtlId);
            }

            //update ASN status is NEW if count ASN details with other statuses (Not new) is greater than 0
            $this->updateAsnNewByDeleteLastOne($vtlCtn->asn_hdr_id);

            $this->updateVtlCtnSumSts($asnDtlId, $status);

            DB::commit();

            $msg = sprintf("Deleted virtual carton has RFID %s successfully!", $ctnRfid);

            return $this->response
                ->noContent()
                ->setContent([
                    'data'    => [],
                    'status'  => true,
                    'message' => $msg
                ])
                ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response
                ->noContent()
                ->setContent(['data' => ['message' => $e->getMessage()]])
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        }
    }


    /**
     * Delete array vtl cartons
     *
     * @param $whsId
     * @param $cusId
     * @param Request $request
     *
     * @return Response
     */


    public function deleteVirtualCartons($whsId, $cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        /*
         * start logs
         */
        $url = "/whs/$whsId/cus/$cusId/carton/delete-virtual-cartons";
        $owner = $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'DVA',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Delete virtual array cartons'
        ]);
        /*
         * end logs
         */

        $ctnRfid = array_get($input, 'ctn_rfid', null);
        if (!is_array($ctnRfid)) {
            $ctnRfid = [$ctnRfid];
        }
        if (empty($ctnRfid)) {
            $msg = "Cartons RFID are required";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }
        // Check duplicate cartons rfid
        $uniqueRFIDs = array_unique($ctnRfid);
        if (count($uniqueRFIDs) != count($ctnRfid)) {
            $duplicateRFID = array_unique(array_diff_assoc($ctnRfid, $uniqueRFIDs));
            $msg = 'Duplicated Carton: ' . implode(', ', $duplicateRFID);
            $data = [
                'data'    => $duplicateRFID,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $params = [
            'whsId'    => $whsId,
            'cusId'    => $cusId,
            'ctn_rfid' => $ctnRfid
        ];

        //validate carton RFID
        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($ctnRfid as $item) {
            $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
            if (!$ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $item;
                if ('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }
        }
        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            $data = [
                'data'    => $ctnRfidInValid,
                'message' => $errorMsg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //check this vtl ctn can delete
        $vtlCtnObj = $this->vtlCtn->getAllVtlToDelete($whsId, $ctnRfid, $cusId);
        if (empty($vtlCtnObj->toArray())) {
            // $msg = "Cartons RFID don't exist.";
            $msg = null;
            //check this vtl ctn can delete
            $ctnRfidNotBelongWhsCus = [];
            foreach ($ctnRfid as $rfid) {
                $isBelongWhs = $this->vtlCtn->getFirstWhere(['ctn_rfid' => $rfid]);
                if ($isBelongWhs) {
                    $ctnRfidNotBelongWhsCus[] = $rfid;
                }
            }
            $data = $ctnRfid;
            if ($ctnRfidNotBelongWhsCus) {
                $msg = sprintf("Cartons RFID [%s] don't belong to customer and warehouse.", implode(', ', $ctnRfidNotBelongWhsCus));
                $data = $ctnRfidNotBelongWhsCus;
            }

            $ctnRfidWrong = $this->cartonModel->getModel()->whereIn('rfid', $ctnRfid)->pluck('rfid');
            if (count($ctnRfidWrong) > 0) {
                $msg = sprintf("Cannot delete a carton registered in inventory");
            }

            // $data = [
            //     'data'    => $data,
            //     'message' => $msg,
            //     'status'  => false,
            // ];

            // return new Response($data, 200, [], null);

            if ($msg) {
                $data = [
                    'data'    => $data,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }

        //check result of vtl carton and input rfid, if not equal, return carton is error
        if (count($vtlCtnObj->toArray()) != count($ctnRfid)) {
            $gotRFIDs = array_column($vtlCtnObj->toArray(), 'ctn_rfid');
            $arrWrongRFID = array_diff($ctnRfid, array_diff($ctnRfid, $gotRFIDs));
            $msg = null;
            $data = [
                'data'    => $arrWrongRFID,
                'message' => 'The cartons RFIDs do not exist or can not delete it.',
                'status'  => false,
            ];

            $ctnRfidNotBelongWhs = [];
            foreach ($arrWrongRFID as $rfid) {
                $isBelongWhs = $this->vtlCtn->getFirstWhere(['ctn_rfid' => $rfid]);
                if ($isBelongWhs) {
                    $ctnRfidNotBelongWhs[] = $rfid;
                }
            }
            if ($ctnRfidNotBelongWhs) {
                $msg = sprintf("Cartons RFID [%s] don't belong to customer and warehouse.", implode(', ', $ctnRfidNotBelongWhs));
                $data = [
                    'data'    => $ctnRfidNotBelongWhs,
                    'message' => $msg,
                    'status'  => false,
                ];
            }

            // return new Response($data, 200, [], null);
            if ($msg) {
                return new Response($data, 200, [], null);
            }
        }

        $arrDataCheck = array_column($vtlCtnObj->toArray(), 'vtl_ctn_sts', 'ctn_rfid');
        $arrVtlCtnCanNotDelete = [];

        foreach ($arrDataCheck as $key => $val) {
            if ('NW' != $val) {
                $arrVtlCtnCanNotDelete[] = $key;
            }
        }

        if ($arrVtlCtnCanNotDelete) {
//            $data = [
//                'data'    => $arrVtlCtnCanNotDelete,
//                'message' => sprintf('Just delete virtual cartons with status is NEW. Current status is %s',
//                    Status::getByKey('VIRTUAL-CARTON-STATUS', $arrDataCheck[$arrVtlCtnCanNotDelete[0]])),
//                'status'  => false,
//            ];
//
//            return new Response($data, 200, [], null);

            // Cartons have put away
            $putAwayCtn = $this->cartonModel->getPutAwayCtnByRfid($arrVtlCtnCanNotDelete);
            if($putAwayCtn){
                $data = [
                    'data'    => $putAwayCtn,
                    'message' => 'Can not delete carton had put away.',
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            // Check gr_sts = 'RE'
            $grHdrIdList = $this->cartonModel->getGrHdrIdListByCtn($arrVtlCtnCanNotDelete);
            $grHdrListByCtn = $this->grHdrModel->getGrHdrListByGrHdrIdList($grHdrIdList);
            if($grHdrListByCtn){
                $data = [
                    'data'    => $grHdrListByCtn,
                    'message' => 'Can not delete carton had complete GoodsReceipt.',
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }
        unset($arrDataCheck);

        try {
            DB::beginTransaction();

            if (!empty($arrVtlCtnCanNotDelete)) {

                $cartonsNotDeletes = $this->cartonModel->getModel()->whereIn('rfid', $arrVtlCtnCanNotDelete)->get();
                $grDtlIds = array_unique($cartonsNotDeletes->pluck('gr_dtl_id')->toArray());
                $grHdrIds = array_unique($cartonsNotDeletes->pluck('gr_hdr_id')->toArray());
                $pltIds = array_unique($cartonsNotDeletes->pluck('plt_id')->toArray());

                /*
                * Re-compute gr_dtl.gr_dtl_act_ctn_ttl, gr_dtl.gr_dtl_act_ctn_ttl...
                */
                $this->cartonModel->deleteCtnList($arrVtlCtnCanNotDelete);
                $this->grDtlModel->updateGrDtlWhenScanPallet($grDtlIds);
                /**
                 * Correct status
                 */
                foreach($grDtlIds as $grDtlId){
                    $this->grDtlModel->updateGoodsReceiptDetailReceived($grDtlId);
                }
                /**
                 * Update pallet: carton total, damage
                 */
                foreach ($pltIds as $pltId){
                    $this->pltModel->updatePalletCtnTtlAndDamaged($whsId, $grHdrIds, $pltId, 'null', [], []);
                }
            }

            $vtlCtnModel = $this->vtlCtn->deleteVtlCtns($whsId, $ctnRfid);

            if ($vtlCtnModel) {
                $tmpArr = $vtlCtnObj->toArray();
                $vtlCtn = array_shift($tmpArr);
                unset($tmpArr);

                $asnDtlId = $vtlCtn['asn_dtl_id'];
                $countVtlCtn = $this->vtlCtn->countCtnByASNDtlIdNew($asnDtlId);

                if ($countVtlCtn == 0) {
                    $status = Status::getByValue('NEW', 'VIRTUAL-CARTON-SUMMARY-STATUS');
                    $this->updateASNDtlNew($asnDtlId);
                } else {
                    $status = Status::getByValue('RECEIVING', 'VIRTUAL-CARTON-SUMMARY-STATUS');
                    $this->updateASNDtlReceiving($asnDtlId);
                }

                //update ASN status is NEW if count ASN details with other statuses (Not new) is greater than 0
                $this->updateAsnNewByDeleteLastOne($vtlCtn['asn_hdr_id']);

                $this->updateVtlCtnSumSts($asnDtlId, $status);

                DB::commit();
            }

            $data = [
                'data'    => $ctnRfid,
                'message' => 'Delete virtual cartons successfully.',
                'status'  => true,
            ];

            return new Response($data, 200, [], null);


        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response
                ->noContent()
                ->setContent(['data' => ['message' => $e->getMessage()]])
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        }
    }

    public function deleteVirtualCartonByASNDtl($whsId, $cusId, $asnDtlId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        /*
         * start logs
         */
        $url = "/whs/$whsId/cus/$cusId/asn-dtl/$asnDtlId/carton/delete-virtual-cartons";
        $owner = $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'DVA',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Delete virtual array cartons by ASN Detail Id'
        ]);
        /*
         * end logs
         */

        //Check asn detail is belong to customer and warehouse
        $this->checkCusBelongWhs($whsId, $cusId);
        $isBelong = $this->asnDtlModel->isBelongWhsAndCus($whsId, $cusId, $asnDtlId);
        if (!$isBelong) {
            $msg = sprintf("ASN detail %d is not existed.", $asnDtlId);
            //check exists ANS detail
            $asnDtlObj = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $asnDtlId]);
            if ($asnDtlObj) {
                $msg = "Asn detail doesn't belong to warehouse and customer";
            }
            $data = [
                'data'    => $asnDtlId,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //check this vtl ctn can delete
        $vtlCtnObj = $this->vtlCtn->getAllVtlToDeleteByASNDtlId($whsId, $cusId, $asnDtlId);
        if (empty($vtlCtnObj->toArray())) {
            $data = [
                'data'    => $asnDtlId,
                'message' => sprintf("There is no carton belong to ASN Detail %d.", $asnDtlId),
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $arrDataCheck = array_column($vtlCtnObj->toArray(), 'plt_rfid', 'ctn_rfid');
        $arrVtlCtnCanNotDelete = [];
        foreach ($arrDataCheck as $ctnRFID => $pltRFID) {
            if (null != $pltRFID) {
                $arrVtlCtnCanNotDelete[] = $ctnRFID;
            }
        }
        unset($arrDataCheck);

        if ($arrVtlCtnCanNotDelete) {
            $data = [
                'data'    => $arrVtlCtnCanNotDelete,
                'message' => sprintf('There are %s cartons RFID which can not delete.', count($arrVtlCtnCanNotDelete)),
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            DB::beginTransaction();

            $vtlCtnModel = $this->vtlCtn->deleteVtlCtnsByASNDtlId($whsId, $cusId, $asnDtlId);
            $arrDataReturn = array_pluck($vtlCtnObj, 'ctn_rfid');
            if ($vtlCtnModel) {
                $tmpArr = $vtlCtnObj->toArray();


                $vtlCtn = array_shift($tmpArr);
                unset($tmpArr);

                $asnDtlId = $vtlCtn['asn_dtl_id'];
                $countVtlCtn = $this->vtlCtn->countCtnByASNDtlIdNew($asnDtlId);

                if ($countVtlCtn == 0) {
                    $status = Status::getByValue('NEW', 'VIRTUAL-CARTON-SUMMARY-STATUS');
                    $this->updateASNDtlNew($asnDtlId);
                } else {
                    $status = Status::getByValue('RECEIVING', 'VIRTUAL-CARTON-SUMMARY-STATUS');
                    $this->updateASNDtlReceiving($asnDtlId);
                }

                //update ASN status is NEW if count ASN details with other statuses (Not new) is greater than 0
                $this->updateAsnNewByDeleteLastOne($vtlCtn['asn_hdr_id']);

                $this->updateVtlCtnSumSts($asnDtlId, $status);

                DB::commit();
                $data = [
                    'data'    => $arrDataReturn,
                    'message' => sprintf('Delete %s virtual cartons of asn detail %s successfully.',
                        count($arrDataReturn),
                        $asnDtlId),
                    'status'  => true,
                ];

                return new Response($data, 200, [], null);
            } else {
                $data = [
                    'data'    => $arrDataReturn,
                    'message' => sprintf('Delete virtual carton of asn detail %s failed.', $asnDtlId),
                    'status'  => false,
                ];
                DB::rollBack();

                return new Response($data, 200, [], null);
            }


        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response
                ->noContent()
                ->setContent(['data' => ['message' => $e->getMessage()]])
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        }
    }


    /**
     * @param $asndtllID
     * @param Request $request
     * @param AsnscanStatusTransformer $asnscanStatusTransformer
     *
     * @return Response|void
     */
    public function showByAsnDtl($asndtllID, Request $request, AsnscanStatusTransformer $asnscanStatusTransformer)
    {
        /*
            * start logs
            */
        $url = "/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asn/status/$asndtllID";
        $owner = $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'DVC',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Get ASN Detail'
        ]);
        /*
         * end logs
         */

        try {

            $asnDtl = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $asndtllID]);

            $asnDtl->rfid = $this->vtlCtn->findWhere(['asn_dtl_id' => $asndtllID], [], [], [
                'ctn_rfid',
                'vtl_ctn_id',
                'vtl_ctn_sts',
                'is_damaged'
            ]);

            foreach ($asnDtl->rfid as $key => $rfid) {
                $asnDtl->rfid[$key]['vtl_ctn_sts_name'] = Status::getByKey("VIRTUAL-CARTON-STATUS",
                    array_get($rfid, 'vtl_ctn_sts', null));
            }

            return $this->response->item($asnDtl, $asnscanStatusTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param $asnDtlId
     * @param Request $request
     *
     * @return Response
     */
    public function completeASNDetail($whsId, $cusId, $asnDtlId, Request $request)
    {

        $message = "Complete carton successfully !";
        $data = [
            'message'   => $message,
            'data'      => [
                'detail' => null,
            ],
            'status'    => true,
            'gr_status' => false,
        ];

        return new Response($data, 200, [], null);

        try {
            /*
             * start logs
             */
            $url = "/whs/$whsId/cus/$cusId/asn-detail/$asnDtlId/complete-sku";
            $owner = "";
            $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'CAD',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Complete ASN Detail'
            ]);
            /*
             * end logs
             */

            DB::beginTransaction();

            // Update discrepancy
            $asnDtlInfo = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $asnDtlId]);
            if (empty($asnDtlInfo) || count($asnDtlInfo) == 0) {
                $msg = Message::get("VR028", 'ASN Detail');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //check this ASN Detail is completed or not
            $isComplete = $this->vtlCtnSum->getFirstWhere(
                [
                    'asn_dtl_id'      => $asnDtlId,
                    'vtl_ctn_sum_sts' => 'RE'
                ]
            );
            if (!empty($isComplete)) {
                $msg = sprintf("The asn detail id %d is completed.", $asnDtlId);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
            $dtn_ctn_ttl = array_get($asnDtlInfo, 'asn_dtl_ctn_ttl', 0);
            $scan_vir_ctn = VirtualCarton::where('asn_dtl_id', $asnDtlId)->count();
            $disc = $scan_vir_ctn - $dtn_ctn_ttl;

            $this->vtlCtnSum->updateWhere([
                'vtl_ctn_sum_sts' => Status::getByValue('RECEIVED', 'VIRTUAL-CARTON-SUMMARY-STATUS'),
                'discrepancy'     => $disc
            ],
                ['asn_dtl_id' => $asnDtlId]
            );

            $this->updateAsnDtlReceived($asnDtlId);
            $asnDtlInfo['cus_id'] = $cusId;
            $asnDtlInfo['whs_id'] = $whsId;

            //check $asnDtlId created GR or note
            $createdGR = (new GoodsReceiptDetailModel())->getModel()->where('asn_dtl_id', $asnDtlId)->count();
            if (!$createdGR) {
                $data = $this->grHdrModel->createGRWhenScanAllCartons($asnDtlInfo);
            }

            $asnHdrId = array_get($asnDtlInfo, 'asn_hdr_id', '');
            $flag = $this->asnHdrModel->checkCompleted($asnHdrId);
            if ($flag) {
                $this->updateASNHdrCompleted($asnHdrId);
            }
            $message = "Complete carton successfully !";
            $detail = [
                'asn_hdr_id' => $asnHdrId,
            ];
            $statusGR = false;
            if (!empty($data)) {
                $message = "Complete carton and create goods receipt successfully !";
                $statusGR = true;
            }

            DB::commit();

            $data = [
                'message'   => $message,
                'data'      => [
                    'detail' => $detail,
                ],
                'status'    => true,
                'gr_status' => $statusGR,
            ];

            return new Response($data, 200, [], null);

        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param $asnDtlId
     * @param Request $request
     *
     * @return Response
     */
    public function completeASNDtl($whsId, $cusId, $asnDtlId, Request $request)
    {
        try {
            /*
             * start logs
             */
            $url = "/whs/$whsId/cus/$cusId/asn-detail/$asnDtlId/complete-sku";
            $owner = "";
            $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'CAD',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Complete ASN Detail'
            ]);
            /*
             * end logs
             */

            DB::beginTransaction();

            // Update discrepancy
            $asnDtlInfo = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $asnDtlId]);
            if (empty($asnDtlInfo) || count($asnDtlInfo) == 0) {
                $msg = Message::get("VR028", 'ASN Detail');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //check this ASN Detail is completed or not
            $isComplete = $this->vtlCtnSum->getFirstWhere(
                [
                    'asn_dtl_id'      => $asnDtlId,
                    'vtl_ctn_sum_sts' => 'RE'
                ]
            );
            if (!empty($isComplete)) {
                $msg = sprintf("The asn detail id %d is completed.", $asnDtlId);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
            $dtn_ctn_ttl = array_get($asnDtlInfo, 'asn_dtl_ctn_ttl', 0);
            $scan_vir_ctn = VirtualCarton::where('asn_dtl_id', $asnDtlId)->count();
            $disc = $scan_vir_ctn - $dtn_ctn_ttl;

            $this->vtlCtnSum->updateWhere([
                'vtl_ctn_sum_sts' => Status::getByValue('RECEIVED', 'VIRTUAL-CARTON-SUMMARY-STATUS'),
                'discrepancy'     => $disc
            ],
                ['asn_dtl_id' => $asnDtlId]
            );

            $this->updateAsnDtlReceived($asnDtlId);
            $asnDtlInfo['cus_id'] = $cusId;
            $asnDtlInfo['whs_id'] = $whsId;

            //check $asnDtlId created GR or note
            $createdGR = (new GoodsReceiptDetailModel())->getModel()->where('asn_dtl_id', $asnDtlId)->count();
            if (!$createdGR) {
                $data = $this->grHdrModel->createGRWhenScanAllCartons($asnDtlInfo);
            }

            $asnHdrId = array_get($asnDtlInfo, 'asn_hdr_id', '');
            $flag = $this->asnHdrModel->checkCompleted($asnHdrId);
            if ($flag) {
                $this->updateASNHdrCompleted($asnHdrId);
            }
            $message = "Complete carton successfully !";
            $detail = [
                'asn_hdr_id' => $asnHdrId,
            ];
            $statusGR = false;
            if (!empty($data)) {
                $message = "Complete carton and create goods receipt successfully !";
                $statusGR = true;
            }

            DB::commit();

            $data = [
                'message'   => $message,
                'data'      => [
                    'detail' => $detail,
                ],
                'status'    => true,
                'gr_status' => $statusGR,
            ];

            return new Response($data, 200, [], null);

        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    private function updateASNHdrCompleted($asnHdrId)
    {
        return $this->asnHdrModel->updateWhere(
            [
                'asn_sts' => "RE"
            ],
            [
                'asn_hdr_id' => $asnHdrId
            ]
        );
    }

    /**
     * @param $whsId
     * @param $ctnRfid
     *
     * @return Response|void
     */
    public function getVirtualCtnRfid(Request $request, $whsId, $ctnRfid)
    {
        try {

            /*
            * start logs
            */
            $url = "/$whsId/get-virtual-ctn/$ctnRfid";
            $owner = $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'VCR',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Get Virtual Carton Reference Id'
            ]);
            /*
             * end logs
             */

            $data = [];
            $vtlCtn = $this->vtlCtn->getFirstBy('ctn_rfid', $ctnRfid);
            if (empty($vtlCtn)) {
                $msg = sprintf("There is no virtual carton %s.", $ctnRfid);
                throw new \Exception($msg);
            }
            $data['data'] = $this->formatArr($vtlCtn);

            return new Response($data, 200, [], null);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * Check customer is belong to warehouse
     *
     * @param integer $whsId
     * @param integer $cusId
     *
     * @throws \Exception
     */
    private function checkCusBelongWhs($whsId, $cusId)
    {
        $cusWhsModel = (new CustomerWarehouse())->where([
            'whs_id' => $whsId,
            'cus_id' => $cusId
        ])->first();
        if (!$cusWhsModel) {
            //throw new \Exception('Customer is not belong to warehouse');
            $msg = sprintf('Customer is not belong to warehouse.');
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);

        }
    }
}
