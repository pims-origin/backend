<?php

namespace App\Modules\Orders\Validators;

use Validator;

class UpdateValidator
{
    public function rules()
    {
        return [
            'order_type'        => 'required|in:bulk,retail,ecom',
            'order_num'         => 'required',
            'po_num'            => 'required',
            'carrier.carrier'   => 'required',
            'order_date'        => 'required',
            'ship_date'         => 'required',
            'items.*.sku'       => 'required',
            'items.*.qty'       => 'required|numeric|min:1',
            'contacts.*.name'   => 'required',
            'addresses.*.state' => 'required',
            'addresses.*.zip'   => 'required'
        ];
    }

    public function validate($input)
    {
        $input['order_type'] = strtolower(array_get($input, 'order_type'));
        $validator = Validator::make($input, $this->rules(), $this->messages());

        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                }
            }
        }

        return $validator;
    }

    public function messages() {
        return [
            'order_type.required'           => 'Order type is required',
            'order_type.in'                 => 'Order type is not supported',
            'order_num.required'            => 'Order number is required',
            'po_num.required'               => 'PO number is required',
            'carrier.required'              => 'Carrier is required',
            'order_date.required'           => 'Order date is required',
            'ship_date.required'            => 'Ship date is required',
            'items.*.sku.required'          => 'Sku is required',
            'items.*.qty.required'          => 'Quantity is required',
            'contacts.*.name.required'      => 'Contact name is required',
            'addresses.*.state.required'    => 'Shipping address is required',
            'addresses.*.zip.required'      => 'Shipping zip code is required'
        ];
    }
}