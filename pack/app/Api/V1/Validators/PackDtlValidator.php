<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class PackHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class PackDtlValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'pack_hdr_id'         => 'required|exists:pack_hdr,pack_hdr_id',
            'items'               => 'required|array',
            'items.*.pack_dtl_id' => 'required|integer|exists:pack_dtl,pack_dtl_id',
            'items.*.itm_id'     => 'required|integer|exists:item,item_id',
            'items.*.piece_qty'   => 'integer',
        ];
    }


}
