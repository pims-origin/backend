<?php

namespace App\Http\Middleware;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        try {
            $key = env("JWT_SECRET");
            $auth = $request->header('Authorization');
            $jwt = str_replace("Bearer ", "", $auth);
            $pub_key = openssl_pkey_get_private(FIREBASE_PRIVATE_KEY);
            $keyData = openssl_pkey_get_details($pub_key);
            $decoded = JWT::decode($jwt, $keyData['key'], array('RS256'));
            $userId = $decoded->sub;
            Config::set('data.userId', $userId);
        }
        catch (\Exception $e) {
            throw new \Exception('Unauthorized');
        }
        catch (TokenExpiredException $e) {
            throw new UnauthorizedHttpException('Bearer', 'Token Expired');
        } catch (JWTException $e) {
            throw new UnauthorizedHttpException('Bearer', 'Token Invalid');
        }
        $response = $next($request);
        return $response;
    }
}
