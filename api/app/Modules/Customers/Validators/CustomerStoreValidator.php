<?php

namespace App\Modules\Customers\Validators;

use Validator;

class CustomerStoreValidator
{
    public function rules()
    {
        return [
            'cus_status'            => 'required|max:2|in:AC,IA',
            'cus_code'              => 'required|max:3',
            'cus_name'              => 'required',
            'cus_billing_account'   => 'required',
            'cus_country_id'        => 'required|integer',

            'addresses.*.cus_add_line_1'      =>  'required',
            'addresses.*.cus_add_country_id'  =>  'required',
            'addresses.*.cus_add_type'        =>  'required',
            'addresses.*.cus_add_city_name'   =>  'required',
            'addresses.*.cus_add_state_id'    =>  'required|integer',
            'addresses.*.cus_add_postal_code' =>  'required',

            'contacts.*.cus_ctt_fname'        =>  'required',
            'contacts.*.cus_ctt_lname'        =>  'required',
            'contacts.*.cus_ctt_email'        =>  'required|email',
            'contacts.*.cus_ctt_phone'        =>  'required'
        ];
    }

    public function validate($input)
    {
        $validator = Validator::make($input, $this->rules());

        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                }
            }
        }

        return $validator;
    }
}