<?php

namespace App\Library;

use Exception;
use Illuminate\Http\Request as IRequest;
use Seldat\Wms2\Models\Location;
use App\Api\V1\Validators\LocationImportValidator;
use App\Api\V1\Models\WarehouseZoneModel;
use App\Api\V1\Models\LocationTypeModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\ZoneLocationStatusModel;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Wms2\UserInfo\Data;

class LocationImport
{
    protected $_hash;

    protected $whsId;

    /**
     * @var LocationImportValidator
     */
    protected $locationImportValidator;

    /**
     * @var LocationTypeModel
     */
    protected $locationTypeModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var WarehouseZoneModel
     */
    protected $warehouseZoneModel;

    /**
     * @var ZoneLocationStatusModel
     */
    protected $locationStatusModel;

    protected $rfids = [];

    protected $_header = [
        'Location Code',
        //'Alternative Name',
        'Type',
        'Length',
        'Width',
        'Height',
        'Capacity%',
        'Weight Capacity',
        'Storage Type',
        //'Min Count',
        'Max Pallet',
        'Zone Code',
        'Status',
        'Rfid',
        'Aisle',
        'Section',
        'Level',
        'Bin',
        //'Area'
        'Shipping Area',
        'Special Handling'
    ];

    protected $_fields = [
        0  => "Location Code",
        //1  => "Alternative Name",
        1  => "Type",
        2  => "Length",
        3  => "Width",
        4  => "Height",
        9  => "Zone Code",
        10  => "Status",
        11 => "Rfid"
    ];

    protected $required = [
        0  => "Location Code",
        //1  => "Alternative Name",
        1  => "Type",
        2  => "Length",
        3  => "Width",
        4  => "Height",
        10  => "Status",
        12 => "Aisle",
        13 => "Section",
        14 => "Level",
        15 => "Bin",
        17 => "Special Handling"
    ];

    protected $integers = [
        5  => "Capacity",
        //8 => "Min Count",
        8  => "Max Pallet",
        15 => "Bin",
        16 => "Shipping Area"

    ];

    protected $locName = [
        0 => "Location Code",
        //1 => "Alternative Name",
    ];

    protected $noZero = [
        2 => "Length",
        3 => "Width",
        4 => "Height",
        6 => "Weight Capacity",
    ];

    protected $_duplicate = [
        0 => 'loc_code',
        //1 => 'loc_alternative_name'
    ];

    protected $_unique = [
        12 => 'aisle',
        13 => 'row',
        14 => 'level',
        15 => 'bin'
    ];

    protected $_notExisted = [
        1 => 'loc_type_code',
        9 => 'zone_code',
        10 => "status_code"
    ];
    protected $_enum = [
        //15 => [null, 1, 2]
        14 => [null, 1, 2]
    ];


    /**
     * LocationImport constructor.
     *
     * @param $hash
     * @param LocationImportValidator $locationImportValidator
     * @param LocationModel $locationModel
     * @param LocationTypeModel $locationTypeModel
     * @param WarehouseZoneModel $warehouseZoneModel
     * @param ZoneLocationStatusModel $locationStatusModel
     */
    public function __construct(
        $hash,
        LocationImportValidator $locationImportValidator,
        LocationModel $locationModel,
        LocationTypeModel $locationTypeModel,
        WarehouseZoneModel $warehouseZoneModel,
        ZoneLocationStatusModel $locationStatusModel
    ) {
        $this->_hash = $hash;
        $this->locationImportValidator = $locationImportValidator;
        $this->locationTypeModel = $locationTypeModel;
        $this->warehouseZoneModel = $warehouseZoneModel;
        $this->locationStatusModel = $locationStatusModel;
        $this->locationModel = $locationModel;
    }

    /**
     * @param IRequest $request
     *
     * @return array
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    public function validate(IRequest $request)
    {

        set_time_limit(0);
        $input = $request->all();

        //----------psx
        //$input['warehouse_id'] = 1;
        //$input['file_type'] = 'xlsx';
        //----------/psx

        $this->whsId = $input['warehouse_id'];
        $this->locationImportValidator->validate($input);

        $locations = $this->locationModel->fetchColumns(
            ['loc_code', 'loc_alternative_name', 'rfid'],
            "deleted = 0 AND loc_whs_id = :whs_id",
            ['whs_id' => $input['warehouse_id']],
            true
        );

        $unique = $this->locationModel->fetchColumns(
            ['aisle', 'row', 'level', 'bin'],
            "deleted = 0 AND loc_whs_id = :whs_id",
            ['whs_id' => $input['warehouse_id']],
            true
        );

        $this->rfids = $this->locationModel->fetchColumns(
            ['rfid'],
            "deleted = 0",
            [],
            true
        );
        $this->rfids = array_filter($this->rfids['rfid']);

        $existedChk = [
            'loc_type_code' => $this->locationTypeModel->fetchColumns(
                ['loc_type_code', 'loc_type_id'],
                'deleted = 0', [],
                true,
                true

            ),
            'zone_code'     => $this->warehouseZoneModel->fetchColumns(
                ['zone_code', 'zone_id'],
                'deleted = 0 AND zone_whs_id = :whs_id',
                ['whs_id' => $input['warehouse_id']],
                true,
                true
            ),
            'status_code'   => $this->locationStatusModel->fetchColumns(
                ['loc_sts_name', 'loc_sts_code'], '', [], true, true
            )
        ];
        //---------------------------------------------------------------------

        // Validate input file
        $errors = [];
        $valids = [];
        $error = false;
        $empty = true;

        // Check file not empty
        $file = $input['file'];
        if (empty($input['file_type'])) {
            return ['status' => false, 'data' => ['code' => 400, 'message' => Message::get("VR029", "File Type")]];
        }

        // Check valid File type
        $fileType = $input['file_type'];
        if (!in_array($fileType, ['xlsx', 'ods', 'csv'])) {
            return [
                'status' => false,
                'data'   => ['code' => 400, 'message' => Message::get("VR005", "(.xlsx|.ods|.csv)")]
            ];
        }

        $file->move(storage_path(), $this->_hash . ".$fileType");

        $filePath = storage_path($this->_hash . ".$fileType");
        $reader = ReaderFactory::create($fileType);
        $reader->open($filePath);

        $totalLines = 0;
        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $index => $row) {
                $row = [
                    0  => !isset($row[0]) ? "" : $row[0],
                    1  => !isset($row[1]) ? "" : $row[1],
                    2  => !isset($row[2]) ? "" : $row[2],
                    3  => !isset($row[3]) ? "" : $row[3],
                    4  => !isset($row[4]) ? "" : $row[4],
                    5  => !isset($row[5]) ? "" : $row[5],
                    6  => !isset($row[6]) ? "" : $row[6],
                    7  => !isset($row[7]) ? "" : $row[7],
                    8  => !isset($row[8]) ? "" : $row[8],
                    9  => !isset($row[9]) ? "" : $row[9],
                    10 => !isset($row[10]) ? "" : $row[10],
                    11 => !isset($row[11]) ? "" : $row[11],
                    12 => !isset($row[12]) ? "" : $row[12],
                    13 => !isset($row[13]) ? "" : $row[13],
                    14 => !isset($row[14]) ? "" : $row[14],
                    15 => !isset($row[15]) ? "" : $row[15],
                    16 => !isset($row[16]) ? "" : $row[16],
                    17 => !isset($row[17]) ? "" : $row[17]
                ];

                $totalLines++;
                if ($totalLines > 10001) {
                    return [
                        'status' => false,
                        'data'   => ['code' => 400, 'message' => "Number of location exceeds 10.000"]
                    ];
                }
                $empty = false;
                if ($index === 1) {
                    if (array_diff($this->_header, $row)) {
                        $reader->close();
                        unlink($filePath);

                        return ['status' => false, 'data' => ['code' => 400, 'message' => Message::get("BZ005")]];
                    }

                    array_push($row, "Import Status", "Error Detail");
                    $errors[] = $row;
                    continue;
                }

                if (array_filter($row)) {

                    // Check Valid
                    $errorDetail = $this->checkValidRow($row, $locations, $existedChk, $unique, $this->whsId);

                    $valid = $row;
                    $valid[] = $input['warehouse_id'];
                    $valid[1] = array_get($existedChk, "loc_type_code." . trim(strtoupper($row[1])), null);
                    $valid[9] = array_get($existedChk, "zone_code." . trim(strtoupper($row[9])), null);
                    $valid[10] = array_get($existedChk, "status_code." . trim(strtoupper($row[10])), null);
                    $valids[] = $valid;

                    // Put error
                    if ($errorDetail) {
                        $error = true;
                        $row[] = "Failed";
                    } else {
                        $row[] = "Successful";
                    }

                    $errorDetail = trim($errorDetail);
                    $row[] = $errorDetail;
                    $errors[] = $row;
                }
            }
            // Only process one sheet
            break;
        }

        if ($empty || $totalLines <= 1) {
            return ['status' => false, 'data' => ['code' => 400, 'message' => Message::get("VR028", "File")]];
        }

        $reader->close();
        unlink($filePath);

        $return = [
            'status' => !$error,
            'data'   => [
                'code'       => $error ? 200 : 201,
                'message'    => 'The Location File is ' . ($error ? "Invalid" : "Valid"),
                'total_rows' => (count($valids))
            ]
        ];

        if ($error) {
            $result = $this->writeExcel(storage_path("Errors"), $errors, $return);

            return $result;
        } else {
            $this->writeExcel(storage_path("valid-import"), $valids, $return);
            $status = $this->processImport();

            return ['status' => true, 'data' => $status];
        }
    }

    /**
     * @param $row
     * @param $locations
     * @param array $existedChk
     * @param $unique
     * @param $whsId
     *
     * @return string
     */
    private function checkValidRow(&$row, &$locations, $existedChk = [], &$unique, $whsId)
    {
        $errorDetail = "";

        // Check Required
        foreach ($this->required as $key => $name) {
            if (!isset($row[$key]) || $row[$key] === "" || $row[$key] === null) {
                $errorDetail .= "$name is required.\n";
            }
        }

        // Check Not Zero
        foreach ($this->noZero as $key => $name) {
            if ((!is_numeric($row[$key]) && $row[$key] !== "") || (is_numeric($row[$key]) && $row[$key] <= 0)) {
                $errorDetail .= Message::get("VR012", $name, "0") . PHP_EOL;
            }
        }

        // Check Data Type
        foreach ($this->integers as $key => $name) {
            if (empty($row[$key])) {
                continue;
            }
            if (!is_numeric($row[$key]) || $row[$key] < 0) {
                $errorDetail .= Message::get("VR014", $name, "0") . PHP_EOL;
            }
        }

        // Check Location Code & Name
        //foreach ($this->locName as $key => $name) {
        //    $locCode = $this->isLocCode($row[$key]);
        //    if (!$locCode) {
        //        $errorDetail .= "The {$this->_fields[$key]} is invalid!.\n";
        //    }
        //    $row[$key] = $locCode;
        //}

        // Check Max Min
        //if (is_numeric($row[8]) && is_numeric($row[9]) && $row[8] >= $row[9]) {
        //    $errorDetail .= "Max Count should be greater than Min Count.\n";
        //}
        /*if (!is_numeric($row[8])) {
            $errorDetail .= "Max Count should be number.\n";
        }*/
        /*if(strtoupper($row[7]) == strtoupper('Floor') && $row[8] <= 1){
            $errorDetail .= "Max pallet should be greater than to 1";
        }
        if(strtoupper($row[7]) == strtoupper('Rack') && $row[8] > 1){
            $errorDetail .= "Max pallet should be equal to 1";
        }*/
        //check max lenght =3 for Aisle, Section
        if (strlen($row[12]) > 10) {
            $errorDetail .= "Max length of Aisle should be less than or equal to 10.\n";
        }
        if (strlen($row[13]) > 3) {
            $errorDetail .= "Max length of Section should be less than or equal to 3.\n";
        }
        if (strlen($row[16]) > 3) {
            $errorDetail .= "Shipping Area should be less than or equal to 999.\n";
        }
        if ($row[16] > 2) {
            $errorDetail .= "Shipping Area should be less than or equal to 2.\n";
        }
        if (strlen($row[15]) > 1) {
            $errorDetail .= "Bin should be less than or equal to 9.\n";
        }
        if (strlen($row[14]) > 1) {
            $errorDetail .= "Level should be less than or equal to 9.\n";
        }

        if (!in_array($row[17], ['RAC','SHE', 'BIN', 'FRE'])) {
            $errorDetail .= "Special Handling should be FRE or RAC or SHE or BIN.\n";
        }
        if (in_array($row[17], ['SHE', 'BIN']) && $row[7] == "Floor") {
            $errorDetail .= "Special Handling SHE or BIN should be not Floor.\n";
        }

        $isRfid = $this->isRfid($row[11]);
        if (!empty($row[11])) {
            if (!$isRfid) {
                $errorDetail .= "The Rfid is invalid!.\n";
            } else {
                if (in_array($row[11], $this->rfids)) {
                    $errorDetail .= "The Rfid is duplicated!.\n";
                } else {
                    $this->rfids[] = $row[11];
                }
            }
        }

        // Check Duplicate loc_code
        //foreach ($this->_duplicate as $key => $name) {
        //    $val = trim(strtoupper($row[$key]));
        //    if (in_array($val, $locations[$name])) {
        //        $errorDetail .= "The {$this->_fields[$key]} is duplicated!.\n";
        //    } else {
        //        $locations[$name][] = $val;
        //    }
        //}

        $unique_info = $this->locationModel->checkUniqueAisleSectionLevelBin($whsId, $row[12], $row[13], $row[14],
            $row[15]);
        if ($unique_info) {
            $errorDetail .= "The Aisle-Section-Level-Bin is duplicated!.\n";
        }

        // Check Not Exist
        foreach ($this->_notExisted as $key => $name) {
            if (in_array($key, [1, 10]) && empty($row[$key])) {
                continue;
            }

            if ($key == 9 && empty($row[$key])) {
                continue;
            }

            $val = trim(strtoupper($row[$key]));
            if (!isset($existedChk[$name][$val])) {
                $errorDetail .= "The {$this->_fields[$key]} is not existed.\n";
            } else {
                $row[$key] = $val;
            }
        }

        // Check Enum
        foreach ($this->_enum as $key => $enumValues) {
            if (!in_array($key, [15])) {
                continue;
            }

            $val = trim(strtoupper($row[$key]));
            $enumValues = is_array($enumValues) ? $enumValues : [$enumValues];
            if (!in_array($row[$key], $enumValues)) {
                $errorDetail .= "The {$this->_header[$key]} is not in accept values.\n";
            } else {
                $row[$key] = $val;
            }
        }

        return $errorDetail;
    }

    /**
     * @param $locCode
     *
     * @return bool
     */
    private function isLocCode($locCode)
    {
        $locCode = trim($locCode);
        $locCode = str_replace("_", "-", $locCode);
        $locCode = str_replace(" ", "", $locCode);
        $locCode = strtoupper($locCode);
        $locCodes = explode("-", $locCode);
        $locCodes = array_filter($locCodes);
        $count = count($locCodes);
        if ($count < 4 || $count > 5) {
            return false;
        }

        foreach ($locCodes as $locItem) {
            if (preg_match("/^[a-zA-Z0-9]+$/", $locItem) != 1) {
                return false;
            }

            if (strlen($locItem) > 9) {
                return false;
            }
        }

        return $locCode;
    }

    /**
     * @param $rfid
     *
     * @return bool
     */
    private function isRfid(&$rfid)
    {
        // Valid Rfid: DDDDDDDD0001123456789101
        // DDDDDDDD         8 chars
        // 0001             4 chars
        // 123456789101     12 chars
        $rfid = trim(strtoupper($rfid));
        $rfid = str_replace(" ", "", $rfid);
        $rfid = str_replace(".", "", $rfid);
        $rfid = str_replace(",", "", $rfid);

        if (strlen($rfid) != 24) {
            return false;
        }

        $rfFirst = substr($rfid, 0, 8);
        $rfMiddle = substr($rfid, 8, 4);
        $rfLast = substr($rfid, 12, 12);

        // First
        if ($rfFirst !== "DDDDDDDD") {
            return false;
        }

        // Middle
        if (!is_numeric($rfMiddle)) {
            return false;
        }

        if (empty((int)$rfMiddle) || (int)$rfMiddle != $this->whsId) {
            return false;
        }

        // Check Last must number
        if (!is_numeric($rfLast)) {
            return false;
        }

        return true;
    }

    /**
     * @param $dir
     * @param $data
     * @param $return
     *
     * @return mixed
     */
    private function writeExcel($dir, $data, $return)
    {
        $hashKey = $this->_hash;

        Excel::create($hashKey, function ($file) use ($data) {

            $file->sheet('Report', function ($sheet) use ($data) {
                $sheet->setAutoSize(true);
                $sheet->setHeight(1, 30);

                $countData = count($data);
                // Fill suggest Data
                $sheet->fromArray($data, null, 'A1', true, false);

                // Wrap Text
                $sheet->getStyle("N1:N" . (count($data)))->getAlignment()->setWrapText(true);

                $sheet->setBorder('A1:S' . (count($data)), 'thin');
                $sheet->cell("A1:S1", function ($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight();
                    $cell->setBackground("#3399ff");
                    $cell->setFontColor("#ffffff");
                });

                $sheet->cell("A1:N" . (count($data)), function ($cell) {
                    $cell->setValignment('center');
                });

                $columsAlign = [
                    "A1:C" . $countData => "left",
                    "D1:J" . $countData => "center",
                    "L1:M" . $countData => "center",
                    "K1:K" . $countData => "left",
                    "N1:N" . $countData => "left"
                ];

                foreach ($columsAlign as $cols => $align) {
                    $sheet->cell($cols, function ($cell) use ($align) {
                        $cell->setAlignment($align);
                    });
                }

            });
        })->store('xlsx', $dir);

        return $return;
    }

    /**
     * @return array
     * @throws Exception
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    public function processImport()
    {
        set_time_limit(0);
        $hashKey = $this->_hash;
        $type = Type::XLSX;
        $filePath = storage_path('valid-import') . "/$hashKey.$type";
        $reader = ReaderFactory::create($type);
        $reader->open($filePath);

        $fieldsName = [
            'loc_code',
            //'loc_alternative_name',
            'loc_type_id',
            'loc_length',
            'loc_width',
            'loc_height',
            'loc_available_capacity',
            'loc_weight_capacity',
            'storage_type',
            //'loc_min_count',
            'loc_max_count',
            'loc_zone_id',
            'loc_sts_code',
            'rfid',
            'aisle',
            'section',
            'level',
            'bin',
            'area',
            'spc_hdl_code',
            'loc_whs_id'
        ];

        // auto add zone for location with type = XDK or SHP
        $autoAddZone = DB::table('loc_type')
            ->whereIn('loc_type_code', ['XDK', 'SHP'])
            ->where('deleted', 0)->pluck('loc_type_id', 'loc_type_code');

        $zoneAuto = DB::table('zone')
            ->select([
                DB::raw("IF(zone_code = 'XDK-D', 'XDK', 'SHP') AS zone_code"),
                'zone_id'
            ])
            ->whereIn('zone_code', ['XDK-D', 'SHP'])
            ->where('zone_whs_id', $this->whsId)
            ->where('deleted', 0)->pluck('zone_id', 'zone_code');

        $result = [];
        $i = 0;

        try {
            // Begin Transaction
            DB::beginTransaction();

            $deleted_at = getDefaultDatetimeDeletedAt();

            $arrVirtualLocations = [];
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $i++;

                    foreach ($fieldsName as $idx => $fieldName) {
                        if($fieldName == 'storage_type'){
                            continue;
                        }

                        if($fieldName == 'spc_hdl_code') {

                            $max_LPN = null;
                            if (strtoupper($row[$idx]) == "RAC") {
                                $max_LPN = null;
                            } else if (strtoupper($row[$idx]) == "SHE") {
                                $max_LPN = 25;
                            } else if (strtoupper($row[$idx]) == "BIN") {
                                $max_LPN = 50;
                            } else if (strtoupper($row[$idx]) == "FRE") {
                                $max_LPN = 50;
                            }

                            $rs["max_lpn"] = $max_LPN;
                        }
                        $rs[$fieldName] = !empty($row[$idx]) ? $row[$idx] : null;
                    }

                    if (!empty($rs['loc_type_id'])) {
                        foreach ($autoAddZone as $type => $id) {
                            if ($rs['loc_type_id'] == $id) {
                                $rs['loc_zone_id'] = !empty($zoneAuto[$type]) ? $zoneAuto[$type] : null;
                                break;
                            }
                        }
                    }
                    $rs['row'] = $rs['section'];
                    unset($rs['section']);
                    $rs['created_by'] = JWTUtil::getPayloadValue('jti') ?? 0;
                    $rs['updated_by'] = JWTUtil::getPayloadValue('jti') ?? 0;
                    $rs['created_at'] = $rs['updated_at'] = time();
                    $rs['deleted_at'] = $deleted_at;
                    $rs['deleted'] = 0;
                    $rs['plt_lv'] = 1;
                    if (empty($rs['loc_max_count'])) {
                        $rs['loc_max_count'] = 1;
                    }
                    $rs['loc_alternative_name'] = $rs['loc_code'];

                    // WMS2-6343
                    //if(strtoupper($row[7]) == strtoupper('Floor')){
                        $rs['is_block_stack'] = 1;
                        $arrVirtualLocations[] = $rs;
                    //}
                    //else{
                        //$rs['is_block_stack'] = 0;
                    //}
                    $result[] = $rs;

                    if ($i >= 500) {
                        Location::insert($result);
                        $this->addLocationToZone($result);
                        $i = 0;
                        $result = [];
                    }
                }

                // Only process one sheet
                break;
            }
            $reader->close();

            if (!empty($result)) {
                Location::insert($result);
                $this->addLocationToZone($result);
            }

            // WMS2-6343
            $i = 0;
            $result = [];
            $predis = new Data();
            $userInfo = $predis->getUserInfo();
            $this->user_id = array_get($userInfo, 'user_id', 0);
            $listCusInUser = DB::table("cus_in_user")->where('user_id', $this->user_id)->where('whs_id', $this->whsId)->get();

            foreach($arrVirtualLocations as $virtualLoc){
                // Get parent loc_id
                $parentLoc = $this->locationModel->getFirstWhere(['loc_code' => $virtualLoc['loc_code'], 'loc_whs_id' => $this->whsId]);
                $keyCus = 0;
                for($j = 2; $j <= $virtualLoc['loc_max_count']; $j++){
                    $temp = $virtualLoc;
                    $temp['loc_code'] = $virtualLoc['loc_code'] . '-' . $j;
                    $temp['loc_alternative_name'] = $virtualLoc['loc_alternative_name'] . '-' . $j;
                    $temp['loc_max_count'] = 1;
                    $temp['bin'] = $j;
                    $temp['plt_lv'] = $j;
                    $temp['parent_id'] = $parentLoc->loc_id;
                    $temp['loc_cus_id'] = $listCusInUser[$keyCus]['cus_id'];
                    $result[] = $temp;
                    $i++;
                    $keyCus++;
                }
                if ($i >= 400) {
                    Location::insert($result);
                    $this->addLocationToZone($result);
                    $i = 0;
                    $result = [];
                }
            }

            if (!empty($result)) {
                Location::insert($result);
                $this->addLocationToZone($result);
            }

            // Commit transaction
            DB::commit();

        } catch (\Illuminate\Database\QueryException $ex) {
            if ($ex->getCode() === '23000') {
                throw new Exception('Duplicate locations', 404);
            } else {
                throw $ex;
            }

            // Rollback transaction
            DB::rollBack();
        }

        // Remove import csv file
        //unlink($filePath);

        return ['code' => 201, 'status' => true, 'message' => 'The Location file was imported successfully!'];
    }

    private function addLocationToZone($locations)
    {
        $param = [];
        if (!empty($locations)) {
            foreach ($locations as $location) {
                if (!empty($location['loc_zone_id'])) {
                    $zone_id = $location['loc_zone_id'];
                    $sts_code = $location['loc_sts_code'];
                    $param[$zone_id]['zone_id'] = $zone_id;

                    if (empty($param[$zone_id]['zone_num_of_loc'])) {
                        $param[$zone_id]['zone_num_of_loc'] = 0;
                    }
                    if (empty($param[$zone_id]['zone_num_of_active_loc'])) {
                        $param[$zone_id]['zone_num_of_active_loc'] = 0;
                    }
                    if (empty($param[$zone_id]['zone_num_of_inactive_loc'])) {
                        $param[$zone_id]['zone_num_of_inactive_loc'] = 0;
                    }

                    $param[$zone_id]['zone_num_of_loc'] += 1;
                    $param[$zone_id]['zone_num_of_active_loc'] += ($sts_code == config("constants.loc_status.ACTIVE"));
                    $param[$zone_id]['zone_num_of_inactive_loc'] +=
                        ($sts_code == config("constants.loc_status.INACTIVE"));
                }
            }
        }

        if (!empty($param)) {
            foreach ($param as $item) {
                $this->warehouseZoneModel->updateNumLoc($item);
            }
        }
    }
}

