<?php

namespace App\Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => OrderResource::collection($this->collection)
        ];
    }
}