<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\XferDtl;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class XferDtlModel extends AbstractModel
{

    /**
     * XferDtlModel constructor.
     *
     * @param XferDtl|null $model
     */
    public function __construct(XferDtl $model = null)
    {
        $this->model = ($model) ?: new XferDtl();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchAllXfer($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['sku', 'item_id', 'xfer_hdr_num', 'cus_upc', 'xfer_hdr_sts', 'cus_id'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }
        $query->where('whs_id', $currentWH);
        // Get
        $models = $query->paginate($limit);
        return $models;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function loadXferDetail($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query = $query->where('xfer_hdr_id', array_get($attributes, 'wv_tf_id', null));
        $this->model->filterData($query);
        $models = $query->paginate($limit);
        return $models;
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveXferDtl($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateXferDtl($data)
    {
        return $this->updateWhere([
            'xfer_ticket'  => $data['xfer_ticket_id'],
            'xfer_dtl_sts' => Status::getByValue("Picking", "XFER-STATUS"),
            'sts'          => 'u'
        ], [
            'xfer_hdr_id' => $data['xfer_hdr_id']
        ]);
    }

    /**
     * @param $xfer_id
     *
     * @return mixed
     */
    public function getReqQty($xfer_id)
    {
        return $this->model->where('xfer_hdr_id', $xfer_id)->sum('allocated_qty');
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function loadXferDtlInfo($id)
    {
        return DB::table('xfer_dtl')
            ->where("xfer_dtl.xfer_hdr_id", $id)
            ->first();
    }

    /**
     * @param $xfer_hdr_id
     *
     * @return mixed
     */
    public function loadAllXferDtlByHdrId($xfer_hdr_id)
    {
        return DB::table('xfer_dtl')
            ->leftJoin('customer', 'customer.cus_id', '=', 'xfer_dtl.cus_id')
            ->where("xfer_dtl.xfer_hdr_id", $xfer_hdr_id)
            ->select(
                'customer.cus_name',
                'xfer_dtl.*'
            )
            ->get();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function removeXferDtlById($id){
        return DB::table("xfer_dtl")->where('xfer_hdr_id',$id)->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function getXferTicketDetail($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            $query->where('xfer_ticket_id', $attributes['xfer_ticket_id']);
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        $query->where('whs_id', $currentWH);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }
}
