<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;

class GoodsReceiptCartonTransformer extends TransformerAbstract
{
    public function transform(Carton $carton)
    {        
        $palletRfid = object_get($carton, 'pallet.rfid', null);

        $status = object_get($carton, 'goodReceiptDtl.goodsReceipt.gr_sts', null);
        $status = Status::getByValue($status, 'GR_STATUS');

        $creater = object_get($carton, 'goodReceiptDtl.goodsReceipt.user', null);
        $created_by = $creater ? $creater->first_name . ' ' . $creater->last_name : null;

        $updater = object_get($carton, 'goodReceiptDtl.goodsReceipt.user', null);
        $updated_by = $updater ? $updater->first_name . ' ' . $updater->last_name : null;
                    
        return [
            'status'            => $status,
            'gr_dtl_id'         => $carton->gr_dtl_id,
            'item_id'           => $carton->item_id,                        
            'sku'               => $carton->sku,
            'size'              => $carton->size,
            'color'             => $carton->color,
            'lot'               => $carton->lot,
            'pack_size'         => $carton->ctn_pack_size,
            'upc'               => $carton->upc,                        
            'ctn_rfid'          => $carton->rfid,
            'pallet_rfid'       => $palletRfid,
            'created_at'        => $carton->created_at->format('m/d/Y H:i:s'),
            'created_by'        => $created_by,
            'updated_at'        => $carton->updated_at->format('m/d/Y H:i:s'),
            'updated_by'        => $updated_by
        ];   
        return $transforms;
    }
}
