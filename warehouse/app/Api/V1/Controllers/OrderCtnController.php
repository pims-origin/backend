<?php
/**
 * Created by PhpStorm.
 * User: PCDell76_ThienNguyen
 * Date: 2/10/2017
 * Time: 7:17 PM
 */

namespace App\Api\V1\Controllers;


use App\Api\V1\Models\OrderDtlModel;
use Seldat\Wms2\Utils\Export;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Models\SysBug;
use App\Api\V1\Transformers\ShippingReportTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class OrderCtnController extends AbstractController
{
  

 

    public function shippingReport(
        Request $request,
        $warehouseId,
        ShippingReportTransformer $orderCtnTransformer
    ) {
        $input = $request->getQueryParams();
        try {

            if (!empty($input['export']) && $input['export'] == 1) {
                $result = (new OrderDtlModel())->invReport($warehouseId, $input, false, true)->toArray();

                $title = [
                    'cus_code'                      => 'Customer Code',
                    'cus_name'                      => 'Customer Name',
                    'odr_num'                       => 'Order',
                    'odr_type|status|ORDER-TYPE'    => 'Order Type',

                    'item_id'                       => 'Item ID',
                    'sku'                           => 'SKU',
                    'size'                          => 'Size',
                    'color'                         => 'Color',
                    'lot'                           => 'Lot',
                    'upc'                           => 'UPC/EAN',
                    'pack'                          => 'Packsize',
                    'piece_qty|div|pack'            => 'Shipping CTNS',
                    'cube'                          => 'Cube',
                    'piece_qty'                     => 'Shipped QTY',
                    'shipped_dt|format()|m/d/Y H:i' => 'Shipped Date',
                    // 'act_cancel_dt|format()|m/d/Y'  => 'Actual Cancel Date',
                    'first_name|.|last_name'        => 'Shipped By',
                    'cus_odr_num'                   => 'Customer Order #',
                    'cus_po'                        => 'PO',
                    'ship_to_name'                  => 'Shipping to name',
                    'ship_to_add_1'                 => 'Address',
                    'ship_to_city'                  => 'City',
                    'sys_state_name'                => 'State',
                    'ship_to_zip'                   => 'Zip Code',
                    'carrier'                       => 'Carrier',

                ];
                Export::showFile("Export OrderShip", $title, $result);die;
            }

            $result = (new OrderDtlModel())->invReport($warehouseId, $input, array_get($input, 'limit', 20));

            return $this->response->paginator($result, $orderCtnTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}