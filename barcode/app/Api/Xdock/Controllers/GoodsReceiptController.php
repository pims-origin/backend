<?php

namespace App\Api\Xdock\Controllers;

use App\Api\Inbound2\Models\AsnDtlModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\Xdock\Models\GoodsReceiptModel;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\AsnMeta;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Wms2\UserInfo\Data;

class GoodsReceiptController extends AbstractController
{
    const MAX_ACTUAL_TTL = 20000;

    public function skuList($whsId, $asnHdrId, $cntrId)
    {
        return (new AsnDtlModel())->getListItem($asnHdrId, $cntrId);
    }

    private function getWhsInfo($whsId)
    {
        foreach (Data::getInstance()->getUserInfo()['user_warehouses'] as $whs) {
            if ($whs['whs_id'] == $whsId) {
                return $whs;
            }
        }
    }

    /**
     * url: inbound/{whsId}/scan-carton/asn-dtl/{asnDtlId}
     *
     * @throws \Exception
     * @internal param lpn string $code
     * @internal param integer $dmg_ttl
     * @internal param integer $act_ctn_ttl
     * @internal param decimal $weight
     * @internal param integer expired_date
     * @internal param string $lot
     *
     * @return mix
     */
    public function xdockScanCartonAndLocation($whsId, $asnDtlId, Request $request)
    {
        $whsInfo = $this->getWhsInfo($whsId);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $input = $request->getParsedBody();

        $xdockTypeId = DB::table('loc_type')->where('loc_type_code', 'XDK')->value('loc_type_id');
        $ctnTtl = array_get($input, 'act_ctn_ttl', null); //this case is ttl kg, not null
        $weight = array_get($input, 'weight', null); //this case is ttl kg, null

        $isKg = false; // OUM is kg

        $xdockLoc = array_get($input, 'xdock_loc', null);
        $arrDmgCartonId = [];
        $expiredDate = array_get($input, 'expired_date', null);
        $lot = trim(array_get($input, 'lot', null));

        if ($expiredDate) {
            $expiredDate = strtotime(\DateTime::createFromFormat('ymd', $expiredDate)->format('Y-m-d'));
            if ($expiredDate < time()) {
                return $this->response->errorBadRequest("Expired date is less than today.");
            }
        }

        if ((int)$ctnTtl <= 0) {
            $msg = sprintf('Carton total must be greater than or equal one');

            return $this->response->errorBadRequest($msg);
        }

        if ((int)$ctnTtl >= self::MAX_ACTUAL_TTL) {
            $msg = sprintf('Actual total must be not greater than max total %d', self::MAX_ACTUAL_TTL);

            return $this->response->errorBadRequest($msg);
        }

        //Validate ASN
        $asnDtl = AsnDtl::where('asn_dtl_id', $asnDtlId)
            ->first();
        if (empty($asnDtl)) {
            return $this->response->errorBadRequest('Asn dtl not existed');
        }

        $asnHdr = AsnHdr::where('asn_hdr_id', $asnDtl->asn_hdr_id)->first();
        if ($asnHdr->whs_id != Data::getCurrentWhsId()) {
            $this->response->errorBadRequest('This ASN do not belong current warehouse');
        }

        //check UOM is KG
        if ($asnDtl->uom_code == 'KG') {
            $isKg = true;

            if ($asnDtl->asn_dtl_pack > 1) {
                return $this->response->errorBadRequest('UOM is Kg only process for pack size 1.');
            }

            if (empty($weight)) {
                return $this->response->errorBadRequest('UOM is Kg only process for weight greater than 0.');
            }
        } else {
            $ctnTtl = (int)$ctnTtl;
        }


        //get gr hdr
        $grHdr = GoodsReceipt::where([
            'asn_hdr_id' => $asnHdr->asn_hdr_id,
            'ctnr_id'    => $asnDtl->ctnr_id
        ])
            ->first();

        $grObj = new GoodsReceiptModel();

        //validate xdock type
        $xdockLocObj = Location::where([
            'loc_code'   => $xdockLoc,
            'loc_whs_id' => Data::getCurrentWhsId()
        ])
            ->first();

        if (empty($xdockLoc) || empty($xdockLocObj)) {
            return $this->response->errorBadRequest("Location isn't existed");
        }

        if ($xdockTypeId != $xdockLocObj->loc_type_id) {
            return $this->response->errorBadRequest("This location isn't cross dock type");
        }

        if ($xdockLocObj->loc_sts_code == 'IA') {
            return $this->response->errorBadRequest("This location is inactive");
        }

        if ($xdockLocObj->loc_sts_code == 'LK') {
            return $this->response->errorBadRequest("This location is locked");
        }

        //validate xdock pallet
        $xdockPalletObj = Pallet::where([
            'loc_id' => $xdockLocObj->loc_id,
        ])->first();

        if (!empty($xdockPalletObj)) {
            if ($xdockPalletObj->cus_id != $asnHdr->cus_id) {
                return $this->response->errorBadRequest("This location isn't belong ASN customer");
            }

            $grHdrId = $grHdr->gr_hdr_id ?? 0;
            if ($xdockPalletObj->gr_hdr_id != $grHdrId) {
                return $this->response->errorBadRequest("This location has a pallet");
            }
        }

        try {
            DB::beginTransaction();

            if (empty($xdockLocObj->loc_zone_id)) {
                $xdkZone = DB::table('zone_type')->where('zone_type_code', 'XDK')->value('zone_type_id');

                // get first zone order by dynamic desc
                // if zone not exist the create zone zone code = cus_code and '-D'
                // update  loc_zone_id for location
                $zone = DB::table('customer_zone as cz')
                    ->join('zone as z', 'cz.zone_id', '=', 'z.zone_id')
                    ->where('cz.cus_id', $asnHdr->cus_id)
                    ->where('cz.deleted', 0)
                    ->orderBy('z.dynamic', 'DESC')
                    ->select('z.zone_id')
                    ->first();
                $zoneId = array_get($zone, 'zone_id');
                if (!$zoneId) {
                    $cus = DB::table('customer')->where('cus_id', $asnHdr->cus_id)->first();

                    $dZone = DB::table('zone')
                        ->where('zone_name', array_get($cus, 'cus_code') . ' - Dynamic Zone')
                        ->where('zone_whs_id', object_get($asnHdr, 'whs_id'))
                        ->where('zone_type_id', $xdkZone)
                        ->where('dynamic', 1)
                        ->where('deleted_at', 915148800)
                        ->where('deleted', 0)
                        ->first();

                    if(empty($dZone)) {
                        $zoneId = DB::table('zone')->insertGetId([
                            'zone_name'      => array_get($cus, 'cus_code') . ' - Dynamic Zone',
                            'zone_code'      => array_get($cus, 'cus_code') . '-D',
                            'zone_whs_id'    => object_get($asnHdr, 'whs_id'),
                            'zone_type_id'   => $xdkZone,
                            'zone_min_count' => 1,
                            'zone_max_count' => 100,
                            'created_at' => time(),
                            'updated_at' => time(),
                            'deleted_at' => '915148800',
                            'updated_by' => Data::getCurrentUserId(),
                            'created_by' => Data::getCurrentUserId(),
                            'deleted' => 0,
                            'dynamic' => 1
                        ]);
                    }else{
                        $zoneId = $dZone['zone_id'];
                    }
                    
                    $cusZone = DB::table('customer_zone')
                        ->where('cus_id', $asnHdr->cus_id)
                        ->where('zone_id', $zoneId)
                        ->where('deleted', 0)
                        ->where('deleted_at', 915148800)
                        ->first();

                    if(empty($cusZone)) {
                        DB::table('customer_zone')->insert([
                            'cus_id' => $asnHdr->cus_id,
                            'zone_id' => $zoneId,
                            'deleted' => 0,
                            'deleted_at' => '915148800',
                            'created_at' => time(),
                            'updated_at' => time(),
                            'updated_by' => Data::getCurrentUserId(),
                            'created_by' => Data::getCurrentUserId(),
                        ]);
                    }                    
                };

                DB::table('location')
                    ->where('loc_id', $xdockLocObj->loc_id)
                    ->update([
                        'loc_zone_id' => $zoneId
                    ]);
            }

            if ($asnDtl->asn_dtl_sts == 'NW') {
                $asnDtl->asn_dtl_sts = 'RG';
                $asnDtl->save();

            }

            // Asn Meta By Qualifier
            $createdAt = AsnMeta::where('qualifier', 'GAD')
                ->where('asn_hdr_id', $asnHdr->asn_hdr_id)
                ->select('value')
                ->first();
            $createdAt = object_get($createdAt, 'value', time());

            if (!$grHdr) {
                $grSeq = GoodsReceipt::where('asn_hdr_id', $asnHdr->asn_hdr_id)->count();
                $grSeq++;
                $asnHdrNum = $asnHdr->asn_hdr_num;
                $grNum = str_replace('ASN', 'GDR', $asnHdrNum);
                $grNum = $grNum . '-' . str_pad($grSeq, 2, '0', STR_PAD_LEFT);


                $grHdr = new GoodsReceipt();
                $arr = [
                    "ctnr_id"       => $asnDtl->ctnr_id,
                    "asn_hdr_id"    => $asnDtl->asn_hdr_id,
                    "gr_hdr_seq"    => $grSeq,
                    "gr_hdr_ept_dt" => $asnHdr->asn_hdr_ept_dt,
                    "gr_hdr_num"    => $grNum,
                    "whs_id"        => $asnHdr->whs_id,
                    "cus_id"        => $asnHdr->cus_id,
                    "gr_in_note"    => "",
                    "gr_ex_note"    => "",
                    "ctnr_num"      => $asnDtl->ctnr_num,
                    "gr_sts"        => "RG",
                    "ref_code"      => $asnHdr->asn_hdr_ref,
                    "created_from"  => 'GUN',
                    "created_at"    => $createdAt,
                    "updated_at"    => $createdAt,
                    "created_by"    => Data::getCurrentUserId(),
                    "updated_by"    => Data::getCurrentUserId(),
                    "deleted"       => 0,
                    "deleted_at"    => 915148800
                ];
                $grHdr->fill($arr)->save();

                //event tracking
                $evtGRCreated = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($grHdr, 'gr_hdr_num', null),
                    'evt_code'   => 'WGN',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - New Goods Receipt %s Created', $grHdr->gr_hdr_num),
                    'created_at' => time(),
                    'created_by' => Data::getCurrentUserId(),
                ];

                EventTracking::insert($evtGRCreated);
            } else {
                $grNum = $grHdr->gr_hdr_num;
                $evtGRUpdate = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($grHdr, 'gr_hdr_num', null),
                    'evt_code'   => 'WGU',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - Xdock Goods Receipt Update %d cartons', ($isKg) ? 1 : $ctnTtl),
                    'created_at' => time(),
                    'created_by' => Data::getCurrentUserId(),
                ];

                EventTracking::insert($evtGRUpdate);
            }

            if ($asnHdr->asn_sts == 'NW') {
                $asnHdr->asn_sts = 'RG';
                $asnHdr->save();

                $evtAsnCreated = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($asnHdr, 'asn_hdr_num', null),
                    'evt_code'   => 'WAR',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - New Goods Receipt %s Created', $grHdr->gr_hdr_num),
                    'created_at' => time(),
                    'created_by' => Data::getCurrentUserId(),
                ];

                EventTracking::insert($evtAsnCreated);
            }

            //create gr dtl
            $grDtl = GoodsReceiptDetail::where([
                'asn_dtl_id' => $asnDtlId,
                'gr_hdr_id'  => $grHdr->gr_hdr_id,
                'item_id'    => $asnDtl->item_id,
                'lot'        => $asnDtl->asn_dtl_lot,
            ])->first();
            if (!$grDtl) {
                $xdockQty = $isKg ? $weight : ($ctnTtl * $asnDtl->asn_dtl_pack);

                if ($xdockQty > $asnDtl->asn_dtl_crs_doc_qty) {
                    return $this->response->errorBadRequest('Cross dock qty greater than ASN cross dock ' .
                        $asnDtl->asn_dtl_crs_doc_qty);
                }

                $grDtl = new GoodsReceiptDetail();
                $arr = [
                    "asn_dtl_id"         => $asnDtlId,
                    "gr_hdr_id"          => $grHdr->gr_hdr_id,
                    "gr_dtl_ept_ctn_ttl" => $asnDtl->asn_dtl_ctn_ttl,
                    "gr_dtl_act_ctn_ttl" => $ctnTtl,
                    "gr_dtl_ept_qty_ttl" => ($isKg) ? $asnDtl->asn_dtl_qty_ttl :
                        $asnDtl->asn_dtl_ctn_ttl * $asnDtl->asn_dtl_pack, //new field

                    "gr_dtl_disc_qty" => ($isKg) ? $weight - $asnDtl->asn_dtl_qty_ttl : $ctnTtl *
                        $asnDtl->asn_dtl_pack - ($asnDtl->asn_dtl_ctn_ttl * $asnDtl->asn_dtl_pack),

                    "gr_dtl_act_qty_ttl" => $xdockQty,
                    "gr_dtl_des"         => $asnDtl->asn_dtl_des,
                    "gr_dtl_disc"        => $ctnTtl - $asnDtl->asn_dtl_ctn_ttl,
                    "gr_dtl_plt_ttl"     => 1,
                    //"gr_dtl_is_dmg"      => $input['dmg_ttl'] > 0 ? 1 : 0,
                    //"gr_dtl_dmg_ttl"     => $input['dmg_ttl'] > 0 ? $input['dmg_ttl'] : 0,
                    "crs_doc"            => $ctnTtl,
                    "crs_doc_qty"        => $xdockQty,
                    "cat_code"           => $asnDtl->cat_code,
                    "cat_name"           => $asnDtl->cat_name,
                    "gr_dtl_sts"         => "RG",
                    "item_id"            => $asnDtl->item_id,
                    "pack"               => $asnDtl->asn_dtl_pack,
                    "sku"                => $asnDtl->asn_dtl_sku,
                    "color"              => $asnDtl->asn_dtl_color,
                    "size"               => $asnDtl->asn_dtl_size,
                    "lot"                => $asnDtl->asn_dtl_lot,
                    "po"                 => $asnDtl->asn_dtl_po,
                    "uom_code"           => $asnDtl->uom_code,
                    "uom_name"           => $asnDtl->uom_name,
                    "uom_id"             => $asnDtl->uom_id,
                    "expired_dt"         => $asnDtl->expired_dt,
                    "upc"                => $asnDtl->asn_dtl_cus_upc,
                    "ctnr_id"            => $asnDtl->ctnr_id,
                    "ctnr_num"           => $asnDtl->ctnr_num,
                    "length"             => $asnDtl->asn_dtl_length,
                    "width"              => $asnDtl->asn_dtl_width,
                    "height"             => $asnDtl->asn_dtl_height,
                    "weight"             => ($isKg) ? $weight : $asnDtl->asn_dtl_weight,
                    "cube"               => $asnDtl->asn_dtl_cube,
                    "volume"             => $asnDtl->asn_dtl_volume,
                    "ucc128"             => $asnDtl->ucc128,
                    "spc_hdl_code"       => $asnDtl->spc_hdl_code,
                    "spc_hdl_name"       => $asnDtl->spc_hdl_name,
                    "created_at"         => $createdAt,
                    "updated_at"         => $createdAt,
                    "created_by"         => Data::getCurrentUserId(),
                    "updated_by"         => Data::getCurrentUserId(),
                    "deleted"            => 0,
                    "deleted_at"         => 915148800
                ];
                $grDtl->fill($arr)->save();

            } else {
                $grDtl->gr_dtl_act_ctn_ttl += $ctnTtl;
                if ($isKg) {
                    $grDtl->weight += $weight;
                    $grDtl->gr_dtl_act_qty_ttl += $weight;
                    $grDtl->crs_doc += 1;
                    $grDtl->crs_doc_qty += $weight;
                } else {
                    $actQty = $ctnTtl * $asnDtl->asn_dtl_pack;
                    $grDtl->gr_dtl_act_qty_ttl += $actQty;
                    $grDtl->crs_doc += $ctnTtl;
                    $grDtl->crs_doc_qty += $actQty;
                }


                if ($grDtl->crs_doc_qty > $asnDtl->asn_dtl_crs_doc_qty) {
                    return $this->response->errorBadRequest('Total cross dock qty greater than ASN cross dock qty ' .
                        $asnDtl->asn_dtl_crs_doc_qty);
                }

                $grDtl->gr_dtl_plt_ttl += 1;
                $grDtl->gr_dtl_disc = $grDtl->gr_dtl_act_ctn_ttl - $asnDtl->asn_dtl_ctn_ttl;
                $grDtl->gr_dtl_disc_qty = $grDtl->gr_dtl_act_qty_ttl - $grDtl->gr_dtl_ept_qty_ttl;
                $grDtl->save();
            }

            $lot = empty($lot) ? $asnDtl->asn_dtl_lot : $lot;

            $palletID = null;
            if (empty($xdockPalletObj)) {
                $pltInfo = $this->generateXpltRfid($xdockLocObj->loc_id);
                //add plt_num
                $plt = [
                    "cus_id"           => $asnHdr->cus_id,
                    "whs_id"           => $asnHdr->whs_id,
                    "plt_num"          => $pltInfo['plt_num'],
                    "rfid"             => $pltInfo['plt_rfid'],
                    "gr_hdr_id"        => $grHdr->gr_hdr_id,
                    "gr_dtl_id"        => $grDtl->gr_dtl_id,
                    "ctn_ttl"          => ($isKg) ? 1 : $input['act_ctn_ttl'],
                    "init_ctn_ttl"     => $input['act_ctn_ttl'],
                    "expired_date"     => !empty($expiredDate) ? $expiredDate : $asnDtl->expired_dt,
                    "storage_duration" => 0,
                    "plt_sts"          => 'RG',
                    "is_movement"      => 0,
                    "item_id"          => $grDtl->item_id,
                    "sku"              => $grDtl->sku,
                    "size"             => $grDtl->size,
                    "color"            => $grDtl->color,
                    "pack"             => ($isKg) ? $ctnTtl : $grDtl->pack,
                    "lot"              => $lot,
                    "is_full"          => 0,
                    "uom"              => ($isKg) ? 'PL' : null,
                    "weight"           => ($isKg) ? $weight : null,
                    "init_piece_ttl"   => ($isKg) ? $weight : $grDtl->pack * $ctnTtl,
                    "loc_id"           => $xdockLocObj->loc_id,
                    "loc_code"         => $xdockLocObj->loc_code,
                    "loc_name"         => $xdockLocObj->loc_code,
                    "created_at"       => $createdAt,
                    "updated_at"       => $createdAt,
                    "created_by"       => Data::getCurrentUserId(),
                    "updated_by"       => Data::getCurrentUserId(),
                    "deleted"          => 0,
                    "deleted_at"       => 915148800,
                    "spc_hdl_code"     => $grDtl->spc_hdl_code,
                    "is_xdock"         => 1,
                ];

                $palletID = DB::table('pallet')->insertGetId($plt);
                $xdockPalletObj = Pallet::where('plt_id', $palletID)->first();
            } else {
                $palletID = $xdockPalletObj->plt_id;
                if (!$isKg) {
                    $xdockPalletObj->ctn_ttl += $ctnTtl;
                    $xdockPalletObj->init_ctn_ttl += $ctnTtl;
                    $xdockPalletObj->init_piece_ttl += $grDtl->pack * $ctnTtl;
                    $xdockPalletObj->save();
                }

            }


            //create cartons
            $ctnNumPrefix = str_replace('GDR', 'CTN', $grHdr->gr_hdr_num) . "-";
            $maxCtnNum = DB::table('cartons')->where('ctn_num', 'LIKE', $ctnNumPrefix . '%')->max('ctn_num');
            $maxCtnNum = $maxCtnNum ?: $ctnNumPrefix . '0000';

            $cartonParams = [];
            $cartonDamages = [];

            $expiredDate = !empty($expiredDate) ? $expiredDate : $asnDtl->expired_dt;
            //$grDt = strtotime($grHdr->created_at);

            $ctnSts = 'RG';
            if ($isKg) {
                $cartonParams = [
                    'asn_dtl_id'    => $asnDtlId,
                    'gr_dtl_id'     => $grDtl->gr_dtl_id,
                    'gr_hdr_id'     => $grDtl->gr_hdr_id,
                    'is_damaged'    => 0,
                    'item_id'       => $grDtl->item_id,
                    'whs_id'        => $whsId,
                    'cus_id'        => $grHdr->cus_id,
                    'ctn_num'       => ++$maxCtnNum,
                    'ctn_sts'       => $ctnSts,
                    'ctn_uom_id'    => $asnDtl->uom_id,
                    'uom_code'      => $asnDtl->uom_code,
                    'uom_name'      => $asnDtl->uom_name,
                    'ctn_pack_size' => 1,
                    'piece_remain'  => round($weight), // this case is weight
                    'piece_ttl'     => round($weight), // this case is weight
                    'gr_dt'         => $createdAt,
                    'sku'           => $asnDtl->asn_dtl_sku,
                    'size'          => $asnDtl->asn_dtl_size,
                    'color'         => $asnDtl->asn_dtl_color,
                    'lot'           => $lot,
                    'po'            => $asnDtl->asn_dtl_po,
                    'upc'           => $asnDtl->asn_dtl_cus_upc,
                    'ctnr_id'       => $asnDtl->ctnr_id,
                    'ctnr_num'      => $asnDtl->ctnr_num,
                    'length'        => $asnDtl->asn_dtl_length,
                    'width'         => $asnDtl->asn_dtl_width,
                    'height'        => $asnDtl->asn_dtl_height,
                    'weight'        => $weight,
                    'volume'        => $asnDtl->asn_dtl_volume,
                    'cube'          => $asnDtl->asn_dtl_cube,
                    'expired_dt'    => $expiredDate,
                    'ucc128'        => $asnDtl->ucc128,
                    'des'           => $asnDtl->asn_dtl_des,
                    'cat_code'      => $asnDtl->cat_code,
                    'cat_name'      => $asnDtl->cat_name,
                    'spc_hdl_code'  => $asnDtl->spc_hdl_code,
                    'spc_hdl_name'  => $asnDtl->spc_hdl_name,
                    'plt_id'        => $palletID,
                    'loc_id'        => $xdockLocObj->loc_id,
                    'loc_code'      => $xdockLocObj->loc_code,
                    'loc_name'      => $xdockLocObj->loc_code,
                    'loc_type_code' => 'XDK',
                    "created_at"    => time(),
                    "updated_at"    => time(),
                    "created_by"    => Data::getCurrentUserId(),
                    "updated_by"    => Data::getCurrentUserId(),
                    "deleted"       => 0,
                    "deleted_at"    => 915148800,
                    "inner_pack"    => $ctnTtl
                ];
            } else {
                for ($i = 0; $i < $ctnTtl; $i++) {
                    $cartonParam = [
                        'asn_dtl_id'    => $asnDtlId,
                        'gr_dtl_id'     => $grDtl->gr_dtl_id,
                        'gr_hdr_id'     => $grDtl->gr_hdr_id,
                        'is_damaged'    => 0,
                        'item_id'       => $grDtl->item_id,
                        'whs_id'        => $whsId,
                        'cus_id'        => $grHdr->cus_id,
                        'ctn_num'       => ++$maxCtnNum,
                        'ctn_sts'       => $ctnSts,
                        'ctn_uom_id'    => $asnDtl->uom_id,
                        'uom_code'      => $asnDtl->uom_code,
                        'uom_name'      => $asnDtl->uom_name,
                        'ctn_pack_size' => $asnDtl->asn_dtl_pack,
                        'piece_remain'  => $asnDtl->asn_dtl_pack,
                        'piece_ttl'     => $asnDtl->asn_dtl_pack,
                        'gr_dt'         => $createdAt,
                        'sku'           => $asnDtl->asn_dtl_sku,
                        'size'          => $asnDtl->asn_dtl_size,
                        'color'         => $asnDtl->asn_dtl_color,
                        'lot'           => $lot,
                        'po'            => $asnDtl->asn_dtl_po,
                        'upc'           => $asnDtl->asn_dtl_cus_upc,
                        'ctnr_id'       => $asnDtl->ctnr_id,
                        'ctnr_num'      => $asnDtl->ctnr_num,
                        'length'        => $asnDtl->asn_dtl_length,
                        'width'         => $asnDtl->asn_dtl_width,
                        'height'        => $asnDtl->asn_dtl_height,
                        'weight'        => $asnDtl->asn_dtl_weight,
                        'volume'        => $asnDtl->asn_dtl_volume,
                        'cube'          => $asnDtl->asn_dtl_cube,
                        'expired_dt'    => $expiredDate,
                        'ucc128'        => $asnDtl->ucc128,
                        'des'           => $asnDtl->asn_dtl_des,
                        'cat_code'      => $asnDtl->cat_code,
                        'cat_name'      => $asnDtl->cat_name,
                        'spc_hdl_code'  => $asnDtl->spc_hdl_code,
                        'spc_hdl_name'  => $asnDtl->spc_hdl_name,
                        'plt_id'        => $palletID,
                        'loc_id'        => $xdockLocObj->loc_id,
                        'loc_code'      => $xdockLocObj->loc_code,
                        'loc_name'      => $xdockLocObj->loc_code,
                        'loc_type_code' => 'XDK',
                        "created_at"    => time(),
                        "updated_at"    => time(),
                        "created_by"    => Data::getCurrentUserId(),
                        "updated_by"    => Data::getCurrentUserId(),
                        "deleted"       => 0,
                        "deleted_at"    => 915148800,
                    ];

                    if ($cartonParam['is_damaged'] == 1) {
                        $cartonDamages[] = $cartonParam;
                    } else {
                        $cartonParams[] = $cartonParam;
                    }
                }
            }

            if (!empty($cartonParams)) {
                Carton::insert($cartonParams);
            }

            $info = sprintf('GUN Xdock - %d  cartons  to pallet  %s', $ctnTtl, $xdockPalletObj->rfid);
            if ($isKg) {
                $info = sprintf('GUN Xdock - %d carton and %s KG  to pallet  %s', $ctnTtl, $weight,
                    $xdockPalletObj->rfid);
            }

            $evtCartonCreated = [
                'whs_id'     => $whsId,
                'cus_id'     => $grHdr->cus_id,
                'owner'      => $grNum,
                'evt_code'   => 'WGS',
                'trans_num'  => $asnDtl->ctnr_num,
                'info'       => $info,
                'created_at' => time(),
                'created_by' => Data::getCurrentUserId(),
            ];

            EventTracking::insert($evtCartonCreated);

            //update location status code
            $xdockLocObj->loc_sts_code = 'RG';
            $xdockLocObj->save();

            //create invertory sumary
            //$grObj->createInventory($grHdr->whs_id, $grHdr->cus_id, $grDtl, $lot);

            DB::commit();

            return [
                'message' => sprintf('`%s` and %d cartons has been created in %s', $xdockPalletObj->rfid, ($isKg) ? 1
                    : $ctnTtl, $grNum)
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getGoodsReceiptDetails($whsId, $grId, Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        /*
         * get special handling from gr dtl
         * Get 4 locations with spc hdl
         */
        $sqlTtl = '(SELECT COUNT(pallet.plt_id)  
                           FROM pallet 
                           WHERE pallet.gr_hdr_id = gr_hdr.gr_hdr_id 
                           AND pallet.ctn_ttl > 0 
                     ) AS pallet_total';
        $sqlScan = '(SELECT COUNT(pallet.plt_id)  
                           FROM pallet 
                           WHERE pallet.gr_hdr_id = gr_hdr.gr_hdr_id 
                                AND pallet.loc_id IS NOT NULL 
                                AND pallet.ctn_ttl > 0 
                     ) AS pallet_scan';

        $grInfo = DB::table('gr_hdr')
            ->select('gr_hdr.gr_hdr_num', 'gr_hdr.gr_hdr_id', DB::raw($sqlTtl), DB::raw($sqlScan))
            ->where(['gr_hdr.gr_hdr_id' => $grId, 'gr_hdr.whs_id' => $whsId])
            ->where('gr_hdr.deleted', 0)
            ->where('gr_hdr.deleted_at', 915148800)
            ->first();

        return ['data' => $grInfo];
    }

    public function getSuggestionLocations($whsId, Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        /*
         * get special handling from gr dtl
         * Get 4 locations with spc hdl
         */
        $input = $request->getQueryParams();
        $lpn = array_get($input, 'lpn', null);

        if (!$lpn) {
            return $this->response->errorBadRequest('LPN is required!');
        }

        $palletInfo = DB::table('pallet')
            ->join('gr_dtl', 'gr_dtl.gr_dtl_id', '=', 'pallet.gr_dtl_id')
            ->select([
                'gr_dtl.spc_hdl_code',
                'pallet.cus_id',
                'pallet.ctn_ttl',
                'pallet.init_ctn_ttl',
                'pallet.loc_id',
                'pallet.whs_id',
                'pallet.is_full'
            ])
            ->where('pallet.whs_id', $whsId)
            ->where('pallet.rfid', $lpn)
            ->where('pallet.ctn_ttl', '>', 0)
            ->where('pallet.deleted', 0)
            ->where('pallet.deleted_at', 915148800)
            ->orderBy('pallet.plt_id', 'DESC')
            ->first();

        /*
         * ctn_ttl  = 0, pallet no cartons or picked
         * loc_id  IS NOT NULL, Put away already!
         */
        if (empty($palletInfo)) {
            $msg = sprintf('%s is not existed!', $lpn);

            return $this->response->errorBadRequest($msg);
        }

        list($whsCode, $pltType) = explode('-', $lpn);

        if (!empty($palletInfo['loc_id'])) {
            $msg = sprintf('%s on %s already!', $lpn, $palletInfo['loc_code']);

            return $this->response->errorBadRequest($msg);
        }

        $isFull = $palletInfo['is_full'] && $palletInfo['init_ctn_ttl'] == $palletInfo['ctn_ttl'];

        $locs = $this->getLocationsByCustomer($palletInfo, false, $pltType, $isFull);
        if (!$locs || count($locs) == 0) {
            $cusConfig = new CustomerConfigModel();
            $isDynZone = $cusConfig->isDynamicZone($palletInfo['whs_id'], $palletInfo['cus_id']);
            if ($isDynZone) {
                $locs = $this->getLocationsByCustomer($palletInfo, $isDynZone, $pltType, $isFull);
            }
        }

        $locs = array_column($locs->toArray(), 'loc_code');

        return [
            'locations' => $locs
        ];
    }

    private function getLocationsByCustomer(array $attributes, $isDynZone, $pltType, $isFull, $limit = 2)
    {

        $query = DB::table('location');
        $query->select([
            'location.loc_id',
            'location.loc_code'
        ])
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('loc_type.loc_type_code', '=', 'RAC')
            ->where('location.loc_sts_code', '=', 'AC')
            ->whereRaw(
                '0 = (SELECT COUNT(pallet.loc_id) FROM pallet WHERE pallet.loc_id = location.loc_id AND pallet.loc_id IS NOT NULL)'
            )
            ->where('location.loc_whs_id', $attributes['whs_id'])
            ->where('location.spc_hdl_code', $attributes['spc_hdl_code'])
            ->where('location.plt_type', $pltType);

        //Dynamic zone select locations
        if ($isDynZone) {
            $query->whereNull('location.loc_zone_id');
        } else {
            $query->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
                ->where('customer_zone.cus_id', $attributes['cus_id']);
        }

        //Full pallet suggestion
        if ($isFull) {
            $query->orderBy(DB::Raw("location.loc_code REGEXP '([A])[1-2]$', location.loc_code"), 'ASC');
        } else {
            $query->orderBy(DB::Raw("location.loc_code REGEXP '([B-Z])[1-2]$', location.loc_code"), 'ASC');
        }


        $query->groupBy('location.loc_id');

        $locs = $query->take($limit)->get();

        if (count($locs)) {
            $locs = $this->sortDeepLocationPutaway($locs);
        }

        return $locs;
    }

    public function putAwayList($whsId)
    {
        $result = (new GoodsReceiptModel())->getPutAwayList($whsId);

        return $result;
    }

    private function sortDeepLocationPutaway($data)
    {
        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i + 1])) {
                $next = $data[$i + 1]['loc_code'];
                $cur = $data[$i]['loc_code'];
                $curloc = substr($cur, 0, strlen($cur) - 1);
                $nextLoc = substr($next, 0, strlen($next) - 1);
                if ($curloc == $nextLoc) {
                    if (substr($cur, -1, 1) < substr($next, -1, 1)) {
                        $next = $data[$i + 1];
                        $current = $data[$i];
                        $data[$i + 1] = $current;
                        $data[$i] = $next;
                        $i = $i + 1;
                    }

                }

            }
        }

        return $data;
    }

    private function generateXpltRfid($locId)
    {
        $whsInfo = $this->getWhsInfo(Data::getCurrentWhsId());
        $whsCode = $whsInfo['whs_code'];

        $rfid = $whsCode . "-XDK-" . str_pad($locId, 6, '0', $pad_type = STR_PAD_LEFT);
        $pltNum = Pallet::where('plt_num', 'LIKE', $rfid . '-%')
            ->max('plt_num');

        if (empty($pltNum)) {
            return [
                "plt_rfid" => $rfid,
                "plt_num"  => $rfid . '-00001'
            ];
        }

        return [
            "plt_rfid" => $rfid,
            "plt_num"  => ++$pltNum
        ];
    }

}