<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/24/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Warehouse;


class WarehouseModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     *
     * @param Warehouse $model
     */
    public function __construct(Warehouse $model = null)
    {
        $this->model = ($model) ?: new Warehouse();
    }

    /**
     * @param int $warehouseId
     *
     * @return int
     */
    public function deleteWarehouse($warehouseId)
    {
        return $this->model
            ->where('whs_id', $warehouseId)
            ->delete();
    }

    public function deleteMassWarehouse(array $warehouseIds)
    {
        return $this->model
            ->whereIn('whs_id', $warehouseIds)
            ->delete();
    }

    /**
     * Search Warehouse
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $keys = [
            "like"  => [
                "whs_name",
                "whs_city_name",
                "whs_code"
            ],
            "equal" => [
                "whs_country_id",
                "whs_state_id",
                "whs_status"
            ]
        ];

        $arrChangeKeySorts = [
            'whs_status_name' => 'whs_status'
        ];

        //  Change key sort
        if (!empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $value){
                if (array_key_exists($key, $arrChangeKeySorts)) {
                    unset($attributes['sort'][$key]);
                    $attributes['sort'][$arrChangeKeySorts[$key]] = $value;
                }
            }
        }

        return $this->_search($attributes, $with, $limit, $keys, '');
    }

}