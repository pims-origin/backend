<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class WavePickHdrModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getWvHdrFirst($userId, $whsId, $cusIds){

        return $this->model
            ->join('wv_dtl', 'wv_dtl.wv_id', '=', 'wv_hdr.wv_id')
            ->where('wv_hdr.wv_sts', 'NW')
            ->where('wv_dtl.wv_dtl_sts', 'NW')
            ->whereRaw("(wv_hdr.picker IS NULL OR wv_hdr.picker = $userId)")
            ->where('wv_hdr.whs_id', $whsId)
            ->whereIn('wv_dtl.cus_id', $cusIds)
            ->select(
                'wv_hdr.wv_id',
                'wv_hdr.wv_num',
                'wv_hdr.wv_sts',
                'wv_hdr.whs_id',
                'wv_hdr.picker'
            )
            ->orderBy('wv_hdr.created_at', 'DESC')
            ->first();
    }
}
