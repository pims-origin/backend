<?php

namespace App\Api\MQ\Jobs;
use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Created by PhpStorm.
 * User: phuctran
 * Date: 12/12/2016
 * Time: 12:40
 */
class ExampleJob extends Job implements SelfHandling
{

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        file_put_contents(storage_path('logs/' . $this->data), 'queue');
        sleep(30);
    }
}