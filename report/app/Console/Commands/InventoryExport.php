<?php

namespace App\Console\Commands;

use App\Api\V1\Models\InventoryModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Seldat\Wms2\Services\DailyInventoryService;

class InventoryExport extends AbstractCommand
{
    protected $signature   = 'inventory-export {today?}';
    protected $description = 'Inventory Export';

    public function __construct()
    {
        parent::__construct();
        $this->inventoryModel = new InventoryModel();
    }

    public function handle()
    {
        try {
            $this->logSuccess('inventory-export');
            $result =  $this->inventoryModel->getDataExportForInventory();
            $file_name = 'Inventory';
            $file = Excel::create($file_name, function($excel) use ($result) {
                foreach ($result as $key=>$value){
                    $excel->sheet($key, function($sheet) use ($value) {
                        $sheet->setStyle(array(
                            'font' => array(
                                'name' => 'Arial',
                                'size' => 11,
                                'bold' => false,
                            ),
                        ))
                            ->row(1, [
                                'CUSTOMER',
                                'SKU',
                                'DESCRIPTION ',
                                'QTY',
                                'LOCATION',
                            ])
                            ->setWidth('A', 30)
                            ->setWidth('B', 50)
                            ->setWidth('C', 50)
                            ->setWidth('D', 20)
                            ->setWidth('E', 20)
                            ->cells('A1:E1', function ($cells) {
                                $cells->setBackground('#e3f1f2')
                                    ->setValignment('center')
                                    ->setAlignment('center');
                            });
                        $i = 2;
                        foreach ($value as $item){
                            $sheet->row($i, array($item['cus_name'], $item['sku'], $item['description'],$item['piece_remain'], $item['loc_code']));
                            $i++;
                        }
                    });
                }

            })->string('xlsx');
            Storage::disk('s3')->put(env('S3_PIMS_DIR').'Inventory.xlsx',$file);
            $this->logSuccess('inventory-export done');

        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }

    }
}
