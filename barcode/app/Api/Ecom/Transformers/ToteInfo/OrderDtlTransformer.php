<?php

namespace App\Api\Ecom\Transformers\ToteInfo;

use League\Fractal\TransformerAbstract;

class OrderDtlTransformer extends TransformerAbstract
{
    public function transform($odrDtl)
    {
        return [
            'sku'           => $odrDtl->sku,
            'size'          => $odrDtl->size,
            'color'         => $odrDtl->color,
            'qty'           => sprintf('%s/%s', $odrDtl->pack_qty, $odrDtl->piece_qty),
            'itm_id'        => $odrDtl->item_id,
            'cus_upc'       => $odrDtl->cus_upc,
            'piece_qty'     => $odrDtl->pack_qty,
            'remain_qty'    => $odrDtl->remain_qty
        ];
    }
}
