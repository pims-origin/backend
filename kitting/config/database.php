<?php
/**
 * Created by PhpStorm.
 * User: vinhpham
 * Date: 8/4/16
 * Time: 11:07 AM
 */

return [
    'default' => 'mysql',
    'connections' => [
        'mysql' => [
            'driver'    => 'mysql',
            'host' => env('DB_HOST', 'localhost'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',

        ],
    ],
    'redis'       => [
        'cluster'   => false,
        'default'   => [
            'host'     => env('REDIS_HOST', 'localhost'),
            'port'     => env('REDIS_PORT', 6379),
            'database' => 0,
            'password' => env('REDIS_PWD', 'redis')
        ],
        'cache-key' => env('CACHE_KEY', 'awms2_dev:cache.user.'),
    ]
];
?>
