<?php

namespace App\Api\V4\Outbound\Order\AssignCartonsToOrder21\Wavepick\UpdateWithRfid\Models;

use Mockery\CountValidator\Exception;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class InventorySummaryModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        // search sku
        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        // search sku
        if (isset($attributes['color'])) {
            $query->where('color', 'like', "%" . SelStr::escapeLike($attributes['color']) . "%");
        }

        // search sku
        if (isset($attributes['size'])) {
            $query->where('size', 'like', "%" . SelStr::escapeLike($attributes['size']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * @param array $data
     * @param int $cus_id
     * @param int $whs_id
     */
    //public function updateInventorySummary(array $data, $cus_id = 0, $whs_id = 0)
    //{
    //    $inserts = [];
    //    $total = $data['gr_dtl_act_ctn_ttl'] * $data['asn_detail']['asn_dtl_pack'];
    //    $allocatedQty = array_get($data, 'allocated_qty', null);
    //    $dmgQty = array_get($data, 'dmg_qty', null);
    //    $whs_id = $whs_id ? $whs_id : array_get($data, 'whs_id', null);
    //
    //    try {
    //        $fields = [
    //            'item_id'       => array_get($data, 'asn_detail.item_id', null),
    //            'cus_id'        => $cus_id ? $cus_id : array_get($data, 'cus_id', null),
    //            'whs_id'        => $whs_id,
    //            'color'         => array_get($data, 'asn_detail.item.color', null),
    //            'size'          => array_get($data, 'asn_detail.item.size', null),
    //            'lot'           => array_get($data, 'asn_detail.lot', null),
    //            'allocated_qty' => $allocatedQty,
    //            'dmg_qty'       => $dmgQty,
    //            'sku'           => array_get($data, 'asn_detail.item.sku', null),
    //            'created_at'    => time(),
    //            'updated_at'    => time()
    //        ];
    //
    //        if ($total > 0) {
    //            $fields = array_merge($fields, [
    //                'ttl'   => $total,
    //                'avail' => ($total - $allocatedQty - $dmgQty)
    //            ]);
    //        }
    //        $this->refreshModel();
    //        $ivt = $this->model
    //            ->where('item_id', $data['asn_detail']['item_id'])
    //            ->where('whs_id', $whs_id)
    //            ->first();
    //        if ($ivt) {
    //            $this->updateWhere([
    //                "ttl"           => $ivt->ttl + $fields['ttl'],
    //                "allocated_qty" => (empty($ivt->allocated_qty) ? 0 : $ivt->allocated_qty) + $fields['allocated_qty'],
    //                "dmg_qty"       => (empty($ivt->dmg_qty) ? 0 : $ivt->dmg_qty) + $fields['dmg_qty'],
    //                "avail"         => (empty($ivt->avail) ? 0 : $ivt->avail) + $fields['avail']
    //            ], [
    //                "item_id" => $data['asn_detail']['item_id'],
    //                "whs_id"  => $whs_id
    //            ]);
    //        } else {
    //            (new InventorySummary)::insert($fields);
    //        }
    //
    //    } catch (Exception $ex) {
    //        return $ex->getMessage();
    //    }
    //
    //
    //}

    /**
     * @param array $data
     */
    public function updateDmgQty(array $data)
    {
        foreach ($data as $detail) {
            $this->refreshModel();
            $ivt = $this->model
                ->where('item_id', $detail['item_id'])
                ->where('cus_id', $detail['cus_id'])
                ->where('whs_id', $detail['whs_id'])
                ->first()->toArray();

            if ($ivt) {
                $ivt['dmg_qty'] = (int)$ivt['dmg_qty'] + $detail['dmg_qty'];
                $ivt['avail'] = (int)$ivt['ttl'] - (int)$ivt['allocated_qty'] - (int)$ivt['dmg_qty'];
                $this->refreshModel();
                $this->update($ivt);
            }
        }
    }

    public function correctActualAllocatedQty($whsId, $itemId, $lot)
    {
        $invtObj = $this->getFirstWhere([
                        'whs_id'  => $whsId,
                        'item_id' => $itemId,
                        'lot'     => $lot,
                    ]);

        $invtAlloc = object_get($invtObj, 'allocated_qty', 0);

        $actAllocPickedQty = \DB::table('odr_hdr as oh')
                                ->select([
                                    DB::raw("SUM(od.alloc_qty) as alloc_qty_ttl"),
                                    DB::raw("SUM(wd.act_piece_qty) as act_qty")
                                ])
                                ->join('odr_dtl as od', 'od.odr_id', '=', 'oh.odr_id')
                                ->join('wv_hdr as wh', 'wh.wv_id', '=', 'oh.wv_id')
                                ->join('wv_dtl as wd', 'wh.wv_id', '=', 'wd.wv_id')
                                ->where('oh.deleted', 0)
                                ->where('od.deleted', 0)
                                ->where('wh.deleted', 0)
                                ->where('wd.deleted', 0)
                                ->whereIn('wh.wv_sts', ['NW', 'PK'])
                                ->where('od.whs_id', $whsId)
                                ->where('od.item_id', $itemId)
                                ->where('od.lot', $lot)
                                ->get();

        $caseAllocatedQty = \DB::table('odr_hdr as oh')
                                ->select([
                                    DB::raw("SUM(od.alloc_qty) as alloc_qty_ttl"),
                                    DB::raw("SUM(wd.act_piece_qty) as act_qty")
                                ])
                                ->join('odr_dtl as od', 'od.odr_id', '=', 'oh.odr_id')
                                ->where('oh.deleted', 0)
                                ->where('od.deleted', 0)
                                ->where('oh.odr_sts', 'AL')
                                ->where('od.whs_id', $whsId)
                                ->where('od.item_id', $itemId)
                                ->where('od.lot', $lot)
                                ->sum('od.alloc_qty');

        $actPickedReturn = \DB::table('wv_hdr as wh')
                            ->join('wv_dtl as wd', 'wh.wv_id', '=', 'wd.wv_id')
                            ->where('wh.deleted', 0)
                            ->where('wd.deleted', 0)
                            ->where('wh.wv_sts', 'RT')
                            ->where('wd.whs_id', $whsId)
                            ->where('wd.item_id', $itemId)
                            ->where('wd.lot', $lot)
                            ->sum('act_piece_qty');

        /*
        * compute real inventory's allocated qty again when applied new feature cancel order (WMS2-4404) and overpicking of wave pick. Then correct it by moving some acllocated qty redundancy to avail qty.
        */

        $allocQtyTtl  = array_get($actAllocPickedQty, 'alloc_qty_ttl') + $caseAllocatedQty;
        $actPickedTtl = array_get($actAllocPickedQty, 'act_qty') + $actPickedReturn;
        $realAlloc    = $allocQtyTtl - $actPickedTtl;

        if ($invtAlloc > $realAlloc && $realAlloc > 0) {
            $invtObj->allocated_qty = $realAlloc;
            $invtObj->avail         = $invtAlloc - $realAlloc;
            $invtObj->save();
        }
    }
}
