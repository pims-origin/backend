<?php

namespace App\Api\V1\Transformers;

use App\OrderHdr;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\DB;

class OrderHdrTransformer extends TransformerAbstract
{

    /**
     * ps:Show WorkOrder
     *
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $odrSts = $orderHdr->odr_sts;
        $odrQty = 0;
        $details = [];
        foreach($orderHdr->details as $item){

            $dimension = DB::table('pack_hdr')->select([
                'pack_hdr.length',
                'pack_hdr.width',
                'pack_hdr.height'])
                ->join('pack_dtl', 'pack_dtl.pack_hdr_id', '=', 'pack_hdr.pack_hdr_id')
                ->where('pack_dtl.sku', $item->sku)
                ->first();
            $description = "";
            if ($dimension) {
                $description = (int)object_get($dimension, 'length', 0) . 'x' . (int)object_get($dimension, 'width', 0) . 'x' . (int)object_get($dimension, 'height', 0);
            }

            $details[] = [
                'sku'           => $item->sku,
                'qty'           => $item->piece_qty,
                'description'   => $description
            ];
        }

        return [
            'odr_id'    => $orderHdr->odr_id,
            'odr_num'   => $orderHdr->odr_num,
            'details'   => $details,
            'is_rush'   => $orderHdr->rush_odr == 1 ? true : false,
            'cus_po'    => $orderHdr->cus_po,
            'qty'       => $odrQty,
        ];
    }
}
