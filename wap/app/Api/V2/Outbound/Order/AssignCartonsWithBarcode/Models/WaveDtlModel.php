<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Outbound\Order\AssignCartonsWithBarcode\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Status;

class WaveDtlModel extends AbstractModel
{

    /**
     * WaveDtlModel constructor.
     *
     * @param WavepickDtl|null $model
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateWvDtl($data)
    {
        return $this->model->where('wv_dtl_id', $data['wv_dtl_id'])
            ->update([
                'act_piece_qty' => $data['act_piece_qty'],
                'wv_dtl_sts'    => $data['wv_dtl_sts'],
            ]);
    }

    public function getActualWvDtl($wvId)
    {
        return $this->model
            ->where('wv_id', $wvId)
            ->whereRaw('act_piece_qty = piece_qty')
            ->select(DB::raw('COUNT(wv_dtl_id) as actual'))->get();
    }

    public function getStatusWvHdr($wvId)
    {
        return $this->model
            ->where('wv_id', $wvId)
            ->select(DB::raw('SUM(act_piece_qty) as actual, SUM(piece_qty) as piece_qty'))
            ->get();
    }

    public function getStatusWvDtl($wvDtlId)
    {
        return $this->model
            ->where('wv_dtl_id', $wvDtlId)
            ->select(DB::raw('SUM(act_piece_qty) as actual, SUM(piece_qty) as piece_qty'))
            ->get();
    }

    public function updateWaveDtl($wvDtlID, $actQty, $sts = 'PK')
    {
        $res =  $this->model
            ->where('wv_dtl_id', '=', $wvDtlID)
            ->update([
                'act_piece_qty' => DB::raw('act_piece_qty + ' . $actQty),
                'wv_dtl_sts'    => $sts ,
            ]);

        return $res;
    }
}
