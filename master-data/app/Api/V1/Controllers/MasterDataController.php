<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MasterDataModel;
use App\Api\V1\Models\MasterDataTypeModel;
use App\Api\V1\Transformers\MasterDataTransformer;
use App\Api\V1\Validators\MasterDataValidator;

use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Request as IRequest;

use Seldat\Wms2\Utils\Status as Status;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Utils\Helper;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\JWTUtil;
use Illuminate\Support\Facades\DB;

use Dingo\Api\Http\Response;

class MasterDataController extends AbstractController
{

    /**
     * @var object $masterDataModel
     */
    protected $masterDataModel;

    /**
     * @var object $masterDataTypeModel
     */
    protected $masterDataTypeModel;

    /**
     * @var object $masterDataTransformer
     */
    protected $masterDataTransformer;

    /**
     * @var object $masterDataValidator
     */
    protected $masterDataValidator;

    /**
     * ItemController constructor.
     * @param ItemModel $model
     * @param MasterDataTransformer $transformer
     * @param ItemValidator $validator
     */
    public function __construct()
    {
        $this->masterDataModel       = new MasterDataModel();
        $this->masterDataTypeModel   = new MasterDataTypeModel();
        $this->masterDataTransformer = new MasterDataTransformer();
        $this->masterDataValidator   = new MasterDataValidator();
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request)
    {
        $input = $request->getParsedBody();
        $limit = array_get($input, 'limit', PAGING_LIMIT);

        try {
            $masterData = $this->masterDataModel->search($input, ['createdBy', 'updatedBy'], $limit);

            return $this->response->paginator($masterData, $this->masterDataTransformer);
        } catch (\Exception $e) {
            //catch errors
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $mdType
     * @param $mdCode
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getInfo($mdType, $mdCode)
    {
        try {
            $masterData = $this->masterDataModel->byTypeCode($mdType, $mdCode);

            return $this->response->item($masterData, $this->masterDataTransformer);
        } catch (\Exception $e) {
            //catch errors
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function store(Request $request)
    {
        $input = $request->getParsedBody();
        $mdCode = array_get($input, 'md_code');
        $mdType = array_get($input, 'md_type');
        $mdName = array_get($input, 'md_name');

        $checkExist = $this->masterDataModel->byTypeCode($mdType, $mdCode);
        if ($checkExist) {
            return $this->response->errorBadRequest(Message::get("BM006", "couple collection Master Data Code, Master Data Type"));
        }

        $checkExist = $this->masterDataModel->byTypeName($mdType, $mdName);
        if ($checkExist) {
            return $this->response->errorBadRequest(Message::get("BM006", "couple collection Master Data Name, Master Data Type"));
        }

        $params = [
            'md_code'     => $mdCode,
            'md_type'     => $mdType,
            'md_name'     => array_get($input, 'md_name'),
            'md_des'      => array_get($input, 'md_des'),
            'md_data'     => array_get($input, 'md_data'),
            'md_sts'      => array_get($input, 'md_sts'),
            'created_at'  => time(),
            'updated_at'  => time(),
            'created_by'  => JWTUtil::getPayloadValue('jti') ?? 0,
            'updated_by'  => JWTUtil::getPayloadValue('jti') ?? 0,
            'deleted_at'  => getDefaultDatetimeDeletedAt(),
            'deleted'     => 0,
        ];

        // validate
        $this->masterDataValidator->validate($params);

        try {
            DB::beginTransaction();

            $masterData = $this->masterDataModel->createMasterData($params);

            DB::commit();

            return ["data" => "successful"];

        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param $mdType
     * @param $mdCode
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(Request $request, $mdType, $mdCode)
    {
        $input = $request->getParsedBody();

        $mdName = array_get($input, 'md_name');
        // dd($mdName);
        $checkExist = $this->masterDataModel->byTypeName($mdType, $mdName, $mdCode);
        if ($checkExist) {
            return $this->response->errorBadRequest(Message::get("BM006", "couple collection Master Data Name, Master Data Type"));
        }

        $params = [
            'md_code'    => $mdCode,
            'md_type'    => $mdType,
            'md_name'    => array_get($input, 'md_name'),
            'md_des'     => array_get($input, 'md_des'),
            'md_data'    => array_get($input, 'md_data'),
            'md_sts'     => array_get($input, 'md_sts'),
            'updated_at' => time(),
            'updated_by' => JWTUtil::getPayloadValue('jti') ?? 0,
        ];

        // validate
        $this->masterDataValidator->validate($params);

        try {
            DB::beginTransaction();

            $masterData = $this->masterDataModel->updateMasterData($mdType, $mdCode, $params);

            DB::commit();

            return ["data" => "successful"];

        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     *
     * @return array|void
     */
    public function mdStatusDropDown()
    {
        try {
            $mdStatus = config('constants.master_data.status');

            return ['data' => $mdStatus];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     *
     * @return array|void
     */
    public function mdTypesDropDown()
    {
        try {
            $types = $this->masterDataTypeModel->masterDataTypes();

            return ['data' => $types];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }
}
