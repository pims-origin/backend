<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\InvoiceListModel;

use Seldat\Wms2\Models\InvoiceDetails;
use Seldat\Wms2\Models\InvoiceDetail;

use Seldat\Wms2\Utils\SelArr;

class InvoiceDetailsModel extends AbstractModel
{
    protected $model;
    protected $invoiceList;
    protected $invDtlModel;

    public function __construct(
        InvoiceDetails $model = NULL,
        InvoiceListModel $invoiceList = NULL
    )
    {
        $this->model = ($model) ?: new InvoiceDetail();
        $this->invSummary = ($model) ?: new InvoiceDetails();
        $this->invoiceList = ($invoiceList) ?: new InvoiceListModel();
        //$this->invDetail = new InvoiceDetail();
    }

    /**
    * @data array
    * @return array
    */

    public function getDetails($data)
    {
        $data['data'] = [
            [
                'cat' => 'Storage',
                'dt' => '2016-05-01 to 2016-09-29',
                'id' => 'STORAGE CARTON',
                'qty' => 50,
                'uom' => 'CARTON',
            ],
            [
                'cat' => 'Receiving',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-06-20',
                'ext' => [
                    'meas' => 'US-Imperial',
                    'name' => 'Container_59_2',
                ],
                'id' => 10000117,
            ],
            [
                'cat' => 'Receiving',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-06-21',
                'ext' => [
                    'meas' => 'US-Imperial',
                    'name' => 'Container_59_3',
                ],
                'id' => 10000118,
            ],
            [
                'cat' => 'Receiving',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-06-22',
                'ext' => [
                    'meas' => 'US-Imperial',
                    'name' => 'Container_59_4',
                ],
                'id' => 10000119,
            ],
            [
                'cat' => 'Receiving',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-06-26',
                'ext' => [
                    'meas' => 'US-Imperial',
                    'name' => 'Container_59_8',
                ],
                'id' => 10000125,
            ],
            [
                'cat' => 'Receiving',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-09-14',
                'ext' => [
                    'meas' => 'US-Imperial',
                    'name' => 'Container_71',
                ],
                'id' => 10000178,
            ],
            [
                'cat' => 'Receiving',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-09-15',
                'ext' => [
                    'meas' => 'US-Imperial',
                    'name' => 'Container_73_1',
                ],
                'id' => 10000179,
            ],
            [
                'cat' => 'Receiving',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-09-16',
                'ext' => [
                    'meas' => 'US-Imperial',
                    'name' => 'Container_72_1',
                ],
                'id' => 10000181,
            ],
            [
                'cat' => 'Order Processing',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-05-27',
                'ext' => [
                    'clientOrdNum' => '0004100526',
                    'cq' => 4,
                    'custNum' => '0004100526',
                    'name' => '0004100526',
                    'pq' => 19,
                ],
                'id' => '0004100526',
                'order' => 'Open',
            ],
            [
                'cat' => 'Order Processing',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-05-28',
                'ext' => [
                    'clientOrdNum' => '0004100526',
                    'cq' => 4,
                    'custNum' => '0004100526',
                    'name' => '0004100526',
                    'pq' => 19,
                ],
                'id' => '0004100526',
                'order' => 'Open',
            ],
            [
                'cat' => 'Order Processing',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-05-29',
                'ext' => [
                    'clientOrdNum' => '0004100526',
                    'cq' => 4,
                    'custNum' => '0004100526',
                    'name' => '0004100526',
                    'pq' => 19,
                ],
                'id' => '0004100526',
                'order' => 'Open',
            ],
            [
                'cat' => 'Order Processing',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-05-09',
                'ext' => [
                    'clientOrdNum' => '0004100474',
                    'cq' => 7,
                    'custNum' => '0004100474',
                    'name' => '0004100474',
                    'pq' => 9,
                ],
                'id' => '0004100474',
                'order' => 'Open',
            ],
            [
                'cat' => 'Order Processing',
                'cust' => 'LA_ACCUTIME WATCH',
                'dt' => '2016-05-23',
                'ext' => [
                    'clientOrdNum' => '0004100522',
                    'cq' => 2,
                    'custNum' => '0004100522',
                    'name' => '0004100522',
                    'pq' => 2,
                ],
                'id' => '0004100522',
                'order' => 'Open',
            ],
            [
                'cat' => 'Storage',
                'dt' => '2016-04-01 to 2016-09-15',
                'id' => 'STORAGE CARTON',
                'qty' => 76823,
                'uom' => 'CARTON',
            ],
            [
                'cat' => 'Storage',
                'dt' => '2016-04-01 to 2016-09-16',
                'id' => 'STORAGE CARTON',
                'qty' => 76823,
                'uom' => 'CARTON',
            ],
            [
                'cat' => 'Storage',
                'dt' => '2016-04-01 to 2016-09-30',
                'id' => 'STORAGE CARTON',
                'qty' => 76823,
                'uom' => 'CARTON',
            ],
            [
                'cat' => 'Storage',
                'dt' => '2016-04-01 to 2016-09-29',
                'id' => 'STORAGE CARTON',
                'qty' => 50,
                'uom' => 'CARTON',
            ],
        ];

        return $this->arrayPagination($data);
    }

    /**
    * @attributes with array
     * @limit int
    * @return array
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $whsID = getDefault($searchAttr['whs_id']);
        $cusID = getDefault($searchAttr['cus_id']);

        $params = [
            'custID' => $cusID,
            'startDate' => $searchAttr['fromDate'],
            'endDate' => $searchAttr['toDate'],
            'sums' => TRUE,
            'whs_id' => $whsID
         ];

        $details = $this->invoiceList->getBillableData($params);

        return $details['items'];
    }

    /**
    * @query string
    * @key string
    * @value mixed
    * @return string
    */

    private function customJoins($query, $key, $value)
    {
        $prefix = $this->invSummary->getTable();

        $query->where($prefix . '.' . $key, $value);

        return $query;
    }

    /**
    * @invoiceNo int
    * @return array
    */

    public function get($invoiceNo)
    {
        $detailSql = $this->getDetailsData($invoiceNo);

        $results = [];
        foreach ($detailSql as $row) {
            $uom = $row->systemUom;
            $charge = $row->chargeCode;
            $type = $row->chargeCode->chargeType;

            $results[] = [
                'inv_id' => $row->inv_id,
                'chg_cd' => $charge->chg_code,
                'chg_cd_name' => $charge->chg_code_name,
                'sys_uom_code' => $uom->sys_uom_code,
                'sys_uom_name' => $uom->sys_uom_name,
                'chg_cd_desc' => $row->chg_cd_desc,
                'chg_cd_type' => $type->chg_type_name,
                'chg_cd_cur' => $row->chg_cd_cur,
                'chg_cd_qty' => $row->chg_cd_qty,
                'chg_cd_price' => $row->chg_cd_price,
                'chg_cd_amt' => $row->chg_cd_amt
            ];
        }

        return $results;
    }

    /**
    * @invoiceNo int
    * @return array
    */

    public function getCancellingData($invoiceNo)
    {
        $cancelSql = $this->getDetailsData($invoiceNo);

        $cancelRes = [];
        foreach ($cancelSql as $row) {
            $uom = $row->systemUom;
            $charge = $row->chargeCode;

            $chg_cd_amt = -1 * $row->chg_cd_amt;

            $cancelRes[] = [
                'inv_id' => $row->inv_id,
                'cus_id' => $row->cus_id,
                'whs_id' => $row->whs_id,
                'chg_cd' => $charge->chg_code,
                'chg_cd_name' => $charge->chg_code_name,
                'chg_code_id' => $row->chg_code_id,
                'sys_uom_code' => $uom->sys_uom_code,
                'sys_uom_name' => $uom->sys_uom_name,
                'chg_uom_id' => $row->chg_uom_id,
                'chg_cd_desc' => $row->chg_cd_desc,
                'chg_cd_cur' => $row->chg_cd_cur,
                'chg_cd_qty' => $row->chg_cd_qty,
                'chg_cd_price' => $row->chg_cd_price,
                'chg_cd_amt' => $chg_cd_amt
            ];
        }

        return $cancelRes;
    }


    /**
    * @invoiceNo int
    * @return array
    */

    function checkIfExists($invoiceNo)
    {
        $result = $this->model
                        ->where('inv_num', $invoiceNo)
                        ->value('inv_id');

        return $result != [];
    }

    /**
    * @invoiceNo int
    * @return array
    */

    public function getDetailsData($invoiceNo)
    {
        $detailSql = $this->model
                            ->select('inv_id', 'cus_id', 'whs_id', 'chg_cd_qty',
                                     'chg_cd_price', 'chg_cd_amt', 'chg_cd_cur',
                                      'chg_cd_desc', 'chg_code_id', 'chg_uom_id')
                            ->with([
                                'systemUom' => function ($q) {
                                    $q->select('sys_uom_id', 'sys_uom_code', 'sys_uom_name');
                                },
                                'chargeCode' => function ($q) {
                                    $q->select('chg_code_id', 'chg_code', 
                                               'chg_code_name', 'chg_type_id');
                                    },
                                'chargeCode.chargeType' => function ($q) {
                                    $q->select('chg_type_id', 'chg_type_name');
                                },
                            ])
                            ->where('inv_num', intVal($invoiceNo))
                            ->get();

        return  $detailSql;
    }
}
