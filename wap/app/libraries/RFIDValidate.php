<?php

// namespace App\Api\V5\Inbound\Pallet\ScanPallet\Validators;

namespace App\libraries;

class RFIDValidate{

    const TYPE_CARTON = 'carton';

    const TYPE_PALLET = 'pallet';

    const TYPE_LOCATION = 'location';

    const TYPE_TABLET = 'tablet';

    const LENGTH = 24;

    protected $types = [
      self::TYPE_PALLET => 'FFFFFFFF',
      self::TYPE_LOCATION => 'DDDDDDDD',
      self::TYPE_TABLET => 'AAAAAAAA',
      self::TYPE_CARTON => '',
    ];

    protected $rfid;

    protected $type;

    public $error;

    protected $pattern = '';

    protected $msg = '';

    protected $whsId ;

    public function __construct($rfid, $type, $whsId)
    {
        $prefix = $this->types[$type];
        $this->rfid = strtoupper($rfid);

        $this->type = $type;
        $this->whsId = (int)$whsId;

        if(self::TYPE_CARTON == $type){
            $this->pattern = sprintf('/^([A-F0-9]{%d})$/', self::LENGTH);
        }else{
            $this->pattern = sprintf('/^(%s)([0-9]{4})([0-9]{12})$/', $prefix);
        }
    }

    public  function validate(){
        $result = preg_match_all($this->pattern , $this->rfid, $matches );

        $prefix = $this->types[$this->type];

        $type = ucfirst($this->type);
        $refidNote = sprintf('Your %s\'s RFID %s is invalid.+ Please check Example: + First 8 %s: %s pattern; + Next 4 digits %04d: Warehouse ID; + Last 12 digits: Sequence %s Number (Decimal)',
            $this->type, $this->rfid, $prefix, $this->type, $this->whsId, $this->type);

        if( ! $result){
            $msg = sprintf( "%s's RFID must be valid pattern '%s' and length %d", $type, $this->pattern, self::LENGTH);
            if (self::TYPE_CARTON == $this->type) {
                $msg = "There were invalid carton tag(s) in the carton list scanned. Please check and re-scan.";
            }
            if(self::TYPE_CARTON != $this->type){
                $msg = $refidNote;
            }

            $this->error = $msg;
            //return $this->error;
           return  false;
        // }else if(self::TYPE_CARTON != $this->type){
        }else if(!in_array($this->type, [self::TYPE_CARTON, self::TYPE_PALLET])){
            $whsId  = (int) array_shift($matches[2]);
            if($whsId != $this->whsId){
                $this->error = $refidNote;
                //return $this->error;
                return  false;
            }
        }

        if ($this->type == self::TYPE_CARTON) {
            $resultLocPlt= preg_match_all('/^([DF]{8})/' , $this->rfid, $matches );
            if ($resultLocPlt) {
                $msg = sprintf( "Your %s's RFID is wrong format!", $this->type);
                $this->error = $msg;
                return false;
            }
        }
        return true;
    }



}