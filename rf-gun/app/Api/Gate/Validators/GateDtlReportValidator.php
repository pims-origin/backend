<?php

namespace App\Api\Gate\Validators;


class GateDtlReportValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'gate_id' => 'integer|exists:gate,gate_id',
            'cus_id'  => 'integer|exists:customer,cus_id',
            'ctnr_id' => 'integer|exists:container,ctnr_id',
        ];

    }


}
