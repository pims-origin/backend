<?php

// Location
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/location', 'namespace' => 'App\Api\V2\Location'], function ($api) {
    // print tally sheet
    $api->get('/tally-sheet/print',
        [
            'action' => "viewCycleCount",
            'uses'   => 'PrintTallySheet\Controllers\LocationController@printTallySheet'
        ]
    );
});
