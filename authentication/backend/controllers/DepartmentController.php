<?php

namespace backend\controllers;

use backend\models\DepartmentSearch;
use Yii;
use yii\di\Instance;
use yii\web\NotFoundHttpException;

class DepartmentController extends ApiController
{
    public $modelClass = 'common\models\department';
    protected $qualifier = 'UTC';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view'], $actions['index'], $actions['create'], $actions['delete'], $actions['update']);

        return $actions;
    }

    public function actionIndex()
    {
        $value = DepartmentSearch::getDepartment();
        return ['data' => $value];
    }
}