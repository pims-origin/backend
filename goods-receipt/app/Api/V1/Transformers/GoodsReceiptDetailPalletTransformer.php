<?php


namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\Status;

class GoodsReceiptDetailPalletTransformer extends TransformerAbstract
{
    //return result
    public function transform(Pallet $pallet)
    {
        $expired_date = object_get($pallet, 'expired_date', null);
        $configPltSts = config('constants.plt_status');
        if (object_get($pallet, 'uom') == 'PL') {
            $ctnTtl = object_get($pallet, 'pallet_pack');
        } else {
            $ctnTtl = object_get($pallet, 'ctn_ttl');
        }

        return [
            'plt_id'           => object_get($pallet, 'plt_id'),
            'gr_dtl_id'        => object_get($pallet, 'gr_dtl_id'),
            'plt_num'          => object_get($pallet, 'lpn_carton'),
            'item_id'          => object_get($pallet, 'item_id'),
            'item_status_code' => object_get($pallet, 'item_status_code'),
            'item_status'      => object_get($pallet, 'item_status'),
            'sku'              => object_get($pallet, 'sku'),
            'size'             => object_get($pallet, 'size'),
            'color'            => object_get($pallet, 'color'),
            'lot'              => object_get($pallet, 'lot'),
            'des'              => object_get($pallet, 'des'),
            'plt_sts'          => $configPltSts[object_get($pallet, 'plt_sts')],
            'plt_sts_code'     => object_get($pallet, 'plt_sts'),
            'dmg_ttl'          => object_get($pallet, 'dmg_ttl'),
            'expired_date'     => $expired_date ? date('Y-m-d', $expired_date) : null,
            'act_loc_code'     => object_get($pallet, 'loc_code'),
            'init_piece_ttl'   => object_get($pallet, 'init_qty'),
            'init_ctns'        => object_get($pallet, 'init_ctns'),
            'putaway_location' => object_get($pallet, 'putaway_location') ?? object_get($pallet, 'loc_name'),
            'uom_code'         => object_get($pallet, 'uom_code'),
            'uom_name'         => object_get($pallet, 'uom_name'),
            'weight'           => number_format(object_get($pallet, 'weight', 0), 2, '.', ','),
            'pack'             => object_get($pallet, 'pack'),
            // 'pallet_uom'       => object_get($pallet, 'uom'),
            // 'pallet_pack'      => object_get($pallet, 'pallet_pack'),
            'ctn_ttl'          => $ctnTtl,
            'cur_qty'          => object_get($pallet, 'cur_qty'),
            'rfid'             => object_get($pallet, 'rfid', ''),
        ];
    }
}
