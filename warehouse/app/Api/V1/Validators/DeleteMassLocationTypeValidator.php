<?php

namespace App\Api\V1\Validators;

class DeleteMassLocationTypeValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_type_id'   => 'required',
            'loc_type_id.*' => 'required|exists:loc_type,loc_type_id'
        ];
    }
}