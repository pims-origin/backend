<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\Ecom\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackHdr;

class PackHdrModel extends AbstractModel
{
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
    }

}
