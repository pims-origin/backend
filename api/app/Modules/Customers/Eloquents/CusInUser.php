<?php

namespace App\Modules\Customers\Eloquents;

use Illuminate\Database\Eloquent\Model;

class CusInUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_in_user';

    protected $fillable = [
        'cus_id',
        'user_id',
        'whs_id'
    ];
}
