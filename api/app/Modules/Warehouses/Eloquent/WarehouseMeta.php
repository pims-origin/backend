<?php
namespace App\Modules\Warehouses\Eloquent;

use Illuminate\Database\Eloquent\Model;

class WarehouseMeta extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whs_meta';

    /**
     * No primary key in this table
     *
     * @var bool
     */
    protected $primaryKey = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'whs_qualifier',
        'whs_meta_value'
    ];
}
