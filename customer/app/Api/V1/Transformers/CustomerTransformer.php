<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\Customer;
use League\Fractal\TransformerAbstract;

class CustomerTransformer extends TransformerAbstract
{
    public function transform(Customer $customer)
    {
        return [
            'cus_id'              => $customer->cus_id,
            'cus_name'            => $customer->cus_name,
            'cus_status'          => $customer->cus_status,
            'cus_billing_account' => $customer->cus_billing_account,
            'cus_des'             => $customer->cus_des,
            'cus_status_name'     => !empty($customer->customerStatus->cus_sts_name)
                ? $customer->customerStatus->cus_sts_name
                : null,
            'cus_status_desc'     => !empty($customer->customerStatus->cus_sts_desc)
                ? $customer->customerStatus->cus_sts_desc
                : null,
            'cus_short_name'      => $customer->cus_short_name,
            'cus_code'            => $customer->cus_code,
            'cus_country_id'      => $customer->cus_country_id,
            'cus_country_name'    => !empty($customer->systemCountry->sys_country_name)
                ? $customer->systemCountry->sys_country_name
                : null,
            'cus_state_id'        => $customer->cus_state_id,
            'cus_state_name'      => !empty($customer->systemState->sys_state_name)
                ? $customer->systemState->sys_state_name
                : null,
            'cus_city_name'       => $customer->cus_city_name,
            'cus_postal_code'     => $customer->cus_postal_code,
            'cus_zone'            => $customer->cus_zone,

            // Color
            'cl_id'               => object_get($customer, 'customerColor.cl_id', null),
            'cl_name'             => object_get($customer, 'customerColor.cl_name', 'NA'),
            'cl_code'             => object_get($customer, 'customerColor.cl_code', '#ffffff'),

            // Customer Warehouse Info
            'whs_id'              => $customer->customerWarehouse,
            // 'whs_name' => $customer->cus_whs,

            // Customer Warehouse
            'warehouses'          => object_get($customer, 'cus_whs'),

            // Customer Contact Info
            'contacts'            => object_get($customer, 'cus_contact'),

            // Customer Address Info
            'addresses'           => object_get($customer, 'cus_address'),

            // Customer Charge Code
            'charge_codes'        => object_get($customer, 'cus_charge_code'),

            // Customer Charge Code
            'configs'             => object_get($customer, 'customer_config'),

            // #5818 - Customer : sales_rep, billed_through
            'sales_rep'           => object_get($customer, 'sales_rep'),
            'billed_through'      => object_get($customer, 'billed_through')
        ];
    }
}