<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackDtl;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class PackDtlModel
 *
 * @package App\Api\V1\Models
 */
class PackDtlModel extends AbstractModel
{
    /**
     * PackDtlModel constructor.
     *
     * @param PackDtl|null $model
     */
    public function __construct(PackDtl $model = null)
    {
        $this->model = ($model) ?: new PackDtl();
    }
}
