<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\DailyInventoryModel;
use App\Api\V1\Models\ForecastIndicatorModel;
use App\Api\V1\Models\ForecastModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Traits\ForecastControllerTrait;
use App\Api\V1\Transformers\ForecastTransformer;
use App\Api\V1\Validators\ImportForecastValidator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

class ForecastController extends AbstractController
{
    use ForecastControllerTrait;
    protected $forecastModel;
    protected $forecastIndicatorModel;
    protected $goodReceiptModel;
    protected $dailyInventoryModel;
    protected $odrHdrModel;
    protected $locationModel;
    protected $forecastTransformer;

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);
        parent::__construct($request, $object);

        $this->forecastModel = new ForecastModel();
        $this->forecastIndicatorModel = new ForecastIndicatorModel();
        $this->goodReceiptModel = new GoodsReceiptModel();
        $this->dailyInventoryModel = new DailyInventoryModel();
        $this->odrHdrModel = new OrderHdrModel();
        $this->locationModel = new LocationModel();
        $this->forecastTransformer = new ForecastTransformer();
    }

    public function import(Request $request) {
        $redis = new Data();
        $whsId = $redis->getCurrentWhs();
        $userId = $redis->getUserInfo()['user_id'];
        $input = $request->getParsedBody();
        $importValidator = new ImportForecastValidator();
        $importValidator->validate($input);

        $file = $request->getUploadedFiles()['file'];
        $extFile = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);

        if (!in_array($extFile, ['xlsx','xls'])) {
            throw new HttpException(404, Message::getOnLang("File only support Excel file"));
        }

        $invPath = storage_path('forecast-import');
        if (!file_exists($invPath)) {
            mkdir($invPath);
        }
        try {
            DB::beginTransaction();
            $invFileName = uniqid() . '.' . $extFile;
            $filePath = $invPath . "/" . $invFileName;
            $file->moveTo($filePath);

            $reader = Excel::load($filePath);
            unlink($filePath);
            $invArr = collect($reader->toArray());

            $invArr = $invArr->each(function($item, $key){
                $validator = Validator::make($item, [
                    'indicator'     => 'required',
                    'inbound'       => 'numeric',
                    'outbound'      => 'numeric',
                    'storage'       => 'numeric',
                    'schedule'      => 'required|numeric'
                ]);
                if($validator->fails()){
                   $messages = $validator->errors();
                   $errorReturn = sprintf("Row %s has invalid data: %s", $key+1, implode(", ", $messages->all()));
                   return $this->response->errorBadRequest($errorReturn);
                }
            });

            $indicators = $this->forecastIndicatorModel->all()->keyBy('fc_ind_name');

            foreach ($invArr as $item) {
                $item['indicator'] = trim(ucfirst($item['indicator']));
                $indicator = $indicators[$item['indicator']];

                switch ($input['period']) {
                    case 'quarter':
                        if ($item['schedule'] < 0 || $item['schedule'] > 4) {
                            return $this->response->errorBadRequest("Invalid quarter value.");
                        }
                        for ($month = ($item['schedule']*3 - 2); $month <= $item['schedule']*3; $month++) {
                            $this->forecastModel->insertOrUpdate($input, $whsId, $indicator, $month, $item, $userId);
                        }
                        break;
                    case 'month':
                        if ($item['schedule'] < 0 || $item['schedule'] > 12) {
                            return $this->response->errorBadRequest("Invalid month value.");
                        }
                        $this->forecastModel->insertOrUpdate($input, $whsId, $indicator, $item['schedule'], $item, $userId);
                        break;
                }
            }

            DB::commit();

            return [
                'data' => 'Imported successfully.'
            ];
        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show(Request $request) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $redis = new Data();
        $whsId = $redis->getCurrentWhs();
        $input = $request->getQueryParams();
        $year = $input['year'];
        $period = $input['period'];
        $periodValue = $input['period_value'];

        $forecasts = collect();
        switch ($period) {
            case "month":
                $month = $periodValue;

                $forecasts = $this->calculateForecastByMonth($year, $month, $input['cus_id'], $whsId);

                if(!$forecasts->isEmpty()) {
                    $this->getPreviousMonthsStorageData($forecasts, $year . "-" . $month);
                }
                break;
            case "quarter":
                $quarter = $periodValue;

                $forecasts = $this->calculateForecastByQuarterV2($year, $quarter, $input['cus_id'], $whsId);

                if(!$forecasts->isEmpty()) {
                    $this->getPreviousQuartersStorageData($forecasts, $year, $quarter, $input['cus_id'], $whsId);
                }
                break;
        }

        return $this->response->collection($forecasts, $this->forecastTransformer);
    }

    private function getPreviousQuartersStorageData(&$forecasts, $year, $quarter, $cusId, $whsId) {
        for ($i = 1; $i <= 3; $i++) {
            $quarter--;
            if ($quarter == 0) {
                $quarter = 4;
                $year--;
            }
            $data = $this->calculateForecastByQuarter($year, $quarter, $cusId, $whsId);
            if (!$data->isEmpty()) {
                foreach ($forecasts as $indicatorCode => &$forecast) {
                    $forecast['fc_st_' . $i . '_period_ago'] = isset($data[$indicatorCode]) ? $data[$indicatorCode]['fc_st_act'] : 0;
                }
            }
        }
    }

    private function calculateForecastByMonth($year, $month, $cus_id, $whsId) {
        $forecasts = $this->forecastModel->search($year, $month, $cus_id, $whsId);

        if (strtotime("{$year}-{$month}") == strtotime(date("Y-m"))) {
            if($forecasts->isEmpty()) {
                //Calculate container
                $this->calculateActual($forecasts, date('Y-m-d'));
            }
        }

        return $forecasts;
    }

    private function calculateForecastByQuarter($year, $quarter, $cusId, $whsId) {
        $forecasts = [];
        for ($month = ($quarter * 3 - 2); $month <= $quarter * 3; $month++) {
            $tempForecasts = $this->calculateForecastByMonth($year, $month, $cusId, $whsId);
            if (empty($forecasts)) {
                $forecasts = $tempForecasts;
            } else {
                foreach ($tempForecasts as $indicatorCode => $tempForecast) {
                    if (isset($forecasts[$indicatorCode])) {
                        $forecasts[$indicatorCode]['fc_ib'] += $tempForecast['fc_ib'];
                        $forecasts[$indicatorCode]['fc_ib_act'] += $tempForecast['fc_ib_act'];
                        $forecasts[$indicatorCode]['fc_ob'] += $tempForecast['fc_ob'];
                        $forecasts[$indicatorCode]['fc_ob_act'] += $tempForecast['fc_ob_act'];
                        $forecasts[$indicatorCode]['fc_st'] += $tempForecast['fc_st'];
                        $forecasts[$indicatorCode]['fc_st_act'] += $tempForecast['fc_st_act'];
                    } else {
                        $forecasts[$indicatorCode] = $tempForecast;
                    }
                }
            }
        }
        return $forecasts;
    }

    private function calculateForecastByQuarterV2($year, $quarter, $cusId, $whsId) {
        $forecasts = [];
        for ($month = ($quarter * 3 - 2); $month <= $quarter * 3; $month++) {
            $tempForecasts = $this->calculateForecastByMonth($year, $month, $cusId, $whsId);
            if (empty($forecasts)) {
                $forecasts = $tempForecasts;
            } else {
                foreach ($tempForecasts as $indicatorCode => $tempForecast) {
                    if (isset($forecasts[$indicatorCode])) {
                        $forecasts[$indicatorCode]['fc_ib'] += $tempForecast['fc_ib'];
                        $forecasts[$indicatorCode]['fc_ib_act'] += $tempForecast['fc_ib_act'];
                        $forecasts[$indicatorCode]['fc_ob'] += $tempForecast['fc_ob'];
                        $forecasts[$indicatorCode]['fc_ob_act'] += $tempForecast['fc_ob_act'];
                        $forecasts[$indicatorCode]['fc_st'] += $tempForecast['fc_st'];
                        $forecasts[$indicatorCode]['fc_st_act'] += $tempForecast['fc_st_act'];
                    } else {
                        $forecasts[$indicatorCode] = $tempForecast;
                    }
                }
            }
        }

        $soDu = (int)date('m') % 3;
        $soChia = $soDu == 0 ? 3 : $soDu;
        $forecasts['LOC']['fc_st_act'] = $forecasts['LOC']['fc_st_act'] / $soChia ;

        return $forecasts;
    }

    public function getYearMonthHaveData($cusId) {
        $redis = new Data();
        $whsId = $redis->getCurrentWhs();
        $yearMonths = $this->forecastModel->getYearMonthHaveData($cusId, $whsId);

        $result = [];
        foreach($yearMonths as $yearMonth) {
            if (isset($result[$yearMonth['fc_year']])) {
                $result[$yearMonth['fc_year']]['months'][] = $yearMonth['fc_month'];
            } else {
                $result[$yearMonth['fc_year']] = [
                    'year' => $yearMonth['fc_year'],
                    'months' => [
                        $yearMonth['fc_month']
                    ]
                ];
            }
        }
        return array_values($result);
    }
}
