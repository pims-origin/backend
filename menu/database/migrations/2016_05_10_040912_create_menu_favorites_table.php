<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorite_menu', function (Blueprint $table) {
            $table->integer('user_id')->default(0);
            $table->integer('menu_id')->default(0);
            $table->integer('display_order');
            $table->primary(['user_id', 'menu_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_favorite');
    }
}
