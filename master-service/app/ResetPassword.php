<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model {

    /**
     * @var string
     */
    protected $table = 'reset_password';
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string
     */
    protected $primaryKey = 'token';

    protected $timestamp = true;
}
