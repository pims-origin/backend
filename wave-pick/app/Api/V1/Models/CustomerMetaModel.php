<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\CustomerMeta;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class CustomerMetaModel extends AbstractModel
{
    /**
     * @param CustomerMeta $model
     */
    public function __construct(CustomerMeta $model = null)
    {
        $this->model = ($model) ?: new CustomerMeta();
    }


    public function getPNP($cus_id, $qualifier = 'CSR')
    {
        $flow = $this->getFirstWhere([
            'cus_id'    => $cus_id,
            'qualifier' => $qualifier
        ]);
        $value = object_get($flow, 'value', null);

        if ($value) {
            $valueJsons = \GuzzleHttp\json_decode($value);
            if ($valueJsons) {
                foreach ($valueJsons as $valueJson) {
                    if (object_get(\GuzzleHttp\json_decode($valueJson), 'set_default', 0)) {
                        return object_get(\GuzzleHttp\json_decode($valueJson), 'user_id', 0);
                    }
                }
            }
        }

        return 0;
    }

    public function getFlow($configs, $flow) {
        foreach ($configs as $config)
        {
            if(array_get($config, 'flow_code') == $flow) {
                return $config;
            }
        }
    }
}