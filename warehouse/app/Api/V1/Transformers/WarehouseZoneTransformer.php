<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseZone;

class WarehouseZoneTransformer extends TransformerAbstract
{
    /**
     * @param WarehouseZone $warehouseZone
     *
     * @return array
     */
    public function transform(WarehouseZone $warehouseZone)
    {
        return [
            'zone_id'        => $warehouseZone->zone_id,
            'zone_name'      => $warehouseZone->zone_name,
            'zone_code'      => $warehouseZone->zone_code,
            'zone_whs_id'    => $warehouseZone->zone_whs_id,
            'whs_code'       => object_get($warehouseZone, 'warehouse.whs_code', ''),
            'whs_name'       => object_get($warehouseZone, 'warehouse.whs_name', ''),

            // Zone Type
            'zone_type_id'   => object_get($warehouseZone, 'zoneType.zone_type_id', null),
            'zone_type_name' => object_get($warehouseZone, 'zoneType.zone_type_name', null),

            'zone_min_count'           => $warehouseZone->zone_min_count,
            'zone_max_count'           => $warehouseZone->zone_max_count,
            'zone_description'         => $warehouseZone->zone_description,
            'zone_num_of_loc'          => (int)$warehouseZone->zone_num_of_loc,
            'zone_num_of_active_loc'   => (int)$warehouseZone->zone_num_of_active_loc,
            'zone_num_of_inactive_loc' => (int)$warehouseZone->zone_num_of_inactive_loc,

            // Customer
            'customers'                => object_get($warehouseZone, 'customers', null),
            'zone_color'               => $warehouseZone->zone_color
        ];
    }
}
