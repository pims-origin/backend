<?php

namespace Seldat\Wms2\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\JWTUtil;

class ItemHier extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'item_hier';

    protected $dateFormat = 'U';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_hier_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = [
        'level_id',
        'pkg_code',
        'pkg_name',
        'pack',
        'item_id',
        'parent',
        'length',
        'width',
        'height',
        'weight',
        'net_weight',
        'volume',
        'cube',
        'uom_name',
        'uom_code',
        'uom_id',
        'upc'
    ];

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function itemLevel()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ItemLevel', 'level_id', 'level_id');
    }

    public static function boot()
    {
        parent::boot();

        $userId = JWTUtil::getPayloadValue('jti') ?: 1;

        // create a event to happen on updating
        static::updating(function ($table) use ($userId) {
            $table->updated_by = $userId;
        });

        // create a event to happen on saving
        static::saving(function ($table) use ($userId) {
            $table->created_by = $table->created_by ?: $userId;
            $table->updated_by = $userId;
        });
    }
}
