<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/24/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class ContainerModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     * @param Container $model
     */
    public function __construct(Container $model = null)
    {
        $this->model = ($model) ?: new Container();
    }

    /**
     * @param int $containerId
     * @return int
     */
    public function deleteContainer($containerId)
    {
        return $this->model
            ->where('ctnr_id', $containerId)
            ->delete();
    }



    /**
     * Search Container
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'ctnr_num') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }
        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);
        return $models;
    }
    
    public function getByNumber($number)
    {
        return $this->getFirstBy('ctnr_num', $number);
    }

}