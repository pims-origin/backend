<?php


namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\PalletSuggestLocation;

class RFGunTransformer extends TransformerAbstract
{
    //return result
    public function transform(PalletSuggestLocation $palletSuggestLocation)
    {
        $total = object_get($palletSuggestLocation, 'total', 0);
        $actual = object_get($palletSuggestLocation, 'actual', 0);

        return [
            'gr_hdr_id'  => object_get($palletSuggestLocation, 'gr_hdr_id'),
            'gr_hdr_num' => object_get($palletSuggestLocation, 'gr_hdr_num'),
            'sku'        => $total . "/" . $actual,
            'status'     => $total == $actual ? "DONE" : ($actual == 0 ? "NOT" : "IN")
        ];
    }

}
