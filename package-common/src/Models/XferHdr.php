<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 12-Oct-16
 * Time: 09:34
 */

namespace Seldat\Wms2\Models;

class XferHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'xfer_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xfer_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'xfer_hdr_num',
        'whs_id',
        'cus_id',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'cus_upc',
        'piece_qty',
        'xfer_sts',
        'sts',
        'xfer_ticket_id'
    ];

    public function xferDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\XferDtl', 'xfer_hdr_id', 'xfer_hdr_id');
    }

    public function xferTicket()
    {
        return $this->hasOne(__NAMESPACE__ . '\XferTicket', 'xfer_ticket_id', 'xfer_ticket_id');
    }
}
