<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UpdateInvoiceEvent;
use App\EventTracking;

class InvoiceTrackingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateInvoiceEvent  $event
     * @return void
     */
    public function handle(UpdateInvoiceEvent $event)
    {
        $header = $event->invoiceHeader;
        $tracking = new EventTracking();
        $tracking->create([
            'whs_id'    => $header->whs_id,
            'cus_id'    => $header->cus_id,
            'owner'     => $header->inv_num,
            'evt_code'  => config('constants.evt_tracking_code.'.$header->inv_sts),
            'trans_num' => $header->inv_num,
            'info'      => sprintf(config('constants.evt_tracking_info.'.$header->inv_sts), $header->inv_num)
        ]);
    }
}
