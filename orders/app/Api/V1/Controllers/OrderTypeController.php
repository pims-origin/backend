<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class OrderTypeController
 *
 * @package App\Api\V1\Controllers
 */
class OrderTypeController extends AbstractController
{
    public function show(
        Request $request
    ) {
        $input = $request->getQueryParams();
        $types = Status::get("Order-type");
        $data = [];
        if (!empty($types) && is_array($types)) {
            foreach ($types as $key => $value) {
                $data[] = [
                    "key"   => $key,
                    "value" => $value,
                ];
            }

        }
        //only using for Shipping report
        if (isset($input['for']) && !empty($input['for'])) {
            $data[] = [
                "key"   => 'EC',
                "value" => 'EComm',
            ];
        }

        return ['data' => $data];
    }
}
