<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\BaseInvoiceModel;

class InvoiceUOM extends BaseInvoiceModel
{
    protected $table = 'inv_uom';
    protected $primaryKey = 'inv_uom_id';
    protected $fillable = [
        'inv_uom_code',
        'inv_uom_des',
        'inv_uom_type',
        'inv_uom_type_des',
    ];
}
