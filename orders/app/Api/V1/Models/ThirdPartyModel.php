<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\ThirdParty;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class ThirdPartyModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     *
     * @param ThirdParty $model
     */
    public function __construct()
    {
        $this->model = new ThirdParty();
    }

    /**
     * @param int $customerId
     *
     * @return int
     */
    public function getThirdPartyById($tpId)
    {
        $thirdParty =  $this->model
            ->where('tp_id', $tpId)
            ->first();

        return $thirdParty;
    }

}