<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use App\Api\V1\Models\SummaryModel;
use App\Api\V1\Models\RcvSumModel;

use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\RcvSum;
use Seldat\Wms2\Utils\Message;

class PopulateSummaryModel extends AbstractModel
{
    /**
    * @var
    */
    protected $rcvModel;
    protected $rcv;
    protected $stor;
    protected $evt;
    protected $sum;
    
    
    /**
     * RcvSumModel constructor.
     * @param RcvSumModel $rcv
    */
    
    public function __construct()          
    {
        $this->rcvModel = new RcvSumModel();
        $this->sum = new SummaryModel();
        
        $this->evt = new EventTracking();
        $this->rcv = new RcvSum();
    }
    
    /**
     * test function
    */
    public function testMessage()
    {
        echo 'test';
    }
    
    
    /**
     * Receiving Summary
    */
    
    public function getReceivingSummary()
    {
        $params = [
            'sumTable' => 'rcv_sum',
            'logTable' => 'evt_tracking',
            'field' => 'event_id',
            'evtCode' => config('constants.event.GR-COMPLETE')
        ];
      
        //get the max event id from evt_tracking
        $eventIDs = $this->sum->getEventID($params);


        if ( ! $eventIDs) {
            return;
        }

        //calling batch
        $this->rcvEventsBatch([
            'eventIDs' => $eventIDs,
        ]);

        var_dump('receiving summary table populated');
        
        return;
    }
    
    /**
     * Batch Receiving Events Summary
    */
    
    public function rcvEventsBatch($params) 
    {
        $eventData = $params['eventIDs'];

        if (! $eventData) {
            return;
        }

        $eventIDs = array_splice($eventData, 0, 500);

        $params['eventIDs'] = $eventData;
   
        //querying trans_num from evt_tracking using event_id
        $grHdr = $this->evt
                      ->select('trans_num')
                      ->whereIn('id', $eventIDs)
                      ->get()
                      ->lists('trans_num');

        //get receiving qty
        $rcvResults = $this->rcvModel->getRcvSql($grHdr);

        //get pallet qty 
        $palletResults = $this->rcvModel->getRcvPalletSql($grHdr);

        
        $dataRcvSum = [];

        foreach ($rcvResults as $row) {
            $grHdrID = $row->gr_hdr_id;
            
            $dataRcvSum[] = [
                'whs_id' => $row->whs_id,
                'cus_id' => $row->cus_id,
                'event_id' => $row->event_id,
                'gr_hdr_id' => $row->gr_hdr_id,
                'dt' => $row->created,
                'cntr_val' => $row->container,
                'ctn_val' => $row->carton,
                'pcs_val' => $row->pieces,
                'vol_val' => $row->vol,
                'plt_val' => getDefault($palletResults[$grHdrID],0)
            ];
        }

        DB::beginTransaction();
           
        if (! empty($dataRcvSum)) {
            foreach($dataRcvSum as &$data) {
              
                $grHdrID = $data['gr_hdr_id'];
                $rcvDate = $data['dt'];
                $cusID = $data['cus_id'];
                $whsID = $data['whs_id'];
                
                //check ctnr_id exists in rcv_sum table
                if (! $this->rcvModel->getFirstWhere([
                        'gr_hdr_id' => $grHdrID,
                        'dt' => $rcvDate,
                        'cus_id' => $cusID,
                        'whs_id' => $whsID
                   ]))
                {    
                    // insert new into rcv_sum table
                    $this->rcv->create($data);
                }
            }
        }
        
        DB::commit();  
       
        $this->rcvEventsBatch($params);
    }
    
}
