<?php

namespace App\Jobs;

use App\Api\V1\Models\Log as DbLog;
use App\Api\V1\Models\VasHdrModel;
use App\VasHdr;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\Logs;

class IntegrateFilesWithDMS extends Job
{
    protected $request;
    protected $workOrderId;
    protected $messages;
    protected $vasHdrModel;
    protected $file;

    protected $dmsApi;

    public function __construct($request, $workOrderId, $file)
    {
        $this->request = $request;
        $this->workOrderId = $workOrderId;
        $this->messages = collect();
        $this->vasHdrModel = new VasHdrModel();
        $this->file = $file;
    }

    public function handle()
    {
        try{
            $file = new \SplFileInfo($this->file);
            $input = $this->getInput();
            $odrNums = $this->vasHdrModel->getAllOrderNum($this->workOrderId);
            $workOrder = VasHdr::find($this->workOrderId);

            foreach($odrNums as $odrNum){
                $this->integrateFileWithDMS($file, $odrNum, 'order_doc');
            }

            $this->integrateFileWithDMS($file, $workOrder->vas_num, 'WOR');

            DbLog::info($this->request, $input->get('whs_id'), [
                'evt_code'      => 'INT',
                'owner'         => 'DMS',
                'transaction'   => $input->get('work_order_num'),
                'url_endpoint'  => $this->dmsApi,
                'message'       => $this->messages->toJson()
            ]);

        }catch(\Exception $e){
             DbLog::info($this->request, $input->get('whs_id'), [
                'evt_code'      => 'INT',
                'owner'         => 'DMS',
                'transaction'   => $input->get('work_order_num'),
                'url_endpoint'  => $this->dmsApi,
                'message'       => $e->getMessage()
            ]);
        }
    }

    private function integrateFileWithDMS($file, $odrNum, $docType)
    {
        $fileName = $file->getFilename();
        $fileUrl = env('SYSTEM_BASE_URL') . "/core/invoice-master/public/download-document?type=dms&file_name={$fileName}";

        $client = new Client();

        $this->dmsApi = env('DMS_DOCUMENT_API');

        $res = $client->request('POST', $this->dmsApi, [
            'http_errors' => false,
            'form_params' => [
                'sys'           => env('SYSTEM_NAME', 'wms360.dev'),
                'jwt'           => str_replace('Bearer ', '', $this->request->getHeaders()['authorization'][0]),
                'file_url'      => $fileUrl,
                'sts'           => 1,
                'transaction'   => $odrNum,
                'document_type' => $docType,
                'doc_date'      => date('m/d/Y'),
            ]
        ]);

        $msg = sprintf('%s - %s - %s - %s', $fileUrl, $this->dmsApi, $res->getStatusCode(), $res->getReasonPhrase());
        $this->messages->push([
            $odrNum => $msg
        ]);
    }

    private function getInput(){
        $input = collect($this->request->getQueryParams());
        $input = $input->merge($this->request->getParsedBody());
        return $input;
    }
}
