<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class PackViewDtlValidator
 *
 * @package App\Api\V1\Validators
 */
class PackViewDtlValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'item_id.*'   => 'required|exists:pack_dtl,item_id',
            'pack_hdr_id' => 'exists:pack_dtl,pack_hdr_id',
        ];
    }


}
