<?php

$app->get('/', function () use ($app) {
    return $app->version();
});
$app->group([
    'prefix' => 'v1',
    'namespace' => 'App\Api\V1\Controllers',
], function ($app) {
    $app->get('warehouses/search', 'WarehouseController@search');
    $app->get('warehouses', 'WarehouseController@index');
    $app->get('warehouses/statuses', 'WarehouseController@getStatuses');
    $app->post('warehouses', 'WarehouseController@store');
    $app->get('warehouses/{warehouseId:[0-9]+}', 'WarehouseController@show');
    $app->put('warehouses/{warehouseId:[0-9]+}', 'WarehouseController@edit');
    $app->delete('warehouses/{warehouseId:[0-9]+}', 'WarehouseController@destroy');
    $app->get('warehouses/{warehouseId:[0-9]+}/locations', 'WarehouseController@getLocations');
    $app->get('warehouses/{whsId:[0-9]+}/locations-by-customer/{cusId:[0-9]+}', 'WarehouseController@getLocationsByCustomers');
    $app->get('warehouses/{warehouseId:[0-9]+}/free-locations', 'WarehouseController@getFreeLocations');
    $app->get('warehouses/{warehouseId:[0-9]+}/zone', 'WarehouseController@getZone');
    $app->get('warehouses/inventory-report', 'WarehouseController@invReport');

    //zone
    $app->post('/warehouses/{warehouseId:[0-9]+}/zone', 'ZoneController@store');
    $app->put('/warehouses/{warehouseId:[0-9]+}/zone/{zoneId:[0-9]+}', 'ZoneController@update');
    $app->get('/warehouses/{warehouseId:[0-9]+}/wh-zone/search', 'ZoneController@viewWHSearch');
    $app->get('/warehouses/{warehouseId:[0-9]+}/zone/search', 'ZoneController@search');
    $app->get('/warehouses/{warehouseId:[0-9]+}/zone/{zoneId:[0-9]+}', 'ZoneController@show');
    $app->get('/warehouses/{warehouseId:[0-9]+}/zone/{zoneId:[0-9]+}/locations', 'ZoneController@getLocations');
    $app->post('/warehouses/{warehouseId:[0-9]+}/zone/{zoneId:[0-9]+}/locations', 'ZoneController@addLocations');
    $app->get('/warehouses/free-zones', 'ZoneController@freeZones');

    //Zone Type
    $app->get('/zone-type', 'ZoneTypeController@index');
    $app->post('/zone-type', 'ZoneTypeController@store');
    $app->get('/zone-type/{id:[0-9]+}', 'ZoneTypeController@show');
    $app->put('/zone-type/{id:[0-9]+}', 'ZoneTypeController@update');
    $app->delete('/zone-type/{id:[0-9]+}', 'ZoneTypeController@destroy');

    //Location
    $app->get('/warehouses/{warehouseId:[0-9]+}/wh-locations', 'LocationController@viewWHSearch');
    $app->get('/warehouses/{warehouseId:[0-9]+}/locations', 'LocationController@search');
    $app->post('/warehouses/{warehouseId:[0-9]+}/locations', 'LocationController@store');
    $app->get('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}', 'LocationController@show');
    $app->post('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}/change-status',
        'LocationController@changeStatus');
    $app->get('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}/change-status',
        'LocationController@getChangeStatus');
    $app->put('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}', 'LocationController@update');
    $app->delete('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}', 'LocationController@destroy');
    $app->delete('/warehouses/{warehouseId:[0-9]+}/locations', 'LocationController@destroyMultiple');
    $app->post('/warehouses/{warehouseId:[0-9]+}/locations/validate-import', 'LocationController@validateImportFile');
    $app->get('/locations/get-import-error', 'LocationController@getImportError');
    $app->get('/locations/process-import', 'LocationController@processImport');
    $app->get('/locations/import-status', 'LocationController@getStatusImport');

    //Location Type
    $app->post('/location-type', 'LocationTypeController@store');
    $app->get('/location-type', 'LocationTypeController@index');
    $app->get('/location-type/{id:[0-9]+}', 'LocationTypeController@show');
    $app->put('/location-type/{id:[0-9]+}', 'LocationTypeController@update');
    $app->delete('/location-type/{id:[0-9]+}', 'LocationTypeController@destroy');
    $app->delete('/location-type', 'LocationTypeController@destroyMultiple');

    $app->get('/warehouses/{warehouseId:[0-9]+}/location/{locId:[0-9]+}/pallet/get-pallet', 'LocationController@getPalletInfo');

    $app->get('/whs-layout', 'WarehouseController@listLayout');
    $app->get('/whs-layout/{zone}', 'WarehouseController@showLayout');
    $app->get('/whs-layout/{locId:[0-9]+}/show', 'WarehouseController@showPopup');

    $app->get('/reasons', 'ReasonController@search');
    $app->post('/reasons', 'ReasonController@store');
    $app->put('/reasons/{reasonId:[0-9]+}', 'ReasonController@update');

    //report
    $app->get('/warehouses/{warehouseId:[0-9]+}/report/shipping', 'WarehouseController@shippingReport');
    $app->get('/warehouses/{warehouseId:[0-9]+}/report/order', 'WarehouseController@orderReport');
    $app->get('/warehouses/report/order-status-not-shipping', 'WarehouseController@orderStatusNotShipping');

    // dashboard
    $app->get('/dashboard/asns', ['action' => "viewASN", 'uses' => 'DashboardController@asnDashboard']);

    $app->get('/dashboard/orders',
        ['action' => 'viewOrder', 'uses' => 'DashboardController@odrDashboard']);

    $app->get('/dashboard/order-inprocess',
        ['action' => 'viewOrder', 'uses' => 'DashboardController@inprocessDashboard']);

    $app->get('/dashboard/asn-admin',
        ['action' => "dashboardReceiving", 'uses' => 'DashboardController@asnAdminDashboard']);

    $app->get('/dashboard/occupancy-pct',
        ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@OccupancyPCTDashboard']);

    $app->get('/dashboard/occupancy-pct-status',
        ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@occupancyPCTDashboardStatus']);

    $app->get('/dashboard/order-admin',
        ['action' => 'dashboardOrder', 'uses' => 'DashboardController@odrAdminDashboard']);

    $app->get('/dashboard/order-inprocess-admin',
        ['action' => 'dashboardInprogress', 'uses' => 'DashboardController@inprocessAdminDashboard']);

    $app->get('/dashboard/putaway',
        ['action' => 'putawayDashboard', 'uses' => 'DashboardController@putawayDashboard']);

    $app->get('/dashboard/putaway-admin',
        ['action' => 'dashboardPutaway', 'uses' => 'DashboardController@putawayAdminDashboard']);


    $app->get('/dashboard/room-admin',
        ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@roomAdminDashboard']);


    $app->get('/dashboard/product-admin',
        ['action' => 'dashboardProduct', 'uses' => 'DashboardController@productAdminDashboard']);



    $app->get('/dashboard/wavepick',
        ['action' => 'wavepickDashboard', 'uses' => 'DashboardController@wavepickDashboard']);

    $app->get('/dashboard/goods-receipt',
        ['action' => 'inboundEventTracking', 'uses' => 'DashboardController@goodReceiptDashboard']);

    $app->get('/dashboard/putaway-received',
        ['action' => 'putawayDashboard', 'uses' => 'DashboardController@putawayReceivedDashboard']);

    $app->get('/dashboard/putaway-received',
        ['action' => 'putawayDashboard', 'uses' => 'DashboardController@putawayReceivedDashboard']);

    $app->get('/dashboard/wave-pick',
        ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@wavePickStatisticDashboard']);

    $app->get('/dashboard/zoneCapacity-admin',
        ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@zoneCapaticyAdminDashboard']);

    $app->get('/dashboard/room-admin',
        ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@roomAdminDashboard']);

    $app->get('/dashboard/alert-bar',
        ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@dashboardAlertBar']);


    //refill
    $app->post('refill', [
        'action' => 'createLocation',
        'uses' => 'RefillController@store'
    ]);
    $app->put('refill/{refillId:[0-9]+}', [
        'action' => 'editLocation',
        'uses' => 'RefillController@update'
    ]);
    $app->get('refill/{refillId:[0-9]+}', [
        'action' => 'viewLocation',
        'uses' => 'RefillController@show'
    ]);
    $app->get('refill/location/{locationId:[0-9]+}', [
        'action' => 'viewLocation',
        'uses' => 'RefillController@showRefillAccordingToLocation'
    ]);
    $app->delete('refill/{refillId:[0-9]+}', [
        'action' => 'deleteLocation',
        'uses' => 'RefillController@destroy'
    ]);

});
