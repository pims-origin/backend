<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\GoodsReceipt;

class GoodsReceiptListTransformer extends TransformerAbstract
{
    public function transform(GoodsReceipt $goodsReceipt)
    {

        $countIsDamage = $goodsReceipt->goodsReceiptDetail()->where('gr_dtl_is_dmg', 1)->count();
        $xDock = AsnDtl::where('asn_hdr_id', object_get($goodsReceipt, 'asn_hdr_id', 0))
            ->where('ctnr_id', object_get($goodsReceipt, 'ctnr_id', 0))->sum('asn_dtl_crs_doc');

        return [
            'asn_hdr_num'    => object_get($goodsReceipt, 'asnHdr.asn_hdr_num', null),
            'asn_hdr_ref'    => object_get($goodsReceipt, 'asnHdr.asn_hdr_ref', null),
            'gr_hdr_id'      => object_get($goodsReceipt, 'gr_hdr_id', null),
            'ctnr_num'       => object_get($goodsReceipt, 'container.ctnr_num', ''),
            'gr_hdr_num'     => object_get($goodsReceipt, 'gr_hdr_num', null),
            'gr_sts_name'    => object_get($goodsReceipt, 'goodsReceiptStatus.gr_sts_name'),
            'whs_id'         => object_get($goodsReceipt, 'whs_id', null),
            'cus_id'         => object_get($goodsReceipt, 'cus_id', null),
            'cus_name'       => object_get($goodsReceipt, 'customer.cus_name', null),
            'gr_ctn_ttl'     => $goodsReceipt->goodsReceiptDetail()->sum('gr_dtl_ept_ctn_ttl'),
            "gr_act_ctn_ttl" => $goodsReceipt->goodsReceiptDetail()->sum('gr_dtl_act_ctn_ttl'),
            "gr_crs_doc"     => $xDock,
            "gr_dtl_is_dmg"  => $countIsDamage == 0 ? $countIsDamage : 1,
            "user_id"        => $goodsReceipt->created_by,
            "user_name"      => trim(object_get($goodsReceipt, "user.first_name", null) . " " .
                object_get($goodsReceipt, "user.last_name", null))
        ];
    }
}
