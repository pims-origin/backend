<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 1-Sep-16
 * Time: 09:25
 */

namespace App\Api\V2\PalletAssignment\Models;

use Seldat\Wms2\Models\OutPallet;


/**
 * Class OutPalletModel
 *
 * @package App\Api\V1\Models
 */
class OutPalletModel extends AbstractModel
{
    /**
     * @param OutPallet $model
     */
    public function __construct(OutPallet $model = null)
    {
        $this->model = ($model) ?: new OutPallet();
    }

    /**
     * Generate new wo number
     *
     * @return string
     */
    public function generateLPNNum($odrNum, $str)
    {
        $currentYearMonth = date('ym');
        $defaultWoNum = "LPN-${currentYearMonth}-00001";

        $lastPL = $this->model->orderBy('plt_id', 'desc')->first();
        $lastLPN = object_get($lastPL, 'plt_num', '');

        if (empty($lastLPN) || strpos($lastLPN, "-${currentYearMonth}-") === false) {
            return $defaultWoNum;
        }

        return ++$lastLPN;
    }


}
