<?php

namespace App\Api\Ecom\Validators;


class PackValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'whs_id' => 'required|integer|exists:warehouse,whs_id'
        ];

    }


}
