<?php

namespace App\Api\V1\Traits;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Transformers\GoodsReceiptDetailPalletTransformer;
use App\Api\V1\Transformers\GoodsReceiptDetailPalletTransformer2;
use App\Api\V1\Transformers\PalletPalletByRFIDTransformer;
use App\Api\V1\Validators\PalletTransferValidator;
use App\Api\V1\Validators\RFIDValidate;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\Zone;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\Status;
use mPDF;

trait PalletControllerTrait
{
    /**
     * @param $goodsReceiptId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function showCartonsPallet($whsId, $grHdrId, Request $request)
    {
        try {
            $input = $request->getQueryParams();

            $grHdr = $this->goodsReceiptModel->getGoodsReceiptById($grHdrId);

            if (!$grHdr) {
                throw new \Exception(Message::get("BM017", "Goods Receipt"));
            }
            $createdAt = $grHdr->created_at->timestamp;
            $fromTime = strtotime(date('Y-m-01', $createdAt));
            $endOfMonth = strtotime(date('Y-m-t 23:59:59', $createdAt))-18000;
            $toTime = strtotime("+1 month", $endOfMonth);
            $rangeTime = [$fromTime, $toTime];
            $limit = array_get($input, 'limit', 20);
            $pallets = $this->palletModel->getPalletSupportMixSku($whsId, $grHdrId, $rangeTime, $limit);
            if($pallets->count() == 0) {
                $pallets = $this->cartonModel->getPalletSupportMixSku($whsId, $grHdrId, $limit);
                return $this->response->paginator($pallets, new GoodsReceiptDetailPalletTransformer2());
            }
            else

            return $this->response->paginator($pallets, new GoodsReceiptDetailPalletTransformer());

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function printPalletLPN(Request $request, $grHdrId)
    {
        $input = $request->getParsedBody();

        if(empty($input['lpns'])) {
            return $this->response->errorBadRequest("LPN(s) is required!");
        }

        $grHdr = $this->goodsReceiptModel->getGoodsReceiptById($grHdrId);

        if (!$grHdr) {
            throw new \Exception(Message::get("BM017", "Goods Receipt"));
        }

        $this->printPalletLPNPdfFile($input['lpns']);

        return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);
    }

    private function printPalletLPNPdfFile(
        $lpns
    ) {
        //$pdf = new Mpdf();
        $pdf = new Mpdf(
            '',    // mode - default ''
            [152.4, 101.6],    // format - A4, for example, default '',(WxH)( 6 inch = 152.4 mm
            // X 4 inch = 101.6 mm X)
            0,     // font size - default 0
            '',    // default font family
            5,    // margin_left
            5,    // margin right
            30,     // margin top
            10,    // margin bottom
            1,     // margin header
            1,     // margin footer
            'p' // L - landscape, P - portrait
        );

        $html = (string)view('PalletLPNListPrintoutTemplate', [
            'lpns' => $lpns
        ]);

        $pdf->WriteHTML($html);
        $pdf->Output("PalletLPN_".substr(time(), -6).".pdf", "D");
    }

    public function updateCartonsOnPallet($whsId, $grHdrId, Request $request)
    {
        $input = $request->getParsedBody();
        $pltId = array_get($input, 'plt_id', null);
        $grDtlId = array_get($input, 'gr_dtl_id', null);
        $ctnTtl = (int)array_get($input, 'ctn_ttl', 0);
        $lpn = array_get($input, 'plt_num', null);

        try {
            $grHdr = $this->goodsReceiptModel->getGoodsReceiptById($grHdrId);
            if (!$grHdr) {
                return $this->response->errorBadRequest(Message::get("BM017", "Goods Receipt"));
            }

            if ($grHdr->gr_sts == 'RE') {
                return $this->response->errorBadRequest("Goods Receipt is already Received");
            }

//            if ($grHdr->created_from != 'WAP') {
//                return $this->response->errorBadRequest("Goods Receipt has not been created from RF GUN");
//            }

//            if ($ctnTtl > 999) {
//                return $this->response->errorBadRequest("CTNS Total must be smaller than 999!");
//            }

            $grDtl = $this->goodsReceiptDetailModel->getFirstWhere(['gr_dtl_id' => $grDtlId]);

            if (!$grDtl) {
                return $this->response->errorBadRequest(Message::get("BM017", "Goods Receipt Detail"));
            }

            $pallet = $this->palletModel->getFirstBy('plt_id', $pltId);

            if (empty($pallet)) {
                return $this->response->errorBadRequest(Message::get("BM017", "Pallet"));
            }

//            $checkSpcHdlCode = DB::table("cartons")->join("item","item.item_id","=","cartons.item_id")
//                                ->where("cartons.plt_id", $pltId)->whereIn("cartons.ctn_sts", ['AC', 'RG'])
//                                ->where("cartons.deleted", 0)->select("spc_hdl_code")->first();
//            if(!empty($checkSpcHdlCode['spc_hdl_code'])) {
//                if($checkSpcHdlCode['spc_hdl_code'] == "BIN" && $ctnTtl > 50) {
//                    return $this->response->errorBadRequest("CTNS Total must be smaller than 50 for BIN type Item!");
//                }
//
//                if($checkSpcHdlCode['spc_hdl_code'] == "SHE" && $ctnTtl > 10) {
//                    return $this->response->errorBadRequest("CTNS Total must be smaller than 50 for SHELF type Item!");
//                }
//            }
//
//            $checkSpcHdlCode = DB::table("cartons")->where("cartons.gr_hdr_id", "<>", $grHdrId)
//                ->where("cartons.plt_id", $pltId)->whereIn("cartons.ctn_sts", ['AC', 'RG'])
//                ->where("cartons.deleted", 0)->select("spc_hdl_code")->first();
//            if(!empty($checkSpcHdlCode)) {
//                return $this->response->errorBadRequest("This Pallet have Item of another Goods Receipt!");
//            }

            if ($pallet->is_xdock) {
                throw new \Exception('Pallet is cross dock!');
            }

            if ($pallet->dmg_ttl > 0 && $ctnTtl != 0) {
                throw new \Exception('Some cartons are damaged on this pallet. CTNS cannot be updated.');
            }

            // start transaction
            DB::beginTransaction();

            if (!$ctnTtl) {
                $carton = $this->cartonModel->deleteCartonsByPltIdAndGrDtlId($grHdr, $pallet, $grDtl, $ctnTtl, $lpn);
            } else {
                $carton = $this->cartonModel->updateCartonsByPltIdAndGrDtlId($grHdr, $pallet, $grDtl, $ctnTtl, $lpn);
            }

            $ctnTtlNew = (int)$ctnTtl - (int)array_get($input, 'curr_ctn_ttl', 0);
            //$this->inventorySummaryModel->updateInvtSmrByItemId($whsId, $pallet->cus_id, $itemId, $lot, $ctnTtlPack);

            $grDtl->gr_dtl_act_ctn_ttl += (int)$ctnTtlNew;
            if ($grDtl->gr_dtl_act_ctn_ttl <= 0) {
                $grDtl->gr_dtl_act_ctn_ttl = 0;
            }

            $grDtl->gr_dtl_act_qty_ttl = $grDtl->gr_dtl_act_ctn_ttl * $grDtl->pack;
            $disQty = $grDtl->gr_dtl_act_qty_ttl - $grDtl->gr_dtl_ept_qty_ttl;
            $grDtl->gr_dtl_disc_qty = $disQty;
            $grDtl->gr_dtl_disc = $disQty;


            $grDtl->save();

            $isKg = ($grDtl->uom_code == 'KG');

            $cartonTotal = $this->cartonModel->getCartonTotalInPallet($pallet);
            $pallet->ctn_ttl = $isKg ? 1 : $cartonTotal;
            if ($cartonTotal === 0) {
                $pallet->plt_sts = 'IA';
                $pallet->zero_date = time();
                $pallet->storage_duration = 0;
                $pallet->rfid = null;
                $pallet->loc_id = null;
                $pallet->loc_code = null;
                $pallet->deleted_at = time();
                $pallet->deleted = 1;
                DB::table('pal_sug_loc')->where('plt_id', $pallet->plt_id)->where('gr_hdr_id', $grHdrId)->update([
                    'deleted_at' =>time(),
                    'deleted'    => 1,
                ]);
            }

            if ($isKg) {
                $pallet->pack = $ctnTtl;
            }
            $pallet->save();


            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $whsId,
                'cus_id'    => $pallet->cus_id,
                'owner'     => $grHdr->gr_hdr_num,
                'evt_code'  => 'GEP',
                'trans_num' => $pallet->plt_num,
                'info'      => sprintf('Change Carton total %d to %d', (int)$carton['ctn_ttl_old'], (int)$ctnTtl)
            ]);

            $this->updateGrDtlAfterAll($grDtl, $grHdr);

            DB::commit();

            return [
                'message' => 'Updated Cartons to pallet successfully',
                'status'  => true
            ];

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function updateCurrentQtyPallet($whsId, $grHdrId, Request $request)
    {
        $input = $request->getParsedBody();
        $pltId = array_get($input, 'plt_id', null);
        $grDtlId = array_get($input, 'gr_dtl_id', null);
        $ctnTtl = (int)array_get($input, 'ctn_ttl', 0);
        $currentQty = (float)array_get($input, 'cur_qty', 0);

        $ctnTtl = empty($ctnTtl) ? $ctnTtl : 1;

        try {
            $grHdr = $this->goodsReceiptModel->getGoodsReceiptById($grHdrId);
            if (!$grHdr) {
                return $this->response->errorBadRequest(Message::get("BM017", "Goods Receipt"));
            }

            if ($grHdr->gr_sts == 'RE') {
                throw new \Exception('Goods Receipt is already Received');
            }

            if ($grHdr->created_from != 'WAP') {
                throw new \Exception('Goods Receipt has not been created from RF GUN');
            }

            $grDtl = $this->goodsReceiptDetailModel->getFirstWhere(['gr_dtl_id' => $grDtlId]);

            if (!$grDtl) {
                return $this->response->errorBadRequest(Message::get("BM017", "Goods Receipt Detail"));
            }

            if ($grDtl->uom_code != 'KG') {
                throw new \Exception('UOM of pallet not must KG');
            }

            $pallet = $this->palletModel->getFirstWhere(['plt_id' => $pltId]);

            if (empty($pallet)) {
                return $this->response->errorBadRequest(Message::get("BM017", "Pallet"));
            }

            if ($pallet->is_xdock) {
                throw new \Exception('Pallet is cross dock!');
            }

            if ($pallet->dmg_ttl > 0 && $ctnTtl != 0) {
                throw new \Exception('Some cartons are damaged on this pallet. QTY cannot be updated.');
            }

            // start transaction
            DB::beginTransaction();

            if ($pallet->ctn_ttl == 0 || $currentQty == 0) {
                $pallet->plt_sts = 'IA';
                $pallet->zero_date = time();
                $pallet->storage_duration = 0;
                $pallet->rfid = null;
                $pallet->loc_id = null;
                $pallet->loc_code = null;
                $pallet->loc_name = null;
            }

            // init_piece_ttl Old
            $init_piece_ttl = $pallet->init_piece_ttl;

            $pallet->pack = $ctnTtl;
            $pallet->weight = $currentQty;
            $pallet->init_piece_ttl = $currentQty;

            $pallet->save();

            $carton = $this->cartonModel->getFirstWhere(['plt_id' => $pltId]);
            if (empty($carton)) {
                return $this->response->errorBadRequest(Message::get("BM017", "Carton"));
            }

            //  Update Cartons
            $carton->piece_remain = $currentQty;
            $carton->piece_ttl = $currentQty;
            $carton->inner_pack = $ctnTtl;
            $carton->weight = $currentQty;
            $carton->save();

            $ctnTtlNew = $ctnTtl - 1;

            $qtyTtlNew = $currentQty - $init_piece_ttl;

            $grDtl->gr_dtl_act_ctn_ttl += (int)$ctnTtlNew;
            //$grDtl->gr_dtl_act_qty_ttl = $grDtl->gr_dtl_act_ctn_ttl * $grDtl->pack;
            $grDtl->gr_dtl_act_qty_ttl += $qtyTtlNew;

            if ($grDtl->gr_dtl_act_ctn_ttl < 0) {
                $grDtl->gr_dtl_act_ctn_ttl = 0;
            }
            if ($grDtl->gr_dtl_act_qty_ttl < 0) {
                $grDtl->gr_dtl_act_qty_ttl = 0;
            }

            $grDtl->gr_dtl_disc_qty = $grDtl->gr_dtl_act_qty_ttl - $grDtl->gr_dtl_ept_qty_ttl;

            $grDtl->save();

            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $whsId,
                'cus_id'    => $pallet->cus_id,
                'owner'     => $grHdr->gr_hdr_num,
                'evt_code'  => 'GEP',
                'trans_num' => $pallet->plt_num,
                'info'      => sprintf('Change Qty total %d to %d', $init_piece_ttl, $currentQty)
            ]);

            $this->updateGrDtlAfterAll($grDtl);

            DB::commit();

            return [
                'message' => 'Updated Cartons to pallet successfully',
                'status'  => true
            ];

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * This function is created for fix bug: wrong ttl carton, SWIS-614 when update CTNS on tab Pallet
     *
     * @param Object $grDtl
     */
    private function updateGrDtlAfterAll($grDtl, $grHdr)
    {
//        $ttlCtn = \Seldat\Wms2\Models\Pallet::where([
//            'gr_dtl_id' => object_get($grDtl, 'gr_dtl_id')
//        ])
//            ->select(DB::raw("sum(ctn_ttl) as ttl"))
//            ->first();

        $ttlCtn = \Seldat\Wms2\Models\Carton::where([
            'gr_dtl_id' => object_get($grDtl, 'gr_dtl_id'),
            'whs_id' => object_get($grHdr, 'whs_id'),
            'cus_id' => object_get($grHdr, 'cus_id')
        ])->where('ctn_sts', 'RG')->count();

        $grDtl->gr_dtl_act_ctn_ttl = $ttlCtn ?? 0;
        $grDtl->gr_dtl_disc = $grDtl->gr_dtl_act_ctn_ttl - $grDtl->gr_dtl_ept_ctn_ttl;
        $grDtl->save();
    }

    /**
     * Update expired date api
     *
     * @param Request $request
     * @param Integer $pltId
     * @param Integer $grDtlId
     *
     * @return Response
     */
    public function updateExpiredDate(Request $request, $pltId, $grDtlId)
    {

        $input = $request->getParsedBody();

        $grDtlId = array_get($input, 'gr_dtl_id', $grDtlId);
        try {

            $grHdr = $this->goodsReceiptModel->getGrHdrByGrDtlId($grDtlId);

            if (!$grHdr) {
                throw new \Exception(Message::get("BM017", "Goods Receipt"));
            }

            if ($grHdr->gr_sts == 'RE') {
                throw new \Exception('Goods Receipt is already Received');
            }

            if ($grHdr->created_from != 'WAP') {
                throw new \Exception('Goods Receipt has not been created from RF GUN');
            }

            $grDtl = $this->goodsReceiptDetailModel->getFirstWhere(['gr_dtl_id' => $grDtlId]);
            if (!$grDtl) {
                return $this->response->errorBadRequest(Message::get("BM017", "Goods Receipt Detail"));
            }

            $pallet = $this->palletModel->getFirstWhere(['plt_id' => $pltId]);
            if (!$pallet) {
                return $this->response->errorBadRequest(Message::get("BM017", "Pallet"));
            }

            if ($pallet->dmg_ttl > 0) {
                throw new \Exception('Some cartons are damaged on this pallet. Expired date cannot be updated.');
            }

            if ($grDtl->crs_doc > 0) {
                throw new \Exception('Goods Receipt Detail is xdoc!');
            }

            DB::beginTransaction();
            $expDate = array_get($input, 'expired_date', null);
            $expDate = !empty($expDate) ? strtotime($expDate) : null;
            $pallet = $this->palletModel->getFirstWhere([
                'plt_id'    => $pltId,
                'gr_dtl_id' => $grDtlId
            ]);

            $expired_date_old = $pallet->expired_date;

            $pallet->expired_date = $expDate;
            $pallet->save();

            if (!$pallet) {
                DB::rollback();

                return $this->response->errorBadRequest("Some cartons are expired date on this pallet. Expired  date cannot be updated.");
            }

            $carton = $this->cartonModel
                ->updateWhere([
                    'expired_dt' => $expDate
                ], [
                    'plt_id'    => $pltId,
                    'gr_dtl_id' => $grDtlId
                ]);

            if (!$carton) {
                DB::rollback();

                return $this->response->errorBadRequest("Some cartons are expired date on this pallet. Expired  date cannot be updated.");
            }

            $expired_date_old = $expired_date_old ?? $grDtl->expired_dt;
            $expired_date_old = $expired_date_old ? date('Y-m-d', $expired_date_old) : null;

            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $grHdr->whs_id,
                'cus_id'    => $grHdr->cus_id,
                'owner'     => $grHdr->gr_hdr_num,
                'evt_code'  => 'GEP',
                'trans_num' => $pallet->plt_num,
                'info'      => sprintf('Change expired date %s to %s', $expired_date_old,
                    array_get($input, 'expired_date', null))
            ]);

            DB::commit();

            return $this->response->item($pallet, new PalletTransformer());
        } catch (Exception $ex) {
            DB::rollback();

            return $this->response->errorBadRequest($ex->getMessage());
        }
    }

    public function getPalletByRFID
    (
        $whsId,
        Request $request,
        PalletPalletByRFIDTransformer $palletPalletByRFIDTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            $pallet = $this->palletModel->getPalletByRFID($whsId, $input, [

            ], array_get($input, 'limit'));

            return $this->response->paginator($pallet, $palletPalletByRFIDTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * [transferPallet description]
     *
     * @param  Request $request [description]
     * @param  [type]                  $whsId                   [description]
     * @param  PalletModel $palletModel [description]
     * @param  CartonModel $cartonModel [description]
     * @param  PalletTransferValidator $palletTransferValidator [description]
     *
     * @return [type]                                           [description]
     */
    public function transferPallet(
        Request $request,
        $whsId,
        PalletModel $palletModel,
        CartonModel $cartonModel,
        PalletTransferValidator $palletTransferValidator
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $palletTransferValidator->validate($input);

        //check from pallet
        $fromPltId = $input['plt_id'];

        $checkFromPallet = $palletModel->getFirstWhere(['plt_id' => $fromPltId]);
        if (empty($checkFromPallet)) {
            throw new \Exception(Message::get("BM017", "Pallet From"));
        }
        if ($checkFromPallet->plt_sts != 'AC') {
            throw new \Exception('Status of Pallet From is not Active!');
        }

        $fromPalletInfo = $palletModel->checkPalletByPltId($whsId, $fromPltId);
        if (!$fromPalletInfo) {
            return $this->response->errorBadRequest("The Pallet From don't have any carton");
        }

        //check to pallet
        $rfid = $input['rfid'];
        $toPalletInfo = $palletModel->checkPalletByRFID($whsId, $rfid);
        if ($toPalletInfo) {
            return $this->response->errorBadRequest("The Pallet To already has carton(s)");
        }

        //Check Pallet Type
        $pltRFIDValid = new RFIDValidate($rfid, RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            return $this->response->errorBadRequest($pltRFIDValid->error);
        }

        try {
            DB::beginTransaction();
            //create pallet num
            $newPalletNum = $palletModel->generatePalletNumFromAvailiable($fromPalletInfo->plt_num);
            //update old pallet
            $fromPalletParam = [
                'plt_id'           => $fromPltId,
                'rfid'             => null,
                'ctn_ttl'          => 0,
                'dmg_ttl'          => 0,
                'loc_id'           => null,
                'loc_name'         => null,
                'loc_code'         => null,
                'plt_sts'          => 'TF',
                'zero_date'        => time(),
                'storage_duration' => DB::raw('IF(DATEDIFF(FROM_UNIXTIME(updated_at), FROM_UNIXTIME(created_at)),
                        DATEDIFF(FROM_UNIXTIME(updated_at), FROM_UNIXTIME(created_at)), 1)'),
                'spc_hdl_code'     => null,
            ];

            //create new pallet
            $newPalletParam = $fromPalletInfo->toArray();
            if (isset($newPalletParam['whs_id'])) {
                $newPalletParam['whs_id'] = $whsId;
            }
            if (isset($newPalletParam['plt_num'])) {
                $newPalletParam['plt_num'] = $newPalletNum;
            }
            if (isset($newPalletParam['rfid'])) {
                $newPalletParam['rfid'] = $rfid;
            }
            if (isset($newPalletParam['created_at'])) {
                unset($newPalletParam['created_at']);
            }
            if (isset($newPalletParam['updated_at'])) {
                unset($newPalletParam['updated_at']);
            }
            if (isset($newPalletParam['updated_by'])) {
                unset($newPalletParam['updated_by']);
            }
            if (isset($newPalletParam['created_by'])) {
                unset($newPalletParam['created_by']);
            }
            $palletModel->update($fromPalletParam);
            $newPallet = $palletModel->create($newPalletParam);
            //update all cartons of from pallet to new pallet id
            $cartonModel->updateWhere([
                "plt_id" => $newPallet->plt_id,
            ], [
                "plt_id" => $fromPltId,
                "whs_id" => $whsId,
            ]);
            DB::commit();

            return ["data" => "successful"];

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest("The current server is busy, please wait a few minutes and try again");
    }

    public function updateCurrentLocationOnPallet(Request $request, $whsId, $grHdrId)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $plt_Id = $input['plt_id'] ?? '';
        $loc_code = $input['loc_code'] ?? '';

        try {
            $grHdr = $this->goodsReceiptModel->getFirstBy('gr_hdr_id', $grHdrId);

            if (!$grHdr) {
                return $this->response->errorBadRequest("The Goods Receipt is not existed!");
            }

            if ($grHdr->gr_sts == 'RE') {
                return $this->response->errorBadRequest("Goods Receipt is already Received");
            }

            $pallet = $this->palletModel->getFirstBy('plt_id' , $plt_Id);
            if (!$pallet) {
                return $this->response->errorBadRequest("The pallet is not existed!");
            }

            $cartonNotInGR = $this->cartonModel->getModel()->where("plt_id", $pallet->plt_id)
                ->whereIn("ctn_sts",["AC",'RG'])->where("gr_hdr_id","<>" ,$grHdrId)->whereNotNull("gr_hdr_id")->first();
            if(!empty($cartonNotInGR)) {
                return $this->response->errorBadRequest("The pallet has item of another Goods receipt!");
            }

//            if ($pallet->loc_id) {
//                return $this->response->errorBadRequest("The pallet has been existed location!");
//            }

            $location = $this->locationModel->getModel()
                ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
                ->where([
                    'loc_type.loc_type_code' => 'RAC',
                    'location.loc_sts_code'  => 'AC',
                    'location.deleted'       => 0,
                    'location.loc_whs_id'       => $whsId,
                    'location.loc_code'      => $loc_code,
                ])->first();
            if (!$location) {
                return $this->response->errorBadRequest("The location is not existed!");
            }

            $locExCartons = $this->cartonModel->getModel()->where("loc_id", $location->loc_id)
                ->whereIn("ctn_sts",["AC",'RG'])->first();

            if ($locExCartons) {
                return $this->response->errorBadRequest("Location has been existed cartons");
            }

            // 21-2-2019 check spc_hdl_code

//            $carton = $this->cartonModel->getModel()->where("plt_id", $pallet->plt_id)
//                ->whereIn("ctn_sts",["AC",'RG'])->select("item_id")->first();
//            if(!empty($carton)) {
//                $item = $this->itemModel->getFirstWhere(['item_id' => $carton->item_id]);
//                if($item->spc_hdl_code != $location->spc_hdl_code) {
//                    return $this->response->errorBadRequest("The Item in this Pallet has special handle code different with the item in this Location.");
//                }
//            }

//            if(in_array($location->spc_hdl_code,['FRE','SHE','BIN'])) {
//                $count_car_loc = $this->cartonModel->getModel()->where("loc_id", $location->loc_id)
//                    ->whereIn("ctn_sts",["AC",'RG'])->count();
//                $count_car_plt = $this->cartonModel->getModel()->where("plt_id", $pallet->plt_id)
//                    ->where("ctn_sts",'RG')->where("gr_hdr_id", $grHdrId)->count();
//                $total = $count_car_loc + $count_car_plt;
//                if($total > $location->max_lpn) {
//                    return $this->response->errorBadRequest("Location is not enought space for this pallet.");
//                }
//            }
            DB::beginTransaction();

            if(!empty($pallet->loc_id)) {
                $OldLocation = $this->locationModel->getModel()->where("loc_id", $pallet->loc_id)->first();
                if(!empty($OldLocation->loc_zone_id)) {
                    $zone = DB::table("zone")
                        ->join("zone_type","zone_type.zone_type_id","=","zone.zone_type_id")
                        ->where("zone_id", $OldLocation->loc_zone_id)->select("zone_type.zone_type_code")->first();

                    if($zone['zone_type_code'] === "DZ") {
                        $OldLocation->loc_zone_id = null;
                    }
                    $OldLocation->loc_sts_code = "AC";
                    $OldLocation->save();
                }
            }

            //Check Dynamic Zone
            if (is_null($location->loc_zone_id)) {

                $zoneId = $this->getDynZone($whsId, $grHdr->cus_id);
                if (!$zoneId) {
                    return ["is_error" => 1,"msg" => "This customer don't support dynamic zone."];
                }

                $location->loc_zone_id = $zoneId;
                $location->save();

                if($location->is_block_stack == 1) {
                    // Update zone to all children location
                    $this->getModel()->where("parent_id", $location->loc_id)->update(["loc_zone_id" => $zoneId]);
                    $location_child = $this->getFirstChildEmpty($location);
                    if(!empty($location_child)) {
                        $location = $location_child;
                    }
                }


                $zone = DB::table("zone")->where("zone_id", $zoneId)->first();
                $zone_num_of_loc = $zone['zone_num_of_loc'] + 1;
                DB::table("zone")->where("zone_id", $location->loc_zone_id)->update(['zone_num_of_loc'=> $zone_num_of_loc]);
            } else {
                $check_zone = DB::table("customer_zone")
                    ->where("zone_id", $location->loc_zone_id)
                    ->where("cus_id", $grHdr->cus_id)
                    ->where("deleted", 0)
                    ->first();
                if(empty($check_zone)) {
                    $msg = sprintf("%s is assigned a Zone but not belonged to this customer.",$location->loc_code);
                    return ["is_error" => 1,"msg" => $msg];
                }

//                if($location->is_block_stack == 1) {
//                    $location_child = $this->getFirstChildEmpty($location);
//                    if(!empty($location_child)) {
//                        $location = $location_child;
//                    }
//                }
            }

            if(in_array($location->spc_hdl_code,['SHE','BIN'])) {
                $this->putawayBinShe($location,$pallet,$grHdr);
            } else {
                $this->putawayRack($location,$pallet,$grHdr);
            }

            DB::commit();

            return ["data" => "Updated pallet to location successfully"];

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function putawayBinShe($location,$pallet,$grHdr) {
        $locExPallet = $this->palletModel->getFirstBy('loc_id', $location->loc_id);
        if(empty($locExPallet)) {
            $plt_num = $this->palletModel->generatePltNum();
            $locExPallet = $this->palletModel->create([
                "plt_num"   => $plt_num,
                "loc_id"    => $location->loc_id,
                "loc_code"  => $location->loc_code,
                "whs_id"    => $grHdr->whs_id,
                "cus_id"    => $grHdr->cus_id,
                "ctn_ttl"   => 0,
                "plt_sts"   => "RG",
                "loc_name"  => $location->loc_code]);
        }
        $location->loc_sts_code = "RG";
        $location->save();

        DB::table('cartons')
            ->where('plt_id', $pallet->plt_id)
            ->whereIn('ctn_sts', ['AC', 'RG'])
            ->where("gr_hdr_id", $grHdr->gr_hdr_id)
            ->update([
                'loc_id'        => $location->loc_id,
                'loc_code'      => $location->loc_code,
                'loc_name'      => $location->loc_alternative_name,
                "plt_id"        => $locExPallet->plt_id,
                'loc_type_code' => 'RAC',
            ]);

        DB::table('pal_sug_loc')
            ->where('plt_id', $pallet->plt_id)
            ->update([
                'act_loc_id'    => $location->loc_id,
                'act_loc_code'  => $location->loc_code,
                "plt_id"        => $locExPallet->plt_id,
                'updated_at'    => time(),
                'updated_by'    => Data::getCurrentUserId()
            ]);

        $check = DB::table('cartons')->where('plt_id', $pallet->plt_id)
                ->where("ctn_sts", 'RG')->first();
        if(empty($check)) {
            $pallet->plt_sts = "AC";
            $pallet->save();
        }

        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->create([
            'whs_id'    => $grHdr->whs_id,
            'cus_id'    => $grHdr->cus_id,
            'owner'     => $grHdr->gr_hdr_num,
            'evt_code'  => 'GEP',
            'trans_num' => $pallet->plt_num,
            'info'      => 'Update pallet to location'
        ]);

    }

    protected function putawayRack($location, $pallet, $grHdr){
        $pallet->loc_id = $location->loc_id;
        $pallet->loc_code = $location->loc_code;
        $pallet->loc_name = $location->loc_alternative_name;
        $pallet->save();

        $location->loc_sts_code = "RG";
        $location->save();

        DB::table('cartons')
            ->where('plt_id', $pallet->plt_id)
            ->whereIn('ctn_sts', ['AC', 'RG'])
            ->where("gr_hdr_id", $grHdr->gr_hdr_id)
            ->update([
                'loc_id'        => $location->loc_id,
                'loc_code'      => $location->loc_code,
                'loc_name'      => $location->loc_alternative_name,
                'loc_type_code' => 'RAC',
            ]);

        DB::table('pal_sug_loc')
            ->where('plt_id', $pallet->plt_id)
            ->update([
                'act_loc_id'    => $location->loc_id,
                'act_loc_code'  => $location->loc_code,
                'updated_at'    => time(),
                'updated_by'    => Data::getCurrentUserId()
            ]);

        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->create([
            'whs_id'    => $grHdr->whs_id,
            'cus_id'    => $grHdr->cus_id,
            'owner'     => $grHdr->gr_hdr_num,
            'evt_code'  => 'GEP',
            'trans_num' => $pallet->plt_num,
            'info'      => 'Update pallet to location'
        ]);
    }

    public function autoAvailLocbyRackType(Request $request, $whsId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        // get data from HTTP
        $input = $request->getQueryParams();
        $loc_code = $input['loc_code'] ?? '';

        try {

            if (empty($loc_code)) {
                return ['data' => []];
            }

            $query = $this->locationModel->getModel()
                ->select('location.loc_code')
                ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
                ->where([
                    'loc_type.loc_type_code' => 'RAC',
                    'location.loc_sts_code'  => 'AC',
                    'location.deleted'       => 0,
                    'location.loc_whs_id'    => $whsId
                ])
                ->where('location.loc_code', 'LIKE', "%" . trim($loc_code) . "%")
                ->whereNull('location.parent_id')
                ->whereRaw(' NOT EXISTS (SELECT * FROM pallet WHERE pallet.loc_id = location.loc_id)');

            $location = $query->orderBy('location.loc_code')->take(10)->get();

            return ['data' => $location];

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function getDynZone($whsId, $cusId)
    {
        /*
         * Check config Dynamic zone customer
         */
//        $chkDynZone = DB::table('cus_config')->where([
//            "whs_id"      => $whsId,
//            "cus_id"      => $cusId,
//            "config_name" => "DNZ",
//            "ac"          => "Y",
//        ])->value('config_value');
//
//        if (empty($chkDynZone)) {
//            return false;
//        }

        $customerObj = Customer::where('cus_id', $cusId)->first();

        $zoneCode = $customerObj->cus_code . "-D";
        $zoneName = $customerObj->cus_code . " - Dynamic Zone";
        $zoneObj = Zone::where('zone_code', $zoneCode)->where("zone_whs_id", $whsId)->first();

        if ($zoneObj) {
            $zoneObj->zone_num_of_loc += 1;
            $zoneObj->save();

            return $zoneObj->zone_id;
        }

        $colorCode = DB::table('customer_color')->where('cus_id', $cusId)
            ->value('cl_code');

        $arrData = [
            "zone_name"       => $zoneName,
            "zone_code"       => $zoneCode,
            "zone_whs_id"     => $whsId,
            "zone_type_id"    => 5,
            "dynamic"         => 1,
            "zone_min_count"  => 1,
            "zone_max_count"  => 100,
            "zone_color"      => $colorCode ?? '#ffffff',
            "zone_num_of_loc" => 1,
            "created_at"      => time(),
            "updated_at"      => time(),
            "created_by"      => Data::getCurrentUserId(),
            "updated_by"      => Data::getCurrentUserId(),
            "deleted_at"      => 915148800,
            "deleted"         => 0
        ];

        $dZone = DB::table('zone')
            ->where('zone_name', $zoneName)
            ->where('zone_whs_id', $whsId)
            ->where('zone_type_id', 5)
            ->where('dynamic', 1)
            ->where('deleted', 0)
            ->first();

        if(empty($dZone)) {
            $zoneId = DB::table('zone')->insertGetId($arrData);

            //add customer_zone
            DB::table('customer_zone')->insert([
                'zone_id'    => $zoneId,
                'cus_id'     => $cusId,
                'created_at' => time(),
                'updated_at' => time(),
                "created_by" => Data::getCurrentUserId(),
                "updated_by" => Data::getCurrentUserId(),
                "deleted_at" => 915148800,
                'deleted'    => 0
            ]);
        }else{
            $zoneId = $dZone['zone_id'];
        }

        

        return $zoneId;
    }

    public function getFirstChildEmpty($location) {
        return Location::where("parent_id", $location->loc_id)
            ->whereRaw(
                '0 = (SELECT COUNT(pallet.loc_id) FROM pallet WHERE pallet.loc_id = location.loc_id AND pallet.loc_id IS NOT NULL)'
            )
            ->where("is_block_stack",1)
            ->orderBy("plt_lv","DESC")
            ->first()
            ;
    }
}
