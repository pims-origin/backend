<?php

namespace App\Api\Ecom\Controllers;

use App\Api\Ecom\Models\PackDtlModel;
use App\Api\Ecom\Models\WaveHdrModel;
use App\Api\Ecom\Transformers\PackingTransformer;
use App\Api\Ecom\Validators\PackDetailValidator;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\Tote;
use Seldat\Wms2\Utils\SystemBug;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PackController
 *
 * @package App\Api\V1\Controllers
 */
class PackingController extends AbstractController
{
    protected $waveHdrModel;

    protected $orderHdrModel;

    protected $orderDtlModel;

    protected $packDtlModel;

    public function __construct(
        WaveHdrModel $waveHdrModel,
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        PackDtlModel $packDtlModel
    ) {
        $this->waveHdrModel  = $waveHdrModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->packDtlModel  = $packDtlModel;
    }

    public function search($whsId, Request $request, PackingTransformer $packingTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            //get list wavepick with odr_sts is Picked(PN) or Packing(PN)
            $packingList = $this->waveHdrModel->getModel()
                ->join('odr_hdr', 'odr_hdr.wv_id', '=', 'wv_hdr.wv_id')
                ->where('wv_hdr.whs_id', $whsId)
                ->where('wv_hdr.is_ecom', 1)
                ->where('odr_hdr.whs_id', $whsId)
                ->where('odr_hdr.is_ecom', 1)
                ->whereIn('odr_hdr.odr_sts', ['PD', 'PN'])
                ->orderBy('odr_hdr.updated_at', 'desc')
                ->paginate(array_get($input, 'limit', 10));

            return $this->response->paginator($packingList, $packingTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show ($whsId, $num, Request $request)
    {
        try {
            //validate and get list odr_dtl_id
            $odrHdr = $this->orderHdrModel->getModel()
                ->with(['details'])
                ->where('whs_id', $whsId)
                ->where('is_ecom', 1)
                ->where(function ($query) use ($num) {
                    $query->where('odr_num', $num)
                        ->orWhere('wv_num', $num);
                })->first();

            if (empty($odrHdr)) {
                return $this->response->errorBadRequest($num." is not exists!");
            }

            if ($odrHdr->odr_sts =='PA') {
                return $this->response->created('Order Packing', ['message' => 'Order has been packed!']);
            }


            // Order status must be in Picked(PN) or Packing(PN)
            if (!in_array($odrHdr->odr_sts, ['PD', 'PN'])) {
                return $this->response->errorBadRequest("Only Picked and Packing Order can be packed");
            }

            if (empty($odrHdr->details)) {
                return $this->response->errorBadRequest($num." details are not exists!");
            }

            $input['odr_dtl_id'] = implode(',', array_pluck($odrHdr->details, 'odr_dtl_id'));

            //call API get list item to pack
            $client = new Client();

            $response = $client->get(env('API_ECOM_PACK') . 'packs/' . $odrHdr->odr_id . '/details?odr_dtl_id=' . $input['odr_dtl_id'],
                [
                    'headers'     => ['Authorization' => $request->getHeader('Authorization')]
                ]
            );

            $packs = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

            $odrDtls = $packs['data']['items'];

            $items = [];
            foreach ($odrDtls as $odrDtl) {
                $sku_size_color = '';

                if (!empty($odrDtl['sku'])) {
                    $sku_size_color .= $odrDtl['sku'];
                }
                if (!empty($odrDtl['size']) && $odrDtl['size'] != 'NA') {
                    $sku_size_color .= '-'.$odrDtl['size'];
                }
                if (!empty($odrDtl['color']) && $odrDtl['color'] != 'NA') {
                    $sku_size_color .= '-'.$odrDtl['color'];
                }

                $item = (new Item())->where('item_id', $odrDtl['itm_id'])->first();

                $items[] = [
                    "odr_dtl_id"     => $odrDtl['odr_dtl_id'],
                    "pack_dtl_id"    => $odrDtl['pack_dtl_id'],
                    "itm_id"         => $odrDtl['itm_id'],
                    "sku_size_color" => $sku_size_color,
                    "sku"            => $odrDtl['sku'],
                    "color"          => $odrDtl['color'],
                    "size"           => $odrDtl['size'],
                    "pack"           => $odrDtl['pack'],
                    "lot"            => $odrDtl['lot'],
                    "requested"      => $odrDtl['requested'],
                    "piece_qty"      => $odrDtl['piece_qty'],
                    "remain_qty"     => $odrDtl['remain_qty'],
                    "qty"            => ($odrDtl['requested'] - $odrDtl['remain_qty']) .'/'.$odrDtl['requested'],
                    "ship_track_id"  => $odrDtl['ship_track_id'],
                    'cus_upc'        => $item->cus_upc,
                    'ucc128'         => Item::generateUpc128($item->cus_id, $item->item_id)
                ];
            }

            return [
                'data' => [
                    'odr_id'  => $odrHdr->odr_id,
                    'odr_num' => $odrHdr->odr_num,
                    'wv_id'   => $odrHdr->wv_id,
                    'wv_num'  => $odrHdr->wv_num,
                    'items'   => $items
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (RequestException $reqEx) {
            if ($reqEx->hasResponse()) {
                $jsonResponse = \GuzzleHttp\json_decode($reqEx->getResponse()->getBody()->getContents(), true);

                return $this->response->errorBadRequest($jsonResponse['errors']['message']);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function store($whsId, $orderHdrId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderHdrId);

        if (empty($orderHdr)) {
            return $this->response->errorBadRequest("Order is not exists!");
        }

        $param = $input;
        $param['number_of_pack'] = 1;
        $param['whs_id'] = $whsId;
        $param['cus_id'] = $orderHdr->cus_id;

        try {
            $client = new Client();

            $response = $client->post(env('API_ECOM_PACK') . 'packs/' . $orderHdrId,
                [
                    'headers'     => ['Authorization' => $request->getHeader('Authorization')],
                    'form_params' => $param
                ]
            );

            $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderHdrId);
            if($orderHdr->odr_sts == 'PA'){
                $this->removeWavepickToteRelation($orderHdr);
            }

            $result = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
            return $result;

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (RequestException $reqEx) {
            if ($reqEx->hasResponse()) {
                $jsonResponse = \GuzzleHttp\json_decode($reqEx->getResponse()->getBody()->getContents(), true);

                return $this->response->errorBadRequest($jsonResponse['errors']['message']);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function removeWavepickToteRelation($orderHdr)
    {
        $rows = DB::table('odr_cartons')
                ->where('odr_hdr_id', $orderHdr->odr_id)
                ->groupBy('odr_hdr_id')
                ->get();

        if ( !$rows->count() ){
            return;
        }

        $toteIds = $rows->pluck('tote_id')->toArray();

        Tote::whereIn('tote_id', $toteIds)->update([
            'wv_id'     => NULL,
            'picker_id' => NULL
        ]);
    }

    public function packRef($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $client = new Client();

            $response = $client->get(env('API_ECOM_PACK') . 'pack-ref',
                [
                    'headers'     => ['Authorization' => $request->getHeader('Authorization')],
                    'form_params' => $input
                ]
            );

            $result = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
            return $result;

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (RequestException $reqEx) {
            if ($reqEx->hasResponse()) {
                $jsonResponse = \GuzzleHttp\json_decode($reqEx->getResponse()->getBody()->getContents(), true);

                return $this->response->errorBadRequest($jsonResponse['errors']['message']);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}

