<?php

namespace App\Api\V1\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;

/**
 * Class BaseController
 *
 * @package App\Api\V1\Controllers
 *
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="seldat.seldat-wms20.warehouse.dev",
 *     basePath="/",
 *     definitions={},
 *     @SWG\Info(
 *         version="WMS 2.0.0",
 *         title="Warehouse Configuration APIs",
 *         description="Total Warehouse Configuration APIs",
 *         termsOfService="",
 *         @SWG\Contact(
 *             name="APIs support team."
 *         ),
 *         @SWG\License(
 *             name="WMS2.0",
 *             url="seldatinc.com"
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="data",
 *         type="object",
 *         properties={
 *             @SWG\Property(property="whs_id", type="string", description=""),
 *             @SWG\Property(property="whs_name", type="string", description=""),
 *             @SWG\Property(property="whs_status", type="string", description=""),
 *             @SWG\Property(property="whs_short_name", type="string", description=""),
 *         },
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */
abstract class AbstractController extends Controller
{
    use Helpers;

    public function __construct()
    {

    }

    protected function _loadBy($request, $inputWanted, $model, $transformer)
    {
        $input = $request->getParsedBody();
        $with = array_get($input, 'with', []);

        foreach ($inputWanted as $inputName) {
            if (!empty($input[$inputName])) {
                $params[$inputName] = $input[$inputName];
            }
        }

        try {
            if (empty($params)) {
                return $this->response->noContent();
            }

            $data = $model->loadByMany($params, $with);

            return $this->response->collection($data, $transformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
