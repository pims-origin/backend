<?php
namespace Seldat\Wms2\Models;

/**
 * Class SourceMessage
 *
 * @package Seldat\Wms2\Models
 */
class SourceMessage extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'source_message';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'category',
        'message'
    ];

    public function Message()
    {
        return $this->hasOne(__NAMESPACE__ . '\Message', 'id', 'id');
    }
}
