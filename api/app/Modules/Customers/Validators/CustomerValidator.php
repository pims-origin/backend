<?php

namespace App\Modules\Customers\Validators;

use Validator;

class CustomerValidator
{
    public function rules()
    {
        
    }

    public function validate($input)
    {
        $validator = Validator::make($input, $this->rules(), $this->messages());

        return $validator;
    }

    public function messages() {
        
    }

    public function checkCustomer($customer) {

        if(empty($customer)) {
           throw new \Exception(json_encode(['title' =>'Customer is not existed.', 'detail' => 'Please try']));
        }
    }
}