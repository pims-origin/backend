<?php

return [
    "driver" 	=> env('MAIL_DRIVER', 'smtp'),
    "host" 		=> env('MAIL_HOST', 'smtp.mailtrap.io'),
    "port" 		=> env('MAIL_PORT', 2525),
    "from" 		=> array(
        "address" 	=> env('MAIL_FROM_ADDRESS', null),
        "name" 		=> env('MAIL_FROM_NAME', 'SELDAT')
    ),
    "username" 	=> env('MAIL_USERNAME', null),
    "password" 	=> env('MAIL_PASSWORD', null),
    "sendmail" 	=> env('MAIL_SENDMAIL', '/usr/sbin/sendmail -bs'),
    "pretend" 	=> env('MAIL_PRETEND', false),
    "encryption"=> env('MAIL_ENCRYPTION', 'tls')
];