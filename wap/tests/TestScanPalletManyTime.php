<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\AsnDtlModel;
use Dingo\Api\Routing\Helpers;
use App\Api\V1\Transformers\AsnListsCanSortV1Transformer;
use Seldat\Wms2\Models\VirtualCarton;

class TestScanPalletManyTime extends TestCase
{
    //use DatabaseTransactions;
    use Helpers;

    private $whsId = 1;
    private $cusId = 31;
    private $palletId = 1;
    private $DB_ROLLBACK = true;
    public static $_caseCreateVTCarton = [];
    private $_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0ODkwMjMxMzIsImV4cCI6MTQ4OTEwOTUzMiwibmFtZSI6IkRhbyBUcmFuIiwidXNlcm5hbWUiOiJjc3JkdHIiLCJqdGkiOjc3fQ.mgAVy8ULskl6BAR-hcdl9FgJOt1_8PLSAVHCJg8w6P4";

    public function setUp()
    {
        parent::setUp();
        
    }

    /**
     * Test Scan pallet many time with adding more carton
     */
    public function testCaseScanPalletManyTime()
    {
        /*
         * Get data to test
         */
        $asnModel = new AsnDtlModel();
        $input['whs_id'] = $this->whsId;
        $return = $asnModel->getListASN(
                $input, null
        );
        $obj = $return->getCollection()->first();
        $tran = new AsnListsCanSortV1Transformer();
        $data = $tran->transform($obj);

        ////////////
        $asnHdrId = $data['asn_hdr_id'];
        //check them item co hay ko
        $asnDtlId = $data['items'][0]['asn_dtl_id'];
        $itemId = $data['items'][0]['item_id'];
        $ctnrId = $data['ctnr_id'];
        $cusId = $data['cus_id'];
        $vtlCtnOnPallet = [];
        for ($i = 0; $i < 4; $i++) {
            $vtlCtnOnPallet[] = strtoupper(uniqid("DD-"));
        }
        /////
        $params = [
            'asn_hdr_id' => $asnHdrId,
            'asn_dtl_id' => $asnDtlId,
            'ctn_rfid.0' => '',
            'ctn_rfid' => "",
            'ctns_rfid' => $vtlCtnOnPallet,
            'gate_code' => '',
            'type' => 1,
            'item_id' => $itemId,
            'ctnr_id' => $ctnrId,
                //'discrepancy' => '',
        ];
        
        $url = "/inbound/whs/{$this->whsId}/cus/{$cusId}/add-virtual-cartons";
        echo "[RURL] 1.{$url}\n";
        $response = $this->call("POST", $url, $params, [], []
                , ['HTTP_Authorization' => $this->_token]
        );
        $content = $response->content();
        $jsonObj = json_decode($content);
        echo "[CONT] {$content}\n";
        if (!isset($jsonObj->data) || !isset($jsonObj->data->status) || $jsonObj->data->status != 1) {
            $this->fail("[FAIL] Step 1 insert carton unsuccessfully!\n");
        }
        
        //Second call
        $pltRfid = uniqid("FF-");
        $ctnFailRfid = array_pop($vtlCtnOnPallet);
        $param = [
            'pallet.ctn-rfid' => $vtlCtnOnPallet,
            'pallet.pallet-rfid' => $pltRfid
        ];
        $url = "/inbound/whs/{$this->whsId}/scan-pallet/";
        echo "[RURL] 2.{$url}\n";
        echo "[DATA] ".json_encode($param)."\n";
        $this->refreshApplication();
        $res = $this->call('PUT', $url, $param);
        echo "[CONT] {$res->content()}\n";
        
        //CAll 3
        array_push($vtlCtnOnPallet, $ctnFailRfid);
        $param['pallet.ctn-rfid'] = $vtlCtnOnPallet;
        $this->refreshApplication();
        echo "[RURL] 3.{$url}\n";
        echo "[DATA] ".json_encode($param)."\n";
        $res2 = $this->call('PUT', $url, $param);
        echo "[CONT] {$res2->content()}\n";
        $numVtlCtn = VirtualCarton::where([
                    'plt_rfid' => $pltRfid
        ])->get();
        
        $expectNum = count($vtlCtnOnPallet);
        $reallyNum = $numVtlCtn->count();
        $this->assertEquals($expectNum, $reallyNum, "[FAIL] Num Carton not the same!\n");
    }

    public function testSkuHasActualQtyLessThanExpectedQty()
    {
        echo "[CASE] SKU actual quantity less than expected Quantity";
        //get ASN
        $obj = $this->getASNInfo();
        if (!$obj) {
            echo "[SKIP] No ANS to test\n";
            echo "[DONE] testSkuHasActualQtyGreaterThanExpectedQty_OK\n";
            $this->markTestSkipped("[SKIP] No ANS to test\n");
        }

        $ranNum = rand(-5, -1);
        $content = $this->createVirtualCarton($obj, $ranNum);

        echo "[RURL] 1.{$content['url']}\n";
        echo "[CONT] {$content['content']}\n";
        $expectTtlCtl = $content['expectTtlCtl'];
        $receivedTtlCtl = $expectTtlCtl + $ranNum;

        $jsonObj = json_decode($content['content']);

        if (!isset($jsonObj->data) && !isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            echo "[ERROR] ".$jsonObj->message ."\n";
            $this->fail("[FAIL] Step 1 insert carton unsuccessfully!\n");
        }

        $vtlCtnOnPallet = $content['vtlCtnOnPallet'];
        $content = $this->createPallet($vtlCtnOnPallet);
        if (!isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            $this->fail("[FAIL] Step 2 create pallet unsuccessfully!\n");
        }
        echo "[RURL] 2.{$content['url']}\n";
        echo "[DATA] ".$content['data']."\n";
        echo "[CONT] {$content['content']}\n";

        $jsonObj = json_decode($content['content']);

        if (isset($jsonObj->status) && $jsonObj->status) {
            $this->assertTrue($jsonObj->status, '[FAIL] Result is false: ' . $jsonObj->message);
            $this->assertLessThan($expectTtlCtl, $receivedTtlCtl, 'ASN expect carton num: '.$expectTtlCtl." An received :". $receivedTtlCtl);
        } else {
            $this->fail("[FAIL] Unknown structure: {$content['content']}\n");
        }

        echo "=============END THIS CASE=============\n";
    }

    public function testSkuHasActualQtyGreaterThanExpectedQty()
    {
        echo "[CASE] SKU actual quantity greater than expected Quantity";
        //get ASN
        $obj = $this->getASNInfo();
        if (!$obj) {
            echo "[SKIP] No ANS to test\n";
            echo "[DONE] testSkuHasActualQtyGreaterThanExpectedQty_OK\n";
            $this->markTestSkipped("[SKIP] No ANS to test\n");
        }

        $ranNum = rand(1, 5);
        $content = $this->createVirtualCarton($obj, $ranNum);

        echo "[RURL] 1.{$content['url']}\n";
        echo "[CONT] {$content['content']}\n";
        $expectTtlCtl = $content['expectTtlCtl'];
        $receivedTtlCtl = $expectTtlCtl + $ranNum;

        $jsonObj = json_decode($content['content']);
        if (!isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            echo "[ERROR] ".$jsonObj->data->message ."\n";
            $this->fail("[FAIL] Step 1 insert carton unsuccessfully!\n");
        }

        $vtlCtnOnPallet = $content['vtlCtnOnPallet'];
        $content = $this->createPallet($vtlCtnOnPallet);
        if (!isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            $this->fail("[FAIL] Step 2 create pallet unsuccessfully!\n");
        }
        echo "[RURL] 2.{$content['url']}\n";
        echo "[DATA] ".$content['data']."\n";
        echo "[CONT] {$content['content']}\n";

        $jsonObj = json_decode($content['content']);

        if (isset($jsonObj->status) && $jsonObj->status) {
            $this->assertTrue($jsonObj->status, '[FAIL] Result is false: ' . $jsonObj->message);
            $this->assertGreaterThan($expectTtlCtl, $receivedTtlCtl, 'ASN expect carton num: '.$expectTtlCtl." An received :". $receivedTtlCtl);
        } else {
            $this->fail("[FAIL] Unknown structure: {$content['content']}\n");
        }

        echo "=============END THIS CASE=============\n";
    }

    public function testSkuHasActualQtyGreaterThanExpectedQty_CompleteSku()
    {
        echo "[CASE] SKU actual quantity greater than expected Quantity";
        //get ASN
        $obj = $this->getASNInfo();
        if (!$obj) {
            echo "[SKIP] No ANS to test\n";
            echo "[DONE] testSkuHasActualQtyGreaterThanExpectedQty_OK\n";
            $this->markTestSkipped("[SKIP] No ANS to test\n");
        }

        $ranNum = rand(1, 5);
        $content = $this->createVirtualCarton($obj, $ranNum);

        echo "[RURL] 1.{$content['url']}\n";
        echo "[CONT] {$content['content']}\n";
        $expectTtlCtl = $content['expectTtlCtl'];
        $receivedTtlCtl = $expectTtlCtl + $ranNum;

        $jsonObj = json_decode($content['content']);
        if (!isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            echo "[ERROR] ".$jsonObj->data->message ."\n";
            $this->fail("[FAIL] Step 1 insert carton unsuccessfully!\n");
        }

        //complete SKU
        $tran = new AsnListsCanSortV1Transformer();
        $data = $tran->transform($obj);
        $asnDtlId = $data['items'][0]['asn_dtl_id'];
        $skuContent = $this->completeSKU($asnDtlId);
        $jsonObj = json_decode($skuContent);
        if (!isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            $this->fail("[FAIL] Step 2 create pallet unsuccessfully!\n");
        }

        $vtlCtnOnPallet = $content['vtlCtnOnPallet'];
        $content = $this->createPallet($vtlCtnOnPallet);
        if (!isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            $this->fail("[FAIL] Step 3 create pallet unsuccessfully!\n");
        }
        echo "[RURL] 2.{$content['url']}\n";
        echo "[DATA] ".$content['data']."\n";
        echo "[CONT] {$content['content']}\n";

        $jsonObj = json_decode($content['content']);

        if (isset($jsonObj->status) && $jsonObj->gr-status) {
            $this->assertTrue($jsonObj->gr-status, '[FAIL] Result is false: ' . $jsonObj->message);
            $this->assertGreaterThan($expectTtlCtl, $receivedTtlCtl, 'ASN expect carton num: '.$expectTtlCtl." An received :". $receivedTtlCtl);
        } else {
            $this->fail("[FAIL] Unknown structure: {$content['content']}\n");
        }

        echo "=============END THIS CASE=============\n";
    }

    private function getASNInfo()
    {
        $asnModel = new AsnDtlModel();
        $input['whs_id'] = $this->whsId;
        $return = $asnModel->getListASN(
            $input, null
        );
        $obj = $return->getCollection()->first();
        return $obj;
    }

    private function createVirtualCarton($obj, $ranNum)
    {
        $tran = new AsnListsCanSortV1Transformer();
        $data = $tran->transform($obj);

        $asnHdrId = $data['asn_hdr_id'];

        //check them item co hay ko
        $asnDtlId = $data['items'][0]['asn_dtl_id'];
        $itemId = $data['items'][0]['item_id'];
        $ctnrId = $data['ctnr_id'];
        $cusId = $data['cus_id'];
        $vtlCtnOnPallet = [];

        $expectTtlCtl = $data['items'][0]['asn_dtl_ctn_ttl'];

        for ($i = 0; $i < $expectTtlCtl + $ranNum; $i++) {
            $vtlCtnOnPallet[] = strtoupper(uniqid("CTN-"));
        }

        $params = [
            'asn_hdr_id' => $asnHdrId,
            'asn_dtl_id' => $asnDtlId,
            'type' => 1,
            'item_id' => $itemId,
            'ctnr_id' => $ctnrId,
            'ctns_rfid' => $vtlCtnOnPallet,
        ];

        $url = "/inbound/whs/{$this->whsId}/cus/{$cusId}/add-virtual-cartons";

        $response = $this->call("POST", $url, $params, [], []
            , ['HTTP_Authorization' => $this->_token]
        );
        $content = $response->content();

        return [
            'content' => $content,
            'url' => $url,
            'vtlCtnOnPallet' => $vtlCtnOnPallet,
            'expectTtlCtl' => $expectTtlCtl,
        ];
    }

    private function createPallet($vtlCtnOnPallet) {
        //Second call
        $pltRfid = strtoupper(uniqid("FF-"));
        $param = [
            'pallet.ctn-rfid' => $vtlCtnOnPallet,
            'pallet.pallet-rfid' => $pltRfid
        ];

        $url = "/inbound/whs/{$this->whsId}/scan-pallet/";
        $this->refreshApplication();
        $res = $this->call('PUT', $url, $param);
        $content = $res->content();

        return [
            'url' => $url,
            'data' => json_encode($param),
            'content' => $content,
        ];
    }

    private function completeSKU($asnDtlId) {
        $url = "/inbound/whs/{$this->whsId}/cus/{$this->cusId}/asn-detail/{$asnDtlId}/complete-sku";
        $this->refreshApplication();
        $response = $this->call("PUT", $url);
        $content = $response->content();
        return $content;
    }


}
