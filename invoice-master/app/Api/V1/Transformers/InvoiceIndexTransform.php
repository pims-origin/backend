<?php

namespace App\Api\V1\Transformers;

use App\InvoiceHeader;
use League\Fractal\TransformerAbstract;

class InvoiceIndexTransform extends TransformerAbstract
{

    public function transform(InvoiceHeader $invoiceHeader)
    {
        return [
            'inv_id'        => $invoiceHeader->inv_id,
            'inv_num'       => $invoiceHeader->inv_num,
            'cus_name'      => $invoiceHeader->cus_name,
            'billing_to'    => $invoiceHeader->bill_to_add1 ?? $invoiceHeader->bill_to_add2,
            'inv_sts'       => $invoiceHeader->inv_sts_name,
            'email_sts'     => $invoiceHeader->email_sts_name,
            'issue_date'    => $invoiceHeader->issue_dt_format,
            'due_date'      => $invoiceHeader->due_dt_format,
            'period'        => $invoiceHeader->period,
            'created_at'    => $invoiceHeader->created_at->format('Y-m-d'),
            'created_name'  => $invoiceHeader->created_name,
            'approved_name' => $invoiceHeader->approved_name,
            'approved_by'   => $invoiceHeader->approved_by,
            'approved_dt'   => $invoiceHeader->approved_date_format ?? '',
            'amount'        => $invoiceHeader->total,
            'amount_due'    => $this->getAmountDueDate($invoiceHeader),
            'paid_dt'       => $invoiceHeader->paid_dt_format,
            'paid_amount'   => $invoiceHeader->paid_amount,
            'balance_amount'=> $invoiceHeader->balance_amount,
            'currency'      => $invoiceHeader->currency ?? 'USD',
            'overdue'       => self::isOverDue($invoiceHeader) ? 1 : 0,
            'ask_when_reopen'=> $invoiceHeader->ask_when_reopen,
            'ask_when_reject'=> $invoiceHeader->ask_when_reject,
            'send_to_customer'=> $invoiceHeader->send_to_customer,
            'customer_email' => $invoiceHeader->customer_email
        ];
    }

    private function getAmountDueDate($invoiceHeader)
    {
        $date = date('Y-m-d');

        return ($invoiceHeader->due_dt >= strtotime($date)) ? $invoiceHeader->total : 0;
    }

    public function isOverDue($invoiceHeader)
    {
        if ($invoiceHeader->inv_sts == 'AP' && $invoiceHeader->due_dt_format < date('Y-m-d')) {
            return true;
        }

        return false;
    }
}
