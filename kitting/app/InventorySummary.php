<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use Illuminate\Support\Facades\DB;

class InventorySummary extends BaseInvoiceModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invt_smr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'inv_sum_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'item_id',
        'cus_id',
        'whs_id',
        'color',
        'size',
        'lot',
        'lot',
        'ttl',
        'allocated_qty',
        'picked_qty',
        'dmg_qty',
        'sku',
        'avail',
        'upc',
		'crs_doc_qty',
        'ecom_qty',
		'back_qty',
		'lock_qty'

    ];

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
	
	//------------------------------Utils functions-----------------------------------
	public static function generateInvtItem($sku, $size, $color, $pack, $lot, $whsId, $cusId)
    {
        $item = Item::generateItem($sku, $size, $color, $pack, $cusId);

        if (empty($item)) {
            return false;
        }


        $invt = self::where([
            'item_id'  => $item->item_id,
            'whs_id'   => $whsId,
            'cus_id'   => $cusId,
            'lot'      => $lot
        ])->first();

        if (empty($invt)) {
            $invt = new static();
			$invt->setIncrementing(true); //return id
            $invt->item_id = $item->item_id;
            $invt->cus_id = $cusId;
            $invt->whs_id = $whsId;

            $invt->sku = $sku;
            $invt->size = $size;
            $invt->color = $color;
            $invt->lot = $lot;
            $invt->upc = $item->cus_upc;

            $invt->ttl = 0;
            $invt->save();
        }

        return $invt;
    }

}
