<?php

namespace App\Api\V3\Inbound\ScanPallet\Validators;


class WaveValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'whs_id' => 'required|integer|exists:warehouse,whs_id'
        ];

    }


}
