<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ChargeTypeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * ChargeTypeTest constructor.
     */

    public function getEndPoint()
    {
        return "/v1/charge-types";
    }


    protected $searchParams = [
        'chg_type_code' => 'abc',
        'chg_type_name' => 'n21113',
    ];

    protected $chargeTypeParams = [
        'chg_type_code' => 'c0D3',
        'chg_type_name' => 'chg_type_name',
        'chg_type_des'  => 'this is desc',
    ];

    public function setUp()
    {
        parent::setUp();
    }

    public function testSearchChargeType_Ok()
    {
        $this->call('GET', $this->getEndPoint(), $this->searchParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_NoParam_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testRead_Ok()
    {
        $chargeTypeId = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;

        $this->call('GET', $this->getEndPoint() . "/{$chargeTypeId}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $this->chargeTypeParams['chg_type_code'] = str_random(2);
        $this->call('POST', $this->getEndPoint(), $this->chargeTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    public function testStore_EmptyField_Fail()
    {
        unset($this->chargeTypeParams['chg_type_code']);
        $this->call('POST', $this->getEndPoint(), $this->chargeTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        $chg_type_id = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;
        $this->chargeTypeParams['chg_type_code'] = str_random(2);

        $this->call('PUT', $this->getEndPoint() . "/{$chg_type_id}", $this->chargeTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_NotExist_Fail()
    {
        $this->call('PUT', $this->getEndPoint() . "/999999", $this->chargeTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

//    public function testUpdate_ExistUniqueFields_Fail()
//    {
//        $chargeType = factory(\Seldat\Wms2\Models\ChargeType::class)->create();
//        $this->chargeTypeParams['chg_type_code'] = $chargeType->chg_type_code;
//
//        $chg_type_id = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;
//        $this->call('PUT', $this->getEndPoint() . "/{$chg_type_id}", $this->chargeTypeParams);
//
//        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
//    }

    public function testUpdate_EmptyField_Fail()
    {
        $chg_type_id = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;

        unset($this->chargeTypeParams['chg_type_code']);
        $this->call('PUT', $this->getEndPoint() . "/{$chg_type_id}", $this->chargeTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDelete_Ok()
    {
        $chg_type_id = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;

        $this->call('DELETE', $this->getEndPoint() . "/{$chg_type_id}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDelete_NotExit_Fail()
    {
        $this->call('DELETE', $this->getEndPoint() . "/9999999999999999");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $chg_type_id1 = '';
        while (empty($chg_type_id1)) {
            $chg_type_id1 = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;
        }

        $chg_type_id2 = '';
        while (empty($chg_type_id2)) {
            $chg_type_id2 = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;
        }

        $chg_type_id3 = '';
        while (empty($chg_type_id3)) {
            $chg_type_id3 = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;
        }

        $chg_type_id4 = '';
        while (empty($chg_type_id4)) {
            $chg_type_id4 = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;
        }
        $chg_type_id5 = '';
        while (empty($chg_type_id5)) {
            $chg_type_id5 = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;
        }

        $this->call('DELETE', $this->getEndPoint(),
            ['chg_type_id' => [$chg_type_id1, $chg_type_id2, $chg_type_id3, $chg_type_id4, $chg_type_id5]]);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }
}