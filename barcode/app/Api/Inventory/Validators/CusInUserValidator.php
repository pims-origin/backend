<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31/10/2016
 * Time: 17:10
 */

namespace App\Api\Inventory\Validators;

class CusInUserValidator extends AbstractValidator
{

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer|exists:users,user_id',
            'cus_id' => 'required|integer|exists:customer,cus_id'
        ];
    }
}