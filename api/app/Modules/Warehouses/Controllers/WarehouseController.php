<?php

namespace App\Modules\Warehouses\Controllers;

use App\Modules\Warehouses\Models\UserModel;
use App\Modules\Warehouses\Models\WarehouseModel;
use App\Modules\Warehouses\Resources\WarehouseDetailtResource;
use App\Modules\Warehouses\Resources\WarehouseResource;
use App\Modules\Warehouses\Resources\WarehouseCollection;
use App\Modules\Warehouses\Validators\WarehouseValidator;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;


class WarehouseController extends Controller
{
    protected $whsModel;

    public function __construct(WarehouseModel $whsModel)
    {
        $this->whsModel = $whsModel;
    }


    public function  index(Request $request) {
        $per_page = (array_get($request, 'per_page', 10));
        $user_id = UserModel::getCurrentUserId();
        $baseUrl = str_replace( $request->root() , env('APP_URL'), \URL::current());
        $warehouses = $this->whsModel->getListWarehouse($user_id, $per_page, $baseUrl);
        return new WarehouseCollection($warehouses);
    }

    public function  show($whsId, Request $request) {
        $whsId = array_get($request->all(),'whs_id', $whsId);
        $validator = new WarehouseValidator();
        $user_id = UserModel::getCurrentUserId();
        $validator->checkWhsExist($whsId);
        $warehouseData = $this->whsModel->getWarehouseById($user_id, $whsId);
        return new WarehouseDetailtResource($warehouseData);
    }

}
