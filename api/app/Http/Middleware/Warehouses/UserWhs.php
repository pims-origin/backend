<?php
namespace App\Http\Middleware\Warehouses;

use Illuminate\Database\Eloquent\Model;

class UserWhs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_whs';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = ['user_id', 'whs_id'];

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'whs_id',
    ];
}
