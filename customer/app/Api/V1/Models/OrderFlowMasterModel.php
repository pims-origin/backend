<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\OrderFlowMaster;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class OrderFlowMasterModel extends AbstractModel
{
    /**
     * @param OrderFlowMaster $model
     */
    public function __construct(OrderFlowMaster $model = null)
    {
        $this->model = ($model) ?: new OrderFlowMaster();
    }


}