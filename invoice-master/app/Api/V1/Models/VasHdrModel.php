<?php

namespace App\Api\V1\Models;

use App\VasHdr;
use App\Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelStr;

class VasHdrModel extends AbstractModel
{
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new VasHdr();
    }

    /*
     * Get next vas_num
     * */
    public function getVasNum() {
        $query = $this->getModel();
        //filter by whs
        $redis = new Data();
        $whsId = $redis->getCurrentWhs();
        $latestVasNum = $query->select('vas_num')
                    ->where('vas_num', 'like', 'WOR-' . date('ym') . '%')
                    ->where('vas_hdr.whs_id', $whsId)
                    ->orderBy('vas_id', 'desc')
                    ->first();
        $nextIndex = 1;
        if (!empty($latestVasNum)) {
            $latestVasNum = $latestVasNum['vas_num'];
            $currentIndex = (int)substr($latestVasNum, -5);
            $nextIndex = $currentIndex + 1;
        }
        return 'WOR-' . date('ym') . '-' . str_pad($nextIndex, 5, '0', STR_PAD_LEFT);
    }

    public function search($input, $with = [], $limit = 10) {
        $query = $this->make($with);
        $query->select([
            'vas_hdr.*',
            'customer.cus_name',
            'odr_hdr.odr_num',
            DB::raw('COUNT(DISTINCT ref_num) as ref_qty'),
            DB::raw('COUNT(ref_num) as item_qty'),
        ]);
        $query->join('customer','vas_hdr.cus_id','=','customer.cus_id');
        $query->join('vas_dtl','vas_hdr.vas_id','=','vas_dtl.vas_id');
        $query->join('users','vas_hdr.created_by','=','users.user_id');
        $query->leftJoin('odr_hdr','vas_hdr.odr_hdr_id','=','odr_hdr.odr_id');
        if (!empty($input['cus_id'])) {
            $query->where('vas_hdr.cus_id', $input['cus_id']);
        }
        if (!empty($input['vas_num'])) {
            $query->where('vas_hdr.vas_num', 'like', "%" . SelStr::escapeLike($input['vas_num']) . "%");
        }
        if (!empty($input['ref_num'])) {
            $query->whereHas('vasDtl', function ($query) use($input) {
                $query->where('vas_dtl.ref_num', 'like', "%" . SelStr::escapeLike($input['ref_num']) . "%");
            });
        }
        if (!empty($input['created_at'])) {
            $fromDate = strtotime($input['created_at']);
            $toDate = strtotime($input['created_at'] . '23:59:59');
            $query->whereBetween('vas_hdr.created_at', [$fromDate, $toDate]);
        }
        if (!empty($input['vas_sts']) && isset(config('constants.vas_hdr_sts')[strtoupper($input['vas_sts'])])) {
            $query->where('vas_hdr.vas_sts', config('constants.vas_hdr_sts')[strtoupper($input['vas_sts'])]);
        }
        //rush order
        if (isset($input['is_rush'])) {
            if ($input['is_rush'] == 'true') {
                $query->join('odr_hdr', function ($queryJoin){
                    $queryJoin->on('vas_dtl.ref_num', '=', 'odr_hdr.odr_num');
                    $queryJoin->where('rush_odr','=', 1);
                });
            }
        }
        //filter by whs
        $redis = new Data();
        $whsId = $redis->getCurrentWhs();
        $query->where('vas_hdr.whs_id', $whsId);
        $query->where('vas_dtl.deleted', 0);
        $query->groupBy('vas_hdr.vas_id');
        if (isset($input['sort'])) {
            foreach ($input['sort'] as $key => $val) {
                if ($key === 'cus_po') {
                    $input['sort']['odr_hdr.cus_po'] = $val;
                    unset($input['sort'][$key]);
                }
                if ($key === 'cus_odr_num') {
                    $attributes['sort']['odr_hdr.cus_odr_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

            }
        }
        $this->sortBuilder($query, $input);
        $query->orderBy('vas_hdr.updated_at', 'desc');
        return $query->paginate($limit);
    }

    public function getAllOrderNum(int $vasId){
        $orderNums = [];

        $workOrder = $this->model
                    ->with('vasDtlOrder')
                    ->where('vas_id', $vasId)
                    ->first();

        if ($workOrder){
            $orderNums = $workOrder->vasDtlOrder
                            ->pluck('ref_num')
                            ->unique()
                            ->values();
        }

        return $orderNums;
    }

    /**
     * Delete WoHdr by vas_id
     * @param int $vasId
     * */
    public function forceDeleteByVasId($vasId) {
        $this->model->where('vas_id', $vasId)->forceDelete();
    }
}

