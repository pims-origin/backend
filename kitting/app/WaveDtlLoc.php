<?php
namespace App;

/**
 * Class WaveDtlLoc
 *
 * @package Seldat\Wms2\Models
 */
class WaveDtlLoc extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_dtl_loc';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_dtl_loc_id';
    /**
     * @var array
     */
    protected $fillable = [
        'wv_dtl_id',
        'wv_id',
        'sug_loc_ids',
        'act_loc_ids',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function waveHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\WavepickHdr', 'wv_id', 'wv_id');
    }

    public function waveDtl()
    {
        return $this->hasOne(__NAMESPACE__ . '\WavepickDtl', 'wv_dtl_id', 'wv_dtl_id');
    }
}
