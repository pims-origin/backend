<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 18-Aug-16
 * Time: 16:38
 */

namespace Seldat\Wms2\Utils;

use Seldat\Wms2\Models\InventorySummary;

class SelInventorySummary
{
    /**
     * Insert if no exist
     *
     * @param array $data
     * @param int $cus_id
     * @param int $whs_id
     */
    public static function upsert(array $data, $cus_id = 0, $whs_id = 0)
    {
        $inserts = [];
        foreach ($data as $detail) {
            $total = $detail['asn_dtl_ctn_ttl'] * $detail['asn_dtl_pack'];
            $allocatedQty = array_get($detail, 'allocated_qty', null);
            $dmgQty = array_get($detail, 'dmg_qty', null);

            $fields = [
                'item_id'        => array_get($detail, 'item_id', null),
                'cus_id'        => $cus_id ? $cus_id : array_get($detail, 'cus_id', null),
                'whs_id'        => $whs_id ? $whs_id : array_get($detail, 'whs_id', null),
                'color'         => array_get($detail, 'item.color', null),
                'size'          => array_get($detail, 'item.size', null),
                'lot'           => array_get($detail, 'lot', null),
                //'ttl'           => $total, // To do after
                'allocated_qty' => $allocatedQty,
                'dmg_qty'       => $dmgQty,
                //'avail'         => ($total - $allocatedQty - $dmgQty), // To do after
                'sku'           => array_get($detail, 'item.sku', null),
                'created_at'    => time(),
                'updated_at'    => time()
            ];

            if ($total > 0) {
                $fields = array_merge($fields, [
                    'ttl'   => $total,
                    'avail' => ($total - $allocatedQty - $dmgQty)
                ]);
            }

            $ivt = (new InventorySummary)->where('item_id', $detail['item_id'])->first();
            if ($ivt) {
                $ivt->ttl = $ivt->ttl + $fields['ttl'];
                $ivt->allocated_qty = (empty($ivt->allocated_qty) ? 0 : $ivt->allocated_qty) + $fields['allocated_qty'];
                $ivt->dmg_qty = (empty($ivt->dmg_qty) ? 0 : $ivt->dmg_qty) + $fields['dmg_qty'];
                $ivt->avail = (empty($ivt->avail) ? 0 : $ivt->avail) + $fields['avail'];
                $ivt->save();
                continue;
            }

            $inserts[] = $fields;
        }

        if (!empty($inserts)) {
            InventorySummary::insert($inserts);
        }
    }

    /**
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public static function search($attributes = [], $limit = null)
    {
        $query = new InventorySummary();
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (isset($attributes['cus_id'])) {
            $query = $query->where('cus_id', $attributes['cus_id']);
        }
        $arrLike = ['color', 'sku', 'size', 'lot'];
        $arrEqual = [
            'item_id',
            'cus_id',
            'whs_id',
            'allocated_qty',
            'dmg_qty',
            'avail'
        ];
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query = $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual)) {
                    $query = $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        $models = $query->paginate($limit);

        return $models;
    }
}
