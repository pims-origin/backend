<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 22-Jul-16
 * Time: 3:05
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\DamageType;

class DamageTypeTransformer extends TransformerAbstract
{
    public function transform(DamageType $dmgType)
    {
        return [
            'dmg_id'        => $dmgType->dmg_id,
            'dmg_code'      => $dmgType->dmg_code,
            'dmg_name'      => $dmgType->dmg_name,
            'dmg_des'       => $dmgType->dmg_des,
            'created_at'    => $dmgType->created_at,
            'updated_at'    => $dmgType->updated_at,
            'deleted_at'    => $dmgType->deleted_at,
        ];
    }
}
