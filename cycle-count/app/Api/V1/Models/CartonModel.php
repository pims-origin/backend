<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/11/2016
 * Time: 15:40
 */

namespace App\Api\V1\Models;

use DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\PackRef;

class CartonModel extends AbstractModel
{
    public function __construct($model = null)
    {
        $this->model = $model ? $model : new Carton();
    }

    /**
     * @param $ctnIds
     *
     * @return array
     */
    public function getCartons($ctnIds)
    {
        $result = [];

        if (!$ctnIds) {
            return $result;
        }

        $result = Carton::select('*')
            ->whereIn('ctn_id', $ctnIds)
            ->distinct()
            ->get()
            ->toArray();

        return $result;
    }

    /**
     * @param $whsId
     * @param $locCode
     * @param $sku
     * @param null $ccnID
     * @param null $locCode_up
     * @param null $sku_up
     *
     * @return mixed
     */
    public function checkLocationNotSku($whsId, $locCode, $sku, $ccnID = null, $locCode_up = null, $sku_up = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = Carton::select('loc_code')
            ->where('whs_id', $whsId)
            ->where('loc_code', $locCode)
            ->where('sku', $sku)
            ->distinct();
        $result = $query->first();

        return $result;
    }

    /**
     * @param $grDtlId
     *
     * @return null
     */
    public function getMaxCtnNumByGoodRCDtlID($grDtlId)
    {
        $result = null;

        if (!$grDtlId) {
            return $result;
        }

        $grDtl = $this->getGoodReceiptDetail($grDtlId);
        $goodRcDtlIds = $this->getGoodReceiptIDByGrHdrId($grDtl->gr_hdr_id);

        $data = $this->model->select('ctn_num')
            ->whereIn('gr_dtl_id', $goodRcDtlIds)
            ->where('is_ecom', '<>', 1)
            ->orderBy('ctn_num', 'desc')
            ->first();

        return $data ? $data['ctn_num'] : null;

    }

    public function generateCartonNum($CycleNum)
    {
        $key = sprintf("%s", $CycleNum) . '%';
        $num = $this->model->select('ctn_num')
            ->where('ctn_num', 'LIKE', $key)
            ->orderBy('ctn_num', 'DESC')
            ->value('ctn_num');
        if (empty($num)) {
            return $CycleNum . "-00000";
        }

        return $num;
    }

    /**
     * @param $grHdrId
     *
     * @return int
     */
    public function getGoodReceiptIDByGrHdrId($grHdrId)
    {
        $data = GoodsReceiptDetail::select('gr_dtl_id')
            ->where('gr_hdr_id', $grHdrId)
            ->get()
            ->toArray();

        return $data ? array_column($data, 'gr_dtl_id') : -1;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getGoodReceiptDetail($grDtlId)
    {
        return GoodsReceiptDetail::where('gr_dtl_id', $grDtlId)
            ->first();
    }

    public static function getCalculateStorageDurationRaw($startDate = 'created_at')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    /**
     * @param $location
     * @return array
     */
    public function unsetCartonsRfid($cartons)
    {
        foreach ($cartons as $c){
            for ($i = 0; $i < count($c); $i++){
                unset($c[$i]);
            }

            // Old cartons RFID -> ctn_sts = AJ
            $oldCarton = $this->model->find($c['ctn_id']);
            $oldCarton->ctn_sts = 'AJ';
            $oldCarton->loc_id = null;
            $oldCarton->loc_code = null;
            $oldCarton->save();

            // Clone all RFID cartons, create new carton number, new rfid = null.
            unset($c['ctn_id']);
            $c['rfid'] = null;
            $c['ctn_num'] = substr_replace($c['ctn_num'], 'C', 3, 0);
            $this->model->insert($c);

        }
    }
}