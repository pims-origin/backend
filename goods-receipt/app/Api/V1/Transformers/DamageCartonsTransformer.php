<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\AsnDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;

class DamageCartonsTransformer extends TransformerAbstract
{

    /**
     * @param $sample
     *
     * @return array
     */
    public function transform($data)
    {

        return [
            'cus_code' => object_get($data, 'cus_code', null),
            'cus_name' => object_get($data, 'cus_name', null),
            'gr_hdr_num' => object_get($data, 'gr_hdr_num', null),
            'gr_in_note' => object_get($data, 'gr_in_note', null),
            'ctnr_num' => object_get($data, 'ctnr_num', null),
            'ctn_num' => object_get($data, 'ctn_num', null),
            'item_id' => object_get($data, 'item_id', null),
            'sku' => object_get($data, 'sku', null),
            'size' => object_get($data, 'size', null),
            'color' => object_get($data, 'color', null),
            'batch' => object_get($data, 'lot', null),
            'lot' => object_get($data, 'lot', null),
            'is_damaged' => object_get($data, 'is_damaged', null),
            'ctn_pack_size' => object_get($data, 'ctn_pack_size', null),
            'dmg_id' => object_get($data, 'dmg_id', null),
            'cus_id' => object_get($data, 'cus_id', null),
            'whs_id' => object_get($data, 'whs_id', null),
            'dmg_qty' => object_get($data, 'dmg_qty', null),
            'po' => object_get($data, 'po', null),
            'dmg_name' => object_get($data, 'dmg_name', null),
            'ctn_pack_size' => object_get($data, 'ctn_pack_size', null),
            'des' => object_get($data, 'des', null),
            'ref_code' => object_get($data, 'ref_code', null),
            'dmg_ctns' => object_get($data, 'dmg_ctns', null),
        ];
    }

}
