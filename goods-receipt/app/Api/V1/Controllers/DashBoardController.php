<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 6-Dec-2016
 * Time: 10:47
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\DamageTypeModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Transformers\DamageTypeTransformer;
use App\Api\V1\Transformers\GoodReceiptDasboardTransformer;
use App\Api\V1\Transformers\GRDashBoardTransformer;
use App\Api\V1\Transformers\GRdbTransformer;
use App\Api\V1\Validators\DamageTypeValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;

class DashBoardController extends AbstractController
{
    /**
     * @var GoodsReceiptModel
     */
    protected $goodReceiptHdrModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodReceiptDtlModel;

    /**
     * DashBoardController constructor.
     */
    public function __construct()
    {
        $this->goodReceiptHdrModel = new GoodsReceiptModel();
        $this->goodReceiptDtlModel = new GoodsReceiptDetailModel();
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function goodReceiptDashboard(Request $request)
    {
        $input = $request->getQueryParams();
        $day = $input['day'];
        try {
            $dashboard = $this->goodReceiptHdrModel->grDashBoard($day);

            return ['data' => $dashboard];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
