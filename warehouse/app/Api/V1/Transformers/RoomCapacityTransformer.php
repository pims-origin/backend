<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use App\CustomObject;
use League\Fractal\TransformerAbstract;



class RoomCapacityTransformer extends TransformerAbstract
{
    /**
     * @param Reason $reason
     *
     * @return array
     */
    public function transform(CustomObject $customObject)
    {
        $total = object_get($customObject, 'total', 0);
        $used = object_get($customObject, 'used', 0);
        $percent = 0;
        if ($total) {
            $percent = $used / $total;
        }
        return [
            'room'         => object_get($customObject, 'room', 0),
            'total'        => intval($total),
            'used'         => intval($used),
            'percent'      => $percent * 100
        ];
    }

}