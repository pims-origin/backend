<?php

namespace App\Api\V1\Validators;


class AsnValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'whs_id' => 'required|integer',
            'cus_id' => 'required|integer',
            'asn_ref' => 'required',
            "measurement_code" => 'required',
            'exp_date' => 'required|Date',
            'ctnr_num' => 'required',
            'details.*.dtl_sku' => 'required',
            'details.*.dtl_color' => 'required',
            'details.*.dtl_length' => 'required|numeric|greater_than_zero|max:999.99',
            'details.*.dtl_width' => 'required|numeric|greater_than_zero|max:999.99',
            'details.*.dtl_height' =>' required|numeric|greater_than_zero|max:999.99',
            'details.*.dtl_weight' =>' required|numeric|greater_than_zero|max:999.99',
            'details.*.dtl_pack' =>' required|integer|greater_than_zero',
            'details.*.dtl_uom_id' => 'required|integer',
            'details.*.dtl_po' => 'required|string',
            //'details.*.dtl_po_date' => 'required|Date',
            'details.*.dtl_ctn_ttl' => 'required|integer|greater_than_zero',
            'details.*.dtl_crs_doc' => 'integer|greater_equal_than_zero',
        ];


    }



}
