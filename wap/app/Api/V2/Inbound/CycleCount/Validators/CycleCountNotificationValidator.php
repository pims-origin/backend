<?php

namespace App\Api\V2\Inbound\CycleCount\Validators;


class CycleCountNotificationValidator extends AbstractValidator
{
    /*
     * Define rules
     * 
     * @return array
     */
    protected function rules()
    {
        return [
            'loc_id'  => 'required|integer',
            'pack'    => 'required|integer',
            'lot'     => 'required',
            'item_id' => 'required|integer',

        ];
    }
}
