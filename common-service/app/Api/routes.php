<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['namespace' => 'App\Api\V1\Controllers'], function ($app) {
    $app->get('/view-system-bugs', 'SystemBugController@getSysBugs');
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'setWarehouseTimezone';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V1'];
        });

        $api->get('countries', 'CountryController@index');
        $api->get('countries/dropdown', 'CountryController@getDropdown');
        $api->get('countries/{id:[0-9]+}/states', 'CountryController@getStates');
        $api->get('countries/{countryCode}/states', 'CountryController@getStatesByCode');
        $api->post('/countries', 'CountryController@store');
        $api->get('/countries/{systemCountryId}', 'CountryController@show');
        $api->put('/countries/{systemCountryId}', 'CountryController@update');

        //states
        $api->get('/states', 'StatesController@index');
        $api->get('/states/dropdown', 'StatesController@getDropdown');
        $api->post('/states', 'StatesController@store');
        $api->get('/states/{systemStateId}', 'StatesController@show');
        $api->put('/states/{systemStateId}', 'StatesController@update');

        // System Uom
        $api->get('/system-uoms', 'SystemUomController@index');
        $api->post('/system-uoms', 'SystemUomController@store');
        $api->get('/system-uoms/dropdown', 'SystemUOMsController@getDropdown');
        $api->get('/system-uoms/{systemUomId}', 'SystemUomController@show');
        $api->put('/system-uoms/{systemUomId}', 'SystemUomController@update');

        // System Measurement
        $api->get('/system-measurements', 'SystemMeasurementController@search');
        $api->get('/system-measurements/{systemMeasurementCode}', 'SystemMeasurementController@show');

        // Locations
        $api->get('/location-statuses', 'LocationStatusesController@getLocationStatuses');

        // Warehouses
        $api->get('/warehouse-names', 'WarehouseNamesController@getWarehouseNames');

        // Customers
        $api->get('/customer-names', 'CustomerNamesController@getCustomerNames');
        $api->get('/customer-statuses/dropdown', 'CustomerStatusesController@getDropdown');
        $api->get('/customers/dropdown', 'CustomersController@getDropdown');

        // Charge Codes
        $api->get('/charge-codes/dropdown', 'ChargeCodesController@getDropdown');

        // Charge Types
        $api->get('/charge-types/dropdown', 'ChargeTypesController@getDropdown');

        // User Department
        $api->get('/user-department', 'UserDepartmentController@search');
        $api->post('/user-department', 'UserDepartmentController@store');
        $api->get('/user-department/{userDepartmentId:[0-9]+}', 'UserDepartmentController@show');
        $api->put('/user-department/{userDepartmentId:[0-9]+}', 'UserDepartmentController@update');
        $api->delete('/user-department/{userDepartmentId:[0-9]+}', 'UserDepartmentController@destroy');

        //commodity
        $api->post('/commodity', ['action' => 'createCommodity', 'uses' => 'CommodityController@store']);
        $api->put('/commodity/{cmdId:[0-9]+}', ['action' => 'editCommodity', 'uses' => 'CommodityController@update']);
        $api->get('/commodity/commoditylist',
            ['action' => 'viewWorkOrder', 'uses' => 'CommodityController@commodityList']);
        $api->get('/commodity/{cmdId:[0-9]+}',
            ['action' => 'viewCommodity', 'uses' => 'CommodityController@show']);

        //Packed carton Dimension
        $api->post('/packedcartondimension', [
            'action' => 'createPackedCartonDimension',
            'uses'   => 'PackRefController@store'
        ]);
        $api->put('/packedcartondimension/{packRefId:[0-9]+}',
            ['action' => 'editPackedCartonDimension', 'uses' => 'PackRefController@update']);
        $api->get('/packedcartondimension/packedcartondimensionlist',
            [
                'action' => 'viewPackedCartonDimension',
                'uses'   => 'PackRefController@packedCartonDimensionList'
            ]);
        $api->get('/packedcartondimension/{packRefId:[0-9]+}',
            ['action' => 'viewPackedCartonDimension', 'uses' => 'PackRefController@show']);

        //Pack type
        $api->get('/packtype/dropdown',
            ['action' => 'viewPackType', 'uses' => 'PackTypeController@getDropDownList']);
        //labor
//        $api->get('/labor/{refId:[0-9]+}/{category}/{whsId}/{cusId}',
//            ['action' => 'viewLabor', 'uses' => 'LaborController@show']);
//        $api->post('/labor/{refId:[0-9]+}/{category}/{whsId}/{cusId}',
//            ['action' => 'viewLabor', 'uses' => 'LaborController@store']);
        $api->get('/labor/{refId:[0-9]+}/{category}/{whsId}/{cusId}',
            ['action' => 'viewLabor', 'uses' => 'LaborController@getLaborTracking']);
        $api->post('/labor/{refId:[0-9]+}/{category}/{whsId}/{cusId}',
            ['action' => 'viewLabor', 'uses' => 'LaborController@laborTracking']);

        $api->get('/currency',
            ['action' => 'viewCurrency', 'uses' => 'CurrencyController@search']);

        //logs
        $api->get('/logs/logslist/{whs_id}',
            ['action' => 'viewWorkOrder', 'uses' => 'LogsController@logsList']);
        $api->get('/logs/type-dropdown/{whs_id}',
            ['action' => 'viewWorkOrder', 'uses' => 'LogsController@typeDropDown']);
        $api->get('/logs/evt-code-dropdown/{whs_id}',
            ['action' => 'viewWorkOrder', 'uses' => 'LogsController@evtCodeDropDown']);

        //EDI
        $api->post('/edi/cus-configs', ['action' => 'createEdi', 'uses' => 'EdiController@store']);
        $api->get('/edi/edi-cus-list/{whsId}',
            ['action' => 'viewEdi', 'uses' => 'EdiController@ediList']);
        $api->get('/edi/{whsId}/{cusId}',
            ['action' => 'viewEdi', 'uses' => 'EdiController@show']);


    });
});
