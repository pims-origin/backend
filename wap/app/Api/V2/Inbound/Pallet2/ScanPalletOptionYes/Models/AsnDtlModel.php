<?php

namespace App\Api\V2\Inbound\Pallet2\ScanPalletOptionYes\Models;

use App\MyHelper;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;


class AsnDtlModel extends AbstractModel
{

    protected $asnHdr;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnDtl();
        $this->asnHdr = new AsnHdr();
    }


    /**
     * @param $asnHrdId
     * @return Collection
     */
    public function getContainersByAsn($asnHrdId)
    {
        $query = $this->make(['container']);
        $query->where('asn_hdr_id', $asnHrdId);
        $query->groupBy('ctnr_id');
        $asnDtls = $query->get();

        $containers = [];
        if (!$asnDtls->isEmpty()) {
            foreach ($asnDtls as $asnDtl) {
                $containers[] = $asnDtl->container;
            }
        }

        return collect($containers);
    }

    /**
     * @param $asnHrdId
     * @return int
     */
    public function calContainersTtlPieceByAsn($asnHrdId)
    {
        $query = $this->model
            ->select('asn_dtl_ctn_ttl', 'asn_dtl_pack', 'item_id')
            ->where('asn_hdr_id', $asnHrdId)->get();


        $ttlPiece = 0;
        if (!$query->isEmpty()) {
            foreach ($query as $asnDtl) {
                $ttlPiece += $asnDtl->asn_dtl_ctn_ttl * $asnDtl->asn_dtl_pack;
            }
        }

        return $ttlPiece;
    }

    /**
     * @param $asnHrdId
     * @return int
     */
    public function calContainersTtlCtnByAsn($asnHrdId)
    {
        $query = $this->model
            ->select('asn_dtl_ctn_ttl')
            ->where('asn_hdr_id', $asnHrdId)->get();


        $ttlCtn = 0;
        if (!$query->isEmpty()) {
            foreach ($query as $asnDtl) {
                $ttlCtn += $asnDtl->asn_dtl_ctn_ttl;
            }
        }

        return $ttlCtn;
    }
    /**
     * @param $asnHrdId
     * @param $ctnrId
     * @return array
     */
    public function getAsnDtlIdItemIdByHeaderAndContainer($asnHrdId, $ctnrId)
    {
        $collections = $this->model
            ->where('asn_hdr_id', $asnHrdId)
            ->where('ctnr_id', $ctnrId)
            ->select('asn_dtl_id', 'item_id')
            ->get();

        $result = [];
        if (!$collections->isEmpty()) {
            foreach ($collections as $row) {
                $result[$row->asn_dtl_id] = $row->item_id;
            }
        }

        return $result;
    }

    public function getTotalItem($item_id)
    {
        return $this->model->where('item_id', $item_id)->sum(DB::raw('asn_dtl_pack * asn_dtl_ctn_ttl'));
    }

    public function getTotalInfoFromAsnDtlByHeader($asnHrdId)
    {
        $row = $this->model
            ->select(DB::raw('count(1) as itm_ttl,
                                          count(distinct ctnr_id) as ctnr_ttl,
                                          sum(asn_dtl_ctn_ttl) as ctn_ttl'))
            ->where('asn_hdr_id', $asnHrdId)
            ->take(1)
            ->get()
            ->first();

        if (!$row) {
            $row = [
                'itm_ttl'  => 0,
                'ctnr_ttl' => 0,
                'ctn_ttl'  => 0,
            ];
        } else {
            $row = $row->toArray();
        }

        return $row;
    }

    public function countContainerOfAsn($asnHrdId)
    {
        return $this->model->where('asn_hdr_id', $asnHrdId)
            ->select('ctnr_id')
            ->groupBy('ctnr_id')
            ->get()
            ->count();
    }

    public function search($attributes, array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        // search asn po
        if (isset($attributes['asn_dtl_po'])) {
            $query->where('asn_dtl_po', 'like', "%" . SelStr::escapeLike($attributes['asn_dtl_po']) . "%");
        }
        $asnHdr = $this->asnHdr;
        $query->whereHas('asnHdr', function ($query) use ($attributes, $asnHdr) {
            // search whs id
            if (isset($attributes['whs_id'])) {
                $query->where('whs_id', (int)$attributes['whs_id']);
            }

            // search asn number
            if (isset($attributes['asn_hdr_num'])) {
                $query->where('asn_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['asn_hdr_num']) . "%");
            }

            // search customer
            if (isset($attributes['cus_id'])) {
                $query->where('cus_id', (int)$attributes['cus_id']);
            }

            // search asn status
            if (isset($attributes['asn_sts'])) {
                $query->where('asn_sts', $attributes['asn_sts']);
            }

            // Search by expected date
            if (isset($attributes['asn_hdr_ept_dt'])) {
                $query->where(
                    DB::raw('DATE_FORMAT(FROM_UNIXTIME(asn_hdr_ept_dt), "%m/%d/%Y")'),
                    $attributes['asn_hdr_ept_dt']
                );
            }

            $asnHdr->filterData($query, true);

        });

        // search container
        $query->whereHas('container', function ($query) use ($attributes) {
            if (isset($attributes['ctnr_num'])) {
                $query->where('ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
            }
        });

        // search item
        $query->whereHas('item', function ($query) use ($attributes) {
            if (isset($attributes['dtl_sku'])) {
                $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['dtl_sku']) . "%");
            }
        });

        // Get X-doc
        if (!empty($attributes['xdoc']) && !empty($attributes['xdoc']) === true) {
            $query->select(DB::raw('concat(asn_hdr_id, "-", ctnr_id) as asn_ctnr, sum(asn_dtl_crs_doc) as xdoc'));
            $query->groupBy(['asn_hdr_id', 'ctnr_id']);
        } else {
            $query->groupBy('asn_hdr_id');
        }
        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }

    public function getListASN($attributes, $limit = 20)
    {
        $query = $this->model
            ->select('asn_hdr.asn_hdr_id', 'asn_hdr.cus_id', 'container.ctnr_id', 'asn_sts_name', 'asn_sts',
                'asn_hdr_num', 'asn_hdr_ept_dt', 'cus_name', 'asn_dtl.ctnr_num', 'asn_hdr_ref', 'asn_hdr.created_at',
                DB::raw('count(asn_dtl.item_id) as num_sku'))
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
            ->join('container', 'container.ctnr_id', '=', 'asn_dtl.ctnr_id')
            ->join('customer', 'customer.cus_id', '=', 'asn_hdr.cus_id')
            ->join('asn_status', 'asn_status.asn_sts_code', '=', 'asn_hdr.asn_sts')
            ->where('asn_hdr.whs_id', $attributes['whs_id']);

        $query = $query -> whereIn('asn_sts', [
            Status::getByKey('ASN_DTL_STS', 'NEW'),
            Status::getByKey('ASN_DTL_STS', 'RECEIVING')
        ]);

        if(isset($attributes['cus_id']) && trim($attributes['cus_id']) != '')
            $query = $query -> where('asn_hdr.cus_id', $attributes['cus_id']);

        if(isset($attributes['ctnr_id']) && trim($attributes['ctnr_id']) != '')
            $query = $query -> where('asn_dtl.ctnr_id', $attributes['ctnr_id']);

        $query = $query -> groupBy('asn_hdr.asn_hdr_id', 'container.ctnr_id');

        return $query->paginate($limit);
    }

    public function getListItem($asn_hdr_id, $ctnr_id)
    {
        $query = $this->model
            ->select('sku', 'size', 'color', 'pack', 'asn_dtl_ctn_ttl', 'asn_dtl_pack', 'asn_dtl_sts', 'asn_dtl_lot',
                'asn_sts_name', 'item.item_id', 'asn_dtl.asn_dtl_id')
            ->join('item', 'item.item_id', '=', 'asn_dtl.item_id')
            ->join('asn_status', 'asn_status.asn_sts_code', '=', 'asn_dtl.asn_dtl_sts')
            ->where('asn_hdr_id', $asn_hdr_id)
            ->where('ctnr_id', $ctnr_id);

        return $query->get();
    }

    public function countAsnDtlNotReceived($asnHdrId)
    {
        return $this->model->where('asn_hdr_id', $asnHdrId)
            ->where('asn_dtl_sts', '<>', Status::getByKey('ASN_DTL_STS', 'RECEIVED'))
            ->count();
    }

    public function countAsnDtlNotNew($asnHdrId)
    {
        return $this->model->where('asn_hdr_id', $asnHdrId)
            ->where('asn_dtl_sts', '<>', Status::getByKey('ASN_DTL_STS', 'NEW'))
            ->count();
    }

    public function countAllAsnDtlReceived($asnHdrId, $ctnrId)
    {
        return $this->model
            ->where('asn_hdr_id', $asnHdrId)
            ->where('ctnr_id', $ctnrId)
            ->where('asn_dtl_sts', '<>', Status::getByKey('ASN_DTL_STS', 'RECEIVED'))
            ->count();
    }

    public function updateAsnDtlStatus($asnDtlId, $status) {

        return $this->model->find($asnDtlId)->update([
            'asn_dtl_sts' => $status
        ]);
    }
}
