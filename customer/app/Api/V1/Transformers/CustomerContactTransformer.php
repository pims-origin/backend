<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:19
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerContact;
use League\Fractal\TransformerAbstract;

class CustomerContactTransformer extends TransformerAbstract
{
    /**
     * @param CustomerContact $customerContact
     *
     * @return array
     */
    public function transform(CustomerContact $customerContact)
    {
        return [
            'cus_ctt_id'         => $customerContact->cus_ctt_id,
            'cus_ctt_fname'      => $customerContact->cus_ctt_fname,
            'cus_ctt_lname'      => $customerContact->cus_ctt_lname,
            'cus_ctt_email'      => $customerContact->cus_ctt_email,
            'cus_ctt_phone'      => $customerContact->cus_ctt_phone,
            'cus_ctt_mobile'     => $customerContact->cus_ctt_mobile,
            'cus_ctt_ext'        => $customerContact->cus_ctt_ext,
            'cus_ctt_position'   => $customerContact->cus_ctt_position,
            'cus_ctt_cus_id'     => $customerContact->cus_ctt_cus_id,
            'cus_ctt_department' => $customerContact->cus_ctt_department
        ];
    }
}
