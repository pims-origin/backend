<?php

namespace Seldat\Wms2\Models;


/**
 * Class Forecast
 * @package Seldat\Wms2\Models
 *
 * @property mixed cus_id
 * @property mixed fc_ib
 * @property mixed fc_ib_act
 * @property mixed fc_ob
 * @property mixed fc_ob_act
 * @property mixed fc_st
 * @property mixed fc_st_act
 * @property mixed fc_ind_id
 * @property mixed fc_month
 * @property mixed fc_year
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed created_by
 * @property mixed updated_by
 */
class Forecast extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'forecast';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'fc_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'fc_ib',
        'fc_ib_act',
        'fc_ob',
        'fc_ob_act',
        'fc_st',
        'fc_st_act',
        'fc_ind_id',
        'fc_month',
        'fc_year',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function indicator()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ForecastIndicator', 'fc_ind_id', 'fc_ind_id');
    }

}
