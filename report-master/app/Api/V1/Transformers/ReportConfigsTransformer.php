<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\ReportConfigs;

class ReportConfigsTransformer extends TransformerAbstract
{
    /**
     * @param ReportConfigs $reportConfigs
     *
     * @return array
     */
    public function transform(ReportConfigs $reportConfigs)
    {
        $data = json_decode(object_get($reportConfigs, 'data', []));
        $fields = collect(object_get($data, 'fields', []));
        $fields = $fields->keyBy('field_name');
        $data->fields = $fields;

        return [
            'qualifier'     => object_get($reportConfigs, 'qualifier', null),
            'data'          => $data,
            'ref'           => object_get($reportConfigs, 'ref', null),
            'des'           => object_get($reportConfigs, 'des', null),
            'name'          => object_get($reportConfigs, 'name', null),
            'level'         => object_get($reportConfigs, 'level', null),
            'system'        => object_get($reportConfigs, 'system', null),
            'created_at'    => object_get($reportConfigs, 'created_at', null),
            'updated_at'    => object_get($reportConfigs, 'updated_at', null),
            'sts'           => object_get($reportConfigs, 'sts', null),
            'url'           => object_get($reportConfigs, 'url', null),
            'url_export'    => object_get($reportConfigs, 'url_export', null)
        ];
    }
}
