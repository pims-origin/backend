<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = [];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        // Menu
        $api->get('/menus/{menuId}', 'MenuController@show');
        $api->post('/menus', 'MenuController@store');
        $api->put('/menus/{menuId}', 'MenuController@update');
        $api->delete('/menus/{menuId}', 'MenuController@destroy');
        $api->post('/groups/{menuGroupId}/list_menu', 'MenuController@storeMass');
        $api->get('/menu-list', 'MenuController@menuList');
        $api->post('/menu-list', 'MenuController@menuListByPermissions');

        // Menu Group
        $api->get('/groups', 'MenuGroupController@index');
        $api->get('/groups/{menuGroupId}', 'MenuGroupController@show');
        $api->post('/groups', 'MenuGroupController@store');
        $api->put('/groups/{menuGroupId}', 'MenuGroupController@update');
        $api->delete('/groups/{menuGroupId}', 'MenuGroupController@destroy');

        // Menu Favorite
        $api->get('/favorites/{userId}', 'MenuFavoriteController@index');
        $api->post('/favorites/{userId}', 'MenuFavoriteController@store');
        $api->delete('/favorites/{userId}/{menuId}', 'MenuFavoriteController@destroy');
    });


    $api->group(['prefix' => 'v2', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
        $api->get('/', function () {
            return 'v2';
        });
    });
});
