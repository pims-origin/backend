<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\GoodsReceiptHdrModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\VasDtlModel;
use App\Api\V1\Models\VasHdrModel;
use App\Api\V1\Models\WavepickHdrModel;
use App\Api\V1\Transformers\CycleHdrTransformer;
use App\Api\V1\Transformers\GrHdrTransformer;
use App\Api\V1\Transformers\OrderHdrTransformer;
use App\Api\V1\Transformers\WorkOrderHdrTransformer;
use App\Api\V1\Transformers\WvHdrTransformer;
use App\Api\V1\Validators\WorkOrderValidator;
use App\Wms2\UserInfo\Data;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface;
use Seldat\Wms2\Utils\Message;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\V1\Models\WorkOrderBillModel;
use App\Api\V1\Models\WorkOrderDetailBillModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Models\CustomerModel;
use Mpdf;
use App\Api\V1\Models\CustomerSac;
use App\Jobs\IntegrateFilesWithDMS;
use App\Api\V1\Models\BillableHdrModel;
use App\Api\V1\Models\BillableDtlModel;

class WorkOrderController extends AbstractController
{
    protected $vasHdrModel;

    protected $vasDtlModel;

    protected $odrHdrModel;

    protected $grHdrModel;

    protected $wvHdrModel;

    protected $cycHdrModel;

    protected $validator;

    protected $workOrderHdrTransformer;

    protected $orderHdrTransformer;

    protected $grHdrTransformer;

    protected $wvHdrTransformer;

    protected $cycHdrTransformer;

    protected $userModel;

    protected $customerModel;

    protected $billableItemHdrModel;

    protected $billableItemDtlModel;

    public static $refTypes = [
        'GR'    => 'Goods Receipt',
        'ORD'   => 'Order'
    ];

    public function __construct()
    {
        $this->vasHdrModel = new VasHdrModel();
        $this->vasDtlModel = new VasDtlModel();
        $this->odrHdrModel = new OrderHdrModel();
        $this->grHdrModel = new GoodsReceiptHdrModel();
        $this->wvHdrModel = new WavepickHdrModel();
        $this->cycHdrModel = new CycleHdrModel();
        $this->validator = new WorkOrderValidator();
        $this->workOrderHdrTransformer = new WorkOrderHdrTransformer();
        $this->orderHdrTransformer = new OrderHdrTransformer();
        $this->grHdrTransformer = new GrHdrTransformer();
        $this->wvHdrTransformer = new WvHdrTransformer();
        $this->cycHdrTransformer = new CycleHdrTransformer();
        $this->userModel = new UserModel;
        $this->customerModel = new CustomerModel;
        $this->billableItemHdrModel = new BillableHdrModel;
        $this->billableItemDtlModel = new BillableDtlModel;
    }

    /**
     * @SWG\Post(
     *     path="/work-order",
     *     tags={"Work Orders"},
     *     summary="Create Work Order",
     *     description="Create Work Order",
     *     operationId="postWorkOrder",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Work Order Object",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    /**
     * Create Work Order
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     * @throws
     **/
    public function store(ServerRequestInterface $request)
    {
        $input = $request->getParsedBody();
        $items = $input['items'];
        //validate input data
        $this->validator->validate($input);

        // WMS2-5719 - [Work Order] 1 billable = 1 work order detail
        //check unique for set (ref_num, vas_item, cus_sac_id)
        // $arrCheck = [];
        // foreach ($items as $item) {
        //     $checkDuplicate = $this->vasDtlModel->checkDuplicateSet($item);
        //     if ($checkDuplicate || isset($arrCheck[$item['ref_num'] . "-" . $item['charge_item'] . "-" . $item['cus_sac_id']])) {
        //         throw new \Exception(Message::get("BM006", "REF # - Charge Items - SAC"));
        //     }
        //     $arrCheck[$item['ref_num'] . "-" . $item['charge_item'] . "-" . $item['cus_sac_id']] = true;
        // }
        try {
            DB::beginTransaction();
            $vasNum = $this->vasHdrModel->getVasNum();
            //Calculate amount_ttl
            $subTotal = 0;
            foreach ($items as $item) {
                $amount = $item['qty'] * $item['unit_price'] * (1 - $item['discount'] / 100);
                $subTotal += $amount;
            }
            //Get current whs_id
            $redis = new Data();
            $input['whs_id'] = $redis->getCurrentWhs();
            $workOrderHdrData = [
                'vas_num'    => $vasNum,
                'cus_id'     => $input['cus_id'],
                'whs_id'     => $input['whs_id'],
                'subtotal'   => $subTotal,
                'tax'        => !empty($input['tax']) ? $input['tax'] : 0,
                'amount_ttl' => $subTotal * (1 + $input['tax'] / 100),
                'vas_sts'    => config('constants.vas_hdr_sts')[strtoupper($input['sts'])],
                'vas_note'   => $input['note'],
                'is_order'   => !empty($input['is_order']) ? (int)$input['is_order'] : null,
                'odr_hdr_id' => !empty($input['odr_id']) ? $input['odr_id'] : null,
            ];
            //Create WorkOrder Header
            $workOrderHdr = $this->vasHdrModel->create($workOrderHdrData);
            if (empty($workOrderHdr)) {
                throw new \Exception(Message::get("BM010"));
            }
            //Create WorkOrder Detail
            $this->saveVasDtl($workOrderHdr, $input);
            // Create billable item
            if ($workOrderHdr->vas_sts == 'FN') {
                // $billItem = new WorkOrderBillModel($workOrderHdr);
                $workOrderDtls = $this->vasDtlModel->findWhere(['vas_id' => $workOrderHdr->vas_id]);
                foreach ($workOrderDtls as $workOrderDtl) {
                    $billItem = new WorkOrderDetailBillModel($workOrderHdr, $workOrderDtl);
                    $billItem->saveBillableItem();
                }
            }
            DB::commit();

            if ($workOrderHdr->vas_sts == 'FN'){
                $pdfPath = $this->printWorkOrder($workOrderHdr->vas_id, storage_path('dms'));
                dispatch(new IntegrateFilesWithDMS($request, $workOrderHdr->vas_id, $pdfPath));
                // Update spec_instruct in Order table
                $items = $input['items'];
                $note = object_get($workOrderHdr, 'vas_note', '');
                foreach ($items as $item) {
                    if(strcmp($item['ref_type'], 'Order') == 0){
                        DB::table('odr_hdr')
                            ->where('odr_num', $item['ref_num'])
                            ->where('deleted', 0)
                            ->update(['spec_instruct' => $note]);
                    }
                }
            }

            return $this->response->item($workOrderHdr, new WorkOrderHdrTransformer())
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Put(
     *     path="/work-order/{vas_id}",
     *     tags={"Work Orders"},
     *     summary="Update Work Order",
     *     description="Update Work Order",
     *     operationId="putWorkOrder",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="vas_id",
     *         in="path",
     *         description="ID of Work Order",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Work Order Object",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    /**
     * Update Work Order
     *
     * @param int $workOrderId
     * @param ServerRequestInterface $request
     *
     * @return mixed
     * @throws
     **/
    public function update($workOrderId, ServerRequestInterface $request)
    {
        $input = $request->getParsedBody();
        $items = $input['items'];

        //validate input data
        $this->validator->validate($input);

        // WMS2-5719 - [Work Order] 1 billable = 1 work order detail
        //check unique for set (ref_num, vas_item, cus_sac_id)
        // $arrCheck = [];
        // foreach ($items as $item) {
        //     $checkDuplicate = $this->vasDtlModel->checkDuplicateSet($item, $workOrderId);
        //     if ($checkDuplicate || isset($arrCheck[$item['ref_num'] . "-" . $item['charge_item'] . "-" . $item['cus_sac_id']])) {
        //         throw new \Exception("The (REF # - Charge Items - SAC) set is duplicated!");
        //     }
        //     $arrCheck[$item['ref_num'] . "-" . $item['charge_item'] . "-" . $item['cus_sac_id']] = true;
        // }
        try {
            DB::beginTransaction();
            //Do not UPDATE if status is Final
            $currentWorkOrderHdr = $this->vasHdrModel->byId($workOrderId);
            if ($currentWorkOrderHdr->vas_sts == 'FN') {
                throw new \Exception("Only Work Order with status is New can be edited!");
            }
            //Calculate amount_ttl
            $subTotal = 0;
            foreach ($items as $item) {
                $amount = $item['qty'] * $item['unit_price'] * (1 - $item['discount'] / 100);
                $subTotal += $amount;
            }
            $workOrderHdrData = [
                'vas_id'     => $workOrderId,
                'cus_id'     => $input['cus_id'],
                'subtotal'   => $subTotal,
                'tax'        => !empty($input['tax']) ? $input['tax'] : 0,
                'amount_ttl' => $subTotal * (1 + $input['tax'] / 100),
                'vas_sts'    => config('constants.vas_hdr_sts')[strtoupper($input['sts'])],
                'vas_note'   => $input['note'],
                'is_order'   => !empty($input['is_order']) ? (int)$input['is_order'] : null,
                'odr_hdr_id' => !empty($input['odr_id']) ? $input['odr_id'] : null,
            ];
            //Update WorkOrder Header
            $workOrderHdr = $this->vasHdrModel->update($workOrderHdrData);
            //Update WorkOrder Detail
            $this->saveVasDtl($workOrderHdr, $input);

            if ($workOrderHdr->vas_sts == 'FN') {
                // $billItem = new WorkOrderBillModel($workOrderHdr);
                // $workOrderDtls = $workOrderHdr->vasDtl->get();
                $workOrderDtls = $this->vasDtlModel->findWhere(['vas_id' => $workOrderHdr->vas_id]);
                foreach ($workOrderDtls as $workOrderDtl) {
                    $billItem = new WorkOrderDetailBillModel($workOrderHdr, $workOrderDtl);
                    $billItem->saveBillableItem();
                }
            }
            DB::commit();

            if ($workOrderHdr->vas_sts == 'FN'){
                $pdfPath = $this->printWorkOrder($workOrderId, storage_path('dms'));
                dispatch(new IntegrateFilesWithDMS($request, $workOrderId, $pdfPath));
                // Update spec_instruct in Order table
                $items = $input['items'];
                $note = object_get($workOrderHdr, 'vas_note', '');
                foreach ($items as $item) {
                    if(strcmp($item['ref_type'], 'Order') == 0){
                        DB::table('odr_hdr')
                            ->where('odr_num', $item['ref_num'])
                            ->where('deleted', 0)
                            ->update(['spec_instruct' => $note]);
                    }
                }
            }

            return $this->response->item($workOrderHdr, new WorkOrderHdrTransformer())
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Create or update WorkOrder detail
     *
     * @param VasHdrModel $workOrderHdr
     * @param array $input
     *
     * @throws \Exception
     * */
    private function saveVasDtl($workOrderHdr, $input)
    {
        $items = $input['items'];
        $workOrderDtlIds = [];
        //Get old WorkOrderDtl id
        $oldWorkOrderDtlIds = $this->vasDtlModel->findWhere([
            'vas_id' => $workOrderHdr->vas_id
        ], [], ['vas_dtl_id'])->pluck('vas_dtl_id')->toArray();
        //Create WorkOrderDtl
        $refTypeArr = config('constants.vas_ref_type');
        //Validate ref_num exist in another WO with is_order = 1
        $refNumArr = array_column($items, 'ref_num');
        $checkVasExist = $this->vasDtlModel->checkRefNumExisted($refNumArr, $oldWorkOrderDtlIds);
        if (!empty($checkVasExist)) {
            throw new \Exception("Some REF # are already existed in another Work Order!");
        }
        foreach ($items as $item) {
            //validate REF # available or not
            $arrayModelMapping = [
                'ORD' => 'odrHdrModel',
                'GR'  => 'grHdrModel',
                'WV'  => 'wvHdrModel',
                'CYC' => 'cycHdrModel',
            ];
            $arrayRefNumMapping = [
                'ORD' => 'odr_num',
                'GR'  => 'gr_hdr_num',
                'WV'  => 'wv_num',
                'CYC' => 'cycle_num',
            ];
            $refType = $refTypeArr[$item['ref_type']];
            if (isset($arrayModelMapping[$refType])) {
                $model = $arrayModelMapping[$refType];
                $refNum = $arrayRefNumMapping[$refType];
                $checkRefNumAvailable = $this->{$model}->getFirstBy($refNum, $item['ref_num']);
                if (empty($checkRefNumAvailable)) {
                    throw new \Exception("The REF #: {$item['ref_num']} doesn't exist for REF Type: {$refType}!");
                }
            }
            //Insert WO detail
            $workOrderDtlData = [
                'vas_id'          => $workOrderHdr->vas_id,
                'cus_sac_id'      => $item['cus_sac_id'],
                'cus_id'          => $input['cus_id'],
                'ref_num'         => $item['ref_num'],
                'ref_type'        => $refTypeArr[$item['ref_type']],
                'vas_item'        => $item['charge_item'],
                'vas_description' => $item['charge_description'],
                'qty'             => $item['qty'],
                'unit_price'      => $item['unit_price'],
                'currency'        => $item['currency'],
                'discount'        => $item['discount'],
                'amount'          => $item['qty'] * $item['unit_price'] * (1 - $item['discount'] / 100),
            ];
            if (isset($input['whs_id'])) {
                $workOrderDtlData['whs_id'] = $input['whs_id'];
            }
            if (!empty($item['vas_dtl_id'])) {
                //Update WorkOrder Detail
                $workOrderDtlData['vas_dtl_id'] = $item['vas_dtl_id'];
                $this->vasDtlModel->refreshModel();
                $workOrderDtl = $this->vasDtlModel->update($workOrderDtlData);
            } else {
                //Create WorkOrder Detail
                $this->vasDtlModel->refreshModel();
                $workOrderDtl = $this->vasDtlModel->create($workOrderDtlData);
            }
            $workOrderDtlIds[] = $workOrderDtl->vas_dtl_id;
        }
        //Delete unused WorkOrderDtl
        $deleteWorkOrderDtlIds = array_diff($oldWorkOrderDtlIds, $workOrderDtlIds);
        if (!empty($deleteWorkOrderDtlIds)) {
            $this->vasDtlModel->deleteById($deleteWorkOrderDtlIds);
        }
    }

    /**
     * @SWG\Get(
     *     path="/work-order/{vas_id}",
     *     tags={"Work Orders"},
     *     summary="Work Order Info",
     *     description="Get Work Order Info",
     *     operationId="getWorkOrderInfo",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="vas_id",
     *         in="path",
     *         description="ID of Work Order",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Work Order Object",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    /**
     * Show WorkOrder detail
     *
     * @param int $workOrderId
     *
     * @return Response
     * */
    public function show($workOrderId)
    {
        $with = [
            'vasDtl',
            'createdBy',
            'order',
        ];
        $vasHdr = $this->vasHdrModel->byId($workOrderId, $with);
        $workOrderHdrTransformer = $this->workOrderHdrTransformer;
        $workOrderHdrTransformer->getDetail = true;

        return $this->response->item($vasHdr, $this->workOrderHdrTransformer);
    }

    /**
     * Get next WorkOrder number
     *
     * @return string
     * */
    public function getNextWONum()
    {
        return $this->vasHdrModel->getVasNum();
    }

    /**
     * @SWG\Get(
     *     path="/work-order/search",
     *     tags={"Work Orders"},
     *     summary="Seach Work Order",
     *     description="Search Work Order",
     *     operationId="getWorkOrderList",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cus_id",
     *         in="query",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="vas_num",
     *         in="query",
     *         description="Vas Num",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="ref_num",
     *         in="query",
     *         description="Ref Num",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="created_at",
     *         in="query",
     *         description="Created Date",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="vas_sts",
     *         in="query",
     *         description="Work Order Status",
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Work Order",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    /**
     * Search for WorkOrder
     *
     * @param ServerRequestInterface $request
     *
     * @return Response
     * */
    public function search(ServerRequestInterface $request)
    {
        $input = $request->getQueryParams();
        try {
            $with = [
                'vasDtl',
                'createdBy',
                'order',
            ];

            // get list WorkOrder
            $workOrders = $this->vasHdrModel->search($input, $with, array_get($input, 'limit'));

            return $this->response->paginator($workOrders, $this->workOrderHdrTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Delete WorkOrder
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     * */
    /*public function delete(ServerRequestInterface $request)
    {
        $input = $request->getParsedBody();
        $workOrderIds = $input['ids'];
        if (!empty($workOrderIds)) {
            try {
                DB::beginTransaction();
                //Delete WorkOrderDtl
                $this->vasDtlModel->deleteByWorkOrderHdrIds($workOrderIds);
                //Delete WorkOrderHdr
                $this->vasHdrModel->deleteById($workOrderIds);
                DB::commit();

                return $this->response->noContent();
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }
    }*/

    /**
     * Search for Odr that doesn't exist in VasDtl
     *
     * @param string $type
     * @param ServerRequestInterface $request
     *
     * @return mixed
     * */
    public function autoComplete($type, ServerRequestInterface $request)
    {
        $input = $request->getQueryParams();
        $input['type'] = $type;

        try {
            //get odr_num exist in vas_dtl
            $refNums = $this->vasDtlModel->findWhere(['ref_type' => strtoupper($type)], [],
                ['ref_num'])->pluck('ref_num')->toArray();
            if (!empty($refNums)) {
                $input['ref_num_arr'] = array_unique($refNums);
            }
            $arrayModelMapping = [
                'ord' => 'odrHdrModel',
                'gr'  => 'grHdrModel',
                'wv'  => 'wvHdrModel',
                'cyc' => 'cycHdrModel',
            ];
            $arrayTransformerMapping = [
                'ord' => 'orderHdrTransformer',
                'gr'  => 'grHdrTransformer',
                'wv'  => 'wvHdrTransformer',
                'cyc' => 'cycHdrTransformer',
            ];
            $model = $arrayModelMapping[$type];
            $transformer = $arrayTransformerMapping[$type];

            //get list WorkOrder
            $odrHdrs = $this->{$model}->search($input, [], array_get($input, 'limit'));

            if($type === 'odr'){
                $odrHdrs->load('details');

                $inputSku = array_get($input, 'sku', '');
                if ($inputSku){
                    $odrHdrs->map(function($order) use($inputSku){
                        $order->details = $order->details->filter(function($odrDetail) use($inputSku){
                            return strpos($odrDetail->sku, $inputSku) !== false;
                        });
                    });
                }
            }

            return $this->response->paginator($odrHdrs, $this->{$transformer});
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getOrdersByPO(ServerRequestInterface $request)
    {
        $input = $request->getQueryParams();

        $cusPO = array_get($input, 'cus_po', null);
        if (!$cusPO){
            return $this->response->errorBadRequest('Cus PO is required');
        }

        $odrHdrs = $this->odrHdrModel->getModel()
                        ->where('cus_po', $cusPO)
                        ->get();

        $odrHdrs->transform(function($orderHdr){
            return [
                'odr_id'    => $orderHdr->odr_id,
                'odr_num'   => $orderHdr->odr_num,
                'is_rush'   => $orderHdr->rush_odr == 1 ? true : false,
                'cus_po'    => $orderHdr->cus_po,
                'qty'       => 0,
            ];
        });

        return ['data' => $odrHdrs];
    }

    /**
     * @param $workOrderId
     * @return mixed
     */
    public function printWorkOrder($workOrderId, $filePath = null) {
        try {
            //Get work order information
            $with = [
                'vasDtl',
                'createdBy',
                'order',
            ];
            $vasHdr = $this->vasHdrModel->byId($workOrderId, $with);
            if (empty($vasHdr)) {
                throw new \Exception(Message::get("BM017", "Work Order"));
            }
            // presentation data
            $result = [];
            // sub total, tax, total
            $result['sub_total'] = object_get($vasHdr, 'subtotal', '');
            $result['tax'] = object_get($vasHdr, 'tax', '');
            $result['total'] = object_get($vasHdr, 'amount_ttl', '');
            // work order
            $result['work_order'] = object_get($vasHdr, 'vas_num', '');
            // Customer
            $cusId = object_get($vasHdr, 'cus_id', '');
            $cusInfo = $this->customerModel->getCusById($cusId)->toArray();
            $result['cus_name'] = array_get($cusInfo, 'cus_name', '');
            // Order
            $orderInfo = array_get($vasHdr, 'order', '');
            $result['order_num'] = object_get($orderInfo, 'odr_num', '');
            // is Rush
            $result['is_rush'] = (!empty($orderInfo) ? ($orderInfo->rush_odr == 1 ? true : false) : null);
            // vas Note
            $result['vas_note'] = object_get($vasHdr, 'vas_note', '');
            // vasDtl
            $vasDtl = object_get($vasHdr, 'vasDtl', '');
            $result['by_one_order'] = (!empty($vasDtl->count()) ? ($vasDtl->count() == 1 ? true : false) : null);

            if(!empty($vasDtl) && $vasDtl->count() > 0){
                $aTemp = [];
                foreach($vasDtl as $item){
                    if(!empty($item)){
                        $sac = (new CustomerSac())->getByCusSacId($item->cus_sac_id);
                        $unit_uom = DB::table('inv_uom')->where('inv_uom_code', object_get($sac,'unit_uom', null))->first();
                        $temp = [];
                        $temp['ref_type'] = self::$refTypes[$item->ref_type];
                        $temp['ref_num'] = (!empty($item->ref_num)) ? $item->ref_num : '';
                        $temp['charge_item'] = (!empty($item->vas_item)) ? $item->vas_item : '';
                        $temp['charge_des'] = (!empty($item->vas_description)) ? $item->vas_description : '';
                        $temp['sac_code'] = object_get($sac,'sac', null);
                        $temp['sac_des'] = object_get($sac,'sac_name', null);
                        $temp['uom'] = object_get($unit_uom,'inv_uom_des', null);
                        $temp['qty'] = (!empty($item->qty)) ? $item->qty : '';
                        $temp['price'] = (!empty($item->unit_price)) ? $item->unit_price : '';
                        $temp['currency'] = (!empty($item->currency)) ? $item->currency : '';
                        $temp['discount'] = (!empty($item->discount)) ? $item->discount : '';
                        $temp['amount'] = (!empty($item->amount)) ? $item->amount : '';
                        array_push($aTemp,$temp);
                    }
                }
                $result['items'] = $aTemp;
            }
            $userInfo = new \App\Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $result['printedBy'] = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            return $this->printPdfData($result, $filePath);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'Print work order', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $result
     * @throws Mpdf\MpdfException
     */
    private function printPdfData($result, $filePath = null)
    {
        $pdf = new Mpdf\Mpdf();
        $workOrderNum = $result['work_order'];
        $html = (string)view('printWorkOrderTemplate', [
            'workOdrHdr' => $result['work_order'],
            'cusName'  => $result['cus_name'],
            'orderNum'  => $result['order_num'],
            'isRush'  => $result['is_rush'],
            'note'  => $result['vas_note'],
            'byOneOrder'  => $result['by_one_order'],
            'items'  => $result['items'],
            'printedBy'  => $result['printedBy'],
            'subTotal'  => $result['sub_total'],
            'tax'  => $result['tax'],
            'total'  => $result['total'],
        ]);

        $pdf->WriteHTML($html);

        if ($filePath && is_dir($filePath)){
            $file = "{$filePath}/{$workOrderNum}.pdf";
            $pdf->Output("$file", 'F');
            return $file;
        }

        $pdf->Output("$workOrderNum.pdf", "D");
    }

    /**
     * Delete WorkOrder Version 2
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     * */
    public function delete(ServerRequestInterface $request)
    {
        $input = $request->getParsedBody();

        $workOrderId = $input['vas_id'];
        $workOrderNum = $input['vas_num'];

        if (!empty($workOrderId))
        {
            $orderNums = $this->vasDtlModel->getModel()
                            ->where('vas_id', $workOrderId)
                            ->where('deleted', 0)
                            ->distinct()
                            ->pluck('ref_num')
                            ->toArray();

            if (!empty($orderNums))
            {
                $billableIds = $this->billableItemDtlModel->getModel()
                                    ->whereIn('ref_num', $orderNums)
                                    ->where('deleted', 0)
                                    ->distinct()
                                    ->pluck('ba_id')
                                    ->toArray();
            }

            if (!empty($billableIds))
            {
                $invoice = DB::table('inv_dtl')
                            ->where('deleted', 0)
                            ->whereIn('ba_id', $billableIds)
                            ->get();
            }
        }

        if (count($invoice) > 0)
        {
            return $this->response->errorBadRequest('Can\'t delete work order ' . $workOrderNum . '. Work order has created invoice.');
        }

        // $invoice = $this->checkWorkOrderCreateInvoice($workOrderId);

        if (!empty($workOrderId)) {
            try {
                DB::beginTransaction();

                //Delete BillableItemDtl
                $this->billableItemDtlModel->deleteByOrderNums($orderNums);

                //Delete BillableItemHdr
                $this->billableItemHdrModel->deleteById($billableIds);

                //Delete WorkOrderDtl
                $this->vasDtlModel->deleteByWorkOrderHdrId($workOrderId);

                //Delete WorkOrderHdr
                $this->vasHdrModel->deleteById($workOrderId);

                DB::commit();

                return $this->response->accepted(null, ['msg' => 'Deleted successfully.']);

                return $this->response->noContent();
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }
    }

    /**
     * Delete Multiple WorkOrder
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     * */
    public function deleteMulti(ServerRequestInterface $request)
    {
        $input = $request->getParsedBody();

        $vas_ids = array_column($input, 'vas_id', '');
        $vas_nums = array_column($input, 'vas_num', '');

        // Check exists vas_num in inv_dtl
        if (!empty($vas_nums))
        {
            if ($this->validateVasNumInInvoiceDtl($vas_nums))
            {
                throw new \Exception("Can't delete work order. Work order has created invoice!");
            }
        }

        // Delete multi work orders
        if (!empty($vas_ids))
        {
            try {
                DB::beginTransaction();

                //Delete BillableItemDtl
                $this->billableItemDtlModel->deleteMultiByWorkOrderHdrIds($vas_nums);

                //Delete BillableItemHdr
                $this->billableItemHdrModel->deleteMultiByWorkOrderHdrIds($vas_nums);

                //Delete WorkOrderDtl
                $this->vasDtlModel->deleteMultiByWorkOrderHdrIds($vas_ids);

                //Delete WorkOrderHdr
                $this->vasHdrModel->deleteById($vas_ids);

                DB::commit();

                return $this->response->accepted(null, ['msg' => 'Deleted successfully.']);
            } catch (\Exception $e) {
                DB::rollback();
                return $this->response->errorBadRequest($e->getMessage());
            }
        }
    }

    private function validateVasNumInInvoiceDtl($ref_nums)
    {
        $invoice = DB::table('inv_dtl')
                    ->where('deleted', 0)
                    ->whereIn('ref_num', $ref_nums)->first();

        return !empty($invoice) ? true : false;
    }

    private function checkWorkOrderCreateInvoice($workOrderId)
    {
        $invoice = [];
        if (!empty($workOrderId))
        {
            $orderNums = $this->vasDtlModel->getModel()
                            ->where('vas_id', $workOrderId)
                            ->where('deleted', 0)
                            ->distinct()
                            ->pluck('ref_num')
                            ->toArray();

            if (!empty($orderNums))
            {
                $billableIds = $this->billableItemDtlModel->getModel()
                                    ->whereIn('ref_num', $orderNums)
                                    ->where('deleted', 0)
                                    ->distinct()
                                    ->pluck('ba_id')
                                    ->toArray();
            }

            if (!empty($billableIds))
            {
                $invoice = DB::table('inv_dtl')
                            ->where('deleted', 0)
                            ->whereIn('ba_id', $billableIds)
                            ->get();
            }
        }

        return count($invoice) > 0 ? true : false;
    }

    /**
     * Delete WorkOrder with status is New
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     * */
    public function deleteWithNewStatus(ServerRequestInterface $request)
    {
        $input = $request->getParsedBody();
        $vasId = $input['id'];
        if (!empty($vasId)) {
            try {
                DB::beginTransaction();
                //Delete WorkOrderDtl
                $this->vasDtlModel->forceDeleteByVasId($vasId);
                //Delete WorkOrderHdr
                $this->vasHdrModel->forceDeleteByVasId($vasId);
                DB::commit();

                return $this->response->accepted(null, ['msg' => 'Deleted successfully.']);
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }
    }
}
