<?php

namespace App\Api\V2\PackDetail\ManualPack\Controllers;

use App\Api\V2\PackDetail\Models\EventTrackingModel;
use App\Api\V2\PackDetail\Models\OrderDtlModel;
use App\Api\V2\PackDetail\Models\OrderHdrModel;
use App\Api\V2\PackDetail\Models\PackDtlModel;
use App\Api\V2\PackDetail\Models\PackHdrModel;
use App\Api\V2\PackDetail\Models\OutPalletModel;
use App\Api\V2\PackDetail\Models\WavePickDtlModel;

use App\Api\V2\PackDetail\ManualPack\Transformers\PackDtlTransformer;
use App\Api\V2\PackDetail\ManualPack\Transformers\PackHdrTransformer;
use App\Api\V2\PackDetail\ManualPack\Transformers\ViewAssignPalletTransformer;
use App\Api\V2\PackDetail\ManualPack\Validators\PackHdrAutoValidator;
use App\Api\V2\PackDetail\ManualPack\Validators\PackHdrValidator;

use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Models\Warehouse;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use TCPDF;
use yii\helpers\VarDumper;
use mPDF;

/**
 * Class PackHdrController
 *
 * @package App\Api\V1\Controllers
 */
class PackHdrController extends AbstractController
{
    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var OutPalletModel
     */
    protected $outPalletModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var WavePickDtlModel
     */
    protected $wavePickDtlModel;

    /**
     * @param PackHdrModel $packHdrModel
     * @param PackDtlModel $packDtlModel
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param OutPalletModel $outPalletModel
     */
    public function __construct(
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel,
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        OutPalletModel $outPalletModel,
        EventTrackingModel $eventTrackingModel,
        WavePickDtlModel $wavePickDtlModel
    ) {
        $this->packHdrModel = $packHdrModel;
        $this->packDtlModel = $packDtlModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->outPalletModel = $outPalletModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->wavePickDtlModel = $wavePickDtlModel;
    }


    /**
     * @param $orderId
     * @param PackDtlTransformer $packDtlTransformer
     *
     * @return Response|void
     */
    public function show($orderId, PackDtlTransformer $packDtlTransformer)
    {
        try {
            // Load Packing Info
            $packHdr = $this->packHdrModel->allBy("odr_hdr_id", $orderId, ['systemUom'])->toArray();

            $itemPieceQty = $this->packDtlModel->loadPieceQty(['odr_hdr_id' => $orderId])->toArray();
            $itemPieceQty = array_pluck($itemPieceQty, "piece_qty_ttl", "item_id");

            $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId, ['packHdr.details', 'packHdr.packRef']);

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            $orderHdr->piece_ttl = !empty($packHdr) ? array_sum(array_pluck($packHdr, "piece_ttl")) : 0;
            $orderHdr->ctn_ttl = count($packHdr);

            // Load Items
            $orderDtl = $this->orderDtlModel
                ->allBy('odr_id', $orderHdr->odr_id, ['inventorySummary.item', 'systemUom'])
                ->toArray();

            // Load wave pick detail by wave pick
            $wvIds = array_pluck($orderDtl, 'wv_id', 'wv_id');
            $wvDts = $this->wavePickDtlModel->getActualQtyByWvIds($wvIds);
            $wvDts = array_pluck($wvDts, 'act_piece_qty', 'item_id');

            $items = [];
            $skuTotal = 0;
            if (!empty($orderDtl)) {
                foreach ($orderDtl as $detail) {
                    // get all items and sum when using new sku
                    // $itemChildArr = DB::table('item_child')
                    //                 ->where('origin', $detail['item_id'])
                    //                 ->get();
                    // $itemChildIds = array_pluck($itemChildArr, 'child');
                    // $itemChildIds = array_prepend($itemChildIds, $detail['item_id']);
                    // $packQty = (int)$this->packDtlModel->sumPieceQty($detail['lot'], $itemChildIds, $detail['odr_id']);
                    // $remain  = $detail['alloc_qty'] - $packQty;

                    $remain = $detail['alloc_qty'] - round($this->packDtlModel->sumPieceQty($detail['lot'],
                            $detail['item_id'], $detail['odr_id']) , 2);
                    if ($remain < 0) {
                        $remain = 0;
                    }

                    if ($detail['alloc_qty'] <= 0) {
                        continue;
                    }
                    $items[] = [
                        'itm_id'        => $detail['item_id'],
                        'sku'           => $detail['sku'],
                        'color'         => $detail['color'],
                        'size'          => $detail['size'],
                        'pack'          => $detail['pack'],
                        'lot'           => $detail['lot'],
                        'alloc_qty'     => $detail['alloc_qty'],//$allocQTY,
                        'remain_qty'    => $remain,
                        'ship_track_id' => $detail['ship_track_id'],
                        'odr_dtl_id'    => $detail['odr_dtl_id']
                    ];
                    if (empty($remain)) {
                        $skuTotal++;
                    }
                }
            }
            $orderHdr->sku_ttl_com = $skuTotal;

            $orderHdr->items = $items;

            $packHdr = $orderHdr->packHdr->toArray();

            $orderHdr->cartons = array_map(function ($value) {
                return [
                    'pack_hdr_id'   => $value['pack_hdr_id'],
                    'odr_hdr_id'    => $value['odr_hdr_id'],
                    'pack_hdr_num'  => $value['pack_hdr_num'],
                    'sku_ttl'       => $value['sku_ttl'],
                    'piece_ttl'     => $value['piece_ttl'],
                    'pack_type'     => array_get($value, 'pack_ref.pack_type', null),
                    'seq'           => $value['seq'],
                    'carrier_name'  => $value['carrier_name'],
                    'pack_sts'      => $value['pack_sts'],
                    'pack_sts_name' => Status::getByKey("Pack-Status", $value['pack_sts']),
                    'ship_to_name'  => $value['ship_to_name'],
                    'length'        => array_get($value, 'pack_ref.length', null),
                    'width'         => array_get($value, 'pack_ref.width', null),
                    'height'        => array_get($value, 'pack_ref.height', null),
                    'dimension'     => array_get($value, 'pack_ref.dimension', null),
                    'is_print'      => (int)$value['is_print'],
                    'is_assigned'   => ($value['out_plt_id']) ? 1 : 0
                ];
            }, $packHdr);

            return $this->response->item($orderHdr, $packDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
