<?php

if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if ( ! function_exists('getDefault'))
{
    function getDefault(&$value, $default=FALSE, $function=FALSE)
    {
        $returnValue = isset($value) ? $value : $default;
        return $function ? call_user_func($function, $returnValue) : $returnValue;
    }
}
