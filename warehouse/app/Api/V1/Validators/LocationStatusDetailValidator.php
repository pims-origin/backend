<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Validators;


class LocationStatusDetailValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_sts_dtl_sts_code'  => 'required|exists:loc_status,loc_sts_code',
            'loc_sts_dtl_from_date' => 'required|integer',
            'loc_sts_dtl_to_date'   => 'integer',
            'loc_sts_reason_id'    => 'required_if:loc_sts_dtl_sts_code,' . config('constants.loc_status.INACTIVE')
                                       . '|integer',
            'loc_sts_dtl_remark'    => 'string'
        ];
    }
}
