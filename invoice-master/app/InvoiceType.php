<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\BaseInvoiceModel;

class InvoiceType extends BaseInvoiceModel
{
    protected $table = 'inv_type';
    protected $primaryKey = 'inv_type_id';
    protected $fillable = [
        'inv_type_code',
        'inv_type_des',
    ];
}
