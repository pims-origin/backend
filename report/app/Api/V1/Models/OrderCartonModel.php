<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Utils\SelArr;

class OrderCartonModel extends AbstractModel
{
    const LIMIT = 10;
    public function __construct(OrderCarton $model = NULL)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    /*
    ****************************************************************************
    */

    /**
     * Search Autocomplete SKU Order Cartons
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function autoSku($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('odr_cartons AS oc')
            ->select(['oc.sku', 'oc.size', 'oc.color', 'oc.lot', 'oc.item_id'])
            ->join('odr_hdr', 'oc.odr_hdr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_hdr.whs_id', $whs_id)
            ->where('odr_hdr.odr_sts', 'SH')
        ;

        if (isset($attributes['cus_id'])) {
            $query->where('odr_hdr.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['odr_id'])) {
            $query->where('oc.odr_hdr_id', $attributes['odr_id']);
        }

        if (isset($attributes['odr_num'])) {
            $query->where('odr_hdr.odr_num', $attributes['odr_num']);
        }

        // Search Customer
        if (isset($attributes['sku'])) {
            $query->where('oc.sku', 'LIKE', "%{$attributes['sku']}%");
        }

        $result = $query->groupBy('oc.item_id', 'oc.lot')
                ->orderBy('oc.sku')->take($limit)->get();

        return $result;
    }

    /**
     * Search Autocomplete autoOrderNum Order Cartons
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function autoOrderNum($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('odr_cartons AS oc')
            ->select(['odr_hdr.odr_id', 'oc.odr_num'])
            ->join('odr_hdr', 'oc.odr_hdr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_hdr.whs_id', $whs_id)
            ->where('odr_hdr.odr_sts', 'SH')
        ;

        if (isset($attributes['cus_id'])) {
            $query->where('odr_hdr.cus_id', $attributes['cus_id']);
        }

        // Search Customer
        if (isset($attributes['odr_num'])) {
            $query->where('oc.odr_num', 'LIKE', "%{$attributes['odr_num']}%");
        }

        $result = $query->groupBy('oc.odr_num', 'odr_hdr.odr_id')
                ->orderBy('oc.odr_num')->take($limit)->get();

        return $result;
    }

    /**
     * Search List Customer
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function customers($whs_id, $attributes = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        // Search whs_id
        $query = DB::table('customer AS c')
            ->select(['c.cus_id', 'c.cus_name'])
            ->join('odr_hdr', 'c.cus_id', '=', 'odr_hdr.cus_id')
            ->where('odr_hdr.whs_id', $whs_id)
            ->where('odr_hdr.odr_sts', 'SH')
        ;

        $result = $query->groupBy('c.cus_id')->orderBy('c.cus_name')->get();

        return $result;
    }

}
