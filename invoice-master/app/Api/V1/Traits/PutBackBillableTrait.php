<?php

namespace App\Api\V1\Traits;

use App\Api\V1\Models\CustomerSac;
use App\BillableHdr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PutBack;
use Seldat\Wms2\Models\ReturnDtl;

trait PutBackBillableTrait
{
    /**
     * [proccessBillable description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    protected function proccessBillable($request)
    {
        $chargeCodes = $this->getChargeCode($request->get('whs_id'), $request->get('cus_id'));
        if ($chargeCodes->isEmpty()) {
            return false;
        }

        $putBacks = $this->getPutBackViaReturnId($request->get('return_id'));
        if ($putBacks->isEmpty()) {
            return false;
        }

        $billableDetails = $this->transformBillableDetailByUnitUom($putBacks, $chargeCodes);
        if (count($billableDetails) < 1) {
            return false;
        }

        return DB::transaction(function () use ($request, $billableDetails) {
            $billableDetailFirst = array_first($billableDetails);
            $params              = [
                'ref_num' => $billableDetailFirst['ref_num'] ?? $request->get('return_id'),
                'cus_id'  => $request->get('cus_id'),
                'whs_id'  => $request->get('whs_id'),
            ];
            if (!$billableHdr = $this->createBillableHdr($params)) {
                return false;
            }

            if (!$billableDetails = $billableHdr->detail()->createMany($billableDetails)) {
                return false;
            }

            if (!$billableHdr->update(['ba_ttl' => $billableDetails->sum('qty'), 'amount_ttl' => $billableDetails->sum('amount')])) {
                return false;
            }

            return true;
        });
    }
    /**
     * [getChargeCode description]
     * @param  [type] $whsId [description]
     * @param  [type] $cusId [description]
     * @param  [type] $type  [description]
     * @return [type]        [description]
     */
    protected function getChargeCode($whsId, $cusId, $type = null)
    {
        $type = $type ?: config('constants.BILL_ABLE_ITEM.CHARGE_CODE_TYPE');
        return CustomerSac::where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->where('type', $type)
            ->get();
    }
    /**
     * [getReturnDtlById description]
     * @param  [type] $returnId [description]
     * @return [type]           [description]
     */
    protected function getReturnDtlById($returnId)
    {
        return ReturnDtl::select(['pb_id', 'return_id', 'return_num', DB::raw('SUM(pallet_id) as pallet_qty, SUM(ctn_ttl) as ctn_qty, SUM(piece_qty) as piece_qty')])
            ->where('return_id', $returnId)
            ->groupBy('return_num')
            ->get();
    }
    /**
     * [getPutBackViaReturnId description]
     * @param  [type] $returnId [description]
     * @return [type]           [description]
     */
    protected function getPutBackViaReturnId($returnId, $putStatus = 'CO')
    {
        return PutBack::select(['pb_id', 'return_dtl_id', 'return_id', 'return_num', DB::raw('COUNT(pallet_id) as pallet_qty, SUM(ctn_ttl) as ctn_qty')])
            ->where('return_id', $returnId)
            ->where('pb_sts', $putStatus)
            ->with('returnOrderDtl.odrHdr')
            ->get();
    }
    /**
     * [createBillableHdr description]
     * @param  array  $attributes [description]
     * @return [type]             [description]
     */
    protected function createBillableHdr(array $attributes)
    {
        $attributes['ba_from'] ?? $attributes['ba_from']       = time();
        $attributes['ba_to'] ?? $attributes['ba_to']           = time();
        $attributes['ba_ttl'] ?? $attributes['ba_ttl']         = 0;
        $attributes['amount_ttl'] ?? $attributes['amount_ttl'] = 0;
        $attributes['ba_type'] ?? $attributes['ba_type']       = config('constants.BILL_ABLE_ITEM.BILL_ABLE_TYPE');
        $attributes['ba_period'] ?? $attributes['ba_period']   = config('constants.BILL_ABLE_ITEM.BILL_ABLE_PERIOD');

        return BillableHdr::create($attributes);
    }
    /**
     * [createBillableDetail description]
     * @param  BillableHdr $model      [description]
     * @param  array       $attributes [description]
     * @return [type]                  [description]
     */
    protected function createBillableDetail(BillableHdr $model, array $attributes)
    {
        return $model->detail()->createMany($attributes);
    }
    /**
     * [transformBillableDetail description]
     * @param  [type] $putBacks    [description]
     * @param  [type] $chargeCodes [description]
     * @return [type]              [description]
     */
    protected function transformBillableDetail($putBacks, $chargeCodes)
    {
        $chargeCodes = $chargeCodes->transfrom(function ($item) {
            return [
                'sac_id'   => $item->sac_id,
                'sac'      => $item->sac,
                'price'    => $item->price,
                'discount' => $item->discount,
                'unit_uom' => $item->unit_uom,
            ];
        })->keyBy('unit_uom');
    }
    /**
     * [transformBillableDetailByUnitUom description]
     * @param  [type] $putBacks    [description]
     * @param  [type] $chargeCodes [description]
     * @return [type]              [description]
     */
    protected function transformBillableDetailByUnitUom($putBacks, $chargeCodes)
    {
        $unit_uom_ct  = config('constants.BILL_ABLE_ITEM.UNIT_UOM_CT'); //Carton
        $unit_uom_pl  = config('constants.BILL_ABLE_ITEM.UNIT_UOM_PL'); //Pallet
        $unit_uom_pc  = config('constants.BILL_ABLE_ITEM.UNIT_UOM_PC'); //PC
        $unit_uom_uni = config('constants.BILL_ABLE_ITEM.UNIT_UOM_UNI'); //UNIT
        $putBacks     = $putBacks->reject(function ($item) {
            return ($item->return_num === null) || ($item->ctn_qty < 0) || ($item->pallet_qty < 0);
        });
        foreach ($putBacks as $item) {

            if ($chargeCode = $chargeCodes->where('unit_uom', $unit_uom_ct)->first()) {
                $qty    = $item['ctn_qty'];
                $amount = $this->calculatorAmount($qty, $chargeCode);
                $data[] = $this->generateBillableDetail($item, $qty, $amount, $chargeCode);
            }

            if ($chargeCode = $chargeCodes->where('unit_uom', $unit_uom_pl)->first()) {
                $qty    = $item['pallet_qty'];
                $amount = $this->calculatorAmount($qty, $chargeCode);
                $data[] = $this->generateBillableDetail($item, $qty, $amount, $chargeCode);
            }

            if ($chargeCode = $chargeCodes->where('unit_uom', $unit_uom_pc)->first()) {
                $qty    = $item->returnOrderDtl->piece_qty ?? 0;
                $amount = $this->calculatorAmount($qty, $chargeCode);
                $data[] = $this->generateBillableDetail($item, $qty, $amount, $chargeCode);
            }

            if ($chargeCode = $chargeCodes->where('unit_uom', $unit_uom_uni)->first()) {
                $qty    = $item['pallet_qty'];
                $amount = $this->calculatorAmount($qty, $chargeCode);
                $data[] = $this->generateBillableDetail($item, $qty, $amount, $chargeCode);
            }
        }

        return $data ?? [];

    }
    /**
     * [generateBillableDetail description]
     * @param  [type] $item       [description]
     * @param  [type] $qty        [description]
     * @param  [type] $amount     [description]
     * @param  [type] $chargeCode [description]
     * @return [type]             [description]
     */
    private function generateBillableDetail($item, $qty, $amount, $chargeCode)
    {
        return [
            'ba_type'     => config('constants.BILL_ABLE_ITEM.CHARGE_TYPE_PB'),
            'sac_id'      => $chargeCode['sac_id'],
            'sac'         => $chargeCode['sac'],
            'cus_id'      => $chargeCode['cus_id'],
            'whs_id'      => $chargeCode['whs_id'],
            'sac_name'    => $chargeCode['sac_name'],
            'unit_price'  => $chargeCode['price'],
            'discount'    => $chargeCode['discount'],
            'ref_num'     => $item['return_num'],
            'item'        => $item['return_order_dtl']['odr_hdr']['cus_odr_num'] ?? '',
            'description' => $item['return_order_dtl']['odr_hdr']['cus_po'] ?? '',
            'qty'         => $qty,
            'amount'      => $amount,
        ];
    }

    private function calculatorAmount($qty, $chargeCode)
    {
        return $qty * $chargeCode['price'] * ((100 - $chargeCode['discount']) / 100);
    }

}
