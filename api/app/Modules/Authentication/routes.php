<?php
$middleware = ['app'];
$router->group(['prefix' => '/rest/wms360/seldat/v1', 'middleware' => $middleware,  'namespace' => 'App\Modules\Authentication\Controllers'], function () use ($router) {
    $router->post('/authenticate', 'UserController@loginUser');
});

