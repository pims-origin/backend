<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;
use stdClass;


/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class CartonModel extends AbstractModel
{
    protected $packRef = [];
    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    protected $currentWH;

    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $this->currentWH = $currentWH;
    }

    /**
     * @param $itemId
     * @param $cus_id
     * @param $locTypeId
     *
     * @return mixed
     */
    public function sumPieceRemainByLoc($itemIds, $cus_id)
    {
        $query = DB::table('cartons')
            ->select(DB::raw('SUM(cartons.piece_remain) as piece_remain, cartons.item_id as item_id'))
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->where('cartons.cus_id', $cus_id)
            ->whereIn('cartons.item_id', $itemIds)
            ->where('location.loc_sts_code', '<>', Status::getByKey('LOCATION_STATUS', 'ACTIVE'))
            ->where('location.deleted', 0)
            ->where('cartons.deleted', 0);


        $query->where('cartons.whs_id', $this->currentWH);

        $model = $query->groupBy('cartons.item_id')->get();

        return $model;
    }

    public function getAvailItem($itemId, $ctn_sts = 'AC', $loc_type_code = 'RAC')
    {
        $query = DB::table('cartons')->select(DB::raw("sum(`piece_remain`) as avail"))
            ->where('item_id', $itemId)
            ->where('ctn_sts', $ctn_sts)
            ->where('is_damaged', 0)
            ->where('whs_id', $this->currentWH)->first();
        $avail = array_get($query, 'avail', -1);

        return $avail;
    }

    /**
     * @param $odrId
     * @param array $with
     *
     * @return mixed
     */
    public function getItemForAssign($odrId, $with = [])
    {
        $query = $this->make($with)
            ->select([
                DB::raw("sum(odr_cartons.piece_qty) as piece_ttl"),
                'cartons.item_id',
                'odr_cartons.odr_dtl_id',
                'odr_cartons.odr_hdr_id',
                'cartons.ctn_pack_size',
                'odr_cartons.color',
                'odr_cartons.sku',
                'odr_cartons.size',
                'odr_cartons.lot',
                'odr_cartons.pack',
                'odr_cartons.upc',
                'odr_cartons.uom_id',
                'odr_cartons.uom_code',
            ])
            ->join('odr_cartons', 'odr_cartons.ctn_id', '=', 'cartons.ctn_id')
            ->where('odr_cartons.odr_hdr_id', $odrId)
            ->groupBy('odr_dtl_id');

        return $query->get();
    }


    public function updateCartonPB($ctn_id)
    {
        DB::table('cartons')->where('ctn_sts', 'PD')->whereIn('ctn_id', $ctn_id)->update(['ctn_sts' => 'AC']);

    }

    /**
     * Get pack size by array item_id
     *
     * @param Array $itemIdArr Array item_id
     *
     * @return Array
     */
    public function getCartonsByItemId($itemIdArr = [])
    {
        $query = DB::table('cartons')->select(['item_id', 'ctn_pack_size'])
            ->whereIn('ctn_sts', ['AC', 'LK'])
            ->whereIn('cartons.item_id', $itemIdArr)
            ->groupBy();
        $data = $query->get();

        return $data;
    }

    public function getAvailablePackSize($itemId, $lot = 'ANY')
    {
        $query = $this->make([]);

        $query->where('item_id', $itemId)->groupBy('ctn_pack_size');

        if ($lot != 'ANY') {
            $query->where('lot', $lot);
        }

        return $query->paginate(20);

    }

    public static function getCalculateStorageDurationRaw($startDate = 'gr_dt')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    public function searchXdoc($sku, $cusId)
    {
        $whsId = Data::getCurrentWhsId();

        $rs = $this->model->select([
            'cartons.item_id',
            'cartons.sku',
            'cartons.size',
            'cartons.color',
            DB::raw('NULL AS lot'),
            DB::raw('SUM(piece_remain) AS avail'),
            'cartons.ctn_uom_id AS uom_id',
            'cartons.upc',
            'cartons.des',
            DB::raw('system_uom.sys_uom_code AS uom_code'),
            DB::raw('system_uom.sys_uom_name AS uom_name'),
            DB::raw('cartons.ctn_pack_size AS pack'),
            'system_uom.level_id'
        ])
            ->join('system_uom', 'cartons.ctn_uom_id', '=', 'system_uom.sys_uom_id')
            ->where('cartons.sku', 'LIKE', "%" . SelStr::escapeLike($sku) . "%")
            ->where('cartons.cus_id', $cusId)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.loc_type_code', 'XDK')
            ->where('cartons.ctn_sts', 'AC')
            ->groupBy('cartons.item_id')
            ->paginate(20)
            ->toArray();

        return $rs;
    }

    public function getPieceSizesByPiece($itemId, $lot, $packSize, $pieceTtl)
    {
        $pieceSizes = $this->getAvailablePieces($itemId, $lot);
        $result = [];
        if (isset($pieceSizes[$packSize])) {
            $result[$packSize] = $pieceSizes[$packSize];
            $pieceTtl -= $pieceSizes[$packSize]['total'];
            unset($pieceSizes[$packSize]);
        }

        if ($pieceTtl > 0) {
            foreach ($pieceSizes as $pieceSize => $ttlAndLoc) {
                $result[$pieceSize] = $pieceSizes[$pieceSize];
                $pieceTtl -= $ttlAndLoc['total'];
                if ($pieceTtl <= 0) {
                    //$result[$pieceSize]['total'] = $pieceTtl + $ttlAndLoc['total']; //reverse ttl
                    break;
                }
            }
        }

        return $result;
    }

    public function getAvailablePieces($itemId, $lot)
    {
        $whsId = Data::getCurrentWhsId();

        $res = $this->model->where([
            'item_id'       => $itemId,
            'lot'           => $lot,
            'loc_type_code' => 'XDK',
            'whs_id'        => $whsId,
            'ctn_sts'       => 'AC'
        ])
            ->whereNotNull('loc_id')
            ->where('piece_remain', '>', 0)
            ->select([
                'piece_remain',
                DB::raw('SUM(piece_remain) AS total'),
                DB::raw('GROUP_CONCAT(DISTINCT loc_id) AS locs')
            ])
            ->groupBy('piece_remain')
            ->orderBy('piece_remain')
            ->get();

        return array_column($res->toArray(), null, 'piece_remain');
    }

    public function updatePickedFull($itemId, $lot, $limit, $pieces, $updateTime)
    {
        if ($limit == 0) {
            return;
        }

        $userId = Data::getCurrentUserId();
        $whsId = Data::getCurrentWhsId();
        $res = DB::table('cartons')->where([
            'item_id'       => $itemId,
            'lot'           => $lot,
            'piece_remain'  => $pieces,
            'loc_type_code' => 'XDK',
            'whs_id'        => $whsId,
            'ctn_sts'       => 'AC'
        ])
            ->limit($limit)
            ->update([
                'ctn_sts'          => 'PD',
                //'loc_id'           => null,
                //'loc_code'         => null,
                //'loc_name'         => null,
                //'plt_id'           => null,
                'picked_dt'        => time(),
                'updated_at'       => $updateTime,
                'updated_by'       => $userId,
                'storage_duration' => self::getCalculateStorageDurationRaw()
            ]);

        return $res;
    }

    public function updatePickedPiece($itemId, $lot, $pdPieces, $pieces, $updateTime)
    {
        if ($pdPieces == 0) {
            return;
        }
        
        $userId = Data::getCurrentUserId();
        $whsId = Data::getCurrentWhsId();
        $remainQty = $pieces - $pdPieces;
        $ctnObj = Carton::where([
            'item_id'       => $itemId,
            'lot'           => $lot,
            'piece_remain'  => $pieces,
            'loc_type_code' => 'XDK',
            'whs_id'        => $whsId,
            'ctn_sts'       => 'AC'
        ])
            ->first();


        if (empty($ctnObj)) {
            throw new HttpException(400, 'Cartons CrossDock is not enough!');
        }

        //clone new Carton
        $cloneCtnArr = $ctnObj->toArray();
        unset($cloneCtnArr['ctn_id']);

        $maxCtnNum = $this->getMaxCtnNum($ctnObj->ctn_num);
        $cloneCtnArr['piece_remain'] = $pdPieces;
        $cloneCtnArr['origin_id'] = $ctnObj->ctn_id;
        //$cloneCtnArr['loc_id'] = $cloneCtnArr['loc_code'] = null;
        $cloneCtnArr['ctn_sts'] = 'PD';
        $cloneCtnArr['picked_dt'] = time();
        $cloneCtnArr['ctn_num'] = ++$maxCtnNum;
        $cloneCtnArr['inner_pack'] = 0;
        $cloneCtnArr['updated_at'] = $updateTime;
        $cloneCtnArr['created_by'] = Data::getCurrentUserId();
        $cloneCtnArr['updated_by'] = Data::getCurrentUserId();

        DB::table('cartons')->insert($cloneCtnArr);

        //update origin carton
        $ctnObj->piece_remain = $remainQty;

        return $ctnObj->save();
    }

    public function getMaxCtnNum($ctnNum)
    {
        $maxCtnNum = $this->model->where('ctn_num', 'LIKE', $ctnNum . '-%')->max('ctn_num');
        if ($maxCtnNum) {
            return $maxCtnNum;
        }

        return $ctnNum . '-C00';
    }

    public function xdocCreatePackRef($condition)
    {
        $packRefs = $this->model->select([
            'length',
            'width',
            'height'
        ])
            ->where($condition)
            ->distinct()
            ->get();

        foreach ($packRefs as $packRef) {
            $this->xdocInsertDimension($packRef->length, $packRef->width, $packRef->height, 'CT');
        }

        return $this->packRef;
    }

    public function xdocInsertDimension($length, $width, $height, $pack_type)
    {
        $key = $dimension = sprintf("%sx%sx%s", $length, $width, $height);
        if (!array_key_exists($key, $this->packRef)) {
            $packRefModel = new PackRefModel();
            $result = $packRefModel->getFirstWhere([
                'dimension' => $dimension,
                'pack_type' => $pack_type

            ]);
            if (empty($result)) {
                $packRefModel = new PackRefModel();
                $result = $packRefModel->create([
                    'width'      => $width,
                    'height'     => $height,
                    'length'     => $length,
                    'dimension'  => $dimension,
                    'pack_type'  => $pack_type,
                    'created_at' => time(),
                    'updated_at' => time()
                ]);
            }
            $this->packRef[$key] = object_get($result, 'pack_ref_id');
        }

        return $this->packRef[$key];
    }
}