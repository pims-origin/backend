<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

use App\Jobs\BillableStorageJob;
use App\Jobs\BillableLaborJob;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Populating receiving summary - rcv_sum TABLE
        $schedule->call('App\Api\V1\Models\PopulateInvoiceModel@createMonthly')
                 ->monthlyOn(1, '00:00');

        $schedule->call('App\Api\V1\Models\PopulateInvoiceModel@createWeekly')
                 ->weekly()->mondays()->at('00:00');
        
        if (env('APP_HOURLY', false)) {
            $schedule->call('App\Api\V1\Models\PopulateInvoiceModel@createWeekly')
                     ->hourly();
        }

        // Billable IMS Sending Queue

        $schedule->call(function () {
            dispatch(new BillableStorageJob());
        })->monthlyOn(1, '03:00');

        $schedule->call(function () {
            dispatch(new BillableLaborJob());
        })->daily();
    }
}
