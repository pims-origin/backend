<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\Warehouse;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\Message;

class AsnControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var
     */
    protected $goodsReceiptId;
    /**
     * @var
     */
    protected $warehouseId;
    /**
     * @var
     */
    protected $customerId;
    /**
     * @var
     */
    protected $asnHeaderId;

    /**
     * @return string
     */
    public function getEndPoint()
    {
        return "/v1/asns/";
    }

    /**
     * @return string
     */
    public function getEndPointContainer()
    {
        return "/v1/asns/{$this->asnHeaderId}/containers/";
    }

    /**
     * @var array
     */
    protected $asnParams = [
        "whs_id"           => 1,
        "cus_id"           => 1,
        "asn_ref"          => "58411",
        "exp_date"         => "2016-07-20",
        "measurement_code" => "CM",
        "ctnr_num"         => 123,
        "details"          => []
    ];

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        // create data into DB
        $warehouse = factory(Warehouse::class)->create();
        $customer = factory(Customer::class)->create();

        $this->warehouseId = $warehouse->whs_id;
        $this->customerId = $customer->cus_id;

        $this->asnParams['whs_id'] = $this->warehouseId;
        $this->asnParams['cus_id'] = $this->customerId;
        $this->asnParams['asn_ref'] = str_random();
        $this->asnParams['ctnr_num'] = str_random();
        $this->asnParams['exp_date'] = date('Y-m-d');

        $l = rand(1, 10);
        $i = 0;
        while ($i < $l) {
            $this->asnParams['details'][] = [
                "dtl_sku"       => str_random(),
                "dtl_size"      => rand(0, 999),
                "dtl_color"     => str_random(),
                "dtl_width"     => rand(0, 999),
                "dtl_length"    => rand(0, 999),
                "dtl_height"    => rand(0, 999),
                "dtl_weight"    => rand(0, 999),
                "dtl_pack"      => rand(0, 999),
                "dtl_uom_id"    => rand(0, 999),
                "dtl_po"        => str_random(),
                "dtl_po_date"   => date('Y-m-d'),
                "dtl_ctn_ttl"   => rand(10, 20),
                "dtl_crs_doc"   => rand(0, 10),
            ];

            ++$i;
        }
    }

    /**
     *
     */
    public function testStore_Ok()
    {
        $response = $this->call('POST', $this->getEndPoint(), $this->asnParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        $responseData = json_decode($response->getContent(), true);


        $this->assertNotEmpty($responseData['data']);
    }

    /**
     *
     */
    public function testStore_NoParam_Fail()
    {
        $this->call('POST', $this->getEndPoint());

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     *
     */
    public function testStore_UnderZero_Fail()
    {
        $this->asnParams['details'][0]['dtl_width'] = -12;

        $this->call('POST', $this->getEndPoint(), $this->asnParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     *
     */
    public function testStore_DuplicateAsnRefCustomer_Fail()
    {
        $this->call('POST', $this->getEndPoint(), $this->asnParams);
        $response = $this->call('POST', $this->getEndPoint(), $this->asnParams);

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(
            Message::get("BM006", "ASN Ref Code"),
            $responseData['errors']['message']
        );
    }

    /**
     *
     */
    public function testUpdate_Ok()
    {
        // make data
        $asn = factory(Seldat\Wms2\Models\AsnHdr::class)->create();

        $this->asnHeaderId = $asn->asn_hdr_id;

        $ctnr_id = rand(0, 1000);

        $response = $this->call('PUT', $this->getEndPointContainer() . $ctnr_id, $this->asnParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    /**
     *
     */
    public function testUpdate_AsnHdrNotExisted_Fail()
    {

        $this->asnHeaderId = PHP_INT_MAX;

        $ctnr_id = rand(0, 1000);

        $response = $this->call('PUT', $this->getEndPointContainer() . $ctnr_id, $this->asnParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            'The ASN is not existed!',
            json_decode($response->getContent(), true)['errors']['message']
        );
    }

    /**
     *
     */
    public function testUpdate_StatusNotNew_Fail()
    {
        // make data
        $asn = factory(Seldat\Wms2\Models\AsnHdr::class)->create();

        //update asn_sts to Receiving
        $asn->asn_sts = "RG";
        $asn->save();

        $this->asnHeaderId = $asn->asn_hdr_id;
        $ctnr_id = rand(0, 1000);

        $response = $this->call('PUT', $this->getEndPointContainer() . $ctnr_id, $this->asnParams);

        // Case 1: Response code is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM032"),
            json_decode($response->getContent(), true)['errors']['message']
        );
    }

    /**
     *
     */
    public function testUpdate_DuplicateRef_Fail()
    {
        // make data
        $asnOld = AsnHdr::first();

        $asn = factory(Seldat\Wms2\Models\AsnHdr::class)->create();

        $this->asnHeaderId = $asn->asn_hdr_id;

        $this->asnParams['asn_ref'] = object_get($asnOld, 'asn_hdr_ref', null);
        $this->asnParams['cus_id'] = object_get($asnOld, 'cus_id', null);

        $ctnr_id = rand(0, 1000);

        $response = $this->call('PUT', $this->getEndPointContainer() . $ctnr_id, $this->asnParams);

        // Case 1: Response code is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM006", "ASN Ref Code"),
            json_decode($response->getContent(), true)['errors']['message']
        );
    }

    /**
     *
     */
    public function testUpdate_NoParam_Fail()
    {
        // make data
        $asn = factory(Seldat\Wms2\Models\AsnHdr::class)->create();

        $this->asnHeaderId = $asn->asn_hdr_id;

        $ctnr_id = rand(0, 1000);

        $this->call('PUT', $this->getEndPointContainer() . $ctnr_id);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     *
     */
    public function testSearch_Ok()
    {
        // create param search
        $params = [
            'asn_number' => str_random(),
            'whs_id'     => $this->asnParams['whs_id'],
            'cus_id'     => rand(0, 1000),
            'ctn_num'    => str_random(),
            'po'         => str_random(),
            'sku'        => str_random(),
        ];
        $search = [];
        $index = rand(-1, 5);
        if ($index > 0) {
            switch ($index) {
                case 1 :
                    $search = array_except($params, 'asn_number');
                    break;
                case 2 :
                    $search = array_except($params, 'whs_id');
                    break;
                case 3 :
                    $search = array_except($params, 'ctn_num');
                    break;
                case 4 :
                    $search = array_except($params, 'po');
                    break;
                case 5 :
                    $search = array_except($params, 'sku');
                    break;
                default :
                    break;
            }
        }

        // Test Show
        $response = $this->call('GET', $this->getEndPoint(), $search);

        // Case 1: Response code is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is not empty
        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData);
    }

    /**
     * pt: Test Show a controller.
     * Input: Valid Param
     * Response: OK.
     */
    public function test_Show_Ok()
    {
        // Create ASN before Show
        $aSNHdrId = factory(\Seldat\Wms2\Models\AsnHdr::class)->create()->asn_hdr_id;

        // Create Container before Show
        $containerId = factory(\Seldat\Wms2\Models\Container::class)->create()->ctnr_id;

        // Test Show
        $data = $this->call('GET', $this->getEndPoint() . $aSNHdrId . "/containers/" . $containerId);

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);
    }

    /**
     *
     */
    public function test_ShowNotExitAsnHrId_Fail()
    {

        $aSNHdrId = '999999999999999999999';

        // Create Container before Show
        $containerId = factory(\Seldat\Wms2\Models\Container::class)->create()->ctnr_id;

        // Test Show
        $data = $this->call('GET', $this->getEndPoint() . $aSNHdrId . "/containers/" . $containerId);

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            'The ASN is not existed!',
            json_decode($data->content(), true)['errors']['message']
        );
    }

    /**
     *
     */
    public function test_Show_MissingContainerParam()
    {
        // Create ASN before Show
        $aSNHdrId = factory(\Seldat\Wms2\Models\AsnHdr::class)->create()->asn_hdr_id;

        // Test Show
        $data = $this->call('GET', $this->getEndPoint() . $aSNHdrId . "/containers/");

        // Case 1: Response code is 404 Not Found: missing containers param
        $this->assertEquals(\Dingo\Api\Http\Response::HTTP_NOT_FOUND, $data->status());
    }

    /**
     *
     */
    public function test_getAsnStatus_OK()
    {
        $url = "/v1/asn-statuses";

        $response = $this->call('GET', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    /**
     *
     */
    public function test_listContainer_Ok()
    {
        $asnHdr = factory(\Seldat\Wms2\Models\AsnHdr::class)->create();
        factory(\Seldat\Wms2\Models\AsnDtl::class)->create([
            'asn_hdr_id' => $asnHdr->asn_hdr_id
        ]);

        // Test Search
        $this->call('GET', $this->getEndPoint() . $asnHdr->asn_hdr_id . '/list-containers');

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }

}
