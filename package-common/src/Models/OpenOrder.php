<?php

namespace Seldat\Wms2\Models;

class OpenOrder extends BaseModel
{
    protected $table = 'open_ord';
    protected $primaryKey = 'open_ord_id';

    protected $fillable = [
        'ord_id',
        'sts',
        'created_at',
        'created_by',
    ];

    public $timestamps = FALSE;

    /*
    ****************************************************************************
    */

    public function csrUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    /*
    ****************************************************************************
    */

}
