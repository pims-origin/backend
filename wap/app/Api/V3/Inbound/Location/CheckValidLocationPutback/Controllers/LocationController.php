<?php

namespace App\Api\V3\Inbound\Location\CheckValidLocationPutback\Controllers;

use App\Api\V3\Inbound\Location\CheckValidLocationPutback\Models\LocationModel;
use App\Api\V3\Inbound\Location\CheckValidLocationPutback\Models\PalletModel;
use App\Api\V3\Inbound\Location\CheckValidLocationPutback\Models\CustomerModel;
use App\libraries\RFIDValidate;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\SelArr;

class LocationController extends AbstractController
{
    const DEFAULT_LOT_PUTBACK = "NA";

    protected $locationModel;
    protected $palletModel;
    protected $customerModel;

    public function __construct()
    {
        $this->locationModel = new LocationModel();
        $this->palletModel   = new PalletModel();
        $this->customerModel = new CustomerModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function checkValidLocation($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input = SelArr::removeNullOrEmptyString($input);
        /*
         * start logs
         */

        $url = "/v3/whs/{$whsId}/location/rack/check-valid-by-customer";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'VLP',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Check valid Location Putback'
        ]);
        /*
         * end logs
         */

        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        $pltRfid    = array_get($input, 'plt_rfid', null);
        $location   = array_get($input, 'location', null);
        $cusId      = array_get($input, 'cus_id', null);
        $ctnPutback = array_get($input, 'ctn_ttl', 0);
//        $packSize   = array_get($input, 'pack_size', 0); // ignore pack_size
        $itemId     = (int)array_get($input, 'item_id', 0);
        $typeLoc    = (int)array_get($input, 'type_loc', 0);

        DB::setFetchMode(\PDO::FETCH_ASSOC);

        if ($pltRfid) {
            // check pallet in table pallet, vtl_ctn
            $pallet = $this->palletModel->getFirstWhere([
                'whs_id' => $whsId,
                'rfid'   => $pltRfid,
            ]);

            if ($pallet && $pallet->cus_id != $cusId) {
                $msg = sprintf("The Pallet Rfid %s doesn't belong to current customer.",
                    $pltRfid, $cusId);
                return $this->_responseErrorMessage($msg);
            }
        }

        // Check customer
        $cusArray = DB::table('customer_warehouse')
            ->where('cus_id', $cusId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->first();
        if (!count($cusArray)) {
            $cusObj = $this->customerModel->getFirstWhere(['cus_id' => $cusId]);
            if (!$cusObj) {
                $msg = sprintf("The customer Id %d doesn't exist.", $cusId);
            } else {
                $msg = sprintf("The customer %s doesn't belong to current warehouse.", $cusObj->cus_name);
            }
            return $this->_responseErrorMessage($msg);
        }

        //Check existed for The loc RFID
        // type location, 1: location rfid, 2: loc_code
        $locationInfo = '';
        if ($typeLoc == 1) {
            $locationInfo = $this->locationModel->getFirstWhere(['rfid' => $location]);
        } else {
            $locationInfo = $this->locationModel->getFirstWhere(['loc_code' => $location]);
        }

        if (!$locationInfo) {
            $msg = sprintf("Location does not exist.");
            return $this->_responseErrorMessage($msg);
        }

        if ($locationInfo && $locationInfo->loc_whs_id != $whsId) {
            $msg = sprintf("The Location %s doesn't belong to current warehouse.", $locationInfo->loc_code);
            return $this->_responseErrorMessage($msg);
        }

        if ($locationInfo && $locationInfo->loc_sts_code != "AC") {
            $msg = sprintf("Location %s is not Active.", $locationInfo->loc_code);
            return $this->_responseErrorMessage($msg);
        }

        if ($locationInfo && (!$locationInfo->rfid || $locationInfo->rfid == '')) {
            $msg = sprintf("Location %s does not be Rfid",
                $locationInfo->loc_code);
            return $this->_responseErrorMessage($msg);
        }

        try {

            $locRfid = $locationInfo->rfid;

            $checkLocation = DB::table('location')
                ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
                ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
                ->join('customer_zone', 'zone.zone_id', '=', 'customer_zone.zone_id')
                ->where('location.rfid', $locRfid)
                ->first();

            if (count($checkLocation)) {
                // if (array_get($checkLocation, 'loc_type_code') != "RAC"){
                //     $msg = sprintf("Unable Put back cartons to Location is not RAC type.");
                //     return $this->_responseErrorMessage($msg);
                // }

                if (array_get($checkLocation, 'cus_id') != $cusId){
                    $msg = sprintf("The Location doesn't belong to customer.");
                    return $this->_responseErrorMessage($msg);
                }
            }

            $limit = 1;
            // WAP-584 - Create API get suggest location list by Pallet tag
            if ($pltRfid) {
                $checkLocationHasPallet = $this->palletModel->getFirstWhere(['loc_id' => $locationInfo->loc_id]);
                if ($checkLocationHasPallet) {
                    if (!$checkLocationHasPallet->rfid){
                        $checkLocationHasPallet->rfid = $checkLocationHasPallet->plt_num;
                    }
                    $msg = sprintf("The location %s existed a Pallet %s.", $locationInfo->loc_code, $checkLocationHasPallet->rfid);
                    return $this->_responseErrorMessage($msg);
                }
                $data = [
                    'location'   => $locationInfo,
                ];
                //o Suggest empty locations if WAP send cartons within a pallet
                $locations = $this->locationModel->getMoreEmptyLocationByCusId($whsId, $cusId, $data,'RAC', $limit);

                if (!count($locations)) {
                    $msg = sprintf("The location is invalid.");

                    return $this->_responseErrorMessage($msg);
                }

            } else {
                $pallet = $this->palletModel->getFirstWhere(['loc_id' => $locationInfo->loc_id]);
                if (!$pallet) {
                    $msg = sprintf("The location doesn't contain any pallet.");
                    return $this->_responseErrorMessage($msg);
//                } else {
//                    $limitCtn = object_get($pallet, 'init_ctn_ttl', 0) - object_get($pallet, 'ctn_ttl', 0);
//                    if ($limitCtn < $ctnPutback) {
//                        $msg = sprintf("There are %d cartons. But the location cannot contain more %d cartons.", $ctnPutback, $limitCtn);
//                        return $this->_responseErrorMessage($msg);
//                    }
                }
                $data = [
                    'ctn_ttl'   => $ctnPutback,
//                    'pack_size' => $packSize,
//                    'lot'       => self::DEFAULT_LOT_PUTBACK,
                    'item_id'   => null,
                    'location'  => $locationInfo
                ];

                //o	Suggest locations at the level 1 and less cartons
                $locations = $this->locationModel->getMoreLocationCanPutCarton($whsId, $cusId, $data, 'RAC', $limit);
                if (!count($locations)) {
                    $msg = sprintf("The location is invalid.");
                    $cartonError = DB::table('cartons')
                        ->where('loc_id', $locationInfo->loc_id)
                        ->whereNotNull('rfid')
                        ->first();
                    if (!count($cartonError)) {
                        $msg = sprintf("The location have to exist at least one carton is Rfid.");
                    }
                    return $this->_responseErrorMessage($msg);
                }
            }

            // Check location is different sku with current one
            $isSameSku = true;
            if (!$pltRfid) {
                $isSameSku = $this->_checkSameSku($locationInfo->loc_id, $itemId);
            }

            $msg = sprintf("Successfully!");

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => $msg
                    ]
                ],
                'data' => [[
                    'is_same_sku' => $isSameSku,
                    'loc_id'      => $locationInfo->loc_id,
                    'loc_code'    => $locationInfo->loc_code,
                    'rfid'        => $locationInfo->rfid,
                ]]
            ];

        } catch (\Exception $e) {

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _validateParams($attributes, $whsId)
    {
        $pltRfid  = array_get($attributes, 'plt_rfid', null);
        $location = array_get($attributes, 'location', null);
        $cusId    = array_get($attributes, 'cus_id', null);
        $ctnTtl   = array_get($attributes, 'ctn_ttl', null);
//        $packSize = array_get($attributes, 'pack_size', null);
        $itemId   = (int)array_get($attributes, 'item_id', null);
        $typeLoc  = (int)array_get($attributes, 'type_loc', null);

        $names = [
            'plt_rfid'  => 'Pallet Rfid',
            'location'  => 'Location',
            'cus_id'    => 'Customer',
            'ctn_ttl'   => 'Number of Cartons',
//            'pack_size' => 'Pack Size',
            'item_id'   => 'Item Id',
            'type_loc'  => 'Type location',
        ];

        // Check Required
        $requireds = [
//            'plt_rfid' => $pltRfid,
            'location'  => $location,
            'cus_id'    => $cusId,
            'ctn_ttl'   => $ctnTtl,
//            'pack_size' => $packSize,
            'item_id'   => $itemId,
            'type_loc'  => $typeLoc,
        ];

        // Check int type and greater than 0
        $intGreaterThan0 = [
            'cus_id'    => $cusId,
            'ctn_ttl'   => $ctnTtl,
//            'pack_size' => $packSize,
            'item_id'   => $itemId,
            'type_loc'   => $typeLoc,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet rfid is string
        if ($pltRfid && !is_string($pltRfid)) {
            $msg = $names['plt_rfid'] . " must be string type";
            return $this->_responseErrorMessage($msg);
        }

        // location rfid is string
        if (!is_string($location)) {
            $msg = $names['location'] . " must be string type";
            return $this->_responseErrorMessage($msg);
        }

        foreach ($intGreaterThan0 as $key => $field) {
            if (!is_int($field) || $field < 1) {
                $errorDetail = "{$names[$key]} must be integer type and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        //validate pallet RFID
        if ($pltRfid) {
            $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
            if (!$pltRFIDValid->validate()) {
                return $this->_responseErrorMessage($pltRFIDValid->error);
            }
        }

        if ($typeLoc == 1) {
            //validate location RFID
            $locRFIDValid = new RFIDValidate($location, RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                return $this->_responseErrorMessage($locRFIDValid->error);
            }
        }

    }

    private function _checkSameSku($locId, $currItem)
    {
        $items = DB::table('cartons')
            ->where('loc_id', $locId)
            ->groupBy('item_id')
            ->pluck('item_id');

        if (count($items) && in_array($currItem, $items)) {
            return true;
        }

        return false;

    }
}