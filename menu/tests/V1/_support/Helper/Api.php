<?php
namespace Helper;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Api extends \Codeception\Module
{
    public function getApplication()
    {
        return $this->getModule('Lumen')->app ;
    }

    /**
     * Define custom actions here
     */

    /**
     * Inserts record into the database without primary key in table.
     *
     * ``` php
     * <?php
     * $user_id = $I->haveRecord('users', array('name' => 'Davert'));
     * ?>
     * ```
     *
     * @param string
     * @param array $attributes
     */
    public function haveRecordNoPrimary($tableName, $attributes = array())
    {
        $app = $this->getModule('Lumen')->app;
        $app['db']->table($tableName)->insert($attributes);
    }
}
