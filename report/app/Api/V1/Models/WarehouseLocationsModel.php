<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\WarehouseLocations;
use Seldat\Wms2\Utils\SelArr;

class WarehouseLocationsModel extends AbstractModel
{
    public function __construct(WarehouseLocations $model = NULL)
    {
        $this->model = ($model) ?: new WarehouseLocations();
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with)
                ->where('loc_whs_id', $attributes['whs_id']);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $attributes['compare_operator']['loc_code'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'loc_code'
            ],
            'attributes' => $attributes,
        ]);

        return $this->runSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

    /*
    ****************************************************************************
    */

}
