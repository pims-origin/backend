<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemUom;

class SystemUomModel extends AbstractModel
{
    /**
     * SystemUomModel constructor.
     * @param SystemUom|null $model
     */
    public function __construct(SystemUom $model = null)
    {
        $this->model = ($model) ?: new SystemUom();
    }

    /**
     * @param array $params
     * @param array $with
     *
     * @return mixed
     */
    public function loadBy($params = [], $with = [])
    {
        $query = $this->make($with);

        if (!empty($params) && is_array($params)) {
            foreach ($params as $key => $value) {
                $query->where($key, $value);
            }
        }
        // Get
        $models = $query->get();
        return $models;
    }
    
    /**
     * @param $sysUomIds
     *
     * @return string
     */
    public function getSysUomByIds($sysUomIds = [])
    {
        $sysUomIds = is_array($sysUomIds) ? $sysUomIds : [$sysUomIds];
        if (empty($sysUomIds)) {
            return [];
        }
        $sysUoms = $this->model
            ->whereIn('sys_uom_id', $sysUomIds)
            ->get();

        return $sysUoms;
    }
}
