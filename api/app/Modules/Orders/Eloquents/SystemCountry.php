<?php

namespace App\Modules\Orders\Eloquents;

class SystemCountry extends BaseSoftModel
{
    protected $table = 'system_country';

    protected $primaryKey = 'sys_country_id';

    protected $fillable = [
        'sys_country_code',
        'sys_country_name',
        'ordinal'
    ];
}
