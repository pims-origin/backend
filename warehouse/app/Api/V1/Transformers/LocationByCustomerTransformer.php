<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\Reason;

class LocationByCustomerTransformer extends TransformerAbstract
{
    /**
     * @param Location $location
     *
     * @return array
     */
    public function transform(Location $location)
    {
        return [
            'loc_id'                 => $location->loc_id,
            'loc_code'               => $location->loc_code,
            'loc_alternative_name'   => $location->loc_alternative_name,
            'loc_sts_code'           => $location->loc_sts_code,
            'loc_sts_code_name'      => !empty($location->locationStatus->loc_sts_name)
                ? $location->locationStatus->loc_sts_name
                : '',
            'has_item'               => $location->has_item,
            'has_pallet'             => (int)object_get($location, 'has_pallet', 0),
        ];
    }
}
