#########################-START-VERSION-6-##################################
- WAP-646: [Outbound - Wave pick] Remove Return Cartons Feature
    + Remove return cartons feature
    + Ignore Picked Cartons

- WAP-707: [Outbound - Wavepick] Modify API update wavepick
    + Completed wave pick detail -> release reserve location
    + Validate location for Update Wave pick
#########################-END-VERSION-6-##################################

#########################-START-VERSION-4-##################################
- WAP-501: [Outbound - Wavepick] Update API update wavepick for processing wrong cartons. The rules for this:
    + Ignore wrong cartons are gone through the gateway for a wave pick.
    + Return wrong cartons, WMS just assigns these cartons to the current pallet.

- WAP-506: [Outbound - Wavepick] Update API Return Cartons from Wave pick
    + Required Pallet RFID is optional.

- WAP-623: [Outbound - Wave Pick] Auto assign cartons to Order if 1 Wave Pick just have only 1 Order (Case RFID)
#########################-END-VERSION-4-##################################