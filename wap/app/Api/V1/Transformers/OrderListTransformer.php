<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\WavePickDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

class OrderListTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $ordDetail = new OrderDtlModel();
        $item = $ordDetail->findWhere(['odr_id' => $orderHdr->odr_id]);
        $item = $item->toArray();
        $cus = new CustomerModel();
        $cusName = $cus->getFirstBy('cus_id', $orderHdr->cus_id);
        $ordCtn = new OrderCartonModel();
        $item_ = [];
        $totalItem = [];
        foreach ($item as $it) {
            $sts = $it['itm_sts'];
            if($sts == NULL){
                $sts = 'NW';
            }
            $item_['ord_dtl_id'] = $it['odr_dtl_id'];
            $item_['sku'] = $it['sku'];
            $item_['size'] = $it['size'];
            $item_['color'] = $it['color'];
            $item_['pack'] = $it['pack'];
            $item_['itm_sts'] = $sts;
            $item_['itm_sts_name'] = Status::getByKey('ORDER-STATUS', $sts);
            $item_['item_id'] = $it['item_id'];
            $act = $ordCtn->findWhere([
                'odr_dtl_id'   => $it['odr_dtl_id']
            ]);

            //get available carton piece_qty where odr_dtl_id = null and ctn_sts = Picking

            //total = $act->piece_qty + available carton piece_qty

            $numCtn = ceil($it['alloc_qty'] / $it['pack']);
            $item_['alloc_piece'] = $it['alloc_qty'] . '/0';
            $item_['alloc_carton'] = $numCtn . '/0';
            if (!empty($act)) {
                $act = $act->toArray();
                $numPi = 0;
                foreach($act as $t){
                    $numPi = $numPi + $t['piece_qty'];
                }
                $item_['alloc_piece'] = $it['alloc_qty'] . '/' . $numPi;
                $item_['alloc_carton'] = $numCtn . '/' . count($act);
            }
            $totalItem[] = $item_;
        }

        return [
            'odr_id'       => $orderHdr->odr_id,
            'cus_id'       => $orderHdr->cus_id,
            'odr_num'      => $orderHdr->odr_num,
            'wv_id'        => $orderHdr->wv_id,
            'wv_num'       => $orderHdr->wv_num,
            "cus_odr_num"  => $orderHdr->cus_odr_num,
            'cus_name'     => $cusName->cus_name,
            'whs_id'       => $orderHdr->whs_id,
            'odr_sts'      => $orderHdr->odr_sts,
            'odr_sts_name' => Status::getByKey('ORDER-STATUS', $orderHdr->odr_sts),
            'created_at'   => $orderHdr->created_at->format('m/d/Y'),
            'total-sku'    => count($item),
            'items'        => $totalItem
        ];

    }
}
