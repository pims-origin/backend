<?php

namespace App\Api\Inventory\Controllers;

use App\Api\Inventory\Models\CycleDtlModel;
use App\Api\Inventory\Models\CycleHdrModel;
use App\Api\Inventory\Models\EventTrackingModel;
use App\Api\Inventory\Services\CycleCountService;
use App\Api\Inventory\Traits\CycleHdrControllerTrait;
use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\View;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\Inventory\Validators\CycleHdrValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use App\Api\Inventory\Transformers\CycleHdrTransformer;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\Inventory\Validators\CusInUserValidator;
use Wms2\UserInfo\Data;

class CycleHdrController extends AbstractController
{
    use CycleHdrControllerTrait;

    /**
     * @var CycleHdrModel
     */
    protected $model;

    /**
     * @var CycleHdrValidator
     */
    protected $validator;

    /**
     * @var $cycleHdrTransform
     */
    protected $cycleHdrTransform;

    /**
     * @var CycleCountService
     */
    protected $cycleCountService;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;


    /**
     * CycleHdrController constructor.
     *
     * @param CycleHdrModel $model
     * @param CycleHdrValidator $validator
     * @param CycleCountService $cycleCountService
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        //CycleHdrModel $model,
        //CycleHdrValidator $validator,
        //CycleCountService $cycleCountService,
        //EventTrackingModel $eventTrackingModel
    ) {
        //$this->model = $model;
        //$this->validator = $validator;
        //$this->cycleHdrTransform = new CycleHdrTransformer();
        //$this->cycleCountService = $cycleCountService;
        //$this->eventTrackingModel = $eventTrackingModel;

        $this->model = new CycleHdrModel();
        $this->validator = new CycleHdrValidator();
        $this->cycleHdrTransform = new CycleHdrTransformer();
        $this->cycleCountService = new CycleCountService(new CycleHdrModel(), new CycleDtlModel());
        $this->eventTrackingModel = new EventTrackingModel();
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $input = $request->getParsedBody();

        $this->validator->validate($input);
        //generate event code
        $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingNum();
        $input['cycle_num'] = $cycleNumAndSeq['cycle_num'];

        $data = $this->model->processCreateCycleHdr($input);

        if (!$data) {
            $errorMsg = implode($this->model->errors);

            return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }
        //event tracking
        $trans_num = str_replace(CYCLE_PREFIX, EventTrackingModel::CYCLE_CREATED_ASSIGNED,
            $cycleNumAndSeq['cycle_num']);
        $this->eventTrackingModel->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => ucfirst($input['cycle_type']) == 'CS' ? $input['cycle_detail'] : null,
            'owner'     => $cycleNumAndSeq['cycle_num'],
            'evt_code'  => EventTrackingModel::CYCLE_CREATED_ASSIGNED,
            'trans_num' => $trans_num,
            'info'      => $cycleNumAndSeq['cycle_num'] . ' ' .
                EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_CREATED_ASSIGNED]
        ]);

        return $this->response->item($data, new CycleHdrTransformer());
    }

    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        $input = $request->getQueryParams();

        try {
            if (!isset($input['sort'])) {
                $input['sort'] = [
                    'cycle_hdr_id' => 'desc'
                ];
            }
            $data = $this->model->search($input, ['warehouse', 'assigneeUser'], array_get($input, 'limit', 20));

            return $this->response->paginator($data, $this->cycleHdrTransform);
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $cycleHdrId
     *
     * @return string
     */
    public function view($cycleHdrId)
    {
        try {
            $data = $this->model->getCycleInfo($cycleHdrId);

            return $data ? $data->toArray() : null;

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $cycleHdrId
     *
     * @return mixed
     */
    public function printPdf($cycleHdrId)
    {
        return $this->cycleCountService->printCycleCountDetail($cycleHdrId);
    }

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function cycleType()
    {
        $types = $this->model->getCycleTypes();

        return $types;
    }

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function cycleStatus()
    {
        $statuses = $this->model->getCycleStatus();

        return $statuses;
    }

    public function destroy(Request $request)
    {
        $input = $request->getParsedBody();

        $cycle_hdr_ids = $input['cycle_hdr_ids'];
        // Parse to integer
        $ccHdrLstAfters = [];
        foreach ($cycle_hdr_ids as $cycle_hdr_id) {
            array_push($ccHdrLstAfters, (int)$cycle_hdr_id);
        }

        $response = $this->cycleCountService->deleteCycleCounts($ccHdrLstAfters);

        if (!$response['status']) {
            return $this->response->error(implode($response), IlluminateResponse::HTTP_BAD_REQUEST);
        }

        return $this->response->accepted(null, ['message' => Message::get('BM074'), 'data' => 'success']);
    }

    /**
     * @param $cycleHdrId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function saveCycled($cycleHdrId)
    {
        $model = new CycleDtlModel();
        $result = $model->processSavedCycled($cycleHdrId);

        if (!$result) {
            $errorMsg = implode($model->errors);

            return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }
        //event tracking
        $data = CycleHdrModel::getCycleHdr($cycleHdrId);
        $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingTransNum(
            $cycleHdrId,
            EventTrackingModel::CYCLE_CYCLED
        );
        $this->eventTrackingModel->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => ucfirst($data['cycle_type']) == 'CS' ? $data['cycle_detail'] : null,
            'owner'     => $cycleNumAndSeq['cycle_num'],
            'evt_code'  => EventTrackingModel::CYCLE_CYCLED,
            'trans_num' => $cycleNumAndSeq['trans_num'],
            'info'      => $cycleNumAndSeq['trans_num'] . ' ' .
                EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_CYCLED]
        ]);

        return $this->response->accepted(null, ['message' => Message::get('BM075'), 'data' => 'success']);
    }

    /**
     *
     */
    public function checkCusInUser(Request $request)
    {
        $this->validator = new CusInUserValidator();

        $input = $request->getParsedBody();
        $this->validator->validate($input);

        $model = new CycleHdrModel();

        $result = $model->checkCusInUser($input);

        if (!$result) {
            return $this->response->accepted(null, [
                'message' => Message::get('BM076'),
                'status'  => false
            ]);
        }

        return $this->response->accepted(null, ['message' => Message::get('BM077'), 'status' => true]);
    }

    /**
     * @param $whsId
     */
    public function getUserWhs()
    {
        $whsId = CycleCountService::getCurrentWhs();
        $result = $this->model->getUserInWarehouse($whsId);

        return ['data' => $result];
    }
}
