<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use App\Api\V1\Traits\WorkOrderTrait;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\BOLOrderDetail;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{
    use WorkOrderTrait;

    const DAS_LIMIT = 0;

    protected $statusType = [
        'csr'               => ['NW'],
        'allocate'          => ['NW', 'AL', 'PAL'],
        'wave-pick'         => ['PK', 'PPK', 'PD', 'PIP', 'AL', 'PAL'],
        'packing'           => ['PD', 'PIP', 'PN', 'PPA', 'PA', 'PAP'],
        'pallet-assignment' => ['PA', 'PAP'],
//        'shipping'          => ['SS', 'RS', 'SH', 'ST', 'PSH'],
        'shipping'          => ['SS', 'RS', 'SH'],
        'cancel-order'      => ['CC', 'PCC', 'RMA']
    ];

    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     * @param BOLOrderDetail|null $bolModel
     */
    public function __construct(OrderHdr $model = null, BOLOrderDetail $bolModel = null)
    {
        $this->model = ($model) ?: new OrderHdr();
        $this->bolModel = ($bolModel) ?: new BOLOrderDetail();
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $orderHdrId
     *
     * @return mixed
     */
    public function deleteOrderHdr($orderHdrId)
    {
        return $this->model
            ->where('odr_id', $orderHdrId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null, $export = false)
    {
        $query = $this->make($with);

        //is_ecom
        $isEcom = 0;

        if (!empty($attributes['type']) && $attributes['type'] == "order") {
            $query->whereHas('shippingOrder', function ($sh) {
                $sh->where('type', '!=', Status::getByValue("Ecomm", "Order-Type"));
            });
        } else if (!empty($attributes['type']) && $attributes['type'] == "online-order") {
            $isEcom = 1;
            $query->whereHas('shippingOrder', function ($sh) {
                $sh->where('type', Status::getByValue("Ecomm", "Order-Type"));
            });
        }

        $query->where('odr_hdr.is_ecom', $isEcom);

        // Order Status
        if (!empty($attributes['odr_sts'])) {
            $query->where(function ($query) use ($attributes) {
                foreach ($attributes['odr_sts'] as $index => $odrSts) {
                    if ($index === 0) {
                        $this->whereBySts($odrSts, $query);
                    } else {
                        $query->orWhere(function ($sub) use ($odrSts) {
                            $this->whereBySts($odrSts, $sub);
                        });
                    }
                }
            });
        } else if (!empty($attributes['type']) && !empty($this->statusType[$attributes['type']])) {
            $query->whereIn('odr_sts', $this->statusType[$attributes['type']]);
        }

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrEqual = ['cus_id'];
        $arrLike = ['odr_num', 'cus_odr_num', 'cus_po'];

        if (!empty($attributes)) {
            if (!empty($attributes['odr_type'])) {
                $query->where("odr_type", $attributes['odr_type']);
            } else {
                $query->whereIn('odr_hdr.odr_type', ['BU', 'RTL', 'BAC', 'XDK', 'TK', 'WHS']);
            }

            foreach ($attributes as $key => $value) {
                if ($key === "csr") {
                    $query->whereHas("csrUser", function ($q) use ($attributes) {
                        $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere(
                            DB::raw("concat(first_name, ' ', last_name)"),
                            'like',
                            "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['csr'])) . "%"
                        );
                    });
                    continue;
                }
                if (in_array($key, $arrLike)) {
                    $query->where($key, "like", "%" . SelStr::escapeLike($value) . "%");
                    continue;
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        // search dashboard day
        if (isset($attributes['day']) && is_numeric($attributes['day'])) {
            $limitday = (!empty($attributes['day']) && is_numeric($attributes['day'])) ? $attributes['day'] : self::DAS_LIMIT;
            $query->where('odr_hdr.updated_at', '>=',
                DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $limitday . ' DAY)'));
        }

        // search dashboard status
        if (isset($attributes['dashboard_status'])) {
            $odrSts = [
                'NW'  => [Status::getByValue("New", "Order-status")],
                'IP'  => [
                    Status::getByValue("Allocated", "Order-status"),
                    Status::getByValue("Partial Allocated", "Order-status"),
                    Status::getByValue("Picking", "Order-status"),
                    Status::getByValue("Partial Picking", "Order-status"),
                    Status::getByValue("Picked", "Order-status"),
                    Status::getByValue("Partial Picked", "Order-status"),
                    Status::getByValue("Packing", "Order-status"),
                    Status::getByValue("Partial Packing", "Order-status"),
                    Status::getByValue("Packed", "Order-status"),
                    Status::getByValue("Partial Packed", "Order-status"),
//                    Status::getByValue("Staging", "Order-status"),
//                    Status::getByValue("Partial Staging", "Order-status"),
                ],
                'OS'  => [
//                    Status::getByValue("Staging", "Order-status"),
//                    Status::getByValue("Partial Staging", "Order-status"),
                ],
                'BAC' => [Status::getByValue("BackOrder", "Order-type")],
                'FN'  => [Status::getByValue("Final", "SHIP-STATUS")]
            ];

            if ($attributes['dashboard_status'] == 'new') {
                $query->whereIn('odr_sts', $odrSts['NW']);
            }

            if ($attributes['dashboard_status'] == 'inprocess') {
                $query->whereIn('odr_sts', $odrSts['IP']);
                $query->select('odr_hdr.*');
                $query->leftJoin('shipment', 'odr_hdr.ship_id', '=', 'shipment.ship_id');
                $query->where(function ($query) use ($odrSts) {
                    $query->whereNotIn('shipment.ship_sts', $odrSts['FN'])
                        ->orWhere('shipment.ship_sts', null);
                });
            }

            if ($attributes['dashboard_status'] == 'ship') {
                $query->whereIn('odr_sts', $odrSts['OS']);
                $query->select('odr_hdr.*');
                $query->leftJoin('shipment', 'odr_hdr.ship_id', '=', 'shipment.ship_id');
                $query->whereIn('shipment.ship_sts', $odrSts["FN"]);
            }

            if ($attributes['dashboard_status'] == 'back') {
                $query->whereIn('odr_type', $odrSts['BAC']);
                $query->whereNotIn('odr_sts', ['CC', 'SH']);
            }

            if ($attributes['dashboard_status'] == 'readyToShip') {
                $query->where('odr_sts', 'RS');
            }

            if ($attributes['dashboard_status'] == 'scheduledToShip') {
                $query->where('odr_sts', 'SS');
            }

            if ($attributes['dashboard_status'] == 'shipped') {
                $query->where('odr_sts', 'SH');
            }
        } else {
            // search dashboard in-process
            if (isset($attributes['inprocess_status'])) {
                $inProcessSts = [
                    'AL' => [
                        Status::getByValue("Allocated", "Order-status"),
                        Status::getByValue("Partial Allocated", "Order-status")
                    ],
                    'PK' => [
                        Status::getByValue("Picking", "Order-status"),
                        Status::getByValue("Partial Picking", "Order-status")
                    ],
                    'PD' => [
                        Status::getByValue("Picked", "Order-status"),
                        Status::getByValue("Partial Picked", "Order-status")
                    ],
                    'PN' => [
                        Status::getByValue("Packing", "Order-status"),
                        Status::getByValue("Partial Packing", "Order-status")
                    ],
                    'PA' => [
                        Status::getByValue("Packed", "Order-status"),
                        Status::getByValue("Partial Packed", "Order-status")
                    ],
                    'ST' => [
//                        Status::getByValue("Staging", "Order-status"),
//                        Status::getByValue("Partial Staging", "Order-status")
                    ],
                    'FN' => [Status::getByValue("Final", "SHIP-STATUS")]
                ];

                if ($attributes['inprocess_status'] == 'allocated') {
                    $query->whereIn('odr_sts', $inProcessSts['AL']);
                }

                if ($attributes['inprocess_status'] == 'picking') {
                    $query->whereIn('odr_sts', $inProcessSts['PK']);
                }

                if ($attributes['inprocess_status'] == 'packing') {
                    $query->whereIn('odr_sts', $inProcessSts['PN']);
                }

                if ($attributes['inprocess_status'] == 'staging') {
//                    $query->whereIn('odr_sts', $inProcessSts['ST']);
                    $query->select('odr_hdr.*');
                    $query->leftJoin('shipment', 'odr_hdr.ship_id', '=', 'shipment.ship_id');
                    $query->where(function ($query) use ($inProcessSts) {
                        $query->whereNotIn('shipment.ship_sts', $inProcessSts['FN'])
                            ->orWhere('shipment.ship_sts', null);
                    });
                }

                if ($attributes['inprocess_status'] == 'picked') {
                    $query->whereIn('odr_sts', $inProcessSts['PD']);
                }

                if ($attributes['inprocess_status'] == 'packed') {
                    $query->whereIn('odr_sts', $inProcessSts['PA']);
                }
            }
        }

        // search sku
        if (isset($attributes['sku'])) {
            $query->whereHas('details', function ($query) use ($attributes) {
                $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
            });
        }
        //search schedule ship date
        if (isset($attributes['schd_ship_dt'])) {
            $time_start = date('Y-m-d 00:00:00', strtotime($attributes['schd_ship_dt']));//2018-5-24 00:00:00
            $time_end = date('Y-m-d H:i:s', strtotime('+23 hours +59 minutes', strtotime($time_start)));
            $query->where('schd_ship_dt', '>=', strtotime($time_start))
                ->where('schd_ship_dt', '<=', strtotime($time_end));
        }
        //search carrier
        if (isset($attributes['carrier'])) {
            $query->where('carrier', 'like', "%" . SelStr::escapeLike($attributes['carrier']) . "%");
        }
        //search rush order
        if (isset($attributes['rush_odr'])) {
            if($attributes['rush_odr'] =='0' ){
                //no rush order
                $query->where('rush_odr', $attributes['rush_odr']);
            }
            if($attributes['rush_odr'] =='priority' ){
                //rush order
                $query->whereIn('rush_odr', ['1', '2', '3', '4', '5']);
            }
        }

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $cusIds = $predis->getCurrentCusIds();

        $currentWH = array_get($userInfo, 'current_whs', 0);

        // dashboard order late
        if (!empty($attributes['order_late'])) {
            $query->whereRaw('DATE(FROM_UNIXTIME(ship_by_dt)) < DATE(NOW())')
                ->whereNotIn('odr_sts', ['SH', 'CC']);
        }
        // dashboard items bo
        if (!empty($attributes['item_bo'])) {
            $query->where('odr_type', 'BAC')
                ->whereNotIn('odr_sts', ['SH', 'CC'])
                ->whereRaw('DATE(FROM_UNIXTIME(ship_by_dt)) < DATE(NOW())');
        }
        // dashboard NON-Interactive orders
        if (!empty($attributes['non_interactive_ord'])) {
            $whsConfig = DB::table('whs_meta')
                ->where('whs_id', $currentWH)
                ->where('whs_qualifier', 'wno')
                ->first();

            if(!is_null($whsConfig)) {
                $nonInteractiveDay = array_get($whsConfig, 'whs_meta_value', 0) * 86400;
                $query->where('odr_sts', 'NW')
                      ->where('odr_hdr.updated_at', '<', time() + $nonInteractiveDay);
            }
        }

        $this->sortBuilder($query, $attributes);

        $query->where('odr_hdr.whs_id', $currentWH);

        $query->whereIn('odr_hdr.cus_id', $cusIds);

        $query->whereNotIn('odr_hdr.odr_sts', ['ST', 'PSH']);

        if ($export) {
            $models = $query->get();
        } else {
            $models = $query->paginate($limit);
        }

        return $models;
    }

    public function getOrderNum()
    {
        $currentYearMonth = date('ym');
        $defaultWoNum = "ORD-${currentYearMonth}-00001";
        $order = $this->model
            ->whereIn('odr_type', ['BU', 'KIT', 'RTL', 'XDK', 'EC', 'TK', 'PC', 'WHS'])
            ->orderBy('odr_num', 'desc')
            ->first();
        $lastNum = object_get($order, 'odr_num', '');
        if (empty($lastNum) || strpos($lastNum, "-${currentYearMonth}-") === false) {
            return $defaultWoNum;
        }

        return ++$lastNum;
    }

    /**
     * @param $odrNum
     *
     * @return string
     */
    public function getBackOrderNum($odrNum)
    {
        $order = $this->model
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "-%")
            ->orderBy('odr_id', 'desc')
            ->first();
        $index = 0;
        $orderNum = object_get($order, "odr_num", null);

        if ($orderNum) {
            $temp = explode("-", $orderNum);
            $index = (int)end($temp);
        } else {
            $orderNum = $odrNum;
        }

        return substr($orderNum, 0, 14) . "-" . str_pad(++$index, 2, "0", STR_PAD_LEFT);
    }

    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function checkWhereIn($orderIds)
    {
        return $this->model
            ->whereIn('odr_id', $orderIds)
            ->count();
    }

    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function getOrderById($order_ids)
    {
        $order_ids = is_array($order_ids) ? $order_ids : [$order_ids];

        $rows = $this->model
            ->whereIn('odr_id', $order_ids)
            ->get();

        return $rows;
    }


    public function fetchColumn($columns = [], $where = "", $params = [], $upperCase = false)
    {
        $tblName = $this->getTable();
        $colNames = implode(', ', $columns);

        $query = "
            SELECT $colNames
            FROM $tblName
        ";
        if (!empty($where)) {
            $query .= " WHERE $where";
        }

        $result = [];

        $db = $this->model->getConnection()->getPdo();

        $statement = $db->prepare($query);
        $statement->execute($params);

        $colKey = $columns;
        end($columns);
        $endKey = key($columns);
        unset($colKey[$endKey]);

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            end($row);
            $end = key($row);
            $val = $row[$columns[$endKey]];
            unset($row[$end]);
            $key = implode("-", $row);

            if ($upperCase) {
                $key = strtoupper($key);
            }

            $result[$key] = $val;
        }

        return $result;
    }

    /**
     * @param $orderHdrIds
     * @param array $with
     *
     * @return mixed
     */
    public function loadByOrdIds($orderHdrIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('odr_id', $orderHdrIds);
        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }

    /**
     * @param $skuTotal
     *
     * @return array
     */
    public function updateImportSku($skuTotal)
    {
        $result = [];
        foreach ($skuTotal as $count => $odrIds) {
            $this->refreshModel();
            $result[] = $this->model
                ->whereIn('odr_id', $odrIds)
                ->update(['sku_ttl' => $count]);
        }

        return $result;
    }

    /**
     * @param $input
     * @param array $with
     *
     * @return mixed
     */
    public function getOrderAddress($input, $with = [])
    {
        $bolOrder = (new BOLOrderDetailModel)->loadExcept()->toArray();
        $exceptIds = array_pluck($bolOrder, 'odr_id');
        if (!empty($input['odr_id'])) {
            $exceptIds = array_diff($exceptIds, $input['odr_id']);
        }

        $query = $this->make($with);
        $query->select([
            DB::raw('group_concat(odr_id) as order_ids'),
            DB::raw('trim(replace(replace(concat(ship_to_name, ", ", ship_to_add_1, ", ", ifnull(ship_to_add_2, ""),
            ", ", ship_to_city, ", ", ship_to_state, " ", ship_to_zip, " ", ship_to_country), ", ,", ","), "  ", " ")) as
            full_address'),
            'carrier',
            'ship_method'
        ]);
        $days = $input['ship_ready'];
        if ($days != 'all') {
            $from = strtotime(date('Y-M-d', strtotime('now')) . '00:00:00');
            $days--;
            $to = strtotime(date('Y-M-d', strtotime('now')) . '23:59:59');
            if ($days > 1) {
                $from = strtotime(date('Y-M-d', strtotime("-$days days")) . ' 00:00:00');
            }

            $query->whereBetween('updated_at', [$from, $to]);
        }

        if (empty($input['t']) || $input['t'] != "l") {
            // Create BOL
            $query->where('cus_id', $input['cus_id']);
            // if (!empty($input['odr_id'])) {
            //     $query->whereIn('odr_id', $input['odr_id']);
            // } else {
                // $query->whereNotIn('odr_id', $exceptIds);
            // }
        } else {
            $bolOrder = (new BOLOrderDetailModel)->loadExcept()->toArray();
            $exceptIds = array_pluck($bolOrder, 'odr_id');
            if (!empty($input['odr_id'])) {
                $exceptIds = array_diff($exceptIds, $input['odr_id']);
            }
            // List BOL
            if (!empty($input['cus_id']) && $input['cus_id'] != 'all' && is_numeric($input['cus_id'])) {
                $query->where('cus_id', $input['cus_id']);
            }
            $query->whereIn('odr_id', $exceptIds);

        }

        $query->whereIn('odr_sts', ["RS", "SS", "ST"]);

        $query->groupBy('full_address');

        $this->model->filterData($query, true);

        return $query->get();
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getBackOrderList($odrNum)
    {
        $backOrderList = $this->model
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "-%")
            ->get();

        return $backOrderList;
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getOriginOrder($odrNum)
    {
        $backOrderList = $this->model
            ->where('odr_num', substr($odrNum, 0, 14))
            ->first();

        return $backOrderList->odr_id;
    }

    public function validateCancelOdr($odrObj)
    {
        $odrStsNotAllowCancel = [
            Status::getByValue("Partial Shipped", "Order-status"),
            Status::getByValue("Hold", "Order-status"),
            Status::getByValue("Shipped", "Order-status"),
            Status::getByValue("Canceled", "Order-status")
        ];

        $odrStatus = $odrObj->odr_sts;
        if (in_array($odrStatus, $odrStsNotAllowCancel)) {
            return false;
        }

        $chkShipFinal = \DB::table('shipment')
            ->where('deleted', 0)
            ->where('ship_id', $odrObj->ship_id)
            ->where('ship_sts', 'FN')
            ->count();

        if ($chkShipFinal) {
            return false;
        }

        return true;
    }

    /**
     * @param $odrId
     *
     * @return bool
     */
    public function getWorkOrderTrueFalseFromOdrId($odrId)
    {
        $workOrderModel = new WorkOrderModel();
        $work_order = false;
        if ($odrId) {
            if ($wo = $workOrderModel->checkWhere(
                [
                    'odr_hdr_id' => $odrId
                ])
            ) {
                $work_order = true;
            }
        }

        return $work_order;
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getDistinctWavePickByOrderIds($odrNum)
    {
        $rows = $this->model
            ->distinct('wv_id')
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "%")
            ->get(['wv_id']);

        return $rows;
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getAllOrderIdsAccordingToOriginOrdNum($odrNum)
    {
        $rows = $this->model
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "%")
            ->get();

        return $rows;
    }

    /**
     * @return mixed
     */
    public function dashboardOdr($input = [])
    {
        $odrSts = [
            'NW'  => [Status::getByValue("New", "Order-status")],
            'IP'  => [
                Status::getByValue("Allocated", "Order-status"),
                Status::getByValue("Partial Allocated", "Order-status"),
                Status::getByValue("Picking", "Order-status"),
                Status::getByValue("Partial Picking", "Order-status"),
                Status::getByValue("Picked", "Order-status"),
                Status::getByValue("Partial Picked", "Order-status"),
                Status::getByValue("Packing", "Order-status"),
                Status::getByValue("Partial Packing", "Order-status"),
                Status::getByValue("Packed", "Order-status"),
                Status::getByValue("Partial Packed", "Order-status")
            ],
            'OS'  => [
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status")
            ],
            'BAC' => [Status::getByValue("BackOrder", "Order-type")],
            'FN'  => [Status::getByValue("Final", "SHIP-STATUS")]
        ];

        foreach ($odrSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $odrSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $query = DB::table('odr_hdr as odr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['NW']}) THEN 1 ELSE 0 END), 0) AS newOrders,
	            COALESCE( SUM(CASE WHEN odr.odr_sts IN ({$odrSts['IP']}) THEN 1
		            WHEN (odr.odr_sts IN ({$odrSts['OS']})
		                AND shipment.`ship_sts` NOT IN ({$odrSts['FN']})) THEN 1
		            ELSE 0 END), 0) AS inprocessOrders,
	            COALESCE( SUM(CASE WHEN (odr.odr_sts IN ({$odrSts['OS']})
	                    AND shipment.`ship_sts` IN ({$odrSts['FN']})) THEN 1
	                ELSE 0 END), 0) AS orderReadyToShip,
	            COALESCE( SUM(CASE WHEN odr.odr_type IN ({$odrSts['BAC']})
	 	            THEN 1 ELSE 0 END), 0) AS backOrders
            "))
            ->leftJoin('shipment', 'odr.ship_id', '=', 'shipment.ship_id')
            ->where('odr.deleted', 0)
            ->where('odr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr.whs_id', $currentWH);

        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('odr.cus_id', $cus_ids);

        $model = $query->get();

        return $model;
    }

    /**
     * @return mixed
     */
    public function dashboardInprocess($input = [])
    {
        $odrSts = [
            'AL' => [
                Status::getByValue("Allocated", "Order-status"),
                Status::getByValue("Partial Allocated", "Order-status")
            ],
            'PK' => [
                Status::getByValue("Picked", "Order-status"),
                Status::getByValue("Partial Picked", "Order-status"),
                Status::getByValue("Picking", "Order-status"),
                Status::getByValue("Partial Picking", "Order-status")
            ],
            'PN' => [
                Status::getByValue("Packing", "Order-status"),
                Status::getByValue("Partial Packing", "Order-status"),
                Status::getByValue("Packed", "Order-status"),
                Status::getByValue("Partial Packed", "Order-status")
            ],
            'ST' => [
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status")
            ],
            'FN' => [Status::getByValue("Final", "SHIP-STATUS")]
        ];

        foreach ($odrSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $odrSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $query = DB::table('odr_hdr as odr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['AL']})
                    THEN 1 ELSE 0 END), 0) AS allocated,
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PK']})
	                THEN 1 ELSE 0 END), 0) AS picked,
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PN']})
	                THEN 1 ELSE 0 END), 0) AS packing,
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['ST']})
	                    AND shipment.`ship_sts` NOT IN ({$odrSts['FN']})
	                THEN 1 ELSE 0 END), 0) AS staging
            "))
            ->leftJoin('shipment', 'odr.ship_id', '=', 'shipment.ship_id')
            ->where('odr.deleted', 0)
            ->where('odr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr.whs_id', $currentWH);

        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('odr.cus_id', $cus_ids);

        $model = $query->get();

        return $model;
    }

    /**
     * @param array $attributes
     *
     * @return mixed
     */
    public function getOrderIdAccordingToOdrNum($attributes = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $odr_num = '';
        if (isset($attributes['odr_num'])) {
            $odr_num = $attributes['odr_num'];
        }

        return $this->model
            ->where('odr_num', 'like', "%" . SelStr::escapeLike($odr_num) . "%")
            ->groupBy('odr_num')
            ->get();
    }

    /**
     * @param $orderIds
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function _CanceledOrdersSearch($orderIds, $attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->whereIn('odr_id', $orderIds);

        if (isset($attributes['cus_id'])) {
            $query->whereHas('customer', function ($query) use ($attributes) {
                $query->where('cus_id', $attributes['cus_id']);
            });
        }

        if (isset($attributes['odr_type'])) {
            $query->where('odr_type', $attributes['odr_type']);
        }

        // Check odr_type!='ECO' & odr_sts = cancel & not in Return Order
        $query->whereNotIn('odr_type', ['ECO']);
        $query->whereIn('odr_sts', ['CC']);

        $query->whereHas('oderCarton', function ($query) use ($attributes) {
            $query->where('piece_qty', '>', 0);
        });

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'cancel_date') {
                    $attributes['sort']['updated_at'] = $val;
                    unset($attributes['sort'][$key]);
                }

            }
        }

        $this->sortBuilder($query, $attributes);
        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }

    public function CanceledOrdersSearch($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->leftJoin('return_hdr', 'odr_hdr.odr_id', '=', 'return_hdr.odr_hdr_id')
            ->select('odr_hdr.*')
            ->whereNull('return_hdr.return_id');
        if (isset($attributes['odr_num'])) {
            $query->where('odr_hdr.odr_num', 'like', "%" . $attributes['odr_num'] . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->whereHas('customer', function ($query) use ($attributes) {
                $query->where('cus_id', $attributes['cus_id']);
            });
        }

        if (isset($attributes['odr_type'])) {
            $query->where('odr_type', $attributes['odr_type']);
        }

        // Check odr_type!='ECO' & odr_sts = cancel & not in Return Order
        $query->whereNotIn('odr_type', ['ECO']);
        $query->where('odr_sts', 'CC');

        $query->whereHas('oderCarton', function ($query) use ($attributes) {
            $query->where('piece_qty', '>', 0);
        });

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'cancel_date') {
                    $attributes['sort']['updated_at'] = $val;
                    unset($attributes['sort'][$key]);
                }

            }
        }

        $this->sortBuilder($query, $attributes);
        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }

    public function createBackOrder($odrHdr, $odrDtls)
    {
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $odrDtlEntity = new OrderDtl();
        /**
         * 1.check backorder = true
         * 1.2 - query order hdr backorder with status = new or create new with seq num + 1 (nO WV)
         * 1.3 query backorder dtl of backorder ID, item_id, lot
         * 1.4 update backorder dtl
         * 1.5 if backorder dtl  not existed , create new
         */
        $odrId = $odrHdr->odr_id;

        // get order flow origin order

        $orderHdrMetaModel = new OdrHdrMetaModel();
        $orderFlow = $orderHdrMetaModel->getFirstWhere(['odr_id' => $odrId, 'qualifier' => 'OFF']);

        $odrHdrBac = null;

        foreach ($odrDtls as $odrDtl) {
            if (!$odrDtl->back_odr) {
                continue;
            }
            //check if order is bac => deny create bac
            if ($odrHdr->org_odr_id) {
                $msg = sprintf("Backorder %s cannot accept to create sub Backorder", $odrHdr->odr_num);
                throw new \Exception($msg);
            }

            if (!$odrHdrBac) {
                if ($odrHdr->odr_type == Status::getByValue('Bulk', 'ORDER-TYPE')) {
                    $origin_num = substr(object_get($odrHdr, 'odr_num', ''), 0, 14);
                    $countOrders = OrderHdr::where('odr_num', 'LIKE', '%' . $origin_num . '%')->count();
                    if ($countOrders > 1) {
                        throw new \Exception('Unable to create two backorders.');
                    }
                }
                //get bac ord HDR
                $odrHdrBac = $this->model
                    ->where('odr_sts', 'NW')
                    ->where('org_odr_id', $odrId)
                    ->first();

                if (!$odrHdrBac) {
                    $odrHdrBac = $odrHdr->replicate();
                    $odrHdrBac->odr_num = $odrHdrBac->odr_num . '-01';
                    $odrHdrBac->org_odr_id = $odrId;
                    $odrHdrBac->odr_id = null;
                    $odrHdrBac->wv_id = null;
                    $odrHdrBac->wv_num = null;
                    $odrHdrBac->odr_sts = 'NW';
                    $odrHdrBac->sku_ttl = 0;
                    $odrHdrBac->back_odr = 0;
                    $odrHdrBac->back_odr_seq = 1;
                    $odrHdrBac->odr_type = Status::getByValue('BackOrder', 'ORDER-type');
                    $odrHdrBac->push();
                }

                $bacOrders[$odrId]['hdr'] = $odrHdrBac;
            }

            $odrHdrBac->sku_ttl++;

            //get bac order detail
            $bacOdrDtl = $odrDtlEntity
                ->where('odr_id', $odrHdrBac->odr_id)
                ->where('item_id', $odrDtl->item_id)
                ->where('lot', 'ANY')
                ->first();

            if (!empty($bacOdrDtl)) {
                //update
                $bacOdrDtl->piece_qty = $odrDtl->back_odr_qty;
                $bacOdrDtl->back_odr_qty = 0;
                $bacOdrDtl->back_odr = false;
                $bacOdrDtl->save();
            } else {
                //create
                $bacOdrDtl = null;
                $bacOdrDtl = $odrDtl->replicate();

                $bacOdrDtl->odr_id = $odrHdrBac->odr_id;
                $bacOdrDtl->piece_qty = $odrDtl->back_odr_qty;
                $bacOdrDtl->back_odr_qty = 0;
                $bacOdrDtl->back_odr = false;
                $bacOdrDtl->alloc_qty = 0;
                $bacOdrDtl->itm_sts = 'NW';
                $bacOdrDtl->lot = 'ANY';
                $bacOdrDtl->qty = $odrDtl->pack > 0 ? ceil($odrDtl->back_odr_qty / $odrDtl->pack) : 0;
                $bacOdrDtl->created_at = time();
                $bacOdrDtl->updated_at = time();
                $bacOdrDtl->push();
            }
        }
        if ($odrHdrBac) {
            $odrHdrBac->save();

            // $orderHdrMetaModel->insertOdrHdrMeta([
            //     'odr_id'    => $odrHdrBac->odr_id,
            //     'qualifier' => 'OFF',
            //     'value'     => array_get($orderFlow, 'value', 0)
            // ]);
            $odrId = $odrHdrBac->odr_id;
            $qualifier = 'OFF';
            $orderHdrMetaModel->getModel()->updateOrCreate(
                [
                    'odr_id'    => $odrId,
                    'qualifier' => $qualifier,
                ],
                [
                    'odr_id'    => $odrId,
                    'qualifier' => $qualifier,
                    'value'     => array_get($orderFlow, 'value', 0),
                ]
            );

            // Insert Event Tracking
            $evtTracking = new EventTrackingModel(new EventTracking());
            $evtTracking->create([
                'whs_id'    => $odrHdrBac['whs_id'],
                'cus_id'    => $odrHdrBac['cus_id'],
                'owner'     => $odrHdr->odr_num,
                'evt_code'  => Status::getByKey("event", "NEW-BACK-ORDER"),
                'trans_num' => $odrHdrBac->odr_num,
                'info'      => sprintf(Status::getByKey("event-info", "BNW"), $odrHdrBac->odr_num, $odrHdr->odr_num)
            ]);
        }

    }

    /**
     * @param $orderId
     *
     * @return mixed
     */
    public function getBolLpnByOrdId($orderId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = OrderHdr::select(
            [
                'odr_hdr.odr_id',
                'odr_hdr.cus_id',
                'odr_hdr.ship_to_name',
                'odr_hdr.whs_id',
                'shipment.bo_num',
                'shipment.ship_id',
            ]
        )
            ->leftJoin('shipment', 'shipment.ship_id', '=', 'odr_hdr.ship_id')
            ->where('odr_hdr.odr_id', $orderId)
            ->distinct();

        $result = $query->first();

        return $result;
    }

    public function createCancelBacOrder($odrHdrObj, $odrDtlCancels)
    {
        $ttlSku = count($odrDtlCancels);
        $bacOdrHdrObj = $this->createCancelBacOrdHdr($odrHdrObj, $ttlSku);
        $this->createCancelBacOrdDtl($bacOdrHdrObj, $odrDtlCancels);


        if ($odrHdrObj->odr_sts === 'PN') {
            $isPa = DB::table('odr_dtl')->leftJoin('pack_dtl', function ($join) {
                $join->on('pack_dtl.odr_hdr_id', '=', 'odr_dtl.odr_id')
                    ->on('pack_dtl.item_id', '=', 'odr_dtl.item_id')
                    ->on('pack_dtl.lot', '=', 'odr_dtl.lot')
                    ->where('pack_dtl.deleted', '=', 0);
            })
                ->where([
                    'odr_dtl.odr_id'  => $odrHdrObj->odr_id,
                    'odr_dtl.deleted' => 0
                ])
                ->where('odr_dtl.picked_qty', '>', 0)
                ->groupBy('odr_dtl.odr_dtl_id')
                ->havingRaw("COALESCE(SUM(pack_dtl.piece_qty), 0) < odr_dtl.picked_qty")
                ->first(['odr_dtl.odr_id', 'odr_dtl.picked_qty']);

            if (empty($isPa)) {
                //update odr_dtl
                DB::table('odr_dtl')
                    ->where('odr_id', $odrHdrObj->odr_id)
                    ->where('deleted', 0)
                    ->where('picked_qty', ">", 0)
                    ->update(['itm_sts' => 'PA']);
                //update odr_hdr
                $odrHdrObj->odr_sts = 'PA';
                $odrHdrObj->save();
            }
        } else {
            //complete origin odr hdr
            DB::statement("
                UPDATE odr_hdr
                SET odr_hdr.odr_sts = IF(odr_hdr.no_pack_bol = 1, 'ST', 'PD')
                WHERE odr_hdr.odr_id = {$odrHdrObj->odr_id}
                AND odr_hdr.odr_sts = 'PK'
                AND NOT EXISTS (
                  SELECT odr_dtl.odr_id
                  FROM odr_dtl
                  WHERE odr_dtl.odr_id  = odr_hdr.odr_id
                  AND odr_dtl.alloc_qty != odr_dtl.picked_qty
                  AND odr_dtl.alloc_qty > 0
                  LIMIT 1
                )
            ");
        }

        return $bacOdrHdrObj;
    }

    public function createCancelBacOrdHdr($originOdrHdrObj, $ttlSku = 0)
    {
        $seq = $this->model->where('odr_num', 'LIKE', $originOdrHdrObj->odr_num . "-%")
            ->count();
        $seq++;

        $odrHdrBac = $originOdrHdrObj->replicate();
        $odrHdrBac->odr_num = $originOdrHdrObj->odr_num . '-' . str_pad($seq, 2, "0", STR_PAD_LEFT);;
        $odrHdrBac->org_odr_id = $originOdrHdrObj->odr_id;
        $odrHdrBac->odr_id = null;
        $odrHdrBac->wv_id = $originOdrHdrObj->wv_id;
        $odrHdrBac->wv_num = $originOdrHdrObj->wv_num;
        $odrHdrBac->odr_sts = $originOdrHdrObj->odr_sts;
        $odrHdrBac->sku_ttl = $ttlSku;
        $odrHdrBac->back_odr = 0;
        $odrHdrBac->back_odr_seq = $seq;
        $odrHdrBac->odr_type = 'BAC';
        $odrHdrBac->push();

        return $odrHdrBac;
    }

    public function createCancelBacOrdDtl($bacOdrHdrObj, $orginOdrDtlDatas)
    {
        foreach ($orginOdrDtlDatas as $orginOdrDtlData) {
            $originOdrDtlObj = $orginOdrDtlData['odr_dtl_obj'];
            $cancelQty = $orginOdrDtlData['cancel_qty'];
            $allocQty = $originOdrDtlObj->alloc_qty - $originOdrDtlObj->picked_qty;
            $newPickedQty = $cancelQty > $allocQty ? $cancelQty - $allocQty : 0;

            //create new bac order detail to cancel
            $bacOdrDtl = $originOdrDtlObj->replicate();
            $bacOdrDtl->odr_id = $bacOdrHdrObj->odr_id;
            $bacOdrDtl->piece_qty = $cancelQty;
            $bacOdrDtl->back_odr_qty = 0;
            $bacOdrDtl->back_odr = false;
            $bacOdrDtl->alloc_qty = $originOdrDtlObj->alloc_qty > 0 ? $cancelQty : 0;
            $bacOdrDtl->itm_sts = $originOdrDtlObj->itm_sts;
            $bacOdrDtl->qty = ceil($cancelQty / $originOdrDtlObj->pack);
            $bacOdrDtl->picked_qty = $newPickedQty;
            $bacOdrDtl->created_at = time();
            $bacOdrDtl->updated_at = time();
            $bacOdrDtl->push();

            //update allocate to origin odr dtl
            if ($originOdrDtlObj->alloc_qty > 0) {
                $originOdrDtlObj->alloc_qty -= $cancelQty;
                $originOdrDtlObj->back_odr_qty = $cancelQty;
                $originOdrDtlObj->back_odr = 1;
                $originOdrDtlObj->picked_qty -= $newPickedQty;
                if ($originOdrDtlObj->alloc_qty == $originOdrDtlObj->picked_qty && $originOdrDtlObj->itm_sts == 'PK') {
                    $originOdrDtlObj->itm_sts = 'PD';
                }

                $originOdrDtlObj->save();
            }

            //update odr cartons
            $odrCartonByPieces = $this->sumOdrCartonsByPieceQty($originOdrDtlObj->odr_dtl_id);
            foreach ($odrCartonByPieces as $odrCartonByPiece) {
                if ($odrCartonByPiece['ttl_piece'] < $newPickedQty) {
                    //update date to new odrDtl info
                    $this->updateFullOdrCartons($bacOdrHdrObj->odr_num, $originOdrDtlObj->odr_dtl_id,
                        $bacOdrDtl->odr_id, $bacOdrDtl->odr_dtl_id, $odrCartonByPiece['piece_qty']);
                    $newPickedQty -= $odrCartonByPiece['ttl_piece'];
                } else {
                    $limit = floor($newPickedQty / $odrCartonByPiece['piece_qty']);
                    $pieceOdrCtn = $newPickedQty % $odrCartonByPiece['piece_qty'];

                    if ($limit > 0) {
                        $this->updateFullOdrCartons($bacOdrHdrObj->odr_num, $originOdrDtlObj->odr_dtl_id,
                            $bacOdrDtl->odr_id,
                            $bacOdrDtl->odr_dtl_id, $odrCartonByPiece['piece_qty'], $limit);
                    }

                    if ($pieceOdrCtn > 0) {
                        $this->updatePieceOdrCartons($bacOdrHdrObj->odr_num, $originOdrDtlObj->odr_dtl_id,
                            $bacOdrDtl->odr_id,
                            $bacOdrDtl->odr_dtl_id, $odrCartonByPiece['piece_qty'], $pieceOdrCtn);
                    }

                    break;
                }
            }
        }

    }

    public function sumOdrCartonsByPieceQty($odrDtlId)
    {
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);

        return OrderCarton::where('odr_dtl_id', $odrDtlId)
            ->select(DB::raw('SUM(piece_qty) AS ttl_piece, piece_qty'))
            ->groupBy('piece_qty')
            ->orderBy('piece_qty', 'desc')
            ->get();
    }

    public function updateFullOdrCartons(
        $bacOdrNum,
        $origOdrDtlId,
        $bacOdrHdrId,
        $bacOdrDtlId,
        $pieceQty,
        $limit = null
    ) {

        $limit = empty($limit) ? 10000 : $limit;
        OrderCarton::where([
            'odr_dtl_id' => $origOdrDtlId,
            'piece_qty'  => $pieceQty
        ])
            ->limit($limit)
            ->update([
                'odr_dtl_id' => $bacOdrDtlId,
                'odr_hdr_id' => $bacOdrHdrId,
                'odr_num'    => $bacOdrNum
            ]);
    }

    public function updatePieceOdrCartons(
        $bacOdrNum,
        $origOdrDtlId,
        $bacOdrHdrId,
        $bacOdrDtlId,
        $pieceQty,
        $pieceOdrCtn
    ) {
        $originOdrCtn = OrderCarton::where([
            'odr_dtl_id' => $origOdrDtlId,
            'piece_qty'  => $pieceQty
        ])
            ->first();

        if ($pieceOdrCtn == $originOdrCtn->piece_qty) {
            $originOdrCtn->odr_hdr_id = $bacOdrHdrId;
            $originOdrCtn->odr_dtl_id = $bacOdrDtlId;
        } else {
            $originOdrCtn->piece_qty -= $pieceOdrCtn;

            //clone new odr ctn
            $newOdrCtn = $originOdrCtn->replicate();
            $newOdrCtn->piece_qty = $pieceOdrCtn;
            $newOdrCtn->odr_hdr_id = $bacOdrHdrId;
            $newOdrCtn->odr_dtl_id = $bacOdrDtlId;
            $newOdrCtn->odr_ctn_id = null;
            $newOdrCtn->odr_num = $bacOdrNum;
            $newOdrCtn->push();
        }

        $originOdrCtn->save();
    }

    /**
     * @param array $orderIds
     * @param array $with
     *
     * @return mixed
     */
    public function findWhereIn(array $orderIds, $with = [])
    {
        $query = $this->make($with);
        $model = $query->whereIn('odr_id', $orderIds)->get();

        return $model;
    }

    /**
     * @param array $orderId
     * @param array $with
     *
     * @return mixed
     */
    public function findOrderShipping($orderId, $with = [])
    {
        $query = $this->make($with);
        $model = $query->where('odr_id', $orderId)->get();

        return $model;
    }

    /**
     * @param $orderId
     *
     * @return mixed
     */
    public function findOrderShippingAccordingToPallet($orderId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('odr_hdr')
            ->select([
                //odr_hdr
                'odr_hdr.odr_id',
                'odr_hdr.ship_id',
                'odr_hdr.odr_num',
                'odr_hdr.ship_by_dt',
                'odr_hdr.cus_id',
                'odr_hdr.created_by',
                'odr_hdr.cus_odr_num',
                'odr_hdr.odr_sts',
                'odr_hdr.carrier as carrier_name',

                //Ship from
                's.ship_id',
                's.bo_num',
                //'odr_hdr.whs_id',
                //'s.ship_from_addr_1',
                //'s.ship_from_addr_2',
                //'s.ship_from_city',
                //'s.ship_from_state',
                //'s.ship_from_zip',

                //Ship To
                'odr_hdr.ship_to_name',
                'odr_hdr.ship_to_add_1',
                'odr_hdr.ship_to_add_2',
                'odr_hdr.ship_to_city',
                'odr_hdr.ship_to_state',
                'odr_hdr.ship_to_zip',
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(odr_hdr.cancel_by_dt), '%m/%d/%Y') AS cancel_by_dt"),
                DB::raw("IF(odr_hdr.cancel_by_dt, DATE_FORMAT(FROM_UNIXTIME(odr_hdr.cancel_by_dt), '%m/%d/%Y'), '') AS cancel_by_dt"),

                //Customer
                'odr_hdr.cus_po',
                'c.cus_id',
                'c.cus_name',
                'c.cus_code',

                // Customer address
                'cAdd.*',
                'st.sys_state_code',
                'st.sys_state_name',

                //Pack_hdr
                'p.pack_hdr_id',
                'p.out_plt_id',
                'p.pack_hdr_num',
                //'p.carrier_name',
                'p.item_id',
                DB::raw("GROUP_CONCAT(p.sku SEPARATOR ',') as sku"),
                'p.lot',
                'p.size',
                'p.color',
                'p.cus_upc',
                'p.out_plt_id',

                //Warehouse
                'w.whs_id',
                'w.whs_name',
                'w.whs_state_id',
                'w.whs_city_name',
                'wa.whs_add_line_1',
                'w.whs_code',
                'wa.whs_add_postal_code',

                //out_pallet
                'o.plt_num',
                DB::raw("IFNULL(o.length, '') as length"),
                DB::raw("IFNULL(o.width, '') as width"),
                DB::raw("IFNULL(o.height, '') as height"),
                DB::raw("IFNULL(o.weight, '') as weight"),

                DB::raw("(COUNT(DISTINCT p.pack_hdr_num) ) AS count_ctns"),
                DB::raw("(COUNT(p.out_plt_id) ) AS pallet_num"),
                DB::raw("(COUNT(DISTINCT p.pack_dt_checksum) ) AS checksum_ttl")

            ]);

        $query->leftJoin('pack_hdr as p', 'odr_hdr.odr_id', '=', 'p.odr_hdr_id');
        $query->leftJoin('shipment as s', 'odr_hdr.ship_id', '=', 's.ship_id');
        $query->leftJoin('customer as c', 'odr_hdr.cus_id', '=', 'c.cus_id');
        $query->leftJoin('cus_address as cAdd', 'c.cus_id', '=', 'cAdd.cus_add_cus_id');
        $query->leftJoin('warehouse as w', 'odr_hdr.whs_id', '=', 'w.whs_id');
        $query->join('out_pallet as o', 'p.out_plt_id', '=', 'o.plt_id');
        $query->leftJoin('system_state as st', 'st.sys_state_id', '=', 'cAdd.cus_add_state_id');
        $query->leftJoin('whs_address as wa', 'wa.whs_add_whs_id', '=', 'w.whs_id');

        $query->where('odr_hdr.odr_id', $orderId);
        $query->where('wa.whs_add_type', 'SH');
        $query->where('p.deleted', '0');
        $query->where('cAdd.cus_add_type', 'ship');
        $query->groupBy('p.out_plt_id');
        $row = $query->get();

        return $row;
    }

    public function findOrderShippingAccordingToCarton($orderId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('odr_hdr')
            ->select([
                //odr_hdr
                'odr_hdr.odr_id',
                'odr_hdr.ship_id',
                'odr_hdr.odr_num',
                'odr_hdr.ship_by_dt',
                'odr_hdr.cus_id',
                'odr_hdr.created_by',
                'odr_hdr.cus_odr_num',
                'odr_hdr.odr_sts',
                'odr_hdr.carrier as carrier_name',

                //Ship from
                's.ship_id',
                's.bo_num',
                //'odr_hdr.whs_id',
                //'s.ship_from_addr_1',
                //'s.ship_from_addr_2',
                //'s.ship_from_city',
                //'s.ship_from_state',
                //'s.ship_from_zip',

                //Ship To
                'odr_hdr.ship_to_name',
                'odr_hdr.ship_to_add_1',
                'odr_hdr.ship_to_add_2',
                'odr_hdr.ship_to_city',
                'odr_hdr.ship_to_state',
                'odr_hdr.ship_to_zip',

                //Customer
                'odr_hdr.cus_po',
                'c.cus_id',
                'c.cus_name',
                'c.cus_code',

                // Customer address
                'cAdd.*',
                'st.sys_state_code',
                'st.sys_state_name',

                //Pack_hdr
                'p.pack_hdr_id',
                'p.out_plt_id',
                'p.pack_hdr_num',
                //'p.carrier_name',
                'p.item_id',
                'p.sku',
                'p.lot',
                'p.size',
                'p.color',
                'p.cus_upc',
                'p.out_plt_id',
                'p.piece_ttl',

                //Warehouse and From
                'w.whs_id',
                'w.whs_name',
                'w.whs_state_id',
                'w.whs_city_name',
                'wa.whs_add_line_1',
                'w.whs_code',
                'wa.whs_add_postal_code',

                DB::raw("(COUNT(p.pack_hdr_id) ) AS carton_num")

            ]);

        $query->leftJoin('pack_hdr as p', 'odr_hdr.odr_id', '=', 'p.odr_hdr_id');
        $query->leftJoin('shipment as s', 'odr_hdr.ship_id', '=', 's.ship_id');
        $query->leftJoin('customer as c', 'odr_hdr.cus_id', '=', 'c.cus_id');
        $query->leftJoin('cus_address as cAdd', 'c.cus_id', '=', 'cAdd.cus_add_cus_id');
        $query->leftJoin('warehouse as w', 'odr_hdr.whs_id', '=', 'w.whs_id');
        $query->leftJoin('system_state as st', 'st.sys_state_id', '=', 'cAdd.cus_add_state_id');
        $query->leftJoin('whs_address as wa', 'wa.whs_add_whs_id', '=', 'w.whs_id');

        $query->where('odr_hdr.odr_id', $orderId);
        $query->where('p.deleted', '0');
        $query->where('wa.whs_add_type', 'SH');
        $query->where('cAdd.cus_add_type', 'ship');
        $query->groupBy('p.pack_hdr_id');
        $row = $query->get();

        return $row;
    }

    public function getWhsName($whsId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('warehouse')
            ->select([
                'warehouse.whs_name',
            ]);

        $query->where('warehouse.whs_id', $whsId);
        $row = $query->first();

        return $row;
    }

    /**
     * @param $orderId
     * @param $whsId
     *
     * @return mixed
     */
    public function getOrderHdrBy($orderId, $whsId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        //$query = DB::table('odr_hdr')
        $query = $this->model
            ->select([
                'odr_hdr.*',
            ]);

        $query->where('odr_hdr.odr_id', $orderId);
        $query->where('odr_hdr.whs_id', $whsId);
        $query->whereNotIn('odr_hdr.odr_sts', ['NW', 'AL', 'PK', 'CC']);
        $row = $query->first();

        return $row;
    }

    /**
     * @param $orderId
     * @param $whsId
     *
     * @return mixed
     */
    public function getOrderHdrRequestBy($orderId, $whsId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        //$query = DB::table('odr_hdr')
        $query = $this->model
            ->select([
                'odr_hdr.*',
            ]);

        $query->where('odr_hdr.odr_id', $orderId);
        $query->where('odr_hdr.whs_id', $whsId);
        //$query->whereNotIn('odr_hdr.odr_sts', ['NW', 'AL', 'PK', 'CC']);
        $row = $query->first();

        return $row;
    }

    /**
     * @param bool $id
     * @param $odrId
     * @param $csr
     *
     * @return mixed
     */
    public function checkDuplicate($id = false, $odrId, $csr)
    {
        $query = DB::Table('odr_csr');
        if ($id) {
            $query->where('odr_csr.odr_csr_id', '!=', $id);
        }
        $query->where('odr_csr.odr_id', $odrId);
        $query->where('odr_csr.csr', $csr);
        $model = $query->first();

        return $model;
    }

    /**
     * @param $oderId
     *
     * @return mixed
     */
    public function searchMultiCSR($oderId)
    {
        $query = DB::table("odr_csr")
            ->select([
                'odr_id',
                'csr'
            ])
            ->where('odr_id', $oderId);

        $models = $query->get();

        return $models;
    }

    public function whereBySts($odrSts, &$query)
    {
        $bacOdr_sts_len = strlen($odrSts);
        $partialStatusArr = [
            'PAL',
            'PPK',
            'PIP',
            'PPA',
            'PAP',
//            'PSH',
            'PRC',
            'PCC'
        ];
        if (in_array($odrSts, $partialStatusArr)) {
            switch ($odrSts) {
                case "PAL": //Partial Allocated
                    $query->where('odr_sts', 'AL')
                        ->where('back_odr', 1);
                    break;
                case "PPK": //Partial Picking
                    $query->where('odr_sts', 'PK')
                        ->where('back_odr', 1);
                    break;
                case "PIP": //Partial Picked
                    $query->where('odr_sts', 'PD')
                        ->where('back_odr', 1);
                    break;
                case "PPA": //Partial Packing
                    $query->where('odr_sts', 'PN')
                        ->where('back_odr', 1);
                    break;
                case "PAP": //Partial Packed
                    $query->where('odr_sts', 'PA')
                        ->where('back_odr', 1);
                    break;
//                case "PSH": //Partial Staging
//                    $query->where('odr_sts', 'ST')
//                        ->where('back_odr', 1);
//                    break;
                case "PRC":
                    //Partial Shipped
                    $query->where('odr_sts', 'SH')
                        ->where('back_odr', 1);
                    break;
                case "PCC":
                    // Partial Canceled
                    $query->where('back_odr', 1)
                        ->where('odr_sts', 'CC');
                    break;
            }
        } elseif ($bacOdr_sts_len == 2) {
            switch ($odrSts) {
                case "AL": //Allocated
                    $query->where('odr_sts', 'AL')
                        ->where('back_odr', 0);
                    break;
                case "PK": //Picking
                    $query->where('odr_sts', 'PK')
                        ->where('back_odr', 0);
                    break;
                case "PD": //Picked
                    $query->where('odr_sts', 'PD')
                        ->where('back_odr', 0);
                    break;
                case "PN": //Packing
                    $query->where('odr_sts', 'PN')
                        ->where('back_odr', 0);
                    break;
                case "PA": //Packed
                    $query->where('odr_sts', 'PA')
                        ->where('back_odr', 0);
                    break;
//                case "ST": //Staging
//                    $query->where('odr_sts', 'ST')
//                        ->where('back_odr', 0);
//                    break;
                case "SH": //Shipped
                    $query->where('odr_sts', 'SH')
                        ->where('back_odr', 0);
                    break;
                case "CC": //Canceled
                    $query->where('back_odr', 0)
                        ->where('odr_sts', 'CC');
                    break;
                case "SP": //Shipping
                    $query->where('odr_sts', 'SP');
                    break;
                case "SS": //Scheduled to ship
                    $query->where('odr_sts', 'SS');
                    break;
                case "RS": //Ready to ship
                    $query->where('odr_sts', 'RS');
                    break;
                case "NW": //New
                    $query->where('odr_sts', 'NW');
                    break;
                case "CO": //Completed
                    $query->where('odr_sts', 'CO');
                    break;
            }
        } else {
            $query->where('odr_sts', $odrSts);
        }
    }

    /**
     * @param $orderIds
     * @param $updateParams
     *
     * @return array
     */
    public function updateCommentsForManyOrders($orderIds, $updateParams)
    {
        $order_ids = is_array($orderIds) ? $orderIds : [$orderIds];
        $result = $this->model
            ->whereIn('odr_id', $order_ids)
            ->update($updateParams);

        return $result;
    }

    /**
     * @param $orderId
     * @param $updateParams
     */
    public function updateSoldTo($orderId, $updateParams)
    {
    	if (!$updateParams) {
    		return;
		}

		$odr = $this->model->findOrFail($orderId);
		if ($odrSoldTo = $odr->orderSoldTo) {
			$odrSoldTo->update($updateParams);
		} else {
			$updateParams['odr_id'] = $orderId;
			$odr->orderSoldTo()->create($updateParams);
		}
    }

}
