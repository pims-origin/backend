<?php

namespace Seldat\Wms2\Models;


class AsnHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'asn_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'asn_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'asn_hdr_seq',
        'asn_hdr_act_dt',
        'asn_hdr_ept_dt',
        'asn_hdr_num',
        'asn_hdr_ref',
        'asn_type',
        'cus_id',
        'whs_id',
        'asn_sts',
        'asn_hdr_des',
        'asn_hdr_ctn_ttl',
        'asn_hdr_itm_ttl',
        'sys_mea_code',
        'asn_hdr_ctnr_ttl',
    ];

    //--pt:show ASN detail
    public function asnStatus()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnStatus', 'asn_sts', 'asn_sts_code');
    }

    public function asnDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\AsnDtl', 'asn_hdr_id', 'asn_hdr_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }

    public function SystemMeasurement()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemMeasurement', 'sys_mea_code', 'sys_mea_code');
    }


	public function goodsReceipt()
	{
		return $this->hasMany(__NAMESPACE__ . '\GoodsReceipt', 'asn_hdr_id', 'asn_hdr_id')->orderBy('created_at', 'asc');
	}
}
