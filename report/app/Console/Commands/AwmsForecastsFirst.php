<?php

namespace App\Console\Commands;

use App\Api\V1\Models\ForecastModel;
use App\Api\V1\Traits\ForecastControllerTrait;
use Illuminate\Support\Facades\DB;

class AwmsForecastsFirst extends AbstractCommand
{
    use ForecastControllerTrait;
    protected $signature = 'awms-forecast-first {dateFrom} {dateTo}';
    protected $description = 'Awms Forecast first';
    protected $forecastModel;

    protected $mapping;
    protected $customerIds = '';
    protected $months;

    public function __construct()
    {
        parent::__construct();
        $this->forecastModel = new ForecastModel();
        $this->mapping = new AwmsMapping();
    }

    public function handle()
    {
        $dateTo = date('Y-m-t', strtotime($this->argument('dateTo')));
        $dateFrom = date('Y-m-01', strtotime($this->argument('dateFrom')));

        $arrDates = [];
        for ($i = $dateFrom; $i <= $dateTo; $i = date('Y-m', strtotime('+1 month', strtotime($i)))) {
            $arrDates[$i] = $i;
        }
        $this->months = $arrDates;

        $warehouses = array_filter($this->mapping->whsMap());
        if (empty ($warehouses)) {
            return false;
        }
        $warehouses = array_flip($warehouses);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {

            DB::beginTransaction();

            foreach ($warehouses as $whsId => $AWMS_WhsId) {

                if (!$AWMS_WhsId || !$whsId) {
                    continue;
                }

                $this->customerIds = $this->mapping->customerMapping($whsId, 'AWMS');
                if (empty($this->customerIds)) {
                    continue;
                }

                $fc_indicators = DB::table('fc_indicator')->where('deleted', 0)->get();

                $fc_ind_ids = array_pluck($fc_indicators, 'fc_ind_id',  'fc_ind_id');

                DB::table('forecast')->where('whs_id', $whsId)
                    ->whereIn('cus_id', $this->customerIds)
                    ->whereIn('fc_ind_id', $fc_ind_ids)
                    ->whereBetween('created_at', [strtotime($dateFrom), strtotime($dateTo)])
                    ->delete();

                $arrInserts = [];
                foreach ($fc_indicators as $fc_indicator) {

                    $fc_ind_code = $fc_indicator['fc_ind_code'];

                    if (!$fc_ind_code) {
                        continue;
                    }

                    $data = [];
                    switch ($fc_ind_code) {
                        case 'CTNR':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw('COUNT(DISTINCT co.recNum) AS fc_ib'),
                                    DB::raw('COUNT(DISTINCT co.recNum) AS fc_ib_act'),
                                    DB::raw('0 AS fc_ob'),
                                    DB::raw('0 AS fc_ob_act'),
                                    DB::raw('COUNT(DISTINCT co.recNum) AS fc_st'),
                                    DB::raw('COUNT(DISTINCT IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), co.recNum, NULL)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id', DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)'))
                                //->groupBy('v.id')
                                ->get();

                            break;
                        case 'PL':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw('COUNT(DISTINCT co.recNum) AS fc_ib'),
                                    DB::raw('COUNT(DISTINCT co.recNum) AS fc_ib_act'),
                                    DB::raw('COUNT(DISTINCT IF(ca.statusID = 12, ca.plate, NULL)) AS fc_ob'),
                                    DB::raw('COUNT(DISTINCT IF(ca.statusID = 12, ca.plate, NULL)) AS fc_ob_act'),
                                    DB::raw('COUNT(DISTINCT IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), ca.plate, NULL)) AS fc_st'),
                                    DB::raw('COUNT(DISTINCT IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), ca.plate, NULL)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id', DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)'))
                                //->groupBy('v.id')
                                ->get();

                            break;

                        case 'CTN':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw('COUNT(DISTINCT ca.`id`) fc_ib'),
                                    DB::raw('COUNT(DISTINCT ca.`id`) AS fc_ib_act'),
                                    DB::raw('SUM(IF(ca.`statusID` = 12, 1, 0)) AS fc_ob'),
                                    DB::raw('SUM(IF(ca.`statusID` = 12, 1, 0)) AS fc_ob_act'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), 1, 0)) AS fc_st'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), 1, 0)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id', DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)'))
                                //->groupBy('v.id')
                                ->get();

                            break;

                        case 'CUB':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw('SUM((b.`length` * b.`weight` * b.`height`)/1728) AS fc_ib'),
                                    DB::raw('SUM((b.`length` * b.`weight` * b.`height`)/1728) AS fc_ib_act'),
                                    DB::raw('SUM(IF(ca.`statusID` = 12, (b.`length` * b.`weight` * b.`height`)/1728, 0)) AS fc_ob'),
                                    DB::raw('SUM(IF(ca.`statusID` = 12, (b.`length` * b.`weight` * b.`height`)/1728, 0)) AS fc_ob_act'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), (b.`length` * b.`weight` * b.`height`)/1728, 0)) AS fc_st'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), (b.`length` * b.`weight` * b.`height`)/1728, 0)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id', DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)'))
                                //->groupBy('v.id')
                                ->get();

                            break;

                        case 'UN':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw('SUM(ca.`uom`) AS fc_ib'),
                                    DB::raw('SUM(ca.`uom`) AS fc_ib_act'),
                                    DB::raw('SUM(IF(ca.`statusID` = 12, ca.`uom`, 0)) AS fc_ob'),
                                    DB::raw('SUM(IF(ca.`statusID` = 12, ca.`uom`, 0)) AS fc_ob_act'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), ca.`uom`, 0)) AS fc_st'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10, 61, 28), ca.`uom`, 0)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id', DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)'))
                                //->groupBy('v.id')
                                ->get();

                            break;

                        case /*'SHP'*/ 'ORD':
                            $data = DB::connection('awms')->table('neworder AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ob.dateCreated) AS fc_month'),
                                    DB::raw('YEAR(ob.dateCreated) AS fc_year'),
                                    DB::raw('0 AS fc_ib'),
                                    DB::raw('0 AS fc_ib_act'),
                                    DB::raw('COUNT(DISTINCT co.ID) AS fc_ob'),
                                    DB::raw('COUNT(DISTINCT co.ID) AS fc_ob_act'),
                                    DB::raw('0 AS fc_st'),
                                    DB::raw('0 AS fc_st_act')
                                ])
                                ->join('order_batches AS ob', 'ob.id', '=', 'co.order_batch')
                                ->join('vendors AS v', 'v.id', '=', 'ob.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("co.statusID", 23)
                                ->whereBetween('co.readyshipdate', [$dateFrom, $dateTo])
                                ->groupBy('v.id', DB::raw('YEAR(co.readyshipdate)'), DB::raw('MONTH(co.readyshipdate)'))
                                //->groupBy('v.id')
                                ->get();

                            break;

                        /*case 'ORD':
                            $data = DB::connection('awms')->table('neworder AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ob.dateCreated) AS fc_month'),
                                    DB::raw('YEAR(ob.dateCreated) AS fc_year'),
                                    DB::raw('0 AS fc_ib'),
                                    DB::raw('0 AS fc_ib_act'),
                                    DB::raw('COUNT(DISTINCT IF(co.statusID = 23, co.ID, NULL)) AS fc_ob'),
                                    DB::raw('COUNT(DISTINCT IF(co.statusID = 23, co.ID, NULL)) AS fc_ob_act'),
                                    DB::raw('COUNT(DISTINCT IF(co.statusID <> 23, co.ID, NULL)) AS fc_st'),
                                    DB::raw('COUNT(DISTINCT IF(co.statusID <> 23, co.ID, NULL)) AS fc_st_act')
                                ])
                                ->join('order_batches AS ob', 'ob.id', '=', 'co.order_batch')
                                ->join('vendors AS v', 'v.id', '=', 'ob.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->whereBetween('ob.dateCreated', [$dateFrom, $dateTo])
                                ->groupBy('v.id', DB::raw('YEAR(ob.dateCreated)'), DB::raw('MONTH(ob.dateCreated)'))
                                //->groupBy('v.id')
                                ->get();

                            break;*/

                        case 'LOC':
                            $data = [];/*DB::connection('awms')->table('locations AS l')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw('COUNT(l.id) AS fc_ib'),
                                    DB::raw('COUNT(l.id) AS fc_ib_act'),
                                    DB::raw('0 AS fc_ob'),
                                    DB::raw('0 AS fc_ob_act'),
                                    DB::raw('COUNT(l.id) AS fc_st'),
                                    DB::raw('COUNT(l.id) AS fc_st_act')
                                ])
                                ->join('inventory_cartons AS ca', 'ca.locID', '=', 'l.id')
                                ->join('inventory_batches AS b', 'b.id', '=', 'ca.batchID')
                                ->join('inventory_containers AS co', 'co.recNum', '=', 'b.recNum')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id', DB::raw('YEAR(ca.created_at)'), DB::raw('MONTH(ca.created_at)'))
                                //->groupBy('v.id')
                                ->get();*/

                            break;
                    }

                    $this->proccess($fc_indicator['fc_ind_id'], $data, $whsId, $arrInserts);
                }

                if (!empty($arrInserts)) {
                    foreach (array_chunk($arrInserts, 300) as $data) {
                        DB::table('forecast')->insert($data);
                    }
                }
            }

            DB::commit();

            $this->logSuccess("Forecasts First: successfully, from $dateFrom to $dateTo" );

        } catch (\PDOException $e) {

            DB::rollback();
            $this->logPdo($e->getMessage());

        } catch (\Exception $e) {

            DB::rollback();
            $this->logPdo($e->getMessage());

        }

    }

    public function proccess($fc_ind_id, $data, $whsId, &$arrInserts)
    {
        $cusIds = $this->customerIds;
        $monthNum = count($this->months);
        $ttlStActs = [];
        foreach ($this->months as $yMonth) {
            list($year, $month) = explode('-', $yMonth);

            foreach ($this->customerIds as $awmsId => $cusId) {

                $ttlIb = 0;
                $ttlOb = 0;
                $ttlSt = 0;

                $key = $whsId . $cusId . $fc_ind_id . $year . $month;
                $arrInserts[$key] = [
                    'whs_id' => $whsId,
                    'cus_id' => $cusId,
                    'fc_ind_id' => $fc_ind_id,
                    'fc_ib' => 0,
                    'fc_ib_act' => 0,
                    'fc_ob' => 0,
                    'fc_ob_act' => 0,
                    'fc_st' => 0,
                    'fc_st_act' => 0,
                    'fc_month' => $month,
                    'fc_year' => $year,
                    'created_at' => time(),
                    'created_by' => 1,
                    'updated_at' => time(),
                    'updated_by' => 1,
                    'deleted_at' => 915148800,
                    'deleted' => 0,
                ];
                foreach ($data as $item) {
                    if ($item['awms_cus_id'] == $awmsId) {
                        if ($item['fc_month'] == $month && $item['fc_year'] == $year) {
                            $arrInserts[$key]['fc_ib_act'] = $item['fc_ib_act'];
                            $arrInserts[$key]['fc_ob_act'] = $item['fc_ob_act'];
                        }
                        if (empty($ttlStActs[$item['awms_cus_id']])) {
                            $ttlStActs[$item['awms_cus_id']] = 0;
                        }
                        $ttlStActs[$item['awms_cus_id']] += $item['fc_st_act'];
                        $ttlIb += $item['fc_ib'];
                        $ttlOb += $item['fc_ob'];
                        $ttlSt += $item['fc_st'];

                        if ($month == date('m') && $year == date('Y')) {
                            $arrInserts[$key]['fc_st_act'] = !empty($ttlStActs[$item['awms_cus_id']])
                                ? (int)$ttlStActs[$item['awms_cus_id']] : 0;
                        }
                    }
                }
                $arrInserts[$key]['fc_ib'] = ceil($ttlIb/$monthNum);
                $arrInserts[$key]['fc_ob'] = ceil($ttlOb/$monthNum);
                $arrInserts[$key]['fc_st'] = ceil($ttlSt/$monthNum);
            }
        }
    }
}
