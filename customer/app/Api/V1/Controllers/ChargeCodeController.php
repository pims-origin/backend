<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ChargeCodeModel;
use App\Api\V1\Transformers\ChargeCodeTransformer;
use App\Api\V1\Validators\ChargeCodeValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\V1\Validators\ChargeCodeDeleteMassValidator;
use Seldat\Wms2\Utils\Message;

class ChargeCodeController extends AbstractController
{
    /**
     * @var $chargeCodeModel
     */
    protected $chargeCodeModel;

    /**
     * @var $chargeCodeTransformer
     */
    protected $chargeCodeTransformer;

    /**
     * @var $chargeCodeValidator
     */
    protected $chargeCodeValidator;

    /**
     * @var $chargeCodeDeleteMassValidator
     */
    protected $chargeCodeDeleteMassValidator;

    /**
     * ChargeCodeController constructor.
     *
     */
    public function __construct()
    {
        $this->chargeCodeModel = new ChargeCodeModel();
        $this->chargeCodeTransformer = new ChargeCodeTransformer();
        $this->chargeCodeValidator = new ChargeCodeValidator();
        $this->chargeCodeDeleteMassValidator = new ChargeCodeDeleteMassValidator();
    }

    /**
     * @SWG\Get(
     *     path="/v1/chargeCodes/{chargeCodeId}",
     *     summary="Read ChargeCode",
     *     description="View Detail ChargeCode",
     *     operationId="showChargeCode",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="chargeCodeId",
     *         description="ChargeCode Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return detail of ChargeCode",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\ChargeCodes(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "chargeCode 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * ChargeCode Detail
     *
     * @param int $chargeCodeId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function show($chargeCodeId)
    {
        try {
            $chargeCode = $this->chargeCodeModel->getFirstBy('chg_code_id', $chargeCodeId, ['systemUom', 'chargeType']);

            return $this->response->item($chargeCode, $this->chargeCodeTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/v1/chargeCodes/search",
     *     summary="Search ChargeCode",
     *     description="Search ChargeCode",
     *     operationId="searchChargeCode",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Return all found result of ChargeCode",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\ChargeCodes(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "chargeCode 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     * )
     *
     * ChargeCode Search
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $chargeCode = $this->chargeCodeModel->search($input, ['systemUom', 'chargeType'],
                array_get($input, 'limit', 20));

            return $this->response->paginator($chargeCode, $this->chargeCodeTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/chargeCodes/{chargeCodeId}",
     *     summary="Create ChargeCode",
     *     description="Create ChargeCode",
     *     operationId="createChargeCode",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="chargeCodeId",
     *         description="ChargeCode Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_name",
     *         description="ChargeCode's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_status",
     *         description="ChargeCode's State Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_short_name",
     *         description="ChargeCode's Short Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of ChargeCode",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\ChargeCodes(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "ChargeCode 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * ChargeCode Create
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->chargeCodeValidator->validate($input);

        $params = [
            'chg_code'     => $input['chg_code'],
            'chg_code_des' => array_get($input, 'chg_code_des', ''),
            'chg_uom_id'   => $input['chg_uom_id'],
            'chg_type_id'  => $input['chg_type_id'],
        ];

        try {
            // check duplicate chargeCode
            if ($this->chargeCodeModel->loadBy(['chg_code' => $params['chg_code']])->first()) {
                return $this->response->errorBadRequest(Message::get("BM006", "Charge Code"));
            }

            if ($chargeCode = $this->chargeCodeModel->create($params)) {
                return $this->response->item($chargeCode,
                    $this->chargeCodeTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Put(
     *     path="/v1/chargeCodes/{chargeCodeId}",
     *     summary="Update ChargeCode",
     *     description="Update ChargeCode",
     *     operationId="updateChargeCode",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="chargeCodeId",
     *         description="ChargeCode Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_name",
     *         description="ChargeCode's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_status",
     *         description="ChargeCode's State Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_short_name",
     *         description="ChargeCode's Short Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of ChargeCode",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\ChargeCodes(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "chargeCode 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * @param int $chargeCodeId
     * @param Request $request
     *
     * @return mixed
     * @internal param int $whs_con_id
     */
    public function update(
        $chargeCodeId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->chargeCodeValidator->validate($input);

        $params = [
            'chg_code_id'  => $chargeCodeId,
            'chg_code'     => $input['chg_code'],
            'chg_code_des' => array_get($input, 'chg_code_des', ''),
            'chg_uom_id'   => $input['chg_uom_id'],
            'chg_type_id'  => $input['chg_type_id'],
        ];

        try {

            // Check not exist
            if (!$this->chargeCodeModel->loadBy(['chg_code_id' => $chargeCodeId])->first()) {
                return $this->response->errorBadRequest(Message::get("BM017", "Charge Code"));
            }

            // check duplicate chargeCode
            $chargeCode = $this->chargeCodeModel->loadBy(['chg_code' => $params['chg_code']])->first();
            if (!empty($chargeCode) && $chargeCode->chg_code_id != $chargeCodeId) {
                return $this->response->errorBadRequest(Message::get("BM006", "Charge Code"));
            }

            if ($chargeCode = $this->chargeCodeModel->update($params)) {
                return $this->response->item($chargeCode, $this->chargeCodeTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Delete(
     *     path="/v1/chargeCode/{chargeCodeId}",
     *     summary="Delete ChargeCode",
     *     description="Delete a ChargeCode",
     *     operationId="updateContact",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="chargeCodeId",
     *         description="ChargeCode Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="ChargeCode was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param int $chargeCodeId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     * @internal param MenuModel $menuModel
     *
     */
    public function destroy($chargeCodeId)
    {
        try {
            if ($this->chargeCodeModel->deleteChargeCode($chargeCodeId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['chg_code_id'] = is_array($input['chg_code_id']) ? $input['chg_code_id'] : [$input['chg_code_id']];

        // validation
        $this->chargeCodeDeleteMassValidator->validate($input);

        try {
            $this->chargeCodeModel->deleteMassChargeCode($input['chg_code_id']);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }
}
