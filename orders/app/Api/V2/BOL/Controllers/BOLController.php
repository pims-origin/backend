<?php

namespace App\Api\V2\BOL\Controllers;

use App\Api\V2\BOL\Models\BOLCarrierDetailModel;
use App\Api\V2\BOL\Models\BOLOrderDetailModel;
use App\Api\V2\BOL\Models\CommodityModel;
use App\Api\V2\BOL\Models\OrderCartonModel;
use App\Api\V2\BOL\Models\OrderDtlModel;
use App\Api\V2\BOL\Models\OrderHdrMetaModel;
use App\Api\V2\BOL\Models\OrderHdrModel;
use App\Api\V2\BOL\Models\ShipmentModel;
use App\Api\V2\BOL\Models\WarehouseModel;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Dingo\Api\Http\Response;

/**
 * Class BOLController
 *
 * @package App\Api\V1\Controllers
 */
class BOLController extends AbstractController
{

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var ShipmentModel
     */
    protected $shipmentModel;

    /**
     * @var BOLCarrierDetailModel
     */
    protected $bolCarrierDtlModel;

    /**
     * @var BOLOrderDetailModel
     */
    protected $bolOdrDtlModel;

    /**
     * @var CommodityModel
     */
    protected $commodityModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    /**
     * @var OrderHdrMetaModel
     */
    protected $orderHdrMetaModel;

    /**
     * BOLController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param ShipmentModel $shipmentModel
     * @param BOLCarrierDetailModel $bolCarrierDetailModel
     * @param BOLOrderDetailModel $bolOrderDetailModel
     * @param CommodityModel $commodityModel
     * @param OrderDtlModel $orderDtlModel
     * @param WarehouseModel $warehouseModel
     * @param OrderCartonModel $orderCartonModel
     * @param OrderHdrMetaModel $orderHdrMetaModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        ShipmentModel $shipmentModel,
        BOLCarrierDetailModel $bolCarrierDetailModel,
        BOLOrderDetailModel $bolOrderDetailModel,
        CommodityModel $commodityModel,
        OrderDtlModel $orderDtlModel,
        WarehouseModel $warehouseModel,
        OrderCartonModel $orderCartonModel,
        OrderHdrMetaModel $orderHdrMetaModel
    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->shipmentModel = $shipmentModel;
        $this->bolCarrierDtlModel = $bolCarrierDetailModel;
        $this->bolOdrDtlModel = $bolOrderDetailModel;
        $this->commodityModel = $commodityModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->warehouseModel = $warehouseModel;
        $this->orderCartonModel = $orderCartonModel;
        $this->orderHdrMetaModel = $orderHdrMetaModel;
    }

    const ORDER_STATUS_CREATE_BOL_ARRAY = ['ST', 'RS', 'SS'];

    /**
     * @param $whsId
     * @param $orderId
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store($whsId, $orderId)
    {

        //  order flow : auto BOL
        //  check order status: ST || PA and skip assign pallet

        $orderFlow = $this->orderHdrMetaModel->getOrderFlow($orderId);
        $isAutoBOL = $this->orderHdrMetaModel->getFlow($orderFlow, 'BOL');
        $isAutoPAM = $this->orderHdrMetaModel->getFlow($orderFlow, 'PAM');
        $isAutoARS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ARS');

        $order = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderId]);
        $isCreatedBol = $this->bolOdrDtlModel->getFirstWhere(['odr_id' => $orderId]);

//        if(array_get($isAutoPAM, 'usage', -1) == 1 &&  object_get($order, 'odr_sts') == 'PA') {
//            $this->orderHdrModel->updateWhere(
//                ['odr_sts' => 'ST'], [
//                'odr_id'
//                => $orderId
//            ]);
//            $order->odr_sts='ST';
//        }

        if (array_get($isAutoBOL, 'usage', -1) == 1 && !$isCreatedBol) {
            $odr_sts = object_get($order, 'odr_sts');

            // if ($odr_sts == 'RS' || ($odr_sts == 'PA'
            //         && array_get($isAutoPAM, 'usage', -1) == 1
            //         && array_get($isAutoARS, 'usage', -1) == 1
            //     ) || ($odr_sts == 'PTD'
            //         && array_get($isAutoARS, 'usage', -1) == 1
            //     )) {
            if ($odr_sts == 'RS' || $odr_sts == 'PA' || $odr_sts == 'PTD') {
                $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId)->toArray();
                $volumes = DB::table('odr_hdr')
                    ->select(['p.odr_hdr_id', DB::raw('sum(p.length * p.width * p.height) as vol_ttl')])
                    ->leftJoin("pack_hdr as p", "p.odr_hdr_id", "=", 'odr_hdr.odr_id')
                    ->where('odr_id', $orderId)
                    ->where([
                        'odr_hdr.deleted'    => 0,
                        'odr_hdr.deleted_at' => 915148800
                    ])->groupBy('p.odr_hdr_id')->get();

                $volumes = array_pluck($volumes, "vol_ttl", "odr_hdr_id");

                $warehouse = $this->warehouseModel->getFirstBy('whs_id', $whsId, [
                    'warehouseAddress',
                    'systemCountry',
                    'systemState'
                ])->toArray();

                $boNum = $this->shipmentModel->getBOLNum();
                $items = $this->collectDataForDetailBOL($orderId);

                $shipmentParam = [
                    'whs_id'   => $whsId,
                    'cus_id'   => object_get($order, 'cus_id'),
                    'bo_num'   => $boNum,
                    'bo_label' => 'NA',
                    'ship_dt'  => $orderHdr['ship_by_dt'],
                    'ship_sts' => Status::getByValue("Final", "Ship-Status"),

                    'ship_from_name'    => $warehouse['whs_name'],
                    'ship_from_addr_1'  => array_get($warehouse, 'warehouse_address.whs_add_line_1', null),
                    'ship_from_addr_2'  => array_get($warehouse, 'warehouse_address.whs_add_line_2', null),
                    'ship_from_city'    => $warehouse['whs_city_name'],
                    'ship_from_state'   => array_get($warehouse, 'system_state.sys_state_code', null),
                    'ship_from_zip'     => array_get($warehouse, 'warehouse_address.whs_add_postal_code', null),
                    'ship_from_country' => array_get($warehouse, 'system_country.sys_country_code', 'US'),

                    'ship_to_name'    => $orderHdr['ship_to_name'],
                    'ship_to_addr_1'  => $orderHdr['ship_to_add_1'],
                    'ship_to_addr_2'  => $orderHdr['ship_to_add_2'],
                    'ship_to_city'    => $orderHdr['ship_to_city'],
                    'ship_to_state'   => $orderHdr['ship_to_state'],
                    'ship_to_zip'     => $orderHdr['ship_to_zip'],
                    'ship_to_country' => $orderHdr['ship_to_country'],

                    'bill_to_name'    => '',
                    'bill_to_addr_1'  => '',
                    'bill_to_city'    => '',
                    'bill_to_state'   => '',
                    'bill_to_zip'     => '',
                    'bill_to_country' => '',

                    'special_inst'         => 'NA',
                    'carrier'              => 'NA',
                    'trailer_num'          => '',
                    'seal_num'             => null,
                    'scac'                 => null,
                    'pro_num'              => null,
                    'freight_charge_terms' => 'NA',
                    'freight_charge_cost'  => null,
                    'freight_counted_by'   => 'NA',
                    'po_qty_ttl'           => 1,
                    'weight_ttl'           => (float)array_sum(array_column($items, 'weight')),
                    'ctn_qty_ttl'          => array_sum(array_column($items, 'pkgs')),
                    'piece_qty_ttl'        => array_sum(array_column($items, 'units')),
                    'vol_qty_ttl'          => array_sum($volumes),
                    'cube_qty_ttl'         => array_sum($volumes) / 1728,
                    'plt_qty_ttl'          => array_sum(array_column($items, 'plts')),
                    'fee_terms'            => 'NA',
                    'cus_accept'           => 0,
                    'trailer_loaded_by'    => 'NA',
                    'sts'                  => 'i',
                    'party_acc'            => 'NA',
                    'deli_service'         => 'NA',
                    'is_attach'            => 0,
                    'ship_method'          => 'NA',
                ];

                try {
                    DB::beginTransaction();

                    $shipment = $this->shipmentModel->create($shipmentParam);

                    foreach ($items as $item) {
                        $volume = !empty($volumes[$item['odr_id']]) ? $volumes[$item['odr_id']] : 0;
                        $bolOdrDtlParam = [
                            'odr_id'           => $item['odr_id'],
                            'ship_id'          => $shipment->ship_id,
                            'cus_po'           => $item['cus_po'],
                            'ctn_qty'          => $item['pkgs'], // pkgs
                            'piece_qty'        => $item['units'], // units
                            'weight'           => (float)array_get($item, 'weight', 0),
                            'plt_qty'          => $item['plts'], //plts
                            'cus_dept'         => $item['cus_dept'],
                            'cus_ticket'       => array_get($item, 'cus_ticket', null),
                            'cus_odr'          => $item['cus_odr'],
                            'add_shipper_info' => array_get($item, 'add_shipper_info'),
                            'odr_hdr_num'      => $item['odr_hdr_num'],
                            'vol_qty_ttl'      => $volume,
                            'cube_qty_ttl'     => $volume / 1728,
                        ];

                        $this->bolOdrDtlModel->refreshModel();
                        $odr = $this->bolOdrDtlModel->getFirstBy('odr_id', $item['odr_id']);
                        if (empty($odr->odr_id)) {
                            // Create
                            $this->bolOdrDtlModel->refreshModel();
                            $this->bolOdrDtlModel->create($bolOdrDtlParam);
                        } else {
                            // Update or Fail
                            if (empty($odr->ship_id)) {
                                $this->bolOdrDtlModel->refreshModel();
                                $this->bolOdrDtlModel->updateWhere($bolOdrDtlParam, ['odr_id' => $item['odr_id']]);
                            } else {
                                throw new \Exception("BOL for this order has been created!");
                            }
                        }

                        // Update Order Header ship_id
                        $arrUpdate = [
                            'ship_id' => $shipment->ship_id,
                        ];
                        $orderHdrMetaModel = new OrderHdrMetaModel();
                        $orderFlow = $orderHdrMetaModel->getOrderFlow($item['odr_id']);
                        $skipShippingLaneFlow = $orderHdrMetaModel->getFlow($orderFlow, 'ASS');
                        if (array_get($skipShippingLaneFlow, 'usage', -1) == 1) {
                            $arrUpdate['odr_sts'] = 'SS';
                        }
                        $this->orderHdrModel->refreshModel();
                        $this->orderHdrModel->updateWhere(
                            $arrUpdate,
                            [
                                'odr_id' => $item['odr_id']
                            ]
                        );

                    }

                    $cmdDes = $this->commodityModel->getFirstBy('cmd_des', 'NA');

                    $carrierDtlParam = [
                        'ship_id'  => $shipment->ship_id,
                        'hdl_type' => 'PLTS',// plts
                        'hdl_qty'  => array_sum(array_column($items, 'plts')), // sum(plts)
                        'pkg_type' => 'CTNS',// ctns
                        'pkg_qty'  => array_sum(array_column($items, 'pkgs')),// sum(pkgs)
                        'weight'   => (int)array_sum(array_column($items, 'weight')),
                        'hm'       => 1,
                        'cmd_id'   => $cmdDes->cmd_id,
                        'cmd_des'  => $cmdDes->cmd_name,
                        'nmfc_num' => $cmdDes->commodity_nmfc, // commodity_nmfc
                        'cls_num'  => $cmdDes->cmd_cls, // cmd_cls
                    ];

                    $this->bolCarrierDtlModel->create($carrierDtlParam);

                    DB::commit();

                    return $this->response->noContent()->setContent([
                        'status' => 'OK'
                    ])->setStatusCode(Response::HTTP_CREATED);
                } catch (\PDOException $e) {
                    DB::rollBack();

                    return $this->response->errorBadRequest(
                        SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
                    );
                } catch (\Exception $e) {
                    DB::rollBack();

                    return $this->response->errorBadRequest($e->getMessage());
                }
            }
        }
    }

    public function collectDataForDetailBOL($orderID)
    {

        $rs = DB::table('odr_hdr as oh')
            ->join('pack_hdr as ph', "oh.odr_id", "=", "ph.odr_hdr_id")
            ->join('pack_dtl as pd', "ph.pack_hdr_id", "=", "pd.pack_hdr_id")
            ->select('oh.odr_id', 'oh.odr_num AS odr_hdr_num', 'oh.cus_odr_num AS cus_odr', 'oh.cus_po',
                'oh.cus_pick_num AS cus_ticket', 'oh.cus_dept_ref AS cus_dept',
                DB::raw('COUNT(DISTINCT ph.out_plt_id) as plts'),
                DB::raw('COUNT( DISTINCT pd.pack_hdr_id) as pkgs'), DB::raw("SUM(pd.piece_qty) AS units"),
                DB::raw("(SELECT
                            sum(
                                CASE
                                WHEN c.ctn_pack_size = oc.piece_qty THEN c.weight
                                ELSE c.weight / c.ctn_pack_size * oc.piece_qty
                                END
                            )
                        FROM
                            cartons as c
                        JOIN odr_cartons as oc ON c.ctn_id = oc.ctn_id
                        WHERE
                            oc.odr_hdr_id = oh.odr_id ) AS weight")
            )
            ->where('oh.deleted', 0)
            ->where('ph.deleted', 0)
            ->where('pd.deleted', 0)
            ->where('oh.odr_id', $orderID)
            ->groupBy('oh.odr_id')
            ->get();

        return $rs;

    }
}
