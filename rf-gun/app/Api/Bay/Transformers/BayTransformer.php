<?php


namespace App\Api\Bay\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Bay;
use Seldat\Wms2\Utils\Status;

class BayTransformer extends TransformerAbstract
{
    //return result
    public function transform(Bay $bay)
    {
        return [
            'bay_name'     => object_get($bay, 'bay_name', null),
            'bay_sts'      => object_get($bay, 'bay_sts', null),
            'bay_sts_name' => Status::getByKey('BAY_STATUS',object_get($bay, 'bay_sts')),
            'pos'          => object_get($bay, 'pos', null),
            'line'         => object_get($bay, 'line', null),
        ];
    }

}
