<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\LocationService;
use App\Api\V1\Traits\ZoneLocController;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use function GuzzleHttp\json_decode;


class LocationController extends AbstractController
{
    use ZoneLocController;
    /**
     * @var MenuService
     */
    protected $service;

    /**
     * MenuController constructor.
     *
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        //parent::__construct($request, new AuthenticationService($request));
        $this->service = new LocationService($request);
    }


    /**
     * @param IRequest $request
     * @param $warehouseId
     * @param $locationId
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function update(IRequest $request, $warehouseId, $locationId)
    {
        $input = $request->getParsedBody();
        $result = $this->service->edit($input, $warehouseId, $locationId);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param $warehouseId
     * @param $locationId
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($warehouseId, $locationId)
    {
        $result = $this->service->destroy($warehouseId, $locationId);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function destroyMultiple(IRequest $request, $warehouseId)
    {
        $input = $request->getParsedBody();
        $result = $this->service->destroyMultiple($input, $warehouseId);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     *
     * @return \Illuminate\Http\Response
     */
    public function search(IRequest $request, $warehouseId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->search($queryStr, $warehouseId);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     * @param $locId
     * @return \Illuminate\Http\Response
     */
    public function getPalletInfo(IRequest $request, $warehouseId, $locId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getPalletInfo($queryStr, $warehouseId, $locId);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     *
     * @return \Illuminate\Http\Response
     */
    public function viewWHSearch(IRequest $request, $warehouseId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->search($queryStr, $warehouseId, 'wh-');
        $response = $this->convertResponse($result);

        return $response;
    }
    /**
     * @param IRequest $request
     * @param $zoneId
     *
     * @return \Illuminate\Http\Response
     */
    public function getLocations(IRequest $request, $zoneId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getLocations($zoneId, $queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param IRequest $request
     * @param $zoneId
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function addLocations(IRequest $request, $zoneId)
    {
        $input = $request->getParsedBody();

        $result = $this->service->addlocations($input, $zoneId);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     *
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function validateImportFile(Request $request, $warehouseId)
    {
        set_time_limit(0);
        $input = $request->all();

        $result = $this->service->validateImportFile($input, $warehouseId);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            //if ($response->getStatusCode() == 400) {
            //    $resArr = json_decode($response->getContent(), true)['errors'];
            //
            //    if ($resArr['message'] === "The Location File is Invalid") {
            //        $hashKey = md5($request->header('Authorization'));
            //        $this->service->getErrFile($hashKey . '.xlsx');
            //    }
            //}

            return $response;
        }

        return $result;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getImportError(Request $request)
    {
        $hashKey = md5($request->header('Authorization'));

        $file = storage_path($hashKey . ".xlsx");

        return response()->download($file);
    }

    public function processImport()
    {
        $result = $this->service->processImport();

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    public function getStatusImport()
    {
        $result = $this->service->getStatusImport();

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     * @param $locationId
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function changeStatus(IRequest $request, $warehouseId, $locationId)
    {
        $input = $request->getParsedBody();

        $result = $this->service->changeStatus($input, $warehouseId, $locationId, $this->userId);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param $warehouseId
     * @param $locationId
     *
     * @return \Illuminate\Http\Response
     */
    public function getChangeStatus($warehouseId, $locationId)
    {
        $result = $this->service->getChangeStatus($warehouseId, $locationId);
        $response = $this->convertResponse($result);

        return $response;
    }
}
