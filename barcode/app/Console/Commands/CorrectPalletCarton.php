<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CorrectPalletCarton extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correct:pallet-carton {whsId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'correct pallet carton ';

    /**
     * Create a new command instance.
     *
     * @param DripEmailer $drip
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $whsId = $this->argument('whsId');
            $cartons = DB::table('cartons')
                ->where('whs_id', $whsId)
                ->whereIn('ctn_sts', ['AC', 'RG', 'LK'])
                ->whereNotNull('loc_code')
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('pallet')
                        ->whereRaw('pallet.cus_id = cartons.cus_id 
                                    AND pallet.whs_id = cartons.whs_id 
                                    AND pallet.loc_code = cartons.loc_code');
                })
                ->selectRaw('whs_id, cus_id, loc_code, loc_id')
                ->groupBy('whs_id', 'cus_id', 'loc_code')
                ->get();

            $newPallets = [];
            foreach ($cartons as $carton) {
                $palletNum = $this->generatePltNum();
                $newPallets[] = [
                    'plt_num' => $palletNum,
                    'loc_id' => $carton['loc_id'],
                    'loc_code' => $carton['loc_code'],
                    'loc_name' => $carton['loc_code'],
                    'whs_id' => $carton['whs_id'],
                    'cus_id' => $carton['cus_id'],
                    'plt_sts' => 'AC',
                    'created_at' => time(),
                    'created_by' => 369,
                    'updated_at' => time(),
                    'updated_by' => 369,
                    'deleted' => 0,
                    'deleted_at' => 915148800,
                    'ctn_ttl' => 0
                ];
            }

            foreach (array_chunk($newPallets, 200) as $dataIds) {
                DB::table('pallet')->insert($dataIds);
            }

        } catch (\PDOException $e) {
            dd($e->getMessage());
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function generatePltNum($prefix = "V") {
        $currentYearMonth = date('ym');
        $pallet = DB::table('pallet')->where('plt_num', 'LIKE', '%-' . $currentYearMonth . '-%')
            ->orderBy('plt_id', 'DESC')
            ->first();

        if (!$pallet){
            return $prefix . '-' . $currentYearMonth . '-' . '000001';
        }

        $aNum = explode("-", $pallet['plt_num']);
        $aTemp = array_keys($aNum);
        $end = end($aTemp);
        $index = (int)$aNum[$end];
        $aNum[0] = $prefix;
        $aNum[$end] = str_pad(++$index, 6, "0", STR_PAD_LEFT);
        return implode("-", $aNum);
    }
}
