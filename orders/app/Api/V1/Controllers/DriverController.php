<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 1/8/2019
 * Time: 9:20 AM
 */

namespace App\Api\V1\Controllers;


use Psr\Http\Message\ServerRequestInterface as Request;

use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Models\SysBug;
use Dingo\Api\Http\Response;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Transformers\OrderHdrMetaTransformer;

class DriverController extends AbstractController
{

    protected $ordrHdrMetaModel;

    public function __construct(
        OrderHdrMetaModel $orderHdrMetaModel
    )
    {
        $this->ordrHdrMetaModel = $orderHdrMetaModel;
    }

    public function store(
        Request $request,
        OrderHdrMetaTransformer $orderHdrMetaTransformer
    ) {
        try {
            $input = $request->getParsedBody();

            $this->validateDriverOrders($input);

            $qualifier = 'DL0';
            $value = json_encode(['driver_id' => $input['driver_id']]);
            $time = time();

            foreach($input['odr_ids'] as $odr_id){
                $params[] = [
                    'odr_id'    => $odr_id,
                    "qualifier" => $qualifier,
                    "value"     => $value,
                    "created_at"=> $time,
                    "updated_at"=> $time,
                ];
            }
            $result = $this->ordrHdrMetaModel->insert($params);
            
            $res['data'] = [
                'message' => "Assign orders to driver successful!"
            ];

            return response()->json($res);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest("The orders has been assigned to this driver before.");
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function validateDriverOrders($attribute) {
        if(empty($attribute['driver_id'])) {
            return $this->response->errorBadRequest("This field driver_id is required!");
        }
        if(empty($attribute['odr_ids'])) {
            return $this->response->errorBadRequest("This field odr_ids is required!");
        }
    }
}