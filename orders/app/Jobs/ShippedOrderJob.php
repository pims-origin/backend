<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;

class ShippedOrderJob extends IMSJob
{
    protected $orderIds;

    public function __construct($orderIds)
    {
        $this->orderIds = $orderIds;
    }

    public function handle()
    {
        $endpoint = env('IMS_API_ORDER');

        // Check IMS Configuration
        if (empty($endpoint)) {
            $this->writeLogment('IMS Shipping Order', 'IMS Shipped Order Endpoint is not define!');

            return false;
        }

        $orders = DB::table('odr_hdr')
            ->select(
                'odr_hdr.odr_id',
                'odr_hdr.odr_num',
                'odr_hdr.shipped_dt'
            )
            ->whereIn('odr_id', $this->orderIds)
            ->get();

        try {
            $token = $this->login();

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-type: application/json"
            ];

            foreach ($orders as $order) {
                $packs = DB::table('pack_hdr')
                    ->where('odr_hdr_id', $order['odr_id'])
                    ->get();

                if(!empty($packs)) {
                    $trackingNum = collect($packs)->pluck('tracking_number');
                    $trackingNum = array_unique($trackingNum->toArray());
                }

                $orderNum = array_get($order, 'odr_num', null);
                $shipDate = array_get($order, 'shipped_dt', null);

                $data = [
                    'ship_date' => empty($shipDate) ? null : date('Y-m-d', $shipDate),
                    'tracking' => implode($trackingNum, ",") ?? null
                ];

                $endpoint = $endpoint . $orderNum;

                $result = $this->call($endpoint, 'POST', json_encode($data), $header);

                $this->writeLogment('IMS Shipping Order ' . $orderNum, 'URL: '. $endpoint .' --- Request: ' . json_encode($data) . ' --- Response: ' . $result);
            }

        } catch (\Exception $e) {
            $this->writeLogment('IMS Shipping Order', $e->getMessage());
        }
    }
}