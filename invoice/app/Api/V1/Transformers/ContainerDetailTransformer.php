<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class ContainerDetailTransformer extends TransformerAbstract
{
    public function transform($detail)
    {
        return [
            'ctnr_id' => $detail->ctnr_id,
            'cus_name' => $detail->cus_name,
            'ctnr_num' => $detail->ctnr_num,
            'sys_mea_code' => $detail->sys_mea_code,
            'created' => $detail->created
        ];
    }

    /*
    ****************************************************************************
    */

}

