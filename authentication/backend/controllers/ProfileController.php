<?php

namespace backend\controllers;

use backend\forms\EditProfileForm;
use backend\models\ProfileSearch;
use Yii;

use yii\web\BadRequestHttpException;

class ProfileController extends ApiController
{
    public $modelClass = 'common\models\User';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['update'], $actions['view']);

        return $actions;
    }

    public function actionView($id)
    {
        $result = ProfileSearch::getProfile($id);
        if (false === $result) {
            throw new BadRequestHttpException('Profile not found');
        }

        return [
            'data' => $result
        ];
    }

    public function actionUpdate($id)
    {
        $user = User::findOne(['user_id' => $id]);

        if (!$user instanceof User) {
            Yii::$app->response->statusCode = 404;

            return [
                'error' => [
                    'id'     => $id,
                    'status' => 404,
                    'code'   => 'profile.edit.not_found',
                    'title'  => 'Profile not found',
                ]
            ];
        }
        $model = new  EditProfileForm();
        $model->load(Yii::$app->request->post());
        if ($model->editProfile($user)) {
            return ['data' => ['success' => true]];
        } else {
            Yii::$app->response->statusCode = 400;

            return ['error' => $model->errors];
        }
    }
}
