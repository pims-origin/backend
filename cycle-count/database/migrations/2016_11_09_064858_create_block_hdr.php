<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockHdr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('block_hdr')) {
            Schema::create('block_hdr', function (Blueprint $table) {
                $table->increments('block_hdr_id');
                $table->string('block_num', 50);
                $table->integer('block_rsn_id');
                $table->integer('whs_id');
                $table->char('block_type', 3);
                $table->text('block_detail')->nullable();
                $table->char('block_sts', 2);
                $table->integer('created_by');
                $table->integer('updated_by');
                $table->integer('created_at');
                $table->integer('updated_at');
                $table->char('sts', 1);
                $table->tinyInteger('deleted');
                $table->integer('deleted_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('block_hdr')) {
            Schema::drop('block_hdr');
        }
    }
}
