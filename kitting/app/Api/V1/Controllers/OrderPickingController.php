<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\WavePickDtlLocModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Transformers\WavePickHdrTransformer;
use App\Api\V1\Transformers\WavePickInfoTransformer;
use App\EventTracking;
use App\Jobs\WavePickJob;
use App\Wms2\UserInfo\Data;
use Exception;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use mPDF;

class OrderPickingController extends AbstractController
{
    protected $wvHdrModel;
    protected $wvDtlModel;
    protected $wvDtlLocModel;
    protected $odrHdrModel;
    protected $odrDtlModel;
    protected $cartonModel;
    protected $locationModel;
    protected $customerConfigModel;
    protected $inventorySummaryModel;
    protected $palletModel;
    /**
     * OrderPickingController constructor.
     */
    public function __construct()
    {
        $this->wvHdrModel = new WavePickHdrModel();
        $this->wvDtlModel = new WavePickDtlModel();
        $this->wvDtlLocModel = new WavePickDtlLocModel();
        $this->odrHdrModel = new OrderHdrModel();
        $this->odrDtlModel = new OrderDtlModel();
        $this->cartonModel = new CartonModel();
        $this->locationModel = new LocationModel();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
        $this->palletModel = new PalletModel();
    }

    /**
     * List order picking
     *
     * @param Request $request
     * @param WavePickHdrTransformer $wvHdrTransformer
     *
     * @return \Dingo\Api\Http\Response
     */
    public function odrPickingList(Request $request, WavePickHdrTransformer $wvHdrTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $wvHdrLs = $this->wvHdrModel->search($input, ['details', 'pickerUser'], array_get($input, 'limit', 20));

            foreach ($wvHdrLs as $wvHdr) {
                $arrCus = [];
                foreach ($wvHdr->details as $details) {
                    $arrCus[] = $details->cus_id;
                }
                $wvHdr->cus_id = $arrCus;
            }

            return $this->response->paginator($wvHdrLs, $wvHdrTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Detail wave pick
     *
     * @param $wvHdrId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function detailWavePick($wvHdrId, Request $request)
    {
        $whsId = Data::getCurrentWhsId();
        try {
            //Load wave pick header
            $dataWvDtl = $this->wvHdrModel->getWvHdrInfo($wvHdrId);
            // Load order_num
            $odrHdrModels = $this->odrHdrModel->getOrderHdrNumsByWvId($wvHdrId, $whsId)->toArray();

            if ($dataWvDtl) {
                $dataWvDtl->odr_num = implode(", ", array_unique(array_column($odrHdrModels, 'odr_num')));

                $wvDtlObjs = $this->wvDtlModel->loadWvDetail($wvHdrId);
                foreach ($wvDtlObjs as $wvDtlObj) {
                    $sqlSum = "sum(piece_qty) as picked_qty";
                    if ($wvDtlObj['wv_dtl_sts'] == 'NW') {
                        $actLocs = json_decode(object_get($this->wvDtlLocModel->getFirstBy('wv_dtl_id',$wvDtlObj['wv_dtl_id']), 'act_loc_ids'));
                    } else {
                        $actLocs = DB::table('odr_cartons')
                            ->select([
                                'loc_id as act_loc_id',
                                'loc_code as act_loc',
                                DB::raw($sqlSum),
                            ])
                            ->where('deleted', 0)
                            ->where('wv_dtl_id', $wvDtlObj['wv_dtl_id'])
                            ->groupBy('loc_id')
                            ->get()
                            ->toArray();
                    }
                    $wvDtlObj['act_loc_ids'] = !empty($actLocs) ? array_values($actLocs) : null;
                }
                $dataWvDtl->wvDetail = $wvDtlObjs;
            }

            return $this->response->item($dataWvDtl, new WavePickInfoTransformer());
        } catch (Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $wvDtlId
     *
     * @return mixed
     */
    public function suggestLocation($wvDtlId)
    {
        try {
            $wv_dtl = $this->wvDtlModel->getFirstWhere(['wv_dtl_id' => $wvDtlId]);

            $item_id = object_get($wv_dtl, 'item_id', 0);


            $wvDtlLoc = $this->wvDtlLocModel->getSugLocIdByWvDtlId($wvDtlId);
            if (empty($wvDtlLoc)) {
                return $this->response->errorBadRequest("The Wave Detail Location is not existed!");
            }
            if (empty($wvDtlLoc->sug_loc_ids)) {
                return $this->response->errorBadRequest("Wave Detail Location do not have Suggest Location Id!");
            }

            //For view wv_dtl when complete wv
            $wvDtlActLocs = $wvDtlLoc->act_loc_ids;
            if (!empty($wvDtlActLocs)) {
                $wvDtlActLocs = \GuzzleHttp\json_decode($wvDtlActLocs, true);

                array_walk($wvDtlActLocs, function (&$v) {
                    if (isset($v['cartons'])) {
                        unset($v['cartons']);
                    }
                });

                return ['data' => $wvDtlActLocs];
            }


            $locIds = explode(',', $wvDtlLoc->sug_loc_ids);

            $suggestLocation = $this->cartonModel->getSugLocByLocIds($locIds, $item_id, $wv_dtl->lot);

            if (!empty($suggestLocation)) {
                $temp = 0;
                $data = [];
                foreach ($suggestLocation as $loc) {
                    $data[] = $loc;
                    $temp += $loc['avail_qty'];
                    if ($temp >= $wv_dtl->piece_qty) {
                        return ['data' => $data];
                    }
                }
            }

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function checkActualLocations(Request $request)
    {
        $input = $request->getParsedBody();

        if (!isset($input['lists']) || empty($input['lists'])) {
            throw new \Exception(Message::getOnLang("Lists loc_id is required!"));
        }
        if (!isset($input['item_id']) || empty($input['item_id'])) {
            throw new \Exception(Message::getOnLang("The loc_ids or item_id input is required!"));
        }
        if (!isset($input['lot']) || empty($input['lot'])) {
            throw new \Exception(Message::getOnLang("The lot input is required!"));
        }
        if (!isset($input['is_ecom'])) {
            throw new \Exception(Message::getOnLang("The is_ecom input is required!"));
        }

        $lists = array_pluck($input['lists'], 'picked_qty', 'loc_id');

        $data = $this->locationModel->checkActualLocation($input['item_id'], $input['lot'], $input['is_ecom'], $lists);

        foreach ($data as $key => $rw) {
            $data[$key]['error'] = '';
            if ($rw['avail_qty'] === null) {
                $data[$key]['error'] = Message::getOnLang('Location is unavailable');
                continue;
            }

            if ($rw['avail_qty'] < $lists[$rw['loc_id']]) {
                $data[$key]['error'] = Message::getOnLang('picked qty > available qty');
                continue;
            }

            if ($rw['loc_sts_code'] != 'AC') {
                $data[$key]['error'] = Message::getOnLang('Status of location not active');
                continue;
            }
        }

        return $data;
    }

    public function updateWavePickNoEcom($wv_id, Request $request)
    {
        // get data from HTTP
        $items = $request->getParsedBody();

        $orders = $this->odrHdrModel->getOrderHdrByWavePikcIds($wv_id);

        if (!empty($orders)) {
            $cancelOdrSts = Status::getByValue("Canceled", "ORDER-STATUS");
            $count = 0;
            foreach ($orders->toArray() as $order) {
                if ($order['odr_sts'] === $cancelOdrSts) {
                    $count++;
                }
            }
            if (count($orders->toArray()) === $count) {
                throw new Exception(Message::get('BM123', 'Canceled order', 'order picking'));
            }
        }

        // Load wvhdr info
        $wvHdrInfo = $this->wvHdrModel->getWvHdrInfo($wv_id);

        // Check complete wv_hdr
        $this->checkCompleteWvHdr($wvHdrInfo);

        try {
            DB::beginTransaction();

            $ttlActLocIds = [];
            $odrDtlList = [];
            $packs = [];
            $eventDatas = [];
            $algorithms = [];

            // Update wavepick detail, inventory summary, carton & pallet
            foreach ((array)$items as $item) {
                /**
                 * 1. Get Cartons
                 * 1.1 - get by picking algorithm: FIFO , LIFO, FEFO
                 * - FIFO order by created_at ASC,
                 * - LIFO order by created_at DESC,
                 * - FEFO order by expired_dt ASC,
                 * 2. Update Wave dtl by sum up piece_remain in carton
                 * 2.1 update wwdtl loc
                 * 3. Update Inventory - where item_id, lot, cus_id, whs_id
                 * 4. update cartons
                 * 4. Update order dtl sts. all_qty
                 * 4.1 update order hdr
                 * 5. Insert order carton
                 * 6. Update pallet
                 * 7. update wv_hdr
                 * 8. auto pack
                 */

                $item['item_id'] = $item['itm_id'];
                $actLocs = (array)$item['act_loc'];
                $actLocIds = array_column($actLocs, 'act_loc_id');


                $ttlActLocIds = array_merge($ttlActLocIds, $actLocIds);
                $pickedQTYs = array_column($actLocs, 'picked_qty');
                $pickedTtl = array_sum($pickedQTYs);
                if (!isset($algorithms[$item['cus_id']])) {
                    $algorithms[$item['cus_id']] = $this->customerConfigModel->getPickingAlgorithm($wvHdrInfo->whs_id,
                        $item['cus_id']);;
                }

                if ($item['piece_qty'] < $item['act_piece_qty']) {
                    $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s picked Qty QTY %d is greater than Allocated Qty %d',
                        $item['sku'], $item['size'], $item['color'], $item['lot'], $item['piece_qty'],
                        $item['act_piece_qty']);
                    throw new Exception($msg);
                }

                $algorithm = $algorithms[$item['cus_id']];
                $qty = $this->cartonModel->getAvailableQuantity($item['item_id'], $item['lot'], $actLocIds, $wvHdrInfo->whs_id);


                if ($qty < $pickedTtl) {
                    $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s available QTY %d is less than Picked Qty %d',
                        $item['sku'], $item['size'], $item['color'], $item['lot'], $qty, $pickedTtl);
                    throw new Exception($msg);
                }

                $item['picked_ttl'] = $pickedTtl;
                $cartons = $this->cartonModel->getAllCartonsByWvDtl($item, $algorithm, $actLocIds, $actLocs);

                $cartons = $this->cartonModel->sortCartonsByLoc($cartons);

                // update wave pick detail
                $this->wvDtlModel->updateWaveDtl($item['wv_dtl_id'], $pickedTtl);

                $data = $this->groupCartonsByLot($cartons);

                $odrCartons = $this->cartonModel->updateCartonByLocs($data['cartons']);
                $this->updateInventoryByLot($data['lots'], $item);
                $this->wvDtlLocModel->updateWaveDtlLoc($actLocs, $item['wv_dtl_id']);

                $data = $this->odrDtlModel->updateOdrDtlPickedQty($item, $odrCartons);
                unset($odrCartons); //free memory

                $odrDtlList[] = $data['odrDtls'];
                $eventDatas = array_merge($eventDatas, $data['eventData']);
                $packs[] = $data['packs'];

                $discrepancyTtl = $item['piece_qty'] - $item['act_piece_qty'];
                if ($discrepancyTtl > 0) {
                    $this->inventorySummaryModel->updateInventoryByLotBackOrder($item, $discrepancyTtl);
                }

                // Update order picked
                $this->odrHdrModel->refreshModel();
                $odrSts = 'PK';
                $this->odrHdrModel->updateWhereIn(['odr_sts' => $odrSts, 'sts' => 'u'], $data['odrIds'], 'odr_id');
            }

            $this->palletModel->updatePalletCtnTtl($ttlActLocIds);
            $this->palletModel->updateZeroPallet($ttlActLocIds);

            // ------------- FOR BACK ORDER ---------------------
            $this->wvHdrModel->updateWvPicking($wv_id);

            $this->logEventTracking($eventDatas, $wvHdrInfo);

            DB::commit();
            $this->dispatch(new WavePickJob($wv_id, $request));
            return $this->response
                ->noContent()
                ->setContent("{}")
                ->setStatusCode(200);
        } catch (Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }
    /**
     * @param $wvHdrInfo
     * @throws Exception
     */
    private function checkCompleteWvHdr($wvHdrInfo)
    {
        if ($wvHdrInfo->wv_sts == Status::getByValue("Completed", "WAVEPICK-STATUS")) {
            throw new Exception(Message::get("BM037"));
        }

        if ($wvHdrInfo->wv_sts == Status::getByValue("Canceled", "WAVEPICK-STATUS")) {
            throw new Exception('This wave pick has been cancelled!');
        }

        if ($wvHdrInfo->wv_sts != Status::getByValue("New", "WAVEPICK-STATUS")) {
            throw new Exception('Order Pick can be executed for New Wave Pick only!');
        }
    }

    private function groupCartonsByLot($locs)
    {
        $lots = [];
        $cartons = [];
        foreach ($locs as $loc) {
            foreach ($loc as $carton) {
                if (!isset($lots[$carton['lot']])) {
                    $lots[$carton['lot']] = 0;
                }
                $lots[$carton['lot']] += $carton['picked_qty'];
                $cartons[] = $carton;
            }
        }

        return [
            'cartons' => $cartons,
            'lots'    => $lots
        ];
    }

    private function updateInventoryByLot($lots, $item)
    {
        foreach ($lots as $lot => $pickedQty) {
            $this->inventorySummaryModel->updateInventoryByLot([
                'whs_id'  => $item['whs_id'],
                'item_id' => $item['itm_id'],
                'lot'     => $lot
            ], $pickedQty);
        }
    }

    /**
     * @param $eventDatas
     * @param $wvHdrInfo
     */
    private function logEventTracking($eventDatas, $wvHdrInfo)
    {


        // event tracking - WAVE PICK
        $evt_code = Status::getByKey("EVENT", "WAVE-PICK-COMPLETED");

        $info = sprintf(Status::getByKey("EVENT-INFO", "WAVE-PICK-UPDATE"), $wvHdrInfo->wv_num);

        $eventData = [
            'whs_id'     => $wvHdrInfo->whs_id,
            'cus_id'     => $wvHdrInfo->cus_id,
            'owner'      => $wvHdrInfo->wv_num,
            'evt_code'   => $evt_code,
            'trans_num'  => $wvHdrInfo->wv_num,
            'info'       => $info,
            'created_at' => time(),
            'created_by' => JWTUtil::getPayloadValue('jti') ?: 1

        ];

        array_push($eventDatas, $eventData);
        (new EventTracking())->insert($eventDatas);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function moreSuggestLocation(Request $request)
    {
        $input = $request->getQueryParams();

        try {

            if (!isset($input['loc_ids']) || empty($input['loc_ids'])) {
                $input['loc_ids'] = [];
            }
            if (!isset($input['item_id']) || empty($input['item_id'])) {
                throw new \Exception(Message::getOnLang("The loc_ids or item_id input is required!"));
            }
            if (!isset($input['lot']) || empty($input['lot'])) {
                throw new \Exception(Message::getOnLang("The lot input is required!"));
            }
            if (!isset($input['is_ecom'])) {
                throw new \Exception(Message::getOnLang("The is_ecom input is required!"));
            }

            if (empty($input['loc_ids'])) {
                $input['loc_ids'] = 0;
            }

            $locIds = explode(',', $input['loc_ids']);
            $locIds = array_filter($locIds, 'is_numeric');

            $suggestLocation = $this->cartonModel->getMoreSugLocByWvDtl($locIds, $input);

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function printWavepick(Request $request)
    {
        $input = $request->getQueryParams();
        return $this->processPrintWavepick($input);
    }

    public function processPrintWavepick($input) {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        if (empty($input['wv_id']) && empty($input['odr_id'])) {
            throw new \Exception(Message::getOnLang("The odr_id or wv_id input is required!"));
        }

        if (empty($input['wv_id'])) {
            $odr = $this->odrHdrModel->getFirstBy("odr_id", $input['odr_id']);
            if (empty($odr)) {
                throw new \Exception(Message::getOnLang("The {0} is not existed!", "Order Wavepick"));
            }

            $input['wv_id'] = $odr->wv_id;
        }

        $wave = $this->wvHdrModel->getFirstBy('wv_id', $input['wv_id'], ['pickerUser']);
        if (empty($wave)) {
            throw new \Exception(Message::getOnLang("The {0} is not existed!", "Wavepick"));
        }

        $wvNum = $wave->wv_num;
        $picker_firstName = object_get($wave, 'pickerUser.first_name', '');
        $picker_lastName = object_get($wave, 'pickerUser.last_name', '');
        $picker = $picker_firstName . " " . $picker_lastName;
        //print pdf file
        if (!file_exists(storage_path("WavePick/$whsId"))) {
            mkdir(storage_path("WavePick/$whsId"), 0777, true);
        }

        $this->odrHdrModel->refreshModel();
        $orders = $this->odrHdrModel->allBy('wv_id', $input['wv_id'])->toArray();
        if (empty($orders)) {
            throw new \Exception(Message::getOnLang("The {0} is not existed!", "Wavepick Order"));
        }

        $odrHdrNum = trim(implode(", ", array_pluck($orders, 'odr_num')));

        // Load data for excel
        $dataWvDtl = $this->wvDtlModel->loadData($input['wv_id']);

        // Get Location for excel
        $tempCartons = $this->getLoationsNumberCartons($input['wv_id'], $dataWvDtl);

        $dataWvDtl = $dataWvDtl->toArray();

        // Prepare data to load excel
        $pickQty = $this->prepareDataForExcel($dataWvDtl);
        $temp = [];
        foreach ($pickQty as $qty) {
            $temp[$qty['ITEM_ID'] . "-" .
            $qty['SKU'] . "-" .
            $qty['Color'] . "-" .
            $qty['SIZE'] . "-" .
            $qty['Lot']][$qty['Pack_size']] = $qty;
        }

        //Export to DPF File
        return $this->printDpfData($temp, $wvNum, $odrHdrNum, $tempCartons, $whsId, $picker);
    }

    /**
     * @param $wvId
     * @param $dataWvDtls
     *
     * @return array
     */
    public function getLoationsNumberCartons($wvId, $dataWvDtls)
    {
        if (empty($dataWvDtls)) {
            return [];
        }
        $itemIds = [];
        $itemLots = [];

        foreach ($dataWvDtls as $dataWvDtl) {
            $itemIds[$dataWvDtl['ITEM_ID']] = $dataWvDtl['ITEM_ID'];
            $itemLots[$dataWvDtl['ITEM_ID']] = $dataWvDtl['Lot'];
        }

        // Load data location for excel
        $wvHdrInfo = $this->wvHdrModel->getFirstWhere(['wv_id' => $wvId]);
        $wvSts = object_get($wvHdrInfo, 'wv_sts', '');
        if ($wvSts == 'CO') {
            $SugLocByWvIds = $this->wvDtlLocModel->getSugLocByWvId($wvId);
            $cartons = [];
            foreach ($SugLocByWvIds as $SugLocByWvId) {
                $act_loc_ids = (\GuzzleHttp\json_decode(array_get($SugLocByWvId, 'act_loc_ids', '')));

                if (!empty($act_loc_ids)) {
                    $loc_ids = [];
                    foreach ($act_loc_ids as $act_loc_id) {
                        if (!empty($loc_ids[object_get($act_loc_id, 'loc_id', '')])) {
                            $loc_ids[object_get($act_loc_id, 'loc_id', '')]['picked_qty'] += object_get($act_loc_id,
                                'picked_qty', '');
                        } else {
                            $loc_ids[object_get($act_loc_id, 'loc_id', '')] = [
                                "item_id"     => array_get($SugLocByWvId, 'item_id', ''),
                                "lot"         => array_get($SugLocByWvId, 'lot', 'NA'),
                                "loc_id"      => object_get($act_loc_id, 'loc_id', ''),
                                "loc_code"    => object_get($act_loc_id, 'loc_code', ''),
                                "plt_num"     => object_get($act_loc_id, 'plt_num', ''),//LPN
                                "numCarton"   => "getWvDtlLoc",//CTNS = QTY/pack size
                                "pieceRemain" => object_get($act_loc_id, 'avail_qty', ''),//QTY, pieceRemain
                                "picked_qty"  => object_get($act_loc_id, 'picked_qty', ''),//ActualQTY =>
                                //ActulCTNS = ActualQTY/packsize
                                "act_loc"     => object_get($act_loc_id, 'act_loc', ''),//Actual location
                                "act_loc_id"  => object_get($act_loc_id, 'act_loc_id', ''),

                            ];
                        }
                    }

                    foreach ($loc_ids as $key => $val) {
                        $cartons[] = $val;
                    }
                }
            }
        } else {
            $wvDtlLocs = $this->wvDtlLocModel->getSugLocIdByWvId($wvId)->toArray();

            $locIds = [];
            foreach ($wvDtlLocs as $wvDtlLoc) {
                $ids = array_filter(explode(',', $wvDtlLoc['sug_loc_ids']));
                $locIds = array_merge($locIds, $ids);
            }

            $cartons = $this->cartonModel->getCartonForWavePick($locIds, $itemIds);
        }

        $tempCartons = [];
        if ($cartons) {
            foreach ($cartons as $carton) {
                $tempCartons[$carton['item_id']][$carton['lot']][] = $carton;
            }
        }


        return $tempCartons;
    }

    /**
     * @param $dataWvDtl
     *
     * @return array
     */
    private function prepareDataForExcel($dataWvDtl)
    {
        $pickQty = array_map(function ($e) {
            return [
                'ITEM_ID'     => $e['ITEM_ID'],
                'SKU'         => $e['SKU'],
                'Color'       => $e['Color'],
                'SIZE'        => $e['SIZE'],
                'Lot'         => $e['Lot'],
                'Pack_size'   => $e['Pack_size'],
                'Cartons'     => $e['Cartons'],
                'Picking_QTY' => $e['Picking_QTY'],
                'primary_loc' => $e['primary_loc'],
                'bu_loc_1'    => $e['bu_loc_1'],
                'bu_loc_2'    => $e['bu_loc_2'],
                'bu_loc_3'    => $e['bu_loc_3'],
                'bu_loc_4'    => $e['bu_loc_4'],
                'bu_loc_5'    => $e['bu_loc_5'],
                'cus_id'      => $e['cus_id'],
                'wv_dtl_id'   => $e['wv_dtl_id']
            ];
        }, $dataWvDtl);

        return $pickQty;
    }

    /**
     * @param $temp
     * @param $wvNum
     * @param $odrHdrNum
     * @param $tempCartons
     * @param $whsId
     * @param $picker
     */
    private function printDpfData($temp, $wvNum, $odrHdrNum, $tempCartons, $whsId, $picker)
    {
        //get user Lang
        $userLang = Message::userLang();
        if (!$userLang) {
            return $this->response->errorBadRequest(Message::getOnLang("Please check Redis server!"));
        }
        //----Multi Lang
        $WavePick = Message::getOnLangFE("Wave Pick", "Common");
        $Handler = Message::getOnLangFE("Handler", "Common");
        $Orders = Message::getOnLangFE("Orders", "Common");

        $No = Message::getOnLangFE("No", "Common");
        $ItemID = Message::getOnLangFE("Item ID", "Common");
        $UPC = Message::getOnLangFE("UPC", "Common");
        $SKU = Message::getOnLangFE("SKU", "Common");
        $Size = Message::getOnLangFE("Size", "Common");
        $Color = Message::getOnLangFE("Color", "Common");
        $Lot = Message::getOnLangFE("Lot", "Common");
        $Description = Message::getOnLangFE("Description", "Common");
        $UOM = Message::getOnLangFE("UOM", "Common");
        $PackSize = Message::getOnLangFE("Pack Size", "Common");

        $Cartons = Message::getOnLangFE("Cartons", "Common");
        $PieceQTY = Message::getOnLangFE("Piece QTY", "Common");
        $LOCATION = Message::getOnLangFE("LOCATION", "Common");
        $CTNS = Message::getOnLangFE("CTNS", "Common");
        $QTY = Message::getOnLangFE("QTY", "Common");
        $ActualCTNS = Message::getOnLangFE("Actual CTNS", "Common");
        $ActualQTY = Message::getOnLangFE("Actual QTY", "Common");
        $ActualLocation = Message::getOnLangFE("Actual Location", "Common");
        $CCN = Message::getOnLangFE("CCN", "Common");
        $Total = Message::getOnLangFE("Total", "Common");
        //----/Multi Lang

        $pdf = new Mpdf();

        $html = (string)view('WavePickBarCodeTemplate', [
            'temp'        => $temp,
            'wvNum'       => $wvNum,
            'odrHdrNum'   => $odrHdrNum,
            'tempCartons' => $tempCartons,
            'picker'      => $picker,
            'userLang'    => $userLang,

            //multi Lang
            'WavePick'    => $WavePick,
            'Handler'     => $Handler,
            'Orders'      => $Orders,
            'No'          => $No,

            'ItemID'      => $ItemID,
            'UPC'         => $UPC,
            'SKU'         => $SKU,
            'Size'        => $Size,
            'Color'       => $Color,
            'Lot'         => $Lot,
            'Description' => $Description,
            'UOM'         => $UOM,
            'PackSize'    => $PackSize,

            'Cartons'        => $Cartons,
            'Piece QTY'      => $PieceQTY,
            'LOCATION'       => $LOCATION,
            'CTNS'           => $CTNS,
            'QTY'            => $QTY,
            'ActualCTNS'     => $ActualCTNS,
            'ActualQTY'      => $ActualQTY,
            'ActualLocation' => $ActualLocation,
            'CCN'            => $CCN,
            'Total'          => $Total,

        ]);
        http_response_code(200);
        $pdf->WriteHTML($html);
        $pdf->Output();
    }

    public function suggestActutalLocations(Request $request)
    {
        $input = $request->getQueryParams();

        try {

            if (!isset($input['loc_ids']) || empty($input['loc_ids'])) {
                $input['loc_ids'] = [];
            }
            if (!isset($input['item_id']) || empty($input['item_id'])) {
                throw new \Exception(Message::getOnLang("The loc_ids or item_id input is required!"));
            }
            if (!isset($input['lot']) || empty($input['lot'])) {
                throw new \Exception(Message::getOnLang("The lot input is required!"));
            }
            if (!isset($input['is_ecom'])) {
                throw new \Exception(Message::getOnLang("The is_ecom input is required!"));
            }

            if (empty($input['loc_ids'])) {
                $input['loc_ids'] = 0;
            }

            if (!isset($input['loc_code']) || empty($input['loc_code'])) {
                throw new \Exception("The location code is required!");
            }

            $locIds = explode(',', $input['loc_ids']);
            $locIds = array_filter($locIds, 'is_numeric');

            $suggestLocation = $this->cartonModel->suggestActutalLocations($locIds, $input);

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
