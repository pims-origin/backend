<?php

namespace Seldat\Wms2\Models;

class WarehouseZoneType extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'zone_type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'zone_type_id';

    /**
     * @var array
     */
    protected $fillable = [
        'zone_type_name',
        'zone_type_code',
        'zone_type_desc'
    ];

    public function zone()
    {
        return $this->hasOne(__NAMESPACE__ . '\WarehouseZone', 'zone_type_id', 'zone_type_id');
    }
}
