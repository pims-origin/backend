<?php

/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 25-May-16
 * Time: 15:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SettingTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var
     */
    protected $endpoint;

    /**
     * MenuGroupTest constructor.
     */
    public function __construct()
    {
        $this->endpoint = "/v1/settings";
    }

    /**
     * @test
     */
    public function listSetting_Ok()
    {
        $this->call('GET', $this->endpoint);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    /**
     * @test
     */
    public function readSetting_Ok()
    {
        // create data into DB
        $setting = factory(App\Setting::class)->create();

        $response = $this->call('GET', "{$this->endpoint}/{$setting->setting_key}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
        $this->assertJson($response->getContent());
    }

    /**
     * @test
     */
    public function readSetting_NotExistKey_Fail()
    {
        // Add Setting
        $setting_key = 'NoExistKey';

        $response = $this->call('GET', "{$this->endpoint}/{$setting_key}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    /**
     * @test
     */
    public function createSetting_Ok()
    {
        $this->call('POST', $this->endpoint, [
            'setting_key'   => "Created from Functional test",
            'setting_value' => ["Value from functional test"],
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    /**
     * @test
     */
    public function createSetting_EmptySettingKey_Fail()
    {
        $this->post($this->endpoint, [
            'setting_key'   => "",
            'setting_value' => ["Value from functional test"],
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @test
     */
    public function createSetting_EmptySettingValue_Ok()
    {
        $this->post($this->endpoint, [
            'setting_key'   => "Created from Functional test",
            'setting_value' => "",
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    /**
     * @test
     */
    public function createSetting_SettingValueNotArray_Ok()
    {
        $this->post($this->endpoint, [
            'setting_key'   => "Created from Functional test",
            'setting_value' => "abc",
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    /**
     * @test
     */
    public function createSetting_SettingKeyExisted_Fail()
    {
        // create data into DB
        $setting = factory(App\Setting::class)->create();

        $response = $this->call('POST', $this->endpoint, [
            'setting_key'   => $setting->setting_key,
            'setting_value' => ["abc"],
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CONFLICT);

        $this->assertEquals("'{$setting->setting_key}' setting is existed",
            json_decode($response->getContent())->errors->message);
    }

    /**
     * @test
     */
    public function createByKey_Ok()
    {
        $setting_key = "Created from Functional test";

        $this->call('POST', "{$this->endpoint}/$setting_key", [
            'setting_value' => ["Value from functional test"],
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    /**
     * @test
     */
    public function createByKey_EmptySettingValue_Ok()
    {
        $setting_key = "Created from Functional test";

        $this->post("{$this->endpoint}/$setting_key");

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    /**
     * @test
     */
    public function createByKey_SettingValueNotArray_Ok()
    {
        $setting_key = "Created from Functional test";

        $this->post("{$this->endpoint}/$setting_key", [
            'setting_value' => "abc",
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    /**
     * @test
     */
    public function createByKey_SettingKeyExisted_Fail()
    {
        // create data into DB
        $setting = factory(App\Setting::class)->create();

        $response = $this->call('POST', "{$this->endpoint}/{$setting->setting_key}", [
            'setting_value' => ["abc"],
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CONFLICT);

        $this->assertEquals("'{$setting->setting_key}' setting is existed",
            json_decode($response->getContent())->errors->message);
    }

    /**
     * @test
     */
    public function updateSetting_Ok()
    {
        // create data into DB
        $setting = factory(App\Setting::class)->create();

        $response = $this->call('PUT', "{$this->endpoint}/{$setting->setting_key}", [
            'setting_value' => ["Value updated 1", "Value updated 2"],
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
        $this->assertNotEmpty(json_decode($response->getContent()));
    }

    /**
     * @test
     */
    public function updateSetting_KeyNotExist_Fail()
    {
        $settingKey = "This key was not exist";
        $response = $this->call('PUT', "{$this->endpoint}/{$settingKey}", [
            'setting_value' => ["Value updated 1", "Value updated 2"],
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function deleteSetting_Ok()
    {
        // create data into DB
        $setting = factory(App\Setting::class)->create();

        $this->call('DELETE', "{$this->endpoint}/{$setting->setting_key}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    /**
     * @test
     */
    public function deleteSetting_NotExist_Fail()
    {
        $settingKey = "This key was not exist";

        $this->call('DELETE', "{$this->endpoint}/{$settingKey}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NOT_FOUND);
    }
}
