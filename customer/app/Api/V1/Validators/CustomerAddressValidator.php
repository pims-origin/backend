<?php

namespace App\Api\V1\Validators;


class CustomerAddressValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_add_line_1'      => 'required',
            'cus_add_country_id'  => 'required',
            'cus_add_city_name'   => 'required',
            'cus_add_state_id'    => 'required',
            'cus_add_postal_code' => 'required',
            'cus_add_type'        => 'required',
        ];
    }
}
