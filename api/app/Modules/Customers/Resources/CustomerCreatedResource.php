<?php

namespace App\Modules\Customers\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerCreatedResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "type" => "customers",
            "id" => 289,
            "attributes" => [
                "code" => $this->cus_code,
                "name" => $this->cus_name,
                "description" => $this->cus_des
            ],
            "relationships" => [
                "addresses" => AddressResource::collection($this->customerAddress),
                "contacts"  => ContactResource::collection($this->customerContact)
            ]
        ];
    }
}