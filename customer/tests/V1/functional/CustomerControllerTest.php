<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\CustomerModel;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Database\QueryException;
use Seldat\Wms2\Utils\Message;

class CustomerControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $endpointCustomer;

    /**
     * CustomerControllerCest constructor.
     */
    public function __construct()
    {
        $this->endpointCustomer = '/v1/customers';
    }

    public function testSearch_NoParam_Ok()
    {
        // make data
        factory(\Seldat\Wms2\Models\Customer::class, 3)->create();

        $response = $this->call('GET', $this->endpointCustomer);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is not null
        $this->assertNotEmpty($responseData['data']);

        // Case 3: Data has 3 records.
        //$this->assertCount(3, $responseData['data']);
    }

    public function testShow_Ok()
    {
        // make data
        $customerId = factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;

        $response = $this->call('GET', $this->endpointCustomer . "/{$customerId}");
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2 Data is not null
        $this->assertNotEmpty($responseData['data']);
    }

    public function testShow_NotExist_Ok()
    {
        $response = $this->call('GET', $this->endpointCustomer . "/99999999999999999999999999999999999");
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2 Data is not null
        $this->assertEmpty($responseData['data']);
    }

    public function testStore_Ok()
    {
        $paramStore = $this->makeParamsStore();

        $response = $this->call('POST', $this->endpointCustomer, $paramStore);
        $responseData = json_decode($response->getContent(), true);

        // Case 1 Status is 201
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        // Case 2 Data is not null
        $this->assertNotEmpty($responseData['data']);
    }

    public function testStore_DuplicateCustomerCode_Fail()
    {
        $paramStore = $this->makeParamsStore();
        $customer = factory(\Seldat\Wms2\Models\Customer::class)->create();
        $paramStore['cus_code'] = $customer->cus_code;

        $response = $this->call('POST', $this->endpointCustomer, $paramStore);
        $responseData = json_decode($response->getContent(), true);

        // Case 1 Status is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Message is: "Customer Code is existed."
        $this->assertEquals(
            Message::get("BM006", "Customer Code"),
            $responseData['errors']['message']
        );
    }

    public function testStore_NoParram_Fail()
    {
        $this->call('POST', $this->endpointCustomer);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        // make data
        $customer = factory(\Seldat\Wms2\Models\Customer::class)->create();
        $paramUpdate = $this->storeWhole($customer);

        $response = $this->call('PUT', $this->endpointCustomer . "/{$customer->cus_id}", $paramUpdate);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 201
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is not null
        $this->assertNotEmpty($responseData['data']);
    }

    public function testUpdate_DuplicateCustomerCode_Fail()
    {
        $customer = factory(\Seldat\Wms2\Models\Customer::class)->create();
        $customer2 = factory(\Seldat\Wms2\Models\Customer::class)->create();

        $paramUpdate = $this->storeWhole($customer2);
        $paramUpdate['cus_code'] = $customer->cus_code;

        $response = $this->call('PUT', $this->endpointCustomer . "/{$customer2->cus_id}", $paramUpdate);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Message is: "Customer Code is existed."
        $this->assertEquals(Message::get("BM006", "Customer Code"), $responseData['errors']['message']);
    }

    public function testUpdate_NotExist_Fail()
    {
        $customer = factory(\Seldat\Wms2\Models\Customer::class)->create();
        $paramUpdate = $this->storeWhole($customer);
        $paramUpdate['cus_code'] = uniqid();

        $response = $this->call('PUT', $this->endpointCustomer . "/9999999999999999999999999999", $paramUpdate);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 422
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Message is: "The selected cus id is invalid."
        $this->assertEquals(
            "The selected cus id is invalid.",
            $responseData['errors']['errors']['cus_id'][0]
        );
    }

    public function testUpdate_WithZero_Fail()
    {
        $customer = factory(\Seldat\Wms2\Models\Customer::class)->create();
        $paramUpdate = $this->storeWhole($customer);
        $paramUpdate['cus_code'] = uniqid();

        $response = $this->call('PUT', $this->endpointCustomer . "/0", $paramUpdate);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 422
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Message is: "The selected cus id is invalid."
        $this->assertEquals(
            "The selected cus id is invalid.",
            $responseData['errors']['errors']['cus_id'][0]
        );
    }

    //////////////////////////// COMMON FUNCTION ////////////////////////////////

    private function storeWhole($customer)
    {
        $params = [
            'cus_name'            => $customer->cus_name,
            'cus_status'          => $customer->cus_status,
            'cus_code'            => $customer->cus_code,
            'cus_billing_account' => $customer->cus_billing_account,
        ];

        $warehouseId = factory(\Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        $chargeCodeId = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;

        $chargeCodeDetail = factory(\Seldat\Wms2\Models\CustomerChargeDetail::class)->create([
            'cus_id'              => $customer->cus_id,
            'whs_id'              => $warehouseId,
            'chg_code_id'         => $chargeCodeId,
            'deleted_at'          => 915148800,
            'cus_charge_dtl_rate' => random_int(1, 100),
            'cus_charge_dtl_des'  => str_random(100),
        ])->toArray();


        // Save Zones
        $zones[] = factory(\Seldat\Wms2\Models\CustomerZone::class)->create([
            'cus_id' => $customer->cus_id,
        ])->toArray();
        $params['zones'] = $zones;

        // Save Warehouse
        $warehouses[] = factory(\Seldat\Wms2\Models\CustomerWarehouse::class)->create([
            'cus_id' => $customer->cus_id,
            'whs_id' => $warehouseId,
        ])->toArray();
        $params['warehouses'] = $warehouses;

        // Save Address
        $addresses[] = factory(\Seldat\Wms2\Models\CustomerAddress::class)->create([
            'cus_add_cus_id' => $customer->cus_id,
        ])->toArray();
        $params['addresses'] = $addresses;

        // Save Contact
        $contacts[] = factory(\Seldat\Wms2\Models\CustomerContact::class)->create([
            'cus_ctt_cus_id' => $customer->cus_id,
        ])->toArray();
        $params['contacts'] = $contacts;

        // Save charge code
        $chargeCodes[] = $chargeCodeDetail;
        $params['charge_codes'] = $chargeCodes;

        return $params;
    }

    private function makeParamsStore()
    {
        $cus_code = '';
        $customer = 1;
        while (!empty($customer)) {
            $cus_code = uniqid();
            $customer = DB::table('customer')->where('cus_code', $cus_code)->get();
        }
        $whsData = factory(\Seldat\Wms2\Models\Warehouse::class)->create();
        $params = [
            'whs_country_id'      => $whsData->whs_country_id,
            'cus_name'            => uniqid(),
            'cus_status'          => 'AC',
            'cus_code'            => $cus_code,
            'cus_billing_account' => uniqid(),
        ];

        $customerId = factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        $warehouseId = $whsData->whs_id;
        $chargeCodeId = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;

        $chargeCodeDetail = factory(\Seldat\Wms2\Models\CustomerChargeDetail::class)->create([
            'cus_id'              => $customerId,
            'whs_id'              => $warehouseId,
            'chg_code_id'         => $chargeCodeId,
            'deleted_at'          => 915148800,
            'cus_charge_dtl_rate' => random_int(1, 100),
            'cus_charge_dtl_des'  => str_random(100),
        ])->toArray();

        // Save Zones
        $zones[] = factory(\Seldat\Wms2\Models\Zone::class)->create()->toArray();
        $params['zones'] = $zones;

        // Save Warehouse
        $warehouses[] = $whsData->toArray();
        $params['warehouses'] = $warehouses;

        $systemState1 = factory(\Seldat\Wms2\Models\SystemState::class)->create();
        $systemState2 = factory(\Seldat\Wms2\Models\SystemState::class)->create();
        $systemState3 = factory(\Seldat\Wms2\Models\SystemState::class)->create();

        $systemCountry1 = factory(\Seldat\Wms2\Models\SystemCountry::class)->create();
        $systemCountry2 = factory(\Seldat\Wms2\Models\SystemCountry::class)->create();
        $systemCountry3 = factory(\Seldat\Wms2\Models\SystemCountry::class)->create();

        // Address
        $params['addresses'] = [
            '0' => [
                'cus_add_type'        => 'type 0',
                'cus_add_line_1'      => 'line 1 0',
                'cus_add_city_name'   => 'city name 0',
                'cus_add_state_id'    => $systemState1->sys_state_id,
                'cus_add_postal_code' => 'postal code 0',
                'cus_add_country_id'  => $systemCountry1->sys_country_id,
            ],
            '1' => [
                'cus_add_type'        => 'type 1',
                'cus_add_line_1'      => 'line 1 1',
                'cus_add_city_name'   => 'city name 1',
                'cus_add_state_id'    => $systemState2->sys_state_id,
                'cus_add_postal_code' => 'postal code 1',
                'cus_add_country_id'  => $systemCountry2->sys_country_id,
            ],
            '2' => [
                'cus_add_type'        => 'type 2',
                'cus_add_line_1'      => 'line 1 2',
                'cus_add_city_name'   => 'city name 2',
                'cus_add_state_id'    => $systemState3->sys_state_id,
                'cus_add_postal_code' => 'postal code 3',
                'cus_add_country_id'  => $systemCountry3->sys_country_id,
            ],

        ];

        $params['contacts'] = [
            '0' => [
                'cus_ctt_fname'      => 'fname 0',
                'cus_ctt_lname'      => 'lname 0',
                'cus_ctt_email'      => 'email0@gmail.com',
                'cus_ctt_phone'      => '0987654321',
                'cus_ctt_ext'        => 'ext 0',
                'cus_ctt_mobile'     => '0987654321',
                'cus_ctt_position'   => 'position 0',
                'cus_ctt_department' => 'department 0',
            ],
            '1' => [
                'cus_ctt_fname'      => 'fname 1',
                'cus_ctt_lname'      => 'lname 1',
                'cus_ctt_email'      => 'email1@gmail.com',
                'cus_ctt_phone'      => '0987654322',
                'cus_ctt_ext'        => 'ext 0',
                'cus_ctt_mobile'     => '0987654322',
                'cus_ctt_position'   => 'position 1',
                'cus_ctt_department' => 'department 1',
            ],
            '2' => [
                'cus_ctt_fname'      => 'fname 2',
                'cus_ctt_lname'      => 'lname 2',
                'cus_ctt_email'      => 'email2@gmail.com',
                'cus_ctt_phone'      => '0987654323',
                'cus_ctt_ext'        => 'ext 0',
                'cus_ctt_mobile'     => '0987654323',
                'cus_ctt_position'   => 'position 2',
                'cus_ctt_department' => 'department 2',
            ],

        ];

        // Save charge code
        $chargeCodes[] = $chargeCodeDetail;
        $params['charge_codes'] = $chargeCodes;

        return $params;
    }
}