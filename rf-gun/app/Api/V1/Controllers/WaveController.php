<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\WaveDtlLocModel;
use App\Api\V1\Models\WaveDtlModel;
use App\Api\V1\Models\WaveHdrModel;
use App\Api\V1\Transformers\WaveDetailTransformer;
use App\Api\V1\Transformers\WaveTransformer;
use App\Api\V1\Validators\WaveValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

/**
 * Class WaveController
 *
 * @package App\Api\V1\Controllers
 */
class WaveController extends AbstractController
{

    /**
     * @var int|mixed
     */
    protected $userId;

    /**
     * @var WaveHdrModel
     */
    protected $waveHdrModel;

    /**
     * @var WaveDtlModel
     */
    protected $waveDtlModel;

    /**
     * @var WaveDtlLocModel
     */
    protected $waveDtlLocModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * WaveController constructor.
     *
     * @param WaveHdrModel $waveHdrModel
     * @param WaveDtlModel $waveDtlModel
     * @param WaveDtlLocModel $waveDtlLocModel
     * @param CartonModel $cartonModel
     */
    public function __construct(
        WaveHdrModel $waveHdrModel,
        WaveDtlModel $waveDtlModel,
        WaveDtlLocModel $waveDtlLocModel,
        CartonModel $cartonModel,
        PackDtlModel $packDtlModel,
        PackHdrModel $packHdrModel,
        ItemModel $itemModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->waveHdrModel = $waveHdrModel;
        $this->waveDtlModel = $waveDtlModel;
        $this->waveDtlLocModel = $waveDtlLocModel;
        $this->cartonModel = $cartonModel;
        $this->palletModel = new PalletModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->locationModel = new LocationModel();
        $this->packHdrModel = $packHdrModel;
        $this->packDtlModel = $packDtlModel;
        $this->itemModel = $itemModel;
        $this->eventTrackingModel = $eventTrackingModel;

        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param WaveTransformer $waveTransformer
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search($whsId, Request $request, WaveTransformer $waveTransformer, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            $allWaves = $this->waveHdrModel->findWhere([
                'whs_id' => $input['whs_id'],
                'picker' => $input['picker']
            ], ['details']);

            if (!empty($allWaves)) {
                foreach ($allWaves as $key => $allWave) {
                    $allWave['total'] = count($allWave['details']);
                    $actual = $this->waveDtlModel->getActualWvDtl($allWave['wv_id']);
                    $allWave['actual'] = 0;
                    if (!empty($actual[0])) {
                        $allWave['actual'] = array_get($actual[0], 'actual', 0);
                    }
                    //
                    $statusWvDtl = $this->waveDtlModel->getStatusWvHdr($allWave['wv_id']);
                    $status = 'NOT';
                    if (!empty($statusWvDtl[0])) {
                        $sumActualWvdtl = array_get($statusWvDtl[0], 'actual', 0);
                        $sumPieceQtyWvdtl = array_get($statusWvDtl[0], 'piece_qty', 0);

                        if ($sumActualWvdtl === $sumPieceQtyWvdtl) {
                            $status = 'DONE';
                        } else if ($sumActualWvdtl > 0 && $sumActualWvdtl < $sumPieceQtyWvdtl) {
                            $status = 'IN';
                        }
                    }
                    $allWave['status'] = $status;
                    $allWaves[$key] = $allWave;
                }
                return $this->response->collection($allWaves, $waveTransformer);
            }else{
                return ['data' => []];
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getStatusWvDtl($wv_id) {

        $statusWvDtl = $this->waveDtlModel->getStatusWvDtl($wv_id);

        $status = 'NOT';
        if (!empty($statusWvDtl[0])) {
            $sumActualWvdtl = array_get($statusWvDtl[0], 'actual', 0);
            $sumPieceQtyWvdtl = array_get($statusWvDtl[0], 'piece_qty', 0);

            if ($sumActualWvdtl === $sumPieceQtyWvdtl) {
                $status = 'DONE';
            } else if ($sumActualWvdtl > 0 && $sumActualWvdtl < $sumPieceQtyWvdtl) {
                $status = 'IN';
            }
        }


        return $status;
    }

    /**
     * @param $whsId
     * @param $wvHdrId
     * @param WaveDetailTransformer $waveDtlTransformer
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($whsId, $wvHdrId, WaveDetailTransformer $waveDtlTransformer, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            $wave = $this->waveHdrModel->getFirstWhere([
                'whs_id' => $input['whs_id'],
                'picker' => $input['picker'],
                'wv_id'  => $wvHdrId
            ], ['details']);

            return $this->response->collection($wave->details, $waveDtlTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $wvDtlId
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function detail($whsId, $wvDtlId, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            $wave = $this->waveDtlModel->getFirstWhere([
                'whs_id'    => $whsId,
                'wv_dtl_id' => $wvDtlId
            ], ['waveHdr']);

            if (empty($wave->waveHdr) || $wave->waveHdr->picker != $this->userId) {
                return $this->response->noContent();
            }

            $sku_size_color = $wave->sku;
            $size = !empty($wave->size) ? strtoupper($wave->size) : '';
            if ($size != 'NA') {
                $sku_size_color .= '-'.$wave->size;
            }
            $color = !empty($wave->color) ? strtoupper($wave->color) : '';
            if ($color != 'NA') {
                $sku_size_color .= '-'.$wave->color;
            }

            $detail['data'] = [
                'wv_id'     => $wave->wv_id,
                'wv_num'    => $wave->wv_num,
                'wv_dtl_id' => $wave->wv_dtl_id,
                'sku'       => $sku_size_color,
                'qty'       => (int)$wave->piece_qty . "/" . (int)$wave->act_piece_qty,
                'status'    => $this->getStatusWvDtl($wvDtlId)
            ];

            return $detail;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $wvDtlId
     * @param Request $request
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function suggest($whsId, $wvDtlId, Request $request, WaveValidator $waveValidator)
    {
        //$this->updateWavePickByLocation([
        //    'whs_id'    => 2,
        //    'wv_dtl_id' => 2,
        //    'codes'     => [
        //        'RD-01-L1-12',
        //        'RE-01-L3-13',
        //        'RJ-01-L5-16',
        //        'RO-01-L1-10'
        //    ]
        //]);
        //die('xong');
        $input = $request->getQueryParams();

        // get data from HTTP
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            if (empty($input['type'])) {
                $input['type'] = "CT";
            }

            if (!in_array($input['type'], ["LC", "CT"])) {
                throw new \Exception("'type' is invalid!");
            }

            $waveDtlLoc = $this->waveDtlLocModel->getFirstWhere(['wv_dtl_id' => $wvDtlId], ['waveHdr', 'waveDtl']);

            if (empty($waveDtlLoc->waveHdr)
                || empty($waveDtlLoc->waveDtl)
                || $waveDtlLoc->waveHdr->picker != $this->userId
            ) {
                return $this->response->noContent();
            }

            $itemId = object_get($waveDtlLoc, "waveDtl.item_id", 0);
            $pieceQty = object_get($waveDtlLoc, "waveDtl.piece_qty", 0);
            $actPieceQty = object_get($waveDtlLoc, "waveDtl.act_piece_qty", 0);
            $packSize = object_get($waveDtlLoc, "waveDtl.pack_size", 0);
            $limit = ceil(($pieceQty - $actPieceQty) / $packSize);

            $locIds = explode(",", $waveDtlLoc->sug_loc_ids);
            if ($input['type'] == "CT") {
                $cartons = $this->cartonModel->getSuggest([
                    'whs_id'  => $whsId,
                    'item_id' => $itemId,
                    'loc_id'  => $locIds,
                ], $limit)->toArray();

                $carton['data'] = array_map(function ($e) {
                    return $e['ctn_num'];
                }, $cartons);
            } else {
                $location = $this->locationModel->getSuggest($locIds)->toArray();
                $carton['data'] = array_map(function ($e) {
                    return $e['loc_alternative_name'];
                }, $location);
            }

            return $carton;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $wv_dtl_id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function updateWavepick($whsId, $wv_dtl_id, Request $request)
    {
        set_time_limit(0);

        // get data from HTTP
        $input = $request->getParsedBody();
        $type = $input['type'];
        $codes = $input['codes'];

        try {

            if ($type == "CT") {
                $dataWaveByCartons = [
                    'whs_id'    => $whsId,
                    'wv_dtl_id' => $wv_dtl_id,
                    'codes'     => $codes
                ];
                $this->updateWavePickByCarton($dataWaveByCartons);
            } elseif ($type == "LC") {
                $dataWaveByPallets = [
                    'whs_id'    => $whsId,
                    'wv_dtl_id' => $wv_dtl_id,
                    'codes'     => $codes
                ];

                $cartonCode = $this->updateWavePickByLocation($dataWaveByPallets);
                // check number
                $dataWaveByCartons = $dataWaveByCartons = [
                    'whs_id'    => $whsId,
                    'wv_dtl_id' => $wv_dtl_id,
                    'codes'     => $cartonCode

                ];
                $this->updateWavePickByCarton($dataWaveByCartons);

            } else {

                $dataWaveByEA = [
                    'whs_id'    => $whsId,
                    'wv_dtl_id' => $wv_dtl_id,
                    'codes'     => null
                ];
                $this->updateWavePickByEA($dataWaveByEA);
            }

            $wvDtl = $this->waveDtlModel->getFirstWhere(['wv_dtl_id' => $wv_dtl_id]);

            $event = [
                'whs_id'    => $whsId,
                'cus_id'    => object_get($wvDtl, 'cus_id'),
                'owner'     => object_get($wvDtl, 'wv_num'),
                'evt_code'  => 'RFW',
                'trans_num' => 'RFG',
                'info'      => sprintf("RF Gun -  Update Wave Pick by %s", ($type=='CT')? 'Carton' : 'Location'),
            ];

            //  Evt tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($event);

            return $this->response->noContent()
                ->setContent(['data' => ['message' => 'Update wavepick successfully']])
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $data
     */
    private function updateWavePickByCarton($data)
    {
        $whs_id = $data['whs_id'];
        $wv_dtl_id = $data['wv_dtl_id'];
        $codes = $data['codes'];

        $ctnLists = $this->cartonModel->checkActiveCartonByCtnNum($codes)->toArray();

        if (count($ctnLists) == 0) {
            throw new Exception(Message::get("BM155"));
        }

        // Check active & existed carton
        $dataCheckActive = [
            'ctnNums'  => $codes,
            'ctnLists' => $ctnLists
        ];
        $this->checkActiveCarton($dataCheckActive);

        // Get wavepick detail info
        $dataWaveDtlInfo = $this->waveDtlModel->getFirstWhere([
            'wv_dtl_id' => $wv_dtl_id
        ]);

        // Check wave pick detail existed
        if (empty($dataWaveDtlInfo)) {
            throw new Exception(Message::get("BM017", "Wavepick detail " . $wv_dtl_id));
        }

        $itemId = array_get($dataWaveDtlInfo, 'item_id', null);
        $cusId = array_get($dataWaveDtlInfo, 'cus_id', null);
        $wv_id = array_get($dataWaveDtlInfo, 'wv_id', null);
        $piece_qty = array_get($dataWaveDtlInfo, 'piece_qty', null);
        $act_piece_qty = array_get($dataWaveDtlInfo, 'act_piece_qty', 0);

        // Get all carton by item id
        $dataGetCtnInfo = [
            'whsId'   => $whs_id,
            'itemId'  => $itemId,
            'cusId'   => $cusId,
            'ctnNums' => $codes
        ];

        $dataCartonInfos = $this->cartonModel->getAllCartonByItemId($dataGetCtnInfo)->toArray();

        if (count($dataCartonInfos) == 0) {
            throw new Exception(Message::get("BM017", "Carton with item " . $itemId));
        }

        $dataCheckCartonHasItem = [
            'item_id' => $itemId,
            'ctnNums' => $codes,
            'cartons' => $dataCartonInfos
        ];

        // Check carton has item_id or not
        $this->checkCartonHasItemId($dataCheckCartonHasItem);

        // Start to handle valid cartons
        $sumPieceRemainFromCtns = 0;
        foreach ($dataCartonInfos as $dataCartonInfo) {
            $sumPieceRemainFromCtns += $dataCartonInfo['piece_remain'];
        }

        $actPicked = $act_piece_qty + $sumPieceRemainFromCtns;

        if ($actPicked > $piece_qty) {
            throw new Exception(Message::get("BM156", $actPicked, $piece_qty));
        }

        // Update wave pick detail
        $dataUpdateWvDtl = [
            'act_piece_qty' => $actPicked,
            'wv_dtl_id'     => $wv_dtl_id,
            'wv_dtl_sts'    => ($actPicked < $piece_qty
                ? Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS")
                : Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS"))
        ];

        \DB::beginTransaction();
        $this->waveDtlModel->updateWvDtl($dataUpdateWvDtl);

        /*
         * ==================================================================
         * HANDLE TABLES: CARTONS, PALLET, ORDER CARTON WHEN PICK FULL CARTON
         * ==================================================================
         * */

        // insert order carton
        $dataOrderCarton = [
            'wv_id'           => $wv_id,
            'dataCartonInfos' => $dataCartonInfos
        ];
        $this->processOrderCarton($dataOrderCarton);

        // update carton status, release all carton from pallet & location
        $this->updateCartonStatusToPicked($dataCartonInfos);

        // update total carton in pallet table, also release pallet from location if has no carton
        $this->updateCtnTtl($dataCartonInfos);

        $this->updateOrderPicked($wv_id, $wv_dtl_id);

        $this->updateWaveHeaderPicked($wv_id);



        /*
         * ==================================================================
         * END PROCESS
         * ==================================================================
         * */
        \DB::commit();
    }

    /**
     * @param $dataCartonInfos
     */
    private function updateCtnTtl($dataCartonInfos)
    {
        $arrLocIds = [];
        $arrPltIds = [];
        foreach ($dataCartonInfos as $dataCartonInfo) {
            if (!in_array($dataCartonInfo['loc_id'], $arrLocIds)) {
                array_push($arrLocIds, $dataCartonInfo['loc_id']);
            }

            if (!in_array($dataCartonInfo['plt_id'], $arrPltIds)) {
                array_push($arrPltIds, $dataCartonInfo['plt_id']);
            }
        }
        //$this->palletModel->updateCtnTtl($arrLocIds);
        $this->updatePalletWhenPickCarton($arrPltIds);
    }

    /**
     * @param $dataOrderCarton
     */
    private function processOrderCarton($dataOrderCarton)
    {
        $wv_id = $dataOrderCarton['wv_id'];
        $dataCartonInfos = $dataOrderCarton['dataCartonInfos'];
        $ordersDtlInfos = $this->orderDtlModel->getListOrderDtlOfWavePick($wv_id);

        foreach ($ordersDtlInfos as $ordersDtlInfo) {
            foreach ($dataCartonInfos as $dataCartonInfo) {
                $item = [
                    'ctn_num' => $dataCartonInfo['ctn_num'],
                    'ctn_id'  => $dataCartonInfo['ctn_id']
                ];
                $dataOrderCarton = [
                    'odr_dtl'    => $ordersDtlInfo,
                    'item'       => $item,
                    'picked_qty' => array_get($dataCartonInfo, 'piece_remain', null),
                    'wv_id'      => $wv_id,
                    'is_storage' => 0
                ];
                $this->insertOrderCarton($dataOrderCarton);
            }
        }
    }

    /**
     * @param $data
     */
    private function insertOrderCarton($data)
    {
        $odr_dtl = $data['odr_dtl'];
        $item = $data['item'];
        $picked_qty = $data['picked_qty'];
        $wv_id = $data['wv_id'];
        $isStorage = $data['is_storage'];

        $dataOrderCartonInsert = [
            'odr_hdr_id' => $odr_dtl['odr_id'],
            'odr_dtl_id' => $odr_dtl['odr_dtl_id'],
            'wv_hdr_id'  => $wv_id,
            'wv_dtl_id'  => $odr_dtl['wv_dtl_id'],
            'odr_num'    => $odr_dtl['odr_num'],
            'wv_num'     => $odr_dtl['wv_num'],
            'ctn_num'    => $item['ctn_num'],
            'ctn_id'     => $item['ctn_id'],
            'piece_qty'  => $picked_qty,
            'ship_dt'    => 0,
            'sts'        => Status::getByKey('CTN_STATUS', 'PICKED'),
            'is_storage' => $isStorage
        ];

        $this->orderCartonModel->refreshModel();
        $this->orderCartonModel->create($dataOrderCartonInsert);
    }

    /**
     * @param $pltIds
     */
    private function updatePalletWhenPickCarton($pltIds)
    {
        $allPalletHasNoCartons = $this->palletModel->getAllPalletHasNoCarton($pltIds);
        foreach ($allPalletHasNoCartons as $allPalletHasNoCarton) {
            $this->palletModel->updatePallet($allPalletHasNoCarton);
        }
    }

    /**
     * Pick full carton
     *
     * @param $dataCartonInfos
     */
    private function updateCartonStatusToPicked($dataCartonInfos)
    {
        foreach ($dataCartonInfos as $ctn) {
            $created_at = (int)array_get($ctn, 'created_at', 0);

            $picked_dt = time();

            // Calculate storage_duration
            $date1 = date("Y-m-d", is_int($created_at) || is_string($created_at) ? (int)$created_at :
                $created_at->timestamp);

            $date2 = date("Y-m-d");

            $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }
            $dataCartonUpdate = [
                'ctn_sts'          => Status::getByKey('CTN_STATUS', 'PICKED'),
                'picked_dt'        => $picked_dt,
                'storage_duration' => $storageDuration,
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'plt_id'           => null,
            ];

            $this->cartonModel->updateWhere($dataCartonUpdate, [
                'ctn_num' => $ctn['ctn_num']
            ]);

            // Update ctn_ttl in pallet table
            $plt_id = array_get($ctn, 'plt_id', null);

            $palletInfo = $this->palletModel->getFirstWhere(['plt_id' => $plt_id]);

            $this->palletModel->updateWhere([
                'ctn_ttl' => array_get($palletInfo, 'ctn_ttl', null) - 1
            ], [
                'plt_id' => $plt_id
            ]);
        }
    }

    /**
     * @param $dataCheckActive
     */
    private function checkActiveCarton($dataCheckActive)
    {
        $ctnNums = $dataCheckActive['ctnNums'];
        $ctnLists = $dataCheckActive['ctnLists'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id'  => $e['ctn_id'],
                'ctn_num' => $e['ctn_num'],
            ];
        }, $ctnLists);
        $allCtns = array_pluck($tempCtns, null, "ctn_num");

        foreach ($ctnNums as $key => $value) {
            if (!isset($allCtns[$value])) {
                throw new Exception("Carton " . $value . " is not active or existed");
            }
        }
    }

    /**
     * @param $dataCheckCarton
     */
    private function checkCartonHasItemId($dataCheckCarton)
    {
        $itemId = $dataCheckCarton['item_id'];
        $ctnNums = $dataCheckCarton['ctnNums'];
        $ctnLists = $dataCheckCarton['cartons'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id'  => $e['ctn_id'],
                'ctn_num' => $e['ctn_num'],
            ];
        }, $ctnLists);
        $allCtns = array_pluck($tempCtns, null, "ctn_num");

        foreach ($ctnNums as $key => $value) {
            if (!isset($allCtns[$value])) {
                throw new Exception("Carton " . $value . " does not has item " . $itemId);
            }
        }
    }

    private function updateWavePickByPallet($data)
    {
        $whs_id = $data['whs_id'];
        $wv_dtl_id = $data['wv_dtl_id'];
        $codes = $data['codes'];

    }

    /**
     * @param $data
     *
     * @return int
     */
    private function updateWavePickByEA($data)
    {
        $whs_id = $data['whs_id'];
        $wv_dtl_id = $data['wv_dtl_id'];
        $codes = $data['codes'];

        return 0;
    }

    private function updateOrderPicked($wvHdrId, $wvDtlId)
    {
        //  Update Oder Dtl
        $this->orderDtlModel->updatePDOdrDtl($wvHdrId);

        //  Update Oder Hdr is picking
        //$this->orderHdrModel->updatePKOdrHdr($wvHdrId);

        //  Update Oder Hdr is picked
        $this->orderHdrModel->updatePDOdrHdr($wvHdrId);

        //  Order Auto pack
        $odrCartons = $this->orderCartonModel->autoPack($wvHdrId);

        if ($odrCartons) {
            $odrIdAutoPacks = [];
            foreach ($odrCartons as $odrCarton) {

                $odrId = array_get($odrCarton, 'odr_id');
                $odrIdAutoPacks[$odrId] = $odrId;

                $item = $this->itemModel->getFirstBy('item_id', $odrCarton['item_id']);

                //  Update Oder Hdr is picked
                $this->packHdrModel->refreshModel();
                $packHdr = $this->packHdrModel->create([
                    'pack_hdr_num' => str_replace('CTN', 'CNT', $odrCarton['ctn_num']),
                    'seq' => 1,
                    'odr_hdr_id' => $odrId,
                    'cnt_id' => array_get($odrCarton, 'ctn_id'),
                    'whs_id' => array_get($odrCarton, 'whs_id'),
                    'cus_id' => array_get($odrCarton, 'cus_id'),
                    'width' => array_get($item, 'width', null),
                    'height' => array_get($item, 'height', null),
                    'length' => array_get($item, 'length', null),
                    'carrier_name' => '',
                    'sku_ttl' => 1,
                    'piece_ttl' => array_get($odrCarton, 'piece_remain'),
                    'pack_sts' => 'AC',
                    'pack_type' => 'CT',
                    'ship_to_name' => '',
                    'sts' => 'i'
                ]);

                //  Update Oder Hdr is picked
                $this->packDtlModel->refreshModel();
                $this->packDtlModel->create([
                    'pack_hdr_id' => $packHdr->pack_hdr_id,
                    'odr_hdr_id' => array_get($odrCarton, 'odr_id'),
                    'whs_id' => array_get($odrCarton, 'whs_id'),
                    'cus_id' => array_get($odrCarton, 'cus_id'),
                    'item_id' => array_get($odrCarton, 'item_id'),
                    'size' => array_get($item, 'size', null),
                    'lot' => array_get($item, 'lot', null),
                    'sku' => array_get($item, 'sku', null),
                    'color' => array_get($item, 'color', null),
                    'cus_upc' => array_get($item, 'cus_upc', null),
                    'uom_id' => array_get($item, 'uom_id', null),
                    'piece_qty' => array_get($odrCarton, 'piece_remain'),
                    'sts' => 'i',
                ]);
            }
            //  Update Oder Hdr is packed
            $this->orderHdrModel->updatePAOdrHdr($odrIdAutoPacks);
        }
    }

    private function updateWaveHeaderPicked($wvHdrId)
    {
        $allDetail = DB::table('wv_dtl')->select(['wv_dtl_sts'])->where(['wv_id' => $wvHdrId])->get();
        if (!empty($allDetail)) {
            $picked = 1;
            $picking = 0;
            foreach ($allDetail as $detail) {

                if ($detail['wv_dtl_sts'] == "PK") {
                    $picking = true;
                    break;
                }

                if ($detail['wv_dtl_sts'] != "PD") {
                    $picked = 0;
                }
            }

            if ($picking) {
                $this->waveHdrModel->update(['wv_id' => $wvHdrId, 'wv_sts' => 'PK']);
            } else if ($picked) {
                $this->waveHdrModel->update(['wv_id' => $wvHdrId, 'wv_sts' => 'CO']);
            }
        }
    }


    public function wavepickDashboard($whsId, Request $request, WaveValidator $waveValidator)
    {
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;
        $waveValidator->validate($input);
        try {


            $wavepick = $this->waveHdrModel->getWaveDashBoard($input, ['details']);

            $result = ["wavepick" => 0, "picking" => 0, "picked" => 0];
            if (!empty($wavepick)) {
                foreach ($wavepick as $key => $allWave) {
                    $total = count($allWave['details']);
                    $picking = 0;
                    foreach ($allWave['details'] as $detail) {
                        if (empty($detail['act_piece_qty'])) {
                            $picking++;
                        }
                    }
                    $result['wavepick'] = $result['wavepick'] + $total;
                    $result['picked'] = $result['picked'] + $total - $picking;
                    $result['picking'] = $result['picking'] + $picking;
                }

            }

            return $this->response->array($result);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function updateWavePickByLocation($params)
    {
        // Wave Pick Detail
        $wvDtl = $this->waveDtlModel->getFirstWhere(['wv_dtl_id' => $params['wv_dtl_id']]);

        if (empty($wvDtl)) {
            throw new Exception("Wave Pick detail is empty!");
        }

        $ctnTotal = ceil(($wvDtl->piece_qty - $wvDtl->act_piece_qty) / $wvDtl->pack_size);

        $locations = $this->locationModel->getByLocCode($params['codes'], $params['whs_id'])->toArray();
        $locationCodes = array_pluck($locations, 'loc_id', 'loc_code');
        foreach ($params['codes'] as $code) {
            if (empty($locationCodes[$code])) {
                return "$code is invalid!";
            }
        }

        $result = $this->cartonModel->getCartonsByLocs($locationCodes)->toArray();

        $result = array_column($result, 'ctn_num');
        if(count($result) == 0) {
            throw new Exception("There're no cartons in this warehouse!");
        }
        // only pick total cartons of location <= total carton
        if (count($result) > $ctnTotal) {
            throw new Exception("Total pieces of locations larger needed pick!");
        }

        return $result;
    }
}

