<?php

namespace App\Api\V1\Validators;


class AddSKUValidator extends AbstractValidator
{
    public function rules()
    {
       return [
           'cycle_hdr_id' => 'required|exists:cycle_hdr,cycle_hdr_id',
           'whs_id'       => 'required|exists:warehouse,whs_id',
           'cus_id'       => 'required|exists:customer,cus_id',
           'sku'          => 'required|max:50',
           'size'         => 'max:30',
           'color'        => 'max:30',
           'pack'         => 'required|integer',
           'remain'       => 'required|integer',
           'cycle_type'   => 'required|max:2',
           'act_loc'      => 'required',
           'act_qty'      => 'required|integer'
       ];
    }
}