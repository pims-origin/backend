<?php

namespace Seldat\Wms2\Models;

class Reason extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reason';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'r_id';

    /**
     * @var array
     */
    protected $fillable = [
        'r_name',
        'reason_desc'
    ];
}
