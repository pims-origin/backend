<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ReturnHdr;

/**
 * Class ReturnHdrModel
 *
 * @package App\Api\V1\Models
 */
class ReturnHdrModel extends AbstractModel
{
    /**
     * ReturnHdrModel constructor.
     *
     * @param ReturnHdr|null $model
     */
    public function __construct(ReturnHdr $model = null)
    {
        $this->model = ($model) ?: new ReturnHdr();
    }

    /**
     * @param $returnHdrId
     *
     * @return mixed
     */
    public function deleteReturnHdr($returnHdrId)
    {
        return $this->model
            ->where('return_id', $returnHdrId)
            ->delete();
    }

    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function checkWhereIn($orderIds)
    {
        return $this->model
            ->whereIn('odr_id', $orderIds)
            ->count();
    }

    /**
     * @param array $width
     *
     * @return mixed
     */
    public function getFirstMax($width = [])
    {
        return $this->make($width)
            ->orderBy('return_id', 'desc')
            ->first();
    }
}
