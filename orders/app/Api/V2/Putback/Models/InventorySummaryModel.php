<?php

namespace App\Api\V2\Putback\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;
use stdClass;

class InventorySummaryModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        // search sku
        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        // search color
        if (isset($attributes['color'])) {
            $query->where('color', 'like', "%" . SelStr::escapeLike($attributes['color']) . "%");
        }

        // search size
        if (isset($attributes['size'])) {
            $query->where('size', 'like', "%" . SelStr::escapeLike($attributes['size']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    public function autoSearch($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        // search sku
        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        // search color
        if (isset($attributes['color'])) {
            $query->where('color', 'like', "%" . SelStr::escapeLike($attributes['color']) . "%");
        }

        // search size
        if (isset($attributes['size'])) {
            $query->where('size', 'like', "%" . SelStr::escapeLike($attributes['size']) . "%");
        }

        // search lot
        if (isset($attributes['lot'])) {
            $query->where('lot', 'like', "%" . SelStr::escapeLike($attributes['lot']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }

        $query->where('avail', '>', 0);

        $query->groupBy('sku');

        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);
        $data = $models;

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whs = array_get($userInfo, 'current_whs', 0);
        $itemIdArr = [];

        $itemModel = new ItemModel();

        foreach ($data as $key => $item) {
            $itemData = $itemModel->getFirstBy('item_id', $item['item_id']);
            $models[$key]->item_code = $itemData->item_code;
            $models[$key]->description = $itemData->description;
            $avail = $this->getInvSumbyOdrDlt($item['item_id'], 'ANY', $whs);
            $itemIdArr[$item['item_id']] = $item['item_id'];
            $models[$key]->avail = $avail;
            $models[$key]->pack = $itemData->pack;
        }

        $this->rebuildPackSizeArrNew($models, $itemIdArr, $attributes);

        return $models;
    }

    /**
     * Rebuild pack size to array on page create order
     *
     * @param LengthAwarePaginator $models Pointer
     * @param Array $itemIdArr Array item id
     */
    private function rebuildPackSizeArr(&$models, $itemIdArr, $attributes)
    {
        $itemIdArr = array_values($itemIdArr);
        $cartonsModel = new CartonModel();
        $packObj = $cartonsModel->getCartonsByItemId($itemIdArr);
        $packArr = [];
        $lotArr = [];

        $avail = $this->getAvailBySku($attributes['sku']);
        $availArr = [];
        foreach ($avail as $itemLot) {
            $key = "{$itemLot['item_id']}-{$itemLot['lot']}";
            $availArr[$key] = $itemLot['available'];
        }

        foreach ($packObj as $key => $value) {
            $packSize = $value['ctn_pack_size'];
            $packArr[$value['item_id']][$packSize] = $packSize;
            $lotUni = "{$value['lot']}-{$value['ctn_pack_size']}";
            $tmpObj = new stdClass();
            $tmpObj->pack_size = $value['ctn_pack_size'];
            $tmpObj->sku = $value['sku'];
            $lotArr[$value['item_id']][$value['lot']][$lotUni] = $tmpObj;
        }

        foreach ($models as $idx => $val) {
            //$models[$idx]->pack_arr = $packArr[$val['item_id']];
            if (isset($packArr[$val['item_id']])) {
                $arr = array_values($packArr[$val['item_id']]);
                $arrItem = [];
                foreach ($arr as $value) {
                    $obj = new stdClass();
                    $obj->pack_size = $value;
                    $arrItem[] = $obj;
                }

                $models[$idx]->pack = $arrItem;
            } else {
                $obj = new \stdClass();
                $obj->pack_size = $val['pack'];
                $tmp = [$obj];
                $models[$idx]->pack = $tmp;
            }

            if (isset($lotArr[$val['item_id']])) {
                $lot = $lotArr[$val['item_id']];
                $lotArr2 = [];
                foreach ($lot as $lotName => $packSize) {
                    $tmp = new \stdClass();
                    $tmp->lot = $lotName;
                    $tmp->pack = array_values($packSize);
                    $cKey = "{$val['item_id']}-{$lotName}";
                    $tmp->avail = isset($availArr[$cKey]) ? $availArr[$cKey] : 0;
                    $lotArr2[] = $tmp;
                }

                $models[$idx]->lot = $lotArr2;

            }

        }

    }

    private function rebuildPackSizeArrNew(&$models, $itemIdArr, $attributes)
    {
        $itemIdArr = array_values($itemIdArr);
        $cartonsModel = new CartonModel();
        $packObj = $cartonsModel->getCartonsByItemId($itemIdArr);
        $packArr = [];
        $lotArr = [];

        $avail = $this->getAvailBySku($attributes['sku']);
        $availArr = [];
        foreach ($avail as $itemLot) {
            $key = "{$itemLot['item_id']}-{$itemLot['lot']}";
            $availArr[$key] = $itemLot['available'];
            $lotArr[$itemLot['item_id']][] = $itemLot['lot'];
        }

        foreach ($models as $idx => $val) {
            //$models[$idx]->pack_arr = $packArr[$val['item_id']];
            if (isset($lotArr[$val['item_id']])) {
                $lot = $lotArr[$val['item_id']];
                $lotArr2 = [];
                $availAny = 0;
                foreach ($lot as $lotName) {
                    $tmp = new \stdClass();
                    $tmp->lot = $lotName;
                    $cKey = "{$val['item_id']}-{$lotName}";
                    $tmp->avail = isset($availArr[$cKey]) ? $availArr[$cKey] : 0;
                    $availAny += $tmp->avail;
                    $lotArr2[] = $tmp;
                }

                $anyClass = new stdClass();
                $anyClass->lot = 'ANY';
                $anyClass->avail = $availAny;
                array_unshift($lotArr2, $anyClass);

                $models[$idx]->lot = $lotArr2;

            }

        }

    }

    /**
     * @param $item_ids
     *
     * @return mixed
     */
    public function getByItemIds($item_ids)
    {
        $userInfo = $this->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $item_ids = is_array($item_ids) ? $item_ids : [$item_ids];

        $rows = $this->model
            ->whereIn('item_id', $item_ids)
            ->where('whs_id', '=', $currentWH)
            ->get();

        return $rows;
    }

    public function getAvailableQty($itemId)
    {
        $query = $this->make([]);

        $query->where('item_id', $itemId);
        $query->where('avail', '>', 0);
        $this->model->filterData($query);

        return $query->first();
    }

    public function getInvSumbyOdrDlt($itemId, $lot, $whs_id, $get = 'avail')
    {
        $query = $this->make([]);

        $query = $query->where('item_id', $itemId);
        if (strtoupper($lot) != 'ANY') {
            $query->where('lot', $lot);
        }

        $query->where('whs_id', $whs_id);

        //$this->model->filterData($query);

        return (int)$query->sum($get);
    }

    public function getUserInfo()
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();

        return $userInfo;
    }


    public function updateAllocatedOrderInventory($whs_id, $odrId, $odrSts)
    {
        $field = 'allocated_qty';
        if ($odrSts === 'PK' || $odrSts === 'PPK') {
            $odrDtls = OrderDtl::leftJoin('odr_cartons', 'odr_dtl.odr_dtl_id', '=', 'odr_cartons.odr_dtl_id')
                ->where('odr_dtl.odr_id', $odrId)
                ->select([
                    'odr_dtl.item_id',
                    'odr_dtl.lot',
                    'odr_dtl.alloc_qty',
                    DB::raw('COALESCE(SUM(odr_cartons.piece_qty), 0) AS picked_qty')
                ])
                ->get()->toArray();

            //update invt_smr
            foreach ($odrDtls as $odrDtl) {
                $updateCondition = [];

                $remainAllocatedQTY = $odrDtl['alloc_qty'] - $odrDtl['picked_qty'];

                $updateCondition['allocated_qty'] = DB::raw("allocated_qty - " . $remainAllocatedQTY);
                $updateCondition['avail'] = DB::raw("avail + " . $remainAllocatedQTY);

                $this->updateWhere($updateCondition, [
                    'whs_id'  => $whs_id,
                    'item_id' => $odrDtl['item_id'],
                    'lot'     => $odrDtl['lot']
                ]);
            }

            return true;
        }


        $sqlStr = sprintf(" `%s` -
            COALESCE(
                ( SELECT SUM(odr_dtl.alloc_qty)
                FROM odr_dtl
                WHERE invt_smr.item_id = odr_dtl.item_id
                    AND odr_dtl.lot = invt_smr.lot
                    AND odr_dtl.odr_id = %s
                    AND odr_dtl.deleted = 0
                )
            ,0)
            ", $field, $odrId);

        $availSql = sprintf(" `avail` +
            COALESCE(
                ( SELECT SUM(odr_dtl.alloc_qty)
                FROM odr_dtl
                WHERE invt_smr.item_id = odr_dtl.item_id
                    AND odr_dtl.lot = invt_smr.lot
                    AND odr_dtl.odr_id = %s
                    AND odr_dtl.deleted = 0
                )
            ,0)
            ", $odrId);

        $res = $this->updateWhere([
            'allocated_qty' => DB::raw($sqlStr),
            'avail'         => DB::raw($availSql)
        ], [
            'whs_id' => $whs_id
        ]);

        return $res;
    }

    public function getAvailBySku($sku)
    {
        //SELECT item_id, sku, size, lot, SUM(avail) AS available FROM invt_smr WHERE sku LIKE "%s1%" GROUP BY item_id, lot
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $query = $this->model
            ->where('sku', 'LIKE', "%{$sku}%")
            ->where('whs_id', $currentWH)
            ->select([
                "item_id",
                "sku",
                "size",
                "lot",
                DB::raw("SUM(avail) AS available")
            ])
            ->groupBy('item_id')
            ->groupBy('lot');

        return $query->get()->toArray();
    }

}
