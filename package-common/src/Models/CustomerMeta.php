<?php

namespace Seldat\Wms2\Models;


class CustomerMeta extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_meta';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cus_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'value',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted',
        'deleted_at',
        'qualifier'
    ];


}
