<?php

namespace Seldat\Wms2\Models;


class Labor extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'labor';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'category',
        'ref_id',
        'type',
        'amount',
        'urgency',
        'int_num',
        'int_id',
        'int_sts',
        'sts'

    ];

    public function invoiceHeader()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceHeader', 'inv_id', 'inv_id');
    }
}

