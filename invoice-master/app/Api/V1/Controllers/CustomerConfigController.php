<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Transformers\CustomerConfigTransformer;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class CustomerConfigController extends AbstractController
{
    protected $customerConfigModel;
    protected $transformer;

    /**
     * Init.
     *
     */
    public function __construct()
    {
        $this->customerConfigModel = new CustomerConfigModel();
        $this->transformer = new CustomerConfigTransformer();
    }
    
    /**
     * @SWG\Get(
     *     path="/customer/{type}/{cusId}",
     *     tags={"Customer Configuration"},
     *     summary="Get Customer Configuration",
     *     description="Get Customer Configuration",
     *     operationId="getCustomerConfig",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Type of Config",
     *         required=true,
     *         enum={
     *             "email", "other"
     *         },
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="cusId",
     *         in="path",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return ar",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */  
    /**
     * Display a listing of the resource.
     * @param IRequest $request
     * @return mixed
     */
    public function showConfig($type, $cusId)
    {
        try {
            // get customer config info
            $cusInfo = $this->customerConfigModel->getInfo($type, $cusId);

            return $this->response->item($cusInfo, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Put(
     *     path="/customer/{type}/{cusId}",
     *     tags={"Customer Configuration"},
     *     summary="Get Customer Configuration",
     *     description="Get Customer Configuration",
     *     operationId="getCustomerConfig",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Type of Config",
     *         required=true,
     *         enum={
     *             "email", "other"
     *         },
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="cusId",
     *         in="path",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Invoice",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */  
    /**
     * Store a newly created data in whs meta.
     *
     * @param  IRequest $request
     * @return Response
     */
    public function updateConfig(IRequest $request, $type, $cusId)
    {
        $input = $request->getParsedBody();
        try {
            // Store customer config info
            $this->customerConfigModel->addInfo($input, $type, $cusId);

            return $this->response->created(null, '{}');
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
