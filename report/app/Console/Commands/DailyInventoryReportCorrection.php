<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class DailyInventoryReportCorrection extends AbstractCommand
{
    protected $signature   = 'correction:daily-inventory-rpt {whsId} {cusId} {year?} {month?} {day?}';
    protected $description = 'correction:daily-inventory-rpt {whsId: } {cusId: } {year:  } {month: } {day:   }';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            if ($this->confirm('Do you wish to continue?')) {

                $this->logSuccess('start-daily-inventory-rpt');

                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $cartons = DB::table('cartons')
                    ->when((strtoupper($this->argument('whsId')) !== 'ALL' && is_numeric($this->argument('whsId'))), function ($q) {
                        return $q->where('cartons.whs_id', '=', $this->argument('whsId'));
                    })
                    ->when((strtoupper($this->argument('cusId')) !== 'ALL' && is_numeric($this->argument('cusId'))), function ($q) {
                        return $q->where('cus_id', '=', $this->argument('cusId'));
                    })
                    ->oldest()->first(['created_at']);

                $year = ($this->argument('year') >= date('Y', $cartons['created_at'] ?? time())) ? $this->argument('year') : date('Y', $cartons['created_at'] ?? time());

                $month = $this->argument('month') ?: date('m', $cartons['created_at'] ?? time());

                $day = $this->argument('day') ?: '1';

                $list = $this->getList($year, $month, $day);

                $customerWhss = DB::table('customer_warehouse AS cw')
                    ->select('cw.cus_id', 'cw.whs_id')
                    ->join('customer as c', 'c.cus_id', '=', 'cw.cus_id')
                    ->where("c.deleted", 0)
                    ->where("cw.deleted", 0)
                    ->when((strtoupper($this->argument('whsId')) !== 'ALL' && is_numeric($this->argument('whsId'))), function ($q) {
                        return $q->where('cw.whs_id', '=', $this->argument('whsId'));
                    })
                    ->when((strtoupper($this->argument('cusId')) !== 'ALL' && is_numeric($this->argument('cusId'))), function ($q) {
                        return $q->where('cw.cus_id', '=', $this->argument('cusId'));
                    })
                    ->get();

                if ($customerWhss) {
                    foreach ($customerWhss as $cusWhs) {
                        $cusId = $cusWhs['cus_id'];
                        $whsId = $cusWhs['whs_id'];
                        foreach ($list as $value) {
                            $today     = $value['today'];
                            $yesterday = $value['yesterday'];
                            DB::statement("CALL update_daily_inv_rpt({$whsId}, {$cusId}, {$today})");
                            $this->insertDailyInventoryReport($cusId, $whsId, $today, $yesterday);
                        }
                    }
                }

                $this->logSuccess('daily-inventory-rpt');
            }
        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }

    }

    private function insertDailyInventoryReport($cusId, $whsId, $today, $yesterday)
    {
        return DB::transaction(function () use ($cusId, $whsId, $today, $yesterday) {
            DB::table('daily_inv_rpt as i')->select(
                'i.whs_id',
                'i.cus_id',
                'i.item_id',
                'i.description',
                'i.sku',
                'i.size',
                'i.color',
                'i.pack',
                'i.uom',
                'i.lot',
                'i.is_rack',
                DB::raw($today . ' as inv_dt'),
                'i.cur_ctns as init_ctns',
                'i.cur_qty as init_qty',
                'i.cur_plt as init_plt',
                'i.cur_ctns',
                'i.cur_qty',
                'i.cur_plt',
                'i.cur_volume',
                DB::raw(time() . ' as created_at'),
                DB::raw(time() . ' as updated_at')
            )
                ->where('i.whs_id', '=', $whsId)
                ->where('i.cus_id', '=', $cusId)
                ->where('i.inv_dt', '=', $yesterday)
                ->whereNotExists(function ($q) use ($today) {
                    $q->select(DB::raw(1))->from('daily_inv_rpt as c')
                        ->whereRaw('i.whs_id = c.whs_id AND i.item_id = c.item_id AND i.lot = c.lot AND i.is_rack = c.is_rack')
                        ->where('c.inv_dt', '=', $today);
                })
                ->chunk(100, function ($inventories) {
                    return DB::table('daily_inv_rpt')->insert($inventories);
                });
        });
    }

    private function getList(int $year, int $month, int $day): array
    {
        $dayInMonth   = $this->days_in_month($month, $year);
        $dayCurrent   = ($dayInMonth < date('d')) ? $dayInMonth : date('d');
        $monthCurrent = ($year < date('Y')) ? 12 : date('m');
        for ($m = $month; $m <= $monthCurrent; $m++) {
            for ($d = $day; $d <= $dayCurrent; $d++) {
                $list[] = [
                    'today'     => strtotime($year . '-' . sprintf('%1$02d', $m) . '-' . sprintf('%1$02d', $d)),
                    'yesterday' => strtotime($year . '-' . sprintf('%1$02d', $m) . '-' . sprintf('%1$02d', ($d > 1) ? ($d - 1) : $d)),
                ];
            }
        }

        return $list ?? [];
    }

    private function days_in_month($month, $year): int
    {
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }
}
