<?php
/**
 * Created by PhpStorm.
 *
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderDtlModel
 *
 * @package App\Api\V1\Models
 */
class OrderDtlModel extends AbstractModel
{
    /**
     * OrderDtlModel constructor.
     *
     * @param OrderDtl|null $model
     */
    public function __construct(OrderDtl $model = null)
    {
        $this->model = ($model) ?: new OrderDtl();
    }

    
    public function invReport($warehouseId, $params, $limit = 20, $export = false)
    {
        $query = $this->make();
        $query->join('odr_hdr', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id');
        $query->join('customer', 'odr_hdr.cus_id', '=', 'customer.cus_id');
        $query->join('users', 'odr_hdr.updated_by', '=', 'users.user_id');
        $query->join('system_state', 'odr_hdr.ship_to_state', '=', 'system_state.sys_state_code');
        $query->join('item', 'item.item_id', '=', 'odr_dtl.item_id');

        $query->where('odr_hdr.whs_id', $warehouseId);
        $query->where('odr_hdr.odr_sts', 'SH');
        if (isset($params['cus_id']) && !empty($params['cus_id'])) {
            $query->where('odr_hdr.cus_id', intval($params['cus_id']));
        }
        if (isset($params['odr_type']) && !empty($params['odr_type'])) {
            $query->where('odr_hdr.odr_type', $params['odr_type']);
        }
        if (isset($params['odr_num']) && !empty($params['odr_num'])) {
            $query->where('odr_hdr.odr_num', $params['odr_num']);
        }

        if (isset($params['sku']) && !empty($params['sku'])) {
            $query->where('odr_dtl.sku', $params['sku']);
        }
        if (isset($params['item_id']) && !empty($params['item_id'])) {
            $query->where('odr_dtl.item_id', $params['item_id']);
        }
        if (isset($params['lot']) && !empty($params['lot'])) {
            $query->where('odr_dtl.lot', $params['lot']);
        }

        //search according to from date to date
        if (!empty($params['ship_dt_from'])) {
            $query->where('odr_hdr.shipped_dt', ">=", strtotime($params['ship_dt_from'] ));
        }

        if (!empty($params['ship_dt_to'])) {
            $query->where('odr_hdr.shipped_dt', "<", strtotime($params['ship_dt_to'] . "+1 day"));
        }

        $query->select([
            'customer.cus_name',
            'customer.cus_code',
            'odr_hdr.odr_num',
            'odr_dtl.item_id',
            'odr_dtl.sku',
            'odr_dtl.size',
            'odr_dtl.color',
            'odr_dtl.lot',
            'odr_dtl.cus_upc',
            \DB::raw('ROUND(SUM(odr_dtl.picked_qty)) AS piece_qty'),
            'odr_hdr.ship_by_dt',
            'users.first_name',
            'users.last_name',
            'odr_hdr.shipped_dt',
            'odr_dtl.pack',
            'odr_hdr.odr_type',
            'odr_hdr.cus_odr_num',
            'odr_hdr.cus_po',
            'odr_hdr.ship_to_name',
            'odr_hdr.carrier',
            'odr_hdr.ship_to_add_1',
            'odr_hdr.ship_to_state',
            'system_state.sys_state_name',
            'odr_hdr.ship_to_city',
            'odr_hdr.ship_to_zip',
            'odr_hdr.act_cancel_dt',

            'item.length',
            'item.width',
            'item.height',
            DB::raw('(ROUND( (SUM(odr_dtl.picked_qty)/odr_dtl.pack) * (item.length*item.width*item.height)/1728, 2 )) as cube'),

        ]);
        $query->where('odr_dtl.picked_qty','>', 0);
        $query->groupBy('odr_dtl.item_id', 'odr_dtl.lot', 'odr_hdr.odr_num');
        
        $query->orderBy('odr_hdr.ship_by_dt', 'DESC');

        if ($export) {
            return $query->get();
        } else {
            return $query->paginate($limit);
        }
    }

}
