<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Utils\SystemBug;

class StorageReportCommand extends AbstractCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage-report {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Storage Report Daily';

    public function handle()
    {
        $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        if ($this->argument('date')) {
            $date = Carbon::createFromFormat('Y-m-d', $this->argument('date'));
        } else {
            $date = Carbon::yesterday();
        }

        $dateToString = $date->toDateString();
        $today = date('Y-m-d');
        if ($dateToString > $today)
            return $output->writeln("<error>Data Error</error>");

        try {
            do {
                $cartons = $this->getStorageReportByDate($dateToString);
                $i = $u = 0;
                if (!empty($cartons)) {
                    foreach ($cartons as $carton) {
                        $whs_id=$carton['whs_id'];
                        $cus_id=$carton['cus_id'];
                        $mix=$this->countMixedLocation($cus_id,$whs_id,$dateToString,$carton['loc_id']);
                        $where = [
                            'whs_id' => $whs_id,
                            'cus_id' => $cus_id,
                            'date' => $dateToString
                        ];
                        if (DB::table('strg_rpt')->where($where)->exists()) {
                            DB::table('strg_rpt')->where($where)->update([
                                'rack' => $carton['rack']/*-$mix['RAC']*/,
                                'bin' => $carton['bin']/*-$mix['BIN']*/,
                                'shelf' => $carton['shelf']/*-$mix['SHE']*/,
                                'mix_rack' => $mix['RAC'],
                                'mix_bin' => $mix['BIN'],
                                'mix_shelf' =>$mix['SHE'],
                            ]);
                            $u++;
                        } else {
                            DB::table('strg_rpt')->insert([
                                'whs_id' => $whs_id,
                                'cus_id' => $cus_id,
                                'date' => $dateToString,
                                'rack' => $carton['rack']/*-$mix['RAC']*/,
                                'bin' => $carton['bin']/*-$mix['BIN']*/,
                                'shelf' => $carton['shelf']/*-$mix['SHE']*/,
                                'mix_rack' => $mix['RAC'],
                                'mix_bin' => $mix['BIN'],
                                'mix_shelf' =>  $mix['SHE'],
                                'created_at' => time()
                            ]);
                            $i++;
                        }
                        $output->writeln("<info>Customer ID: $cus_id </info>");
                    }
                }
                $output->writeln("<info>Storage Report Daily. Date: $dateToString, Insert: $i, Update: $u </info>");
                Log::info('Storage Report Daily. Date: ' . $dateToString . ', Insert: ' . $i . ', Update: ' . $u);
                $dateToString = $date->addDay()->toDateString();
            } while ($dateToString < $today);
        } catch (\Exception $e) {
            $output->writeln("<info>".$e->getMessage().'. Trace: '.$e->getTraceAsString()."</info>");
            SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
            Log::error($e->getMessage());
        }
    }

    protected function countMixedLocation($cus_id,$whs_id,$date,$loc_id)
    {
        $mix=[
            'RAC'=>0,
            'BIN'=>0,
            'SHE'=>0,
        ];
        if(empty($loc_id))
            return $mix;
        $query='
            SELECT GROUP_CONCAT(DISTINCT loc_id) AS loc_id
            FROM cartons FORCE INDEX(loc_id)
            WHERE whs_id=1 AND cus_id<>'.$cus_id.' AND deleted=0
            AND loc_id IN ('.$loc_id.')
            AND cartons.ctn_sts IN ("AC","RG","LK")
            AND '.$this->_time($date);
        $results  = DB::select(DB::raw($query));
        if(empty($results))
            return $mix;
        $result_loc_id=$results[0]['loc_id']??'';
        if(empty($result_loc_id))
            return $mix;
        $query='
SELECT spc_hdl_code,COUNT(1) AS cnt
FROM `location`
WHERE loc_id IN ('.$result_loc_id.') AND deleted=0
GROUP BY location.spc_hdl_code
';
        $results=DB::select(DB::raw($query));
        if(empty($results))
            return $mix;
        foreach ($results as $value){
            $mix[$value['spc_hdl_code']]=$value['cnt'];
        }
        return $mix;
    }

    protected function _time($date){
        return 'IF (`cartons`.`ctn_sts` = "SH", `cartons`.`updated_at` >='.strtotime($date.' 00:00:00').',`cartons`.`updated_at` <='.strtotime($date.' 23:59:59').' ) AND cartons.updated_at <= '.  strtotime($date.' 23:59:59');
    }

    protected function getStorageReportByDate($date)
    {
        $query = DB::table('cartons')
        ->select([
            'cartons.whs_id',
            'cartons.cus_id',
            DB::raw('GROUP_CONCAT(DISTINCT cartons.loc_id) AS loc_id'),
            DB::raw('COUNT(DISTINCT CASE WHEN location.spc_hdl_code = "RAC" THEN location.loc_id ELSE NULL END ) AS rack'),
            DB::raw('COUNT(DISTINCT CASE WHEN location.spc_hdl_code = "BIN" THEN location.loc_id ELSE NULL END ) AS bin'),
            DB::raw('COUNT(DISTINCT CASE WHEN location.spc_hdl_code = "SHE" THEN location.loc_id ELSE NULL END ) AS shelf'),
        ])
        ->join('location','location.loc_id', '=', 'cartons.loc_id')
        ->where('location.deleted', 0)
        ->where('cartons.deleted', 0)
        ->whereIn('cartons.ctn_sts', ['AC', 'RG', 'LK'])
        //->whereNull('location.parent_id')
        ->whereRaw($this->_time($date))
        ->groupBy([
            'cartons.whs_id',
            'cartons.cus_id',
        ]);
        return $query->get();
    }
}