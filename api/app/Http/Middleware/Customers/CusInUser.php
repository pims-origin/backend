<?php
namespace App\Http\Middleware\Customers;

use Illuminate\Database\Eloquent\Model;

class CusInUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_in_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = ['user_id', 'cus_id', 'whs_id'];

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'cus_id',
        'whs_id',
    ];
}
