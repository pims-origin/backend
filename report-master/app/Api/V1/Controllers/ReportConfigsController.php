<?php

    namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\ReportListTransformer;
use App\Api\V1\Validators\LogoValidator;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Seldat\Wms2\Models\ReportSystem;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\Api\V1\Models\ReportConfigsModel;
use App\Api\V1\Transformers\ReportConfigsTransformer;
use App\Api\V1\Transformers\ReportDetailTotalsTransformer;
use App\Api\V1\Validators\ReportConfigsValidator;
use DB;
use Illuminate\Http\Response as IlluminateResponse;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\ReportConfigs;
use Seldat\Wms2\Utils\Message;
use mPDF;

class ReportConfigsController extends AbstractController
{
    /**
     * @var $validator
     */
    protected $validator;

    /**
     * @var $reportConfigsModel
     */
    protected $reportConfigsModel;

    /**
     * ReportConfigsController constructor.
     *
     * @param ReportConfigsValidator $validator
     */
    public function __construct(ReportConfigsValidator $validator, ReportConfigsModel $reportConfigsModel)
    {
        $this->validator = $validator;
        $this->reportConfigsModel = $reportConfigsModel;
    }

    /**
     * @param Request $request
     * @param ReportConfigsModel $reportConfigsModel
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function storeReportConfigs(Request $request, ReportConfigsModel $reportConfigsModel)
    {
        // Get input value
        $input = $request->getParsedBody();

        //validate data input
        $this->validator->validate($input);
        $dataArrays = $input['data'];
        $arrNew = [];
        $arrDataDefault = [];
        foreach ($dataArrays as $key => $dataArray) {
            if (!empty($dataArray['new_column']) && $dataArray['new_column'] === true) {
                unset($dataArray['new_column']);
                $arrNew[$key] = $dataArray;
            }
            $arrDataDefault[$key] = $dataArray;
        }
        // data for create
        $dataConfigs = [
            'qualifier'  => array_get($input, 'qualifier', ''),
            'name'       => array_get($input, 'name', ''),
            'des'        => array_get($input, 'des', ''),
            'ref'        => array_get($input, 'ref', ''),
            'level'      => array_get($input, 'level', ''),
            'system'     => array_get($input, 'system', ''),
            'updated_at' => time(),
            'sts'        => array_get($input, 'sts', ''),
            'url'        => array_get($input, 'url', ''),
            'url_export' => array_get($input, 'url_export', '')
        ];

        try {
            DB::beginTransaction();
            $dataCheck = [
                'qualifier' => array_get($input, 'qualifier', ''),
                'system'    => array_get($input, 'system', ''),
                'level'     => array_get($input, 'level', ''),
                'ref'     => array_get($input, 'ref', ''),
            ];
            // check 2 params have exists
            if ($reportConfigsModel->getFirstWhere($dataCheck)) {
                $level = array_get($input, 'level', 1);
                // Check level is default
                if ($level === 1) {
                    $dataUpdates = [
                        'name'       => array_get($input, 'name', ''),
                        'des'        => array_get($input, 'des', ''),
                        'updated_at' => time(),
                        'sts'        => array_get($input, 'sts', ''),
                        'url'        => array_get($input, 'url', ''),
                        'url_export' => array_get($input, 'url_export', '')
                    ];
                    $dataUpdates['data'] = json_encode($arrDataDefault);
                    $reportConfigsModel->updateWhere($dataUpdates, $dataCheck);

                    $columnData = $reportConfigsModel->findWhere([
                        'qualifier' => array_get($input, 'qualifier', ''),
                        'system'    => array_get($input, 'system', ''),
                        'level'     => ['level', '!=', 1]
                    ])->toArray();

                    foreach ($columnData as $key => $value) {
                        $dataUpdates['data'] = json_encode(array_merge($arrNew, json_decode($value['data'], true)));
                        // Update all level
                        $reportConfigsModel->refreshModel();
                        $reportConfigsModel->updateWhere($dataUpdates, ['id' => $value['id']]);
                    }
                } else {
                    $dataConfigs['data'] = json_encode($input['data']);

                    //Update configs
                    $reportConfigsModel->refreshModel();
                    $reportConfigsModel->updateWhere($dataConfigs, [
                        'ref'       => array_get($input, 'ref', ''),
                        'qualifier' => array_get($input, 'qualifier', ''),
                        'level'     => array_get($input, 'level', ''),
                        'system'    => array_get($input, 'system', '')
                    ]);
                }
                $dataReturns = $reportConfigsModel->getFirstWhere([
                    'qualifier' => array_get($input, 'qualifier', ''),
                    'level'     => array_get($input, 'level', ''),
                    'system'    => array_get($input, 'system', '')
                ]);
            } else {
                $dataConfigs['data'] = json_encode($input['data']);

                //  Add configs
                $reportConfigsModel->refreshModel();
                $dataReturns = $reportConfigsModel->create($dataConfigs);
            }
            DB::commit();

            return $this->response->item($dataReturns, new ReportConfigsTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (ValidationHttpException $e) {
            DB::rollBack();
            throw new ValidationHttpException($e->getErrors());
        }
    }

    /**
     * @param Request $request
     * @param $qualifier
     * @param ReportConfigsModel $reportConfigsModel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function viewReportConfigs(Request $request, $qualifier, ReportConfigsModel $reportConfigsModel)
    {

        $arr = ['qualifier' => $qualifier];
        $input = $request->getParsedBody();

        if (isset($input['system']) && empty($reportConfigsModel->getFirstWhere(['system' => $input['system']]))) {
            return $this->response->errorBadRequest(sprintf("The %s is not existed!", "system"));
        }

        $data = $reportConfigsModel->getFirstWhere(['qualifier' => $qualifier]);

        if (empty($data)) {
            return $this->response->errorBadRequest(sprintf("The %s is not existed!", "qualifier"));
        }

        if (isset($input['ref']) && isset($input['level']) ) {
            if (empty($reportConfigsModel->getFirstWhere(['qualifier' => $qualifier, 'ref' => $input['ref'], 'level' => $input['level']]))) {
                $refLev = ['ref'  => 0, 'level' =>1];
            } else {
                $refLev = ['ref' => $input['ref'], 'level' => $input['level']];
            }
        }

        $dataQuery = array_merge($arr, $refLev);
        $data = $reportConfigsModel->getFirstWhere($dataQuery);

        if (empty($data)) {
            throw new \Exception("No results");
        }

        return $this->response->item($data, new ReportConfigsTransformer);
    }

    /**
     * @param Request $request
     * @param ReportConfigsModel $reportConfigsModel
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewReportDetailTotals(Request $request, ReportConfigsModel $reportConfigsModel)
    {
        $dataTableRef = ReportConfigs::select('qualifier', 'tbl_ref')->get();

        foreach ($dataTableRef as $key => $value) {
            if (!empty(trim($value['tbl_ref']))) {
                $dataTableRef[$key]['total'] = DB::table(trim($value["tbl_ref"]))->count();
            }
        }

        return $this->response->collection($dataTableRef, new ReportDetailTotalsTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
    }

    public function importReport(Request $request, ReportConfigsModel $reportConfigsModel)
    {
        $input = $request->getParsedBody();
        $fileTmp = $request->getUploadedFiles()['file'];
        $reportConfig = config('constants.report');
        $extFile = pathinfo($fileTmp->getClientFilename(), PATHINFO_EXTENSION);
        if (!in_array($extFile, $reportConfig['ext_file'])) {
            $collection = [
                'data' => [
                    'message' => "The extension file is not support!",
                    'code'    => '400',
                ]
            ];

            return $this->response->noContent()->setContent(['data' => $collection['data']])->setStatusCode($collection['data']['code']);
        }

        $filename = sprintf('%s.%s', uniqid(), $extFile);
        $storage_path = storage_path('files');
        // check exist storage path directory
        if (!file_exists($storage_path)) {
            mkdir($storage_path, 0777, true);
        }

        $file = storage_path('files/' . $filename);
        $fileTmp->moveTo($file);
        $loadFile = Excel::load($file);
        $headingExcel = $loadFile->get()->getHeading();
        $reportArr = $loadFile->toArray();
        $total = count($reportArr);

        //Check minimum row in file import
        if ($total == 0) {
            $collection = [
                'data' => [
                    'message' => sprintf("%s is empty", "File"),
                    'code'    => '400',
                ]
            ];

            return $this->response->noContent()->setContent(['data' => $collection['data']])->setStatusCode($collection['data']['code']);
        }

        try {
            set_time_limit(0);
            foreach ($reportArr as $index => $reportItem) {
                if ($this->checkRowEmpty($reportItem)) {
                    continue;
                }
                $reportArrHeading = $reportConfig['array_key'];
                $item = array_combine(array_keys($reportArrHeading), $reportItem);

                $key = $item['field_name'];
                $data[$key] = $item;
            }
            $data_encode = json_encode($data);

            $dataReportconfigs = [
                'qualifier'  => array_get($input, 'qualifier', ''),
                'ref'        => array_get($input, 'ref', ''),
                'data'       => $data_encode,
                'des'        => array_get($input, 'des', ''),
                'name'       => array_get($input, 'name', ''),
                'url'        => array_get($input, 'url', ''),
                'url_export' => array_get($input, 'url_export', ''),
                'level'      => 1,
                'system'     => "WMS360",
                'sts'        => 1,
                'tbl_ref'    => "NA",
                'updated_at' => time(),
            ];

            // check params have exists
            if ($reportConfigsModel->getFirstWhere(['qualifier' => $input['qualifier']])) {
                //Update configs
                DB::beginTransaction();
                $reportConfigsModel->updateWhere($dataReportconfigs, ['qualifier' => $input['qualifier']]);
                DB::commit();
                $collection = [
                    'data' => [
                        'message' => sprintf('Update %s Successfully.', 'Report Configs'),
                    ]
                ];

                return new IlluminateResponse($collection, 201);
            } else {
                //  Add configs
                DB::beginTransaction();
                $reportConfigsModel->refreshModel();
                $reportConfigsModel->create($dataReportconfigs);
                DB::commit();
                $collection = [
                    'data' => [
                        'message' => sprintf('Import %s Successfully.', 'Report Configs'),
                    ]
                ];

                return new IlluminateResponse($collection, 201);
            }
        } catch (\PDOException $e) {
            throw $e;
        }
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function checkRowEmpty($data)
    {
        foreach ($data as $key => $value) {
            if (!empty($value)) {
                return false;
            }
        }

        return true;
    }

    public function resetReportConfigs(Request $request, $qualifier, ReportConfigsModel $reportConfigsModel)
    {
        $input = $request->getParsedBody();
        if ($qualifier && !$reportConfigsModel->getFirstWhere(['qualifier' => $qualifier])) {
            return $this->response->errorBadRequest(sprintf("The %s is not existed!", "qualifier"));
        }
        if (isset($input['system']) && !$reportConfigsModel->getFirstWhere(['system' => $input['system']])) {
            return $this->response->errorBadRequest(sprintf("The %s is not existed!", "system"));
        }
        if (isset($input['ref']) && !$reportConfigsModel->getFirstWhere(['ref' => $input['ref']])) {
            return $this->response->errorBadRequest(sprintf("The %s is not existed!", "ref"));
        }
        if (isset($input['level']) && !$reportConfigsModel->getFirstWhere(['level' => $input['level']])) {
            return $this->response->errorBadRequest(sprintf("The %s is not existed!", "level"));
        }

        try {
            DB::beginTransaction();
            $reportConfigsModel->refreshModel();
            $reportReturnData = $reportConfigsModel->getFirstWhere(['qualifier' => $qualifier, 'system' => $input['system'], 'ref' => 0, 'level' => 0]);
            $reportConfigsModel->updateWhere(
                ['data' => $reportReturnData['data']],
                [
                    'qualifier' => $qualifier, 
                    'system' => $input['system'], 
                    'ref' => $input['ref'], 
                    'level' => $input['level']
                ]
            );
            DB::commit();
            $data = $reportConfigsModel->getFirstWhere(['qualifier' => $qualifier, 'system' => $input['system'], 'ref' => $input['ref'], 'level' => $input['level']]);

            return $this->response->item($data, new ReportConfigsTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED); 
        } catch (\PDOException $e) {
            throw $e;
        }
    }


    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function getReportTableNameList(
        Request $request
    ) {
        try {
            $data = $this->reportConfigsModel->getInfoReportTables();

            return ['data' => $data];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function createReport(Request $request)
    {
        $input = $request->getParsedBody();

        $qualifier      = array_get($input, 'report_qualifier');
        $tableRef       = array_get($input, 'table_name');
        $fields         = array_get($input, 'fields', []);
        if ( !$qualifier || !$tableRef || !$fields) {
            $msg = 'Report code, table name and fields is required!';
            throw new \Exception($msg);
        }

        $isExistReport  = $this->reportConfigsModel->getModel()
                            ->where([
                                'qualifier' => $qualifier,
                                'system'    => 'WMS360',
                                'ref'       => 0,
                                'level'     => 1
                            ])
                            ->count();

        if ( $isExistReport ) {
            $msg = 'Report qualifier existed in the system.';
            throw new \Exception($msg);
        }

        $fields = collect($fields);

        $fields->transform(function($field, $index) {
            $fieldName  = array_get($field, 'field_name', '');
            $name       = str_replace('_', ' ', $fieldName);
            $name       = ucwords($name);

            return [
                'show'              => true,
                'index'             => $index,
                'width'             => 100,
                'condition'         => '=',
                'data_type'         => array_get($field, 'data_type', ''),
                'field_name'        => $fieldName,
                'search_type'       => true,
                'display_name'      => $name,
                'default_display_name' => $name
            ];
        });

        $reportData                     = [];
        $reportData['filter']           = array_get($input, 'filter', []);
        $reportData['default_search']   = array_get($input, 'default_search', []);
        $reportData['aggregate']        = array_get($input, 'aggregate', []);
        $reportData['subtotal_label']   = array_get($input, 'subtotal_label', 'Sub total');
        $reportData['total_label']      = array_get($input, 'total_label', 'Total');
        $reportData['order_by']         = array_get($input, 'order_by', []);
        $reportData['group_by']         = array_get($input, 'group_by', []);
        $reportData['fields']           = $fields;

        $report = [
            'qualifier'     => $qualifier,
            'ref'           => 0,
            'level'         => 1,
            'system'        => array_get($input, 'report_system', 'WMS360'),
            'data'          => json_encode($reportData),
            'des'           => array_get($input, 'report_des', ''),
            'name'          => array_get($input, 'report_name', ''),
            'sts'           => 1,
            'tbl_ref'       => $tableRef,
            'url'           => '/report/' . $qualifier . '/show',
            'url_export'    => '/report/' . $qualifier . '/export',
        ];

        $result = $this->reportConfigsModel->getModel()
                    ->create($report);

        $report['level'] = 0;
        $reportReturn = $this->reportConfigsModel->getModel()
                    ->create($report);

        $data = [
            'msg'   => 'Create report successfully'
        ];

        return response(['data' => $data], 201);
    }

    public function updateFieldsReportConfigs(Request $request, string $qualifier)
    {
        $input  = $request->getParsedBody();
        $fields = array_get($input, 'fields', []);
        $ref    = array_get($input, 'ref');
        $level  = array_get($input, 'level');
        $system = array_get($input, 'system');

        if ( !$fields  || !$level || !$system || !is_numeric($ref) ) {
            $msg = 'Fields, ref, level and system is required';
            throw new \Exception($msg);
        }

        $report = $this->reportConfigsModel->getModel()
                        ->where('qualifier', $qualifier)
                        ->where([
                            'level'     => $level,
                            'ref'       => $ref,
                            'system'    => $system
                        ])
                        ->first();

        if ( !$report ) {
            $msg = 'Report is not exists';
            throw new \Exception($msg);
        }

        $data = json_decode($report->data);
        $data->fields = array_values($fields);
        $report->update([
            'data'  => json_encode($data)
        ]);

        $data = [
            'msg'   => 'Update report successfully'
        ];

        return response(['data' => $data], 200);
    }

    public function updateSearchReportConfigs(Request $request, string $qualifier)
    {
        $input  = $request->getParsedBody();
        $search = array_get($input, 'search', []);
        $ref    = array_get($input, 'ref');
        $level  = array_get($input, 'level');
        $system = array_get($input, 'system');

        if ( !$search  || !$level || !$system || !is_numeric($ref) ) {
            $msg = 'Search, ref, level and system is required';
            throw new \Exception($msg);
        }

        $report = $this->reportConfigsModel->getModel()
                        ->where('qualifier', $qualifier)
                        ->where([
                            'level'     => $level,
                            'ref'       => $ref,
                            'system'    => $system
                        ])
                        ->first();

        if ( !$report ) {
            $msg = 'Report is not exists';
            throw new \Exception($msg);
        }

        $data = json_decode($report->data);
        $data->default_search = array_values($search);
        $report->update([
            'data'  => json_encode($data)
        ]);

        $data = [
            'msg'   => 'Update report successfully'
        ];

        return response(['data' => $data], 200);
    }


    public function updatePdfConfig(Request $request, string $qualifier)
    {
        $input  = $request->getParsedBody();
        $pdf_config = array_get($input, 'pdf', []);
        $ref    = array_get($input, 'ref');
        $level  = array_get($input, 'level');
        $system = array_get($input, 'system');

        if ( !$pdf_config  || !$level || !$system || !is_numeric($ref) ) {
            $msg = 'Fields, ref, level and system is required';
            throw new \Exception($msg);
        }

        $report = $this->reportConfigsModel->getModel()
            ->where('qualifier', $qualifier)
            ->where([
                'level'     => 1,
                'ref'       => 0,
                'system'    => 'WMS360'
            ])
            ->first();

        if ( !$report ) {
            $msg = 'Report is not exists';
            throw new \Exception($msg);
        }

        $data = json_decode($report->data);
        $data->pdf = $pdf_config;
        $report->update([
            'data'  => json_encode($data)
        ]);

        $data = [
            'msg'   => 'Update pdf setting successfully'
        ];

        return response(['data' => $data], 200);
    }

    public function uploadLogo(Request $request, $idSystem)
    {
        $input = $request->getParsedBody();
        $name_logo = array_get($input, 'name_logo', '');
        $fileTmp = $request->getUploadedFiles()['file'];

        $logoValidate = new LogoValidator();
        $logoValidate->validate($input);
        if (empty($fileTmp)) {
            throw new Exception('Expected a image file');
        }
        $logoConfig = config('constants.logo');
        $extFile = strtolower(pathinfo($fileTmp->getClientFilename(), PATHINFO_EXTENSION));
        if (! in_array($extFile, $logoConfig['ext_file'])) {
            $collection = [
                'data' => [
                    'message' => 'The extension file is not support!',
                    'code'    => '400',
                ]
            ];
            return $this->response->noContent()
                ->setContent(['data' => $collection['data']])->setStatusCode($collection['data']['code']);
        }

        $filename = sprintf('%s.%s', uniqid(), $extFile);
        $public_path = public_path('files/'. $idSystem);
        $url_logo = "/files/" . $idSystem . "/" . $filename;
        $sys = 'WMS360';
        $file = new Filesystem();
        $file->cleanDirectory($public_path);
        // check exist storage path directory
        if (!file_exists($public_path)) {
            mkdir($public_path, 0777, true);
        }
        $rpt_sys = new ReportSystem();

        $file = public_path('files/' . $idSystem . "/" . $filename);
        $fileTmp->moveTo($file);
        $data = $rpt_sys->getModel()
            ->where('sys_rpt_id', $idSystem)
            ->update(
                [
                    'name_logo' => $name_logo,
                    'url_logo'  => base64_encode($url_logo),
                    'sys'       =>  $sys
                ]
            );
        if($data) {
            return response(['data' => [
                'message' => 'Upload logo successfully'
            ]], 200);
        } else {
            return response(['data' => [
                'message' => 'Upload logo failed'
            ]], 400);
        }
    }

    public function getLogo(Request $request, $idSystem)
    {
        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $rpt_sys = new ReportSystem();
            $data = $rpt_sys->getModel()->where('sys_rpt_id', $idSystem)->first();
            return ['data' => $data];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_REPORT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function createLogo(Request $request)
    {
        $input = $request->getParsedBody();
        $name_logo = array_get($input, 'name_logo', '');
        $fileTmp = $request->getUploadedFiles()['file'];

        $logoValidate = new LogoValidator();
        $logoValidate->validate($input);
        if (empty($fileTmp)) {
            throw new Exception('Expected a image file');
        }
        $logoConfig = config('constants.logo');
        $extFile = strtolower(pathinfo($fileTmp->getClientFilename(), PATHINFO_EXTENSION));
        if (! in_array($extFile, $logoConfig['ext_file'])) {
            $collection = [
                'data' => [
                    'message' => 'The extension file is not support!',
                    'code'    => '400',
                ]
            ];
            return $this->response->noContent()
                ->setContent(['data' => $collection['data']])->setStatusCode($collection['data']['code']);
        }

        $filename = sprintf('%s.%s', uniqid(), $extFile);
        $rpt_sys = new ReportSystem();
        $sys = 'WMS360';
        $data = $rpt_sys->getModel()
            ->create(
                [
                    'name_logo' => $name_logo,
                    'sys'       =>  $sys
                ]
            );
        if(!$data){
            return response(['data' => [
                'message' => 'Upload logo failed'
            ]], 400);
        }
        $public_path = public_path('files/'. $data['sys_rpt_id']);
        $url_logo =  "/files/" . $data['sys_rpt_id'] . "/" . $filename;

        $file = new Filesystem();
        $file->cleanDirectory($public_path);
        // check exist storage path directory
        if (!file_exists($public_path)) {
            mkdir($public_path, 0777, true);
        }

        $file = public_path('files/' . $data['sys_rpt_id'] . "/" . $filename);
        $fileTmp->moveTo($file);

        $data = $rpt_sys->getModel()
            ->where('sys_rpt_id', $data['sys_rpt_id'])
            ->update(
                [
                    'url_logo'  => base64_encode($url_logo),
                ]
            );
        if($data) {
            return response(['data' => [
                'message' => 'Upload logo successfully'
            ]], 200);
        } else {
            return response(['data' => [
                'message' => 'Upload logo failed'
            ]], 400);
        }
    }

    public function getListLogo(Request $request)
    {
        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $rpt_sys = new ReportSystem();
            $data = $rpt_sys->getModel()->get();
            return ['data' => $data];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_REPORT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function deleteLogo(Request $request, $idSystem) {
        $input = $request->getParsedBody();
        $file = new Filesystem();
        $public_path = public_path('files/'. $idSystem);
        $file->cleanDirectory($public_path);
        $rpt_sys = new ReportSystem();
        $data = $rpt_sys->getModel()->where('sys_rpt_id', $idSystem)->first();
        $data->delete();
        return response(['data' => [
            'message' => 'Deleted logo successfully'
        ]], 200);
    }

    public function view($url){
        $url = base64_decode($url);
        $filePath = public_path($url);
        header('Content-type: ' . $this->mimeType($filePath));
        readfile($filePath);
    }
    private function mimeType($path) {
        preg_match("|\.([a-z0-9]{2,4})$|i", $path, $fileSuffix);
        switch(strtolower($fileSuffix[1])) {
            case 'js' :
                return 'application/x-javascript';
            case 'json' :
                return 'application/json';
            case 'jpg' :
            case 'jpeg' :
            case 'jpe' :
                return 'image/jpg';
            case 'png' :
            case 'gif' :
            case 'bmp' :
            case 'tiff' :
                return 'image/'.strtolower($fileSuffix[1]);
            case 'css' :
                return 'text/css';
            case 'xml' :
                return 'application/xml';
            case 'doc' :
                return 'application/msword';
            case 'docx':
                return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            case 'xlsx':
                return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            case 'xls' :
            case 'xlt' :
            case 'xlm' :
            case 'xld' :
            case 'xla' :
            case 'xlc' :
            case 'xlw' :
            case 'xll' :
                return 'application/vnd.ms-excel';
            case 'ppt' :
            case 'pps' :
                return 'application/vnd.ms-powerpoint';
            case 'rtf' :
                return 'application/rtf';
            case 'pdf' :
                return 'application/pdf';
            case 'html' :
            case 'htm' :
            case 'php' :
                return 'text/html';
            case 'txt' :
                return 'text/plain';
            case 'mpeg' :
            case 'mpg' :
            case 'mpe' :
                return 'video/mpeg';
            case 'mp3' :
                return 'audio/mpeg3';
            case 'wav' :
                return 'audio/wav';
            case 'aiff' :
            case 'aif' :
                return 'audio/aiff';
            case 'avi' :
                return 'video/msvideo';
            case 'wmv' :
                return 'video/x-ms-wmv';
            case 'mov' :
                return 'video/quicktime';
            case 'zip' :
                return 'application/zip';
            case 'tar' :
                return 'application/x-tar';
            case 'swf' :
                return 'application/x-shockwave-flash';
            default :
                if(function_exists('mime_content_type')) {
                    $fileSuffix = mime_content_type($path);
                }
                return 'unknown/' . trim($fileSuffix[0], '.');
        }
    }

    public function getReportList(ReportListTransformer $reportListTransformer){
        try {
            $userInfo = new \Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
            $listMyReport = $this->reportConfigsModel->getModel()->where("level", 1)
                                                        ->get();
            return $this->response->collection($listMyReport, new $reportListTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getMyReportList(ReportListTransformer $reportListTransformer){
        try {
            $userInfo = new \Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
            $listMyReport = $this->reportConfigsModel->getModel()
                ->where('created_by',$userId)
                ->get();
            return $this->response->collection($listMyReport, new $reportListTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}