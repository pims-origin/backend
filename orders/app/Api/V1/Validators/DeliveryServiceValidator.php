<?php

namespace App\Api\V1\Validators;


class DeliveryServiceValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'ds_key' => 'required|max:3',
            'ds_value' => 'required|max:50'
        ];
    }
}
