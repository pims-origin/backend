<?php

namespace Seldat\Wms2\Models;


class DailyInventorySummary extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'daily_inv_rpt';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'item_id',
        'description',
        'sku',
        'color',
        'size',
        'pack',
        'lot',
        'uom',
        'is_rack',
        'inv_dt',
        'init_ctns',
        'init_qty',
        'init_plt',
        'in_ctns',
        'in_qty',
        'in_plt',
        'other_ctns',
        'other_qty',
        'other_plt',
        'out_ctns',
        'out_qty',
        'out_plt',
        'cur_ctns',
        'cur_qty',
        'cur_plt',
        'created_at',
        'update_at'

    ];

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

}
