<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 09-May-16
 * Time: 10:24
 */

namespace App\Api\V1\Transformers;

use App\MenuGroup;
use League\Fractal\TransformerAbstract;

class MenuGroupTransformer extends TransformerAbstract
{
    /**
     * @param MenuGroup $menuGroup
     *
     * @return array
     */
    public function transform(MenuGroup $menuGroup)
    {
        return [
            'menu_group_id' => $menuGroup->menu_group_id,
            'name'          => $menuGroup->name,
            'description'   => $menuGroup->description
        ];
    }
}
