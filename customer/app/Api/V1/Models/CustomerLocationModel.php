<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:32
 */

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\Location;
use Illuminate\Database\Query\Builder;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class CustomerLocationModel extends AbstractModel
{
    /**
     * @var Builder
     */
    protected $model;

    /**
     * EloquentAddress constructor.
     *
     * @param Location $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    public function search($zoneIds, $attributes, $with, $limit)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!is_array($zoneIds)) {
            $zoneIds = [$zoneIds];
        }
        $query->whereIn('loc_zone_id', $zoneIds);
        $query->whereNull('parent_id');
        $this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->orderBy('created_at', 'DESC')->paginate($limit);

        return $models;
    }
}
