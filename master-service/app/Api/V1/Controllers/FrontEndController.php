<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\SystemService;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Models\UserService;
use App\Api\V1\Validators\ForgotPasswordValidator;
use App\Api\V1\Validators\SetupPasswordValidator;
use App\Api\V1\Validators\ResetPasswordValidator;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FrontEndController extends Controller
{
    /**
     * @var UserService
     */
    protected $service;

    /**
     * @var UserModel
     */
    protected $model;

    /**
     * @var SystemService
     */
    protected $system;

    /**
     * FrontEndController constructor.
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        $this->service = new UserService($request);
        $this->system = new SystemService($request);
        $this->model = new UserModel();
    }

    /**
     * @param IRequest $request
     * @return string
     */
    public function forgotPassword(IRequest $request)
    {
        $body = $request->getParsedBody();

        // validate input
        (new ForgotPasswordValidator)->validate($body);

        //get user information by email
        $user = $this->model->getUserByEmail($body['email']);

        if (! $user) {
            throw new BadRequestHttpException('Email not found');
        }

        //create token
        $token = $this->model->makeResetPasswordToken($user);

        //generate reset password url
        $url = $body['reset_password_url'] . '?token=' . $token;

        try {
            $this->system->sendForgetPasswordEmail($user, $url);
        } catch (\Exception $e){
            throw new BadRequestHttpException($e->getMessage());
        }

        return [
            'data' => ['forgot_password' => 'OK']
        ];
    }
    
    /**
     * Run when create user or signup
     * @param IRequest $request
     * @return string
     */
    public function setupPassword(IRequest $request)
    {
        $body = $request->getParsedBody();

        // validate input
        (new SetupPasswordValidator)->validate($body);

        //get user information by email
        $user = $this->model->getUserByEmail($body['email']);

        if (! $user) {
            throw new BadRequestHttpException('Email not found');
        }

        //create token
        $token = $this->model->makeResetPasswordToken($user);

        //generate reset password url
        $url = $body['setup_password_url'] . '?token=' . $token;

        $this->system->sendSetupPasswordEmail($user, $url);

        return [
            'data' => ['setup_password' => 'OK']
        ];
    }

    /**
     * @param IRequest $request
     * @return array
     */
    public function resetPassword(IRequest $request)
    {
        $body = $request->getParsedBody();

        // validate input
        (new ResetPasswordValidator)->validate($body);

        // Validate valid token
        $tokenValidation = $this->model->validateTokenByEmail($body['token']);

        if (empty($tokenValidation)) {
            throw new BadRequestHttpException('Invalid Token Or Expired');
        }

        $userId = $tokenValidation->user_id;

        // Update password
        $response = $this->service->updatePassword($userId, $body);

        if ($response->getStatusCode() == Response::HTTP_OK) {
            // reset expired time
            $this->model->updateExpiredTime($userId);
        }

        return $response;
    }

    public function checkResetPasswordToken($token)
    {
        $tokenValidation = $this->model->validateTokenByEmail($token);

        if (empty($tokenValidation)) {
            throw new BadRequestHttpException('Invalid Token Or Expired');
        }

        return [
            'data' => ['token' => 'OK']
        ];
    }
}
