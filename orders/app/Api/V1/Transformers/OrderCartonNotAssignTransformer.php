<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/9/2018
 * Time: 5:49 PM
 */
namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\OrderCarton;
use League\Fractal\TransformerAbstract;

/**
 * Class OrderHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderCartonNotAssignTransformer extends TransformerAbstract
{
    /**
     * @param OrderCarton $orderCtn
     *
     * @return array
     */
    public function transform(OrderCarton $orderCtn)
    {
        $pickedAt = new \DateTime();
        $pickedAt->setTimestamp($orderCtn->picked_at);

        return [
            "wv_num"            => object_get($orderCtn, 'wv_num', ''),
            "cus_name"          => object_get($orderCtn, 'cus_name', ''),
            "ship_dt"           => object_get($orderCtn, 'ship_dt', ''),
            "sku"               => object_get($orderCtn, 'sku', ''),
            "loc_code"          => object_get($orderCtn, 'loc_code', ''),
            "ctn_num"           => object_get($orderCtn, 'ctn_num', ''),
            "ctn_rfid"          => object_get($orderCtn, 'ctn_rfid', ''),
            'description'       => object_get($orderCtn, 'description', ''),
            "lot"               => object_get($orderCtn, 'lot', ''),
            "uom_name"          => object_get($orderCtn, 'uom_name', ''),
            "pack"              => object_get($orderCtn, 'pack', ''),
            "ctn_id"            => object_get($orderCtn, 'ctn_id', ''),
            "piece_qty"         => object_get($orderCtn, 'piece_qty', ''),
            "plt_num"           => object_get($orderCtn, 'plt_num', ''),
            "picked_at"         => $pickedAt->format('Y-m-d H:i:s'),
            "picked_by"         => object_get($orderCtn, 'picked_by', ''),
        ];
    }
}