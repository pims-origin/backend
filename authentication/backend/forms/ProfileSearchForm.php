<?php

namespace backend\forms;

class ProfileSearchForm extends Form
{
    const ALLOW_SELECT_FIELDS = [
        'usr_dpm_id',
        'user_id',
        'username',
        'email',
        'type',
        'first_name',
        'last_name',
        'created_at',
        'updated_at',
        'status'
    ];

    public $type;
    public $username;
    public $first_name;
    public $last_name;
    public $email;
    public $status;
    public $limit;
    public $sort;


    public function rules()
    {
        return [
            [['username'], 'string', 'max' => 50],
            [['first_name', 'last_name'], 'string', 'max' => 35],
            [['email'], 'string', 'max' => 35],
            ['status', 'string', 'max' => 2],
            ['type', 'string', 'max' => 2]
        ];
    }
}
