<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class DashboardService extends BaseService
{
    public function asnDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/asns?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function odrDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/orders?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function inprocessDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/order-inprocess?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function putawayDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/putaway?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function productAdminDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/product-admin?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function asnAdminDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/asn-admin?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function OccupancyPCTDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/occupancy-pct?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function occupancyPCTDashboardStatus($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/occupancy-pct-status?' . $queryStr;
        $response = $this->client->get($uri);
        return $response;
    }

    public function odrAdminDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/order-admin?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function inprocessAdminDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/order-inprocess-admin?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function putawayAdminDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/putaway-admin?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function wavepickDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/wavepick?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function goodReceiptDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/goods-receipt?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

	public function putawayReceivedDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/putaway-received?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function zoneCapaticyAdminDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/zoneCapacity-admin?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function wavePickStatisticDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/wave-pick?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function roomAdminDashboard($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/room-admin?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function dashboardAlertBar($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'dashboard/alert-bar?' . $queryStr;

        $response = $this->client->get($uri);

        return $response;
    }
}