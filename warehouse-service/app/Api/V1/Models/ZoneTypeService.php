<?php
namespace App\Api\V1\Models;


use App\Api\V1\Traits\ZoneTypeLocTypeService;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class ZoneTypeService extends BaseService
{
    use ZoneTypeLocTypeService;

    public function getDetail($id)
    {
        $uri = $this->getUri() . $id;

        $response = $this->client->get($uri);

        return $response;
    }

    public function destroy($zoneTypeId)
    {
        $uri = $this->getUri() . $zoneTypeId;

        return $this->client->delete($uri);
    }

    public function getList($queryStr)
    {
        $uri = $this->getUri();
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }

    protected function getUri()
    {
        return env('API_WAREHOUSE') . 'zone-types/';
    }

}
