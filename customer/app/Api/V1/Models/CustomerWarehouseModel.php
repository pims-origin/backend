<?php

namespace App\Api\V1\Models;

use App\Api\V1\Traits\CusZoneCusWarehouseService;
use \Seldat\Wms2\Models\CustomerWarehouse;

class CustomerWarehouseModel extends AbstractModel
{
    use CusZoneCusWarehouseService;
    protected $model;

    public function __construct(CustomerWarehouse $model = null)
    {
        $this->model = ($model) ?: new CustomerWarehouse();
    }

    /**
     * ps: @param int $warehouseId
     *
     * @param string|array $qualifier
     *
     * @return mixed
     */
    public function deleteCustomerWarehouse($warehouseId, $customerId)
    {
        return $this->model
            ->where('whs_id', $warehouseId)
            ->where('cus_id', $customerId)
            ->delete();
    }

    public function getCustomerWarehouse($customerId, $warehouseId)
    {
        $query = $this->model->where('whs_id', $warehouseId)
            ->where('cus_id', $customerId);
        $this->model->filterData($query);

        return $query->first();
    }

    public function update(array $data)
    {
        return $this->_update($data);
    }

    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->where('cus_id', $attributes['cus_id']);
        $this->model->filterData($query);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * Delete By
     *
     * @param $customerIds
     * @param $warehouseIds
     *
     * @return mixed
     */
    public function deleteByMany($customerIds, $warehouseIds)
    {
        $customerIds = is_array($customerIds) ? $customerIds : [$customerIds];
        $warehouseIds = is_array($warehouseIds) ? $warehouseIds : [$warehouseIds];

        return $this->model
            ->whereIn('cus_id', $customerIds)
            ->whereIn('whs_id', $warehouseIds)
            ->delete();
    }

    public function searchCustomerByWarehouse($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        if (!empty($attributes['mode'])) {
            $query->where('whs_id', $attributes['whs_id']);
        }
        else 
        $this->model->filterData($query, true);
        // Get
        $this->sortBuilder($query, $attributes);
        $models = $query->paginate($limit);

        return $models;
    }
}
