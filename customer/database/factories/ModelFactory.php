<?php

$factory->define(\Seldat\Wms2\Models\Customer::class, function ($faker) {

    return [
        'cus_name'            => str_random(10),
        'cus_code'            => str_random(10),
        'cus_status'          => 'AC',
        'cus_billing_account' => str_random(10),
    ];
});

$factory->define(\Seldat\Wms2\Models\CustomerStatus::class, function ($faker) {
    return [
        'cus_sts_code' => strtoupper($faker->randomLetter . $faker->randomLetter),
        'cus_sts_desc' => str_random(10),
    ];
});

$factory->define(\Seldat\Wms2\Models\CustomerContact::class, function ($faker) {
    return [
        'cus_ctt_fname'      => str_random(10),
        'cus_ctt_lname'      => str_random(10),
        'cus_ctt_email'      => $faker->email,
        'cus_ctt_phone'      => substr($faker->phoneNumber, 0, 18),
        'cus_ctt_mobile'     => substr($faker->phoneNumber, 0, 18),
        'cus_ctt_ext'        => $faker->randomNumber,
        'cus_ctt_position'   => str_random(10),
        'cus_ctt_cus_id'     => function () {
            return factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'cus_ctt_department' => str_random(10),
    ];
});
$factory->define(\Seldat\Wms2\Models\CustomerWarehouse::class, function () {
    return [
        'cus_id' => function () {
            return factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'whs_id' => function () {
            return factory(\Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
    ];
});

$factory->define(\Seldat\Wms2\Models\CustomerZone::class, function () {
    return [
        'cus_id'     => function () {
            return factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'zone_id'    => function () {
            return factory(\Seldat\Wms2\Models\Zone::class)->create()->zone_id;
        },
        'created_by' => 0
    ];
});

$factory->define(\Seldat\Wms2\Models\CustomerAddress::class, function ($faker) {
    return [
        'cus_add_line_1'      => str_random(10),
        'cus_add_line_2'      => str_random(10),
        'cus_add_country_id'  => function () {
            return factory(\Seldat\Wms2\Models\SystemCountry::class)->create()->sys_country_id;
        },
        'cus_add_city_name'   => str_random(10),
        'cus_add_state_id'    => function () {
            return factory(\Seldat\Wms2\Models\SystemState::class)->create()->sys_state_id;
        },
        'cus_add_postal_code' => str_random(10),
        'cus_add_cus_id'      => function () {
            return factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'cus_add_type'        => 'SH',
    ];
});

$factory->define(\Seldat\Wms2\Models\SystemCountry::class, function () {
    return [
        'sys_country_code' => str_random(7),
        'sys_country_name' => str_random(10),
    ];
});

$factory->define(\Seldat\Wms2\Models\SystemState::class, function () {
    return [
        'sys_state_country_id' => function () {
            return factory(\Seldat\Wms2\Models\SystemCountry::class)->create()->sys_country_id;
        },
        'sys_state_code'       => str_random(7),
        'sys_state_name'       => str_random(20),
        'sys_state_desc'       => str_random(100),
    ];
});

$factory->define(\Seldat\Wms2\Models\ChargeCode::class, function () {
    $chg_uom_id = 0;
    while ($chg_uom_id <= 0) {
        $chg_uom_id = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
    }

    $chg_type_id = 0;
    while ($chg_type_id <= 0) {
        $chg_type_id = factory(\Seldat\Wms2\Models\ChargeType::class)->create()->chg_type_id;
    }

    return [
        'chg_code'    => str_random(10),
        'chg_uom_id'  => $chg_uom_id,
        'chg_type_id' => $chg_type_id,
    ];
});

$factory->define(\Seldat\Wms2\Models\CustomerChargeDetail::class, function () {
    return [
        'cus_id'              => function () {
            return factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'whs_id'              => function () {
            return factory(\Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'chg_code_id'         => function () {
            return factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;
        },
        'deleted_at'          => 915148800,
        'cus_charge_dtl_rate' => random_int(1, 100),
        'cus_charge_dtl_des'  => str_random(100),
    ];
});

$factory->define(\Seldat\Wms2\Models\SystemUom::class, function ($faker) {
    $code = '';
    $systemCode = 1;
    while (!empty($systemCode)) {
        $code = substr($faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+[A-Z]{2,4}'), 0, 10);
        $systemCode = DB::table('system_uom')->where('sys_uom_code', $code)->get();
    }

    return [
        'sys_uom_code' => $code,
        'sys_uom_name' => str_random(10),
        'sys_uom_des'  => str_random(10),
    ];
});

$factory->define(\Seldat\Wms2\Models\ChargeType::class, function ($faker) {
    $code = '';
    $systemCode = 1;
    while (!empty($systemCode)) {
        $code = substr($faker->regexify('[A-Za-z0-9]{2}'), 0, 2);
        $systemCode = DB::table('charge_type')->where('chg_type_code', $code)->get();
    }

    return [
        'chg_type_code' => $code,
        'chg_type_name' => str_random(10),
        'chg_type_des'  => str_random(10),
    ];
});

$factory->define(\Seldat\Wms2\Models\Warehouse::class, function () {
    return [
        'whs_name'       => str_random(10),
        'whs_code'       => str_random(10),
        'whs_status'     => 'AC',
        'whs_short_name' => str_random(10),
        'whs_country_id' => function () {
            return factory(\Seldat\Wms2\Models\SystemCountry::class)->create()->sys_country_id;
        },
        'whs_state_id'   => function () {
            return factory(\Seldat\Wms2\Models\SystemState::class)->create()->sys_state_id;
        },
    ];
});

$factory->define(\Seldat\Wms2\Models\Zone::class, function () {
    return [
        'zone_name'    => str_random(10),
        'zone_code'    => str_random(10),
        'zone_whs_id'  => function () {
            return factory(\Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'zone_type_id' => function () {
            return factory(\Seldat\Wms2\Models\ZoneType::class)->create()->zone_type_id;
        },
    ];
});

$factory->define(\Seldat\Wms2\Models\ZoneType::class, function () {
    return [
        'zone_type_name' => str_random(10),
        'zone_type_code' => str_random(10),
    ];
});