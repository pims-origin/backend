<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use App\Invoice;

use Seldat\Wms2\Models\InvoiceList;
use Seldat\Wms2\Models\InvoiceHeader;

use App\Api\V1\Models\CommonInvoiceModel;
use App\Api\V1\Models\ReceivingChargeCodeModel;
use App\Api\V1\Models\StorageChargeCodeModel;
use App\Api\V1\Models\OrderProcChargeCodeModel;
use App\Api\V1\Models\WorkHoursModel;

use Seldat\Wms2\Models\CustomerWarehouse;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\ChargeCode;
use Seldat\Wms2\Models\RcvSum;
use Seldat\Wms2\Models\InvoiceReceivingHistory;
use Seldat\Wms2\Models\InvoiceOrdProcHistory;
use Seldat\Wms2\Models\GoodsReceipt;

use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;

class InvoiceListModel extends AbstractModel
{
    protected $model;
    protected $invoiceHeader;
    protected $common;
    protected $receiving;
    protected $storage;
    protected $processing;
    protected $cusWarehouse;
    protected $customer;
    protected $workHours;
    protected $charge;
    protected $rcvSum;
    protected $hisRcv;
    protected $hisOrd;

    public $finalCurrent;
    /**
    *  Constructor
    *
    */

    public function __construct(
        InvoiceList $model = NULL,
        InvoiceHeader $invoiceHeader = NULL,
        CommonInvoiceModel $common = NULL,
        NewReceivingChargeCodeModel $receiving = NULL,
        NewStorageChargeCodeModel $storage = NULL,
        OrderProcChargeCodeModel $processing = NULL,
        CustomerWarehouse $cusWarehouse = NULL,
        Customer $customer = NULL,
        WorkHoursModel $workHours = NULL,
        ChargeCode $charge = NULL,
        RcvSum $rcvSum = NULL,
        InvoiceReceivingHistory $hisRcv = NULL,
        InvoiceOrdProcHistory $hisOrd = NULL,
        GoodsReceipt $grdHdr = NULL,
        SummaryModel $invSum = NULL
    ) {
        $this->model = ($model) ?: new InvoiceList();
        $this->invoiceHeader = ($invoiceHeader) ?: new InvoiceHeader();
        $this->common = ($common) ?: new CommonInvoiceModel();
        $this->receiving = ($receiving) ?: new ReceivingChargeCodeModel();
        $this->storage = ($storage) ?: new StorageChargeCodeModel();
        $this->processing = ($processing) ?: new OrderProcChargeCodeModel();
        $this->cusWarehouse = ($cusWarehouse) ?: new CustomerWarehouse();
        $this->customer = ($customer) ?: new Customer();
        $this->workHours = ($workHours) ?: new WorkHoursModel();
        $this->charge = ($charge) ?: new ChargeCode();
        $this->rcvSum = ($rcvSum) ?: new RcvSum();
        $this->hisRcv = ($hisRcv) ?: new InvoiceReceivingHistory();
        $this->hisOrd = ($hisOrd) ?: new InvoiceOrdProcHistory();
        $this->grdHdr = ($grdHdr) ?: new GoodsReceipt();
        $this->invSum = ($invSum) ?: new SummaryModel();
    }

    /**
     * @getVars array
     * @return mixed
    */

    public function getBillableCusts($whs_id)
    {
        $billableCusts = [];

        $custList = $this->cusWarehouse
                        ->with([
                        'warehouse' => function($q) use($whs_id)
                        {
                            $q->select('whs_id', 'whs_code');
                            // search whs id
                            if (isset($whs_id)) {
                                $q->where('whs_id', (int)$whs_id);
                            }

                        },
                    ])
                    ->select('cus_id','whs_id')
                    ->where('whs_id', $whs_id)
                    ->get();

        // Check if there is something billable from the time frame for
        // each client
        // For now including all customers

        foreach ($custList as $row)
        {
            $cus_name = DB::table('customer')
                            ->where('cus_id', '=', $row->cus_id)
                            ->pluck('cus_name');

            foreach($cus_name as $cus)
            {
                $name = $cus;
            }

            $billableCusts[$row->cus_id] = [
                'radio' => NULL,
                'sts' => NULL,
                'whs_id' => $row->whs_id,
                'fullCustomerName' => $row->warehouse->whs_code . '-' . $name,
                'invNbr' => NULL,
                'invDT' => NULL,
                'cnclNbr' => NULL,
                'currency' => CURRENCY_CODE,
                'total' => 0,
                'pmntDT' => NULL,
                'check' => NULL,
                'custID' => $row->cus_id,
                'type' => NULl,
            ];
        }

        return $billableCusts;
    }

    /**
     * @getVars array
     * @return mixed
    */

    public function getItemDetails($data=[])
    {
        $categories = getDefault($data['categories'], NULL);

        $results = [];

        //get receiving items
        $rcvUOMs = $this->common->customerUOMs(Invoice::RC);

        if ($rcvUOMs && (! $categories || in_array('receiving', $categories))) {
            $catResults = $this->getReceivingInvoices($data);
            $results = array_merge($results, $catResults);
        }

        //get order processing items
        $opUOMs = $this->common->customerUOMs(Invoice::OP);

        if ($opUOMs && (! $categories || in_array('processing', $categories))) {
            $catResults = $this->getProcessingInvoices($data);
            $results = array_merge($results, $catResults);
        }

        return $results;
    }

    /**
     * @data array
     * @return array
    */

    public function getReceivingInvoices($data)
    {
        $custID = getDefault($data['custID']);
        $whsID = getDefault($data['whs_id']);
        $goodReceiptIDs = getDefault($data['gr_hdr_id']);
        $startDate = getDefault($data['startDate']);
        $endDate = getDefault($data['endDate']);
        $displayDetails = getDefault($data['displayDetails']);
       
        $hisRecNums = $goodReceiptIDs || $displayDetails ? [] :
            $this->receiving->getReceivingHisCarton($data);

        $query = DB::table('rcv_sum AS rs')
            ->join('gr_hdr AS gh', 'gh.gr_hdr_id', '=', 'rs.gr_hdr_id')
            ->join('asn_hdr AS ah', 'ah.asn_hdr_id', '=', 'gh.asn_hdr_id')
            ->join('system_measurement AS sy', 'sy.sys_mea_code', '=', 'ah.sys_mea_code')
            ->join('customer AS cu', 'cu.cus_id', '=', 'rs.cus_id')
            ->select(
                'rs.gr_hdr_id AS recNum',
                'gh.gr_hdr_num AS gr_num',
                'gh.ctnr_num AS container',
                'ah.asn_hdr_num AS asn_num',
                'ah.asn_hdr_ref AS refCode',
                'cus_name AS clientName',
                'sys_mea_name AS measure',
                'dt AS setDate',
                'rs.ctn_val AS carton',
                'rs.plt_val AS pallet',
                'rs.vol_val AS weight'
            );

        if ($startDate) {
            $query->where('dt', '>=', $data['startDate']);
        }

        if ($endDate) {
            $query->where('dt', '<=', $data['endDate']);
        }

        if ($goodReceiptIDs) {
            $query->whereIn('rs.gr_hdr_id', $goodReceiptIDs);
        }

        $query->where(function ($q) use ($custID, $whsID, $hisRecNums) {
                if ($custID) {
                    $q->where('rs.cus_id', $custID);
                }

                if ($whsID) {
                    $q->where('rs.whs_id', $whsID);
                }

                if ($hisRecNums) {
                    $q->whereNotIn('rs.gr_hdr_id', $hisRecNums);
                }
            })
            ->orderBy('gh.gr_hdr_num')
            ->groupBy('rs.gr_hdr_id');

        $results = $query->get();

        if ($results || ! $displayDetails) {

            $recNums = array_column($results, 'recNum');
         
            $hisRecNums = $this->hisRcv
                ->select('gr_hdr_id')
                ->whereIn('gr_hdr_id', $recNums)
                ->where('inv_sts', 1)
                ->lists('gr_hdr_id')
                ->toArray();
        }

        $return = [];

        foreach ($results as $values) {

            $recNum = $values->recNum;

            if (! in_array($recNum, $hisRecNums) || $displayDetails) {
                $return[] = [
                    'id' => $values->recNum,
                    'cat' => 'Receiving',
                    'cust' => $values->clientName,
                    'ext' => [
                        'name' => $values->container,
                        'refCode' => $values->refCode,
                        'meas' => $values->measure,
                    ],
                    'dt' => $values->setDate,
                    'asnNum' => $values->asn_num,
                    'grNum' => $values->gr_num,
                    'ctnVal' => $values->carton,
                    'pltVal' => $values->pallet,
                    'volVal' => $values->weight,
                ];
            }
        }

        return $return;
    }

    /**
     * @data array
     * @processing array
     * @return mixed
    */

    function getProcessingInvoices($data)
    {
        $return = $hisOrderNums = [];

        $cus_id = getDefault($data['custID']);
        $displayDetails = getDefault($data['displayDetails']);

        $processedOrderIDs = $this->processing->getBilledOrders($cus_id);

        $params = [
            'endDate' => getDefault($data['endDate']),
            'startDate' => getDefault($data['startDate']),
            'processedOrderIDs' => $processedOrderIDs,
            'odr_id' => getDefault($data['odr_id']),
            'cusID' => $cus_id,
            'whsID' => getDefault($data['whs_id'])
        ];

        $first = $this->processing->processingQuery($params, 'shipped');
        $second = $this->processing->processingQuery($params, 'cancelled');

        $results = $first->union($second)
            ->get()
            ->toArray();

        if ($results && ! $displayDetails) {

            $orderNums = array_column($results, 'odr_num');

            $hisOrderNums = $this->hisOrd
                ->select('ord_num')
                ->whereIn('ord_num', $orderNums)
                ->where('inv_sts', 1)
                ->lists('ord_num')
                ->toArray();
        }

        foreach ($results as $values) {

            $ordNum = $values['odr_num'];

            if (! in_array($ordNum, $hisOrderNums) || $displayDetails) {
                $return[] = [
                    'id' => $values['id'],
                    'cat' => 'Order Processing',
                    'cust' => $values['clientName'],
                    'ext' => [
                        'name' => $ordNum,
                        'custName' => $values['name'],
                        'custNum' => $values['cus_odr_num'],
                        'clientOrdNum' => $values['cus_po'],
                        'cq' => $values['cartonQuantity'],
                        'pq' => $values['pieceQuantity'],
                        'plq' => $values['palletQuantity'],
                    ],
                    'dt' => $values['dt'],
                    'order' => $values['type'],
                ];
           }
        }

        return $return;
    }

    /**
     * @getVars array
     * @return mixed
    */

    public function getBillableData($getVars=[])
    {
        // Check if this is a details item search
        $items = isset($getVars['items']) ?
            array_filter($getVars['items']) : [];

        if ($items) {

            foreach ($items as &$catItems) {
                $catItems = array_keys($catItems);
            }

            $getVars['items'] = $items;
        } else {
            $getVars['items'] = [];
        }

        // If not an items search defer to details value passed
        $details = $items || getDefault($getVars['details']);

        $dateRange = getDefault($getVars['startDate']) &&
            getDefault($getVars['endDate']);

        if (! $dateRange && ! $details) {
            throw new \Exception(Message::get("IN002", "dateMissing"));
        }

        $custID = getDefault($getVars['custID']);

        if (! $custID) {
            $getVars['sums'] = TRUE;
        } else if ($custID && ! $details) {
            $getVars['items'] = TRUE;
        } else if ($custID && $details) {
            $getVars['details'] = TRUE;
        } else {
            throw new \Exception(Message::get("BM006", "InvalidParams"));
        }

        $custSearch = $custID ? [$custID] : [];
        $whsID = getDefault($getVars['whs_id']);

        $uomRes = $getVars['uomRes'] =
            $this->common->getCosts($whsID, $custSearch, 'uom');

        $model = $this->common
                      ->setRates($uomRes)
                      ->customerUOMs();

        //REC-Flat Month
        $getVars['checkItems'] = $this->getReceivingInvoices($getVars);

        //calling calculate() for all charge types
        $rcvRes = $this->receiving->calculate($getVars, $model);
        $strRes = $this->storage->calculate($getVars, $model);
        $opRes = $this->processing->calculate($getVars, $model);

        $all = $strRes + $rcvRes;

        if (array_filter($opRes)) {
            foreach ($opRes as $row) {

                $custIDs = array_keys($row);

                foreach ($custIDs as $custID) {
                    $all[$custID] = TRUE;
                }
            }
        }

        $costRes = $getVars['costRes'] = $this->common->getCosts($whsID, $custSearch);

        //get the criteria to show each category
        $invoiceCriteria = $this->getInvoiceCriteria($getVars);

        // Storage
        $array = $strRes;
        $rates = getDefault($costRes['rates'], []);
        $costInfo = getDefault($costRes['info'], []);

        $storageTotals = [];

        //Storage Invoice Details
        $getVars['details'] = TRUE;

        if ($invoiceCriteria['calculateAll'] || $invoiceCriteria['storage']) {
            foreach ($array as $custID => $ccs) {
                self::getStorageDetails($storageTotals, [
                    'ccs' => $ccs,
                    'rates' => $rates,
                    'category' => Invoice::ST,
                    'custID' => $custID,
                    'costInfo' => $costInfo,
                ]);
            }
        }

        $results = [];

        //Storage Sums
        foreach (getDefault($storageTotals['details'], []) as $custID => $ccs) {
            $results['sums'][$custID] = getDefault($results['sums'][$custID], 0);
            $ccTotals = array_column($ccs, 'ccTotal');
            $results['sums'][$custID] += array_sum($ccTotals);
        }

        //Receiving Invoice Details
        if ($invoiceCriteria['calculateAll'] || $invoiceCriteria['receiving']) {
            $this->getReceivingInvoiceCosts($results, [
                'array' => $rcvRes,
                'costRes' => $costRes,
                'category' => Invoice::RC,
                'details' => $getVars['details'],
            ]);
        }

        //get the Labor charge code from charge code mstr
        $customerId = !empty($getVars['openCust']) ? $getVars['openCust'] : null;
        $laborCharges = $this->common->getLaborCharge($customerId);


        // Get labor quantity(hours)
        $laborQuantities = $this->getLaborCharges([
            'getVars'  => $getVars,
            'laborCharges' => $laborCharges,
            'startDate' => $getVars['startDate'],
            'endDate' => $getVars['endDate'],
            'custIDs' => $custSearch,
            'whsID' => $getVars['whs_id'],
            'invoiceCriteria' => $invoiceCriteria,
        ]);

        self::laborChargeInvoiceCosts($results, [
            'laborCharges' => $laborCharges,
            'laborQuantities' => $laborQuantities,
            'details' => $getVars['details']
        ]);


        //get Order Processing Details
        if ($invoiceCriteria['calculateAll'] || $invoiceCriteria['processing']) {
            // order processing + work orders
            // updating $results inside getProcessingCosts() function
            self::getInvoiceProcessingCosts($results, [
                'array' => $opRes,
                'costRes' => $costRes,
                'category' => Invoice::OP,
                'details' => $getVars['details'],
            ]);
        }


//        CommonInvoiceModel::formatDtsQty($results['details']);

        //Item details
        if (isset($getVars['items'])) {

            $getVars['categories'] = [
                'receiving',
                'processing',
            ];

            //get Receiving and Order Processing Items
            $results['items'] = $this->getItemDetails($getVars);

            //get Storage Items
            if (isset($strRes[$custID]) && isset($storageTotals['details'][$custID])) {
                foreach ($storageTotals['details'][$custID] as $cc => $row) {

                    if ($row['quantity'] > 0) {
                        $results['items'][] = [
                            'id' => $row['chg_cd_desc'],
                            'cat' => 'Storage',
                            'qty' => $row['quantity'],
                            'uom' => $row['sys_uom_name'],
                            'dt' => $getVars['startDate'].' to '.$getVars['endDate'],
                        ];

                        $results['details'][$custID][$cc] = $row;
                    }
                }
            }
        }


        CommonInvoiceModel::formatDtsQty($results['details']);

        if ($results['details']) {
            foreach ($results['details'] as $cus_id => $customerData) {
                foreach ($customerData as $charge => $values) {

                    if ($values['quantity'] == '-') {
                        continue;
                    }

                    if (! $values['chg_cd_price']) {
                        unset($results['details'][$cus_id][$charge]);
                    }
                }
            }
        }

        return $results;
    }

    /**
     * @getVars array
     * @return mixed
    */

    function getInvoiceCriteria($getVars)
    {
        $rcvGlobalCheck = getDefault($getVars['selectedCategories']['receivingChecked']) == 'on';
        $strGlobalCheck = getDefault($getVars['selectedCategories']['storageChecked']) == 'on';
        $ordGlobalCheck = getDefault($getVars['selectedCategories']['processingChecked']) == 'on';

        $rcvItemCheck = is_array($getVars['items'])
                 && getDefault($getVars['items']['Receiving']);

        $storItemCheck = is_array($getVars['items'])
                 && getDefault($getVars['items']['Storage']);

        $ordItemCheck = is_array($getVars['items'])
                 && getDefault($getVars['items']['Order Processing']);

        return [
            'receiving' => $rcvGlobalCheck && $rcvItemCheck,
            'storage' => $strGlobalCheck && $storItemCheck,
            'processing' => $ordGlobalCheck && $ordItemCheck,
            'calculateAll' => ! $rcvGlobalCheck && ! $strGlobalCheck && ! $ordGlobalCheck
        ];
    }

    /**
     * @storageTotals array
     * @params  array
     * @return mixed
    */

    static function getStorageDetails(&$storageTotals, $params)
    {
        $ccs = $params['ccs'];
        $rates = $params['rates'];
        $category = $params['category'];
        $custID = $params['custID'];
        $costInfo = $params['costInfo'];

        if (! $ccs) {
            return;
        }

        $uomCCs = [];
        foreach ($costInfo[$category] as $cc => $row) {
            $uom = $row['sys_uom_name'];
            $uomCCs[$uom] = $cc;
        }

        foreach ($ccs as $uom => $quantity) {

            // Get charge code from UOM
            $cc = $uomCCs[$uom];

            if (! isset($rates[$category][$custID][$cc]) || ! $quantity) {
                continue;
            }

            $rate = $rates[$category][$custID][$cc];
            $row = $costInfo[$category][$cc];
            $row['rate'] = $rates[$category][$custID][$cc];
            $row['quantity'] = round($quantity, 3);
            $row['ccTotal'] = $rate * $row['quantity'];
            $storageTotals['details'][$custID][$cc] = $row;
        }
       
    }


    /**
     * @results array
     * @data  array
     * @return mixed
    */

    function getReceivingInvoiceCosts(&$results, $data)
    {
        if (! getDefault($data['costRes'])) {
            return [];
        }

        $array = $data['array'];
        $rates = $data['costRes']['rates'];
        $details = $data['details'];
        $category = $data['category'];
        $costInfo = $data['costRes']['info'];

        foreach ($array as $chargeCode => $chgCodeQtys) {
            if (! $chgCodeQtys) {
                continue;
            }

            foreach ($chgCodeQtys as $vendorID => $value) {
                //sums
                $results['sums'][$vendorID] =
                    getDefault($results['sums'][$vendorID], 0);

                $cost = isset($rates[$category][$vendorID][$chargeCode]) ?
                        $rates[$category][$vendorID][$chargeCode] : 0;

                $results['sums'][$vendorID] +=  $value * $cost;

                 if (! $details) {
                    continue;
                }

                //details
                $results['details'][$vendorID][$chargeCode] = $costInfo[$category][$chargeCode];
                $results['details'][$vendorID][$chargeCode]['rate'] = $cost;
                $results['details'][$vendorID][$chargeCode]['quantity'] = $value;

                $results['details'][$vendorID][$chargeCode]['ccTotal'] =
                    $cost * $value;
            }
        }
        return $results;
    }

    /**
     * @data array
     * @return mixed
    */

    public function getLaborCharges($data)
    {
        $invoiceCriteria = $data['invoiceCriteria'];

        $recResult = $rushAmt = $ordResult = $woResult = $otAmt = $laborAmt = [];

        $vendorIDs = $data['custIDs'] ? $data['custIDs'] : [];

        $whsIDs = $data['whsID'] ? $data['whsID'] : [];

        $recNums = $data['getVars']['details'] ?
                    array_get($data['getVars']['items'], 'Receiving', []) : [];

        $orderIDs = $data['getVars']['details'] ?
                array_get($data['getVars']['items'], 'Order Processing', []) : [];

        if ($invoiceCriteria['calculateAll'] || $invoiceCriteria['receiving']) {
            //get receiving labor history from labor table
            $hisRcvNums = $this->workHours->getReceivingHisLabor($data, $recNums);

            //get Receiving labor cost, rush cost and overtime cost
            $recResult['RUSH-LABOR'] =  $this->workHours->getRecLaborQuery($data, [
                    'custClause' => $vendorIDs,
                    'whsID' => $whsIDs,
                    'containerClause' =>  $recNums,
                    'laborClause'  => 'rush',
                    'hisRcvNums'  => $hisRcvNums,
                    'chgCode' => 'RUSH-LABOR'
            ]);

            $recResult['OVERTIME-LABOR'] = $this->workHours->getRecLaborQuery($data, [
                    'custClause' => $vendorIDs,
                    'whsID' => $whsIDs,
                    'containerClause' =>  $recNums,
                    'laborClause'  =>  'overtime',
                    'hisRcvNums'  => $hisRcvNums,
                    'chgCode' => 'OVERTIME-LABOR'
            ]);

            $recResult['LABOR'] =  $this->workHours->getRecLaborQuery($data, [
                    'custClause' => $vendorIDs,
                    'whsID' => $whsIDs,
                    'containerClause' =>  $recNums,
                    'laborClause'  =>  'labor',
                    'hisRcvNums'  => $hisRcvNums,
                    'chgCode' => 'LABOR'
            ]);

        }

        if ($invoiceCriteria['calculateAll'] || $invoiceCriteria['processing']) {
            //get OP labor and WO labor from labor table
            $hisOrdNums = $this->workHours->getOrderProcHisLabor($data, $orderIDs);

            //get Ord_Proc labor cost, rush cost and overtime cost
            $ordResult['RUSH-LABOR'] = $this->workHours->getOrdLaborQuery($data, [
                    'custClause' =>   $vendorIDs,
                    'whsID' => $whsIDs,
                    'orderClause' =>  $orderIDs,
                    'laborClause'  =>  'rush',
                    'hisOrdNums'  => $hisOrdNums,
                    'chgCode' => 'RUSH-LABOR'
            ]);

            $ordResult['OVERTIME-LABOR'] = $this->workHours->getOrdLaborQuery($data, [
                    'custClause' =>   $vendorIDs,
                    'whsID' => $whsIDs,
                    'orderClause' =>  $orderIDs,
                    'laborClause'  => 'overtime',
                    'hisOrdNums'  => $hisOrdNums,
                    'chgCode' => 'OVERTIME-LABOR'
            ]);

            $ordResult['LABOR'] = $this->workHours->getOrdLaborQuery($data, [
                    'custClause' =>   $vendorIDs,
                    'whsID' => $whsIDs,
                    'orderClause' =>  $orderIDs,
                    'laborClause'  => 'labor',
                    'hisOrdNums'  => $hisOrdNums,
                    'chgCode' => 'LABOR'
            ]);


            //get WO labor cost, rush cost and overtime cost
            $woResult['RUSH-LABOR'] = $this->workHours->getWoLaborQuery($data, [
                    'custClause' =>   $vendorIDs,
                    'whsID' => $whsIDs,
                    'orderClause' =>  $orderIDs,
                    'laborClause'   =>  'rush',
                    'hisOrdNums'  => $hisOrdNums,
                    'chgCode' => 'RUSH-LABOR'
            ]);


            $woResult['OVERTIME-LABOR'] = $this->workHours->getWoLaborQuery($data, [
                    'custClause' =>   $vendorIDs,
                    'whsID' => $whsIDs,
                    'orderClause' =>  $orderIDs,
                    'laborClause'   =>  'overtime',
                    'hisOrdNums'  => $hisOrdNums,
                    'chgCode' => 'OVERTIME-LABOR'
            ]);


            $woResult['LABOR'] = $this->workHours->getWoLaborQuery($data, [
                    'custClause' =>   $vendorIDs,
                    'whsID' => $whsIDs,
                    'orderClause' =>  $orderIDs,
                    'laborClause'   =>  'labor',
                    'hisOrdNums'  => $hisOrdNums,
                    'chgCode' => 'LABOR'
            ]);

       }


        $laborInfo = getDefault($data['laborCharges']['info'], []);
        $laborCodes = array_keys($laborInfo);


        //calculate total rush cost and total overtime cost for each category
        foreach ($vendorIDs as $custID) {
           foreach ($laborCodes as $chgCd) {
               if(getDefault($recResult[$chgCd][$custID]) ||
                       getDefault($ordResult[$chgCd][$custID]) ||
                          getDefault($woResult[$chgCd][$custID])
               ) {
                   $laborAmt[$custID][$chgCd] =
                        array_sum(getDefault($recResult[$chgCd][$custID], [])) +
                        array_sum(getDefault($ordResult[$chgCd][$custID], [])) +
                        array_sum(getDefault($woResult[$chgCd][$custID], []));
               }

            }
        }

       return $laborAmt ? $laborAmt : [];
    }

    /**
     * @results array
     * @data array
     * @return mixed
    */

    static function laborChargeInvoiceCosts(&$results, $data)
    {
        //calculate sums and details
            foreach ($data['laborQuantities'] as $custID => $value) {
                foreach ($value as $cc => $quantity) {
                    //sums
                    $results['sums'][$custID] =
                            getDefault($results['sums'][$custID], 0);
                    $price = $data['laborCharges']['info'][$cc]['chg_cd_price'];
                    $cost = $quantity * $price;
                    $results['sums'][$custID] += $cost;


                    if (! $data['details']) {
                        continue;
                    }

                    //details
                    $results['details'][$custID][$cc] = $data['laborCharges']['info'][$cc];

                    $results['details'][$custID][$cc]['cust_id'] = $custID;

                    $results['details'][$custID][$cc]['rate'] = $price;
                    $results['details'][$custID][$cc]['quantity'] = $quantity;

                    $results['details'][$custID][$cc]['ccTotal'] = $cost;
                }
            }
        }

    /**
     * @results array
     * @data array
     * @return mixed
    */

    static function getInvoiceProcessingCosts(&$results, $data)
    {
        if (! getDefault($data['costRes'])) {
            return [];
        }

        foreach ($data['array'] as $chargeCode => $categoryValues) {

            if (! $categoryValues) {
                continue;
            }

            foreach ($categoryValues as $vendorID => $value) {

                $data['chargeCode'] = $chargeCode;
                $data['vendorID'] = $vendorID;
                $data['value'] = $value;

                // updating $results inside getProcessingCosts() function
                $continue = self::getProcessingCosts($results, $data);

                if ($continue) {
                    continue;
                }
            }
        }

        return $results;
    }

    /**
     * @param results array
     * @param data array
     * @return mixed
    */

    static function getProcessingCosts(&$results, $data)
    {
        $rates = $data['costRes']['rates'];
        $details = $data['details'];
        $category = $data['category'];
        $costInfo = $data['costRes']['info'];

        $vendorID = $data['vendorID'];
        $cc = $data['chargeCode'];
        $value = $data['value'];

        if (! isset($rates[$category][$vendorID][$cc]) || ! $value) {
            return TRUE;
        }

        $cost = $rates[$category][$vendorID][$cc];

        $results['sums'][$vendorID] = getDefault($results['sums'][$vendorID], 0);
        $results['sums'][$vendorID] += $value * $cost;

        if (! $details) {
            return TRUE;
        }

        $quantity = getDefault($results['details'][$vendorID][$cc]['quantity'], 0);
        $ccTotal = getDefault($results['details'][$vendorID][$cc]['ccTotal'], 0);

        $results['details'][$vendorID][$cc] = $costInfo[$category][$cc];
        $results['details'][$vendorID][$cc]['rate'] = $cost;
        $results['details'][$vendorID][$cc]['quantity'] = $quantity + $value;
        $results['details'][$vendorID][$cc]['ccTotal'] = $ccTotal + $cost * $value;

        return FALSE;
    }

    /**
     * @open array
     * @getvars array
     * @return mixed
    */

    function getClauses($data, $dateField, $vendorField='vendorID')
    {
        $fromDate = getDefault($data['startDate']);
        $toDate = getDefault($data['endDate']);
        $vendorID = $vendorIDs = getDefault($data['custID']);

        if ($vendorID) {
            $vendorIDs = is_array($vendorID) ? $vendorID : [$vendorID];
        }

        $qMarks = $this->db->getQMarkString($vendorIDs);

        $fromClause = $fromDate ? 'AND ' . $dateField . ' >= ?' : NULL;
        $toClause = $toDate ? 'AND ' . $dateField . ' <= ?' : NULL;
        $vendorClause = ! $vendorID ? NULL :
                'AND ' . $vendorField . ' IN (' . $qMarks . ')';

        $params = $vendorIDs ? $vendorIDs : [];

        $fromDate && $params[] = $fromDate;
        $toDate && $params[] = $toDate;

        return [
            'whereClause' => $vendorClause . ' ' . $fromClause . ' ' . $toClause,
            'params' => $params,
        ];
    }

    /**
     * @open array
     * @getvars array
     * @return mixed
    */

    public function updateInvoTables($getVars=[])
    {
        $openItems = $getItems = [];

        $return = [];

        $open = $this->getBillableData($getVars);

        //Open Items
        foreach (getDefault($open['items'], []) as $key => $value) {
            $cat = $value['cat'];
            if ($cat == "Storage") {
                continue;
            }
            $openItems[$cat][] = $value['id'];
        }

        //getVars['item']
        if (getDefault($getVars['items'])) {
            foreach ($getVars['items'] as $key => $value) {
                if ($key == "Storage") {
                   continue;
                }
                $cat = $key == 'orderPrc' ? 'Order Processing' : $key;
                $getItems[$cat] = array_keys($value);
            }
        }

        $invItems = is_array($getVars['items']) ? $getItems : $openItems;

        return [
                'sums' => array_get($open, 'sums', 0),
                'items' => getDefault($open['items'], []),
                'details' => getDefault($open['details'], []),
                'invItemsIDs' => $invItems
        ];
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $searchParams = [
            'searchAttr' => $searchAttr,
            'attributes' => $attributes,
            'searches' => [
                'cus_id',
                'whs_id',
                'fromDate',
                'toDate',
            ],
        ];

        // check billable entires from invoice_summary table
        $checkCust = $this->model
            ->select(
                DB::raw('DISTINCT cus_id')
            )
            ->where('invoice_summary.inv_sts', '=', '1')
            ->where('invoice_summary.whs_id', '=', $attributes['whs_id'])
            ->where('invoice_summary.dt', '>=', $attributes['fromDate'])
            ->where('invoice_summary.dt', '<=', $attributes['toDate'])
            ->lists('cus_id');

        // get billable invoices from invoice_summary table
        $firstUnion = $this->model
                ->select(
                    DB::raw('"' . INVOICE_STATUS_OPEN['code'] . '" AS inv_sts'),
                    'invoice_summary.cus_id',
                    'invoice_summary.whs_id',
                    DB::raw('NULL AS inv_num'),
                    DB::raw('NULL AS inv_dt'),
                    DB::raw('NULL AS cncl_inv_num'),
                    DB::raw('"' . CURRENCY_CODE . '" AS inv_cur'),
                    DB::raw('NULL AS inv_amt'),
                    DB::raw('NULL AS pmnt_rcv_dt'),
                    DB::raw('NULL AS check_num'),
                    DB::raw('NULL AS invoice_number')
                )
                ->join('invoice_cost AS ic', 'ic.chg_code_id', '=', 'invoice_summary.chg_code_id')
                ->where('ic.chg_cd_price', '>', '0')
                ->where('ic.deleted', 0)
                ->whereRaw('ic.cus_id = invoice_summary.cus_id')
                ->whereRaw('ic.whs_id = invoice_summary.whs_id')
                ->where('invoice_summary.inv_sts', '=', '1')
                ->whereIn('invoice_summary.cus_id', $checkCust)
                ->groupBy([
                    'invoice_summary.cus_id',
                    'invoice_summary.whs_id',
                    'invoice_summary.chg_code_id'
                ]);

        $searchParams['query'] = $firstUnion;
        $searchParams['model'] = $this->model;

        $first = $this->addSearchWhereClauses($searchParams);

        // get processed (invoiced, paid, etc.) invoices from invoice_hdr table
        $secondUnion = $this->invoiceHeader
                ->select(
                    'inv_sts',
                    'cus_id',
                    'whs_id',
                    'inv_num',
                    'inv_dt',
                    DB::raw('inv_org AS cncl_inv_num'),
                    'inv_cur',
                    DB::raw('FORMAT(inv_amt, 2) AS inv_amt'),
                    'inv_paid_dt',
                    'inv_paid_ref',
                    DB::raw('inv_num AS invoice_number')
                )
                ->where('inv_typ', INVOICE_TYPE_ORIGINAL['code'])
                ->union($first);


        $searchParams['query'] = $secondUnion;
        $searchParams['model'] = $this->invoiceHeader;

        $second = $this->addSearchWhereClauses($searchParams);

        // join billable and processed invoices into a single table
        $final = DB::table(
                    DB::raw("({$second->toSql()}) as subQuery")
                )
                ->mergeBindings($second->getQuery())
                ->select(
                    '*',
                    'cus_name'
                )
                ->join('customer AS c', 'c.cus_id', '=', 'subQuery.cus_id');

        if (isset($searchAttr['inv_sts']) && isset($attributes['inv_sts'])) {
            foreach (INVOICE_STATUSES as $code => $name) {
                if ($searchAttr['inv_sts'] == $name) {

                    $final->whereRaw('subQuery.inv_sts = "' . $code . '"');

                    break;
                }
            }
        }

//        $this->sortBuilder($query, $attributes);

        // Get
        $models = $final->paginate($limit);

        return $models;
    }

    /*
    ****************************************************************************
    */

    private function addSearchWhereClauses($data)
    {
        $query = $data['query'];
        $model = $data['model'];
        $searchAttr = $data['searchAttr'];
        $attributes = $data['attributes'];
        $searches = $data['searches'];

        foreach ($searchAttr as $key => $value) {
            if (in_array($key, $searches) && isset($attributes[$key])) {
                $query = $this->addSearchWhereClause([
                    'query' => $query,
                    'key' => $key,
                    'value' => $value,
                    'prefix' => $model->getTable(),
                ]);
            }
        }

        return $query;
    }

    /*
    ****************************************************************************
    */

    private function addSearchWhereClause($data)
    {
        $query = $data['query'];
        $key = $data['key'];
        $value = $data['value'];
        $prefix = $data['prefix'];

        if (! in_array($key, ['fromDate', 'toDate'])) {

            $query->where($prefix . '.' . $key, $value);

            return $query;
        }

        $field = $prefix == 'invoice_hdr' ? 'inv_dt' : 'dt';

        $compareOperator = $key == 'fromDate' ? '>=' : '<=';

        $query->where($prefix . '.' . $field, $compareOperator, $value);

        return $query;
    }

    /*
   ****************************************************************************
   */

    public function currentRevenueData($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $this->currentRevData = $this->getCurrentRevenueData($attributes);

        //return  $this->currentRevData;
        $final['data'] = $this->currentRevData;

        if ($final['data']["REVENUE_BY_CUSTOMER"]["customer"] == "NULL") {
            $final['data'] = [];
        }

        $models = $this->arrayPagination($final);

        return $models;
    }

    /*
    ****************************************************************************
   */

    public function getCurrentRevenueData($attributes)
    {
        //get current month
        $startMonth = 1;
        $curMonth = date('m');
        $curYear = date('Y');
        $type = 'REVENUE_BY_CUSTOMER';

        $this->finalCurrent =  $this->invSum->getCurrentRevenueArray($type);

        $invCurAmtRes = $this->invoiceHeader
            ->join('customer AS c', 'c.cus_id', '=', 'invoice_hdr.cus_id')
            ->select(
                'invoice_hdr.cus_id',
                'cus_name',
                DB::raw('SUM(inv_amt) AS amt')
            )
            ->where('inv_typ', 'o')
            ->whereIn('invoice_hdr.inv_sts', [
                INVOICE_STATUS_INVOICED['code'],
                INVOICE_STATUS_PAID['code']
            ])
            ->where('invoice_hdr.whs_id', $attributes['whs_id'])
            ->where(DB::Raw('MONTH(inv_dt)'), (int) $curMonth -1)
            ->where(DB::Raw('YEAR(inv_dt)'), $curYear)
            ->groupBy('invoice_hdr.inv_num')
            ->get()
            ->toArray();

        $invYTDAmtRes = $this->invoiceHeader
            ->join('customer AS c', 'c.cus_id', '=', 'invoice_hdr.cus_id')
            ->select(
                'invoice_hdr.cus_id',
                DB::raw('SUM(inv_amt) AS amt')
            )
            ->where('inv_typ', 'o')
            ->whereIn('invoice_hdr.inv_sts', [
                INVOICE_STATUS_INVOICED['code'],
                INVOICE_STATUS_PAID['code']
            ])
            ->where('invoice_hdr.whs_id', $attributes['whs_id'])
            ->where(DB::Raw('MONTH(inv_dt)'), '>=', (int) $startMonth -1)
            ->where(DB::Raw('MONTH(inv_dt)'), '<=', (int) $curMonth -1)
            ->where(DB::Raw('YEAR(inv_dt)'), '=', $curYear)
            ->groupBy('invoice_hdr.cus_id')
            ->get()
            ->lists('amt','cus_id');

        foreach($invCurAmtRes AS $row) {
            $cusID = $row['cus_id'];
            $this->finalCurrent[$type]['customer'] = $row['cus_name'];
            $this->finalCurrent[$type]['currentMonthRevenue'] = number_format($row['amt'], 2,',', '.');
            $this->finalCurrent[$type]['YTDRevenue'] = number_format($invYTDAmtRes[$cusID], 2,',', '.');
        }

        return $this->finalCurrent;
    }
}

