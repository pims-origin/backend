<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\MasterDataType;
use League\Fractal\TransformerAbstract;

class MasterDataTypeTransformer extends TransformerAbstract
{
    public function transform(MasterDataType $masterDataType)
    {
        $createdBy = trim(object_get($masterDataType, "createdBy.first_name", null) . " " . object_get($masterDataType, "createdBy.last_name", null));
        $updatedBy = trim(object_get($masterDataType, "updatedBy.first_name", null) . " " . object_get($masterDataType, "updatedBy.last_name", null));

        return [
            'md_type'    => $masterDataType->md_type,
            'mdt_name'   => $masterDataType->mdt_name,
            'mdt_des'    => $masterDataType->mdt_des,
            'mdt_data'   => $masterDataType->mdt_data,
            'md_sts'     => $masterDataType->md_sts,
            'created_at' => date("Y-m-d H:i:s", strtotime($masterDataType->created_at)),
            'created_by' => $createdBy,
            'updated_at' => date("Y-m-d H:i:s", strtotime($masterDataType->updated_at)),
            'updated_by' => $updatedBy,
        ];
    }
}