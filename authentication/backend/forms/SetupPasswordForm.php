<?php

namespace backend\forms;

use Yii;
use common\models\User;

class SetupPasswordForm extends UpdatePasswordForm
{
    const PASSWORD_MIN_LENGTH = 8;

    public $new_password;
    public $repeat_new_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['new_password', 'validatePassword'],
            [['new_password', 'repeat_new_password'], 'required'],
            ['repeat_new_password', 'compare', 'compareAttribute' => 'new_password']
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (strlen($this->new_password) < self::PASSWORD_MIN_LENGTH) {
            $this->addError($attribute, 'New password should be at least ' . self::PASSWORD_MIN_LENGTH . ' length.');
        }
        if (!preg_match("/([A-Z])/", $this->new_password)) {
            $this->addError($attribute, 'New password should be at one Upper case character.');
        }

        if (!preg_match("/([0-9])/", $this->new_password)) {
            $this->addError($attribute, 'New password should be at one number.');
        }

        if (!preg_match("/([a-z])/", $this->new_password)) {
            $this->addError($attribute, 'New password should be at one lower case character.');
        }

        if (!preg_match('/[\!@#\$%\^&\*\`~\(\)\{\}\[\]\,\?\=+\-_\.:;\<\>]/', $this->new_password)) {
            $this->addError($attribute, 'New password should be at one symbol character.');
        }

        if (strpos($this->new_password, " ") !== false) {
            $this->addError($attribute, 'User can not use space character in password');
        }
    }
}
