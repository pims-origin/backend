<?php

namespace App\Api\MQ\Commands;
use Illuminate\Console\Command;

/**
 * Created by PhpStorm.
 * User: phuctran
 * Date: 12/12/2016
 * Time: 14:19
 */
class ExampleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mq:example-command {param}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
//        print_r($this->argument());
        $data = \GuzzleHttp\json_decode($this->argument('param'));
        file_put_contents(storage_path('logs/' . $data->file), 'queue');
        sleep(20);
        file_put_contents(storage_path('logs/' . $data->file), 'queue after');
    }
}