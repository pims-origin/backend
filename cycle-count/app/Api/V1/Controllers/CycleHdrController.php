<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Services\CycleCountService;
use App\Api\V1\Traits\CycleHdrControllerTrait;
use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\View;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V1\Validators\CycleHdrValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use App\Api\V1\Transformers\CycleHdrTransformer;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\V1\Validators\CusInUserValidator;
use Wms2\UserInfo\Data;

class CycleHdrController extends AbstractController
{
    use CycleHdrControllerTrait;

    /**
     * @var CycleHdrModel
     */
    protected $model;

    /**
     * @var CycleHdrValidator
     */
    protected $validator;

    /**
     * @var $cycleHdrTransform
     */
    protected $cycleHdrTransform;

    /**
     * @var CycleCountService
     */
    protected $cycleCountService;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /*
     * @var CartonModel
     */
    protected $cartonModel;

    /*
     * @var PalletModel
     */
    protected $palletModel;


    /**
     * CycleHdrController constructor.
     *
     * @param CycleHdrModel $model
     * @param CycleHdrValidator $validator
     * @param CycleCountService $cycleCountService
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        CycleHdrModel $model,
        CycleHdrValidator $validator,
        CycleCountService $cycleCountService,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->model = $model;
        $this->validator = $validator;
        $this->cycleHdrTransform = new CycleHdrTransformer();
        $this->cycleCountService = $cycleCountService;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->cartonModel = new CartonModel();
        $this->palletModel = new PalletModel();
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $input = $request->getParsedBody();

        $this->validator->validate($input);
        //generate event code
        $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingNum();
        $cycle_num = EventTrackingModel::CYCLE_COMPLETED;
        $cycle_num .= "-";
        $cycle_num .= date("ymd");
        $cycle_num .= "-";
        $cycle_num .= date("His");
        $input['cycle_num'] = $cycleNumAndSeq['cycle_num'] = $cycle_num;
        
        if ($input['cycle_type'] == 'CS') {
            $checkCartons = Carton::where('cus_id', '=', $input['cycle_detail'])
                ->whereIn('ctn_sts', ['LK', 'RG', 'AC'])
                ->count();
            
            if ($checkCartons == 0) {
                return $this->response->errorBadRequest('There is no more stock for this Customer.');
            }
        }

        // validate create cycle count
        $this->validateCreateCycleCount($input);

        $data = $this->model->processCreateCycleHdr($input);
        if (!$data) {
            $errorMsg = implode($this->model->errors);

            return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }
        //event tracking
        $trans_num = str_replace(CYCLE_PREFIX, EventTrackingModel::CYCLE_CREATED_ASSIGNED,
            $cycleNumAndSeq['cycle_num']);
        $this->eventTrackingModel->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => ucfirst($input['cycle_type']) == 'CS' ? $input['cycle_detail'] : null,
            'owner'     => $cycleNumAndSeq['cycle_num'],
            'evt_code'  => EventTrackingModel::CYCLE_CREATED_ASSIGNED,
            'trans_num' => $trans_num,
            'info'      => $cycleNumAndSeq['cycle_num'] . ' ' .
                EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_CREATED_ASSIGNED]
        ]);

        return $this->response->item($data, new CycleHdrTransformer());
    }

    public function validateCreateCycleCount($input)
    {
        if ($input['cycle_type'] == 'CS') {
            $cycleByCus = $this->model->getCycleCountNotComplete($input['whs_id'], $input['cycle_detail']);
            if (! empty($cycleByCus)) {
                return $this->response->errorBadRequest(sprintf('Cannot create cycle count. Please complete cycle count ID: %s', $cycleByCus->cycle_hdr_id));
            }
            $checkCartons = Carton::where('cus_id', '=', $input['cycle_detail'])->whereIn('ctn_sts', ['LK', 'RG', 'AC'])->count();
            if ($checkCartons == 0) {
                return $this->response->errorBadRequest('There is no more stock for this Customer.');
            }
        }
    }

    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        $input = $request->getQueryParams();

        try {
            if (!isset($input['sort'])) {
                $input['sort'] = [
                    'cycle_hdr_id' => 'desc'
                ];
            }

            $data = $this->model->search(
                $input,
                ['warehouse', 'assigneeUser', 'cycleDtl'],
                array_get($input, 'limit', 20)
            );

            return $this->response->paginator($data, $this->cycleHdrTransform);
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $cycleHdrId
     *
     * @return string
     */
    public function view($cycleHdrId)
    {
        try {
            $data = $this->model->getCycleInfo($cycleHdrId);

            return $data ? $data->toArray() : null;

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $cycleHdrId
     *
     * @return mixed
     */
    public function printPdf($cycleHdrId)
    {
        return $this->cycleCountService->printCycleCountDetail($cycleHdrId);
    }

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function cycleType()
    {
        $types = $this->model->getCycleTypes();

        return $types;
    }

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function cycleStatus()
    {
        $statuses = $this->model->getCycleStatus();

        return $statuses;
    }

    public function destroy(Request $request)
    {
        $input = $request->getParsedBody();

        $cycle_hdr_ids = $input['cycle_hdr_ids'];
        // Parse to integer
        $ccHdrLstAfters = [];
        foreach ($cycle_hdr_ids as $cycle_hdr_id) {
            array_push($ccHdrLstAfters, (int)$cycle_hdr_id);
        }

        $response = $this->cycleCountService->deleteCycleCounts($ccHdrLstAfters);

        if (!$response['status']) {
            return $this->response->error(implode($response), IlluminateResponse::HTTP_BAD_REQUEST);
        }

        return $this->response->accepted(null, ['message' => Message::get('BM074'), 'data' => 'success']);
    }

    /**
     * @param $cycleHdrId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function saveCycled($cycleHdrId)
    {
        $model = new CycleDtlModel();
        $result = $model->processSavedCycled($cycleHdrId);

        if (!$result) {
            $errorMsg = implode($model->errors);

            return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
        }
        //event tracking
        $data = CycleHdrModel::getCycleHdr($cycleHdrId);
        $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingTransNum(
            $cycleHdrId,
            EventTrackingModel::CYCLE_CYCLED
        );
        $this->eventTrackingModel->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => ucfirst($data['cycle_type']) == 'CS' ? $data['cycle_detail'] : null,
            'owner'     => $cycleNumAndSeq['cycle_num'],
            'evt_code'  => EventTrackingModel::CYCLE_CYCLED,
            'trans_num' => $cycleNumAndSeq['trans_num'],
            'info'      => $cycleNumAndSeq['trans_num'] . ' ' .
                EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_CYCLED]
        ]);

        return $this->response->accepted(null, ['message' => Message::get('BM075'), 'data' => 'success']);
    }

    /**
     *
     */
    public function checkCusInUser(Request $request)
    {
        $this->validator = new CusInUserValidator();

        $input = $request->getParsedBody();
        $this->validator->validate($input);

        $model = new CycleHdrModel();

        $result = $model->checkCusInUser($input);

        if (!$result) {
            return $this->response->accepted(null, [
                'message' => Message::get('BM076'),
                'status'  => false
            ]);
        }

        return $this->response->accepted(null, ['message' => Message::get('BM077'), 'status' => true]);
    }

    /**
     * @param $whsId
     */
    public function getUserWhs()
    {
        $whsId = CycleCountService::getCurrentWhs();
        $result = $this->model->getUserInWarehouse($whsId);

        return ['data' => $result];
    }

    public function updateAssignee($cycle_id, $assignee_id)
    {
        $whsId = Data::getCurrentWhsId();
        $currentUser = Data::getCurrentUserId();
        $chkExist = DB::table('user_whs')
            ->where([
                'user_id' => $assignee_id,
                'whs_id'  => $whsId
            ])
            ->count();

        if (!$chkExist) {
            return $this->response->errorBadRequest('Assignee do not existed in warehouse');
        }

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $cycleHdr = $this->model->getModel()
            ->where('cycle_hdr_id', $cycle_id)
            ->first();

        if (!$cycleHdr) {
            return $this->response->errorBadRequest('This cycle count don\'t existed');
        }

        $cycleHdr->assignee_id = $assignee_id;
        $cycleHdr->save();

        return ['data' => 'successfull'];
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function skuAutoComplete(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $skus = $this->model->skuAutoComplete($input, array_get($input, 'limit'));
            $data = [];
            foreach ($skus as $key => $sku) {
                $data[$key] = $sku;
                $data[$key]['lot'] = explode(',', $sku['lot']);
            }

            return ['data' => $data];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }
}
