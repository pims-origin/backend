<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\LocationStatusDetailModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use Maatwebsite\Excel\Excel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Validators\PalletValidator;
use Swagger\Annotations as SWG;
use TCPDF;

/**
 * Class PalletController
 *
 * @package App\Api\V1\Controllers
 */
class PickController extends AbstractController
{
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var LocationStatusDetailModel
     */
    protected $locationStatusDetailModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var PalletSuggestLocationModel
     */
    protected $palletSuggestLocationModel;

    /**
     * PickController constructor.
     * @param PalletModel $palletModel
     * @param PalletSuggestLocationModel $palletSuggestLocationModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        PalletModel $palletModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        CartonModel $cartonModel,
        LocationModel $locationModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->palletModel = $palletModel;
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->eventTrackingModel = $eventTrackingModel;
    }

}
