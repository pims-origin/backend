<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\BOLCarrierDetail;

/**
 * Class BOLCarrierDetailModel
 *
 * @package App\Api\V1\Models
 */
class BOLCarrierDetailModel extends AbstractModel
{
    /**
     * @var BOLCarrierDetail
     */
    protected $model;

    /**
     * BOLCarrierDetailModel constructor.
     *
     * @param BOLCarrierDetailModel|null $model
     */
    public function __construct(BOLCarrierDetail $model = null)
    {
        $this->model = ($model) ?: new BOLCarrierDetail();
    }

    
}
