<?php

namespace App\Api\V2\PackDetail\Models;

use Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class SystemUomModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new SystemUom();
    }

}
