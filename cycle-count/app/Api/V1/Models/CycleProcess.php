<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21/11/2016
 * Time: 13:59
 */

namespace App\Api\V1\Models;

use Illuminate\Http\Response;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CycleDis;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\Invt;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\Pallet;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

class CycleProcess extends AbstractModel
{
    public $errors = [];
    protected $invSumModel;
    protected $inventory;

    public function __construct($model = null)
    {
        $this->model = $model ? $model : new CycleHdr();
        $this->invSumModel = new InventorySummary();
        $this->inventory = new Invt();
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function recount($data)
    {
        $model = new CycleDtlModel();
        $validate = $model->validateCycleDtlApprove($data);

        if (!$validate) {
            $this->errors = $model->errors;

            return false;
        }

        $result = $model->processRecountCycle($data);
        if (!$result) {
            $this->errors = $model->errors;
        }

        return $result;

    }

    public function approve($data)
    {

    }

    /**
     * @param $cycleHdrId
     * @param $cycleDtlIds
     *
     * @return mixed
     */
    public function getCycleDtlDataProcess($cycleHdrId, $cycleDtlIds)
    {
        if (!($cycleHdrId && $cycleDtlIds)) {
            return [];
        }

        $data = CycleDtl::select(
            'cycle_dtl.cycle_dtl_sts',
            'cycle_discrepancy.dicpy_qty',
            'cycle_discrepancy.ctn_id',
            'cycle_dtl.item_id',
            'cycle_dtl.lot',
            'cycle_dtl.whs_id',
            'cycle_dtl.remain',
            'cycle_dtl.pack',
            'cycle_discrepancy.dicpy_ctn_id',
            'cycle_discrepancy.dicpy_sts',
            'cycle_dtl.sys_loc_id',
            'cycle_dtl.act_loc_id',
            'cycle_dtl.act_loc_name',
            'cycle_hdr.cycle_num',
            'cycle_dtl.cycle_hdr_id',
            'cycle_dtl.cycle_dtl_id',
            'cycle_dtl.act_qty',
            'cycle_dtl.sys_qty',
            'cycle_dtl.cus_id',
            'cycle_dtl.is_new_sku',
            'cycle_dtl.plt_rfid'
        )
            ->join('cycle_hdr', 'cycle_hdr.cycle_hdr_id', '=', 'cycle_dtl.cycle_hdr_id')
            ->leftjoin('cycle_discrepancy', function ($join) {
                $join->on('cycle_discrepancy.cycle_dtl_id', '=', 'cycle_dtl.cycle_dtl_id')
                    ->whereNotIn('cycle_discrepancy.dicpy_sts', [
                        CycleDisModel::STATUS_DIS_RECOUNT,
                        CycleDisModel::STATUS_DIS_ADJUSTED
                    ]);
            })
            ->where('cycle_dtl.cycle_hdr_id', $cycleHdrId)
            ->where('cycle_dtl.cycle_dtl_sts', CycleDtlModel::STATUS_CYCLEDTL_OPEN)
            ->whereIn('cycle_dtl.cycle_dtl_id', $cycleDtlIds)
            ->get()
            ->toArray();

        return $data;
    }

    /**
     * @param $cycleHdrId
     * @param $cycleDtlIds
     *
     * @return bool
     * @deprecated DONT USE IT
     */
    public function updateActLocForCycleDtl($cycleHdrId, $cycleDtlIds)
    {
        if (!($cycleHdrId && $cycleDtlIds)) {
            return false;
        }


        $result = DB::update("UPDATE cartons INNER JOIN pallet  ON (cartons.plt_id= pallet.plt_id),
                (
                    SELECT  item_id,
                            remain,
                            sys_loc_id,
                            act_loc_id,
                            act_loc_name as loc_code,
                            act_loc_name AS loc_name,
                            pack,
                            lot
                    FROM cycle_dtl
                    WHERE sys_loc_id <> act_loc_id
                        AND cycle_hdr_id = ?
                        AND cycle_dtl_id IN (" . implode($cycleDtlIds, ',') . ")
                        AND cycle_dtl_sts = ?
                ) cd
                SET     cartons.loc_id = cd.act_loc_id,
                        cartons.loc_code = cd.loc_code,
                        cartons.loc_name = cd.loc_name,
                        pallet.loc_id = cd.act_loc_id,
                        pallet.loc_code = cd.loc_code,
                        pallet.loc_name = cd.loc_name
                WHERE
                    cartons.item_id = cd.item_id
                    AND cartons.lot = cd.lot
                    AND cartons.ctn_pack_size = cd.pack
                    AND cartons.piece_remain = cd.remain
                    AND cartons.loc_id = cd.sys_loc_id", [
            $cycleHdrId,
            CycleDtlModel::STATUS_CYCLEDTL_OPEN
        ]);

        return $result;
    }

    /**
     * @param $discreArr
     * @param $oddCartonIds
     * @param $alignCntIds
     */
    private function filterDelCartons($isEach, $discreArr, &$oddCartonIds, &$alignCntIds)
    {
        if ($isEach) {
            foreach ($discreArr as $ctn) {
                if ($ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_DELETE) {
                    $oddCartonIds[] = $ctn['ctn_id'];
                } elseif ($ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_ALIGN) {
                    $alignCntIds[] = $ctn;
                }
            }
        } else {
            $oddCartonIds = array_column($discreArr, 'ctn_id');
        }
    }

    /**
     * @param $oddCartons
     * @param $userId
     * @param $isEach
     */
    public function delCtnDiscrepancy($oddCartons, $userId, $isEach)
    {
        $oddCartonIds = $alignCntIds = [];
        $this->filterDelCartons($isEach, $oddCartons, $oddCartonIds, $alignCntIds);

        if ($oddCartonIds) {
            foreach (array_chunk($oddCartonIds, 200) as $chunkOldCartonIds) {
                Carton::whereIn('ctn_id', $chunkOldCartonIds)
                    ->update([
                        'ctn_sts'          => LockModel::STATUS_ADJUSTED,
                        //'plt_id'           => null,
                        'loc_id'           => null,
                        'loc_code'         => null,
                        //'loc_name'         => null,
                        'loc_type_code'    => null,
                        'picked_dt'        => time(),
                        'storage_duration' => CartonModel::getCalculateStorageDurationRaw('gr_dt'),
                        'updated_by'       => $userId,
                        'updated_at'       => time()
                    ]);
            }


        }
        $locIds = [];

        if ($alignCntIds) {
            $created = time();
            $alCtnIds = [];
            foreach ($alignCntIds as $alCtn) {
                $locIds[] = $alCtn['act_loc_id'];
                $alCtnIds[] = $alCtn['ctn_id'];
                //WMS2-4109
                if($carton = Carton::find($alCtn['ctn_id'])){
                    $pieceRemain = $carton->piece_remain + $alCtn['dicpy_qty'];
                    if($pieceRemain < 0){
                        $pieceRemain = $alCtn['act_qty'];
                    }

                    $carton->update([
                        'updated_by'    =>  $userId,
                        'updated_at'    =>  $created,
                        'piece_remain'  =>  $pieceRemain,
                        'loc_id'        =>  $alCtn['act_loc_id'],
                        'loc_code'      =>  $alCtn['act_loc_name'],
                        'loc_name'      =>  $alCtn['act_loc_name']
                    ]);
                }
            }

            //AJ align cartons
            foreach (array_chunk($alCtnIds, 200) as $chunkAlCtnIds) {
                Carton::whereIn('ctn_id', $chunkAlCtnIds)
                    ->where('piece_remain', 0)
                    ->update([
                        'ctn_sts'          => LockModel::STATUS_ADJUSTED,
                        //'plt_id'           => null,
                        'loc_id'           => null,
                        'loc_code'         => null,
                        //'loc_name'         => null,
                        'loc_type_code'    => null,
                        'picked_dt'        => time(),
                        'storage_duration' => CartonModel::getCalculateStorageDurationRaw('gr_dt'),
                        'updated_by'       => $userId,
                        'updated_at'       => time()
                    ]);
            }
        }
        $locIds = array_unique($locIds);

        return $locIds;
    }

    /**
     * @param      $lackCtn
     * @param      $user_id
     * @param bool $isEach
     */
    public function cloneCtnDiscrepancy($lackCtn, $user_id, $isEach = false)
    {
        $cartonModel = new CartonModel();

        $cartonIds = array_column($lackCtn, 'ctn_id');

        $cartons = $cartonModel->getCartons($cartonIds);
        $totalLack = array_sum(array_column($lackCtn, 'dicpy_qty'));

        //contain index off cycle_dtl_id to create carton_rfid
        $cycleDtlIndexArr = [];
        $temp = 0;

        $errorMessage = null;
        foreach ($lackCtn as $ctn) {
            $temp++;
            $ctnArr = $this->getCartonIdInArr($cartons, $ctn['ctn_id']);

            $itemObj = Item::where('item_id', $ctn['item_id'])->select(['cus_upc', 'pack'])->first();
            if (empty($itemObj)) {
                $itemCtn = DB::table('item')
                    ->where('item_id', $ctn['item_id'])
                    ->where('deleted', '<>', 0)
                    ->first();
                if (empty($itemCtn)) {
                    $errorMessage = sprintf('SKU %s does not exists', $itemCtn['sku']);
                    break;
                }
                $errorMessage = sprintf('SKU %s was deleted', $itemCtn['sku']);
                break;
            }

            //$maxCtnNum = $cartonModel->generateCartonNum($ctn['cycle_num']);

            if ($ctnArr) {
                unset($ctnArr['ctn_id']);

                //discrepancy process
                $disObj = CycleDisModel::getCycleDtl($ctn['dicpy_ctn_id']);
                $disArr = $disObj->toArray();
                $pallet = CycleDisModel::getPalletByLocID($disObj->cycleDtl->act_loc_id, $disObj->cycleDtl->cus_id, $disObj->cycleDtl->whs_id);

                if (!$pallet) {
                    $pallet = $this->createPallet($disObj->cycleDtl, $ctn);
                }

                $disArr['dicpy_qty'] = 1;
                $disArr['dicpy_sts'] = CycleDisModel::STATUS_DIS_ADJUSTED;
                unset($disArr['dicpy_ctn_id']);
                unset($disArr['cycle_dtl']);

                //get item
                //$itemObj = Item::where('item_id', $ctn['item_id'])->select(['cus_upc', 'pack'])->first();

                for ($i = 1; $i <= $ctn['dicpy_qty']; $i++) {
                    //$max++;
                    //$maxCtnNum++;


                    $objCarton = new Carton();
                    $this->setAttributeByArr($ctnArr, $objCarton);
                    //carton clone
                    $objCarton->ctn_num = 'CYC-' . bin2hex(random_bytes(10));

                    $objCarton->loc_id = $disObj->cycleDtl->act_loc_id;
                    $objCarton->loc_code = $disObj->cycleDtl->act_loc_name;
                    $objCarton->loc_name = $disObj->cycleDtl->act_loc_name;
                    //ctn process
                    $objCarton->updated_at = $disArr['updated_at'] = time();
                    $objCarton->updated_by = $disArr['updated_by'] = $user_id;
                    $objCarton->gr_dt = $ctnArr['gr_dt'] ?? null;
                    $objCarton->ctn_sts = LockModel::STATUS_LOCK;
                    $objCarton->is_damaged = null;
                    // $objCarton->piece_remain = $disObj->cycleDtl->remain;
                    // $objCarton->piece_ttl = $disObj->cycleDtl->remain;
                    $objCarton->ctn_pack_size = $itemObj->pack;
                    $objCarton->is_ecom = 0;
                    $objCarton->plt_id = $pallet->plt_id;
                    $objCarton->gr_hdr_id = null;
                    $objCarton->asn_dtl_id = null;
                    $objCarton->is_damaged = 0;
                    $objCarton->gr_dtl_id = null;
                    $objCarton->return_id = null;
                    $objCarton->po = null;
                    $objCarton->ctnr_num = null;
                    $objCarton->ctnr_id = null;
                    if (!empty($pallet->rfid)) {
                        if (isset($cycleDtlIndexArr[$disObj->cycleDtl->cycle_dtl_id])) {
                            $cycleDtlNum = $cycleDtlIndexArr[$disObj->cycleDtl->cycle_dtl_id] + 1;
                        } else {
                            $cycleDtlNum = 1;
                        }
                        $cycleDtlIndexArr[$disObj->cycleDtl->cycle_dtl_id] = $cycleDtlNum;
                        //carton rfid format: CCTC + cycle_dtl_id with(10 characters with leading zeros) + $i with leading zeros
                        $objCarton->rfid = "CCTC" . str_pad($disObj->cycleDtl->cycle_dtl_id, 10, '0', STR_PAD_LEFT) . str_pad($cycleDtlNum, 10, '0', STR_PAD_LEFT);
                    } else {
                        $objCarton->rfid = null;
                    }
                    $objCarton->loc_type_code = "RAC";
                    $objCarton->picked_dt = 0;
                    $objCarton->storage_duration = 0;
                    $objCarton->upc = $itemObj->cus_upc;
                    $objCarton->lpn_carton = $ctn['plt_rfid'];

                    //process for count_by = each
                    if ($isEach === true) {
                        if ($ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_CLONE && !$ctn['is_new_sku']) {
                            $objCarton->piece_remain = $disObj->cycleDtl->pack;
                            $objCarton->piece_ttl = $disObj->cycleDtl->pack;
                        }
                        if ($ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_ALIGN) {
                            $objCarton->piece_remain = $objCarton->piece_ttl = $ctn['dicpy_qty'];
                        }
                    }
                    $totalLack--;
                    //discrepacy clone
                    if (!$objCarton->save()) {
                        continue;
                    }

                    $disArr['ctn_id'] = $objCarton->ctn_id;
                    CycleDis::insert([$disArr]);

                    if ($isEach && $ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_ALIGN) {
                        break;
                    }
                }
            }
            else{
                $this->cloneCtn($ctn, $temp);
            }
        }

        return $errorMessage;
    }

    public function getMaxCtnNum($ctnNum)
    {
        $maxCtnNum = DB::table('cartons')->where('ctn_num', 'LIKE', substr($ctnNum, 0, strlen($ctnNum) - 6) . '%')->max('ctn_num');
        if ($maxCtnNum) {
            return $maxCtnNum;
        }

        return $ctnNum . '-C00';
    }

    private function createPalletNum()
    {
        $today_ddmm = date('dm');
        $palletCode = "LPN-TMP-" . $today_ddmm;

        $countPallet = \DB::table('pallet')->select('plt_num')
            ->where('plt_num', 'like', $palletCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();
        $max = 0;
        if ($countPallet) {
            $pltCode = array_get($countPallet, 'plt_num', null);
            $arrMax = explode('-', $pltCode);
            $max = (int)end($arrMax);
        }

        $pltNum = $palletCode . "-" . str_pad($max + 1, 5, "0", STR_PAD_LEFT);

        return $pltNum;
    }

    public function generatePltNum($prefix = "V") {
        $currentYearMonth = date('ym');
        $pallet = Pallet::where('plt_num', 'LIKE', '%-' . $currentYearMonth . '-%')
            ->orderBy('plt_id', 'DESC')
            ->first();

        if (!$pallet){
            return $prefix . '-' . $currentYearMonth . '-' . '000001';
        }

        $aNum = explode("-", $pallet['plt_num']);
        $aTemp = array_keys($aNum);
        $end = end($aTemp);
        $index = (int)$aNum[$end];
        $aNum[0] = $prefix;
        $aNum[$end] = str_pad(++$index, 6, "0", STR_PAD_LEFT);
        return implode("-", $aNum);
    }

    public function cloneCtn($ctn, $temp){
        $maxCtnNum = null;
        $userId = (int)Data::getCurrentUserId();
        $dataCtns = [];
        $cyclDtl = DB::table('cycle_dtl')->where('cycle_dtl_id', $ctn['cycle_dtl_id'])->first();
        $whsId = array_get($ctn, 'whs_id');
        $cusId = array_get($ctn, 'cus_id');
        $palletObj = DB::table('pallet')
            ->where('whs_id', $cyclDtl['whs_id'])
            ->where('cus_id', $cyclDtl['cus_id'])
            ->where('loc_id', $cyclDtl['act_loc_id'])
            ->where('deleted', 0)
            ->whereNotIn('plt_sts', ['CC', 'AJ'])
            ->first();
        $loc_code = array_get($cyclDtl, 'act_loc_name');
        $loc_id = array_get($cyclDtl, 'act_loc_id');

        if (!$palletObj) {
            // $pltNum = $this->palletModel->createPalletNum();
            $pltNum = $this->generatePltNum();
            $data = [
                'cus_id'       => $cusId,
                'whs_id'       => $whsId,
                'plt_num'      => $pltNum,
                'rfid'         => null,
                'created_at'   => time(),
                'created_by'   => $userId,
                'updated_at'   => time(),
                'updated_by'   => $userId,
                'deleted_at'   => 915148800,
                'deleted'      => 0,
                'ctn_ttl'      => 0,
                'dmg_ttl'      => 0,
                'init_ctn_ttl' => 0,
                'plt_sts'      => 'AC',
                'loc_id'       => $loc_id,
                'loc_code'     => $loc_code,
                'loc_name'     => $loc_code,
            ];
            $palletObj = Pallet::create($data);
        }
        $itemArrs = [];
        $itemIds = [];
        $itemQty = [];
        for($i = 1; $i <= $ctn['dicpy_qty']; $i++){
            $item_id = array_get($ctn, 'item_id');

            $item = array_get($itemArrs, $item_id);
            if (!in_array($item_id, $itemIds)) {
                $itemIds[] = $item_id;
                $item = DB::table('item')
                    ->where('item_id', $item_id)
                    ->first();
                $itemArrs[$item_id] = $item;
            }
//
//            if (!$maxCtnNum) {
//                $lastCtnNum = DB::table('cartons')
//                    ->where('whs_id', $whsId)
//                    ->where('cus_id', $cusId)
//                    ->where('deleted', 0)
//                    ->orderBy('ctn_id', 'DESC')
//                    ->first();
//
//                $lastCtnNum = array_get($lastCtnNum, 'ctn_num', 'CTN-000003AC8F27061301');
//                $maxCtnNum = $this->getMaxCtnNum($lastCtnNum);
//            }
            $rand = rand( $temp * 5, $temp * 10);
            $itemQty[$item_id][] = array_get($item, 'pack');
            $dataCtns[] = [
                'whs_id'        => $whsId,
                'cus_id'        => $cusId,
                'ctn_num'       => 'CYC-' . bin2hex(random_bytes(10)),
                'rfid'          => null,
                'ctn_sts'       => 'AC',
                'ctn_uom_id'    => array_get($item, 'uom_id'),
                'uom_code'      => array_get($item, 'uom_code'),
                'uom_name'      => array_get($item, 'uom_name'),
                'is_damaged'    => 0,
                'piece_remain'  => array_get($item, 'pack'),
                'piece_ttl'     => array_get($item, 'pack'),
                'created_at'    => time(),
                'updated_at'    => time(),
                'created_by'    => $userId,
                'updated_by'    => $userId,
                'deleted'       => 0,
                'deleted_at'    => '915148800',
                'is_ecom'       => 0,
                'picked_dt'     => 0,
                'item_id'       => $item_id,
                'sku'           => array_get($item, 'sku'),
                'size'          => array_get($item, 'size'),
                'color'         => array_get($item, 'color'),
                'lot'           => "NA",
                'ctn_pack_size' => array_get($item, 'pack'),
                'inner_pack'    => 0,
                'upc'           => array_get($item, 'cus_upc'),
                'length'        => array_get($item, 'length'),
                'width'         => array_get($item, 'width'),
                'height'        => array_get($item, 'height'),
                'weight'        => array_get($item, 'weight'),
                'cube'          => array_get($item, 'cube'),
                'volume'        => array_get($item, 'volume'),
                'des'           => array_get($item, 'description'),
                'loc_id'        => $loc_id,
                'loc_code'      => $loc_code,
                'loc_name'      => $loc_code,
                'loc_type_code' => "RAC",
                'plt_id'        => array_get($palletObj, 'plt_id'),
                'lpn_carton'    => $cyclDtl['plt_rfid']
            ];

            if (!in_array($item_id, $itemIds)) {
                $itemIds[] = $item_id;
                $item = DB::table('item')
                    ->where('item_id', $item_id)
                    ->first();
                $itemArrs[$item_id] = $item;
            }
        }

        foreach (array_chunk($dataCtns, 200) as $dataCtn) {
            DB::table('cartons')->insert($dataCtn);
        }

//        foreach ($itemArrs as $itemId => $item) {
//            $pieceTtl = array_sum(array_get($itemQty, $itemId));
//            $invt = $this->_insertInvtSmr($whsId, $item, 'NA', $pieceTtl);
//        }
    }

    /**
     * @param $arrCartons
     * @param $ctnId
     *
     * @return array
     */
    private function getCartonIdInArr($arrCartons, $ctnId)
    {
        foreach ($arrCartons as $carton) {
            if ($carton['ctn_id'] == $ctnId) {
                return $carton;
            }
        }

        return [];
    }


    /**
     * Get unclock itemIds
     *
     * @param $itemIds
     *
     * @return array. List valid itemIds to unclock.
     */
    public function getUnclockItemIds($cycleHdrId)
    {
        $result = [];

        if (!($cycleHdrId)) {
            return $result;
        }

        $data = CycleDtl::select('item_id')
            ->where('cycle_hdr_id', $cycleHdrId)
            ->whereNotIn('item_id', function ($query) use ($cycleHdrId) {
                $query->select('item_id')
                    ->from('cycle_dtl')
                    ->where('cycle_hdr_id', $cycleHdrId)
                    ->where('cycle_dtl_sts', '<>', CycleDtlModel::STATUS_CYCLEDTL_ACCEPTED);
            })
            ->distinct()->pluck('item_id')->toArray();

        return $data;

        if (!$data) {
            return $itemIds;
        }

        return $result;
    }

    /**
     * @param $arr
     * @param $model
     */
    private function setAttributeByArr($arr, &$model)
    {
        foreach ($arr as $key => $val) {
            $model->$key = $val;
        }
    }

    public function revertOrderToNew($whsId, $itemId, string $lot)
    {
        return DB::table('odr_hdr')
            ->join('odr_dtl', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->where([
                'odr_hdr.odr_sts' => 'AL',
                'odr_hdr.whs_id'  => $whsId,
                'odr_dtl.item_id' => $itemId,
                'odr_dtl.lot'     => $lot,
            ])
            ->update([
                'odr_hdr.odr_sts' => 'NW'
            ]);
    }

    /**
     * @param $whsId
     * @param $itemId
     * @param $lot
     * @param $discrepancy
     *
     * @return mixed
     */
    private function _insertInvtSmr($whsId, $itemArr, $lot, $pieceTtl= 0)
    {
        $itemId = array_get($itemArr, 'item_id');

        $ivt = InventorySummary::where('item_id', $itemId)
            ->where('lot', $lot)
            ->where('whs_id', $whsId)
            ->first();
        // ->toArray();

        if (!$ivt) {
            $insertInvtSmr = [
                'item_id'       => $itemId,
                'cus_id'        => array_get($itemArr, 'cus_id'),
                'whs_id'        => $whsId,
                'color'         => array_get($itemArr, 'color'),
                'sku'           => array_get($itemArr, 'sku'),
                'size'          => array_get($itemArr, 'size'),
                'lot'           => $lot,
                'ttl'           => $pieceTtl,
                'allocated_qty' => 0,
                'dmg_qty'       => 0,
                'avail'         => $pieceTtl,
                'back_qty'      => 0,
                'upc'           => array_get($itemArr, 'cus_upc'),
                'crs_doc_qty'   => 0,
                'created_at'    => time(),
                'updated_at'    => time(),
            ];
            $ivt = InventorySummary::create($insertInvtSmr);
        }

        return (object)$ivt;
    }

    public function updateInventory($row, $discrepancy, $dmgQty = 0)
    {
        $whsId = $row['whs_id'];
        $itemId = $row['item_id'];
        $lot = $row['lot'];

        $operator = '+';
        if ($row['act_qty'] - $row['sys_qty'] < 0) {
            $operator = '-';
        }

        //convert to positive number
        $discrepancy = abs($discrepancy);
        $invItem = $this->invSumModel->where(['whs_id' => $whsId, 'item_id' => $itemId, 'lot' => $lot])->first();
        if(!$invItem){
            $itemArr = DB::table('item')->where('item_id', $itemId)->where('deleted', 0)->first();
            $invItem = $this->_insertInvtSmr($whsId, $itemArr, $lot);
        }

        /*
         * 1. If $discrepancy > avail qty,
         * 1.1 avail = 0
         * 1.2 allocated_qty = allocated_qty - ($discrepancy - avail qty)
         * 2. update avail = $discrepancy
         * $discrepancy = 10
         * avail = 5, 0
         * allocated_qty = 15, 10
         */

        $availQty = $discrepancy;

        $availQtySql = sprintf('`avail` %s %d', $operator, $availQty);
        //$ttlQty = sprintf('`ttl` %s %d', $operator, $availQty + $dmgQty);
        $dmgQTYSql = sprintf('`dmg_qty` %s %d', $operator, $dmgQty);

        if ($invItem->avail < $discrepancy && $operator == '-') {
            $remainQty = -$discrepancy;
            // $orderRevertCancelList = $this->getRevertCancelOrders($invItem->item_id, $invItem->lot);

            $availQtySql = sprintf('IF(`avail` < %d, 0, `avail`  %s %d)',$availQty, $operator, $availQty);
            $ttlQty = sprintf('IF(`ttl` < %d, 0, `ttl`  %s %d)',$availQty + $dmgQty, $operator, $availQty + $dmgQty);
            $dmgQTYSql = sprintf('IF(`dmg_qty` < %d, 0, `dmg_qty`  %s %d)', $dmgQty, $operator, $dmgQty);
            // if (empty($orderRevertCancelList)) {

            //     $availQtySql = sprintf('IF(`avail` < %d, 0, `avail`  %s %d)',$availQty, $operator, $availQty);
            //     $ttlQty = sprintf('IF(`ttl` < %d, 0, `ttl`  %s %d)',$availQty + $dmgQty, $operator, $availQty + $dmgQty);
            //     $dmgQTYSql = sprintf('IF(`dmg_qty` < %d, 0, `dmg_qty`  %s %d)', $dmgQty, $operator, $dmgQty);

            // } else {
            //     //response error with orders list
            //     $itemInfo = [
            //         "sku"             => $invItem->sku,
            //         "size"            => $invItem->size,
            //         "color"           => $invItem->color,
            //         "lot"             => $invItem->lot,
            //         "available_qty"   => $invItem->avail,
            //         "discrepancy_qty" => $remainQty + $invItem->avail,

            //     ];
            //     $data = [
            //         "data" => [
            //             "item_info" => $itemInfo,
            //             "orders"    => $orderRevertCancelList
            //         ]
            //     ];
            //     Response::create($data, 405)->send();
            // }

            //
            // $allocQty = $invItem->allocated_qty - ($discrepancy - $invItem->avail);
            //$availQty = $invItem->avail;
            //$invItem->allocated_qty = $allocQty;
        }

        $res = $this->invSumModel->where(['whs_id' => $whsId, 'item_id' => $itemId, 'lot' => $lot])
            ->update([
                'avail'         => DB::raw($availQtySql),
                'ttl'           => DB::raw($availQtySql),
                'dmg_qty'       => DB::raw($dmgQTYSql),
                'allocated_qty' => $invItem->allocated_qty
            ]);
        $dataInv = $this->invSumModel->where(['whs_id' => $whsId, 'item_id' => $itemId, 'lot' => $lot])->first();
        $this->inventory->where(['whs_id' => $whsId, 'item_id' => $itemId])
            ->update([
                'in_hand_qty'         => array_get($dataInv, 'ttl', 0)
            ]);

        return $res;
    }


    public function generatePLNNum($plRfid)
    {
        $key = sprintf("%s", $plRfid) . '-%';
        $pallet = Pallet::select('plt_num')
            ->where('plt_num', 'LIKE', $key)
            ->orderBy('plt_num', 'DESC')
            ->first();

        $num = null;
        if (empty($pallet)) {
            $num = $plRfid . "-00000";
        } else {
            if ($pallet->ctn_ttl > 0) {
                throw new HttpException(404, "Pallet '$plRfid' was used.");
            }

            $num = $pallet->plt_num;
        }

        return $num;
    }

    public function createPallet($cycleDtl, $ccHdr)
    {
        $userId = Data::getCurrentUserId();
        $maxPltNum = $this->generatePLNNum($cycleDtl->plt_rfid);
        $maxPltNum++;
        $ctnTtl = $ccHdr['dicpy_qty'];

        return Pallet::create([
            'loc_id'       => $cycleDtl->act_loc_id,
            'loc_name'     => $cycleDtl->act_loc_name,
            'loc_code'     => $cycleDtl->act_loc_name,
            'whs_id'       => $cycleDtl->whs_id,
            'cus_id'       => $cycleDtl->cus_id,
            'plt_num'      => $maxPltNum,
            'rfid'         => $cycleDtl->plt_rfid,
            'created_at'   => time(),
            'updated_at'   => time(),
            'created_by'   => $userId,
            'updated_by'   => $userId,
            'lot'          => $cycleDtl->lot,
            'ctn_ttl'      => $ctnTtl,
            'init_ctn_ttl' => $ctnTtl
        ]);


    }

    public function getRevertCancelOrders($itemId, $lot)
    {
        $rs = OrderHdr::select([
            'odr_hdr.odr_id',
            'odr_hdr.odr_num',
            'odr_hdr.odr_sts',
            DB::raw("IF(odr_hdr.odr_sts = 'AL', 'Allocated', 'Picking') AS odr_sts_name"),
            'odr_dtl.sku',
            'odr_dtl.size',
            'odr_dtl.color',
            'odr_dtl.lot',
            DB::raw('SUM(odr_dtl.alloc_qty) AS alloc_qty'),
            DB::raw('SUM(odr_dtl.piece_qty) AS request_qty'),
            DB::raw('COALESCE( SUM(wv_dtl.act_piece_qty), 0 ) AS picked_qty'),
        ])
            ->join('odr_dtl', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->leftJoin('wv_dtl', function ($join) {
                $join->on('wv_dtl.wv_id', '=', 'odr_dtl.wv_id')
                    ->on('wv_dtl.item_id', '=', 'odr_dtl.item_id')
                    ->on('odr_dtl.lot', '=', 'wv_dtl.lot');
            })
            ->where([
                'odr_hdr.whs_id'  => Data::getCurrentWhsId(),
                'odr_dtl.deleted' => 0,
                'odr_dtl.item_id' => $itemId,
                'odr_dtl.lot'     => $lot,
            ])
            ->whereIn('odr_hdr.odr_sts', ['AL', 'PK'])
            ->groupBy('odr_dtl.odr_dtl_id')
            ->orderBy('alloc_qty')
            ->get()->toArray();

        return $rs;
    }

    public function createNewPallet($cycleDtl, $cycle_num, $ctnTtl)
    {
        $userId = Data::getCurrentUserId();
        $maxPltNum = $this->generatePLNNum($cycle_num);
        $maxPltNum++;

        return Pallet::create([
            'loc_id'     => $cycleDtl['act_loc_id'],
            'loc_name'   => $cycleDtl['act_loc_name'],
            'loc_code'   => $cycleDtl['act_loc_name'],
            'whs_id'     => $cycleDtl['whs_id'],
            'cus_id'     => $cycleDtl['cus_id'],
            'plt_num'    => $maxPltNum,
            'created_at' => time(),
            'updated_at' => time(),
            'created_by' => $userId,
            'updated_by' => $userId,
            'ctn_ttl'    => $ctnTtl
        ]);


    }

    public function updateInventoryEach($whsId, array $itemIds, array $lots)
    {
        foreach (array_chunk($itemIds, 200) as $chunkItemIds) {
            $this->invSumModel
                ->join(DB::raw('(
                SELECT item_id, lot,
                    SUM(IF(!is_damaged, piece_remain, 0)) AS c_avail,
                    SUM(IF(is_damaged, piece_remain, 0)) AS c_dmg_qty
                FROM cartons
                WHERE whs_id = ' . $whsId . '
                    AND item_id IN (' . implode(",", $chunkItemIds) . ')
                    AND lot IN (' . '\'' . implode("','", $lots) . '\'' . ')
                    AND ctn_sts IN (\'AC\', \'LK\')
                    AND loc_type_code != \'XDK\'
                GROUP BY item_id, lot
            ) t'), function ($join) {
                    $join->on('invt_smr.item_id', '=', 't.item_id')
                        ->on('invt_smr.lot', '=', 't.lot');
                })
                ->where('invt_smr.whs_id', $whsId)
                ->whereIn('invt_smr.item_id', $chunkItemIds)
                ->whereIn('invt_smr.lot', $lots)
                ->update([
                    'avail' => DB::raw('IF(t.c_avail < invt_smr.allocated_qty, 0, t.c_avail - invt_smr.allocated_qty)'),
                    // 'ttl'     => DB::raw('invt_smr.crs_doc_qty + invt_smr.allocated_qty + invt_smr.picked_qty + t.c_avail + t.c_dmg_qty'),
                    'ttl'     => DB::raw('invt_smr.crs_doc_qty + invt_smr.picked_qty + t.c_avail + t.c_dmg_qty'),
                    'dmg_qty' => DB::raw('t.c_dmg_qty')
                ]);
        }
    }
}