<?php

namespace App\Api\Gate\Controllers;

use App\Api\Gate\Models\GateModel;
use App\Api\Gate\Models\GateDtlModel;
use App\Api\Gate\Models\ContainerModel;
use App\Api\Gate\Models\CustomerModel;
use App\Api\Gate\Transformers\GateDtlTransformer;
use App\Api\Gate\Validators\GateDtlReportValidator;
use App\Api\Gate\Validators\GateDtlValidator;
use Illuminate\Support\Facades\DB;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;


/**
 * Class GateDtlController
 *
 * @package App\Api\Gate\Controllers
 */
class GateDtlController extends AbstractController
{

    protected $userId;
    /**
     * @var GateModel
     */
    protected $gateModel;
    /**
     * @var GateDtlModel
     */
    protected $gateDtlModel;
    /**
     * @var CustomerModel
     */
    protected $customerModel;
    /**
     * @var ContainerModel
     */
    protected $containerModel;

    /**
     * GateDtlController constructor.
     *
     * @param GateModel $gateModel
     * @param GateDtlModel $gateDtlModel
     * @param CustomerModel $customerModel
     * @param ContainerModel $containerModel
     */
    public function __construct(
        GateModel $gateModel,
        GateDtlModel $gateDtlModel,
        CustomerModel $customerModel,
        ContainerModel $containerModel
    ) {
        $this->gateModel = $gateModel;
        $this->gateDtlModel = $gateDtlModel;
        $this->customerModel = $customerModel;
        $this->containerModel = $containerModel;
        $this->userId = JWTUtil::getPayloadValue('jti') ? JWTUtil::getPayloadValue('jti') : 0;
    }

    /**
     * @param Request $request
     * @param GateDtlTransformer $gateDtlTransformer
     * @param GateDtlValidator $gateDtlValidator
     *
     * @return mixed
     */
    public function search(
        Request $request,
        GateDtlTransformer $gateDtlTransformer,
        GateDtlValidator $gateDtlValidator
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $gateDtlValidator->validate($input);

        try {

            $gateDtls = $this->gateDtlModel->search($input, [], array_get($input, 'limit', null));

            return $this->response->collection($gateDtls, $gateDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $ccId
     * @param GateDtlTransformer $gateDtlTransformer
     *
     * @return mixed
     */
    public function show($ccId, GateDtlTransformer $gateDtlTransformer)
    {
        try {
            $gateDtls = $this->gateDtlModel->getFirstWhere([
                'gate_dtl_id'  => $ccId,
                'gate_dtl_sts' => 'O'
            ], ['createdUser']);

            if (!$gateDtls) {
                throw new \Exception(Message::get("BM017", "Gate Detail"));
            }

            return $this->response->item($gateDtls, $gateDtlTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    /**
     * @param $gateId
     * @param Request $request
     * @param GateDtlValidator $gateDtlValidator
     *
     * @return $this|void
     * @throws \Exception
     */
    public function store(
        $gateId,
        Request $request,
        GateDtlValidator $gateDtlValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['gate_id'] = $gateId;
        $gateDtlValidator->validate($input);

        $gate = $this->gateModel->getGateByIds($gateId);
        if (empty($gate->gate_id)) {
            throw new \Exception(Message::get("BM017", "Gate"));
        }

        $customer = $this->customerModel->getCusByIds($input['cus_id']);
        if (empty($customer->cus_id)) {
            throw new \Exception(Message::get("BM017", "Customer"));
        }

        $ctnr = $this->containerModel->getFirstBy('ctnr_num', $input['ctnr_num']);

        $ctnrId = '';
        $ctnrNum = '';

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        try {
            DB::beginTransaction();

            if (empty($ctnr->ctnr_id)) {
                $container = $this->containerModel->create([
                    'ctnr_num' => $input['ctnr_num'],
                ]);

                $ctnrId = object_get($container, 'ctnr_id', null);
                $ctnrNum = object_get($container, 'ctnr_num', null);
            } else {
                $ctnrId = $ctnr->ctnr_id;
                $ctnrNum = $ctnr->ctnr_num;
            }

            $params = [
                'open'         => date('Y-m-d H:i:m', strtotime($input['open'])),
                'gate_dtl_sts' => array_get($input, 'gate_dtl_sts', 'O'),
                'duration'     => (int)array_get($input, 'duration', 0),
                'gate_id'      => $gate->gate_id,
                'gate_name'    => $gate->gate_name,
                'cus_id'       => $customer->cus_id,
                'cus_name'     => $customer->cus_name,
                'ctnr_id'      => $ctnrId,
                'ctnr_num'     => $ctnrNum,
                'whs_id'       => $currentWH
            ];

            $gate_sts = 'U';
            if (!empty($input['close'])) {
                $params['close'] = date('Y-m-d H:i:m', strtotime($input['close']));
            }

            $gateDtls = $this->gateDtlModel->getFirstWhere([
                'gate_id'      => $gateId,
                'gate_dtl_sts' => 'O'
            ], [], ['created_at' => 'desc']);

            if ($gateDtls) {
                $gateDtl = $this->gateDtlModel->updateWhere($params, ['gate_dtl_id' => $gateDtls['gate_dtl_id']]);
            } else {
                $gateDtl = $this->gateDtlModel->create($params);
            }

            if (array_get($input, 'gate_dtl_sts', null) === 'C') {
                $gate_sts = 'A';
            }
            $this->gateModel->refreshModel();
            $this->gateModel->updateWhere([
                'gate_sts' => $gate_sts
            ], ['gate_id' => $gateId]);

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'gate_dtl_id' => $gateDtls['gate_dtl_id']
                ]
            ])->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $ccId
     * @param Request $request
     * @param GateDtlValidator $gateDtlValidator
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function update(
        $ccId,
        Request $request,
        GateDtlValidator $gateDtlValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $gateDtls = $this->gateDtlModel->getFirstWhere([
            'gate_dtl_id'  => $ccId,
            'gate_dtl_sts' => 'O'
        ]);

        if (!$gateDtls) {
            throw new \Exception(Message::get("BM017", "Gate Detail"));
        }

        $input['gate_id'] = $gateDtls->gate_id;
        $gateDtlValidator->validate($input);

        $gate = $this->gateModel->getGateByIds($gateDtls->gate_id);
        if (empty($gate->gate_id)) {
            throw new \Exception(Message::get("BM017", "Gate"));
        }

        $customer = $this->customerModel->getCusByIds($input['cus_id']);
        if (empty($customer->cus_id)) {
            throw new \Exception(Message::get("BM017", "Customer"));
        }

        $ctnr = $this->containerModel->getFirstBy('ctnr_num', $input['ctnr_num']);
        $ctnrId = '';
        $ctnrNum = '';
        if (!empty($ctnr->ctnr_id)) {
            $ctnrId = $ctnr->ctnr_id;
            $ctnrNum = $ctnr->ctnr_num;
        }

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $paramsUpdate = [
            'open'         => date('Y-m-d H:i:m', strtotime($input['open'])),
            'temp'         => array_get($input, 'temp', null),
            'gate_dtl_sts' => array_get($input, 'gate_dtl_sts', 'O'),
            'duration'     => (int)array_get($input, 'duration', 0),
            'gate_id'      => $gate->gate_id,
            'gate_name'    => $gate->gate_name,
            'cus_id'       => $customer->cus_id,
            'cus_name'     => $customer->cus_name,
            'ctnr_id'      => $ctnrId,
            'ctnr_num'     => $ctnrNum,
            'whs_id'       => $currentWH
        ];

        if (!empty($input['close'])) {
            $paramsUpdate['close'] = date('Y-m-d H:i:m', strtotime($input['close']));
        }

        try {
            DB::beginTransaction();

            // Create container
            if (empty($ctnr->ctnr_id)) {
                $container = $this->containerModel->create([
                    'ctnr_num' => $input['ctnr_num'],
                ]);

                $paramsUpdate['ctnr_id'] = object_get($container, 'ctnr_id', null);
                $paramsUpdate['ctnr_num'] = object_get($container, 'ctnr_num', null);
            }

            // Update Gate Model
            $this->gateDtlModel->refreshModel();
            $this->gateDtlModel->updateWhere($paramsUpdate, ['gate_dtl_id' => $ccId]);

            if (array_get($input, 'gate_dtl_sts', null) === 'C') {
                $this->gateModel->refreshModel();
                $this->gateModel->updateWhere([
                    'gate_sts' => 'A'
                ], ['gate_id' => $gateDtls->gate_id]);

            }

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'gate_dtl_id' => $ccId
                ]
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param GateDtlTransformer $gateDtlTransformer
     * @param GateDtlReportValidator $gateDtlReportValidator
     *
     * @return Response|void
     */
    public function reportGateDtl(
        Request $request,
        GateDtlTransformer $gateDtlTransformer,
        GateDtlReportValidator $gateDtlReportValidator
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $gateDtlReportValidator->validate($input);

        try {

            $gateDtls = $this->gateDtlModel->report($input, [
                'gate',
                'customer',
                'warehouse'
            ], array_get($input, 'limit', 20), ['updated_at' => 'DESC']);

            return $this->response->paginator($gateDtls, $gateDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
