<?php

namespace Seldat\Wms2\Models;

class InvoiceHeader extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'inv_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'inv_id',
        'inv_num',
        'inv_dt',
        'inv_paid_dt',
        'inv_paid_sts',
        'inv_paid_ref',
        'inv_paid_typ',
        'inv_org',
        'inv_typ',
        'inv_cur',
        'inv_sts',
        'sts',
        'create_by',
        'update_by'
    ];

    public function invoiceDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\InvoiceDetail', 'inv_num', 'inv_num');
    }
    
    public function invHisRcv()
    {
        return $this->hasMany(__NAMESPACE__ . '\InvoiceReceivingHistory', 'inv_id', 'inv_id');
    }
    
    public function invHisOrdPrc()
    {
        return $this->hasMany(__NAMESPACE__ . '\InvoiceOrdProcHistory', 'inv_id', 'inv_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }
}

