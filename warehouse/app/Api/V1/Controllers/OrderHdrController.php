<?php
/**
 * Created by PhpStorm.
 *
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Transformers\OrderReportTransformer;
use App\Api\V1\Transformers\OrderHdrTransformer;
use Illuminate\Http\Request as IRequest;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

/**
 * Class OrderHdrController
 *
 * @package App\Api\V1\Controllers
 */
class OrderHdrController extends AbstractController
{

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;


    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    protected $statusType = [
        'csr'               => ['NW'],
        'allocate'          => ['NW', 'AL', 'PAL'],
        'wave-pick'         => ['PK', 'PPK', 'PD', 'PIP', 'AL', 'PAL'],
        'packing'           => ['PD', 'PIP', 'PN', 'PPA', 'PA', 'PAP'],
        'pallet-assignment' => ['PA', 'PAP'],
        'cancel-order'      => ['CC']
    ];

    /**
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel

    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
    }

    /**
     * @param Request $request
     * @param $warehouseId
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return Response|void
     */
    public function shippingReport(
        Request $request,
        $warehouseId,
        OrderHdrTransformer $orderHdrTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $orderHdr = $this->orderHdrModel->shippingReport($warehouseId, $input, [
                'customer',
                'OrderDtl',
                'updatedBy'
            ], array_get($input, 'limit'));

            return $this->response->paginator($orderHdr, $orderHdrTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $warehouseId
     * @param OrderHdrTransformer $orderReportTransformer
     *
     * @return Response|void
     */
    public function orderReport(
        Request $request,
        $warehouseId,
        OrderReportTransformer $orderReportTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $orderHdr = $this->orderHdrModel->orderReport($warehouseId, $input, [
                'customer',
                'OrderDtl'
            ], array_get($input, 'limit'));

            return $this->response->paginator($orderHdr, $orderReportTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|null|string
     */
    public function orderStatusNotShipping(Request $request)
    {
        $input = $request->getQueryParams();
        $types = Status::get("Order-status");
        $types = array_diff_key($types,['SH' => 'Shipped', 'ST'=> 'Staging', 'PSH'=> 'Partial Staging']);
        //$orderStatusCurrent = $this->orderHdrModel->orderStatus();
        $data = [];
        if (!empty($types) && is_array($types)) {
            foreach ($types as $key => $value) {
                if (!empty($input['type']) && !empty($this->statusType[$input['type']]) &&
                    !in_array($key, $this->statusType[$input['type']])
                ) {
                    continue;
                }
                $data[] = [
                    "key"   => $key,
                    "value" => $value,
                ];
            }
        }

        return ['data' => $data];
    }

}
