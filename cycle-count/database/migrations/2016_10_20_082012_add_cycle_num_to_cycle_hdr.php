<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCycleNumToCycleHdr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cycle_hdr')) {
            Schema::table('cycle_hdr', function (Blueprint $table) {
                $table->string('cycle_num', 50)->after('cycle_name')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cycle_hdr')) {
            Schema::table('cycle_hdr', function (Blueprint $table) {
                $table->dropColumn('cycle_num');
            });
        }
    }
}
