<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\BaseInvoiceModel;
use Illuminate\Support\Facades\DB;

class CustomerSac extends BaseInvoiceModel
{
    protected $table = 'cus_sac';
    protected $primaryKey = 'cus_sac_id';
    protected $fillable = [
        'cus_id',
        'whs_id',
        'sac_id',
        'sac',
        'sac_name',
        'description',
        'type',
        'range_from',
        'range_to',
        'range_uom',
        'period',
        'period_uom',
        'block',
        'price',
        'discount',
        'unit_uom',
        'sac_period',
    ];
    protected $visible = [
        'cus_sac_id', 
        'sac_id', 
        'sac', 
        'sac_name', 
        'description', 
        'type', 
        'range_from', 
        'range_to', 
        'range_uom', 
        'period_uom', 
        'block', 
        'price',
        'discount',
        'unit_uom',
        'sac_period'
    ];

    public function sacModel()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Sac', 'sac_id', 'sac_id');
    }

    public function getBasePriceAttribute(Type $var = null)
    {
        return $this->sacModel->price ?? '';
    }

    public function getByCusSacId($cusSacId) {
        return DB::table('cus_sac')
            ->where('cus_sac.cus_sac_id', $cusSacId)
            ->where('deleted',0)
            ->first();
    }
}
