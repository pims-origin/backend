<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\PermissionService;
use App\Api\V1\Models\RoleService;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Models\UserService;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Validator;

class UserController extends AbstractController
{
    protected $service;

    protected $model;

    protected $roleService;

    protected $permissionService;

    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new UserService($request, $this->userId);
        $this->roleService = new RoleService($request);
        $this->permissionService = new PermissionService($request);
        $this->model = new UserModel();
    }

    public function getList(IRequest $request)
    {
        $this->checkPermission($request, 'viewUser');
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getUserListByQueryString($queryStr);
        return $this->convertResponse($result);
    }

    public function getUserDetail(IRequest $request, $userId)
    {
        $this->checkPermission($request, 'viewUser');
        $result = $this->service->getUser($userId);
        return $this->convertResponse($result);
    }

    public function createUser(IRequest $request)
    {
        $this->checkPermission($request, 'createUser');
        $result = $this->service->createUser($request->getParsedBody());
        $response = $this->convertResponse($result);
        return $response;
    }

    public function updateUser(IRequest $request, $userId)
    {
        $user = $this->service->getUserData($userId);
        if (empty($user)) {
            return [];
        }
        $this->checkPermission($request, 'editUser', $user);
        $result = $this->service->updateUser($userId, $request->getParsedBody());
        $response = $this->convertResponse($result);
        return $response;
    }

    public function deleteUser(IRequest $request, $userId)
    {
        $user = $this->service->getUser($userId);
        if (empty($user)) {
            return [];
        }
        $this->checkPermission($request, 'deleteUser', $user);
        $result = $this->service->deleteUser($userId);
        $response = $this->convertResponse($result);
        return $response;
    }

    public function deleteMultipleUser(IRequest $request)
    {
        $this->checkPermission($request, 'deleteUser');
        $body = $request->getParsedBody();
        $result = $this->service->deleteMultipleUser($body);
        $response = $this->convertResponse($result);
        return $response;
    }

    public function getRoleByUser(IRequest $request, $userId)
    {
        $this->checkPermission($request, 'viewRole');
        $queryStr = $request->getUri()->getQuery();
        $result = $this->roleService->getRoleByUser($userId, $queryStr);
        return $this->convertResponse($result);
    }

    public function getRoleByLoggedUser(IRequest $request)
    {
        $this->checkPermission($request, 'viewRole');
        $queryStr = $request->getUri()->getQuery();
        $result = $this->roleService->getRoleByUser($this->userId, $queryStr);
        return $this->convertResponse($result);
    }

    public function getPermissionByLoggedUser(IRequest $request)
    {
        $result = $this->permissionService->getPermissionByUser($this->userId);
        return $this->convertResponse($result);
    }

    public function assignUser(IRequest $request, $id)
    {
        $this->checkPermission($request, 'assignRoleToUser');
        $body = $request->getParsedBody();
        $result = $this->roleService->assignUser($id, $this->userId, $body);
        return $this->convertResponse($result);
    }

    public function changePassword(IRequest $request)
    {
        $body = $request->getParsedBody();

        $result = $this->service->changePassword($this->userId, $body);
        return $this->convertResponse($result);
    }

    public function getProfile(IRequest $request)
    {
        $result = $this->service->getProfile($this->userId);
        return $this->convertResponse($result);
    }

    public function updateProfile(IRequest $request)
    {
        $body = $request->getParsedBody();
        $result = $this->service->updateProfile($this->userId, $body);
        return $this->convertResponse($result);
    }
}
