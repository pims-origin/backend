<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 12-Oct-16
 * Time: 09:34
 */

namespace Seldat\Wms2\Models;

class XferTicket extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'xfer_ticket';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xfer_ticket_id';

    /**
     * @var array
     */
    protected $fillable = [
        'xfer_ticket_num',
        'whs_id',
        'xfer_ttl',
        'xfer_ticket_sts',
        'sts'
    ];

    public function xferHdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\XferHdr', 'xfer_ticket_id', 'xfer_ticket_id');
    }

    public function xferDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\XferDtl', 'xfer_ticket_id', 'xfer_ticket_id');
    }
}
