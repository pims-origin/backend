<?php

namespace backend\forms;

use common\models\ResetPassword;
use Yii;
use common\models\User;

class UpdatePasswordForm extends Form
{
    const PASSWORD_MIN_LENGTH = 8;

    public $new_password;
    public $repeat_new_password;
    public $user_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'integer'],
            ['new_password', 'validatePassword'],
            [['new_password', 'repeat_new_password'], 'required'],
            ['repeat_new_password', 'compare', 'compareAttribute' => 'new_password']
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (strlen($this->new_password) < self::PASSWORD_MIN_LENGTH) {
            $this->addError($attribute, 'New password should be at least ' . self::PASSWORD_MIN_LENGTH . ' length.');
        }
        if (!preg_match("/([A-Z])/", $this->new_password)) {
            $this->addError($attribute, 'New password should be at one Upper case character.');
        }

        if (!preg_match("/([0-9])/", $this->new_password)) {
            $this->addError($attribute, 'New password should be at one number.');
        }

        if (!preg_match("/([a-z])/", $this->new_password)) {
            $this->addError($attribute, 'New password should be at one lower case character.');
        }

        if (!preg_match('/[\!@#\$%\^&\*\`~\(\)\{\}\[\]\,\?\=+\-_\.:;\<\>]/', $this->new_password)) {
            $this->addError($attribute, 'New password should be at one symbol character.');
        }

        if (strpos($this->new_password, " ") !== false) {
            $this->addError($attribute, 'User can not use space character in password');
        }

        //  Check new password
        $security = Yii::$app->security;
        $user = User::find()->where([
            'user_id' => $this->user_id
        ])->one();
        $verify = $security->validatePassword($this->new_password, $user->password);
        if ($verify) {
            $this->addError($attribute, 'Your new password must not be the same to the previous password.');
        }
    }

    public function updatePassword(User $user)
    {
        $user->setPassword($this->new_password);
        $user->updated_at = time();
        $result = $user->save();

        return $result;
    }

    public function updateResetPassword(ResetPassword $resetPassword)
    {
        $resetPassword->updated_at = date('Y-m-d H:i:s');
        $result = $resetPassword->save();

        return $result;
    }
}
