<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;
use GuzzleHttp\Client;

abstract class Job implements ShouldQueue
{
    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "queueOn" and "delay" queue helper methods.
    |
    */

    use InteractsWithQueue, Queueable, SerializesModels;

    public function writeLogment($title, $content)
    {
        DB::table('sys_bugs')->insert([
            'date'       => time(),
            'api_name'   => $title,
            'error'      => $content,
            'created_at' => time(),
            'created_by' => null,
            'updated_at' => time()
        ]);
    }

    public function wmsLogin()
    {
        $apiLogin = env('API_LOGIN');
        $userLogin = env('USER_LOGIN');
        $pwdLogin = env('PWD_LOGIN');

        if(empty($apiLogin) || empty($userLogin) || empty($pwdLogin)) {
            $this->writeLogment('Authentication', "API Login is not define!");
            return false;
        }

        $client = new Client();

        try {
            $response = $client->request('POST', $apiLogin,
                [
                    'form_params' => ["username" => $userLogin, "password" => $pwdLogin]
                ]
            );
            $content = $response->getBody()->getContents();

            $token = json_decode($content)->data->token;

            return $token;
        } catch (\Exception $e) {
            $this->writeLogment('Authentication', $e->getMessage());
        }
    }
}
