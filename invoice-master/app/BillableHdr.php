<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;

class BillableHdr extends BaseInvoiceModel
{
    protected $table = 'billable_hdr';
    protected $primaryKey = 'ba_id';
    protected $fillable = [
        'ref_num',
        'ba_from',
        'ba_to',
        'ba_ttl',
        'amount_ttl',
        'ba_type',
        'ba_period',
        'cus_id',
        'whs_id',
    ];

    public function detail()
    {
        return $this->hasMany(__NAMESPACE__ . '\BillableDtl', 'ba_id');
    }
}
