<?php

namespace App\Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    public function toArray($request)
    {
        if ($this->odrHdr->odr_type == 'EC')
        {
            $trackingNum = $this->odrHdr->packHdr->pluck('tracking_number');
            $trackingNum = $trackingNum->implode(',');
        }
        else
        {
            $trackingNum = $this->ship_track_id;
        }

        return [
            'sku'               => $this->sku,
            'description'       => $this->des,
            'lot'               => $this->lot,
            'uom'               => $this->sysUom->sys_uom_name,
            'qty'               => $this->qty,
            'tracking_num'      => $trackingNum,
            'meta'              => $this->odrDtlMeta ? json_decode($this->odrDtlMeta->value) : NULL,
        ];
    }
}