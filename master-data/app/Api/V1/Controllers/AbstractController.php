<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class BaseController
 * @package App\Api\V1\Controllers
 *
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="api.domain",
 *     basePath="/",
 *     definitions={},
 *     @SWG\Info(
 *         version="WMS 2.0.0",
 *         title="Your API Functional Name",
 *         description="Description for APIs",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="your@email.address"
 *         ),
 *         @SWG\License(
 *             name="WMS2.0",
 *             url="seldatinc.com"
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="data",
 *         type="object",
 *         properties={
 *             @SWG\Property(property="menu_group_id", type="integer"),
 *             @SWG\Property(property="name", type="string"),
 *             @SWG\Property(property="description", type="string"),
 *         },
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */
abstract class AbstractController extends Controller
{
    use Helpers;

    function __construct()
    {

    }

    function download_csv_results($results, $name = NULL, $header = null)
    {
        if( ! $name)
        {
            $name = md5(uniqid() . microtime(TRUE) . mt_rand()). '.csv';
        }

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename='. $name);
        header('Pragma: no-cache');
        header("Expires: 0");

        $outstream = fopen("php://output", "w");

        if ($header) {
            fputcsv($outstream, $header);
        }

        foreach($results as $result)
        {
            fputcsv($outstream, $result);
        }

        fclose($outstream);
        exit;
    }

    public function download($data, $ext = 'csv')
    {
        Excel::create('Errors', function($excel) use ($data) {
            $excel->sheet('Error', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($ext);
    }
}
