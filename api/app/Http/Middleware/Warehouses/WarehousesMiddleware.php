<?php

namespace App\Http\Middleware\Warehouses;

use App\Http\Middleware\Customers\Customer;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class WarehousesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        $whsId = array_get($request->route()[2], 'whsId', null);
        if($whsId != null && is_numeric($whsId) == false) {
            $whsCode = mb_strtoupper($whsId);
            $modelCus = new Warehouse();
            $whsId = $modelCus->where('whs_code', $whsCode)
                ->where('deleted' , 0)
                ->select('whs_id')
                ->first();
            $whsId = array_get($whsId, 'whs_id', 1);
            $request->merge(['whs_id' => $whsId]);
        }

        //middleware whs of user
        try {
            $userId = Config::get('data.userId');
            //get list
            // $cusExUsModel = new CusExUser();
            $userWhsModel = new UserWhs();
            $modelWhs = new Warehouse();

            $listData =  DB::table($userWhsModel->getTable())
                ->join($modelWhs->getTable(), $modelWhs->getTable() . '.whs_id', '=', $userWhsModel->getTable() .'.whs_id')
                ->where($userWhsModel->getTable() . '.user_id',$userId)
                ->where($modelWhs->getTable(). '.deleted' , 0)
                ->select($userWhsModel->getTable() . '.whs_id')
                ->groupBy($userWhsModel->getTable() . '.whs_id')
                ->get();
            Config::set('data.whsOfUser', $listData);
        }
        catch (\Exception $e) {
            throw new \Exception('Unauthorized');
        }
        $response = $next($request);

        //response time
        $lumen_end = microtime(true);
        LogsResponseTime::create([
            'rpt_url'           => $request->url(),
            'user_id'       => Config::get('data.userId'),
            'rpt_start'         => LUMEN_START,
            'rpt_end'           => $lumen_end,
            'rpt_time'          => $lumen_end - LUMEN_START
        ]);
        return $response;
    }
}
