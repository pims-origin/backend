<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\Zone;
use Seldat\Wms2\Utils\SelArr;
use Wms2\UserInfo\Data;

class LocationModel extends AbstractModel
{
    /**
     * ItemModel constructor.
     *
     * @param Item|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    /**
     * @param $cus_id
     * @param $whs_id
     * @param array $exceptId
     *
     * @return mixed
     */
    public function loadSuggestedLocation($cus_id, $whs_id, $exceptId = [])
    {
        $query = $this->model->select(['loc_id', 'loc_code', 'loc_alternative_name'])
            ->whereHas('customerZone', function ($q) use ($cus_id) {
                $q->where('cus_id', $cus_id);
            })
            ->where('loc_type_id', 3)
            ->where('loc_sts_code', config('constants.location_status.ACTIVE'))
            ->where('loc_whs_id', $whs_id);

        if (!empty($exceptId)) {
            $query->whereNotIn('loc_id', $exceptId);
        }

        return $query->get();
    }

    /**
     * @param $cus_id
     * @param $whs_id
     * @param $exceptIds
     * @param $limit
     *
     * @return mixed
     */
    public function getSuggestedLocations($cusId, $whs_id, $limit)
    {
        $query = $this->model
            ->select(['location.loc_id', 'location.loc_code', 'location.loc_alternative_name'])
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->leftJoin('pallet', 'pallet.loc_id', '=', 'location.loc_id')

            ->where('loc_type_id', 3)
            ->where('loc_sts_code', config('constants.location_status.ACTIVE'))
            ->where('loc_whs_id', $whs_id)
            ->where([
                'customer_zone.cus_id' => $cusId,
                'pallet.plt_id'        => null
            ]);
        //#6091 task 
        $query = $query->orderBy('location.aisle', 'ASC')
            ->orderBy('location.level', 'ASC')
            ->orderBy('location.row', 'ASC')
            ->orderBy('location.bin', 'ASC');


        $locs = $query->take($limit)->get();

        return $locs;
    }

    /**
     * @param array $loc_id
     */
    public function countLocationNotHasStatus(array $loc_id, $sts_code)
    {

        return $this->model->where('loc_sts_code', '<>', $sts_code)->whereIn('loc_id', $loc_id)->count();
    }

    /**
     * @param array $loc_id
     */
    public function updateLocStatusWhereIn(array $loc_id, $sts_code)
    {
        return $this->model->whereIn('loc_id', $loc_id)->update(['loc_sts_code' => $sts_code]);
    }

    /**
     * @param array $loc_id
     */
    public function getLocationByCus($whsId, $locIds, $cusId)
    {
        $locIds = is_array($locIds) ? $locIds : [$locIds];

        return $this->model
            ->select('loc_code', 'loc_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->where('cus_id', $cusId)
            ->where('loc_whs_id', $whsId)
            ->whereIn('loc_id', $locIds)->get();
    }

    /**
     * @param array $loc_id
     */
    public function getLocationByIds($locIds)
    {
        $locIds = is_array($locIds) ? $locIds : [$locIds];

        return $this->model
            ->select('loc_code', 'loc_id')
            ->whereIn('loc_id', $locIds)->get();
    }

    /**
     * @param array $loc_id
     */
    public function checkLocationsByCus($locId, $whsId, $cusIds)
    {
        $cusIds = is_array($cusIds) ? $cusIds : [$cusIds];

        return $this->model
            ->select('loc_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->whereIn('cus_id', $cusIds)
            ->where('loc_whs_id', $whsId)
            ->where('loc_id', $locId)->first();
    }

    /**
     * @param array $loc_id
     */
    public function getCusIdsByZoneIds($zoneIds)
    {
        $zoneIds = is_array($zoneIds) ? $zoneIds : [$zoneIds];
        $cusIds = DB::table('customer_zone')
            ->select('cus_id')
            ->whereIn('zone_id', $zoneIds)->get();

        return $cusIds;
    }

    /**
     * @param array $loc_id
     */
    public function getCusIdsByLocIds($whsId, $locId)
    {
        return $this->model
            ->select('cus_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->where('loc_whs_id', $whsId)
            ->where('loc_id', $locId)->get();
    }

    /**
     * @param $locCode
     *
     * @return bool
     */
    public function isLocCode($locCode)
    {
        $locCode = trim($locCode);
        $locCode = str_replace(" ", "", $locCode);
        $locCode = strtoupper($locCode);
        $locCodes = explode("-", $locCode);
        $locCodes = array_filter($locCodes);
        if (count($locCodes) != 4) {
            return false;
        }

        foreach ($locCodes as $locItem) {
            if (preg_match("/^[a-zA-Z0-9]+$/", $locItem) != 1) {
                return false;
            }

            if (strlen($locItem) > 10) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $attributes
     *
     * @return mixed
     */
    public function getUnAvailableLocation($attributes)
    {
        $query = $this->model->select('loc_id')
            ->whereNull('loc_zone_id')
            ->orWhere(function ($q) {
                $q->where('loc_sts_code', '!=', "AC");
                $q->where('loc_sts_code', '!=', "LK");
                $q->where('loc_sts_code', '!=', "IA");
            });

        if (!empty($attributes['whs_id'])) {
            $query->orWhere('loc_whs_id', '!=', $attributes['whs_id']);
        }

        if (!empty($attributes['loc_zone_id'])) {
            $query->where('loc_zone_id', '!=', $attributes['loc_zone_id']);;
        }

        $result = $query->get();

        return $result;
    }

    /**
     * @param array $where
     * @param array $with
     * @param int $limit
     *
     * @return mixed
     */
    public function getAll($where = [], $with = [], $limit = 0)
    {
        $query = $this->make($with);

        if (empty($where)) {
            return $query->get();
        }

        foreach ($where as $column => $value) {
            if (is_array($value)) {
                reset($value);
                $first = key($value);
                switch ($first) {
                    case "in":
                        $query->whereIn(DB::raw($column), $value[$first]);
                        break;
                    case "not in":
                        $query->whereNotIn(DB::raw($column), $value[$first]);
                        break;
                    default:
                        $query->where(DB::raw($column), $first, $value[$first]);
                }

            } else {
                $query->where($column, $value);
            }
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query->get();
    }

    public function getDynZone($whsId, $cusId)
    {
        $customerObj = Customer::where('cus_id', $cusId)->first();

        $zoneCode = $customerObj->cus_code . "-D";
        $zoneName = $customerObj->cus_code . " - Dynamic Zone";
        $zoneObj = Zone::where('zone_code', $zoneCode)->where("zone_whs_id", $whsId)->first();

        if ($zoneObj) {
            $zoneObj->zone_num_of_loc += 1;
            $zoneObj->save();

            return $zoneObj->zone_id;
        }

        $colorCode = DB::table('customer_color')->where('cus_id', $cusId)
            ->value('cl_code');

        $arrData = [
            "zone_name"       => $zoneName,
            "zone_code"       => $zoneCode,
            "zone_whs_id"     => $whsId,
            "zone_type_id"    => 5,
            "dynamic"         => 1,
            "zone_min_count"  => 1,
            "zone_max_count"  => 100,
            "zone_color"      => $colorCode ?? '#ffffff',
            "zone_num_of_loc" => 1,
            "created_at"      => time(),
            "updated_at"      => time(),
            "created_by"      => Data::getCurrentUserId(),
            "updated_by"      => Data::getCurrentUserId(),
            "deleted_at"      => 915148800,
            "deleted"         => 0
        ];

        $dZone = DB::table('zone')
            ->where('zone_name', $zoneName)
            ->where('zone_whs_id', $whsId)
            ->where('zone_type_id', 5)
            ->where('dynamic', 1)
            ->where('deleted', 0)
            ->first();

        if(empty($dZone)) {
            $zoneId = DB::table('zone')->insertGetId($arrData);

            //add customer_zone
            DB::table('customer_zone')->insert([
                'zone_id'    => $zoneId,
                'cus_id'     => $cusId,
                'created_at' => time(),
                'updated_at' => time(),
                "created_by" => Data::getCurrentUserId(),
                "updated_by" => Data::getCurrentUserId(),
                "deleted_at" => 915148800,
                'deleted'    => 0
            ]);
        }else{
            $zoneId = $dZone['zone_id'];
        }
        

        return $zoneId;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param int $limit
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = 20)
    {
        $query = $this->make($with)->select([$this->model->getTable().'.*']);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query = $query->whereNull('parent_id');

        if (isset($attributes['loc_lpn_carton'])) {
            $query = $query->where('loc_code', 'LIKE', '%'.$attributes['loc_lpn_carton'] . '%');
        }

        $query = $query->where('loc_sts_code', 'AC');

        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query);

        $models = $query->paginate($limit);

        return $models;
    }

}