<?php

/*INBOUND PROCESS*/
/*$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/inbound/scan-pallet', 'namespace' => 'App\Api\V2\Inbound\ScanPallet\Controllers'], function ($api) {
    $api->put('/assign-carton-to-pallet',
        ['uses' => 'PalletController@assignCartonsToPallet']);
});*/

$api->group(['prefix' => '/inbound/whs/{whsId:[0-9]+}/', 'namespace' => 'App\Api\V2\Inbound\ScanPallet\Controllers'], function ($api) {
    $api->put('/scan-pallet',
        ['uses' => 'PalletController@assignCartonsToPallet']);
});

$api->group(['prefix' => '/inbound/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/', 'namespace' => 'App\Api\V2\Inbound\ScanCartons\Controllers'], function ($api) {
    $api->post('/add-virtual-cartons/mess-queue',
        ['action' => "scanCartons", 'uses' => 'CartonsController@scanCartons']);
});

$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/inbound/scan-cartons', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
    $api->put('/scan-pallet',
        ['action' => "saveAssignPallet", 'uses' => 'CartonsController@assignCartonsToPallet']);
});

// search list
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/', 'namespace' => 'App\Api\V2\Inbound\Containers\Controllers'], function ($api) {
    $api->get('/containers',
        [
            'action' => "viewContainer",
            'uses'   => 'ContainerController@search'
        ]
    );
    $api->get('/skus',
        [
            'action' => "viewSku",
            'uses'   => 'ContainerController@searchSku'
        ]
    );

    // customer list
    $api->get('/cus-list',
        [
            'action' => "viewCus",
            'uses'   => 'CustomerWarehouseController@loadBy'
        ]
    );

});

// wave pick
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/wave', 'namespace' => 'App\Api\V2\Outbound\Wavepick\Controllers'], function ($api) {
    $api->put('/{wvId:[0-9]+}/update',
        [
            'action' => "updateWavepick",
            'uses'   => 'WavepickController@updateWavePick'
        ]
    );

    $api->get('/list',
        [
            'action' => "updateWavepick",
            'uses'   => 'WavepickController@getWavePickList'
        ]
    );

});

// wave pick
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/wave-detail', 'namespace' => 'App\Api\V2\Outbound\Wave'], function ($api) {

    // get pallet location
    $api->get('/sku/{wvDtlID: [0-9]+}',
        [
            'action' => "updateWavepick",
            'uses'   => 'GetWavePickDetail\Controllers\WavePickController@getSuggestPalletLocation'
        ]
    );

    // get pallet location for next sku
    $api->get('/{wv_id:[0-9]+}/next-sku/{current_wv_dtl_id:[0-9]+}',
        [
            'action' => "updateWavepick",
            'uses'   => 'GetWavePickDetail\Controllers\WavePickController@getNextSKU'
        ]
    );

    // get more suggest location
    $api->post('/{wv_dtl_id:[0-9]+}/more-location',
        [
            'action' => "updateWavepick",
            'uses'   => 'GetMoreLocation\Controllers\LocationController@moreLocation'
        ]
    );

    // get more suggest location
    $api->post('/{wv_dtl_id:[0-9]+}/location-info',
        [
            'action' => "updateWavepick",
            'uses'   => 'GetMoreLocation\Controllers\LocationController@locationInfo'
        ]
    );

});

// Wave pick without RFID
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/wave-without-rfid', 'namespace' => 'App\Api\V2\Outbound\WavepickWithoutRfid'], function ($api) {
    $api->put('/update',
        [
            'action' => "updateWavepick",
            'uses'   => 'UpdateWavepick\Controllers\WavepickController@updateWavePick'
        ]
    );

});

// Order
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/order', 'namespace' => 'App\Api\V2\Outbound\Order'], function ($api) {
    $api->put('/{odrId:[0-9]+}/cartons',
        [
            'action' => "assignCartonsToOrder",
            'uses'   => 'AssignCartonsToOrder\Controllers\OrderController@putCartonOrder'
        ]
    );
    // Assign Pieces to Order with Barcode
    $api->put('/{odrId:[0-9]+}/cartons-without-rfid',
        [
            'action' => "assignCartonsToOrder",
            'uses'   => 'AssignBarcodeToOrder\Controllers\OrderController@putBarcodeOrder'
        ]
    );
    // Assign Cartons to Order with Barcode
    $api->put('/{odrId:[0-9]+}/barcodes',
        [
            'action' => "assignCartonsToOrder",
            'uses'   => 'AssignCartonsWithBarcode\Controllers\OrderController@putCartonsWithBarcode'
        ]
    );

    $api->get('/list-assign-carton',
        [
            'action' => "viewOrder",
            'uses'   => 'GetOrderListAssignCarton\Controllers\OrderController@getListOrder'
        ]
    );

    $api->get('/status-filter',
        [
            'action' => "viewOrder",
            'uses'   => 'GetOrderListStatusFilter\Controllers\OrderController@getListStatusFilter'
        ]
    );

    $api->get('/sku-by-rfid/{rfid}',
        [
            'action' => "viewOrder",
            'uses'   => 'GetSkuByRfid\Controllers\CartonController@getSku'
        ]
    );

    $api->post('/sku-and-order-info',
        [
            'action' => "viewOrder",
            'uses'   => 'GetSkuAndOrder\Controllers\CartonController@getSku'

        ]
    );

    $api->post('/get-lpn-by-order-no-tag',
        [
            'action' => "viewOrder",
            'uses'   => 'GetSkuAndOrderNoTag\Controllers\CartonController@getLpn'

        ]
    );

    $api->post('/validate-pack-carton',
        [
            'action' => "viewOrder",
            'uses'   => 'ValidatePackCarton\Controllers\CartonController@validateCartons'
        ]
    );

    $api->put('/{odrId:[0-9]+}/pieces-with-rfid',
        [
            'action' => "assignCartonsToOrder",
            'uses'   => 'AssignPiecesToOrderWithRfid\Controllers\OrderController@putPiecesToOrder'
        ]
    );
});

// Scanned pallet migration inventory
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/pallet', 'namespace' => 'App\Api\V2\Outbound\ScannedPallet\Controllers'], function ($api) {
    $api->post('/scanned-pallet-migrate',
        [
            'action' => "assignCartonsToPallet",
            'uses'   => 'PalletController@scannedPallet'
        ]
    );
    // search list customer by warehouse
    $api->get('/customer-list',
        [
            'action' => "assignCartonsToPallet",
            'uses'   => 'PalletController@customerList'
        ]
    );
});


// Order
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}', 'namespace' => 'App\Api\V2\Outbound\Item\Controllers'], function ($api) {

    $api->get('/{cusId:[0-9]+}/item/search/sku',
        [
            'action' => "assignCartonsToOrder",
            'uses'   => 'ItemController@getItemSku'
        ]
    );

    $api->get('/item/search/upc',
        [
            'action' => "assignCartonsToOrder",
            'uses'   => 'ItemController@getItemUpc'
        ]
    );
});

//Pallet
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/inbound', 'namespace' => 'App\Api\V2\Inbound\Pallet\Controllers'], function ($api) {
    $api->get('/pallet/{rfid}',
        [
            'action' => "viewPallet",
            'uses'   => 'PalletController@getInfoPallet'
        ]
    );
});

// Pallet
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/ib/pallet', 'namespace' => 'App\Api\V2\Inbound\Pallet2'], function ($api) {
    $api->put('/scan-with-sku',
        ['uses' => 'ScanPalletOptionYes\Controllers\PalletController@assignCartonsToPallet']);

    $api->get('/info/{rfid}',
        ['uses' => 'GetInfoByRfid\Controllers\PalletController@getInfoPallet']);
});

// Out pallet consolidate
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/out-plt-consolidate', 'namespace' => 'App\Api\V2\Outbound\OutPalletConsolidate'], function ($api) {

    $api->post('/create',
        [
            'action' => "createOutPalletConsolidate",
            'uses'   => 'Create\Controllers\OutPltConsolidateController@store'
        ]
    );

    $api->get('/validate-pallet',
        [
            'action' => "getOutPalletConsolidate",
            'uses'   => 'Create\Controllers\OutPltConsolidateController@validationPallet'
        ]
    );
});

// Carton consolidate
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/pallet/{palletRFID}', 'namespace' => 'App\Api\V2\Outbound\CartonConsolidate'], function ($api) {
    // Scan Carton -update carton rfid, and create new pallet... Consolidate_plt
    $api->put('/consolidate',
        [
            'action' => "saveAssignPallet",
            'uses'   => 'Create\Controllers\CartonController@consolidatePallet'
        ]
    );

});

//cartons
$api->group(['prefix' => '/inbound/whs/{whsId:[0-9]+}/carton', 'namespace' => 'App\Api\V2\Inbound\GetCartonInfo\Controllers'], function ($api) {
    $api->get('/carton-information',
        ['uses' => 'CartonController@getCartonInfo']);
});

$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/location', 'namespace' => 'App\Api\V2\Inbound\Putaway\Controllers'], function ($api) {
    // Update location for pallet, carton
    $api->put('/rack/put-pallet',
        ['action' => "updateInventory", 'uses' => 'PutAwayController@putAway']);
});

// Cycle Count notification inbound
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/', 'namespace' => 'App\Api\V2\Inbound\CycleCount\Controllers'],function ($api) {
    // add Cycle Count Notification
    $api->post('/cycle-count-notification',
        [
            'action' => "viewCycleCountNotification",
            'uses'   => 'CycleNotificationController@addCycleCountNotification'
        ]
    );


});

// Cycle Count outbound
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/ob/', 'namespace' => 'App\Api\V2\Outbound\CycleCount'],function ($api) {
    // add Cycle Count Notification to Location
//    $api->post('/mark-location-cycle-count',
//        [
//            'action' => "viewCycleCountNotification",
//            'uses'   => 'MarkLocCCNotification\Controllers\MarkLocCCNotificationController@store'
//        ]
//    );

    $api->post('/mark-location-cycle-count',
        [
            'action' => "viewCycleCountNotification",
            'uses'   => 'MarkLocCCNotification\Controllers\MarkLocCCNotificationController@store'
        ]
    );

    // get Cycle Count Notification Reason list
    $api->get('/cycle-count-reason',
        [
            'action' => "viewCycleCountNotification",
            'uses'   => 'GetCCReasonList\Controllers\CycleNotificationController@getReasonDropDown'
        ]
    );
});

// Remove cartons
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/order-detail', 'namespace' => 'App\Api\V2\Outbound\RemoveCartons'],function ($api) {
    //Remove cartons assigned to order
    $api->put('/{odrDtlId:[0-9]+}/remove-carton-from-order',
        [
            'action' => "removeScannedCartons",
            'uses'   => 'Order\Controllers\OrderController@removeScannedCartons'
        ]
    );

});

$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/drop-pallet-shipping-lane', 'namespace' => 'App\Api\V2\Outbound\PalletShippingLane\Update\Controllers'],function ($api){
    $api->post('/',
        [
            'action' => 'dropPalletShippingLane',
            'uses'   => 'UpdatePalletShippingLane@update'
        ]);
});

$api->get('/v2/whs/{whsId:[0-9]+}/check-workorder/{wvDtl_Id:[0-9]+}',
    [
        'action'=>'checkworkorder',
        'uses'=>'App\Api\V2\Outbound\WorkOrder\Controllers\WorkOrderController@show'
    ]);

// Out Pallet
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/outpallet', 'namespace' => 'App\Api\V2\Outbound\OutPallet'], function ($api) {
    $api->put('/assign-cartons',
        [
            'action' => "assignBarcodeToOutpallet",
            'uses'   => 'AssignCartons\Controllers\OutPalletController@assignCartonToPallet'
        ]
    );

    $api->put('/unassign-barcodes',
        [
            'action' => "assignBarcodeToOutpallet",
            'uses'   => 'UnassignBarcode\Controllers\OutPalletController@unassignBarcodeFromPallet'
        ]
    );

    $api->put('/unassign-pack-carton',
        [
            'action' => "assignBarcodeToOutpallet",
            'uses'   => 'UnassignCartonRfid\Controllers\OutPalletController@unassignPackCarton'
        ]
    );

    $api->put('/unassign-from-master',
        [
            'action' => "outPalletConsolidate",
            'uses'   => 'UnassignOutpalletFromMaster\Controllers\OutPalletController@unassignFromMaster'
        ]
    );

    $api->put('/assign-pack-retail-order',
        [
            'action' => "assignPackRetailOrder",
            'uses'   => 'AssignPackRetailOrder\Controllers\OutPalletController@assignPackRetailOrder'
        ]
    );

    $api->put('/unassign-pack-retail-order',
        [
            'action' => "unassignPackRetailOrder",
            'uses'   => 'AssignPackRetailOrder\Controllers\OutPalletController@unassignPackRetailOrder'
        ]
    );
});

// Put back
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/putback', 'namespace' => 'App\Api\V2\Outbound\Putback'], function ($api) {

    $api->put('/unassign-rfid-from-pallet',
        [
            'action' => "palletPutback",
            'uses'   => 'RemoveCartonFromPallet\Controllers\PalletController@removeCartonPutback'
        ]
    );

});

// Location
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/location', 'namespace' => 'App\Api\V2\Inbound\Location'], function ($api) {

    $api->get('/check-empty/{locScanned}',
        [
            'action' => "putaway",
            'uses'   => 'CheckEmptyLocation\Controllers\LocationController@checkEmpty'
        ]
    );

    $api->get('/validate-pallet/{palletScanned}',
        [
            'action' => "putaway",
            'uses'   => 'ValidatePallet\Controllers\LocationController@validatePallet'
        ]
    );

    $api->put('/rack/drop-pallet',
        [
            'action' => "putaway",
            'uses'   => 'DropPallet\Controllers\PutAwayController@dropPalletFromLocation'
        ]
    );

    $api->post('/rack/get-empty-by-customer',
        [
            'action' => "putaway",
            'uses'   => 'ListEmptyLocation\Controllers\LocationController@getEmptyLocations'
        ]
    );

    $api->post('/rack/list-suggest-chino',
        [
            'action' => "putaway",
            'uses'   => 'GetLocationChino\Controllers\PutAwayController@getSuggestLocations'
        ]
    );

    $api->post('/rack/check-valid-by-customer',
        [
            'action' => "putaway",
            'uses'   => 'CheckValidLocationPutback\Controllers\LocationController@checkValidLocation'
        ]
    );

    $api->put('/rack/putback',
        [
            'action' => "putaway",
            'uses'   => 'Putback\Controllers\PutBackController@putBack'
        ]
    );

    $api->put('/ecommerce/putback',
        [
            'action' => "putaway",
            'uses'   => 'EcommercePutback\Controllers\PutBackController@putBack'
        ]
    );

    $api->post('/rack/relocate',
        [
            'action' => "putaway",
            'uses'   => 'RelocatePallet\Controllers\PutAwayController@relocate'
        ]
    );

    $api->post('/rack/relocate-valid-location',
        [
            'action' => "putaway",
            'uses'   => 'GetLocationInfo\Controllers\PutAwayController@validateLocation'
        ]
    );

    $api->post('/rack/valid-location-chino',
        [
            'action' => "putaway",
            'uses'   => 'ValidLocationChino\Controllers\PutAwayController@validateLocation'
        ]
    );

    $api->post('/rack/put-cartons',
        [
            'action' => "putaway",
            'uses'   => 'PutawayWithoutPallet\Controllers\PutAwayController@putAway'
        ]
    );

    $api->post('/rack/remove-cartons',
        [
            'action' => "putaway",
            'uses'   => 'RemoveCartonsFromLoc\Controllers\PutAwayController@removeCartons'
        ]
    );

});


// Carton Inbound
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/carton/', 'namespace' => 'App\Api\V2\Inbound\Carton'],function ($api) {
    $api->get('/get-damage-type',
        [
            'action' => "viewDamageCarton",
            'uses'   => 'GetDamageType\Controllers\DamageTypeController@getDamageType'
        ]
    );

    $api->post('/create-goods-receipt',
        [
            'action' => "viewGoodsReceipt",
            'uses'   => 'ScanCartonWithoutPallet\Controllers\CartonController@createGoodsReceipt'
        ]
    );

    $api->post('/validate-item-tag',
        [
            'action' => "viewGoodsReceipt",
            'uses'   => 'ValidateItemTag\Controllers\CartonController@validateItemTag'
        ]
    );

});

// Set damage cartons
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/carton/', 'namespace' => 'App\Api\V2\Inbound\Carton'],function ($api) {
    $api->put('/set-damage-carton',
        [
            'action' => "viewDamageCarton",
            'uses'   => 'SetDamageCarton\Controllers\VirtualCartonController@setDamageCarton'
        ]
    );

    $api->put('/set-damage-multi-cartons',
        [
            'action' => "viewDamageCarton",
            'uses'   => 'SetDamageCarton\Controllers\VirtualCartonController@setDamageToMultiCartons'
        ]
    );

    $api->put('/unset-damage-carton',
        [
            'action' => "viewDamageCarton",
            'uses'   => 'UnSetDamageCarton\Controllers\VirtualCartonController@unSetDamageCarton'
        ]
    );
});

// Set damage cartons not CusId
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/carton/', 'namespace' => 'App\Api\V2\Inbound\Carton'],function ($api) {
   $api->put('/set-damage-multi-cartons',
        [
            'action' => "viewDamageCarton",
            'uses'   => 'SetDamageCarton\Controllers\VirtualCartonController@setDamageToMultiCartons2'
        ]
    );
});

// Pack
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/pack/', 'namespace' => 'App\Api\V2\Outbound\PackCarton'],function ($api) {
    $api->get('/get-skus-by-order',
        [
            'action' => "viewPackCarton",
            'uses'   => 'SkuList\Controllers\OrderController@skuListSuggestion'
        ]
    );

});

// PiecesRemainFromCarton
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}', 'namespace' => 'App\Api\V2\Outbound\Carton\PiecesRemainFromCarton'],function ($api) {
    $api->get('/wave-detail/{waveDetailId:[0-9]+}/{cartonRfid}',
        [
            'action' => "viewCarton",
            'uses'   => 'Controllers\CartonController@getPiecesRemain'
        ]
    );

    $api->post('/wv_dtl/{wvDtlId:[0-9]+}/carton/split-carton-tag',
        [
            'action' => "viewCarton",
            'uses'   => 'Controllers\CartonController@splitCartonTag'
        ]
    );

});

$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/ib/pallet/', 'namespace' => 'App\Api\V2\Inbound\Pallet'], function ($api) {
    $api->put('scan-cartons-to-pallet',
        ['uses' => 'ScanPallet\Controllers\PalletController@assignCartonsToPallet']);
});

