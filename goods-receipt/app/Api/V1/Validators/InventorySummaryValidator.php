<?php

namespace App\Api\V1\Validators;


class InventorySummaryValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'cus_id'        => 'required|integer|exists:customer',
            'whs_id'        => 'required|integer|exists:warehouse',
            'color'         => 'required',
            'size'          => 'required',
            'ttl'           => 'required|integer',
            'allocated_qty' => 'required|integer',
            'dmg_qty'       => 'required|integer',
            'sku'           => 'required',
            'avail'         => 'required|integer',
        ];


    }


}

