<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use App\Api\V1\Models\Log;
use App\MyHelper;

class PutAwayController extends AbstractController
{
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var VirtualCartonModel
     */
    protected $virtualCartonModel;

    protected $palletSuggestLocationModel;

    protected $eventTrackingModel;

    /**
     * PutAwayController constructor.
     *
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param VirtualCartonModel $virtualCartonModel
     */
    public function __construct(
        PalletModel $palletModel,
        LocationModel $locationModel,
        VirtualCartonModel $virtualCartonModel
    ) {
        $this->palletModel = $palletModel;
        $this->cartonModel = new CartonModel();
        $this->locationModel = $locationModel;
        $this->virtualCartonModel = $virtualCartonModel;
        $this->eventTrackingModel = new EventTrackingModel();
        $this->palletSuggestLocationModel = new PalletSuggestLocationModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function putAway($whsId, Request $request)
    {
        $url = "/inbound/whs/$whsId/location/rack/put-pallet";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'PAW',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Put pallet to RACK'
        ]);

        $input = $request->getParsedBody();
        if ('' == trim($input['pallet-rfid'])) {
            $msg = "pallet-rfid is required!";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        if ('' == trim($input['loc-rfid'])) {
            $msg = "loc-rfid is required!";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($input['pallet-rfid'], RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $pltRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //validate location RFID
        $locRFIDValid = new RFIDValidate($input['loc-rfid'], RFIDValidate::TYPE_LOCATION, $whsId);
        if (!$locRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $locRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        // Get Pallet by Rfid
        $pallet = $this->palletModel->getModel()->where([
            'rfid'    => $input['pallet-rfid'],
            'whs_id'  => $whsId,
            'plt_sts' => 'AC',
        ])->where('ctn_ttl', '>', 0)->first();

        if (!$pallet) {
            $msg = sprintf("The pallet RFID %s doesn't exist, Please try to pass through the inbound gateway again!", $input['pallet-rfid']);
            return $this->returnError($msg);
        }

        if ($pallet->loc_code && $pallet->is_movement == 0) {
            $msg = sprintf("%s is already on rack.", $input['pallet-rfid']);
            return $this->returnError($msg);
        }

        $cusId = object_get($pallet, 'cus_id', '');

        //Check existed for The loc RFID
        $locationInfo = $this->locationModel->getRackLocationByLocRFID($whsId, $input['loc-rfid']);
        if (empty($locationInfo)) {
            $msg = sprintf("The location RFID %s doesn't exist!", $input['loc-rfid']);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $location = $this->locationModel->getRackLocationByCustomer($whsId, $input['loc-rfid'], $cusId);

        $returnData = [];
        try {
            DB::beginTransaction();

            if (empty($location)) {
                $msg = sprintf("The location RFID %s is not belong to this customer.", $input['loc-rfid']);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            } elseif ($location->loc_sts_code != 'AC') {
                $msg = sprintf("Location %s - %s is not active. Current Status %s", $input['loc-rfid'],
                    $location->loc_code,
                    $location->loc_sts_code);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $chkPlt = $this->palletModel->getModel()->where([
                'loc_id' => $location->loc_id,
                'whs_id' => $whsId
            ])
                ->where('ctn_ttl', '>', 0)
                ->where('plt_sts', 'AC')
                ->first();

            if ($chkPlt) {
                $msg = sprintf("There is a pallet %s in this location %s", $chkPlt->plt_num, $location->loc_code);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            // Update Pallet
            $this->palletModel->refreshModel();
            $this->palletModel->updateWhere([
                    'loc_id'   => $location->loc_id,
                    'loc_code' => $location->loc_code,
                    'loc_name' => $location->loc_alternative_name
                ],
                [
                    'rfid'             => $input['pallet-rfid'],
                    'plt_sts'          => 'AC',
                    'storage_duration' => 0
                ]);

            // Update Carton
            $data = [
                'loc_id'        => $location->loc_id,
                'loc_code'      => $location->loc_code,
                'loc_name'      => $location->loc_alternative_name,
                'loc_type_code' => Status::getByValue('RACK', 'LOC_TYPE_CODE'),
                'plt_id'        => $pallet->plt_id
            ];

            $this->cartonModel->updateCartonWithPltID($data);

            // turn on putaway when enough pallets have put on rack
            $this->_turnOnPutAway($pallet->gr_hdr_id);

            //unlock this location
            $this->locationModel->updateWhere(
                [
                    'loc_sts_code' => Status::getByKey('LOCATION_STATUS', 'ACTIVE'),
                ],
                [
                    'loc_id' => $location->loc_id
                ]
            );

            //insert latest pallet to RAC and put to table pal_sug_loc
            $grObj = (new GoodsReceiptDetailModel())->getModel()
                ->where('gr_dtl_id', $pallet->gr_dtl_id)
                ->join('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'gr_dtl.gr_hdr_id')
                ->select('gr_hdr_num', 'item_id', 'sku', 'size', 'color', 'lot')
                ->first();

            if ($grObj) {
                $this->initPalletSugLoc($pallet, $location, $grObj);
            }

            $returnData = $this->cartonModel->getCartonsInfoByPalletRFID($whsId, $pallet->plt_id);

            //write even tracking
            $vtlObj = $this->virtualCartonModel->getASNByVtlCtn($input['pallet-rfid']);
            $dataEvt = [
                'whs_id'    => object_get($vtlObj, 'whs_id', null),
                'cus_id'    => object_get($vtlObj, 'cus_id', null),
                'owner'     => object_get($vtlObj, 'gr_hdr_num', ''),
                'evt_code'  => 'PUT',
                'trans_num' => object_get($vtlObj, 'ctnr_num', ''),
                'info'      => sprintf('Move %s to %s', $input['pallet-rfid'], $location->loc_code)
            ];

            //call Event tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($dataEvt);

            DB::commit();
            $msg = sprintf("Pallet %s has been put on location %s", $input['pallet-rfid'], $location->loc_code);

            $data = [
                'data'    => ['skus' => $returnData],
                'message' => $msg,
                'status'  => true,
            ];

            return new Response($data, 200, [], null);
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * checking enough pallets have put on rack then turn on putaway within gr_hdr table
     *
     * @param $grHdrId
     *
     * @throws \Exception
     */
    private function _turnOnPutAway($grHdrId)
    {
        try {
            $grHdrObj = (new GoodsReceiptModel)->getModel()->where('gr_hdr_id', $grHdrId)->first();
            if ($grHdrObj) {
                $grStatus = object_get($grHdrObj, 'gr_sts');
                if ($grStatus == Status::getByKey('GR_STATUS', 'RECEIVED')) {
                    // // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                    // if ($grHdrObj && $grHdrObj->putaway != 2) {
                    //     $grHdrObj->putaway = 2;
                    //     $grHdrObj->save();
                    // }
                    return;
                }
                $totalPallet = (new GoodsReceiptDetailModel())->palletTotalOfGrHdr($grHdrId);
                $countPallet = $this->palletModel->countPalletOnRackByGRHdrId($grHdrId);

                // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                $grHdrObj->putaway = 2;
                if ($totalPallet == $countPallet) {
                    $grHdrObj->putaway = 1;
                }
                $grHdrObj->save();
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Input last pallet to RAC and input data into table pal_sug_log
     *
     * @param $pallet
     * @param $location
     * @param $grObj
     *
     * @throws \Exception
     */
    private function initPalletSugLoc($pallet, $location, $grObj)
    {
        try {
            $userId = JWTUtil::getPayloadValue('jti');

            if ($palSugLoc = $this->_checkPalletRFIDExistedOnSuggestLocation($pallet->plt_id)) {
                $palSugLoc->act_loc_id   = $location->loc_id;
                $palSugLoc->act_loc_code = $location->loc_code;
                $palSugLoc->save();
                return;
            }

            $suPltLocData = [
                'plt_id'       => $pallet->plt_id,
                'loc_id'       => $location->loc_id,
                'data'         => $location->loc_code,
                'ctn_ttl'      => $pallet->ctn_ttl,
                'item_id'      => object_get($grObj, 'item_id', null),
                'sku'          => object_get($grObj, 'sku', 0),
                'size'         => object_get($grObj, 'size', 'NA'),
                'color'        => object_get($grObj, 'color', 'NA'),
                'lot'          => object_get($grObj, 'lot', 'NA'),
                'putter'       => $userId,
                'gr_hdr_id'    => $pallet->gr_hdr_id,
                'gr_dtl_id'    => $pallet->gr_dtl_id,
                'gr_hdr_num'   => object_get($grObj, 'gr_hdr_num', ''),
                'whs_id'       => $pallet->whs_id,
                'put_sts'      => "CO",
                'act_loc_id'   => $location->loc_id,
                'act_loc_code' => $location->loc_code,
            ];

            $this->palletSuggestLocationModel->refreshModel();
            $this->palletSuggestLocationModel->create($suPltLocData);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function _checkPalletRFIDExistedOnSuggestLocation($pltId)
    {
        $result = $this->palletSuggestLocationModel->getFirstWhere(
            [
                'plt_id' => $pltId,
            ]
        );

        return $result;
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function paPutAway($whsId, $cusId, Request $request)
    {
        $input = $request->getParsedBody();

        if (('' == trim($input['pallet-rfid'])) || ('' == trim($input['loc-code']))) {
            $msg = "pallet-rfid or loc-code is required!";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($input['pallet-rfid'], RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $pltRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        // Get Pallet by Rfid
        $pallet = $this->palletModel->getFirstWhere([
            'rfid'   => $input['pallet-rfid'],
            'cus_id' => $cusId,
            'whs_id' => $whsId
        ]);

        try {
            /*
            * start logs
            */

            $url = "/whs/$whsId/cus/$cusId/location/put-away/put-pallet";
            $owner = $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'PPA',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'pa Put Away'
            ]);
            /*
            * end logs
            */

            DB::beginTransaction();

            $location = $this->locationModel->getLocWithType([
                'loc_alternative_name' => $input['loc-code'],
                'loc_whs_id'           => $whsId
            ], Status::getByValue('PUT-AWAY', 'LOC_TYPE_CODE'));

            if (empty($location)) {
                $msg = Message::get("BM017", "Location Code");
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            if ($pallet) {
                // Update Pallet
                $this->palletModel->refreshModel();
                $this->palletModel->update([
                    'plt_id'   => $pallet->plt_id,
                    'loc_id'   => $location->loc_id,
                    'loc_code' => $location->loc_code,
                    'loc_name' => $location->loc_alternative_name
                ]);

                // Update Carton
                $this->cartonModel->refreshModel();
                $this->cartonModel->updateWhere([
                    'loc_id'        => $location->loc_id,
                    'loc_code'      => $location->loc_code,
                    'loc_name'      => $location->loc_alternative_name,
                    'loc_type_code' => Status::getByValue('PUT-AWAY', 'LOC_TYPE_CODE'),
                ], [
                    'plt_id' => $pallet->plt_id
                ]);

                //update vtl ctn have loc info is null
                $this->virtualCartonModel->updateWhere(
                    [
                        'loc_id'   => null,
                        'loc_code' => null,
                    ],
                    ['plt_rfid' => $input['pallet-rfid']]
                );

                //unlock this location
                $this->locationModel->updateWhere(
                    [
                        'loc_sts_code' => Status::getByKey('LOCATION_STATUS', 'ACTIVE'),
                    ],
                    [
                        'loc_id' => $location->loc_id
                    ]
                );
            } else {
                $this->virtualCartonModel->updateWhere(
                    [
                        'loc_id'   => $location->loc_id,
                        'loc_code' => $location->loc_alternative_name,
                    ],
                    ['plt_rfid' => $input['pallet-rfid']]
                );

                //lock this location
                $this->locationModel->updateWhere(
                    [
                        'loc_sts_code' => Status::getByKey('LOCATION_STATUS', 'LOCKED'),
                    ],
                    [
                        'loc_id' => $location->loc_id
                    ]
                );
            }

            DB::commit();
            $msg = sprintf("Pallet %s has been put on location %s", $input['pallet-rfid'], $location->loc_code);

            return $this->response->noContent()
                ->setContent(['message' => $msg, 'Status' => 'OK'])
                ->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
