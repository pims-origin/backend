<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Bx8S7VE5zScbQB_gY_XQvIZJ619RFyt_',
        ],
        'redis' => [
            'class'    => 'yii\redis\Connection',
            'hostname' => 'redis.dev.seldatdirect.com',
            'port'     => 6379,
            'database' => 0,
            'password' => 'c3sellthat'
        ],
        'cache' => [
            'class'     => 'backend\library\MyRedisCache',
            'keyPrefix' => 'awms2_dev:',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
