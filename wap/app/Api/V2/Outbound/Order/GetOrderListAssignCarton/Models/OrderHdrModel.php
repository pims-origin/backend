<?php

namespace App\Api\V2\Outbound\Order\GetOrderListAssignCarton\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

class OrderHdrModel extends AbstractModel
{

    protected $model;

    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getListOrderHdrInfo($odr_ids)
    {
        return $this->model
            ->where('csr', '!=', 0)
            ->whereIn('odr_id', $odr_ids)
            ->whereIn('odr_sts', ["AL", "PAL"])
            ->get();
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getOrderHdrNums($odr_ids)
    {
        return $this->model
            ->whereIn('odr_id', $odr_ids)
            ->lists('odr_num');
    }

    /**
     * @param $wvId
     *
     * @return mixed
     */
    public function getOrderHdrNumsByWvId($wvId)
    {
        return $this->model
            ->whereIn('wv_id', $wvId)
            ->lists('odr_num');
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateOrdHdr($data)
    {
        // For partial order
        if ($data['odr_sts'] == Status::getByValue("Partial Allocated", "ORDER-STATUS")) {
            $sts = Status::getByValue("Partial Picking", "ORDER-STATUS");
        } // For full order
        elseif ($data['odr_sts'] == Status::getByValue("Allocated", "ORDER-STATUS")) {
            $sts = Status::getByValue("Picking", "ORDER-STATUS");
        }

        return $this->model->where('odr_id', '=', $data['odr_id'])
            ->update([
                'wv_id' => $data['wv_id'],
                'wv_num' => $data['wv_num'],
                'odr_sts' => $sts
            ]);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getOdrHdrListByWvId($wv_id)
    {
        return $this->model->where('wv_id', '=', $wv_id)->get();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function updatePickedOrdHdr($wv_id)
    {
        $odrHdrLsts = $this->getOdrHdrListByWvId($wv_id);
        foreach ($odrHdrLsts as $odrHdr) {
            if ($odrHdr['odr_sts'] == Status::getByValue("Picking", "ORDER-STATUS")) {
                $this->saveDataPickedOdrHdr([
                    'wv_id' => $wv_id,
                    'odr_sts' => Status::getByValue("Picked", "ORDER-STATUS"),
                ]);
            } elseif ($odrHdr['odr_sts'] == Status::getByValue("Partial Picking", "ORDER-STATUS")) {
                $this->saveDataPickedOdrHdr([
                    'wv_id' => $wv_id,
                    'odr_sts' => Status::getByValue("Partial Picked", "ORDER-STATUS"),
                ]);
            }
        }
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function saveDataPickedOdrHdr($data)
    {
        return $this->model->where('wv_id', '=', $data['wv_id'])
            ->update([
                'odr_sts' => $data['odr_sts']
            ]);
    }

    public function countOdrHdrByWvId($wv_id)
    {
        return $this->model->where('wv_id', '=', $wv_id)->count();
    }

    public function updatePDOdrHdr($wvHdrId)
    {
        $result = DB::table('odr_hdr as oh')
            ->where('oh.wv_id', $wvHdrId)
            ->where('oh.odr_sts', 'PK')
            ->where(
                DB::raw("(SELECT COUNT(1) FROM `odr_dtl` as od WHERE od.`odr_id` = oh.odr_id AND od.`itm_sts` = 'PD')"),
                DB::raw("(SELECT COUNT(1) FROM `odr_dtl` as od WHERE od.`odr_id` = oh.odr_id)")
            )
            ->update(['oh.odr_sts' => 'PD']);

        return $result;
    }

    public function updatePAOdrHdr($odrHdrIds)
    {
        $result = DB::table('odr_hdr')
            ->where('odr_id', $odrHdrIds)
            ->where('odr_sts', 'PD')
            ->update(['odr_sts' => 'PA']);

        return $result;
    }

    public function updatePKOdrHdr($wvHdrId)
    {
        $result = DB::table('odr_hdr as oh')
            ->where('oh.wv_id', $wvHdrId)
            ->update(['oh.odr_sts' => 'PK']);

        return $result;
    }

    // get list order has status NEW, PACKING
    public function getPackingOrders($whsId, $sku, $limit = 20, $odrSts, $odrNum)
    {

        // WAP-739 - [Outbound - Order] Update API get order list
        $status = [
            Status::getByValue("New", "ORDER-STATUS"),
            Status::getByValue("Picked", "ORDER-STATUS"),
            Status::getByValue("Picking", "ORDER-STATUS"),
            Status::getByValue("Packing", "ORDER-STATUS"),
            Status::getByValue("Packed", "ORDER-STATUS"),
            Status::getByValue("Palletizing", "ORDER-STATUS"),
        ];
        $query = $this->model
            ->select('odr_hdr.odr_id', 'odr_hdr.cus_id', 'odr_hdr.whs_id', 'odr_hdr.wv_id', 'odr_num', 'cus_odr_num', 'odr_type',
                'odr_sts', 'wv_num',
                'odr_hdr.created_at', 'customer.cus_name')
            ->join('customer', 'customer.cus_id', '=', 'odr_hdr.cus_id')
            ->where('odr_hdr.whs_id', $whsId)
            ->whereIn('odr_sts', $status);

        if (trim($sku) !== '') {
            $sku = trim($sku);
            $query->join('odr_dtl', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')
                    ->where('odr_dtl.sku', 'LIKE', "%{$sku}%")
                    ->groupBy('odr_hdr.odr_id')
            ;
        }

        if (count($odrSts)) {
            $query->whereIn('odr_hdr.odr_sts', $odrSts);
        }

        if (trim($odrNum) !== '') {
            $odrNum = trim($odrNum);
            $query->where('odr_hdr.odr_num', 'LIKE', "{$odrNum}%");
        }

        $query->orderBy('odr_hdr.updated_at', 'DESC');
        $query->orderBy('odr_hdr.odr_id');

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    public function getListOrderPacked($whsID)
    {
        $data = $this->model
            ->select(
                [
                    'odr_hdr.odr_id',
                    'odr_hdr.cus_id',
                    'odr_hdr.whs_id',
                    'customer.cus_name',
                    'odr_num',
                    /*DB::raw("(SELECT COUNT(sku) FROM `odr_dtl` as total_sku WHERE odr_hdr.`odr_id` = odr_dtl.odr_id) as total_sku"),*/
                    'odr_hdr.odr_sts',
                    'odr_hdr.created_at',
                ]
            )
            /*->join('odr_dtl', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')*/
            ->join('customer', 'customer.cus_id', '=', 'odr_hdr.cus_id')
            ->where('odr_hdr.whs_id', $whsID)
            ->whereIn(
                'odr_hdr.odr_sts',
                [
                    Status::getByValue('Packing', 'ORDER-STATUS'),
                    Status::getByValue('Packed', 'ORDER-STATUS'),
                    Status::getByValue('New', 'ORDER-STATUS'),
                ]
            );

        //$this->model->filterData($data, true);
        return $data->get();
    }


    public function updateOrderPicked($odr_id)
    {
        $sql = "(SELECT COUNT(1) FROM odr_dtl WHERE odr_dtl.odr_id = odr_hdr.odr_id AND odr_dtl.itm_sts != 'PD' AND deleted =0  AND alloc_qty > 0) = 0";
        $result = $this->model
            ->where('odr_id', $odr_id)
            ->where('odr_sts', 'PK')
            ->whereRaw(DB::raw($sql))->update(["odr_sts" => 'PD']);
        return $result;

    }

    public function updateOrderStaging($odr_id)
    {
        $sql = "(SELECT COUNT(1) FROM pack_hdr WHERE pack_hdr.odr_hdr_id = odr_hdr.odr_id AND pack_hdr.pack_sts != 'AS' AND deleted =0) = 0";
        $result = $this->model
            ->where('odr_id', $odr_id)
            ->where('odr_sts', 'PA')
            ->whereRaw(DB::raw($sql))->update(["odr_sts" => 'ST']);
        return $result;

    }


    public function isLPNinOrder($odrId, $lpn, &$odrNum)
    {
        $num = explode('-', $lpn);
        array_shift($num);
        array_unshift($num, 'ORD');
        array_pop($num);

        $odrNum = implode('-', $num);

        $wv = (new OrderHdrModel())->getModel()->where('odr_id', $odrId)->value('odr_num');
        if ($wv == $odrNum) {

            return true;
        }
        return false;
    }

    /**
     * @param $wvNum
     *
     * @return mixed
     */
    public function getOdrIdByWvNum($wvNum)
    {
        $query = $this->make(['waveHdr']);
        $query->whereHas('waveHdr', function ($query) use ($wvNum) {
                $query->where('wv_num', $wvNum);
            });

        return $query->pluck('odr_id')->toArray();
    }
}
