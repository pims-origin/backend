<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\InventoriesModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\InventoryModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\WarehouseAddressModel;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Models\WarehouseStatusModel;
use App\Api\V1\Models\WarehouseZoneModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\ZoneModel;
use App\Api\V1\Models\WarehouseContactModel;
use App\Api\V1\Traits\WarehouseControllerTrait;
use App\Api\V1\Transformers\InventoryReportTransformer;
use App\Api\V1\Transformers\SkuReportTransformer;
use App\Api\V1\Transformers\WarehouseTransformer;
use App\Api\V1\Validators\DeleteMassWarehouseValidator;
use App\Api\V1\Validators\WarehouseValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\Inventories;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Export;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\Status;

class WarehouseController extends AbstractController
{
    use WarehouseControllerTrait;

    const SPECIAL_ZONE = [
        'XDK-D' => 'X-Dock',
        'SHP'   => 'Shipping',
    ];

    /**
     * @var WarehouseModel
     */
    //protected $warehouseModel;

    /**
     * @var WarehouseAddressModel
     */
    protected $warehouseAddressModel;

    /**
     * @var WarehouseContactModel
     */
    protected $warehouseContactModel;

    /**
     * @var WarehouseStatusModel
     */
    protected $warehouseStatusModel;

    /**
     * @var SystemCountryModel
     */
    protected $systemCountryModel;

    /**
     * @var WarehouseTransformer
     */
    protected $warehouseTransformer;
    /**
     * @var $warehouseValidator
     */
    protected $warehouseValidator;
    /**
     * @var $delMassWarehouseValidator
     */
    protected $delMassWarehouseValidator;

    protected $whs_id;

    protected $user_id;

    /**
     * WarehouseController constructor.
     *
     */
    public function __construct()    /**
     * @param Request $request
     * @param InventoryReportTransformer $inventoryReportTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    {
        parent::__construct();

        $this->warehouseModel = new WarehouseModel();
        $this->warehouseAddressModel = new WarehouseAddressModel();
        $this->warehouseContactModel = new WarehouseContactModel();
        $this->warehouseStatusModel = new WarehouseStatusModel();
        $this->systemCountryModel = new SystemCountryModel();
        $this->warehouseTransformer = new WarehouseTransformer();
        $this->warehouseValidator = new WarehouseValidator();
        $this->delMassWarehouseValidator = new DeleteMassWarehouseValidator();
        $this->locationModel = new LocationModel();
        $this->customerModel = new CustomerModel();
        $this->zoneModel = new ZoneModel();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $this->whs_id = array_get($userInfo, 'current_whs', 0);
        $this->user_id = array_get($userInfo, 'user_id', 0);
    }

    /**
     * @SWG\Get(
     *     path="/v1/warehouses/{warehouseId}",
     *     summary="Read Warehouse",
     *     description="View Detail Warehouse",
     *     operationId="showWarehouse",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return detail of Warehouse",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "warehouse 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Warehouse Detail
     *
     * @param int $warehouseId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function show($warehouseId)
    {
        try {
            $warehouse = $this->warehouseModel->getFirstBy('whs_id', $warehouseId, [
                'warehouseStatus',
                'systemCountry',
                'systemState'
            ]);

            return $this->response->item($warehouse, $this->warehouseTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/v1/warehouses/search",
     *     summary="Search Warehouse",
     *     description="Search Warehouse",
     *     operationId="searchWarehouse",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Return all found result of Warehouse",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "warehouse 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     * )
     *
     * Warehouse Search
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $warehouse = $this->warehouseModel->search($input, [
                'warehouseAddress',
                'warehouseContact',
                'warehouseStatus',
                'systemCountry',
                'systemState'
            ], array_get($input, 'limit'));

            return $this->response->paginator($warehouse, $this->warehouseTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/warehouses/{warehouseId}",
     *     summary="Create Warehouse",
     *     description="Create Warehouse",
     *     operationId="createWarehouse",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_name",
     *         description="Warehouse's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_status",
     *         description="Warehouse's State Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_short_name",
     *         description="Warehouse's Short Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Warehouse",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "warehouse 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Warehouse Create
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseValidator->validate($input);

        $params = [
            'whs_name'       => $input['whs_name'],
            'whs_status'     => $input['whs_status'],
            'whs_short_name' => array_get($input, 'whs_short_name', ''),
            'whs_code'       => $input['whs_code'],
            'whs_country_id' => array_get($input, 'whs_country_id', ''),
            'whs_state_id'   => array_get($input, 'whs_state_id', ''),
            'whs_city_name'  => array_get($input, 'whs_city_name', ''),
        ];

        try {
            // check duplicate code
            if ($this->warehouseModel->getFirstBy('whs_code', $params['whs_code'])) {
                throw new \Exception(Message::get("BM006", "Warehouse Code"));
            }

            // check duplicate code
            if ($this->warehouseModel->getFirstBy('whs_name', $params['whs_name'])) {
                throw new \Exception(Message::get("BM006", "Warehouse Name"));
            }

            if ($warehouse = $this->warehouseModel->create($params)) {
                return $this->response->item($warehouse,
                    $this->warehouseTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Put(
     *     path="/v1/warehouses/{warehouseId}",
     *     summary="Update Warehouse",
     *     description="Update Warehouse",
     *     operationId="updateWarehouse",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_name",
     *         description="Warehouse's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_status",
     *         description="Warehouse's State Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_short_name",
     *         description="Warehouse's Short Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Warehouse",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "warehouse 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * @param int $warehouseId
     * @param Request $request
     *
     * @return mixed
     * @internal param int $whs_con_id
     */
    public function update(
        $warehouseId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseValidator->validate($input);

        $params = [
            'whs_id'         => $warehouseId,
            'whs_name'       => $input['whs_name'],
            'whs_status'     => $input['whs_status'],
            'whs_short_name' => array_get($input, 'whs_short_name', ''),
            'whs_code'       => $input['whs_code'],
            'whs_country_id' => array_get($input, 'whs_country_id', ''),
            'whs_state_id'   => array_get($input, 'whs_state_id', ''),
            'whs_city_name'  => array_get($input, 'whs_city_name', ''),
        ];

        try {
            // check duplicate code
            if (($warehouse = $this->warehouseModel->getFirstBy('whs_code', $params['whs_code']))
                && $warehouse->whs_id != $warehouseId
            ) {
                throw new \Exception(Message::get("BM006", "Warehouse Code"));
            }

            // check duplicate code
            if (($warehouse = $this->warehouseModel->getFirstBy('whs_name', $params['whs_name']))
                && $warehouse->whs_id != $warehouseId
            ) {
                throw new \Exception(Message::get("BM006", "Warehouse Name"));
            }

            if ($warehouse = $this->warehouseModel->update($params)) {
                return $this->response->item($warehouse, $this->warehouseTransformer);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Delete(
     *     path="/v1/warehouse/{warehouseId}",
     *     summary="Delete Warehouse",
     *     description="Delete a Warehouse",
     *     operationId="updateContact",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Warehouse was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param int $warehouseId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     * @internal param MenuModel $menuModel
     * @internal param MenuFavoriteModel $menuFavoriteModel
     *
     */
    public function destroy($warehouseId)
    {
        try {
            // Check Warehouse exist in Zone
            $zoneModel = new WarehouseZoneModel();
            $zone = $zoneModel->loadBy(['zone_whs_id' => $warehouseId])->first();
            if (!empty($zone)) {
                return $this->response->errorBadRequest(Message::get("BM003", "Warehouse"));
            }

            // Check Exist Warehouse in Location
            $locationModel = new LocationModel();
            $locations = $locationModel->loadBy(['loc_whs_id' => $warehouseId])->first();
            if (!empty($locations)) {
                return $this->response->errorBadRequest(Message::get("BM003", "Warehouse"));
            }

            // Check Warehouse exist in Warehouse Address
            $warehouseAddressModel = new WarehouseAddressModel();
            $warehouseAddress = $warehouseAddressModel->loadBy(['whs_add_whs_id' => $warehouseId])->first();
            if (!empty($warehouseAddress)) {
                return $this->response->errorBadRequest(Message::get("BM003", "Warehouse"));
            }

            // Check Warehouse exist in Warehouse Contact
            $warehouseContactModel = new WarehouseContactModel();
            $warehouseContact = $warehouseContactModel->loadBy(['whs_con_whs_id' => $warehouseId])->first();
            if (!empty($warehouseContact)) {
                return $this->response->errorBadRequest(Message::get("BM003", "Warehouse"));
            }

            if ($this->warehouseModel->deleteWarehouse($warehouseId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM017", "Warehouse"));
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->delMassWarehouseValidator->validate($input);

        $input['whs_id'] = is_array($input['whs_id']) ? $input['whs_id'] : [$input['whs_id']];

        try {
            // Check Warehouse exist in Zone
            $zoneModel = new WarehouseZoneModel();
            $zone = $zoneModel->loadByMany(['zone_whs_id' => $input['whs_id']])->first();
            if (!empty($zone)) {
                return $this->response->errorBadRequest(Message::get("BM003", "Warehouse"));
            }

            // Check Exist Warehouse in Location
            $locationModel = new LocationModel();
            $locations = $locationModel->loadByMany(['loc_whs_id' => $input['whs_id']])->first();
            if (!empty($locations)) {
                return $this->response->errorBadRequest(Message::get("BM003", "Warehouse"));
            }

            // Check Warehouse exist in Warehouse Address
            $warehouseAddressModel = new WarehouseAddressModel();
            $warehouseAddress = $warehouseAddressModel->loadByMany(['whs_add_whs_id' => $input['whs_id']])->first();
            if (!empty($warehouseAddress)) {
                return $this->response->errorBadRequest(Message::get("BM003", "Warehouse"));
            }

            // Check Warehouse exist in Warehouse Contact
            $warehouseContactModel = new WarehouseContactModel();
            $warehouseContact = $warehouseContactModel->loadByMany(['whs_con_whs_id' => $input['whs_id']])->first();
            if (!empty($warehouseContact)) {
                return $this->response->errorBadRequest(Message::get("BM003", "Warehouse"));
            }

            $this->warehouseModel->deleteMassWarehouse($input['whs_id']);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function loadBy(Request $request)
    {
        $wantedFields = [
            "whs_id",
            "whs_name",
            "whs_status",
            "whs_code",
            "whs_country_id",
            "whs_state_id"
        ];

        return $this->_loadBy($request, $wantedFields, $this->warehouseModel, $this->warehouseTransformer);
    }

    public function export(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $type = strtolower(array_get($input, 'type', 'csv'));

        try {
            if (!in_array($type, ['csv', 'pdf'])) {
                throw new \Exception(Message::get("VR005", "(.csv|.pdf)"));
            }

            $warehouses = $this->warehouseModel->search($input, [
                'warehouseAddress',
                'warehouseContact',
                'warehouseStatus',
                'systemCountry',
                'systemState'
            ], array_get($input, 'limit', 999999999));

            $dataTitle = [
                'Warehouse Code' => "whs_code",
                'Warehouse Name' => "whs_name",
                'Contact Name'   => "warehouseContact.whs_con_fname|warehouseContact.whs_con_lname",
                'Contact Email'  => "warehouseContact.whs_con_email",
                'Phone'          => "warehouseContact.whs_con_phone",
                'Country'        => "systemCountry.sys_country_name",
                'City'           => "warehouseAddress.whs_add_city_name",
                'State'          => "systemState.sys_state_name",
                'Status'         => "warehouseStatus.whs_sts_name",
            ];

            if (SelExport::setDataList($warehouses, $dataTitle)) {
                SelExport::exportList("Export_Warehouse_List_" . date("Y-m-d", time()), $type);
            }

            return $this->response->noContent();
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    public function inventorReport(Request $request)
    {
        try {
            $input = $request->getQueryParams();
            $inventory = new InventoryModel();
            //$inventory = new InventorySummaryModel();

            if (!empty($input['export']) && $input['export'] == 1) {
                $result = $inventory->inventoryReportsV2($input, ['customer', 'Item'], false, true)->toArray();
                if (empty($result)) {
                    return $this->response->noContent()->setContent(['status' => 'OK', 'message' => 'Data is empty!']);
                }

                $title = [
                    'cus_name' => 'Customer',
                    'item_id'  => 'Item ID',
                    'sku'      => 'SKU',
                    'size'     => 'Size',
                    'color'    => 'Color',
                    //'lot'               => 'Lot',
                    'upc'         => 'UPC',
                    'avail'       => 'Available QTY',
                    //'allocated_qty'     => 'Allocated QTY',
                    'picked_qty'  => 'Picked QTY',
                    //'crs_doc_qty'       => 'X-dock QTY',
                    //'dmg_qty'           => 'Damaged QTY',
                    'ttl'         => 'Total QTY',
                    'pack'        => 'Pack',
                    'description' => 'Description',
                    'ctns'        => 'Total CTNS'
                ];

                Export::showFile("Report_Inventory", $title, $result);
                die;
            }
            $data = $inventory->inventoryReportsV2($input, [
                'customer',
                'Item'
            ], array_get($input, 'limit'));
            $statusData=$data['statusData'];
            $items=[];
            foreach ($data['data'] as $inventorySummary){
                $key=$inventorySummary['item_id'].'.'.$inventorySummary['cus_id'].'.'.$inventorySummary['whs_id'];
                $inventorySummary['avail']=round($inventorySummary['avail']);
                $inventorySummary['picked_qty']=round($inventorySummary['picked_qty']);
                $inventorySummary['ttl']=round($inventorySummary['ttl']);
                $inventorySummary['ctns']=round($inventorySummary['ctns']);
                $inventorySummary['gr_sts']=$statusData->contains($key)?1:0;
                $items[]=$inventorySummary;
            }
            $links=[];
            if($data['data']->previousPageUrl())
                $links['previous']=$data['data']->previousPageUrl();
            if($data['data']->nextPageUrl())
                $links['next']=$data['data']->nextPageUrl();
            $result=[
                'data'=>$items,
                'meta'=>[
                    'count'=>$data['data']->count(),
                    'current_page'=>$data['data']->currentPage(),
                    'links'=>$links,
                    'per_page'=>$data['data']->perPage(),
                    'total'=>$data['data']->total(),
                    'total_pages'=>$data['data']->lastPage(),
                ]
            ];
            return $result;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    /**
     * @param $warehouseId
     * @param Request $request
     * @param SkuReportTransformer $skuReportTransformer
     *
     * @return $this|\Dingo\Api\Http\Response|void
     */
    public function skuReport(
        $warehouseId,
        Request $request,
        SkuReportTransformer $skuReportTransformer
    ) {
        try {
            $input = $request->getQueryParams();
            $cartonsSummary = new CartonModel();
            if (!empty($input['export']) && $input['export'] == 1) {
                $result = $cartonsSummary->skuReports($input, ['customer'], false, true, $warehouseId)->toArray();
                if (empty($result)) {
                    return $this->response->noContent()->setContent(['status' => 'OK', 'message' => 'Data is empty!']);
                }

                for($i = 0; $i < count($result); $i++){
                    $result[$i]['length'] = round($result[$i]['length'], 2);
                    $result[$i]['width'] = round($result[$i]['width'], 2);
                    $result[$i]['height'] = round($result[$i]['height'], 2);
                    $result[$i]['weight'] = round($result[$i]['weight'], 1);
                    $result[$i]['ctn_sts'] = Status::getByValue($result[$i]['ctn_sts'], 'CTN_STATUS');
                }

                $title = [
                    'whs_name'          => 'Warehouse',
                    'item_id'           => 'Item ID',
                    'customer.cus_name' => 'Customer Name',
                    'sku'              => 'SKU',
                    'ctn_pack_size'    => 'Pack Size',
                    'color'            => 'Color',
                    'size'             => 'Size',
                    'length'           => 'Length',
                    'width'            => 'Width',
                    'height'           => 'Height',
                    'weight'           => 'Weight',
                    //'volume'           => 'Volume',
                    'uom'              => 'Uom',
                    'carton_ttl'       => 'Total CTNS',
                    'piece_remain_ttl' => 'Total Piece',
                    'ctn_sts'          => 'Status',

                ];

                Export::showFile("Report_Inventory", $title, $result);
                die;
            }

            $int = $cartonsSummary->skuReports($input, [
                'customer'
            ], array_get($input, 'limit'), '', $warehouseId);

            return $this->response()->paginator($int, $skuReportTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
