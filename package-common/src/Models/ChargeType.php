<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:49
 */

namespace Seldat\Wms2\Models;

/**
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property mixed chg_type_id
 * @property mixed chg_type_code
 * @property mixed chg_type_name
 * @property mixed chg_type_des
 * @property mixed created_by
 * @property mixed updated_by
 */
class ChargeType extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'charge_type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'chg_type_id';

    /**
     * @var array
     */
    protected $fillable = [
        'chg_type_code',
        'chg_type_name',
        'chg_type_des',
        'created_by',
        'updated_by',
    ];

    public function chargeCode()
    {
        return $this->hasOne(__NAMESPACE__ . '\ChargeCode', 'chg_type_id', 'chg_type_id');
    }
}
