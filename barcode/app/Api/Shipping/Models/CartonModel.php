<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\Shipping\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class CartonModel extends AbstractModel
{
    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

}
