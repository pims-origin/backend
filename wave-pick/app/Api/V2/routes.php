<?php

// Wave pick detail
$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/wave-pick-details', 'namespace' => 'App\Api\V2\WavePickDetails'], function ($api) {

    $api->get('/list',
        [
            'action' => "createWavePick",
            'uses'   => 'WvDtlList\Controllers\WavePickDetailController@detailWavePick'
        ]
    );

    $api->get('/export',
        [
            'action' => "createWavePick",
            'uses'   => 'WvDtlList\Controllers\WavePickDetailController@exportWavePickDetail'
        ]
    );

});
//Wave pick
$api->group(['prefix' => '/v2/wave-pick', 'namespace' => 'App\Api\V2\WavePick\Controllers'], function ($api) {

    $api->get('/print', [
        'action' => 'createWavePick',
        'uses'   => 'WavePickController@printWavepick'
    ]);
});