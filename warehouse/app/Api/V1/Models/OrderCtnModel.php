<?php
/**
 * Created by PhpStorm.
 * User: PCDell76_ThienNguyen
 * Date: 2/10/2017
 * Time: 7:11 PM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderCarton;

class OrderCtnModel extends AbstractModel
{
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    public function invReport($warehouseId, $params, $limit = 20, $export = false)
    {
        DB::statement('SET @@session.time_zone = "-05:00";');
        $query = $this->make();
        $query->leftJoin('odr_hdr', 'odr_cartons.odr_hdr_id', '=', 'odr_hdr.odr_id');
        $query->leftJoin('customer', 'odr_hdr.cus_id', '=', 'customer.cus_id');
        $query->leftJoin('users', 'odr_hdr.updated_by', '=', 'users.user_id');
        $query->leftJoin('system_state', 'odr_hdr.ship_to_state', '=', 'system_state.sys_state_code');

        $query->where('odr_hdr.whs_id', $warehouseId);
        $query->where('odr_cartons.ctn_sts', 'SH');
        if (isset($params['cus_id']) && !empty($params['cus_id'])) {
            $query->where('odr_hdr.cus_id', intval($params['cus_id']));
        }
        if (isset($params['odr_type']) && !empty($params['odr_type'])) {
            $query->where('odr_hdr.odr_type', $params['odr_type']);
        }
        if (isset($params['odr_num']) && !empty($params['odr_num'])) {
            $query->where('odr_cartons.odr_num', $params['odr_num']);
        }

        if (isset($params['sku']) && !empty($params['sku'])) {
            $query->where('odr_cartons.sku', $params['sku']);
        }
        if (isset($params['item_id']) && !empty($params['item_id'])) {
            $query->where('odr_cartons.item_id', $params['item_id']);
        }
        if (isset($params['lot']) && !empty($params['lot'])) {
            $query->where('odr_cartons.lot', $params['lot']);
        }

        //search according to from date to date
        if (!empty($params['ship_dt_from'])) {
            // $query->where('odr_cartons.ship_dt', ">", strtotime($params['ship_dt_from'] . "-1 day"));
            $query->where('odr_hdr.shipped_dt', ">=", strtotime($params['ship_dt_from']));
        }

        if (!empty($params['ship_dt_to'])) {
            $query->where('odr_hdr.shipped_dt', "<", strtotime($params['ship_dt_to'] . "+1 day"));
        }

        $query->select([
            'customer.cus_name',
            'customer.cus_code',
            'odr_cartons.odr_num',
            'odr_cartons.item_id',
            'odr_cartons.sku',
            'odr_cartons.size',
            'odr_cartons.color',
            'odr_cartons.lot',
            'odr_cartons.upc',
            DB::raw('SUM(odr_cartons.piece_qty) AS piece_qty'),
            'odr_cartons.ship_dt',
            'users.first_name',
            'users.last_name',
            'odr_hdr.shipped_dt',
            'odr_cartons.pack',
            'odr_hdr.odr_type',
            'odr_hdr.cus_odr_num',
            'odr_hdr.cus_po',
            'odr_hdr.ship_to_name',
            'odr_hdr.carrier',
            'odr_hdr.ship_to_add_1',
            'odr_hdr.ship_to_state',
            'system_state.sys_state_name',
            'odr_hdr.ship_to_city',
            'odr_hdr.ship_to_zip',
            'odr_hdr.act_cancel_dt',
        ]);

        $query->groupBy('odr_cartons.item_id', 'odr_cartons.lot', 'odr_cartons.odr_num');

        $query->orderBy('odr_cartons.ship_dt', 'DESC');

//        $this->sortBuilder($query, $params);

        if ($export) {
            return $query->get();
        } else {
            return $query->paginate($limit);
        }

    }
}