<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 1/31/2019
 * Time: 3:11 PM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Inventories;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;


class InventoriesModel extends AbstractModel
{
    public function __construct(Inventories $model = null)
    {
        $this->model = ($model) ?: new Inventories();
    }

    public function inventoryReports($input, $limit = 2000, $export = false) {
        $query = $this->getModel()->join('customer', 'customer.cus_id', '=', 'inventory.cus_id')
            ->join('item', 'item.item_id', '=', 'inventory.item_id')
            ->select([
                'item.sku',
                'item.size',
                'item.color',
                'customer.cus_id',
                'customer.cus_name',
                'customer.cus_code',
                'inventory.in_hand_qty',
                'inventory.in_pick_qty',
                'inventory.total_qty',
                'item.cus_upc',
                'item.pack',
                'item.description',
                DB::raw('round(inventory.total_qty/item.pack, 2) AS ctns'),
            ]);
        if (!empty($input['cus_id'])) {
            $query->where('inventory.cus_id', intval($input['cus_id']));
        }

        if (!empty($input['sku'])) {
            $query->where('item.sku', 'like', "%" . SelStr::escapeLike($input['sku']) . "%");
        }

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('inventory.whs_id', $currentWH);
        $query->where('inventory.total_qty', '!=', 0);
        $query->orderBy('inventory.updated_at', 'desc');

        $query->groupBy('inventory.item_id');
        $this->sortBuilder($query, $input);

        if ($export) {
            return $query->get();
        } else {
            return $query->paginate($limit);
        }

    }

}