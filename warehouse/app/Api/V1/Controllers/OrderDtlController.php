<?php
/**
 * Created by PhpStorm.
 *
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Transformers\OrderDtlTransformer;
use App\Api\V1\Validators\OrderDtlValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

/**
 * Class OrderDtlController
 *
 * @package App\Api\V1\Controllers
 */
class OrderDtlController extends AbstractController
{
    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * OrderDtlController constructor.
     *
     * @param OrderDtlModel $orderDtlModel
     */
    public function __construct(OrderDtlModel $orderDtlModel)
    {
        $this->orderDtlModel = $orderDtlModel;
    }


}
