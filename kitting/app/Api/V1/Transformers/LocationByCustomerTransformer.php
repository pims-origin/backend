<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use App\Location;
use League\Fractal\TransformerAbstract;

class LocationByCustomerTransformer extends TransformerAbstract
{
    /**
     * @param Location $location
     *
     * @return array
     */
    public function transform(Location $location)
    {
        return [
            'loc_id'                 => $location->loc_id,
            'loc_code'               => $location->loc_code,
            'loc_name'               => $location->loc_alternative_name,
            'loc_sts_code'           => $location->loc_sts_code,
            'has_item'               => $location->has_item,
            'has_pallet'             => (int)object_get($location, 'has_pallet', 0),
        ];
    }
}
