<?php

namespace App\Api\V1\Models;

use App\InvoiceHeader;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use App\Wms2\UserInfo\Data;

class ReportModel extends AbstractModel
{
    /**
     * Constructor.
     */

    public function __construct()
    {
        $this->model = new InvoiceHeader();
    }

    public function search($input, $with = [], $limit = 10) {
        $query = $this->make($with);
        if (isset($input['inv_num']) && $input['inv_num'] != '') {
            $query->where('inv_num', 'like', "%" . SelStr::escapeLike($input['inv_num']) . "%");
        }

        if (!empty($input['cus_id'])) {
            $query->where('cus_id', $input['cus_id']);
        }

        if (!empty($input['whs_id'])) {
            $query->where('whs_id', $input['whs_id']);
        }

        if (!empty($input['created_by'])) {
            $query->where('created_by', $input['created_by']);
        }

        if (!empty($input['inv_sts'])) {
            $query->where('inv_sts', $input['inv_sts']);
        }

        if (array_has($input, 'sort')) {
            
        }

        $this->sortBuilder($query, $input);
        return $query->paginate($limit);
    }

    // get details 30, 60, 90, over 90 days
    public function agingSearchThirtyDetails($input, $with = [], $from = null, $to = null)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $attributes = SelArr::removeNullOrEmptyString($input);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_hdr as ih')->select([
            'ih.inv_num as inv_num',
            'customer.cus_name as cus_name',
            'ih.due_dt as due_dt',
            DB::raw("SUM(ih.total) AS amount")
        ]);

        $query->join('customer', 'customer.cus_id', '=', 'ih.cus_id');

        $query->where('ih.whs_id', $currentWH);
        if($from == null && $to != null){
            $query->where('ih.due_dt', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . 30 . ' DAY)'));
        }
        if($from != null && $to == null){
            $query->where('ih.due_dt', '<=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . 91 . ' DAY)'));
        }
        if($from != null && $to != null){
            $query->where('ih.due_dt', '<=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . (int)($from) . ' DAY)'));
            $query->where('ih.due_dt', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . (int)($to) . ' DAY)'));
        }

        if (isset($attributes['cus_id'])) {
            $query->where('ih.cus_id', $attributes['cus_id']);
        }

        $query->groupBy('ih.inv_num', 'ih.due_dt');

        $row = $query->get();
        return $row;
    }

    public function agingSearch($input, $with = [], $from = null, $to = null)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        //analyze date format on php
        $currentDate_from = date_create(date("Y-m-d"));
        $currentDate_to = date_create(date("Y-m-d"));

        //date_sub($currentDate, date_interval_create_from_date_string('30 days'));
        date_sub($currentDate_from, date_interval_create_from_date_string($from.'days'));
        date_sub($currentDate_to, date_interval_create_from_date_string($to.'days'));

        $fromDate=strtotime(date_format($currentDate_from, 'Y-m-d'));
        $toDate=strtotime(date_format($currentDate_to, 'Y-m-d'));

        $attributes = SelArr::removeNullOrEmptyString($input);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_hdr as ih')->select([
            'ih.cus_id',
            'ih.cus_name',
            DB::raw("count(ih.inv_id) AS num_of_invoice"),
            DB::raw("SUM(ih.total) AS amount"),
        ]);

        $query->where('ih.whs_id', $currentWH);
        //days: 0 -> 30
        if(empty($from) && !empty($to)){
            $query->where('ih.due_dt', '>=', (int)($toDate));
        }

        //days: 31 -> 60 & 61 -> 90
        if(!empty($from) && !empty($to)){
            $query->where('ih.due_dt', '<=', (int)($fromDate));
            $query->where('ih.due_dt', '>=', (int)($toDate));
        }

        //days: over 90
        if(!empty($from) && empty($to)){
            $query->where('ih.due_dt', '<=', (int)($fromDate));
        }

        if (isset($attributes['cus_id'])) {
            $query->where('ih.cus_id', $attributes['cus_id']);
        }
        //$query->groupBy('ih.inv_id');
        $row = $query->first();

        return $row;
    }

    //days: 0 -> 30
    public function agingSearchThirty($input, $with = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($input);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_hdr as ih')->select([
            'ih.cus_id',
            'ih.cus_name',
            DB::raw("count(ih.inv_id) AS num_of_invoice"),
            DB::raw("SUM(ih.total) AS amount"),
        ]);
        $query->where('ih.due_dt', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . 30 . ' DAY)'));
        if (isset($attributes['cus_id'])) {
            $query->where('ih.cus_id', $attributes['cus_id']);
        }
        //$query->groupBy('ih.inv_id');
        $row = $query->first();

        return $row;
    }

    //days:  31->60
    public function agingSearchSixty($input, $with = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($input);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_hdr as ih')->select([
            DB::raw("count(ih.inv_id) AS num_of_invoice"),
            DB::raw("SUM(ih.total) AS amount"),
        ])
            //-60 < = currentDate >= -31
            ->where('ih.due_dt', '<=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . 31 . ' DAY)'))
            ->where('ih.due_dt', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . 60 . ' DAY)'));
        if (isset($attributes['cus_id'])) {
            $query->where('ih.cus_id', $attributes['cus_id']);
        }
        $row = $query->first();

        return $row;
    }

    //days: 61 -> 90
    public function agingSearchNinety($input, $with = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($input);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_hdr as ih')->select([
            DB::raw("count(ih.inv_id) AS num_of_invoice"),
            DB::raw("SUM(ih.total) AS amount"),
        ])
            ->where('ih.due_dt', '<=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . 61 . ' DAY)'))
            ->where('ih.due_dt', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . 90 . ' DAY)'));
        if (isset($attributes['cus_id'])) {
            $query->where('ih.cus_id', $attributes['cus_id']);
        }
        $row = $query->first();

        return $row;
    }

    //days: over 91
    public function agingSearchOverNinety($input, $with = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($input);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_hdr as ih')->select([
            DB::raw("count(ih.inv_id) AS num_of_invoice"),
            DB::raw("SUM(ih.total) AS amount"),
        ])
            ->where('ih.due_dt', '<=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . 91 . ' DAY)'));
        if (isset($attributes['cus_id'])) {
            $query->where('ih.cus_id', $attributes['cus_id']);
        }
        $row = $query->first();

        return $row;
    }

    //days: total all
    public function agingSearchTotal($input, $with = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($input);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_hdr as ih')->select([
            DB::raw("count(ih.inv_id) AS num_of_invoice"),
            DB::raw("SUM(ih.total) AS amount"),
        ]);

        if (isset($attributes['cus_id'])) {
            $query->where('ih.cus_id', $attributes['cus_id']);
        }
        $row = $query->first();

        return $row;
    }


}

