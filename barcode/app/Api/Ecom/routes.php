<?php

// WavePick
$api->get('/wave', ['action' => "wavePick", 'uses' => 'WaveController@search']);

$api->get('/wave/{wvHdrNum}',
    ['action' => "wavePick", 'uses' => 'WaveController@show']);

$api->get('/wave/sku/{wvDtlId:[0-9]+}', [
    'action' => "wavePick",
    'uses'   =>
        'WaveController@detail'
]);

$api->get('/more-suggest-location', [
    'action' => 'wavePick',
    'uses'   => 'SuggestMoreLocationPickPalletController@moreSuggestLocation'
]);

$api->post('/check-wavepick-location', [
    'action' => 'createWavePick',
    'uses'   => 'WaveController@checkWvLocation'
]);

$api->put('/update-wavepick', [
    'action' => 'wavePick',
    'uses' => 'WaveController@updateWavePickEcom'
]);

// Pack
$api->get('/packs', ['action' => "orderPacking", 'uses' => 'PackController@search']);

$api->get('/packs/{trackingNum}', ['action' => "orderPacking", 'uses' => 'PackController@show']);

$api->post('/check-pack-detail', ['action' => "orderPacking", 'uses' => 'PackController@checkPackDetail']);

// Ecom Packing Updated
$api->get('/packing', ['action' => "orderPacking", 'uses' => 'PackingController@search']);

$api->get('/packing/{num}', ['action' => "orderPacking", 'uses' => 'PackingController@show']);

$api->post('/packing/{orderHdrId:[0-9]+}', ['action' => "orderPacking", 'uses' => 'PackingController@store']);

$api->get('/pack-ref', ['action' => "orderPacking", 'uses' => 'PackingController@packRef']);

// X-Fer Tickets
$api->get('/xfer-tickets', ['action' => "viewXFER", 'uses' => 'XferController@search']);

$api->get('/xfer-tickets/{xTicketNum}', ['action' => "viewXFER", 'uses' => 'XferController@show']);

$api->get('/xfer-tickets/{xTicketNum}/{xHdrNum}', ['action' => "viewXFER", 'uses' => 'XferController@detail']);

$api->put('/xfer-tickets/update', ['action' => "viewXFER", 'uses' => 'XferController@update']);

$api->get('/xfer-putaway-suggest/{upc}', ['action' => "viewXFER", 'uses' => 'XferController@ecomPutawaySuggest']);

$api->put('/xfer-tickets/putaway', ['action' => "viewXFER", 'uses' => 'XferController@ecomPutawayUpdate']);

$api->post('/totes/scan', [
    'action' => "viewXFER",
    'uses' => 'ToteController@scan'
]);

$api->get('/totes/{toteNum}', [
    'action'    => 'viewXFER',
    'uses'      => 'ToteController@getInfo'
]);


