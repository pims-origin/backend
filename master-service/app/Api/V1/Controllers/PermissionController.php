<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\PermissionService;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class PermissionController extends AbstractController
{
    protected $service;

    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new PermissionService($request);
    }

    public function getList(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getPermissionListByQueryString($queryStr);
        return $this->convertResponse($result);
    }
    
    public function getGrpList(IRequest $request)
    {
        $result = $this->service->getGrpList($this->userId);
        return $this->convertResponse($result);
    }

    public function getUserByPermission(IRequest $request, $permission)
    {
        $result = $this->service->getUserByPermission($permission);
        return $this->convertResponse($result);
    }
}
