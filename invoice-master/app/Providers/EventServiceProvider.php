<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // fire when invoice change status
        'App\Events\UpdateInvoiceEvent' => [
            'App\Listeners\InvoiceTrackingListener',
            'App\Listeners\SendMailListener', //uncomment for enable        
        ],
    ];

    public function boot() {
        parent::boot();
    }
}
