<?php

namespace App\Modules\Items\Eloquents;

use Illuminate\Database\Eloquent\Model;

class CusExUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_ex_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = ['user_id', 'cus_id', 'whs_id'];
}
