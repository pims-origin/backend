<?php

namespace App\Console\Commands;

use App\Api\V1\Models\DailyInventoryModel;
use App\Api\V1\Models\ForecastModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Traits\ForecastControllerTrait;
use Illuminate\Support\Facades\DB;

class DailyForecastsRpt extends AbstractCommand
{
    use ForecastControllerTrait;
    protected $signature = 'daily-forecast-rpt';
    protected $description = 'Update last month Forecast report ';
    protected $goodReceiptModel;
    protected $dailyInventoryModel;
    protected $odrHdrModel;
    protected $locationModel;
    protected $forecastModel;

    public function __construct()
    {
        parent::__construct();
        $this->goodReceiptModel = new GoodsReceiptModel();
        $this->dailyInventoryModel = new DailyInventoryModel();
        $this->odrHdrModel = new OrderHdrModel();
        $this->locationModel = new LocationModel();
        $this->forecastModel = new ForecastModel();
    }

    public function handle()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {
            DB::beginTransaction();
            //if ((int)date('d') == 1) {
                $currentDateTime = new \DateTime();
                $currentDateTime->modify('last day of previous month');
                $year = $currentDateTime->format('Y');
                $month = $currentDateTime->format('m');
                $forecastModel = new ForecastModel();
                $lastMonthForecasts = $forecastModel->search($year, $month, null, null, true);
                $groupedForecasts = [];
                //Group by warehouse and customer to use calculateActual function
                foreach ($lastMonthForecasts as $lastMonthForecast) {
                    $groupedForecasts[$lastMonthForecast['whs_id'] . $lastMonthForecast['cus_id']][] = $lastMonthForecast;
                }

                foreach ($groupedForecasts as &$groupedForecast) {
                    $groupedForecast = collect($groupedForecast)->keyBy('fc_ind_code');
                    $this->calculateActual($groupedForecast, $currentDateTime->format('Y-m-d'));
                }

                $groupedForecasts = collect($groupedForecasts)->flatten();

                foreach ($groupedForecasts as $groupedForecast) {
                    $this->forecastModel->refreshModel();
                    $this->forecastModel->update($groupedForecast->toArray());
                }
            //}
            DB::commit();

            $this->logSuccess('Update last month Forecast report successfully');
        } catch (\PDOException $e) {
            DB::rollback();
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            $this->logPdo($e->getMessage());
        }

    }
}
