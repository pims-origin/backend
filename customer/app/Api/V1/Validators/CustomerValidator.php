<?php

namespace App\Api\V1\Validators;


class CustomerValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_name'   => 'required',
            'cus_status' => 'exists:cus_status,cus_sts_code',
            'cus_code'   => 'required',
        ];
    }
}
