<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\RcvSum;

class RcvSumModel extends AbstractModel
{
    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model =  new RcvSum();
    }
    
    /**
     * @grHdr array
     * @return mixed
    */
    
    public function getRcvSql($grHdr) 
    {
        $result = DB::table('gr_hdr AS gh')
                    ->join('evt_tracking AS et', 'et.trans_num', '=', 'gh.gr_hdr_num')
                    ->join('gr_dtl AS gd', 'gd.gr_hdr_id', '=', 'gh.gr_hdr_id')
                    ->select('gh.whs_id',
                             'gh.cus_id',
                             'gh.ctnr_id', 
                             'gh.gr_hdr_id',
                              DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d") AS created'), 
                              'et.id AS event_id', 
                              DB::raw('1 AS container'), 
                              DB::raw('SUM(gr_dtl_act_ctn_ttl) AS carton'), 
                              DB::raw('SUM(gr_dtl_act_ctn_ttl * cube) AS vol'),
                              DB::raw('SUM(gr_dtl_act_ctn_ttl * pack) AS pieces')
                           )
                    ->where('evt_code', config('constants.event.GR-COMPLETE'))
                    ->whereIn('gh.gr_hdr_num', $grHdr)
                    ->groupBy('gh.gr_hdr_id')
                    ->get();        

        return $result;
    }
    
    /**
     * @grHdr array
     * @return mixed
    */
    
    public function getRcvPalletSql($grHdr) 
    {
        //get pallets total for each gr
        $palResult = DB::table('gr_hdr AS gh')
                            ->join('evt_tracking AS et', 'et.trans_num', '=', 'gh.gr_hdr_num')
                            ->join('pallet AS pl', 'pl.gr_hdr_id', '=', 'gh.gr_hdr_id')
                            ->select('gh.gr_hdr_id', 
                                     'gh.ctnr_id', 
                                      DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d") AS created_at'),
                                      DB::raw('COUNT(plt_id) AS plate')
                                    )
                            ->whereIn('gh.gr_hdr_num', $grHdr)
                            ->where('evt_code', config('constants.event.GR-COMPLETE'))
                            ->groupBy('gh.gr_hdr_id')
                            ->get();                

        $pallet=[];
        foreach ($palResult as $row) {
            $grHdrID = $row->gr_hdr_id;
            
            $pallet[$grHdrID] = $row->plate;
        }

        return $pallet;
    }
    
}
