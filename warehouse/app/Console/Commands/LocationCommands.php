<?php

namespace App\Console\Commands;

use App\Api\V1\Models\WarehouseZoneModel;
use Illuminate\Console\Command;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\File;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\JWTUtil;
use Exception;
use Wms2\UserInfo\Data;

class LocationCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'location:import {action} {hash} {offset=0} {limit=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Location';

    /**
     * @var WarehouseZoneModel
     */
    protected $warehouseZoneModel;

    /**
     * ImportLocation constructor.
     */
    public function __construct(WarehouseZoneModel $warehouseZoneModel)
    {
        parent::__construct();
        $this->warehouseZoneModel = $warehouseZoneModel;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action');
        $hash = $this->argument('hash');
        $offset = $this->argument('offset');
        $limit = $this->argument('limit');

        if ($action == "status") {
            $this->showStatus($hash);
        } elseif ($action == "process") {
            if (!empty($offset)) {
                // Offset not empty
                if (!empty($limit)) {
                    // Limit not empty
                    $this->importLocation($hash, $offset, $limit);
                } else {
                    // Limit empty
                    $this->error("There is not argument \"limit\".");
                }
            } else {
                $this->importLocation($hash, $offset, $limit);
            }
        } else {
            $this->error("Command \"location:import $action\" is not defined.");
        }
    }

    /**
     * @param $hashKey
     * @param $offset
     * @param $limit
     * @return array
     * @throws Exception
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    private function importLocation($hashKey, $offset, $limit)
    {
        $type = Type::XLSX;
        $filePath = storage_path('valid-import') . "/$hashKey.$type";
        $reader = ReaderFactory::create($type);
        $reader->open($filePath);

        $fieldsName = [
            'loc_code',
            'loc_alternative_name',
            'loc_type_id',
            'loc_length',
            'loc_width',
            'loc_height',
            'loc_available_capacity',
            'loc_weight_capacity',
            'loc_min_count',
            'loc_max_count',
            'loc_zone_id',
            'loc_sts_code',
            'rfid',
            'loc_whs_id',
        ];

        $result = [];
        $i = 0;
        $totalSave = 500;
        if (!empty($limit) && $limit > 500) {
            $min = round($limit / 5);
            $totalSave = (int)rand($min, $min + round(($limit / 6)));
        }

        try {

            $deleted_at = getDefaultDatetimeDeletedAt();

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $i++;
                    if ($i < $offset) {
                        continue;
                    }

                    if (!empty($limit) && ($limit + $offset) == $i) {
                        break;
                    }

                    foreach ($fieldsName as $idx => $fieldName) {
                        $rs[$fieldName] = !empty($row[$idx]) ? $row[$idx] : null;
                    }

                    $rs['created_by'] = JWTUtil::getPayloadValue('jti') ?? 0;
                    $rs['updated_by'] = JWTUtil::getPayloadValue('jti') ?? 0;
                    $rs['created_at'] = $rs['updated_at'] = time();
                    $rs['deleted_at'] = $deleted_at;
                    $rs['deleted'] = 0;

                    $result[] = $rs;

                    if ($i >= $totalSave) {
                        Location::insert($result);
                        $this->addLocationToZone($result);

                        // Save Status
                        DB::table('progress_bar')
                            ->where('transaction', $hashKey)
                            ->update([
                                'value' => DB::raw("value + $totalSave")
                            ]);

                        $i = 0;
                        $result = [];
                    }
                }
                // Only process one sheet
                break;
            }

            $reader->close();

            if (!empty($result)) {
                Location::insert($result);
                DB::table('progress_bar')
                    ->where('transaction', $hashKey)
                    ->update([
                        'value' => DB::raw("value + " . (count($result)))
                    ]);
                $this->addLocationToZone($result);
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            DB::table('sys_bugs')->insert([
                'date' => time(),
                'api_name' => 'Import Location - ' . date('Y-m-d'),
                'error' => $ex->getMessage(),
                'created_at' => time(),
                'created_by' => Data::getCurrentUserId(),
                'updated_at' => time()
            ]);
            if ($ex->getCode() === '23000') {
                throw new Exception('Duplicate locations', 404);
            } else {
                throw $ex;
            }
        }

        return ['status' => true, 'message' => 'The Location file was imported successfully!'];
    }

    /**
     * @param $locations
     */
    private function addLocationToZone($locations)
    {
        $param = [];
        if (!empty($locations)) {
            foreach ($locations as $location) {
                if (!empty($location['loc_zone_id'])) {
                    $zone_id = $location['loc_zone_id'];
                    $sts_code = $location['loc_sts_code'];
                    $param[$zone_id]['zone_id'] = $zone_id;

                    if (empty($param[$zone_id]['zone_num_of_loc'])) {
                        $param[$zone_id]['zone_num_of_loc'] = 0;
                    }
                    if (empty($param[$zone_id]['zone_num_of_active_loc'])) {
                        $param[$zone_id]['zone_num_of_active_loc'] = 0;
                    }
                    if (empty($param[$zone_id]['zone_num_of_inactive_loc'])) {
                        $param[$zone_id]['zone_num_of_inactive_loc'] = 0;
                    }

                    $param[$zone_id]['zone_num_of_loc'] += 1;
                    $param[$zone_id]['zone_num_of_active_loc'] += ($sts_code == config("constants.loc_status.ACTIVE"));
                    $param[$zone_id]['zone_num_of_inactive_loc'] +=
                        ($sts_code == config("constants.loc_status.INACTIVE"));
                }
            }
        }

        if (!empty($param)) {
            foreach ($param as $item) {
                $this->warehouseZoneModel->updateNumLoc($item);
            }
        }
    }

    /**
     * @param $hash
     */
    private function showStatus($hash)
    {
        if (!file_exists(storage_path("valid-import/$hash.txt"))) {
            return $this->error("File have not import yet.");
        }
        $contents = file_get_contents(storage_path("valid-import/$hash.txt"));

        return $this->info($contents);
    }
}