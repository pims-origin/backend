<?php
$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../../../')
);
$router = $app->router;
$router->group(['prefix' => 'ecom/orders'], function () use ($router) {
    $router->get('/', function ()    {
        echo "Ecom Orders module";
    });
});