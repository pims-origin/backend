<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14/08/2018
 * Time: 1:04 PM
 */

namespace App\Api\V2\BillOfLadings\UpdateBOL\Models;

use \Seldat\Wms2\Models\Customer;

class CustomerModel extends AbstractModel
{
    /**
     * WarehouseModel constructor.
     */
    public function __construct(Customer $model = null)
    {
        $this->model = ($model) ?: new Customer();
    }
}