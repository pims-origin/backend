<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\GoodsReceipt;

use Seldat\Wms2\Utils\SelArr;

class InventoryHistoryModel extends AbstractModel
{
    public function __construct(GoodsReceipt $model = NULL)
    {
        $this->model = ($model) ?: new GoodsReceipt();
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $isOrder = ! empty($searchAttr['odr_num']);
        $isContainer = ! empty($searchAttr['ctnr_num']);

        $emptyBoth = ! $isOrder && ! $isContainer;

        //get order
        $order = $emptyBoth || $isOrder ?
                $this->orderQuery($attributes, $searchAttr) : NULL;

        //get container
        $container = $emptyBoth || $isContainer ?
                $this->containerQuery($attributes, $searchAttr) : NULL;

        //history - either container or order or both
        if ($emptyBoth || $isOrder && $isContainer) {
            $history = $container->union($order);
        } else {
            $history = $order ? $order : $container;
        }

        //final query
        $final = DB::table(DB::raw("({$history->toSql()}) as inv"))
                    ->mergeBindings($history);

        $this->sortQuery($searchAttr, $final);

        $models = $final->paginate($limit);

        return $models;
    }

    /*
    ****************************************************************************
    */

    public function sortQuery($searchAttr, $model)
    {
        foreach (getDefault($searchAttr['sort'], []) as $key => $value) {

            $value = $value ?: 'asc';

            if (! in_array($value, ['asc', 'desc'])) {
                continue;
            }

            $model->orderBy($key, $value);
        }

        return $model;
    }

    /*
    ****************************************************************************
    */

    public function orderQuery($attributes, $searchAttr)
    {
        $query = DB::table('shipment AS sh')
            ->join('odr_hdr AS oh', 'oh.ship_id', '=', 'sh.ship_id')
            ->join('evt_tracking AS et', 'et.trans_num', '=', 'oh.odr_num')
            ->join('customer AS c', 'c.cus_id', '=', 'oh.cus_id')
            ->join('warehouse AS w', 'w.whs_id', '=', 'oh.whs_id')
            ->select(
                'oh.whs_id AS whs_id',
                'whs_name',
                'oh.cus_id AS cus_id',
                'cus_name',
                DB::raw('"" AS ctnr_num'),
                'et.created_at AS log_date',
                'odr_num AS odr_num',
                'ctn_qty_ttl AS box',
                'piece_qty_ttl AS pieces'
            )
            ->where('evt_code', 'OSH');

        $attributes['compare_operator']['odr_num'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'odr_num',
                'whs_id',
                'cus_id',
                'log_date1',
            ],
            'attributes' => $attributes,
            'tablePreface' => [
                'cus_id' => 'oh',
                'whs_id' => 'oh',
            ],
        ]);

        return $query;
    }

    /*
    ****************************************************************************
    */

    public function containerQuery($attributes, $searchAttr)
    {
        $subGdr = DB::table('gr_hdr AS gh')
            ->join('evt_tracking AS et', 'gh.gr_hdr_num' , '=',
                DB::raw('
                    REPLACE(
                        SUBSTRING_INDEX(et.trans_num, "-", 4), "LPN", "GDR"
                    )'
                )
            )
            ->join('gr_dtl AS gd', 'gd.gr_hdr_id', '=', 'gh.gr_hdr_id')
            ->join('asn_dtl AS ad', 'ad.asn_dtl_id', '=', 'gd.asn_dtl_id')
            ->join('container AS co', 'co.ctnr_id', '=', 'gh.ctnr_id')
            ->select(
                'gh.whs_id',
                'gh.cus_id',
                'gh.ctnr_id',
                'co.ctnr_num',
                'et.created_at AS log_date',
                DB::raw('gr_dtl_act_ctn_ttl AS box'),
                DB::raw('gr_dtl_act_ctn_ttl * asn_dtl_pack AS pieces')
            )
            ->where('evt_code', 'PUT')
            ->where('gh.deleted', 0)
            ->groupBy('gd.gr_dtl_id');

        $subQuery = DB::raw("({$subGdr->toSql()}) as sub");

        $query = DB::table($subQuery)
            ->mergeBindings($subGdr)
            ->join('customer AS c', 'c.cus_id', '=', 'sub.cus_id')
            ->join('warehouse AS w', 'w.whs_id', '=', 'sub.whs_id')
            ->select(
                'sub.whs_id',
                'whs_name',
                'sub.cus_id',
                'cus_name',
                'ctnr_num',
                'log_date',
                DB::raw('"" AS odr_num'),
                DB::raw('SUM(box) AS box'),
                DB::raw('SUM(pieces) AS pieces')
            )
            ->groupBy('ctnr_id','whs_id','cus_id');

        $attributes['compare_operator']['ctnr_num'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'ctnr_num',
                'whs_id',
                'cus_id',
                'log_date',
            ],
            'attributes' => $attributes,
            'tablePreface' => [
                'cus_id' => 'sub',
                'whs_id' => 'sub',
            ],
            'dateFields' => [
                'log_date',
            ],
        ]);

        return $query;
    }

    /*
    ****************************************************************************
    */
}
