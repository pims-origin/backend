<?php

namespace App\Modules\Asns\Eloquents;

class AsnDtlMeta extends BaseSoftModel
{
    protected $table = 'asn_dtl_meta';

    protected $primaryKey = 'asn_dtl_id';

    protected $fillable = [
        'qualifier',
        'asn_dtl_id',
        'value'
    ];
}
