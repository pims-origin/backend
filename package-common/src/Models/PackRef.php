<?php
namespace Seldat\Wms2\Models;

/**
 * Class PackRef
 *
 * @package Seldat\Wms2\Models
 */
class PackRef extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pack_ref';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pack_ref_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'width',
        'height',
        'length',
        'dimension',
        'pack_type',
        'created_at',
        'updated_at',
        'pack_ref_label'
    ];

    public function packType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\PackType', 'pack_type', 'pack_code');
    }

}
