<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class ClientMasterTransformer extends TransformerAbstract
{
    public function transform($sac)
    {
        return [
            "block"           => $sac->block,
            "description"     => $sac->description,
            "period_uom"      => $sac->period_uom,
            "period_uom_name" => $sac->period_uom_name,
            "price"           => $sac->price,
            "base_price"      => $sac->price,
            "range_from"      => $sac->range_from,
            "range_to"        => $sac->range_to,
            "range_uom"       => $sac->range_uom,
            "range_uom_name"  => $sac->range_uom_name,
            "sac"             => $sac->sac,
            "sac_id"          => $sac->sac_id,
            "sac_name"        => $sac->sac_name,
            "sac_period"      => $sac->sac_period,
            "type"            => $sac->type,
            'type_name'       => $sac->type_name,
            "unit_uom"        => $sac->unit_uom,
            'unit_uom_name'   => $sac->unit_uom_name,
            'group'           => $sac->group,
        ];
    }
}
