<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class InventorySummaryModel extends AbstractModel
{
    /**
     * @var GoodsReceiptDetailModel
     */
    protected $grDtlModel;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
        $this->grDtlModel = new GoodsReceiptDetailModel();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        // search sku
        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        // search sku
        if (isset($attributes['color'])) {
            $query->where('color', 'like', "%" . SelStr::escapeLike($attributes['color']) . "%");
        }

        // search sku
        if (isset($attributes['size'])) {
            $query->where('size', 'like', "%" . SelStr::escapeLike($attributes['size']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * @param array $data
     * @param int $cus_id
     * @param int $whs_id
     */
    public function updateInventorySummary(array $data, $cus_id = 0, $whs_id = 0)
    {
        foreach ($data as $detail) {
            $total = $detail['gr_dtl_act_ctn_ttl'] * $detail['asn_detail']['asn_dtl_pack'];
            $allocatedQty = array_get($detail, 'allocated_qty', null);
            $dmgQty = array_get($detail, 'dmg_qty', null);
            $whs_id = $whs_id ? $whs_id : array_get($detail, 'whs_id', null);
            $fields = [
                'item_id'       => array_get($detail, 'asn_detail.item_id', null),
                'cus_id'        => $cus_id ? $cus_id : array_get($detail, 'cus_id', null),
                'whs_id'        => $whs_id,
                'color'         => array_get($detail, 'asn_detail.item.color', null),
                'size'          => array_get($detail, 'asn_detail.item.size', null),
                'lot'           => array_get($detail, 'asn_detail.lot', 'NA'),
                'allocated_qty' => $allocatedQty,
                'dmg_qty'       => $dmgQty,
                'sku'           => array_get($detail, 'asn_detail.item.sku', null),
                'created_at'    => time(),
                'updated_at'    => time()
            ];

            if ($total > 0) {
                $fields = array_merge($fields, [
                    'ttl'   => $total,
                    'avail' => ($total - $allocatedQty - $dmgQty)
                ]);
            }
            $this->refreshModel();
            $ivt = $this->model
                ->where('item_id', $detail['asn_detail']['item_id'])
                ->where('whs_id', $whs_id)
                ->first();

            if ($ivt) {
                $this->updateWhere([
                    "ttl"           => $ivt->ttl + $fields['ttl'],
                    "allocated_qty" => (empty($ivt->allocated_qty) ? 0 : $ivt->allocated_qty) + $fields['allocated_qty'],
                    "dmg_qty"       => (empty($ivt->dmg_qty) ? 0 : $ivt->dmg_qty) + $fields['dmg_qty'],
                    "avail"         => (empty($ivt->avail) ? 0 : $ivt->avail) + $fields['avail']
                ], [
                    "item_id" => $detail['asn_detail']['item_id'],
                    "whs_id"  => $whs_id
                ]);
            } else {
                $this->model->insert($fields);
            }
        }
    }

    /**
     * @param array $data
     */
    public function updateDmgQty(array $data)
    {
        foreach ($data as $detail) {
            // Update Status
            $this->updateGrSt($detail['gr_dtl_id'], 'RG', $detail['dmg_qty']);
        }
    }

    private function updateGrSt($grId, $sts, $dmgQty = 0)
    {
        $this->grDtlModel->refreshModel();
        $this->grDtlModel->update([
            'gr_dtl_id'      => $grId,
            'gr_dtl_sts'     => $sts,
            'gr_dtl_dmg_ttl' => $dmgQty,
        ]);
    }

    public function xdocUpdateInvtSmr(array $grDtl, $cusId)
    {
        $whsId = Data::getCurrentWhsId();
        $ivt = $this->getFirstWhere([
            'item_id' => $grDtl['item_id'],
            'lot'     => $grDtl['lot'],
            'whs_id'  => $whsId
        ]);

        if ($ivt) {
            $this->updateWhere([
                "crs_doc_qty" => DB::raw("crs_doc_qty + " . $grDtl['crs_doc_qty']),
                "ttl"         => DB::raw("ttl + " . $grDtl['crs_doc_qty'])
            ], [
                "item_id" => $grDtl['item_id'],
                "whs_id"  => $whsId,
                'lot'     => $grDtl['lot'],
            ]);
        } else {
            $this->model->create([
                'item_id'       => $grDtl['item_id'],
                'cus_id'        => $cusId,
                'whs_id'        => $whsId,
                'color'         => array_get($grDtl, "color", null),
                'sku'           => array_get($grDtl, "sku", null),
                'size'          => array_get($grDtl, "size", null),
                'lot'           => $grDtl['lot'],
                'ttl'           => $grDtl['crs_doc_qty'],
                'allocated_qty' => 0,
                'dmg_qty'       => 0,
                'avail'         => 0,
                'back_qty'      => 0,
                'upc'           => array_get($grDtl, "upc", null),
                'crs_doc_qty'   => $grDtl['crs_doc_qty']
            ]);
        }
    }
}
