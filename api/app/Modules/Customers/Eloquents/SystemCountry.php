<?php

namespace App\Modules\Customers\Eloquents;

use Illuminate\Database\Eloquent\Model;

class SystemCountry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_country';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_country_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sys_country_code',
        'sys_country_name',
        'ordinal'
    ];
}
