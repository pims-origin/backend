<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 1-July-17
 * Time: 09:25
 */

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReturnDtl;

use League\Fractal\TransformerAbstract;

/**
 * Class ReturnDtlTransformer
 *
 * @package App\Api\V1\Transformers
 */
class ReturnDtlTransformer extends TransformerAbstract
{
    /**
     * @param ReturnDtl $returnDtl
     *
     * @return array
     */
    public function transform(ReturnDtl $returnDtl)
    {
        return [
            'cus_code'   => object_get($returnDtl, 'item.customer.cus_code', ''),
            'cus_name'   => object_get($returnDtl, 'item.customer.cus_name', ''),
            'return_num' => $returnDtl->return_num,
            'item_id'    => $returnDtl->item_id,
            'sku'        => $returnDtl->sku,
            'size'       => $returnDtl->size,
            'color'      => $returnDtl->color,
            'lot'        => $returnDtl->lot,
            'cus_upc'    => object_get($returnDtl, 'item.cus_upc', ''),
            'return_qty' => $returnDtl->piece_qty ?: 0,
            'created_at' => date("m/d/Y", strtotime($returnDtl->created_at)),
            'created_by' => trim(object_get($returnDtl, "createdBy.first_name", null) . " " . object_get($returnDtl,
                    "createdBy.last_name", null)),
        ];
    }

}
