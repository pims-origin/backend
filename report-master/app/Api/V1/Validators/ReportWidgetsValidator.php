<?php

namespace App\Api\V1\Validators;


class ReportWidgetsValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'data'    => 'required',
            'name'    => 'required',
            'ordinal' => 'required',
        ];
    }
}
