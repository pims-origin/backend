<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\CustomerConfig;
use Seldat\Wms2\Models\Edi;
use Seldat\Wms2\Utils\SelArr;

class CustomersModel extends AbstractModel
{
    public function __construct(Customers $model = null)
    {
        $this->model = ($model) ?: new Customer();
    }

    /*
    ****************************************************************************
    */

    /**
     * @param $whsId
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function search($whsId, $attributes, array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        //get configVal in edi
        $configVal = array_pluck(
            Edi::select('edi_trans')
                ->where('edi_sts', 'AC')
                ->get(), 'edi_trans', 'edi_trans');

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }

        $query->whereHas('customerConfig', function ($query) use ($whsId, $attributes, $configVal) {
            $query->where('whs_id', $whsId);

            // search according to Order  Number
            if (isset($attributes['edi'])) {
                if (strtoupper($attributes['edi']) == 'Y') {

                    $cusIds = array_pluck(
                        CustomerConfig::select('cus_id')
                            ->whereIn('config_value', $configVal)
                            ->groupBy('cus_id')
                            ->get(), 'cus_id', 'cus_id');

                    foreach ($cusIds as $cusId) {
                        //count configVal
                        $rcNos = CustomerConfig::select(DB::raw('count(*) as rcNo'))
                            ->whereIn('config_value', $configVal)
                            ->where('cus_id', $cusId)
                            ->groupBy('cus_id')
                            ->first()->toArray();
                        $rcNo = CustomerConfig::select(DB::raw('count(*) as rcNo'))
                            ->whereIn('config_value', $configVal)
                            ->where('cus_id', $cusId)
                            ->where('ac', 'N')
                            ->groupBy('cus_id')
                            ->first()->toArray();
                        if (isset($rcNo['rcNo']) && $rcNo['rcNo'] == $rcNos['rcNo']) {
                            $getCusId = $cusId;
                            unset($cusIds[$getCusId]);
                        }
                    }
                    $query->whereIn('cus_id', $cusIds);

                }
                if (strtoupper($attributes['edi']) == 'N') {

                    $cusIds = array_pluck(
                        CustomerConfig::select('cus_id')
                            ->whereIn('config_value', $configVal)
                            ->groupBy('cus_id')
                            ->get(), 'cus_id', 'cus_id');
                    foreach ($cusIds as $cusId) {
                        //count configVal
                        $rcNos = CustomerConfig::select(DB::raw('count(*) as rcNo'))
                            ->whereIn('config_value', $configVal)
                            ->where('cus_id', $cusId)
                            ->groupBy('cus_id')
                            ->first()->toArray();

                        $rcNo = CustomerConfig::select(DB::raw('count(*) as rcNo'))
                            ->whereIn('config_value', $configVal)
                            ->where('cus_id', $cusId)
                            ->where('ac', 'N')
                            ->groupBy('cus_id')
                            ->first()->toArray();
                        if (isset($rcNo['rcNo']) && $rcNo['rcNo'] == $rcNos['rcNo']) {
                            $getCusId = $cusId;
                            unset($cusIds[$getCusId]);
                        }
                    }
                    $query->whereNotIn('cus_id', $cusIds);

                }
            }

        });

        $this->sortBuilder($query, $attributes);

        $rows = $query->paginate($limit);

        return $rows;
    }

}
