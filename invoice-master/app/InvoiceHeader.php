<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\User;
use App\InvoiceTransaction;
use App\InvoiceDetail;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Models\CustomerMeta;
use App\Wms2\UserInfo\Data;

class InvoiceHeader extends BaseInvoiceModel
{
    protected $table             = 'inv_hdr';
    protected $primaryKey        = 'inv_id';
    protected $dateFormat        = 'Y-m-d';
    protected $invoiceStatusName = [
        'NE' => 'New',
        'PE' => 'Pending',
        'RE' => 'Rejected',
        'AP' => 'Approved',
        'PA' => 'Paid',
        'OV' => 'Overdue',
    ];
    protected $emailStatus = [
        '1' => 'Not sent',
        '2' => 'Sent',
    ];
    protected $fillable = [
        'ref_num',
        'inv_num',
        'inv_sts',
        'whs_id',
        'cus_id',
        'ref_dt',
        'inv_dt',
        'terms',
        'currency',
        'subtotal',
        'total',
        'issue_dt',
        'due_dt',
        'inv_from',
        'inv_to',
        'note',
        'tax',
        'email_sts',
        'discount',
        'approved_by',
        'approved_dt',
        'bill_from_name',
        'bill_from_add1',
        'bill_from_add2',
        'bill_from_city_name',
        'bill_from_state',
        'bill_from_zip',
        'bill_from_country',
        'bill_to_name',
        'bill_to_add1',
        'bill_to_add2',
        'bill_to_city_name',
        'bill_to_state',
        'bill_to_zip',
        'bill_to_country',
        'paid_dt',
    ];

    protected $dispatchesEvents = [
        'created' => Events\UpdateInvoiceEvent::class,
        'updated' => Events\UpdateInvoiceEvent::class,
    ];

    public static function boot()
    {
        parent::boot();

        $userId = Data::getCurrentUserId() ?? 1;

        static::updating(function ($table) use ($userId) {
            if ($table->inv_sts == 'AP') {
                $table->updated_by = $userId;
                $table->approved_by = $userId;
                $table->approved_dt = time();
            } else {
                $table->approved_by = Null;
                $table->approved_dt = Null;
                $table->updated_by = $userId;
            }
        });
    }

    public function detail()
    {
        return $this->hasMany(InvoiceDetail::class, 'inv_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'cus_id');
    }

    public function customerMeta()
    {
        return $this->belongsTo(CustomerMeta::class, 'cus_id');
    }

    public function approvedBy()
    {
        return $this->belongsTo(User::class, 'approved_by');
    }

    public function getApprovedNameAttribute()
    {
        if ($this->approvedBy) {
            return $this->approvedBy->first_name . ' ' . $this->approvedBy->last_name;
        }

        return null;
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getCreatedNameAttribute()
    {
        if ($this->createdBy) {
            return $this->createdBy->first_name . ' ' . $this->createdBy->last_name;
        }

        return null;
    }

    public function getApprovedDateFormatAttribute()
    {
        if ($this->approved_dt) {
            return date($this->dateFormat, $this->approved_dt);
        }

        return '';
    }

    public function getCusNameAttribute()
    {
        return $this->customer->cus_name ?? null;
    }

    public function getDueDtFormatAttribute()
    {
        return date($this->dateFormat, $this->due_dt);
    }

    public function getInvDtFormatAttribute()
    {
        return date($this->dateFormat, $this->inv_dt);
    }

    public function getIssueDtFormatAttribute()
    {
        return date($this->dateFormat, $this->issue_dt);
    }

    public function getPaidDtFormatAttribute()
    {
        return $this->paid_dt ? date($this->dateFormat, $this->paid_dt) : '';
    }

    public function getPeriodFormatAttribute()
    {
        return ($this->inv_from && $this->inv_to) ? date($this->dateFormat, $this->inv_from) . ' / ' . date($this->dateFormat, $this->inv_to) : '';
    }

    public function getInvStsNameAttribute()
    {
        return $this->getInvoiceStatusName($this->inv_sts);
    }

    public function getEmailStsNameAttribute()
    {
        return $this->getEmailStatusName($this->email_sts);
    }

    public function transaction()
    {
        return $this->hasMany(InvoiceTransaction::class, 'inv_id');
    }

    public function setInvDtAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['inv_dt'] = strtotime(date($this->dateFormat));
        }

        return $this->attributes['inv_dt'] = strtotime($value);
    }

    public function setIssueDtAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['issue_dt'] = strtotime(date($this->dateFormat));
        }

        return $this->attributes['issue_dt'] = strtotime($value);
    }

    public function setDueDtAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['due_dt'] = strtotime(date($this->dateFormat));
        }

        return $this->attributes['due_dt'] = strtotime($value);
    }

    public function setInvFromAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['inv_from'] = strtotime(date($this->dateFormat));
        }

        return $this->attributes['inv_from'] = strtotime($value);
    }

    public function setInvToAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['inv_to'] = strtotime(date($this->dateFormat));
        }

        return $this->attributes['inv_to'] = strtotime($value);
    }

    public function setInvStsAttribute($value)
    {
        return $this->attributes['inv_sts'] = strtoupper($value);
    }

    public function setRefDtAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['ref_dt'] = strtotime(date($this->dateFormat));
        }

        return $this->attributes['ref_dt'] = strtotime($value);
    }

    public function setInvNumAttribute($value)
    {
        if (empty($value)) {
            $invCode = 'INV-' . date('ym');
            $invoice = $this->where('inv_num', 'LIKE', $invCode . '-%')
                ->withTrashed()
                ->latest()
                ->first();
            $invNum                             = $invoice ? $invoice->inv_num : null;
            return $this->attributes['inv_num'] = $invNum ? ++$invNum : $this->generateInvoiceNum($invCode);
        }
        return $this->attributes['inv_num'] = $value;

    }

    private function generateInvoiceNum($invCode = null)
    {
        return ($invCode ?: 'INV-' . date('ym')) . '-' . str_pad(1, 5, '0', STR_PAD_LEFT);
    }

    private function getInvoiceStatusName($invoiceStatusCode)
    {
        if (array_key_exists($invoiceStatusCode, $this->invoiceStatusName)) {
            return $this->invoiceStatusName[$invoiceStatusCode];
        }

        return null;
    }

    private function getEmailStatusName($invoiceStatusCode)
    {
        if (array_key_exists($invoiceStatusCode, $this->emailStatus)) {
            return $this->emailStatus[$invoiceStatusCode];
        }

        return null;
    }

    public function setDiscountAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['discount'] = 0;
        }

        return $this->attributes['discount'] = $value;
    }

    public function setTaxAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['tax'] = 0;
        }

        return $this->attributes['tax'] = $value;
    }

    public function setSubtotalAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['subtotal'] = 0;
        }

        return $this->attributes['subtotal'] = $value;
    }

    public function setTotalAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['total'] = 0;
        }

        return $this->attributes['total'] = $value;
    }

    public function paidAmountRelation()
    {
        return $this->transaction()->selectRaw('inv_id, sum(amount) as paid')->groupBy('inv_id');
    }

    public function getPaidAmountAttribute(Type $var = null)
    {
        return $this->paidAmountRelation->first() ? $this->paidAmountRelation->first()->paid : 0;
    }

    public function getBalanceAmountAttribute(Type $var = null)
    {
        return $this->total - $this->paid_amount;
    }

    public function getCustomerConfig()
    {   
        $customerConfig = $this->customerMeta()->where('qualifier', 'CIO')->first();
        return $customerConfig ? json_decode($customerConfig->value) : false;
    }

    public function getCustomerEmailConfig()
    {   
        $customerEmailConfig = $this->customerMeta()->where('qualifier', 'CIE')->first();
        return $customerEmailConfig ? json_decode($customerEmailConfig->value) : false;
    }

    public function getAskWhenReopenAttribute()
    {
        return $this->getCustomerConfig() ? $this->getCustomerConfig()->ask_when_reopen : 0;
    }

    public function getAskWhenRejectAttribute()
    {
        return $this->getCustomerConfig() ? $this->getCustomerConfig()->ask_when_reject : 0;
    }

    public function getCustomerEmailAttribute()
    {
        return $this->getCustomerEmailConfig() ? $this->getCustomerEmailConfig()->customer_email : '';
    }

    public function getSendToCustomerAttribute()
    {
        return $this->getCustomerEmailConfig() ? $this->getCustomerEmailConfig()->send_to_customer : false;
    }
}
