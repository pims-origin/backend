<?php

namespace Seldat\Wms2\Models;

class CustomerWarehouse extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_warehouse';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id'
    ];

    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function location()
    {
        return $this->hasMany(__NAMESPACE__ . '\Location', 'loc_whs_id', 'whs_id');
    }

    public function asnHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnHdr', 'cus_id', 'cus_id');
    }
}
