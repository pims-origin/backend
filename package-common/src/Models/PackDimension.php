<?php

namespace Seldat\Wms2\Models;

/**
 * Class PackDimension
 *
 * @package Seldat\Wms2\Models
 */
class PackDimension extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pack_dimension';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'dms_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'width',
        'height',
        'length',
        'uom_id'
    ];

    public function systemUom()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemUom', 'sys_uom_id', 'uom_id');
    }
}
