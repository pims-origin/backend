<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Transformers\OrderTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class OrderController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new OrderHdrModel();
        $this->transformer = new OrderTransformer();
    }

    /*
    ****************************************************************************
    */

}
