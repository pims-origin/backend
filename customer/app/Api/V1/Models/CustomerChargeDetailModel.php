<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\CustomerChargeDetail;

class CustomerChargeDetailModel extends AbstractModel
{
    /**
     * CustomerChargeDetailModel constructor.
     *
     * @param CustomerChargeDetail|null $model
     */
    public function __construct(CustomerChargeDetail $model = null)
    {
        $this->model = ($model) ?: new CustomerChargeDetail();
    }

    /**
     * @param $customer_id
     * @param $warehouseId
     * @param $chargeCodeId
     *
     * @return mixed
     */
    public function deleteCustomerChargeDetail($customer_id, $warehouseId, $chargeCodeId)
    {
        return $this->model
            ->where('cus_id', $customer_id)
            ->where('whs_id', $warehouseId)
            ->where('chg_code_id', $chargeCodeId)
            ->delete();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateCustomerChangeDetail($data)
    {
        return $this->model
            ->where('cus_id', $data['cus_id'])
            ->where('whs_id', $data['whs_id'])
            ->where('chg_code_id', $data['chg_code_id'])
            ->update($data);
    }

    public function saveChargeDetail(array $data)
    {
        $temp = $this->model
            ->where('cus_id', $data['cus_id'])
            ->where('whs_id', $data['whs_id'])
            ->where('chg_code_id', $data['chg_code_id'])
            ->first();

        $this->refreshModel();

        if ($temp) {
            // Update
            return $this->model
                ->where('cus_id', $data['cus_id'])
                ->where('whs_id', $data['whs_id'])
                ->where('chg_code_id', $data['chg_code_id'])
                ->update($data);
        } else {
            // Create
            return $this->model->create($data);
        }
    }
}
