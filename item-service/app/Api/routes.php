<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group([
    'prefix' => 'v1',
    'namespace' => 'App\Api\V1\Controllers',
], function ($app) {
    $app->get('items', 'ItemController@index');
    $app->get('items/{id:[0-9]+}', 'ItemController@show');
    $app->get('items/{id:[0-9]+}/detail', 'ItemController@getItemById');
    $app->post('items', 'ItemController@store');
    $app->post('items/check-items', 'ItemController@check');
    $app->post('items/multi', 'ItemController@multiStore');
    $app->put('items/{id:[0-9]+}', 'ItemController@update');
    $app->post('items/import', 'ItemController@import');

    $app->get('/item-uom', 'ItemController@getItemUom');
    $app->get('/item-level', 'ItemLevelController@index');

    //item packing
    $app->post('/item-packing', 'ItemPackingController@create');
    $app->get('/item-packing/{itemId:[0-9]+}', 'ItemPackingController@show');

    //Export item to excel
    $app->get('/item-export-excel', 'ItemController@exportToExcel');

    $app->get('/item-charge-by', 'ItemController@getItemChargeBy');


});
