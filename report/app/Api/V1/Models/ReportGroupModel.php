<?php

namespace App\Api\V1\Models;

use App\ReportGroup;

class ReportGroupModel extends AbstractModel
{

    /**
     * ReportGroupModel constructor.
     *
     * @param ReportGroup|null $model
     */
    public function __construct(ReportGroup $model = null)
    {
        $this->model = ($model) ?: new ReportGroup();
    }

    /**
     * @return object
     */
    public function getListReportGroup($with = [])
    {
        $query = $this->make($with);
        $query->orderBy('disp_odr');

        return $query->get();
    }
}
