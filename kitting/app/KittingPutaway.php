<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

class KittingPutaway extends BaseInvoiceModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kitting_putaway';

    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $fillable = [
        'kitting_id',
        'kitting_dtl_id',
        'plt_id',
        'loc_id',
        'loc_code',
        'loc_name',
        'item_id',
        'sku',
        'size',
        'color',
        'ctn_qty',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted',
        'deleted_at',
    ];
}
