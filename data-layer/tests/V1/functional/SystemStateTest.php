<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\WarehousesystemState;
use App\WarehousesystemStateType;
use App\systemStateLocationStatus;
use App\SystemState;
use App\SystemCountry;

class SystemStateTest extends TestCase
{
    use DatabaseTransactions;

    protected $systemStateParams = [
        'sys_state_code' => 's1c',
        'sys_state_name' => 's1s73ms7a7311a111e',
        'sys_state_desc' => 'DESC'

    ];

    public function setUp()
    {
        parent::setUp();

        // create data into DB
        $sysCountryId = factory(SystemCountry::class)->create()->sys_country_id;

        $this->systemStateParams['sys_state_country_id'] = $sysCountryId;
    }

    private function getEndPoint()
    {
        return "/v1/system-states";
    }

    public function testStore_NoParam_Fail()
    {
        $endPoint = $this->getEndPoint();
        $this->call('POST', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_HasParam_Ok()
    {
        $response = $this->call('POST', $this->getEndPoint(), $this->systemStateParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
        $response = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('sys_state_id', $response['data']);
        $this->assertArrayHasKey('sys_state_code', $response['data']);
        $this->assertArrayHasKey('sys_state_name', $response['data']);
        $this->assertArrayHasKey('sys_state_desc', $response['data']);
    }

    public function testStore_DuplicateSystemStateCode_Fail()
    {
        // make data
        $systemStateCode = factory(SystemState::class)->create()->sys_State_code;

        $this->systemStateParams['sys_state_code'] = $systemStateCode;

        $this->call('POST', $this->getEndPoint(), $this->systemStateParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testShow_Ok()
    {
        $systemStateId = factory(SystemState::class)->create()->sys_state_id;
        $endPoint = $this->getEndPoint() . '/' . $systemStateId;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testShow_SystemStateNotExisted_Fails()
    {
        $endPoint = $this->getEndPoint() . '/999999999';
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testUpdate_Ok()
    {
        // make data
        $systemState = factory(SystemState::class)->create();

        $endPoint = $this->getEndPoint() . '/' . $systemState->sys_state_id;

        $response = $this->call('PUT', $endPoint, $this->systemStateParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $response = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('sys_state_name', $response['data']);
        $this->assertArrayHasKey('sys_state_code', $response['data']);
        $this->assertArrayHasKey('sys_state_id', $response['data']);
        $this->assertArrayHasKey('sys_state_desc', $response['data']);
    }

    public function testUpdate_DuplicateSystemStateCode_Fail()
    {
        // make data
        $sysStateCode = factory(SystemState::class)->create();
        $systemStateId = factory(SystemState::class)->create()->sys_state_id;

        $this->systemStateParams['sys_state_code'] = $sysStateCode->sys_state_code;
        $this->systemStateParams['sys_state_country_id'] = $sysStateCode->sys_state_country_id;

        $re = $this->call('PUT', $this->getEndPoint() . '/' . $systemStateId, $this->systemStateParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDestroy_SingleItem_Ok()
    {
        $sysStateId = factory(SystemState::class)->create()->sys_state_id;
        $endPoint = $this->getEndPoint() . '/' . $sysStateId;

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDestroy_systemStateIdNotExisted_Fail()
    {
        $endPoint = $this->getEndPoint() . '/999999';

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $sysCountries = factory(SystemState::class, 3)->create();
        $params = [];
        foreach ($sysCountries as $key => $sysState) {
            $params['sys_state_id'][$key] = $sysState->sys_state_id;
        }
        $this->call('DELETE', $this->getEndPoint(), $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDeleteMass_NoParams_Fail()
    {
        $this->call('DELETE', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testSearch_NoParams_Fail()
    {
        $endPoint = $this->getEndPoint();
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_Ok()
    {
        $systemState = factory(SystemState::class)->create();

        $endPoint = $this->getEndPoint() . '?sys_state_id=' . $systemState->sys_state_id
            . '&sys_state_name=' . $systemState->sys_state_name
            . '&sys_state_code=' . $systemState->sys_state_code
            . '&sys_state_desc=' . $systemState->sys_state_desc;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testListStatesByCountryId_Ok()
    {
        factory(SystemState::class, 3);
        factory(SystemState::class, 2)
            ->create(['sys_state_country_id' => $this->systemStateParams['sys_state_country_id']]);

        $response = $this->call('GET',
            "/v1/system-countries/{$this->systemStateParams['sys_state_country_id']}/states");

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
        $this->assertCount(2, $responseData['data']);
    }
}
