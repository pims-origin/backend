<?php

namespace App\Api\Ecom\Models;

use Seldat\Wms2\Models\EventTracking;

class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * @param EventTracking $model
     */
    public function __construct(EventTracking $model)
    {
        $this->model = $model;
    }

}
