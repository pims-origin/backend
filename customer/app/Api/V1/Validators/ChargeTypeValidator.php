<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:58
 */

namespace App\Api\V1\Validators;


class ChargeTypeValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'chg_type_code' => 'required',
            'chg_type_name' => 'required',
        ];
    }
}