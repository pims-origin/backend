<?php

namespace App\Api\V2\Order\GetLabelLink\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderLabelLink;
use Seldat\Wms2\Utils\Status;

/**
 * Class LabelLinkTransformer
 *
 * @package App\Api\V2\Order\GetLabelLink\Transformers
 */
class LabelLinkTransformer extends TransformerAbstract
{
    /**
     * @param OrderLabelLink $orderLabelLink
     *
     * @return array
     */
    public function transform(OrderLabelLink $orderLabelLink)
    {
        return [
            'odr_lbl_id'  => object_get($orderLabelLink, 'odr_lbl_id'),
            'label'       => object_get($orderLabelLink, 'ucc128'),
            'qty'         => object_get($orderLabelLink, 'qty'),
        ];
    }
}
