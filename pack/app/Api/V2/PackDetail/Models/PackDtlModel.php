<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\PackDetail\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackDtl;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class PackDtlModel
 *
 * @package App\Api\V1\Models
 */
class PackDtlModel extends AbstractModel
{
    /**
     * PackDtlModel constructor.
     *
     * @param PackDtl|null $model
     */
    public function __construct(PackDtl $model = null)
    {
        $this->model = ($model) ?: new PackDtl();
    }

    /**
     * @param $odrDtlIds
     *
     * @return mixed
     */
    public function deleteDtls($odrDtlIds)
    {
        return $this->model
            ->whereIn('odr_dtl_id', $odrDtlIds)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->where('ord_hdr_id', $attributes['ord_hdr_id']);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrLike = ['color', 'sku', 'size', 'lot'];
        $arrEqual = [
            'ord_hdr_id',
            'whs_id',
            'cus_id',
            'item_id',
            'uom_id',
            'request_qty',
            'qty_to_allocated',
            'allocated_qty',
            'damaged',
            'special_handling',
            'back_ord'
        ];
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }
        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $orderHdrId
     *
     * @return mixed
     */
    public function loadPieceQty($orderHdrId)
    {
        $query = $this->model
            ->select(['item_id', DB::raw('sum(piece_qty) as piece_qty_ttl')])
            ->where('odr_hdr_id', $orderHdrId)
            ->groupBy('item_id');

        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }

    /**
     * @param $ord_ids
     *
     * @return mixed
     */
    public function checkWhereIn($odr_ids)
    {
        return $this->model
            ->whereIn('odr_hdr_id', $odr_ids)
            ->count();
    }

    /**
     * @param $ord_ids
     *
     * @return mixed
     */
    public function getDistinctPackHdrIDByOrderIds()
    {
        $rows = $this->model
            ->distinct('pack_hdr_id')
            ->get(['pack_hdr_id']);

        return $rows;
    }

    /**
     * @param $lot
     * @param $itemIds
     * @param $odrHdrId
     *
     * @return integer
     */
    public function sumPieceQty($lot, $itemId, $odrHdrId)
    {
        $result = DB::table('pack_dtl')
            ->select(DB::raw('SUM(piece_qty) as piece_qty'))
            ->where('item_id', $itemId)
            ->where('lot', $lot)
            ->where('odr_hdr_id', $odrHdrId)
            ->where('deleted', 0)
            ->first()
        ;

        return $result['piece_qty'];
    }
}
