<?php

namespace App\Api\V1\Controllers;

use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Item;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ItemPackingController extends AbstractController
{
    public function create(Request $request)
    {
        $input = $request->getParsedBody();

        if (!isset($input['child_items'])) {
            $input['child_items'] = [];
        }

        $parentItem = $input['parent_item'];
        $parentItemLevel = $parentItem['level_id'];

        $checkSum = '';

        try {
            DB::beginTransaction();
            $type = isset($input['child_items'][1]) ? 'MIX' : 'SGL';

            //delete children items
            DB::table('item_child')->where('parent', $parentItem['item_id'])->delete();

            if (!empty($input['child_items'])) {
                $input['child_items'] = array_values(collect($input['child_items'])->sortBy('item_id')->toArray());
            }

            foreach ($input['child_items'] as $itemArr) {
                $pack = $itemArr['pack'] != 1 ? '_' . $itemArr['pack'] : ''; //avoid impact with pack create new sku

                $childLevel = DB::table('system_uom')->select('level_id')->where('sys_uom_id', $itemArr['uom_id'])->first();

                // check parentItemLevel must be greater than childItemLevel
                if ($parentItemLevel <= $childLevel['level_id']) {
                    return $this->response->errorBadRequest('Level of parent Item must be greater than level of child Item');
                }

                //create or get item
                $itemObj = $this->upsertItem([
                    'item_id'  => $itemArr['item_id'],
                    'sku'      => $itemArr['sku'],
                    'size'     => $itemArr['size'] ?? 'NA',
                    'color'    => $itemArr['color'] ?? 'NA',
                    'cus_id'   => $itemArr['cus_id'],
                    'pack'     => $itemArr['inner_pack'],
                    'level_id' => $childLevel['level_id']
                ]);

                //create item children
                DB::table('item_child')->insert([
                    'parent' => $parentItem['item_id'],
                    'child'  => $itemObj->item_id,
                    'type'   => $type,
                    'origin' => $itemArr['item_id'],
                    'pack'   => $itemArr['pack']

                ]);

                $checkSum .=
                    $itemObj['sku'] . "_" .
                    $itemObj['size'] . "_" .
                    $itemObj['color'] . "_" .
                    $itemObj['cus_id'] . "_" .
                    $itemObj['pack'] .
                    $pack . "&";
            }

            if ($checkSum != '') {
                $checkSum = md5(strtoupper($checkSum));
                $chkParentItem = Item::where('children_checksum', $checkSum)
                    ->where('item_id', '!=', $parentItem['item_id'])
                    ->first();

                if ($chkParentItem) {
                    throw new HttpException(400, "This item pack is existed in item: " . $chkParentItem->item_id);
                }

                //update checksum to parent item
                Item::where('item_id', $parentItem['item_id'])->update([
                    'children_checksum' => $checkSum
                ]);
            }

            DB::commit();

            return ['data' => 'Successful'];
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }


    }

    protected function upsertItem($data)
    {
        $newSku = Item::where([
            'sku'    => $data['sku'],
            'size'   => $data['size'],
            'color'  => $data['color'],
            'cus_id' => $data['cus_id'],
            'pack'   => $data['pack']
        ])->first();

        if ($newSku) {
            return $newSku;
        }
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $item = Item::where('item_id', $data['item_id'])->first();
        $newSku = $item->replicate();
        $newSku->pack = $data['pack'];
        $newSku->is_new_sku = 1;
        $newSku->cus_upc = null;
        $newSku->level_id = $data['level_id'];
        $newSku->save();

        return $newSku;
    }

    public function show($itemId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $data = Item::select([
            'item.item_id',
            'item.cus_id',
            'item.sku',
            'item.size',
            'item.color',
            'item.uom_id',
            'item.pack AS inner_pack',
            'item_child.pack'
        ])
            ->join('item_child', 'item.item_id', '=', 'item_child.child')
            ->where('item_child.parent', $itemId)
            ->get();

        return ['data' => $data];
    }

    public function isExistNewSku(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $itemDataInputs = collect($input['items'])->sortBy('itm_id')->toArray();
        $pieceTotal = array_sum(array_column($input['items'], 'piece_qty'));

        // them same item but difference lot
        $itemidsCheckUnique = array_unique(array_pluck($itemDataInputs, 'itm_id'));
        $itemDataFirst = array_first($itemDataInputs);

        $itemChildChecksum = "";
        foreach ($itemDataInputs as $item) {
            if (empty($item['piece_qty'])) {
                continue;
            }

            $itemChildChecksum .= $itemChildChecksum . '_'
                . $item['sku'] . '_'
                . $item['size'] . '_'
                . $item['color'] . '_'
                . $input['cus_id'] . '_'
                . $item['piece_qty'] . "&";
        }

        if (count($itemidsCheckUnique) == 1) {
            // re checksum item
            $itemChildChecksum = '' . '_'
                . $itemDataFirst['sku'] . '_'
                . $itemDataFirst['size'] . '_'
                . $itemDataFirst['color'] . '_'
                . $input['cus_id'] . '_'
                . $pieceTotal . "&";

        }

        \DB::setFetchMode(\PDO::FETCH_ASSOC);

        $data = ['data' => null];
        // check exist new sku by checksum
        $checkItem = $this->itemModel->getFirstWhere(['children_checksum' => md5($itemChildChecksum)]);
        if ($checkItem) {
            $data = ['data' => $checkItem];
        }

        return $this->response->noContent()->setContent($data)->setStatusCode
        (Response::HTTP_CREATED);
    }

}
