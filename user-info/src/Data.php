<?php

namespace Wms2\UserInfo;

use \Predis\Client;
use \Firebase\JWT\JWT;
use Wms2\UserInfo\HttpService\UserService;
use function GuzzleHttp\json_decode;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Seldat\Wms2\Utils\Profiler;

class Data
{
    protected $_redis;
    protected $_cacheKey;
    protected $_data;
    private static $__instance;
    static $check=null;

    public static function getInstance()
    {
        if (self::$__instance === null) {
            self::$__instance = new Data();
        }

        return self::$__instance;
    }

    function __construct()
    {
        $config = config('database.redis.default');
        $this->_cacheKey = config('database.redis.cache-key') ?? 'awms2_dev:cache.user.';
        $this->_redis = new Client($config);
        Profiler::log('new Client: '.json_encode(['_cacheKey'=>$this->_cacheKey,'config'=>$config]));
        $this->__getUserInfo();
    }

    private function __getUserInfo()
    {
        if(self::$check!==null)
            return $this->_data = self::$check;
        $key = env("JWT_SECRET"); //"CGunzjKYdoErvLvz83pk0ucNTnRL9sC5";
        $auth = app('request')->header('Authorization');
        $jwt = str_replace("Bearer ", "", $auth);
        $decoded = JWT::decode($jwt, $key, ['HS256']);
        $userId = $decoded->jti;
        Profiler::log('JWT decode: ' . json_encode(['userId' => $userId]));

        $rs = $this->_redis->get($this->_cacheKey . $userId);

        if (empty($rs)) {
            //call api
            try {
                $http = new UserService($auth);
                $resp = $http->getUserCache();
                if ($resp->getStatusCode() !== 200) {
                    $rep = json_decode($resp->getBody()->getContents(), true);
                    self::$check = false;
                    throw new \Exception($rep['message']);
                }
                Profiler::log('__getUserInfo end by UserService getUserCache: OK');
                return $this->_data = self::$check = json_decode($resp->getBody()->getContents(), true)['data'];
            }
            catch (\Exception $e)
            {
                Profiler::log('__getUserInfo end by UserService getUserCache: ERROR',[Profiler::KEY_TRACE=>$e]);
                throw new HttpException(400, $e->getMessage());
            }
        }

        Profiler::log('__getUserInfo end by redis');
        return $this->_data = self::$check = unserialize($rs)[0]; //for array alway get value[0]
    }

    public function getUserInfo()
    {
        return $this->_data;
    }

    public function getCustomersByWhs($whsId=false)
    {
        if (!$whsId) {
            $whsId = $this->getCurrentWhs();
        }

        if (empty($this->_data['user_customers'])) {
            return false;
        }

        $rs = [];
        foreach ($this->_data['user_customers'] as $userCus) {
            if ($userCus['whs_id'] == $whsId) {
                $rs[] = (int)$userCus['cus_id'];
            }
        }

        return $rs;
    }

    public function getCurrentWhs()
    {
        return $this->_data['current_whs'];
    }


    //------------------static function-------------------------------

    public static function getCurrentWhsId()
    {
        $userInfo = self::getInstance()->getUserInfo();

        return $userInfo['current_whs'];
    }

    public static function getCurrentUserId()
    {
        $userInfo = self::getInstance()->getUserInfo();

        return $userInfo['user_id'];
    }

    public static function getCurrentCusIds()
    {
        $userInfo = self::getInstance();

        return (array) $userInfo->getCustomersByWhs();
    }

    final public static function isAccessWhsAndCus($cusId, $whsId){
        $cusIds = self::getCurrentCusIds();
        if( $whsId == self::getCurrentWhsId() && in_array($cusId, $cusIds)){
            return true;
        }
        return false;
    }

}