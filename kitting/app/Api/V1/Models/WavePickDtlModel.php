<?php

namespace App\Api\V1\Models;

use App\WavepickDtl;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\Status;

class WavePickDtlModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickDtl($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function loadData($wv_id)
    {
        return $this->model->select([
            'item_id as ITEM_ID',
            'sku as SKU',
            'color as Color',
            'size as SIZE',
            'lot as Lot',
            'pack_size as Pack_size',
            'ctn_qty as Cartons',
            'piece_qty as Picking_QTY',
            'primary_loc_id',
            'primary_loc',
            'bu_loc_1',
            'bu_loc_2',
            'bu_loc_3',
            'bu_loc_4',
            'bu_loc_5',
            'bu_loc_1_id',
            'bu_loc_2_id',
            'bu_loc_3_id',
            'bu_loc_4_id',
            'bu_loc_5_id',
            'cus_id',
            'wv_dtl_id'
        ])
            ->where('wv_id', '=', $wv_id)
            ->where('piece_qty', '>', 0)
            ->distinct()
            ->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     * @deprecated DONT USE THIS FUNCTION
     */
    public function updateWvDtl($data)
    {
        return $this->model
            ->where('item_id', '=', $data['item_id'])
            ->where('wv_id', '=', $data['wv_id'])
            ->update([
                'act_piece_qty' => $data['act_piece_qty'],
                'wv_dtl_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }


    public function updateWaveDtl($wvDtlID, $actQty)
    {
        $res =  $this->model
            ->where('wv_dtl_id', '=', $wvDtlID)
            ->update([
                'act_piece_qty' => $actQty,
                'wv_dtl_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);

        return $res;
    }
    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function loadWvDetail($wvId)
    {
        return $this->model
            ->select([
                'wv_dtl.*',
                DB::raw("CONCAT(users.first_name, ' ', users.last_name) AS picker_name"),
            ])
            ->leftJoin('users', 'users.user_id', '=', 'wv_dtl.picker_id')
            ->where('wv_id', $wvId)
            ->get();
    }

    /**
     * @param $wv_dtl_ids
     *
     * @return mixed
     */
    public function getWvDtlById($wv_dtl_ids)
    {
        $wv_dtl_ids = is_array($wv_dtl_ids) ? $wv_dtl_ids : [$wv_dtl_ids];

        return $this->model
            ->whereIn('wv_dtl_id', $wv_dtl_ids)
            ->get()
            ;
    }

    /**
     * @param $wvIds
     *
     * @return mixed
     */
    public function getActualQtyByWvIds($wvIds)
    {
        $wvIds = is_array($wvIds) ? $wvIds : [$wvIds];
        if (empty($wvIds)) {
            return [];
        }

        return $this->model
            ->whereIn('wv_id', $wvIds)
            ->get();
    }
}
