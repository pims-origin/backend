<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V3\Outbound\Models;


use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class LocationModel extends AbstractModel
{
    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    public function getLocationByKey($whsId, $key, $locCode)
    {
        $query = $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where($key, $locCode)
          //  ->where('location.loc_sts_code', 'AC')
            ->where('loc_type.loc_type_code', 'SHP')
            ->where('location.loc_whs_id', $whsId)
        ;

        return $query->first();
    }

    public function getLocationByKeyNoType($whsId, $key, $locCode)
    {
        $query = $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where($key, $locCode)
          //  ->where('location.loc_sts_code', 'AC')
            ->where('location.loc_whs_id', $whsId)
        ;

        return $query->first();
    }

}
