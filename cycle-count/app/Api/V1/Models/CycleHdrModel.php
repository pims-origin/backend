<?php

namespace App\Api\V1\Models;

use App\Api\V1\Traits\CycleNotificationModelTrait;
use App\Api\V1\Validators\CycleDtlValidator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Paginator;
use Mockery\CountValidator\Exception;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\CustomerWarehouse;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Models\Warehouse;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

class CycleHdrModel extends AbstractModel
{
    use CycleNotificationModelTrait;

    const COUNT_BY_EACH = 'EACH';
    const COUNT_BY_CARTON = 'CARTON';
    const STATUS_INSERT = 'I';
    const STATUS_UPDATE = 'U';
    const STATUS_DELETE = 'D';
    const CYCLE_TYPE_CUSTOMER = 'CS';
    const CYCLE_TYPE_SKU = 'SK';
    const CYCLE_TYPE_LOCATION = 'LC';
    const CYCLE_TYPE_CCNOTIFICATION = 'CCN';
    const CYCLE_TYPE_IMPORT = 'IMP';
    const NA_VALUE = 'NA';

    const STATUS_CYCLE_OPEN = 'OP';
    const STATUS_CYCLE_ASSIGNED = 'AS';
    const STATUS_CYCLE_CYCLED = 'CC';
    const STATUS_CYCLE_RECOUNT = 'RC';
    const STATUS_CYCLE_COMPLETED = 'CP';
    const STATUS_CYCLE_DELETED = 'DL';

    const CYCLE_METHOD_RFGUN = 'rfgun';
    const CYCLE_METHOD_PAPER = 'paper';

    public static $arrCycleType = [
        self::CYCLE_TYPE_CUSTOMER       => 'Customer',
        self::CYCLE_TYPE_SKU            => 'SKU',
        self::CYCLE_TYPE_LOCATION       => 'Location',
        self::CYCLE_TYPE_CCNOTIFICATION => 'CC Notification',
        self::CYCLE_TYPE_IMPORT         => 'CC Import',
    ];

    public static $arrCycleStatus = [
        self::STATUS_CYCLE_OPEN      => 'Open',
        self::STATUS_CYCLE_ASSIGNED  => 'Assigned',
        self::STATUS_CYCLE_CYCLED    => 'Cycled',
        self::STATUS_CYCLE_RECOUNT   => 'Recount',
        self::STATUS_CYCLE_COMPLETED => 'Completed',
        self::STATUS_CYCLE_DELETED   => 'Deleted',
    ];

    public static $arrCycleMethod = [
        self::CYCLE_METHOD_RFGUN => 'rfgun',
        self::CYCLE_METHOD_PAPER => 'paper',
    ];

    private $arrNumberFields = [
        'cycle_hdr_id',
        'whs_id',
        'assignee_id',
        'created_by',
        'updated_by'
    ];

    private $arrStringFields = [
        'cycle_name',
        'count_by',
        'cycle_method',
        'cycle_type',
        'cycle_detail',
        'description',
        'due_dt',
        'cycle_sts',
        'sts'
    ];

    public $cycleType = ['CS', 'SK', 'LC', 'CCN'];

    public $errors = [];
    public $waring = [];
    public $cycleDtlData = [];
    private $locIdsRange = [];

    /**
     * CycleHdrModel constructor.
     *
     * @param CycleHdr|NULL $model
     */
    public function __construct(CycleHdr $model = null)
    {
        $this->model = $model ? $model : new CycleHdr();
    }

    /**
     * @param $cycleId
     *
     * @return bool
     */
    public function deleteCycleHdr($cycleId)
    {
        if (!$cycleId) {
            return false;
        }

        return $this->model
            ->where('cycle_hdr_id', $cycleId)
            ->delete();
    }

    /**
     * Search Cycle
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return object|null $models
     */
    public function search($attributes = [], $with = [], $limit = null, $userId = null)
    {
        // $query = $this->make($with);
        $query = $this->model->select('cycle_hdr.*',
        'users.first_name',
        'users.last_name',
        'warehouse.whs_name'
        )
        ->leftJoin('users', 'users.user_id', '=', 'cycle_hdr.assignee_id')
        ->leftJoin('warehouse', 'warehouse.whs_id', '=', 'cycle_hdr.whs_id');
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes)) {

            foreach ($attributes as $key => $value) {
                if (in_array($key, $this->arrNumberFields)) {
                    $query->where('cycle_hdr.'.$key, $value);
                } elseif (in_array($key, $this->arrStringFields)) {
                    $query->where('cycle_hdr.'.$key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['dashboard'])) {
            $query->whereNotIn("cycle_hdr.cycle_sts",["CP","CE","DL"]);
        }

        if (isset($attributes['sku'])) {
            $query->whereHas('cycleDtl', function ($query) use ($attributes) {
                $query->where('cycle_dtl.sku', $attributes['sku']);
            });
        }
        if (isset($attributes['size'])) {
            $query->whereHas('cycleDtl', function ($query) use ($attributes) {
                $query->where('cycle_dtl.size', $attributes['size']);
            });
        }
        if (isset($attributes['color'])) {
            $query->whereHas('cycleDtl', function ($query) use ($attributes) {
                $query->where('cycle_dtl.color', $attributes['color']);
            });
        }
        if (isset($attributes['lot'])) {
            $query->whereHas('cycleDtl', function ($query) use ($attributes) {
                $query->where('cycle_dtl.lot', $attributes['lot']);
            });
        }

        $this->model->filterDataIn($query, false, false, false);
        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $cycleIds
     *
     * @return bool
     */
    public function deleteCycleHdrs($cycleIds)
    {
        if (!($cycleIds && is_array($cycleIds))) {
            return false;
        }

        foreach (array_chunk($cycleIds, 200) as $chunkCycleIds) {
            $this->model
                ->whereIn('cycle_hdr_id', $chunkCycleIds)
                ->delete();
        }
        return true;
    }

    /**
     * @param $input
     *
     * @return bool
     */
    public function processCreateCycleHdr($input)
    {
        $cycleHdr = new CycleHdr();
        $dueDt = $input['cycle_due_date'];

        $validate = $this->validateCreateCycleHdr($input);
        if (!$validate) {
            return false;
        }

        $itemIds = array_column($this->cycleDtlData, 'item_id');
        $locIds = array_column($this->cycleDtlData, 'sys_loc_id');
        $cycleDetail = $input['cycle_type'] == self::CYCLE_TYPE_LOCATION ? $this->locIdsRange : $input['cycle_detail'];

        try {
            DB::beginTransaction();
            $model = $cycleHdr->create([
                'cycle_name'     => $input['loc_code'] ? $input['cycle_name'] . '-' . $input['loc_code'] : $input['cycle_name'],
                'cycle_num'      => $input['cycle_num'],
                'whs_id'         => $input['whs_id'],
                'count_by'       => $input['cycle_count_by'],
                'cycle_method'   => $input['cycle_method'],
                'cycle_type'     => $input['cycle_type'],
                'cycle_detail'   => implode(',', $cycleDetail),
                'description'    => $input['cycle_des'],
                'assignee_id'    => $input['cycle_assign_to'],
                'due_dt'         => date('Y-m-d', strtotime($dueDt)),
                'has_color_size' => $input['cycle_has_color_size'],
                'cycle_sts'      => self::STATUS_CYCLE_ASSIGNED,
                'sts'            => self::STATUS_INSERT,
                'plt_rfid'       => array_get($input, 'plt_rfid', '')
            ]);
            $this->createCycleDtls($model, $this->cycleDtlData);

            $this->processLockWithCycleHdr($model, $itemIds, $locIds);

            $locIds = [];
            if (isset($input['loc_id']) && !empty($input['loc_id'])) {
                $locIds[] = $input['loc_id'];
                $this->processLockWithEmptyLocation($input['whs_id'], $locIds);
            }

            CycleHdrModel::updateStatusLocationPutAwayHistory();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

        return $model;
    }

    /**
     * @param $cycleHdr
     * @param $cycleDtls
     */
    public function processLockWithCycleHdr($cycleHdr, $itemIds, $locIds)
    {
        if (!($cycleHdr && $itemIds && $locIds)) {
            return false;
        }

//        switch ($cycleHdr->cycle_type) {
//            case self::CYCLE_TYPE_CUSTOMER:
//                //lock carton
//                $locIds = LocationModel::getLocationIdsByCustomer($cycleHdr->whs_id, $cycleHdr->cycle_detail);
//
//                break;
//            case self::CYCLE_TYPE_LOCATION:
//
//                $locIds = explode(',', $cycleHdr->cycle_detail);
//                break;
//            case self::CYCLE_TYPE_SKU:
//
//                break;
//        }

        if ($cycleHdr->cycle_type == self::CYCLE_TYPE_LOCATION) {
            $locIds = explode(',', $cycleHdr->cycle_detail);
        }
        LockModel::lockCartonsByCycleHdr($cycleHdr->whs_id, $itemIds, $locIds);
        LockModel::lockLocationByIds($cycleHdr->whs_id, $locIds);
        LockModel::updateLockQtyInInventorySummary($cycleHdr->whs_id, $itemIds, $locIds);
    }

    public function processLockWithEmptyLocation($whsId, $locIds)
    {
        LockModel::lockLocationByIds($whsId, $locIds);
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function validateCreateCycleHdr(&$data)
    {
        $validInput = $this->validateInputCreateCycleHdr($data);

        if (!$validInput) {
            return $validInput;
        }

        $itemModel = new ItemModel();
        $data['cycle_detail'];
        $cycleDetail = $data['cycle_type'] == self::CYCLE_TYPE_LOCATION ?
            $this->locIdsRange : $data['cycle_detail'];
        if ($data['cycle_type'] === 'SK') {
            $cycleDetail = $data['items'];
        }

        //fix bug SWIS-1001
        if ($data['cycle_type'] === 'SK' && empty(array_filter($data['items']))) {
            throw new HttpException(404, 'Please select one SKU with Size, Color and Batch');
        }

        // When cycle count by sku, remove restriction of only one cycle count per location
        // $loccationLockOnCycle = LocationModel::getLocationLockByCycleType(
        //     $data['whs_id'],
        //     $data['cycle_type'],
        //     $cycleDetail
        // );
        //         
        // if ($loccationLockOnCycle) {
        //     $this->errors['sku'] = 'This Cycle count can not create. All Locations locked';
        //                 
        //     return false;
        // }

        $cycleDtls = $this->getCycleData(
            $data['whs_id'],
            $data['cycle_type'],
            $cycleDetail
        );

        // if (!$cycleDtls) {
        //     $this->errors['cus_id'] = $this->getErrorMsgWithNoInventory($data['cycle_type'], $cycleDetail);
        //     
        //     return false;
        // }

        $itemIds = array_unique(array_column($cycleDtls, 'item_id'));
        if ($data['cycle_type'] == self::CYCLE_TYPE_SKU) {
            $itemIds = $cycleDetail;
        }

        // Remove restriction of only one cycle count per location
        $itemsLock = $itemModel->getItemLocked($data['whs_id'], $itemIds, true);

        if ($itemsLock) {
            $this->errors[] = $itemModel->getErrorMsgWithSKUExist($itemsLock);

            return false;
        }

        if (!$data['cycle_has_color_size']) {
            $hasColorSize = $this->checkHasColorSize($cycleDtls);
            if ($hasColorSize) {
                $this->errors['sku'] = Message::get('BM071', implode(',', array_keys($hasColorSize)));

                return false;
            }
        }

        $this->cycleDtlData = $cycleDtls;

        return true;
    }

    /**
     * @param $cycleType
     * @param $cycleDetail
     *
     * @return string
     */
    private function getErrorMsgWithNoInventory($cycleType, $cycleDetail)
    {
        $tmpError = 'This customer';

        switch ($cycleType) {
            case self::CYCLE_TYPE_LOCATION:
                $tmpError = count($cycleDetail) > 1 ? 'Locations are' : 'Location is';
                //$result = sprintf($result, $tmpError);
                break;
            case self::CYCLE_TYPE_SKU:
                $tmpError = count($cycleDetail) > 1 ? 'SKUs are' : 'SKU is';
                //$result = sprintf($result, $tmpError);
                break;
            case  self::CYCLE_TYPE_CUSTOMER:
                //$result = sprintf($result, $tmpError);
                break;
        }
        $result = Message::get('BM087', $tmpError);

        return $result;
    }

    /**
     * @param $data
     */
    public function validateInputCreateCycleHdr(&$data)
    {
        if (!$data) {
            return false;
        }

        if (!$data['whs_id']) {
            $this->errors['whs_id'] = Message::get('VR029', 'Warehouse');

            return false;
        }

        if (!$wareHouseCurrent = $this->checkExitsWarehouse($data['whs_id'])) {
            $this->errors['whs_id'] = Message::get('BM067', 'Warehouse');

            return false;
        }
        $data['loc_code'] = null;
        if ($data['cycle_type'] === 'LC') {
            $loc_ids = explode(',', array_get($data, 'cycle_detail'));
            if (count($loc_ids) > 1) {
                $this->errors['loc_id'] = 'Please choose 1 location';
            } else {
                $data['loc_id'] = array_get($data, 'cycle_detail');
                $loc_info = DB::table('location')
                    ->join('cartons', function ($join) {
                        $join->on('cartons.loc_id', '=', 'location.loc_id');
                        $join->on('cartons.whs_id', '=', 'location.loc_whs_id');
                    })
                    ->where('location.loc_id', '=', $data['loc_id'])
                    ->where('location.loc_whs_id', '=', $data['whs_id'])
                    ->first();

                if (!$loc_info) {
                    $loc_info = DB::table('location')
                        ->where('location.loc_id', '=', $data['loc_id'])
                        ->where('location.loc_whs_id', '=', $data['whs_id'])
                        ->first();
                    $data['loc_code'] = $loc_info['loc_code'];
                    $data['loc_id'] = $loc_info['loc_id'];
                }
            }
        }

        if (!$data['cycle_detail']) {
            $this->errors['cycle_detail'] = Message::get('VR029', 'Cycle detail');
        } else {
            if ($data['cycle_type'] === 'SK') {
                $data['cycle_detail'] = [$data['cycle_detail']];
            } else {
                $data['cycle_detail'] = array_map('trim', explode(',', $data['cycle_detail']));
            }
        }

        if (!isset($data['cycle_has_color_size'])) {
            $this->errors['cycle_has_color_size'] = Message::get('BM088', 'has_color_size');
        } else {
            $data['cycle_has_color_size'] = $data['cycle_has_color_size'] == 'true' ? 1 : 0;
        }

        if (!isset($data['cycle_count_by'])) {
            $this->errors['cycle_count_by'] = Message::get('BM088', 'count_by');
        } else {
            $data['cycle_count_by'] = strtoupper($data['cycle_count_by']);
        }

        if (!$data['cycle_type']) {
            $this->errors['cycle_type'] = Message::get('VR029', 'Cycle type');
        } else if (!in_array(strtoupper($data['cycle_type']), $this->cycleType)) {
            $this->errors['cycle_type'] = Message::get('BM081', 'Cycle type');
        } else {
            $data['cycle_type'] = strtoupper($data['cycle_type']);
            //validate by cycle type
            $this->validateDataByCycleType($data);
        }

        if ($data['cycle_due_date']) {
            $data['cycle_due_date'] = $this->reFormatDueDate($data['cycle_due_date']);
        }

        return $this->errors ? false : true;
    }

    /**
     * @param $string
     */
    private function reFormatDueDate($string)
    {
        $result = null;

        if (!$string) {

            return $result;
        }

        $arr = explode('/', $string);

        if (count($arr) != 3) {
            return $result;
        }

        $result = $arr[2] . '-' . $arr[0] . '-' . $arr[1];

        return $result;
    }

    /**
     * @param $cycleType
     * @param $cycleDetail
     */
    public function validateDataByCycleType($data)
    {
        $result = false;

        switch (strtoupper($data['cycle_type'])) {
            case self::CYCLE_TYPE_CUSTOMER:
                $result = $this->checkValidCusInput($data);
                break;
            case self::CYCLE_TYPE_SKU:
                $result = $this->checkValidSKUInput($data);
                break;
            case self::CYCLE_TYPE_LOCATION:
                $result = $this->checkValidLocIdsInput($data);
                break;
        }

        return $result;
    }

    /**
     * @param      $whsId
     * @param      $cycleType
     * @param      $cycleDetail
     * @param bool $checkHasData
     */
    public function getCycleData($whsId, $cycleType, $cycleDetail, $checkHasData = false)
    {
        $status = LockModel::STATUS_ACTIVE;
        $statusIA = LockModel::STATUS_INACTIVE;
        $statusLK = LockModel::STATUS_LOCK;
        $field = $this->getFieldCycleByType($cycleType);
        $locTypeRacId = LockModel::getLocTypeId();

        $obj = Carton::select(
            'cartons.whs_id',
            'cartons.cus_id',
            'cartons.item_id',
            'cartons.sku',
            'cartons.size',
            'cartons.color',
            'cartons.lot',
            'cartons.ctn_pack_size AS pack_size',
            'cartons.piece_remain AS piece_remain',
            DB::raw('SUM(cartons.piece_remain) AS sys_pieces_qty'),
            'cartons.ctn_uom_id',
            DB::raw('COUNT(cartons.ctn_id) AS sys_carton_qty'),
            'cartons.loc_id AS sys_loc_id',
            'cartons.loc_code AS sys_loc_name',
            'cartons.lpn_carton AS plt_rfid'
        )
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
            //->join('item', 'item.item_id', '=', 'cartons.item_id')
            //->where('item.status', $status)
            // ->whereNotIn('location.loc_sts_code', [$statusIA, $statusLK])
            ->whereNotIn('location.loc_sts_code', [$statusIA])
            ->whereIn('cartons.ctn_sts', [$status, $statusLK])
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.piece_remain', '>', 0)
            ->where('location.deleted', 0)
            ->groupBy(
                'cartons.whs_id',
                'cartons.cus_id',
                'cartons.item_id',
                'cartons.lpn_carton',
                'cartons.piece_remain',
                //'cartons.ctn_pack_size',
                'cartons.loc_id',
                'cartons.lot'
            );


        if ($cycleType == 'SK') {
            $ItemsLot = [];
            foreach ($cycleDetail as $item) {
                $ItemsLot[$item['item_id']][] = $item['lot'];
            }
            $sqls = [];
            foreach ($ItemsLot as $itemId => $lots) {
                $sqlLot = implode("','", $lots);
                if ($sqlLot === 'ALL') {
                    $sqls[] = sprintf("(cartons.item_id = %d)", $itemId);
                } else {
                    $sqls[] = sprintf("(cartons.item_id = %d  AND cartons.lot IN ('%s'))", $itemId, $sqlLot);
                }
            }
            $sql = sprintf('(%s)', implode(' OR ', $sqls));

            $obj->whereRaw($sql);

        } else {
            $obj->whereIn($field, $cycleDetail);
        }

        $data = $checkHasData ? $obj->count() : $obj->get()->toArray();

        return $data;
    }

    public function getCycleDataV2($whsId, $cycleType, $cycleDetail, $checkHasData = false)
    {
        $status = LockModel::STATUS_ACTIVE;
        $statusIA = LockModel::STATUS_INACTIVE;
        $statusLK = LockModel::STATUS_LOCK;
        $field = $this->getFieldCycleByType($cycleType);
        $locTypeRacId = LockModel::getLocTypeId();

        $obj = Carton::select(
            'cartons.whs_id',
            'cartons.cus_id',
            'cartons.item_id',
            'cartons.sku',
            'cartons.size',
            'cartons.color',
            'cartons.lot',
            'cartons.ctn_pack_size AS pack_size',
            'cartons.piece_remain AS piece_remain',
            DB::raw('SUM(cartons.piece_remain) AS sys_pieces_qty'),
            'cartons.ctn_uom_id',
            DB::raw('COUNT(cartons.ctn_id) AS sys_carton_qty'),
            'cartons.loc_id AS sys_loc_id',
            'cartons.loc_code AS sys_loc_name',
            'cartons.lpn_carton AS plt_rfid'
        )
        ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
        ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
        //->join('item', 'item.item_id', '=', 'cartons.item_id')
        //->where('item.status', $status)
        // ->whereNotIn('location.loc_sts_code', [$statusIA, $statusLK])
        ->whereNotIn('location.loc_sts_code', [$statusIA])
        ->whereIn('cartons.ctn_sts', [$status, $statusLK])
        ->where('location.loc_type_id', $locTypeRacId)
        ->where('cartons.whs_id', $whsId)
        ->where('cartons.piece_remain', '>', 0)
        ->where('location.deleted', 0)
        ->groupBy(
            'cartons.whs_id',
            'cartons.cus_id',
            'cartons.item_id',
            'cartons.lpn_carton',
            'cartons.piece_remain',
            //'cartons.ctn_pack_size',
            'cartons.loc_id',
            'cartons.lot'
        );


        if ($cycleType == 'SK') {
            $ItemsLot = [];
            foreach ($cycleDetail as $item) {
                $ItemsLot[$item['item_id']][] = $item['lot'];
            }
            $sqls = [];
            foreach ($ItemsLot as $itemId => $lots) {
                $sqlLot = implode("','", $lots);
                if ($sqlLot === 'ALL') {
                    $sqls[] = sprintf("(cartons.item_id = %d)", $itemId);
                } else {
                    $sqls[] = sprintf("(cartons.item_id = %d  AND cartons.lot IN ('%s'))", $itemId, $sqlLot);
                }
            }
            $sql = sprintf('(%s)', implode(' OR ', $sqls));
            $obj->whereRaw($sql);
            $data = $obj->get()->toArray();
            $returnItemIds=[];
            foreach ($data as $value){
                $returnItemIds[]=$value['item_id'];
            }
            foreach ($cycleDetail as $item) {
                if(!in_array($item['item_id'],$returnItemIds))
                {
                    $itemInfo=DB::table('item')->where('item_id',$item['item_id'])->where('deleted',0)->first();
                    if(empty($itemInfo))
                        continue;
                    $data[]=[
                        'whs_id'=>$whsId,
                        'cus_id'=>$itemInfo['cus_id'],
                        'item_id'=>$itemInfo['item_id'],
                        'sku'=>$itemInfo['sku'],
                        'size'=>$itemInfo['size'],
                        'color'=>$itemInfo['color'],
                        'lot'=>$item['lot'],
                        'pack_size'=>$itemInfo['pack'],
                        'piece_remain'=>0,
                        'sys_pieces_qty'=>0,
                        'ctn_uom_id'=>$itemInfo['uom_id'],
                        'sys_carton_qty'=>0,
                        'sys_loc_id'=>null,
                        'sys_loc_name'=>null,
                        'plt_rfid'=>null,
                    ];
                }
            }
        } else {
            $obj->whereIn($field, $cycleDetail);
            $data = $obj->get()->toArray();
        }
        return $data;
    }

    public function getCycleDataWhenCreateFromCcn($whsId, $cycleType, $cycleDetail, $checkHasData = false)
    {
        $status = LockModel::STATUS_ACTIVE;
        $field = $this->getFieldCycleByType($cycleType);
        $locTypeRacId = LockModel::getLocTypeId();

        $obj = DB::table('item')
            ->select([
                'cw.whs_id',
                'item.cus_id',
                'item.item_id',
                'item.sku',
                'item.size',
                'item.color',
                'loc_ctn.lot',
                'loc_ctn.ctn_pack_size AS pack_size',
                'loc_ctn.piece_remain AS piece_remain',
                DB::raw('SUM(loc_ctn.piece_remain) AS sys_pieces_qty'),
                'item.uom_id',
                DB::raw('COUNT(loc_ctn.ctn_id) AS sys_carton_qty'),
                'loc_ctn.loc_id AS sys_loc_id',
                'loc_ctn.loc_name AS sys_loc_name'
            ])
            ->leftJoin(DB::raw('
                (SELECT cartons.*, location.loc_type_id, location.loc_sts_code
                FROM cartons
                INNER JOIN location ON cartons.loc_id = location.loc_id) AS loc_ctn
            '), 'item.item_id', '=', 'loc_ctn.item_id')
            ->join('customer', 'customer.cus_id', '=', 'item.cus_id')
            ->join('customer_warehouse as cw', 'item.cus_id', '=', 'cw.cus_id')
            ->where('cw.whs_id', $whsId)
            ->whereRaw(DB::raw('(loc_ctn.loc_sts_code is null or loc_ctn.loc_sts_code IN ("AC","LK"))'))
            ->whereRaw(DB::raw('(loc_ctn.ctn_sts is null or loc_ctn.ctn_sts IN ("AC","LK"))'))
            ->whereRaw(DB::raw(sprintf('(loc_ctn.loc_type_id is null or loc_ctn.loc_type_id = %s)', $locTypeRacId)))
            ->groupBy(
                'cw.whs_id',
                'item.cus_id',
                'item.item_id',
                'loc_ctn.piece_remain',
                'loc_ctn.loc_id',
                'loc_ctn.lot'
            );


        if ($cycleType == 'SK') {
            $ItemsLot = [];
            foreach ($cycleDetail as $item) {
                $ItemsLot[$item['item_id']][] = $item['lot'];
            }
            $sqls = [];
            foreach ($ItemsLot as $itemId => $lots) {
                $sqlLot = implode("','", $lots);
                if ($sqlLot === 'ALL') {
                    $sqls[] = sprintf("(item.item_id = %d)", $itemId);
                } else {
                    $sqls[] = sprintf("(item.item_id = %d  AND ( cartons.lot is null or cartons.lot IN ('%s')))",
                        $itemId, $sqlLot);
                }
            }
            $sql = sprintf('(%s)', implode(' OR ', $sqls));

            $obj->whereRaw($sql);

        } else {
            $cycleDetail = implode(',', $cycleDetail);
            $raw = sprintf("(loc_ctn.loc_id is null or loc_ctn.loc_id IN ('%s'))", $cycleDetail);
            $obj->whereRaw(DB::raw($raw));
        }

        $data = $checkHasData ? $obj->count() : $obj->get();

        return $data;
    }

    /**
     * @param $whsId
     * @param $cycleType
     * @param $cycleDetail
     * @param $ccnIds
     * @param bool $checkHasData
     *
     * @return mixed
     */
    public function getCycleFromInvData($whsId, $cycleType, $cycleDetail, $skus, $checkHasData = false)
    {
        //$cycleDetail are loc_ids
        $status = LockModel::STATUS_ACTIVE;
        $field = $this->getFieldCycleByType($cycleType);
        $locTypeRacId = LockModel::getLocTypeId();

        $obj = Carton::select(
            'cartons.whs_id',
            'cartons.cus_id',

            'invt_smr.item_id',
            'invt_smr.sku',
            'invt_smr.size',
            'invt_smr.color',
            'invt_smr.lot',

            'cartons.ctn_pack_size AS pack_size',
            'cartons.piece_remain AS piece_remain',
            DB::raw('SUM(cartons.piece_remain) AS sys_pieces_qty'),
            'cartons.ctn_uom_id',
            DB::raw('COUNT(cartons.ctn_id) AS sys_carton_qty'),
            'cartons.loc_id AS sys_loc_id',
            'cartons.loc_code AS sys_loc_name'
        )
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('invt_smr', 'invt_smr.item_id', '=', 'cartons.item_id')
            ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
            //->join('item', 'item.item_id', '=', 'cartons.item_id')
            //->where('item.status', $status)
            ->where('location.loc_sts_code', $status)
            ->where('cartons.ctn_sts', $status)
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.piece_remain', '>', 0)
            ->whereIn($field, $cycleDetail)
            ->whereIn('invt_smr.sku', $skus)
            ->groupBy(
                'cartons.whs_id',
                'cartons.cus_id',
                'invt_smr.item_id',
                'cartons.piece_remain',
                //'cartons.ctn_pack_size',
                'cartons.loc_id',
                'invt_smr.lot'
            );

        $data = $checkHasData ? $obj->count() : $obj->get()->toArray();

        return $data;
    }

    public function countSysQty($whsId, $cycleType, $cycleDetail, $checkHasData = false)
    {
        $status = LockModel::STATUS_ACTIVE;
        $field = $this->getFieldCycleByType($cycleType);
        $locTypeRacId = LockModel::getLocTypeId();

        $obj = Carton::select(
            'cartons.whs_id',
            'cartons.cus_id',
            'cartons.item_id',
            'cartons.sku',
            'cartons.size',
            'cartons.color',
            'cartons.lot',
            'cartons.ctn_pack_size AS pack_size',
            'cartons.piece_remain AS piece_remain',
            DB::raw('SUM(cartons.piece_remain) AS sys_pieces_qty'),
            'cartons.ctn_uom_id',
            DB::raw('COUNT(cartons.ctn_id) AS sys_carton_qty'),
            'cartons.loc_id AS sys_loc_id',
            'cartons.loc_code AS sys_loc_name'
        )
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
            //->join('item', 'item.item_id', '=', 'cartons.item_id')
            //->where('item.status', $status)
            ->where('location.loc_sts_code', $status)
            ->where('cartons.ctn_sts', $status)
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.piece_remain', '>', 0)
            ->whereIn($field, $cycleDetail)
            ->groupBy(
                'cartons.whs_id',
                'cartons.cus_id',
                'cartons.item_id',
                'cartons.piece_remain',
                //'cartons.ctn_pack_size',
                'cartons.loc_id',
                'cartons.lot'
            );

        $data = $checkHasData ? $obj->count() : $obj->get()->toArray();

        return $data;
    }

    /**
     * @param $input
     */
    public function prepareDataCycle($input)
    {
        return $this->getCycleData($input['whs_id'], $input['cycle_type'], $input['cycle_detail']);
    }

    /**
     * @param $cycleType
     */
    public function getFieldCycleByType($cycleType)
    {
        $field = '';

        switch ($cycleType) {
            case self::CYCLE_TYPE_CUSTOMER:
                $field = 'cartons.cus_id';
                break;
            case self::CYCLE_TYPE_SKU:
                $field = 'cartons.sku';
                break;
            default:
                $field = 'cartons.loc_id';
                break;
        }

        return $field;
    }

    /**
     * @param $whsId
     */
    public function checkExitsWarehouse($whsId)
    {
        return Warehouse::where('whs_id', $whsId)->first();
    }

    /**
     * @param $cusIds
     */
    public function getCustomerInfo($cusIds, $select = ['cus_id'])
    {
        $cusIds = (array)$cusIds;

        $result = Customer::select($select)
            ->whereIn('cus_id', $cusIds)
            ->get();

        return $result;
    }

    /**
     * @param $cusId
     */
    public function checkExistCustomer($cusId)
    {
        return Customer::where('cus_id', $cusId)->count();
    }

    /**
     * @param $whsId
     * @param $cusId
     */
    public function getWarehouseHasCusIds($whsId, $cusIds)
    {
        $cusIds = (array)$cusIds;

        $result = CustomerWarehouse::where('whs_id', $whsId)
            ->whereIn('cus_id', $cusIds)
            ->get();

        return $result;
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function checkHasColorSize($data)
    {
        $result = [];

        if (!$data) {
            return $result;
        }

        foreach ($data as $item) {
            if ($item['size'] != self::NA_VALUE || $item['color'] == self::NA_VALUE) {
                $result[$item['sku']] = true;
            }
        }

        return $result;
    }

    /**
     * @param $userId
     *
     * @return mixed
     */
    public function checkExistUser($userId)
    {
        return User::where('user_id', $userId)->count();
    }

    /**
     * @param $cusIds
     */
    public function checkValidCusInput($data)
    {
        $cusIds = isset($data['cycle_detail']) ? $data['cycle_detail'] : null;
        $whsId = isset($data['whs_id']) ? $data['whs_id'] : null;
        //get customer by cusId
        $customer = $this->getCustomerInfo($cusIds);
        $cusInvalid = $cusIdValid = [];

        if ($customer->isEmpty()) {
            $this->errors['cus_id'] = Message::get('BM089', 'Customer', implode(',', $cusIds));

            return false;
        }

        $customerIds = $customer->pluck('cus_id')->toArray();
        $this->validationUserPermissionToCustomer($whsId, $customerIds);

        //cusId valid
        $cusIdValid = array_column($customer->toArray(), 'cus_id');
        $cusInvalid = array_diff($cusIds, $cusIdValid);

        // exists cusId invalid
        if ($cusInvalid) {
            $this->errors['cus_id'] = Message::get('BM089', 'Customer', implode(',', $cusInvalid));

            return false;
        }

        $warehouseCus = $this->getWarehouseHasCusIds($whsId, $cusIds);

        if ($warehouseCus->isEmpty()) {
            $this->errors['cus_id'] = Message::get('BM090', implode(',', $cusIds));

            return false;
        }

        $cusIdValid = array_column($warehouseCus->toArray(), 'cus_id');
        $cusInvalid = array_diff($cusIds, $cusIdValid);

        // exists cusId invalid
        if ($cusInvalid) {
            $this->errors['cus_id'] = Message::get('BM090', implode(',', $cusInvalid));

            return false;
        }

        $isCusInWarehouse = $this->checkCusInUser([
            'cus_id'  => $cusIds,
            'user_id' => $data['cycle_assign_to']
        ]);

        if (!$isCusInWarehouse) {
            $this->errors['cus_id'] = Message::get('BM076');

            return false;
        }

        return true;
    }

    /**
     * @param $skus
     * @param $whsId
     */
    private function checkValidSKUInput($data)
    {
        $itemIds = !empty($data['items']) ? array_column($data['items'], 'item_id') : null;
        $skus = isset($data['cycle_detail']) ? $data['cycle_detail'] : null;
        $whsId = isset($data['whs_id']) ? $data['whs_id'] : null;

        if (!($itemIds && $skus && $whsId)) {
            $this->errors['sku'] = Message::get('BM081', 'Data');

            return false;
        }

        $skus = array_unique($skus);

        $data = ItemModel::getItemBySKUs($skus);

        foreach ($data as $key => $val) {
            if (!in_array($val['item_id'], $itemIds)) {
                unset($data[$key]);
            }
        }

        //check sku valid on database
        $skuValids = array_column($data->toArray(), 'sku');
        $skuInValid = array_diff($skus, $skuValids);

        if ($skuInValid) {
            $this->errors['sku'] = Message::get('BM089', 'SKU', implode(',', $skuInValid));

            return false;
        }

        $customerIds = array_unique($data->pluck('cus_id')->toArray());
        $this->validationUserPermissionToCustomer($whsId, $customerIds);

        //get SKU valid in warehouse
        $items = ItemModel::getInfoItemBySkus($skus);
        $skuValids = ItemModel::getSKUInWarehouse($whsId, $items);
        $skuNotInWarehouse = array_diff($skus, array_keys($skuValids));

        if ($skuNotInWarehouse) {
            $this->errors['sku'] = Message::get('BM091', implode(',', $skuNotInWarehouse));

            return false;
        }

        return true;
    }

    /**
     * @param $locIds
     * @param $whsId
     */
    private function checkValidLocIdsInput($data)
    {
        $locIds = isset($data['cycle_detail']) ? $data['cycle_detail'] : null;
        $whsId = isset($data['whs_id']) ? $data['whs_id'] : null;

        if (!($locIds && $whsId)) {
            $this->errors['locId'] = Message::get('BM081', 'Locations');

            return false;
        }

        $locations = LocationModel::getLocationByLocIds($locIds);
        $locationNotInWarehouse = $locations->filter(function ($item) use ($whsId) {
            return $item->loc_whs_id !== (int)$whsId;
        });

        if ($locationNotInWarehouse->count() !== 0) {
            $this->errors['locId'] = 'Don\'t have permission on location code:' . $locationNotInWarehouse->pluck('loc_code')->implode(',') . ' belong to current warehouse';

            return false;
        }

        $customerIds = array_unique($locations->pluck('customerZone.cus_id')->toArray());
        $this->validationUserPermissionToCustomer($whsId, $customerIds);

        $locInValid = array_diff($locIds, $locations->pluck('loc_id')->toArray());
        if ($locInValid) {
            $locationInvalid = LocationModel::getLocationNotActive($locInValid);
            $locationErrors = $locationInvalid->pluck('loc_code')->implode(', ');
            $this->errors['locId'] = Message::get('BM094', $locationErrors);

            return false;
        }

        $this->locIdsRange = $locations->pluck('loc_id')->toArray();

        return true;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function createCycleDtls($cycleHdr, $data, $ccnIds = null)
    {
        $validator = new CycleDtlValidator();
        $cycleDtl = new CycleDtl();
        $ccNotificationModel = new CycleCountNotificationModel();

        // Get data from cc_notification
        $ccNotificationInfo = $ccNotificationModel->getCCNByIds($ccnIds);
        $countBy = strtoupper($cycleHdr->count_by);

        if (!$ccnIds) {
            $ccNotificationInfo = $data;
        }

        foreach ($ccNotificationInfo as $countItem) {
            // Allow empty
            if (array_get($countItem, 'color') === null) {
                throw new HttpException(403, "The color field is required");
            }
            $countItem['cycle_hdr_id'] = $cycleHdr->cycle_hdr_id;
            $countItem['pack'] = $countItem['pack_size'];
            $countItem['remain'] = $countItem['piece_remain'];
            $countItem['sys_qty'] = $countBy == self::COUNT_BY_EACH ? $countItem['sys_pieces_qty'] : $countItem['sys_carton_qty'];
            $countItem['cycle_dtl_sts'] = CycleDtlModel::STATUS_CYCLEDTL_NEW;
            $countItem['sts'] = self::STATUS_INSERT;
            $validator->validate($countItem);
            $cycle_dtl_id = $cycleDtl->create($countItem)->cycle_dtl_id;

            //---- Insert Cycle Count Info for SKU Tracking report
            $cusId = $countItem['cus_id'];
            $cusInfo = DB::Table('customer')
                ->where('customer.cus_id', $cusId)
                ->first();

            $itemId = $countItem['item_id'];
            $itemInfo = DB::Table('item')
                ->where('item.item_id', $itemId)
                ->first();
            $length = array_get($itemInfo, 'length', '');
            $width = array_get($itemInfo, 'width', '');
            $height = array_get($itemInfo, 'height', '');

            $CycDtlInfo = DB::Table('cycle_dtl')
                ->where('cycle_dtl.cycle_dtl_id', $cycle_dtl_id)
                ->first();

            if ($cycleHdr->count_by = 'EACH'){
                $count_by=1;
            }else{
                $count_by = array_get($CycDtlInfo,'remain', 0);
            }
            $qty = round(
                            $count_by *
                                        (
                                            array_get($CycDtlInfo,'act_qty', 0) - array_get($CycDtlInfo,'sys_qty', 0)
                                        )
                         );

            $ctns = round(
                        $count_by *
                             (
                                 array_get($CycDtlInfo,'act_qty', 0) - array_get($CycDtlInfo,'sys_qty', 0)
                             )/array_get($CycDtlInfo,'pack', 0)
                          );

            $cube = round(
                            (
                                (
                                    $count_by *
                                    (
                                        array_get($CycDtlInfo,'act_qty', 0) - array_get($CycDtlInfo,'sys_qty', 0)
                                     )/array_get($CycDtlInfo,'pack', 0)
                                )*($length*$width*$height)
                            )/1728, 2
                          );

            $dataSKUTrackingRpt = [
                'cus_id'   => $cusId,
                'cus_name' => array_get($cusInfo, 'cus_name', ''),
                'cus_code' => array_get($cusInfo, 'cus_code', ''),

                'cycle_hdr_id'        => $cycleHdr->cycle_hdr_id,
                'trans_num'     => $cycleHdr->cycle_num,
                'whs_id'        => $cycleHdr->whs_id,
                'po_ctnr'       => $cycleHdr->cycle_num,
                'ref_cus_order' => $cycleHdr->cycle_name,
                'actual_date'   => strtotime($cycleHdr->updated_at),

                'item_id' => $CycDtlInfo['item_id'],
                'sku'     => $CycDtlInfo['sku'],
                'size'    => $CycDtlInfo['size'],
                'color'   => $CycDtlInfo['color'],
                'pack'    => empty($CycDtlInfo['pack']) ? null : $CycDtlInfo['pack'],
                'lot'     => $CycDtlInfo['lot'],

                'qty'  => $qty,
                'ctns' => $ctns,
                'cube' => $cube
            ];
            $SKUTrackingReportModel = new SKUTrackingReportModel();
            $SKUTrackingReportModel->create($dataSKUTrackingRpt);
            //---- /Insert Cycle Count Info for SKU Tracking report

            //update cycle_dtl_id to CC_Notification
            if ($ccnIds) {
                foreach ($ccnIds as $ccnId) {
                    $ccNotificationModel->updateWhere([
                        "cycle_dtl_id" => $cycle_dtl_id
                    ], [
                        "ccn_id"   => $ccnId,
                        "loc_code" => $countItem['sys_loc_name'],
                    ]);
                }
            }
        }
        //foreach ($data as $countItem) {
        //    $countItem['cycle_hdr_id'] = $cycleHdr->cycle_hdr_id;
        //    $countItem['pack'] = $countItem['pack_size'];
        //    $countItem['remain'] = $countItem['piece_remain'];
        //    $countItem['sys_qty'] = $countBy == self::COUNT_BY_EACH ?
        //        $countItem['sys_pieces_qty'] : $countItem['sys_carton_qty'];
        //    //$countItem['sys_qty'] = 0;
        //    $countItem['cycle_dtl_sts'] = CycleDtlModel::STATUS_CYCLEDTL_NEW;
        //    $countItem['sts'] = self::STATUS_INSERT;
        //    $validator->validate($countItem);
        //    //$cycleDtl->create($countItem);
        //    $cycle_dtl_id = $cycleDtl->create($countItem)->cycle_dtl_id;
        //
        //    //update cycle_dtl_id to CC_Notification
        //    if($ccnIds) {
        //        foreach ($ccnIds as $ccnId) {
        //            $ccNotificationModel->updateWhere([
        //                "cycle_dtl_id" => $cycle_dtl_id
        //            ], [
        //                "ccn_id"   => $ccnId,
        //                "loc_code" => $countItem['sys_loc_name'],
        //            ]);
        //        }
        //    }
        //}


        return true;
    }

    private function createCycleDtls_bk($cycleHdr, $data, $ccnIds)
    {
        if (!$data) {
            return false;
        }

        $validator = new CycleDtlValidator();

        $cycleDtl = new CycleDtl();
        $ccNotificationModel = new CycleCountNotificationModel();

        $countBy = strtoupper($cycleHdr->count_by);

        foreach ($data as $countItem) {
            $countItem['cycle_hdr_id'] = $cycleHdr->cycle_hdr_id;
            $countItem['pack'] = $countItem['pack_size'];
            $countItem['remain'] = $countItem['piece_remain'];
            $countItem['sys_qty'] = $countBy == self::COUNT_BY_EACH ?
                $countItem['sys_pieces_qty'] : $countItem['sys_carton_qty'];
            $countItem['cycle_dtl_sts'] = CycleDtlModel::STATUS_CYCLEDTL_NEW;
            $countItem['sts'] = self::STATUS_INSERT;

            $validator->validate($countItem);
            //$cycleDtl->create($countItem);
            $cycle_dtl_id = $cycleDtl->create($countItem)->cycle_dtl_id;

            //update cycle_dtl_id to CC_Notification
            foreach ($ccnIds as $ccnId) {
                $ccNotificationModel->updateWhere([
                    "cycle_dtl_id" => $cycle_dtl_id
                ], [
                    "ccn_id"   => $ccnId,
                    "loc_code" => $countItem['sys_loc_name'],
                ]);
            }

        }

        return true;
    }

    /**
     * @param $cycleID
     * @param bool $cycleDtl
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getCycleInfo($cycleID, $cycleDtl = false)
    {
        $cycleHdr = CycleHdr::with('assigneeUser')->with('createdBy');
        if ($cycleDtl) {
            $cycleHdr->with('cycleDtl');
        }
        $data = $cycleHdr->where('cycle_hdr_id', $cycleID)->first();
        if (!$data) {
            return false;
        }
        $data->assignee_to_name = $data->assigneeUser->first_name . ' ' . $data->assigneeUser->last_name;
        $data->assignee_by_name = $data->createdBy->first_name . ' ' . $data->createdBy->last_name;
        $data->cycle_has_size_color_name = $data->has_color_size == 0 ? 'No' : 'Yes';
        $data->cycle_sts_name = CycleHdrModel::$arrCycleStatus[$data->cycle_sts];
        $data->cycle_type_name = CycleHdrModel::$arrCycleType[$data->cycle_type];
        $data->cycle_method_name = CycleHdrModel::$arrCycleMethod[$data->cycle_method];

        return $data;
    }

    /**
     * Get type list
     */
    public function getCycleTypes()
    {
        $types = self::$arrCycleType;

        $data = [];

        foreach ($types as $key => $val) {
            $data[] = [
                'cycle_type'      => $key,
                'cycle_type_name' => $val,
                'cycle_type_des'  => null
            ];
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getCycleStatus()
    {
        $statuses = self::$arrCycleStatus;

        $data = [];

        foreach ($statuses as $key => $val) {
            $data[] = [
                'cycle_sts'      => $key,
                'cycle_sts_name' => $val,
                'cycle_sts_des'  => null
            ];
        }

        return $data;
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    public function getCycleHdrByIds($ids)
    {
        $result = [];

        if (!$ids) {
            return $result;
        }

        $result = CycleHdr::whereIn('cycle_hdr_id', $ids)
            ->get();

        return $result;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public static function getCycleHdr($id)
    {
        return CycleHdr::where('cycle_hdr_id', $id)
            ->first();
    }

    /**
     * @param $cycleHdrId
     * @param $status
     *
     * @return bool
     */
    public static function updateCycleHdrStatus($cycleHdrId, $status)
    {
        if (!$cycleHdrId) {
            return false;
        }
        if (strtoupper($status) == 'CP') {
            $params = [
                'cycle_sts'    => $status,
                'completed_at' => time(),
            ];
        } else {
            $params = [
                'cycle_sts' => $status
            ];
        }

        return CycleHdr::where('cycle_hdr_id', $cycleHdrId)
            ->update($params);
    }

    public function getCycleStatusByCycleId($cycleHdrs, $status = null)
    {
        $isAssigned = false;

        foreach ($cycleHdrs as $cycleHdr) {
            if ($cycleHdr->cycle_sts != CycleHdrModel::STATUS_CYCLE_ASSIGNED) {
                $isAssigned = $cycleHdr->cycle_name;
            }

        }

        return $isAssigned;
    }

    /**
     * @param $messages
     */
    public function setErrors($messages)
    {
        $this->errors = $messages;
    }

    /**
     * @param null $key
     *
     * @return array|mixed
     */
    public function getErrors($key = null)
    {

        return isset($this->errors[$key]) ? $this->errors[$key] : $this->errors;
    }

    /**
     * @param $cycleHdrId
     */
    public function updateCycelHdrAfterProcessed($cycleHdrId)
    {
        //check accept status for cycleDtl
        $query = "
            SELECT cycle_dtl_id FROM cycle_dtl
            WHERE cycle_hdr_id = $cycleHdrId AND deleted = 0 AND cycle_dtl_sts <> 'AC'
            LIMIT 1
        ";
        $rs = $this->naturalExec($query);

        if (empty($rs)) {
            self::updateCycleHdrStatus($cycleHdrId, CycleHdrModel::STATUS_CYCLE_COMPLETED);

            return true;
        }

        return false;
    }

    /**
     * @param $whsId
     * @param $items
     *
     * @return int
     */
    public function checkCartonLockByItems($whsId, $items)
    {
        $result = 0;

        if (!($items && $whsId)) {
            return $result;
        }

        $result = DB::table('cartons')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('location.deleted', 0)
            ->where('cartons.ctn_sts', '=', LockModel::STATUS_LOCK)
            ->whereIn('cartons.item_id', $items)
            ->count();

        return $result;
    }

    /**
     * @param $data
     */
    public function checkCusInUser($data)
    {
        $userId = $data['user_id'];
        $cusId = $data['cus_id'];

        $result = DB::table('cus_in_user')
            ->where('cus_id', $cusId)
            ->where('user_id', $userId)
            ->count();

        return $result > 0 ? true : false;
    }

    /**
     * @param $whsId
     */
    public function getUserInWarehouse($whsId)
    {
        return DB::table('users')
            ->select([
                "users.user_id",
                "users.first_name",
                "users.last_name",
                "users.email",
                "users.created_at",
                "users.updated_at",
                "emp_code",
                "status"
            ])
            ->join('user_whs', "users.user_id", "=", "user_whs.user_id")
            ->join('user_role', 'users.user_id', "=", "user_role.user_id")
            ->join('authority_child', 'user_role.item_name', '=', 'authority_child.parent')
            ->whereIn('authority_child.child', ['editCycleCount', 'cycleCount'])
            ->where("user_whs.whs_id", $whsId)
            ->where("user_whs.user_id", "!=", Data::getCurrentUserId())
            ->orderBy('users.first_name')
            ->distinct()
            ->get();
    }

    /**
     * @param $cycleDtls
     */
    private function getItemLockByAnotherCycle($whsId, $cycleDtls)
    {
        $result = CycleDtl::select(
            'cycle_dtl.sku',
            'cycle_hdr.cycle_hdr_id',
            'cycle_hdr.cycle_name'
        )
            ->join('cycle_hdr', 'cycle_hdr.cycle_hdr_id', '=', 'cycle_dtl.cycle_hdr_id')
            ->whereNotIn('cycle_hdr.cycle_sts', [
                self::STATUS_CYCLE_COMPLETED,
                self::STATUS_CYCLE_DELETED
            ])
            ->whereIn('cycle_dtl.item_id', $cycleDtls)
            ->where('cycle_hdr.whs_id', $whsId)
            ->distinct()
            ->get()
            ->toArray();

        return $result;
    }

    /**
     * @param $cycleHdrs
     * @param $currentUserId
     */
    public function getCycleAssigToUserId($cycleHdrs, $userId)
    {
        $result = [];

        foreach ($cycleHdrs as $cycleHdr) {
            if ($cycleHdr->assignee_id == $userId) {
                $result[] = $cycleHdr->cycle_hdr_id;
            }
        }

        return $result;
    }

    /**
     * @param $cycleHdrID
     * @param $locIds
     *
     * @return array
     */
    public function getSysLocIdCanUnLock($whsId, $cycleHdrID, $locIds)
    {
        $result = [];

        if (!($cycleHdrID && $locIds)) {
            return $result;
        }

        $itemInOtherCC = CycleHdr::select('cycle_dtl.sys_loc_id')
            ->join('cycle_dtl', 'cycle_dtl.cycle_hdr_id', '=', 'cycle_hdr.cycle_hdr_id')
            ->where('cycle_hdr.whs_id', $whsId)
            ->whereNotIn('cycle_hdr.cycle_sts', [
                CycleDtlModel::STATUS_CYCLE_COMPLETED, 
                CycleDtlModel::STATUS_CYCLE_DELETED
            ])
            ->where('cycle_hdr.cycle_hdr_id', '<>', $cycleHdrID)
            ->distinct()
            ->pluck('sys_loc_id')
            ->toArray();

        $result = CycleDtl::select('sys_loc_id')
            ->where('whs_id', $whsId)
            ->whereIn('sys_loc_id', $locIds)
            ->where('cycle_hdr_id', $cycleHdrID)
            ->where('cycle_dtl_sts', '<>', CycleDtlModel::STATUS_CYCLEDTL_ACCEPTED)
            ->distinct()
            ->get()
            ->toArray();

        $locIdsLock = $result ? array_column($result, 'sys_loc_id') : [];

        $locCanUnLock = array_diff($locIds, $locIdsLock);


        foreach($locCanUnLock as $key => $value) {
            if(in_array($value, $itemInOtherCC)) {
                unset($locCanUnLock[$key]);
            }
        }

        return $locCanUnLock;
    }

    /**
     * @param array $attributes
     * @param int $limit
     *
     * @return mixed
     */

    public function skuAutoComplete($attributes = [], $limit = 20)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
//        $whsId = Data::getCurrentWhsId();

        $query = DB::table('item')
            ->leftJoin('invt_smr', function ($join) {
                $join->on('invt_smr.item_id', '=', 'item.item_id');
                $join->on('invt_smr.cus_id', '=', 'item.cus_id');
            })
            ->selectRaw("item.item_id, item.sku, item.size, item.color, IF(invt_smr.lot IS NULL, 'NA', invt_smr.lot) as lot");
//            ->where('invt_smr.whs_id', $whsId);

//        $query = DB::table('invt_smr')
//            ->selectRaw('item_id, sku, size, color, GROUP_CONCAT(lot) as lot')
//            ->where('whs_id', $whsId);

        if (isset($attributes['cus_id'])) {
            $query->where('item.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['sku'])) {
//            $query->where('sku', 'LIKE', "%{$attributes['sku']}%");
            $query->where('item.sku', 'LIKE', "{$attributes['sku']}%");
        }

//        if (isset($attributes['groupBySku'])) {
//            $query->groupBy('invt_smr.sku');
//        } else {
//            $query->groupBy('invt_smr.item_id');
//        }

        $query->groupBy('item.item_id', 'lot');
        $query->orderBy('item.sku');

        return $query->take($limit)->get();
    }

    private function validationUserPermissionToCustomer($whsId, array $data)
    {
        /*$userInfo = new Data();

        $cusInUser = DB::table('cus_in_user')
            ->where('whs_id', $whsId)
            ->whereIn('cus_id', $data)
            ->where('user_id', $userInfo->getCurrentUserId())
            ->groupBy('cus_id')
            ->get(['cus_id']);

        if (array_diff($data, array_column($cusInUser, 'cus_id'))) {
            $this->errors[] = 'User don\'t have permission for this customer on current warehouse';

            return false;
        }*/

        return true;
    }

    public static function updateStatusLocationPutAwayHistory()
    {
        DB::statement('
            UPDATE putaway_hist
            JOIN location ON location.loc_id = putaway_hist.loc_id 
            SET putaway_hist.loc_sts_code = location.loc_sts_code 
            WHERE location.deleted = 0;

        ');
    }

    public function getCycleCountNotComplete($whsId, $cusId)
    {
        return $this->model
            ->where('whs_id', $whsId)
            ->where('cycle_detail', $cusId)
            ->where('cycle_type', 'CS')
            ->where('cycle_sts', '<>', 'CP')
            ->first();
    }
}
