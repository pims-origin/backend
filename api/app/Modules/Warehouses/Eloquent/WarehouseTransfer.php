<?php
namespace App\Modules\Warehouses\Eloquent;

use Illuminate\Database\Eloquent\Model;

class WarehouseTransfer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_transfer';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_tf_id';

    /**
     * @var array
     */
    protected $fillable = [
        'wv_tf_num',
        'whs_id',
        'cus_id',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'cus_upc',
        'peice_qty',
        'act_peice_qty',
        'req_loc_id',
        'req_loc',
        'act_req_loc_id',
        'act_req_loc',
        'mez_loc_id',
        'mez_loc',
        'act_mez_loc',
        'act_mez_loc_id',
        'wv_tf_sts',
        'sts',
        'wv_tk_num'
    ];
}
