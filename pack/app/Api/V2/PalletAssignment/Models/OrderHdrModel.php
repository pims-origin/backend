<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\PalletAssignment\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Dingo\Api\Exception\UnknownVersionException;
use Seldat\Wms2\Utils\Message;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{
    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }

    /**
     * @param $orderHdrId
     *
     * @return mixed
     */
    public function deleteOrderHdr($orderHdrId)
    {
        return $this->model
            ->where('ord_hdr_id', $orderHdrId)
            ->delete();
    }



    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function checkWhereIn($orderIds)
    {
        return $this->model
            ->whereIn('odr_id', $orderIds)
            ->count();
    }

    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function getOrderById($order_ids)
    {
        $order_ids = is_array($order_ids) ? $order_ids : [$order_ids];

        $rows = $this->model
            ->whereIn('odr_id', $order_ids)
            ->get();

        return $rows;
    }

    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function findWhereIn(array $orderIds, $with = [])
    {
        $query = $this->make($with);
        $model = $query->whereIn('odr_id', $orderIds)->get();
        return $model;
    }



}
