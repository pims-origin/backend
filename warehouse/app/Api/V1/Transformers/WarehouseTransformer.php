<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Warehouse;

class WarehouseTransformer extends TransformerAbstract
{
    public function transform(Warehouse $warehouse)
    {
        return [
            'whs_id'           => $warehouse->whs_id,
            'whs_name'         => $warehouse->whs_name,
            'whs_status'       => $warehouse->whs_status,
            'whs_status_name'  => !empty($warehouse->warehouseStatus->whs_sts_name)
                                ? $warehouse->warehouseStatus->whs_sts_name
                                : '',
            'whs_status_desc'  => !empty($warehouse->warehouseStatus->whs_sts_desc)
                                ? $warehouse->warehouseStatus->whs_sts_desc
                                : '',
            'whs_short_name'   => $warehouse->whs_short_name,
            'whs_code'         => $warehouse->whs_code,
            'whs_country_id'   => $warehouse->whs_country_id,
            'whs_country_name' => !empty($warehouse->systemCountry->sys_country_name)
                                ? $warehouse->systemCountry->sys_country_name
                                : null,
            'whs_state_id'     => $warehouse->whs_state_id,
            'whs_state_name'   => !empty($warehouse->systemState->sys_state_name)
                                ? $warehouse->systemState->sys_state_name
                                : null,
            'whs_city_name'    => $warehouse->whs_city_name,
            'whs_contact'      => [
                'whs_con_id'         => !empty($warehouse->warehouseContact->whs_con_id)
                                        ? $warehouse->warehouseContact->whs_con_id
                                        : null,
                'whs_con_fname'      => !empty($warehouse->warehouseContact->whs_con_fname)
                                        ? htmlspecialchars($warehouse->warehouseContact->whs_con_fname)
                                        : null,
                'whs_con_lname'      => !empty($warehouse->warehouseContact->whs_con_lname)
                                        ? htmlspecialchars($warehouse->warehouseContact->whs_con_lname)
                                        : null,
                'whs_con_email'      => !empty($warehouse->warehouseContact->whs_con_email)
                                        ? $warehouse->warehouseContact->whs_con_email
                                        : null,
                'whs_con_phone'      => !empty($warehouse->warehouseContact->whs_con_phone)
                                        ? $warehouse->warehouseContact->whs_con_phone
                                        : null,
                'whs_con_mobile'     => !empty($warehouse->warehouseContact->whs_con_mobile)
                                        ? $warehouse->warehouseContact->whs_con_mobile
                                        : null,
                'whs_con_position'   => !empty($warehouse->warehouseContact->whs_con_position)
                                        ? htmlspecialchars($warehouse->warehouseContact->whs_con_position)
                                        : null,
                'whs_con_department' => !empty($warehouse->warehouseContact->whs_con_department)
                                        ? $warehouse->warehouseContact->whs_con_department
                                        : null,
            ],
        ];
    }
}
