<?php

namespace Seldat\Wms2\Models;


class InvEmailed extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inv_emailed';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'inv_email_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'inv_num',
        'inv_dt',
        'inv_sts',
        'file_url'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

}
