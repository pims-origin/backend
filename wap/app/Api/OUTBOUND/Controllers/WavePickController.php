<?php

namespace App\Api\OUTBOUND\Controllers;

use App\Api\OUTBOUND\Models\CartonModel;
use App\Api\OUTBOUND\Models\OrderCartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\Log;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\WaveDtlLocModel;
use App\Api\OUTBOUND\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Transformers\WvHdrDetailsTransformer;
use App\Api\V1\Validators\WaveValidator;
use App\MyHelper;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Seldat\Wms2\Utils\Status;
use Illuminate\Support\Facades\DB;

class WavePickController extends AbstractController
{
    /**
     * @var Wave pick header
     */
    protected $waveHdrModel;

    /**
     * @var Wave pick detail
     */
    protected $waveDtlModel;

    /**
     * @var WaveDtlLocModel
     */
    protected $waveDtlLocModel;

    /**
     * @var OrderHdrModel
     */
    protected $odrHdr;
    protected $odrCtn;

    /**
     * @var Order Detail
     */
    protected $odrDtl;
    protected $ctnModel;

    /**
     * @var Location
     */
    protected $loc;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    protected $STR_PAD_LEFT = 0;
    protected $userId;

    /**
     * WavePickController constructor.
     */
    public function __construct()
    {
        $this->waveHdrModel = new WavePickHdrModel();
        $this->waveDtlModel = new WavePickDtlModel();
        $this->waveDtlLocModel = new WaveDtlLocModel();
        $this->odrHdr = new OrderHdrModel();
        $this->odrDtl = new OrderDtlModel();
        $this->loc = new LocationModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->ctnModel = new CartonModel();
        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    public function getWavePickList($whsId, Request $request, WaveValidator $waveValidator)
    {
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        /*
         * start logs
         */

        $url = "/$whsId/waves";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'WPL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get wave pick list'
        ]);

        /*
         * end logs
         */

        //$waveValidator->validate($input);
        $requireFieldsValidate = $waveValidator->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        try {
            $attributes =[
                'whs_id' => $input['whs_id'],
                'picker' => $input['picker'],
            ];

            $wvs = $this->waveHdrModel->getWaveList($attributes, []);

            if($wvs){
                foreach ($wvs as $wv){
                    $wv->OdrTtl = $this->waveHdrModel->getNumOfOrders($wv->wv_id);
                    $wv->SkuTtl  = $this->waveHdrModel->getNumOfSkus($wv->wv_id);
                }
            }

            return $this->response->paginator($wvs, new WvHdrDetailsTransformer());

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return new Response($this->getResponseData(), 200, [], null);
        }

    }

    private function getWvHdr($wvHdr)
    {
        $wvHdr['of_order'] = $this->odrHdr->countOdrHdrByWvId($wvHdr['wv_id']);
        $wvHdr['total_sku'] = count($wvHdr['details']);
        $actual = $this->waveDtlModel->getActualWvDtl($wvHdr['wv_id']);
        $wvHdr['actual'] = 0;
        if (!empty($actual[0])) {
            $wvHdr['actual'] = array_get($actual[0], 'actual', 0);
        }

        $statusWvDtl = $this->waveDtlModel->getStatusWvHdr($wvHdr['wv_id']);
        $status = 'NOT';
        if (!empty($statusWvDtl[0])) {
            $sumActualWvdtl = array_get($statusWvDtl[0], 'actual', 0);
            $sumPieceQtyWvdtl = array_get($statusWvDtl[0], 'piece_qty', 0);

            if ($sumActualWvdtl === $sumPieceQtyWvdtl) {
                $status = 'DONE';
            } else if ($sumActualWvdtl > 0 && $sumActualWvdtl < $sumPieceQtyWvdtl) {
                $status = 'IN';
            }
        }

        $wvHdr['status'] = $status;

        return $wvHdr;
    }

    public function getSuggestPalletLocation($whsId = false, $wvDtlID = false, Request $request)
    {
        $input = $request->getParsedBody();
        $limit = array_get($input, 'limit', 6);
        /*
         * start logs
         */

        $url = "/outbound/whs/$whsId/wave/sku/$wvDtlID";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'SPL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get suggest pallet location'
        ]);
        /*
         * end logs
         */

        if (!$whsId || !$wvDtlID) {
            return $this->response->errorNotFound();
        }
        //1. select sug_loc_ids where wv_dtl_id = $wvDtlID on table wv_dt_loc
        /*$location = $this->waveDtlLocModel->getFirstWhere(['wv_dtl_id' => $wvDtlID]);
        if (empty($location)) {
            $msg = sprintf('This wv_dtl_id %s is not exists', $wvDtlID);

            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }*/

        //2. select * from wv_dtl where wv_dt_id =  $wvDtlID,

        $detail = $this->waveDtlModel->getFirstWhere([
                                                        'wv_dtl_id' => $wvDtlID,
                                                        'whs_id'    => $whsId]);
        if (!$detail) {
            $msg = sprintf("This Wavepick detail id: %s doesn't exist", $wvDtlID);
            $data = [
                'data'    => $wvDtlID,
                'message' => $msg,
                'status'  => false,
            ];
            return new Response($data, 200, [], null);
        }

        $actualWvTtl = (new OrderCartonModel())->findWhere(['wv_dtl_id' => $wvDtlID])->count();
        $data = [];
        $t1 = floor($detail->piece_qty / $detail->pack_size);
        $t2 = floor($detail->act_piece_qty / $detail->pack_size);
        $wv_hdr = $this->waveHdrModel->getFirstBy('wv_id', $detail->wv_id);

        $getWvDtlSts = $detail->wv_dtl_sts;
        $getWvId     = $detail->wv_id;
        $getItemId   = $detail->item_id;
        $getLot      = $detail->lot;
        $isProcessing = $this->_isProcessing($whsId, $getWvId, $getItemId, $getLot);

        $dataWvDtl = [
            'wv_dtl_id'        => $detail->wv_dtl_id,
            'wv_id'            => $getWvId,
            'item_id'          => $getItemId,
            'wv_num'           => $detail->wv_num,
            'color'            => $detail->color,
            'sku'              => $detail->sku,
            'size'             => $detail->size,
            'pack_size'        => $detail->pack_size,
            'lot'              => $detail->lot,
            'wv_dtl_sts'       => $detail->wv_dtl_sts,
            'wv_sts'           => $wv_hdr->wv_sts,
            'allocate_cartons' => $t1 . '/' . $t2,
            'allocate_pieces'  => ceil($detail->piece_qty . '/' . $detail->act_piece_qty),
            'sts_destination'  => $isProcessing,
            'qty_limit'        => $detail->piece_qty,
            'qty_current'      => $detail->act_piece_qty,
            'ctn_limit'        => $t1,
            'ctn_current'      => $t2,
        ];

        //3. get location: loc_id, loc_code, loc_name from location table where 1.
        //$arrIds = explode(',', $location->sug_loc_ids);
        //$locIds = $this->ctnModel->getCtnByLocIds($arrIds)->toArray();

        /*foreach ($arrIds as $id) {
            $locDt = [];
            $ctn = $this->ctnModel->getCtnByLocationId($id);

            if (!empty($ctn)) {
                $countCtn = $this->ctnModel->countCtnByLocationId($id);
                $sumPiece = $this->ctnModel->sumPieceOfCtnByLocationId($id);
                $locRFID = $this->loc->getLocRFIDByLocId($ctn->loc_id);
                $locDt['loc_id'] = $ctn->loc_id;
                $locDt['loc_code'] = $ctn->loc_code;
                $locDt['loc_rfid'] = $locRFID->rfid;
                $locDt['sku'] = $ctn->sku;
                $locDt['cartons'] = $countCtn;
                $locDt['pieces'] = $sumPiece->pieces;
                $loc[] = $locDt;
                $locIds[] = $ctn->loc_id;
            }
        }*/

        $locIds = [];
        $locObjs = $this->ctnModel->getMoreSugLocByWvDtl($whsId, $detail, $locIds, $limit);

        $loc = [];
        foreach ($locObjs as $locObj) {
            $getLocId = array_get($locObj, 'loc_id', null);
            $loc[] = [
                'loc_id'         => $getLocId,
                'loc_code'       => array_get($locObj, 'loc_code', null),
                'loc_rfid'       => array_get($locObj, 'loc_rfid', null),
                'sku'            => array_get($locObj, 'sku', null),
                'lot'            => array_get($locObj, 'lot', null),
                'cartons'        => array_get($locObj, 'cartons', null),
                'pieces'         => array_get($locObj, 'avail_qty', null),
                'is_cycle_count' => $this->_isCycleCount($whsId, $getLocId),
                'is_rfid'        => $this->_isRfid($whsId, $getLocId),
            ];
        }
        //count result return
        /*while (count($loc) <= $numMaxDataReturn) {
            //call get more function
            $locObj = $this->ctnModel->getMoreLocation($whsId, $wvDtlID, $locIds);
            if (is_null($locObj)) {
                break;
            }
            $locIds[] = array_get($locObj, 'loc_id', null);
            $locTmp = [
                'loc_id' => array_get($locObj, 'loc_id', null),
                'loc_code' => array_get($locObj, 'loc_code', null),
                'loc_rfid' => array_get($locObj, 'loc_rfid', null),
                'sku' => array_get($locObj, 'sku', null),
                'cartons' => array_get($locObj, 'cartons', null),
                'pieces' => array_get($locObj, 'avail_qty', null),
            ];
            $loc[] = $locTmp;
            unset($locTmp);
        }*/

        $data['wv_dtl'] = $dataWvDtl;
        $data['location'] = $loc;
        $data['histories'] = $this->getHistorySKUs($detail->wv_id);

        $array['data'] = $data;

        return new Response($array, 200, [], null);
    }

    private function getHistorySKUs($wvHdrID)
    {
        $wvDtls = $this->waveDtlModel->findWhere(['wv_id' => $wvHdrID]);
        $data = [];
        if (!is_null($wvDtls)) {
            foreach ($wvDtls as $wvDtl) {
                $item = [];
                $item['sku'] = $wvDtl->sku;
                $item['color'] = $wvDtl->color;
                $item['size'] = $wvDtl->size;
                $item['pack_size'] = $wvDtl->pack_size;
                $t1 = $wvDtl->piece_qty / $wvDtl->pack_size;
                $t2 = $wvDtl->act_piece_qty / $wvDtl->pack_size;
                $item['allocate_cartons'] = $t1 . '/' . $t2;
                $item['allocate_pieces'] = $wvDtl->piece_qty . '/' . $wvDtl->act_piece_qty;
                $data[] = $item;
            }
        }

        return $data;
    }

    public function getNextWvDtl($wvId, $currentWvDtlID)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $wvdtls = $this->model
            ->select(
                'wv_dtl_id', 'whs_id', 'wv_id', 'wv_num', 'wv_dtl_sts', 'act_piece_qty', 'piece_qty', 'lot', 'item_id', 'cus_id', 'pack_size', 'sku', 'size', 'color'
            )
            ->where('wv_id', $wvId)
            //->where('wv_dtl_id', '!=', $currentWvDtlID)
            ->whereIn('wv_dtl_sts', ['NW', 'PK'])
            ->get();

        $result = null ;
        $wvdtls->each(function($wvdtl, $index) use ($wvdtls, $currentWvDtlID, &$result){
            if ($wvdtl->wv_dtl_id == $currentWvDtlID){
                if(count($wvdtls) == $index + 1){
                    return $result = $wvdtls->first();

                }elseif (count($wvdtls) ==1){
                    return $result =  null;
                }
                else{
                    return $result = $wvdtls->pull($index + 1);
                }
            }
        });
        return $result;
    }

    public function getNextSKU($whsId, $wv_id, $current_wv_dtl_id, Request $request) {

        $input = $request->getParsedBody();
        $limit = array_get($input, 'limit', 6);
        /*
         * start logs
         */

        $url = "/outbound/whs/$whsId/wave/$wv_id/next-sku/$current_wv_dtl_id";
        $owner = "";
        $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GNS',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get next SKU'
        ]);
        /*
         * end logs
         */

        try {
            $detail = $this->waveDtlModel->getNextWvDtl($wv_id, $current_wv_dtl_id, $whsId);
            if (!$detail) {
                $msg = null;
                $detail = $this->waveDtlModel->getFirstWhere([
                                                    'wv_dtl_id' => $current_wv_dtl_id,
                                                    'wv_id'     => $wv_id,
                                                    'whs_id'    => $whsId,
                                                    ]);
                if (!$detail) {
                    $detail = $this->waveDtlModel->getFirstWhere(['wv_dtl_id' => $current_wv_dtl_id]);
                    $wvNum = object_get($detail, 'wv_num', null);
                    if (!$detail && !$msg) {
                        $msg = sprintf("Wavepick detail doesn't exist!");
                    } else {
                        $detail = $this->waveDtlModel->getFirstWhere(['wv_id' => $wv_id]);
                        if (!$detail && !$msg) {
                            $msg = sprintf("Wavepick doesn't exist!");
                        }
                    }

                    $detail = $this->waveDtlModel->getFirstWhere([
                                                        'wv_dtl_id' => $current_wv_dtl_id,
                                                        'wv_id' => $wv_id,
                                                        ]);
                    if (!$detail && !$msg) {
                        $msg = sprintf("Wavepick detail id: %s doesn't belong to Wavepick num: %s!",
                            $current_wv_dtl_id,
                            $wvNum);
                    }

                    if (!$msg) {
                        $msg = sprintf("Wavepick detail id: %s and Wavepick num: %s don't belong to Wavehouse!",
                            $current_wv_dtl_id,
                            $wvNum);
                    }
                    $data = [
                        'data' => [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false
                        ]
                    ];

                    return new Response($data, 200, [], null);
                }

                $res =  ['data' => [
                    'locations' => [],
                    'wp_dtl_ttl' => new WavePickDtlModel(),
                    'wv_dtl' => null
                ]];

                return $res;
            }

            $wv_hdr = $this->waveHdrModel->getFirstBy('wv_id', $detail->wv_id);
            if (!$wv_hdr) {
                $msg = sprintf("Wavepick is not existed!");
                $data = [
                    'data' => [
                        'data'    => null,
                        'message' => $msg,
                        'status'  => false,
                    ]
                ];

                return new Response($data, 200, [], null);
            }

            $countWPDetails = $this->waveDtlModel->getModel()->where('wv_id', $detail->wv_id)->whereIn('wv_dtl_sts', ['NW', 'PK'])->count();
            $t1 = floor($detail->piece_qty / $detail->pack_size);
            $t2 = floor($detail->act_piece_qty / $detail->pack_size);

            $getWvDtlSts = $detail->wv_dtl_sts;
            $getWvId     = $detail->wv_id;
            $getItemId   = $detail->item_id;
            $getLot      = $detail->lot;
            $isProcessing = $this->_isProcessing($whsId, $getWvId, $getItemId, $getLot);

            $dataWvDtl = [
                'wv_dtl_id'        => $detail->wv_dtl_id,
                'wv_id'            => $getWvId,
                'item_id'          => $getItemId,
                'wv_num'           => $detail->wv_num,
                'color'            => $detail->color,
                'sku'              => $detail->sku,
                'size'             => $detail->size,
                'pack_size'        => $detail->pack_size,
                'lot'              => $detail->lot,
                'wv_dtl_sts'       => $detail->wv_dtl_sts,
                'wv_sts'           => $wv_hdr->wv_sts,
                'allocate_cartons' => $t1 . '/' . $t2,
                'allocate_pieces'  => ceil($detail->piece_qty . '/' . $detail->act_piece_qty),
                'sts_destination'  => $isProcessing,
                'qty_limit'        => $detail->piece_qty,
                'qty_current'      => $detail->act_piece_qty,
                'ctn_limit'        => $t1,
                'ctn_current'      => $t2,

            ];

            if($detail){
                $locIds = [];
                $result = $this->ctnModel->getMoreSugLocByWvDtl($whsId, $detail, $locIds, $limit);
                $loc = [];
                foreach ($result as $locObj) {
                    $getLocId = array_get($locObj, 'loc_id', null);
                    $loc[] = [
                        'loc_id'         => $getLocId,
                        'loc_code'       => array_get($locObj, 'loc_code', null),
                        'loc_rfid'       => array_get($locObj, 'loc_rfid', null),
                        'sku'            => array_get($locObj, 'sku', null),
                        'lot'            => array_get($locObj, 'lot', null),
                        'cartons'        => array_get($locObj, 'cartons', null),
                        'pieces'         => array_get($locObj, 'avail_qty', null),
                        'is_cycle_count' => $this->_isCycleCount($whsId, $getLocId),
                        'is_rfid'        => $this->_isRfid($whsId, $getLocId),

                    ];
                }

                $res =  ['data' => [
                    'wv_dtl' => $dataWvDtl,
                    'location' => $loc,
                    'wp_dtl_ttl' => $countWPDetails,
                    'histories' => $this->getHistorySKUs($detail->wv_id)
                ]];
                return $res;
            }

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _isCycleCount($whsId, $locId)
    {
        $query = DB::table('cc_notification')->where([
                    'whs_id'  => $whsId,
                    'loc_id'  => $locId,
                    'deleted' => 0,
                ])
                ->whereIn('cc_ntf_sts', ['NW'])
                ->first();

        return $query ? true : false;
    }

    private function _isRfid($whsId, $locId)
    {
        $query = DB::table('cartons')->where([
                    'whs_id'  => $whsId,
                    'loc_id'  => $locId,
                    'deleted' => 0,
                ])
                ->whereNotNull('rfid')
                ->where('ctn_sts', '!=', 'AJ')
                ->first();

        return $query ? true : false;
    }

    private function _isProcessing($whsId, $wvId, $itemId, $lot)
    {
        $query = DB::table('odr_hdr')
                    ->join('vas_hdr', 'odr_hdr.odr_id', '=', 'vas_hdr.odr_hdr_id')
                    ->where([
                    'odr_hdr.whs_id'  => $whsId,
                    'odr_hdr.wv_id'   => $wvId,
                    // 'wo_dtl.item_id'  => $itemId,
                    // 'wo_dtl.lot'      => $lot,
                    'odr_hdr.deleted' => 0,
                    'vas_hdr.deleted'  => 0,
                ])
                ->first();

        return $query ? "Work Order" : "Processing";
    }
}
