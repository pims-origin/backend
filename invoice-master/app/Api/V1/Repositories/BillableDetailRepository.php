<?php

namespace App\Api\Respositories;

use App\BillableDtl;

class BillableDetailRepository extends BaseRepository
{
    public function __construct(BillableDtl $model)
    {
        $this->model = $model;
    }
}
