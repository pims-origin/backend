<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CustomerChargeDetailTest extends TestCase
{
    use DatabaseTransactions;

    protected $customerId;

    public function getEndPoint()
    {
        return "/v1/customers/{$this->customerId}/charge-details";
    }

    protected $params = [
        'cus_charge_dtl_des' => ['descr1', 'descr2', 'descr3'],
    ];

    public function setUp()
    {
        parent::setUp();
        $this->customerId = factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;

        $warehouse1 = factory(\Seldat\Wms2\Models\Warehouse::class)->create();
        $warehouse2 = factory(\Seldat\Wms2\Models\Warehouse::class)->create();
        $warehouse3 = factory(\Seldat\Wms2\Models\Warehouse::class)->create();

        $chargeCode1 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create();
        $chargeCode2 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create();
        $chargeCode3 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create();

        $cusChargeDtl1 = factory(\Seldat\Wms2\Models\CustomerChargeDetail::class)->create();
        $cusChargeDtl2 = factory(\Seldat\Wms2\Models\CustomerChargeDetail::class)->create();
        $cusChargeDtl3 = factory(\Seldat\Wms2\Models\CustomerChargeDetail::class)->create();

        $this->params['whs_id'] = [
            $warehouse1->whs_id,
            $warehouse2->whs_id,
            $warehouse3->whs_id,
        ];

        $this->params['chg_code_id'] = [
            $chargeCode1->chg_code_id,
            $chargeCode2->chg_code_id,
            $chargeCode3->chg_code_id,
        ];

        $this->params['cus_charge_dtl_rate'] = [
            $cusChargeDtl1->cus_charge_dtl_rate,
            $cusChargeDtl2->cus_charge_dtl_rate,
            $cusChargeDtl3->cus_charge_dtl_rate
        ];

    }

    public function testSaveMass_Ok()
    {
        $this->call('POST', $this->getEndPoint(), $this->params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testStore_EmptyField_Fail()
    {
        unset($this->params['cus_charge_dtl_rate']);
        $result = $this->call('POST', $this->getEndPoint(), $this->params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertEquals(json_decode($result->content())->errors->message, '422 Unprocessable Entity');
    }

    public function testStore_InvalidField_Fail()
    {
        unset($this->params['cus_charge_dtl_rate'][1]);

        $result = $this->call('POST', $this->getEndPoint(), $this->params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
        $this->assertEquals(json_decode($result->content())->errors->message, 'Some Input Missing!');
    }

}