<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\InventorySummary;


class InventorySummaryTransformer extends TransformerAbstract
{
    //return result
    public function transform(InventorySummary $inventorySummary)
    {

        return [
            'item_id'       => object_get($inventorySummary, 'item_id', null),
            'color'         => object_get($inventorySummary, 'color', null),
            'size'          => object_get($inventorySummary, 'size', null),
            'lot'           => object_get($inventorySummary, 'lot', null),
            'ttl'           => object_get($inventorySummary, 'ttl', 0),
            'cus_upc'       => object_get($inventorySummary, 'item.cus_upc', null),
            'width'         => object_get($inventorySummary, 'item.width', null),
            'weight'        => object_get($inventorySummary, 'item.weight', null),
            'height'        => object_get($inventorySummary, 'item.height', null),
            'length'        => object_get($inventorySummary, 'item.length', null),
            'uom_id'        => object_get($inventorySummary, 'item.uom_id', null),
            'uom_name'      => object_get($inventorySummary, 'item.systemUom.sys_uom_name', null),
            'allocated_qty' => object_get($inventorySummary, 'allocated_qty', null),
            'dmg_qty'       => object_get($inventorySummary, 'dmg_qty', null),
            'sku'           => object_get($inventorySummary, 'sku', null),
            'avail'         => object_get($inventorySummary, 'avail', null),
        ];
    }


}
