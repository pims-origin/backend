<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\BOLCarrierDetailModel;
use App\Api\V1\Models\BOLOrderDetailModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CommodityModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\ReportModel;
use App\Api\V1\Models\ReportService;
use App\Api\V1\Models\ShipmentModel;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Traits\OrderFlowControllerTrait;
use App\Api\V1\Transformers\BOLListTransformer;
use App\Api\V1\Transformers\CommodityTransformer;
use App\Api\V1\Validators\BOLHdrValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Wms2\UserInfo\Data;
use mPDF;

/**
 * Class BOLController
 *
 * @package App\Api\V1\Controllers
 */
class BOLController extends AbstractController
{
    use OrderFlowControllerTrait;
    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var ShipmentModel
     */
    protected $shipmentModel;

    /**
     * @var BOLCarrierDetailModel
     */
    protected $bolCarrierDtlModel;

    /**
     * @var BOLOrderDetailModel
     */
    protected $bolOdrDtlModel;

    /**
     * @var CommodityModel
     */
    protected $commodityModel;

    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    /**
     * @var ReportModel
     */
    protected $reportModel;

    /**
     * @var ReportService
     */
    protected $reportService;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    protected $invtSmrModel;

    /**
     * BOLController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param ShipmentModel $shipmentModel
     * @param BOLCarrierDetailModel $bolCarrierDetailModel
     * @param BOLOrderDetailModel $bolOrderDetailModel
     * @param CommodityModel $commodityModel
     * @param WarehouseModel $warehouseModel
     * @param ReportModel $reportModel
     * @param ReportService $reportService
     * @param CustomerConfigModel $customerConfigModel
     * @param EventTrackingModel $eventTrackingModel
     * @param OrderDtlModel $orderDtlModel
     * @param InventorySummaryModel $invtSmrModel
     * @param OrderCartonModel $orderCartonModel
     * @param CartonModel $cartonModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        ShipmentModel $shipmentModel,
        BOLCarrierDetailModel $bolCarrierDetailModel,
        BOLOrderDetailModel $bolOrderDetailModel,
        CommodityModel $commodityModel,
        WarehouseModel $warehouseModel,
        ReportModel $reportModel,
        ReportService $reportService,
        CustomerConfigModel $customerConfigModel,
        EventTrackingModel $eventTrackingModel,
        OrderDtlModel $orderDtlModel,
        InventorySummaryModel $invtSmrModel,
        OrderCartonModel $orderCartonModel,
        CartonModel $cartonModel
    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->shipmentModel = $shipmentModel;
        $this->bolCarrierDtlModel = $bolCarrierDetailModel;
        $this->bolOdrDtlModel = $bolOrderDetailModel;
        $this->commodityModel = $commodityModel;
        $this->warehouseModel = $warehouseModel;
        $this->reportModel = $reportModel;
        $this->reportService = $reportService;
        $this->customerConfigModel = $customerConfigModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->invtSmrModel = $invtSmrModel;
        $this->orderCartonModel = $orderCartonModel;
        $this->cartonModel = $cartonModel;
    }

    /**
     * @param Request $request
     * @param BOLHdrValidator $bolHdrValidator
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(Request $request, BOLHdrValidator $bolHdrValidator)
    {
        //get params
        $input = $request->getParsedBody();

        $bolHdrValidator->validate($input);

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $input['items'][0]['odr_id'])->toArray();
        $volumes = DB::table('odr_hdr')
            ->select(['p.odr_hdr_id', DB::raw('sum(p.length * p.width * p.height) as vol_ttl')])
            ->leftJoin("pack_hdr as p", "p.odr_hdr_id", "=", 'odr_hdr.odr_id')
            ->whereIn('odr_id', array_column($input['items'], 'odr_id'))
            ->where([
                'odr_hdr.deleted'    => 0,
                'odr_hdr.deleted_at' => 915148800
            ])->groupBy('p.odr_hdr_id')->get();

        $volumes = array_pluck($volumes, "vol_ttl", "odr_hdr_id");

        $warehouse = $this->warehouseModel->getFirstBy('whs_id', $whsId, [
            'warehouseAddress',
            'systemCountry',
            'systemState'
        ])->toArray();

        $boNum = $this->shipmentModel->getBOLNum();

        $shipmentParam = [
            'whs_id'   => $whsId,
            'cus_id'   => $input['cus_id'],
            'bo_num'   => $boNum,
            'bo_label' => $input['bo_label'],
            'ship_dt'  => $orderHdr['ship_by_dt'],
            'ship_sts' => !empty($input['ship_sts']) ?
                $input['ship_sts'] : Status::getByValue("New", "Ship-Status"),

            'ship_from_name'    => $warehouse['whs_name'],
            'ship_from_addr_1'  => array_get($warehouse, 'warehouse_address.whs_add_line_1', null),
            'ship_from_addr_2'  => array_get($warehouse, 'warehouse_address.whs_add_line_2', null),
            'ship_from_city'    => $warehouse['whs_city_name'],
            'ship_from_state'   => array_get($warehouse, 'system_state.sys_state_code', null),
            'ship_from_zip'     => array_get($warehouse, 'warehouse_address.whs_add_postal_code', null),
            'ship_from_country' => array_get($warehouse, 'system_country.sys_country_code', 'US'),

            'ship_to_name'    => $orderHdr['ship_to_name'],
            'ship_to_addr_1'  => $orderHdr['ship_to_add_1'],
            'ship_to_addr_2'  => $orderHdr['ship_to_add_2'],
            'ship_to_city'    => $orderHdr['ship_to_city'],
            'ship_to_state'   => $orderHdr['ship_to_state'],
            'ship_to_zip'     => $orderHdr['ship_to_zip'],
            'ship_to_country' => $orderHdr['ship_to_country'],

            'bill_to_name'    => array_get($input, 'bill_to_name', null),
            'bill_to_addr_1'  => array_get($input, 'bill_to_addr_1', null),
            'bill_to_city'    => array_get($input, 'bill_to_city', null),
            'bill_to_state'   => array_get($input, 'bill_to_state', null),
            'bill_to_zip'     => array_get($input, 'bill_to_zip', null),
            'bill_to_country' => "US",

            'special_inst'         => $input['special_inst'],
            'carrier'              => $input['carrier'],
            'trailer_num'          => array_get($input, 'trailer_num', null),
            'seal_num'             => array_get($input, 'seal_num', null),
            'scac'                 => array_get($input, 'scac', null),
            'pro_num'              => array_get($input, 'pro_num', null),
            'freight_charge_terms' => $input['freight_charge_terms'],
            'freight_charge_cost'  => array_get($input, 'freight_charge_cost', 0),
            'freight_counted_by'   => $input['freight_counted_by'],
            'po_qty_ttl'           => count($input['items']),
            'weight_ttl'           => (float)array_sum(array_column($input['items'], 'weight')),
            'ctn_qty_ttl'          => array_sum(array_column($input['items'], 'pkgs')),
            'piece_qty_ttl'        => array_sum(array_column($input['items'], 'units')),
            'vol_qty_ttl'          => array_sum($volumes),
            'cube_qty_ttl'         => array_sum($volumes) / 1728,
            'plt_qty_ttl'          => array_sum(array_column($input['items'], 'plts')),
            'fee_terms'            => $input['fee_terms'],
            'cus_accept'           => $input['cus_accept'],
            'trailer_loaded_by'    => $input['trailer_loaded_by'],
            'sts'                  => 'i',
            'party_acc'            => $input['party_acc'],
            'deli_service'         => $input['deli_service'],
            'is_attach'            => array_get($input, 'is_attach', 0),
            'ship_method'          => array_get($input, 'ship_method', null),
        ];

        try {
            DB::beginTransaction();

            $shipment = $this->shipmentModel->create($shipmentParam);

            foreach ($input['items'] as $item) {
                $volume = !empty($volumes[$item['odr_id']]) ? $volumes[$item['odr_id']] : 0;
                $bolOdrDtlParam = [
                    'odr_id'           => $item['odr_id'],
                    'ship_id'          => $shipment->ship_id,
                    'cus_po'           => $item['cus_po'],
                    'ctn_qty'          => $item['pkgs'], // pkgs
                    'piece_qty'        => $item['units'], // units
                    'weight'           => (float)array_get($item, 'weight', 0),
                    'plt_qty'          => $item['plts'], //plts
                    'cus_dept'         => $item['cus_dept'],
                    'cus_ticket'       => array_get($item, 'cus_ticket', null),
                    'cus_odr'          => $item['cus_odr'],
                    'add_shipper_info' => array_get($item, 'add_shipper_info'),
                    'odr_hdr_num'      => $item['odr_hdr_num'],
                    'vol_qty_ttl'      => $volume,
                    'cube_qty_ttl'     => $volume / 1728,
                ];

                $this->bolOdrDtlModel->refreshModel();
                $odr = $this->bolOdrDtlModel->getFirstBy('odr_id', $item['odr_id']);
                if (empty($odr->odr_id)) {
                    // Create
                    $this->bolOdrDtlModel->refreshModel();
                    $this->bolOdrDtlModel->create($bolOdrDtlParam);
                } else {
                    // Update or Fail
                    if (empty($odr->ship_id)) {
                        $this->bolOdrDtlModel->refreshModel();
                        $this->bolOdrDtlModel->updateWhere($bolOdrDtlParam, ['odr_id' => $item['odr_id']]);
                    } else {
                        throw new \Exception("BOL for this order has been created!");
                    }
                }

                // Update Order Header ship_id
                $this->orderHdrModel->refreshModel();
                $this->orderHdrModel->updateWhere(['ship_id' => $shipment->ship_id], ['odr_id' => $item['odr_id']]);

            }

            $cmdDes = $this->commodityModel->getFirstBy('cmd_id', $input['cmd_des']);

            $carrierDtlParam = [
                'ship_id'  => $shipment->ship_id,
                'hdl_type' => 'PLTS',// plts
                'hdl_qty'  => array_sum(array_column($input['items'], 'plts')), // sum(plts)
                'pkg_type' => 'CTNS',// ctns
                'pkg_qty'  => array_sum(array_column($input['items'], 'pkgs')),// sum(pkgs)
                'weight'   => (int)array_sum(array_column($input['items'], 'weight')),
                'hm'       => 1,
                'cmd_id'   => $input['cmd_des'],
                'cmd_des'  => $cmdDes->cmd_name,
                'nmfc_num' => $cmdDes->commodity_nmfc, // commodity_nmfc
                'cls_num'  => $cmdDes->cmd_cls, // cmd_cls
            ];

            $this->bolCarrierDtlModel->create($carrierDtlParam);

            // order flow shipping
            if ($input['ship_sts'] == 'FN') {
                $ordIds = array_column($input['items'], 'odr_id');
                $this->shippedFlow($ordIds, $request);
            }

            DB::commit();

            //push MQ receiving
            if ($this->customerConfigModel->checkWhere([
                'whs_id'       => $whsId,
                'cus_id'       => $input['cus_id'],
                'config_name'  => config('constants.cus_config_name.EDI_INTEGRATION'),
                'config_value' => config('constants.cus_config_value.EDI858'),
                'ac'           => config('constants.cus_config_active.YES')
            ])
            ) {

                $reportDetail = $this->reportModel->shipment($shipment->ship_id);
                if ($reportDetail) {
                    $this->reportService->init($reportDetail);
                    $this->reportService->process();
                }
            }

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'ship_id' => $shipment->ship_id
                ]
            ])->setStatusCode(Response::HTTP_CREATED);
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }


    /**
     * @param $shipId
     * @param Request $request
     * @param BOLHdrValidator $bolHdrValidator
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function update($shipId, Request $request, BOLHdrValidator $bolHdrValidator)
    {
        //get params
        $input = $request->getParsedBody();
        $input['ship_id'] = $shipId;
        $bolHdrValidator->validate($input);

        $shipment = $this->shipmentModel->getFirstWhere(['ship_id' => $shipId]);
        if (empty($shipment->ship_id)) {
            throw new \Exception(Message::get("BM017", "Shipment"));
        }

        $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $input['items'][0]['odr_id'])->toArray();
        $volumes = DB::table('odr_hdr')
            ->select(['p.odr_hdr_id', DB::raw('sum(p.length * p.width * p.height) as vol_ttl')])
            ->leftJoin("pack_hdr as p", "p.odr_hdr_id", "=", 'odr_hdr.odr_id')
            ->whereIn('odr_id', array_column($input['items'], 'odr_id'))
            ->where([
                'odr_hdr.deleted'    => 0,
                'odr_hdr.deleted_at' => 915148800
            ])->groupBy('p.odr_hdr_id')->get();

        $volumes = array_pluck($volumes, "vol_ttl", "odr_hdr_id");

        $shipmentParam = [
            'whs_id'          => $shipment->whs_id,
            'cus_id'          => $shipment->cus_id,
            'bo_num'          => $shipment->bo_num,
            'bo_label'        => $input['bo_label'],
            'ship_dt'         => $orderHdr['ship_by_dt'],
            'ship_to_name'    => $orderHdr['ship_to_name'],
            'ship_to_addr_1'  => $orderHdr['ship_to_add_1'],
            'ship_to_addr_2'  => $orderHdr['ship_to_add_2'],
            'ship_to_city'    => $orderHdr['ship_to_city'],
            'ship_to_state'   => $orderHdr['ship_to_state'],
            'ship_to_zip'     => $orderHdr['ship_to_zip'],
            'ship_to_country' => $orderHdr['ship_to_country'],

            'bill_to_name'    => array_get($input, 'bill_to_name', null),
            'bill_to_addr_1'  => array_get($input, 'bill_to_addr_1', null),
            'bill_to_city'    => array_get($input, 'bill_to_city', null),
            'bill_to_state'   => array_get($input, 'bill_to_state', null),
            'bill_to_zip'     => array_get($input, 'bill_to_zip', null),
            'bill_to_country' => "US",

            'special_inst'         => $input['special_inst'],
            'carrier'              => $input['carrier'],
            'trailer_num'          => array_get($input, 'trailer_num', null),
            'seal_num'             => array_get($input, 'seal_num', null),
            'scac'                 => array_get($input, 'scac', null),
            'pro_num'              => array_get($input, 'pro_num', null),
            'freight_charge_terms' => $input['freight_charge_terms'],
            'freight_charge_cost'  => array_get($input, 'freight_charge_cost', 0),
            'freight_counted_by'   => $input['freight_counted_by'],
            'po_qty_ttl'           => count($input['items']),
            'weight_ttl'           => (float)array_sum(array_column($input['items'], 'weight')),
            'ctn_qty_ttl'          => array_sum(array_column($input['items'], 'pkgs')),
            'piece_qty_ttl'        => array_sum(array_column($input['items'], 'units')),
            'vol_qty_ttl'          => array_sum($volumes),
            'cube_qty_ttl'         => array_sum($volumes) / 1728,
            'plt_qty_ttl'          => array_sum(array_column($input['items'], 'plts')),
            'fee_terms'            => $input['fee_terms'],
            'cus_accept'           => $input['cus_accept'],
            'trailer_loaded_by'    => $input['trailer_loaded_by'],
            'sts'                  => 'u',
            'party_acc'            => $input['party_acc'],
            'deli_service'         => $input['deli_service'],
            'is_attach'            => array_get($input, 'is_attach', 0),
            'ship_method'          => array_get($input, 'ship_method', null),
            'ship_sts'             => !empty($input['ship_sts']) ?
                $input['ship_sts'] : Status::getByValue("New", "Ship-Status"),
        ];

        try {
            DB::beginTransaction();

            $this->shipmentModel->refreshModel();
            $this->shipmentModel->updateWhere($shipmentParam, ['ship_id' => $shipId]);
            $allOrder = $this->orderHdrModel->findWhere(['ship_id' => $shipId])->toArray();
            $deleteIds = array_pluck($allOrder, 'odr_id', 'odr_id');

            foreach ($input['items'] as $item) {
                $volume = !empty($volumes[$item['odr_id']]) ? $volumes[$item['odr_id']] : 0;
                $bolOdrDtlParam = [
                    'ship_id'          => $shipId,
                    'ctn_qty'          => $item['pkgs'], // pkgs
                    'piece_qty'        => $item['units'], // units
                    'weight'           => (float)array_get($item, 'weight', 0),
                    'plt_qty'          => $item['plts'], //plts
                    'cus_dept'         => $item['cus_dept'],
                    'cus_ticket'       => array_get($item, 'cus_ticket', null),
                    'cus_odr'          => $item['cus_odr'],
                    'add_shipper_info' => $item['add_shipper_info'],
                    'odr_hdr_num'      => $item['odr_hdr_num'],
                    'vol_qty_ttl'      => $volume,
                    'cube_qty_ttl'     => $volume / 1728,
                    'odr_id'           => $item['odr_id'],
                    'cus_po'           => $item['cus_po'],
                ];

                $this->bolOdrDtlModel->refreshModel();
                $odr = $this->bolOdrDtlModel->getFirstBy('odr_id', $item['odr_id']);
                if (empty($odr->odr_id)) {
                    // Create
                    $this->bolOdrDtlModel->refreshModel();
                    $this->bolOdrDtlModel->create($bolOdrDtlParam);
                } else {
                    // Update or Fail
                    $this->bolOdrDtlModel->refreshModel();
                    $this->bolOdrDtlModel->updateWhere($bolOdrDtlParam, ['odr_id' => $item['odr_id']]);
                }

                // Update Order Header ship_id
                if (in_array($item['odr_id'], $deleteIds)) {
                    unset($deleteIds[$item['odr_id']]);
                } else {
                    $this->orderHdrModel->refreshModel();
                    $this->orderHdrModel->updateWhere(['ship_id' => $shipId], ['odr_id' => $item['odr_id']]);
                }
            }

            if (!empty($deleteIds)) {
                foreach ($deleteIds as $deleteId) {
                    $this->orderHdrModel->refreshModel();
                    $this->orderHdrModel->updateWhere(['ship_id' => null], ['odr_id' => $deleteId]);

                    $this->bolOdrDtlModel->refreshModel();
                    //$this->bolOdrDtlModel->updateWhere(['ship_id' => null], ['odr_id' => $deleteId]);
                    DB::table('bol_odr_dtl')->where('odr_id', $deleteId)
                        ->delete();
                    //->update([
                    //    'deleted'    => 1,
                    //    'deleted_at' => time()
                    //]);
                }
            }

            $carrierDtlParam = [
                'hdl_qty' => array_sum(array_column($input['items'], 'plts')), // sum(plts)
                'pkg_qty' => array_sum(array_column($input['items'], 'pkgs')),// sum(pkgs)
                'weight'  => array_sum(array_column($input['items'], 'weight')),
                'cmd_id'  => $input['cmd_des'],
            ];

            $this->bolCarrierDtlModel->updateWhere($carrierDtlParam, ['ship_id' => $shipId]);

            // order flow shipping
            if ($input['ship_sts'] == 'FN') {
                $ordIds = array_column($input['items'], 'odr_id');
                $this->shippedFlow($ordIds, $request);

            }

            DB::commit();

            //push MQ receiving
            if ($this->customerConfigModel->checkWhere([
                'whs_id'       => $shipment->whs_id,
                'cus_id'       => $shipment->cus_id,
                'config_name'  => config('constants.cus_config_name.EDI_INTEGRATION'),
                'config_value' => config('constants.cus_config_value.EDI858'),
                'ac'           => config('constants.cus_config_active.YES')
            ])
            ) {

                $reportDetail = $this->reportModel->shipment($shipment->ship_id);

                if ($reportDetail) {
                    $this->reportService->init($reportDetail);
                    $this->reportService->process();
                }

            }

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'ship_id' => $shipId
                ]
            ])->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            \DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            \DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $shipId
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function printBOL($shipId, Request $request)
    {
        $input = $request->getParsedBody();

        if (empty($input['cus_id'])) {
            throw new \Exception(Message::get('BM088', 'Customer'));
        }

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);
        $cusIds = array_column(array_get($userInfo, 'user_customers', []), 'cus_id');

        if (empty($whsId) || !in_array($input['cus_id'], $cusIds)) {
            throw new \Exception(Message::get('BM081', 'Customer'));
        }

        if (!file_exists(storage_path("BOL/$whsId/{$input['cus_id']}"))) {
            mkdir(storage_path("BOL/$whsId/{$input['cus_id']}"), 0777, true);
        }

        $file = "BOL-$whsId-{$input['cus_id']}-$shipId.pdf";

        /*if (file_exists(storage_path("BOL/$whsId/{$input['cus_id']}/$file"))) {
            header('Content-type: application/pdf');
            header("Content-Disposition: attachment; filename=$file");
            readfile(storage_path("BOL/$whsId/{$input['cus_id']}/$file"));

            return $this->response->noContent()->setContent([
                'status' => 'OK',
            ])->setStatusCode(Response::HTTP_OK);
        }*/

        $shipment = $this->shipmentModel->getFirstWhere(['ship_id' => $shipId, 'cus_id' => $input['cus_id']], [
            'fromState',
            'toState',
            'bolOdrDtl',
            'bolCarrierDtl'
        ]);


        //Get Order Detail list
        $OrderDetailInfos = $this->shipmentModel->getOrderDetailInfos($shipId);
        if (empty($OrderDetailInfos)) {
            throw new \Exception(Message::get("The {0} is not existed!", "Order"));
        }
        $OrderInfos = $this->shipmentModel->getOrderInfos($shipId);


        if (!empty($shipment)) {
            $this->printData($shipment->toArray(), $OrderInfos, $OrderDetailInfos);
        }

        return $this->response->noContent();
    }

    /**
     * @param $shipment
     * @param $OrderInfos
     * @param $OrderDetailInfos
     */
    private function printData($shipment, $OrderInfos, $OrderDetailInfos)
    {
        $html = (string)view('printBOLTemplate', [
            'shipment'         => $shipment,
            'OrderInfos'       => $OrderInfos,
            'OrderDetailInfos' => $OrderDetailInfos

        ]);

        $pdf = new \Mpdf\Mpdf(['utf-8', 'A4']);

        $htmlOdrDTL = (string)view('BolLpnDetailOrderListPrintoutTemplate', [
            'OrderInfos'       => $OrderInfos,
            'OrderDetailInfos' => $OrderDetailInfos
        ]);

        // output the HTML content
        $pdf->writeHTML($html);
        $pdf->AddPage();
        $pdf->WriteHTML($htmlOdrDTL);

        //Close and output PDF document
        $dir = storage_path("BOL/{$shipment['whs_id']}/{$shipment['cus_id']}");
        $pdf->Output("$dir/BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf", 'F');
        $pdf->Output("BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf", 'D');
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function getAddress(Request $request)
    {
        $input = $request->getQueryParams();

        if ((empty($input['t']) || $input['t'] != 'l') && (empty($input['cus_id']) || !is_numeric($input['cus_id']))) {
            throw new \Exception(Message::get('BM088', 'Customer'));
        }

        if (empty($input['ship_ready']) || !in_array($input['ship_ready'], [1, 3, 7])) {
            $input['ship_ready'] = 'all';
        }

        $odrIds = [];
        if (!empty($input['odr_id'])) {
            $odrIds = explode(',', $input['odr_id']);
            foreach ($odrIds as $odrId) {
                if (!($this->orderHdrModel->checkWhere(['odr_id' => $odrId]))) {
                    throw new \Exception(Message::get("BM017", "Order: $odrId"));
                }
            }
        }
        $input['odr_id'] = $odrIds;

        $odrHdr = $this->orderHdrModel->getOrderAddress($input)->toArray();

        $data = [];
        if (!empty($odrHdr) && is_array($odrHdr)) {
            foreach ($odrHdr as $value) {
                $arrFullAddress = explode(', ', $value['full_address']);
                $data[] = [
                    "company"     => array_shift($arrFullAddress),
                    "key"         => $value['order_ids'],
                    "value"       => implode(', ', $arrFullAddress),
                    "carrier"     => $value['carrier'],
                    "ship_method" => $value['ship_method']
                ];
            }
        }

        return ['data' => $data];
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function getAddressV2(Request $request)
    {
        $input = $request->getQueryParams();

        if ((empty($input['t']) || $input['t'] != 'l') && (empty($input['cus_id']) || !is_numeric($input['cus_id']))) {
            throw new \Exception(Message::get('BM088', 'Customer'));
        }

        if (empty($input['ship_ready']) || !in_array($input['ship_ready'], [1, 3, 7])) {
            $input['ship_ready'] = 'all';
        }

        $odrIds = [];
        if (!empty($input['odr_id'])) {
            $odrIds = explode(',', $input['odr_id']);
            foreach ($odrIds as $odrId) {
                if (!($this->orderHdrModel->checkWhere(['odr_id' => $odrId]))) {
                    throw new \Exception(Message::get("BM017", "Order: $odrId"));
                }
            }
        }
        $input['odr_id'] = $odrIds;

        $odrHdr = $this->orderHdrModel->getOrderAddress($input)->toArray();

        $data = [];
        if (!empty($odrHdr) && is_array($odrHdr)) {
            foreach ($odrHdr as $value) {
                $arrFullAddress = explode(', ', $value['full_address']);
                $data[] = [
                    "company"     => array_shift($arrFullAddress),
                    "key"         => $value['order_ids'],
                    "value"       => implode(', ', $arrFullAddress),
                    "carrier"     => $value['carrier'],
                    "ship_method" => $value['ship_method']
                ];
            }
            $unsetIndex =[];
            for ($i = 0; $i < count($data); $i++) {
                $data[$i]['company'] = (array)$data[$i]['company'];
                for($j = $i + 1; $j < count($data); $j++){
                    if($data[$i]['value'] == $data[$j]['value']){
                        array_push($data[$i]['company'], $data[$j]['company']);
                        $data[$i]['key'] .= ',' . $data[$j]['key'];
                        $unsetIndex[] = $j;
                    }
                }
            }
            foreach ($unsetIndex as $index){
                unset($data[$index]);
            }
        }

        return ['data' => array_values($data)];
    }

    public function getBOLList(Request $request, BOLListTransformer $bolListTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $ship = $this->shipmentModel->getBOLList($input, [
                'customer',
                'odr_hdr'
            ], array_get($input, 'limit'));

            return $this->response->paginator($ship, $bolListTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param CommodityTransformer $commodityTransformer
     *
     * @return Response|void
     */
    public function getCommodityList(CommodityTransformer $commodityTransformer)
    {
        try {
            $commondityInfo = $this->commodityModel->all();

            return $this->response->collection($commondityInfo, $commodityTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function printInvoice($orderId, Request $request)
    {
        $input = $request->getQueryParams();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);
        $cusIds = array_column(array_get($userInfo, 'user_customers', []), 'cus_id');

        if (empty($input['cus_id'])) {
            throw new \Exception(Message::get('BM088', 'Customer'));
        }

        $shipment = $this->orderHdrModel->getFirstWhere([
            'odr_id' => $orderId, 'cus_id' => $input['cus_id'], 'whs_id' => $whsId
        ], [
            'shipment',
            'customer',
            'customer.customerContact',
            'customer.customerAddress',
            'customer.customerAddress.systemState',
            'customer.customerAddress.systemCountry',
            'warehouse',
            'warehouse.warehouseContact',
            'warehouse.warehouseAddress',
            'details',
            'details.item',
            'orderSoldTo',
        ]);

        if (empty($shipment)) {
            return $this->response()->errorBadRequest('Order not found');
        }

        if (! in_array($shipment->odr_sts, ['PD', 'PN', 'PA', 'RS', 'SS', 'SH'])) {
            $statusCanPrintInvoice = 'Picked, Packing, Packed, Ready to Ship, Scheduled to Ship, Shipped';
            return $this->response()->errorBadRequest(sprintf('You can only print invoice with order in %s status', $statusCanPrintInvoice));
        }

        if (empty($whsId) || !in_array($input['cus_id'], $cusIds)) {
            throw new \Exception(Message::get('BM081', 'Customer'));
        }

        if (!file_exists(storage_path("BOL/$whsId/{$input['cus_id']}"))) {
            mkdir(storage_path("BOL/$whsId/{$input['cus_id']}"), 0777, true);
        }

        $cusShipAddr = DB::table('cus_address')->where('cus_add_cus_id', $input['cus_id'])->where('cus_add_type', 'ship')->first();
        $cusShipState = DB::table('system_state')->where('sys_state_id', $cusShipAddr['cus_add_state_id'])->where('deleted', 0)->first();
        $cusShipCountry = DB::table('system_country')->where('sys_country_id', $cusShipAddr['cus_add_country_id'])->where('deleted', 0)->first();
        $shipment->cus_ship_addr = $cusShipAddr;
        $shipment->cus_ship_state = $cusShipState;
        $shipment->cus_ship_country = $cusShipCountry;

        $whsShipAddr = DB::table('whs_address')->where('whs_add_whs_id', $whsId)->where('whs_add_type', 'SH')->where('deleted', 0)->first();
        $whsSysState = DB::table('system_state')->where('sys_state_id', $whsShipAddr['whs_add_state_id'])->where('deleted', 0)->first();
        $shipment->whs_ship_addr = $whsShipAddr;
        $shipment->whs_sys_state = $whsSysState;
//        $shipment = $this->orderHdrModel->getFirstWhere(['odr_id' => $shipId, 'cus_id' => $input['cus_id']], [
//            'fromState',
//            'toState',
//            'bolOdrDtl',
//            'bolCarrierDtl',
//            'odr_hdr',
//            'odr_hdr.details',
//            'customer',
//            'customer.customerAddress',
//            'customer.customerAddress.systemState',
//            'odr_hdr.details.item',
//            'warehouse',
//        ]);

        $cartonTotal = $weightTotal = 0;
        /*
        foreach ($shipment->details as &$detail) {
            $cartonTotal += $detail->picked_qty;
            $weightTotal += $detail->item->weight;
            $detailMetaValue = DB::table('odr_dtl_meta')->where('qualifier', 945)->where('odr_dtl_id', $detail->odr_dtl_id)->value('value');
            $detailMetaValue = json_decode($detailMetaValue, true);
            $uom = DB::table('system_uom')->where('sys_uom_id', $detail->uom_id)->value('sys_uom_name');
            if (!empty($detailMetaValue)) {
                if ($detailMetaValue['sku'] == $detail->sku) {
                    $detail->price = $detailMetaValue['price'] ?? 0;
                    $detail->uom = $uom;
                }
            } else {
                $detail->price = 0;
                $detail->uom = 'NA';
            }
        }
        */
        foreach ($shipment->details as &$detail) {
            $cartonTotal += $detail->picked_qty;
            $weightTotal += $detail->item->weight;
            $uom = DB::table('system_uom')->where('sys_uom_id', $detail->uom_id)->value('sys_uom_name');
            $detail->uom = $uom ?? 'NA';
            $orderDetail = DB::table('odr_dtl')->where('deleted', 0)->where('odr_dtl_id', $detail->odr_dtl_id)->first();
            $detail->price = array_get($orderDetail, 'price', 0);
            $detail->hs_num = array_get($orderDetail, 'hs_num');
            $detail->hs_unit = array_get($orderDetail, 'hs_unit');
        }

        $shipment->ctn_qty_ttl = $cartonTotal;
        $shipment->weight_ttl = $weightTotal;

        if (!empty($shipment)) {
            $this->printInvoicePdf($shipment->toArray());
        }

        return $this->response->noContent();
    }

    private function printInvoicePdf($shipment)
    {
        $html = (string)view('printInvoiceTemplate', ['shipment' => $shipment])->render();

        $pdf = new \Mpdf\Mpdf(['utf-8', 'A4']);

        // output the HTML content
        $pdf->writeHTML($html);

        //Close and output PDF document
        $pdf->Output("BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf", 'D');
    }

}
