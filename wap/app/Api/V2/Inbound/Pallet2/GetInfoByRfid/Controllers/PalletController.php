<?php

namespace App\Api\V2\Inbound\Pallet2\GetInfoByRfid\Controllers;

use App\Api\V2\Inbound\Pallet2\GetInfoByRfid\Models\PalletModel;
use App\Api\V2\Inbound\Pallet2\GetInfoByRfid\Models\CartonModel;
use App\Api\V2\Inbound\Pallet2\GetInfoByRfid\Models\CustomerModel;
use App\libraries\RFIDValidate;
use Dingo\Api\Http\Response as Response;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use App\Api\V2\Inbound\Pallet2\GetInfoByRfid\Models\Log;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Support\Facades\DB;

class PalletController extends AbstractController
{
    /**
     * @var $palletModel
     */
    protected $palletModel;

    /**
     * @var $cartonModel
     */
    protected $cartonModel;


    /**
     * @var $customerModel
     */
    protected $customerModel;

    public function __construct()
    {
        $this->palletModel     = new PalletModel();
        $this->cartonModel     = new CartonModel();
        $this->customerModel   = new CustomerModel();
    }

    public function getInfoPallet($whsId, $rfid, Request $request)
    {
        // WAP-701 - [Inbound - Relocate] Add a new screen “Relocate”
        $owner = $transaction = '';
        $url = "/whs/{$whsId}/ib/pallet/info/{$rfid}";
        Log:: info($request, $whsId, [
            'evt_code'     => 'ACC',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Get Pallet Information By Pallet rfid'
        ]);

        // get data from HTTP
        $input = $request->getQueryParams();
        $input['rfid'] = $rfid;

        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        try {
            $itemPallet = $this->palletModel->getFirstWhere([
                'rfid'    => $rfid
            ]);

            if (!$itemPallet) {
                $msg = "Pallet does not exist.";
                return $this->_responseErrorMessage($msg);
            }
            if ($itemPallet && $itemPallet->whs_id != $whsId) {
                $itemPallet = $this->palletModel->getFirstWhere([
                    'rfid'    => $rfid
                ]);
                if ($itemPallet) {
                    $msg = "Pallet does not belong to current warehouse.";
                    return $this->_responseErrorMessage($msg);
                }
            }

            $itemId = object_get($itemPallet, 'item_id');
            $sku    = object_get($itemPallet, 'sku');
            $size   = object_get($itemPallet, 'size');
            $color  = object_get($itemPallet, 'color');
            $pack   = object_get($itemPallet, 'pack');
            $cartonTotal = object_get($itemPallet, 'ctn_ttl', 0);
            $pltId  = $itemPallet->plt_id;
            if (!$itemId) {
                $primarySku = DB::table('cartons')
                    ->select([
                        DB::raw('COUNT(*) AS total'),
                        'item_id',
                        'sku',
                        'size',
                        'color',
                        DB::raw('ctn_pack_size AS pack'),
                    ])
                    ->where('plt_id', $pltId)
                    ->where('whs_id', $whsId)
                    ->where('deleted', 0)
                    ->groupBy('item_id')
                    ->orderBy(DB::raw('total'), 'DESC')
                    ->first();

                if (!count($primarySku)) {
                    $msg = "Pallet does not contain any cartons.";
                    return $this->_responseErrorMessage($msg);
                }

                $itemId = array_get($primarySku, 'item_id');
                $sku    = array_get($primarySku, 'sku');
                $size   = array_get($primarySku, 'size');
                $color  = array_get($primarySku, 'color');
                $pack   = array_get($primarySku, 'pack');
            }
            // $cartonTotal = $this->cartonModel->getModel()
            //     ->where('plt_id', $pltId)
            //     ->where('whs_id', $whsId)
            //     ->count();

            $itemCustomer = $this->customerModel->getFirstBy('cus_id', $itemPallet->cus_id);

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => 'Successfully!'
                    ]
                ],
                'data' => [
                    [
                        'customer' => [
                            'cus_name' => $itemCustomer->cus_name,
                            'cus_id'   => $itemCustomer->cus_id
                        ],
                        'pallet' => [
                            'plt_id'   => $itemPallet->plt_id,
                            'plt_rfid' => $itemPallet->rfid,
                            'plt_num'  => $itemPallet->plt_num,
                            'loc_name' => $itemPallet->loc_name,
                            'plt_sts'  => $itemPallet->plt_sts
                        ],
                        'primary_sku' => [
                            'item_id' => $itemId,
                            'sku'     => $sku,
                            'size'    => $size,
                            'color'   => $color,
                            'pack'    => $pack,
                        ],
                        'carton_total' => $cartonTotal
                    ]
                ]
            ];

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($this->getResponseData());
        }
    }

    private function _validateParams($attributes, $whsId)
    {
        $rfid = array_get($attributes, 'rfid');

        $names = [
            'rfid' => 'Pallet rfid',
        ];

        // Check Required
        $requireds = [
            'rfid' => $rfid,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet number is string
        if (!is_string($rfid)) {
            $msg = "The Pallet Rfid must be string type.";
            return $this->_responseErrorMessage($msg);
        }

        $rfidValidate = new RFIDValidate($rfid, 'pallet', $whsId);
        if (! $rfidValidate->validate()) {
            return $this->_responseErrorMessage($rfidValidate->error);
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}
