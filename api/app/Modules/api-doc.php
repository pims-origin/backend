<?php

/**
 * @OA\Info(
 *     version="1.0",
 *     title="WMS Seldat API"
 * )
 */

/**
 * @OA\Server(
 *      url="{schema}://apigw2.seldatdirect.com/qc/sip/api/core/api/rest/wms360/seldat/v1",
 *      description="Protocol parameters",
 *      @OA\ServerVariable(
 *          serverVariable="schema",
 *          enum={"https", "http"},
 *          default="https"
 *      )
 * )
 */

/**
 * @OA\SecurityScheme(
 *     securityScheme="token",
 *     type="apiKey",
 *     in="header",
 *     name="Authorization"
 * )
 */

/**
    @OA\Tag(
        name="Authenticate",
        description="Get your token to access api"
    ),
    @OA\Tag(
        name="Warehouse",
        description="Information about your warehouse"
    ),
    @OA\Tag(
        name="Customer",
        description="Information about your customer"
    ),
    @OA\Tag(
        name="Item",
        description="Information about your item"
    ),
    @OA\Tag(
        name="Order",
        description="Information about your order"
    )
*/

/**
    @OA\Schema(
        schema="warehouse",
        description="Warehouse information",
        title="Warehouse",
        @OA\Property(property="whs_id", type="integer"),
        @OA\Property(property="code", type="string"),
        @OA\Property(property="name", type="string")
    ),

    @OA\Schema(
        schema="address",
        description="Shipping address or billing address",
        title="Address",
        @OA\Property(property="address_type", type="string"),
        @OA\Property(property="address_line_1", type="string"),
        @OA\Property(property="address_line_2", type="string"),
        @OA\Property(property="city", type="string"),
        @OA\Property(property="state", type="string"),
        @OA\Property(property="zip", type="integer"),
        @OA\Property(property="country", type="string")
    ),

    @OA\Schema(
        schema="contact",
        description="Contact information",
        title="Contact",
        @OA\Property(property="first_name", type="string"),
        @OA\Property(property="last_name", type="string"),
        @OA\Property(property="position", type="string"),
        @OA\Property(property="email", type="string"),
        @OA\Property(property="phone", type="integer"),
        @OA\Property(property="extension", type="integer"),
        @OA\Property(property="mobile", type="string")
    ),

    @OA\Schema(
        schema="warehouse_detail",
        description="Detail of a warehouse",
        title="Warehouse Detail",
        @OA\Property(property="whs_id", type="integer"),
        @OA\Property(property="code", type="string"),
        @OA\Property(property="name", type="string"),
        @OA\Property(property="addresses", type="array", @OA\Items(ref="#/components/schemas/address")),
        @OA\Property(property="contacts", type="array", @OA\Items(ref="#/components/schemas/contact"))
    ),

    @OA\Schema(
        schema="customer",
        description="Customer information",
        title="Customer",
        @OA\Property(property="cus_id", type="integer"),
        @OA\Property(property="code", type="string"),
        @OA\Property(property="name", type="string"),
        @OA\Property(property="description", type="string")
    ),

    @OA\Schema(
        schema="courier",
        description="Courier information",
        title="Courier",
        @OA\Property(property="name", type="string"),
        @OA\Property(property="number", type="integer"),
        @OA\Property(property="type", type="string")
    ),

    @OA\Schema(
        schema="customer_detail",
        description="Customer Detail",
        title="Customer Detail",
        @OA\Property(property="cus_id", type="integer"),
        @OA\Property(property="code", type="string"),
        @OA\Property(property="name", type="string"),
        @OA\Property(property="description", type="string"),
        @OA\Property(property="addresses", type="array", @OA\Items(ref="#/components/schemas/address")),
        @OA\Property(property="contacts", type="array", @OA\Items(ref="#/components/schemas/contact")),
        @OA\Property(property="warehouses", type="array", @OA\Items(ref="#/components/schemas/warehouse")),
        @OA\Property(property="picking_algorithm", type="string", enum={"FIFO", "LIFO", "FEFO"}),
        @OA\Property(property="couriers", type="array", @OA\Items(ref="#/components/schemas/courier")),
    ),

    @OA\Schema(
        schema="item",
        description="Item List",
        title="Item List",
        @OA\Property(property="item_id", type="integer"),
        @OA\Property(property="cus_id", type="string"),
        @OA\Property(property="sku", type="string"),
        @OA\Property(property="size", type="string"),
        @OA\Property(property="color", type="string"),
        @OA\Property(property="description", type="string"),
        @OA\Property(property="uom", type="string"),
        @OA\Property(property="pack", type="string"),
        @OA\Property(property="upc", type="string"),
        @OA\Property(property="length", type="string"),
        @OA\Property(property="width", type="string"),
        @OA\Property(property="height", type="string"),
        @OA\Property(property="dim_uom", type="string"),
        @OA\Property(property="net_weight", type="string"),
        @OA\Property(property="gross_weight", type="string"),
        @OA\Property(property="weight_uom", type="string"),
        @OA\Property(property="category", type="string")
    ),

    @OA\Schema(
        schema="body_insert_order_contact",
        @OA\Property(property="contact_type", type="string", example="Receiver"),
        @OA\Property(property="name", type="string", example="AVRIO LOGISTICS"),
        @OA\Property(property="first_name", type="string", example="AVRIO"),
        @OA\Property(property="last_name", type="string", example="LOGISTICS"),
        @OA\Property(property="phone", type="string", example="0901456321"),
        @OA\Property(property="extension", type="string", example="test extension"),
        @OA\Property(property="mobile", type="string", example="0901456321"),
        @OA\Property(property="email", type="string", example="tuyen.tran@seldatinc.com"),
    ),
    @OA\Schema(
        schema="body_insert_order_address",
        @OA\Property(property="address_type", type="string", example="ShipTo"),
        @OA\Property(property="address_line_1", type="string", example="549 MILL ROAD"),
        @OA\Property(property="address_line_2", type="string", example="549 MILL ROAD"),
        @OA\Property(property="city", type="string", example="EDISON"),
        @OA\Property(property="country", type="string", example="US"),
        @OA\Property(property="state", type="string", example="NJ"),
        @OA\Property(property="mobile", type="string", example="0901456321"),
        @OA\Property(property="zip", type="string", example="08837"),
    ),
    @OA\Schema(
        schema="body_insert_order_item",
        @OA\Property(property="sku", type="string", example="XANG-20181108-004"),
        @OA\Property(property="size", type="string", example="NA"),
        @OA\Property(property="color", type="string", example="NA"),
        @OA\Property(property="lot", type="string", example="NA"),
        @OA\Property(property="description", type="string", example="CORNETTO OREO ANZ MP4 1X440ML 12345"),
        @OA\Property(property="qty", type="string", example="50"),
        @OA\Property(property="uom", type="string", example="PC"),
        @OA\Property(property="pack", type="string", example="10"),
        @OA\Property(property="tracking_number", type="string", example="T00001"),
    ),
    @OA\Schema(
        schema="body_insert_order",
        description="Insert Order",
        title="Insert Order",
        @OA\Property(property="order_type", type="string", enum={"Bulk", "Retail", "Ecom"}, example="Bulk"),
        @OA\Property(property="order_number", type="string", example="T00001"),
        @OA\Property(property="po_num", type="string", example="T00001"),
        @OA\Property(property="order_ref", type="string", example="181116"),
        @OA\Property(property="rush_priority", type="integer", enum={1,2,3,4,5}, example=1),
        @OA\Property(property="order_date", type="string", example="2018-11-16"),
        @OA\Property(property="ship_date", type="string", example="2018-11-16"),
        @OA\Property(property="additional_information", type="string", example="something note to add for this order"),
        @OA\Property(property="special_instruction", type="string", example="intruction information"),
        @OA\Property(property="carrier", type="string", example="John"),
        @OA\Property(property="delivery_service", type="string", example=""),
        @OA\Property(property="consignee_signature", type="string", example=""),
        @OA\Property(property="pick_ref", type="string", example="test pick ref"),
        @OA\Property(property="dept_ref", type="string", example="test dept ref"),
        @OA\Property(property="items", type="array", @OA\Items(ref="#/components/schemas/body_insert_order_item")),
        @OA\Property(property="contacts", type="array", @OA\Items(ref="#/components/schemas/body_insert_order_contact")),
        @OA\Property(property="addresses", type="array", @OA\Items(ref="#/components/schemas/body_insert_order_address")),
    ),

    @OA\Schema(
        schema="error",
        @OA\Property(property="code", type="integer"),
        @OA\Property(property="message", type="string")
    ),
*/




/**
 * @OA\Post(
 *     path="/authenticate",
 *     tags={"Authenticate"},
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="username",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="password",
 *                     type="string"
 *                 ),
 *                 example={"username": "citlph", "password": "Seldat@123"}
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Return Token"
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/warehouses",
 *     tags={"Warehouse"},
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/warehouse")
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Get(
 *     path="/warehouses/{whs_id}",
 *     tags={"Warehouse"},
 *     @OA\Parameter(
 *         name="whs_id",
 *         in="path",
 *         description="ID of warehouse",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\JsonContent(
 *             ref="#/components/schemas/warehouse_detail"
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Get(
 *     path="/warehouses/{whs_id}/customers",
 *     tags={"Customer"},
 *     @OA\Parameter(
 *         name="whs_id",
 *         in="path",
 *         description="ID of warehouse",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\JsonContent(
  *             type="array",
  *             @OA\Items(ref="#/components/schemas/customer")
  *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Get(
 *     path="/customers/{cus_id}",
 *     tags={"Customer"},
 *     @OA\Parameter(
 *         name="cus_id",
 *         in="path",
 *         description="ID of customer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\JsonContent(
 *             ref="#/components/schemas/customer_detail"
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Get(
 *     path="/customers/{cus_id}/items",
 *     tags={"Item"},
 *     @OA\Parameter(
 *         name="cus_id",
 *         in="path",
 *         description="ID of customer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/item")
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Get(
 *     path="/customers/{cus_id}/items/{item_id}",
 *     tags={"Item"},
 *     @OA\Parameter(
 *         name="cus_id",
 *         in="path",
 *         description="ID of customer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="item_id",
 *         in="path",
 *         description="ID of item",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\JsonContent(
 *             ref="#/components/schemas/item"
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Post(
 *     path="/warehouses/{whs_id}/customers/{cus_id}/orders",
 *     tags={"Order"},
 *     @OA\Parameter(
 *         name="whs_id",
 *         in="path",
 *         description="ID of warehouse",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cus_id",
 *         in="path",
 *         description="ID of customer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\RequestBody(
 *         description="Order information",
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(ref="#/components/schemas/body_insert_order")
 *         )
 *     ),
 *     @OA\Response(
 *         response=201,
 *         description="",
 *         @OA\JsonContent(
 *             example={"msg" : "Insert order successfully"}
 *         )
 *     ),
 *     @OA\RequestBody(
 *          request="UserArray",
 *          description="List of warehouse object",
 *          required=true,
 *
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Get(
 *     path="/warehouses/{whs_id}/customers/{cus_id}/orders",
 *     tags={"Order"},
 *     @OA\Parameter(
 *         name="cus_id",
 *         in="path",
 *         description="ID of customer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="whs_id",
 *         in="path",
 *         description="ID of warehouse",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Get(
 *     path="/warehouses/{whs_id}/customers/{cus_id}/orders/{order_id}",
 *     tags={"Order"},
 *     @OA\Parameter(
 *         name="cus_id",
 *         in="path",
 *         description="ID of customer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="whs_id",
 *         in="path",
 *         description="ID of warehouse",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="order_id",
 *         in="path",
 *         description="ID of order_hdr",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=465
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Patch(
 *     path="/warehouses/{whs_id}/customers/{cus_id}/orders/{order_id}/cancel",
 *     tags={"Order"},
 *     @OA\Parameter(
 *         name="cus_id",
 *         in="path",
 *         description="ID of customer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="whs_id",
 *         in="path",
 *         description="ID of warehouse",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="order_id",
 *         in="path",
 *         description="ID of order_hdr",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=465
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */

/**
 * @OA\Get(
 *     path="/warehouses/{whs_id}/customers/{cus_id}/asns",
 *     tags={"Asn"},
 *     @OA\Parameter(
 *         name="cus_id",
 *         in="path",
 *         description="ID of customer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="whs_id",
 *         in="path",
 *         description="ID of warehouse",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             default=1
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Bad Request"
 *     ),
 *     security={
 *        {"token": {}}
 *     }
 * )
 */