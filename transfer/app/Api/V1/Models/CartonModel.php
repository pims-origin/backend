<?php

namespace App\Api\V1\Models;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Item;
use Wms2\UserInfo\Data;


class CartonModel extends AbstractModel
{
    protected $model;

    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    /**
     * @param $sku
     * @param $cus_id
     *
     * @return mixed
     */
    public function checkSkuBelongToCustomerAndWH($sku, $cus_id)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table('cartons')
            ->leftJoin('item', 'cartons.item_id', '=', 'item.item_id')
            ->where('cartons.cus_id', $cus_id)
            ->where('cartons.whs_id', $currentWH)
            ->where('item.sku', $sku)
            ->count();
    }

    /**
     * @param $item_id
     * @param $cus_id
     *
     * @return mixed
     */
    public function checkItemIdBelongToCustomerAndWH($item_id, $cus_id)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table('cartons')
            ->where('cartons.cus_id', $cus_id)
            ->where('cartons.whs_id', $currentWH)
            ->where('cartons.item_id', $item_id)
            ->count();
    }

    /**
     * @param $cus_upc
     * @param $cus_id
     *
     * @return mixed
     */
    public function checkCusUpcBelongToCustomerAndWH($cus_upc, $cus_id)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table('cartons')
            ->leftJoin('item', 'cartons.item_id', '=', 'item.item_id')
            ->where('cartons.cus_id', $cus_id)
            ->where('cartons.whs_id', $currentWH)
            ->where('item.cus_upc', $cus_upc)
            ->count();
    }

    /**
     * @param $locId
     */
    public function getCartonByLocId($locId, $cusId)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table('cartons')
            ->where('cartons.loc_id', $locId)
            ->where('cartons.cus_id', $cusId)
            ->where('cartons.whs_id', $currentWH)
            ->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function checkLocationAlreadyHasItem($data, $check)
    {
        $itemObj = Item::where('item_id', $data['item_id'])->first();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        if ($check == "create") {
            $type = "<>";
        } else {
            $type = "=";
        }

        return DB::table('cartons')
            ->where('cartons.loc_id', $type, $data['loc_id'])
            ->where('cartons.whs_id', $currentWH)
            ->where([
                'cartons.sku'    => $itemObj['sku'],
                'cartons.cus_id' => $itemObj['cus_id'],
                'cartons.size'   => $itemObj['size'],
                'cartons.color'  => $itemObj['color'],
                'cartons.lot'    => 'ECO'
            ])
            ->where('is_ecom', 1)
            ->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function checkLocationHasOtherItemWhenUpdate($data)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table('cartons')
            ->where('cartons.loc_id', $data['loc_id'])
            ->where('cartons.cus_id', $data['cus_id'])
            ->where('cartons.whs_id', $currentWH)
            ->where('cartons.item_id', "<>", $data['item_id'])
            ->count();
    }

    /**
     * @param $itemId
     * @param $locId
     * @param $cusId
     *
     * @return int
     */
    public function getAvailQty($itemId, $locId, $cusId)
    {
        $query = DB::table('cartons')
            ->where('item_id', $itemId)
            ->where('loc_id', $locId)
            ->where('cus_id', $cusId)
            ->where('deleted', 0)
            ->whereRaw('(is_ecom is null or is_ecom = 0)');

        return $query->sum('piece_remain') === null ? 0 : $query->sum('piece_remain');
    }
}
