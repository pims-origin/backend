<?php

namespace Seldat\Wms2\Models;

/**
 * Class ReturnDtl
 *
 * @package Seldat\Wms2\Models
 */
class ReturnDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'return_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'return_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'return_id',
        'return_num',
        'odr_hdr_num',
        'odr_hdr_id',
        'odr_dtl_id',
        'ctn_ttl',
        'original_ctns',
        'new_ctns',
        'piece_qty',
        'return_dtl_sts',
        'sku',
        'size',
        'color',
        'lot',
        'sts',
        'item_id',
        'pack',
        'uom_id',
        'uom_code',
        'uom_name'
    ];

    public function returnHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\ReturnHdr', 'return_id', 'return_id');
    }

    public function odrHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_hdr_id');
    }

    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }

    public function orderDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\OrderDtl', 'odr_dtl_id', 'odr_dtl_id');
    }

    /**
     * @return Created item
     */
    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    /**
     * @return Created user
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

}
