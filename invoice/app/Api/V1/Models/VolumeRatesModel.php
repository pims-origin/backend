<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\InvoiceVolumeRates;
use App\Invoice;

use Illuminate\Support\Facades\DB;

class VolumeRatesModel extends AbstractModel
{
    protected $volumeRates;
    /**
     * InvoiceVolumeRates constructor.
     * @param InvoiceVolumeRates|null $volumeRates
     */
    
    public function __construct(InvoiceVolumeRates $volumeRates = null)
    {
        $this->model = ($volumeRates) ?: new InvoiceVolumeRates();
    }
 
    
    /**
    * @param string type
    * 
    * @return array
    */
    
    public function volumeRates($whsID, $model, $type)
    {
        if (! getDefault($model->customerUOMs[$type][Invoice::$smallCartonImperial])
                && ! getDefault($model->customerUOMs[$type][Invoice::$smallCartonMetric])
        ) {
             return;
        }
        
        if (! $model->volRates) {
            // Get the volume rates
            
            $query = DB::table('inv_vol_rates AS iv')
                                ->join('charge_code AS cc', 'cc.chg_code_id', '=', 'iv.chg_code_id')
                                ->join('charge_type AS ch', 'ch.chg_type_id', '=', 'cc.chg_type_id')
                                ->join('system_uom AS su', 'su.sys_uom_id', '=', 'cc.chg_uom_id')
                                ->join('invoice_cost AS ic', function($join) {
                                    $join->on('ic.chg_code_id', '=', 'iv.chg_code_id');
                                    $join->on('ic.cus_id', '=', 'iv.cus_id');
                                    $join->on('ic.whs_id', '=', 'iv.whs_id');
                                })
                                ->select('iv.cus_id', 'iv.whs_id', 
                                         'su.sys_uom_name as uom', 'cc.chg_code',
                                         'ch.chg_type_name', 'ic.chg_cd_price', 
                                         'iv.min_vol', 'iv.max_vol'
                                        )
                                ->where('ic.deleted', 0);
                          
            $volumeResults = $whsID ? $query->where('iv.whs_id', $whsID)->get() : $query->get();

            foreach ($volumeResults as $row) {
                $uom = $row->uom;
                $custID = $row->cus_id;
                $category = $row->chg_type_name;
                $model->volRates[$category][$custID][$uom] = $row;
            }
        }

        return $model;
    }
    
    /**
    * @param string category
    * @param int custID
    * @return array
    */
    
    static function custVolumeRates($category, $model, $custID)
    {
        return getDefault($model->volRates[$category][$custID], []);
    }
    
    /**
    * @param string type volField custField
    * @param array params
    * 
    * @return array
    */
    
    public function volClause($inv, $params, $custField='cus_id')
    {
        $model = $params['passed']['model'];
        $uom = $params['passed']['uom'];
        $custs = $params['passed']['custs'];
        $type = $params['type'];
        $volField = $params['volField'];

        $volRates = getDefault($model->volRates[$type]);
      
        if (! $volRates) {
            return NULL;
        }

        $val = [
            'cusField' => $custField,
            'vol' => $volField,
            'uom' => $uom,
            'rates' => $volRates
        ];
        
        $query = $inv->where(function($query) use($val,$custs) {
            $uom = $val['uom'];
            $volRates = $val['rates'];

            foreach ($custs as $custID) {

                if (! isset($volRates[$custID][$uom])) {
                    continue;
                }

                $max = getDefault($volRates[$custID][$uom]->max_vol);

                $query = $query->orWhere(function($query) use($val, $custID, $max) {
                    $uom = $val['uom'];

                    $query->where($val['cusField'], $custID);

                    $query->where($val['vol'], '>', $val['rates'][$custID][$uom]->min_vol);

                    $query = floatVal($max) ? $query->where($val['vol'], '<=', $max) :
                                       $query;
               });
            }
        });
       
        return $query;
    }

}
