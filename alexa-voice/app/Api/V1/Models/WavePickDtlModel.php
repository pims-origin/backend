<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Status;

class WavePickDtlModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function loadData($wv_id)
    {
        return $this->model->select([
            'item_id',
            'sku',
            'color',
            'size',
            'lot',
            'pack_size',
            'ctn_qty',
            'piece_qty',
            'cus_id',
            'wv_dtl_id',
            'wv_dtl_sts'
        ])
            ->where('wv_id', '=', $wv_id)
            ->where('piece_qty', '>', 0)
            ->distinct()
            ->get();
    }

    public function updateWvDtlByWvId($wvId)
    {
        return $this->model
            ->where('wv_id', $wvId)
            ->update([
            'wv_dtl_sts' => 'PK'
        ]);
    }

    public function getPickingWvDtlByWvId($wvId)
    {
        return $this->model
            ->where('wv_id', $wvId)
            ->where('wv_dtl_sts', 'PK')
            ->first();
    }
}
