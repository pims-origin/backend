<?php

namespace App\Api\V1\Models;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    const REP_LIMIT = 1;

    /**
     * @param EventTracking $model
     */
    public function __construct(EventTracking $model)
    {
        $this->model = $model;
    }

    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);

        if (isset($attributes['evt_code'])) {
            $query->where('evt_code', 'like', "%" . SelStr::escapeLike($attributes['evt_code']) . "%");
        }

        if (isset($attributes['owner'])) {
            $query->where('owner', 'like', "%" . SelStr::escapeLike($attributes['owner']) . "%");
        }

        if (isset($attributes['trans_num'])) {
            $query->where('trans_num', 'like', "%" . SelStr::escapeLike($attributes['trans_num']) . "%");
        }

        //search according to from date to date
        if (!empty($attributes['created_at_from'])) {
            $fromDate = strtotime($attributes['created_at_from']);
            if ($fromDate) {
                $query->where('created_at', '>=', $fromDate);
            }
        }

        if (!empty($attributes['created_at_to'])) {
            $toDate = strtotime($attributes['created_at_to'] . " 23:59:59");
            if ($toDate) {
                $query->where('created_at', '<=', $toDate);
            }
        }

        //search according to indicator
        if (isset($attributes['indicator']) && !empty($attributes['indicator'])) {
            $query->Join('indicator as idc', 'idc.evt_code', '=', 'evt_tracking.evt_code')
                ->where('ind_group', SelStr::escapeLike($attributes['indicator']));
        }

        //Filter according to type = ib/ob/wo
        $query->whereHas('event', function ($query) use ($attributes) {
            //inbound process
            $trans_ib = [
                'ASN',
                'GRD',
                'ANW',
                'ARV',
                'ARP',
                'ARX',
                'ARC',
                'GRP',
                'GRX',
                'GRC',
                'PUT',
                'GRU',
                'UVC',
                'IVC',
                'WGC',
                'WAC'
            ];

            //outbound process
            $trans_ob = [
                'ORD',
                'ONW',
                'OWN',
                'OIP',
                'ORA',
                'OPA',
                'OPS',
                'OPC',
                'OAC',
                'OPK',
                'OPI',
                'WPN',
                'WCA',
                'WPC',
                'ORS',
                'OSH',
                'OBO',
                'OHD',
                'OCN',
                'OCE',
                'PAL',
                'PPK',
                'PIP',
                'PPA',
                'PSH',
                'PRC',
                'BNW',
                'WOP',
                'WOH',
            ];
            //warehouse operating
            $trans_wo = [
                'RLC',
                'CNC',
                'WXF',
                'CTL',
                'LPL',
                'RLC',
                'CTL',
                'CTP',
                'CNC',
                'XFC',
                'XFP',
                'XFT',
                'XFX',
                'XFE',
                'WXF',
            ];

            if (isset($attributes['type'])) {
                $type = $attributes['type'];
                switch ($type) {
                    case config('constants.INBOUND'):
                        $query->whereIn('trans', $trans_ib);
                        break;
                    case config('constants.OUTBOUND'):
                        $query->whereIn('trans', $trans_ob);
                        break;
                    case config('constants.WAREHOUSE-OPERATING'):
                        $query->whereIn('trans', $trans_wo);
                        break;
                    default:
                        $query->whereIn('trans', $trans_ib);
                        break;
                }
            }
        });

        //$this->model->filterData($query, true);
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);

    }

    public function evtReports($userId, $attributes, array $with, $limit)
    {
        //  Limit
        $limitDay = (!empty($attributes['day']) && is_numeric($attributes['day']))
            ? $attributes['day'] : self::REP_LIMIT;

        if (!isset($attributes['sort']['created_at'])) {
            $attributes['sort']['created_at'] = 'desc';
        }

        $query = $this->make($with);

        $query->where('created_by', $userId);
        $date = "CONCAT(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(NOW() - INTERVAL $limitDay DAY)), '%Y-%m-%d'), ' 00:00:00')";
        $query->where('created_at', '>=', DB::raw("UNIX_TIMESTAMP($date)"));

        $this->model->filterData($query);

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);

    }

    public function performance($userId, $attributes)
    {
        //  Limit
        $limitDay = (!empty($attributes['day']) && is_numeric($attributes['day']))
            ? $attributes['day'] : self::REP_LIMIT;

        $warehouseId = (!empty($attributes['warehouse']) && is_numeric($attributes['warehouse']))
            ? $attributes['warehouse'] : null;

        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $raw = 'SUM((SELECT COUNT(1) FROM evt_tracking WHERE evt_tracking.`evt_code` = ind.`evt_code`
                and evt_tracking.`whs_id`= ' . intval($warehouseId) . '
                and evt_tracking.created_at >= UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limitDay . ' DAY))) as task';

        $query = DB::table('indicator as ind')
            ->select(
                'ind.ind_group',
                DB::raw($raw))
            ->groupBy('ind.ind_group')
            ->orderBy('ind.ind_group');

        // Task
        $tasks = $query->get();


        $rawUser = 'SUM((SELECT COUNT(1) FROM evt_tracking WHERE evt_tracking.`evt_code` = ind.`evt_code`
                and evt_tracking.`created_by`= ' . $userId . '
                and evt_tracking.`whs_id`= ' . intval($warehouseId) . '
                and evt_tracking.created_at >= UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limitDay . ' DAY))) as task';
        $query = DB::table('indicator as ind')
            ->select(
                'ind.ind_group',
                DB::raw($rawUser))
            ->groupBy('ind.ind_group')
            ->orderBy('ind.ind_group');
        // For User
        $userTasks = $query->get();

        $taskForUser = [];

        foreach ($userTasks as $userTask) {
            $taskForUser[$userTask['ind_group']] = $userTask['task'];
        }

        $performance = [];
        $results = [];

        foreach ($tasks as $task) {
            $performance[$task['ind_group']]['indicator'] = $task['ind_group'];
            $performance[$task['ind_group']]['warehouse_task'] = $task['task'];
            $performance[$task['ind_group']]['user_task_amount'] =
                isset($taskForUser[$task['ind_group']]) ? $taskForUser[$task['ind_group']] : 0;
            $performance[$task['ind_group']]['user_task_percent'] =
                (isset($taskForUser[$task['ind_group']]) && !empty($task['task'])) ?
                    number_format($taskForUser[$task['ind_group']] / $task['task'] * 100, 2, '.', '') : 0;
            $results [] = $performance[$task['ind_group']];
        }

        $response = ['data' => $results];

        return $response;

    }
    public function getListUser($params, array $with, $limit)
    {
        $query = $this->make($with);
        $params = SelArr::removeNullOrEmptyString($params);

        $query->leftJoin('users', 'user_id', '=', 'created_by');
        $query->groupBy('created_by');

        $this->sortBuilder($query, $params);

        return $query->paginate($limit);
    }

    public function autocomplete($params, array $with, $limit)
    {
        $query = $this->make($with);
        $params = SelArr::removeNullOrEmptyString($params);

        if (isset($params['owner'])) {
            $query->select('owner');
            $query->where('owner', 'like', "%" . SelStr::escapeLike($params['owner']) . "%");
            $query->groupBy('owner');
        }
        $this->model->filterData($query, true);
        $this->sortBuilder($query, $params);
        return $query->paginate($limit);
    }
}
