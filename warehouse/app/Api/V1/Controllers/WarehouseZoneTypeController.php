<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\WarehouseZoneModel;
use App\Api\V1\Models\WarehouseZoneTypeModel;
use App\Api\V1\Transformers\WarehouseZoneTypeTransformer;
use App\Api\V1\Validators\DeleteMassZoneTypeValidator;
use App\Api\V1\Validators\WarehouseZoneTypeValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;

class WarehouseZoneTypeController extends AbstractController
{
    /*
     * @var $warehouseZoneTypeModel
     * */
    protected $warehouseZoneTypeModel;

    /*
     * @var $warehouseZoneTypeTransformer
     * */
    protected $warehouseZoneTypeTransformer;

    /*
     * @var $warehouseZoneTypeValidator
     * */
    protected $warehouseZoneTypeValidator;

    /*
     * @var $delMassZoneTypeValidator
     * */
    protected $delMassZoneTypeValidator;

    public function __construct()
    {
        parent::__construct();

        $this->warehouseZoneTypeModel = new WarehouseZoneTypeModel();
        $this->warehouseZoneTypeTransformer = new WarehouseZoneTypeTransformer();
        $this->warehouseZoneTypeValidator = new WarehouseZoneTypeValidator();
        $this->delMassZoneTypeValidator = new DeleteMassZoneTypeValidator();
    }

    public function index()
    {
        $warehouseZoneType = $this->warehouseZoneTypeModel->all();

        return $this->response->collection($warehouseZoneType, $this->warehouseZoneTypeTransformer);
    }

    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseZoneTypeValidator->validate($input);

        $params = [
            'zone_type_name' => $input['zone_type_name'],
            'zone_type_code' => $input['zone_type_code'],
            'zone_type_desc' => array_get($input, 'zone_type_desc', '')
        ];
        try {
            // check duplicate code
            if ($this->warehouseZoneTypeModel->getFirstBy('zone_type_code', $params['zone_type_code'])) {
                throw new \Exception(Message::get("BM006", "Zone Type Code"));
            }

            // check duplicate name
            if ($this->warehouseZoneTypeModel->getFirstBy('zone_type_name', $params['zone_type_name'])) {
                throw new \Exception(Message::get("BM006", "Zone Type Name"));
            }

            if ($warehouseZoneType = $this->warehouseZoneTypeModel->create($params)) {
                return $this->response->item($warehouseZoneType,
                    $this->warehouseZoneTypeTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function show($zoneTypeId)
    {
        $warehouseZoneType = $this->warehouseZoneTypeModel->getFirstBy('zone_type_id', $zoneTypeId);

        return $this->response->item($warehouseZoneType, $this->warehouseZoneTypeTransformer);
    }

    public function update(
        $zoneTypeId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseZoneTypeValidator->validate($input);

        $params = [
            'zone_type_id'   => $zoneTypeId,
            'zone_type_name' => $input['zone_type_name'],
            'zone_type_code' => $input['zone_type_code'],
            'zone_type_desc' => array_get($input, 'zone_type_desc', '')
        ];

        try {
            // check duplicate code
            if (($zoneType = $this->warehouseZoneTypeModel->getFirstBy('zone_type_code', $params['zone_type_code']))
                && $zoneType->zone_type_id != $zoneTypeId
            ) {

                throw new \Exception(Message::get("BM006", "Zone Type Code"));
            }

            // check duplicate name
            if (($zoneType = $this->warehouseZoneTypeModel->getFirstBy('zone_type_name', $params['zone_type_name']))
                && $zoneType->zone_type_id != $zoneTypeId
            ) {

                throw new \Exception(Message::get("BM006", "Zone type name"));
            }

            if ($warehouseZoneType = $this->warehouseZoneTypeModel->update($params)) {
                return $this->response->item($warehouseZoneType,
                    $this->warehouseZoneTypeTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function destroy($zoneTypeId)
    {
        try {
            // Check Zone Type exist in Zone
            $zoneModel = new WarehouseZoneModel();
            $zone = $zoneModel->loadBy(['zone_type_id' => $zoneTypeId])->first();
            if (!empty($zone)) {
                return $this->response
                    ->errorBadRequest(Message::get("BM002", "Zone Type"));
            }

            if ($this->warehouseZoneTypeModel->deleteById($zoneTypeId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM017", "Zone Type"));
    }

    public function search(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $warehouse = $this->warehouseZoneTypeModel
                ->search($input, [], array_get(
                        $input,
                        'limit',
                        config('constants.PAGING_LIMIT'))
                );

            return $this->response->paginator($warehouse, $this->warehouseZoneTypeTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->delMassZoneTypeValidator->validate($input);
        $input['zone_type_id'] = is_array($input['zone_type_id']) ? $input['zone_type_id'] : [$input['zone_type_id']];

        try {
            if (!empty($input['zone_type_id'])) {
                // Check Zone Type exist in Zone
                $zoneModel = new WarehouseZoneModel();
                $zone = $zoneModel->loadByMany(['zone_type_id' => $input['zone_type_id']])->first();
                if (!empty($zone)) {
                    return $this->response
                        ->errorBadRequest(Message::get("BM003", "Zone Type"));
                }
            }

            $this->warehouseZoneTypeModel->deleteMassZoneType($input['zone_type_id']);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }
}
