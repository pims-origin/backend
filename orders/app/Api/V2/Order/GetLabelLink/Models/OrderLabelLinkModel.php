<?php

namespace App\Api\V2\Order\GetLabelLink\Models;

use Seldat\Wms2\Models\OrderLabelLink;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;


/**
 * Class OutPalletModel
 *
 * @package App\Api\V1\Models
 */
class OrderLabelLinkModel extends AbstractModel
{
    /**
     * @param OrderLabelLink $model
     */
    public function __construct(OrderLabelLink $model = null)
    {
        $this->model = ($model) ?: new OrderLabelLink();
    }

    public function getLabelLink($whsId, $odrId, $limit = 20)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $query = $this->model
            ->where('whs_id', $whsId)
            ->where('odr_hdr_id', $odrId);

        return $query->paginate($limit);
    }

}
