<?php

namespace App\Api\V1\Validators;


class SystemCountryValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'sys_country_code' => 'required',
            'sys_country_name' => 'required'
        ];
    }
}
