<?php

namespace App\Api\Inventory\Models;

use App\Api\Inventory\Services\CycleCountService;
use Illuminate\Validation\Validator;
use Closure;
use phpDocumentor\Reflection\Location;
use phpDocumentor\Reflection\Types\Null_;
use Seldat\Wms2\Models\CycleDis;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class CycleDtlModel extends AbstractModel
{
    const STATUS_CYCLEDTL_NEW = 'NW';
    const STATUS_CYCLEDTL_OPEN = 'OP';
    const STATUS_CYCLEDTL_NOT_APPLICABLE = 'NA';
    const STATUS_CYCLEDTL_RECOUNT = 'RC';
    const STATUS_CYCLEDTL_ACCEPTED = 'AC';

    const STATUS_CYCLE_OPEN = 'OP';
    const STATUS_CYCLE_ASSIGNED = 'AS';
    const STATUS_CYCLE_CYCLED = 'CC';
    const STATUS_CYCLE_RECOUNT = 'RC';
    const STATUS_CYCLE_COMPLETED = 'CP';
    const STATUS_CYCLE_DELETED = 'DL';

    public $errors = [];
    private $cycleHdr;
    private $cycleDtls;

    /**
     * @var
     */
    protected $arrNumberFields = [
        'cycle_hdr_id',
        'cycle_dtl_id'
    ];

    /**
     * @var
     */
    protected $arrStringFields = [
        'sku',
        'size',
        'color'
    ];

    public static $arrCycleDtlStatus = [
        self::STATUS_CYCLEDTL_NEW => 'New',
        self::STATUS_CYCLEDTL_OPEN => 'Open',
        self::STATUS_CYCLEDTL_NOT_APPLICABLE => 'Not Applicable',
        self::STATUS_CYCLEDTL_RECOUNT => 'Recount',
        self::STATUS_CYCLEDTL_ACCEPTED => 'Accepted'
    ];

    /**
     * CycleDtlModel constructor.
     * @param CycleDtl $model
     */
    public function __construct($model = NULL)
    {
        $this->model = $model ? $model : new CycleDtl();
        $this->getModel()->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $cycleHdrId
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @return mixed
     */
    public function search($whs_id, $cycleHdrId, $cycleDtlId = null, $attributes = [], $with = [], $limit = null, $status = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->where('cycle_hdr_id', $cycleHdrId);

        if ($cycleDtlId) {
            $query->where('cycle_dtl_id', $cycleDtlId);
        }
        if ($whs_id) {
            $query->where('whs_id', $whs_id);
        }

        if ($status) {
            $this->filterByStatus($query, $status);
        }

        if (!empty($attributes)) {

            foreach ($attributes as $key => $value) {
                if (in_array($key, $this->arrNumberFields)) {
                    $query->where($key, $value);
                } elseif (in_array($key, $this->arrStringFields)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $query->whereIn('cycle_dtl_sts', ['NW', 'RC']);
        $this->model->filterDataIn($query, false, false, false);
        $query->orderBy('act_loc_name');
        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);
        return $models;
    }

    /**
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public static function updateCycleDtl($model, $data)
    {
        $model->act_qty = isset($data['act_qty']) ? $data['act_qty'] : $model->act_qty;

        if (isset($data['act_loc_name'])) {
            $model->act_loc_name = $data['act_loc_name'];
            $model->act_loc_id = $data['act_loc_id'];
        } else {
            $model->act_loc_name = $model->sys_loc_name;
            $model->act_loc_id = $model->sys_loc_id;
        }

        $model->cycle_dtl_sts = self::STATUS_CYCLEDTL_OPEN;
        return $model->save();
    }

    /**
     * @param $cycleHdrId
     */
    public function getCycleDtlByCycleHdrId($cycleHdrId, Closure $condition = null)
    {
        $result = [];

        if (! $cycleHdrId) {
            return $result ;
        }

        $query = CycleDtl::select(
            'cycle_hdr.cycle_hdr_id',
            'cycle_hdr.count_by',
            'cycle_hdr.cycle_type',
            'cycle_hdr.has_color_size',
            'cycle_hdr.cycle_sts',
            'cycle_dtl.cycle_dtl_id',
            'cycle_dtl.whs_id',
            'cycle_dtl.cus_id',
            'cycle_dtl.item_id',
            'cycle_dtl.sku',
            'cycle_dtl.size',
            'cycle_dtl.color',
            'cycle_dtl.pack',
            'cycle_dtl.remain',
            'cycle_dtl.sys_qty',
            'cycle_dtl.act_qty',
            'cycle_dtl.sys_loc_id',
            'cycle_dtl.sys_loc_name',
            'cycle_dtl.act_loc_id',
            'cycle_dtl.act_loc_name',
            'cycle_dtl.cycle_dtl_sts'
        )
            ->join('cycle_hdr', 'cycle_hdr.cycle_hdr_id', '=', 'cycle_dtl.cycle_hdr_id')
            ->where('cycle_dtl.cycle_hdr_id', $cycleHdrId)
        ;

        if ($condition !== null) {
            call_user_func($condition, $query);
        }

        $result = $query->get()->toArray();

        return $result;
    }

    
    /**
     * @param $cycleHdrId
     */
    private function validateProcessSavedCycled($cycleHdrId)
    {
        if (! $cycleHdrId) {
            $this->errors[] = Message::get('VR029', 'Cycle Id');
            return false;
        }

        $cycleHdr = CycleHdrModel::getCycleHdr($cycleHdrId);

        if (! $cycleHdr) {
            $this->errors[] = Message::get('BM017', 'Cycle Count');
            return false;
        }

        $userID = CycleCountService::getUserId();

        if ($userID != $cycleHdr->assignee_id) {
            $this->errors[] = Message::get('BM078');
            return false;
        }

        if (! in_array($cycleHdr->cycle_sts, ['AS', 'RC'])) {
            $this->errors[] = Message::get('BM079', 'Cycle Count');
            return false;
        }
        $this->cycleHdr = $cycleHdr;

        $cycleDtls = $this->getCycleDtlByCycleHdrId($cycleHdrId, function($query) {
            //addition condition
            $query->where('cycle_dtl.cycle_dtl_sts', '!=', CycleDtlModel::STATUS_CYCLEDTL_ACCEPTED);
        });

        if (! $cycleDtls) {
            $this->errors[] = Message::get('BM067', 'Cycle Detail');
            return false;
        }

        $this->cycleDtls = $cycleDtls;

        return true;
    }

    /**
     * @param $cycleHdrId
     */
    public static function setDefaultActValueCycleDtl($cycleHdrId)
    {
        if (! $cycleHdrId) {
            return false;
        }

        return DB::update('
                UPDATE  cycle_dtl
                SET     act_loc_id =  IF (act_loc_id IS NULL, sys_loc_id, act_loc_id),
                        act_loc_name =  IF (act_loc_name IS NULL, sys_loc_name, act_loc_name),
                        act_qty = IF (act_qty IS NULL, sys_qty, act_qty)
                WHERE   cycle_hdr_id = ?',
                [
                    $cycleHdrId
                ]
        );
    }

    /**
     * @param $cycleDtlIds
     * @param $status
     */
    public static function updateCycleDtlStatus($cycleDtlIds, $status)
    {
        if (! $cycleDtlIds) {
            return false;
        }

        $cycleDtlIds = (array) $cycleDtlIds;

        return CycleDtl::whereIn('cycle_dtl_id', $cycleDtlIds)
            ->update([
                'cycle_dtl_sts' => $status
            ]);
    }

    /**
     * @param $cycleHdrId
     */
    public function processSavedCycled($cycleHdrId)
    {
        $validate = $this->validateProcessSavedCycled($cycleHdrId);

        if (! $validate) {
            return false;
        }

        $model = strtoupper($this->cycleHdr->count_by) == CycleHdrModel::COUNT_BY_EACH ?
            new CycleDisByEach() :
            new CycleDisByCarton();

        return $model->processDiscrepancy($this->cycleHdr,$this->cycleDtls);
    }

    /**
     * @param $cycleId
     * @param $cycleDtlIds
     * @return bool
     */
    public function deleteCycleDtls($cycleId, $cycleDtlIds)
    {
        if (! ($cycleDtlIds && is_array($cycleDtlIds))) {
            return false;
        }

        return $this->model
            ->where('cycle_hdr_id', $cycleId)
            ->whereIn('cycle_dtl_id', $cycleDtlIds)
            ->delete();
    }

    /**
     * @param $cycleDtls
     * @return string
     */
    public function getCycleHdrStatusByCycleDtls($cycleDtls)
    {
        $nOpen = 0;

        foreach ($cycleDtls as $cycleDtl) {
            $status = $cycleDtl['cycle_dtl_sts'];

            if ($status == self::STATUS_CYCLEDTL_NEW) {
                return CycleHdrModel::STATUS_CYCLE_ASSIGNED;
            } elseif($status == self::STATUS_CYCLEDTL_RECOUNT) {
                return CycleHdrModel::STATUS_CYCLE_RECOUNT;
            } elseif ($status == self::STATUS_CYCLEDTL_OPEN) {
                $nOpen++;
            }
        }

        return $nOpen ? CycleHdrModel::STATUS_CYCLE_CYCLED : CycleHdrModel::STATUS_CYCLE_COMPLETED;
    }

    /**
     * @param $query
     * @param $status
     */
    private function filterByStatus(&$query, $status)
    {
        switch ($status) {
            case self::STATUS_CYCLEDTL_NEW:
            case self::STATUS_CYCLEDTL_OPEN:
            case self::STATUS_CYCLEDTL_NOT_APPLICABLE:
            case self::STATUS_CYCLEDTL_RECOUNT:
            case self::STATUS_CYCLEDTL_ACCEPTED:
                $query->where('cycle_dtl_sts', $status);
                break;
            case 'DS':
                $query->whereRaw('(sys_qty != act_qty OR sys_loc_id != act_loc_id)');
        }
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getCycleDtlById($id)
    {
        return $this->model->with('cycleHdr')->find($id);
    }

    /**
     * @param $data
     */
    public function validateUpdateData($cycleDtl, &$data)
    {
        $result = [];
        $customRules = [];
        $location = NULL;
        $statusesValid = [
            self::STATUS_CYCLEDTL_NEW,
            self::STATUS_CYCLEDTL_RECOUNT
        ];

        if ($cycleDtl->cycleHdr->assignee_id != CycleCountService::getUserId()) {
            $result['errors'] = [Message::get('BM078')];
            return $result;
        }

        if (! in_array($cycleDtl->cycle_dtl_sts, $statusesValid)) {
            $result['errors'] = [Message::get('BM080')];
            return $result;
        }

        if (! isset($data['act_loc_name'])) {
            return $result;
        }

        $location = LocationModel::getLocationByCode($data['act_loc_name']);

        //check valid of location on cycle
        if (! $location) {
            $result['errors'] = [Message::get('BM081', 'Location: ' . $data['act_loc_name'])];
            return $result;
        }

        $isValid = $this->checkValidLocationByCycleType($cycleDtl, $location);
        if (! $isValid) {
            $result['errors'] = $this->errors;
        } else {
            $data['act_loc_id'] = $location->loc_id;
        }

        return $result;
    }

    /**
     * @param $cycleDtl
     * @param $location
     */
    private function checkValidLocationByCycleType($cycleDtl, $location)
    {
        if ($cycleDtl->whs_id != $location->loc_whs_id) {
            $this->errors[] = Message::get('BM066', $location->loc_code, 'the Cycle warehouse');
            return false;
        }

        if ($cycleDtl->cus_id != $location->cus_id) {
            $this->errors[] = $location->loc_code . ' is not exists on this Customer';

            return false;
        }

        $arrStatusValid = [LockModel::STATUS_LOCK, LockModel::STATUS_ACTIVE];

        if (! in_array($location->loc_sts_code, $arrStatusValid)) {
            $this->errors[] = Message::get('BM081', $location->loc_code);
            return false;
        }

        $locationIdValid = $this->getLocIdValidOnCycle($cycleDtl);

        if ($location->loc_sts_code == LockModel::STATUS_LOCK
            && ! in_array($location->loc_id, $locationIdValid)) {

            $msg = $location->loc_code . " is clocked";
            $this->errors[] = $msg; //Message::get('BM082', $location->loc_code);
            return false;
        }

        return true;
    }

    /**
     * @param $cycleDtl
     *
     * @return array
     */
    public function getLocIdValidOnCycle($cycleDtl)
    {
        $result = [];
        if (! $cycleDtl) {
            return $result;
        }

        $cycleType = strtoupper($cycleDtl->cycleHdr->cycle_type);

        switch ($cycleType) {
            case CycleHdrModel::CYCLE_TYPE_LOCATION:

                $result = explode(',', $cycleDtl->cycleHdr->cycle_detail);
                break;
            case CycleHdrModel::CYCLE_TYPE_SKU:

                $result = $this->getLocIdExistsOnCycleHdr($cycleDtl->cycle_hdr_id);

                break;
            case CycleHdrModel::CYCLE_TYPE_CUSTOMER:
                // get location of cycle count by customer and 2 cycle count with location (of that customer)
                $locationIdValid = $this->getLocIdExistsOnCycleHdr($cycleDtl->cycle_hdr_id);

                $locIdFree= LocationModel::getLocationFreeOfCustomer(
                    $cycleDtl->whs_id, $cycleDtl->cus_id, LockModel::STATUS_LOCK);

                $result = array_merge($locationIdValid, $locIdFree);

                break;
            default:
                break;
        }

        return $result;
    }

    /**
     * @param $cycleHdrId
     */
    public function getLocIdExistsOnCycleHdr($cycleHdrId)
    {
        $result = [];

        if (! $cycleHdrId) {
            return $result;
        }

        $result = $this->model->select(
                'sys_loc_id'
            )
            ->where('cycle_hdr_id', $cycleHdrId)
            ->distinct()
            ->get()
            ->toArray();

        return $result ? array_column($result, 'sys_loc_id') : [];
    }

    /**
     * @param $cycleDtlIds
     *
     * @return array
     */
    public function getCycleDtlByIds($cycleDtlIds)
    {
        $result =[];

        if (! $cycleDtlIds) {
            return $result;
        }

        $result = $this->model->select(
            'cycle_hdr_id',
            'cycle_dtl_sts',
            'cycle_dtl_id'
        )
            ->whereIn('cycle_dtl_id', $cycleDtlIds)
            ->where('deleted', 0)
            ->get()
        ->toArray();

        return $result;
    }

    /**
     * @param $input
     *
     * @return bool
     */
    public function validateCycleDtlApprove($input)
    {
        if (empty($input['cycle_dtl_ids'])) {
            $this->errors[] = Message::get('VR029', 'cycle_dtl_ids');
            return false;
        }
        $cycleHdrId = $input['cycle_hdr_id'];
        $cycleDtlIds = $input['cycle_dtl_ids'];

        if (!array_filter($cycleDtlIds, 'is_int')) {
            $this->errors[] = Message::get('BM083', 'cycle_dtl_id');
            return false;
        }
        
        $cycleHdr = CycleHdrModel::getCycleHdr($cycleHdrId);

        if (! $cycleHdr) {
            $this->errors[] = Message::get('BM067', 'Cycle Count');
            return false;
        }

        if ($cycleHdr->assignee_id == CycleCountService::getUserId()) {
            $this->errors[] = Message::get('BM078');
            return false;
        }

        $cycleDtls = $this->getCycleDtlByIds($cycleDtlIds);

        if (! $cycleDtlIds) {
            $this->errors[] = Message::get('BM067', 'Cycle count deitals');
            return false;
        }

        $cycleDtlIDValid = array_column($cycleDtls, 'cycle_dtl_id');

        $cycleDtlIdInvalid = array_diff($cycleDtlIds, $cycleDtlIDValid);

        if ($cycleDtlIdInvalid) {
            $this->errors[] = Message::get('BM084', implode(',', $cycleDtlIdInvalid));
            return false;
        }

        $cycleDtlNotInCycleHdr = $cycleDtlNotOpenStatus = [];
        foreach($cycleDtls as $cycleDtl) {
            if ($cycleDtl['cycle_hdr_id'] != $cycleHdrId) {
                $cycleDtlNotInCycleHdr[] = $cycleDtl['cycle_dtl_id'];
            }

            if ($cycleDtl['cycle_dtl_sts'] != self::STATUS_CYCLEDTL_OPEN) {
                $cycleDtlNotOpenStatus[] = $cycleDtl['cycle_dtl_id'];
            }
        }

        if ($cycleDtlNotInCycleHdr) {
            $this->errors[] = Message::get('BM085', implode(',', $cycleDtlNotInCycleHdr), $cycleHdrId);
            return false;
        }

        if ($cycleDtlNotOpenStatus) {
            $this->errors[] = Message::get('BM086', implode(',', $cycleDtlNotOpenStatus));
            return false;
        }

        return true;
    }

    /**
     * @param $input
     *
     * @return bool
     */
    public function processRecountCycle($input)
    {
        if (! $input) {
            $this->errors[] = Message::get('VR029', 'Input');
            return false;
        }

        $cycleHdrId = $input['cycle_hdr_id'];
        $arrCycleDtlId = $input['cycle_dtl_ids'];
        $eventTrackingModel = new EventTrackingModel();
        $recountType = CycleHdrModel::STATUS_CYCLE_RECOUNT;
        $data = CycleHdrModel::getCycleHdr($cycleHdrId);

        if (! $data) {
            $this->errors[] = "Data not found";
            return false;
        }

        try {
            DB::beginTransaction();

            //update cycle_hdr
            CycleHdr::where('cycle_hdr_id', $cycleHdrId)
                ->update([
                    'cycle_sts' => $recountType
                ]);

            CycleDtl::where('cycle_hdr_id', $cycleHdrId)
                ->whereIn('cycle_dtl_id', $arrCycleDtlId)
                ->update([
                    'cycle_dtl_sts' => $recountType
                ]);

            CycleDis::where('cycle_hdr_id', $cycleHdrId)
                ->whereIn('cycle_dtl_id', $arrCycleDtlId)
                ->update([
                    'dicpy_sts' => $recountType
                ]);

            //event tracking
            $cycleNumAndSeq = $eventTrackingModel->generateEventTrackingTransNum(
                $cycleHdrId,
                EventTrackingModel::CYCLE_RECOUNT
            );
            
            $eventTrackingModel->create([
                'whs_id'    => $data['whs_id'],
                'cus_id'    => ucfirst($data['cycle_type']) == 'CS' ? $data['cycle_detail'] : NULL,
                'owner'     => $cycleNumAndSeq['cycle_num'],
                'evt_code'  => EventTrackingModel::CYCLE_RECOUNT,
                'trans_num' => $cycleNumAndSeq['trans_num'],
                'info'      => EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_RECOUNT] . ' ' .
                    $cycleNumAndSeq['trans_num']
            ]);

            DB::commit();

        } catch (\PDOException $e) {
            DB::rollBack();

            $this->errors[] = $e->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @param $itemIds
     */
    public static function getLocIdByItem($cycleDtls, $itemIds)
    {
        $result = [];

        foreach ($cycleDtls as $cycleDtl) {
            if (in_array($cycleDtl['item_id'], $itemIds)) {
                $result[] = $cycleDtl['sys_loc_id'];
                $result[] = $cycleDtl['act_loc_id'];
            }
        }

        return array_unique($result);
    }
}

