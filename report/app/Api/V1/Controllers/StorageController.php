<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\StoragesModel;
use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Transformers\StoragesTransformer;
use App\Api\V1\Transformers\StorageReportsTransformer;
use Carbon\Carbon;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Seldat\Wms2\Utils\Message;
use Illuminate\Support\Facades\DB;
use Dingo\Api\Routing\Helpers;
use Laravel\Lumen\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Psr\Http\Message\ResponseInterface as IResponse;
use Illuminate\Http\Response as LumenResponse;
use Seldat\Wms2\Models\Storages;
use Seldat\Wms2\Utils\Status;

use App\Api\V1\Models\CartonsModel;

class StorageController extends AbstractController
{

    protected $cartonReceivingModel;

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new StoragesModel();

        $this->transformer = new StoragesTransformer();

        $this->cartonModel = new CartonsModel();
    }


    public function search(
        IRequest $request,
        $data = [],
        Storages $storages
    )
    {
        $model = array_get($data, 'model', NULL);

        $method = array_get($data, 'method', NULL);
        $with = array_get($data, 'with', []);

        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', PAGING_LIMIT);

        $searchModel = $model ?: $this->model;

        try {

            //Export CSV
            if (!empty($input['export']) && $input['export'] == 1) {
                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $results = $method && method_exists($searchModel, $method) ?
                    $searchModel->$method($input, $with, $limit) :
                    $searchModel->search($input, $with, $limit);
                $saveStorages = $results;
                $results = $results->get()->toArray();
                //if (!$results) {
                //    $this->response->errorBadRequest(Message::get('VR003'));
                //}
                $storageinfos = [];
                if ($results) {
                    foreach ($results as $key => $receivingReport) {

                        // $pieceRemain = array_get($receivingReport, 'piece_remain', 0);
                        $totalQty = array_get($receivingReport, 'sum_piece_remain', 0);

                        $whs_id = array_get($receivingReport, 'whs_id', NULL);
                        $cus_id = array_get($receivingReport, 'cus_id', NULL);
                        $item_id = array_get($receivingReport, 'item_id', NULL);
                        $loc_id = array_get($receivingReport, 'loc_id', NULL);
                        $plt_id = array_get($receivingReport, 'plt_id', NULL);
                        $lot = array_get($receivingReport, 'lot', NULL);
                        $ctn_pack_size = array_get($receivingReport, 'ctn_pack_size', NULL);

                        $percarton = round(((array_get($receivingReport, 'length', 0) *
                                array_get($receivingReport, 'width', 0) *
                                array_get($receivingReport, 'height', 0)) / config('constants.CUBE_CONVERT_2_FEET')), 2);


                        $storageinfos[] = [
                            'whs_name' => array_get($receivingReport, 'whs_name', ''),
                            'cus_name' => array_get($receivingReport, 'cus_name', ''),
                            'item_id' => array_get($receivingReport, 'item_id', ''),
                            'lot' => array_get($receivingReport, 'lot', ''),
                            'ctn_sts' => Status::getByValue(array_get($receivingReport, 'ctn_sts', ''), 'CTN_STATUS'),
                            'sku' => array_get($receivingReport, 'sku', ''),
                            'upc' => array_get($receivingReport, 'upc', ''),
                            'color' => array_get($receivingReport, 'color', ''),
                            'size' => array_get($receivingReport, 'size', ''),
                            'length' => round(array_get($receivingReport, 'length', ''), 2),
                            'width' => round(array_get($receivingReport, 'width', ''), 2),
                            'width' => round(array_get($receivingReport, 'width', ''), 2),
                            'height' => round(array_get($receivingReport, 'height', ''), 2),
                            'cube' => $percarton,
                            'weight' => round(array_get($receivingReport, 'weight', ''), 1),
                            'loc_name' => array_get($receivingReport, 'loc_code', ''),
                            'pack_size' => array_get($receivingReport, 'ctn_pack_size', 0),
                            'uom' => array_get($receivingReport, 'uom_name', ''),
                            'ctns' => round(($totalQty / $ctn_pack_size), 2),
                            'qty' => $totalQty,
                            'total_cube' => round(($percarton * ($totalQty / $ctn_pack_size)), 2),

                        ];
                    }
                }

                $title = [
                    'whs_name' => 'Warehouse',
                    'cus_name' => 'CUST',
                    'item_id' => 'Item Id',
                    'lot' => 'Lot',
                    'ctn_sts' => 'Status',
                    'loc_name' => 'Location',
                    'upc' => 'UPC',
                    'sku' => 'SKU',
                    'color' => 'Color',
                    'size' => 'Size',
                    'length' => 'Length',
                    'width' => 'Width',
                    'height' => 'Height',
                    'cube' => 'Cube',
                    'weight' => 'Weight',
                    // 'sys_mea_name' => 'sys_mea_name',
                    'pack_size' => 'Pack size',
                    'uom' => 'UOM',
                    'ctns' => 'CTNS',
                    'qty' => 'QTY',
                    'total_cube' => 'Total Cube',
                ];

                // $filePath = storage_path() . "/Report_PBPM_{$warehouse->whs_name}.csv";

                $today = time();
                $filePath = "Report_Storages_{$today}.csv";
                $this->saveFile($title, $storageinfos, $filePath);
                die;
            }

            $results = $method && method_exists($searchModel, $method) ?
                $searchModel->$method($input, $with, $limit) :
                $searchModel->search($input, $with, $limit);

            if ($results) {

                $autocomplete = array_get($input, 'autocomplete', NULL);

                return $autocomplete ?
                    array_column($results->toArray(), $autocomplete) :
                    $this->response->paginator($results, $this->transformer);
            } else {
                $this->response->errorBadRequest(Message::get('VR003'));
            }

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->openToBrowser($filePath); // stream data directly to the browser

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();
    }

    public function storageAging(Request $request)
    {
        $input = $request->getParsedBody();

        $query30 = $this->storageAgingQuery(30, $input);
        $query60 = $this->storageAgingQuery(60, $input);
        $query90 = $this->storageAgingQuery(90, $input);

        $res['data'] = [
            [
                'count' => $query30['count'],
                'period' => $query30['period'],
            ],
            [
                'count' => $query60['count'],
                'period' => $query60['period'],
            ],
            [
                'count' => $query90['count'],
                'period' => $query90['period'],
            ],
        ];

        return response()->json($res);
    }

    public function storageAgingQuery($numOfDay, $attribute)
    {
        $query = DB::table('cartons')
            ->select(
                DB::raw("'" . $numOfDay . "' AS period"),
                DB::raw(
                    'SUM(
                            IF(
                                IFNULL(shipped_dt, NOW()) > GREATEST(gr_dt , DATE(NOW() - INTERVAL ' . $numOfDay . ' DAY)), 
                                DATEDIFF(
                                    IFNULL(shipped_dt, NOW()), GREATEST(gr_dt , DATE(NOW() - INTERVAL ' . $numOfDay . ' DAY))
                                ), 
                                0
                            )
                        ) AS count')
            )
            ->whereIn('ctn_sts', array('AC', 'PD', 'SH', 'LK'))
            ->where('created_at', '>=', DB::raw('DATE(NOW() - INTERVAL ' . $numOfDay . ' DAY)'));

        // Filter by Customer
        if (!empty($attribute['cus_id'])) {
            $query->where('cus_id', $attribute['cus_id']);
        }

        // Filter by SKU
        if (!empty($attribute['sku'])) {
            $query->where('sku', $attribute['sku']);
        }

        $result = $query->first();

        return $result;
    }

    public function storageAgingValidate($attribute)
    {
        if (empty($attribute['sku']) && empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest('Please select a Sku or Customer!');
        }
    }

    public function storageReportDaily(Request $request, $whsId)
    {
        $input = $request->getQueryParams();

        $isExport = (!empty($input['export']) && $input['export']) ? true : false;

        if (empty($input['date_to'])) {
            $input['date_to'] = Carbon::yesterday()->toDateString();
        } else {
            $input['date_to'] = Carbon::createFromFormat('m/d/Y', $input['date_to'])->toDateString();
        }

        if ($input['date_to'] > date('Y-m-d')) {
            return $this->response()->errorBadRequest('The date cannot be greater than the current date ');
        }
        if (empty($input['limit'])) {
            $input['limit'] = 20;
        }

        $data = $this->getStorageReport($whsId, $input, $isExport);

        if ($isExport) {

            $title = [
                'cus_name' => 'Customer',
                'rac' => 'Rack',
                'mix_rac' => 'Mix Rack',
                'bin' => 'Bin',
                'mix_bin' => 'Mix Bin',
                'she' => 'Shelf',
                'mix_she' => 'Mix Shelf',
                'num_of_loc' => 'Total Locations',
            ];

            $filePath = "Report_Storages_" . time() . ".csv";
            $this->saveFile($title, $data->toArray(), $filePath);

            die;
        }
        return $this->response->paginator($data, new StorageReportsTransformer());
    }

    private function getStorageReport($whsId, $attributes, $isExport)
    {
        $query = \Seldat\Wms2\Models\StorageReport::join('customer', 'customer.cus_id', '=', 'strg_rpt.cus_id')
            ->select([
                'customer.cus_name',
                DB::raw('strg_rpt.rack+ strg_rpt.bin +strg_rpt.shelf as num_of_loc'),
                DB::raw('strg_rpt.rack-strg_rpt.mix_rack as rac'),
                DB::raw('strg_rpt.mix_rack as mix_rac'),
                DB::raw('strg_rpt.bin-strg_rpt.mix_bin as bin'),
                DB::raw('strg_rpt.mix_bin as mix_bin'),
                DB::raw('strg_rpt.shelf-strg_rpt.mix_shelf as she'),
                DB::raw('strg_rpt.mix_shelf as mix_she'),
            ])
            ->where('whs_id', $whsId);

        if (!empty($attributes['cus_id'])) {
            $query->where('strg_rpt.cus_id', $attributes['cus_id']);
        }

        $query->where('strg_rpt.date', $attributes['date_to']);

        $query->groupBy('strg_rpt.whs_id', 'strg_rpt.cus_id');

        if ($isExport) {
            return $query->get();
        }

        return $query->paginate($attributes['limit']);
    }
}
