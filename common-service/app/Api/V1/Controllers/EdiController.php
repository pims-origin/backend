<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\EdiModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\CustomersModel;
use App\Api\V1\Transformers\SystemCountryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use App\Api\V1\Transformers\EdiTransformer;
use App\Api\V1\Transformers\CustomerConfigTransformer;
use App\Api\V1\Validators\EdiValidator;
use App\Api\V1\Validators\SystemCountryValidator;
use App\Api\V1\Validators\CustomerConfigValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\Edi;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\SystemBug;

/**
 * Class EdiController
 *
 * @package App\Api\V1\Controllers
 */
class EdiController extends AbstractController
{

    /**
     * @var SystemStateModel
     */
    protected $systemStateModel;

    /**
     * @var SystemCountryModel
     */
    protected $systemCountryModel;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var SystemCountryValidator
     */
    protected $systemCountryValidator;

    /**
     * @var SystemCountryTransformer
     */
    protected $systemCountryTransformer;

    /**
     * @var SystemStateTransformer
     */
    protected $systemStateTransformer;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var EdiModel
     */
    protected $ediModel;

    /**
     * @var CustomersModel
     */
    protected $customersModel;

    /**
     * @var EdiTransformer
     */
    protected $ediTransformer;

    /**
     * CountryController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->model = new SystemCountryModel();
        $this->systemCountryModel = new SystemCountryModel();
        $this->systemStateModel = new SystemStateModel();
        $this->customerConfigValidator = new CustomerConfigValidator();
        $this->systemCountryTransformer = new SystemCountryTransformer();
        $this->customerConfigTransformer = new CustomerConfigTransformer();
        $this->systemStateTransformer = new SystemStateTransformer();
        $this->ediModel = new EdiModel();
        $this->ediValidator = new EdiValidator();
        $this->ediTransformer = new EdiTransformer();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->customersModel = new CustomersModel();
    }


    /**
     * @param $request
     *
     * @return array|void
     */
    public function store(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $whsId = array_get($input['data'], '0.whs_id', 0);
        $cusId = array_get($input['data'], '0.cus_id', 0);

        try {
            $userId = JWTUtil::getPayloadValue('jti');
            // check exist of cus_config
            $ediTrans = array_pluck(
                Edi::select('edi_trans')
                    ->where('edi_sts', 'AC')
                    ->get(), 'edi_trans', 'edi_trans');

            $CusConfigInfo = $this->customerConfigModel->getCusConfigByIds($whsId, $cusId, $ediTrans)->toArray();
            if ($CusConfigInfo) {
                //update
                foreach ($input['data'] as $UpVal) {
                    $cusCfgId = $this->customerConfigModel->getCusConfigId(
                        array_get($UpVal, 'whs_id', 0),
                        array_get($UpVal, 'cus_id', 0),
                        array_get($UpVal, 'edi_trans', 0))->toArray();

                    $inputParam = [
                        'id'           => $cusCfgId['id'],
                        'cus_id'       => array_get($UpVal, 'cus_id', 0),
                        'whs_id'       => array_get($UpVal, 'whs_id', 0),
                        'config_name'  => array_get($UpVal, 'edi_name', ''),
                        'config_value' => array_get($UpVal, 'edi_trans', 0),
                        'ac'           => array_get($UpVal, 'edi_sts', 'N'),
                        'sts'          => 'u',

                    ];
                    $this->customerConfigModel->update($inputParam);
                }

                return ["data" => "successful"];
            }
            //create new
            $CusConfigData = [];
            foreach ($input['data'] as $val) {
                $inputParam = [
                    'cus_id'       => array_get($val, 'cus_id', 0),
                    'whs_id'       => array_get($val, 'whs_id', 0),
                    'config_name'  => array_get($val, 'edi_name', ''),
                    'config_value' => array_get($val, 'edi_trans', 0),
                    'ac'           => array_get($val, 'edi_sts', 'N'),
                    'sts'          => 'i',
                    'created_at'   => time(),
                    'created_by'   => $userId,
                    'updated_at'   => time(),
                    'deleted_at'   => 915148800,
                    'updated_by'   => $userId,
                    'deleted'      => 0,

                ];
                $CusConfigData[] = $inputParam;
            }

            /**
             * Insert batch
             */
            $res = DB::table('cus_config')->insert(
                $CusConfigData
            );

            return ["data" => "successful"];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $warehouseId
     * @param $customerId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function deleteCustomerConfigWarehouse($warehouseId, $customerId)
    {
        $this->customerConfigModel->deleteCustomerConfigWarehouse($warehouseId, $customerId);

    }

    /**
     * @param Request $request
     * @param $whsId
     * @param CustomerConfigTransformer $customerConfigTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function ediList(
        Request $request,
        $whsId,
        CustomerConfigTransformer $customerConfigTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            // get list Cmd
            $customerConfigInfos = $this->customersModel->search($whsId, $input,
                ['customerConfig'],
                array_get($input, 'limit'));

            return $this->response->paginator($customerConfigInfos, $customerConfigTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'ediList', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param EdiTransformer $ediTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(
        $whsId,
        $cusId,
        EdiTransformer $ediTransformer
    ) {

        try {
            $ediTrans = array_pluck(
                Edi::select('edi_trans')
                    ->where('edi_sts', 'AC')
                    ->get(), 'edi_trans', 'edi_trans');

            //EDI is Yes
            $edidatas = array_pluck($this->ediModel->getEdiInfos()->toArray(), 'edi_name', 'edi_trans');
            $cusConfigInfo = $this->customerConfigModel->getCusConfigByIds($whsId, $cusId, $ediTrans)->toArray();
            if (!empty($cusConfigInfo)) {
                $CusConfigData = [];
                foreach ($cusConfigInfo as $val) {
                    $inputParam = [
                        'edi_name'  => $edidatas[array_get($val, 'config_value', 0)],
                        'edi_trans' => array_get($val, 'config_value', 0),
                        'edi_sts'   => array_get($val, 'ac', 'N'),

                    ];
                    $CusConfigData[] = $inputParam;
                }

                return ['data' => $CusConfigData];
            }

            //EDI is No
            $ediInfos = $this->ediModel->getEdiInfos()->toArray();
            if (!empty($ediInfos)) {
                $ediData = [];
                foreach ($ediInfos as $EdiVal) {
                    $getParam = [
                        'edi_name'  => array_get($EdiVal, 'edi_name', ''),
                        'edi_trans' => array_get($EdiVal, 'edi_trans', 0),
                        'edi_sts'   => 'N',

                    ];
                    $ediData[] = $getParam;
                }

                return ['data' => $ediData];
            }

            return ['data' => 'No Data.'];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                $e->getMessage() . " " . $e->getLine()
            //SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
