<?php
namespace App;

/**
 * Class WavepickDtl
 *
 * @property mixed cus_id
 * @property mixed whs_id
 * @property mixed wv_id
 * @property mixed item_id
 * @property mixed uom_id
 * @property mixed wv_num
 * @property mixed color
 * @property mixed sku
 * @property mixed size
 * @property mixed pack_size
 * @property mixed lot
 * @property mixed ctn_qty
 * @property mixed piece_qty
 * @property mixed act_piece_qty
 * @property mixed primary_loc_id
 * @property mixed primary_loc
 * @property mixed bu_loc_1_id
 * @property mixed bu_loc_1
 * @property mixed bu_loc_2_id
 * @property mixed bu_loc_2
 * @property mixed bu_loc_3_id
 * @property mixed bu_loc_3
 * @property mixed bu_loc_4_id
 * @property mixed bu_loc_4
 * @property mixed bu_loc_5_id
 * @property mixed bu_loc_5
 * @property mixed wv_dtl_sts
 *
 * @package Seldat\Wms2\Models
 */
class WavepickDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_dtl';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_dtl_id';
    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'wv_id',
        'item_id',
        'uom_id',
        'wv_num',
        'color',
        'sku',
        'size',
        'pack_size',
        'lot',
        'ctn_qty',
        'piece_qty',
        'act_piece_qty',
        'primary_loc_id',
        'primary_loc',
        'bu_loc_1_id',
        'bu_loc_1',
        'bu_loc_2_id',
        'bu_loc_2',
        'bu_loc_3_id',
        'bu_loc_3',
        'bu_loc_4_id',
        'bu_loc_4',
        'bu_loc_5_id',
        'bu_loc_5',
        'wv_dtl_sts',
        'act_loc_id',
        'act_loc',
        'cus_upc',
    ];

    public function waveHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\WavepickHdr', 'wv_id', 'wv_id');
    }
}
