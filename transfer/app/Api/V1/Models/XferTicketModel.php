<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\XferTicket;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class XferTicketModel extends AbstractModel
{

    /**
     * XferTicketModel constructor.
     *
     * @param XferTicket|null $model
     */
    public function __construct(XferTicket $model = null)
    {
        $this->model = ($model) ?: new XferTicket();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveXferTicket($data)
    {
        return $this->model->create($data);
    }

    /**
     * @return mixed
     */
    public function getLatestWvTKNumber()
    {
        return $this->model->orderBy('xfer_ticket_num', 'desc')->select('xfer_ticket_num')->first();
    }

    /**
     * @return string
     */
    public function generateWvTKNum()
    {
        $currentYearMonth = date('ym');
        $defaultWvTKNum = "WXF-${currentYearMonth}-00001";

        $lastWvTK = $this->getLatestWvTKNumber();
        $lastWvTKNum = $lastWvTK['xfer_ticket_num'];

        if (empty($lastWvTKNum) || strpos($lastWvTKNum, "-${currentYearMonth}-") === false) {
            return $defaultWvTKNum;
        }

        return ++$lastWvTKNum;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getTicketInfoById($id)
    {
        return $this->model->where('xfer_ticket_id', $id)->first();
    }
}
