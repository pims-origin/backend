<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\BaseService;
use App\Api\V1\Models\MenuService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;

class MenuController extends AbstractController
{
    /**
     * @var MenuService
     */
    protected $service;

    /**
     * MenuController constructor.
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new MenuService($request);
    }

    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getFullLeftMenu(IRequest $request)
    {
        $result = $this->service->getFullLeftMenu();
        $response = $this->convertResponse($result);
        return $response;
    }

    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getLeftMenu(IRequest $request)
    {
        $result = $this->service->getLeftMenu($request);
        if ($result === false) {
            throw new NotFoundHttpException('Menu not found');
        }
        $response = $this->convertResponse($result);
        return $response;
    }

    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getFavouriteMenu(IRequest $request)
    {
        $result = $this->service->getFavouriteMenu($this->userId);
        $response = $this->convertResponse($result);
        return $response;
    }

    public function addFavouriteMenuItem(IRequest $request)
    {
        $result = $this->service->addFavouriteMenuItem($request, $this->userId);
        if ($result === false) {
            throw new BadRequestHttpException('Add favorite menu fail');
        }
        $response = $this->convertResponse($result);
        return $response;
    }

    public function removeFavouriteMenuItem($id)
    {
        $result = $this->service->removeFavouriteMenuItem($id, $this->userId);
        if ($result === false) {
            throw new BadRequestHttpException('Remove favorite menu fail');
        }
        $response = $this->convertResponse($result);
        return $response;
    }

    public function getMenu(IRequest $request)
    {
        $this->checkPermission($request, 'viewMenu');
        $result = $this->service->getMenuList();
        $response = $this->convertResponse($result);
        return $response;
    }

    public function createMenu(IRequest $request)
    {
        $this->checkPermission($request, 'createMenu');
        $bodyData = $request->getParsedBody();
        $result = $this->service->createMenuGroup($bodyData);
        $response = $this->convertResponse($result);
        return $response;
    }

    public function createMenuItem(IRequest $request, $groupId)
    {
        $this->checkPermission($request, 'createMenu');
        $bodyData = $request->getParsedBody();
        $result = $this->service->createMenuItem($groupId, $bodyData);
        $response = $this->convertResponse($result);
        return $response;
    }

    public function getMenuItem(IRequest $request, $groupId)
    {
        $this->checkPermission($request, 'viewMenu');
        $result = $this->service->getMenuItem($groupId);
        $response = $this->convertResponse($result);
        return $response;
    }
}
