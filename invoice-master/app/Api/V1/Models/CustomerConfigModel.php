<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\CustomerMeta;
use Illuminate\Support\Facades\DB;

class CustomerConfigModel extends AbstractModel
{
    protected $emailCode = 'CIE';
    protected $otherCode = 'CIO';

    /**
     * Constructor.
     */

    public function __construct()
    {
        $this->model = new CustomerMeta();
    }

    public function getInfo($type, $cusId)
    {
        $cusInfo = $this->getFirstWhere([
            'cus_id'        => $cusId,
            'qualifier' => $this->{$type.'Code'}
        ]);

        return $cusInfo;
    }
    
    public function addInfo($input = [], $type, $cusId)
    {
        $cusInfo = CustomerMeta::where([
            'cus_id'        => $cusId,
            'qualifier' => $this->{$type.'Code'}
        ])->first();
        
        if ($cusInfo) {
            CustomerMeta::where([
                'cus_id'        => $cusId,
                'qualifier' => $this->{$type.'Code'}
            ])->update(['value' => json_encode($input)]);
        } else {
            $this->create([
                'cus_id' => $cusId,
                'qualifier'  => $this->{$type.'Code'},
                'value' => json_encode($input)
            ]);
        }
    }
}
