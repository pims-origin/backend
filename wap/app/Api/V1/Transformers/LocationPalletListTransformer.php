<?php


namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Pallet;

class LocationPalletListTransformer extends TransformerAbstract
{
    //return result
    public function transform(Pallet $pallet)
    {
        return [
            'plt_id' => object_get($pallet,'plt_id'),
            'loc_id' => object_get($pallet,'loc_id'),
            'loc_name' => object_get($pallet,'loc_name'),
            'plt_num' => object_get($pallet,'plt_num'),
            'cus_id' => object_get($pallet,'cus_id'),
            'cus_id' => object_get($pallet,'cus_id'),
            'whs_id' => object_get($pallet,'whs_id'),
            'rfid' => object_get($pallet,'rfid'),
            'plt_tier' => object_get($pallet,'plt_tier'),
            'ctn_ttl' => object_get($pallet,'ctn_ttl'),
            'gr_hdr_id' => object_get($pallet,'gr_hdr_id'),
            'gr_hdr_id' => object_get($pallet,'gr_hdr_id'),
            'gr_dtl_id' => object_get($pallet,'gr_dtl_id'),
            'storage_duration' => object_get($pallet,'storage_duration'),
            //'location' => object_get($pallet,'dtlLoc'),
        ];
    }

}
