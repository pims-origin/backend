<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerZone;
use League\Fractal\TransformerAbstract;

class CustomerZoneTransformer extends TransformerAbstract
{
    public function transform(CustomerZone $customerZone)
    {
        return [
            // Customer Info
            'cus_id'    => $customerZone->cus_id,
            'cus_name'  => object_get($customerZone, 'customer.cus_name', null),
            'customers' => object_get($customerZone, 'customer', null),

            'zone_id'          => $customerZone->zone_id,
            'zone_code'        => object_get($customerZone, 'zone.zone_code', null),
            'zone_name'        => object_get($customerZone, 'zone.zone_name', null),
            'zone_min_count'   => object_get($customerZone, 'zone.zone_min_count', null),
            'zone_max_count'   => object_get($customerZone, 'zone.zone_max_count', null),
            'zone_description' => object_get($customerZone, 'zone.zone_description', null),
            'zone_whs_id'      => object_get($customerZone, 'zone.zone_whs_id', null),
            'whs_name'         => object_get($customerZone, 'zone.warehouse.whs_name', ''),
            'whs_code'         => object_get($customerZone, 'zone.warehouse.whs_code', ''),
            'zone_type_name'   => !empty($customerZone->zone->zoneType->zone_type_name)
                ? $customerZone->zone->zoneType->zone_type_name
                : null,
        ];
    }
}