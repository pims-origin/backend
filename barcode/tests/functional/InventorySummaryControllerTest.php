<?php
//
//use Illuminate\Http\Response as IlluminateResponse;
//use Laravel\Lumen\Testing\DatabaseTransactions;
//use Seldat\Wms2\Models\InventorySummary;
//use Seldat\Wms2\Models\Item;
//use Seldat\Wms2\Models\Warehouse;
//use Seldat\Wms2\Models\Customer;
//use Seldat\Wms2\Utils\Message;
//
//class InventorySummaryControllerTest extends TestCase
//{
//    use DatabaseTransactions;
//
//    protected $goodsReceiptId;
//    protected $warehouseId;
//    protected $customerId;
//    protected $asnHeaderId;
//
//    public function getEndPoint()
//    {
//        return "/v1/inventory/";
//    }
//
//
//    protected $params = [];
//
//    public function setUp()
//    {
//        parent::setUp();
//        // create data into DB
//        $warehouse = factory(Warehouse::class)->create();
//        $customer = factory(Customer::class)->create();
//
//        $this->warehouseId = $warehouse->whs_id;
//        $this->customerId = $customer->cus_id;
//
//        $this->params['whs_id'] = $this->warehouseId;
//        $this->params['cus_id'] = $this->customerId;
//
//
//    }
//
//    public function testSearch_Ok()
//    {
//        // create param search
//        $params = [
//            'cus_id' => $this->params['cus_id'],
//            'sku'    => str_random(10),
//            'color'  => str_random(10),
//            'size'   => str_random(10),
//        ];
//        $search = [];
//        $index = rand(-1, 4);
//        if ($index > 0) {
//            switch ($index) {
//                case 1 :
//                    $search = array_except($params, 'cus_id');
//                    break;
//                case 2 :
//                    $search = array_except($params, 'sku');
//                    break;
//                case 3 :
//                    $search = array_except($params, 'color');
//                    break;
//                case 4 :
//                    $search = array_except($params, 'size');
//                    break;
//                default :
//                    break;
//            }
//        }
//
//
//        $response = $this->call('GET', $this->getEndPoint(), $search);
//
//        // Case 1: Response code is 200
//        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
//
//        // Case 2: Data is not empty
//        $responseData = json_decode($response->getContent(), true);
//
//        $this->assertNotEmpty($responseData);
//    }
//
//    public function testCreate_Ok()
//    {
//        $params = $this->params;
//        $item = factory(Item::class)->create();
//        $params['lot'] = $item->lot;
//        $params['size'] = $item->size;
//        $params['color'] = $item->color;
//        $params['sku'] = $item->sku;
//        $params['ttl'] = 1010;
//        $params['allocated_qty'] = 0;
//        $params['dmg_qty'] = 10;
//        $params['avail'] = 1000;
//
//        $this->call('POST', $this->getEndPoint() . $item->item_id, $params);
//
//        // Case 1: Response code is 200
//        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
//
//
//    }
//
//    public function testCreateNotExistWareHouse_Fail()
//    {
//        $params = $this->params;
//        $params['whs_id'] = PHP_INT_MAX;
//        $item = factory(Item::class)->create();
//        $params['lot'] = $item->lot;
//        $params['size'] = $item->size;
//        $params['color'] = $item->color;
//        $params['sku'] = $item->sku;
//        $params['ttl'] = 1010;
//        $params['allocated_qty'] = 0;
//        $params['dmg_qty'] = 10;
//        $params['avail'] = 1000;
//
//        $data = $this->call('POST', $this->getEndPoint() . $item->item_id, $params);
//
//        // Case 1: Response code is 200
//        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
//
//        // Case 2: Show message
//        $this->assertEquals(
//            'The selected whs id is invalid.',
//            json_decode($data->content(), true)['errors']['errors']['whs_id'][0]
//        );
//
//    }
//
//    public function testCreateNotExistCustomer_Fail()
//    {
//        $params = $this->params;
//        $params['cus_id'] = PHP_INT_MAX;
//        $item = factory(Item::class)->create();
//        $params['lot'] = $item->lot;
//        $params['size'] = $item->size;
//        $params['color'] = $item->color;
//        $params['sku'] = $item->sku;
//        $params['ttl'] = 1010;
//        $params['allocated_qty'] = 0;
//        $params['dmg_qty'] = 10;
//        $params['avail'] = 1000;
//
//        $data = $this->call('POST', $this->getEndPoint() . $item->item_id, $params);
//
//        // Case 1: Response code is 200
//        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
//
//        // Case 2: Show message
//        $this->assertEquals(
//            'The selected cus id is invalid.',
//            json_decode($data->content(), true)['errors']['errors']['cus_id'][0]
//        );
//
//
//    }
//
//    public function testCreateItemExist_Fail()
//    {
//        $evt = factory(InventorySummary::class)->create();
//        $params = $this->params;
//
//
//        $params['size'] = "Z";
//        $params['color'] = "red";
//        $params['sku'] = "te";
//        $params['ttl'] = 1010;
//        $params['allocated_qty'] = 0;
//        $params['dmg_qty'] = 10;
//        $params['avail'] = 1000;
//
//        $data = $this->call('POST', $this->getEndPoint() . $evt->itm_id, $params);
//
//        // Case 1: Response code is 200
//        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
//
//        // Case 2: Show message
//        $this->assertEquals(
//            'Item already existed',
//            json_decode($data->content(), true)['errors']['message']
//        );
//    }
//
//    public function testUpdate_Ok()
//    {
//        $evt = factory(InventorySummary::class)->create();
//
//
//        $data = $this->call('PUT', $this->getEndPoint() . $evt->itm_id . "/inbound");
//
//        // Case 1: Response code is 200
//        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
//
//        // Case 2: Data is not empty
//        $responseData = json_decode($data->getContent(), true);
//
//        $this->assertNotEmpty($responseData);
//
//
//        $data1 = $this->call('PUT', $this->getEndPoint() . $evt->itm_id . "/outbound");
//
//        // Case 3: Response code is 200
//        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
//
//        // Case 4: Data is not empty
//        $responseData = json_decode($data1->getContent(), true);
//
//        $this->assertNotEmpty($responseData);
//    }
//
//    public function testUpdateNotExist_Fail()
//    {
//
//
//        $data = $this->call('PUT', $this->getEndPoint() . PHP_INT_MAX . "/inbound");
//
//        // Case 1: Response code is 200
//        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
//
//        // Case 2: Show message
//        $this->assertEquals(
//            Message::get("BM017", "Item"),
//            json_decode($data->content(), true)['errors']['message']
//        );
//    }
//}
