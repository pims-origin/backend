<?php

namespace App\Modules\Customers\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\Customers\Resources\CustomerDetailResource;

class AddressResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'addresses',
            'id' => (int) $this->cus_add_id,
            'attributes' => [
                'address_type'      => $this->cus_add_type,
                'address_line_1'    => $this->cus_add_line_1,
                'address_line_2'    => $this->cus_add_line_2,
                'city'              => $this->cus_add_city_name,
                'state'             => $this->systemState->sys_state_name,
                'zip'               => $this->cus_add_postal_code,
                'country'           => $this->systemCountry->sys_country_name
            ]
        ];
    }
}