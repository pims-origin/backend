<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:49
 */

namespace Seldat\Wms2\Models;

/**
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property mixed whs_id
 * @property mixed chg_code_id
 * @property mixed cus_charge_dtl_rate
 * @property mixed cus_charge_dtl_des
 * @property mixed created_by
 * @property mixed updated_by
 */
class CustomerChargeDetail extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_charge_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'chg_code_id',
        'cus_charge_dtl_rate',
        'cus_charge_dtl_des',
    ];

    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function chargeCode()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ChargeCode', 'chg_code_id', 'chg_code_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
}
