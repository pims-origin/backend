<?php

namespace App\Modules\Orders\Eloquents;

class SystemState extends BaseSoftModel
{
    protected $table = 'system_state';

    protected $primaryKey = 'sys_state_id';

    protected $fillable = [
        'sys_state_code',
        'sys_state_name',
        'sys_state_country_id',
        'sys_state_desc'
    ];

    public function systemCountry()
    {
        return $this->belongsTo(__NAMESPACE__ .'\SystemCountry', 'sys_state_country_id', 'sys_country_id');
    }
}
