<?php

namespace App\Api\V1\Validators;

use App\WarehouseQualifier;

class WarehouseConfigurationMetaValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            '*.whs_meta_value' => 'required',
            '*.whs_qualifier' => 'required',
        ];
    }
}
