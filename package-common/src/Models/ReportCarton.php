<?php

namespace Seldat\Wms2\Models;

class ReportCarton extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rpt_carton';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ctn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'plt_id',
        'asn_dtl_id',
        'gr_dtl_id',
        'item_id',
        'whs_id',
        'cus_id',
        'loc_id',
        'ctn_num',
        'rfid',
        'ctn_sts',
        'ctn_uom_id',
        'ctn_pack_size',
        'loc_code',
        'loc_name',
        'is_damaged',
        'piece_remain',
        'piece_ttl',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted',
        'deleted_at',
        'is_ecom',
        'gr_hdr_id',
        'gr_dt',
        'picked_dt',
        'storage_duration',
        'loc_type_code',
        'sku',
        'size',
        'color',
        'lot',
        'po',
        'expired_dt',
        'uom_code',
        'uom_name',
        'upc',
        'ctnr_id',
        'ctnr_num',
        'length',
        'width',
        'height',
        'weight',
        'cube',
        'volume',
        'return_id',
        'shipped_dt',
        'des',
        'spc_hdl_code',
        'spc_hdl_name',
        'cat_code',
        'cat_name',
        'ucc128',
    ];
}
