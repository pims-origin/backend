<?php

namespace App\Api\V1\Validators;

class ImportForecastValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_id'    => 'required',
            'period'   => 'required',
            'year'      => 'required',
        ];
    }
}
