<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class BOLHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class BOLHdrValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'ship_id'              => 'integer|exists:shipment,ship_id',
            'cus_id'               => 'required|integer|exists:customer,cus_id',
            'bo_label'             => 'required',
            'special_inst'         => 'required',
            'carrier'              => 'required',
            'deli_service'         => 'required',
            'party_acc'            => 'required',
            'cmd_des'              => 'required|integer|exists:commodity,cmd_id',
            'trailer_loaded_by'    => 'required',
            'freight_counted_by'   => 'required',
            'freight_charge_terms' => 'required',
            'fee_terms'            => 'required',
            'trailer_num'          => 'integer',
            'seal_num'             => 'integer',
            'pro_num'              => 'integer',
            'cus_accept'           => 'integer',

            // Item
            'items'                => 'required|array',
            'items.*.odr_id'       => 'required|integer|exists:odr_hdr,odr_id',
            'items.*.cus_odr'      => 'required',
            'items.*.cus_po'       => 'required',
            'items.*.plts'         => 'integer',
            'items.*.pkgs'         => 'integer',
            'items.*.units'        => 'integer',
            'items.*.weight'       => 'numeric',
        ];
    }


}
