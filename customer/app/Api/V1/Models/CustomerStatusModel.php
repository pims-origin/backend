<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\CustomerStatus;

class CustomerStatusModel extends AbstractModel
{
    public function __construct(CustomerStatus $model = null)
    {
        $this->model = ($model) ? : new CustomerStatus();
    }
}