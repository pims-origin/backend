<?php

namespace Seldat\Wms2\Utils;

class SelStr
{
    public static function escapeLike($string)
    {
        $search = ['%', '_'];
        $replace = ['\%', '\_'];

        return str_replace($search, $replace, $string);
    }

    /**
     * @param $maxHdrNum Max Header Num
     *
     * @param string $code
     * @param int $number
     *
     * @return string
     */
    public static function getHdrNum($maxHdrNum, $code, $number = 4)
    {
        $aHdrNum = explode("-", $maxHdrNum);
        $num = strlen(end($aHdrNum));
        $index = (int)end($aHdrNum);

        return $code . "-" . date('ym') . "-" . str_pad(++$index, (empty($num) ? $number : $num), "0", STR_PAD_LEFT);
    }

}