<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 4:59 PM
 */

namespace App\Api\V1\Validators;


class MenuValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'parent_id'       => 'integer',
            'menu_group_id'   => 'required|integer|exists:menu_group',
            'name'            => 'required',
            'description'     => '',
            'url'             => '',
            'display_order'   => 'integer',
            'target'          => '',
            'permission_name' => 'required',
            'icon_class'      => '',
        ];
    }
}
