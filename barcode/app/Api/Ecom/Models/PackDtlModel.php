<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\Ecom\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackDtl;

class PackDtlModel extends AbstractModel
{

    /**
     * WaveHdrModel constructor.
     *
     * @param WavepickHdr|null $model
     */
    public function __construct(PackDtl $model = null)
    {
        $this->model = ($model) ?: new PackDtl();
    }

    public function sumPieceQty($lot, $item_id, $odr_hdr_id)
    {
        $result = DB::table('pack_dtl')
            ->select(DB::raw('SUM(piece_qty) as piece_qty'))
            ->where('item_id', $item_id)
            ->where('lot', $lot)
            ->where('odr_hdr_id', $odr_hdr_id)
            ->where('deleted', 0)
            ->first()
        ;

        return $result['piece_qty'];
    }
}
