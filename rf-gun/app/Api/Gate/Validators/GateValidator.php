<?php

namespace App\Api\Gate\Validators;


class GateValidator extends AbstractValidator
{
    protected function rules()
    {

        return [
            'gate_name' => 'required|string',
            'gate_sts'  => 'required|string',
            'pos'       => 'required|integer',
            'line'      => 'required|integer'
        ];

    }
}
