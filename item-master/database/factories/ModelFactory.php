<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name'  => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(\Seldat\Wms2\Models\Item::class, function ($faker) {
    return [
        //'item_code'    => $faker->randomLetter . $faker->numberBetween(1000, 9000),
        'lot'       => $faker->name,
        'sku'       => uniqid(),
        'size'      => uniqid(),
        'color'     => uniqid(),
        'uom_id'    => $faker->numberBetween(0, 100),
        'pack'      => $faker->numberBetween(0, 100),
        'cus_id'    => $faker->numberBetween(1000, 9000),
        'cus_upc'   => $faker->randomLetter,
        'status'    => 'AC',
        'condition' => $faker->randomLetter,
    ];
});