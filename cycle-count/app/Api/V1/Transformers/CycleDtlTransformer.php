<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\CycleDtl;
use Illuminate\Support\Facades\DB;

class CycleDtlTransformer extends TransformerAbstract
{
    /*
     * Transform CycleHdr
     *
     * @param object $CycleHdr
     * @return array
     */
    public function transform(CycleDtl $cycleDtl)
    {
        $statusData=$this->getAvailableIncludes();
        $key=$cycleDtl->item_id.'.'.$cycleDtl->cus_id.'.'.$cycleDtl->whs_id;
//        $lpn_carton = DB::table('cartons')
//                ->where('whs_id', $cycleDtl->whs_id)
//                ->where('cus_id', $cycleDtl->cus_id)
//                ->where('item_id', $cycleDtl->item_id)
//                ->where('loc_id', $cycleDtl->act_loc_id)
//                ->where('deleted', 0)
//                ->value('lpn_carton');
        return [
            'cycle_dtl_id'       => $cycleDtl->cycle_dtl_id,
            'cycle_hdr_id'       => $cycleDtl->cycle_hdr_id,
            'whs_id'             => $cycleDtl->whs_id,
            'cus_id'             => $cycleDtl->cus_id,
            'item_id'            => $cycleDtl->item_id,
            'sku'                => $cycleDtl->sku,
            'size'               => $cycleDtl->size,
            'color'              => $cycleDtl->color,
            'lot'                => $cycleDtl->lot,
            'pack'               => $cycleDtl->pack,
            'remain'             => $cycleDtl->remain,
            'sys_qty'            => $cycleDtl->sys_qty,
            'act_qty'            => $cycleDtl->act_qty,
            'sys_loc_id'         => $cycleDtl->sys_loc_id,
            'sys_loc_name'       => $cycleDtl->sys_loc_name,
            'act_loc_id'         => $cycleDtl->act_loc_id,
            'act_loc_name'       => $cycleDtl->act_loc_name,
            'cycle_dtl_sts'      => $cycleDtl->cycle_dtl_sts,
            'is_new_sku'         => $cycleDtl->is_new_sku,
            'created_by'         => $cycleDtl->created_by,
            'updated_by'         => $cycleDtl->updated_by,
            'created_at'         => $cycleDtl->created_at,
            'updated_at'         => $cycleDtl->updated_at,
            'sts'                => $cycleDtl->sts,
            'deleted'            => $cycleDtl->deleted,
            'deleted_at'         => $cycleDtl->deleted_at,
            'whs_name'           => object_get($cycleDtl, 'warehouse.whs_name', ''),
            'cus_name'           => object_get($cycleDtl, 'customer.cus_name', ''),
            'cus_code'           => object_get($cycleDtl, 'customer.cus_code', ''),
            'ttlCtn'             => strtoupper(object_get($cycleDtl, 'cycleHdr.count_by',
                '')) == CycleHdrModel::COUNT_BY_CARTON ?
                $cycleDtl->sys_qty : ($cycleDtl->remain ? ceil($cycleDtl->sys_qty / $cycleDtl->remain) : ceil($cycleDtl->sys_qty / $cycleDtl->pack)),
            'ttlPieces'          => strtoupper(object_get($cycleDtl, 'cycleHdr.count_by',
                '')) == CycleHdrModel::COUNT_BY_CARTON ?
                $cycleDtl->sys_qty * $cycleDtl->remain : $cycleDtl->sys_qty,
            'cycle_dtl_sts_name' => CycleDtlModel::$arrCycleDtlStatus[$cycleDtl->cycle_dtl_sts],
            'gr_sts'=>empty($statusData)?null:($statusData->contains($key)?1:0),
            'plt_rfid' => $cycleDtl->plt_rfid ?? $cycleDtl->lpn_carton
        ];
    }
}
