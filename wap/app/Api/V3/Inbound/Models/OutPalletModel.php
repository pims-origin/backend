<?php
namespace App\Api\V3\Inbound\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class OutPalletModel extends AbstractModel
{

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OutPallet $model = null)
    {
        $this->model = ($model) ?: new OutPallet();
    }

}
