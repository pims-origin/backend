<?php

namespace App\Api\V2\Putback\Models;

use Seldat\Wms2\Models\ReturnDtl;

use Seldat\Wms2\Utils\SelArr;

use Illuminate\Support\Facades\DB;

/**
 * Class ReturnDtlModel
 *
 * @package App\Api\V1\Models
 */
class ReturnDtlModel extends AbstractModel
{
    /**
     * ReturnDtlModel constructor.
     *
     * @param ReturnDtl|null $model
     */
    public function __construct(ReturnDtl $model = null)
    {
        $this->model = ($model) ?: new ReturnDtl();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null  $limit
     * @param false $export
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null, $export = false)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['year']) && isset($attributes['month'])) {
            $year  = $attributes['year'];
            $month = $attributes['month'];

        }else{
            $year  = date('Y');
            $month = date('m');
        }

        $month_start = strtotime('first day of this month', mktime(0, 0, 0, $month, 1, $year));
        $month_end   = strtotime('last day of this month',  mktime(0, 0, 0, $month, 1, $year));

        $query->where('created_at', '>=', $month_start);
        $query->where('created_at', '<=', $month_end);

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'created_at') {
                    $attributes['sort']['created_at'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'cus_name') {
                    $attributes['sort']['customer.cus_name'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'sku') {
                    $attributes['sort']['sku'] = $val;
                    unset($attributes['sort'][$key]);
                }
            }
        }

        // // Get
        $this->sortBuilder($query, $attributes);

        if ($export) {
            $models = $query->get();
        } else {
            $models = $query->paginate($limit);
        }

        return $models;
    }
}
