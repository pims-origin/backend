<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\CustomersModel;
use App\Api\V1\Models\DailyInventoryModel;
use App\Api\V1\Models\InventoryModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Transformers\InventoryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Services\DailyInventoryService;
use App\Api\V1\Traits\DailyInventoryTrait;

class DailyInventoryController extends AbstractController
{
    use DailyInventoryTrait;
    protected $systemStateModel;
    protected $InventoryModel;
    protected $dailyInventoryModel;
    protected $customersModel;

    protected $InventoryTransformer;
    protected $systemStateTransformer;

    protected $orderCartonModel;

    protected $dailyInventoryService;

    /*
     ****************************************************************************
     */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->InventoryModel         = new InventoryModel();
        $this->InventoryTransformer   = new InventoryTransformer();
        $this->systemStateModel       = new SystemStateModel();
        $this->systemStateTransformer = new SystemStateTransformer();

        $this->orderCartonModel    = new OrderCartonModel();
        $this->dailyInventoryModel = new DailyInventoryModel();
        $this->customersModel      = new CustomersModel();

        $this->dailyInventoryService = new DailyInventoryService();
    }



    /*
     ****************************************************************************
     */

    /**
     * @param $whs_id
     * @param $cus_id
     * @param Request $request
     */
    public function dailyInventoryReport(
        $whs_id,
        $cus_id,
        Request $request
    ) {
        //date_default_timezone_set(config('constants.timezone_utc_zero'));

        $input      = $request->getQueryParams();
        $limit      = array_get($input, 'limit', PAGING_LIMIT);
        $startDay   = array_get($input, 'select_day', date('Y-m-d'));
        $endDay     = $startDay . ' 23:59:59';
        $betweenDay = [strtotime($startDay . ' UCT'), strtotime($endDay . ' UCT')];
        $reset      = array_get($input, 'reset', false);
        if($reset) {
            $this->correctDailyInventoryByDay($request, $cus_id);
        }
        try {
            $cusInfo = $this->customersModel->getFirstBy('cus_id', $cus_id);
            if (empty($cusInfo)) {
                throw new \Exception(Message::get("BM017", "Customer"));
            }
            $cusName = object_get($cusInfo, 'cus_name', '');
            //Daily Inventory On Rack and Block Stack
            $dailyInventoryReports = $this->dailyInventoryModel
                ->getDailyInventoryReport($whs_id, $cus_id, $betweenDay, $input, [], $limit, [1]);

            //Daily Inventory On Rack for RM Kayu
            $dailyInventoryOnRack = $this->transformDailyInventoryReportViaIsRack($dailyInventoryReports, 1);

            //Subtotal RM Kayu for Daily Inventory On Rack
            $subtotalForDailyInventoryOnRack = $this->transformDailyInventoryReport($dailyInventoryOnRack);
            $dailyInventoryOnRackInfos       = $this->convertNumberFormat($dailyInventoryOnRack);

            // //Daily Inventory Block Stack
            // $dailyInventoryBlockStack = $this->transformDailyInventoryReportViaIsRack($dailyInventoryReports, 0);

            // //Subtotal Block Stack for Daily Inventory
            // $subtotalBlockStackForDailyInventory = $this->transformDailyInventoryReport($dailyInventoryBlockStack);
            // $dailyInventoryBlockStackInfos       = $this->convertNumberFormat($dailyInventoryBlockStack);

            //Total Daily Inventory
            $totalForDailyInventory = $this->transformDailyInventoryReport($dailyInventoryReports);

            $userInfo  = new \Wms2\UserInfo\Data();
            $userInfo  = $userInfo->getUserInfo();
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            $ext = 'xls';
            Excel::create('DailyInventoryReport', function ($excel) use (
                &$dailyInventoryOnRackInfos,
                &$subtotalForDailyInventoryOnRack,
                // &$dailyInventoryBlockStackInfos,
                // &$subtotalBlockStackForDailyInventory,
                &$totalForDailyInventory,
                &$cusName,
                &$printedBy,
                $startDay
            ) {
                // Set sheet 1
                $excel->sheet('Daily Inventory Report', function ($sheet) use (
                    &$dailyInventoryOnRackInfos,
                    &$subtotalForDailyInventoryOnRack,
                    // &$dailyInventoryBlockStackInfos,
                    // &$subtotalBlockStackForDailyInventory,
                    &$totalForDailyInventory,
                    &$cusName,
                    &$printedBy,
                    $startDay
                ) {
                    $totalOnRack     = $dailyInventoryOnRackInfos->count();
                    $totalBlockStack = -1;
                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'Arial',
                            'size' => 11,
                            'bold' => false,
                        ),
                    ))
                        ->row(4, array(
                            'DAILY INVENTORY REPORT',
                        ))->mergeCells('A4:X4')
                        ->cells('A4:X4', function ($cells) {
                            $cells->setFontSize(24)
                                ->setFontWeight('600');
                        })
                        ->row(5, array(
                            'Customer: ' . $cusName,
                        ))
                        ->cells('A5:X5', function ($cells) {
                            $cells->setFontSize(18)
                                ->setFontWeight('bold');
                        })
                        ->mergeCells('A5:v5')
                        ->row(6, [
                            $startDay,
                            '', '', '', '', '', '', '', '', '', '',
                            date('h:i:s a') . ' WB',
                        ])
                        ->cells('A6:X6', function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontWeight('bold italic');
                        })
                        ->mergeCells('A6:K6')
                        ->mergeCells('L6:v6')
                        ->cells('L6:X6', function ($cells) {
                            $cells->setAlignment('right');
                        })
                        ->row(9, [
                            'Item/SKU',
                            'Description',

                            'Size',
                            'Color ',
                            'Length',
                            'Width',
                            'Height',
                            'Weight',

                            'UOM',
                            'Pack',
                        ])
                        ->cells('A9:v9', function ($cells) {
                            $cells->setBorder('1px solid #c0c0c0', '1px solid #ddd', '1px solid #c0c0c0', 'solid');
                        })
                        ->setMergeColumn(array(
                            'columns' => array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'),
                            'rows'    => array(
                                array(9, 11),
                            ),
                        ))
                        ->setWidth('A', 50)
                        ->setWidth('B', 40)
                        ->setWidth('C', 5)
                        ->setWidth('D', 5)
                        ->setWidth('E', 5)
                        ->setWidth('F', 5)
                        ->setWidth('G', 5)
                        ->setWidth('H', 5)
                        ->setWidth('I', 5)
                        ->setWidth('J', 5)
                        //->mergeCells('E9:P9')
                        //->mergeCells('E10:H10')
                        //->mergeCells('I10:L10')
                        //->mergeCells('M10:P10')
                        ->mergeCells('K9:V9')
                        ->mergeCells('K10:N10')
                        ->mergeCells('O10:R10')
                        ->mergeCells('S10:V10')
                        // ->mergeCells('Q10:T10')
                        // ->mergeCells('U10:X10')
                        ->row(10, [
                            '', '', '', '',
                            '', '', '', '','', '',
                            // 'Initial Stock',
                            // '', '', '',
                            'In Activity',
                            '', '', '',
                            'Out Activity',
                            '', '', '',
                            // 'Other Activity',
                            // '', '', '',
                            'Current Stock',
                        ])->row(11, [
                        '', '', '', '',
                        '', '', '', '','', '',
                        // 'Carton',
                        // 'QTY/KG',
                        // 'Pallet',
                        // 'Cube',
                        'Carton',
                        'QTY',
                        'Pallet',
                        'Cube',
                        'Carton',
                        'QTY',
                        'Pallet',
                        'Cube',
                        // 'Carton',
                        // 'QTY/KG',
                        // 'Pallet',
                        // 'Cube',
                        'Carton',
                        'QTY',
                        'Pallet',
                        'Cube',
                    ])->cells('A9:V9', function ($cells) {
                        $cells->setBackground('#e3f1f2')
                            ->setValignment('center')
                            ->setAlignment('center');
                    })->cells('A10:V10', function ($cells) {
                        $cells->setBackground('#e3f1f2')
                            ->setValignment('center')
                            ->setAlignment('center');
                    })->cells('A11:V11', function ($cells) {
                        $cells->setBackground('#e3f1f2')
                            ->setValignment('center')
                            ->setAlignment('center');
                    })
                        ->rows($dailyInventoryOnRackInfos->toArray())
                        ->row($totalOnRack + 12, [
                            '','','','',
                            'Rack Subtotal',
                            '', '','','','',
                            // $subtotalForDailyInventoryOnRack['init_ctns_qty'] ?? '0,00',
                            // $subtotalForDailyInventoryOnRack['init_qty'] ?? '0,00',
                            // $subtotalForDailyInventoryOnRack['init_plt_qty'] ?? '0',
                            // $subtotalForDailyInventoryOnRack['init_cube'] ?? '0,00',

                            $subtotalForDailyInventoryOnRack['in_ctns_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['in_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['in_plt_qty'] ?? '0',
                            $subtotalForDailyInventoryOnRack['in_cube'] ?? '0,00',

                            $subtotalForDailyInventoryOnRack['out_ctns_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['out_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['out_plt_qty'] ?? '0',
                            $subtotalForDailyInventoryOnRack['out_cube'] ?? '0,00',

                            // $subtotalForDailyInventoryOnRack['other_ctns_qty'] ?? '0,00',
                            // $subtotalForDailyInventoryOnRack['other_qty'] ?? '0,00',
                            // $subtotalForDailyInventoryOnRack['other_plt_qty'] ?? '0',
                            // $subtotalForDailyInventoryOnRack['other_cube'] ?? '0,00',

                            $subtotalForDailyInventoryOnRack['cur_ctns_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['cur_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['cur_plt_qty'] ?? '0',
                            $subtotalForDailyInventoryOnRack['cur_cube'] ?? '0,00',
                        ])
                        ->cells('A' . ($totalOnRack + 12) . ':V' . ($totalOnRack + 12), function ($cells) {
                            $cells->setBackground('#e7e7e7');
                        })
                        ->mergeCells('E' . ($totalOnRack + 12) . ':J' . ($totalOnRack + 12))
                        // ->rows($dailyInventoryBlockStackInfos->toArray())
                        // ->row($totalOnRack + 12 + $totalBlockStack + 1, [
                        //     '',
                        //     'Block Stack Subtotal',
                        //     '', '',
                        //     $subtotalBlockStackForDailyInventory['init_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['init_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['init_plt_qty'] ?? '0',

                        //     $subtotalBlockStackForDailyInventory['in_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['in_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['in_plt_qty'] ?? '0',

                        //     $subtotalBlockStackForDailyInventory['out_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['out_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['out_plt_qty'] ?? '0',

                        //     $subtotalBlockStackForDailyInventory['other_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['other_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['other_plt_qty'] ?? '0',

                        //     $subtotalBlockStackForDailyInventory['cur_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['cur_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['cur_plt_qty'] ?? '0',
                        // ])
                        ->cells('A' . ($totalOnRack + 12 + $totalBlockStack + 1) . ':V' . ($totalOnRack + 12 +
                                $totalBlockStack + 1), function ($cells) {
                            $cells->setBackground('#e7e7e7');
                        })
                        ->mergeCells('E' . ($totalOnRack + 12 + $totalBlockStack + 1) . ':J' . ($totalOnRack + 12 +
                                $totalBlockStack + 1))
                        ->appendRow([
                            '','','','',
                            'Total',
                            '', '','','','',
                            // $totalForDailyInventory['init_ctns_qty'] ?? '0,00',
                            // $totalForDailyInventory['init_qty'] ?? '0,00',
                            // $totalForDailyInventory['init_plt_qty'] ?? '0',
                            // $totalForDailyInventory['init_cube'] ?? '0,00',

                            $totalForDailyInventory['in_ctns_qty'] ?? '0,00',
                            $totalForDailyInventory['in_qty'] ?? '0,00',
                            $totalForDailyInventory['in_plt_qty'] ?? '0',
                            $totalForDailyInventory['in_cube'] ?? '0,00',

                            $totalForDailyInventory['out_ctns_qty'] ?? '0,00',
                            $totalForDailyInventory['out_qty'] ?? '0,00',
                            $totalForDailyInventory['out_plt_qty'] ?? '0',
                            $totalForDailyInventory['out_cube'] ?? '0,00',

                            // $totalForDailyInventory['other_ctns_qty'] ?? '0,00',
                            // $totalForDailyInventory['other_qty'] ?? '0,00',
                            // $totalForDailyInventory['other_plt_qty'] ?? '0',
                            // $totalForDailyInventory['other_cube'] ?? '0,00',

                            $totalForDailyInventory['cur_ctns_qty'] ?? '0,00',
                            $totalForDailyInventory['cur_qty'] ?? '0,00',
                            $totalForDailyInventory['cur_plt_qty'] ?? '0',
                            $totalForDailyInventory['cur_cube'] ?? '0,00',

                        ])
                        ->mergeCells('E' . ($totalOnRack + 12 + $totalBlockStack + 1) . ':J' . ($totalOnRack + 12 +
                                $totalBlockStack + 1))
                        ->cells('A' . ($totalOnRack + 12 + $totalBlockStack + 2) . ':V' . ($totalOnRack + 12 +
                                $totalBlockStack + 2), function ($cells) {
                            $cells->setBackground('#fff000');
                        })
                        ->mergeCells('E' . ($totalOnRack + 12 + $totalBlockStack + 2) . ':J' . ($totalOnRack + 12 +
                                $totalBlockStack + 2))
                        ->setBorder('A9:V' . ($totalOnRack + 12 + $totalBlockStack + 2), 'thin')
                        ->cells('A' . ($totalOnRack + 12 + $totalBlockStack + 2) . ':X' . ($totalOnRack + 12 + $totalBlockStack + 2), function ($cells) {
                            $cells->setFontWeight('100');
                        })
                        ->rows(
                            [
                                [], [], [],
                                [date("m/d/Y h:i:s a") . ' By ' . $printedBy],
                            ]
                        );
                });

            })->download($ext);
                //->store($ext, storage_path('excel/exports'));

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    private function transformDailyInventoryReport($items)
    {
        $item = $items->first() ?? null;
        if (!$item) {
            return [];
        }

        return [
            'sku'            => $item['sku'],
            'description'    => $item['description'],
            'uom'            => $item['uom'],
            'pack'           => $item['pack'],
            'init_ctns_qty'  => number_format($items->sum('init_ctns_qty'), 2, '.', ','),
            'init_qty'       => number_format($items->sum('init_qty'), 2, '.', ','),
            'init_plt_qty'   => number_format($items->sum('init_plt_qty'), 0, '.', ','),
            'init_cube'      => number_format($items->sum('init_cube'), 2, '.', ','),

            'in_ctns_qty'    => ceil($items->sum('in_ctns_qty')),
            'in_qty'         => ceil($items->sum('in_qty')),
            'in_plt_qty'     => number_format($items->sum('in_plt_qty'), 0, '.', ','),
            'in_cube'        => number_format($items->sum('in_cube'), 2, '.', ','),

            'out_ctns_qty'   => ceil($items->sum('out_ctns_qty')),
            'out_qty'        => ceil($items->sum('out_qty')),
            'out_plt_qty'    => number_format($items->sum('out_plt_qty'), 0, '.', ','),
            'out_cube'       => number_format($items->sum('out_cube'), 2, '.', ','),

            'other_ctns_qty' => number_format($items->sum('other_ctns_qty'), 2, '.', ','),
            'other_qty'      => number_format($items->sum('other_qty'), 2, '.', ','),
            'other_plt_qty'  => number_format($items->sum('other_plt_qty'), 0, '.', ','),
            'other_cube'     => number_format($items->sum('other_cube'), 2, '.', ','),

            'cur_ctns_qty'   => ceil($items->sum('cur_ctns_qty')),
            'cur_qty'        => ceil($items->sum('cur_qty')),
            'cur_plt_qty'    => number_format($items->sum('cur_plt_qty'), 0, '.', ','),
            'cur_cube'       => number_format($items->sum('cur_cube'), 2, '.', ','),

        ];
    }

    private function transformDailyInventoryReportViaIsRack($items, $isRack)
    {
        return $items->reject(function ($item) use ($isRack) {
            return (int) $item->is_rack !== $isRack;
        })
//            ->sortbydesc('updated_at')
            ->transform(function ($item) {
                $init_cube = $this->calcuCube($item['init_ctns_qty'],$item['length'],$item['width'],$item['height']);
                $in_cube = $this->calcuCube($item['in_ctns_qty'],$item['length'],$item['width'],$item['height']);
                $out_cube = $this->calcuCube($item['out_ctns_qty'],$item['length'],$item['width'],$item['height']);
                $other_cube = $this->calcuCube($item['other_ctns_qty'],$item['length'],$item['width'],$item['height']);
                $cur_cube = $this->calcuCube($item['cur_ctns_qty'],$item['length'],$item['width'],$item['height']);
                return [
                    'item_id'       => $item['item_id'],
                    'sku'            => $item['sku'],
                    'description'    => $item['description'],

                    'size'   => $item['size'],
                    'color'  => $item['color'],
                    'length' => $item['length'],
                    'width'  => $item['width'],
                    'height' => $item['height'],
                    'weight' => $item['weight'],

                    'uom'            => $item['uom'],
                    'pack'           => $item['pack'],
                    'init_ctns_qty'  => $item['init_ctns_qty'],
                    'init_qty'       => $item['init_qty'],
                    'init_plt_qty'   => $item['init_plt_qty'],
                    'init_cube'      => $init_cube,

                    'in_ctns_qty'    => $item['in_ctns_qty'],
                    'in_qty'         => $item['in_qty'],
                    'in_plt_qty'     => $item['in_plt_qty'],
                    'in_cube'        => $in_cube,

                    'out_ctns_qty'   => $item['out_ctns_qty'],
                    'out_qty'        => $item['out_qty'],
                    'out_plt_qty'    => $item['out_plt_qty'],
                    'out_cube'       => $out_cube,

                    'other_ctns_qty' => $item['other_ctns_qty'],
                    'other_qty'      => $item['other_qty'],
                    'other_plt_qty'  => $item['other_plt_qty'],
                    'other_cube'     => $other_cube,

                    'cur_ctns_qty'   => $item['cur_ctns_qty'],
                    'cur_qty'        => $item['cur_qty'],
                    'cur_plt_qty'    => $item['cur_plt_qty'],
                    'cur_cube'       => $cur_cube,
                ];
            });
    }

    private function convertNumberFormat($items)
    {
        return $items->transform(function ($item) {
            return [
                'item'        => $item['item_id'],
                'sku'         => $item['sku'],
                'description' => $item['description'],

                'size'        => $item['size'],
                'color'       => $item['color'],
                'length'      => $item['length'],
                'width'       => $item['width'],
                'height'      => $item['height'],
                'weight'      => $item['weight'],

                'uom'         => $item['uom'],
                'pack'        => $item['pack'],

                'init_ctns_qty'  => $item['init_ctns_qty'],
                'init_qty'       => $item['init_qty'],
                'init_plt_qty'   => $item['init_plt_qty'],
                'init_cube'      => $item['init_cube'],


                'in_ctns_qty'    => ceil($item['in_ctns_qty']),
                'in_qty'         => ceil($item['in_qty']),
                'in_plt_qty'     => $item['in_plt_qty'],
                'in_cube'        => $item['in_cube'],


                'out_ctns_qty'   => ceil($item['out_ctns_qty']),
                'out_qty'        => ceil($item['out_qty']),
                'out_plt_qty'    => $item['out_plt_qty'],
                'out_cube'       => $item['out_cube'],

                 'other_ctns_qty' => $item['other_ctns_qty'],
                 'other_qty'      => $item['other_qty'],
                 'other_plt_qty'  => $item['other_plt_qty'],
                 'other_cube'     => $item['other_cube'],

                'cur_ctns_qty'   => ceil($item['cur_ctns_qty']),
                'cur_qty'        => ceil($item['cur_qty']),
                'cur_plt_qty'    => $item['cur_plt_qty'],
                'cur_cube'       => $item['cur_cube'],
            ];
        });
    }

    public function correctDailyInventory(Request $request) {
        try{
            $input = $request->getQueryParams();
            $fromDate = strtotime($input['from_date'] . ' UCT');
            $toDate = strtotime($input['to_date'] . ' UCT');
            if (!$fromDate || !$toDate) {
                return false;
            }

            $days = ($toDate - $fromDate) / 86400;
            if ($days <= 0) {
                return false;
            }

            for ($i = 0; $i <= $days; $i++) {
                $date = $fromDate + $i * 86400;
                $dateAfter = $date + 86400;
                DB::statement("DELETE FROM daily_inv_rpt where `inv_dt` = ?;", [$date]);
                DB::statement("
                INSERT INTO `daily_inv_rpt` (
                    `whs_id`,
                    `cus_id`,
                    `item_id`,
                    `description`,
                    `sku`,
                    `size`,
                    `color`,
                    `pack`,
                    `uom`,
                    `is_rack`,
                    `inv_dt`,
                    `init_ctns`,
                    `init_qty`,
                    `init_plt`,
                    `init_volume`,
                    `in_ctns`,
                    `in_qty`,
                    `in_plt`,
                    `in_volume`,
                    `out_ctns`,
                    `out_qty`,
                    `out_plt`,
                    `out_volume`,
                    `cur_ctns`,
                    `cur_qty`,
                    `cur_plt`,
                    `cur_volume`,
                    `created_at`,
                    `updated_at`,
                    `lot`,
                    `ctn_type`
                )
                SELECT
                    c.whs_id,                   #whs_id
                    c.cus_id,                   #cus_id
                    c.item_id,                  #item_id
                    c.des,                      #description
                    c.sku,                      #sku
                    c.size,                     #size
                    c.color,                    #color
                    c.ctn_pack_size,            #pack
                    c.uom_code,                 #uom
                    1,                          #is_rack
                    ?,                          #inv_dt
                    COUNT(c.ctn_id),            #init_ctns
                    SUM(c.piece_remain),        #init_qty
                    COUNT(DISTINCT (c.plt_id)), #init_plt
                    IFNULL(SUM(c.volume), 0),   #init_volume
                    0,                          #in_ctns
                    0,                          #in_qty
                    0,                          #in_plt
                    0,                          #in_volume
                    0,                          #out_ctns
                    0,                          #out_qty
                    0,                          #out_plt
                    0,                          #out_volume
                    COUNT(c.ctn_id),            #cur_ctns
                    SUM(c.piece_remain),        #cur_qty
                    COUNT(DISTINCT (c.plt_id)), #cur_plt
                    IFNULL(SUM(c.volume), 0),   #cur_volume
                    UNIX_TIMESTAMP(),           #created_at
                    UNIX_TIMESTAMP(),           #updated_at
                    c.lot,                      #lot
                    IFNULL(c.ctn_type, 'null')  #ctn_type
                FROM
                    cartons c
                WHERE c.created_at < ?
                    AND (
                        (
                            c.shipped_dt >= ?
                            AND c.ctn_sts = 'SH'
                        )
                        OR c.ctn_sts IN ('AC', 'LK', 'PD')
                    )
                GROUP BY c.item_id,
                    c.ctn_type;
            ", [$date, $dateAfter, $dateAfter]);
            }
            echo 'Corrected data successfully';
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function correctDailyInventoryByDay(Request $request, Int $cusId) {
        try{
            $input = $request->getQueryParams();
            $this->dailyInventoryService->correctCustomerDataByDay($cusId, $input);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    protected function calcuCube($gns, $length, $width, $height) {
        return ($gns*($length*$width*$height))/1728;
    }


}
