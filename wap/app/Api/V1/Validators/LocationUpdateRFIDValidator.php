<?php

namespace App\Api\V1\Validators;


class LocationUpdateRFIDValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_rfid' => 'required',
            'loc_code' => 'required',
        ];
    }
}
