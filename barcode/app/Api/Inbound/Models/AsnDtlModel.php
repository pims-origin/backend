<?php

namespace App\Api\Inbound\Models;

use App\MyHelper;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;


class AsnDtlModel extends AbstractModel
{

    protected $asnHdr;

    protected $palletModel;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnDtl();
        $this->asnHdr = new AsnHdr();
        $this->palletModel = new PalletModel();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
    }


    /**
     * @param $asnHrdId
     * @return Collection
     */
    public function getContainersByAsn($asnHrdId)
    {
        $query = $this->make(['container']);
        $query->where('asn_hdr_id', $asnHrdId);
        $query->groupBy('ctnr_id');
        $asnDtls = $query->get();

        $containers = [];
        if (!$asnDtls->isEmpty()) {
            foreach ($asnDtls as $asnDtl) {
                $containers[] = $asnDtl->container;
            }
        }

        return collect($containers);
    }

    /**
     * @param $asnHrdId
     * @return int
     */
    public function calContainersTtlPieceByAsn($asnHrdId)
    {
        $query = $this->model
            ->select('asn_dtl_ctn_ttl', 'asn_dtl_pack', 'item_id')
            ->where('asn_hdr_id', $asnHrdId)->get();


        $ttlPiece = 0;
        if (!$query->isEmpty()) {
            foreach ($query as $asnDtl) {
                $ttlPiece += $asnDtl->asn_dtl_ctn_ttl * $asnDtl->asn_dtl_pack;
            }
        }

        return $ttlPiece;
    }

    /**
     * @param $asnHrdId
     * @return int
     */
    public function calContainersTtlCtnByAsn($asnHrdId)
    {
        $query = $this->model
            ->select('asn_dtl_ctn_ttl')
            ->where('asn_hdr_id', $asnHrdId)->get();


        $ttlCtn = 0;
        if (!$query->isEmpty()) {
            foreach ($query as $asnDtl) {
                $ttlCtn += $asnDtl->asn_dtl_ctn_ttl;
            }
        }

        return $ttlCtn;
    }
    /**
     * @param $asnHrdId
     * @param $ctnrId
     * @return array
     */
    public function getAsnDtlIdItemIdByHeaderAndContainer($asnHrdId, $ctnrId)
    {
        $collections = $this->model
            ->where('asn_hdr_id', $asnHrdId)
            ->where('ctnr_id', $ctnrId)
            ->select('asn_dtl_id', 'item_id')
            ->get();

        $result = [];
        if (!$collections->isEmpty()) {
            foreach ($collections as $row) {
                $result[$row->asn_dtl_id] = $row->item_id;
            }
        }

        return $result;
    }

    public function getTotalItem($item_id)
    {
        return $this->model->where('item_id', $item_id)->sum(DB::raw('asn_dtl_pack * asn_dtl_ctn_ttl'));
    }

    public function getTotalInfoFromAsnDtlByHeader($asnHrdId)
    {
        $row = $this->model
            ->select(DB::raw('count(1) as itm_ttl,
                                          count(distinct ctnr_id) as ctnr_ttl,
                                          sum(asn_dtl_ctn_ttl) as ctn_ttl'))
            ->where('asn_hdr_id', $asnHrdId)
            ->take(1)
            ->get()
            ->first();

        if (!$row) {
            $row = [
                'itm_ttl'  => 0,
                'ctnr_ttl' => 0,
                'ctn_ttl'  => 0,
            ];
        } else {
            $row = $row->toArray();
        }

        return $row;
    }

    public function countContainerOfAsn($asnHrdId)
    {
        return $this->model->where('asn_hdr_id', $asnHrdId)
            ->select('ctnr_id')
            ->groupBy('ctnr_id')
            ->get()
            ->count();
    }

    public function search($attributes, array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        // search asn po
        if (isset($attributes['asn_dtl_po'])) {
            $query->where('asn_dtl_po', 'like', "%" . SelStr::escapeLike($attributes['asn_dtl_po']) . "%");
        }
        $asnHdr = $this->asnHdr;
        $query->whereHas('asnHdr', function ($query) use ($attributes, $asnHdr) {
            // search whs id
            if (isset($attributes['whs_id'])) {
                $query->where('whs_id', (int)$attributes['whs_id']);
            }

            // search asn number
            if (isset($attributes['asn_hdr_num'])) {
                $query->where('asn_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['asn_hdr_num']) . "%");
            }

            // search customer
            if (isset($attributes['cus_id'])) {
                $query->where('cus_id', (int)$attributes['cus_id']);
            }

            // search asn status
            if (isset($attributes['asn_sts'])) {
                $query->where('asn_sts', $attributes['asn_sts']);
            }

            // Search by expected date
            if (isset($attributes['asn_hdr_ept_dt'])) {
                $query->where(
                    DB::raw('DATE_FORMAT(FROM_UNIXTIME(asn_hdr_ept_dt), "%m/%d/%Y")'),
                    $attributes['asn_hdr_ept_dt']
                );
            }

            $asnHdr->filterData($query, true);

        });

        // search container
        $query->whereHas('container', function ($query) use ($attributes) {
            if (isset($attributes['ctnr_num'])) {
                $query->where('ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
            }
        });

        // search item
        $query->whereHas('item', function ($query) use ($attributes) {
            if (isset($attributes['dtl_sku'])) {
                $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['dtl_sku']) . "%");
            }
        });

        // Get X-doc
        if (!empty($attributes['xdoc']) && !empty($attributes['xdoc']) === true) {
            $query->select(DB::raw('concat(asn_hdr_id, "-", ctnr_id) as asn_ctnr, sum(asn_dtl_crs_doc) as xdoc'));
            $query->groupBy(['asn_hdr_id', 'ctnr_id']);
        } else {
            $query->groupBy('asn_hdr_id');
        }
        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }

    public function getListASN($attributes, $limit = 20)
    {
        $cusIds = Data::getCurrentCusIds();
        $query = $this->model
            ->select('asn_hdr.asn_hdr_id', 'asn_hdr.cus_id', 'container.ctnr_id', 'asn_sts_name', 'asn_sts',
                'asn_hdr_num', 'asn_hdr_ept_dt', 'cus_name', 'asn_dtl.ctnr_num', 'asn_hdr_ref', 'asn_hdr.created_at',
                DB::raw('count(asn_dtl.item_id) as num_sku'), 'customer.cus_code'
            )
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
            ->join('container', 'container.ctnr_id', '=', 'asn_dtl.ctnr_id')
            ->join('customer', 'customer.cus_id', '=', 'asn_hdr.cus_id')
            ->join('asn_status', 'asn_status.asn_sts_code', '=', 'asn_hdr.asn_sts')
            ->leftJoin('gr_hdr', function($join){
                $join->on('gr_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
                    ->on('gr_hdr.ctnr_id', '=', 'asn_dtl.ctnr_id')
                ;
            })
            ->whereRaw("(gr_hdr.created_from = 'GUN' OR gr_hdr.created_from IS NULL)")
            ->where('asn_hdr.whs_id', $attributes['whs_id'])
            ->whereIn('asn_hdr.cus_id', $cusIds)
        ;

        $query = $query -> whereIn('asn_dtl.asn_dtl_sts', [
            Status::getByKey('ASN_DTL_STS', 'NEW'),
            Status::getByKey('ASN_DTL_STS', 'RECEIVING')
        ]);

        if(isset($attributes['cus_id']))
            $query = $query -> where('asn_hdr.cus_id', $attributes['cus_id']);

        if(isset($attributes['ctnr_id']))
            $query = $query -> where('asn_dtl.ctnr_id', $attributes['ctnr_id']);

        $query->groupBy('asn_hdr.asn_hdr_id', 'container.ctnr_id');
        $query->orderBy('asn_hdr.updated_at', 'DESC');

        return $query->paginate($limit);
    }

    public function getListItem($asn_hdr_id, $ctnr_id)
    {
        $query = $this->model
            ->select('sku', 'size', 'color', 'pack', 'asn_dtl_ctn_ttl', 'asn_dtl_pack', 'asn_dtl_sts', 'asn_dtl_lot',
                'asn_sts_name', 'item.item_id', 'asn_dtl.asn_dtl_id', 'asn_dtl.uom_code', 'asn_dtl_qty_ttl',
                'asn_dtl_crs_doc')
            ->join('item', 'item.item_id', '=', 'asn_dtl.item_id')
            ->join('asn_status', 'asn_status.asn_sts_code', '=', 'asn_dtl.asn_dtl_sts')
            ->where('asn_hdr_id', $asn_hdr_id)
            ->where('ctnr_id', $ctnr_id);

        return $query->get();
    }

    public function countAsnDtlNotReceived($asnHdrId)
    {
        return $this->model->where('asn_hdr_id', $asnHdrId)
            ->where('asn_dtl_sts', '<>', Status::getByKey('ASN_DTL_STS', 'RECEIVED'))
            ->count();
    }

    public function countAsnDtlNotNew($asnHdrId)
    {
        return $this->model->where('asn_hdr_id', $asnHdrId)
            ->where('asn_dtl_sts', '<>', Status::getByKey('ASN_DTL_STS', 'NEW'))
            ->count();
    }

    public function countAllAsnDtlReceived($asnHdrId, $ctnrId)
    {
        return $this->model
            ->where('asn_hdr_id', $asnHdrId)
            ->where('ctnr_id', $ctnrId)
            ->where('asn_dtl_sts', '<>', Status::getByKey('ASN_DTL_STS', 'RECEIVED'))
            ->count();
    }

    public function addPallet($attribute, $asnHdr, $grHdr, $grDtl, $item, $ctn_qty)
    {
        $plt_num = (!empty($attribute['lpn']))?$attribute['lpn']:null;
        if(!empty($attribute['loc_code'])) {
            $loc_plt_num = $this->palletModel->getFirstWhere(['loc_code'=>$attribute['loc_code']]);
            $plt_num = (!empty($loc_plt_num))?$loc_plt_num->plt_num:$plt_num;
        }
        $pallet = null;

        $userId = Data::getCurrentUserId();

        if($plt_num != null) {
            $pallet = $this->palletModel->getFirstWhere([
                "plt_num" => $plt_num,
                "whs_id" => $asnHdr->whs_id
            ]);

            if(empty($pallet)) {
                $pallet = $this->palletModel->create([
                    "cus_id"            => $asnHdr->cus_id,
                    "whs_id"            => $asnHdr->whs_id,
                    "plt_num"           => $plt_num,
                    "rfid"              => $plt_num,
                    "gr_hdr_id"         => $grHdr->gr_hdr_id,
                    "gr_dtl_id"         => $grDtl->gr_dtl_id,
                    "ctn_ttl"           => $ctn_qty,
                    "item_id"           => $item->item_id,
                    "pack"              => $item->pack,
                    "sku"               => $item->sku,
                    "size"              => $item->size,
                    "color"             => $item->color,
                    "lot"               => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                    "storage_duration"  => 0,
                    "plt_sts"           => 'RG',
                    "is_movement"       => 0,
                    "uom"               => $item->uom_id,
                    "dmg_ttl"           => 0,
                    "init_ctn_ttl"      => $ctn_qty,
                    "mixed_sku"         => 0,
                    "is_full"           => 0,
                    "init_piece_ttl"    => $ctn_qty*$item->pack
                ]);
                $cus_name = '';
                $cus_code = '';
                if(!empty($asnHdr->cus_id)) {
                    $cusInfo = DB::table('customer')->where('cus_id',$asnHdr->cus_id)->first();
                    $cus_name = array_get($cusInfo,'cus_name','');
                    $cus_code = array_get($cusInfo,'cus_code','');
                }

                DB::table("rpt_pallet")->insert([
                    "plt_id"            => $pallet->plt_id,
                    "cus_id"            => $asnHdr->cus_id,
                    "whs_id"            => $asnHdr->whs_id,
                    "plt_num"           => $plt_num,
                    "rfid"              => $plt_num,
                    "gr_hdr_id"         => $grHdr->gr_hdr_id,
                    "gr_dtl_id"         => $grDtl->gr_dtl_id,
                    "ctn_ttl"           => $ctn_qty,
                    "item_id"           => $item->item_id,
                    "pack"              => $item->pack,
                    "sku"               => $item->sku,
                    "size"              => $item->size,
                    "color"             => $item->color,
                    "lot"               => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                    "description"       => $item->description,
                    "storage_duration"  => 0,
                    "plt_sts"           => 'RG',
                    "is_movement"       => 0,
                    "uom"               => $item->uom_id,
                    "dmg_ttl"           => 0,
                    "init_ctn_ttl"      => $ctn_qty,
                    "mixed_sku"         => 0,
                    "is_full"           => 0,
                    "init_piece_ttl"    => $ctn_qty*$item->pack,
                    'cus_name'          => $cus_name,
                    'cus_code'          => $cus_code,
                    'current_ctns'      => $ctn_qty,
                    'created_at'        => time(),
                    'updated_at'        => time(),
                    'created_by'        => $userId,
                    'updated_by'        => $userId,
                    'deleted'           => 0,
                    'deleted_at'        => 915148800
                ]);
            } else {
                // Check pallet
                if(!empty($pallet->gr_hdr_id) && $pallet->gr_hdr_id != $grHdr->gr_hdr_id) {
                    return ["is_error" => 1,"msg" => "This LPN is belong to another Good Receipt!"];
                }

                if($pallet->cus_id != $grHdr->cus_id && $pallet->plt_sts != 'NW') {
                    return ["is_error" => 1,"msg" => "This LPN is belong to another Customer!"];
                }

                $cartons_plt = DB::table("cartons")
                    ->join("item", "item.item_id","=", "cartons.item_id")
                    ->where("cartons.plt_id", $pallet->plt_id)->where("cartons.deleted", 0)
                    ->select("item.spc_hdl_code")->first();

                if(!empty($cartons_plt)) {
                    if($cartons_plt['spc_hdl_code'] != $item->spc_hdl_code) {
                        return [
                            "is_error" => 1,
                            "msg" => "This Pallet have item different special handle code with this sku!"
                        ];
                    }
                }

                if(!in_array($pallet->plt_sts,['RG','NW','AC'])) {
                    return ["is_error" => 1,"msg" => "This LPN is not active!"];
                }
                if($pallet->plt_sts == 'NW') {
                    $pallet->cus_id =  $asnHdr->cus_id;
                }
                $pallet->ctn_ttl   += $ctn_qty;
                $pallet->plt_sts    = "RG";
                $pallet->init_piece_ttl += $ctn_qty*$item->pack;
                $pallet->gr_hdr_id  = $grHdr->gr_hdr_id;
                $pallet->gr_dtl_id  = $grDtl->gr_dtl_id;
                $pallet->cus_id     = $asnHdr->cus_id;
                $pallet->save();

                DB::table("rpt_pallet")->where('plt_id', $pallet->plt_id)->update([
                    'ctn_ttl'               =>  sprintf('`ctn_ttl` %s %d', '+', $ctn_qty),
                    'plt_sts'               =>  "RG",
                    'init_piece_ttl'        =>  sprintf('`init_piece_ttl` %s %d', '+', $ctn_qty*$item->pack),
                    'gr_hdr_id'             =>  $grHdr->gr_hdr_id,
                    'gr_dtl_id'             =>  $grDtl->gr_dtl_id,
                    'cus_id'                =>  $asnHdr->cus_id,
                    'updated_at'            =>  time(),
                    'updated_by'            =>  $userId
                ]);
            }
        }

        return $pallet;
    }
}
