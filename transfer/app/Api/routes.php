<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
    $middleware[] = 'setWarehouseTimezone';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V1'];
        });

        $api->get('/list-mz-location',
            ['action' => "viewXFER", 'uses' => 'LocationController@listMzLocation']);
        $api->get('/list-act-mz-location',
            ['action' => "viewXFER", 'uses' => 'LocationController@listActMzLocation']);
        $api->get('/list-item-location',
            ['action' => "viewXFER", 'uses' => 'LocationController@listItemLocation']);
        $api->get('/list-all-xfer',
            ['action' => "viewXFER", 'uses' => 'XferController@listAllWvXfer']);
        $api->get('/list-all-ticket',
            ['action' => "viewXFER", 'uses' => 'XferController@listAllTicket']);
        $api->get('/item-info',
            ['action' => "viewXFER", 'uses' => 'ItemController@itemInfo']);
        $api->get('/detail-xfer',
            ['action' => "viewXFER", 'uses' => 'XferController@detailXfer']);
        $api->get('/detail-ticket',
            ['action' => "viewXFER", 'uses' => 'XferController@detailTicket']);
        $api->get('/download-ticket',
            ['action' => "viewXFER", 'uses' => 'XferController@downloadTicket']);
        $api->post('/create-xfer-ticket',
            ['action' => "createXFER", 'uses' => 'XferController@createXferTicket']);
        $api->post('/create-xfer-record',
            ['action' => "createXFER", 'uses' => 'XferController@createXferRecord']);
        $api->put('/edit-xfer-record',
            ['action' => "editXFER", 'uses' => 'XferController@editXfer']);
        $api->put('/update-xfer',
            ['action' => "editXFER", 'uses' => 'XferController@updateXfer']);
        $api->get('/get-avail-qty/{itemId:[0-9]+}/{locId:[0-9]+}/{cusId:[0-9]+}',
            ['action' => "viewXFER", 'uses' => 'XferController@getAvailQty']);

        $api->post('/transfer-zone',
            ['action' => "createXFER", 'uses' => 'ZoneController@tranfer']);
    });
});
