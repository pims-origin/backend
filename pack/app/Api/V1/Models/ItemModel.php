<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ItemModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new Item();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (isset($attributes['cus_id'])) {
            $query = $query->where('cus_id', $attributes['cus_id']);
        }
        $arrLike = ['color', 'sku', 'size', 'lot'];
        $arrEqual = [
            'item_id',
            'cus_id',
        ];
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query = $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual)) {
                    $query = $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        $models = $query->paginate($limit);

        return $models;
    }
}
