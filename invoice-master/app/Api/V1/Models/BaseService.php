<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class BaseService
{
    /**
     * @var Client
     */
    protected $client;
    protected $_baseUrl;
    protected $_apiName = "";

    /**
     * BaseService constructor.
     * @param $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $baseUrl = $baseUrl ?: env('API_DATA_LAYER');
        $this->_baseUrl = $baseUrl;
        $this->client = new Client([
            'base_uri' => $baseUrl,
            'headers' => [
                'Authorization' => $request->getHeader('Authorization')
            ],
            'http_errors' => false
        ]);
    }
    
    protected function getUri()
    {
        return $this->_baseUrl . $this->_apiName;
    }
    
    /**
     * @param $json
     * @param string $type
     * @return array
     */
    public function responseToArray($json, $type = 'data')
    {
        return \json_decode($json, true)[$type];
    }
    
    /**
     * 
     * @param string $method post|get|put|delete
     * @param int $countryId
     * @param string $queryStr
     * @param array $postParam
     * 
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function commonCall($method, $countryId, $queryStr, $postParam)
    {
        $uri = $this->getUri();
        $method = strtolower($method);
        if (!empty($countryId)) {
            $uri .= "/$countryId";
        }
        if (!empty($queryStr)) {
            $uri = sprintf('%s?%s', $uri, $queryStr);
        }
    
        $options = [];
        if (!empty($postParam)) {
            $options = [
                            'form_params' => $postParam
            ];
        }
    
        return $this->client->$method($uri, $options);
    }
}
