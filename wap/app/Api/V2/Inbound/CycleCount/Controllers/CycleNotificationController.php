<?php

namespace App\Api\V2\Inbound\CycleCount\Controllers;

use App\Api\V2\Inbound\Models\CycleCountNotificationModel;
use App\Api\V2\Inbound\Models\CartonModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V2\Inbound\CycleCount\Validators\CycleCountNotificationValidator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SystemBug;
use App\Api\V1\Models\Log;
use Illuminate\Support\Collection;
use App\Api\V1\Models\EventTrackingModel;
use Illuminate\Http\Response as IlluminateResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CycleNotificationController extends AbstractController
{

    /**
     * @var CycleCountNotificationModel
     */
    protected $model;

    protected $cartonModel;

    protected $cycleCountNotificationModel;

    /**
     * @var CycleCountNotificationValidator
     */
    protected $validator;

    /**
     * @var $transform
     */
    protected $transform;

    /**
     * @var CycleCountNotificationValidator
     */
    protected $cycleCountNotificationTransformer;

    /**
     * CycleNotificationController constructor.
     *
     * @param CycleCountNotificationModel $model
     * @param CycleCountNotificationValidator $validator
     * @param CycleCountNotificationModel $cycleCountNotificationModel
     * @param CartonModel $cartonModel
     */
    public function __construct(
        CycleCountNotificationModel $model,
        CycleCountNotificationValidator $validator,
        CycleCountNotificationModel $cycleCountNotificationModel,
        CartonModel $cartonModel
    ) {
        $this->model = $model;
        $this->validator = $validator;
        $this->cycleCountNotificationModel = $cycleCountNotificationModel;
        $this->cartonModel = $cartonModel;
    }

    /**
     * @param Request $request
     * @param $whsId
     *
     * @return array|IlluminateResponse
     */
    public function addCycleCountNotification(
        Request $request,
        $whsId
    ) {
        $input = $request->getParsedBody();
        $requireFieldsValidate = $this->validator->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['messages'][] = [
                'status_code' => -1,
                'msg'         => $requireFieldsValidate
            ];

            return $msg;
        }
        $owner = $transaction = "";
        $locId = array_get($input, 'loc_id', '');
        $itemId = array_get($input, 'item_id', '');
        $lot = array_get($input, 'lot', '');
        $pack = array_get($input, 'pack', '');
        $locInfo = $this->cycleCountNotificationModel->getLocationById($whsId, $locId);
        if (!$locInfo) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['messages'][] = [
                'status_code' => -1,
                'msg'         => sprintf("Location is not existed.")
            ];

            return $msg;
        }
        $locCode = array_get($locInfo, 'loc_code', '');
        $itemInfo = $this->cycleCountNotificationModel->getInfoItem($whsId, $itemId, $lot, $pack);

        $ccnExisted = $this->model->getExistedCCN($whsId, $locId, $itemId, $lot, $pack);
        $ccnID = array_get($ccnExisted, 'ccn_id', '');

        if (true !== ($locNotBelong = $this->model->checkWhsLocationBelong($whsId, $locCode))) {
            $locStr = implode(', ', $locNotBelong);
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['messages'][] = [
                'status_code' => -1,
                'msg'         => sprintf("Location %s not belong to warehouse %s", $locStr, $whsId)
            ];

            return $msg;
        }

        //check duplicate location
        $duplicateInfo = '';
        $duplicateLocInfo = $this->model->checkCcIdDuplication($whsId, $input, $ccnID);
        if ($duplicateLocInfo) {
            $duplicateInfo .= '  ' . $locCode . ':' . $itemId . ':' . $lot . ':' . $pack;
        }

        if ($duplicateInfo) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['messages'][] = [
                'status_code' => -1,
                'msg'         => sprintf("Cycle Count Notification duplicated, (Location:Item:Lot:Pack): (%s).",
                    trim($duplicateInfo))
            ];

            return $msg;
        }

        try {
            $url = "/v2/whs/{$whsId}/cycle-count-notification";
            Log:: info($request, $whsId, [
                'evt_code'     => 'ACC',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Cycle Count Notification'
            ]);

            DB::beginTransaction();
            if (!$itemInfo) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = [];
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => sprintf("Item out of inventory for Cycle Count.")
                ];

                return $msg;
            }
            $params = [
                "loc_id"      => $locId,
                "loc_code"    => $locCode,
                "cc_ntf_sts"  => CycleCountNotificationModel::CCN_STATUS_NEW,
                "reason"      => "WAP",
                "item_id"     => $itemId,
                "sku"         => array_get($itemInfo, 'sku', ''),
                "lot"         => $lot,
                "pack"        => $pack,
                "size"        => array_get($itemInfo, 'size', ''),
                "color"       => array_get($itemInfo, 'color', ''),
                "cus_id"      => array_get($itemInfo, 'cus_id', ''),
                "uom_code"    => array_get($itemInfo, 'uom_code', ''),
                "uom_name"    => array_get($itemInfo, 'uom_name', ''),
                "remain_qty"  => array_get($itemInfo, 'remain', ''),
                "des"         => array_get($itemInfo, 'des', ''),
                "cc_ntf_date" => time(),
                "whs_id"      => $whsId,

            ];

            if ($ccnID) {
                $params['ccn_id'] = $ccnID;
                //update
                $data = $this->model->update($params);
            } else {
                //create
                $data = $this->model->create($params);
            }

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['messages'][] = [
                'status_code' => 1,
                'msg'         => sprintf("Cycle count Notification created successfully!")
            ];
            $msg['data'] = [$data];

            return $msg;

        } catch (\Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => "/v2/whs/{$whsId}/cycle-count-notification",
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    /**
     * Check duplication loc_code
     *
     * @param type $input
     *
     * @throws \Exception
     */
    public function checkDupLocCode($input)
    {
        $locCodeArr = array_column($input, 'loc_code');
        $locCodeArr2 = array_flip($locCodeArr);
        if (count($locCodeArr) != count($locCodeArr2)) {
            $count = array_count_values($locCodeArr);
            $dupArr = [];
            foreach ($count as $locCode => $num) {
                if ($num > 1) {
                    $dupArr[] = $locCode;
                }
            }
            $dupString = implode(', ', $dupArr);

            throw new \Exception("Duplicate loc_code: {$dupString}");

        }
    }

    /**
     * @param $input
     *
     * @throws \Exception
     */
    public function checkInputtingDataDup($input)
    {
        $checkArray = [];
        foreach ($input as $data) {
            $string = $data['loc_code'] . ':' . $data['item_id'] . ':' . $data['lot'] . ':' . $data['pack'] . ':' .
                $data['remain_qty'];
            array_push($checkArray, $string);

        }
        $locCodeArr = $checkArray;
        $locCodeArr2 = array_flip($checkArray);

        if (count($locCodeArr) != count($locCodeArr2)) {
            $count = array_count_values($locCodeArr);
            $dupArr = [];
            foreach ($count as $locCode => $num) {
                if ($num > 1) {
                    $dupArr[] = $locCode;
                }
            }
            $dupString = implode(', ', $dupArr);

            throw new \Exception("Your inputting duplicated (Location:Item:Lot:Pack:RemainQty): {$dupString}");

        }
    }

    /**
     * @param $whs_id
     * @param Request $request
     * @param CycleCountNotificationTransformer $cycleCountNotificationTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function index(
        $whs_id,
        Request $request,
        CycleCountNotificationTransformer $cycleCountNotificationTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        if (!empty($input['export']) && $input['export'] == 1) {
            $this->export($whs_id, $input);
            die;
        }
        //$input['gr_sts'] = 'RE';
        try {
            $cycleCountNotificationInfo = $this->model->search($whs_id, $input, [], array_get($input, 'limit'));

            return $this->response->paginator($cycleCountNotificationInfo, $cycleCountNotificationTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
