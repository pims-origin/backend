<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 8/19/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\CustomerConfig;


class CustomerConfigModel extends AbstractModel
{
    /**
     * @param CustomerConfig $model
     */
    public function __construct(CustomerConfig $model = null)
    {
        $this->model = ($model) ?: new CustomerConfig();
    }


}