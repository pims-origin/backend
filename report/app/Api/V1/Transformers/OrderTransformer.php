<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

class OrderTransformer extends AbstractTransformer
{
    public function transform(OrderHdr $order)
    {
        $details = object_get($order, 'details', NULL);

        return [
            'cus_name' => object_get($order, 'customer.cus_name', ''),
            'user_name' => trim(object_get($order, 'csrUser.first_name', NULL) .
                    ' ' . object_get($order, 'csrUser.last_name', NULL)),
            'odr_sts' => $order->odr_sts,
            'odr_type' => Status::getByKey('Order-Type', $order->odr_type),
            'odr_num' => $order->odr_num,
            'cus_po' => $order->cus_po,
            'cus_odr_num' => $order->cus_odr_num,
            'ship_to_name' => $order->ship_to_name,
            'ship_to_address' => trim($order->ship_to_add_1 . ' ' .
                    $order->ship_to_add_2),
            'ship_to_city' => $order->ship_to_city,
            'ship_to_state' => $order->ship_to_state,
            'ship_to_zip' => $order->ship_to_zip,
            'ship_to_country' => $order->ship_to_country,
            'whs_name' => object_get($order, 'warehouse.whs_name', ''),
            'qty' => $details->sum('qty'),
            'pieces' => $details->sum('piece_qty'),
            'odr_req_dt' => $this->getDate($order->odr_req_dt),
            'ship_by_dt' => $this->getDate($order->ship_by_dt),
            'cancel_by_dt' => $this->getDate($order->cancel_by_dt),
            'carrier' => $order->carrier,
            'cus_notes' => $order->cus_notes,
        ];
    }

    /*
    ***************************************************************************
    */

}
