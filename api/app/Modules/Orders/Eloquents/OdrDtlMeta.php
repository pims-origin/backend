<?php

namespace App\Modules\Orders\Eloquents;

class OdrDtlMeta extends BaseSoftModel
{
    protected $table = 'odr_dtl_meta';

    protected $primaryKey = 'odr_dtl_id';

    protected $fillable = [
        'qualifier',
        'odr_dtl_id',
        'value'
    ];
}
