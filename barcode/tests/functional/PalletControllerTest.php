<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Dingo\Api\Http\Response;
use Seldat\Wms2\Utils\Message;

class PalletControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var AsnHdr
     */
    protected $asnHdr;

    /**
     * @var AsnDtl
     */
    protected $asnDt;

    /**
     * @var GoodsReceipt
     */
    protected $goodsReceiptId;

    /**
     * @var GoodsReceiptDetail
     */
    protected $goodsReceiptDetail;

    /**
     * @var Pallet
     */
    protected $pallet;

    protected $asnHdrId;

    protected $asnDtlIds;

    protected $warehouseId;

    protected $cusId;

    /**
     * @return string
     */
    public function getEndPoint()
    {
        return "/v1/";
    }

    public function setUp()
    {
        parent::setUp();
        // ASN
        $this->asnHdr = factory(Seldat\Wms2\Models\AsnHdr::class)->create();

        $this->warehouseId = $this->asnHdr->whs_id;
        $this->cusId = $this->asnHdr->cus_id;

        // ASN Detail
        $this->asnDt = factory(Seldat\Wms2\Models\AsnDtl::class)->create([
            'asn_hdr_id' => $this->asnHdr->asn_hdr_id
        ]);

        // Goods Receipt
        $this->goodsReceiptId = factory(Seldat\Wms2\Models\GoodsReceipt::class)->create([
            'asn_hdr_id' => $this->asnHdr->asn_hdr_id,
            'ctnr_id'    => $this->asnDt->ctnr_id,
            'cus_id'     => $this->asnHdr->cus_id,
            'whs_id'     => $this->asnHdr->whs_id
        ])->gr_hdr_id;

        $this->goodsReceiptDetail = factory(Seldat\Wms2\Models\GoodsReceiptDetail::class)->create([
            'asn_dtl_id'         => $this->asnDt->asn_dtl_id,
            'gr_hdr_id'          => $this->goodsReceiptId,
            'gr_dtl_ept_ctn_ttl' => $this->asnDt->asn_dtl_ctn_ttl,
            'gr_dtl_act_ctn_ttl' => $this->asnDt->asn_dtl_ctn_ttl,
        ]);
    }

    public function test_Update_Ok()
    {
        // Create before Show
        $palletFrom = factory(Pallet::class)->create();
        $from_loc_id = $palletFrom->loc_id;

        $loc_to = factory(Location::class)->create();
        $to_loc_id = $loc_to->loc_id;

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "relocations", [
            'from_loc_id' => $from_loc_id,
            'to_loc_id'   => $to_loc_id,
        ]);

        // Case 1: Response code is 200 ok
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);

    }

    public function test_Update_DuplicateNum_Fail()
    {
        // Create before Show
        $palletFrom = factory(Pallet::class)->create();
        $from_loc_id = $palletFrom->loc_id;

        $palletTo = factory(Pallet::class)->create();
        $to_loc_id = $palletTo->loc_id;
        $toLocName = $palletTo->loc_name;

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "relocations", [
            'from_loc_id' => $from_loc_id,
            'to_loc_id'   => $to_loc_id,
        ]);

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message:Locations already have pallets. Allow to overwrite current pallets..
        //$this->assertEquals(
        //    Message::get("BM029", "$toLocName"),
        //    json_decode($data->content(), true)['errors']['message']
        //);
    }

    public function test_Update_CurrentLocation_NotExit_Fail()
    {
        // Create before Show
        $palletFrom = factory(Pallet::class)->create();
        $from_loc_id = $palletFrom->loc_id;

        $palletTo = factory(Pallet::class)->create();
        $to_loc_id = $palletTo->loc_id;

        //test update
        $data = $this->call('PUT', $this->getEndPoint() . "relocations", [
            'from_loc_id' => "999999999999999",
            'to_loc_id'   => $to_loc_id,
        ]);

        // Case 1: Response code is 500 internal Server Name:
        $this->assertEquals(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR, $data->status()); //500=500

        //Case 2: show message: "Current location is not existed in pallet."
        $this->assertEquals(
            Message::get("BM017", "Current Location"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_Update_CurrentLocation_NotHavePallet_Fail()
    {
        // Create before Show
        $palletTo = factory(Pallet::class)->create();
        $to_loc_id = $palletTo->loc_id;

        $CartonData = factory(Carton::class)->create();
        $loc_id = $CartonData->loc_id;

        //test update
        $data = $this->call('PUT', $this->getEndPoint() . "relocations", [
            'from_loc_id' => $loc_id,
            'to_loc_id'   => $to_loc_id,
        ]);

        // Case 1: Response code is 500 internal Server Name:
        $this->assertEquals(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR, $data->status()); //500=500

        //Case 2: show message: "Current location is not existed in pallet."
        $this->assertEquals(
            "The current location does not have any pallet!",
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_getItemsList_Ok()
    {
        // Create before Show
        $carton = factory(Carton::class)->create();
        $loc_id = $carton->loc_id;

        // Test Search
        $data = $this->call('GET', $this->getEndPoint() . "relocations/" . $loc_id);

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }


    public function test_consolidationLoc_Ok()
    {
        //$carton1 = factory(Carton::class)->create();
        //$ctn_id1 = $carton1->ctn_id;
        //$from_loc_id = $carton1->loc_id;
        //
        //$carton2 = factory(Carton::class)->create();
        //$ctn_id2 = $carton2->ctn_id;
        //$to_loc_id = $carton2->loc_id;
        //
        //$ctn_id = [$ctn_id1, $ctn_id2];
        //
        //// Test update
        //$data = $this->call('PUT', $this->getEndPoint() . "consolidation_loc", [
        //    'from_loc_id' => $from_loc_id,
        //    'to_loc_id'   => $to_loc_id,
        //    'ctn_id'      => $ctn_id
        //]);dd($data);
        //
        //// Case 1: Response code is 200 ok
        //$this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

    }

    public function test_consolidationLoc_currentLocationNotExit_Fail()
    {
        // Create before Show
        $carton1 = factory(Carton::class)->create();
        $ctn_id1 = $carton1->ctn_id;

        $carton2 = factory(Carton::class)->create();
        $ctn_id2 = $carton2->ctn_id;
        $to_loc_id = $carton2->loc_id;

        $ctn_id = [$ctn_id1, $ctn_id2];

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "consolidation_loc", [
            'from_loc_id' => '999999999',
            'to_loc_id'   => $to_loc_id,
            'ctn_id'      => $ctn_id
        ]);

        // Case 1: Response code is 500 internal Server Error
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "Current Location"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_consolidationLoc_NewLocationNotExit_Fail()
    {
        // Create before Show
        $carton1 = factory(Carton::class)->create();
        $ctn_id1 = $carton1->ctn_id;

        $carton2 = factory(Carton::class)->create();
        $ctn_id2 = $carton2->ctn_id;
        $to_loc_id = $carton2->loc_id;

        $ctn_id = [$ctn_id1, $ctn_id2];

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "consolidation_loc", [
            'from_loc_id' => $to_loc_id,
            'to_loc_id'   => 34543599,
            'ctn_id'      => $ctn_id
        ]);

        // Case 1: Response code is 500 internal Server Error
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "New Location"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_consolidationLoc_CartonNotExit_Fail()
    {
        // Create before Show
        $carton1 = factory(Carton::class)->create();
        $ctn_id1 = $carton1->ctn_id;

        $carton2 = factory(Carton::class)->create();
        $ctn_id2 = $carton2->ctn_id;
        $to_loc_id = $carton2->loc_id;

        $ctn_id = [464564564, $ctn_id2];

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "consolidation_loc", [
            'from_loc_id' => $to_loc_id,
            'to_loc_id'   => $to_loc_id,
            'ctn_id'      => $ctn_id
        ]);

        // Case 1: Response code is 500 internal Server Error
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "carton(s)"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_consolidationPlt_Ok()
    {
        $carton1 = factory(Carton::class)->create();
        $ctn_id1 = $carton1->ctn_id;

        $carton2 = factory(Carton::class)->create();
        $ctn_id2 = $carton2->ctn_id;
        $to_plt_id = $carton2->plt_id;

        $ctn_id = [$ctn_id1, $ctn_id2];

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "consolidation_plt", [
            'from_plt_id' => $to_plt_id,
            'from_plt_num' => "from_plt_num",
            'to_plt_id'   => $to_plt_id,
            'to_plt_num' =>  "to_plt_num",
            'ctn_id'      => $ctn_id,
            'ctn_num' => ["ctn_num1", "ctn_num2"]
        ]);


        // Case 1: Response code is 200 ok
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

    }

    public function test_consolidationPlt_currentPalletNotExit_Fail()
    {
        // Create before Show
        $carton1 = factory(Carton::class)->create();
        $ctn_id1 = $carton1->ctn_id;

        $carton2 = factory(Carton::class)->create();
        $ctn_id2 = $carton2->ctn_id;
        $to_plt_id = $carton2->plt_id;

        $ctn_id = [$ctn_id1, $ctn_id2];

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "consolidation_plt", [
            'from_plt_id' => 3432435,
            'to_plt_id'   => $to_plt_id,
            'ctn_id'      => $ctn_id
        ]);

        // Case 1: Response code is 500 internal Server Error
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "Current Pallet"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_consolidationPlt_NewPalletNotExit_Fail()
    {
        // Create before Show
        $carton1 = factory(Carton::class)->create();
        $ctn_id1 = $carton1->ctn_id;

        $carton2 = factory(Carton::class)->create();
        $ctn_id2 = $carton2->ctn_id;
        $to_plt_id = $carton2->plt_id;

        $ctn_id = [$ctn_id1, $ctn_id2];

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "consolidation_plt", [
            'from_plt_id' => $to_plt_id,
            'to_plt_id'   => 3454359,
            'ctn_id'      => $ctn_id
        ]);

        // Case 1: Response code is 500 internal Server Error
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "New Pallet"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_consolidationPlt_CartonNotExit_Fail()
    {
        // Create before Show
        $carton1 = factory(Carton::class)->create();
        $ctn_id1 = $carton1->ctn_id;

        $carton2 = factory(Carton::class)->create();
        $ctn_id2 = $carton2->ctn_id;
        $to_plt_id = $carton2->plt_id;

        $ctn_id = [464564564, $ctn_id2];

        // Test update
        $data = $this->call('PUT', $this->getEndPoint() . "consolidation_plt", [
            'from_plt_id' => $to_plt_id,
            'to_plt_id'   => $to_plt_id,
            'ctn_id'      => $ctn_id
        ]);

        // Case 1: Response code is 500 internal Server Error
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "carton(s)"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_getCartonListByLocation_Ok()
    {
        // Create before Show
        $carton = factory(Carton::class)->create();
        $loc_id = $carton->loc_id;

        // Test Search
        $data = $this->call('GET', $this->getEndPoint() . "consolidation_loc/" . $loc_id);

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }

    public function test_getCartonListByPallet_Ok()
    {
        // Create before Show
        $carton = factory(Carton::class)->create();
        $plt_id = $carton->plt_id;

        // Test Search
        $data = $this->call('GET', $this->getEndPoint() . "consolidation_plt/" . $plt_id);

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }

    public function test_search_Ok()
    {
        // Create before Show
        $pallet = factory(Pallet::class)->create();
        $plt_num = $pallet->plt_num;

        // Test Search
        $data = $this->call('GET', $this->getEndPoint() . "pallets/?plt_num=" . $plt_num);

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }

    ////////////////// ASSIGN PALLET /////////////////
    public function testAssignPallet_Ok()
    {
        $actualCarton = $this->goodsReceiptDetail->gr_dtl_act_ctn_ttl;

        factory(Carton::class, $actualCarton)->create([
            'asn_dtl_id' => $this->asnDt->asn_dtl_id,
            'item_id'    => $this->asnDt->item_id,
            'cus_id'     => $this->asnHdr->cus_id,
            'whs_id'     => $this->asnHdr->whs_id,
            'plt_id'     => null
        ]);

        $param = [
            0 => [
                "item_id" => $this->asnDt->item_id,
                "pallets" => [
                    [
                        "pallet"            => 1,
                        "carton_per_pallet" => $actualCarton
                    ]
                ]
            ]
        ];

        $response = $this->call('POST', "v1/goods-receipts/{$this->goodsReceiptId}/pallets", $param);

        $responseData = json_decode($response->content())->data;

        // Case 1: Response is 201
        $this->assertResponseStatus(Response::HTTP_CREATED);

        // Case 2: Message is: Pallets assigned successfully!
        $this->assertEquals("Pallets assigned successfully!", $responseData->message);
    }

    public function testAssignPallet_InputInvalid_Fail()
    {
        $param = [
            "this_is" => "invalid input"
        ];

        $response = $this->call('POST', "v1/goods-receipts/{$this->goodsReceiptId}/pallets", $param);
        $responseData = json_decode($response->content())->errors;

        // Case 1: Response is 201
        $this->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        // Case 2: Show Error Message
        $this->assertEquals(Message::get("BZ005"), $responseData->message);
    }

    public function testAssignPallet_Assigned_Fail()
    {
        // Make data exist.
        factory(Carton::class)->create(['asn_dtl_id' => $this->asnDt->asn_dtl_id]);
        $actualCarton = $this->goodsReceiptDetail->gr_dtl_act_ctn_ttl;

        $param = [
            0 => [
                "item_id" => $this->asnDt->item_id,
                "pallets" => [
                    [
                        "pallet"            => 1,
                        "carton_per_pallet" => $actualCarton
                    ]
                ]
            ]
        ];

        $response = $this->call('POST', "v1/goods-receipts/{$this->goodsReceiptId}/pallets", $param);
        $responseData = json_decode($response->content())->errors;

        // Case 1: Response is 201
        $this->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        // Case 2: Show Error Message!
        $this->assertEquals("This goods receipt assigned Pallet!", $responseData->message);
    }

    ///////////////// PRINT PUT AWAY /////////////////
    public function testPrintPutAway_NotExist_Fail()
    {
        $response = $this->call('GET', "v1/goods-receipts/99999999999999999/print");
        $responseData = json_decode($response->content())->errors;

        // Case 1: Response is 201
        $this->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        // Case 2: Show Error Message
        $this->assertEquals(Message::get("BM017", "goods receipt"), $responseData->message);
    }

    public function testPrintPutAway_NotAssignPallet_Fail()
    {
        $response = $this->call('GET', "v1/goods-receipts/{$this->goodsReceiptId}/print");
        $responseData = json_decode($response->content())->errors;

        // Case 1: Response is 201
        $this->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        // Case 2: Show Error Message
        $this->assertEquals("This goods receipt doesn't assign pallet!", $responseData->message);
    }

    /**
     * Return Error: Print Successfully.
     * With Error Message: "[ERROR]: Headers already sent".
     */
    public function testPrintPutAway_Ok()
    {
        // Create Customer Zone
        $zoneId = factory(\Seldat\Wms2\Models\CustomerZone::class)->create([
            'cus_id' => $this->cusId
        ])->zone_id;

        // Create Location to Put Pallet In.
        factory(\Seldat\Wms2\Models\Location::class)->create([
            'loc_whs_id'  => $this->warehouseId,
            'loc_zone_id' => $zoneId
        ]);

        $actualCarton = $this->goodsReceiptDetail->gr_dtl_act_ctn_ttl;

        factory(Carton::class, $actualCarton)->create([
            'asn_dtl_id' => $this->asnDt->asn_dtl_id,
            'item_id'    => $this->asnDt->item_id,
            'cus_id'     => $this->asnHdr->cus_id,
            'whs_id'     => $this->asnHdr->whs_id,
            'plt_id'     => null
        ]);

        $param = [
            0 => [
                "item_id" => $this->asnDt->item_id,
                "pallets" => [
                    [
                        "pallet"            => 1,
                        "carton_per_pallet" => $actualCarton
                    ]
                ]
            ]
        ];

        $this->call('POST', "v1/goods-receipts/{$this->goodsReceiptId}/pallets", $param);

        $response = $this->call('GET', "v1/goods-receipts/{$this->goodsReceiptId}/print");

        $responseData = json_decode($response->content())->errors;

        // Case 1: Response is 400
        $this->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        // Case 2: Show success message
        $this->assertEquals("[ERROR]: Headers already sent", $responseData->message);
    }
}