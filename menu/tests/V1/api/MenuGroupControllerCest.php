<?php

use App\MenuGroup;
use App\Menu;
use Illuminate\Http\Response as IlluminateResponse;

class MenuGroupControllerCest
{
//    protected $endpoint = '/v1';
//
//    /**
//     * @var array
//     */
//    protected $dataName = [
////        'logic' => '@#dD90_="/?.,<>',
//        'type' => ['1'],
////        'empty' => '',
////        'null' => null,
////        'max' => 'aldsfkjlk*(&)*(klj%$#%3JGHLJ[]p]p[TGJHHGFuytfu*&^%*^&fghVFjhgf8&^TR*&^F8^TigfYUR86tdfyTCFUGHd856tdhgcvJTDF867tfcighCV*I^&R*F^T&fuyghCVHJGViytr8fTFCVuygcvyghf8^&TFcvuYvkhjvLGOU[Oi]=PO)i={)9ujOI',
////        'min' => 'a',
//    ];
//
//    /**
//     * @var array
//     */
//    protected $dataDescription = [
////        'logic' => '@#dD90_="/?.,<>dgadfg434',
////        'type' => ['1'],
////        'empty' => '',
//        'null' => null,
////        'max' => 'aldsfkjlk*(&)*(klj%$#%3JGHLJ[]p]p[TGJHHGFuytfu*&^%*^&fghVFjhgf8&^TR*&^F8^TigfYUR86tdfyTCFUGHd856tdhgcvJTDF867tfcighCV*I^&R*F^T&fuyghCVHJGViytr8fTFCVuygcvyghf8^&TFcvuYvkhjvLGOU[Oi]=PO)i={)9ujOI',
////        'min' => 'a',
//    ];
//
//    public function listGroup_Ok(ApiTester $I)
//    {
//        $uri = $this->endpoint . '/groups';
//        $I->sendGET($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//    }
//
//    public function showGroup_Ok(ApiTester $I, MenuGroup $menuGroup)
//    {
//        // Add Menu Group
//        $paramMenuGroup = [
//            'name' => 'Name Test',
//            'description' => 'Description Test'
//        ];
//        $menuGroupId = $I->haveRecord($menuGroup->getTable(), $paramMenuGroup);
//
//        $uri = $this->endpoint . "/groups/$menuGroupId";
//        $I->sendGET($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//    }
//
//    public function showGroup_NotExistGroup_Fail(ApiTester $I)
//    {
//        $uri = $this->endpoint . '/groups/100';
//        $I->sendGET($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_BAD_REQUEST);
//
//        $uri = $this->endpoint . '/groups/a';
//        $I->sendGET($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_BAD_REQUEST);
//
//        $uri = $this->endpoint . '/groups/&abc';
//        $I->sendGET($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_BAD_REQUEST);
//    }
//
//    public function storeGroup_AllCase_Fail(ApiTester $I)
//    {
//        $uri = $this->endpoint . '/groups';
//
//        foreach ($this->dataName as $keyName => $valueName) {
//            foreach ($this->dataDescription as $keyDescription => $valueDescription) {
//                $I->sendPOST($uri, [
//                    'name' => $valueName,
//                    'description' => $valueDescription
//                ]);
//
//                if ($keyName === 'logic' && $keyDescription === 'logic') {
//                    $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//                } else {
//                    $I->seeResponseCodeIs(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
//                }
//            }
//        }
//    }
//
//    public function updateGroup_Ok(ApiTester $I, MenuGroup $menuGroupEntity)
//    {
//        $paramMenuGroup = [
//            'name' => 'Name Test',
//            'description' => 'Description Test'
//        ];
//        $menuGroupId = $I->haveRecord($menuGroupEntity->getTable(), $paramMenuGroup);
//
//        $uri = $this->endpoint . '/groups/' . $menuGroupId;
//
//        $I->sendPUT($uri, [
//            'name' => 'Name Test Updated',
//            'description' => 'Description Test Updated'
//        ]);
//
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//
//    }
//
//    public function updateGroup_AllCase_Fail(ApiTester $I, MenuGroup $menuGroupEntity)
//    {
//        $paramMenuGroup = [
//            'name' => 'Name Test',
//            'description' => 'Description Test'
//        ];
//        $menuGroupId = $I->haveRecord($menuGroupEntity->getTable(), $paramMenuGroup);
//
//        $uri = $this->endpoint . "/groups/$menuGroupId";
//
//        $arrayRe = [];
//
//        foreach ($this->dataName as $keyName => $valueName) {
//            foreach ($this->dataDescription as $keyDescription => $valueDescription) {
//                $params = [
//                    'name' => $valueName,
//                    'description' => $valueDescription,
//                ];
//                $I->sendPUT($uri, $params);
//
//                if ($keyName === 'logic' && $keyDescription === 'logic') {
//                    $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//                } else {
//                    $I->seeResponseCodeIs(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
//                }
//            }
//        }
//    }
//
//    public function updateGroup_NotExistGroup_Fail(ApiTester $I)
//    {
//        $uri = $this->endpoint . '/groups/9999';
//
//        $I->sendPUT($uri, [
//            'name' => 'exampleName',
//            'description' => 'example Description'
//        ]);
//
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_BAD_REQUEST);
//    }
//
//    public function destroyGroup_Ok(ApiTester $I, MenuGroup $menuGroup)
//    {
//        // Add Menu Group
//        $paramMenuGroup = [
//            'name' => 'Name Test',
//            'description' => 'Description Test'
//        ];
//        $menuGroupId = $I->haveRecord($menuGroup->getTable(), $paramMenuGroup);
//
//        $uri = $this->endpoint . "/groups/$menuGroupId";
//        $I->sendDELETE($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_NO_CONTENT);
//    }
//
//    public function destroyGroup_NotExistGroup_Fail(ApiTester $I)
//    {
//        $uri = $this->endpoint . '/groups/9999';
//        $I->sendDELETE($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_BAD_REQUEST);
//    }
}