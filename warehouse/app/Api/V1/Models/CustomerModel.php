<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;
use \Seldat\Wms2\Models\CustomerAddress;
use Illuminate\Support\Facades\DB;

class CustomerModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     *
     * @param Customer $model
     */
    public function __construct(Customer $model = null, CustomerAddress $customerAddressModel = null)
    {
        $this->model = ($model) ?: new Customer();
        $this->customerAddressModel = ($customerAddressModel) ?: new CustomerAddress();
    }

    public function loadCustomerForWarehouseLayout($whsId, $userId)
    {
        $models = DB::table('customer')
            ->join('cus_in_user', 'customer.cus_id', '=', 'cus_in_user.cus_id')
            ->where('cus_in_user.whs_id', $whsId)
            ->where('cus_in_user.user_id', $userId)
            ->where('customer.deleted', 0)
            ->where('customer.deleted_at', 915148800)
            ->select('customer.*')
            ->orderBy('customer.cus_code', 'ASC')
            ->get();

        return $models;
    }
}
