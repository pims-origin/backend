<?php

namespace App\Api\V3\Outbound\AssignPallet\Clients;
use Psr\Http\Message\ServerRequestInterface as Request;
use Symfony\Component\HttpFoundation\Response;

class SampleClient extends AbstractClient
{
    /**
     * SampleClient constructor.
     * @param Request $request
     * @param string $baseUrl
     */
    public function __construct(Request $request, $baseUrl = '')
    {
        $api = $baseUrl ?: 'https://httpbin.org/';
        parent::__construct($request, $api);
    }

    /**
     * @return array
     */
    public function get()
    {
        $response = $this->client->get('get', [
            'verify' => false
        ]);

        if ($response->getStatusCode() == Response::HTTP_OK) {
            $body = $response->getBody()->getContents();
            return \GuzzleHttp\json_decode($body, true);
        }
    }
}
