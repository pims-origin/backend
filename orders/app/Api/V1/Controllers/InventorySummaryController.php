<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventoryModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\ReportInventoryModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Traits\OrderFlowControllerTrait;
use App\Api\V1\Transformers\InventorySummaryTransformer;
use App\Api\V1\Transformers\ItemPackSizeTransformer;
use App\Api\V1\Transformers\ItemSummaryTransformer;
use App\Jobs\AutoCreateWavepickJob;
use Dingo\Api\Exception\UnknownVersionException;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class InventorySummaryController extends AbstractController
{
    use OrderFlowControllerTrait;
    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    protected $inventoryModel;

    /**
     * @var ReportInventoryModel
     */
    protected $reportInventoryModel;

    /**
     * @var $orderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    protected $userModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var ShippingOrderModel
     */
    protected $shippingOrderModel;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    protected $cartonModel;

    /**
     * InventorySummaryController constructor.
     *
     * @param InventorySummaryModel $inventorySummaryModel
     * @param OrderDtlModel $orderDtlModel
     * @param OrderHdrModel $orderHdrModel
     * @param EventTrackingModel $eventTrackingModel
     * @param UserModel $userModel
     * @param ItemModel $itemModel
     * @param ShippingOrderModel $shippingOrderModel
     */
    public function __construct(
        InventorySummaryModel $inventorySummaryModel,
        OrderDtlModel $orderDtlModel,
        OrderHdrModel $orderHdrModel,
        EventTrackingModel $eventTrackingModel,
        UserModel $userModel,
        ItemModel $itemModel,
        InventoryModel $inventoryModel,
        ShippingOrderModel $shippingOrderModel,
        CartonModel $cartonModel,
        ReportInventoryModel $reportInventoryModel
    ) {
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->userModel = $userModel;
        $this->itemModel = $itemModel;
        $this->shippingOrderModel = $shippingOrderModel;
        $this->customerConfigModel = new CustomerConfigModel();
        $this->cartonModel = $cartonModel;
        $this->inventoryModel = $inventoryModel;
        $this->reportInventoryModel = $reportInventoryModel;
    }
    
    public function updateAllocate(Request $request, $orderHdrId) {
        $input = $request->getParsedBody();
        
        $orderInfo = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderHdrId]);
        
        if (empty($orderInfo)) {
            throw new \Exception(Message::get("BM017", "Order"));
        }
        
        // check if order type is BAC and origin order must be picked
        $odr_type = object_get($orderInfo, 'odr_type', '');
        if ($odr_type == Status::getByValue('BackOrder', 'ORDER-TYPE')) {
            $origin_num = substr(object_get($orderInfo, 'odr_num', ''), 0, 14);
            $orderOrigin = $this->orderHdrModel->getFirstWhere(['odr_num' => $origin_num]);
            $odr_origin_sts = object_get($orderOrigin, 'odr_sts', '');
            
            // WMS2-5403 - [Outbound - Orders] ABLE TO ALLOCATE AND CREATE WAVE PICK FOR PART 2 ORDERS
            $checkStatus = [Status::getByValue('New', 'ORDER-STATUS')];

            if (in_array($odr_origin_sts, $checkStatus)) {
                throw new \Exception(Message::get("BM153", $origin_num));
            }
        }

        //check order status: Only New Orders can be allocated!
        if (object_get($orderInfo, 'odr_sts', null) !== config('constants.odr_status.NEW')) {
            throw new \Exception(Message::get("BM034"));
        }
        
        //Check: Order {0}  need to be assign CSR! Do you want to assign  now?
        if (!object_get($orderInfo, 'csr', 0)) {
            throw new UnknownVersionException(Message::get("BM019", object_get($orderInfo, 'odr_num', '')), null,
                7);
        }
        
        // Check: At least 1 item should be allocated!
        $checkNull = false;
        
        $algorithm = $this->customerConfigModel->getPickingAlgorithm($orderInfo->whs_id, $orderInfo->cus_id);
        $odrDtls = $this->orderDtlModel->findWhere(['odr_id' => $orderHdrId]);
        
        foreach ($odrDtls as $odrDtl) {
            $inventory = $this->inventorySummaryModel->getModel()
            ->where([
                'item_id'   => $odrDtl->item_id,
                'whs_id'   => $odrDtl->whs_id,
                'cus_id'   => $odrDtl->cus_id
            ])
            ->where(function ($where) use ($odrDtl) {
                if (strtoupper($odrDtl->lot) != 'ANY') {
                    $where->where('lot', '=', $odrDtl->lot);
                }
            })->sum('avail');
            
            if ($inventory > 0) {
                $checkNull = true;
            }
        }
        
        
        if (!$checkNull) {
            throw new \Exception("At least 1 item is available quantity in inventory summary");
        }
        
        try {
            DB::beginTransaction();
            
            $isPartial = $this->checkPartial($orderHdrId);
            
            $bacOdrDtls = $this->orderDtlModel->updateOrdDtl($orderInfo, $odrDtls, $isPartial, $algorithm);
            $this->orderHdrModel->createBackOrder($orderInfo, $bacOdrDtls);
            
            //            $this->orderDtlModel->updateWhere(['lot' => 'Any'], [
            //                'alloc_qty' => 0,
            //                'back_odr'  => 1,
            //                'deleted'   => 0,
            //                'odr_id'    => $orderHdrId
            //            ]);
            //            DB::table('odr_dtl')->where('alloc_qty', 0)->where('deleted', 1)->where('back_odr', 1)->where('odr_id',
            //                $orderHdrId)
            //                ->update([
                //                    'deleted_at' => 915148800,
                //                    'deleted'    => 0
                //                ]);

                //TODO: DUYNQK
                //$this->correctReportInventory($orderInfo->whs_id, $orderInfo->cus_id, $input);
                
                DB::commit();
                
                dispatch(new AutoCreateWavepickJob($orderHdrId, $orderInfo->cus_id, $request));
                
                return ['data' => 'Update successfully'];
                
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }


    public function correctReportInventory($whsId, $cusId, $items)
    {
        foreach($items as $key => $item) {
            $inventory = $this->inventorySummaryModel->getModel()
                ->where("item_id", $item['itm_id'])
                ->where("whs_id", $whsId)
                ->where("cus_id", $cusId)
                ->first();

            if(!empty($inventory)) {
                $reportInventory = $this->reportInventoryModel->getFirstWhere([
                    "whs_id"    => $inventory['whs_id'],
                    "cus_id"    => $inventory['cus_id'],
                    "item_id"   => $inventory['item_id'],
                ]);

                $data = [
                    "whs_id"        => $inventory['whs_id'],
                    "cus_id"        => $inventory['cus_id'],
                    "item_id"       => $inventory['item_id'],
                    "whs_code"      => $inventory['whs_code'],
                    "cus_code"      => $inventory['cus_code'],
                    "type"          => $inventory['type'],
                    "upc"           => $inventory['upc'],
                    "des"           => $inventory['des'],
                    "sku"           => $inventory['sku'],
                    "size"          => $inventory['size'],
                    "color"         => $inventory['color'],
                    "pack"          => $inventory['pack'],
                    "uom"           => $inventory['uom'],
                    "in_hand_qty"   => $inventory['in_hand_qty'],
                    "in_pick_qty"   => $inventory['in_pick_qty'],
                ];

                if(empty($reportInventory)) {
                    $this->reportInventoryModel->create($data);
                }else{
                    $reportInventory->update($data);
                }
            }
        }
    }


    /**
     *
     * Update origin order to partial order
     *
     * @param $input
     * @param $orderInfo
     * @param $inventorySummaryTransformer
     *
     * @return \Dingo\Api\Http\Response|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    private function updateToPartialOrder($input, $orderInfo, $inventorySummaryTransformer)
    {
        // Load original order
        $originalOrderNum = substr($orderInfo['odr_num'], 0, 14);
        $originalOrder = $this->orderHdrModel->getFirstWhere(['odr_num' => $originalOrderNum]);

        //get all item from inventory_summary table
        $arrItems = [];

        // Get all item from input to get data from inventory_summary
        foreach ($input as $val) {
            if (!in_array($val['itm_id'], $arrItems)) {
                array_push($arrItems, $val['itm_id']);
            }
        }

        // Get all item in inventory_summary
        $dataIvt = $this->inventorySummaryModel->getByItemIds($arrItems)->toArray();

        if (empty($dataIvt)) {
            throw new \Exception(Message::get("BM017", "Item"));
        }

        // Make an array to decrease "for" loop
        $aTemp = array_map(function ($e) {
            return [
                "item_id"       => $e['item_id'],
                "avail"         => $e['avail'],
                "allocated_qty" => $e['allocated_qty']
            ];
        }, $dataIvt);


        $allItem = array_pluck($aTemp, null, "item_id");

        // Allow also item not exist in inventory summary table
        if (count($arrItems) != count($dataIvt)) {
            foreach ($arrItems as $ar) {
                if (!isset($allItem[$ar])) {
                    $allItem[$ar] = [
                        "item_id"       => $ar,
                        "avail"         => 0,
                        "allocated_qty" => 0
                    ];
                }
            }
        }

        // detect qty to make decision that the order need partial or not
        $checkPartial = 0;
        $checkFull = 0;
        foreach ($input as $val) {
            if (isset($allItem[$val['itm_id']])) {
                // Order can be allocated
                if ($val['allocated_qty'] == $val['piece_qty'] &&
                    $val['allocated_qty'] <= $allItem[$val['itm_id']]['avail']
                ) {
                    $checkFull++;
                }
                if ($val['allocated_qty'] <= $allItem[$val['itm_id']]['avail'] &&
                    $val['allocated_qty'] <= $val['piece_qty']
                ) {
                    $checkPartial++;
                } // check if allocate_qty > avail
                else {
                    throw new \Exception(Message::get("BM047"));
                }
            } else {
                throw new \Exception(Message::get("BM017", "Item " . $val['itm_id']));
            }
        }
        try {
            if ($checkFull == count($input)) {
                return $this->updateFullOrder($input, $orderInfo, $inventorySummaryTransformer);
            } // Update Original Order to Partial Order
            elseif ($checkPartial > 0) {
                //---------- FOR Order header
                // update order_status from new-> partial for order header
                // for current order
                $this->orderHdrModel->updateWhere([
                    'odr_sts'      => Status::getByValue("Partial Allocated", "PARTIAL-ORDER-STATUS"),
                    'back_odr_seq' => 0,
                    'back_odr'     => 1,
                    'sts'          => 'u'
                ], ['odr_id' => $orderInfo['odr_id']]);

                //update original order header
                if ($originalOrderNum == substr($orderInfo['odr_num'], 0, 14) && strtolower($orderInfo['odr_type']) ==
                    "bac"
                ) {
                    $this->orderHdrModel->updateWhere([
                        'back_odr_seq' => 0,
                        'back_odr'     => 1,
                        'sts'          => 'u'
                    ], ['odr_id' => $originalOrder['odr_id']]);
                }

                // Insert Event Tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $orderInfo['whs_id'],
                    'cus_id'    => $orderInfo['cus_id'],
                    'owner'     => $orderInfo['odr_num'],
                    'evt_code'  => Status::getByKey("event", "PARTIAL-ALLOCATED"),
                    'trans_num' => $orderInfo['odr_num'],
                    'info'      => sprintf(Status::getByKey("event-info", "PAL"), $orderInfo['odr_num'])
                ]);

                // -------- For Order detail
                $dtOdrDtl = [];
                $i = 0;
                foreach ($input as $val) {
                    if (isset($allItem[$val['itm_id']])) {
                        if ($val['allocated_qty'] <= $allItem[$val['itm_id']]['avail'] &&
                            $val['allocated_qty'] <= $val['piece_qty']
                        ) {
                            $back_odr_qty = $val['piece_qty'] - $val['allocated_qty'];
                            $this->orderDtlModel->updateWhere([
                                'alloc_qty'    => $val['allocated_qty'],
                                'back_odr_qty' => $back_odr_qty,
                                'back_odr'     => ($back_odr_qty == 0 ? 0 : 1),
                            ], [
                                'odr_id'  => $orderInfo['odr_id'],
                                'item_id' => $val['itm_id'],
                            ]);

                            if ($back_odr_qty > 0) {
                                // prepare data for back order
                                $originOdrDtl = $this->orderDtlModel->getFirstWhere([
                                    'item_id' => $val['itm_id'],
                                    'odr_id'  => $orderInfo['odr_id']
                                ]);

                                $dtOdrDtl['items'][$i] = [
                                    'item_id'       => $val['itm_id'],
                                    "allocated_qty" => ((is_null($val['allocated_qty']) || $val['allocated_qty'] == 0) ?
                                        0 : $val['allocated_qty']),
                                    'piece_qty'     => $back_odr_qty,
                                    'alloc_qty'     => 0,
                                    'uom_id'        => $val['uom_id'],
                                    'color'         => $val['color'],
                                    'sku'           => $val['sku'],
                                    'size'          => $val['size'],
                                    'lot'           => $val['lot'],
                                    'pack'          => $val['pack'],
                                    'des'           => null,
                                    'qty'           => $val['qty'],
                                    'cus_upc'       => $val['cus_upc'],
                                    'itm_sts'       => null,
                                    'back_odr'      => 0,
                                    'special_hdl'   => null,
                                    'back_odr_qty'  => 0,
                                    'ship_track_id' => object_get($originOdrDtl, 'ship_track_id', null),
                                    'sts'           => "i"
                                ];
                                $i++;
                            }

                        }
                    }
                }

                if (count($dtOdrDtl) > 0) {
                    // BACK ORDER
                    return $this->createBackOrder($orderInfo, $dtOdrDtl, $originalOrder);

                } else {
                    return $this->response
                        ->noContent()
                        ->setContent("{}")
                        ->setStatusCode(IlluminateResponse::HTTP_OK);
                }

            } else {
                return $this->updateFullOrder($input, $orderInfo, $inventorySummaryTransformer);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     *
     * Create back order
     *
     * @param $orderInfo
     * @param $dataDtl
     *
     * @deprecated DO not use it
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    private function createBackOrder($orderInfo, $dataDtl, $originalOrder)
    {
        $params = [
            // Order Hdr
            'ship_id'         => $orderInfo['ship_id'],
            'carrier'         => $orderInfo['carrier'],
            'ship_to_name'    => $orderInfo['ship_to_name'],
            'ship_to_add_1'   => $orderInfo['ship_to_add_1'],
            'ship_to_add_2'   => array_get($orderInfo, 'ship_to_add_2', null),
            'ship_to_city'    => $orderInfo['ship_to_city'],
            'ship_to_country' => $orderInfo['ship_to_country'],
            'ship_to_state'   => $orderInfo['ship_to_state'],
            'ship_to_zip'     => $orderInfo['ship_to_zip'],
            'ship_by_dt'      => $orderInfo['ship_by_dt'],
            'odr_req_dt'      => $orderInfo['odr_req_dt'],
            'cancel_by_dt'    => $orderInfo['cancel_by_dt'],
            'req_cmpl_dt'     => $orderInfo['req_cmpl_dt'],
            'act_cmpl_dt'     => array_get($orderInfo, 'act_cmpl_dt', null),
            'act_cancel_dt'   => array_get($orderInfo, 'act_cancel_dt', null),
            'ship_after_dt'   => $orderInfo['ship_after_dt'],
            'shipped_dt'      => $orderInfo['shipped_dt'],
            'cus_pick_num'    => $orderInfo['cus_pick_num'],
            'ship_method'     => $orderInfo['ship_method'],
            'cus_dept_ref'    => $orderInfo['cus_dept_ref'],
            'csr'             => $orderInfo['csr'],
            'hold_sts'        => $orderInfo['hold_sts'],
            'back_odr_seq'    => '1',
            'back_odr'        => '0',
            'org_odr_id'      => (strtolower($orderInfo['odr_type']) == "bac" ?
                $orderInfo['org_odr_id'] :
                $orderInfo['odr_id']),
            'sku_ttl'         => count($dataDtl['items']),
            'wv_num'          => $orderInfo['wv_num'],
            'in_notes'        => array_get($orderInfo, 'in_notes', null),
            'cus_notes'       => array_get($orderInfo, 'cus_notes', null),
            // Order Info
            'cus_id'          => $orderInfo['cus_id'],
            'whs_id'          => $orderInfo['whs_id'],
            'wv_id'           => $orderInfo['wv_id'],
            'cus_odr_num'     => $orderInfo['cus_odr_num'],
            'ref_cod'         => array_get($orderInfo, 'ref_cod', null),
            'cus_po'          => $orderInfo['cus_po'],
            'odr_type'        => "BAC",
            'rush_odr'        => (int)array_get($orderInfo, 'rush_odr', null),
            'odr_sts'         => Status::getByValue("New", "Order-Status"),
        ];

        try {
            $odrNum = $this->orderHdrModel->getBackOrderNum($orderInfo['odr_num']);
            $params['odr_num'] = $odrNum;

            $this->getShippingOdr($originalOrder);

            $params['so_id'] = $originalOrder['so_id'];

            // Insert Order Header
            $orderHdr = $this->orderHdrModel->create($params);
            if (!$orderHdr) {
                throw new \Exception(Message::get("BM010"));
            }

            // Insert Order Detail & update inventory summary
            if (!empty($dataDtl['items'])) {
                foreach ($dataDtl['items'] as $detail) {

                    if ($detail['pack'] == "") {
                        $ctn_qty = null;
                    } else {
                        $ctn_qty = ceil($detail['piece_qty'] / $detail['pack']);
                    }

                    $this->orderDtlModel->refreshModel();
                    $dtOdrDtl = [
                        'whs_id'        => $orderInfo['whs_id'],
                        'cus_id'        => $orderInfo['cus_id'],
                        'odr_id'        => $orderHdr->odr_id,
                        'item_id'       => $detail['item_id'],
                        'uom_id'        => $detail['uom_id'],
                        'color'         => $detail['color'],
                        'sku'           => $detail['sku'],
                        'size'          => $detail['size'],
                        'lot'           => $detail['lot'],
                        'pack'          => $detail['pack'],
                        'des'           => $detail['des'],
                        'qty'           => $ctn_qty,
                        'piece_qty'     => $detail['piece_qty'],
                        'cus_upc'       => $detail['cus_upc'],
                        'itm_sts'       => $detail['itm_sts'],
                        'back_odr'      => $detail['back_odr'],
                        'alloc_qty'     => $detail['alloc_qty'],
                        'special_hdl'   => $detail['special_hdl'],
                        'back_odr_qty'  => $detail['back_odr_qty'],
                        'ship_track_id' => $detail['ship_track_id'],
                        'sts'           => $detail['sts']
                    ];
                    $this->orderDtlModel->create($dtOdrDtl);
                }
            }

            // Insert Event Tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $orderInfo['whs_id'],
                'cus_id'    => $orderInfo['cus_id'],
                'owner'     => $odrNum,
                'evt_code'  => Status::getByKey("event", "NEW-BACK-ORDER"),
                'trans_num' => $odrNum,
                'info'      => sprintf(Status::getByKey("event-info", "BNW"), $odrNum, $orderInfo['odr_num'])
            ]);

            return $this->response
                ->noContent()
                ->setContent("{}")
                ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     *
     * Update full order
     *
     * @param $orderInfo
     * @param $inventorySummaryTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */

    private function updateFullOrder($input, $orderInfo, $inventorySummaryTransformer)
    {
        try {
            $allItem = array_pluck($input, null, "itm_id");

            //get ItemIDs
            $orderDtls = $this->orderDtlModel->findWhere(['odr_id' => $orderInfo['odr_id']]);
            if ($orderDtls->isEmpty()) {
                throw  new \Exception(Message::get("BM017", "Order"));
            }

            // Also check item not be allocated
            foreach ($orderDtls as $ar) {
                if (!isset($allItem[$ar['item_id']])) {
                    $allItem[$ar['item_id']] = [
                        "allocated_qty" => 0
                    ];
                } else {
                    $allocated_qty = $allItem[$ar['item_id']]['allocated_qty'];
                    $allItem[$ar['item_id']] = [
                        "allocated_qty" => ((is_null($allocated_qty) || $allocated_qty == 0) ? 0 : $allocated_qty)
                    ];
                }
            }

            $item_ids = [];
            foreach ($orderDtls as $orderDtl) {
                if (isset($allItem[$orderDtl['item_id']])) {
                    $alloc_qty = $allItem[$orderDtl['item_id']]['allocated_qty'];

                    $params = [
                        'odr_dtl_id'    => $orderDtl['odr_dtl_id'],
                        'item_id'       => $orderDtl['item_id'],
                        'whs_id'        => $orderDtl['whs_id'],
                        'piece_qty'     => $orderDtl['piece_qty'],
                        'allocated_qty' => $alloc_qty
                    ];

                    //Customer does not allow to partially allocate. The available quantity of some items is not sufficient!
                    $inventorySummaryInfo = $this->inventorySummaryModel->getFirstWhere([
                        'item_id' => $orderDtl['item_id'],
                        'whs_id'  => $params['whs_id']
                    ],
                        [
                            'item.carton',
                        ]);

                    //get is_ecom from carton
                    //                   $cartonModel = new CartonModel();
//                    $cartonEcom = $cartonModel->getFirstWhere([
//                        'item_id' => $orderDtl['item_id'],
//                        'is_ecom' => 1
//                    ]);
//                    $ecom_qty = object_get($cartonEcom, 'piece_remain', 0);

                    if (!(object_get($inventorySummaryInfo, 'avail', 0) >= $params['piece_qty'] &&
                        $params['allocated_qty'] == $params['piece_qty'])
                    ) {
                        return $this->response->errorBadRequest(Message::get("BM033"));
                    }


                    // Check BAC order
                    $c = strtolower($orderInfo['odr_type']) == "bac" ? -1 * $params['piece_qty'] : 0;

                    //update inventory summary
                    $this->inventorySummaryModel->updateWhere(
                        [
                            'allocated_qty' => object_get($inventorySummaryInfo, 'allocated_qty',
                                    0) + $params['piece_qty'],
                            'avail'         => object_get($inventorySummaryInfo, 'avail',
                                    0) - $params['piece_qty'],
                            'back_qty'      => object_get($inventorySummaryInfo, 'back_qty', 0) + $c
                        ],
                        [
                            'item_id' => $params['item_id'],
                            'whs_id'  => $params['whs_id'],
                            'cus_id'  => $orderInfo['cus_id']
                        ]
                    );

                    //Update alloc_qty on orderDtl according to odr_dtl_id and odr_id
                    $this->orderDtlModel->updateWhere(
                        [
                            'alloc_qty' => $params['piece_qty']
                        ],
                        ['odr_dtl_id' => $params['odr_dtl_id']]
                    );

                    $item_ids[] = $orderDtl['item_id'];
                }
            }

            //Update odr_sts = allocated
            $this->orderHdrModel->updateWhere(
                [
                    'odr_sts' => config('constants.odr_status.ALLOCATED')
                ],
                ['odr_id' => $orderInfo['odr_id']]
            );

            //Get user name according to userId
            $userInfo = $this->userModel->getFirstWhere(['user_id' => object_get($orderInfo, 'csr', '')]);

            //event tracking in Ordering
            $this->eventTrackingModel->create([
                'whs_id'    => object_get($orderInfo, 'whs_id', ''),
                'cus_id'    => object_get($orderInfo, 'cus_id', ''),
                'owner'     => object_get($orderInfo, 'odr_num', ''),
                'evt_code'  => config('constants.event.ALLOCATED-ORD'),
                'trans_num' => object_get($orderInfo, 'odr_num', ''),
                'info'      => sprintf(config('constants.event-info.ALLOCATED-ORD'), object_get($orderInfo, 'odr_num',
                    ''))

            ]);

            if ($invSumInfo = $this->inventorySummaryModel->getByItemIds($item_ids)
            ) {
                return $this->response->collection($invSumInfo, $inventorySummaryTransformer);
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $input
     * @param bool $isUpdate
     *
     * @return mixed
     */
    private function getShippingOdr($input, $oldRef = null, $isUpdate = false)
    {
        $this->shippingOrderModel->refreshModel();

        $shipParams = [
            'cus_id'      => $input['cus_id'],
            'whs_id'      => $input['whs_id'],
            'cus_odr_num' => array_get($input, 'cus_odr_num', null),
            'so_sts'      => Status::getByValue("New", "Order-Status"),
            'type'        => $input['odr_type'],
        ];

        // Create Shipping Order.
        $so = $this->shippingOrderModel->getFirstWhere([
            'cus_odr_num' => $shipParams['cus_odr_num'],
            'whs_id'      => $shipParams['whs_id'],
            'cus_id'      => $shipParams['cus_id']
        ]);

        if (empty($so)) {
            // Create Shipping Odr
            $shipParams['po_total'] = 1;
            $soId = $this->shippingOrderModel->create($shipParams)->so_id;
        } else {
            // Update po_total
            $soId = $so->so_id;
            if ($isUpdate) {
                if ($oldRef !== $shipParams['cus_odr_num']) {
                    $shipParams['po_total'] = (int)$so->po_total + 1;
                    $shipParams['so_id'] = $soId;
                    $soId = $this->shippingOrderModel->update($shipParams)->so_id;
                }
            }
        }

        return $soId;
    }

    /**
     * @param Request $request
     * @param ItemSummaryTransformer $itemSummaryTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function searchItem(Request $request, ItemSummaryTransformer $itemSummaryTransformer)
    {
        $input = $request->getQueryParams();

        try {
            // get list asn
            $items = $this->itemModel->search($input, [], array_get($input, 'limit', 20));

            return $this->response->paginator($items, $itemSummaryTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $string
     * @param $test
     *
     * @return bool
     */
    public function strEndsWith($string, $test)
    {
        $strlen = strlen($string);
        $testlen = strlen($test);
        if ($testlen > $strlen) {
            return false;
        }

        return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
    }

    public function listPackSize($itemId, Request $request, ItemPackSizeTransformer $itemPackSizeTransformer)
    {
        $input = $request->getQueryParams();

        try {
            // get list asn
            $lot = array_get($input, 'lot', 'ANY');
//            $items = $this->cartonModel->getAvailablePackSize($itemId, $lot);
//
//            if ($items->isEmpty()) {
//                $items = DB::table('item')->where('item_id', '=', $itemId)->select(DB::raw('pack as pack_size'))->first();
//                $items['lot'] = 'NA';
//                return [
//                    'data' => [
//                        $items
//                    ]
//                ];
//            }

            $items = DB::table('item')->where('item_id', '=', $itemId)->select(DB::raw('pack as pack_size'))->first();
            $items['lot'] = 'NA';
            return [
                'data' => [
                    $items
                ]
            ];

            //return $this->response->paginator($items, $itemPackSizeTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function xdocAllocate($ordHdrId)
    {
        $whsInfo = $this->getWhsInfo(Data::getCurrentWhsId());
        $whsCode = $whsInfo['whs_code'];

        $sqlOdrCtn = "
            INSERT odr_cartons (
                piece_qty, ctn_id, ctn_num, ctn_rfid, item_id, sku, size, color, lot, upc, pack, uom_id,
                uom_code, uom_name, whs_id, cus_id, is_storage,
                deleted, deleted_at, created_at, updated_at, created_by, updated_by,
                odr_hdr_id, odr_num, odr_dtl_id, sts, ctn_sts,
                spc_hdl_code, spc_hdl_name, cat_code, cat_name, plt_rfid,
                loc_id, loc_code, length, width, height, volume, plt_id, inner_pack
            )
            SELECT
                piece_remain, ctn_id, ctn_num, rfid, item_id, sku, size, color, lot, upc, ctn_pack_size, ctn_uom_id,
                uom_code, uom_name, whs_id, cus_id, if(origin_id IS NULL, 0, 1),
                deleted, deleted_at, :created_at, :updated_at, :created_by, :updated_by,
                :odr_hdr_id, :odr_num, :odr_dtl_id, 'I', 'PD',
                spc_hdl_code, spc_hdl_name, cat_code, cat_name, CONCAT('$whsCode-XDK-', LPAD(loc_id,6,0)),
                loc_id, loc_code, length, width, height, volume, plt_id, inner_pack
            FROM cartons
            WHERE deleted      = 0
                AND updated_at = :update_time
                AND item_id    = :item_id
                AND lot        = :lot
                AND loc_type_code = 'XDK'
        ";

        $sqlPackHdr = "
            INSERT pack_hdr (
                pack_hdr_num, cnt_id, whs_id, cus_id, piece_ttl, pack_dt_checksum,
                deleted_at, deleted,
                seq, carrier_name, sku_ttl, pack_sts, sts, ship_to_name, out_plt_id, pack_type,
                width, height, length, pack_ref_id, is_print,
                odr_hdr_id, created_at, updated_at, created_by, updated_by
            )
            SELECT
                ctn_num, ctn_id, whs_id, cus_id, piece_remain, MD5( CONCAT(item_id,'-',lot,'-',ctn_pack_size) ),
                deleted_at, deleted,
                0, ' ', 1, 'NW', 'i', ' ', NULL, 'CT',
                width, height, length, CASE CONCAT(length,'x',width,'x',height) %s END, 0,
                :odr_hdr_id, :created_at, :updated_at, :created_by, :updated_by

            FROM cartons
            WHERE   updated_at = :update_time
                AND item_id    = :item_id
                AND lot        = :lot
                AND loc_type_code = 'XDK'
        ";

        $sqlPackDtl = "
            INSERT pack_dtl (
                pack_hdr_id, odr_hdr_id, whs_id, cus_id, piece_qty, deleted_at, deleted, sts,
                item_id, sku, size, color, lot, cus_upc, uom_id,
                created_at, updated_at, created_by, updated_by
            )
            SELECT
                pack_hdr_id, odr_hdr_id, whs_id, cus_id, piece_ttl, deleted_at, deleted, 'i',
                :item_id, :sku, :size, :color, :lot, :cus_upc, :uom_id,
                :created_at, :updated_at, :created_by, :updated_by
            FROM pack_hdr
            WHERE odr_hdr_id = :odr_hdr_id
                AND updated_at = :update_time
        ";

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $palletModel = new PalletModel();
        $odrDtls = $this->orderDtlModel->getModel()
            ->where('odr_id', $ordHdrId)
            ->get();
        $odrHdr = $this->orderHdrModel->getModel()->where('odr_id', $ordHdrId)->first();

        try {
            DB::beginTransaction();
            $ctnModel = new CartonModel();

            //create new odr dtl with lot is any
            $this->orderDtlModel->updateXdocOrdDtl($odrHdr, $odrDtls, false, "FIFO");
            $odrDtls = $this->orderDtlModel->getModel()
                ->where('odr_id', $ordHdrId)
                ->get();

            $userId = Data::getCurrentUserId();
            $updateTime = time(); //use as key for create order carton
            $locIdArr = [];
            foreach ($odrDtls as $odrDtl) {
                $updateTime++;
                $odrPiece = $odrDtl->piece_qty;
                $cartonsTotal = InventorySummary::where([
                    'item_id' => $odrDtl->item_id,
                    'lot'     => $odrDtl->lot,
                    'whs_id'  => $odrDtl->whs_id,
                    'cus_id'  => $odrDtl->cus_id
                ])
                    ->sum('crs_doc_qty');

                if ($odrDtl->piece_qty > $cartonsTotal) {
                    return $this->response->errorBadRequest('Cross Dock cartons are not enough');
                }

                //update odrDtl
                $odrDtl->alloc_qty = $odrPiece;
                $odrDtl->itm_sts = 'PA';
                $odrDtl->save();

                //update inventory summary
                InventorySummary::where([
                    'item_id' => $odrDtl->item_id,
                    'lot'     => $odrDtl->lot,
                    'whs_id'  => $odrDtl->whs_id,
                    'cus_id'  => $odrDtl->cus_id
                ])
                    ->update([
                        'crs_doc_qty' => DB::raw('crs_doc_qty - ' . $odrPiece),
                        'picked_qty'  => DB::raw('picked_qty + ' . $odrPiece)
                    ]);

                //get cartons packsize
                $pieceSizes = $this->cartonModel->getPieceSizesByPiece($odrDtl->item_id, $odrDtl->lot, $odrDtl->pack,
                    $odrPiece);

                foreach ($pieceSizes as $pieceSize => $ttlAndLoc) {
                    $pieceTll = $ttlAndLoc['total'];
                    $locs = explode(',', $ttlAndLoc['locs']);
                    $locIdArr = array_merge($locIdArr, $locs);
                    $pdPieces = 0;
                    $fullCartons = floor($odrPiece / $pieceSize);

                    if ($odrPiece < $pieceTll) {
                        $pdPieces = $odrPiece - ($fullCartons * $pieceSize);
                        $odrPiece = 0;
                    } else {
                        $fullCartons = floor($pieceTll / $pieceSize);
                        $odrPiece -= $pieceTll;
                    }

                    //update picked full
                    $this->cartonModel->updatePickedFull($odrDtl->item_id, $odrDtl->lot, $fullCartons, $pieceSize,
                        $updateTime);

                    ////update picked piece
                    if ($pdPieces != 0) {
                        $this->cartonModel->updatePickedPiece($odrDtl->item_id, $odrDtl->lot, $pdPieces, $pieceSize,
                            $updateTime);
                    }
                    //create pack ref data
                    $packRefs = $ctnModel->xdocCreatePackRef([
                        'updated_at' => $updateTime,
                        'item_id'    => $odrDtl->item_id,
                        'lot'        => $odrDtl->lot
                    ]);
                }


                //move to queues -------------------------------------
                //create order cartons
                DB::insert($sqlOdrCtn, [
                    ':created_by'  => $userId,
                    ':updated_by'  => $userId,
                    ':created_at'  => $updateTime,
                    ':updated_at'  => $updateTime,
                    ':odr_hdr_id'  => $odrDtl->odr_id,
                    ':odr_num'     => $odrHdr->odr_num,
                    ':odr_dtl_id'  => $odrDtl->odr_dtl_id,
                    ':update_time' => $updateTime,
                    ':item_id'     => $odrDtl->item_id,
                    ':lot'         => $odrDtl->lot
                ]);
                //Remove loc_id, loc_code in cartons
                DB::table('cartons')->where([
                    "deleted"       => 0,
                    "updated_at"    => $updateTime,
                    "item_id"       => $odrDtl->item_id,
                    "lot"           => $odrDtl->lot,
                    "loc_type_code" => 'XDK',
                    "ctn_sts"       => 'PD'
                ])
                    ->update([
                        'loc_id'   => null,
                        'loc_name' => null
                    ]);


                //create auto pack
                $packRefCond = ''; //for insert pack ref id
                foreach ($packRefs as $dimension => $packRefId) {
                    $packRefCond .= ' WHEN \'' . $dimension . '\' THEN ' . $packRefId;
                }

                $sqlPackHdr = sprintf($sqlPackHdr, $packRefCond);
                DB::insert($sqlPackHdr, [
                    ':created_by'  => $userId,
                    ':updated_by'  => $userId,
                    ':created_at'  => $updateTime,
                    ':updated_at'  => $updateTime,
                    ':odr_hdr_id'  => $odrDtl->odr_id,
                    ':update_time' => $updateTime,
                    ':item_id'     => $odrDtl->item_id,
                    ':lot'         => $odrDtl->lot
                ]);
                //create pack dtl
                DB::insert($sqlPackDtl, [
                    ':item_id'     => $odrDtl->item_id,
                    ':sku'         => $odrDtl->sku,
                    ':size'        => $odrDtl->size,
                    ':color'       => $odrDtl->color,
                    ':lot'         => $odrDtl->lot,
                    ':cus_upc'     => $odrDtl->cus_upc,
                    ':uom_id'      => $odrDtl->uom_id,
                    ':created_by'  => $userId,
                    ':updated_by'  => $userId,
                    ':created_at'  => $updateTime,
                    ':updated_at'  => $updateTime,
                    ':odr_hdr_id'  => $odrDtl->odr_id,
                    ':update_time' => $updateTime
                ]);
            }

            //update palette ttl ctn
            $locIdArr = array_unique($locIdArr);
            $palletModel->updatePalletCtnTtl($locIdArr);

            //move to queue
            $this->orderHdrModel->getModel()->where('odr_id', $ordHdrId)
                ->update(['odr_sts' => 'PA']);

            DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }

        return ['data' => 'Successful'];
    }

    public function searchXdocItem(Request $request)
    {
        $input = $request->getQueryParams();

        return $this->cartonModel->searchXdoc($input['sku'], $input['cus_id']);
    }

    private function getWhsInfo($whsId)
    {
        foreach (Data::getInstance()->getUserInfo()['user_warehouses'] as $whs) {
            if ($whs['whs_id'] == $whsId) {
                return $whs;
            }
        }
    }
	
    public function getLot($itemId)
    {
		try {
			$palletModel = new PalletModel();
			return $palletModel->getLot($itemId);
		} catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}