<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\InvoiceCreateDataModel;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class InvoiceCreateDataController extends AbstractController
{
    protected $model;

    public function __construct(IRequest $request)
    {
//        $object = new AuthenticationService($request);
//
//        parent::__construct($request, $object);

        $this->model = new InvoiceCreateDataModel();
    }

    /*
    ****************************************************************************
    */

    public function get(Request $request)
    {
        $input = $request->getQueryParams();

        $inv_cr_id = array_get($input, 'inv_cr_id');

        try {

            $results = $this->model->getCreateParams($inv_cr_id);

            return $results['data'];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /*
    ****************************************************************************
    */

    public function save(Request $request)
    {
        $params = $request->getParsedBody();

        $inv_cr_id = $this->model->saveCreateParams($params);

        return $inv_cr_id;
    }

    /*
    ****************************************************************************
    */

}
