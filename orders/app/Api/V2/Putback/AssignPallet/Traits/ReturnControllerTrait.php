<?php

namespace App\Api\V2\Putback\AssignPallet\Traits;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Pallet;
use mPDF;
use Seldat\Wms2\Utils\Helper;

trait ReturnControllerTrait
{
    /**
     * @param $returnHdr
     * @param $itemDetail
     *
     * @return mixed
     */
    private function updateOrCreateReturnDtl($returnHdr, $itemDetail)
    {
        $odrCtn = null;
        if ($itemDetail['new_sku'] == 0) {
            $odrCtn = DB::table('odr_cartons')
                ->select([
                    'sku',
                    'size',
                    'color',
                    'lot',
                    'pack',
                    'item_id',
                    'uom_id',
                    'uom_code',
                    'uom_name'
                ])
                ->where('odr_dtl_id', array_get($itemDetail, 'odr_dtl_id', null))
                ->where('deleted', 0)
                ->groupBy('odr_dtl_id')
                ->first();
        } else {
            $odrCtn = DB::table('item')
                ->select([
                    'sku',
                    'size',
                    'color',
                    'pack',
                    'item_id',
                    'uom_id',
                    'uom_code',
                    'uom_name'
                ])
                ->where('item_id', $itemDetail['item_id'])
                ->where('deleted', 0)
                ->first();
        }

        $originalCtns = 0;
        $newCtns = 0;
        $ctnTtl = $itemDetail['ctns'];
        $qtyKg = array_get($itemDetail, 'qty_kg', 0);
        if ($itemDetail['new_sku'] == 0) {
            // old sku
            $originalCtns += $itemDetail['ctns'];
        } else {
            // new sku
            $newCtns += $itemDetail['ctns'];
        }

        //check existed return detail
        $returnDtlObj = $this->returnDtlModel->getFirstWhere([
            'return_id'  => $returnHdr->return_id,
            'item_id'    => array_get($odrCtn, 'item_id', null),
            'lot'        => array_get($odrCtn, 'lot', 'NA')
        ]);

        if ($returnDtlObj) {
            // update case
            $returnDtlObj->original_ctns += $originalCtns;
            $returnDtlObj->new_ctns  += $newCtns;
            $returnDtlObj->ctn_ttl   += $ctnTtl;
            // $returnDtlObj->piece_qty += round($qtyKg);
            $returnDtlObj->piece_qty += $qtyKg;
            $returnDtlObj->save();

            return $returnDtlObj;
        }

        $paramReturnDtl = [
            'return_id'      => $returnHdr->return_id,
            'return_num'     => $returnHdr->return_num,
            'odr_hdr_num'    => $returnHdr->odr_hdr_num,
            'odr_hdr_id'     => $returnHdr->odr_hdr_id,
            'odr_dtl_id'     => array_get($itemDetail, 'odr_dtl_id', null),
            'ctn_ttl'        => $ctnTtl,
            'original_ctns'  => $originalCtns,
            'new_ctns'       => $newCtns,
            'return_dtl_sts' => 'NW',
            // 'piece_qty'      => round($qtyKg),
            'piece_qty'      => $qtyKg,
            'sku'            => array_get($odrCtn, 'sku', null),
            'size'           => array_get($odrCtn, 'size', null),
            'color'          => array_get($odrCtn, 'color', null),
            'lot'            => array_get($odrCtn, 'lot', 'NA'),
            'pack'           => array_get($odrCtn, 'pack'),
            'item_id'        => array_get($odrCtn, 'item_id'),
            'uom_id'         => array_get($odrCtn, 'uom_id'),
            'uom_code'       => array_get($odrCtn, 'uom_code'),
            'uom_name'       => array_get($odrCtn, 'uom_name'),
            'sts'            => 'i',
        ];

        $this->returnDtlModel->refreshModel();

        return $this->returnDtlModel->create($paramReturnDtl);
    }

    /**
     * @param $returnHdr
     * @param $itemDetail
     * @param $returnDtl
     *
     * @return mixed
     */
    private function createPallet($returnHdr, $itemDetail, $returnDtl)
    {
        $lpnPattern = $itemDetail['plt_rfid'];

        $itemObj = Item::where('item_id', $itemDetail['item_id'])->first();

        $pltNum = (new Pallet())
            ->where('pallet.plt_num', 'LIKE', $lpnPattern . '-%')
            ->orderBy('plt_id', 'desc')
            ->value('plt_num');

        if ($pltNum) {
            $pltNum++;
        } else {
            $pltNum = sprintf('%s-00001', $lpnPattern);
        }

        $ctnTtl = array_get($itemDetail, 'ctns', 0);

        $palletParram = [
            'cus_id'         => object_get($returnHdr, 'odrHdr.cus_id'),
            'whs_id'         => object_get($returnHdr, 'odrHdr.whs_id'),
            'plt_num'        => $pltNum,
            'rfid'           => array_get($itemDetail, 'plt_rfid'),
            'item_id'        => array_get($itemDetail, 'item_id'),
            'sku'            => array_get($itemDetail, 'sku'),
            'size'           => array_get($itemDetail, 'size'),
            'color'          => array_get($itemDetail, 'color'),
            'pack'           => array_get($itemDetail, 'pack'),
            'ctn_ttl'        => $ctnTtl,
            'return_id'      => $returnHdr->return_id,
            'init_ctn_ttl'   => $ctnTtl,
            'init_piece_ttl' => array_get($itemDetail, 'qty_kg', 0),
            'dmg_ttl'        => 0,
            'plt_sts'        => 'NW',
            'spc_hdl_code'   => $itemObj->spc_hdl_code,
            'gr_dt'          => time(),
        ];

        //uom = KG
        if ($itemDetail['uom_code'] === 'KG') {
            $palletParram['weight'] = $itemDetail['qty_kg'];
            // $palletParram['init_piece_ttl'] = round($itemDetail['qty_kg']);
            $palletParram['init_piece_ttl'] = $itemDetail['qty_kg'];
            $palletParram['init_ctn_ttl'] = $ctnTtl;
            $palletParram['pack'] = $ctnTtl;
            $palletParram['ctn_ttl'] = 1;
            $palletParram['uom'] = "PL";
            $palletParram['item_id'] = $itemDetail['item_id'];
            $palletParram['sku'] = $itemDetail['sku'];

        }

        $this->palletModel->refreshModel();

        $pallet = $this->palletModel->create($palletParram);

        $this->createPutBack($returnHdr, $returnDtl, $pallet, $itemDetail);

        return $pallet;
    }

    private function generateUpc128($itemId, $cusId)
    {
        return str_pad($itemId, 5, '0', STR_PAD_LEFT) . str_pad($cusId, 8, '0', STR_PAD_LEFT);
    }

    /**
     * @param $returnHdr
     * @param $pallet
     * @param $input
     * @param $cartons
     */
    private function createNewCartonAndAssignPallet($returnHdr, $pallet, $input)
    {
        $parentObj = $this->itemModel->getFirstBy('item_id', $input['item_id']);
        if ($input['new_sku']) {
            $parentObj->spc_hdl_code = $input['spc_hdl_code'];
            $parentObj->spc_hdl_name = $input['spc_hdl_name'];
        }

        $packSize = array_get($input, 'pack_size');
        $qtyKg = $input['qty_kg'];
        $needToCreate = ceil($qtyKg / $packSize);
        while ($needToCreate > 0) {
            $ctnNum = $this->cartonModel->generateCtnNum();

            $length = array_get($parentObj, "length", 0);
            $width = array_get($parentObj, "width", 0);
            $height = array_get($parentObj, "height", 0);
            $volume = Helper::calculateVolume($length, $width, $height);
            $cube = Helper::calculateCube($length, $width, $height);
            $grDate = time();

            $ucc128 = $this->generateUpc128($input['item_id'], object_get($returnHdr, 'odrHdr.cus_id'));

            $dataCarton = [
                'plt_id'        => $pallet->plt_id,
                'item_id'       => $input['item_id'],
                'cus_id'        => object_get($returnHdr, 'odrHdr.cus_id'),
                'whs_id'        => object_get($returnHdr, 'odrHdr.whs_id'),
                'ctn_num'       => $ctnNum,
                'ctn_sts'       => 'AC',
                'ctn_uom_id'    => object_get($parentObj, 'uom_id'),
                'uom_name'      => object_get($parentObj, 'uom_name'),
                'uom_code'      => object_get($parentObj, 'uom_code'),
                'return_id'     => $returnHdr->return_id,
                'sku'           => object_get($parentObj, "sku"),
                'size'          => object_get($parentObj, "size"),
                'color'         => object_get($parentObj, "color"),
                'lot'           => array_get($input, "lot", "NA"),
                'upc'           => object_get($parentObj, "cus_upc", null),
                'length'        => $length,
                'width'         => $width,
                'height'        => $height,
                'weight'        => object_get($parentObj, "weight", null),
                'ctn_pack_size' => object_get($parentObj, "pack", null),
                'is_damaged'    => 0,
                'cube'          => $cube,
                'volume'        => $volume,
                'gr_dt'         => $grDate,
                'cat_code'      => object_get($parentObj, "cat_code", 'NA'),
                'cat_name'      => object_get($parentObj, "cat_name", 'NA'),
                'des'           => object_get($parentObj, "description", null),
                'spc_hdl_code'  => object_get($parentObj, "spc_hdl_code", 'NA'),
                'spc_hdl_name'  => object_get($parentObj, "spc_hdl_name", 'NA'),
                'ucc128'        => $ucc128,
                'expired_dt'    => null,
            ];

            //uom = KG
            if ($input['uom_code'] === 'KG') {
                $dataCarton['weight'] = $input['qty_kg'];
                $dataCarton['lot'] = $input['lot'] ?? 'NA';
                // $dataCarton['piece_remain'] = $dataCarton['piece_ttl'] = round($qtyKg);
                $dataCarton['piece_remain'] = $dataCarton['piece_ttl'] = $qtyKg;
                $dataCarton['inner_pack'] = $pallet['pack'];
                $needToCreate = 1;
            } else {
                $dataCarton['piece_remain'] = $dataCarton['piece_ttl'] = $qtyKg > $packSize ? $packSize : $qtyKg;
                $qtyKg -= $packSize;
                if ($qtyKg <= 0) {
                    $needToCreate = 1;
                }
            }

            $needToCreate--;
            $this->cartonModel->refreshModel();
            $this->cartonModel->create($dataCarton);
        }

        // init inventory summary
        $invtItm = DB::table('invt_smr')
            ->where([
                'item_id' => $input['item_id'],
                'lot'     => $input['lot'],
            ])
            ->value('inv_sum_id')
        ;

        // $ttl = round($input['qty_kg']);
        $ttl = $input['qty_kg'];
        if (empty($invtItm)) {
            $dataInvtSmr = [
                'item_id'     => $input['item_id'],
                'cus_id'      => object_get($returnHdr, 'odrHdr.cus_id'),
                'whs_id'      => object_get($returnHdr, 'odrHdr.whs_id'),
                'sku'         => object_get($parentObj, "sku", null),
                'size'        => object_get($parentObj, "size", null),
                'color'       => object_get($parentObj, "color", null),
                'lot'         => object_get($parentObj, "lot", "NA"),
                'upc'         => object_get($parentObj, "upc", null),
                'ttl'         => $ttl,
                'picked_qty'  => 0,
                'dmg_qty'     => 0,
                'avail'       => $ttl,
                'back_qty'    => 0,
                'ecom_qty'    => 0,
                'crs_doc_qty' => 0,
                'created_at'  => time(),
                'updated_at'  => time(),
            ];
            DB::table('invt_smr')->insert($dataInvtSmr);
        } else {
            DB::table('invt_smr')
                ->where('inv_sum_id', $invtItm)
                ->update([
                    "avail" => DB::raw("avail + " . $ttl),
                    "ttl"   => DB::raw("ttl + " . $ttl)
                ]);
        }
    }

    /**
     * @param $pallet
     * @param $info
     *
     * @throws \MpdfException
     */
    private function printPDF($pallet, $info)
    {
        $pdf = new Mpdf();

        $userInfo = new \Wms2\UserInfo\Data();
        $userInfo = $userInfo->getUserInfo();
        $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

        $CurrentDate=date('m/d/Y h:i:s a ', strtotime("now"));

        $html = (string)view('PalletAssignPrint', [
            'dataSuggest' => $pallet,
            'info'        => $info,
            'printedBy'   => $printedBy,
            'CurrentDate' => $CurrentDate,
        ]);

        $pdf->WriteHTML($html);
        $pdf->Output("PalletAssign.pdf", "D");

        $dir = storage_path("PutBack/{$info['whs_id']}/{$info['return_id']}");
        $pdf->Output("$dir/{$info['return_num']}.pdf", 'F');
        $pdf->Output("{$info['return_num']}.pdf", 'D');
    }

    /**
     * @param $returnHdr
     * @param $returnDtl
     * @param $pallet
     *
     * @return mixed
     */
    private function createPutBack($returnHdr, $returnDtl, $pallet, $itemDetail)
    {
        $odrCarton = null;
        if ($itemDetail['new_sku'] == 0) {
            $odrCarton = $this->orderCartonModel->getFirstWhere(['odr_dtl_id' => $returnDtl->odr_dtl_id]);
        } else {
            $odrCarton = $this->itemModel->getFirstWhere(['item_id' => object_get($returnDtl, 'item_id')]);
        }

        $ctnTtl = array_get($itemDetail, 'ctns', 0);
        $putBackParam = [
            'return_id'     => object_get($returnHdr, 'return_id'),
            'return_num'    => object_get($returnHdr, 'return_num'),
            'return_dtl_id' => $returnDtl->return_dtl_id,
            'item_id'       => object_get($odrCarton, 'item_id'),
            'sku'           => object_get($odrCarton, 'sku'),
            'size'          => object_get($odrCarton, 'size'),
            'color'         => object_get($odrCarton, 'color'),
            'lot'           => object_get($odrCarton, 'lot', "NA"),
            'pallet_id'     => array_get($pallet, 'plt_id'),
            'is_new'        => 1,
            'sts'           => "i",
            'pb_sts'        => "NW",
            'ctn_ttl'       => $ctnTtl,
            'pack_size'     => $itemDetail['pack_size'],
        ];

        $this->putbackModel->refreshModel();

        return $this->putbackModel->create($putBackParam);
    }

}