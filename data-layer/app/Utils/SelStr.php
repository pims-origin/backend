<?php

namespace App\Utils;

class SelStr
{
    public static function escapeLike($string)
    {
        $search = ['%', '_'];
        $replace = ['\%', '\_'];

        return str_replace($search, $replace, $string);
    }

    public static function removeNullOrEmptyString($array)
    {
        return array_fill($array, function ($v) {
            $v !== null && $v !== '';
        }, ARRAY_FILTER_USE_BOTH);
    }
}