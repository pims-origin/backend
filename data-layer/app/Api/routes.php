<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        // System Country
        $api->get('/system-countries', 'SystemCountryController@search');
        $api->post('/system-countries', 'SystemCountryController@store');
        $api->get('/system-countries/{systemCountryId}', 'SystemCountryController@show');
        $api->put('/system-countries/{systemCountryId}', 'SystemCountryController@update');
        $api->delete('/system-countries/{systemCountryId}', 'SystemCountryController@destroy');
        $api->delete('/system-countries', 'SystemCountryController@deleteMass');

        $api->get('/system-countries/{countryId}/states', 'SystemStateController@listStatesByCountryId');

        // System State
        $api->get('/system-states', 'SystemStateController@search');
        $api->post('/system-states', 'SystemStateController@store');
        $api->get('/system-states/{systemStateId}', 'SystemStateController@show');
        $api->put('/system-states/{systemStateId}', 'SystemStateController@update');
        $api->delete('/system-states/{systemStateId}', 'SystemStateController@destroy');
        $api->delete('/system-states', 'SystemStateController@deleteMass');

        // System Measurement
        $api->get('/system-measurements', 'SystemMeasurementController@search');
        $api->get('/system-measurements/{systemMeasurementCode}', 'SystemMeasurementController@show');

        // Charge Uom
        $api->get('/system-uoms', 'SystemUomController@search');
        $api->get('/system-uoms/{systemUomId}', 'SystemUomController@show');
        $api->post('/system-uoms', 'SystemUomController@store');
        $api->put('/system-uoms/{systemUomId}', 'SystemUomController@update');
        $api->delete('/system-uoms/{systemUomId}', 'SystemUomController@destroy');
        $api->delete('/system-uoms', 'SystemUomController@deleteMass');
    });
});
