<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseMeta;

class WarehouseConfigurationMetaTransformer extends TransformerAbstract
{
    /**
     * @param WarehouseMeta $warehouseMeta
     *
     * @return array
     */
    public function transform(WarehouseMeta $warehouseMeta)
    {
        return [
            'whs_id'         => array_get($warehouseMeta, 'whs_id', ''),
            'whs_qualifier'  => array_get($warehouseMeta, 'whs_qualifier', ''),
            'whs_meta_value' => array_get($warehouseMeta, 'whs_meta_value', ''),
        ];
    }
}
