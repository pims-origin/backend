<?php

namespace App\Api\V1\Controllers;

use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\EventTrackingModel;

class RelocationController extends AbstractController
{
	protected $eventTrackingModel;
	protected $locationModel;
	protected $palletModel;
	protected $cartonModel;
	protected $customerModel;

	public function __construct(
		EventTrackingModel $eventTrackingModel,
		LocationModel $locationModel,
		PalletModel $palletModel,
		CartonModel $cartonModel,
		CustomerModel $customerModel
	)
	{
		$this->eventTrackingModel = $eventTrackingModel;
		$this->locationModel = $locationModel;
		$this->palletModel = $palletModel;
		$this->cartonModel = $cartonModel;
		$this->customerModel = $customerModel;
	}

	public function getCustomerByLocation(Request $request, $whsId)
	{
		$input = $request->getParsedBody();

		if(empty($input['loc_code'])) {
			return $this->response->errorBadRequest("Location Code is required!");
		}

		$location = $this->locationModel->getFirstWhere([
			'deleted'	=> 0,
			'loc_whs_id' => $whsId,
			'loc_code' 	=> $input['loc_code']
		]);

		$this->checkLocation($location);

		$pallet = $this->palletModel->getFirstWhere([
			'deleted'	=> 0,
			'whs_id' 	=> $whsId,
			'loc_code'	=> $input['loc_code']
		]);

		if (!$pallet) {
	        return $this->response->errorBadRequest("From Location is empty");
	    }

		$customer = $this->customerModel->getFirstWhere([
			'deleted'	=> 0,
			'cus_id' => $pallet['cus_id'],
			'cus_status'	=>	'AC'
		]);

		return ['data' => ['cus_name' => $customer['cus_name']]];
	}

	public function relocation_backup(Request $request, $whsId)
	{
		try {
			$input = $request->getParsedBody();

			$this->validateRelocation($input);

			// Check From Location
			$fromLoc = $this->locationModel->getFirstWhere([
				'deleted'	=> 0,
				'loc_whs_id' => $whsId,
				'loc_code' 	=> $input['from']
			]);

			$this->checkLocation($fromLoc);

			$fromPallet = $this->palletModel->getFirstWhere([
				'deleted'	=> 0,
				'whs_id' 	=> $whsId,
				'loc_code'	=> $input['from'],
			]);

			if (!$fromPallet) {
		        return $this->response->errorBadRequest("From Location is empty");
		    }

		    if ($fromPallet['plt_sts'] != 'AC') {
		    	return $this->response->errorBadRequest("From Pallet status must be Active");
		    }

		    // Check To Location
		    $toLoc = $this->locationModel->getFirstWhere([
				'deleted'	=> 0,
				'loc_whs_id' => $whsId,
				'loc_code' 	=> $input['to']
			]);

			$this->checkLocation($toLoc, 'To');

			$checkEmptyToLoc = $this->palletModel->checkWhere([
				'deleted'	=> 0,
				'whs_id' 	=> $whsId,
				'loc_code'	=> $input['to']
			]);

			if ($checkEmptyToLoc > 0) {
		        return $this->response->errorBadRequest("To Location is not empty");
		    }

		    if ($fromLoc['spc_hdl_code'] != $toLoc['spc_hdl_code']) {
		        return $this->response->errorBadRequest("From Location and To Location are not same Special Handling");
		    }

		    DB::beginTransaction();

		    // Update Dymanic Zone
		    if (!empty($fromPallet['loc_id'])) {
		        // Check and remove dynamic zone
		        DB::table('location')->join('zone', 'location.loc_zone_id', '=', 'zone.zone_id')
		            ->where([
		                'zone.dynamic'    => 1,
		                'location.loc_id' => $fromPallet['loc_id']
		            ])
		            ->update([
		                'location.loc_zone_id' => null,
		                'zone.zone_num_of_loc' => DB::raw('zone.zone_num_of_loc - 1')
		            ]);
		    }

		    if (empty($toLoc['loc_zone_id'])) {
		        $dynZoneId = $this->locationModel->getDynZone($whsId, $fromPallet['cus_id']);

		        if (!$dynZoneId) {
		            return $this->response->errorBadRequest("To Location has't the zone");
		        }

		        // Update zone num of loc
		        DB::table('zone')->where('zone_id', $dynZoneId)
		            ->update([
		                'zone_num_of_loc' => DB::raw('zone_num_of_loc + 1')
		            ]);

		        // Update location dynamic zone
		        $this->locationModel->updateWhere(['loc_zone_id' => $dynZoneId], ['loc_id' => $toLoc['loc_id']]);
		    } else {
		        $cusZone = DB::table('customer_zone')->where([
		            "cus_id"  => $fromPallet['cus_id'],
		            "zone_id" => $toLoc['loc_zone_id']
		        ])
		            ->first();

		        if (empty($cusZone)) {
		            return $this->response->errorBadRequest("To Location doesn't belong to customer");
		        }
		    }

			$this->palletModel->updateWhere([
				'loc_id'   => $toLoc['loc_id'],
				'loc_code' => $input['to'],
				'loc_name' => $input['to'],
			], [
				'plt_id' => $fromPallet['plt_id']
			]);

			$this->cartonModel->updateWhere([
				'loc_id'   => $toLoc['loc_id'],
				'loc_code' => $input['to'],
				'loc_name' => $input['to']
			], [
				'plt_id' => $fromPallet['plt_id']
			]);

			$event = [
				'whs_id'    => $whsId,
				'cus_id'    => $fromPallet['cus_id'],
				'owner'     => $input['from'],
				'evt_code'  => 'RFR',
				'trans_num' => $input['to'],
				'info'      => sprintf("RF Gun - Update Relocate from %s to %s", $input['from'], $input['to']),
			];

	        //  Evt tracking
			$this->eventTrackingModel->refreshModel();
			$this->eventTrackingModel->create($event);

			DB::commit();

		    $res['data'] = [
		        'message' => sprintf('Relocate location %s to %s successfully!', $input['from'], $input['to'])
		    ];

	   		return $res;

	   	} catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
	}

    public function relocation(Request $request, $whsId)
    {
        try {
            $input = $request->getParsedBody();

            $this->validateRelocation($input);

            // Check From Location
            $fromLoc = $this->locationModel->getFirstWhere([
                'deleted'	=> 0,
                'loc_whs_id' => $whsId,
                'loc_code' 	=> $input['from']
            ]);

            $this->checkLocation($fromLoc);
            // Check To Location
            $toLoc = $this->locationModel->getFirstWhere([
                'deleted'	=> 0,
                'loc_whs_id' => $whsId,
                'loc_code' 	=> $input['to']
            ]);

            $this->checkLocation($toLoc, 'To');

            $checkEmptyToLoc = $this->palletModel->checkWhere([
                'deleted'	=> 0,
                'whs_id' 	=> $whsId,
                'loc_code'	=> $input['to']
            ]);

            if ($checkEmptyToLoc > 0) {
                return $this->response->errorBadRequest("To Location is not empty");
            }

            if ($fromLoc['spc_hdl_code'] != $toLoc['spc_hdl_code']) {
                return $this->response->errorBadRequest("From Location and To Location are not same Special Handling");
            }

            $fromPallets = $this->palletModel->findWhere([
                'deleted'	=> 0,
                'whs_id' 	=> $whsId,
                'loc_code'	=> $input['from'],
            ]);
            if(!empty($fromPallets)){
                DB::beginTransaction();
                foreach ($fromPallets as $fromPallet) {
                    if (!$fromPallet) {
                        return $this->response->errorBadRequest("From Location is empty");
                    }

                    if ($fromPallet['plt_sts'] != 'AC') {
                        return $this->response->errorBadRequest("From Pallet status must be Active");
                    }




                    // Update Dymanic Zone
                    if (!empty($fromPallet['loc_id'])) {
                        // Check and remove dynamic zone
                        DB::table('location')->join('zone', 'location.loc_zone_id', '=', 'zone.zone_id')
                            ->where([
                                'zone.dynamic'    => 1,
                                'location.loc_id' => $fromPallet['loc_id']
                            ])
                            ->update([
                                'location.loc_zone_id' => null,
                                'zone.zone_num_of_loc' => DB::raw('zone.zone_num_of_loc - 1')
                            ]);
                    }

                    if (empty($toLoc['loc_zone_id'])) {
                        $dynZoneId = $this->locationModel->getDynZone($whsId, $fromPallet['cus_id']);

                        if (!$dynZoneId) {
                            return $this->response->errorBadRequest("To Location has't the zone");
                        }

                        // Update zone num of loc
                        DB::table('zone')->where('zone_id', $dynZoneId)
                            ->update([
                                'zone_num_of_loc' => DB::raw('zone_num_of_loc + 1')
                            ]);

                        // Update location dynamic zone
                        $this->locationModel->updateWhere(['loc_zone_id' => $dynZoneId], ['loc_id' => $toLoc['loc_id']]);
                    } else {
                        $cusZone = DB::table('customer_zone')->where([
                            "cus_id"  => $fromPallet['cus_id'],
                            "zone_id" => $toLoc['loc_zone_id']
                        ])
                            ->first();

                        if (empty($cusZone)) {
                            return $this->response->errorBadRequest("To Location doesn't belong to customer");
                        }
                    }

                    $this->palletModel->updateWhere([
                        'loc_id'   => $toLoc['loc_id'],
                        'loc_code' => $input['to'],
                        'loc_name' => $input['to'],
                    ], [
                        'plt_id' => $fromPallet['plt_id']
                    ]);

                    $this->cartonModel->updateWhere([
                        'loc_id'   => $toLoc['loc_id'],
                        'loc_code' => $input['to'],
                        'loc_name' => $input['to']
                    ], [
                        'plt_id' => $fromPallet['plt_id']
                    ]);

                    $event = [
                        'whs_id'    => $whsId,
                        'cus_id'    => $fromPallet['cus_id'],
                        'owner'     => $input['from'],
                        'evt_code'  => 'RFR',
                        'trans_num' => $input['to'],
                        'info'      => sprintf("RF Gun - Update Relocate from %s to %s", $input['from'], $input['to']),
                    ];

                    //  Evt tracking
                    $this->eventTrackingModel->refreshModel();
                    $this->eventTrackingModel->create($event);
                }
            }
            DB::commit();

            $res['data'] = [
                'message' => sprintf('Relocate location %s to %s successfully!', $input['from'], $input['to'])
            ];

            return $res;

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
	public function checkLocation($location, $type = 'From')
	{
	    if (!$location) {
	        return $this->response->errorBadRequest($type . " Location is not existed");
	    }

	    if ($location['loc_sts_code'] == 'LK') {
	        return $this->response->errorBadRequest($type . " Location is locked");
	    }

	    if ($location['loc_sts_code'] == 'IA') {
	        return $this->response->errorBadRequest($type . " Location is inactive");
	    }

	    if ($location['loc_sts_code'] != 'AC') {
	        return $this->response->errorBadRequest($type . " Location is not active");
	    }
	}

	protected function validateRelocation($attribute) {

		if(empty($attribute['from'])) {
			return $this->response->errorBadRequest("From Location Code is required!");
		}

		if(empty($attribute['to'])) {
			return $this->response->errorBadRequest("To Location Code is required!");
		}

		if($attribute['from'] === $attribute['to']) {
			return $this->response->errorBadRequest("To Location must be different From Location!");
		}

	}
}