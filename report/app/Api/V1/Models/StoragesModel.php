<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Storages;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelStr;

class StoragesModel extends AbstractModel
{
    public function __construct(Storages $model = null)
    {
        $this->model = ($model) ?: new Storages();
    }

    /*
    ***************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    { 

        $c_status = config('constants.ctn_status');
        $searchAttr = SelArr::removeNullOrEmptyString($attributes);
         $query = $this->make($with)
                ->select(DB::raw("customer.cus_name, cartons.*, SUM(cartons.piece_remain) as sum_piece_remain, warehouse.whs_name"))
                ->leftJoin('location', 'location.loc_id', '=', 'cartons.loc_id')
                ->leftjoin('customer', 'customer.cus_id','=', 'cartons.cus_id' )
                ->leftjoin('warehouse', 'warehouse.whs_id','=', 'cartons.whs_id')
                ->where('cartons.whs_id', $attributes['whs_id'] )
                ->whereIn('cartons.ctn_sts', [ $c_status['ACTIVE'], $c_status['LOCKED'], $c_status['PICKED'] ]);

                if (!empty($attributes['cus_id'])) {
                    $query->where('cartons.cus_id', $attributes['cus_id']);
                }
                if (!empty($attributes['sku'])) {
                    $query->where("cartons.sku", 'like', "%".SelStr::escapeLike($attributes['sku'])."%");
                }
                if(!empty($attributes['from']) && !empty($attributes['to'])){
                    $query->where(function($query ) use ($attributes, $c_status ){
                            $query->orWhere(function ($query)use($attributes,$c_status) {
                            $query->whereBetween('cartons.gr_dt',[strtotime($attributes['from']),strtotime($attributes['to'])]);
                        });
                    });
                }

        $query->groupBy([
                'cartons.whs_id',
                'cartons.item_id',
                'cartons.loc_id',
                'cartons.lot',
                'cartons.ctn_sts',
            ]);
           

        return $this->customRunSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

}
