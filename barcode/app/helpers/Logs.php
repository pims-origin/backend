<?php
namespace Helper;

class Logs
{
    public static function log($payload){

        $response = [
            'success' => true,
            'payload' => []
        ];
        $validation = \Validators::make(
            $payload,
            array(
                'log_date' => array('required',),
                'id_user' => array('required','numeric'),
                'system' => array('required','max:100'),
                'module_type' => array('required','max:1000'),
                'action' => array('required','max:250'),
                'description' => array('required'),
                'user_ip' => array('required','ip'),
                'data' => array('required'),
            )
        );

        if ($validation->fails()) {
            $errors = [];
            $messages = $validation->messages();
            foreach ($messages->all() as $message) {
                //
                $errors[] = ($message);
            }

            $response['success'] = false;
            $response['payload']['msg'] = $errors;
            return $response;
        }
        // finish validate
        Queue::push(Log::class, array('payload' => $payload));

        return $response;
    }
}