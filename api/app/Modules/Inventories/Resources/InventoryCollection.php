<?php

namespace App\Modules\Inventories\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InventoryCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => InventoryResource::collection($this->collection)
        ];
    }
}