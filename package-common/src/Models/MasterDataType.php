<?php
namespace Seldat\Wms2\Models;

/**
 * Class MasterDataType
 *
 * @property mixed md_type
 * @property mixed mdt_name
 * @property mixed mdt_des
 * @property mixed mdt_data
 * @property mixed md_sts
 *
 * @package Seldat\Wms2\Models
 */
class MasterDataType extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'master_data_type';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;
    /**
     * @var array
     */
    protected $fillable = [
        'md_type',
        'mdt_name',
        'mdt_des',
        'mdt_data',
        'md_sts',
    ];

    /**
     * @return Created user
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return Updated user
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'user_id');
    }
}
