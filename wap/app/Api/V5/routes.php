<?php

$api->group(['prefix' => '/v5/whs/{whsId:[0-9]+}/ib/pallet/', 'namespace' => 'App\Api\V5\Inbound\Pallet'], function ($api) {
    $api->put('scan-pallet',
        ['uses' => 'ScanPallet\Controllers\PalletController@assignCartonsToPallet']);
});
