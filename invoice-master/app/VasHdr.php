<?php

namespace App;


use App\Api\V1\Models\BaseInvoiceModel;

class VasHdr extends BaseInvoiceModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vas_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'vas_id';

    /**
     * @var array
     */
    protected $fillable = [
        'vas_num',
        'cus_id',
        'whs_id',
        'subtotal',
        'tax',
        'amount_ttl',
        'vas_sts',
        'vas_note',
        'is_order',
        'odr_hdr_id',
        'created_at',
        'created_by',
    ];

    public function vasDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\VasDtl', 'vas_id', 'vas_id');
    }

    public function vasDtlOrder()
    {
        return $this->hasMany(__NAMESPACE__ . '\VasDtl', 'vas_id', 'vas_id')->order();
    }

    public function customer() {
        return $this->belongsTo('App\Api\V1\Models\CustomerModel', 'cus_id', 'cus_id');
    }

    public function createdBy() {
        return $this->belongsTo('App\Api\V1\Models\UserModel', 'created_by', 'user_id');
    }

    public function order() {
        return $this->belongsTo(__NAMESPACE__.'\OrderHdr', 'odr_hdr_id', 'odr_id');
    }
}
