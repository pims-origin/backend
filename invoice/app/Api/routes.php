<?php
$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['SELDAT INVOICE API V1'];
        });

       //List invoice
        $api->get('/list-invoice', 'InvoiceListController@getInvoices');
        $api->get('/invoice-details', 'InvoiceDetailsController@getDetails');
        $api->post('/invoice-process', 'ProcessInvoicesController@processInvoice');
        $api->post('/invoice-process/saveCreateParams',
                'InvoiceCreateDataController@save');
        $api->get('/invoice-process/getCreateParams',
                'InvoiceCreateDataController@get');
        $api->get('/invoice-process/inv/{invNum:[0-9]+}',
                'ProcessInvoicesController@displayInvoicedInvoice');
        $api->get('/invoice-process/checkSummaryDate',
                'ProcessInvoicesController@checkSummaryDate');
        $api->post('/cancel-invoice/{inv_num:[0-9]+}',
                'ProcessInvoicesController@cancelInvoice');
        $api->put('/make-payment', 'MakePaymentController@updatePayment');
        $api->post('/store-invoice/{cus_id:[0-9]+}/{inv_num:[0-9]+}',
                'CreateInvoicesController@storeInvoice');

        //customer
        $api->get('/list-invoice/bill-customer',
                'InvoiceListController@billableCustomer');

        // client info
        $api->get('/client-info/get', 'ClientMasterController@getClientInfo');
        $api->post('/client-info/update/{custID:[0-9]+}',
                'ClientMasterController@update');

        //Client Contacts
        $api->get('/client-contacts/get', 'ClientContactsController@get');
        $api->post('/client-contacts/setDefault/{contactID:[0-9]+}/{clientID:[0-9]+}',
                'ClientContactsController@setDafaultContact');
        $api->post('/client-contacts/addContact/{clientID:[0-9]+}',
                'ClientContactsController@addContact');
        $api->post('/client-contacts/remove',
                'ClientContactsController@removeContacts');

        //Invoice Costs
        $api->get('/invoice-costs/check-charge-code',
                'InvoiceCostsController@checkChagreCode');
        $api->get('/invoice-costs/get', 'InvoiceCostsController@index');
        $api->post('/invoice-costs/update', 'InvoiceCostsController@updateCosts');

        //summary table
        $api->post('/receiving-summary',
                'InvoiceSummaryController@receivingInvoiceSummary');
        $api->post('/receiving-masterSummary',
                'InvoiceSummaryController@receivingMasterSummary');
        $api->post('/storage-masterSummary',
                'InvoiceSummaryController@storageMasterSummary');
        $api->post('/order-masterSummary',
                'InvoiceSummaryController@orderProcessingMasterSummary');
        $api->post('/migrateStorage-masterSummary',
                'InvoiceSummaryController@migrateStorageMasterSummary');

        //Make Payment
        $api->post('/receive-payment/{inv_num:[0-9]+}',
                'MakePaymentController@receivePayment');

        //Print Invoice
        $api->post('/print-invoice', 'CreateInvoicesController@printInvoice');
        $api->get('/get-print-statement-data',
                'CreateInvoicesController@getInvoiceStatementData');
        $api->post('/print-statement',
                'CreateInvoicesController@printStatementInvoice');

        //View Detail
        $api->get('/view-details/receiving',
                'CreateInvoicesController@getReceivingDetails');
        $api->get('/view-details/order-processing',
                'CreateInvoicesController@getOrderProcessingDetails');

        //Testing
        $api->get('/test-invoice', 'InvoiceListController@listInvoice');

        //financial revenue - get current month revenue and YTD revenue
        $api->get('/current-revenue-details',
            'InvoiceListController@getCurrentRevenue');
    });
});
