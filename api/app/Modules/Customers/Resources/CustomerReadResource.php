<?php

namespace App\Modules\Customers\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerReadResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'customers',
            'id' => (int) $this->cus_id,
            'attributes' => [
                'code'          => $this->cus_code,
                'name'          => $this->cus_name,
                'description'   => $this->cus_des
            ],
            "relationships" => [
                'addresses'     => AddressResource::collection($this->customerAddress),
                'contacts'      => ContactResource::collection($this->customerContact),
                'warehouses'    => WarehouseResource::collection($this->warehouse)
            ]
        ];
    }
}