<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerAddressModel;
use App\Api\V1\Models\CustomerChargeDetailModel;
use App\Api\V1\Models\CustomerColorModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\CustomerWarehouseModel;
use App\Api\V1\Models\CustomerZoneModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\CustomerContactModel;
use App\Api\V1\Models\WODetailModel;
use App\Api\V1\Models\ZoneModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Transformers\CustomerConfigsTransformer;
use App\Api\V1\Validators\CustomerConfigValidator;
use App\Api\V1\Validators\CustomerSaveMassValidator;
use App\Api\V1\Transformers\CustomerTransformer;
use App\Api\V1\Transformers\CustomerListTransformer;
use App\Api\V1\Validators\DeleteMassCustomerValidator;
use App\Api\V1\Validators\CustomerValidator;
use Dingo\Api\Facade\Route;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use Wms2\UserInfo\Data;

class CustomerConfigController extends AbstractController
{
    /*
     * @var CustomerModel
     * */
    protected $customerModel;

    /**
     * @var CustomerColorModel
     */
    protected $customerColorModel;
    /*
     * @var $customerSaveMassValidator
     * */
    protected $customerSaveMassValidator;
    /*
     * @var $customerTransformer
     * */
    protected $customerTransformer;
    /*
     * @var $customerZoneModel
     * */
    protected $customerZoneModel;
    /*
     * @var $customerWarehouseModel
     * */
    protected $customerWarehouseModel;
    /*
     * @var $customerAddressModel
     * */
    protected $customerAddressModel;
    /*
     * @var $customerContactModel
     * */
    protected $customerContactModel;
    /*
     * @var $customerChargeDetailModel
     * */
    protected $customerChargeDetailModel;
    /*
     * @var $zoneModel
     * */
    protected $zoneModel;
    /*
     * @var $customerListTransformer
     * */
    protected $customerListTransformer;
    /*
     * @var $customerValidator
     * */
    protected $customerValidator;
    /*
     * @var $delMassCustomerValidator
     * */
    protected $delMassCustomerValidator;

    /**
     * @var CustomerConfigsTransformer
     */
    protected $customerConfigsTransformer;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var CustomerConfigValidator
     */
    protected $customerConfigValidate;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var $cartonModel
     */
    protected $cartonModel;

    public function __construct(
        LocationModel $locationModel,
        CartonModel $cartonModel
    ) {
        parent::__construct();

        $this->customerModel = new CustomerModel();
        $this->customerColorModel = new CustomerColorModel();
        $this->customerSaveMassValidator = new CustomerSaveMassValidator();
        $this->customerTransformer = new CustomerTransformer();
        $this->customerZoneModel = new CustomerZoneModel();
        $this->customerWarehouseModel = new CustomerWarehouseModel();
        $this->customerAddressModel = new CustomerAddressModel();
        $this->customerContactModel = new CustomerContactModel();
        $this->customerChargeDetailModel = new CustomerChargeDetailModel();
        $this->zoneModel = new ZoneModel();
        $this->customerListTransformer = new CustomerListTransformer();
        $this->customerValidator = new CustomerValidator();
        $this->delMassCustomerValidator = new DeleteMassCustomerValidator();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->customerConfigsTransformer = new CustomerConfigsTransformer();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->customerConfigsTransformer = new CustomerConfigsTransformer();
        $this->customerConfigValidate = new CustomerConfigValidator();
        $this->locationModel = $locationModel;
        $this->cartonModel = $cartonModel;
    }

    /**
     * @param Request $request
     * @param CustomerConfigsTransformer $customerConfigsTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getCustomerEdi(
        Request $request,
        CustomerConfigsTransformer $customerConfigsTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $customerConfig = $this->customerConfigModel->search($input, [
                'customer',
                'warehouse'
            ], array_get($input, 'limit'));

            return $this->response->paginator($customerConfig, $customerConfigsTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getCustomerEmail($cusId)
    {
        $db = DB::table('cus_meta')
        ->where('cus_id', $cusId)
        ->where('qualifier', 'EEX')
        ->first();

        if(is_null($db)) {
            $email = '';
        }else{
            $email = json_decode($db['value'])->email;
        }

        $res['data'] = [
            'cus_email'   => $email,
        ];

        return response()->json($res);
    }

    public function updateCustomerEmail(Request $request, $cusId)
    {
        $input = $request->getParsedBody();

        $this->customerValidate($input);

        $countMeta = DB::table('cus_meta')
        ->where('cus_id', $cusId)
        ->where('qualifier', 'EEX')
        ->get();

        $value = json_encode(['email' => $input['cus_email']]);

        if(count($countMeta) > 0) {
            DB::table('cus_meta')
            ->where('cus_id', $cusId)
            ->where('qualifier', 'EEX')
            ->update([
                'value'         => $value,
                'updated_at'    => time()
            ]);
        }else{
            DB::table('cus_meta')
            ->where('cus_id', $cusId)
            ->where('qualifier', 'EEX')
            ->insert([
                'cus_id'        => $cusId,
                'value'         => $value,
                'created_at'    => time(),
                'updated_at'    => time(),
                'deleted'       => 0,
                'qualifier'     => 'EEX'
            ]);
        }
        

        $res['data'] = [
            'message'   => "Update email successfully!",
        ];

        return response()->json($res);
    }

    public function customerValidate($attribute)
    {
        if(empty($attribute['cus_email'])) {
            return $this->response->errorBadRequest("This field cus_email is required!");
        }
    }
}
