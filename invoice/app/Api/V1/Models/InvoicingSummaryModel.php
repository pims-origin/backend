<?php

namespace App\Api\V1\Models;

use App\Invoice;

use Seldat\Wms2\Models\InvoicingSummary;
use Seldat\Wms2\Models\EventTracking;

use Illuminate\Support\Facades\DB;

class InvoicingSummaryModel extends AbstractModel
{
    protected $model;

    /**
    *  Constructor
    *
    */
    public $volume = [];
    
    protected $event;
    

    /**
    *  Model constructor.
    */
    
    public function __construct(InvoicingSummary $model = NULL)
    {
        $this->model = ($model) ?: new InvoicingSummary();
        
        $this->event = new EventTracking();
    }

    /**
    * @type string
    * 
    * @return array
    */
    
    public function getMaxDate($type)
    {
        $maxDate = DB::Table('invoice_summary')
                    ->select(DB::raw('MAX(dt) AS maxDate'))
                    ->join('charge_type', 'invoice_summary.chg_type_id', '=', 'charge_type.chg_type_id')
                    ->where('charge_type.chg_type_name', $type)
                    ->first();
        
        return $maxDate;
    }
    
    /**
     * @param array row
     * @return array
    */
    
    public function checkVolumeRates($row, $val) 
    {
        $result =  DB::table('inv_vol_rates AS iv')
                        ->join('charge_code AS cc', 'cc.chg_code_id', '=', 'iv.chg_code_id')
                        ->join('charge_type AS ch', 'ch.chg_type_id', '=', 'cc.chg_type_id')
                        ->join('system_uom AS su', 'su.sys_uom_id', '=', 'cc.chg_uom_id')
                        ->select('iv.cus_id', 'iv.whs_id', 'iv.min_vol', 'iv.max_vol')
                        ->where('iv.cus_id', $row['cus_id'])
                        ->where('iv.whs_id', $row['whs_id'])
                        ->where('iv.chg_type_id', $val['chg_type_id'])
                        ->whereIn('iv.chg_code_id', $val['chg_code_id'])
                        ->where('iv.deleted', 0)
                        ->first();
      
       return $result;
    }
   
    /**
     * @type string
     * @return array
    */
    
    public function volumeRanges($type)
    {
        $volumeUoms = $this->getVolRangeUOMs();

        $volumeRes = DB::table('inv_vol_rates AS iv')
                        ->join('charge_code AS cc', 'cc.chg_code_id', '=', 'iv.chg_code_id')
                        ->join('charge_type AS ch', 'ch.chg_type_id', '=', 'cc.chg_type_id')
                        ->join('system_uom AS su', 'su.sys_uom_id', '=', 'cc.chg_uom_id')
                        ->select('iv.cus_id', 'iv.whs_id', 'sys_uom_name AS uom', 'iv.min_vol', 'iv.max_vol')
                        ->where('ch.chg_type_name', $type)
                        ->whereIn('sys_uom_name', $volumeUoms)
                        ->where('iv.deleted', 0)
                        ->get();

        foreach ($volumeRes as $row) {
            $cusID = $row->cus_id;
            $whsID = $row->whs_id;
            $uom = $row->uom;
            
            $this->volume[$type][$whsID][$cusID][$uom]['min_vol'] = $row->min_vol;
            $this->volume[$type][$whsID][$cusID][$uom]['max_vol'] = $row->max_vol;
        }

        return $this;
    }
    
    /**
     * @type string
     * @return array
    */
    
    
    public function custVolumeRange($type , $param)
    {
        $cusID = $param['cus_id'];
        $whsID = $param['whs_id'];
        
        return getDefault($this->volume[$type][$whsID][$cusID], []);
    }
    
    /**
     * @type string
     * @return array
    */
    
    
    public function getRcvDate($cusID=NULL, $whsID=NULL)
    {
        $minRcv = $this->event
                        ->select(DB::raw('MIN(FROM_UNIXTIME(created_at, "%Y-%m-%d")) AS minDate'))
                        ->where('evt_code', '=', config('constants.event.GR-COMPLETE'))
                        ->where(function($q) use($cusID, $whsID) {
                            $cusID ? $q->whereIn('cus_id', $cusID) : NULL;
                            $whsID ? $q->where('whs_id', $whsID) : NULL;
                        })
                        ->first();
        
       $rcvDate = $minRcv->minDate;
       
       return $rcvDate;
    }
    
    /**
    * @return array
    */
    
    public function getVolRangeUOMs() 
    {
        return [
            Invoice::$smallCarton,
            Invoice::$mediumCarton,
            Invoice::$largeCarton,
            Invoice::$xlCarton,
	    Invoice::$xxlCarton,
            Invoice::$smallCartonImperial,
            Invoice::$mediumCartonImperial,
            Invoice::$largeCartonImperial,
            Invoice::$xlCartonImperial,
            Invoice::$xxlCartonImperial,
            Invoice::$smallCartonMetric,
            Invoice::$mediumCartonMetric,
            Invoice::$largeCartonMetric,
            Invoice::$xlCartonMetric,
            Invoice::$xxlCartonMetric
        ];
    }
    
    /**
    * @type string
    * 
    * @return array
    */
    
    public function getCustomerMaxDate($type, $cus, $whs)
    {
        $maxDate = DB::Table('invoice_summary')
                    ->select(DB::raw('MAX(dt) AS maxDate'))
                    ->join('charge_type', 'invoice_summary.chg_type_id', '=', 'charge_type.chg_type_id')
                    ->where('charge_type.chg_type_name', $type)
                    ->where('cus_id', $cus)
                    ->where('whs_id', $whs)
                    ->first();
        
        return $maxDate;
    }
}
