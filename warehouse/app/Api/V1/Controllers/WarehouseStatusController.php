<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\WarehouseStatusModel;
use App\Api\V1\Transformers\WarehouseStatusTransformer;
use App\Api\V1\Validators\WarehouseStatusValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class WarehouseStatusController extends AbstractController
{
    /**
     * @var $warehouseStatusModel
     */
    protected $warehouseStatusModel;

    /**
     * @var $warehouseStatusTransformer
     */
    protected $warehouseStatusTransformer;

    /**
     * WarehouseStatusController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseStatusModel = new WarehouseStatusModel();
        $this->warehouseStatusTransformer = new WarehouseStatusTransformer();
    }

    /**
     * @SWG\Get(
     *     path="/v1/warehouseStatus/search",
     *     summary="Search Zone Location Status",
     *     description="Search Zone Location Status",
     *     operationId="searchWarehouseStatus",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Return all found result of Zone Location Status",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="whs_zone_location_status_id", type="integer", description=""),
     *             @SWG\Property(property="whs_loc_sts_qualifier", type="string", description=""),
     *             @SWG\Property(property="whs_loc_sts_desc", type="string", description=""),
     *             @SWG\Property(property="created_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="updated_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="deleted_at", type="string", format="date-time", description=""),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_zone_location_status_id": 2,
     *                     "whs_loc_sts_qualifier": "ddd",
     *                     "whs_loc_sts_desc": "cccccccccc",
     *                     "created_at": {
     *                         "date": "2016-06-02 14:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "updated_at": {
     *                         "date": "2016-06-02 14:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": 915148800
     *                 },
     *             },
     *         },
     *     ),
     * )
     *
     * Zone Location Status Search
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $warehouseStatus = $this->warehouseStatusModel->search($input);

            return $this->response->paginator($warehouseStatus, $this->warehouseStatusTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
