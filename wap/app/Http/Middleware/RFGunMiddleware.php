<?php
/**
 * Created by PhpStorm.
 * User: phuctran
 * Date: 11/05/2017
 * Time: 10:32
 */

namespace App\Http\Middleware;

use Closure;
use Dingo\Api\Facade\API;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RFGunMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $client = $_SERVER['HTTP_USER_AGENT'];
        if (0 === strpos($client, 'okhttp')) {
            if(!$request->hasHeader('gun-version')){
                return API::response()->error('This version is out of date', Response::HTTP_GONE);
            }
        }

        return $next($request);
    }
}