<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycleHdr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('cycle_hdr')) {
            Schema::create('cycle_hdr', function (Blueprint $table) {
                $table->increments('cycle_hdr_id');
                $table->string('cycle_name', 50);
                $table->integer('whs_id');
                $table->string('cycle_method', 5);
                $table->string('count_by', 10);
                $table->char('cycle_type', 3);
                $table->text('cycle_detail')->nullable();
                $table->text('description')->nullable();
                $table->integer('assignee_id');
                $table->date('due_dt');
                $table->tinyInteger('has_color_size');
                $table->char('cycle_sts', 2);
                $table->integer('created_by');
                $table->integer('updated_by');
                $table->integer('created_at');
                $table->integer('updated_at');
                $table->char('sts', 1);
                $table->tinyInteger('deleted');
                $table->integer('deleted_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cycle_hdr')) {
            Schema::drop('cycle_hdr');
        }
    }
}
