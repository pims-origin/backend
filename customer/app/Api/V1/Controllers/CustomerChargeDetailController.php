<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerChargeDetailModel;
use App\Api\V1\Models\InvoiceCostModel;
use App\Api\V1\Transformers\CustomerChargeDetailTransformer;
use App\Api\V1\Validators\CustomerChargeDetailValidator;
use App\Api\V1\Validators\InvoiceCostValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\InvoiceCost;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\V1\Validators\CustomerChargeDetailDeleteMassValidator;

class CustomerChargeDetailController extends AbstractController
{
    /**
     * @var $customerChargeDetailModel
     */
    protected $invoiceCostModel;

    /**
     * @var customerChargeDetailValidator
     */
    protected $customerChargeDetailValidator;


    /**
     * CustomerChargeDetailController constructor.
     *
     */
    public function __construct()
    {
        $this->invoiceCostModel = new InvoiceCostModel();

    }
    /*
    public function saveMass(
        $customerId,
        InvoiceCostValidator $invoiceCostValidator,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['cus_id'] = $customerId;
        // validation
        $invoiceCostValidator->validate($input);

        $n = count($input['whs_id']);
        if ($n != count($input['chg_code_id']) ||
            $n != count($input['chg_cd_price']) ||
            (!empty($input['chg_cd_']) && $n != count($input['chg_cd_des']))
        ) {
            return $this->response->errorBadRequest('Some Input Missing!');
        }

        try {
            $allCustomerDetail = $this->invoiceCostModel->loadBy(['cus_id' => $customerId])->toArray();
            // Update Mass
            for ($i = 0; $i < $n; $i++) {
                $this->invoiceCostModel->updateCustomerChangeDetail([
                    'cus_id'              => $customerId,
                    'whs_id'              => $input['whs_id'][$i],
                    'chg_code_id'         => $input['chg_code_id'][$i],
                    'chg_cd_price' => $input['chg_cd_price'][$i],
                    'chg_cd_des'  => array_get($input, "chg_cd_des.{$i}", ''),
                ]);

                foreach ($allCustomerDetail as $key => $chargeDetail) {
                    // Update and delete
                    if ($input['whs_id'][$i] == $chargeDetail['whs_id'] &&
                        $input['chg_code_id'][$i] == $chargeDetail['chg_code_id']
                    ) {
                        unset($allCustomerDetail[$key]);
                        break;
                    }
                }
            }

//            // Delete Mass
            if (!empty($allCustomerDetail)) {
                foreach ($allCustomerDetail as $chargeDetail) {
                    $this->invoiceCostModel->deleteCustomerChargeDetail(
                        $customerId,
                        $chargeDetail['whs_id'],
                        $chargeDetail['chg_code_id']
                    );
                }
            }

            return $this->response->noContent();
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    } */
}
