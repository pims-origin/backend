<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\InvoiceOrdProcHistory;

use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class OrderModel extends AbstractModel
{
    /**
    * @var
    */
    
    protected $invHistory;
       
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new OrderHdr();
        $this->invHistory = new InvoiceOrdProcHistory();
    }

    /**
     * @recNum array
     * return array
    */

    public function getClientOrderNums($orderIDs)
    {
        $result = $this->model
                        ->select('cus_po')
                        ->whereIn('odr_id', $orderIDs)
                        ->lists('cus_po')
                        ->toArray();
       
        return $result;
    }
    
    /**
     * @scanOrderNumber array
     * return array
    */
    
    public function getCancelOrderIDs($orderIDs)
    {
        $result = $this->model
                        ->select('odr_id')
                        ->whereIn('odr_id', $orderIDs)
                        ->where('odr_sts', config('constants.odr_status.CANCELLED'))
                        ->lists('odr_id')
                        ->toArray();
        return $result;
    }
    
    /**
     * @scanOrderNumber array
     * return array
    */
    
    public function getOrderNumber($orderIDs)
    {
        $result = $this->model
                        ->select('odr_num')
                        ->whereIn('odr_id', $orderIDs)
                        ->lists('odr_num')
                        ->toArray();
        return $result;
    }
    
    
    /**
     * @orders array
     * return array
    */
    
    public function getOrderDetails($params)
    {
        $orders = $params;
        
        if (getDefault($params['items'])) {
            $orders = explode(',', $params['items']);
        }

        $results = $this->model 
                        ->join('customer AS c', 'c.cus_id', '=', 'odr_hdr.cus_id')
                        ->join('users AS u', 'u.user_id', '=', 'odr_hdr.csr')
                        ->select('odr_id', 'odr_hdr.cus_id', 'cus_name', 'csr', 
                                 'user_id', 'first_name', 'last_name', 'odr_num', 
                                  'cus_odr_num', 'cus_po', DB::raw('FROM_UNIXTIME(ship_by_dt, "%Y-%m-%d") AS ship_by_dt'), 
                                   DB::raw('IF(act_cmpl_dt, FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"), NULL) AS act_cmpl_dt'), 
                                   DB::raw('IF(act_cancel_dt, FROM_UNIXTIME(act_cancel_dt, "%Y-%m-%d"), NULL) AS act_cancel_dt'),
                                   'odr_sts'
                                )
                        ->whereIn('odr_num', $orders);
                        
        return $results;
    }
    
    /**
    * @params array
    * @return array
    */

    public function billableOrders($params)
    {
        $orderIDs = getDefault($params['orderNums']);
        $retBilled = getDefault($params['retBilled']);
        
        $ids = array_keys($orderIDs);
  
        $hisOrder =  $this->invHistory
                            ->select('ord_id', 'ord_num')
                            ->whereIn('ord_id', $ids)
                            ->where('inv_sts', 1)
                            ->lists('ord_num', 'ord_id')
                            ->toArray();

        if ($retBilled) {
            return $hisOrder;
        }

        $billable = array_diff_key($orderIDs, $hisOrder);


        return $billable;
    }
    
       
    /**
     * @invNum array
     * return mixed
    */

    public function getInvoicedOrders($invNum)
    {
        $results = $this->invHistory
                        ->where('inv_num', $invNum)
                        ->lists('ord_num')
                        ->toArray();

        return $results;
        
    }

    /**
     * @attributes with array
     * return mixed
    */
    
    public function viewDetail($attributes = [], $limit = PAGING_LIMIT)
    {
        $query = $this->getOrderDetails($attributes);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $searchLike = ['odr_num'];
        $searchEqual = [];
        
        foreach ($searchAttr as $key => $value) {
            if (in_array($key, $searchLike)) {
                $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                continue;
            } 
            if (in_array($key, $searchEqual)) {
                $query->where($$key, $value);
            }
        }

        $this->sortBuilder($query, $searchAttr);

        $models = $query->paginate($limit);

        return $models;
   }
   
    /**
     * @recNum array
     * return array
    */

    public function getOrderID($odrNum)
    {
        $results = $this->model
                        ->select('odr_id', 'odr_num')
                        ->whereIn('odr_num', $odrNum)
                        ->lists('odr_num', 'odr_id')
                        ->toArray();
       
        return $results;
    }
}
