<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ChargeCode;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class ChargeCodeModel extends AbstractModel
{
    /**
     * ChargeCodeModel constructor.
     *
     * @param ChargeCode|null $model
     */
    public function __construct(ChargeCode $model = null)
    {
        $this->model = ($model) ?: new ChargeCode();
    }

    /**
     * @param int $char_code_id
     *
     * @return int
     */
    public function deleteChargeCode($char_code_id)
    {
        return $this->model
            ->where('chg_code_id', $char_code_id)
            ->delete();
    }

    public function deleteMassChargeCode(array $char_code_ids)
    {
        return $this->model
            ->whereIn('chg_code_id', $char_code_ids)
            ->delete();
    }

    /**
     * Search ChargeCode
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'chg_code') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                } elseif ($key === 'chg_uom_name') {
                    $query->whereHas('systemUom', function ($q) use ($value) {
                        $q->where('sys_uom_name', 'like', "%" . SelStr::escapeLike($value) . "%");
                    });
                } elseif ($key === 'chg_type_name') {
                    $query->whereHas('chargeType', function ($q) use ($key, $value) {
                        $q->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                    });
                }
            }
        }
        $this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }
}
