<?php

namespace App\Api\V1\Validators;

class LocationImportValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'warehouse_id' => 'required|exists:warehouse,whs_id',
            'file'   => 'required',
        ];
    }
}
