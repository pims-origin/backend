<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    //$middleware[] = 'authorize';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {

    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat Alexa Voice API'];
        });

        $api->get('/check-wave-pick', ['action' => 'viewMenu', 'uses' => 'WavePickController@checkNewWavepick']);
        $api->get('/wave-pick/update-status/{wv_id:[0-9]+}', ['action' => 'viewMenu', 'uses' => 'WavePickController@updateWavePickStatus']);
        $api->get('/wave-pick-first', ['action' => 'viewMenu', 'uses' => 'WavePickController@firstWavepick']);
        $api->put('/update-wave-pick/{wv_id:[0-9]+}', ['action' => 'viewMenu', 'uses' => 'WavePickController@updateWavepick']);
        $api->get('/more-suggest-location', ['action' => 'viewMenu', 'uses' => 'WavePickController@moreSuggestLocation']);
        $api->get('/remove-file', ['action' => 'viewMenu', 'uses' => 'WavePickController@removeFile']);
    });
});
