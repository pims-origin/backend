<?php

namespace App\Api\V2\PackDetail\Models;

use \Seldat\Wms2\Models\SystemState;

class SystemStateModel extends AbstractModel
{

    /**
     * EloquentMenuGroup constructor.
     *
     * @param SystemState $model
     */
    public function __construct(SystemState $model = null)
    {
        $this->model = ($model) ?: new SystemState();
    }

    /**
     * @param string $stateCode
     * @param string $countryCode
     *
     * @return mixed
     */
    public function checkStateInCountry($stateCode, $countryCode)
    {
        return $this->model
            ->where('sys_state_code', $stateCode)
            ->where('system_country.sys_country_code', $countryCode)
            ->leftJoin('system_country', 'system_state.sys_state_country_id', '=', 'system_country.sys_country_id')
            ->count();
    }
}
