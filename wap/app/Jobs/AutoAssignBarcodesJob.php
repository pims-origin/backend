<?php

namespace App\Jobs;

use DB;
use GuzzleHttp\Client;
use App\Api\V1\Models\Log;

class AutoAssignBarcodesJob extends Job
{
    protected $whsId;
    protected $wvDtlId;
    protected $pieceQty;
    protected $request;
    /**
     * AutoPackJob constructor.
     */
    public function __construct($whsId, $wvDtlId, $pieceQty, $request)
    {
        $this->whsId    = $whsId;
        $this->wvDtlId  = $wvDtlId;
        $this->pieceQty = $pieceQty;
        $this->request  = $request;
    }

    public function handle()
    {
        $odrDtls = DB::table('wv_dtl')
            ->join('odr_dtl', function ($join) {
                $join->on('odr_dtl.wv_id',     '=', 'wv_dtl.wv_id');
                $join->on('odr_dtl.item_id',   '=', 'wv_dtl.item_id');
                $join->on('odr_dtl.pack',      '=', 'wv_dtl.pack_size');
                $join->on('odr_dtl.lot',       '=', 'wv_dtl.lot');
            })
            ->where('wv_dtl.wv_dtl_id', $this->wvDtlId)
            ->where('odr_dtl.deleted', 0)
            ->get();
        ;
        $odrId = array_get(array_first($odrDtls), 'odr_id');

        try{

            if ($odrId && count($odrDtls)) {
                $client = new Client();
                $version = "v2";
                $url = sprintf('/%s/whs/%d/order/%d/cartons-without-rfid', $version, $this->whsId, $odrId);
                $authorization = $this->request->getHeader('Authorization');

                $data = [];
                foreach ($odrDtls as $odrDtl) {
                    $data['items'][] = [
                        'odr_dtl_id' => array_get($odrDtl, 'odr_dtl_id'),
                        'piece_qty'  => (int)$this->pieceQty,
                    ];
                }
                $response = $client->request('PUT', env("API_WAP"). $url,
                    [
                        'headers'     => ['Authorization' => $authorization],
                        'form_params' => $data,
                    ]
                );

                $owner = $transaction = "";
                Log:: info($this->request, $this->whsId, [
                    'evt_code'     => 'AAJ',
                    'owner'        => $owner,
                    'transaction'  => $transaction,
                    'url_endpoint' => $url,
                    'message'      => 'Auto Assign Cartons to Order in Barcode case. '. json_encode($data)
                ]);
            }

        }catch (\Exception $exception) {
            $dataError = [
                'date'     => time(),
                'api_name' => "WAP",
                'error'    => $exception->getMessage(),
            ];

            DB::table('sys_bugs')->insert($dataError);
        }

    }
}
