<?php

namespace App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Controllers;

use App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Models\OrderHdrModel;
use App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Models\EventTrackingModel;
use App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Models\OutPalletModel;
use App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Models\PackHdrModel;
use App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Models\CartonModel;
use App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Models\LocationModel;
use App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Validators\OutPalletValidator;
use App\libraries\RFIDValidate;
use App\Api\V1\Models\Log;
use App\Jobs\BOLJob;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OutPalletController extends AbstractController
{

    protected $orderHdrModel;
    protected $eventTrackingModel;
    protected $outPalletModel;
    protected $packHdrModel;
    protected $cartonModel;
    protected $locationModel;

    public function __construct()
    {
        $this->orderHdrModel      = new OrderHdrModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->outPalletModel     = new OutPalletModel();
        $this->packHdrModel       = new PackHdrModel();
        $this->cartonModel        = new CartonModel();
        $this->locationModel      = new LocationModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     * @return array
     */
    public function assignPackRetailOrder($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $dataList = array_get($input, 'data', '');
        $locCode = array_get($input, 'loc_code', '');

        /*
         * start logs
         */
        $url = "v2/{$whsId}/outpallet/assign-pack-retail-order";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'APRO',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Assign Packed Retail Order'
        ]);
        /*
         * end logs
         */

        // Check null input data
        if (empty($dataList) || empty($locCode)) {
            $msg = sprintf("Order/Tracking num list and Location code field are required");
            return $this->_responseErrorMessage($msg);
        }

        // Check location
        $location = $this->locationModel->getFirstWhere(['loc_code' => $locCode]);
        if (empty($location)) {
            $msg = sprintf("Get Location failed.");
            return $this->_responseErrorMessage($msg);
        }

        try {
            // start transaction
            DB::beginTransaction();

            $arrayOdrId = [];
            $arrayPltNums = [];
            foreach($dataList as $data){
                // Check is Order num
                $lpnNumNext = null;
                $flag = null;
                if(substr($data, 0, 3) == 'ORD'){
                    $odrObj = $this->orderHdrModel->getFirstWhere(['odr_num' => $data]);
                    $flag = 1;
                }
                else{
                    $packHdrObject = $this->packHdrModel->getFirstWhere(['tracking_number' => $data]);
                    $odrId = object_get($packHdrObject, 'odr_hdr_id', '');
                    $odrObj = $this->orderHdrModel->getFirstWhere(['odr_id' => $odrId]);
                    $flag = 2;
                }

                // Get LPN num text
                if (!$odrObj) {
                    $msg = sprintf("Get OrderHdr failed.");
                    return $this->_responseErrorMessage($msg);
                }

                if ($odrObj && $odrObj->whs_id != $whsId) {
                    $msg = sprintf("OrderHdr does not belong to current warehouse.");
                    return $this->_responseErrorMessage($msg);
                }

                if ($odrObj->odr_type != 'RTL') {
                    $msg = sprintf("Only OrderHdr with type RTL can assign.");
                    return $this->_responseErrorMessage($msg);
                }

                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $odrId  = $odrObj->odr_id;
                $odrNum = $odrObj->odr_num;
                array_push($arrayOdrId, $odrId);

                $lpnNumNext = null;
                $outPltIds = $this->packHdrModel->getModel()
                    ->where('odr_hdr_id', $odrId)
                    ->pluck('out_plt_id')->toArray();
                if (count($outPltIds)) {
                    $outPltObjFirst = $this->outPalletModel->getModel()
                        ->whereIn('plt_id', $outPltIds)
                        ->where('deleted', 0)
                        ->orderBy('plt_num', 'DESC')
                        ->first();
                    if (count($outPltObjFirst)) {
                        $lpnNumLast = object_get($outPltObjFirst, 'plt_num');
                        $lpnNumNext = ++$lpnNumLast;
                    } else {
                        $lpnNumNext = $this->_getLpnByOdrNum($odrNum);
                    }
                } else {
                    $lpnNumNext = $this->_getLpnByOdrNum($odrNum);
                }

                if(empty($lpnNumNext)){
                    $msg = sprintf("Get LPN num text by Order failed.");
                    return $this->_responseErrorMessage($msg);
                }
                array_push($arrayPltNums, $lpnNumNext);

                if($flag == 1){
                    // 2. Create out pallet from LPN and Update location shipping lane.
                    $packHdr = $this->packHdrModel->getFirstWhere(['odr_hdr_id' => $odrObj->odr_id]);
                    $pallet = $this->outPalletModel->createNewPallet($packHdr, $lpnNumNext, 0, $location);

                    // 3. Update pack_hdr.out_plt_id = pallet.plt_id
                    $this->packHdrModel->getModel()
                        ->where('odr_hdr_id', $odrObj->odr_id)
                        ->update([
                            'out_plt_id' => $pallet->plt_id,
                            'pack_sts'   => 'AS'
                        ]);
                }

                if($flag == 2){
                    // 2. Create out pallet from LPN and Update location shipping lane.
                    $packHdr = $this->packHdrModel->getFirstWhere(['tracking_number' => $data]);
                    $pallet = $this->outPalletModel->createNewPallet($packHdr, $lpnNumNext, 0, $location);

                    // 3. Update pack_hdr.out_plt_id = pallet.plt_id
                    $this->packHdrModel->getModel()
                        ->where('tracking_number', $data)
                        ->update([
                            'out_plt_id' => $pallet->plt_id,
                            'pack_sts'   => 'AS'
                        ]);
                }
            }

            // assigned shipping lane  $location
            array_unique($arrayOdrId);
            array_unique($arrayPltNums);
            $this->outPalletModel->updateOutPalletLocation($location, $arrayPltNums);
            $checkOdrsShipped = $this->outPalletModel->getOdrsByPltNums($arrayPltNums, $whsId);
            $this->_updateOrderStatusShipping($checkOdrsShipped, $location);

            $this->eventTrackingModel->eventTracking([
                'whs_id'    => $whsId,
                'cus_id'    => object_get($odrObj, 'cus_id', null),
                'owner'     => object_get($odrObj, 'odr_num', null),
                'evt_code'  => 'WOP',
                'trans_num' => object_get($odrObj, 'odr_num', null),
                'info'      => "WAP - Assigning Retail Order to Pallet"
            ]);

            DB::commit();

            foreach ($checkOdrsShipped as $checkOdrShipped) {
                if ($odrId = object_get($checkOdrShipped, 'odr_id')) {
                    dispatch(new BOLJob($odrId, $whsId, $request));
                }
            }

            $msg = sprintf("Successfully!");
            return [
                "iat"     => time(),
                "status"  => true,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => 1,
                ],
                "data"    => []
            ];

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $e->getMessage(),
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function unassignPackRetailOrder($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $dataList = array_get($input, 'data', '');
        $locCode = array_get($input, 'loc_code', '');

        /*
         * start logs
         */
        $url = "v2/{$whsId}/outpallet/unassign-pack-retail-order";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'APRO',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Assign Packed Retail Order'
        ]);
        /*
         * end logs
         */

        // Check null input data
        if (empty($dataList) || empty($locCode)) {
            $msg = sprintf("Order/Tracking num list and Location code field are required");
            return $this->_responseErrorMessage($msg);
        }

        // Check location
        $location = $this->locationModel->getFirstWhere(['loc_code' => $locCode]);
        if (empty($location)) {
            $msg = sprintf("Get Location failed.");
            return $this->_responseErrorMessage($msg);
        }

        try {
            // start transaction
            DB::beginTransaction();

            foreach($dataList as $data){
                if(substr($data, 0, 3) == 'ORD'){
                    $odrObj = $this->orderHdrModel->getFirstWhere(['odr_num' => $data]);
                    $packHdr = $this->packHdrModel->getFirstWhere(['odr_hdr_id' => $odrObj->odr_id]);
                    $this->outPalletModel->getModel()
                        ->where('plt_id', $packHdr->out_plt_id)
                        ->update([
                            'deleted'     => 0,
                            'deleted_at'  => time()
                        ]);

                    // 3. Update pack_hdr.out_plt_id = null
                    $this->packHdrModel->getModel()
                        ->where('odr_hdr_id', $odrObj->odr_id)
                        ->update([
                            'out_plt_id' => null,
                            'pack_sts'   => 'NW'
                        ]);
                }
                else{
                    $packHdr = $this->packHdrModel->getFirstWhere(['tracking_number' => $data]);
                    $outPltId = $packHdr->out_plt_id;
                    $countSamePlt = count($this->packHdrModel->where('out_plt_id', $outPltId)->get());
                    // 3. Update pack_hdr.out_plt_id = null
                    $this->packHdrModel->getModel()
                        ->where('pack_hdr_id', $packHdr->pack_hdr_id)
                        ->update([
                            'out_plt_id' => null,
                            'pack_sts'   => 'NW'
                        ]);
                    // 2. Delete out palet
                    if($countSamePlt == 1){
                        $this->outPalletModel->getModel()
                            ->where('plt_id', $packHdr->out_plt_id)
                            ->update([
                                'deleted'     => 0,
                                'deleted_at'  => time()
                            ]);
                    }
                }
            }

            $this->eventTrackingModel->eventTracking([
                'whs_id'    => $whsId,
                'cus_id'    => object_get($odrObj, 'cus_id', null),
                'owner'     => object_get($odrObj, 'odr_num', null),
                'evt_code'  => 'WOP',
                'trans_num' => object_get($odrObj, 'odr_num', null),
                'info'      => "WAP - Unassigning Retail Order to Pallet"
            ]);

            DB::commit();

            $msg = sprintf("Successfully!");
            return [
                "iat"     => time(),
                "status"  => true,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => 1,
                ],
                "data"    => []
            ];

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $e->getMessage(),
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _getLpnByOdrNum($odrNum)
    {
        $licensePlate = $odrNum . "-" . str_pad(1, 4, "0", STR_PAD_LEFT);
        return str_replace("ORD", "LPN", $licensePlate);
    }

    private function _updateOrderStatusShipping($checkOdrsShipped, $locObj)
    {
        foreach ($checkOdrsShipped as $checkOdrShipped) {
            if ($odrId = object_get($checkOdrShipped, 'odr_id')) {
                $isUpdated = "(SELECT COUNT(1) FROM pack_hdr
                        LEFT JOIN out_pallet ON out_pallet.plt_id = pack_hdr.out_plt_id
                        WHERE pack_hdr.odr_hdr_id = odr_hdr.odr_id AND pack_hdr.deleted = 0
                            AND out_pallet.loc_id IS NULL) = 0";
                DB::table('odr_hdr')
                    ->where('odr_id', $odrId)
                    ->where('deleted', 0)
                    ->whereRaw($isUpdated)
                    ->update(["odr_sts" => 'RS']);
            }
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}