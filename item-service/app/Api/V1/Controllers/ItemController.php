<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\ItemService;
use Illuminate\Http\Request as Request;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Utils\Profiler;

class ItemController extends AbstractController
{
    /**
     * @var ItemService
     */
    protected $service;

    /**
     * ItemController constructor.
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        Profiler::log('start controller __construct');
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new ItemService($request);
    }

    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(IRequest $request)
    {
        Profiler::log('start controller');
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getList($queryStr);
        $response = $this->convertResponse($result);
        //Profiler::end();
        return $response;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->service->getDetail($id);
        $response = $this->convertResponse($result);
        return $response;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getItemById($id)
    {
        $result = $this->service->getItemById($id);
        $response = $this->convertResponse($result);
        return $response;
    }

    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(IRequest $request)
    {
        $input = $request->getParsedBody();
        $result = $this->service->create($input);
        $response = $this->convertResponse($result);
        return $response;
    }
    
    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function multiStore(IRequest $request)
    {
        $inputs = $request->getParsedBody();
        $response = [];
        foreach ($inputs as $input){
            $result = $this->service->create($input);
            $content = $result->getBody()->getContents();
            $objContent = \GuzzleHttp\json_decode($content);
            if(isset($objContent->data)){
                $response[] = $objContent->data;
            }
        }
        return $response;
    }
    
    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function check(IRequest $request)
    {
        $input = $request->getParsedBody();
        $result = $this->service->check($input);
        $response = $this->convertResponse($result);
        return $response;
    }

    /**
     * @param IRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(IRequest $request, $id)
    {
        $input = $request->getParsedBody();
        $result = $this->service->edit($input, $id);
        $response = $this->convertResponse($result);
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $inputs = $request->all();
        $result = $this->service->import($inputs);
        return $response = $this->convertResponse($result);
    }
    /**
     * [exportToExcel description]
     * @param  IRequest $request [description]
     * @return [type]            [description]
     */
    public function exportToExcel(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->export($queryStr);
        $response = $this->convertResponse($result);
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getItemUom(Request $request)
    {
        $inputs = $request->all();
        $result = $this->service->getItemUom($inputs);
        return $response = $this->convertResponse($result);
    }

    public  function getItemChargeBy(Request $request){
        $inputs = $request->all();
        $result = $this->service->getChargeBy($inputs);
        return $response = $this->convertResponse($result);
    }

   

}
