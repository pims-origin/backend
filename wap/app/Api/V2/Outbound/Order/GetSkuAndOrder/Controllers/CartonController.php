<?php

namespace App\Api\V2\Outbound\Order\GetSkuAndOrder\Controllers;

use App\Api\V2\Outbound\Order\GetSkuAndOrder\Models\CartonModel;
use App\Api\V2\Outbound\Order\GetSkuAndOrder\Models\OrderHdrModel;
use App\Api\V2\Outbound\Order\GetSkuAndOrder\Models\OrderCartonModel;
use App\Api\V2\Outbound\Order\GetSkuAndOrder\Models\OutPalletModel;
use App\Api\V2\Outbound\Order\GetSkuAndOrder\Models\PackHdrModel;
use App\libraries\RFIDValidate;

use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class CartonController extends AbstractController
{

    protected $cartonModel;
    protected $orderHdrModel;
    protected $orderCartonModel;
    protected $outPalletModel;
    protected $packHdrModel;

    public function __construct()
    {
        $this->cartonModel      = new CartonModel();
        $this->orderHdrModel    = new OrderHdrModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->outPalletModel   = new OutPalletModel();
        $this->packHdrModel     = new PackHdrModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function getSku($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $packs = array_get($input, 'cartons', null);
        /*
         * start logs
         */

        $url = "/v2/{$whsId}/order/sku-and-order-info";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GLO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get SKU and Order information'
        ]);
        /*
         * end logs
         */

        if (!is_array($packs)) {
            $packs = [$packs];
        }
        if (empty($packs)) {
            $msg = sprintf("Packs field is required.");
            return $this->_responseErrorMessage($msg);
        }

        // Check duplicate Pack hdl num
        $uniquePacks = array_unique($packs);
        if (count($uniquePacks) != count($packs)) {
            $duplicatePack = array_unique(array_diff_assoc($packs, $uniquePacks));
            $msg = 'Duplicate Pack: ' . implode(', ', $duplicatePack);
            return $this->_responseErrorMessage($msg);
        }

        $ctnRfidInValid = [];
        $errorMsg = '';
        $isAssignRfid = 0; //0: is assigned with barcode, 1: is assigned with rfid
        foreach ($packs as $key => $pack) {
            //check invalid carton rfid when auto packs
            $checkRfid = substr($pack, 0, 3);
            if (!in_array($checkRfid, ["CTN", "CYC"])) {
                $isAssignRfid = 1;
                $ctnRFIDValid = new RFIDValidate($pack, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $pack;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }

                $ctnObj = $this->cartonModel->getFirstWhere(['rfid' => $pack]);
                if (!$ctnObj) {
                    $msg = sprintf("The Carton Rfid %s doesn't exist.", $pack);
                    return $this->_responseErrorMessage($msg);
                }

                if ($ctnObj && $ctnObj->ctn_sts != 'PD') {
                    $msg = sprintf("Unable to assign Carton Rfid %s to pallet, current status is %s.", $pack, Status::getByValue($ctnObj->ctn_sts, "CTN_STATUS"));
                    return $this->_responseErrorMessage($msg);
                }

                $ctnId = object_get($ctnObj, 'ctn_id', null);
                $packHdr = $this->packHdrModel->getFirstWhere([
                    'cnt_id' => $ctnId,
                    'pack_hdr_num' => object_get($ctnObj, 'ctn_num'),
                ]);

                $packs[$key] = object_get($packHdr, 'pack_hdr_num', null);
            }
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
        }

        try {

            DB::setFetchMode(\PDO::FETCH_ASSOC);

            // update location in out_pallet
            $odrId  = null;
            $odrNum = null;
            $cusPo  = null;
            $ctnInfos = [];

            foreach ($packs as $packCode) {
                $packHdr = $this->packHdrModel->getFirstWhere(['pack_hdr_num' => $packCode]);

                if (!$packHdr) {
                    $msg = sprintf("Packed Carton %s is not found!", $packCode);
                    return $this->_responseErrorMessage($msg);
                }

                if ($packHdr && $packHdr->whs_id != $whsId) {
                    $msg = sprintf("Packed Carton %s does not belong to current warehouse!", $packCode);
                    return $this->_responseErrorMessage($msg);
                }

                if ($outPltIdErr = $packHdr->out_plt_id) {
                    $outPltObjErr = $this->outPalletModel->getFirstWhere(['plt_id' => $outPltIdErr]);
                    $msg = sprintf("The carton %s has been assigned to pallet %s already!", $packCode, $outPltObjErr->plt_num);
                    return $this->_responseErrorMessage($msg);
                }

                if (!$odrId) {
                    $odrId = object_get($packHdr, 'odr_hdr_id');
                    $odrObj = $this->orderHdrModel->getFirstWhere(['odr_id' => $odrId]);
                    $odrNum = object_get($odrObj, 'odr_num');
                    $cusPo = object_get($odrObj, 'cus_po');

                    if (!$odrObj) {
                        $msg = sprintf("Order Id %s does not exist!", $odrId);
                        return $this->_responseErrorMessage($msg);
                    }

                    if ($odrObj && $odrObj->whs_id != $whsId) {
                        $msg = sprintf("Order %s does not belong to current warehouse!", $odrNum);
                        return $this->_responseErrorMessage($msg);
                    }

                    if ($odrObj &&
                        $odrObj->odr_sts != Status::getByValue('Packing', 'ORDER-STATUS')
                        && $odrObj->odr_sts != Status::getByValue('Packed', 'ORDER-STATUS')
                        && $odrObj->odr_sts != Status::getByValue('Palletizing', 'ORDER-STATUS')
                    ) {
                        $msg = sprintf("Order %s is %s!", $odrNum, Status::getByKey('ORDER-STATUS', $odrObj->odr_sts));
                        return $this->_responseErrorMessage($msg);
                    }
                }

                if ($odrId != $packHdr->odr_hdr_id) {
                    $msg = sprintf("Please scan cartons the same as order", $odrNum);
                    return $this->_responseErrorMessage($msg);
                }

                $ctnId = object_get($packHdr, 'cnt_id');
                if ($ctnId) {
                    $cartonObj = $this->cartonModel->getFirstWhere(['ctn_id' => $ctnId]);

                    if (!$cartonObj) {
                        $msg = sprintf('Carton Id %s is not scanned through conveyor inbound!', $ctnId);
                        return $this->_responseErrorMessage($msg);
                    }

                    if ($cartonObj && $cartonObj->whs_id != $whsId) {
                        $msg = sprintf('Carton RFID %s does not belong to current Warehouse!', $cartonObj->rfid);
                        return $this->_responseErrorMessage($msg);
                    }

                    if ($cartonObj && $cartonObj->is_damaged == 1) {
                        $msg = sprintf('Carton RFID %s is damaged!', $cartonObj->rfid);
                        return $this->_responseErrorMessage($msg);
                    }

                    $ctnInfos[] = [
                        'rfid'      => object_get($cartonObj,'rfid'),
                        'item_id'   => object_get($cartonObj,'item_id'),
                        'sku'       => object_get($cartonObj,'sku'),
                        'size'      => object_get($cartonObj,'size'),
                        'color'     => object_get($cartonObj,'color'),
                        'lot'       => object_get($cartonObj,'lot'),
                        'pack'      => object_get($cartonObj,'ctn_pack_size'),
                        'piece_qty' => object_get($cartonObj,'piece_remain'),
                        'ctn_sts'   => object_get($cartonObj,'ctn_sts'),
                    ];
                } else {
                    $ctnInfos[] = [
                        'rfid'      => object_get($packHdr,'rfid'),
                        'item_id'   => object_get($packHdr,'item_id'),
                        'sku'       => object_get($packHdr,'sku'),
                        'size'      => object_get($packHdr,'size'),
                        'color'     => object_get($packHdr,'color'),
                        'lot'       => object_get($packHdr,'lot'),
                        'pack'      => object_get($packHdr,'ctn_pack_size'),
                        'piece_qty' => object_get($packHdr,'piece_ttl'),
                        'ctn_sts'   => object_get($packHdr,'ctn_sts'),
                    ];
                }
            }

            $lpnNumNext = null;
            $outPltIds = $this->packHdrModel->getModel()
                ->where('odr_hdr_id', $odrId)
                ->pluck('out_plt_id')->toArray();
            if (count($outPltIds)) {
                $outPltObjFirst = $this->outPalletModel->getModel()
                    ->whereIn('plt_id', $outPltIds)
                    ->where('deleted', 0)
                    ->orderBy('plt_num', 'DESC')
                    ->first();
                if (count($outPltObjFirst)) {
                     $lpnNumLast = object_get($outPltObjFirst, 'plt_num');
                     $lpnNumNext = ++$lpnNumLast;
                } else {
                    $lpnNumNext = $this->_getLpnByOdrNum($odrNum);
                }
            } else {
                $lpnNumNext = $this->_getLpnByOdrNum($odrNum);
            }

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data'] = [[
                'order' => [
                    'odr_id'  => $odrId,
                    'odr_num' => $odrNum,
                    'cus_po'  => $cusPo,
                ],
                'skus'    => $ctnInfos,
                'lpn_num' => $lpnNumNext,
            ]];

            return $msg;

        } catch (\Exception $e) {

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _getLpnByOdrNum($odrNum)
    {
        $licensePlate = $odrNum . "-" . str_pad(1, 4, "0", STR_PAD_LEFT);
        return str_replace("ORD", "LPN", $licensePlate);
    }

}