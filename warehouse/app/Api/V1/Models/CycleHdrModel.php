<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class CycleHdrModel extends AbstractModel
{
    const STATUS_CYCLE_OPEN = 'OP';
    const STATUS_CYCLE_ASSIGNED = 'AS';
    const STATUS_CYCLE_CYCLED = 'CC';
    const STATUS_CYCLE_RECOUNT = 'RC';
    const STATUS_CYCLE_COMPLETED = 'CP';
    const STATUS_CYCLE_DELETED = 'DL';

    public static $arrCycleStatus = [
        self::STATUS_CYCLE_OPEN      => 'Open',
        self::STATUS_CYCLE_ASSIGNED  => 'Assigned',
        self::STATUS_CYCLE_CYCLED    => 'Cycled',
        self::STATUS_CYCLE_RECOUNT   => 'Recount',
        self::STATUS_CYCLE_COMPLETED => 'Completed',
        self::STATUS_CYCLE_DELETED   => 'Deleted',
    ];

    /**
     * @param CycleHdr $model
     */
    public function __construct(CycleHdr $model = null)
    {
        $this->model = ($model) ?: new CycleHdr();
    }

    public function getForDashboard($cus_ids, $whsId)
    {
        $result = $this->model
            ->where('deleted', 0)
            ->where('whs_id', $whsId)
            ->whereNotIn("cycle_sts",["CP","CE","DL"])
            ->count();
        return $result;
    }

    /**
     * @param $input
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function cycleCountStatisticDashboard($attributes, $with = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis::getCurrentCusIds();

        $query = $this->make($with);
        $query->select([
            'cycle_hdr.cycle_hdr_id',
            'cycle_hdr.cycle_num',
            'cycle_hdr.cycle_sts',
            'cycle_hdr.cycle_type',

            DB::raw("(count(cycle_dtl.cycle_dtl_id)) AS loc_total"),

            'cycle_hdr.created_at',
            'cycle_hdr.due_dt',
            'cycle_hdr.completed_at',
        ]);
        $query->join('cycle_dtl', 'cycle_dtl.cycle_hdr_id', '=', 'cycle_hdr.cycle_hdr_id');

        $query->where('cycle_hdr.whs_id', $currentWH);
        $query->whereNotIn('cycle_hdr.cycle_sts', ['DL', 'CP', 'CE']);
        $query->orderBy('cycle_hdr.created_at', 'desc');
        $query->groupBy('cycle_hdr.cycle_hdr_id');

        if (isset($attributes['type'])) {
            $query->where('cycle_hdr.cycle_type', SelStr::escapeLike($attributes['type']));
        }

        $models = $query->paginate($limit);

        return $models;

    }

    /**
     * @param $cycleHdrId
     *
     * @return mixed
     */
    public function countCycleCountUsedLoc($cycleHdrId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis::getCurrentCusIds();

        $query = DB::table('cycle_dtl');
        $query->select([
            DB::raw("(count(cycle_dtl.cycle_dtl_id)) AS used_loc_total"),
        ]);

        $query->where('cycle_dtl.whs_id', $currentWH);
        $query->where('cycle_dtl.cycle_hdr_id', $cycleHdrId);
        $query->whereNotIn('cycle_dtl.cycle_dtl_sts', ['NW']);
        $query->groupBy('cycle_dtl.cycle_hdr_id');

        $models = $query->first();

        return $models;

    }

    /**
     * @param $statusKey
     *
     * @return mixed|string
     */
    public function getCycleStatus($statusKey)
    {
        $statuses = self::$arrCycleStatus;

        $statusName = 'NA';
        foreach ($statuses as $key => $val) {
            if ($key == $statusKey){
                $statusName =  $val;
            }
        }

        return $statusName;
    }


}