<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Location;
use Wms2\UserInfo\Data;

class LocationModel extends AbstractModel
{

    protected $model;

    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    /**
     * Get cartons
     *
     * @param $itemId
     *
     * @return mixed,p
     */
    public static function getLocationList($itemId, $algorithm, $ctn_qty = 6, $lot)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $locTypes = ["RAC", "PRO"];

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table("cartons")
            ->Join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->Join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->where([
                    'cartons.item_id'       => $itemId,
//                    'cartons.lot'           => $lot,
                    'cartons.whs_id'        => $currentWH,
                    // 'cartons.loc_type_code' => $locType
                    'cartons.ctn_sts'       => 'AC'
            ])
            ->whereIn('location.loc_sts_code', ['AC', 'RG', 'LK'])
            ->whereIn('cartons.loc_type_code', $locTypes)
            ->whereNotNull('cartons.loc_id')
            ->select(
                'cartons.loc_id',
                'cartons.loc_code',
                'pallet.plt_num',
                DB::raw('sum(piece_remain) as avail_qty')
            )
            ->groupBy('cartons.plt_id');
        
        if(empty($lot)) {
            $lot = 'ANY';
        }
//        if (strtoupper($lot) != 'ANY') {
//            $query = $query->where('cartons.lot', $lot);
//        }

        // ->orderBy('cartons.created_at', $sort)

        //#6091 task
        $query = $query->orderBy('location.aisle', 'ASC')
            ->orderBy('location.level', 'ASC')
            ->orderBy('location.row', 'ASC')
            ->orderBy('location.bin', 'ASC');
        /* switch (strtoupper($algorithm)) {
             case "LIFO":
                 $query->orderBy('cartons.gr_dt', 'DESC');
                 $query->orderBy('cartons.created_at', 'DESC');
                 break;
             case "FEFO":
                 $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                 $query->orderBy('cartons.created_at', 'ASC');
                 break;
             case "FIFO":
                 $query->orderBy('cartons.gr_dt', 'ASC');
                 $query->orderBy('cartons.created_at', 'ASC');
                 break;
         }*/
        // $query->orderBy('cartons.loc_id', 'ASC');


        $rs = $query->take($ctn_qty)
            ->get();

        return $rs;
    }



    /**
     * @return mixed
     * @throws \Exception
     */
    public static function getAllItemInLocation($locIds, $itemIds)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table("cartons")
            ->where('cartons.whs_id', '=', $currentWH)
            ->whereIn('loc_id', $locIds)
            ->whereIn('item_id', $itemIds)
            ->select([
                'cartons.loc_id',
                'cartons.loc_code',
                'cartons.item_id'
            ])
            ->distinct()
            ->get();
    }

    /**
     * @param $locIds
     * @param $itemIds
     *
     * @return mixed
     */
    public static function getTtlItemInLocation($locIds, $itemIds)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table("cartons")
            ->where('cartons.whs_id', '=', $currentWH)
            ->where('loc_id', $locIds)
            ->where('item_id', $itemIds)
            ->where('piece_remain', '>', 0)
            ->where('ctn_sts', 'AC')
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->select([
                'item_id as ITEM_ID',
                'loc_id as primary_loc_id',
                DB::raw("(select count(piece_remain) from cartons where item_id = ITEM_ID and loc_id = primary_loc_id)
                as numItems")
            ])
            ->distinct()
            ->get();
    }


    public static function countItemInLocation($locIds, $itemIds)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table("cartons")
            ->where('cartons.whs_id', '=', $currentWH)
            ->where('loc_id', $locIds)
            ->where('item_id', $itemIds)
            ->where('piece_remain', '>', 0)
            ->where('ctn_sts', 'AC')
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->sum('piece_remain');
    }


    /**
     * @param $locIds
     *
     * @return mixed
     */
    public static function getLocationActive($locIds)
    {
        return DB::table("location")
            ->whereIn('loc_id', $locIds)
            ->where('loc_sts_code', 'AC')
            ->get();
    }

    /**
     * @param $locIds
     *
     * @return mixed
     */
    public static function updateLockedLocation($locIds)
    {
        return DB::table("location")
            ->whereIn('loc_id', $locIds)
            ->update([
                'loc_sts_code' => 'AC'
            ]);
    }

    public function checkActualLocation($itemId, $lot, $isEcom, $lists)
    {
        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();
        // Loc ids
        $locIds = array_keys($lists);
        $is_ecom = (int)$isEcom;

        $cus_id = (new Item())->select('cus_id')->where('item_id', $itemId)->value('cus_id');
        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cus_id);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->leftJoin('cartons', 'location.loc_id', '=', 'cartons.loc_id')
            ->select([
                'location.loc_id',
                'location.loc_code',
                'location.loc_sts_code',
                DB::raw('sum(piece_remain) as avail_qty')
            ])
            ->where(function ($query) use ($locIds, $itemId, $is_ecom, $lot) {
                $query
                    ->where(
                        [
                            'cartons.is_damaged' => 0,
                            'cartons.ctn_sts'    => 'AC',
                            'cartons.item_id'    => $itemId,
                            'cartons.is_ecom'    => $is_ecom,
                            'cartons.lot'        => $lot
                        ]
                    )
                    ->orWhere('cartons.ctn_id', null);
            })
            ->whereIn('location.loc_id', $locIds)
            ->where('loc_type_code', 'RAC')
       //     ->whereNotNull('cartons.gr_hdr_id')
            ->groupBy('location.loc_id');
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        return $query->get()->toArray();
    }
}
