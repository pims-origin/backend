<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\Reason;

class LocationTransformer extends TransformerAbstract
{
    public static $locReasons = [];

    public function transform(Location $location)
    {
        $availableQuantity = object_get($location, 'availableQuantity', 0);

        if (!self::$locReasons){
            self::$locReasons = Reason::all()->keyBy('r_id')->toArray();
        }
        $reasonId = object_get($location, 'reason_id', null);
        $reasonDtl = $reasonId ? self::$locReasons[$reasonId]['r_name'] : '';

        return [
            'loc_id'                 => $location->loc_id,
            'loc_code'               => $location->loc_code,
            'loc_alternative_name'   => $location->loc_alternative_name,
            'loc_whs_id'             => $location->loc_whs_id,
            'loc_zone_id'            => $location->loc_zone_id,
            'loc_zone_name'          => !empty($location->zone->zone_name) ? $location->zone->zone_name : null,
            'loc_type_id'            => $location->loc_type_id,
            'loc_type_name'          => !empty($location->locationType->loc_type_name)
                ? $location->locationType->loc_type_name
                : null,
            'loc_sts_code'           => $location->loc_sts_code,
            'loc_sts_code_name'      => !empty($location->locationStatus->loc_sts_name)
                ? $location->locationStatus->loc_sts_name
                : '',
            'loc_sts_code_desc'      => !empty($location->locationStatus->loc_sts_desc)
                ? $location->locationStatus->loc_sts_desc
                : '',
            'loc_available_capacity' => $location->loc_available_capacity,
            'loc_width'              => $location->loc_width,
            'loc_length'             => $location->loc_length,
            'loc_height'             => $location->loc_height,
            'loc_max_weight'         => $location->loc_max_weight,
            'loc_weight_capacity'    => $location->loc_weight_capacity,
            'loc_min_count'          => $location->loc_min_count,
            'loc_max_count'          => $location->loc_max_count,
            'loc_desc'               => $location->loc_desc,
            'spc_hdl_code'           => $location->spc_hdl_code,
            'spc_hdl_name'           => $location->spc_hdl_name,
            'max_lpn'                => $location->max_lpn,
            'created_at'             => $location->created_at,
            'updated_at'             => $location->updated_at,
            'deleted_at'             => $location->deleted_at,
            'loc_sts_dtl_reason'     => $reasonDtl,
            'has_item'               => $location->has_item,
            'ctn_ttl'                => (int)object_get($location, 'ctn_ttl', 0),
            'sku_ttl'                => (int)object_get($location, 'sku_ttl', 0),
            'piece_ttl'              => (int)object_get($location, 'piece_ttl', 0),
            'has_pallet'             => (int)object_get($location, 'has_pallet', 0),
            'available_quantity'     => $availableQuantity,
            'rfid'                   => $location->rfid,
            'whs_code'               => object_get($location, 'warehouse.whs_code', null),
            'whs_name'               => object_get($location, 'warehouse.whs_name', null),
            'cus_name'               => object_get($location, 'cus_name', null),
            'aisle'                  => object_get($location, 'aisle', null),
            'level'                  => object_get($location, 'level', null),
            'row'                    => object_get($location, 'row', null),
            'bin'                    => object_get($location, 'bin', null),
            'area'                   => object_get($location, 'area', null)
        ];
    }
}
