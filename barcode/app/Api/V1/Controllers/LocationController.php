<?php
/**
 *
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\PalletModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;


class LocationController extends AbstractController
{
    protected $userId;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    protected $locationModel;

    protected $cartonModel;

    protected $palletModel;

    public function __construct(
        EventTrackingModel $eventTrackingModel,
        LocationModel $locationModel,
        CartonModel $cartonModel,
        PalletModel $palletModel
    ) {
        $this->userId = JWTUtil::getPayloadValue('jti') ? JWTUtil::getPayloadValue('jti') : 0;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->locationModel = $locationModel;
        $this->cartonModel = $cartonModel;
        $this->palletModel = $palletModel;
    }

    public function isPalletId($lpn)
    {
        // $whsId = Data::getCurrentWhsId();
        // $whsInfo = null;
        // foreach (Data::getInstance()->getUserInfo()['user_warehouses'] as $whs) {
        //     if ($whs['whs_id'] == $whsId) {
        //         $whsInfo = $whs;
        //         break;
        //     }
        // }


        // $whsCode = $whsInfo['whs_code'];
        // $pattern = "/^$whsCode-PL([F,B,G]{0,1})-([0-9]{6})$/";
        $pattern = "/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/";

        return preg_match($pattern, $lpn);
    }

    public function relocation($whsId, Request $request)
    {
        try {
            $input = $request->getParsedBody();
            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $fromPallet = null;
            if ($this->isPalletId($input['from'])) {
                $fromPallet = DB::table('pallet')->where([
                    'deleted' => 0,
                    'rfid'    => $input['from'],
                ])
                    ->where('ctn_ttl', '>', 0)
                    ->first();

                if (empty($fromPallet)) {
                    return $this->response->errorBadRequest('From pallet is not existed');
                }
            }

            $fromLoc = trim($input['from']);
            $toLoc = trim($input['to']);
            $from = null;
            $fromPltLocId = null;

            if ($fromPallet && !empty($fromPallet['loc_id'])) {
                $fromPltLocId = $fromPallet['loc_id'];
                $fromLoc = $fromPallet['loc_code'];
            }

            //relocation for pallet
            if (empty($fromPallet) || $fromPltLocId) {
                if ($toLoc == $fromLoc) {
                    return $this->response->errorBadRequest("Location from and location to is the same");
                }
                $from = \DB::table('location')
                    ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
                    ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                    ->where('location.loc_code', $fromLoc)
                    ->where('location.loc_whs_id', $whsId)
                    ->where('location.deleted', '=', 0)
                    ->where('loc_type.loc_type_code', 'RAC')
                    ->first([
                        'location.loc_id',
                        'location.loc_sts_code',
                        'location.loc_code',
                        'pallet.plt_id',
                        'pallet.cus_id',
                        'pallet.plt_num',
                        'location.spc_hdl_code',
                        'location.loc_sts_code'
                    ]);

                if (!$from) {
                    //Not existed
                    return $this->response->errorBadRequest("Location from is not existed");
                }
                if ($from['loc_sts_code'] == 'LK') {
                    return $this->response->errorBadRequest("Location from is locked");
                }

                if ($from['loc_sts_code'] == 'IA') {
                    return $this->response->errorBadRequest("Location from is inactive");
                }

                if ($from['loc_sts_code'] != 'AC') {
                    return $this->response->errorBadRequest("Location from is not active");
                }

                if (empty($from['plt_id'])) {
                    return $this->response->errorBadRequest("No pallet on location from");
                }

                $fromPallet = DB::table('pallet')->where([
                    'deleted'  => 0,
                    'loc_code' => $fromLoc,
                ])
                    ->where('ctn_ttl', '>', 0)
                    ->first();
            }

            $to = \DB::table('location')
                ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
                ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                ->where('location.loc_code', $toLoc)
                ->where('location.loc_whs_id', $whsId)
                ->where('location.deleted', '=', 0)
                ->where('loc_type.loc_type_code', 'RAC')
                ->first([
                    'location.loc_id',
                    'location.loc_sts_code',
                    'location.loc_code',
                    'location.plt_type',
                    'location.loc_zone_id',
                    'pallet.plt_id',
                    'pallet.cus_id',
                    'pallet.plt_num',
                    'location.spc_hdl_code',
                    'location.loc_sts_code'
                ]);

            if (!$to) {
                return $this->response->errorBadRequest("Location to is not existed");
            }

            if ($to['loc_sts_code'] == 'LK') {
                return $this->response->errorBadRequest("Location to is locked");
            }

            if ($to['loc_sts_code'] == 'IA') {
                return $this->response->errorBadRequest("Location to is inactive");
            }

            if ($to['loc_sts_code'] != 'AC') {
                return $this->response->errorBadRequest("Location to is not active");
            }

            //if ($from && $to['spc_hdl_code'] != $from['spc_hdl_code']) {
            //    return $this->response->errorBadRequest("Two Locations are not in the same temperature!");
            //}

            if (empty($fromPallet['loc_id'])) {
                $cartonObj = Carton::where('plt_id', $fromPallet['plt_id'])->first();
                if (!count($cartonObj)) {
                    return $this->response->errorBadRequest("From pallet hasn't the cartons");
                }

                if ($cartonObj->spc_hdl_code != $to['spc_hdl_code']) {
                    $msg = "From pallet and To Location are not in the same temperature!";

                    return $this->response->errorBadRequest($msg);
                }
            }

            if ($to['plt_id']) {
                return $this->response->errorBadRequest("Existed pallet on location to");
            }

            //check pallet type VIET temporary disable
            //if (strpos($fromPallet['rfid'], "-" . $to['plt_type'] . "-") === false) {
            //    return $this->response->errorBadRequest("Pallet type don't map with location ({$to['plt_type']})");
            //}

            if (!in_array($fromPallet['plt_sts'], ['AC'])) {
                return $this->response->errorBadRequest("From Pallet status have to be: Active");
            }

            DB::beginTransaction();

            if (!empty($fromPallet['loc_id'])) {
                //check and remove dynamic zone
                DB::table('location')->join('zone', 'location.loc_zone_id', '=', 'zone.zone_id')
                    ->where([
                        'zone.dynamic'    => 1,
                        'location.loc_id' => $fromPallet['loc_id']
                    ])
                    ->update([
                        'location.loc_zone_id' => null,
                        'zone.zone_num_of_loc' => DB::raw('zone.zone_num_of_loc - 1')
                    ]);
            }

            if (empty($to['loc_zone_id'])) {
                $locModel = new LocationModel();
                $dynZoneId = $locModel->getDynZone($whsId, $fromPallet['cus_id']);

                if (!$dynZoneId) {
                    return $this->response->errorBadRequest("To Location has't the zone");
                }

                //update zone num of loc
                DB::table('zone')->where('zone_id', $dynZoneId)
                    ->update([
                        'zone_num_of_loc' => DB::raw('zone_num_of_loc + 1')
                    ]);

                //update location dynamic zone
                DB::table('location')->where('loc_id', $to['loc_id'])
                    ->update(['loc_zone_id' => $dynZoneId]);
            } else {
                $cusZone = DB::table('customer_zone')->where([
                    "cus_id"  => $fromPallet['cus_id'],
                    "zone_id" => $to['loc_zone_id']
                ])
                    ->first();

                if (empty($cusZone)) {
                    return $this->response->errorBadRequest("To Location don't belong to customer");
                }
            }

            \DB::table('pallet')->where('plt_id', $fromPallet['plt_id'])->update([
                'loc_id'   => $to['loc_id'],
                'loc_code' => $toLoc,
                'loc_name' => $toLoc,
                //'plt_sts'  => DB::raw("IF(plt_sts='NW', 'AC', plt_sts)")
            ]);

            \DB::table('cartons')->where('plt_id', $fromPallet['plt_id'])->update([
                'loc_id'   => $to['loc_id'],
                'loc_code' => $toLoc,
                'loc_name' => $toLoc
            ]);

            $event = [
                'whs_id'    => $whsId,
                'cus_id'    => $from['cus_id'],
                'owner'     => $fromLoc,
                'evt_code'  => 'RFR',
                'trans_num' => $toLoc,
                'info'      => sprintf("RF Gun - Update Relocate from %s to %s", $fromLoc, $toLoc),
            ];

            //  Evt tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($event);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return [
            'message'  => "Relocate successfully",
            'pallet'   => $fromPallet['plt_num'],
            'loc_from' => $fromLoc,
            'loc_to'   => $toLoc,
        ];

    }

    public function checkLocation(\Illuminate\Http\Request $request, $idWH)
    {
        $locationCode = trim($request->get('location'));
        $chk = explode('-', $locationCode);

        if (count($chk) == 3 && !preg_match('/^([A,C]{1})([0-9]{2})/', $chk[0])) {
            return response()->json([
                'message' => "Location is OK"
            ]);
        }

        $location = Location::where('location.loc_code', $locationCode)
            ->where('location.loc_whs_id', $idWH)
            ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->where('loc_type.loc_type_code', 'RAC')
            ->select([
                'location.loc_code',
                'location.loc_sts_code'
            ])
            ->first();
        if (is_null($location)) {
            return $this->response()->errorNotFound(Message::get('BM017', $locationCode));
        }

        if (in_array($location->loc_sts_code, ['IA', 'LK'])) {
            return $this->response()->errorBadRequest("Location {$locationCode} is inactive or locked");
        }

        $format = $request->get("format", null);

        if (is_null($format)) {
            $available = Pallet::where('loc_code', $locationCode)
                ->where('whs_id', $idWH)
                ->count();
            if ($available > 0) {
                return $this->response()->errorBadRequest("Location {$locationCode} is not available");
            }
        }

        return response()->json([
            'message' => "Location is OK"
        ]);
    }

    public function getLocationByCode(Request $request, $whsId) {
        try {
            $input = $request->getQueryParams();
            if(empty($input['loc_code'])) {
                return $this->response()->errorBadRequest("loc_code is required");
            }
            $result = $this->locationModel->getLocByLocCode($input, $whsId);

            if(empty($result)) {
                return $this->response()->errorBadRequest("Location {$input['loc_code']} does not exist");
            }

            if(!empty($result->cus_id) && $result->cus_id != $input['cus_id']) {
                return $this->response()->errorBadRequest("Location {$input['loc_code']} is not belong to this customer");
            }

            return [
                "data" => [
                    "location" => $result,
                    "message" => "Get successful!!"
                ]
            ];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function suggestLocation(Request $request, $whsId) {
        try {
            $input = $request->getQueryParams();
            $input['whs_id'] = $whsId;

            if(empty($input['cus_id'])) {
                return $this->response()->errorBadRequest("Field cus_id is required");
            }

//            if(empty($input['item_id'])) {
//                return $this->response()->errorBadRequest("Field item_id is required");
//            }
            $result = [];

//            $pallet_info = DB::table('pallet')
//                ->where('cus_id', $input['cus_id'])
//                ->whereNotNull('loc_code')
//                ->get()->toArray();
//
//            if ($pallet_info) {
//                foreach ($pallet_info as $pallet) {
//                    $result[] = $pallet['loc_code'];
//                }
//            }

            $locations = $this->locationModel->getSuggestLocation($input, $whsId);

            if (!empty($locations)) {
                if (!empty($locations['parent_id'])) {
                    $location_parent = $this->locationModel->getFirstWhere(['loc_id' => $locations['parent_id'], 'loc_whs_id' => $whsId]);
                    $result[] = $location_parent->loc_code;
                }
                $result[] = $locations['loc_code'];
            }

            if (empty($locations)) {
                $locations = $this->locationModel->getLocationDynamicZone($input, $whsId);
                //foreach ($locations as $key => $location) {
                    $result[] = $locations['loc_code'];
                //}
            }

            if (empty($locations)) {
                $locations = $this->locationModel->getLocationDynamicZoneNotInCustomer($input, $whsId);
                //foreach ($locations as $key => $location) {
                    $result[] = $locations['loc_code'];
                //}
            }

            return [
                'data' => [
                    'locations' => $result,
                    'message' => "Get Successful"
                ]
            ];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function suggestLocationPutAway(Request $request, $whsId) {
        try {
            $input = $request->getQueryParams();
            $input['whs_id'] = $whsId;
            if (empty($input['plt_num'])) {
                return $this->response()->errorBadRequest('LPN can not be empty');
            }
            if (!$this->palletModel->validateLPNFormat($input['plt_num'])) {
                return $this->response()->errorBadRequest(sprintf('LPN %s is not valid format', $input['plt_num']));
            }

            $result = [];

            $carton = $this->cartonModel->getCartonByLPNCarton($input['plt_num'], $whsId);
            if (empty($carton)) {
                return $this->response()->errorBadRequest(sprintf('LPN %s does not contain any receiving item', $input['plt_num']));
            }

            $input['item_id'] = $carton->item_id;
            $input['cus_id'] = $carton->cus_id;
            $location = $this->locationModel->getLocationPutAwayHistory($input['item_id']);

            if (empty($location)) {
                $itemMeta = DB::table('item')
                    ->join('item_meta', 'item_meta.itm_id', 'item.item_id')
                    ->where('item.deleted', 0)
                    ->where('item_meta.itm_id', $input['item_id'])
                    ->value('item_meta.value as spc_hdl_code');
                $spcCode = $itemMeta ? json_decode($itemMeta, true) : [];
                if (!empty($spcCode)) {
                    $spcCode = array_keys(array_filter($spcCode, function($value) {
                        return $value > 0;
                    }));
                }
                $input['spc_hdl_code'] = $spcCode;
                $location = $this->locationModel->getSuggestionLocation($input);
            }

            $result[] = [
                'item_id' => $input['item_id'],
                'cus_id' => $input['cus_id'],
                'loc_code' => !empty($location) ? $location->loc_code : null
            ];

            return [
                'data' => $result
            ];

        } catch (\PDOException $e) {
            return $this->response(SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__));
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}