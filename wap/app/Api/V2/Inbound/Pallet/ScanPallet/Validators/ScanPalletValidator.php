<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 22/July/2016
 * Time: 11:05
 */

namespace App\Api\V2\Inbound\Pallet\ScanPallet\Validators;

class ScanPalletValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'pallet' => 'required',
            'ctns' => 'required',
            'is_confirm' => 'required',
            'tablet_tag' => 'required'
        ];
    }
}
