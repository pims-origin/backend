<?php

namespace App\Api\V2\PackDetail\Models;

use Seldat\Wms2\Models\WavepickDtl;

class WavePickDtlModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getActualQtyByWvIds($wvIds)
    {
        $wvIds = is_array($wvIds) ? $wvIds : [$wvIds];
        if (empty($wvIds)) {
            return [];
        }

        return $this->model
            ->whereIn('wv_id', $wvIds)
            ->get();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getActualQty($wvIds, $itemId, $lot)
    {
        $wvIds = is_array($wvIds) ? $wvIds : [$wvIds];
        if (empty($wvIds)) {
            return [];
        }

        return $this->model
            ->whereIn('wv_id', $wvIds)
            ->where('item_id', $itemId)
            ->where('lot', $lot)
            ->first();
    }
}
