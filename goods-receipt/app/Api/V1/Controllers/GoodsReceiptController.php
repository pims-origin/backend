<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\DamageCartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\InventoryModel;
use App\Api\V1\Models\InventoriesModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\ReportInventoryModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Models\ReceivingModel;
use App\Api\V1\Models\ReceivingService;
use App\Api\V1\Models\SystemUomModel;
use Seldat\Wms2\Models\GoodsReceipt;
use App\Api\V1\Models\UserMetaModel;
use App\Api\V1\Models\GoodsReceiptReportModel;
use App\Api\V1\Models\SKUTrackingReportModel;
use App\Api\V1\Traits\GoodsReceiptControllerTrait;
use App\Api\V1\Transformers\DamagedCartonListTransformer;
use App\Api\V1\Transformers\GoodsReceiptDetailTransformer;
use App\Api\V1\Transformers\GoodsReceiptListTransformer;
use App\Api\V1\Transformers\GoodsReceiptTransformer;
use App\Api\V1\Validators\DamageCartonValidator;
use App\Api\V1\Validators\GoodsReceiptUpdateValidator;
use App\Api\V1\Validators\GoodsReceiptValidator;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Jobs\AutoUpdateDailyInventoryAndPalletReport;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class GoodsReceiptController extends AbstractController
{
    use GoodsReceiptControllerTrait;
    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;

    /**
     * @var AsnHdrModel
     */
    protected $asnHdrModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    protected $inventoriesModel;

    /**
     * @var ReportInventoryModel
     */
    protected $reportInventoryModel;

    /**
     * @var ReceivingModel
     */
    protected $receivingModel;

    /**
     * @var ReceivingService
     */
    protected $receivingService;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var DamageCartonModel
     */
    protected $damageCartonModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var PalletSuggestLocationModel
     */
    protected $palletSuggestLocationModel;

    protected $userMetaModel;
    /**
     * @var InventoryModel
     */
    protected  $inventoryModel;

    /**
     * @var GoodsReceiptReportModel
     */
    protected $goodsReceiptReportModel;

    protected $SKUTrackingReportModel;

    /**
     * GoodsReceiptController constructor.
     *
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param AsnDtlModel $asnDtlModel
     * @param AsnHdrModel $asnHdrModel
     * @param CartonModel $cartonModel
     * @param EventTrackingModel $eventTrackingModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param ReceivingModel $receivingModel
     * @param ReceivingService $receivingService
     * @param CustomerConfigModel $customerConfigModel
     */
    public function __construct
    (
        GoodsReceiptModel $goodsReceiptModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        AsnDtlModel $asnDtlModel,
        AsnHdrModel $asnHdrModel,
        CartonModel $cartonModel,
        EventTrackingModel $eventTrackingModel,
        InventorySummaryModel $inventorySummaryModel,
        InventoriesModel $inventoriesModel,
        ReportInventoryModel $reportInventoryModel,
        ReceivingModel $receivingModel,
        ReceivingService $receivingService,
        CustomerConfigModel $customerConfigModel,
        DamageCartonModel $damageCartonModel,
        PalletModel $palletModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        GoodsReceiptReportModel $goodsReceiptReportModel,
        SKUTrackingReportModel $SKUTrackingReportModel
    ) {
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
        $this->asnDtlModel = $asnDtlModel;
        $this->asnHdrModel = $asnHdrModel;
        $this->cartonModel = $cartonModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->inventoriesModel = $inventoriesModel;
        $this->reportInventoryModel = $reportInventoryModel;
        $this->receivingModel = $receivingModel;
        $this->receivingService = $receivingService;
        $this->customerConfigModel = $customerConfigModel;
        $this->damageCartonModel = $damageCartonModel;
        $this->palletModel = $palletModel;
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->userMetaModel = new UserMetaModel();
        $this->inventoryModel =new InventoryModel();
        $this->goodsReceiptReportModel = New GoodsReceiptReportModel();
        $this->SKUTrackingReportModel = New SKUTrackingReportModel();
    }

    /**
     * @param $goodsReceiptId
     * @param GoodsReceiptDetailTransformer $goodsReceiptDetailTransformer
     * @param SystemUomModel $systemUomModel
     * @param AsnDtlModel $asnDtlModel
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(
        $goodsReceiptId,
        GoodsReceiptDetailTransformer $goodsReceiptDetailTransformer,
        SystemUomModel $systemUomModel,
        AsnDtlModel $asnDtlModel
    ) {
        $whs_id = Data::getCurrentWhsId();
        $currentCusIds = Data::getCurrentCusIds();

        try {
            $query = $this->goodsReceiptModel->make([
                'asnHdr',
                'goodsReceiptStatus',
                'goodsReceiptDetail',
                'container',
                'customer',
                'warehouse'
            ]);
            $goodsReceipt = $query
                ->where('gr_hdr_id', $goodsReceiptId)
                ->where('gr_hdr.whs_id', $whs_id)
                ->whereIn('gr_hdr.cus_id', $currentCusIds)
                ->first();

            if ($goodsReceipt) {
                $goodsReceipt['details'] = $this->getGoodsReceiptDetail($goodsReceipt, $systemUomModel, $asnDtlModel);
            } else {
                return $this->response->errorBadRequest('Goods receipt not existed');
            }

            $key = array_search('PD', array_column($goodsReceipt['details'], 'gr_dtl_sts'));
            if ($key === 0 || $key > 0) {
                $goodsReceipt['is_pending'] = 1;
            }

            return $this->response->item($goodsReceipt, $goodsReceiptDetailTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param GoodsReceiptValidator $goodsReceiptUpdateValidator
     * @param GoodsReceiptTransformer $goodsReceiptTransformer
     * @param AsnHdrModel $asnHdrModel
     *
     * @return mixed|\Symfony\Component\HttpFoundation\Response|void
     */
    public function store(
        Request $request,
        GoodsReceiptValidator $goodsReceiptUpdateValidator,
        GoodsReceiptTransformer $goodsReceiptTransformer,
        AsnHdrModel $asnHdrModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validate
        $goodsReceiptUpdateValidator->validate($input);

        $asnHeader = $asnHdrModel->getFirstBy('asn_hdr_id', $input['asn_hdr_id']);

        //get asn_dtl.asn_dtl_ept_dt //#5980
        $res = DB::table('asn_dtl')->where('ctnr_id', $input['ctnr_id'])
                                    ->where('asn_hdr_id', $input['asn_hdr_id'])
                                   ->first();
        $expectedDate =  $res['asn_dtl_ept_dt'];

        try {
            DB::beginTransaction();

            $asnDetails = $this->asnDtlModel->findWhere([
                'ctnr_id'    => $input['ctnr_id'],
                'asn_hdr_id' => $input['asn_hdr_id'],
            ]);

            if ($asnDetails->isEmpty()) {
                return $this->response->errorBadRequest(Message::get("VR028", "ASN"));
            }

            $asnDetails = array_pluck($asnDetails->toArray(), null, 'asn_dtl_id');

            // GR is already in DB
            if ($this->goodsReceiptModel->checkWhere([
                'asn_hdr_id' => $input['asn_hdr_id'],
                'ctnr_id'    => $input['ctnr_id']
            ])
            ) {
                return $this->response->errorBadRequest(Message::get("BM007", "container", "Good receipt"));
            }

            // generate GR num
            $grNumAndSeq = $this->generateGrNumAndSeq($asnHeader->asn_hdr_num, $asnHeader->asn_hdr_id);

            $paramsHeader = [
                'ctnr_id'       => $input['ctnr_id'],
                'asn_hdr_id'    => $input['asn_hdr_id'],
                'gr_in_note'    => array_get($input, 'gr_in_note', null),
                'gr_ex_note'    => array_get($input, 'gr_ex_note', null),
                'gr_hdr_seq'    => $grNumAndSeq['gr_seq'],
                //'gr_hdr_ept_dt' => $asnHeader->asn_hdr_ept_dt,
                'gr_hdr_ept_dt' => $expectedDate,//#5980
                'gr_hdr_num'    => $grNumAndSeq['gr_num'],
                'whs_id'        => $asnHeader->whs_id,
                'cus_id'        => $asnHeader->cus_id,
                'gr_sts'        => config('constants.gr_status.RECEIVING'),
                'ctnr_num'      => object_get(Container::where('ctnr_id', $input['ctnr_id'])->select('ctnr_num')->first
                (), 'ctnr_num', null),
                'ref_code'      => $asnHeader->asn_hdr_ref
            ];

            $grHrd = $this->goodsReceiptModel->create($paramsHeader);

            if (!empty($grHrd->gr_hdr_id)) {
                $asn_hdr_num = object_get($grHrd, 'asnHdr.asn_hdr_num', null);

                // event tracking good receipt started for
                $ctnrNum = $paramsHeader['ctnr_num'];
                $this->eventTrackingModel->create([
                    'whs_id'    => $paramsHeader['whs_id'],
                    'cus_id'    => $paramsHeader['cus_id'],
                    'owner'     => $asn_hdr_num,
                    'evt_code'  => config('constants.event.GR-RECEIVING'),
                    'trans_num' => $grNumAndSeq['gr_num'],
                    'info'      => sprintf(config('constants.event-info.GR-RECEIVING'), $ctnrNum)

                ]);

                // Insert goods receipt detail
                $grDtl = [];
                $grDtlIds = [];
                if (!empty($input['details'])) {
                    foreach ($input['details'] as $detail) {
                        if (empty($asnDetails[$detail['asn_dtl_id']])) {
                            throw new \Exception("asn_dtl_id not belong ASN");
                        }

                        $carton = intval(array_get($asnDetails, $detail['asn_dtl_id'] . ".asn_dtl_ctn_ttl", 0));
                        $actualCarton = array_get($detail, 'gr_dtl_act_ctn_ttl', 0);
                        $discrepancy = $actualCarton - $carton;

                        $gr_dtl_dmg_ttl = intval(array_get($detail, 'gr_dtl_dmg_ttl', 0));
                        $isDamaged = $gr_dtl_dmg_ttl ? 1 : 0;

                        $asnDtl = array_get($asnDetails, $detail['asn_dtl_id']);
                        $dataGrDtl = [
                            'asn_dtl_id'         => $detail['asn_dtl_id'],
                            'gr_hdr_id'          => $grHrd->gr_hdr_id,
                            'gr_dtl_act_ctn_ttl' => intval($actualCarton),
                            'gr_dtl_ept_ctn_ttl' => $carton,
                            'gr_dtl_disc'        => $discrepancy,
                            'gr_dtl_plt_ttl'     => intval(array_get($detail, 'gr_dtl_plt_ttl', 0)),
                            'gr_dtl_dmg_ttl'     => $gr_dtl_dmg_ttl,
                            'gr_dtl_is_dmg'      => $isDamaged,
                            'gr_dtl_sts'         => "RG",
                            'item_id'            => array_get($asnDtl, 'item_id', ''),
                            'sku'                => array_get($asnDtl, 'asn_dtl_sku', ''),
                            'size'               => array_get($asnDtl, 'asn_dtl_size', ''),
                            'color'              => array_get($asnDtl, 'asn_dtl_color', ''),
                            'pack'               => array_get($asnDtl, 'asn_dtl_pack', ''),
                            'lot'                => array_get($asnDtl, 'asn_dtl_lot', ''),
                            'po'                 => array_get($asnDtl, 'asn_dtl_po', ''),
                            'uom_id'             => array_get($asnDtl, 'uom_id', ''),
                            'uom_code'           => array_get($asnDtl, 'uom_code', ''),
                            'uom_name'           => array_get($asnDtl, 'uom_name', ''),
                            'upc'                => array_get($asnDtl, 'asn_dtl_cus_upc', ''),
                            'ctnr_id'            => array_get($asnDtl, 'ctnr_id', ''),
                            'ctnr_num'           => array_get($asnDtl, 'ctnr_num', ''),
                            'length'             => array_get($asnDtl, 'asn_dtl_length', ''),
                            'width'              => array_get($asnDtl, 'asn_dtl_width', ''),
                            'height'             => array_get($asnDtl, 'asn_dtl_height', ''),
                            'weight'             => array_get($asnDtl, 'asn_dtl_weight', ''),
                            'cube'               => array_get($asnDtl, 'asn_dtl_cube', ''),
                            'volume'             => array_get($asnDtl, 'asn_dtl_volume', ''),
                            'expired_dt'         => array_get($asnDtl, 'expired_dt', ''),
                            'gr_dtl_des'         => array_get($asnDtl, 'asn_dtl_des', ''),
                            'crs_doc'            => $asnDtl['asn_dtl_crs_doc'] ?: 0
                        ];

                        // Check duplicate unique fields
                        $this->goodsReceiptDetailModel->refreshModel();
                        $grDtlNew = $this->goodsReceiptDetailModel->create($dataGrDtl)->toArray();
                        $grDtlIds[] = $grDtlNew['gr_dtl_id'];
                        $grDtl[$detail['asn_dtl_id']] = $grDtlNew;

                        // Change Status ASN Dtl
                        $this->asnDtlModel->updateWhere(
                            ['asn_dtl_sts' => 'RG'],
                            ['asn_dtl_id' => $detail['asn_dtl_id']]
                        );

                        //---- Insert for goods receipt report
                        $cusId=$asnHeader->cus_id;
                        $cusInfo = DB::Table('customer')
                        ->where('customer.cus_id', $cusId)
                        ->first();

                        $dataGrRpt = [
                            'cus_id'   => $cusId,
                            'cus_name' => array_get($cusInfo, 'cus_name', ''),
                            'cus_code' => array_get($cusInfo, 'cus_code', ''),

                            'gr_hdr_id'  => $grHrd->gr_hdr_id,
                            'gr_hdr_num' => $grNumAndSeq['gr_num'],
                            'whs_id'     => $asnHeader->whs_id,
                            'ctnr_num'   => array_get($asnDtl, 'ctnr_num', ''),
                            'ref_code'   => $asnHeader->asn_hdr_ref,

                            'item_id'            => array_get($asnDtl, 'item_id', ''),
                            'sku'                => array_get($asnDtl, 'asn_dtl_sku', ''),
                            'size'               => array_get($asnDtl, 'asn_dtl_size', ''),
                            'color'              => array_get($asnDtl, 'asn_dtl_color', ''),
                            'pack'               => array_get($asnDtl, 'asn_dtl_pack', ''),
                            'lot'                => array_get($asnDtl, 'asn_dtl_lot', ''),
                            'gr_dtl_act_qty_ttl' => array_get($asnDtl, 'asn_dtl_qty_ttl', ''),
                            'gr_dtl_act_ctn_ttl' => intval($actualCarton),
                            'gr_dtl_ept_ctn_ttl' => $carton,
                            'gr_dtl_disc'        => $discrepancy,
                            'gr_dtl_plt_ttl'     => intval(array_get($detail, 'gr_dtl_plt_ttl', 0)),

                            'length'         => array_get($asnDtl, 'asn_dtl_length', ''),
                            'width'          => array_get($asnDtl, 'asn_dtl_width', ''),
                            'height'         => array_get($asnDtl, 'asn_dtl_height', ''),
                            'weight'         => array_get($asnDtl, 'asn_dtl_weight', ''),
                            'volume'         => array_get($asnDtl, 'asn_dtl_volume', ''),
                            'crs_doc'        => $asnDtl['asn_dtl_crs_doc'] ?: 0,
                            'gr_dtl_dmg_ttl' => $gr_dtl_dmg_ttl,
                            'asn_hdr_ept_dt' => $asnHeader->asn_hdr_ept_dt,
                            'asn_hdr_act_dt' => $asnHeader->asn_hdr_act_dt,
                            'gr_hdr_act_dt'  => $grHrd->gr_hdr_act_dt ?: time(),
                            'cube'           => array_get($asnDtl, 'asn_dtl_cube', ''),
                        ];
                        $grrInfo=$this->goodsReceiptReportModel->create($dataGrRpt)->toArray();
                        //---- /Insert for goods receipt report

                        //---- Insert for SKU Tracking report
                        $dataSKUTrackingRpt = [
                            'cus_id'   => $cusId,
                            'cus_name' => array_get($cusInfo, 'cus_name', ''),
                            'cus_code' => array_get($cusInfo, 'cus_code', ''),

                            'gr_hdr_id'  => $grHrd->gr_hdr_id,
                            'trans_num' => $grNumAndSeq['gr_num'],
                            'whs_id'     => $asnHeader->whs_id,
                            'po_ctnr'   => array_get($asnDtl, 'ctnr_num', ''),
                            'ref_cus_order'   => $asnHeader->asn_hdr_ref,
                            'actual_date'  => $grHrd->gr_hdr_act_dt ?: time(),

                            'item_id'            => array_get($asnDtl, 'item_id', ''),
                            'sku'                => array_get($asnDtl, 'asn_dtl_sku', 0),
                            'size'               => array_get($asnDtl, 'asn_dtl_size', null),
                            'color'              => array_get($asnDtl, 'asn_dtl_color', null),
                            'pack'               => array_get($asnDtl, 'asn_dtl_pack', ''),
                            'lot'                => array_get($asnDtl, 'asn_dtl_lot', 'NA'),
                            'qty' => array_get($asnDtl, 'asn_dtl_qty_ttl', ''),
                            'ctns' => intval($actualCarton),

                            'cube'           => array_get($asnDtl, 'asn_dtl_cube', ''),
                        ];
                        $this->SKUTrackingReportModel->create($dataSKUTrackingRpt)->toArray();
                        //---- /Insert for SKU Tracking report

                    }

                    // event tracking after create good receipt detail
                    $gr_hdr_num = $grHrd->gr_hdr_num;
                    $this->eventTrackingModel->refreshModel();
                    $this->eventTrackingModel->create([
                        'whs_id'    => $paramsHeader['whs_id'],
                        'cus_id'    => $paramsHeader['cus_id'],
                        'owner'     => $gr_hdr_num,
                        'evt_code'  => config('constants.event.GR-RECEIVING'),
                        'trans_num' => $gr_hdr_num,
                        'info'      => sprintf(config('constants.event-info.GR-CREATED'), $gr_hdr_num)

                    ]);

                }

                // Change Status ASN
                $this->asnHdrModel->updateWhere(
                    ['asn_sts' => config('constants.asn_status.RECEIVING')],
                    ['asn_hdr_id' => $input['asn_hdr_id']]
                );
                $this->eventTrackingModel->refreshModel();

                // event tracking asn
                $ctnrNum = $grHrd->ctnr_num;
                $grHdrNum = $paramsHeader['gr_hdr_num'];
                $this->eventTrackingModel->create([
                    'whs_id'    => $paramsHeader['whs_id'],
                    'cus_id'    => $paramsHeader['cus_id'],
                    'owner'     => $asn_hdr_num,
                    'evt_code'  => config('constants.event.ASN-RECEIVING'),
                    'trans_num' => $asn_hdr_num,
                    'info'      => sprintf(config('constants.event-info.ASN-RECEIVING'), $grHdrNum)
                ]);

                $grDtl = array_map(function ($e) use ($asnDetails) {
                    if (!empty($asnDetails[$e['asn_dtl_id']])) {
                        $e["asn_detail"] = $asnDetails[$e['asn_dtl_id']];
                    }

                    return $e;
                }, $grDtl);

                // Create Carton Number
                $this->cartonModel->createCartonNumber($grDtl, [
                    'gr_hdr_num' => $grHrd->gr_hdr_num,
                    'whs_id'     => $grHrd->whs_id,
                    'cus_id'     => $grHrd->cus_id,
                ], 'RG'); //WMS2-4889

                // Get cartons damage by gr_dtl_ids
                $cartonIds = $this->cartonModel->getCartonByGrDtlIds($grDtlIds)->toArray();

                // Create Damage Carton
                $this->damageCartonModel->createDamageCarton($cartonIds);
            }

            DB::commit();

            return $this->response->item($grHrd, $goodsReceiptTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $goodsReceiptId
     * @param Request $request
     * @param GoodsReceiptUpdateValidator $goodsReceiptUpdateValidator
     * @param GoodsReceiptTransformer $goodsReceiptTransformer
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param AsnHdrModel $asnHdrModel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function update(
        $goodsReceiptId,
        Request $request,
        GoodsReceiptUpdateValidator $goodsReceiptUpdateValidator,
        GoodsReceiptTransformer $goodsReceiptTransformer,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        AsnHdrModel $asnHdrModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validate
        $goodsReceiptUpdateValidator->validate($input);

        try {
            DB::beginTransaction();
            $whs_id = self::getCurrentWhsId();
            $grHrd = $this->goodsReceiptModel->getFirstWhere([
                'gr_hdr_id' => $goodsReceiptId,
                'whs_id'    => $whs_id
            ]);

            if (empty($grHrd->gr_hdr_id)) {
                throw new \Exception(Message::get("BM017", "Goods Receipt"));
            }

            $gr_status = object_get($grHrd, 'gr_sts', null);
            if ($gr_status == config('constants.gr_status.RECEIVED')) {
                return $this->response->errorBadRequest('Status of this Goods Receipt is received!');
            }

            $allGoodsReceiptDetail = $goodsReceiptDetailModel->findWhere([
                'gr_hdr_id' => $goodsReceiptId,
            ], ['asnDetail.item', 'goodsReceipt']);

            $goodsReceiptDeletedIds = array_pluck($allGoodsReceiptDetail, null, 'asn_dtl_id');
            $isDamageOrDiscrepancy = false;

            $grDtl = [];
            $grDtlIds = [];
            if (!empty($input['details'])) {
                $ctnTtl = 0;
                foreach ($input['details'] as $detail) {
                    if (empty($goodsReceiptDeletedIds[$detail['asn_dtl_id']])) {
                        throw new \Exception('Details input don\'t belong to this goods receipt');
                    }

                    $curDtl = $goodsReceiptDeletedIds[$detail['asn_dtl_id']];

                    $carton = !empty($curDtl['gr_dtl_ept_ctn_ttl']) ? $curDtl['gr_dtl_ept_ctn_ttl'] : 0;
                    $actualCarton = array_get($detail, 'gr_dtl_act_ctn_ttl', 0);
                    $ctnTtl += $actualCarton;
                    $discrepancy = $actualCarton - $carton;

                    $temp = $curDtl->toArray();
                    $temp['gr_dtl_act_ctn_ttl'] = $actualCarton;
                    $grDtl[] = $temp;

                    $gr_dtl_dmg_ttl = intval(array_get($detail, 'gr_dtl_dmg_ttl', 0));
                    $isDamaged = $gr_dtl_dmg_ttl ? 1 : 0;

                    $dataGrDtl = [
                        'gr_dtl_id'          => $curDtl['gr_dtl_id'],
                        'gr_dtl_act_ctn_ttl' => intval($actualCarton),
                        'gr_dtl_disc'        => $discrepancy,
                        'gr_dtl_plt_ttl'     => intval(array_get($detail, 'gr_dtl_plt_ttl', 0)),
                        'gr_dtl_dmg_ttl'     => $gr_dtl_dmg_ttl,
                        'gr_dtl_is_dmg'      => $isDamaged,
                        'gr_dtl_sts'         => $isDamaged ? "PD" : "RE",
                    ];
                    $grDtlIds[] = $curDtl['gr_dtl_id'];

                    $isDamageOrDiscrepancy = $isDamageOrDiscrepancy
                        || $dataGrDtl['gr_dtl_is_dmg']
                        || $dataGrDtl['gr_dtl_disc'];

                    // Check duplicate unique fields
                    $goodsReceiptDetailModel->refreshModel();
                    $goodsReceiptDetailModel->update($dataGrDtl);

                    //---- Update for goods receipt report
                    $gr_hdr_id = $curDtl['gr_hdr_id'];
                    $item_id = $curDtl['item_id'];

                    $dataGrRpt = [
                        'gr_dtl_act_ctn_ttl' => intval($actualCarton),
                        'gr_dtl_disc'        => $discrepancy,
                        'gr_dtl_plt_ttl'     => intval(array_get($detail, 'gr_dtl_plt_ttl', 0)),
                        'gr_dtl_dmg_ttl'     => $gr_dtl_dmg_ttl,
                    ];
                    DB::table('rpt_receiving')->where('item_id', $item_id)->where('gr_hdr_id', $gr_hdr_id)
                        ->update($dataGrRpt);
                    //---- /Update for goods receipt report

                    //---- update for SKU Tracking report
                    $dataSKUTrackingRpt = [
                        'ctns' => intval($actualCarton),
                        'qty'  => intval(array_get($detail, 'gr_dtl_plt_ttl', 0)),
                    ];
                    DB::table('rpt_sku_tracking')->where('item_id', $item_id)->where('gr_hdr_id', $gr_hdr_id)
                        ->update($dataSKUTrackingRpt);
                    //---- /Update for SKU Tracking report

                }
            }

            $grHrd = $this->goodsReceiptModel->update([
                'gr_hdr_id'  => $goodsReceiptId,
                'gr_in_note' => array_get($input, 'gr_in_note', ''),
                'gr_ex_note' => array_get($input, 'gr_ex_note', ''),
            ]);

            // event tracking when GoodsReceipt completed
            $asn_hdr_num = object_get($grHrd, 'asnHdr.asn_hdr_num', null);
            $evt_code = config('constants.event.GR-COMPLETE');
            $info = sprintf(config('constants.event-info.GR-COMPLETE'), $ctnTtl);
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $grHrd['whs_id'],
                'cus_id'    => $grHrd['cus_id'],
                'owner'     => $asn_hdr_num,
                'evt_code'  => $evt_code,
                'trans_num' => $grHrd['gr_hdr_num'],
                'info'      => $info

            ]);

            // update ASN status
            $this->updateAsnStatus($grHrd->asn_hdr_id, $asn_hdr_num, $asnHdrModel);

            // Create Carton Number
            $this->cartonModel->createCartonNumber($grDtl, [
                'gr_hdr_num' => $grHrd->gr_hdr_num,
                'whs_id'     => $grHrd->whs_id,
                'cus_id'     => $grHrd->cus_id,
            ]);

            // Get cartons damage by gr_dtl_ids
            $cartonIds = $this->cartonModel->getCartonByGrDtlIds($grDtlIds)->toArray();

            // Create Damage Carton
            $this->damageCartonModel->createDamageCarton($cartonIds);

            DB::commit();

            //push MQ receiving
            $receivingReports = $this->receivingModel->receiving($grHrd->asn_hdr_id);
            if ($receivingReports && $this->customerConfigModel->checkWhere([
                    'whs_id'       => $grHrd->whs_id,
                    'cus_id'       => $grHrd->cus_id,
                    'config_name'  => config('constants.cus_config_name.EDI_INTEGRATION'),
                    'config_value' => config('constants.cus_config_value.EDI861'),
                    'ac'           => config('constants.cus_config_active.YES')
                ])
            ) {
                $this->receivingService->init($receivingReports);
                $this->receivingService->process();
            }

            return $this->response->item($grHrd, $goodsReceiptTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param GoodsReceiptListTransformer $goodsReceiptListTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        Request $request,
        GoodsReceiptListTransformer $goodsReceiptListTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $inputStatuses = array_get($input, 'gr_sts', []);
        $type = array_get($input, 'type', 'gr');
        $limit = array_get($input, 'limit', 20);

        try {

            $userId = Data::getCurrentUserId();
            $this->userMetaModel->modifyUserMeta(['user_id' => $userId, 'qualifier' => config('constants.qualifier.GR_STATUS')], $inputStatuses, $type);

            //show list search
            //Export CSV
            if (!empty($input['export']) && $input['export'] == 1) {
                $this->exportGRpdf($input);
                die;
            }

            //show list search
            $goodsReceipt = $this->goodsReceiptModel->search($input, [
                'goodsReceiptDetail',
                'asnHdr',
                'asnHdr.asnDtl',
                'container'
            ], $limit);

            foreach ($goodsReceipt as &$model) {
                if (!empty($this->checkIsApproveAll($model->whs_id, $model->gr_hdr_id))) {
                    $model->is_approve_all = 0;
                } else {
                    $model->is_approve_all = 1;
                }
            }

            return $this->response->paginator($goodsReceipt, $goodsReceiptListTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function rescanLPN($goodsReceiptId, Request $request)
    {
        $input = $request->getParsedBody();

        if(empty($input['lpn'])) {
            return $this->response->errorBadRequest("LPN is required!");
        }

        if(empty($input['gr_dtl_id'])) {
            return $this->response->errorBadRequest("gr_dtl_id is required!");
        }

        $lpn = $input['lpn'];
        $grDtlId = $input['gr_dtl_id'];
        $palletId = $input['plt_id'];

        if(!$this->checkLPNFormat($lpn)) {
            return $this->response->errorBadRequest("LPN is properly formatted!");
        }

        $grDtl = $this->goodsReceiptDetailModel->getFirstWhere([
            'gr_hdr_id' => $goodsReceiptId,
            'gr_dtl_id' => $grDtlId
        ]);

        if(empty($grDtl)) {
            return $this->response->errorBadRequest("GR Detail ". $grDtlId ." is not exist!");
        }

        if($grDtl->gr_dtl_sts != 'RG') {
            return $this->response->errorBadRequest("GR Detail is Received!");
        }

        $checkLpnHasSku = $this->cartonModel->getModel()
                        ->where('lpn_carton', $input['lpn'])
                        ->where('gr_hdr_id', $grDtl->gr_hdr_id)
                        ->where('sku', '<>', $grDtl->sku)
                        ->first();

        if ($checkLpnHasSku) {
            return $this->response->errorBadRequest(sprintf("LPN %s already exists another SKU!", $input['lpn']));
        }

        $this->cartonModel->updateWhere(['lpn_carton' => $lpn], ['gr_dtl_id' => $grDtlId, 'gr_hdr_id' => $goodsReceiptId, 'plt_id' => $palletId]);
        $this->palletModel->updateWhere(['plt_num' => $lpn, 'rfid' => $lpn], ['plt_id' => $palletId]);

        return [
            'data' => [
                'message' => "Update LPN Successfully!"
            ]
        ];
    }

    public function checkLPNFormat($lpn)
    {
        if((bool)preg_match("/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $lpn)) {
            return true;
        }
        return false;
    }

    public function checkIsApproveAll($whsId, $grHdrId)
    {
        return DB::table('pallet')
            ->join('gr_dtl', function($join){
                $join->on('gr_dtl.gr_hdr_id', '=', 'pallet.gr_hdr_id');
            })
            ->join('item', 'item.item_id', '=', 'gr_dtl.item_id')
            //->join('item_status', 'item_status.item_sts_code', '=', 'item.status')
            ->where('item.status', '=', 'RG')
            ->where('gr_dtl.gr_hdr_id', '=', $grHdrId)
            ->where('pallet.whs_id', $whsId)
            ->first();
    }

    /**
     * @param $input
     *
     * @return $this|void
     */
    private function exportGRpdf($input)
    {
        try {
            $goodsReceipt = $this->goodsReceiptModel->search($input, [
                'goodsReceiptDetail',
                'asnHdr',
                'asnHdr.asnDtl',
                'container',
                'asnHdr.customer',
                'goodsReceiptStatus',
                'user'
            ], null, true);

            $goodsReceipt = $goodsReceipt->transform(function($item){
                $item->gr_hdr_act_dt = (is_int($item->gr_hdr_act_dt) && $item->gr_hdr_act_dt > 0 )?date('m-d-Y',$item->gr_hdr_act_dt):'';
                $item->asn_hdr_act_dt = (object_get($item, 'asnHdr.asn_hdr_act_dt', null)) ? date('m/d/Y', object_get($item, 'asnHdr.asn_hdr_act_dt', null)) : '';
                return $item;
            })->toArray();

            $title = [
                'goods_receipt_status.gr_sts_name' => 'Status',
                'gr_hdr_num'                       => 'Goods Receipt #',

                'asn_hdr.asn_hdr_num'                                   => 'ASN Number',
                'item_ttl'                                              => '# of SKUs',
                'gr_act_ctn_ttl'                                        => '# of Cartons',
                'asn_hdr.customer.cus_code|.|asn_hdr.customer.cus_name' => 'Customer',
                'ctnr_num'                                              => 'Container',
                'asnHdr.asn_hdr_ref'                                    => 'Ref Code',
                'gr_hdr_act_dt'                                         => 'Completed Date',
                'asn_hdr_act_dt'                                        => 'Goods Receipt Date',

                'user.first_name|.|user.last_name' => 'User',
            ];

            // $filePath = storage_path() . "/Report_PBPM_{$warehouse->whs_name}.csv";

            $today = time();
            $filePath = "Report_GR_{$today}";
            $this->saveFile($title, $goodsReceipt, $filePath);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->openToBrowser($filePath); // stream data directly to the browser

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();
    }

    /**
     * @param $goodsReceiptHeader
     * @param SystemUomModel $systemUomModel
     * @param AsnDtlModel $asnDtlModel
     *
     * @return array
     */
    private function getGoodsReceiptDetail(
        $goodsReceiptHeader,
        SystemUomModel $systemUomModel,
        AsnDtlModel $asnDtlModel
    ) {
        $details = [];
        if (!empty($goodsReceiptHeader->goodsReceiptDetail)) {

            $grDetails = array_pluck($goodsReceiptHeader->goodsReceiptDetail, null, 'asn_dtl_id');

            if (!empty($grDetails)) {
                // All System Uom
                $uoms = $systemUomModel->all();
                $uoms = array_pluck($uoms, null, 'sys_uom_id');

                foreach ($grDetails as $grDetail) {
                    $asnDtl = $asnDtlModel->getFirstWhere([
                        'asn_dtl_id' => $grDetail['asn_dtl_id'],
                        'ctnr_id'    => $goodsReceiptHeader->ctnr_id,
                    ], ['asnHdr']);

                    if ($asnDtl) {
                        $uomId = array_get($asnDtl, 'uom_id', 0);
                        $crsDoc = $asnDtl['asn_dtl_crs_doc'] ?: 0;

                        $detail = [
                            'dtl_asn_dtl_id'         => $asnDtl['asn_dtl_id'],
                            'dtl_gr_dtl_id'          => $grDetail['gr_dtl_id'],
                            'dtl_item_id'            => $asnDtl->item_id, // Item Id
                            'asn_dtl_cus_upc'        => $asnDtl->item['cus_upc'],
                            'dtl_sku'                => $asnDtl->item['sku'],
                            'dtl_size'               => $asnDtl->item['size'],
                            'dtl_color'              => $asnDtl->item['color'],
                            'dtl_item_status_code'   => $asnDtl->item['status'],
                            'dtl_item_status'        => $asnDtl->item->itemStatus['item_sts_name'],
                            'asn_dtl_lot'            => !empty($grDetail['lot']) ? $grDetail['lot']:'NA' ,
                            'asn_dtl_pack'           => array_get($asnDtl, 'asn_dtl_pack', ''),
                            'dtl_length'             => array_get($asnDtl, 'asn_dtl_length', ''),
                            'dtl_width'              => array_get($asnDtl, 'asn_dtl_width', ''),
                            'dtl_height'             => array_get($asnDtl, 'asn_dtl_height', ''),
                            'dtl_weight'             => array_get($asnDtl, 'asn_dtl_weight', ''),
                            'dtl_gr_dtl_ept_ctn_ttl' => $grDetail['gr_dtl_ept_ctn_ttl'],
                            'dtl_gr_dtl_act_ctn_ttl' => $grDetail['gr_dtl_act_ctn_ttl'],
                            'dtl_gr_dtl_plt_ttl'     => $grDetail['gr_dtl_plt_ttl'],
                            'dtl_asn_dtl_crs_doc'    => $crsDoc,
                            'act_ctn_except_xdoc'    => $grDetail['gr_dtl_act_ctn_ttl'] - $crsDoc,
                            'dtl_gr_dtl_disc'        => $grDetail['gr_dtl_disc'],
                            'dtl_gr_dtl_des'         => $asnDtl->item['description'],
                            'gr_dtl_dmg_ttl'         => $grDetail['gr_dtl_dmg_ttl'],
                            'dtl_gr_dtl_is_dmg'      => $grDetail['gr_dtl_is_dmg'] ? $grDetail['gr_dtl_is_dmg'] : 0,
                            'dtl_asn_dtl_po'         => $asnDtl['asn_dtl_po'],

                            'dtl_asn_dtl_po_dt' => !empty($asnDtl['asn_dtl_po_dt'])
                                ? date('m/d/Y', $asnDtl['asn_dtl_po_dt']) : '',
                            'dtl_uom_id'        => $uomId,
                            'dtl_uom_name'      => array_get($uoms, "{$uomId}.sys_uom_name", ''),
                            'dtl_gr_dtl_sts_code' => $grDetail['gr_dtl_sts'],
                            'dtl_gr_dtl_sts'    => Status::getByValue($grDetail['gr_dtl_sts'], 'GR_STATUS')
                                ?: Status::getByValue('RE', 'GR_STATUS'),
                            'expired_dt'        => !empty($grDetail['expired_dt'])
                                ? date('m/d/Y', $grDetail['expired_dt'])
                                : '',
                            'asn_dtl_hdl_code'  => array_get($asnDtl, 'spc_hdl_code', ''),
                            'asn_dtl_hdl_name'  => array_get($asnDtl, 'spc_hdl_name', ''),
                            'prod_line'  => array_get($asnDtl, 'prod_line', ''),
                            'cmp'  => array_get($asnDtl, 'cmp', ''),
                        ];

                        $detail['dtl_length'] = empty($detail['dtl_length']) ? '' : number_format
                        ($detail['dtl_length'], 2);
                        $detail['dtl_width'] = empty($detail['dtl_width']) ? '' : number_format
                        ($detail['dtl_width'], 2);
                        $detail['dtl_height'] = empty($detail['dtl_height']) ? '' : number_format
                        ($detail['dtl_height'], 2);
                        $detail['dtl_weight'] = empty($detail['dtl_weight']) ? '' : number_format
                        ($detail['dtl_weight'], 2);

                        $details[] = $detail;
                    }
                }
            }
        }

        return $details;
    }

    /**
     * @param $goodsReceiptHeader
     * @param $cartonModel
     *
     * @return array
     */
    public static function getDamagedCartonAccordingToGRDtl(
        $goodsReceiptHeader,
        CartonModel $cartonModel,
        $input,
        $with = [],
        $limit
    ) {
        $damageCartons = [];
        if (!empty($goodsReceiptHeader->goodsReceiptDetail)) {
            $grDetails = array_pluck($goodsReceiptHeader->goodsReceiptDetail, null, 'gr_dtl_id');
            if (!empty($grDetails)) {
                //$itemIds = array_pluck($grDetails, 'item_id');
                $grDtlIds = array_pluck($grDetails, 'gr_dtl_id');

                $damageCartons = $cartonModel->findDamagedCartonByItemIds($grDtlIds, $limit);
                if ($damageCartons) {
                    $damageCartons = $damageCartons->toArray();

                    if ($damageCartons) {
                        foreach ($damageCartons['data'] as $idx => $damageCartonInfo) {
                            $damageCartonInfoArrays = [
                                'item_id'       => array_get($damageCartonInfo, 'item_id', ''),
                                'ctn_id'        => array_get($damageCartonInfo, 'ctn_id', ''),
                                'ctn_num'       => array_get($damageCartonInfo, 'ctn_num', ''),
                                'piece_ttl'     => array_get($damageCartonInfo, 'piece_ttl', ''),
                                'piece_remain'  => array_get($damageCartonInfo, 'piece_remain', ''),
                                'gr_dtl_id'     => array_get($damageCartonInfo, 'gr_dtl_id', ''),
                                'ctnr_id'       => array_get($damageCartonInfo, 'ctnr_id', ''),
                                'ctnr_num'      => array_get($damageCartonInfo, 'ctnr_num', ''),
                                'rfid'          => array_get($damageCartonInfo, 'rfid', ''),
                                'sku'           => array_get($damageCartonInfo, 'sku', ''),
                                'upc'           => array_get($damageCartonInfo, 'upc', ''),
                                'des'           => array_get($damageCartonInfo, 'des', ''),
                                'size'          => array_get($damageCartonInfo, 'size', ''),
                                'color'         => array_get($damageCartonInfo, 'color', ''),
                                'lot'           => array_get($damageCartonInfo, 'lot', ''),
                                'ctn_pack_size' => array_get($damageCartonInfo, 'ctn_pack_size', ''),
                                'dmg_id'        => array_get($damageCartonInfo, 'dmg_id', ''),
                                'dmg_name'      => array_get($damageCartonInfo,
                                    'dmg_name', ''),
                                'dmg_note'      => array_get($damageCartonInfo,
                                    'dmg_note', ''),
                                'updated_at'    => !empty($damageCartonInfo['updated_at'])
                                    ? date('m/d/Y', $damageCartonInfo['updated_at']) : '',
                                'is_damaged'    => array_get($damageCartonInfo, 'is_damaged', ''),
                                'updated_by'    => trim(array_get($damageCartonInfo, "first_name",
                                        null) . " " .
                                    array_get($damageCartonInfo, "last_name", null))

                            ];
                            $damageCartons['data'][$idx] = $damageCartonInfoArrays;
                        }
                    }

                }
            }
        }

        return $damageCartons;
    }

    /**
     * @param $asnNum
     * @param $asnHrdId
     *
     * @return array
     */
    private function generateGrNumAndSeq($asnNum, $asnHrdId)
    {
        $numOfGr = $this->goodsReceiptModel->checkWhere(['asn_hdr_id' => $asnHrdId]);

        $result = [
            'gr_num' => '',
            'gr_seq' => '',
        ];

        $result['gr_seq'] = ($numOfGr) ? $numOfGr + 1 : 1;

        $grNum = str_replace(config('constants.asn_prefix'), config('constants.gr_prefix'), $asnNum);

        $result['gr_num'] = sprintf('%s-%s',
            $grNum,
            str_pad($result['gr_seq'], 2, '0', STR_PAD_LEFT));

        return $result;
    }

    /**
     * @param $asnHrdId
     * @param $asnHdrNum
     * @param AsnHdrModel $asnHdrModel
     */
    private function updateAsnStatus(
        $asnHrdId,
        $asnHdrNum,
        AsnHdrModel $asnHdrModel
    ) {
        $asnStatus = $this->shouldAsStatus($asnHrdId);
        $dataUpdated = [
            'asn_sts' => $asnStatus
        ];

        $asnHdrModel->updateWhere($dataUpdated, ['asn_hdr_id' => $asnHrdId]);

        $asnHdrInfo = $asnHdrModel->getFirstBy('asn_hdr_id', $asnHrdId);
        $asnHdrInfo_whs_id = object_get($asnHdrInfo, 'whs_id', 0);
        $asnHdrInfo_cus_id = object_get($asnHdrInfo, 'cus_id', 0);

        $evt_code = config('constants.event.ASN-RECEIVING');
        $info = sprintf(config('constants.event-info.ASN-RECEIVING'), $asnHdrNum);
        switch ($asnStatus) {
            case config('constants.asn_status.RCVD-PARTIAL'):
                $evt_code = config('constants.event.ASN-RCVD-PARTIAL');
                $info = sprintf(config('constants.event-info.ASN-RCVD-PARTIAL'), $asnHdrNum);
                break;
            case config('constants.asn_status.RECEIVING_COMPLETE'):
                $evt_code = config('constants.event.ASN-COMPLETE');
                $info = sprintf(config('constants.event-info.ASN-COMPLETE'), $asnHdrNum);
                break;
            default:
                break;
        }

        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->create([
            'whs_id'    => $asnHdrInfo_whs_id,
            'cus_id'    => $asnHdrInfo_cus_id,
            'owner'     => $asnHdrNum,
            'evt_code'  => $evt_code,
            'trans_num' => $asnHdrNum,
            'info'      => $info

        ]);

    }

    /**
     * @param $asnHrdId
     *
     * @return mixed
     */
    private function shouldAsStatus($asnHrdId)
    {
        //countGrOfAsnWithStatus
        $hasReceivingGr = $this->goodsReceiptModel->checkWhere(
            [
                "asn_hdr_id" => $asnHrdId,
                "gr_sts"     => config('constants.gr_status.RECEIVING')
            ]);

        //countGrOfAsnWithStatus
        $hasRcvdDiscrepantGr = $this->goodsReceiptModel->checkWhere(
            [
                "asn_hdr_id" => $asnHrdId,
                "gr_sts"     => config('constants.gr_status.RCVD-DISCREPANT')
            ]);

        if ($hasReceivingGr || $hasRcvdDiscrepantGr) {
            return config('constants.asn_status.RECEIVING');
        }
        //countGrHrdOfAsn
        $grTotal = $this->goodsReceiptModel->checkWhere(['asn_hdr_id' => $asnHrdId]);
        $asnTotalContainer = $this->asnDtlModel->countContainerOfAsn($asnHrdId);

        $hasRemainingContainer = $asnTotalContainer - $grTotal;

        if ($hasRemainingContainer) {
            return config('constants.asn_status.RCVD-PARTIAL');
        } else {
            return config('constants.asn_status.COMPLETED');
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function export(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $type = strtolower(array_get($input, 'type', 'csv'));

        try {
            if (!in_array($type, ['csv', 'pdf'])) {
                throw new \Exception(Message::get("VR005", "(.csv|.pdf)"));
            }

            $goodsReceipts = $this->goodsReceiptModel->search($input, [
                'goodsReceiptStatus',
                'goodsReceiptDetail',
                'asnHdr.asnDtl',
                'customer',
                'container'
            ], array_get($input, 'limit', 999999999));

            $xDocks = $this->asnDtlModel->search(['xdoc' => true], [], array_get($input, 'limit', 999999999))
                ->toArray()
            ['data'];
            $xDocks = array_pluck($xDocks, 'xdoc', 'asn_ctnr');

            $data[] = [
                'Goods Receipt Number',
                'Status',
                'Container',
                'ASN Number',
                'Customer',
                'REF Code',
                'Number of Expected Cartons',
                'Number of Actual Cartons',
                'X-Dock',
                'Damaged',
                'Completed Date',
            ];

            foreach ($goodsReceipts as $goodsReceipt) {
                $countIsDamage = array_sum(
                    array_column($goodsReceipt->goodsReceiptDetail->toArray(), 'gr_dtl_is_dmg')
                );

                $expectedCartons = array_sum(
                    array_column($goodsReceipt->goodsReceiptDetail->toArray(), 'gr_dtl_ept_ctn_ttl')
                );

                $actualCartons = array_sum(
                    array_column($goodsReceipt->goodsReceiptDetail->toArray(), 'gr_dtl_act_ctn_ttl')
                );

				$act_date = object_get($goodsReceipt, 'gr_hdr_act_dt', '');
				$act_date = ($act_date) ? date('m/d/Y', is_int($act_date) ?$act_date: $act_date->timestamp) : '';
                $data[] = [
                    object_get($goodsReceipt, 'gr_hdr_num', null),
                    object_get($goodsReceipt, 'goodsReceiptStatus.gr_sts_name', null),
                    object_get($goodsReceipt, 'container.ctnr_num', ''),
                    object_get($goodsReceipt, 'asnHdr.asn_hdr_num', null),
                    object_get($goodsReceipt, 'customer.cus_name', null),
                    object_get($goodsReceipt, 'asnHdr.asn_hdr_ref', null),
                    $expectedCartons,
                    $actualCartons,
                    (int)array_get($xDocks, "$goodsReceipt->asn_hdr_id-$goodsReceipt->ctnr_id", 0),
                    $countIsDamage > 0 ? "Yes" : "No",
					$act_date
                ];
            }

            SelExport::exportList("Export_GoodsReceipt_List_" . date("Y-m-d", time()), $type, $data);

            return $this->response->noContent();
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function updateXdoc(Request $request)
    {
        $input = $request->getParsedBody();
        //validate

        $grDtlId = $input['gr_dtl_id'];
        $xdoc = $input['crs_doc'];

        \DB::setFetchMode(\PDO::FETCH_ASSOC);
        $grDtlObj = GoodsReceiptDetail::where('gr_dtl_id', $grDtlId)->first();
        if (!$grDtlObj) {
            return $this->response->errorBadRequest('Good receipt id ' . $grDtlId . ' not existed');
        }

        $damageCartons = $grDtlObj->gr_dtl_dmg_ttl > 0 ? ($grDtlObj->gr_dtl_dmg_ttl / $grDtlObj->pack) : 0;
        if ($grDtlObj->gr_dtl_act_ctn_ttl - $damageCartons < $xdoc) {
            return $this->response->errorBadRequest('New CrossDock Total have to <= (actual - damage)');
        }

        try {
            \DB::beginTransaction();

            //update xdoc in asn and gr detail
            AsnDtl::where('asn_dtl_id', $grDtlObj->asn_dtl_id)
                ->update(['asn_dtl_crs_doc' => $xdoc]);
            $grDtlObj->crs_doc = $xdoc;
            $grDtlObj->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }

        return ['data' => 'successful'];
    }

    public function assignXdocForm($grHdrId)
    {
        \DB::setFetchMode(\PDO::FETCH_ASSOC);
        $cusId = $this->goodsReceiptModel->getModel()
            ->where('gr_hdr_id', $grHdrId)
            ->value('cus_id');

        $rs = $this->goodsReceiptDetailModel->getModel()
            ->select([
                'gr_hdr_id',
                'gr_dtl_id',
                'item_id',
                'sku',
                'size',
                'color',
                'lot',
                'gr_dtl_act_ctn_ttl',
                //\DB::raw('IF(gr_dtl_dmg_ttl > 0, (gr_dtl_dmg_ttl DIV pack), 0) AS gr_dtl_dmg_ttl'),
                'gr_dtl_dmg_ttl',
                'crs_doc'
            ])
            ->where('gr_hdr_id', $grHdrId)
            ->where('gr_dtl_sts', 'RG')
            ->where('crs_doc', '>', 0)
            ->get()->toArray();

        return ['data' => ['cus_id' => $cusId, 'xdoc_dtl' => $rs]];
    }

    public function xdocLocation($cusId)
    {
        $whsId = Data::getCurrentWhsId();
        \DB::setFetchMode(\PDO::FETCH_ASSOC);
        $rs = Location::join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->select('location.loc_id', 'location.loc_code')
            ->where([
                'location.loc_whs_id'    => $whsId,
                'loc_type.loc_type_code' => 'XDK',
                'location.loc_sts_code'  => 'AC'
            ])
            ->where(function ($query) use ($cusId) {
                return $query->where('pallet.cus_id', $cusId)
                    ->orWhere('pallet.loc_id', null);
            })
            ->get()->toArray();

        return ['data' => $rs];
    }

    public function assignXdoc(Request $request, $grHdrId)
    {
        $input = $request->getParsedBody();
        $whsId = Data::getCurrentWhsId();
        \DB::setFetchMode(\PDO::FETCH_ASSOC);
        $grHdrObj = $this->goodsReceiptModel->getModel()
            ->where('gr_hdr_id', $grHdrId)
            ->where('whs_id', $whsId)
            ->first(['gr_hdr_id', 'whs_id', 'cus_id', 'created_from','gr_hdr_num','asn_hdr_id','ctnr_id']);
        if (!$grHdrObj) {
            return $this->response->errorBadRequest('Goods receipt not existed in current warehouse');
        }

        $locObjs = [];

        try {
            \DB::beginTransaction();
            $xdocEntryCntr = GoodsReceiptDetail::where([
                    'gr_hdr_id' => $grHdrId
                ])->select(DB::raw('IF( SUM(gr_dtl_act_ctn_ttl) = SUM(crs_doc), 1, 0 ) AS chk'))
                    ->groupBy('gr_hdr_id')
                    ->value('chk');

            foreach ($input as $rw) {
                $grDtlId = $rw['gr_dtl_id'];
                $locId = $rw['loc_id'];

                $grDtlObj = $this->goodsReceiptDetailModel->getModel()
                    ->where('gr_dtl_id', $grDtlId)
                    ->where('gr_hdr_id', $grHdrId)
                    ->first();
                if (!$grDtlObj) {
                    return $this->response->errorBadRequest('Goods Receipt Detail Id: ' . $grDtlId . ' Invalid');
                }

                $damageCartons = $grDtlObj->gr_dtl_dmg_ttl > 0 ? ($grDtlObj->gr_dtl_dmg_ttl / $grDtlObj->pack) : 0;
                if ($grDtlObj->gr_dtl_act_ctn_ttl - $damageCartons < $grDtlObj->crs_doc) {
                    return $this->response->errorBadRequest('New CrossDock Total have to <= (actual - damage)');
                }

                if ($grDtlObj->gr_dtl_sts === 'XD') {
                    return $this->response->errorBadRequest('Goods Receipt Detail: ' . $grDtlId . " was assigned");
                }

                //update gr dtl sts
                $grDtlObj->gr_dtl_sts = 'XD';
                $grDtlObj->save();

                if ($grDtlObj->crs_doc === 0) {
                    continue;
                }

                if (isset($locObjs[$locId])) {
                    $locObj = $locObjs[$locId];
                } else {
                    $locObj = $this->cartonModel->getCrsDockLoc($grHdrObj, $grDtlObj->gr_dtl_id, $locId);
                    $locObjs[$locId] = $locObj;
                }

                //update xdoc cartons location
                Carton::where('gr_dtl_id', $grDtlId)
                    ->where('is_damaged', 0) //Xdoc don't have damage
                    ->limit($grDtlObj->crs_doc)
                    ->update([
                        'loc_id'        => $locObj->loc_id,
                        'loc_code'      => $locObj->loc_code,
                        'loc_name'      => $locObj->loc_alternative_name,
                        'plt_id'        => $locObj->plt_id,
                        'loc_type_code' => 'XDK'
                    ]);

                $crsDocQty = $grDtlObj->crs_doc * $grDtlObj->pack;
                //update ctn_ttl for pallet
                Pallet::where('plt_id', $locObj->plt_id)
                    ->update([
                        'ctn_ttl'        => \DB::raw('ctn_ttl + ' . $grDtlObj->crs_doc),
                        'init_ctn_ttl'   => \DB::raw('ctn_ttl + ' . $grDtlObj->crs_doc),
                        'init_piece_ttl' => \DB::raw('ctn_ttl + ' . $crsDocQty),
                        'pack'           => $grDtlObj->pack,
                        'item_id'        => $grDtlObj->item_id,
                        'sku'            => $grDtlObj->sku,
                        'size'           => $grDtlObj->size,
                        'color'          => $grDtlObj->color,
                        'lot'            => $grDtlObj->lot,
                    ]);

                //update invt smr for xdoc qty
                $crsDocQty = $grDtlObj->crs_doc * $grDtlObj->pack;
                if($grHdrObj->created_from !== 'WMS' || $xdocEntryCntr) {
                    $this->inventorySummaryModel->xdocUpdateInvtSmr([
                        'item_id'     => $grDtlObj->item_id,
                        'lot'         => $grDtlObj->lot,
                        'crs_doc_qty' => $crsDocQty,
                        'sku'         => $grDtlObj->sku,
                        'size'        => $grDtlObj->size,
                        'color'       => $grDtlObj->color,
                        'upc'         => $grDtlObj->upc
                    ], $grHdrObj->cus_id);
                }
            }

            //process entry xdoc
            if ($xdocEntryCntr) {
                //update sts 'RE' for gr_hdr
                $grHdrObj->gr_sts   = 'RE';
                $grHdrObj->putaway  = 1;
                if($grHdrObj->save()){
                    $this->eventTrackingModel->refreshModel();
                    $this->eventTrackingModel->create([
                        'whs_id'    => $whsId,
                        'cus_id'    => $grHdrObj->cus_id,
                        'owner'     => $grHdrObj->gr_hdr_num,
                        'evt_code'  => Status::getByKey("Event", "GR-COMPLETE"),
                        'trans_num' => $grHdrObj->gr_hdr_num,
                        'info'      => 'GR Received - Complete GR'
                    ]);
                }

                //update sts 'RE' for asn_dtl, gr_dtl
                DB::table('gr_dtl')
                    ->join('asn_dtl', 'gr_dtl.asn_dtl_id', '=', 'asn_dtl.asn_dtl_id')
                    ->where('gr_dtl.gr_hdr_id', $grHdrId)
                    ->update([
                        'gr_dtl.gr_dtl_sts'   => 'RE',
                        'asn_dtl.asn_dtl_sts' => 'RE',
                        'gr_dtl.updated_at'   => time(),
                        'asn_dtl.updated_at'  => time(),
                        'gr_dtl.updated_by'   => Data::getCurrentUserId(),
                        'asn_dtl.updated_by'  => Data::getCurrentUserId()
                    ]);

                //update sts 'RE' for asn_hdr
                $this->asnDtlModel->updateAsnDtlsReceivedByCtnrID($grHdrObj->ctnr_id, $grHdrObj->asn_hdr_id);
                if($this->asnHdrModel->updateAsnHdrReceived($grHdrObj->asn_hdr_id)){
                    $asnHdr = $this->asnHdrModel->getAsnHdrById($grHdrObj->asn_hdr_id);
                    // Add Event Tracking Asn received
                    $this->eventTrackingModel->refreshModel();
                    $this->eventTrackingModel->create([
                        'whs_id'    => $whsId,
                        'cus_id'    => $grHdrObj->cus_id,
                        'owner'     => $asnHdr->asn_hdr_num,
                        'evt_code'  => Status::getByKey("Event", "ASN-COMPLETE"),
                        'trans_num' => $asnHdr->asn_hdr_num,
                        'info'      => sprintf(Status::getByKey("Event-info", "ASN-COMPLETE"), $asnHdr->asn_hdr_num)
                    ]);
                }
                //Update status Carton
                $timeCurrent = time();
                DB::table('cartons')
                    ->where('gr_hdr_id', $grHdrId)
                    ->update([
                        'ctn_sts' => 'AC',
                        'updated_at' => $timeCurrent,
                        'gr_dt'     => $timeCurrent,
                        'ref'       => md5('GR' . $timeCurrent)
                    ]);
            }

            // WMS2-5190 - Save gr_dtl_act_qty_ttl,gr_dtl_ept_qty_ttl  when Complete Goods Receipt
            $this->goodsReceiptDetailModel->computeGrDtlQty($grHdrId);

            \DB::commit();
            //process entry xdoc
            if($xdocEntryCntr){
                $invDate = strtotime(date('Y-m-d'));
                dispatch(new AutoUpdateDailyInventoryAndPalletReport($whsId, $grHdrObj->cus_id, $invDate));
            }
            return ['data' => 'successful'];
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }


    }

    /**
     * Export data
     *
     * @param Request $request
     *
     * @return Pagination
     */
    public function exportCsv(Request $request)
    {
        ini_set('memory_limit', '2G');
        ini_set('max_execution_time', 0);
        $cartonModel = new CartonModel();
        $userId = $this->getUserId();
        $input = $request->getQueryParams();
        $data = $cartonModel->cartonsHaveDamage($userId, $input, 'cartons', PHP_INT_MAX);

        return $this->response->paginator($data, new DamageCartonsTransformer);
    }

    /**
     * @param Request $request
     * @param $goodsReceiptId
     * @param CartonModel $cartonModel
     * @param DamagedCartonListTransformer $damagedCartonListTransformer
     *
     * @return array|void
     */
    public function DamagedCartonList(
        Request $request,
        $goodsReceiptId,
        CartonModel $cartonModel,
        DamagedCartonListTransformer $damagedCartonListTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $whs_id = Data::getCurrentWhsId();
        $currentCusIds = Data::getCurrentCusIds();

        try {
            $query = $this->goodsReceiptModel->make([
                'goodsReceiptDetail',
                'warehouse'
            ]);
            $goodsReceipt = $query
                ->where('gr_hdr_id', $goodsReceiptId)
                ->where('gr_hdr.whs_id', $whs_id)
                ->whereIn('gr_hdr.cus_id', $currentCusIds)
                ->first();

            if ($goodsReceipt) {
                $goodsReceiptDtl = $this->getDamagedCartonAccordingToGRDtl(
                    $goodsReceipt,
                    $cartonModel,
                    $input, [],
                    array_get($input, 'limit', 20));

            } else {
                return $this->response->errorBadRequest('Goods receipt not existed');
            }

            $return = [
                'gr_hdr_id'      => object_get($goodsReceipt, 'gr_hdr_id', null),
                'gr_hdr_num'     => object_get($goodsReceipt, 'gr_hdr_num', null),
                'whs_id'         => object_get($goodsReceipt, 'whs_id', null),
                'cus_id'         => object_get($goodsReceipt, 'cus_id', null),
                'damagedCartons' => $goodsReceiptDtl['data']
            ];

            unset($goodsReceiptDtl['data']);
            $data = array_merge($return, $goodsReceiptDtl);
            $meta = [
                "pagination" => [
                    "total"         => (int)array_get($data, 'total', 0),
                    "per_page"      => (int)array_get($data, 'per_page', 0),
                    "count"         => (int)array_get($data, 'to', 0),
                    "current_page"  => (int)array_get($data, 'current_page', 0),
                    "total_pages"   => (int)array_get($data, 'last_page', 0),
                    "next_page_url" => array_get($data, 'next_page_url', null),
                    "prev_page_url" => array_get($data, 'prev_page_url', null),
                    "from"          => array_get($data, 'from', 0),
                    "to"            => array_get($data, 'to', 0),
                    "last_page"     => (int)array_get($data, 'last_page', 0),
                ]
            ];
            unset($goodsReceiptDtl['total']);
            unset($goodsReceiptDtl['per_page']);
            unset($goodsReceiptDtl['current_page']);
            unset($goodsReceiptDtl['total_pages']);
            unset($goodsReceiptDtl['next_page_url']);
            unset($goodsReceiptDtl['prev_page_url']);
            unset($goodsReceiptDtl['from']);
            unset($goodsReceiptDtl['to']);
            unset($goodsReceiptDtl['last_page']);

            return [
                'data' => array_merge($return, $goodsReceiptDtl),
                'meta' => $meta
            ];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param DamageCartonValidator $damageCartonValidator
     * @param DamageCartonModel $damageCartonModel
     *
     * @return array|void
     * @throws \Exception
     */
    public function UnSetDamagedCarton(
        Request $request,
        DamageCartonValidator $damageCartonValidator,
        DamageCartonModel $damageCartonModel
    ) {

        // get data from HTTP
        $input = $request->getParsedBody();
        $damagedCartons = array_get($input['data'], 'damagedCartons', '');
        if (!$damagedCartons) {
            throw new \Exception(Message::get("Data is empty!"));
        }
        // validation
        $damageCartonValidator->validate($damagedCartons);
        try {
            DB::beginTransaction();
            $damageCartonsUnsetNum = 0;
            //for each damaged carton
            foreach ($damagedCartons as $damagedCarton) {
                if (isset($damagedCarton['unset']) && (($damagedCarton['unset']) === true)) {

                    $damageCartonsUnsetNum++;
                    //soft delete DamagedCarton
                    $damageCartonModel->deleteDamageCarton($damagedCarton['ctn_id']);

                    //update Carton
                    $carton_params = [
                        'ctn_id'     => $damagedCarton['ctn_id'],
                        'is_damaged' => 0
                    ];
                    DB::table('cartons')->where('ctn_id',
                        $carton_params['ctn_id'])->update($carton_params);

                    //update Intv_smr
                    $query = DB::table('invt_smr')
                        ->select([
                            'invt_smr.inv_sum_id',
                            'invt_smr.dmg_qty',
                            'invt_smr.avail',

                        ]);
                    $query->where('item_id', $damagedCarton['item_id']);
                    $query->where('whs_id', array_get($input['data'], 'whs_id', ''));
                    $query->where('lot', $damagedCarton['lot']);
                    $invtSmr = $query->first();

                    $invt_smr_params = [
                        'inv_sum_id' => $invtSmr['inv_sum_id'],
                        'dmg_qty'    => ($invtSmr['dmg_qty'] - $damagedCarton['piece_ttl']) < 0 ? 0 : $invtSmr['dmg_qty'] - $damagedCarton['piece_ttl'],
                        'avail'      => $invtSmr['avail'] + $damagedCarton['piece_ttl']
                    ];
                    if ($invt_smr_params['dmg_qty'] < 0) {
                        $invt_smr_params['dmg_qty'] = 0;
                    }
                    DB::table('invt_smr')->where('inv_sum_id',
                        $invtSmr['inv_sum_id'])->update($invt_smr_params);

                    //update Goods receipt detail
                    $grDtlQuery = DB::table('gr_dtl')
                        ->select([
                            'gr_dtl.gr_dtl_id',
                            'gr_dtl.gr_dtl_dmg_ttl'

                        ]);
                    $grDtlQuery->where('gr_dtl_id', $damagedCarton['gr_dtl_id']);
                    $grDtlQueryInfo = $grDtlQuery->first();

                    $grDtl_params = [
                        'gr_dtl_id'      => $damagedCarton['gr_dtl_id'],
                        'gr_dtl_dmg_ttl' => $grDtlQueryInfo['gr_dtl_dmg_ttl'] - 1
                    ];
                    if ($grDtl_params['gr_dtl_dmg_ttl'] < 0) {
                        $grDtl_params['gr_dtl_dmg_ttl'] = 0;
                    }
                    if ($grDtl_params['gr_dtl_dmg_ttl'] == 0) {
                        $grDtl_params['gr_dtl_is_dmg'] = 0;
                    }

                    DB::table('gr_dtl')->where('gr_dtl_id',
                        $grDtl_params['gr_dtl_id'])->update($grDtl_params);

                }

            }
            //Event tracking for Unset Damaged Carton
            $grNum = array_get($input['data'], 'gr_hdr_num', '');
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => array_get($input['data'], 'whs_id', ''),
                'cus_id'    => array_get($input['data'], 'cus_id', ''),
                'owner'     => $grNum,
                'evt_code'  => config('constants.event.UNSET-DAMAGED'),
                'trans_num' => $grNum,
                'info'      => sprintf('%d damaged carton(s) have been unset and pickable now', $damageCartonsUnsetNum)

            ]);

            DB::commit();

            return ["data" => "successful"];
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param DamageCartonValidator $damageCartonValidator
     * @param DamageCartonModel $damageCartonModel
     *
     * @return array|void
     * @throws \Exception
     */
    public function UpdateDamagedCarton(
        Request $request,
        DamageCartonValidator $damageCartonValidator,
        DamageCartonModel $damageCartonModel
    ) {

        // get data from HTTP
        $input = $request->getParsedBody();
        $damagedCartons = array_get($input['data'], 'damagedCartons', '');
        if (!$damagedCartons) {
            throw new \Exception(Message::get("Data is empty!"));
        }
        // validation
        $damageCartonValidator->validate($damagedCartons);

        try {
            foreach ($damagedCartons as $damagedCarton) {
                $damageCarton_params = [
                    'ctn_id'   => $damagedCarton['ctn_id'],
                    'dmg_id'   => $damagedCarton['dmg_id'],
                    'dmg_note' => $damagedCarton['dmg_note'],
                ];
                DB::table('damage_carton')->where('ctn_id',
                    $damageCarton_params['ctn_id'])->update($damageCarton_params);

            }

            return ["data" => "successful"];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest("The current server is busy, please wait a few minutes and try again");
    }

    public function getGrType()
    {
        try {
            $gr_type = Status::get('GOODS-RECEIPT-TYPE');
            $result = [];
            foreach($gr_type as $key => $value) {
                $result[] = [
                    'code' => $key,
                    'name' => $value,
                ];
            }
            return ["data" => $result];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function approve(Request $request)
    {
        $input = $request->getParsedBody();

        if(empty($input['gr_ids'])) {
            return $this->response->errorBadRequest('The field gr_ids is required!');
        }

        $grDTL = DB::table('gr_dtl')
            ->whereIn('gr_hdr_id', $input['gr_ids'])
            ->pluck('item_id');

        if(!empty($grDTL)) {

            $items = DB::table('item')
                ->whereIn('item_id', $grDTL)
                ->where('status', 'RG')
                ->update(['status' => 'AC']);

            if($items > 0) {
                $res['data'] = [
                    'message' => $items . ' items in '. count($input['gr_ids']) .' Goods Receipt have been approved!'
                ];

                return response()->json($res, 200);
            }
        }

        $res['data'] = [
            'message' => 'There are no items need to approve!'
        ];

        return response()->json($res, 200);
    }

    public function insertCartons(Request $request)
    {
        $input = $request->getQueryParams();
        $lpn_carton = array_get($input, 'lpn', []);
        $qty = array_get($input, 'qty', []);

        try {

            $carton = DB::table('cartons')
                ->where('lpn_carton', $lpn_carton)
                ->where('deleted', 0)
                ->orderBy('ctn_id', 'DESC')
                ->first();

            $idx = DB::table('cartons')->count() + 1;

            $data_insert = [];
            for($i = 0; $i < $qty; $i++) {
                $ctn_num = 'CTN-'.date('ym').'-'.str_pad($idx, 9, '0', STR_PAD_LEFT);
                $data_insert[] = [
                    'plt_id' => $carton['plt_id'],
                    'asn_dtl_id' => $carton['asn_dtl_id'],
                    'gr_dtl_id' => $carton['gr_dtl_id'],
                    'whs_id' => $carton['whs_id'],
                    'cus_id' => $carton['cus_id'],
                    'ctn_num' => $ctn_num,
                    'rfid' => $carton['rfid'],
                    'ctn_sts' => $carton['ctn_sts'],
                    'ctn_uom_id' => $carton['ctn_uom_id'],
                    'loc_id' => $carton['loc_id'],
                    'loc_code' => $carton['loc_code'],
                    'loc_name' => $carton['loc_name'],
                    'loc_type_code' => $carton['loc_type_code'],
                    'is_damaged' => $carton['is_damaged'],
                    'piece_remain' => $carton['piece_remain'],
                    'piece_ttl' => $carton['piece_ttl'],
                    'created_at' => time(),
                    'created_by' => Data::getCurrentUserId(),
                    'updated_at' => time(),
                    'updated_by' => Data::getCurrentUserId(),
                    'deleted' => $carton['deleted'],
                    'deleted_at' => $carton['deleted_at'],
                    'is_ecom' => $carton['is_ecom'],
                    'gr_hdr_id' => $carton['gr_hdr_id'],
                    'gr_dt' => $carton['gr_dt'],
                    'picked_dt' => $carton['picked_dt'],
                    'storage_duration' => $carton['storage_duration'],
                    'item_id' => $carton['item_id'],
                    'sku' => $carton['sku'],
                    'size' => $carton['size'],
                    'color' => $carton['color'],
                    'lot' => $carton['lot'],
                    'ctn_pack_size' => $carton['ctn_pack_size'],
                    'po' => $carton['po'],
                    'expired_dt' => $carton['expired_dt'],
                    'uom_code' => $carton['uom_code'],
                    'uom_name' => $carton['uom_name'],
                    'upc' => $carton['upc'],
                    'ctnr_id' => $carton['ctnr_id'],
                    'ctnr_num' => $carton['ctnr_num'],
                    'length' => $carton['length'],
                    'width' => $carton['width'],
                    'height' => $carton['height'],
                    'weight' => $carton['weight'],
                    'cube' => $carton['cube'],
                    'volume' => $carton['volume'],
                    'return_id' => $carton['return_id'],
                    'des' => $carton['des'],
                    'shipped_dt' => $carton['shipped_dt'],
                    'origin_id' => $carton['origin_id'],
                    'inner_pack' => $carton['inner_pack'],
                    'spc_hdl_code' => $carton['spc_hdl_code'],
                    'spc_hdl_name' => $carton['spc_hdl_name'],
                    'cat_code' => $carton['cat_code'],
                    'cat_name' => $carton['cat_name'],
                    'ref' => $carton['ref'],
                    'ucc128' => $carton['ucc128'],
                    'ctn_type' => $carton['ctn_type'],
                    'lpn_carton' => $carton['lpn_carton']
                ];
                $idx++;
            }

            DB::table('cartons')->insert($data_insert);

            $res['data'] = [
                'message' => 'Insert cartons successfully!'
            ];

            return response()->json($res, 200);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'Insert cartons', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function deleteGoodsReceipt($goodsReceiptId)
    {
        $whsId = Data::getCurrentWhsId();
        $goodsReceipt = $this->goodsReceiptModel->getFirstWhere(['gr_hdr_id' => $goodsReceiptId, 'whs_id' => $whsId]);
        if (empty($goodsReceipt)) {
            return $this->response()->errorBadRequest('Goods Receipt does not exists');
        }

        if ($goodsReceipt->gr_sts == 'RE') {
            return $this->response()->errorBadRequest('Goods Receipt has been completed');
        }

        try {
            DB::beginTransaction();

            $dataUpdate = [
                'deleted' => 1,
                'updated_by' => Data::getCurrentUserId(),
                'updated_at' => time()
            ];

            $cartons = $this->cartonModel->findWhere([
                'whs_id' => $goodsReceipt->whs_id,
                'gr_hdr_id' => $goodsReceipt->gr_hdr_id,
                'cus_id' => $goodsReceipt->cus_id
            ])->toArray();

            $pltIds = array_unique(array_pluck($cartons, 'plt_id'));
            $locIds = array_unique(array_pluck($cartons, 'loc_id'));

            $locationStillReceiving = $this->cartonModel->getLocationStillReceiving($goodsReceiptId, $whsId, $locIds);
            $locIdsCanActive = array_diff($locIds, $locationStillReceiving);

            DB::table('location')->whereIn('loc_id', $locIdsCanActive)->update(['loc_sts_code' => 'AC']);

            // update pallet
            $this->palletModel->updateWhereIn($dataUpdate, $pltIds, 'plt_id');

            // update carton
            $this->cartonModel->updateWhere($dataUpdate, [
                'whs_id' => $goodsReceipt->whs_id,
                'gr_hdr_id' => $goodsReceipt->gr_hdr_id,
                'cus_id' => $goodsReceipt->cus_id
            ]);

            // update pal_sug_loc
            DB::table('pal_sug_loc')->where('gr_hdr_id', $goodsReceiptId)->update($dataUpdate);

            // update goods receipt detail
            $this->goodsReceiptDetailModel->updateWhere($dataUpdate, ['gr_hdr_id' => $goodsReceiptId]);

            // update goods receipt header
            $this->goodsReceiptModel->updateWhere($dataUpdate, ['asn_hdr_id' => $goodsReceipt->asn_hdr_id, 'whs_id' => $whsId]);

            DB::commit();

            return [
                'data' => [
                    'message' => sprintf('Delete Goods Receipt %s successfully', $goodsReceipt->gr_hdr_num)
                ]
            ];
        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response()->errorBadRequest(
                SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }
}
