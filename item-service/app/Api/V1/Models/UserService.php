<?php
namespace App\Api\V1\Models;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use GuzzleHttp\Exception\RequestException;

class UserService extends BaseService
{
    private $userId;

    public function __construct(IRequest $request, $userId = null, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_AUTHENTICATION');
        parent::__construct($request, $api);
        $this->userId = $userId;
    }

    public function getUserListByQueryString($queryStr)
    {
        $uri = env('USER_URI');
        $uri = sprintf('%s?%s', $uri, $queryStr);

        try {
            $response = $this->client->get($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function getUser($userId)
    {
        $uri = $this->getUserUri($userId);

        try {
            $response = $this->client->get($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function getUserData($userId)
    {
        $response = $this->getUser($userId);
        $content = $response->getBody()->getContents();

        return $this->responseToArray($content);
    }

    public function getUserUri($userId)
    {
        $uri = env('USER_URI') . '/' . $userId;

        return $uri;
    }

    public function createUser($params)
    {
        $uri = env('USER_URI');
        $options = [
            'form_params' => $params
        ];
        try {
            $response = $this->client->post($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function updateUser($userId, $params)
    {
        $uri = $this->getUserUri($userId);
        $options = [
            'form_params' => $params
        ];
        try {
            $response = $this->client->put($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function deleteUser($userId)
    {
        $uri = $this->getUserUri($userId);

        try {
            $response = $this->client->delete($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function deleteMultipleUser($body)
    {
        $uri = env('USER_URI') . '/multiple';
        $options = [
            'form_params' => $body
        ];
        try {
            $response = $this->client->delete($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    /**
     * @param $userId
     * @param $body
     * @param bool $isChange . true for changePassword, otherwise for updatePassword ACTION
     *
     * @return bool
     */
    protected function _editPassword($userId, $body, $isChange = false)
    {
        $uri = $isChange ? 'users/change-password' : 'users/update-password';
        $options = [
            'form_params' => [
                //'old_password' => $body['old_password'],
                'new_password'        => $body['password'],
                'repeat_new_password' => $body['password_confirmation'],
                'user_id'             => $userId,
            ]
        ];
        if ($isChange) {
            $options['form_params']['old_password'] = $body['old_password'];
        }

        try {
            $response = $this->client->post($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function changePassword($userId, $body)
    {
        return $this->_editPassword($userId, $body, true);
    }

    public function updatePassword($userId, $body)
    {
        return $this->_editPassword($userId, $body);
    }

    public function getProfile($userId)
    {
        $uri = 'users/' . $userId;

        try {
            $response = $this->client->get($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function updateProfile($userId, $body)
    {
        $uri = 'users/' . $userId;

        $options = [
            'form_params' => $body
        ];
        try {
            $response = $this->client->put($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }
}
