<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 07-Mar-2018
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class OrderHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class OrderMultiCSRValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'odr_id'          => 'required|integer',
            'csr'          => 'required',
        ];
    }


}
