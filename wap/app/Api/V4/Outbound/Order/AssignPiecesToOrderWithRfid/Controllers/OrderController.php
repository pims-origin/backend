<?php

namespace App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Controllers;

use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models\EventTrackingModel;
use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models\OrderCartonModel;
use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models\OrderDtlModel;
use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models\OrderHdrModel;
use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models\CartonModel;
use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models\WavePickDtlModel;
use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models\WavePickHdrModel;
use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models\WaveDtlLocModel;
use App\libraries\RFIDValidate;
use App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Wavepick\UpdateWithRfid\Controllers\WavepickController;

use App\Api\OUTBOUND\Transformers\OrderListShippingTransformer;
use App\Api\OUTBOUND\Transformers\OrderListTransformer;
use App\Api\OUTBOUND\Transformers\OrderDetailTransformer;
use App\Api\OUTBOUND\Transformers\OrderByOdrIDTransformer;
use App\Api\V1\Models\Log;
use App\Jobs\AutoAssignRfidsJob;
use App\Jobs\AssignOverPickingCartonToProcessingLocation;
use App\Jobs\AutoPackJob;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;
use GuzzleHttp\Client;

class OrderController extends AbstractController
{

    protected $orderCartonModel;
    protected $orderDtlModel;
    protected $orderHdrModel;
    protected $cartonModel;
    protected $eventTrackingModel;
    protected $wavePickDtlModel;
    protected $wavePickHdrModel;
    protected $waveDtlLocModel;

    public function __construct()
    {
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->cartonModel = new CartonModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->wavePickDtlModel = new WavePickDtlModel();
        $this->wavePickHdrModel = new WavePickHdrModel();
        $this->waveDtlLocModel  = new WaveDtlLocModel();
    }

    /**
     * @param $whsId
     * @param $odrId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function putPiecesToOrder($whsId, $odrId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v4/whs/{$whsId}/order/{$odrId}/pieces-with-rfid";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'PCO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Assign Pieces to Order with RFID'
        ]);

        /*
         * end logs
         */

        $input['whs_id'] = $whsId;
        $input['odr_id'] = $odrId;

        $ctnRfid   = array_get($input, 'ctn-rfid', null);
        $pickedQty = (int)array_get($input, 'picked-qty', null);
        $pickerId = (int)array_get($input, 'picker', Data::getCurrentUserId());

        if (!$pickedQty || !$ctnRfid) {
            $msg = "Picked qty and carton RFID are required";
            return $this->_responseErrorMessage($msg);
        }

        if (!is_int($pickedQty) || $pickedQty < 1) {
            $msg = "Picked qty have to be integer and greater than 0";
            return $this->_responseErrorMessage($msg);
        }

        //validate carton RFID
        $ctns = [$ctnRfid];
        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($ctns as $item) {

            $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
            if (! $ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $item;
                if('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }
        }

        // WMS2-5622 - [BE][Outbound - Order] Auto Assign Cartons To Order
        //if has errors, return all error data and message
        // if ($ctnRfidInValid) {

        //     return $this->_responseErrorMessage($errorMsg);
        // }

        $checkOdr = $this->orderHdrModel->getModel()
            ->where('whs_id', $whsId)
            ->where('odr_id', $odrId)
            // ->whereIn('odr_sts', ['NW', 'PK'])
            ->where('odr_sts', 'PK')
            ->first();
        $odrNum = object_get($checkOdr, 'odr_num');
        if (empty($checkOdr)) {
            $errorMessage = sprintf("The order %s doesn't exist", $odrId);
            $checkOdr = $this->orderHdrModel->getFirstWhere([
                'whs_id' => $whsId,
                'odr_id' => $odrId,
            ]);

            if ($checkOdr) {
                $odrNum = object_get($checkOdr, 'odr_num');
                $odrSts = object_get($checkOdr, 'odr_sts');
                if ($odrSts == 'NW' || $odrSts == "AL") {
                    $errorMessage = sprintf("The order %s doesn't create wavepick yet", $odrNum);
                } else {
                    $errorMessage = sprintf("The order %s is %s", $odrNum, Status::getByKey('ORDER-STATUS', $odrSts));
                }
            } else {
                $checkOdr = $this->orderHdrModel->getFirstWhere([
                    'odr_id' => $odrId,
                ]);
                if ($checkOdr) {
                    $odrNum = object_get($checkOdr, 'odr_num');
                    $errorMessage = sprintf("The order %s doesn't belong to warehouse", $odrNum);
                }
            }

            return $this->_responseErrorMessage($errorMessage);
        }

        $odrDts = $this->orderDtlModel->findWhere(['odr_id' => $odrId]);
        $itemLots = [];
        if (count($odrDts)) {
            foreach ($odrDts as $odrDtl) {
                $itemLots[$odrDtl->item_id][] = $odrDtl->lot;
            }
        }
        $itemIdArr = array_keys($itemLots);
        $checkNotMatchItemLot = [];

        // check ctns exists on odr ctn table
        $ctnRFIDNotExisted = [];
        $wvId = null;
        foreach ($ctns as $ctn) {
            $checkCtn = $this->orderCartonModel->getFirstWhere([
                'ctn_rfid'   => $ctn,
                'odr_hdr_id' => null
            ]);

            if (empty($checkCtn)) {
                $ctnRFIDNotExisted[] = $ctn;
            }

            if (!$wvId) {
                $wvId = object_get($checkCtn, 'wv_hdr_id');
            }

            if ($checkCtn) {
                $itemId = $checkCtn->item_id;
                if (!in_array($itemId, $itemIdArr)) {
                    $checkNotMatchItemLot[] = $ctn;
                } else {
                    // check lot
                    $lot = $checkCtn->lot;
                    if (!in_array($lot, array_get($itemLots, $itemId))) {
                        $checkNotMatchItemLot[] = $ctn;
                    }
                }
            }
        }

        $updateWavePickFlag = false;
        if ($ctnRFIDNotExisted) {
            $ctnRFIDAssigned = [];
            foreach ($ctnRFIDNotExisted as $ctn) {
                $checkCtn = $this->orderCartonModel->getModel()
                                    ->where('ctn_rfid', $ctn)
                                    ->whereNotNull('odr_hdr_id')
                                    ->where('deleted', 0)
                                    ->first();

                if ($checkCtn) {
                    $ctnRFIDAssigned[$ctn] = object_get($checkCtn, 'odr_num');
                }
            }
            if (count($ctnRFIDAssigned) != 0) {
                $ctns = array_keys($ctnRFIDAssigned);
                $msg = sprintf("The carton %s has been assigned to order %s", implode(', ', $ctns), implode(', ', $ctnRFIDAssigned));
                return $this->_responseErrorMessage($msg);
            } else {
//                $msg = "The carton hasn't been picked.";

                $checkCarton = $this->cartonModel->getFirstwhere(['rfid' => array_first($ctnRFIDNotExisted)]);
                if (!$checkCarton) {
                    $msg = "The carton doesn't exist.";

                    return $this->_responseErrorMessage($msg);
                } else {
                    $ctnSts = object_get($checkCarton, 'ctn_sts');
                    if (!in_array($ctnSts, ['AC', 'PD', 'AJ'])) {
                        $msg = sprintf("Unable assign carton %s to Order. Current status is %s", array_first($ctnRFIDNotExisted),
                            Status::getByValue($ctnSts, 'CTN_STATUS'));

                        return $this->_responseErrorMessage($msg);
                    }
                    $updateWavePickFlag = true;

                    // WMS2-5586 - [Outbound - Order] Validate different SKU
                    $itemId = $checkCarton->item_id;
                    if (!in_array($itemId, $itemIdArr)) {
                        $checkNotMatchItemLot[] = $ctn;
                    } else {
                        // check lot
                        $lot = $checkCarton->lot;
                        if (!in_array($lot, array_get($itemLots, $itemId))) {
                            $checkNotMatchItemLot[] = $ctn;
                        }
                    }

                }
            }
        }

        if (count($checkNotMatchItemLot)) {
            $msg['message'] = [
                'status_code' => -2,
                'msg' => sprintf("The carton %s does not belong to order %s", implode(', ', $checkNotMatchItemLot), $odrNum),
            ];
            $msg['data'] = [];
            return $msg;
        }

        try {
            // start transaction
            DB::beginTransaction();

            if ($updateWavePickFlag) {
                //WAP-594 - Assign RFID pieces to Order with both status AC, AJ and PD
                $error = $this->_checkAndUpdateWavePick($request, $whsId, $odrId, $ctns);
                if ($error) {
                    return $this->_responseErrorMessage($error);
                }
            }

            if ($this->_isNotAutoAssignCartonsToOrder($wvId)) {
                dispatch(new AutoAssignRfidsJob($whsId, $wvId, $ctns, $request, $odrId, $pickedQty));
            }

            $data = [];
            // WAP 458 - Remove checking number of cartons for Assigning pieces
            // $actOrderCarton = $this->orderCartonModel->countOdrHdr($odrId);
            // if(count($ctns) + $actOrderCarton > $this->orderDtlModel->getTotalCarton($odrId))
            // {
            //     $msg = "Assigned Carton QTY is greater than the required carton QTY for this Order!";

            //     return $this->_responseErrorMessage($msg);
            // }

            $wvStsFrom = null;
            // update carton
            foreach ($ctns as $ctn) {
                // checking cross Cartons between Wave picks
                $ctnDt = $this->orderCartonModel->getFirstWhere([
                    'ctn_rfid' => $ctn,
                    'wv_hdr_id' => $wvId,
                ]);
                if (!$ctnDt) {
                    continue;
                }
                $originPieceQty = $ctnDt->piece_qty;
                if ($originPieceQty < $pickedQty) {
                    DB::rollback();

                    $msg = sprintf("Carton RFID %s is not enough pieces qty [%s] to assign [%s]!",
                            $ctn,
                            $ctnDt->piece_qty,
                            $pickedQty);

                    return $this->_responseErrorMessage($msg);
                }

                $odrDtl = $this->orderDtlModel->getFirstWhere([
                    'odr_id'  => $odrId,
                    'item_id' => $ctnDt->item_id,
                    'lot'     => $ctnDt->lot,
                ]);

                if (empty($odrDtl)) {
                    DB::rollback();

                    $msg = sprintf("Carton RFID %s doesn't belong to Order %s!",
                            $ctn,
                            object_get($checkOdr, 'odr_num'));

                    return $this->_responseErrorMessage($msg);
                }

                $odrDtlId = $odrDtl->odr_dtl_id;
                $odrNum   = $checkOdr->odr_num;
                // assign pieces carton. Create a new carton and assign to the order
                $newCarton = false;
                if ($pickedQty < $originPieceQty) {
                    // Create a new carton
                    $carton = $this->cartonModel->getFirstwhere(['ctn_id' => $ctnDt->ctn_id])->toArray();
                    $newCarton = $this->_updateAssignPieceCarton($carton, $pickedQty);
                    $ctnDt = $this->_updateOdrCartonWithPieceCarton($ctnDt, $newCarton, $carton, $odrId, $odrDtlId, $odrNum);
                }
                // in case cross cartons
                $wvNum = object_get($ctnDt, 'wv_num');

                $orderIds = $this->orderHdrModel->getOdrIdByWvNum($wvNum);

                if (!in_array($odrId, $orderIds)) {
                    $wvInfo = $this->_getWvInfoByOdrId($ctnDt, $checkOdr);
                    // cross cartons
                    $wvStsFrom = $this->_updateWvPickInCross($ctnDt, $wvInfo, $checkOdr);

                    // $this->_updateWvPickDtlLocInCross($ctnDt, $wvInfo);

                    $this->_updateOdrCartonInCrossCartonCase($ctnDt, $wvInfo);
                }

                if (!$newCarton) {
                    $dataOdr = [
                        'odr_hdr_id' => $odrId,
                        'odr_num'    => $checkOdr->odr_num,
                        'odr_dtl_id' => $odrDtlId,
                        'ctn_sts'    => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                        'updated_at' => time()
                    ];

                    $this->orderCartonModel->updateWhere($dataOdr, ['ctn_rfid' => $ctn, 'updated_by' => $pickerId]);
                }

                // WMS2-4435 - BUG - Able to assign cartons to order detail more than expected if order has more than one SKU
                $actPieceQtyOdrDtl = $this->orderCartonModel->sumPieceQtyOdrCtnByOdrDtlID($odrDtlId);
                if ($actPieceQtyOdrDtl > object_get($odrDtl, 'alloc_qty', 0)) {
                    DB::rollback();

                    $msg = sprintf("Assigned Carton QTY is greater than the required carton QTY for Order detail!");

                    return $this->_responseErrorMessage($msg);
                }
                //Update Picking for order details
                $odrDtl->itm_sts = 'PK';
                $odrDtl->save();
                $data = [
                    'result' => [
                        "item_id"   => object_get($odrDtl, 'item_id'),
                        "sku"       => object_get($odrDtl, 'sku'),
                        "size"      => object_get($odrDtl, 'size'),
                        "color"     => object_get($odrDtl, 'color'),
                        "lot"       => object_get($odrDtl, 'lot'),
                        "pack"      => object_get($odrDtl, 'pack'),
                        "sts"       => true,
                    ]
                ];
                break;
            }

            //Update Picking for order header
            $checkOdr->odr_sts = 'PK';
            $checkOdr->save();

            // WMS2-4636 - Update picked_qty of order detail
            $this->orderDtlModel->updateOrderDetailPickedQty($odrId);

            //update odr_sts to PD
            $dtlPicked = $this->orderDtlModel->updateOrderDetailPicked($odrId);
            if ($dtlPicked) {
                $evtTracking = [
                    'whs_id' => $whsId,
                    'cus_id' => $checkOdr->cus_id,
                    'owner' => $checkOdr->odr_num,
                    'evt_code' => 'WOD',
                    'trans_num' => $checkOdr->odr_num,
                    'info' => sprintf('Wap - Assigning Carton(s) to Order Details %s - %s - %s - %s Completed', $ctnDt->sku, $ctnDt->size, $ctnDt->color, $ctnDt->lot),
                    'created_by' => $pickerId,
                    'created_at' => time()
                ];
                DB::table('evt_tracking')->insert($evtTracking);
            }

            $hdrPicked = $this->orderHdrModel->updateOrderPicked($odrId);
            if ($hdrPicked) {
                $evtTracking = [
                    'whs_id' => $whsId,
                    'cus_id' => $checkOdr->cus_id,
                    'owner' => $checkOdr->odr_num,
                    'evt_code' => 'WOH',
                    'trans_num' => $checkOdr->odr_num,
                    'info' => "Wap - Assigning Carton(s) to Order Completed",
                    'created_by' => $pickerId,
                    'created_at' => time()
                ];
                DB::table('evt_tracking')->insert($evtTracking);
            }

            // update wave pick is picking when status is new
            $this->wavePickHdrModel->getModel()
                ->where('wv_id', $checkOdr->wv_id)
                ->where('wv_sts', 'NW')
                ->update(['wv_sts'=> 'PK', 'updated_by' => $pickerId]);

            // update wave pick completed
            $wvId = $checkOdr->wv_id;
            $wvSts = $this->wavePickHdrModel->updatePicked($wvId, $pickerId);
            // set status of both two wave pick header
            $dataWvSts[] = [
                'wv_id'  => $wvId,
                'picked' => $wvSts,
            ];
            if ($wvStsFrom) {
                $dataWvSts[] = $wvStsFrom;
            }

            DB::commit();

            $odrHdr = $this->orderHdrModel->getModel()
                            ->where('whs_id', $whsId)
                            ->where('odr_id', $odrId)
                            ->first();

            if ( $this->_isAutoAssignCartonsToOrder($odrHdr->wv_id) && $this->_isOverpicking($odrHdr->wv_id) &&
                 $odrHdr->odr_sts === 'PD' ) {
                dispatch(new AssignOverPickingCartonToProcessingLocation($odrId));
            }

            //Call Auto Pack Job if pick successful
            if ($wvSts == 1) {
                dispatch(new AutoPackJob($wvId, $request));
            }

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => sprintf("Successfully!")
            ]];
            $data['wv_sts'] = $dataWvSts;
            $msg['data']   = [$data];

            return $msg;

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return $this->_responseErrorMessage($this->getResponseData());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _updateWvPickInCross($odrCtnFrom, $wvInfo, $odrObj)
    {
        $wvHdrIdFrom = object_get($odrCtnFrom, 'wv_hdr_id');
        $wvHdrIdTo   = array_get($wvInfo, 'wv_hdr_id');

        $wvDtlIdFrom = object_get($odrCtnFrom, 'wv_dtl_id');
        $wvDtlIdTo   = array_get($wvInfo, 'wv_dtl_id');

        $wvPieceQtyTo = $wvPieceQtyFrom = object_get($odrCtnFrom, 'piece_qty');

        $wvDtlFrom = $this->wavePickDtlModel->getFirstWhere(['wv_dtl_id' => $wvDtlIdFrom]);
        $wvDtlTo   = $this->wavePickDtlModel->getFirstWhere(['wv_dtl_id' => $wvDtlIdTo]);

        if ($wvDtlFrom->act_piece_qty < $wvPieceQtyFrom) {
            $msg = "Wave pick actual piece QTY [from] is not enough!";
            throw new \Exception($msg);
        }

        if (object_get($wvDtlTo, 'pack_size') != object_get($odrCtnFrom, 'pack')) {
            $msg = sprintf(
                'The Carton RFID %s pack size %d is not same pack size %d of Order %s!',
                object_get($odrCtnFrom, 'ctn_rfid'),
                object_get($odrCtnFrom, 'pack'),
                object_get($wvDtlTo, 'pack_size'),
                object_get($odrObj, 'odr_num')
            );
            throw new \Exception($msg);
        }

        $userId = Data::getCurrentUserId();
        // wave pick [from]
        $wvDtlFrom->act_piece_qty -= $wvPieceQtyFrom;
        $wvDtlFrom->wv_dtl_sts = $wvDtlFrom->act_piece_qty >= $wvDtlFrom->piece_qty ? "PD" : "PK";
        $wvDtlFrom->updated_at = time();
        $wvDtlFrom->updated_by = $userId;

        $availChange     = 0;
        $allocatedChange = 0;
        // $remainingQty    = 0;
        // update wave pick [data]
        $wvDataArrFrom = json_decode($wvDtlFrom->data, true);
        if (!$wvDataArrFrom) {
            $wvDataArrFrom['avail']     = 0;
            $wvDataArrFrom['allocated'] = 0;
        }
        // over picking
        if ($wvDataArrFrom['avail'] > 0 || $wvDataArrFrom['allocated'] > 0) {
            // return available qty
            if ($wvDataArrFrom['avail'] > $wvPieceQtyFrom) {
                $availChange = $wvPieceQtyFrom;
            } elseif ($wvDataArrFrom['avail'] + $wvDataArrFrom['allocated']> $wvPieceQtyFrom) {
                $availChange     = $wvDataArrFrom['avail'];
                $allocatedChange = $wvPieceQtyFrom - $wvDataArrFrom['avail'];
            } else { // return over picking
                $allocatedChange = $wvDataArrFrom['allocated'];
                $availChange     = $wvDataArrFrom['avail'];
                // $remainingQty    = $wvPieceQtyFrom - ($allocatedChange + $availChange);
            }
        }

        $wvDataArrFrom['avail']     -= $availChange;
        $wvDataArrFrom['allocated'] -= $allocatedChange;
        $wvDtlFrom->data = json_encode($wvDataArrFrom);
        $wvDtlFrom->update();

        // wave pick [to]
        // over picking
        // update wave pick [data]
        $wvDataArrTo = json_decode($wvDtlTo->data, true);
        if (!$wvDataArrTo) {
            $wvDataArrTo['avail']     = 0;
            $wvDataArrTo['allocated'] = 0;
        }
        if ($wvDtlTo->act_piece_qty + $wvPieceQtyTo > $wvDtlTo->piece_qty) {
            // exist over picking
            if ($wvDataArrTo['avail'] > 0 || $wvDataArrTo['allocated'] > 0) {
                // $remainingQty == 0
                if ($availChange + $allocatedChange == $wvPieceQtyTo) {
                    $wvDataArrTo['avail']     += $availChange;
                    $wvDataArrTo['allocated'] += $allocatedChange;
                } else {
                    $wvDataArrTo['avail']     += $availChange;
                    $wvDataArrTo['allocated'] += $wvPieceQtyTo - $availChange;
                }
            } else { // a half over picking
                $overPicking = $wvDtlTo->act_piece_qty + $wvPieceQtyTo - $wvDtlTo->piece_qty;
                if ($overPicking > $availChange) {
                    $wvDataArrTo['avail']     = $availChange;
                    $wvDataArrTo['allocated'] = $overPicking - $allocatedChange;
                } else {
                    $wvDataArrTo['avail']     = $overPicking;
                    $wvDataArrTo['allocated'] = 0;
                }
            }
        }

        $wvDtlTo->data = json_encode($wvDataArrTo);
        $wvDtlTo->act_piece_qty += $wvPieceQtyTo;
        $wvDtlTo->wv_dtl_sts = $wvDtlTo->act_piece_qty >= $wvDtlTo->piece_qty ? "PD" : "PK";
        $wvDtlTo->updated_at = time();
        $wvDtlTo->updated_by = $userId;

        $wvDtlTo->save();

        //update wave header status
        $wvSts = $this->wavePickHdrModel->updatePicked($wvHdrIdFrom);

        return [
            'wv_id'  => $wvHdrIdFrom,
            'picked' => $wvSts,
        ];
    }

    private function _updateOdrCartonInCrossCartonCase($odrCtnFrom, $wvInfo)
    {
        $wvNumTo   = array_get($wvInfo, 'wv_num');
        $wvHdrIdTo = array_get($wvInfo, 'wv_hdr_id');
        $wvDtlIdTo = array_get($wvInfo, 'wv_dtl_id');

        $odrCtnFrom->wv_num    = $wvNumTo;
        $odrCtnFrom->wv_hdr_id = $wvHdrIdTo;
        $odrCtnFrom->wv_dtl_id = $wvDtlIdTo;
        $odrCtnFrom->updated_at = time();
        $odrCtnFrom->updated_by = Data::getCurrentUserId();
        $odrCtnFrom->save();
    }

    private function _getWvInfoByOdrId($odrCtnObj, $odrHdrObj)
    {
        $wvId   = object_get($odrHdrObj, 'wv_id');
        $wvNum  = object_get($odrHdrObj, 'wv_num');
        $itemId = object_get($odrCtnObj, 'item_id');

        $wvDtlObj = $this->wavePickDtlModel->getFirstWhere([
                                            "item_id" => $itemId,
                                            "wv_id"   => $wvId,
                                            ]);
        $wvDtlId = object_get($wvDtlObj, 'wv_dtl_id');

        if (!$wvDtlId) {
            $msg = sprintf("Carton RFID %s doesn't belong to Order %s!", object_get($odrCtnObj, 'ctn_rfid'), object_get($odrHdrObj, 'odr_num'));
            if (!$wvId) {
                $msg = sprintf("The Order %s need to be created wave pick before this step!", object_get($odrHdrObj, 'odr_num'));
            }
            throw new \Exception($msg);
        }

        return [
            "wv_hdr_id" => $wvId,
            "wv_dtl_id" => $wvDtlId,
            "wv_num"    => $wvNum,
        ];
    }

    private function _updateWvPickDtlLocInCross($odrCtnFrom, $wvInfo)
    {
        $wvDtlIdFrom = array_get($odrCtnFrom, 'wv_dtl_id');
        $ctnId       = array_get($odrCtnFrom, 'ctn_id');
        $pieceQty    = array_get($odrCtnFrom, 'piece_qty');
        $wvDtlIdTo   = array_get($wvInfo, 'wv_dtl_id');

        $wvDtlLocFrom = $this->waveDtlLocModel->getFirstwhere(['wv_dtl_id'=> $wvDtlIdFrom]);
        $wvDtlLocTo   = $this->waveDtlLocModel->getFirstwhere(['wv_dtl_id'=> $wvDtlIdTo]);

        $changeActLocData = null;
        $changeCartonData = null;
        if ($wvDtlLocFrom && $wvDtlLocFrom->act_loc_ids) {
            $actLocs = json_decode($wvDtlLocFrom->act_loc_ids, true);
            foreach ($actLocs as $keyLoc => $actLoc) {
                foreach ($actLoc['cartons'] as $keyCtn => $cartonOld) {
                    if ($cartonOld['ctn_id'] == $ctnId) {
                        $changeActLocData = $actLoc;
                        $changeCartonData = $cartonOld;
                        // remove carton
                        unset($actLocs[$keyLoc]['cartons'][$keyCtn]);
                        // update location
                        $actLocs[$keyLoc]['picked_qty'] -= $pieceQty;
                        break;
                    }
                }
                // update carton
                $actLocs[$keyLoc]['cartons'] = array_values($actLocs[$keyLoc]['cartons']);
                // update location
                if (count($actLocs[$keyLoc]['cartons']) == 0) {
                    // remove location
                    unset($actLocs[$keyLoc]);
                }
            }

            $wvDtlLocFrom->act_loc_ids = json_encode(array_values($actLocs));

            $wvDtlLocFrom->update();
        }

        if (!$changeActLocData || !$changeCartonData) {
            $msg = sprintf("Not found carton RFID %s in any location!", array_get($odrCtnFrom, 'ctn_id'));
            throw new \Exception($msg);
        }

        if ($wvDtlLocTo) {
            if ($wvDtlLocTo->act_loc_ids) {
                $actLocs = json_decode($wvDtlLocTo->act_loc_ids, true);
                $update = false;
                foreach ($actLocs as $keyLoc => $actLoc) {
                    if ($actLoc['loc_id'] == $changeActLocData['loc_id']) {
                        $update = true;
                        // update loc
                        $actLocs[$keyLoc]['cartons'][]   = $changeCartonData;
                        $actLocs[$keyLoc]['picked_qty'] += $pieceQty;
                        break;
                    }
                }

                if (!$update) {
                    $changeActLocData['cartons']    = [$changeCartonData];
                    $changeActLocData['picked_qty'] = $pieceQty;
                    $actLocs[] = $changeActLocData;
                }
                $wvDtlLocTo->act_loc_ids = json_encode($actLocs);
            } else {

                $changeActLocData['cartons']    = [$changeCartonData];
                $changeActLocData['picked_qty'] = $pieceQty;

                $wvDtlLocTo->act_loc_ids = json_encode([$changeActLocData]);
            }

            $wvDtlLocTo->update();
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _updateAssignPieceCarton($carton, $pickQty)
    {
        $remainQty = $carton['piece_remain'] - $pickQty;
        $res = DB::table('cartons')->where('ctn_id', $carton['ctn_id'])->update(
            ['piece_remain' => $remainQty]
        );

        //change business clone new carton
        $origCtnId = $carton['ctn_id'];
        $origLocId = $carton['loc_id'];
        $maxCtnNum = $this->cartonModel->getMaxCtnNum($carton['ctn_num']);
        unset($carton['ctn_id']);

        // $carton['inner_pack'] = 0;

        $newCarton = $this->cartonModel->getModel()->fill($carton);
        $newCarton->rfid         = null;
        $newCarton->piece_remain = $pickQty;
        $newCarton->origin_id    = $origCtnId;
        $newCarton->loc_id       = $newCarton->loc_code = null;
        $newCarton->ctn_sts      = 'PD';
        $newCarton->picked_dt    = time();
        $newCarton->ctn_num      = ++$maxCtnNum;
        $newCarton->save();

        $return = $newCarton->toArray();
        $return['pick_piece'] = true;
        $return['loc_id']     = $origLocId;
        $return['loc_code']   = $return['loc_name'];
        $return["picked_qty"] = $pickQty;

        return $return;
    }

    private function _updateOdrCartonWithPieceCarton(
        $odrCarton,
        $newCarton,
        $oldCarton,
        $odrId,
        $odrDtlId,
        $odrNum,
        $item = null
    )
    {
        $pieceRemain = array_get($oldCarton, 'piece_remain') - array_get($newCarton, 'piece_remain');

        $newOdrCarton = $odrCarton->toArray();
        unset($newOdrCarton['odr_ctn_id']);
        $newOdrCarton['ctn_rfid']   = null;
        $newOdrCarton['piece_qty']  = $newCarton['piece_remain'];
        $newOdrCarton['ctn_id']     = $newCarton['ctn_id'];
        $newOdrCarton['ctn_num']    = $newCarton['ctn_num'];
        $newOdrCarton['odr_hdr_id'] = $odrId;
        $newOdrCarton['odr_dtl_id'] = $odrDtlId;
        $newOdrCarton['odr_num']    = $odrNum;
        if ($item) {
            $newOdrCarton['wv_num']    = array_get($item, 'wv_num');
            $newOdrCarton['wv_hdr_id'] = array_get($item, 'wv_hdr_id');
            $newOdrCarton['wv_dtl_id'] = array_get($item, 'wv_dtl_id');
        }

        $odrCtnId = DB::table('odr_cartons')->insertGetId($newOdrCarton);

        // update old order cartons
        $odrCarton->piece_qty = $pieceRemain;
        $odrCarton->ctn_id    = array_get($oldCarton, 'ctn_id');
        $odrCarton->ctn_num   = array_get($oldCarton, 'ctn_num');

        $odrCarton->save();

        return $this->orderCartonModel->getFirstWhere(['odr_ctn_id' => $odrCtnId]);
    }


    private function _checkAndUpdateWavePick($request, $whsId, $odrId, $ctnRfids)
    {
        //check cartons that did not be updated wave pick yet
        $cartons = $this->cartonModel->getModel()
            ->whereIn('rfid', $ctnRfids)
            ->whereIn('ctn_sts', ['AC', 'AJ'])
            ->pluck('rfid')->toArray();

        if (count($cartons)) {
            // $wavePickController = new WavepickController();
            $odrObj = $this->orderHdrModel->getFirstWhere(['odr_id' => $odrId]);
            $wvId = object_get($odrObj, 'wv_id');
            if (!$wvId) {
                $msg = sprintf("The Order %s hasn't created wavepick yet.", object_get($odrObj, 'odr_num'));
                return $msg;
            }
            // $data = [
            //     'code' => [
            //         "pallet" => null,
            //         "ctns"   => $cartons,
            //     ]
            // ];
            // $result = $wavePickController->updateWavePick($request, $whsId, $wvId, $data);
            // return $result;

            // WAP-809 - [Wavepick] Modify auto update wavepick when assign cartons AC, AJ to Order using update wavepick v6
            try{

                $client = new Client();
                $data = [
                    'code' => [
                        "pallet" => '',
                        "ctns"   => $cartons,
                    ]
                ];
                $response = $client->request('PUT', sprintf('%s/v6/whs/%s/wave/%s/update', env('API_WAP'), $whsId, $wvId),
                    [
                        'headers'     => ['Authorization' => $request->getHeader('Authorization')],
                        'form_params' => $data
                    ]
                );
            }catch (\Exception $exception) {
                $data = [
                    'date'       => date("Ymd"),
                    'api_name'   => 'Auto update Wavepick',
                    'error'      => $exception->getMessage(),
                    'created_at' => time(),
                    'created_by' => Data::getCurrentUserId(),
                    'updated_at' => time(),
                ];
                // DB::table('sys_bugs')->where('id', 5 )->update(['error' => $exception->getMessage()]);
                DB::table('sys_bugs')->insert([$data]);
            }
        }
    }

    private function _isNotAutoAssignCartonsToOrder($wvId)
    {
        $countOdr = $this->orderHdrModel->getModel()
            ->where('wv_id', $wvId)
            ->whereNull('org_odr_id')
            ->count();

        if ($countOdr > 1) {
            return true;
        }
        return false;
    }

    private function _isOverpicking($wvId)
    {
        $countOverpick = DB::table('wv_dtl')
            ->where('wv_id', $wvId)
            ->where('piece_qty', '<', 'act_piece_qty')
            ->where('deleted', 0);

        if ($countOverpick) {
            return true;
        }
        return false;
    }

    private function _isAutoAssignCartonsToOrder($wvId)
    {
        $countOdr = $this->orderHdrModel->getModel()
            ->where('wv_id', $wvId)
            ->whereNull('org_odr_id')
            ->count();

        if ($countOdr <= 1) {
            return true;
        }
        return false;
    }
}