<?php
//pt:

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WavepickDtl;

class WvHistorySkuTransformer extends TransformerAbstract
{
    //return result
    public function transform(WavepickDtl $wvDtl)
    {
        $piecesQty = object_get($wvDtl, 'piece_qty', 0);
        $actPiecesQty = object_get($wvDtl, 'act_piece_qty', 0);
        return [
            'sku'   => object_get($wvDtl, 'sku'),
            'size'   => object_get($wvDtl, 'size'),
            'color'   => object_get($wvDtl, 'color'),
            'alloc_pieces'   => $piecesQty .'/'. $actPiecesQty,
        ];
    }

}
