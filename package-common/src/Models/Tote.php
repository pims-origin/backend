<?php

namespace Seldat\Wms2\Models;

class Tote extends BaseSoftModel
{
    protected $table = 'tote';

    protected $primaryKey = 'tote_id';

    protected $fillable = [
        'whs_id',
        'wv_id',
        'picker_id',
        'tote_num',
        'ctn_ttl',
        'tote_sts'
    ];

    public function odrHdrs()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\OrderHdr', 'odr_cartons', 'tote_id', 'odr_hdr_id')
                    ->groupBy('odr_cartons.odr_hdr_id');
    }

    public function wvHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\WavepickHdr', 'wv_id', 'wv_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function picker()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'picker_id');
    }

    public function updatedBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'updated_by');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }
}