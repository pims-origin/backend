<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:19
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerAddress;
use League\Fractal\TransformerAbstract;

class CustomerAddressTransformer extends TransformerAbstract
{
    /**
     * @param CustomerAddress $customerAddress
     *
     * @return array
     */
    public function transform(CustomerAddress $customerAddress)
    {
        return [
            'cus_add_id'          => $customerAddress->cus_add_id,
            'cus_add_line_1'      => $customerAddress->cus_add_line_1,
            'cus_add_line_2'      => $customerAddress->cus_add_line_2,
            'cus_add_country_id'  => $customerAddress->cus_add_country_id,
            'cus_add_city_name'   => $customerAddress->cus_add_city_name,
            'cus_add_state_id'    => $customerAddress->cus_add_state_id,
            'cus_add_postal_code' => $customerAddress->cus_add_postal_code,
            'cus_add_cus_id'      => $customerAddress->cus_add_cus_id,
            'cus_add_type'        => $customerAddress->cus_add_type
        ];
    }
}
