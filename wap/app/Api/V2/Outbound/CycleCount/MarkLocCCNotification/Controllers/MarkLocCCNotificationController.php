<?php

namespace App\Api\V2\Outbound\CycleCount\MarkLocCCNotification\Controllers;

use App\Api\V2\Outbound\CycleCount\MarkLocCCNotification\Models\CartonModel;
use App\Api\V2\Outbound\CycleCount\MarkLocCCNotification\Models\WavePickDtlModel;
use App\Api\V2\Outbound\CycleCount\MarkLocCCNotification\Models\LocationModel;
use App\Api\V2\Outbound\CycleCount\MarkLocCCNotification\Models\CycleCountNotificationModel;

use App\Api\OUTBOUND\Transformers\OrderListShippingTransformer;
use App\Api\OUTBOUND\Transformers\OrderListTransformer;
use App\Api\OUTBOUND\Transformers\OrderDetailTransformer;
use App\Api\OUTBOUND\Transformers\OrderByOdrIDTransformer;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Dompdf\Exception;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;
use GuzzleHttp\Client;

class MarkLocCCNotificationController extends AbstractController
{

    protected $wavePickDtlModel;
    protected $locationModel;
    protected $cartonModel;
    protected $ccNotificationModel;
    protected $url;

    public function __construct()
    {
        $this->wavePickDtlModel    = new WavePickDtlModel();
        $this->locationModel       = new LocationModel();
        $this->cartonModel         = new CartonModel();
        $this->ccNotificationModel = new CycleCountNotificationModel();
        $this->url = env('API_CYCLE_COUNT')."cycle-count";
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function store1($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/ob/mark-location-cycle-count";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'CCL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Mark cycle count to location'
        ]);

        /*
         * end logs
         */

        $wvDtlId = array_get($input, 'wv_dtl_id');
        $locId   = array_get($input, 'loc_id');
        $reason  = array_get($input, 'reason.r_name', 'WAP request');

        if (!$wvDtlId || !$locId || !$reason) {
            $msg = "Wave pick ID, Location ID and reason are required.";

            return $this->_responseErrorMessage($msg);
        }

        $wvDtlObj = $this->wavePickDtlModel->getFirstWhere([
                        'whs_id'    => $whsId,
                        'wv_dtl_id' => $wvDtlId,
                    ]);

        if (!$wvDtlObj) {
            $msg = sprintf("The Wave pick ID %s doesn't existed.", $wvDtlId);
            $wvDtlObj = $this->wavePickDtlModel->getFirstWhere([
                        'wv_dtl_id' => $wvDtlId,
                    ]);
            if ($wvDtlObj) {
                $msg = sprintf("The Wave pick number %s doesn't belong to current warehouse.", object_get($wvDtlObj, 'wv_num'));
            }
            return $this->_responseErrorMessage($msg);
        }

        $locationObj = $this->locationModel->getFirstWhere([
                            'loc_whs_id' => $whsId,
                            'loc_id'     => $locId,
                        ]);

        if (!$locationObj) {
            $msg = sprintf("The Location ID %s doesn't existed.", $locId);
            $locationObj = $this->locationModel->getFirstWhere([
                            'loc_id' => $locId,
                        ]);
            if ($locationObj) {
                $msg = sprintf("The Location code %s doesn't belong to current warehouse.", object_get($locationObj, 'loc_code'));
            }
            return $this->_responseErrorMessage($msg);
        }

        $locCode = object_get($locationObj, 'loc_code');

        if ($locationObj->loc_sts_code != 'AC') {
            $msg = sprintf("The Location %s is not ACTIVE. The status is %s",
                            $locCode,
                            Status::getByValue($locationObj->loc_sts_code, 'LOCATION_STATUS')
                        );

            return $this->_responseErrorMessage($msg);
        }

        $isExistCCN = $this->ccNotificationModel->getFirstWhere([
                            'whs_id'     => $whsId,
                            'loc_id'     => $locId,
                            'cc_ntf_sts' => 'NW',
                        ]);

        if ($isExistCCN) {
            $msg = sprintf("The Location code %s was marked cycle count.", $locCode);

            return $this->_responseErrorMessage($msg);
        }

        try {

            $itemId = object_get($wvDtlObj, 'item_id');
            $sku    = object_get($wvDtlObj, 'sku');
            $pack   = object_get($wvDtlObj, 'pack_size');
            $lot    = object_get($wvDtlObj, 'lot');
            $cusId  = object_get($wvDtlObj, 'cus_id');

            $cartonRemain = DB::table('cartons')->where('whs_id', $whsId)
                                ->select([
                                    DB::raw('sum(piece_remain) as remain_qty'),
                                    'uom_name',
                                    'uom_code',
                                    'des',
                                ])
                                ->where('loc_id', $locId)
                                ->where('item_id', $itemId)
                                ->where('ctn_pack_size', $pack)
                                ->where('lot', $lot)
                                ->where('deleted', 0)
                                ->get();

            $cartonRemain = array_first($cartonRemain);
            if (!array_get($cartonRemain, 'remain_qty')) {
                $msg = sprintf("There are no cartons: SKU %s, Pack %s, Lot %s on the location %s.", $sku, $pack, $lot, $locCode);

                return $this->_responseErrorMessage($msg);
            }

            $userId = Data::getCurrentUserId();
            $dataCCN = [
                'whs_id'      => $whsId,
                'loc_id'      => $locId,
                'loc_code'    => $locCode,
                'cc_ntf_sts'  => 'NW',
                'cc_ntf_date' => time(),
                'reason'      => $reason,
                'item_id'     => object_get($wvDtlObj, 'item_id'),
                'sku'         => object_get($wvDtlObj, 'sku'),
                'lot'         => $lot,
                'pack'        => $pack,
                'size'        => object_get($wvDtlObj, 'size'),
                'color'       => object_get($wvDtlObj, 'color'),
                'cus_id'      => object_get($wvDtlObj, 'cus_id'),
                'uom_code'    => array_get($cartonRemain, 'uom_code'),
                'uom_name'    => array_get($cartonRemain, 'uom_name'),
                'remain_qty'  => array_get($cartonRemain, 'remain_qty'),
                'des'         => array_get($cartonRemain, 'des'),
                'created_at'  => time(),
                'created_by'  => $userId,
                'updated_at'  => time(),
                'updated_by'  => $userId,
                'deleted_at'  => 915148800,
                'deleted'     => 0,
            ];

            DB::beginTransaction();

            DB::table('cc_notification')->insert($dataCCN);

            // WMS2-5395 - [Cycle Count] Update Cycle Count Notification
            // Lock location and cartons
            DB::table('cartons')->where('loc_id', $locId)
                ->where('deleted', 0)
                ->where('ctn_sts', 'AC')
                ->where('item_id', $itemId)
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId)
                ->where('lot', $lot)
                ->update([
                    'ctn_sts'    => 'LK',
                    'updated_at' => time(),
                    'updated_by' => $userId,
                ]);
            DB::table('location')->where('loc_id', $locId)
                ->where('deleted', 0)
                ->update([
                    'loc_sts_code' => 'LK',
                    'updated_at'   => time(),
                    'updated_by'   => $userId,
                ]);

            $lockQty = DB::table('cartons')
                ->where('deleted', 0)
                ->where('ctn_sts', 'LK')
                ->where('item_id', $itemId)
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId)
                ->where('lot', $lot)
                //->where('is_damaged', 0)
                ->sum('piece_remain');

            DB::table('invt_smr')
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId)
                ->where('item_id', $itemId)
                ->where('lot', $lot)
                ->update(['lock_qty' => $lockQty]);

            DB::commit();

            // $client = new Client();

            // $url = sprintf('%sccn/%s/save', env('API_CYCLE_COUNT'), $whsId);

            // $response = $client->request('POST', $url,
            //     [
            //         'headers'     => ['Authorization' => $request->getHeader('Authorization')],
            //         'form_params' => [$dataCCN],
            //     ]
            // );

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "The location has been marked cycle count successfully!"
            ]];
            $msg['data']   = [];

            return $msg;

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    public function store(Request $request, $whsId) {
        $input = $request->getParsedBody();
        $url = "/v2/whs/{$whsId}/ob/mark-location-cycle-count";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'CCL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Mark cycle count to location'
        ]);

        /*
         * end logs
         */

//        $wvDtlId = array_get($input, 'wv_dtl_id');
        $locId   = array_get($input, 'loc_id');
//        $reason  = array_get($input, 'reason.r_name', 'WAP request');

        if (!$locId) {
            $msg = "Location ID are required.";

            return $this->_responseErrorMessage($msg);
        }

//        $wvDtlObj = $this->wavePickDtlModel->getFirstWhere([
//            'whs_id'    => $whsId,
//            'wv_dtl_id' => $wvDtlId,
//        ]);
//
//        if (!$wvDtlObj) {
//            $msg = sprintf("The Wave pick ID %s doesn't existed.", $wvDtlId);
//            $wvDtlObj = $this->wavePickDtlModel->getFirstWhere([
//                'wv_dtl_id' => $wvDtlId,
//            ]);
//            if ($wvDtlObj) {
//                $msg = sprintf("The Wave pick number %s doesn't belong to current warehouse.", object_get($wvDtlObj, 'wv_num'));
//            }
//            return $this->_responseErrorMessage($msg);
//        }


        $locationObj = $this->locationModel->getFirstWhere([
            'loc_whs_id' => $whsId,
            'loc_id'     => $locId,
        ]);

        if (!$locationObj) {
            $msg = sprintf("The Location ID %s doesn't existed.", $locId);
            $locationObj = $this->locationModel->getFirstWhere([
                'loc_id' => $locId,
            ]);
            if ($locationObj) {
                $msg = sprintf("The Location code %s doesn't belong to current warehouse.", object_get($locationObj, 'loc_code'));
            }
            return $this->_responseErrorMessage($msg);
        }

        $locCode = object_get($locationObj, 'loc_code');
        if ($locationObj->loc_sts_code != 'AC') {
            $msg = sprintf("The Location %s is not ACTIVE. The status is %s",
                $locCode,
                Status::getByValue($locationObj->loc_sts_code, 'LOCATION_STATUS')
            );

            return $this->_responseErrorMessage($msg);
        }


        try {
            $ccn_cycle_exit = DB::table("cycle_hdr")->where("whs_id",$whsId)
                ->where("cycle_sts","<>","CC")
                ->where("cycle_sts","<>","CP")
                ->where("cycle_type","CCN")
                ->where("deleted",0)
                ->first();
            if(!empty($ccn_cycle_exit)) {
                $items = DB::table("cartons")->where("loc_id",$locId)
                    ->where("ctn_sts","AC")
                    ->where("deleted",0)
                    ->groupBy("item_id","lot","piece_remain")
                    ->select(DB::raw("item_id, lot, piece_remain, sku, whs_id, cus_id, COUNT(1) as ctns"))->get();
                if(empty($items)) {
                    return("Item not exit");
                }
                foreach ($items as $item) {

                    $data = [
                        "cus_id" =>  $item['cus_id'],
                        "whs_id" =>$whsId,
                        "sku"    => $item['sku'],
                        "cycle_hdr_id" => $ccn_cycle_exit["cycle_hdr_id"],
                        "cycle_type" => "LC",
                        "lot"       => $item['lot'],
                        "item_id"   => $item['item_id'],
                        "remain"    => $item['piece_remain'],
                        "act_qty"   => 0,
                        "sys_qty"   => $item['ctns'],
                        "sys_loc_id" => $locId,
                        "sys_loc_name"=> $locCode,
                        "act_loc_id"   => $locId,
                        "plt_rfid"  => ""
                    ];
                    $client = new Client();
                    $conn = $client->request('POST', $this->url."/".$ccn_cycle_exit["cycle_hdr_id"]."/add-sku-cycle",[
                        'headers'     => ['Authorization' => $request->getHeader('Authorization')],
                        'form_params' => $data
                    ]);
                    \GuzzleHttp\json_decode($conn->getBody()->getContents());
                }

            } else {
                $assign_to = 1;
                $wms_data = DB::table("whs_meta")->where("whs_qualifier","dfu")->where("whs_id", $whsId)->first();
                if(!empty($wms_data)) {
                    $assign_to = $wms_data['whs_meta_value'];
                }
                $data = [
                    "cycle_assign_to"=> $assign_to,
                    "cycle_des"=> "",
                    "cycle_detail"=> $locId,
                    "cycle_due_date"=> date("m/d/Y"),
                    "cycle_has_color_size"=> "true",
                    "cycle_type"=> "LC",
                    "cycle_method"=> "paper",
                    "cycle_count_by"=> "carton",
                    "cycle_name"=> "ccn".date("Y").date("m").date('d'),
                    "whs_id"=> $whsId
                ];
                $client = new Client();
                $conn = $client->request('POST', $this->url,[
                    'headers'     => ['Authorization' => $request->getHeader('Authorization')],
                    'form_params' => $data
                ]);

                $result = \GuzzleHttp\json_decode($conn->getBody()->getContents());

                $cycle_id = $result->data->cycle_hdr_id;

                DB::table("cycle_hdr")->where("cycle_hdr_id",$cycle_id)->update(["cycle_type"=>"CCN"]);
            }
            return("Create Cycle Count Successfull");


        } catch (RequestException $re) {
            throw $re;
        }
        catch (Exception $e) {
            throw $e;
        }
    }


}