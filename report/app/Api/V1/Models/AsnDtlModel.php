<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class AsnDtlModel extends AbstractModel
{

    protected $asnHdr;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnDtl();
        $this->asnHdr = new AsnHdr();
    }
}
