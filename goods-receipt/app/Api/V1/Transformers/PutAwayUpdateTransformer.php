<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GoodsReceipt;


class PutAwayUpdateTransformer extends TransformerAbstract
{

    public function transform(GoodsReceipt $goodsReceipt)
    {
        return [
            'gr_hdr_id' => object_get($goodsReceipt, 'gr_hdr_id', null)
        ];
    }
}
