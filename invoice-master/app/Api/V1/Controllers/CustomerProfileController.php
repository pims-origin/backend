<?php
/**
 * Created by PhpStorm.
 * User: duong.tran
 * Date: 1/25/2018
 * Time: 3:25 PM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerMetaModel;
use Psr\Http\Message\ServerRequestInterface;
use  Psr\Http\Message\ServerRequestInterface as Request;

class CustomerProfileController extends AbstractController
{

    protected $customerMetaModel;

    public function __construct()
    {
        $this->customerMetaModel = new CustomerMetaModel();
    }

    /**
     * Insert currency by warehouse to CusMeta or Update if existed
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     * */
    public function updateCurrencyByWarehouse(ServerRequestInterface $request)
    {
        try {
            $input = $request->getParsedBody();
            $cusId = $input['cus_id'];
            //check existed
            $cusMeta = $this->customerMetaModel->getFirstWhere([
                'cus_id'    => $cusId,
                'qualifier' => 'CUR'
            ]);
            $cusMetaData = json_encode([
                $input['whs_id'] => [
                    'whs_id'   => $input['whs_id'],
                    'currency' => $input['currency'],
                ]
            ]);
            if (!empty($cusMeta)) {
                $isEditable = $this->customerMetaModel->checkCurrencyEditableOrNot($cusId);
                if ($isEditable) {
                    //Update CusMeta
                    $value = json_decode($cusMeta->value, true);
                    $value[$input['whs_id']] = [
                        'whs_id'   => $input['whs_id'],
                        'currency' => $input['currency'],
                    ];
                    $this->customerMetaModel->updateCurrencyByCusId($cusId, $value);
                }
            } else {
                //Create CusMeta
                $this->customerMetaModel->create([
                    'cus_id'    => $cusId,
                    'value'     => $cusMetaData,
                    'qualifier' => 'CUR',
                ]);
            }

            return $this->response->noContent();
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getCurrencyByWarehouse($cusId)
    {
        return [
            'currency' => $this->customerMetaModel->getCurrencyByCurrentWhs($cusId),
            'editable' => $this->customerMetaModel->checkCurrencyEditableOrNot($cusId),
            'tax'      => $this->customerMetaModel->getTaxByWarehouse($cusId)
        ];
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function updateTaxByWarehouse(ServerRequestInterface $request)
    {
        try {
            $input = $request->getParsedBody();
            $cusId = $input['cus_id'];
            $whsId = $input['whs_id'];
            //check existed
            $cusMeta = $this->customerMetaModel->getFirstWhere([
                'cus_id'    => $cusId,
                'qualifier' => 'TAX'
            ]);

            $cusMetaData = json_encode([
                $input['whs_id'] => [
                    'whs_id'   => $input['whs_id'],
                    'sale_tax' => $input['sale_tax'],
                ]
            ]);
            if (!empty($cusMeta)) {
                //Update CusMeta
                $cusMetaArr = $cusMeta->toArray();
                $valueJson = $cusMetaArr['value'];
                $value = json_decode($valueJson, true);

                $haveWarehouse = 0;
                if ($value) {
                    foreach ($value as $key => $val) {
                        if ($whsId == $key) {
                            //update Tax for this warehouse
                            $haveWarehouse = $whsId;
                        }
                    }
                } else {
                    //value is empty
                    $cusMetaDataArr = [
                        $input['whs_id'] => [
                            'whs_id'   => $input['whs_id'],
                            'sale_tax' => $input['sale_tax'],
                        ]
                    ];
                    $this->customerMetaModel->updateTaxByCusId($cusId, $cusMetaDataArr);

                    return ["data" => "Successful"];
                }

                //create array to update
                $arrItems = [];
                if ($haveWarehouse) {
                    // haveWarehouse == a warehouse
                    foreach ($value as $key => $val) {
                        if ($whsId == $key) {
                            $arrItems[$key] = [
                                'whs_id'   => $val['whs_id'],
                                'sale_tax' => $input['sale_tax'],
                            ];
                        } else {
                            $arrItems[$key] = [
                                'whs_id'   => $val['whs_id'],
                                'sale_tax' => $val['sale_tax'],
                            ];
                        }

                    }

                } else {
                    foreach ($value as $key => $val) {
                        $arrItems[$key] = [
                            'whs_id'   => $val['whs_id'],
                            'sale_tax' => $val['sale_tax'],
                        ];
                    }
                    //add more new warehouse array
                    $arrItems[$whsId] = [
                        'whs_id'   => $input['whs_id'],
                        'sale_tax' => $input['sale_tax'],
                    ];
                }

                $value_arrItems = $arrItems;
                $this->customerMetaModel->updateTaxByCusId($cusId, $value_arrItems);
            } else {
                //Create CusMeta
                $this->customerMetaModel->create([
                    'cus_id'    => $cusId,
                    'value'     => $cusMetaData,
                    'qualifier' => 'TAX',
                ]);
            }

            return ["data" => "Successful"];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $cusId
     * @param Request $request
     *
     * @return array|void
     */
    public function getTaxByWarehouse(
        $cusId,
        Request $request
    ) {
        try {
            $input = $request->getQueryParams();
            $whsId = $input['whs_id'];
            //check existed
            $cusMeta = $this->customerMetaModel->getFirstWhere([
                'cus_id'    => $cusId,
                'qualifier' => 'TAX'
            ]);

            $arrItems = [];
            if (!empty($cusMeta)) {
                $cusMetaArr = $cusMeta->toArray();
                $valueJson = $cusMetaArr['value'];
                $value = json_decode($valueJson, true);
                if ($value) {
                    foreach ($value as $key => $val) {
                        if ($whsId == $key) {
                            $arrItems[] = [
                                'cus_id'    => $cusId,
                                'qualifier' => $cusMetaArr['qualifier'],
                                'whs_id'    => array_get($val, 'whs_id', ''),
                                'sale_tax'  => array_get($val, 'sale_tax', 0),
                            ];
                        }
                    }
                }

            }
            if (empty($arrItems)) {
                $arrItems[] = [
                    'cus_id'    => $cusId,
                    'qualifier' => 'TAX',
                    'whs_id'    => array_get($input, 'whs_id', ''),
                    'sale_tax'  => 0,
                ];
            }

            return ["data" => $arrItems];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}