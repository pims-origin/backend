<?php

return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Bx8S7VE5zScbQB_gY_XQvIZJ619RFyt_',
        ],
        'redis' => [
            'class'    => 'yii\redis\Connection',
            'hostname' => 'redis.dev.seldatdirect.com',
            'port'     => 6379,
            'database' => 0,
            'password' => 'c3sellthat'
        ],
        'cache' => [
            'class'     => 'backend\library\MyRedisCache',
            'keyPrefix' => 'awms2_dev:',
        ],
    ],
];

