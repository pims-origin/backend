<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/1/2016
 * Time: 11:05 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed name
 * @property mixed code
 * @property mixed type
 * @property mixed description
 * @property mixed rule_name
 * @property mixed data
 */
class Authority extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'authority';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'name';
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'type',
        'description',
        'rule_name',
        'data',
    ];

    public function menu()
    {
        return $this->hasMany('App\Menu', 'name', 'permission_name');
    }
}
