<?php

namespace App\Api\Inventory\Validators;


class CycleDtlValidator extends AbstractValidator
{
    public function rules()
    {
       return [
           'cycle_hdr_id'   => 'required|exists:cycle_hdr,cycle_hdr_id',
           'whs_id'       => 'required|exists:warehouse,whs_id',
           'cus_id'       => 'required|exists:customer,cus_id',
           'item_id'       => 'required|exists:item,item_id',
           'sku'            => 'required|max:50',
           'size'       => 'required|max:30',
           'color'       => 'required|max:30',
           'pack'       => 'required|integer',
           'remain'       => 'required|integer',
           'sys_qty'       => 'required|integer',
           'sys_loc_id'       => 'required|exists:location,loc_id',
           'sys_loc_name'       => 'required',
           'cycle_dtl_sts'       => 'required',
           'sts'       => 'required'
       ];

    }
}