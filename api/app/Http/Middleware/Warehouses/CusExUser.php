<?php
namespace App\Http\Middleware\Warehouses;

use Illuminate\Database\Eloquent\Model;

class CusExUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_ex_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = ['user_id', 'cus_id', 'whs_id'];

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'cus_id',
        'whs_id',
    ];
}
