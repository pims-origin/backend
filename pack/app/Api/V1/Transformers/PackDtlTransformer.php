<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

/**
 * Class PackDtlTransformer
 *
 * @package App\Api\V1\Transformers
 */
class PackDtlTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderDtl
     *
     * @return array
     */
    public function transform(OrderHdr $orderDtl)
    {
        $partial = $orderDtl->back_odr == 1 ? "Partial " : "";
        return [
            // Customer
            'cus_id'   => $orderDtl->cus_id,
            'cus_name' => object_get($orderDtl, 'customer.cus_name', null),

            'cus_odr_num'          => $orderDtl->cus_odr_num,
            'ref_cod'              => $orderDtl->ref_cod,
            'cus_po'               => $orderDtl->cus_po,
            'odr_type_key'         => $orderDtl->odr_type,
            'odr_type'             => Status::getByKey("Order-type", $orderDtl->odr_type),
            'rush_odr'             => $orderDtl->rush_odr,
            'sku_ttl'              => $orderDtl->sku_ttl,
            'odr_sts_key'          => $orderDtl->odr_sts,
            'odr_sts'              => $partial . str_replace($partial, '', Status::getByKey("Order-status",
                    $orderDtl->odr_sts)),
            'odr_num'              => $orderDtl->odr_num,
            'carrier'              => $orderDtl->carrier,
            'ship_to_name'         => $orderDtl->ship_to_name,
            'ship_to_add_1'        => $orderDtl->ship_to_add_1,
            'ship_to_city'         => $orderDtl->ship_to_city,
            'in_notes'             => $orderDtl->in_notes,
            'cus_notes'            => $orderDtl->cus_notes,

            // Country
            'ship_to_country'      => $orderDtl->ship_to_country,
            'ship_to_country_name' => object_get($orderDtl, 'systemCountry.sys_country_name', null),

            // State
            'ship_to_state'        => $orderDtl->ship_to_state,
            'ship_to_state_name'   => object_get($orderDtl, 'systemState.sys_state_name', null),

            'ship_to_zip'   => $orderDtl->ship_to_zip,
            'ship_by_dt'    => $this->dateFormat($orderDtl->ship_by_dt),
            'cancel_by_dt'  => $this->dateFormat($orderDtl->cancel_by_dt),
            'req_cmpl_dt'   => $this->dateFormat($orderDtl->req_cmpl_dt),
            'act_cmpl_dt'   => $this->dateFormat($orderDtl->act_cmpl_dt),
            'act_cancel_dt' => $this->dateFormat($orderDtl->act_cancel_dt),
            'sku_ttl_com'   => $orderDtl->sku_ttl_com,
            'piece_ttl'     => $orderDtl->piece_ttl,
            'ctn_ttl'       => $orderDtl->ctn_ttl,
            'origin_odr_type' => object_get($orderDtl, 'shippingOrder.type', ''),
            'items'         => $orderDtl->items,
            'cartons'       => $orderDtl->cartons
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date ? date("m/d/Y", $date) : null;
    }
}
