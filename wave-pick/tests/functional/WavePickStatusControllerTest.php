<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Response as IlluminateResponse;

class WavePickStatusControllerTest extends TestCase
{

    use DatabaseTransactions;

    protected $urlWvStsLs = '/v1/wave-pick-statuses';



    public function testGetDetailWavePick_ok()
    {
         $this->call('GET', $this->urlWvStsLs);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }
}
