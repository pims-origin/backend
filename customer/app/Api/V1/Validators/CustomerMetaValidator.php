<?php

namespace App\Api\V1\Validators;


class CustomerMetaValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'qualifier' => 'required'
        ];
    }
}
