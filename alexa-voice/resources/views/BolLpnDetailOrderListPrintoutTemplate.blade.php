﻿<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>BOL LPNs List pdf</title>
    <style type="text/css">

        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 36px;
            font-weight: 100;
            margin: 0;
        }

        h2 {
            color: #282828;
            text-transform: uppercase;
            float: left;
            width: 100%;
            margin: 5px 0;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 14px;
            font-family: "Calibri";
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            border: 1px solid #ddd;
            padding: 20px;
            margin: 0 auto;

        }

        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #a9a9a9;
            font-size: 19px;
        }

        .table-style tbody tr td b {
            font-size: 20px;
            font-weight: 600;
            color: #000;
            margin-bottom: 50px;
        }

        .table-style tbody tr td {
            line-height: 38px;
        }

        .table-style2 {
            border: 0px solid #a9a9a9;
            margin: 15px 0;
            line-height: 30px;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            text-transform: capitalize;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 0px dashed #d5d5d5;
            border-bottom: 0px solid #c0c0c0;
            color: #595959;
            line-height: 36px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }

        .table-style2 tbody tr:nth-child(2n) td {
            background-color: #ffffff;
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }

        footer {
            border-top: 1px solid #ddd;
            margin-top: 50px;
            padding-top: 10px;
        }

        .barcodeborder {
            background-color: #0F6;
            border: 1px solid #ddd;
            width: 350px;
            float: left;
            text-align: center;
            padding: 15px;
        }

        .table-style3 {
            border: 1px solid #a9a9a9;
            margin: 15px 0;
        }

        .table-style3 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            /*text-transform: capitalize;*/
            line-height: 20px;
        }

        .table-style3 tbody tr td {
            padding-left: 5px;
            border-right: 1px dashed #d5d5d5;
            border-bottom: 1px dashed #d5d5d5;
            color: #595959;
            line-height: 30px;
        }

        .table-style3 tbody tr td:last-child {
            border-right: none;
        }

        .table-style3 tbody tr td.title {
            font-weight: 600;
            color: #364150;
        }

        .table-style3 tbody tr:nth-child(2n) td {
            background-color: #e3f1f2;
            padding-left: 5px;
            padding-right: 5px;
        }

        .table-style3 tbody tr:last-child td {
            border-bottom: none;

        }

    </style>
</head>
<body>

<!--Order Detail-->
<table class="table-style3" cellpadding="0" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Order #</th>
        <th>Item ID</th>
        <th>UPC</th>
        <th>SKU</th>
        <th>Size</th>
        <th>Color</th>
        <th>Lot</th>
        <th>UOM</th>
        {{--<th>QTY</th>--}}
        <th>Pack Size</th>
        <th>QTY #</th>

    </tr>
    </thead>
    <tbody>

    @foreach($OrderInfos as $OrderInfo)
        {{--<tr style="background: #ffff00 !important;">--}}
        {{--<td colspan="11" style="background: #ffffdd !important;">Order: {{array_get($OrderInfo, 'odr_num', '')--}}
        {{--}}</td>--}}
        {{--</tr>--}}
        {{--<td rowspan="{{array_get($OrderInfo, 'ord_no', '')}}">{{array_get($OrderInfo, 'odr_num', '')}}</td>--}}
        <?php $i = 0;?>
        @foreach($OrderDetailInfos as $OrderDetailInfo)
            @if(array_get($OrderInfo, 'odr_id', '') == array_get($OrderDetailInfo, 'odr_id', ''))
                <tr><?php $i++?>

                    @if($i==1)
                        <td rowspan="{{array_get($OrderInfo, 'ord_no', '')}}"
                            style="background-color: #FFF">{{array_get($OrderDetailInfo, 'odr_num', '')}}</td>
                    @endif
                    <td>{{array_get($OrderDetailInfo, 'item_id', '')}}</td>
                    <td>{{array_get($OrderDetailInfo, 'cus_upc', '')}}</td>
                    <td>{{array_get($OrderDetailInfo, 'sku', '')}}</td>
                    <td>{{array_get($OrderDetailInfo, 'size', '')}}</td>
                    <td>{{array_get($OrderDetailInfo, 'color', '')}}</td>
                    <td>{{array_get($OrderDetailInfo, 'lot', '')}}</td>
                    <td>{{array_get($OrderDetailInfo, 'uom_code', '')}}</td>
{{--                    <td>{{array_get($OrderDetailInfo, 'qty', '')}}</td>--}}
                    <td>{{array_get($OrderDetailInfo, 'pack', '')}}</td>
                    <td>{{array_get($OrderDetailInfo, 'piece_qty', '')}}</td>

                </tr>
            @endif
        @endforeach
    @endforeach
    </tbody>
</table>
<!--/Item: Receiving list-->
<!--/Order Detail-->

<footer>
    <table width="100%">
        <tr>
            <td>{{date("Y/m/d h:i:s")}}
                {{--By--}}
                {{--{{--}}
                {{--!empty($createdBy[array_get($pack, 'created_by', '')]) ? $createdBy[array_get($pack, 'created_by', '')] : ''--}}
                {{--. " " .--}}
                {{--$lastName--}}
                {{--}}--}}
            </td>
            <td class="text-right">seldatinc.com</td>
        </tr>
    </table>
</footer>
</body>
</html>
