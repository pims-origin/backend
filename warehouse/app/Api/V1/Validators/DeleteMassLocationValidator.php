<?php

namespace App\Api\V1\Validators;

class DeleteMassLocationValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_id'   => 'required',
            'loc_id.*' => 'required|exists:location,loc_id'
        ];
    }
}