<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V2\Putback\AssignPallet\Validators;

/**
 * Class PalletAssignValidator
 *
 * @package App\Api\V1\Validators
 */
class PalletAssignValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'return_id'          => 'required|integer|exists:return_hdr,return_id',
            'items'              => 'required|array',
            'items.*.item_id'    => 'required|integer|exists:item,item_id',
            'items.*.pack_size'  => 'required|integer',
            'items.*.odr_hdr_id' => 'required|integer',
            // 'items.*.odr_dtl_id' => 'required|integer',
            'items.*.new_ctns'   => 'integer',
            'items.*.plt_rfid'   => 'required|string',
            'items.*.new_sku'    => 'required|integer',
            'items.*.ctns'       => 'required|integer',
            'items.*.qty_kg'     => 'required',
        ];
    }


}
