<?php

namespace App\Api\V2\Inbound\Carton\GetDamageType\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Support\Collection;
use Illuminate\Http\Response as IlluminateResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Api\V1\Models\Log;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class DamageTypeController extends AbstractController
{

    /**
     * DamageTypeController constructor.
     *
     */
    public function __construct() {}

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function getDamageType($whsId, Request $request) {
        $input = $request->getQueryParams();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/carton/get-damage-type";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GDT',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get damage type'
        ]);

        /*
         * end logs
         */

        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $attributes = SelArr::removeNullOrEmptyString($input);

            // Search whs_id
            $query = DB::table('damage_type')
                ->select([
                    'dmg_id',
                    'dmg_code',
                    'dmg_name',
                    'dmg_des',
                ]);

            $query->where('deleted', 0);
            $query->orderBy('dmg_id');

            $limit = array_get($input, 'limit');
            if ($limit > 0){
                $query->take($limit);
            }
            $result = $query->get();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data']   = $result;

            return $msg;

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }

    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

}
