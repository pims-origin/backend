<?php

namespace App\Jobs;

use DB;

class BillableLaborJob extends IMSJob
{
    protected $whsId;
    protected $grHdrId;
    protected $sac = 'I025';

    public function __construct($whsId, $grHdrId)
    {
        $this->whsId   = $whsId;
        $this->grHdrId = $grHdrId;
    }

    public function handle()
    {
        $grHdr = DB::table('gr_hdr')
            ->where('gr_hdr_id', $this->grHdrId)
            ->first();

        try {
            $token = $this->login();

            $endpoint = env('IMS_BILLABLE_LABOR');

            // Check IMS Configuration
            if(empty($endpoint)) {
                $this->writeLogment('IMS Billable Labor ' . $grHdr['gr_hdr_num'], 'IMS Billable Labor Endpoint is not define!');
                return false;
            }

            $data = DB::table('billable_hdr')
                ->join('billable_dtl', 'billable_dtl.ba_id', '=', 'billable_hdr.ba_id')
                ->leftJoin('users', 'users.user_id', '=', 'billable_dtl.ba_user_id')
                ->where('billable_hdr.deleted', 0)
                ->where('billable_hdr.whs_id', $this->whsId)
                ->where('billable_hdr.cus_id', $grHdr['cus_id'])
                ->where('billable_hdr.ref_num', $grHdr['gr_hdr_num'])
                ->where('billable_dtl.sac', $this->sac)
                ->select([
                    'users.username',
                    'billable_hdr.cus_id',
                    'billable_hdr.whs_id',
                    'billable_dtl.sac_name',
                    'billable_dtl.ref_num',
                    'billable_dtl.qty',
                    'billable_dtl.ba_type',
                ])
                ->groupBy('billable_dtl.ref_num')
                ->groupBy('billable_dtl.ba_user_id')
                ->get();

            $maked = $this->transformMake($data);
            
            $data = ['data' => $maked];

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-Type: application/json"
            ];

            $result = $this->call($endpoint, 'POST', json_encode($data), $header);

            $this->writeLogment('IMS Billable Labor ' . $grHdr['gr_hdr_num'], 'URL: '. $endpoint .' --- Request: '. json_encode($data) .' --- Response: ' . $result);

        } catch (\Exception $e) {
            $this->writeLogment('IMS Billable Labor ' . $grHdr['gr_hdr_num'], $e->getMessage());
        }
    }

    public function transformMake($data) {
        $result = [];
        foreach($data as $key => $val) {
            $result[$key] = [
                "user_name"     => array_get($val, 'username', null),
                "cus_id"        => array_get($val, 'cus_id', null),
                "whs_id"        => array_get($val, 'whs_id', null),
                "sac_name"      => array_get($val, 'sac_name', null),
                "ref_num"       => array_get($val, 'ref_num', null),
                "hours"         => array_get($val, 'qty', 0),
                "ba_type"       => array_get($val, 'ba_type', null)
            ];
        }

        return $result;
    }
}
