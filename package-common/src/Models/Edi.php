<?php

namespace Seldat\Wms2\Models;

class Edi extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'edi';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'edi_trans';

    /**
     * @var array
     */
    protected $fillable = [
        'edi_trans',
        'edi_name',
        'des',
        'edi_sts',
    ];
}
