<?php
/**
 * Created by PhpStorm.
 *
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use App\Api\V1\Models\OrderHdrModel;
use Seldat\Wms2\Models\OrderDtl;
use App\Api\V1\Models\OrderDtlModel;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderReportTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {

        $req_cmpl_dt = ($orderHdr->req_cmpl_dt) ? $this->dateFormat($orderHdr->req_cmpl_dt) : '';
        $created_at = ($orderHdr->created_at) ? $orderHdr->created_at->format('d/m/Y') : '';

        return [
            'odr_id'      => $orderHdr->odr_id,
            'odr_num'     => $orderHdr->odr_num,
            'cus_id'      => $orderHdr->cus_id,
            'status'      => $orderHdr->odr_sts,
            'cus_name'    => object_get($orderHdr, 'customer.cus_name', ''),
            'sku_ttl'     => OrderDtl::where('odr_id', $orderHdr->odr_id)->count('item_id'),
            'req_qty'     => OrderDtl::where('odr_id', $orderHdr->odr_id)->sum('piece_qty'),
            'req_cmpl_dt' => $req_cmpl_dt,
            'created_by'  => object_get($orderHdr, 'createdBy.first_name', '') . ' ' .
                object_get($orderHdr, 'createdBy.last_name', ''),
            'created_at'  => $created_at,

        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }
}
