<?php

namespace App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Controllers;

use App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Models\AsnDtlModel;
use App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Models\AsnHdrModel;
use App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Models\DamageCartonModel;
use App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Models\GoodsReceiptModel;
use App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Models\EventTrackingModel;
use App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Models\Log;
use App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Models\CartonModel;
use App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Models\GoodsReceiptDetailModel;
use App\libraries\RFIDValidate;

use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Models\Reason;
use Wms2\UserInfo\Data;


/**
 * Class PalletController
 *
 * @package App\Api\V2\Inbound\Carton\ScanCartonWithoutPallet\Controllers
 */
class CartonController extends AbstractController
{
    protected $cartonModel;
    protected $asnDtlModel;
    protected $eventTrackingModel;
    protected $goodsReceiptModel;
    protected $goodsReceiptDetailModel;
    protected $asnHdrModel;
    protected $dmgCtnModel;

    private   $_request;
    private   $_whsId;
    private   $_url;
    private   $_transaction;


    /**
     * PalletController constructor.
     *
     * @param CartonModel $cartonModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        CartonModel $cartonModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->asnDtlModel             = new AsnDtlModel();
        $this->asnHdrModel             = new AsnHdrModel();
        $this->cartonModel             = $cartonModel;
        $this->eventTrackingModel      = $eventTrackingModel;
        $this->goodsReceiptModel       = new GoodsReceiptModel();
        $this->goodsReceiptDetailModel = new GoodsReceiptDetailModel();
        $this->dmgCtnModel             = new DamageCartonModel();
    }

    /**
     * @param  Integer $whsId
     * @param  Request $request
     * @return mixed
     */
    public function createGoodsReceipt($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        /**
         * Logs
         */
        $this->_transaction = $owner = $transaction = "";
        $url = "/v2/whs/{$whsId}/carton/create-goods-receipt";
        Log:: info($request, $whsId, [
            'evt_code'     => 'CGR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Scan Cartons and Create Goods Receipt'
        ]);

        $this->_request = $request;
        $this->_url     = $url;
        $this->_whsId   = $whsId;

        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        $asnDtlId = (int)array_get($input, 'asn_dtl_id');
        $ctnRfids = array_get($input, 'ctn_rfids');

        $asnDtl = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $asnDtlId]);
        if (!$asnDtl) {
            $msg = "ASN detail doesn't exist!";
            return $this->_responseErrorMessage($msg);
        }
        if ($asnDtl && $asnDtl->asn_dtl_sts == "CC") {
            $msg = sprintf('Asn detail Id %s already canceled', $asnDtlId);
            return $this->_responseErrorMessage($msg);
        }

        $asnHdrId = object_get($asnDtl, 'asn_hdr_id', 0);
        $itemId   = object_get($asnDtl, 'item_id', 0);
        $ctnrId   = object_get($asnDtl, 'ctnr_id', 0);
        $ctnrNum  = object_get($asnDtl, 'ctnr_num');
        $expectDate = object_get($asnDtl, 'asn_dtl_ept_dt');

        $asnHdr = $this->asnHdrModel->getFirstWhere(['asn_hdr_id' => $asnHdrId]);
        if ($asnHdr && $asnHdr->asn_sts === 'CO'){
            $msg = 'ASN already completed receiving';
            return $this->_responseErrorMessage($msg);
        }

        try {

            // start transaction
            DB::beginTransaction();

            $cusId    = object_get($asnHdr, 'cus_id');

            //delete carton and virtual carton if asn type is RMA
            if ( $asnHdr->asn_type === 'RMA' ){
                $this->cartonModel->deletePutBackAndShippedCartons($ctnRfids);
            }

            // check carton which status is ADJUSTED
            $checkExists = $this->cartonModel->getModel()
                            ->whereIn('rfid', $ctnRfids)
                            ->whereNotIn('ctn_sts', ['AJ', 'CC'])
                            ->get();
            if (count($checkExists) > 0) {
                $ctnRfidExists = array_pluck($checkExists, 'rfid');
                $msg = sprintf("Carton RFIDs exists: %s ", implode(', ', $ctnRfidExists));
                return $this->_responseErrorMessage($msg);
            }

            if (!$asnHdr) {
                $msg = "ASN Header doesn't exist!";
                return $this->_responseErrorMessage($msg);
            }

            if ($asnHdr && $asnHdr->asn_sts == 'RE') {
                $msg = "ASN Header already received!";
                return $this->_responseErrorMessage($msg);
            }

            if ($asnHdr && $asnHdr->asn_sts == 'CC') {
                $msg = "ASN Header already canceled!";
                return $this->_responseErrorMessage($msg);
            }

            $this->cartonModel->getModel()
                            ->whereIn('rfid', $ctnRfids)
                            ->whereIn('ctn_sts', ['AJ', 'CC'])
                            ->update([
                                'deleted_at' => time(),
                                'deleted'    => 1,
                            ]);

            $statusGR = false;
            $grHdr = $this->goodsReceiptModel->isGoodsReceiptExist($asnHdrId, $ctnrId);
            if ($grHdr) {
                $statusGR = false;
                if ($grHdr && $grHdr->gr_sts == 'RE') {
                    $msg = "Goods Receipt already received!";
                    return $this->_responseErrorMessage($msg);
                }

                if ($grHdr && $grHdr->gr_sts == 'CC') {
                    $msg = "Goods Receipt already canceled!";
                    return $this->_responseErrorMessage($msg);
                }

                if ($grHdr && $grHdr->created_from != 'WAP') {
                    $msg = "Only scan cartons for Goods Receipt has been already created by WAP.";
                    return $this->_responseErrorMessage($msg);
                }
                // $createdFrom = object_get($grHdr, 'created_from');
                // if ($createdFrom != "WAP") {
                //     $msg= sprintf('Unable scan pallet for Goods Receipt that has been already created by %s', $createdFrom);

                //     DB::rollback();
                //     $this->_responseErrorMessage($msg, $vtlCtn);
                // }
            } else {
                $statusGR = true;
                $grHdr = $this->goodsReceiptModel->createGRHeader($asnHdr, $ctnrId, $ctnrNum, $expectDate);
            }

            //2     Create One Goods Receipt Details
            $grDtl = $this->goodsReceiptDetailModel->createGrDetail($grHdr, $asnDtl);

            $params = compact('grHdr', 'grDtl', 'asnDtl', 'ctnRfids');

            $cartonTtl = $this->cartonModel->createCartons($params);

            $this->asnHdrModel->updateWhere(
                [
                    'asn_sts' => Status::getByKey('ASN_STATUS', 'RECEIVING')
                ],
                ['asn_hdr_id' => $asnHdrId,
                  'asn_sts'   => 'NW',
                ]
            );

            //params for ASN event tracking receiving
            $evtASNReceiving = [
                'whs_id'    => $whsId,
                'cus_id'    => $cusId,
                'owner'     => $asnHdr->asn_hdr_num,
                'evt_code'  => Status::getByKey('EVENT', 'ASN-RECEIVING'),
                'trans_num' => $asnHdr->asn_hdr_num,
                'info'      => sprintf(Status::getByKey('EVENT-INFO', 'ASN-RECEIVING'),
                    $asnHdr->asn_hdr_num),
            ];

            //call save event tracking for ASN receiving
            $this->eventTracking($evtASNReceiving);

            $this->updateASNDtlReceiving($asnDtlId);

            //13    Create Event Tracking for Create/Update Goods Receipt
            $info = sprintf("WAP - Goods Receipt Update %d cartons", count($ctnRfids));
            $evtCode = 'WGU';

            if ($statusGR) {
                $info = "WAP - Goods Receipt Created";
                $evtCode = 'WGN';
            }


            $evtGRCreated = [
                'whs_id'    => $whsId,
                'cus_id'    => $cusId,
                'owner'     => object_get($grHdr, 'gr_hdr_num', null),
                'evt_code'  => $evtCode,
                'trans_num' => $ctnrNum,
                'info'      => $info,
            ];
            $this->goodsReceiptModel->eventTracking($evtGRCreated);

            //15    Update Goods receipt dtl Received when asn_dtl Received and count all virtual carton status GR
            // Created

            $grDtlId = array_get($grDtl, 'gr_dtl_id');
            //gr_dtl_plt_ttl,  gr_dtl_act_ctn_ttl, gr_dtl_dmg_ttl, gr_dtl_is_dmg, gr_dtl_disc
            $this->goodsReceiptDetailModel->updateGrDtlWhenScanPallet($grDtlId);

            //update GR Dtail is received
            $this->goodsReceiptDetailModel->updateGoodsReceiptDetailReceived($grDtlId);

            //16    Update Goods receipt Header when all ASN dtls of one container are received and all gr dtls are received
            $grHdrId = array_get($grHdr, 'gr_hdr_id');
            $grHdrStatus = $this->goodsReceiptModel->updateGrHdrStatus($grHdrId);

            if ($grHdrStatus) {
                $evtGRCreated = [
                    'whs_id'    => $whsId,
                    'cus_id'    => $cusId,
                    'owner'     => object_get($grHdr, 'gr_hdr_num', null),
                    'evt_code'  => 'WGC',
                    'trans_num' => $ctnrNum,
                    'info'      => "WAP - Goods Receipt Completed",
                ];

                $this->goodsReceiptModel->eventTracking($evtGRCreated);
            }

            // Update ASN header when all Grs/Containers Received
            $asnHdrStatus = $this->asnHdrModel->updateASNHdrStatusRE($asnDtl['asn_hdr_id']);

            if ($asnHdrStatus) {
                $evtASNRE = [
                    'whs_id'    => $whsId,
                    'cus_id'    => $cusId,
                    'owner'     => $asnHdr->asn_hdr_num,
                    'evt_code'  => 'WAC',
                    'trans_num' => $asnHdr->asn_hdr_num,
                    'info'      => sprintf("WAP - %s received", $asnHdr->asn_hdr_num),
                ];

                $this->goodsReceiptModel->eventTracking($evtASNRE);
            }

            DB::commit();
            $msg = sprintf("%d cartons has been scanned successfully!",
                count($ctnRfids));

            return [
                "iat"     => time(),
                "status"  => true,
                "messages" => [[
                    "msg"         => $msg,
                    "status_code" => 1,
                ]],
                "data"    => []
            ];

        } catch (\Exception $e) {
            DB::rollBack();

            if ($e->getCode() === '23000') {
                preg_match("/1062 Duplicate entry '([^\-]+)/i", $e->getMessage(), $dupCartons, PREG_OFFSET_CAPTURE);
                if (count($dupCartons) > 1) {
                    unset($dupCartons[0]);
                    $errorRfids = [];
                    foreach ($dupCartons as $dupCarton) {
                        $errorRfids[] = array_first($dupCarton);
                    }

                    $msg = sprintf("Duplicated Carton Rfid %s. Unfortunately, It exists in another warehouse.", implode(', ', $errorRfids));
                    return $this->_responseErrorMessage($msg);
                }
            }
            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    private function eventTracking($params)
    {

        // event tracking asn
        $this->eventTrackingModel->refreshModel();

        return $this->eventTrackingModel->create([
            'whs_id'    => $params['whs_id'],
            'cus_id'    => $params['cus_id'],
            'owner'     => $params['owner'],
            'evt_code'  => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info'      => $params['info'],
        ]);
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }


    private function _validateParams($attributes, $whsId)
    {
        $asnDtlId = (int)array_get($attributes, 'asn_dtl_id');
        $ctnRfids = array_get($attributes, 'ctn_rfids');

        $names = [
            'asn_dtl_id' => 'Asn Detail Id',
            'ctn_rfids'  => 'Carton Array',
        ];

        // Check Required
        $requireds = [
            'asn_dtl_id'  => $asnDtlId,
            'ctn_rfids'   => $ctnRfids,
        ];

        // Check int type and greater than 0
        $intGreaterThan0 = [
            'asn_dtl_id'    => $asnDtlId,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet rfid is string
        // if ($pltRfid && !is_string($pltRfid)) {
        //     $msg = $names['pallet_rfid'] . " must be string type.";
        //     return $this->_responseErrorMessage($msg);
        // }

        // cartons is array
        if (!is_array($ctnRfids) || !count($ctnRfids)) {
            $msg = $names['ctn_rfids'] . " must be array type and not be empty.";
            return $this->_responseErrorMessage($msg);
        }

        foreach ($intGreaterThan0 as $key => $field) {
            if (!is_int($field) || $field < 1) {
                $errorDetail = "{$names[$key]} must be integer type and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        //check invalid code carton
        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($ctnRfids as $ctnRfid)
        {
            $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
            if (!$ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $ctnRfid;
                if ('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }

//            if (!is_int($pieceRemain) || $pieceRemain < 1 || $pieceRemain > $packSize) {
//                $errorDetail = sprintf("The piece remain of carton Rfid %s must be integer type,
//                greater than 0 and not greater than pack size %s.", $ctnRfid, $packSize);
//                return $this->_responseErrorMessage($errorDetail);
//            }
        }

        if (count($ctnRfids) != count(array_unique($ctnRfids))) {
            $errorDetail = sprintf("It is duplicated carton.");
            return $this->_responseErrorMessage($errorDetail);
        }

        if (count($ctnRfidInValid)) {
            return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
        }
    }

    /**
     * @param $ansDtlId
     *
     * @return mixed
     */
    private function updateASNDtlReceiving($ansDtlId)
    {
        $status = Status::getByKey('ASN_DTL_STS', 'RECEIVING');

        return $this->asnDtlModel->updateWhere(
            [
                'asn_dtl_sts' => $status
            ],
            [
                'asn_dtl_id'  => $ansDtlId,
                'asn_dtl_sts' => 'NW',
            ]
        );
    }

}
