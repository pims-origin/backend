<?php

namespace Seldat\Wms2\Models;

/**
 * @property mixed whs_id
 * @property mixed whs_name
 * @property mixed whs_status
 * @property mixed whs_short_name
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property mixed whs_country_id
 * @property mixed whs_state_id
 * @property mixed whs_city_name
 * @property mixed warehouseContact
 * @property mixed whs_code
 */
class SystemCountry extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_country';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_country_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sys_country_code',
        'sys_country_name',
        'ordinal'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_country_id', 'sys_country_id');
    }

    public function customerAddress()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerAddress', 'cus_add_country_id', 'sys_country_id');
    }
}
