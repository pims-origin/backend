<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ChargeCode;
use Seldat\Wms2\Models\InvoiceCost;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class InvoiceCostModel extends AbstractModel
{
    /**
     * ChargeCodeModel constructor.
     *
     * @param ChargeCode|null $model
     */
    public function __construct(InvoiceCost $model = null)
    {
        $this->model = ($model) ?: new InvoiceCost();
    }

    public function deleteCustomerChargeDetail($customer_id, $warehouseId, $chargeCodeId)
    {
        return $this->model
            ->where('cus_id', $customer_id)
            ->where('whs_id', $warehouseId)
            ->where('chg_code_id', $chargeCodeId)
            ->delete();
    }

    public function updateCustomerChangeDetail($data)
    {
        return $this->model
            ->where('cus_id', $data['cus_id'])
            ->where('whs_id', $data['whs_id'])
            ->where('chg_code_id', $data['chg_code_id'])
            ->update($data);
    }

    public function saveChargeDetail(array $data)
    {
        $temp = $this->model
            ->where('cus_id', $data['cus_id'])
            ->where('whs_id', $data['whs_id'])
            ->where('chg_code_id', $data['chg_code_id'])
            ->first();

        $this->refreshModel();

        if ($temp) {
            // Update
            return $this->model
                ->where('cus_id', $data['cus_id'])
                ->where('whs_id', $data['whs_id'])
                ->where('chg_code_id', $data['chg_code_id'])
                ->update($data);
        } else {
            // Create
            return $this->model->create($data);
        }
    }
}
