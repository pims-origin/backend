<?php

namespace App\Api\Inventory\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\BlockRsn;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ReasonModel extends AbstractModel
{
    const STATUS_INSERT = 'I';
    const STATUS_UPDATE = 'U';
    const STATUS_DELETE = 'D';

    public $errors = [];

    private $arrStringFields = [
        'block_rsn_name',
        'block_rsn_des'
    ];

    /**
     * ReasonModel constructor.
     *
     * @param BlockRsn $model
     */

    public function __construct(BlockRsn $model)
    {
        $this->model = $model;
    }

    /**
     * @param $post
     *
     * @return bool|static
     */

    public function processCreateBlockReason($post)
    {
        $validate = $this->validateDataInput($post);

        if (!$validate) {
            return false;
        }

        $evt = new EventTrackingModel();

        //generate event code
        $result = $evt->generateReasonNum();
        $post['reason_num'] = $result['reason_num'];
        $post['reason_seq'] = $result['reason_seq'];

        try {

            DB::beginTransaction();

            $model = $this->model->create([
                'whs_id'         => $post['whs_id'],
                'reason_num'     => $post['reason_num'],
                'block_rsn_name' => $post['block_rsn_name'],
                'block_rsn_des'  => $post['block_rsn_des'],
                'sts'            => self::STATUS_INSERT
            ]);

            $this->createEventTracking($post);

            DB::commit();

        } catch (Exception $e) {

            DB::rollback();
            $this->errors[] = $e->getMessage();

            return false;
        }

        return $model;
    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function validateDataInput(&$post)
    {
        return true;
    }

    /**
     * Search Cycle
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return object|null $models
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes)) {

            foreach ($attributes as $key => $value) {
                if (in_array($key, $this->arrStringFields)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $this->model->filterDataIn($query, true, false);
        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $data
     *
     * @return bool
     */

    public function createEventTracking($data)
    {
        $evt = new EventTrackingModel();

        $evt->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => isset($data['cus_id']) ? $data['cus_id'] : null,
            'owner'     => $data['reason_num'],
            'evt_code'  => BlockHdrModel::CREATE_BLOCK_REASON,
            'trans_num' => $data['reason_num'],
            'info'      => BlockHdrModel::$blockEvtTracking[BlockHdrModel::CREATE_BLOCK_REASON]
        ]);
    }
}