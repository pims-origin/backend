<?php
namespace App\Api\V1\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\Profiler;
use Seldat\Wms2\Utils\SelArr;

class ItemModel extends AbstractModel
{
    /**
     * ItemModel constructor.
     *
     * @param Item|null $model
     */
    public function __construct(Item $model = null)
    {
        $this->model = ($model) ?: new Item();
    }

    public function updateOrCreate($array_condition, $array_value) {
        return $this->model->updateOrCreate($array_condition, $array_value);
    }

    /**
     * Delete a Item
     *
     * @param int $itemId
     *
     * @return bool
     */
    public function deleteItem($itemId)
    {
        $item=$this->model->find($itemId);
        if(!$item)
            throw new \Exception("Item not found");
        if($item->status!='RG')
            throw new \Exception("Item is only deleted at status: Recv-Waitapp");
        return $this->model
            ->where('item_id', $itemId)
            ->delete();
    }

    /**
     * Delete a list Items
     *
     * @param array $itemIds
     *
     * @return bool
     */
    public function deleteMassItem(array $itemIds)
    {
        return $this->model
            ->whereIn('item_id', $itemIds)
            ->delete();
    }

    /**
     * Search Item
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return object|null $models
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        Profiler::log('start model');
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['cus_id', 'item_id', 'pack', 'status'])) {
                    $query->where($key, $value);
                } elseif (in_array($key, ['sku', 'color', 'size', 'description'])) {
                    $value = addslashes($value);
                    $query->where($key, 'LIKE', "%{$value}%");
                }
            }
        }
    
        //ignored item is new sku
        $query->whereNull('is_new_sku');

        $this->model->filterDataIn($query, true, false);
        $this->sortBuilder($query, $attributes);
        Profiler::log('build query');

        // Get
        $models = $query->paginate($limit);
        Profiler::log('paginate');
        return $models;
    }

    public function getItemForExport($attributes = [])
    {
        \DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->model
            ->where(function($q){
                return $q->where('is_new_sku',0)->orWhereNull('is_new_sku');
            })
            // ->whereIn('cus_id', Data::getCurrentCusIds())
            ->when(!empty($attributes['item_id']),function($q)use($attributes){
                return $q->where('item_id',$attributes['item_id']);
            })
            ->when(!empty($attributes['sku']),function($q)use($attributes){
                return $q->where('sku','like','%'.$attributes['sku'].'%');
            })
            ->when(!empty($attributes['cus_id']),function($q)use($attributes){
                return $q->where('cus_id',$attributes['cus_id']);
            })
            ->when(!empty($attributes['size']),function($q)use($attributes){
                return $q->where('size','like','%'.$attributes['size'].'%');
            })
            ->when(!empty($attributes['color']),function($q)use($attributes){
                return $q->where('color','like','%'.$attributes['color'].'%');
            })
            ->when(!empty($attributes['description']),function($q)use($attributes){
                return $q->where('description','like','%'.$attributes['description'].'%');
            });

        $this->model->filterDataIn($query, true, false);

        return $query->with(['customer'])->get();
    }

    /**
     * @param $input
     * @param $itemId
     * @param $volume
     * @param $cube
     */
    public function updateItemForRelativeObjects($input, $itemId, $volume, $cube)
    {
        \DB::table('asn_dtl')->where('item_id', $itemId)->update(
            [
                'asn_dtl_length' => floatval(array_get($input, 'length')),
                'asn_dtl_width'  => floatval(array_get($input, 'width')),
                'asn_dtl_height' => floatval(array_get($input, 'height')),
                'asn_dtl_weight' => floatval(array_get($input, 'weight')),
                'asn_dtl_volume' => $volume,
                'asn_dtl_cube'   => $cube,
            ]);

        \DB::table('cartons')->where('item_id', $itemId)->update(
            [
                'length' => floatval(array_get($input, 'length')),
                'width'  => floatval(array_get($input, 'width')),
                'height' => floatval(array_get($input, 'height')),
                'weight' => floatval(array_get($input, 'weight')),
                'volume' => $volume,
                'cube'   => $cube,
            ]);

        \DB::table('odr_cartons')->where('item_id', $itemId)->update(
            [
                'length' => floatval(array_get($input, 'length')),
                'width'  => floatval(array_get($input, 'width')),
                'height' => floatval(array_get($input, 'height')),
                'weight' => floatval(array_get($input, 'weight')),
                'volume' => $volume,

            ]);

        \DB::table('pack_hdr')->where('item_id', $itemId)->update(
            [
                'length' => floatval(array_get($input, 'length')),
                'width'  => floatval(array_get($input, 'width')),
                'height' => floatval(array_get($input, 'height')),
            ]);

        \DB::table('pallet')->where('item_id', $itemId)->update(
            [
                'weight' => floatval(array_get($input, 'weight')),
            ]);

        \DB::table('gr_dtl')->where('item_id', $itemId)->update(
            [
                'length' => floatval(array_get($input, 'length')),
                'width'  => floatval(array_get($input, 'width')),
                'height' => floatval(array_get($input, 'height')),
                'weight' => floatval(array_get($input, 'weight')),
                'volume' => $volume,
                'cube'   => $cube,
            ]);

        \DB::table('out_pallet')->where('item_id', $itemId)->update(
            [
                'length' => floatval(array_get($input, 'length')),
                'width'  => floatval(array_get($input, 'width')),
                'height' => floatval(array_get($input, 'height')),
                'weight' => floatval(array_get($input, 'weight')),
                'volume' => $volume,
            ]);

    }

    public function getItemIdsApproved(array $itemIds)
    {
        return $this->model->whereIn('item_id', $itemIds)->where('status', 'AC')->pluck('item_id');
    }

    public function getAsnRefCodeList($itemIds, $whsId)
    {
        $query = $this->model->join('asn_dtl', 'asn_dtl.item_id', '=', 'item.item_id')
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
            ->where('asn_hdr.whs_id', $whsId)
            ->whereIn('item.item_id', $itemIds)
            ->select(['asn_hdr.asn_hdr_id', 'asn_hdr.asn_hdr_num', 'asn_hdr.asn_hdr_ref'])
            ->groupBy('asn_dtl.asn_hdr_id')
            ->get();

        return $query;
    }


    public function getItemTransactions($itemId, $whsId)
    {
        if (empty($itemId) || empty($whsId)) {
            return null;
        }

        $transNum[] = DB::table('asn_hdr')
            ->join('asn_dtl', 'asn_dtl.asn_hdr_id', '=', 'asn_hdr.asn_hdr_id')
            ->where('asn_hdr.deleted', 0)
            ->where('asn_hdr.asn_sts', '<>', 'CC')
            ->where('asn_hdr.whs_id', $whsId)
            ->where('asn_dtl.item_id', $itemId)
            ->value('asn_hdr.asn_hdr_num');

        $transNum[] = DB::table('gr_hdr')
            ->join('gr_dtl', 'gr_dtl.gr_hdr_id', '=', 'gr_hdr.gr_hdr_id')
            ->where('gr_hdr.deleted', 0)
            ->where('gr_hdr.whs_id', $whsId)
            ->where('gr_dtl.item_id', $itemId)
            ->value('gr_hdr.gr_hdr_num');

        $transNum[] = DB::table('odr_hdr')
            ->join('odr_dtl', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_hdr.deleted', 0)
            ->where('odr_hdr.odr_sts', '<>', 'CC')
            ->where('odr_hdr.whs_id', $whsId)
            ->where('odr_dtl.item_id', $itemId)
            ->value('odr_hdr.odr_num');

        $transNum[] = DB::table('cycle_hdr')
            ->join('cycle_dtl', 'cycle_dtl.cycle_hdr_id', '=', 'cycle_hdr.cycle_hdr_id')
            ->where('cycle_hdr.deleted', 0)
            ->where('cycle_hdr.cycle_sts', '<>', 'CC')
            ->where('cycle_hdr.whs_id', $whsId)
            ->where('cycle_dtl.item_id', $itemId)
            ->value('cycle_hdr.cycle_num');

        return $transNum;
    }
}
