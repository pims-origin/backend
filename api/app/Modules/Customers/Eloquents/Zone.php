<?php

namespace App\Modules\Customers\Eloquents;

class Zone extends Model
{
    protected $table = 'zone';

    /**
     * @var array
     */
    protected $fillable = [
        'zone_name',
        'zone_code',
        'zone_whs_id',
        'zone_type_id',
        'zone_min_count',
        'zone_max_count',
        'zone_description',
        'zone_num_of_loc',
        'zone_num_of_active_loc',
        'zone_num_of_inactive_loc',
        'created_by',
        'updated_by',
        'zone_color',
    ];
    
    public function customerZone()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerZone', 'zone_id', 'zone_id');
    }

    public function zoneType()
    {
        return $this->hasOne(__NAMESPACE__ . '\ZoneType', 'zone_type_id', 'zone_type_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'zone_whs_id', 'whs_id');
    }
}
