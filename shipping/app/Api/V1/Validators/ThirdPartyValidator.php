<?php

namespace App\Api\V1\Validators;

class ThirdPartyValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_id' => 'required|integer|exists:customer,cus_id',
            'type'   => 'required',
            'name'   => 'required',
            'add_1'  => 'required',
            'city'   => 'required',
            'state'  => 'required',
            'zip'    => 'required',
            'country'=> 'required'
        ];
    }
}
