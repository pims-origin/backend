<?php
/**
 * Created by PhpStorm.
 * User: PhucTran
 * Date: 3/30/2017
 * Time: 2:21 PM
 */

namespace App\Api\V1\Controllers;


use App\Api\V1\Models\File;
use App\Api\V1\Models\ItemMeta;
use App\Api\V1\Transformers\ItemImageTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class ItemImageController extends AbstractController
{
    protected $_request;

    /**
     * FileController constructor.
     */
    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function postUpload()
    {
        $userCache = $this->getUserCache();
        $idUser = $userCache['user_id'];
        $whID = $userCache['current_whs'];
        $fullName = $userCache['first_name'] . ' ' . $userCache['last_name'];

        $data = $this->_request->only(
            'item_id',
            'file'
        );

        $validator = $this->getValidationFactory()->make($data, [
            'item_id' => 'required|exists:item,item_id',
            'file' => 'required|mimes:jpeg,bmp,png,gif,jpg',
        ]);

        if($validator->fails()){
            $this->response()->errorBadRequest($validator->getMessageBag()->first());
        }


        if (is_null($idUser)) {
            throw new \Exception("Unknown user");
        }

        $file = $this->_request->file('file');
        $fileName = $file->getClientOriginalName();
        $pathServer = 'upload/' . uniqid() . '/' . $fileName;
        Storage::put($pathServer, file_get_contents($file->path()));

        $key = base64_encode($pathServer);

        $itemMeta = new ItemMeta();
        $itemMeta->itm_id = $data['item_id'];
        $itemMeta->qualifier = 'img';
        $itemMeta->value = [
            'fileName' => $fileName,
            'key' => $key
        ];
        $itemMeta->created_at = time();
        $itemMeta->created_by = $idUser;
        $itemMeta->save();

        return [
            'data' => $itemMeta
        ];
    }

    public function getList(){
        if($this->_request->has('item_id')){
            $items = ItemMeta::where('itm_id', $this->_request->get('item_id'))
                ->get();
        }
        else {
            $items = ItemMeta::all();
        }

        return $this->response->collection($items, new ItemImageTransformer());
    }

    public function delete($idItemMeta){

        $itemMeta = ItemMeta::find($idItemMeta);

        if(!$itemMeta){
            return $this->response->errorNotFound('File not found');
        }

        $itemMeta->delete();

        return [
            'data' => $itemMeta
        ];
    }
}