<?php

namespace App\Api\V1\Transformers;

use App\InvoiceTransaction;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract
{

    /**
     * ps:Show SAC
     *
     * @param TransactionModel $trans
     *
     * @return array
     */
    public function transform(InvoiceTransaction $trans)
    {
        return [
            "inv_tran_num"    => $trans->inv_tran_num,
            "method"          => $trans->method,
            "amount"          => $trans->amount,
            "inv_tran_note"   => $trans->inv_tran_note,
            "inv_tran_dt"     => $trans->inv_tran_dt_format,
        ];
    }
}
