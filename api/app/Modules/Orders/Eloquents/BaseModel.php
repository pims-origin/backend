<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 6/2/16
 * Time: 3:23 PM
 */

namespace App\Modules\Orders\Eloquents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class BaseModel extends Model
{   
    /**
     * @return int
     */
    public function freshTimestamp()
    {
        return time();
    }

    /**
     * @param \DateTime|int $value
     *
     * @return \DateTime|int
     */
    public function fromDateTime($value)
    {
        return $value;
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return 'U'; // PHP date() Seconds since the Unix Epoch
    }

    /**
     * @return array
     */
    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function filterData(&$query, $filterCus = false, $filterWH = true, $filterCusWH = true)
    {

        if (env('APP_ENV') != 'testing') {
            try {

                $tableName = $this->getTable();
                $predis = new Data();
                $userInfo = $predis->getUserInfo();
               // $isSuperAdmin = array_get($userInfo, 'isSuperAdmin', false);
               // if (!$isSuperAdmin) {
                    $schema = $this->getConnection()->getSchemaBuilder();
                    $currentWH = array_get($userInfo, 'current_whs', 0);
                    if ($schema->hasColumns($tableName, ['whs_id']) && $filterWH) {

                        $query->where($tableName . '.whs_id', $currentWH);
                    }
                    if ($schema->hasColumns($tableName, ['cus_id']) && $filterCus) {
                        $cus_ids = array_column(array_get($userInfo, 'user_customers', []), 'cus_id');
                        if ($filterCusWH) {
                            $ciu = CusInUserModel::where('user_id', array_get($userInfo, 'user_id', 0))->where('whs_id',
                                $currentWH)->get()->toArray();
                            $ciu = array_column($ciu, "cus_id");
                            $query->whereIn($tableName . '.cus_id', array_intersect($cus_ids, $ciu));
                        } else {
                            $query->whereIn($tableName . '.cus_id', $cus_ids);
                        }
                    }
                //}
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function filterDataIn(&$query, $filterCus = false, $filterWH = true, $filterCusWH = true)
    {
        if (env('APP_ENV') != 'testing') {
            try {
                $tblName = $this->getTable();
                $predis = new Data();
                $userInfo = $predis->getUserInfo();
                $isSuperAdmin = array_get($userInfo, 'isSuperAdmin', false);
                if (!$isSuperAdmin) {
                    $schema = $this->getConnection()->getSchemaBuilder();
                    if ($schema->hasColumns($this->getTable(), ['whs_id']) && $filterWH) {
                        $whs_ids = array_column(array_get($userInfo, 'user_warehouses', []), 'whs_id');
                        $query->whereIn($tblName . '.whs_id', $whs_ids);
                    }
                    if ($schema->hasColumns($this->getTable(), ['cus_id']) && $filterCus) {
                        $cus_ids = array_column(array_get($userInfo, 'user_customers', []), 'cus_id');
                        if ($filterCusWH) {
                            $ciu = CusInUserModel::where('user_id',
                                array_get($userInfo, 'user_id', 0))->get()->toArray();
                            $ciu = array_column($ciu, "cus_id");
                            $query->whereIn($tblName . '.cus_id', array_intersect($cus_ids, $ciu));
                        } else {
                            $query->whereIn($tblName . '.cus_id', $cus_ids);
                        }
                    }
                }
            } catch (\Exception $e) {
            }
        }

    }
}