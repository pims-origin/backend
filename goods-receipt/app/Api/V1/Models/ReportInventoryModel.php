<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Inventories;
use Seldat\Wms2\Models\ReportInventory;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class ReportInventoryModel extends AbstractModel
{
    public function __construct()
    {
        $this->model = new ReportInventory();
    }
}