<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

abstract class AbstractTransformer extends TransformerAbstract
{
    protected function getDate($date)
    {
        return strlen($date) >= 10 ? date('m/d/Y', $date) : '';
    }

    public function getWhsTimeZone($whsId)
    {
        $whsConfig = \DB::table('whs_meta')
                    ->where('whs_id', $whsId)
                    ->where('whs_qualifier', 'wtz')
                    ->where('deleted', 0)
                    ->first();
        if($whsConfig) {
            return $whsConfig['whs_meta_value'];
        }
        return false;
    }

    public function setTimeZone($whsId, $time)
    {
        $timezone = $this->getWhsTimeZone($whsId);

        if($timezone && $time) {
            $dateTime = new \DateTime(date('m/d/Y H:i:s', $time));
            $dateTime->setTimeZone(new \DateTimeZone($timezone));

            return $dateTime->format('m/d/Y H:i:s');
        }
        
        return null;
    }
}
