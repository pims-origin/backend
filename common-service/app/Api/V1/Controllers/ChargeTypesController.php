<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\ChargeTypesModel;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class ChargeTypesController extends AbstractController
{
    protected $model;

    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->model = new ChargeTypesModel();
    }

    /*
    ****************************************************************************
    */

}
