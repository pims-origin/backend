<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Forecast;
use Seldat\Wms2\Models\ForecastIndicator;

class ForecastModel extends AbstractModel
{
    const NUMBER_MONTHS_OF_QUARTER = 3;

    public function __construct(Forecast $model = null)
    {
        $this->model = ($model) ?: new Forecast();
    }

    public function search($year, $month, $cusId = null, $whsId = null, $searchAll = false) {
        $query = $this->model
            ->join('fc_indicator', 'forecast.fc_ind_id', '=', 'fc_indicator.fc_ind_id')
            ->where('fc_year', $year)
            ->where('fc_month', $month);
        if ($cusId) {
            $query->where('cus_id', $cusId);
        }

        if ($whsId) {
            $query->where('whs_id', $whsId);
        }

        if (!$searchAll) {
            return $query->get()->keyBy('fc_ind_code');
        }

        return $query->get();
    }

    public function clearOldData($input, $whsId) {
        DB::table('forecast')
            ->where('whs_id', $whsId)
            ->where('cus_id', $input['cus_id'])
            ->whereBetween('fc_month', [$input['quarter']*3 - 2, $input['quarter']*3])
            ->where('fc_year', $input['year'])
            ->delete();
    }

    public function getPreviousMonthsStorageData($data, $storageDate, $numberMonthsAgo) {
        $storageDateTime = new \DateTime($storageDate);
        $monthYear = $storageDateTime
            ->modify("{$numberMonthsAgo} months ago")
            ->format('m-Y');
        $arr = explode('-', $monthYear);

        return $this->model
            ->where('cus_id', $data['cus_id'])
            ->where('whs_id', $data['whs_id'])
            ->where('fc_month', $arr[0])
            ->where('fc_year', $arr[1])
            ->get()
            ->keyBy('fc_ind_id');

    }

    public function getYearMonthHaveData($cusId, $whsId) {
        return $this->model->select([
                'fc_month',
                'fc_year',
            ])
            ->where('cus_id', $cusId)
            ->where('whs_id', $whsId)
            ->groupBy(['fc_year', 'fc_month'])
            ->get();
    }

    public function insertOrUpdate(Array $input, int $whsId, ForecastIndicator $indicator, int $month, Array $row, int $userId)
    {
        if ($input['period'] === 'quarter'){
            $dataForecast = $this->prepareForecastDataByQuarter($input, $whsId, $indicator, $month, $row, $userId);
        }

        if ($input['period'] === 'month'){
            $dataForecast = $this->prepareForecastDataByMonth($input, $whsId, $indicator, $month, $row, $userId);
        }

        $oldForecastData = $this->getFirstWhere([
            'cus_id'    => (int)$input['cus_id'],
            'whs_id'    => $whsId,
            'fc_month'  => $month,
            'fc_year'   => (int)$input['year'],
            'fc_ind_id' => $indicator->fc_ind_id,
        ]);

        if ($oldForecastData) {
            DB::table('forecast')
                ->where('fc_id', $oldForecastData->fc_id)
                ->update([
                'fc_ib' => $dataForecast['fc_ib'],
                'fc_ob' => $dataForecast['fc_ob'],
                'fc_st' => $dataForecast['fc_st'],
            ]);
        } else {
            DB::table('forecast')->insert($dataForecast);
        }
    }

    protected function prepareForecastDataByQuarter(Array $input, int $whsId, ForecastIndicator $indicator, int $month, Array $row, int $userId)
    {
        $index = $month % self::NUMBER_MONTHS_OF_QUARTER;

        $fc_ib = ($row['inbound'] / self::NUMBER_MONTHS_OF_QUARTER) ?? 0;
        $fc_ob = ($row['outbound'] / self::NUMBER_MONTHS_OF_QUARTER) ?? 0;
        $fc_st = ($row['storage'] / self::NUMBER_MONTHS_OF_QUARTER) ?? 0;

        $fc_ib = round($fc_ib);
        $fc_ob = round($fc_ob);
        $fc_st = round($fc_st);

        if ($index == 0){
            $fc_ib = $row['inbound'] - (2 * $fc_ib);
            $fc_ob = $row['outbound'] - (2 * $fc_ob);
            $fc_st = $row['storage'] - (2 * $fc_st);
        }

        return [
            'cus_id'        => $input['cus_id'],
            'whs_id'        => $whsId,
            'fc_ib'         => $fc_ib,
            'fc_ib_act'     => 0,
            'fc_ob'         => $fc_ob,
            'fc_ob_act'     => 0,
            'fc_st'         => $fc_st,
            'fc_st_act'     => 0,
            'fc_ind_id'     => $indicator->fc_ind_id,
            'fc_month'      => $month,
            'fc_year'       => $input['year'],
            'created_at'    => time(),
            'updated_at'    => time(),
            'created_by'    => $userId,
            'updated_by'    => $userId,
            'deleted_at'    => 915148800,
            'deleted'       => 0,
        ];
    }

    protected function prepareForecastDataByMonth(Array $input, int $whsId, ForecastIndicator $indicator, int $month, Array $row, int $userId)
    {
        return [
            'cus_id'        => $input['cus_id'],
            'whs_id'        => $whsId,
            'fc_ib'         => $indicator->has_inbound ? (!empty($row['inbound']) ? $row['inbound'] : 0) : 0,
            'fc_ib_act'     => 0,
            'fc_ob'         => $indicator->has_outbound ? (!empty($row['outbound']) ? $row['outbound'] : 0) : 0,
            'fc_ob_act'     => 0,
            'fc_st'         => $indicator->has_storage ? (!empty($row['storage']) ? $row['storage'] : 0) : 0,
            'fc_st_act'     => 0,
            'fc_ind_id'     => $indicator->fc_ind_id,
            'fc_month'      => $month,
            'fc_year'       => $input['year'],
            'created_at'    => time(),
            'updated_at'    => time(),
            'created_by'    => $userId,
            'updated_by'    => $userId,
            'deleted_at'    => 915148800,
            'deleted'       => 0,
        ];
    }
}