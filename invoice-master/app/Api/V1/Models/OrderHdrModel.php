<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use App\OrderHdr;
use App\Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\Status;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\ChargeCode;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{
    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }
    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $type = array_get($attributes, 'type', '');
        $query = $this->make($with);

        $searchEqualFields = [
            'cus_id'
        ];

        $searchLikeFields = [
            'odr_num',
            'cus_po'
        ];

        foreach ($attributes as $key => $value) {
            //search equal
            if (in_array($key, $searchEqualFields)) {
                if ($value != '') {
                    $query->where($this->getTable().'.' . $key, $value);
                }
            }
            //search like
            if (in_array($key, $searchLikeFields)) {
                if ($value != '') {
                    $query->where($this->getTable().'.' . $key, 'like', '%' . escape_like($value) . '%');
                }

                if ($key == 'cus_po' && $type == 'ord'){
                    $notValidStatusOrder = [
                        Status::getByValue('Shipped', 'ORDER-STATUS'),
                        Status::getByValue('Canceled', 'ORDER-STATUS')
                    ];
                    $query->whereNotIn($this->getTable().'.' . 'odr_sts', $notValidStatusOrder);
                    $query->groupBy($this->getTable().'.' . 'cus_po');
                }
            }
        }
        //filter Order that odr_num not in ref_num_arr array
        // if (!empty($attributes['ref_num_arr'])) {
        //     $query->whereNotIn($this->getTable().'.odr_num', $attributes['ref_num_arr']);
        // }
        //filter Order that odr_id not in odr_id_arr array
        if (!empty($attributes['odr_id_arr'])) {
            $query->whereNotIn($this->getTable().'.odr_id', $attributes['odr_id_arr']);
        }

        //Filter whs_id and cus_id
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where($this->getTable().'.whs_id', $currentWH);
        $cus_ids = $predis->getCurrentCusIds();
        $query->whereIn($this->getTable().'.cus_id', $cus_ids);

        $models = $query->paginate($limit);

        return $models;
    }
}
