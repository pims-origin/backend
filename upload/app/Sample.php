<?php

namespace App;

use App\Utils\Database\Eloquent\SoftDeletes;

class Sample extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sample';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sample_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sample_code',
    ];
}
