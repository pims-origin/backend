<?php

namespace App\Api\V3\Outbound\Wavepick\UpdateWavepick\Controllers;

use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\WavePickHdrModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\WavePickDtlModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\CartonModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\LocationModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\EventTrackingModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\OrderCartonModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\OrderDtlModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\OrderHdrModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\PalletModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\InventorySummaryModel;
use App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models\WaveDtlLocModel;
use App\libraries\RFIDValidate;
use App\Api\V1\Models\Log;

use Seldat\Wms2\Utils\Status;
use App\MyHelper;
use Psr\Http\Message\ServerRequestInterface as Request;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;

class WavepickController extends AbstractController
{
    const DEFAULT_USER_UPDATE_WAVE = 'citaus';

    protected $orderDtlModel;
    protected $locationModel;
    protected $cartonModel;
    protected $eventTrackingModel;
    protected $orderCartonModel;
    protected $wvDtlModel;
    protected $waveHdrModel;
    protected $orderHdrModel;
    protected $palletModel;
    protected $inventorySummaryModel;

    public function __construct(
        CartonModel $cartonModel
    ) {
        $this->cartonModel           = $cartonModel;
        $this->wvDtlModel            = new WavePickDtlModel();
        $this->orderDtlModel         = new OrderDtlModel();
        $this->orderCartonModel      = new OrderCartonModel();
        $this->orderHdrModel         = new OrderHdrModel();
        $this->palletModel           = new PalletModel();
        $this->waveHdrModel          = new WavePickHdrModel();
        $this->eventTrackingModel    = new EventTrackingModel();
        $this->locationModel         = new LocationModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
    }

    public function updateWavePick(Request $request, $whsId, $wv_id)
    {
        set_time_limit(0);
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v3/whs/{$whsId}/wave/{$wv_id}/update";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'UWP',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Update wave pick'
        ]);

        /*
         * end logs
         */

        $codes = array_get($input, 'code', null);

        if (!$codes) {
            $msg = "Please check input params.";
            return $this->_responseErrorMessage($msg);
        }

        $palletRfid = array_get($codes, 'pallet', null);
        $cartons = array_get($codes, 'ctns', null);

        //check invalid code carton
        $ctnRfidInValid = [];
        $errorMsg = '';
        if (!empty($cartons)) {
            foreach ($cartons as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }
        } else {
            $msg = 'Ctns array is required!';
            return $this->_responseErrorMessage($msg);
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
        }

        if ($palletRfid) {
            $pltRFIDValid = new RFIDValidate($palletRfid, RFIDValidate::TYPE_PALLET, $whsId);
            //validate pallet RFID
            if (!$pltRFIDValid->validate()) {
                return $this->_responseErrorMessage($pltRFIDValid->error);
            }
        }

        try {
            \DB::beginTransaction();

            $returnFlag = false;
            //get carton list by carton RFID
            $ctnLists = $this->_getCartonListByRFIDs($cartons);
            $pickedCtn = null;
            // Throw out error message
            if (count($ctnLists) != count($cartons)) {
                //get carton list by carton RFID
                $pickedCtn = $this->cartonModel->getPickedCartonByCtnRFID($cartons, $whsId);
                // checking when it is return picking
                if (count($pickedCtn) == count($cartons)) {
                    $returnFlag = true;
                } else {
                    //validate carton RFID
                    foreach ($cartons as $carton) {
                        $cartonByRFIDInfo = $this->cartonModel->getCartonByCtnRFID($carton, $whsId);
                        //check carton RFID is existed or not
                        if (!$cartonByRFIDInfo) {
                            $msg = sprintf("Carton RFID %s doesn't exist.", $carton);
                            $cartonObj = $this->cartonModel->getFirstWhere([
                                'rfid' => $carton,
                                ]);
                            if ($cartonObj) {
                                $msg = sprintf("Carton RFID %s doesn't belong to warehouse.", $carton);
                            }

                            \DB::rollback();
                            return $this->_responseErrorMessage($msg);
                        }

                        //check carton RFID should be ACTIVE
                        if ($cartonByRFIDInfo->ctn_sts != 'AC' && $cartonByRFIDInfo->ctn_sts != 'AJ') {
                            $msg = sprintf("Unable to update wavepick with mixed statuses {'ACTIVE', 'ADJUSTED'} and 'PICKED'.");
                            if ($cartonByRFIDInfo->ctn_sts == 'SH') {
                                $msg = sprintf("Carton RFID %s has been shipped.", $carton);
                            }
                            if ($cartonByRFIDInfo->ctn_sts == 'LK') {
                                $msg = sprintf("Carton RFID %s has been locked.", $carton);
                            }

                            \DB::rollback();
                            return $this->_responseErrorMessage($msg);
                        }

                        //check carton RFID with piece remain must > 0
                        if ($cartonByRFIDInfo->piece_remain < 1) {
                            $msg = sprintf("Piece remain of carton RFID %s should greater than zero.", $carton);

                            \DB::rollback();
                            return $this->_responseErrorMessage($msg);
                        }

                        //check carton RFID is damaged or not
                        if ($cartonByRFIDInfo->is_damaged == 1) {
                            $msg = sprintf("Carton RFID %s has damaged.", $carton);

                            \DB::rollback();
                            return $this->_responseErrorMessage($msg);
                        }

                        //check is_ecom of carton RFID
                        if ($cartonByRFIDInfo->is_ecom == 1) {
                            $msg = sprintf("Carton RFID %s is only for eCommerce Order.", $carton);

                            \DB::rollback();
                            return $this->_responseErrorMessage($msg);
                        }

                        //check carton RFID must belong to a location
                        if (!$cartonByRFIDInfo->loc_id && $cartonByRFIDInfo->ctn_sts != 'AJ') {
                            $msg = sprintf("Carton RFID %s is not belong to any location.", $carton);

                            \DB::rollback();
                            return $this->_responseErrorMessage($msg);
                        }

                        //check carton RFID must belong to a pallet
                        if (!$cartonByRFIDInfo->plt_id && $cartonByRFIDInfo->ctn_sts != 'AJ') {
                            $msg = sprintf("Carton RFID %s is not belong to any pallet.", $carton);

                            \DB::rollback();
                            return $this->_responseErrorMessage($msg);
                        }

                    }

                    $trueRFIDs = array_pluck($ctnLists, 'rfid', null);
                    $wrongRFIDS = array_diff($cartons, $trueRFIDs);
                    $msg = "At least 1 carton rfid doesn't exist";

                    \DB::rollback();
                    return $this->_responseErrorMessage($msg, $wrongRFIDS);
                }
            }

            /*---------------------------------------------------------------*/
            /*-------------------Start Return picking process----------------*/
            /*---------------------------------------------------------------*/
            if ($returnFlag)
            {
                // When return the pallet RFID is required!
                if (empty($palletRfid)) {
                    $msg = 'The pallet RFID is required when return cartons!';

                    \DB::rollback();
                    return $this->_responseErrorMessage($msg);
                }

                $cartonAssignedToOrder = $this->orderCartonModel->getCartonAssignedToOrder($cartons);
                if (count($cartonAssignedToOrder) != 0) {
                    $cartonRfids = $cartonAssignedToOrder->pluck('ctn_rfid')->toArray();
                    $msg = sprintf('Cannot return cartons: %s which have been assigned to order!', implode(', ',$cartonRfids));

                    \DB::rollback();
                    return $this->_responseErrorMessage($msg);
                }

                $dataWaveByCartons = [
                    'whs_id'     => $whsId,
                    'cartons'    => $cartons,
                    'ctnLists'   => $pickedCtn,
                    'palletRfid' => $palletRfid,
                ];

                $wavePickPickeds = $this->_updateWavePickReturnPicking($dataWaveByCartons);

                \DB::commit();

                $msg = "Return carton(s) belong to wave pick successfully";
                $data = [
                    'message' => $msg,
                    'data'    => [
                        'wv_sts' => $wavePickPickeds,
                    ],
                    'status'  => true,
                ];

                return new Response($data, 200, [], null);
            }
            /*---------------------------------------------------------------*/
            /*--------------------End Return picking process-----------------*/
            /*---------------------------------------------------------------*/

            // update wave pick normaly
            // check valid wave pick
            $wvDtlData = $this->wvDtlModel->getProperlyWvDtl($wv_id, $whsId);

            // Check wave pick detail existed
            if (empty($wvDtlData) || count($wvDtlData) == 0) {
                $msg = "Wave pick doesn't exist.";
                $wvDtlData = $this->wvDtlModel->getModel()
                    ->where([
                        'wv_id'  => $wv_id,
                        'whs_id' => $whsId,
                    ])
                    ->first();

                if ($wvDtlData) {
                    $msg = "Wave pick already completed or canceled.";
                } else {
                    $wvDtlData = $this->wvDtlModel->getModel()
                        ->where([
                            'wv_id' => $wv_id,
                        ])
                        ->first();

                    if ($wvDtlData) {
                        $msg = "Wave pick doesn't belong to warehouse.";
                    }
                }

                \DB::rollback();
                return $this->_responseErrorMessage($msg);
            }

            $userData = new Data();
            $currentUser = $userData->getUserInfo();
            if ((array_get($wvDtlData->toArray(), '0.picker') != $currentUser['user_id'])
                &&
                ($currentUser['username'] != self::DEFAULT_USER_UPDATE_WAVE)) {
                $msg = "This wave pick item doesn't belong to current user";
                return $this->_responseErrorMessage($msg);
            }

            // cartons with Active status on a LOCKED location
            $locIds = array_pluck($ctnLists, 'loc_id');
            $notActiveLocs = $this->locationModel->getNotActiveLocByIds($locIds);
            if ($notActiveLocs) {
                $locRfidNotActive = array_pluck($notActiveLocs, 'rfid');
                $msg = sprintf("Cannot update wavepick for cartons on locations that are not active: %s.", implode(', ', $locRfidNotActive));

                \DB::rollback();
                return $this->_responseErrorMessage($msg);
            }

            $dataWaveByCartons = [
                'whs_id'   => $whsId,
                'cartons'  => $cartons,
                'wv_id'    => $wv_id,
                'ctnLists' => $ctnLists,
                'palletRfid' => $palletRfid,
            ];

            $this->_updateWavePickByCarton($dataWaveByCartons, $wvDtlData);
            //update wave header status
            $wvSts = $this->waveHdrModel->updatePicked($wv_id);

            $wvDtlData = $wvDtlData->first();
            if ($wvSts) {
                $event = [
                    'whs_id'    => $whsId,
                    'cus_id'    => object_get($wvDtlData, 'cus_id'),
                    'owner'     => object_get($wvDtlData, 'wv_num'),
                    'evt_code'  => 'WPC',
                    'trans_num' => object_get($wvDtlData, 'wv_num'),
                    'info'      => "Wave Pick completed",
                ];

                //  Evt tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create($event);
            }

            \DB::commit();

            $msg = "Update wave pick successfully";
            $data = [
                'message' => $msg,
                'data'    => [
                    'picked' => $wvSts,
                ],
                'status'  => true,
            ];

            //WMS2-4274: [Outbound][Update wave pick] - No message display when scanning over expected carton for the order
            $wvDtlTotalCarton = $this->wvDtlModel->getDetailTotalCartons($whsId, $wv_id);
            $data['data']['wavepick_detail'] = $wvDtlTotalCarton;

            return new Response($data, 200, [], null);

        } catch (\Exception $e) {
            \DB::rollback();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    private function _responseErrorMessage($msg, $data = null)
    {
        return new Response([
                        'data'    => $data,
                        'message' => $msg,
                        'status'  => false,
                    ], 200, [], null);
    }

    private function _getCartonListByRFIDs($cartons)
    {
        //get carton list by carton RFID
        $ctnLists = $this->cartonModel->getActiveCartonByCtnRFID($cartons)->toArray();

        if (is_null($ctnLists)) {
            $msg = Message::get("BM155");

            throw new \Exception($msg);

        }

        return $ctnLists;
    }

    private function _updateWavePickByCarton($data, $dataWaveDtlInfo)
    {
        $whs_id     = $data['whs_id'];
        $wv_id      = $data['wv_id'];
        $cartons    = $data['cartons'];
        $ctnLists   = $data['ctnLists'];
        $palletRfid = $data['palletRfid'];

        /* WMS2-4474 - Remove virtual-Non-RFID Cartons when picking*/
        if ($palletRfid != "" && !empty($palletRfid)) {
            $pltObj = $this->palletModel->getFirstWhere(['rfid' => $palletRfid]);

            // get carton that rfid is null and belong to the pallet
            if ($pltObj) {
                // update carton status to AJ and remove location.
                $this->cartonModel->updateWhere(
                    [
                        'loc_id'   => null,
                        'loc_code' => null,
                        'loc_name' => null,
                        'ctn_sts'  => Status::getByKey('CTN_STATUS', 'ADJUSTED'),
                    ],
                    [
                        'plt_id' => $pltObj->plt_id,
                        'rfid'   => null,
                    ]
                );

                //remove pallet when all carton rfids are null
                $this->palletModel->removePalletWhenCtnRfidsAreNull([$pltObj->plt_id], $whs_id);
            }
        } else {
            if ($pltIds = array_pluck($ctnLists, 'plt_id'))
            {
                $pltIds = array_unique($pltIds);
                if (count($pltIds) > 0) {
                    // update carton status to AJ and remove location.
                    $this->cartonModel->getModel()
                        ->whereIn('plt_id', $pltIds)
                        ->whereRaw("(rfid = '' or rfid is null)")
                        ->where('deleted', 0)
                        ->update([
                            'loc_id'     => null,
                            'loc_code'   => null,
                            'loc_name'   => null,
                            'ctn_sts'    => Status::getByKey('CTN_STATUS', 'ADJUSTED'),
                            'updated_at' => time(),
                            'updated_by' => Data::getCurrentUserId(),
                        ]);
                }

                //remove pallet when all carton rfids are null
                $this->palletModel->removePalletWhenCtnRfidsAreNull($pltIds, $whs_id);
            }
        }
        /* END - WMS2-4474 - Remove virtual-Non-RFID Cartons when picking*/

        // Check active & existed carton
        $dataCheckActive = [
            'ctn-rfid' => $cartons,
            'ctnLists' => $ctnLists
        ];
        $this->checkActiveCarton($dataCheckActive);

        //check carton Item ID = WV_dtl ID
        $wvDtlCodes = [];

        foreach ($dataWaveDtlInfo as $wvDtl) {
            $wvDtlCodes[] = ($wvDtl['item_id'] . '@^' . strtoupper($wvDtl['lot']));
        }

        $wvDtlCodesAll = $wvDtlCodes;
        $arrCartonProcessed = []; //cartons which are picked

        foreach ($dataWaveDtlInfo as $wvDtl) {
            $pickedQTY = 0;
            $cartonItemID = null;
            $wvDtlCode = ($wvDtl['item_id'] . '@^' . strtoupper($wvDtl['lot']));

            $locIds = [];
            foreach ($ctnLists as $key => $ctn) {
                $cartonCode = ($ctn['item_id'] . '@^' . strtoupper($ctn['lot']));

                // //check cartons QTY is enough to wave pick or not
                // $ctn_Qty_wvDtl = $wvDtl['ctn_qty'];
                // $wvDtlID = $wvDtl['wv_dtl_id'];
                // $wvNum = $wvDtl['wv_num'];
                // $ctn_Qty_odr_ctn = $this->orderCartonModel->countOrderCTByWvDtlId($wvDtlID);
                // if ($ctn_Qty_wvDtl == $ctn_Qty_odr_ctn) {
                //     $msg = sprintf(
                //         'Had already received enough carton!'
                //     );
                //     throw new \Exception($msg);
                // }

                if (!in_array($cartonCode, $wvDtlCodesAll)) {
                    $msg = sprintf(
                        'The carton rfid %s is not matched with wave'
                        . ' pick: %s', $ctn['rfid'], $wvDtl['wv_num']
                    );
                    throw new \Exception($msg);
                }

                if ($cartonCode == $wvDtlCode) {
                    if ($wvDtl['pack_size'] != $ctn['ctn_pack_size']) {
                        $msg = sprintf(
                            'The carton rfid %s pack size %d is not same pack size %d in wave pick details of %s!',
                            $ctn['rfid'], $ctn['ctn_pack_size'], $wvDtl['pack_size'], $wvDtl['wv_num']

                        );
                        throw new \Exception($msg);
                    }
                    $arrCartonProcessed[$key] = $ctn;
                    $pickedQTY += $ctn['piece_remain'];
                }


                $locIds[] = $ctn['loc_id'];
            }

            if (empty($arrCartonProcessed)) {
                continue;
            }

            //remove wave detail item which processed
            $wvDtlCodes = $this->removeItemFromArray($wvDtlCodes, $cartonCode);

            $actPicked = $wvDtl['act_piece_qty'] + $pickedQTY;

            // update invetory summary
            if ($actPicked > $wvDtl['piece_qty']) {
                $this->_updateInvtSmrOverPicking($wvDtl, $pickedQTY);
            } else {
                $this->_updateInventorySummary($wvDtl, $pickedQTY);
            }

            $wvDtlSts = Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS");
            if ($actPicked >= $wvDtl['piece_qty']) {
                $wvDtlSts = Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS");
            }

            // Update wave pick detail
            $dataUpdateWvDtl = [
                'act_piece_qty' => $actPicked,
                'wv_dtl_id'     => $wvDtl['wv_dtl_id'],
                'wv_dtl_sts'    => $wvDtlSts,
            ];

            //update wave pick detail
            $this->wvDtlModel->updateWvDtlPickCtn($dataUpdateWvDtl);

            $this->waveHdrModel->updateWhere([
                'wv_sts' => 'PK'
            ], [
                'wv_id' => $wv_id
            ]);

            //insert order carton
            $this->orderCartonModel->insertOrderCartons($arrCartonProcessed, $wvDtl);

            //update table carton to pick, pick_date, status, storage_duration
            $this->updateCartonInfoToPicked($arrCartonProcessed);

            //update pallet ctn_ttl
            // update pallet zero date
            $locIds = array_unique($locIds);
            $this->palletModel->updatePalletCtnTtl($locIds);
            $this->palletModel->updateZeroPallet($locIds);

            /**
             * 1. sort cartons by wave details id
             * 2. sort cartons by locations
             * 3. calculate picked qty and actual location
             * 4. Get wv_dtl_loc
             * 5. if existed, update or create
             */
            // $wdtLoc = $this->cartonModel->updateWvDtlLoc($arrCartonProcessed);

            // if ($wdtLoc) {
            //     $this->_insertWaveDtlLoc($wdtLoc, $wvDtl);
            // }

            //remove array cartons which is processed from cartonList
            foreach ($arrCartonProcessed as $key => $value) {
                unset($ctnLists[$key]);
            }
            unset($arrCartonProcessed);
        }

    }

    private function _updateWavePickReturnPicking($data)
    {
        /**
         * Update inventory Availble QTY if Over Pick QTY/Allocated QTY and reduce picked qty
         * Update Wave pick details to reduce actual qty
         * Delete odr Carton table
         * Create pallet and assign cartons to this pallet
         * IF existed pallet and then assign cartons to this pallet, cc notification
         */

        $whs_id = $data['whs_id'];
        // $wv_id = $data['wv_id'];
        $cartons = $data['cartons'];
        $ctnLists = $data['ctnLists'];
        $palletRfid = $data['palletRfid'];

        $wvIds = $this->orderCartonModel->getModel()
                ->select(\DB::raw('distinct wv_hdr_id'))
                ->whereIn('ctn_rfid', $cartons)
                ->where('deleted', 0)
                ->whereNull('odr_hdr_id')
                ->get()->toArray();

        if (count($wvIds) == 0) {
            $msg = "Cartons don't belong to any wave pick";
            throw new \Exception($msg);
        }
        $wvIds = array_pluck($wvIds, 'wv_hdr_id');
        $wavePickPickeds = [];
        foreach ($wvIds as $keyWv => $wv_id) {
            $dataWaveDtlInfo = $this->wvDtlModel->getProperlyWvDtlInReturn($wv_id, $whs_id);

            //check carton Item ID = WV_dtl ID
            $wvDtlCodes = [];
            foreach ($dataWaveDtlInfo as $wvDtl) {
                $wvDtlCodes[] = ($wvDtl['item_id'] . '@^' . strtoupper($wvDtl['lot']));
            }

            $wvDtlCodesAll = $wvDtlCodes;
            $arrCartonProcessed = []; //cartons which are picked

            foreach ($dataWaveDtlInfo as $wvDtl) {
                $returnQTY = 0;
                $cartonItemID = null;
                $wvDtlCode = ($wvDtl['item_id'] . '@^' . strtoupper($wvDtl['lot']));

                $locIds = [];
                foreach ($ctnLists as $key => $ctn) {
                    $cartonCode = ($ctn['item_id'] . '@^' . strtoupper($ctn['lot']));

                    // if (!in_array($cartonCode, $wvDtlCodesAll)) {
                    //     $msg = sprintf(
                    //         'The carton rfid %s is not matched with wave'
                    //         . ' pick: %s', $ctn['rfid'], $wvDtl['wv_num']
                    //     );
                    //     throw new \Exception($msg);
                    // }

                    if ($cartonCode == $wvDtlCode) {
                        // if ($wvDtl['pack_size'] != $ctn['ctn_pack_size']) {
                        //     $msg = sprintf(
                        //         'The carton rfid %s pack size %d is not same pack size %d in wave pick details of %s!',
                        //         $ctn['rfid'], $ctn['ctn_pack_size'], $wvDtl['pack_size'], $wvDtl['wv_num']

                        //     );
                        //     throw new \Exception($msg);
                        // }
                        $orderCartonCheck = $this->orderCartonModel->getFirstWhere([
                            'ctn_rfid'  => $ctn['rfid'],
                            'wv_dtl_id' => $wvDtl['wv_dtl_id'],
                            ]);
                        if ($orderCartonCheck) {
                            $arrCartonProcessed[$key] = $ctn;
                            $returnQTY += $ctn['piece_remain'];
                        }
                    }

                    $locIds[] = $ctn['loc_id'];
                }

                if (empty($arrCartonProcessed)) {
                    continue;
                }

                //remove wave detail item which processed
                $wvDtlCodes = $this->removeItemFromArray($wvDtlCodes, $cartonCode);

                $actPicked = $wvDtl['act_piece_qty'] - $returnQTY;
                if ($actPicked < 0) {
                    $msg = "Unable return picked QTY greater than actual piece QTY of wavepick detail";
                    throw new \Exception($msg);
                }

                $wvDtlSts = Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS");
                if ($actPicked >= $wvDtl['piece_qty']) {
                    $wvDtlSts = Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS");
                }

                // Update wave pick detail
                $dataUpdateWvDtl = [
                    'act_piece_qty' => $actPicked,
                    'wv_dtl_id'     => $wvDtl['wv_dtl_id'],
                    'wv_dtl_sts'    => $wvDtlSts,
                ];

                //update wave pick detail
                $this->wvDtlModel->updateWvDtlPickCtn($dataUpdateWvDtl);

                $this->waveHdrModel->updateWhere([
                    'wv_sts' => 'PK'
                ], [
                    'wv_id' => $wv_id
                ]);

                // delete order carton
                $this->orderCartonModel->deleteOdrCartonInReturn(
                    array_pluck($arrCartonProcessed, 'rfid')
                );

                // update invetory summary
                $this->_updateInvtSmrReturnPicking($wvDtl, $returnQTY);
                //update pallet and carton
                $this->_assignCartonToPallet(
                    $wvDtl,
                    array_pluck($arrCartonProcessed, 'rfid'),
                    $palletRfid,
                    $returnQTY
                );

                // update wavepick detail location
                // $this->_updateWvDtlLocInReturn($arrCartonProcessed, $wvDtl);

                //remove array cartons which is processed from cartonList
                foreach ($arrCartonProcessed as $key => $value) {
                    unset($ctnLists[$key]);
                }
                unset($arrCartonProcessed);
            }

            //update wave header status
            $wvSts = $this->waveHdrModel->updatePicked($wv_id);

            // set wavepick status is cancel
            // 1. Wavepick that  is not overpick will change to Completed when all orders are picked
            // 2. Wavepick that  is not overpick will change to Canceled when all orders are canceled
            // Else wavepick is PICKING
            if ($wvSts == 0) {
                $wvSts = $this->waveHdrModel->updateCanceled($wv_id);
            }

            $wavePickPickeds[] = [
                'wv_id'  => $wv_id,
                'picked' => $wvSts,
            ];
        }

        return $wavePickPickeds;
    }

    private function _insertWaveDtlLoc($wdtLocArr, $wvDtl)
    {
        $wvDtlLoc = (new WaveDtlLocModel())->getModel()->where('wv_dtl_id', $wvDtl->wv_dtl_id)->first();

        if ($wvDtlLoc) {
            if ($wvDtlLoc->act_loc_ids) {
                $actLocs = json_decode($wvDtlLoc->act_loc_ids, true);
                foreach ($actLocs as $keyActLoc => $actLoc) {
                    foreach ($wdtLocArr as $keyWdtLoc => $wdtLoc) {
                        // update existing location
                        if ($wdtLoc['loc_id'] == $actLoc['loc_id']) {
                            $actLocs[$keyActLoc]['cartons']     = array_merge($actLocs[$keyActLoc]['cartons'], $wdtLoc['cartons']);
                            $actLocs[$keyActLoc]['picked_qty'] += $wdtLoc['picked_qty'];
                            $actLocs[$keyActLoc]['avail_qty']   = $wdtLoc['avail_qty'];
                            unset($wdtLocArr[$keyWdtLoc]);
                        }
                    }
                }
                $actLocs = array_merge($actLocs, $wdtLocArr);
                $wvDtlLoc->act_loc_ids = json_encode($actLocs);
            } else {
                $wvDtlLoc->act_loc_ids = json_encode($wdtLocArr);
            }

            return $wvDtlLoc->update();
        }

        return;

    }

    private function _updateInventorySummary($wvData, $pickedQTY)
    {
        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $wvData['item_id'],
                'whs_id'  => $wvData['whs_id'],
                'lot'     => $wvData['lot'],
            ]
        );

        $invtAvail     = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;

        $overAvailQTY = 0;
        if ($invtAllocated < $pickedQTY) {
            if ($invtAllocated + $invtAvail < $pickedQTY) {
                $msg = "Not enough allocated QTY to pick";
                throw new \Exception($msg);
            }

            $wvDataArr = json_decode($wvData->data, true);
            if (!$wvDataArr) {
                $wvDataArr['avail']    = 0;
                $wvDataArr['allocated'] = 0;
            }

            $overAvailQTY = $pickedQTY - $invtAllocated;
            $wvDataArr['avail'] += $overAvailQTY;

            $wvData->data = json_encode($wvDataArr);
            $wvData->update();
        }

        $allocatedSql = sprintf('`allocated_qty` - %d', $pickedQTY - $overAvailQTY);
        $pickedSql    = sprintf('`picked_qty` + %d', $pickedQTY);
        $availSql     = sprintf('`avail` - %d', $overAvailQTY);

        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $wvData['item_id'],
            'whs_id'  => $wvData['whs_id'],
            'lot'     => $wvData['lot'],
        ]);
    }

    private function _updateInvtSmrOverPicking($wvData, $pickedQTY)
    {
        $allocatedQTY = $wvData['piece_qty'] - $wvData['act_piece_qty'];
        $overPicking  = $wvData['act_piece_qty'] + $pickedQTY - $wvData['piece_qty'];
        if ($wvData['act_piece_qty'] >= $wvData['piece_qty']) {
            // over in case over picking continuous
            $overPicking = $pickedQTY;
        }

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $wvData['item_id'],
                'whs_id'  => $wvData['whs_id'],
                'lot'     => $wvData['lot'],
            ]
        );

        $invtAvail     = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;

        $wvDataArr = json_decode($wvData->data, true);
        if (!$wvDataArr) {
            $wvDataArr['avail']    = 0;
            $wvDataArr['allocated'] = 0;
        }

        if ($pickedQTY > $invtAvail + $invtAllocated) {
            $msg = "Not enough available QTY to pick";
            throw new \Exception($msg);
        }

        $overAllocatedQTY = 0;
        $overAvailQTY     = 0;

        // using inventory available qty to overpicking
        if ($wvData['act_piece_qty'] >= $wvData['piece_qty']) {
            // over in case over picking continuous
            if ($overPicking > $invtAllocated) {
                $overAllocatedQTY = $invtAllocated;
                $overAvailQTY     = $overPicking - $overAllocatedQTY;
            } else {
                $overAllocatedQTY = $overPicking;
            }

        } elseif ($overPicking > $invtAllocated - $allocatedQTY) {
            // over picking > inventory allocated remain
            $overAllocatedQTY = $invtAllocated - $allocatedQTY;
            // using inventory allocated qty when inventory available qty is not enough
            $overAvailQTY     = $overPicking - $overAllocatedQTY;
        } else {
            $overAllocatedQTY = $overPicking;
        }

        $wvDataArr['avail']     += $overAvailQTY;
        $wvDataArr['allocated'] += $overAllocatedQTY;

        $wvData->data = json_encode($wvDataArr);
        $wvData->update();

        // check value less than zero
        if ($invtAvail < $overAvailQTY) {
            $msg = "Not enough available QTY to pick";
            throw new \Exception($msg);
        }

        if ($wvData['act_piece_qty'] >= $wvData['piece_qty']) {
            // over in case over picking continuous
            if ($invtAllocated < $overAllocatedQTY) {
                $msg = "Not enough allocated QTY to pick";
                throw new \Exception($msg);
            }
        } elseif ($invtAllocated < $allocatedQTY + $overAllocatedQTY) {
            $msg = "Not enough allocated QTY to pick";
            throw new \Exception($msg);
        }

        $allocatedSql = sprintf('`allocated_qty` - %d', $allocatedQTY + $overAllocatedQTY);
        if ($wvData['act_piece_qty'] >= $wvData['piece_qty']) {
            // over in case over picking continuous
            $allocatedSql = sprintf('`allocated_qty` - %d', $overAllocatedQTY);
        }
        $pickedSql    = sprintf('`picked_qty` + %d', $pickedQTY);
        $availSql     = sprintf('`avail` - %d', $overAvailQTY);

        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $wvData['item_id'],
            'whs_id'  => $wvData['whs_id'],
            'lot'     => $wvData['lot'],
        ]);
    }

    private function _updateInvtSmrReturnPicking($wvData, $returnQTY)
    {
        $returnOverPicking = 0;
        // piece qty < actual picked
        if ($wvData['piece_qty'] <= $wvData['act_piece_qty'] - $returnQTY) {
            // over picking
            $returnOverPicking = $returnQTY;
        } elseif ($wvData['act_piece_qty'] > $wvData['piece_qty']) {
            // over picking a part
            $returnOverPicking = $wvData['act_piece_qty'] - $wvData['piece_qty'];
        }

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $wvData['item_id'],
                'whs_id'  => $wvData['whs_id'],
                'lot'     => $wvData['lot'],
            ]
        );

        $invtAvail     = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;
        $invtPicked    = (int)$ivt->picked_qty;

        $wvDataArr = json_decode($wvData->data, true);
        $wvDataArr = json_decode($wvData->data, true);
        if (!$wvDataArr) {
            $wvDataArr['avail']    = 0;
            $wvDataArr['allocated'] = 0;
        }

        $returnAllocatedQTY = 0;
        $returnAvailQTY    = 0;

        // return picking < overpicking
        if ($returnOverPicking < $wvDataArr['allocated'] + $wvDataArr['avail']) {
            // return to available is higher priority
            if ($returnOverPicking < $wvDataArr['avail']) {
                $returnAvailQTY = $returnOverPicking;
            } else {
                $returnAllocatedQTY = $returnOverPicking - $wvDataArr['avail'];
                $returnAvailQTY     = $wvDataArr['avail'];
            }
        } else {
            // return to inventory allocated Qty a part
            $returnAllocatedQTY = $wvDataArr['allocated'];
            $returnAvailQTY     = $returnOverPicking - $wvDataArr['allocated'];
        }

        $wvDataArr['allocated'] = $wvDataArr['allocated'] - $returnAllocatedQTY;
        $wvDataArr['avail']     = $wvDataArr['avail'] - $returnAvailQTY;
        $wvDataArr['avail']     = $wvDataArr['avail'] < 0 ? 0 : $wvDataArr['avail'];

        $wvData->data = json_encode($wvDataArr);
        $wvData->update();

        if ($invtPicked < $returnQTY) {
            $msg = "Not enough picked QTY to return picking";
            throw new \Exception($msg);
        }

        $allocatedSql = sprintf('`allocated_qty` + %d', $returnQTY - $returnAvailQTY);
        $pickedSql    = sprintf('`picked_qty` - %d', $returnQTY);
        $availSql     = sprintf('`avail` + %d', $returnAvailQTY);


        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $wvData['item_id'],
            'whs_id'  => $wvData['whs_id'],
            'lot'     => $wvData['lot'],
        ]);

        // correct Actual Allocated Qty
        $this->inventorySummaryModel->correctActualAllocatedQty(
            $wvData['whs_id'],
            $wvData['item_id'],
            $wvData['lot']
        );
    }

    private function removeItemFromArray($arr, $val)
    {
        unset($arr[array_search($val, $arr)]);

        return array_values($arr);
    }

    private function checkActiveCarton($data)
    {
        $ctnRFID = $data['ctn-rfid'];
        $ctnLists = $data['ctnLists'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id' => $e['ctn_id'],
                'rfid'   => $e['rfid'],
            ];
        }, $ctnLists);

        $allCtns = array_pluck($tempCtns, null, "rfid");

        foreach ($ctnRFID as $key => $value) {
            if (!isset($allCtns[$value])) {
                $msg = "Carton " . $value . " is not active or existing";
                throw new \Exception($msg);
            }
        }
    }

    private function updateCartonInfoToPicked($cartonList)
    {
        foreach ($cartonList as $ctn) {
            $created_at = array_get($ctn, 'created_at', 0);

            // Calculate storage_duration
            if (is_string($created_at) || is_int($created_at)) {
                $created_at = (int)$created_at;
            } else {
                $created_at = $created_at->timestamp;
            }

            $storageDate = date("Y-m-d", $created_at);

            $pickedDate = date("Y-m-d");

            $dateDiff = (int)round(abs(strtotime($storageDate) - strtotime($pickedDate)) / 86400);

            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }

            $dataCartonUpdate = [
                'ctn_sts'          => Status::getByKey('CTN_STATUS', 'PICKED'),
                'picked_dt'        => time(),
                'storage_duration' => $storageDuration,
                'loc_id'           => null,
                'loc_type_code'    => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'plt_id'           => null,
            ];

            $this->cartonModel->updateWhere($dataCartonUpdate, [
                'ctn_id' => $ctn['ctn_id']
            ]);

            $this->updatePalletPick($ctn);
        }
    }

    private function updatePalletPick($ctn)
    {
        // Update ctn_ttl in pallet table
        $plt_id = array_get($ctn, 'plt_id', null);

        $palletInfo = $this->palletModel->getFirstWhere(['plt_id' => $plt_id]);

        $this->palletModel->updateWhere([
            'ctn_ttl'    => array_get($palletInfo, 'ctn_ttl', null) - 1,
            'updated_at' => time(),
        ], [
            'plt_id' => $plt_id
        ]);

        //update total of carton on pallet
        if (0 == $palletInfo['ctn_ttl']) {
            $this->palletModel->updatePallet($palletInfo);
        }
    }

    private function _assignCartonToPallet($wvData, $ctnRfids, $palletRfid, $returnQTY)
    {
        $pallet = $this->palletModel->getFirstWhere([
            'whs_id' => $wvData['whs_id'],
            'rfid'   => $palletRfid,
        ]);

        if (!$pallet) {
            $wvNum = object_get($wvData, 'wv_num');

            $pltNum = $this->palletModel->generatePalletNumFromWvNum($wvNum);

            $ctnTtl = count($ctnRfids);
            $data = [
                "cus_id"           => $wvData['cus_id'],
                "whs_id"           => $wvData['whs_id'],
                "plt_num"          => $pltNum,
                "rfid"             => $palletRfid,
                "ctn_ttl"          => $ctnTtl,
                "init_ctn_ttl"     => $ctnTtl,
                "storage_duration" => 0,
                "plt_sts"          => "AC",
            ];
            $pallet = $this->palletModel->create($data);
        } else {
            // update pallet
            $pallet->loc_id           = null;
            $pallet->loc_code         = null;
            $pallet->loc_name         = null;
            $pallet->ctn_ttl          += count($ctnRfids);
            $pallet->updated_at       = time();
            $pallet->zero_date        = time();
            $pallet->storage_duration = 0;

            $pallet->save();
        }

        // update cartons status
        $this->cartonModel->getModel()
                        ->whereIn('rfid', $ctnRfids)
                        ->update([
                                'ctn_sts'          => Status::getByKey('CTN_STATUS', 'ACTIVE'),
                                'picked_dt'        => 0,
                                'storage_duration' => 0,
                                'loc_id'           => null,
                                'loc_type_code'    => null,
                                'loc_code'         => null,
                                'loc_name'         => null,
                                'plt_id'           => object_get($pallet, 'plt_id'),
                                'updated_at'       => time(),
                                'updated_by'       => Data::getCurrentUserId(),
                            ]);
    }

    private function _updateWvDtlLocInReturn($cartons, $wvDtl)
    {
        $wvDtlLoc = (new WaveDtlLocModel())->getModel()->where('wv_dtl_id', $wvDtl->wv_dtl_id)->first();

        if ($wvDtlLoc && $wvDtlLoc->act_loc_ids) {
            $actLocs = json_decode($wvDtlLoc->act_loc_ids, true);
            $ctnIdsCheck = array_pluck($cartons, 'ctn_id');
            foreach ($actLocs as $keyLoc => $actLoc) {
                foreach ($actLoc['cartons'] as $keyCtn => $cartonOld) {
                    if (in_array($cartonOld['ctn_id'], $ctnIdsCheck)) {
                        // remove carton
                        unset($actLocs[$keyLoc]['cartons'][$keyCtn]);
                        // update location
                        foreach ($cartons as $carton) {
                            if ($cartonOld['ctn_id'] == $carton['ctn_id']) {
                                // return QTY
                                $actLocs[$keyLoc]['picked_qty'] -= $carton['piece_remain'];
                                break;
                            }
                        }

                    }
                }
                // update carton
                $actLocs[$keyLoc]['cartons'] = array_values($actLocs[$keyLoc]['cartons']);
                if (count($actLocs[$keyLoc]['cartons']) == 0) {
                    // remove location
                    unset($actLocs[$keyLoc]);
                }
            }
            $wvDtlLoc->act_loc_ids = json_encode(array_values($actLocs));

            $wvDtlLoc->update();
        }
    }
}
