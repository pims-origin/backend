<?php

namespace backend\models;

use common\models\Department;
use yii\db\Query;
use yii\data\Pagination;
use backend\forms\Form;
use Yii;

class DepartmentSearch extends BaseUser
{
    const ALLOW_SELECT_FIELDS = [
        'usr_dpm_id',
        'usr_dpm_code',
        'usr_dpm_name',
        'usr_dpm_des',
        'created_at',
        'updated_at'
    ];

    private static $_instance;

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new DepartmentSearch();
        }

        return self::$_instance;
    }

    public static function getDepartment()
    {
        $query = (new Query());
        $departments = $query->from('user_department')
            ->select(self::ALLOW_SELECT_FIELDS)
            ->all();
        if (null === $departments) {
            return false;
        }

        return $departments;
    }
}
