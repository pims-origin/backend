<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:41
 */

namespace App\Api\V1\Validators;


class CustomerLocationValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_id'     => 'required|integer|exists:location,loc_id',
            'cus_id'     => 'required|integer',
            'created_by' => 'required',
        ];
    }
}
