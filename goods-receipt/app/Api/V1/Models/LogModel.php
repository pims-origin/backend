<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Logs;
use Psr\Http\Message\ServerRequestInterface as Request;

class LogModel extends AbstractModel
{

    protected $asnHdr;
    /*
     * @var Log
     */
    protected static $instance = null;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new Logs();
    }

    public static function getInstance(){
        if(! self::$instance){
            self::$instance = new self();
        }
        return self::$instance ;
    }

    /**
     * 
     * @param Integer $whsId
     * @param String $asnNum
     * @param String $ctnrId
     * @param Integer $limit
     * 
     * @return Paginate
     */
    public function getErrorLogs($whsId, $asnNum, $ctnrId = null, $limit = 20)
    {
        $query = $this->model
                ->select(['logs.*', DB::raw('created_at as created_at_str'), 'evt_lookup.des'])
                ->where([
                    'logs.type' => 'error',
                    'whs_id' => $whsId,
                    'owner' => $asnNum
                ])
                ->leftJoin('evt_lookup', 'evt_lookup.evt_code', '=', 'logs.evt_code')
        ;
        if($ctnrId && intval($ctnrId) > 0) {
            $query->where('transaction', $ctnrId);
        }
        
        $query->orderBy('created_at', 'desc');

        return $query->paginate($limit);
    }

}
