<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V3\Inbound\Models;

use Seldat\Wms2\Models\ItemStatus;

class ItemStatusModel extends AbstractModel
{
    /**
     * ItemStatusModel constructor.
     * @param ItemStatus|null $model
     */
    public function __construct(ItemStatus $model = null)
    {
        $this->model = ($model) ?: new ItemStatus();
    }

}
