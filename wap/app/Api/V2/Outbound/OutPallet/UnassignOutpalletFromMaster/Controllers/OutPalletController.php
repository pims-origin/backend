<?php

namespace App\Api\V2\Outbound\OutPallet\UnassignOutpalletFromMaster\Controllers;

use App\Api\V2\Outbound\OutPallet\UnassignOutpalletFromMaster\Models\OutPalletModel;
use App\Api\V2\Outbound\OutPallet\UnassignOutpalletFromMaster\Validators\OutPalletValidator;
use App\Api\V2\Outbound\OutPallet\UnassignOutpalletFromMaster\Validators\MasterOutPalletValidator;

use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OutPalletController extends AbstractController
{

    protected $outPalletModel;

    public function __construct()
    {
        $this->outPalletModel = new OutPalletModel();
    }

    const PREFIX_MASTER_PALLET = "MPL";

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function unassignFromMaster($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/outpallet/unassign-from-master";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'UAO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Unassign Out Pallet from Master'
        ]);

        /*
         * end logs
         */

        $input['whs_id'] = $whsId;
        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        $pltNum = array_get($input, 'plt_num');
        $mplNum = array_get($input, 'mpl_num');

        $pallet = $this->outPalletModel->getFirstWhere([
            'whs_id'  => $whsId,
            'plt_num' => $pltNum,
        ]);

        if (!$pallet) {
            $msg = "The Pallet number doesn't existed.";
            $pallet = $this->outPalletModel->getFirstWhere([
                'plt_num' => $pltNum,
            ]);
            if ($pallet) {
                $msg = "The Pallet number doesn't belong to warehouse.";
            }
            return $this->_responseErrorMessage($msg);
        }

        $masterPallet = $this->outPalletModel->getFirstWhere([
            'whs_id'  => $whsId,
            'plt_num' => $mplNum,
        ]);

        $isMplExist = true;
        if (!$masterPallet) {
            $msg = "The master pallet number doesn't existed.";
            $isMplExist = false;
            $masterPallet = $this->outPalletModel->getFirstWhere([
                'plt_num' => $mplNum,
            ]);
            if ($masterPallet) {
                $msg = "The master pallet number doesn't belong to warehouse.";
            }
            return $this->_responseErrorMessage($msg);
        }

        //Cannot Consolidate when Finalize BOL
        if ($bolIdPlt = object_get($pallet, 'bol_id')) {
            $shipmentObj = DB::table('shipment')
                ->where('ship_id' , $bolIdPlt)
                ->where('whs_id' , $whsId)
                ->where('deleted' , 0)
                ->first();
            $sts = array_get($shipmentObj, 'ship_sts');
            if ($sts == "FN") {
                $msg = sprintf("Unable to removing out pallet when BOL already finalized.", $pltNum);
                return $this->_responseErrorMessage($msg);
            }

            // check master pallet belong to BOL number
            $codeArr     = explode('-', $mplNum);
            $bolNumCheck = "BOL-{$codeArr[1]}-{$codeArr[2]}";
            $realBoNum   = array_get($shipmentObj, 'bo_num');
            if ($bolNumCheck != $realBoNum) {
                $msg = sprintf("Master out pallet %s doesn't belong to Bill of Ladings number %s.", $mplNum, $realBoNum);
                return $this->_responseErrorMessage($msg);
            }

        } else {
            $msg = sprintf("Unable to removing out pallet when BOL isn't created yet.", $pltNum);
            return $this->_responseErrorMessage($msg);
        }

        if ($masterPallet->plt_id != $pallet->parent) {
            $msg = sprintf("Out pallet number %s doesn't belong to Master out pallet %s.", $pltNum, $mplNum);
            return $this->_responseErrorMessage($msg);
        }

        try {
            // start transaction
            DB::beginTransaction();

            if ($masterPallet->bol_id && $masterPallet->bol_id != $pallet->bol_id) {
                \DB::rollback();
                $msg = sprintf("Out pallet number %s and Master out pallet %s are not the same a Bill of Ladings.", $pltNum, $mplNum);
                return $this->_responseErrorMessage($msg);
            }

            $pallet->parent = $pallet->plt_id;
            $pallet->save();

            $masterPallet->ctn_ttl -= $pallet->ctn_ttl;
            $masterPallet->save();

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Unassign Pallet from Master successfully!"
            ]];
            $msg['data']   = [];

            return $msg;

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _validateParams($attributes, $whsId)
    {
        $mplNum = array_get($attributes, 'mpl_num');
        $pltNum = array_get($attributes, 'plt_num');

        $names = [
            'mpl_num' => 'Master pallet number',
            'plt_num' => 'Pallet number',
        ];

        // Check Required
        $requireds = [
            'mpl_num' => $mplNum,
            'plt_num' => $pltNum,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet number is string
        if (!is_string($mplNum) || !is_string($pltNum)) {
            $msg = "Master outpallet and pallet number must be string type";
            return $this->_responseErrorMessage($msg);
        }

        //validate pallet number
        $masterOutPalletValidator = new MasterOutPalletValidator();
        $mplNumValid = $masterOutPalletValidator->validate($mplNum, $whsId);
        if (!$mplNumValid) {
            return $this->_responseErrorMessage($masterOutPalletValidator->getError());
        }

        //validate pallet num
        $pltNumValid = OutPalletValidator::validate($pltNum);
        if (!$pltNumValid) {
            return $this->_responseErrorMessage(OutPalletValidator::ERROR_MSG);
        }

    }
}