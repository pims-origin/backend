<?php

namespace App\Modules\Warehouses\Models;

use Firebase\JWT\JWT;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class UserModel
{
    public static function getCurrentUserId(){
        try {
            $key = env("JWT_SECRET");
            $auth = app('request')->header('Authorization');
            $jwt = str_replace("Bearer ", "", $auth);
            $pub_key = openssl_pkey_get_private(FIREBASE_PRIVATE_KEY);
            $keyData = openssl_pkey_get_details($pub_key);
            $decoded = JWT::decode($jwt, $keyData['key'], array('RS256'));
            $userId = $decoded->sub;
            return $userId;
        }
        catch (\Exception $e) {
            throw new \Exception('Unauthorized');
        }
        catch (TokenExpiredException $e) {
            throw new UnauthorizedHttpException('Bearer', 'Token Expired');
        } catch (JWTException $e) {
            throw new UnauthorizedHttpException('Bearer', 'Token Invalid');
        }

    }
}