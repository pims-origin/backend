<?php

namespace Seldat\Wms2\Models;

class ActualReceiving extends BaseSoftModel
{
    protected $table = 'gr_dtl';
    
    protected $primarykey = 'gr_dtl_id';
    
    protected $fillable = [
        'ctnr_num',
        'po',
        'sku',
        'color',
        'size',
        'pack',
        'gr_dtl_ept_ctn_ttl',
        'gr_dtl_act_ctn_ttl',
        'length',
        'width',
        'height',
        'cube',
    ];
    
    public function GoodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'gr_hdr_id', 'gr_hdr_id');
    }
    
    public function Customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
    
    public function Warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }
    
        public function AsnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }
}