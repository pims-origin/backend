<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemUom;

class SystemUomModel extends AbstractModel
{
    /**
     * SystemUomModel constructor.
     *
     * @param SystemUom|null $model
     */
    public function __construct(SystemUom $model = null)
    {
        $this->model = ($model) ?: new SystemUom();
    }

}
