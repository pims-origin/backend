<?php

namespace App\Modules\Inventories\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
            'type' => 'inventories',
            'id' => (int) $this->id,
            'attributes' => [
                'whs_id'        => $this->whs_id,
                'whs_code'      => $this->whs_code,
                'cus_id'        => $this->cus_id,
                'cus_code'      => $this->cus_code,
                'item_id'       => $this->item_id,
                'type'          => $this->type,
                'sku'           => $this->sku,
                'size'          => $this->size,
                'color'         => $this->color,
                'description'   => !empty($this->des) ? $this->des : '',
                'uom'           => $this->uom,
                'pack'          => $this->pack,
                'on_hand_qty'   => $this->in_hand_qty,
                'in_pick_qty'   => $this->in_pick_qty,
                'total'         => $this->total_qty
            ]
        ];
    }
}