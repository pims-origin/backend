<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
    $middleware[] = 'setWarehouseTimezone';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {

    require(__DIR__ . '/V2/routes.php');

    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->post('/create-wave-pick', [
            'action' => "createWavePick",
            'uses'   => 'WavePickController@createWavePick'
        ]);
        $api->get('/print', [
            'action' => "createWavePick",
            'uses'   => 'WavePickController@printWavepick'
        ]);
        $api->get('/detail-wave-pick', [
            'action' => "createWavePick",
            'uses'   => 'OrderPickingController@detailWavePick'
        ]);
        $api->get('/order-picking-list', [
            'action' => "updateOrderPicking",
            'uses'   => 'OrderPickingController@odrPickingList'
        ]);

        $api->put('/update-wave-pick/{wv_id:[0-9]+}', [
            'action' => "updateOrderPicking",
            'uses'   => 'OrderPickingController@updateWavePick'
        ]);

        $api->put('/update-wave-pick/{wv_id:[0-9]+}/no-ecom', [
            'action' => "updateOrderPicking",
            'uses'   => 'OrderPickingController@updateWavePickNoEcom'
        ]);

        $api->post('/{whsID:[0-9]+}/update-wave-pick/{wvId:[0-9]+}/queue', [
            'action' => "updateOrderPicking",
            'uses'   => 'OrderPickingController@startInsertOrderCartonQueue'
        ]);

        // Wave pick Status
        $api->get('/wave-pick-statuses', ['action' => "createWavePick", 'uses' => 'WavePickStatusController@show']);

        // CSR
        $api->put('/assign-picker', ['action' => "wavepickAssignment", 'uses' => 'WavePickController@updatePicker']);

        // Multi CSR
        $api->put('/assign-multi-picker', ['action' => "wavepickAssignment", 'uses' => 'WavePickController@updateMultiPicker']);

        $api->post('/order-picking/checkLocation', [
            'action' => "updateOrderPicking",
            'uses'   => 'OrderPickingController@checkAvail'
        ]);

        $api->post('/order-picking/autoConsolidate', [
            'action' => "updateOrderPicking",
            'uses'   => 'OrderPickingController@autoConsolidate'
        ]);

        $api->get('/list-suggest-location/{wvDtlId:[0-9]+}', [
            'action' => 'createWavePick',
            'uses'   => 'WavePickController@suggestLocation'
        ]);

        $api->get('/more-suggest-location', [
            'action' => 'createWavePick',
            'uses'   => 'WavePickController@moreSuggestLocation'
        ]);

        $api->get('/suggest-actual-locations', [
            'action' => 'createWavePick',
            'uses'   => 'WavePickController@suggestActutalLocations'
        ]);

        $api->post('/check-actual-locations', [
            'action' => 'createWavePick',
            'uses'   => 'WavePickController@checkActutalLocations'
        ]);

        // Assign Multiple Pickers
        $api->get('/wave-pick/{wvHdrId:[0-9]+}/pickers', [
            'action' => 'createWavePick',
            'uses' => 'WavePickController@getAssignedPickers'
        ]);
        $api->post('/wave-pick/{wvHdrId:[0-9]+}/assign-pickers', [
            'action' => 'createWavePick',
            'uses' => 'WavePickController@assignPickers'
        ]);
    });
});
