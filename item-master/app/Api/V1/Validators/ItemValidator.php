<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:58
 */

namespace App\Api\V1\Validators;


class ItemValidator extends AbstractValidator
{
    /*
     * Define rules
     * 
     * @return array
     */
    protected function rules()
    {
        return [
            'uom_id'       => 'required|integer',
            'pack'         => 'numeric|greater_than_zero',
            'sku'          => 'required|max:50',
            'size'         => 'max:50',
            'color'        => 'max:50',
            'cus_id'       => 'required|integer',
            'length'       => 'numeric',
            'width'        => 'numeric',
            'height'       => 'numeric',
            'weight'       => 'numeric',
            'description'  => 'max:255',
            'lot'          => 'max:50',
            'upc'          => 'max:20',
            //'cus_upc'      => 'max:20',
            'condition'    => 'max:50',
            'status'       => 'max:2',
            'spc_hdl_code' => 'required'
        ];
    }
}