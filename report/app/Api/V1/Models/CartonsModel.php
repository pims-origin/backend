<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 23-June-2017
 * Time: 18:27
 */

namespace App\Api\V1\Models;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\LocationType;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class CartonsModel extends AbstractModel
{

    private $allowedLocationTypeCode = ['RAC', 'ECO', 'PRO'];
    /**
     * CartonsModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function search($whs_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        $query = $this->make($with);

        if (isset($attributes['getTotalRec'])) {
            $query->select([
                DB::Raw('count(cartons.ctn_id) as total')
            ]);
        } else {
            $query->select([
                'cartons.*',
                'location.level',
                'location.row',
                'location.aisle',
                'customer.cus_name as cus_name',
                'customer.cus_code as cus_code',
                'pallet.plt_num as plt_num',
                'pallet.rfid as plt_rfid',
                'gr_hdr.gr_hdr_num as gr_hdr_num',
            ]);
        }


        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query->leftJoin('location', 'location.loc_id', '=', 'cartons.loc_id');
        $query->leftJoin('pallet', 'pallet.plt_id', '=', 'cartons.plt_id');
        $query->leftJoin('customer', 'customer.cus_id', '=', 'cartons.cus_id');
        $query->leftJoin('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'cartons.gr_hdr_id');

        $query->where('cartons.whs_id', $whs_id);
        $query->whereNull('location.parent_id');
        // $query->whereIn('cartons.ctn_sts', ['AC', 'LK', 'PD', 'SH']);
        // remove status SH according to #wms2-6014
        $query->whereIn('cartons.ctn_sts', ['AC', 'LK', 'PD']);

        // Search Customer
        if (isset($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['rfid'])) {
            $query->where('cartons.rfid', 'like', "%" . SelStr::escapeLike($attributes['rfid']) . "%");
        }

        if (isset($attributes['plt_rfid'])) {
            $query->where('pallet.rfid', 'like', "%" . SelStr::escapeLike($attributes['plt_rfid']) . "%");
        }

        // Search Gr_num
        //$query->whereHas('goodReceiptDtl.goodsReceipt', function ($query) use ($whs_id, $attributes) {
        //    $query->where('whs_id', $whs_id);
        //    if (isset($attributes['gr_hdr_num'])) {
        //        $query->where('gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['gr_hdr_num']) . "%");
        //    }
        //});

        if (isset($attributes['ctnr_num'])) {
            $query->where('cartons.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['ctn_num'])) {
            $query->where('cartons.ctn_num', 'like', "%" . SelStr::escapeLike($attributes['ctn_num']) . "%");
        }

        if (isset($attributes['loc_code'])) {
            $query->where('cartons.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }


        //search according to from date to date
        if (!empty($attributes['created_at_from'])) {
            $query->where('cartons.created_at', ">=", strtotime($attributes['created_at_from']));
        }

        if (!empty($attributes['created_at_to'])) {
            $query->where('cartons.created_at', "<", strtotime($attributes['created_at_to'] . "+1 day"));
        }

        if (isset($attributes['sku'])) {
            $query->where('cartons.sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        //search more fields from Sku auto complete
        if (isset($attributes['item_id'])) {
            $query->where('cartons.item_id', $attributes['item_id']);
        }

        if (isset($attributes['lot'])) {
            $query->where('cartons.lot', $attributes['lot']);
        }

        if (isset($attributes['loc_type_code'])) {
            $query->where('cartons.loc_type_code', $attributes['loc_type_code']);
        }

        if(isset($attributes['row'])){
            $query->where('location.row', 'LIKE', $attributes['row'] . '%');
        }

        if(isset($attributes['level'])){
            $query->where('location.level', 'LIKE', $attributes['level'] . '%');
        }

        if(isset($attributes['aisle'])){
            $query->where('location.aisle', 'LIKE', $attributes['aisle'] . '%');
        }

        if (isset($attributes['upc'])) {
            $query->where('cartons.upc', 'like', "%" . SelStr::escapeLike($attributes['upc']) . "%");
        }

        if (isset($attributes['ctn_sts'])) {
            $query->where('cartons.ctn_sts', $attributes['ctn_sts']);
        }

        if (isset($attributes['upc'])) {
            $query->where('cartons.upc', 'like', "%" . SelStr::escapeLike($attributes['upc']) . "%");
        }

        if (isset($attributes['ctn_sts'])) {
            $query->where('cartons.ctn_sts', $attributes['ctn_sts']);
        }

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'cus_name') {
                    $attributes['sort']['customer.cus_name'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'cus_code') {
                    $attributes['sort']['customer.cus_code'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ctnr_num') {
                    $attributes['sort']['cartons.ctnr_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'gr_hdr_num') {
                    $attributes['sort']['goodReceiptDtl.goodsReceipt.gr_hdr_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'piece_remain') {
                    $attributes['sort']['cartons.piece_remain'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'upc') {
                    $attributes['sort']['cartons.upc'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'sku') {
                    $attributes['sort']['cartons.sku'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'size') {
                    $attributes['sort']['cartons.size'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'color') {
                    $attributes['sort']['cartons.color'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'po') {
                    $attributes['sort']['cartons.po'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'lot') {
                    $attributes['sort']['cartons.lot'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'length') {
                    $attributes['sort']['cartons.length'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'width') {
                    $attributes['sort']['cartons.width'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'height') {
                    $attributes['sort']['cartons.height'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'weight') {
                    $attributes['sort']['cartons.weight'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'cube') {
                    $attributes['sort']['cartons.cube'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'volume') {
                    $attributes['sort']['cartons.volume'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ctn_pack_size') {
                    $attributes['sort']['cartons.ctn_pack_size'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ctn_num') {
                    $attributes['sort']['cartons.ctn_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'rfid') {
                    $attributes['sort']['cartons.rfid'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'rfid') {
                    $attributes['sort']['cartons.rfid'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'plt_num') {
                    $attributes['sort']['pallet.plt_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'loc_name') {
                    $attributes['sort']['pallet.loc_name'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ctn_sts') {
                    $attributes['sort']['cartons.ctn_sts'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'created_at') {
                    $attributes['sort']['cartons.created_at'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'expired_dt') {
                    $attributes['sort']['cartons.expired_dt'] = $val;
                    unset($attributes['sort'][$key]);
                }
            }
        }
        // Get
        $this->sortBuilder($query, $attributes);

        if ($export) {
            if (isset($attributes['getTotalRec'])) {
                $models = $query->get();
            } else {
                //get part number
                if (isset($attributes['part'])) {
                    $part = $attributes['part'];
                }

                // get limit row per query
                if (isset($attributes['limitRow'])) {
                    $limitRow = $attributes['limitRow'];
                }

                $offset = $limitRow * ($part -1);
                $models = $query->limit($limitRow)->skip($offset)->get();
            }

        } else {
            $models = $query->paginate($limit);
        }

        return $models;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function skuCartonsAutoComplete($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('cartons AS cartons')
            ->select(['cartons.sku', 'cartons.size', 'cartons.color', 'cartons.lot', 'cartons.item_id'])
            ->where('cartons.whs_id', $whs_id);

        if (isset($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('cartons.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        //if (isset($attributes['gr_hdr_num'])) {
        //    $query->where('grHdr.gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['gr_hdr_num']) . "%");
        //}

        if (isset($attributes['ctn_num'])) {
            $query->where('cartons.ctn_num', 'like', "%" . SelStr::escapeLike($attributes['ctn_num']) . "%");
        }

        if (isset($attributes['loc_code'])) {
            $query->where('cartons.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        if (isset($attributes['rfid'])) {
            $query->where('cartons.rfid', 'like', "%" . SelStr::escapeLike($attributes['rfid']) . "%");
        }

        // Search sku
        if (isset($attributes['sku'])) {
            $query->where('cartons.sku', 'LIKE', "%{$attributes['sku']}%");
        }

        $result = $query->groupBy('cartons.item_id', 'cartons.lot')
            ->orderBy('cartons.sku')->take($limit)->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     *
     * @return mixed
     */
    public function customers($whs_id, $attributes = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        // Search whs_id
        $query = DB::table('customer AS c')
            ->select(['c.cus_id', 'c.cus_name'])
            ->join('cartons', 'c.cus_id', '=', 'cartons.cus_id')
            ->where('cartons.whs_id', $whs_id);

        $result = $query->groupBy('c.cus_id')->orderBy('c.cus_name')->get();

        return $result;
    }

    public function cartonCustomers($whs_id)
    {
        $query = DB::table('cartons')->where('cartons.whs_id', $whs_id)->select('cartons.cus_id');
        $result = $query->distinct()->get();
        return $result;
    }

    /**
     * @return mixed
     */
    public function cartonsStatusDropDown()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('ctn_status AS c')
            ->select(['c.status', 'c.ctn_sts_name', 'c.ctn_sts_des'])
            ->whereIn('c.status', ['AC', 'LK', 'PD']);

        $result = $query->orderBy('c.status')->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function autoCtnrNum($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('cartons AS cartons')
            ->select(['cartons.ctnr_num'])
            ->where('cartons.whs_id', $whs_id);;

        if (isset($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('cartons.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        $result = $query->groupBy('cartons.ctnr_num')
            ->orderBy('cartons.ctnr_num')->take($limit)->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function autoCtnNum($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('cartons AS cartons')
            ->select(['cartons.ctn_id', 'cartons.ctn_num'])
            ->where('cartons.whs_id', $whs_id);

        if (isset($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('cartons.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['ctn_num'])) {
            $query->where('cartons.ctn_num', 'like', "%" . SelStr::escapeLike($attributes['ctn_num']) . "%");
        }

        $result = $query->groupBy('cartons.ctn_id', 'cartons.ctn_num')
            ->orderBy('cartons.ctn_num')->take($limit)->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function autoLocCode($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('cartons AS cartons')
            ->select(['cartons.loc_code', 'cartons.loc_name'])
            ->where('cartons.whs_id', $whs_id);
        //->whereNotNull('cartons.loc_code');

        if (isset($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('cartons.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['ctn_num'])) {
            $query->where('cartons.ctn_num', 'like', "%" . SelStr::escapeLike($attributes['ctn_num']) . "%");
        }

        if (isset($attributes['loc_code'])) {
            $query->where('cartons.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        $result = $query->groupBy('cartons.loc_code', 'cartons.loc_name')
            ->orderBy('cartons.loc_code')->take($limit)->get();

        return $result;
    }

    public function autoRFID($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('cartons AS cartons')
            ->select(['cartons.rfid'])
            ->where('cartons.whs_id', $whs_id);

        if (isset($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('cartons.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['ctn_num'])) {
            $query->where('cartons.ctn_num', 'like', "%" . SelStr::escapeLike($attributes['ctn_num']) . "%");
        }

        if (isset($attributes['loc_code'])) {
            $query->where('cartons.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        if (isset($attributes['rfid'])) {
            $query->where('cartons.rfid', 'like', "%" . SelStr::escapeLike($attributes['rfid']) . "%");
        }

        $result = $query->groupBy('cartons.rfid')
            ->orderBy('cartons.rfid')->take($limit)->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function autoPalletRFID($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('pallet AS pallet')
            ->select(['pallet.rfid'])
            ->where('pallet.whs_id', $whs_id);;

        $query->leftJoin('cartons AS cartons', 'cartons.plt_id', '=', 'pallet.plt_id')
            ->where('cartons.whs_id', $whs_id);

        if (isset($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('cartons.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['ctn_num'])) {
            $query->where('cartons.ctn_num', 'like', "%" . SelStr::escapeLike($attributes['ctn_num']) . "%");
        }

        if (isset($attributes['loc_code'])) {
            $query->where('cartons.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        if (isset($attributes['rfid'])) {
            $query->where('cartons.rfid', 'like', "%" . SelStr::escapeLike($attributes['rfid']) . "%");
        }

        if (isset($attributes['plt_rfid'])) {
            $query->where('pallet.rfid', 'like', "%" . SelStr::escapeLike($attributes['plt_rfid']) . "%");
        }

        $result = $query->groupBy('cartons.rfid')
            ->orderBy('cartons.rfid')->take($limit)->get();

        return $result;
    }

    /**
     *
     */
    public function getLocType() {
        $query = LocationType::whereIn('loc_type_code', $this->allowedLocationTypeCode);
        return $query->get();
    }

    public function searchLPNReport($whs_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        // DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->select([
                'customer.cus_id',
                'customer.cus_name',
                'customer.cus_code',
                'cartons.ctn_id',
                'cartons.plt_id',
                'cartons.loc_id',
                'cartons.loc_name',
                'cartons.loc_code',
                DB::raw('COUNT(cartons.ctn_id) AS init_ctn_ttl'),
                DB::raw('SUM(cartons.piece_ttl) AS init_piece_ttl'),
                'cartons.created_at',
                'cartons.lpn_carton',
                'cartons.item_id',
                'cartons.sku',
                'cartons.size',
                'cartons.color',
               'cartons.ctn_pack_size',
                 'item.pack',
                'cartons.lot',
                'cartons.upc',
                'cartons.size',
                'cartons.deleted',
                'location.loc_sts_code',
                'loc_status.loc_sts_name as location_sts_name',
                DB::raw('SUM(IF(cartons.is_damaged = 1, 1, 0)) AS dmg_ctn_ttl'),
                DB::raw('COUNT(*) AS current_ctn_ttl'),
          //      DB::raw('SUM(piece_remain) AS current_piece_ttl'),
                DB::raw('COUNT(*) AS current_piece_ttl'),
                'item.description as des',
                'location.row',
                'location.level',
                'location.aisle'
            ]);
        $query->join('customer', 'customer.cus_id', '=', 'cartons.cus_id');
        $query->join('item', 'cartons.item_id', '=', 'item.item_id');
        $query->leftJoin('location', 'location.loc_id', '=', 'cartons.loc_id');
        $query->leftJoin('loc_status', 'location.loc_sts_code', '=', 'loc_status.loc_sts_code');

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query->where('cartons.whs_id', $whs_id);
        $query->whereNotNull('cartons.lpn_carton');
        $query->whereNull('location.parent_id');
        // $query->where('COUNT(cartons.ctn_id)', '>', 0);

        //Check on Rack or not
        if (isset($attributes['on_rack']) && ($attributes['on_rack']) == 1) {
            $query->whereNotNull('cartons.loc_id');
        } else if (isset($attributes['on_rack']) && ($attributes['on_rack']) == 0) {
            $query->whereNull('cartons.loc_id');
        }

        if (isset($attributes['loc_code'])) {
            $query->where('cartons.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        if (isset($attributes['lpn_num'])) {
            $query->where('cartons.lpn_carton', 'like', "%" . SelStr::escapeLike($attributes['lpn_num']) . "%");
        }

        //Search Location
        if (isset($attributes['row'])){
            $query->where('location.row', 'LIKE', SelStr::escapeLike($attributes['row']) . "%");
        }

        if (isset($attributes['level'])){
            $query->where('location.level', 'LIKE', SelStr::escapeLike($attributes['level']) . "%");
        }

        if (isset($attributes['aisle'])){
            $query->where('location.aisle', 'LIKE', SelStr::escapeLike($attributes['aisle']) . "%");
        }

        // Search Customer
        if (isset($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }

        $query->where('cartons.deleted', 0);
        $query->whereIn('cartons.ctn_sts', ['AC', 'LK', 'RG']);
        if (isset($attributes['sku'])) {
            $query->where('cartons.sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }
        //search more fields from Sku auto complete
        if (isset($attributes['item_id'])) {
            $query->where('cartons.item_id', $attributes['item_id']);
        }

        if (isset($attributes['lot'])) {
            $query->where('cartons.lot', $attributes['lot']);
        }

        //search according to from date to date
        if (!empty($attributes['created_at_from'])) {
            $query->where('cartons.created_at', ">=", strtotime($attributes['created_at_from']));
        }

        if (!empty($attributes['created_at_to'])) {
            $query->where('cartons.created_at', "<", strtotime($attributes['created_at_to'] . "+1 day"));
        }

        $query->groupBy('cartons.lpn_carton', 'cartons.item_id', 'cartons.lot', 'cartons.ctn_pack_size');

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'cus_name') {
                    $query->orderBy("customer.cus_name", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'cus_code') {
                    $query->orderBy("customer.cus_code", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'lpn_num') {
                    $query->orderBy("cartons.lpn_carton", $val);
                    unset($attributes['sort'][$key]);
                }

                // if ($key === 'plt_sts_name') {
                //     $query->orderBy("pallet.plt_sts", $val);
                //     unset($attributes['sort'][$key]);
                // }

                if ($key === 'loc_code') {
                    $query->orderBy("cartons.loc_code", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'loc_sts_name') {
                    $query->orderBy("location.loc_sts_code", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'sku') {
                    $query->orderBy("cartons.sku", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'size') {
                    $query->orderBy("cartons.size", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'color') {
                    $query->orderBy("cartons.color", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ctn_pack_size') {
                    $query->orderBy("cartons.ctn_pack_size", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'lot') {
                    $query->orderBy("cartons.lot", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'upc') {
                    $query->orderBy("cartons.upc", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'init_ctn_ttl') {
                    $query->orderBy("init_ctn_ttl", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'created_at') {
                    $query->orderBy("cartons.created_at", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'des') {
                    $query->orderBy("item.description", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'dmg_ctns') {
                    $query->orderBy("dmg_ctn_ttl", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'current_ctns') {
                    $query->orderBy("current_ctn_ttl", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'current_pieces') {
                    $query->orderBy("current_piece_ttl", $val);
                    unset($attributes['sort'][$key]);
                }
            }
        }
        // Get
        $this->sortBuilder($query, $attributes);

        $query->orderBy("cartons.created_at", "desc");

        if ($export) {
            $models = $query->get();
        } else {
            $models = $query->paginate($limit);
        }

        return $models;
    }
}
