<?php

namespace Seldat\Wms2\Models;


class ReceivingSumPlt extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rcv_sum_plt';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gr_hdr_num';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'event_id',
        'gr_hdr_num',
        'dt',
        'val'
    ];

    public function goodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'gr_hdr_num', 'gr_hdr_num');
    }
    
    public function eventTracking()
    {
        return $this->belongsTo(__NAMESPACE__ . '\EventTracking', 'event_id', 'id');
    }
}

