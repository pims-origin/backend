<?php
namespace Seldat\Wms2\Models;

/**
 * Class WavepickDtl
 *
 * @property mixed cus_id
 * @property mixed whs_id
 * @property mixed wv_id
 * @property mixed item_id
 * @property mixed uom_id
 * @property mixed wv_num
 * @property mixed color
 * @property mixed sku
 * @property mixed size
 * @property mixed pack_size
 * @property mixed lot
 * @property mixed ctn_qty
 * @property mixed piece_qty
 * @property mixed act_piece_qty
 * @property mixed primary_loc_id
 * @property mixed primary_loc
 * @property mixed bu_loc_1_id
 * @property mixed bu_loc_1
 * @property mixed bu_loc_2_id
 * @property mixed bu_loc_2
 * @property mixed bu_loc_3_id
 * @property mixed bu_loc_3
 * @property mixed bu_loc_4_id
 * @property mixed bu_loc_4
 * @property mixed bu_loc_5_id
 * @property mixed bu_loc_5
 * @property mixed wv_dtl_sts
 *
 * @package Seldat\Wms2\Models
 */
class WavepickDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_dtl';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_dtl_id';
    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'wv_id',
        'item_id',
        'uom_id',
        'wv_num',
        'color',
        'sku',
        'size',
        'pack_size',
        'lot',
        'ctn_qty',
        'piece_qty',
        'act_piece_qty',
        'primary_loc_id',
        'primary_loc',
        'bu_loc_1_id',
        'bu_loc_1',
        'bu_loc_2_id',
        'bu_loc_2',
        'bu_loc_3_id',
        'bu_loc_3',
        'bu_loc_4_id',
        'bu_loc_4',
        'bu_loc_5_id',
        'bu_loc_5',
        'wv_dtl_sts',
        'act_loc_id',
        'act_loc',
        'cus_upc',
    ];

    public function waveHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\WavepickHdr', 'wv_id', 'wv_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function item()
    {
        return $this->hasOne(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function systemUom()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemUom', 'sys_uom_id', 'uom_id');
    }

    public function buLoc1()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_id', 'bu_loc_1_id');
    }

    public function buLoc2()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_id', 'bu_loc_2_id');
    }

    public function buLoc3()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_id', 'bu_loc_3_id');
    }

    public function buLoc4()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_id', 'bu_loc_4_id');
    }

    public function buLoc5()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_id', 'bu_loc_5_id');
    }

    public function actLoc()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_id', 'act_loc_id');
    }

    public function picker()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'picker_id');
    }
}
