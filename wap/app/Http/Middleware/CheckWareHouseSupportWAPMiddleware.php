<?php

namespace App\Http\Middleware;

use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\WarehouseMetaModel;
use App\Api\V1\Models\WarehouseModel;
use Closure;
use Wms2\UserInfo\Data;

class CheckWareHouseSupportWAPMiddleware
{
    private $whs_meta;
    private $user;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currWhsId = array_get($request->route()[2], 'whsId', null);
        $cId = array_get($request->route()[2], 'cusId', null);
        if(!$currWhsId) {
            $currWhsId = array_get($request->route()[1], 'whsId');
        }
        $whModel = new WarehouseModel();
        if(empty($whModel->checkWhere(['whs_id' => $currWhsId]))) {
            $msg = sprintf('This Warehouse id: %d is not existed!', $currWhsId);
            return response($msg, 400);
        }
        if(!empty($cId)) {
            $customerModel = new CustomerModel();
            $cus =$customerModel->getFirstWhere(["cus_id" => $cId]);
            if(empty($cus)) {
                $msg = sprintf('This Customer id: %d is not existed!', $cId);
                return response($msg, 400);
            }
        }

        $this->whs_meta = new WarehouseMetaModel();
        $countWhsHaveWAP = $this->whs_meta->countWarehouseMeta($currWhsId, 'mod');
        if ($countWhsHaveWAP == 0) {
            $msg = sprintf('This Warehouse id: %d has not support WAP yet!', $currWhsId);
            return response($msg, 401);
        }

        $this->user = (new Data())->getUserInfo();

        $userWhs = array_get($this->user, 'user_warehouses');
        $whsIds  = array_pluck($userWhs, 'whs_id');

        if (!in_array($currWhsId, $whsIds)) {
            $msg = sprintf("You don't have permission on this warehouse id: %d!", $currWhsId);
            return response($msg, 401);
        }

        return $next($request);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handleOld($request, Closure $next)
    {
        $currWhsId = array_get($request->route()[2], 'whsId', null);
        $cId = array_get($request->route()[2], 'cusId', null);
        if(!$currWhsId) {
            $currWhsId = array_get($request->route()[1], 'whsId');
        }
        $whModel = new WarehouseModel();
        if(empty($whModel->checkWhere(['whs_id' => $currWhsId]))) {
            $msg = sprintf('This Warehouse id: %d is not existed!', $currWhsId);
            return response($msg, 400);
        }
        if(!empty($cId)) {
            $customerModel = new CustomerModel();
            $cus =$customerModel->getFirstWhere(["cus_id" => $cId]);
            if(empty($cus)) {
                $msg = sprintf('This Customer id: %d is not existed!', $cId);
                return response($msg, 400);
            }
        }

        $this->whs_meta = new WarehouseMetaModel();
        $countWhsHaveWAP = $this->whs_meta->countWarehouseMeta($currWhsId, 'mod');
        if ($countWhsHaveWAP == 0) {
            $msg = sprintf('This Warehouse id: %d has not support WAP yet!', $currWhsId);
            return response($msg, 401);
        }
        return $next($request);
    }
}
