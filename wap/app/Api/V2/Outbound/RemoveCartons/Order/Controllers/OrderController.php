<?php

namespace App\Api\V2\Outbound\RemoveCartons\Order\Controllers;

use App\Api\V2\Outbound\RemoveCartons\Order\Models\EventTrackingModel;
use App\Api\V2\Outbound\RemoveCartons\Order\Models\OrderCartonModel;
use App\Api\V2\Outbound\RemoveCartons\Order\Models\OrderDtlModel;
use App\Api\V2\Outbound\RemoveCartons\Order\Models\OrderHdrModel;
use App\Api\V2\Outbound\RemoveCartons\Order\Models\CartonModel;
use App\Api\V2\Outbound\RemoveCartons\Order\Models\WavePickHdrModel;
use App\Api\V2\Outbound\RemoveCartons\Order\Models\WavePickDtlModel;
use App\Api\V2\Outbound\RemoveCartons\Order\Models\LocationModel;
use App\Api\V1\Models\Log;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;

class OrderController extends AbstractController
{

    const LOCATION_TYPE_PROCESSING = 'PRO';

    protected $orderCartonModel;
    protected $orderDtlModel;
    protected $orderHdrModel;
    protected $cartonModel;
    protected $eventTrackingModel;
    protected $wvHdrModel;
    protected $locationModel;

    public function __construct()
    {
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->cartonModel = new CartonModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->wvHdrModel  = new WavePickHdrModel();
        $this->locationModel  = new LocationModel();
    }

    /**
     * Remove cartons assigns
     *
     * @param Integer $whsId
     * @param Integer $odrDtlId
     *
     * @return Response
     */
    public function removeScannedCartons($whsId, $odrDtlId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        $url = "v2/whs/{$whsId}/order-detail/{$odrDtlId}/remove-carton-from-order";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'RCO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Remove cartons from order'
        ]);

        //Check order detail exist
        $orderDtlCol = $this->orderDtlModel->getModel()
                ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
                ->where('odr_hdr.deleted', 0)
                ->where('odr_dtl_id', $odrDtlId)
                ->where('odr_dtl.whs_id', $whsId)
                // ->whereIn('odr_hdr.odr_sts', ['NW', 'AL', 'PK', 'PD'])
                ->whereIn('odr_hdr.odr_sts', ['NW', 'AL', 'PK'])
                ->first();

        if (!$orderDtlCol) {
            $msg = "Order detail doesn't exist!";
            $orderDtlCol = $this->orderDtlModel->getModel()
                ->where('odr_dtl_id', $odrDtlId)
                ->first();
            if ($orderDtlCol) {
                $msg = "Order detail doesn't belong to current warehouse!";
                $orderDtlCol = $this->orderDtlModel->getModel()
                    ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
                    ->where('odr_hdr.deleted', 0)
                    ->where('odr_dtl_id', $odrDtlId)
                    ->where('odr_dtl.whs_id', $whsId)
                    ->first();
                if ($orderDtlCol) {
                    $msg = sprintf("Unable remove cartons from order when order is already %s",
                            Status::getByKey('ORDER-STATUS', $orderDtlCol->odr_sts));
                }
            }
            return $this->_responseErrorMessage($msg);
        }

        $sku = object_get($orderDtlCol, 'sku', '');
        //Check order detail is have cartons
        $query = $this->orderCartonModel->getModel()
                ->where('odr_dtl_id', $odrDtlId)
        ;
        $collection = $query->select([
                    'odr_ctn_id'
                ])->get();

        $ctnRfids = $query->select([
                    'ctn_rfid'
                ])->get();

        if (!$collection->count()) {
            $msg = "No carton is found for SKU '{$sku}'!";
            return $this->_responseErrorMessage($msg);
        }

        try {
            DB::beginTransaction();

            // WAP-725 - [Outbound - Wavepick] Modify API update wavepick
            if (count($ctnRfids)) {
                $this->_updateProcessingLocationForCartons($whsId, array_pluck($ctnRfids->toArray(), 'ctn_rfid'));
            }

            $ctnIdList = $collection->toArray();
            $update = [
                'odr_hdr_id' => null,
                'odr_dtl_id' => null,
                'odr_num' => null
            ];

            $result = $query->update($update);

            if (!$result) {
                DB::rollback();
                $msg = 'Remove cartons from order FAILED!';
                return $this->_responseErrorMessage($msg);
            }

            $odrDtl = $this->orderDtlModel->getModel()->find($odrDtlId);
            $odrHdrId = object_get($odrDtl, 'odr_id');
            $this->orderDtlModel->updateOrderDetailPicking($odrHdrId);
            $this->orderHdrModel->updateOrderPicking($odrHdrId);

            // WMS2-4636 - Update picked_qty of order detail
            $this->orderDtlModel->updateOrderDetailPickedQty($odrHdrId);

            $wvHdrId = object_get($odrDtl, 'wv_id');
            $itemId = object_get($odrDtl, 'item_id');
            // $updResult = (new WavePickDtlModel())->updatePickingAfterDeleteCartons($wvHdrId, $itemId);
            // $updateWvSts = false;

            // if ($updResult) {
            //     $this->wvHdrModel->updatePickingAfterDeleteCartons($wvHdrId);
            //     $updateWvSts = true;
            // }

            //update wave header status
            $wvSts = $this->wvHdrModel->updatePicking($wvHdrId);
            // invert wave pick status when updating, 1: picked, 0: picking
            $wvSts = $wvSts == 1 ? 0 : 1;


            // if (!$updateWvSts) {
            //     DB::rollback();
            //     $msg = 'Update Wave Pick Status Failed!';
            //     return $this->_responseErrorMessage($msg);
            // }

            DB::commit();

            $ctnFilter = [];
            foreach($ctnIdList as $ctn) {
                $ctnRfid = $ctn["odr_ctn_id"];
                $ctnObj = ["odr_ctn_id" => $ctnRfid];
                $ctnFilter[] = $ctnObj;
            }
            $ctnFilterData['ctns'] = $ctnFilter;
            $ctnFilterData['wv_sts'] = [
                'wv_id'  => $wvHdrId,
                'picked' => $wvSts];

            $data = [
                'status' => true,
                'iat' => time(),
                'message' => [
                    [
                        'status_code' => 1,
                        'msg' => 'Remove cartons from order successfully!'
                    ]
                ],
                'data' => [$ctnFilterData],
            ];
            // WMS-4309 - Changing response wave pick is picked


            return new Response($data, 200, [], null);
        } catch (\PDOException $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'RCO',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'message' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _updateProcessingLocationForCartons($whsId, $rfids)
    {
        $locObj = $this->locationModel->getModel()
            ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->where('loc_type.loc_type_code', self::LOCATION_TYPE_PROCESSING) // processing
            ->where('location.loc_whs_id', $whsId)
            ->first();

        if ($locObj) {
            return $this->cartonModel->getModel()
                ->whereIn('rfid', $rfids)
                ->where('whs_id', $whsId)
                ->update([
                    'loc_id'        => $locObj->loc_id,
                    'loc_code'      => $locObj->loc_code,
                    'loc_name'      => $locObj->loc_alternative_name,
                    'loc_type_code' => self::LOCATION_TYPE_PROCESSING,
                ]);
        }
        return false;
    }
}
