<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\VirtualCartonSumModel;
use Swagger\Annotations as SWG;

class GoodsReceiptController extends AbstractController
{
    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var VirtualCartonModel
     */
    protected $vtlCtnModel;

    /**
     * @var VirtualCartonSummaryModel
     */
    protected $vtlCtnSumModel;

    private $palletModel;
    /**
     * GoodsReceiptController constructor.
     *
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param AsnDtlModel $asnDtlModel
     * @param CartonModel $cartonModel
     * @param EventTrackingModel $eventTrackingModel
     * @param InventorySummaryModel $inventorySummaryModel
     */
    public function __construct
    (
        GoodsReceiptModel $goodsReceiptModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        AsnDtlModel $asnDtlModel,
        CartonModel $cartonModel,
        EventTrackingModel $eventTrackingModel,
        InventorySummaryModel $inventorySummaryModel
    ) {
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
        $this->asnDtlModel = $asnDtlModel;
        $this->cartonModel = $cartonModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->vtlCtnModel = new VirtualCartonModel();
        $this->vtlCtnSumModel = new VirtualCartonSumModel();
        $this->palletModel = new PalletModel();
    }
}
