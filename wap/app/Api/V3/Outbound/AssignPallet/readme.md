#########################-START-VERSION-3-##################################
- WAP-724: [Outbound - Shipping Lane] Modify API assign Out pallet to Shipping Lane
    + Add more shipping dock location - for every shipping dock add location A,B,C,D (ex. Shipping_Dock_72_A).
    + Make status "Ready to Ship" when pallets are scanned into A,B,C,D locations.
    + Make status "Scheduled to Ship" when pallets scanned into shipping dock (ex. Shipping_Dock_72).
#########################-END-VERSION-3-##################################