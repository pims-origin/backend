<?php

namespace App\Modules\Warehouses\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    public function toArray($request)
    {
        $type = [
            'SH' => "ship",
            "BI" => "bill"
        ];

        return [
            'type' => 'addresses',
            'id' => (int) $this->whs_add_id,
            'attributes' => [
                'address_type'              => array_get($type, $this->whs_add_type),
                'address_line_1'            => $this->whs_add_line_1,
                'address_line_2'            => $this->whs_add_line_2,
                'city'                      => $this->whs_add_city_name,
                'state'                     => $this->systemState->sys_state_name,
                'zip'                       => $this->whs_add_postal_code,
                'country'                   => $this->warehouses->systemCountry->sys_country_name
            ]
        ];
    }
}