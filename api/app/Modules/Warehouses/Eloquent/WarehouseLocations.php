<?php
namespace App\Modules\Warehouses\Eloquent;

use Illuminate\Database\Eloquent\Model;

class WarehouseLocations extends Model
{
    protected $table = 'location';
    protected $primaryKey = 'loc_id';
    protected $fillable = [
        'loc_code',
        'loc_alternative_name',
        'whs_name',
        'zone_name',
        'loc_type_name',
        'loc_sts_name',
        'loc_available_capacity',
        'loc_length',
        'loc_width',
        'loc_height',
        'loc_max_weight',
        'loc_weight_capacity',
        'loc_min_count',
        'loc_max_count',
    ];
    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'loc_whs_id',
            'whs_id');
    }

    /*
    ****************************************************************************
    */

    public function zone()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Zone', 'loc_zone_id',
            'zone_id');
    }

    /*
    ****************************************************************************
    */

    public function locationType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\LocationType', 'loc_type_id',
            'loc_type_id');
    }

    /*
    ****************************************************************************
    */

    public function locationStatus()
    {
        return $this->belongsTo(__NAMESPACE__ . '\LocationStatus', 'loc_sts_code',
            'loc_sts_code');
    }

}