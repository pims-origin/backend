<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\CartonReceiving;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelStr;

class CartonReceivingModel extends AbstractModel
{
    public function __construct(CartonReceiving $model = null)
    {
        $this->model = ($model) ?: new CartonReceiving();
    }

    public function searchReport($attributes, $whsId)
    {
        $query = $this->model->select(
            'customer.cus_name',
            DB::raw('COUNT(DISTINCT cartons_rev.loc_id) as num_of_loc'),
            DB::raw('COUNT(DISTINCT CASE WHEN location.spc_hdl_code = "RAC" THEN location.loc_id ELSE NULL END ) AS rac'),
            DB::raw('COUNT(DISTINCT CASE WHEN location.spc_hdl_code = "BIN" THEN location.loc_id ELSE NULL END ) AS bin'),
            DB::raw('COUNT(DISTINCT CASE WHEN location.spc_hdl_code = "SHE" THEN location.loc_id ELSE NULL END ) AS she'),
            DB::raw('COUNT(DISTINCT CASE WHEN location.spc_hdl_code = "FRE" THEN location.loc_id ELSE NULL END ) AS fre')
        )
            ->leftJoin('customer', 'customer.cus_id', '=', 'cartons_rev.cus_id')
            ->join('location', 'location.loc_id', '=', 'cartons_rev.loc_id')
            ->whereNotNull('cartons_rev.loc_id')
            ->where('cartons_rev.whs_id', $whsId)
            ->whereNull('location.parent_id');

        if(!empty($attributes['cus_id'])) {
            $query->where('cartons_rev.cus_id', $attributes['cus_id']);
        }

        if(!empty($attributes['date_from'])) {
            $query->where('cartons_rev.created_at', '>=', strtotime($attributes['date_from'].'00:00'));
        }

        if(!empty($attributes['date_to'])) {
            $query->where('cartons_rev.created_at', '<=', strtotime($attributes['date_to'].'23:59'));
        }

        $query->groupBy([
            'cartons_rev.whs_id',
            'cartons_rev.cus_id'
        ])
            ->orderBy('cus_name', 'ASC');

        if(!empty($attributes['export']) && $attributes['export'] == 1) {
            return $query->get();
        }

        return $query->paginate($attributes['limit'] ?? 20);
    }

}
