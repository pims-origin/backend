<?php
/**
 * Created by PhpStorm.
 * User: Dung.Dang
 */

namespace App\Api\V1\Models;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQService
{
    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    /**
     * @param array $config
     */
    protected $config = [];

    /**
     * @var array
     */
    protected $inputs = [];

    /**
     * @param $inputs
     */
    public function init($inputs)
    {
        $this->inputs = $inputs;
    }

    public function __construct()
    {

        $this->config = config('queue.connections.rabbitmq_edi');
        $this->connection = new AMQPStreamConnection(
            $this->config['host'],
            $this->config['port'],
            $this->config['login'],
            $this->config['password'],
            $this->config['vhost']
        );

        $this->channel = $this->connection->channel();

        $this->channel->queue_declare( $this->config['queue'], false, true, false, false);

        $this->queue =  $this->config['queue'];
    }

    /**
     * @return mixed
     */
    public function send()
    {
        try {

            $msg = new AMQPMessage($this->inputs);

            $this->channel->basic_publish($msg, '',  $this->queue);

            $this->channel->close();

            $this->connection->close();

        } catch (Exception $ex) {
            var_dump($ex->getMessage());die;
        }
    }
}
