<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\PopulateSummaryModel;
use App\Api\V1\Models\ReceivingMasterSummaryModel;
use App\Api\V1\Models\StorageMasterSummaryModel;
use App\Api\V1\Models\OrderProcessingMasterSummaryModel;
use App\Api\V1\Models\MigrateStorageSummaryModel;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use App\Api\V1\Models\AuthenticationService;

class InvoiceSummaryController extends AbstractController
{

    protected $invoiceSummaryModel;
    protected $receivingSummaryModel;
    protected $storageSummaryModel;
    protected $orderProcessingSummaryModel;
    protected $migrateStorageSummaryModel;

    /**
    * Receiving InvoiceSummary Model constructor.
    */

    public function __construct(IRequest $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->invoiceSummaryModel = new PopulateSummaryModel();
        $this->receivingSummaryModel = new ReceivingMasterSummaryModel();
        $this->storageSummaryModel = new StorageMasterSummaryModel();
        $this->orderProcessingSummaryModel = new OrderProcessingMasterSummaryModel();
        $this->migrateStorageSummaryModel = new MigrateStorageSummaryModel();
    }

    /**
     * Receiving Summary
    */
    public function receivingInvoiceSummary()
    {
        //populating receiving summary table
        $this->invoiceSummaryModel->getReceivingSummary();
    }

    /**
     * Receiving charge codes Summary
    */
    public function receivingMasterSummary()
    {
        //populating receiving master summary table
        $this->receivingSummaryModel->receivingChargeCodes();
    }

    /**
     * Storage charge codes Summary
    */
    public function storageMasterSummary()
    {
        //populating storage master summary table
        $this->storageSummaryModel->storageChargeCodes();
    }

    /**
     * Ordeer Processing charge codes Summary
    */
    public function orderProcessingMasterSummary()
    {
        //populating order processing master summary table
        $this->orderProcessingSummaryModel->orderProcessingChargeCodes();
    }
    
    /**
     * Storage charge codes Summary
    */
    public function migrateStorageMasterSummary()
    {
        //populating storage master summary table
        $this->migrateStorageSummaryModel->storageChargeCodes();
    }
}
