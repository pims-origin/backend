<?php
return [
    'adminEmail' => 'admin@example.com',
    'authentication_api' => 'http://tyk-portal.seldatdirect.com/wms2-core-dev/authentication/',
    'menu_api_v1' => [
        'url' => 'http://tyk-portal.seldatdirect.com/wms2-core-dev/menu/v1/',
        'left_menu_uri' => 'groups/1/list_menu'
    ]
];
