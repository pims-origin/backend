﻿<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>Load Planning pdf</title>
    <style type="text/css">

        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 32px;
            font-weight: 100;
            margin: 0;
        }

        h2 {
            color: #364150;
            text-transform: uppercase;
            margin: 5px 0;
            font-weight: 500;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 14px;
            font-family: "Calibri";
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            border: 1px solid #ddd;
            padding: 20px;
            margin: 0 auto;

        }


        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #a9a9a9;
            font-size: 19px;
        }

        .table-style tbody tr td {
            padding-top: 5px;
            vertical-align: top;
            padding-bottom: 20px;
        }

        .table-style tbody tr td b {
            font-size: 20px;
            font-weight: 600;
            color: #282828;
        }
        .table-style1{
            width: 100%;
            border: 1px solid #ddd;
            line-height: 30px;
            margin-bottom: 15px;
        }
        .table-style1 thead tr th{
            color: #36c6d3;
            padding: 0 5px;
            border-bottom: 1px dashed #d2d2d2;
            font-size: 16px;
            text-transform: uppercase;
            font-weight: bold;
            text-align: left;
            border-right: 1px solid #e3f1f2;
        }
        .table-style1 tbody tr td{
            border-bottom: 1px dashed #ddd;
            padding: 0 5px;
            color: #595959;
        }
        .table-style1 tbody tr:last-child td{
            border-bottom: none;
        }
        .table-style1 tbody tr td.title{
            border-right: 1px solid #ddd;
            background-color: #e3f1f2;
            width: 35%;
        }
        .table-style2 {
            border: 1px solid #a9a9a9;
            margin: 15px 0;
            line-height: 30px;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            text-transform: capitalize;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 1px dashed #d5d5d5;
            border-bottom: 1px dashed #d5d5d5;
            color: #595959;
            line-height: 36px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }
        .table-style2 tbody tr td.title{
            font-weight: 600;
            color: #364150;
        }
        .table-style2 tbody tr:nth-child(2n) td {
            background-color: #e3f1f2;
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }
        .table-style3 thead tr th{
            font-weight: 600;
            text-align: left;
            color: #364150;
            text-transform: capitalize;
        }
        .table-style3 tr td {
            line-height: 20px;
            font-size: 14px;
            color: #595959;
            padding-right: 5px;
        }

        .table-style3 tr td b {
            color: #1c1c1c;
        }

        footer {
            border-top: 1px dashed #b7bbc1;
            margin-top: 20px;
            padding-top: 5px;
        }

    </style>
</head>
<body>
<table style="width: 100%; border-bottom:1px solid #ddd;margin-bottom: 10px" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td width="25%" style="vertical-align: middle">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALEAAABqCAYAAADz0LApAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpENkJCMTgzQzI4NjFFNzExQjMxMENFQUQ2QUFGRDlGOSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxMkY0Mzg2MjYxMkQxMUU3QTdCQ0VFMkZDRENFQjU4OSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxMkY0Mzg2MTYxMkQxMUU3QTdCQ0VFMkZDRENFQjU4OSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IFdpbmRvd3MiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpENkJCMTgzQzI4NjFFNzExQjMxMENFQUQ2QUFGRDlGOSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpENkJCMTgzQzI4NjFFNzExQjMxMENFQUQ2QUFGRDlGOSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PkfXjFgAAB2xSURBVHja7F0HmFTVFT5v2s7OLFtZtrBUQWkWBERABVFjwYaoWBBBRMXyJbHFGGNEP2MiSEBAIoJBIaKiRAEVhagoSlWUjiBlG8v2Or3knPvusDOz8960ZcPu3vN9x8V5dd7777n/+e+5dySv1wvChLVmkwSIhQkQCxMmQCxMmACxMAFiAWJhAsTChAkQCxMmQCxMgFiYMAFiYcIEiIUJEyAWJkAsTJgAsTBhAsTChMUM4oyrZ4fdyeLRw7CkfFjacwXUewxRX8SoccEuSxbceugOSNQ4xVMPsgZ8pqOTf4VFPVZCnTuhpS5rRO+Jfib3Pujd0Tuip6L7bkRCd6HXo1eiF6EfQt+DfhD9MPqJlnxeZ87cF/D/OgGh9hW00C9GH4M+CL0/enYc57OjE6J+Qv8WfQ16aUt/KQHitm969PPQL0e/Gb0vemIznTuBn5t8Eo/SazmYt6AfFyAWFq+NRZ/MI6+mBa7XGX0Kd4rQy9DfRi88lRfViPfcJo2owvvcr/s/vWeK+C+ib+CgNgsQC4vEKFFbyPnpLadJT0v3tAj9C/TbBYiFqdkE9PXoU5uR8zanDUdfij4PZAVEgFjYSUtDn80B0uM0v1ct+kM8Ko8QIBZGNgB9FfpvW9l9D0RfjX6fAHH7tmEcCBe14h7kdZ78aQWI25+N5PShexv4Lk+jvxwPkIVO3PrsCvTl6Blt6Ds9ykH8e/Soi3lEJG59HHhJGwOwz4jXvwDy0LgAcRs1Gg17Cz23DX/Hp0AeGBEgboNGNQqkr57fxr8nUYoZ6BcIELcyIxKolTygR/cqR6gb28njoDLQ13jPI0DcWkyH0C13mqHIkQw6BHKQXYj+53b2SKj245VI+bEA8WlgNFFgU31XWFXdB5I0joBNHMDadvhYxkGEo3oCxKcJnaAIbJDcwXTifvRr2m0HBfB39KRmA7GEj1cDsc/H00hiLl+U1pNz4fZsw3lDDov2aehnozeE2EZTTWYgMXE5vDo2147A7AWJurnfoWehOxWCC2XUVAw9C90dNHcsD/1BkOd5uf0+p1kINJfrH+gVIc57KciF3g4IFMVN6D+ir0R/At0QtJ0aK02leRW9pBkeLt3/w/w6wfdfyq9Tzz+j4eHb+fWDjb4/FcOsDrFtIn++7d0IxO+AyiwRHQfENJWTFCBnW0acbVnFeXB/p61Q4zbaQJ6ScmuYG/gTAt5NWfdfi0f5Jy0EgD8oHPMxuiXE5xkcHAMUjruLg/mPKvdDoKBSRU8cD5W6N6qPvVJhez76Yj8QT+ANVs1WK9yrMIDeHMjPqdEJQvmHKic5Wcjs8p4kFPRnSZiLH0N/24yJyvKKcyDfkcpkJLSu6PcoHEPRaia6NcS20SoA3gbyLAazQsTz2W3NwDHvUAGw7zv49wLhpKJ03wMlTuxH2ZppSg/2nW4XeOwN4LHVgdtaCx5yWz14nRiLolqyAftkD/J2hxXPZwEP/XVY5POEpJoS+9zrssv74zXpHjzWOvmvvZ7dl9dp5/chqSV5OrVIbOXd99U8kgXbUPQhyVr7ttfKLoQxaQcgW1cHdq+OCrA3gnIV1Tq8rUIC8X5rJtS4EiFNZ/FFzEyFY1bxcwabnke0UObh9++IQJIx8UbyU4wgOScGnipFcM5M7K3KDtoyiKr5KNu3nKrErkxIEoKlAaSERDB1Hwr69C6g7ZDBQOWqKgZ7ySFwnDgEksEEklYXBtASO47MkNMbJD3SQ2wcoNGApDGAo+yIDGZJc/LaXocNPHiMPj0XvSvozCmgMaeDxpAIHic2gPpqcFuqwVlZgF7ErqExhszjaEmBS9C/VAIx2Sb0dxUiJHXjD+ItTbZ6dPBO+bnwZM43YPOCE9/OfC6DBL+oOgKWUXLBPmsn2NrQBcxa9gDSVCgIcfIZCtsu540slNHL/iCKV3sW+gL08Qq0RY1G/BOiLzwvCrOdps1fk6RxvLWobAiYNC54NHsj1HkMmyT5u42KFcNuSy0kdukPmTf8Ccxn4WvSBLYHJwK5+rtlULV+IXg9dpB0BkUAE+i0iSmQNRbP1f8yBL6RgZgaQM2W96F05fNyIyAkYLR2Y6Q1ZJ0BqReOh6RzrgRDZveQ56dewlVxDBoObobabR+C5dA21qAkvdF/NwPPEb5UUyc8/MVaFb4FvfD+Wtzt46q+cMLZAfkta7WUlPwcYn/q2veSIvFNXXfYacmGBMnl6xbOUeHC2xTucRqPxqHsXYXkUs2u5T1CNPZHnqRFa5siiNRP4NNMoWf0cXVfAjBgAHB6ZYnJHQuFoK7b3GcE5D2wBMx9L2kCYNa9peVC5rVPQtbtf2PxH8Nm6GiOXb4GI2/u3a9C8tBbQJtE0dQEmsRkaNj3FQOw1423ieDzIr3wuByQcdn90O3RjyDjykcgIedMxQZCgNV3QrCPuBPyHlwGWbc8DxpTKjbAmuBdR3EqqiqxbUf/WuGpkBrxmAE5LXHbJeXnQ6rWSiyokicx/kaqwlziQlaPHuaeGAYpbF8pnSsaoYyowFKFbVdxD2VHQF7jIBajxLJPhPteBrHPnvic32e4aDw7QeM20qjd1CM3gcWrJ45Mazj8NWoOjF24Li0Hcu6cBbrUnLBHpAy9GdJG38t4a0CnygBswQjcAXImzJIbg5/V714PJe/8AXmxjYGUGo4OG0bne+ZDp3HPga5DdFPpqGGkjZwMXR9ZDimDrmO82Y/idAOFwY9gnXiByjVuQh+cgrTgo+p+sNuaxbo+tH/zjPxkZMTL/pyms8HC0iGkZPg04jv4ywplJI99pRClHleJwoviSIB6yI1N8dw+S0Z/CWKfcl7KOXs4m4T+AOUQm+q7wG+PXUfRmBo/9tNsQZIIzcu4afrFE5GHNs0p7SUHwVl2tMnnaaMmgx67fDlJ8wMwctTcu+dCh/MC8+G6H1dD8b8eBFddBWgSzAzA+o7dIG/qItx3TOg7Iy5eWwrOigJw1ZSwxDCUJeT2gZxJ8yDp7N+Au6Ga3YsfrUwIB+L1PHKEbLCk0ZJMVuJIgjXVfcDtZSevQv8v34fu6t+UZR+2pcO62l7Ac0469iGVJz9XQVWg7vtihWMIvG/GmbpfznVlta6eps8MifM6pOR8FsF+f0PQTqKh5x0NufCjpTN00NhdXnkBlPURQRi7cl1KFgLp6iDu6YSyT2fCsZevgSMzroHq75cHJkfJWZDUbxRSAZtMRwjACSYE8BzkwJcGAvinT6F42aN4LRcDMF1T0ukhe/yLkJDXNE7ZC/dA+SczIX/u7XD079fA0ZfHMM+fPQ5KVzwL1sPbmiSVklaPPckMSOw9lKkZvIcYFkp6DAYxgZBmztoUntEteKkMk8bJEjxPY9fzNtdYNqP/kIgR+svansiFc33R+gaVrnsjH6QIZVNUpJXmGrh4WkVhmRJGQ4/U6jgdKQ6zH0WZBVrJ+5gNk+j7joxFIOcidbOVeGV58ONIQKzv1AO0KYHvumL9AihfPRNoFVSPpQ7KVr0EjpKDQRGwL0gaHYuqWuSlufe8xpK4gGQRIykB0l1bxl+5h0XUzGt+hxz8kiYNp3z13yF/zq1QtmYmgnU747oejPYk9dkK90LFl29AwWsToeiNe8F+/EDA8dqkDKQxr7AehfcQvULJrKGGndcqZYFceL5T4prgPlsmKx/kycs69IUYqR359lSYh1w4TddAe+pVkihKWuYrNJrzOYUJZQc5jWkOI5pAXbYxBN14AZqv+Ibu+T6V5NlndB8zjBrXdTWeBLgX+fH2BorIjgqM0jRoskcdxV6MjknMT2qQCJjaLStYdJO0CSBhhKVu2lndtE2RJKdNzkBeiwDu21QY0WBS13nKQug09hkwIH2gBmHuNQRSRwYKWwS6kuVPQtknrzCQa81p7J6Y8oBJJjUW4sDapDRseE6o/XENFC6YCLaC3YENK6sXpI++DxsEU1B968qFBTHZHFCe6zQNI0XHUlcSvFk2GMwau0/gfxwP+JToBnFgq1fvi9NjVCjBdyqJGb2wVIVtn0YQ1aIx6i+fDXouNEKUDc1rn6A/EmZAhvWm+CwXICceSEvpUqJHETlFa6PvfLdaHkDduquyAOp2fAINe79C/xrqfl4rZ/uk3Tqt4K4rB0NmDzBk9Q7UOPd9jRzYDLkT54DpzOGhz4/gM2T3go5jHocuDy3Dc/SAlBETGCD9rXztq1D93XIGXqYpA4SAlJc1OrpnirqOymIGfFdQ4+owcAyjSBzIfSMF8UYFuQs4LbirAyZ462vOgG/qegL9G29nV4LkriUt+amCK9noniRTgSdCkXE/Ka4+xOcX8JelpCcvjgFA1jC68FN+kf9hrksqmU2FcoWzxTzChzPKyv6VKLm61mFEnorUYjMmfNjz/UAJoNL1JUMiGzgofuu32EVPhcKF90LJu08z7ZaAQKBLHXYbUoX5TGLzWQ1G6vo9/4XO9y9uQguUzON1I3XpyXTgYA5ctWEJRl5T4+BHBAmp1tiBUY6y1TPY/Z7k63jf5v6j5URQajpQpnQFC3/YStH4Hg14051eLayoHMAAa8RkrsGjhweO3Mi0YUruQF7MbrjCOYjP/kdh2yMqXHgF+q4Yu/N5YZI4ir7Xg3oROn2xl+Pk46RWrI5gv3PxBbxI+nGlywQvFY9kAYNH9PlKdIKAQ102/fUi0LTmZMi5azZ0+90HTLvNnTwfjHmN1NJdVwYVa+dA1rjpkDTgiqZfGLfb8neCveSXk6N2LCoc2gr6lGymYPhb9ffvIs2oCh6wiAjIGgQy9Ry2wsBXnNRnJGhoVNHtMkUKYp90tllhGz2BK82Y4H1Y1R+eyL+aFXbvtXZCAPegbs83wvWoyvlnK1ACGlG7QuGYygjlqlBGavuMMIMPZ3O9Wk3gXMrVBmMcILbwnmZdBPuSNHmXWeuAg/aOsODEUBYgJFk12agsumrYAAcNYkj6RDB2PYdJYASSYDvxwV/A1HsYpF4c2PnYCndD8ZvT4Oism6Bg3h1Q8OptcOwfY6EU93dVHwfb0Z+QPwcGRhdSlfrd67BHMEdZl+GjQwZsANVQvzdwyMKQcyajHB6X3RgNiBs40BR1TbxFDWbO8EHVAPh9/hiYXnQZpxaMDQ9RkaYOcUVD6aVlqUThnTECx8gHYogq1IbRhZXsOI/W9mZI+Kq4+hFuIITe0QyMqUzdeb54NBy2p0GCxlXFv0t1ZEEudOGe5eBm5MIbIP2ywBWlGg58AwXzJ0DNtv8gxy6UFQXk1faCvVD55UI4Nmss1O5YDQmZgaPwjuP7wVVzgvHc2AyxozVAw+7/svOQwkFO3FqX0olohiYaEAPv8pQiF1WVjWRCudYKH1X1gyJHin+55Z0qXHgxhK4PpeGlySoy1cI4QENhIZEPrLwQ4zmeAbk6r7nW2i0AeeTQEWY/atTPIpC19Psnr5dewJ6zVx7yf0W9CWgYBSAq4G6oIt0rqOt/B0y9LgxI8uzF++H40seQRlSwIWaiBUzZwChJnFuDgKJBDldtBWgSAyO7q74KA7AnrtekweTSVrSXNZRjM69nXvT6PXi9clarEWzhVgCycqUiVM0AHfskKQwYeR1BPygzIkxiVKPw+WPoXRS2reIAjMd8wvYrvJe4NYpjZzbD4IpS75Ibptcjo5LYj3XgeY8o225LFvQyVlA+MpcnpANDfmEEn8daD0WLp7GBCVIlEnsOQh8M+tQc5LWboeO1TwRw6sov5oGz/Bhoadg4FCUgRYHJdRpWVxFIoJ0x0YhAfdmFPDsZOpx7ldyAJAkcJ34FW/E+po5EC2Kyb3j33yvENorElMquD5GYqfUnk0CuY/YHc3dQX4T5vWYEDj3lv4Fck5EcYVL4Mpw6m8cDxfgw+92qlTzvlTqTYFn5eTCz66dQ7U6s4br+QJk1uANAJCd4ch0wacOWmi1IFb4FDYKDCnjoc9J7TyoOLhvYTxyWI543TDhwezA6lgUB0B0niGUZUJ/XHzrd1Kh6klxY+/PnEKqyNRL9g7r9fypso+75oSB+SJpwuMJzktDGBn02BZRXt1kHEQ67RmE7OD0IVyXm4vuVnUIQu3mvdijMfoNJetPQOL7O5qMUZB/45EOKWhLV+NJff3mLFAvM7ikaa01pjBqQXkyRjuhB47e1g8dWK4NfDcW8noAVtAd87oo/vnhcYD4rcBCVaj4oyuN9eWIBMXAu+ovCthv8Ejgdj8IdIjjnY37ckp7iOIX97LyrtZ6iCBhOc57N9exTbfkRyG40t2+EEanb5vqurFY7QZYyt2H0O0DdcMdrH4O8+xajvwF5DyyGpAGXstkXgejjMhxxXOSf/hKZ1yvJibkUNmBS3MZoHiivua01jA7467zRmAcbhTYlG5IH3xCQlFoObgIv9SgajS1WEFNSNVfl64z1E+gvjfCcA/wiNq1u01dhPxrV++IUAYfe6F9UFAKSGF+ClrPNoD7/j97XWUbJCdvr8+CALdOnx9N/dhNwEnsMBhNGMVOfkWA68yJWEQYup4Kk7AUtRWI/ELNh4bCzPOiKbmwERkjsHkjFXTVloEWaQg2E6jj8KtAiA7GtDlKHjcdEs5G90vC49ciPoKFCfK/XGiuIyejnnH5Q2DaRU4HJEN3vMVBCQhVuSrW6Ts5FXacQODRo8acQ16jlVKmyBUFcBuGHpDMpUho0LtD7r1MhSUVe7IZJlgpIelJyZM04FDVAkCVk9w7QjilRIxXDw+a9qWDYUg3Jg24AY5dzAhIy69EfIf3SKdBp3F/Y/zO6ESGQ6brJg66HjCsCCx7rtn0E7voKX1JXEQ+Iq1W6VaoxoEGIe6J8aaN4hO+nsJ3mwn3VAuBZHkJ5mN8Maki0Zowg2Q4NLUnSEogdRXsDPjb3uxT0GV15OWOjQENDuFTTkDLijkA56ugOtn/6JRP5RM56ptOyCaL4l46jAQ1z34tZEZD/jBHSiJ0VhQyIKUNvgezbXmRRnQDv9fUGBGgGagl8iShrOAhSc79LIOe2lwIalb14H1R+tdh/ZsjBeEBMthKU5//fqiKPlSlEUwL/XQpKhpeDy9FCACJasYX/e2ML0wifnQvhi/SPE2uldUBo2F9qfFpdJK0B6vZ8GVBsrkvOZGBjo10NNQxQpFKQOpF5/R/A1Ht4wCOv2/4RpAy5EbLGvwRZt7wACZ16UftgxTckq+nTOmOkvRc6T31DluD8ddOtHyJ9OQsMuXLVbeqICWx6lKnnBaDRkdRXw0bjPJZa9m9WlIQgTsg6AzJvfBry7l8C2uROjZEZgX3i/Wex0ZQhiBN8tOmnWCQ2f6PsmX5n4bkojiEi/jDvmi+J4jgqOXyrBQFUwpPNN3hyWtfCAE4OodgEGzXoXRb2g+aHYKCpGGxeHVeJvOcTZ7Tm74TqjW9D+ujGhXNoVgZxzIb9G8BRkQ96pBhU5GPsEliaSxVv1mM/Q6eb5debNnISJJ9/LdhLf2XlnJrEFNCn5wUUDp28sdLDUL35fcgeH9j2aTi7yyPLwY49BGnStqJ9bDoTVctR8ZCp27mQkDegSYOgianFS38Pll++Y3PueAdEye/OeEHsUyqmQuRLb37PaYguShC/0cJ81HevN6ooMafC6LnQklUvQPh1eakEcytF4X7GMuiWUAVlLjNFY1pWoTub8o7RsuLzuZDYbSAkntF4OpqsSa6oI2K0O/H+M5BywU1slofPCFymMHPlKLqXvPsUS+pqvl8Oph7ngy49r7G7NyRiwjmIuYxQD+fpSty4Gko/+DM07FrPRwRPMijqKYubA8THOV+MZAIjhf9Z/N8f8ZsYGsFx9LvAS/8P3bm3GQDs5QnpzVx9cSskrHZOHYiCkSiaHsG5vyZqRlWDVGfsaqQTNEjCSCPpvlSoTlEsdxKCuXv4dbkpGSx550k23EzDytGYrWAXnFg5HSPmZtB1yMBo/w0UvHY3ZF77OCSdp7DKggqAqRy04rPZYDn8A2hNKcGb14WSWmP94RlKgmhw4oww+5E09in/N4mVNNz7HoRXISlJrILWa8T/rwDlOpBYrB658JxatxEGmwvgwazNQHXGIC8scr2fbgZSghmclYVQuGASpF9+H6QOnwBac9P5BcSdG3avh/LP5jAAU9QtXzMDnOX5eNw0tlaEYnSqLYWabSuhcv3r4MR/k6zG8InAs584BIVvToOkvpdA6kV3MUqhsCgKvxEX0oz9ULXhTaj9YRWbFaJtpBD+wXOjUlcWi5GO8xnnumr8bUHQnazh0fhCleOoKGYttG6TILqFWcLrpyBNr3KZdg4yF8GiHishXWelBR71kpyA5gZ3BjTTgiZ7lv3nr1CzZSXy4qvA2LkfmyJEBUHEYSnqWQ5tZQMfvkIeSqCqNy5jNb00QTTprItBl5rNlAxSGCjZoomddTu/YGCnyO8DsK8RUTE8qQ71e76C+n0bkHufDaZeQ8GY158VuBO9oCIhUkycyNEb9n+LvpHxbgnBLqsTTUSYjaAwohnPT4D9kysLKVHIY9QVvB0GxBSpi0CYbzkrqHUnPJeis82Z120VXJhUALQcAgLYwFdguknxeB1VnqWCs+wIi7Zy0Y6eKQK0vBQNLdMwdOBBGhZNSfaq2bwCareuZKAkjZbJYaR8IJglQwLbT2lQhM2j42C05e9igxV0PwRgdg80YRUjLi11xfbHa2iotwh9PurZ/g0KA0HxgHgPB+QjCtuJ04aaevQuVwHOUJDiFkB8q1a2CfN4JeS9RhtG4Bm/STk4/c6Mn+CqlF8I0GD16DIleTj8jkgoOhXF08gcDd9SBCTtVqs3cIVVuUpNm2hgpZtU1MOGkilxJKmL8WYpgkIfebvcULzy9elcLpfcV0l87TWJ34fy+WhE9XO1zDgeo8VLqPIsOH2ludfLFI6p4onhrBDbqLUdjrMbV6phNjQzXTCEuY4u1hNjlK3USZ7PxqXvfm1M6oHvRyf/Ck6PBspdpm64/SpJVocGRZdrylE2oCgIIgAhKxzSKJ8z2uvHdq7VoDKnMV4Q7+SR88GgOyGepjbj4C0uZQ3gUZe+WaQr5YRLqI6GALLE5anmWq7eyc9nDTqnxCUgL2+sRXxfN//Mxf+6ec5Ax9PwdgXvhY46vNpfMnWWn2d2XWO6qMOx4Yka1+gTziQqJOgtyfUlae2sU6LnOTecRhmvUbH4Kg5Gib+k3WGOqeR8Opu/WC0HfX6c95LPZa3gftK3UrytmR5sEZfQtArXoUpx+hmrdzhQnfz5+Fa4d/DPmoxGkvrwaM5GuDlj98CD1swlNW7QSNCubREPTKcUxBRJtscIuPxm/sIEin0t8GDpOvvD7FMCUc6I9oEVIy9UOs07EO3T8bPp7RjA1NO/Gm4n8etJp5E5vRrINdTA8KRjbDCDv8Dv2vEjeQZC/3aLAPHpDOIcfT1TIWhZXE6xnm+nj4NG5yIaLxAgPo2Mfq9jiLkQqMBHaqTaNOo5r509il9BHkhzChC3MtNKXpiSud23ZK6/UdH+9+2lQwJ5ksQvkTd+YaeVWTx6peR5Ko9Qbd2eA3mZLhAgboXmZj+xpiio0ZSNu0F99aLWblR+OzN6GibstDD6xdWHszZBz4QKVmKpYKRU0KLf9jb4CKgcgSZOOASIW6nRqvsd9Q1gYL/ToWo0gEI1EyVt6Osv43TJGcvBOgGf08dc6nTC32iuI410LoHWPwz9L5DLFmIeTRWRuPUaDfXTnLx9rfT+iRLRlKwpEGc5gABx67YNIP/O9Put7L6peIpqZ2ixtbiLsgSIW7/RTBiaY0e/eFrZCu6Xyirpp9dWNNcJBYjbjtEqnzQz8+PT9P5oahFNhqA19w4054kFiNuWbQW5RJS48pbT5J4qOfelNatnxapACBC3LyPVgpZHGAXy8O0P/6f7oEkBtEbJbzj3LT1VFxISW9s1yviplJOmfA3h0Zl+U7DzKbwmrZpEPx1Hk31pcfb9LfFFBYjbvlE97lruNBeSZr6QokHLWdJ0p3h/QIcGXah4fRPn4zta+gsKELcvKwd5hjo5rVVBvzZzLgdzHw5ymp/oW52TAE7TqnxzAim60wz2Yxy4e3i0PXgquG6kJnm9XvFqhbVqEyAWJkAsTJgAsTBhAsTCBIgFiIUJEAsTJkAsTJgAsTABYvEUhAkQCxMmQCxMmACxMAFiYcIEiIUJEyAWJixm+58AAwBHKKuQjvsn6AAAAABJRU5ErkJggg==">
        </td>
        <td class="text-right">
            <h1>Work Order</h1>
        </td>
    </tr>
    </tbody>
</table>
@if($workOdrHdr)
    <h2>{{array_get($workOdrHdr,'wo_hdr_num','')}}</h2>
@endif
<p>
    {{--<label><input type="checkbox" checked> By Order</label>--}}
    {{--<label><input type="radio" checked> By SKU</label>--}}
    {{--<label><input type="radio">By Charge Code</label>--}}
    @if($orderInfo)
        <!--order: Checked-->
        <label><img src="data:image/png;base64,
        iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjE1N0U5QjkzMDNEQjExRTc4RDQzOTM0NDY1RTVDNjE4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjE1N0U5Qjk0MDNEQjExRTc4RDQzOTM0NDY1RTVDNjE4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTU3RTlCOTEwM0RCMTFFNzhENDM5MzQ0NjVFNUM2MTgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTU3RTlCOTIwM0RCMTFFNzhENDM5MzQ0NjVFNUM2MTgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5z41MFAAAJrklEQVR42tRaC3BU1Rk+r7uP7CYBJAPySpFmINIEIo2V8tAW1EIRcdqhWJ3W0WltccapFqE6pQhMQYtjh+lD+pgR61imYypT5GEtYAWhFFAQAiZNGBIwBEgij5Ddu3vvuaf/fx+7d0MS8lrFO7N77p57/3P///zf///fOXfpzJl3EMuySCyWKGeMTSKEMEoJVYooX0ugtY/2fR3cm01ZbEFd60gkEt6D/QK/WltjSw1DLoPbOfkcHNSx4oW8vOhTTNeT5aD8cujjcEHCx7OZZLbdGppkT5baiqOO+CuRMBeB3tMFWFIOMw932xc4uMnUNLFVKVXP4ZDSsjhnDP3GAFyWpeyBnMM+xz64xph3b/u2L7LYCsGJacqhUsq5IBiAj0S0xOPx6QIuUNda/EpGIqGHwuHgBjCAwENJ5kMzD7wHr/lbT6a/ZNP9hOh6YnY8rr8OXWEHSkwwtA5nA4MXzt/KyQltQE+AMHpDQMvgt4bX3ZjhXgvX/K3wyfSbLLbwW0P0hEKBreCsjR6+wDgLXaS8WQGrT2JGcg/mWoYPtdxW+fqpryXt+vpbFlsByhNN02qUm5oQlgg/WxY7NY1rzm97EKuDlvrarhTIlmzGgZPP0jNOscOyHIdYnQx2rdmjWZYlrkecE87RA568Ihj1btDRLmbiWq5nWZT1JtcOcPSA8BcH9IB7n/osYEOVhKA0LMCzVIxDcocP4+1lvQDGCafCCQgvXTHqC+Duzl5Xru+2LCqeDOfLS8PG5SX5gJJkNP/ciGNbakUyzsAI5s/AachLhemKemP5MlBPXG/1ATYp5WUoLCunLfpyc+Ftf4EyVQz+uHLxhptXlr6z+lfUkliC25VsyLUCYgCCVnljuhmIfJqwYdIEDag8Omvxl5pHT9tEEsliKnVCjES0adS0588VTikXpu6Nh3YoD/ImiDLO00Hu5ddPK9tQVD7MZeXsJWPOD5i+iepXbqTECUOqHDQ0jygvkjygPF7qqejEANQBoBLtgjzlgexmG6iihDJ5fPJjIxqLvvEmlfHRxAOD/bGHk4MbDh5nlul1+CBk0w2F9CEVAz7akvUiJYwYBmxBw6i7N5GLsWJClW8ewT4tTG764JXFw2v+eViKoPAVMs8HNkdiGMne83AR0Z5zZaNIMZmUeqRgQPUtj24EBlZGbXLp3Ult5Ucd/dvyovfXvyi5rbw3AfbCxkd9CMSASBUyJE6+QpaVIoXKm4FI5MOvL624NKRkCjUTPtig8hEy8tjfXyje99KzlghyQI7/uTjJVrp2wQXTNFPPtZwja7Cx0yUPBj684+d/vTB80gyavOKDjaP8sOrN64r/89unLCa4olfxsgwq4RBqXwwglXBT6dWuV6pPsKGWCdyXs6O3L3mlpXDyXJq4konoQIQMOfGvV8e/9+JCUByLV0cFlTiLmVQmUik6je7wkTmf66Ggm7oBaU1yM4Ftj2EDhUjBitw6NvWnfzg/ZsYCW/kUbODGYJQMrtv1RsmuNQ/bAzFBO/E86pnyADI34afT9vwz+9x5sLIg0Rnm2dG3l58ZM3PYjSd3Vhac+u8JLhPcYlq3YIP5HNKgVTnlibWNY2c9QpOtGVlbBaJk4McHtk14Z/WD8CxpAaPvik6DmtJPp4WfTjshoOytC7gTls+GrLr10dX1ExYsJjDzTV+YcmF49eYnx+7/83qRvMIsHqBdwgaVlwlZ9ZWFv2wYP+9xmmzLKDcIm/zGw7sm7lj+HW7G4zAev0bMdZ9OwyybTaMmT6gvnf8zYugMswV8BjaMm/fysa/+ZLEZiNoZpVPYwARwQ5f/m/TI0/WlC56xlScqzWQCOSS3qfrAxJ3L79MSra3dUL5DOs06o9MKoNVUPLnALnDEq+TgHaONnBvzteffv3PlmljuMAXxYSJqMx6Iyptxs7bswcdPln1vFTXibji5DeT5yIW6o2Xbf3FvKNbyiZ0uu5/lMug064xOU0vR4Uff+oiqRLPyqrhyQEhNnVwaWrro0Ixl6y/fUBQC7BpEeUYoKpJt5smS+Q/Xlv9wLYF7ibLS88c1Em49Uztx+7J7wq2NjVKEeA+K41V0mnVGp4FAifxzVR+P+eC1ByDZfgKZwceSHCPaBo3+/sG7VlVcKigeyKWdqWzlT9187/zqW3+0jsgEsckZTXFJME87XVhZMSevpbZeamHew+LYAzpNwQeEi5sOvfb22AO/m8WJrFM8mNrUtFtpECM84JuH7nx26/nCySN5Mo7Kzzk+9Yn1MOsapM80RUDoRKPnC2J7546o2lZtBCK8F8XxKjotuqLTCss4F6LwyBv7ISDvqpn0g4pkKL/UxjTAyt5tBaaYDOTfdmTa09uGD3177elxs9eA/8K28j5uC1X24qC6ffeV7H7+MLMM5qbLntKSntNpxD+wQT7y+Js1pdtX3J1zueFdzN02rl1PwFoW6GVg/KmSb/0R8lc+/k7DBjcBI22D2g5+G9LlXnG5FZXnveRUvaXTlJpahA8+c/Bs6e5V9+S21FSoYG6GEfaQhu7GsTsCnkaiyUHNBx6YWLFsh9BjDAzlfeBUfaPTkPv5gIbK1tKdK+4fdHr/S44RKsOIjEIlcmTexY8eKt3z3D+0RFtHyveUU/WVTiuGRkQv1MuSd59bWHBy1wqspqk7/bCB/jy96sdlG5/ZEGxqYVILctL3/aL+oNNAALUcFoy1MDBiGVCLx1QgLHEPJ5VtoMrmnT/2ZNmWpX8KxVtoB8r3dtOgD3S6nRFQPakw4rx4729+P/rwq/cD327EWVeB3LZIvG7JxN0rfx260ESlCIt+3Ga8ik6Ljui0m2uvuecD2URBuuRFB19+Pa+59r14ZOgtKsBPDKndXRW+fJbKQEg4kd67/aIOWtkjOt2dQmNvAUIoDanb3Qi0eYvCsSDHS9FvsOklne5RiYeyDWzSR6/7tM3YL3T687I7za6n3ek+02khhHI9cD291GhPp40O6TQ2hmEOdQmdeR3BxpO1UDdQusiZZIdOC2cXwvEKXJwTjydmO28DqQELK0K7flctO2s9htFfsqh8W5s+PZk052Gax4kHyBM3jdpGoNXhWEyv0PXkxkBAq/G9LZQduNX3XwdK8ZUn5micJOdVqZL9I4vLQSWBMXwREALK01zHSIUpx14PHPalJxQMQy79bjyuX5f/kXB1RCMB8sYeBnDZEwwG1ih73a7sC+l1TW/++0CzJuvo5vwhBRCyLi8v+m97oZubm7M4GDQ363piKkBKw2qGKzXEmK+ldt4Fv+F1bJUb8em/zzjr6izKIlsw4XRfTk5oB8r8X4ABAPwImSG0Ha5cAAAAAElFTkSuQmCC" width="2%"> By Order </label>
        &nbsp;&nbsp;
    @else
        <!--order: not Checked-->
        <label><img src="data:image/png;base64,
        iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjIyM0JFMkEzMDNEQjExRTc5OUQ1RTdGQjE0NDhDN0YxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjIyM0JFMkE0MDNEQjExRTc5OUQ1RTdGQjE0NDhDN0YxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MjIzQkUyQTEwM0RCMTFFNzk5RDVFN0ZCMTQ0OEM3RjEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MjIzQkUyQTIwM0RCMTFFNzk5RDVFN0ZCMTQ0OEM3RjEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5ei/CqAAAD4klEQVR42uxaTW4TMRS2PZ78tln3AHAC1B2qqNRsKoRYs+IQwA4h1nAAbtAFSN2gikURbCqxRGy7QJyhamaSGdu8b2xnnDQpUZsIVxpLyZux/Wbe8/vx57H5wcEjprVmo9F4VwjxgDEmOGfcGGYCyohWZb5uQd9N8oKSuPpXv989Q73E38XF6HVRqDfUPWF3oHCrxfvBYOulyPPJLgn/luoSalD08zqzWbrSo9nmeHklOGTE3XhcviC59yRpsksjT72rhoTMVKapPDHG/EmoKKV1kggBuwlyLq1N9SBbqmvUUZsQvu88vQ0vqJQJK0u1o5R6Qowt+il4S5Zle5IauNMWf5N+v/O8220fkQKMXspmXzpb0AdtIfU86+Kt6xnL8/FhluUfqaprXUlIAe0wGgheuv7S63WOYAlihjUkUUH3KdpdzCSeUltIZcCzNl5Quk/hPZ1O64SMdez9i5TTMJHxo0Ja/0ZGckU4zfBS7agJ6nlA2VzdunlBJQnP0jQ9Ny41wS3hfhUvKtM0Se199RC9gPKAXifApnhnCgZf1iPOUaHhe6en3wrfZ46uUtQmeIfDfekup6mecgyXCBAXAwxRvyzoIije1bWfC2ABEU4OsEDdL775K6RweRpwRLiZxoCw5oi9mNrllYHLcB9HQQaK2YW4v6W0z2mWrCBSVeEyUNQuhLnNu3xZUgyQH00zmc+vkRrA1P8+BmgeICgRpOGoDSBmXaiCGwbwYRoDPO4Q1rOxYDGSQCR7pbCIiFgB4Rc2AfRhFAPSKVWhv5gnMu0GWddzF2lVluXUrbQtUVsghBIWUAcxACgRcSrV8/iIMpGZwmmYw4O5yGNgaoErcLoafxF9DKgQTosQTtsQiB7MNXC6gdMNnG7g9H+0wB2G06yB0w2cbuB0A6cbOL1+OC0XwenhcD9h13/qXvZ5fJXP5LfhvQKnRQ2nTQin+Q0EELcQflXeleG0YfFsaszzLofTUkrjLBA+zKxger1EkHXzohQL4TRIUZQ7DtCVEbmN59WQjYS+bwfZwmlp06a1CjU+zrLxod0N5IUx/8RHy7aElDsWsDZeCH95me9NJuVTQGoMPLm83flzikLr7miUf8rzyXGrlZ6zerdQLTBrcNaBc2x5IkdjkOxWqVHr4cVHXaMIMdwjDyHh+bZV0iDlGEmB8DNIT2DsUi59lmV5fCsaaxZ3osCQyxdngtzlrN1uvYPZoBUa6nXNTc4+8I3xWtnsgRTykA+Dwdb3autye7v3qt0uP+f5+CG5VIrZDCs1+FhAeZV3yW5oBzUu4uvjM3ZdvUFeoIWSLn/0ep2v4PkrwACiipZvbKDxTQAAAABJRU5ErkJggg==" width="2%"> By Order </label>
        &nbsp;&nbsp;
    @endif
    @if($type)
        <!--Charge code: not Checked-->
        <label><img src="data:image/png;base64,
        iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYxNEVBMjYwMDNEQjExRTc4OTM1ODYxOUYyMzc3QkJDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYxNEVBMjYxMDNEQjExRTc4OTM1ODYxOUYyMzc3QkJDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjE0RUEyNUUwM0RCMTFFNzg5MzU4NjE5RjIzNzdCQkMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjE0RUEyNUYwM0RCMTFFNzg5MzU4NjE5RjIzNzdCQkMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4lMsd8AAAFAElEQVR42tRabUhkVRh+d2ZdPxLTJJEG02BVWhFHBDVBU/FjLVYUwmZ/bGZGoCYbQSyRBoVpJv4Q8s9gIEn+8480mKSma5rILg5oyuAXBoPS6MCYGDs42vvePWe4e3fmjqMz4+mF53LuDHM9z7nveT+e4zW9Xg//Z7seoOe8hihH5CAyES8jbsq+30NYEcsIC+JXxONA/OFrl3gDNMlGxD3ErQv83oYYQgwyYiEjEI34DPEpG4NOpwN6TnZ2NqSmpkJcXBwkJSW5f7C/vw82mw3W19fBYrHA0tISrK2tyZ9JRNoQfwWTgBbxEeIrtvpQWloKNTU1UFhY6PfKrayswOjoKJhMJjg+PqaPnIjvEV8jHIEm8BLiJ8RtuikoKID6+nrIzc29tA9brVbo7++HsbEx90cIA+L3QBGgzUhPv0mu0tLSAlVVVQGPJvRGjEYjzM7O0q0L0Yww+nSLxMREte/fRPyGeCU/Px/6+vogKysrKOEwISFBWhjaL7g/NPjRHcQTX29CjQD5xwRtVHpwd3c3xMTEBD2uFxUVSQFgamqKbssQfyMe+UtAh3iIeLG6uho6OjpAq9WGLDmlpaVBcnIyTE5OAtt3i4iN8xKgmf6MeL2srAw6OzuvJMNSOJa5UyViGPHPeQg8QHxAD+jt7YXw8PArKxPInTY3N2Fra+sFvE1nJJ4xjQfX+YIGra2tIfF5X9be3g5skd9mUCVAk4+urKy8UHIKhtEi1tXV8dvv1AgkstoGGhsbhao4GxoapDKF1Vy13gi8j7hBG5f8XzQrKSnhw4+9EaCqEihsimgGg0GeXHVKAlTP36JSQRTfV1pYWBjQ3mRh/raSADUjIHp3RkUks1IlAeqk+EYR1nJycvjwDSUBShKQkpIiNAFZ0n2VAo5yD0idlchG9Rjr9LSMhJuA1GHFxsYKr0JERUXJW1s3gUi6RERECE8gMjLSIwGXdHG5hCdwcnLCh045ATtdHA6H8ASOjo740CEnYJWEGptNeAJ2u12uK7kJSMLSxsaG8JM/PDzkk7c/R2B1dVVoAsvLbgFvUZnIpA6aFDORbW5ujg9nlATM9FpI7lNIfkKZ2Wzmw1+UBFy83yS5T0QbHx+XtFW22Mue+oFBukxPT8tDlTAmW9gfvDU0xMy0t7cHAwMDQk1+ZmYG5ufnefQZVGvqv6HL0NAQSRnCEBgedqspPZTL1Aj8gTCenp5KmpAIRqrg4qIUNSlJ9fmSVcg+R2zTK2tra7vSydMcRkZGeJB5j9c/z5TYHpS5f1leuIe7/gZVf1fRau7s7EBzczM4ndKcvwQPqpw3AmSkCP+JuLuwsADx8fGQkZER0snTOcTBwQGwiX/itclRkdfpNJHOfsrp0GF3d1euzQTNaMGampr45ClhGXi57y8ByQ0RlD3eslgsYZRI8vLygtb40Ibt6enhbjPMJu9UbTN9EOCFHp3r3tne3o6mg4ezszPIzMwMaJzv6uqCiYkJvmHpIPG+2spz8+eUkpj+yDUkkmAqKiqgtrb2whI8LQZFGZakuNt+COc84POXALd36G1zKYY2OJEpLi6WNrqaNMMOLKSDPCrMWG3DO8JvWZx3+jOZy5zUvwtP1exy5Rfp6emg0WieiyzsPFjpnlQaGJUZNhQE5K5FijDJfXr+ZrwY/c/EY+YiJrjEvxgEkoAn0zPxSRmWA17mXofgmDlUSe8/AQYAdMKRyE17UWIAAAAASUVORK5CYII=" width="2%"> By Charge Code </label>
        &nbsp;&nbsp;

        <!--SKU: Checked-->
        <label><img src="data:image/png;base64,
        iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkMwQTE4NjAwMDNEQjExRTdBRjBEQ0VEM0FFOUFBQzY5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkMwQTE4NjAxMDNEQjExRTdBRjBEQ0VEM0FFOUFBQzY5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QzBBMTg1RkUwM0RCMTFFN0FGMERDRUQzQUU5QUFDNjkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QzBBMTg1RkYwM0RCMTFFN0FGMERDRUQzQUU5QUFDNjkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6Zmx2RAAAGg0lEQVR42tRafUxVZRh/uMin8hVFTIYYKYSMAbsTzIYh40MimWxt0B9KRmsD22xtrbWxms1ARvyBi7ZgOReTv+IfJkMWmmg6oJwUhN34CoOh8aGgo+Sz53fuOXdnR7jnvZdzYT3bT845957nvr/3fd7n69UtISGB/s+yxSA9LzAyGGZGHOM5xi7V5/cYY4wehoXxPeOWET/sto4VwCCLGEcZe5x4f4JRzzgvE9swAtsYHzI+kK8pLCyMoCcxMZF2795NQUFBFB4ebnthcnKSJiYmqL+/nywWC92+fZvu3Lmj1gkipYy7riTgzniXcUqefUpLS6MjR45QSkqKwzPX29tLTU1N1NzcTHNzc3g0z/iS8RljxmgCzzAuMA7hZv/+/VRYWEhJSUnrtuGxsTGqqamhlpYW2yNGAeNHowhgM0L7LpjKiRMnKDs723BvghWpra2l69ev43aJUcKo1TWL0NBQe5+/yviBsX3fvn1UXV1N8fHxLnGHISEh0sRgv/D+MPGjw4wneithjwDsow0bFYorKirI39/f5X79wIEDkgO4cuUKbtMZfzN+dpRAGOMaIyA3N5dOnz5N7u7uGxacoqKiKCIigi5fvkzyvutiDIgSwEgvMmLS09OprKxsUyIs3LHKnLIYDYxHIgQ+YrwNBVVVVeTl5bVpaQLMaXBwkIaGhrbybbRMwq4Xgun8Drs/e/asw/79F/bePz3gHOEh0TR79UnGs57sgxnmQKK9QUTxAY6RmJ2dpfz8fLp3D9kIvc5otrcCFYxXsrKyqKioSNwFznJ0Y9oX/rJeT/HA55asn+Ev7vG8lbdjFxOM8GWvI7iwsICFhQXq7OzELWb7q7UI4OJbPCsvL6fg4GChH6jn4H/mD+tsiwi+d+k+EQxbdDWQonR1dWEVkAH8KluJJCbV995ieGLjwv5F5It+zsTuOmffeA/vi8rBgweVy/fUz9UEkFUS3KaIfMcBv+X++jYp3oceESkoKFAH1zAtAeTze5AqiGzcsX+J6v40xtNAD/TpiYeHB2Fvym7+kJZAhrRDBDPTc/yjiyvGEICec4KTgSRSljQtAbOyWUQ24bUpY/099Ik4AbPZrFy+rCWAIEE7d+7UVdIxTbS8YiwB6INePVF5zB1wONo9IFVWetL/2DVRV0Qv8jG50nOXSdgISBVWYGCgkAm5QkT1+vr6qktbGwEf/OPt7a2r4MmyawiI6vXx8VmVgBT4l5aWdBX4bXENAVG9i4uLyuW8moC0hWZm9GvpCF/XEBDV+/ixbbPMqAlI8RCtDz2Jd1FRFieod3p6Wt1XshGQGksDAwP6BAKsKbKRAn2JAWKDR3otD376KQJ9fX26SkxunDTtMJYA9EGvnvT02Bp4XdpAJlXQ6JiJyGvPc926zZjB79pq1SciN27cUC7btQS6sSxo92lafmuuwqcvcdzwWN/g8f6pGLHZlwbZ3a1cXtISWFLqTbT7RCSUQ0Z5rLVcdEbwHt4P9Rb7fmtrq9RblSe7Z7V64Dz+uXr1qtpV2W9/sBl9zQlsYqBjg8f38Z4jZqia2G/WKilRNe/lwUctLy8TOnFCkZGzkswQLib8iB4sEI3bye1R2J98keh4hPU9UWlvb6e6ujrF+xxXghhEG/8+Z+TU19dLlVlkZKTwj6DjADxatHYnHjKZeU4PPE1WW4f7dTaKNzTYuimViGX2uhKjjO0rKyvm0dFRysnJcfjHvHjAO3yt5hHjZ/2Ley+Tc4NHV7CtrY3kztwxJe1ZbQ8o8jFj+ObNm1RaWkqbKRhDY2Oj4mSOqU3HXmfuHzkuHOVd74nsbzMOAkdGRqikpITm56Uxf7JaV24tAhB0hH9jvNnR0SH1iGJjYzd08DiHmJqSalcM/P01ixw77XWcJuLsJwOHDuPj4+rejMsEE1ZcXKwMHgGrQGv3ogQkM0S1h+zBYrF4IJAkJycLFT7ObtjKykrFbBrkwdut1fQIKIkeznUPDw8Pb8PBA3spiouLM2zg8PNoZ8reBrONg8ST9mZeEUdOKZXeaYbSgsnMzKS8vDynW/CYDHgZeBuV2b5Dggd8jhJQ5A2sttKKwQYHmdTUVGmj22vNyAcW0kEeEjM5t1EqwjOMaj2TMYKAIvlkPanP0H4QHR1NJpPpKc8inwdrzRM5WK02wm4EAbVp5crtvgRlZdYQ5Fu3ZBNppnX8FwMjCawmCXLzSeuWDW+LuahJQt0bFfT+E2AA934baOi+ACIAAAAASUVORK5CYII=" width="2%"> By SKU </label>
        &nbsp;&nbsp;
    @else
        <!--Charge code: Checked-->
        <label><img src="data:image/png;base64,
        iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkMwQTE4NjAwMDNEQjExRTdBRjBEQ0VEM0FFOUFBQzY5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkMwQTE4NjAxMDNEQjExRTdBRjBEQ0VEM0FFOUFBQzY5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QzBBMTg1RkUwM0RCMTFFN0FGMERDRUQzQUU5QUFDNjkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QzBBMTg1RkYwM0RCMTFFN0FGMERDRUQzQUU5QUFDNjkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6Zmx2RAAAGg0lEQVR42tRafUxVZRh/uMin8hVFTIYYKYSMAbsTzIYh40MimWxt0B9KRmsD22xtrbWxms1ARvyBi7ZgOReTv+IfJkMWmmg6oJwUhN34CoOh8aGgo+Sz53fuOXdnR7jnvZdzYT3bT845957nvr/3fd7n69UtISGB/s+yxSA9LzAyGGZGHOM5xi7V5/cYY4wehoXxPeOWET/sto4VwCCLGEcZe5x4f4JRzzgvE9swAtsYHzI+kK8pLCyMoCcxMZF2795NQUFBFB4ebnthcnKSJiYmqL+/nywWC92+fZvu3Lmj1gkipYy7riTgzniXcUqefUpLS6MjR45QSkqKwzPX29tLTU1N1NzcTHNzc3g0z/iS8RljxmgCzzAuMA7hZv/+/VRYWEhJSUnrtuGxsTGqqamhlpYW2yNGAeNHowhgM0L7LpjKiRMnKDs723BvghWpra2l69ev43aJUcKo1TWL0NBQe5+/yviBsX3fvn1UXV1N8fHxLnGHISEh0sRgv/D+MPGjw4wneithjwDsow0bFYorKirI39/f5X79wIEDkgO4cuUKbtMZfzN+dpRAGOMaIyA3N5dOnz5N7u7uGxacoqKiKCIigi5fvkzyvutiDIgSwEgvMmLS09OprKxsUyIs3LHKnLIYDYxHIgQ+YrwNBVVVVeTl5bVpaQLMaXBwkIaGhrbybbRMwq4Xgun8Drs/e/asw/79F/bePz3gHOEh0TR79UnGs57sgxnmQKK9QUTxAY6RmJ2dpfz8fLp3D9kIvc5otrcCFYxXsrKyqKioSNwFznJ0Y9oX/rJeT/HA55asn+Ev7vG8lbdjFxOM8GWvI7iwsICFhQXq7OzELWb7q7UI4OJbPCsvL6fg4GChH6jn4H/mD+tsiwi+d+k+EQxbdDWQonR1dWEVkAH8KluJJCbV995ieGLjwv5F5It+zsTuOmffeA/vi8rBgweVy/fUz9UEkFUS3KaIfMcBv+X++jYp3oceESkoKFAH1zAtAeTze5AqiGzcsX+J6v40xtNAD/TpiYeHB2Fvym7+kJZAhrRDBDPTc/yjiyvGEICec4KTgSRSljQtAbOyWUQ24bUpY/099Ik4AbPZrFy+rCWAIEE7d+7UVdIxTbS8YiwB6INePVF5zB1wONo9IFVWetL/2DVRV0Qv8jG50nOXSdgISBVWYGCgkAm5QkT1+vr6qktbGwEf/OPt7a2r4MmyawiI6vXx8VmVgBT4l5aWdBX4bXENAVG9i4uLyuW8moC0hWZm9GvpCF/XEBDV+/ixbbPMqAlI8RCtDz2Jd1FRFieod3p6Wt1XshGQGksDAwP6BAKsKbKRAn2JAWKDR3otD376KQJ9fX26SkxunDTtMJYA9EGvnvT02Bp4XdpAJlXQ6JiJyGvPc926zZjB79pq1SciN27cUC7btQS6sSxo92lafmuuwqcvcdzwWN/g8f6pGLHZlwbZ3a1cXtISWFLqTbT7RCSUQ0Z5rLVcdEbwHt4P9Rb7fmtrq9RblSe7Z7V64Dz+uXr1qtpV2W9/sBl9zQlsYqBjg8f38Z4jZqia2G/WKilRNe/lwUctLy8TOnFCkZGzkswQLib8iB4sEI3bye1R2J98keh4hPU9UWlvb6e6ujrF+xxXghhEG/8+Z+TU19dLlVlkZKTwj6DjADxatHYnHjKZeU4PPE1WW4f7dTaKNzTYuimViGX2uhKjjO0rKyvm0dFRysnJcfjHvHjAO3yt5hHjZ/2Ley+Tc4NHV7CtrY3kztwxJe1ZbQ8o8jFj+ObNm1RaWkqbKRhDY2Oj4mSOqU3HXmfuHzkuHOVd74nsbzMOAkdGRqikpITm56Uxf7JaV24tAhB0hH9jvNnR0SH1iGJjYzd08DiHmJqSalcM/P01ixw77XWcJuLsJwOHDuPj4+rejMsEE1ZcXKwMHgGrQGv3ogQkM0S1h+zBYrF4IJAkJycLFT7ObtjKykrFbBrkwdut1fQIKIkeznUPDw8Pb8PBA3spiouLM2zg8PNoZ8reBrONg8ST9mZeEUdOKZXeaYbSgsnMzKS8vDynW/CYDHgZeBuV2b5Dggd8jhJQ5A2sttKKwQYHmdTUVGmj22vNyAcW0kEeEjM5t1EqwjOMaj2TMYKAIvlkPanP0H4QHR1NJpPpKc8inwdrzRM5WK02wm4EAbVp5crtvgRlZdYQ5Fu3ZBNppnX8FwMjCawmCXLzSeuWDW+LuahJQt0bFfT+E2AA934baOi+ACIAAAAASUVORK5CYII=" width="2%"> By Charge Code </label>
        &nbsp;&nbsp;

        <!--SKU: not Checked-->
        <label><img src="data:image/png;base64,
        iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYxNEVBMjYwMDNEQjExRTc4OTM1ODYxOUYyMzc3QkJDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYxNEVBMjYxMDNEQjExRTc4OTM1ODYxOUYyMzc3QkJDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjE0RUEyNUUwM0RCMTFFNzg5MzU4NjE5RjIzNzdCQkMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjE0RUEyNUYwM0RCMTFFNzg5MzU4NjE5RjIzNzdCQkMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4lMsd8AAAFAElEQVR42tRabUhkVRh+d2ZdPxLTJJEG02BVWhFHBDVBU/FjLVYUwmZ/bGZGoCYbQSyRBoVpJv4Q8s9gIEn+8480mKSma5rILg5oyuAXBoPS6MCYGDs42vvePWe4e3fmjqMz4+mF53LuDHM9z7nveT+e4zW9Xg//Z7seoOe8hihH5CAyES8jbsq+30NYEcsIC+JXxONA/OFrl3gDNMlGxD3ErQv83oYYQgwyYiEjEI34DPEpG4NOpwN6TnZ2NqSmpkJcXBwkJSW5f7C/vw82mw3W19fBYrHA0tISrK2tyZ9JRNoQfwWTgBbxEeIrtvpQWloKNTU1UFhY6PfKrayswOjoKJhMJjg+PqaPnIjvEV8jHIEm8BLiJ8RtuikoKID6+nrIzc29tA9brVbo7++HsbEx90cIA+L3QBGgzUhPv0mu0tLSAlVVVQGPJvRGjEYjzM7O0q0L0Yww+nSLxMREte/fRPyGeCU/Px/6+vogKysrKOEwISFBWhjaL7g/NPjRHcQTX29CjQD5xwRtVHpwd3c3xMTEBD2uFxUVSQFgamqKbssQfyMe+UtAh3iIeLG6uho6OjpAq9WGLDmlpaVBcnIyTE5OAtt3i4iN8xKgmf6MeL2srAw6OzuvJMNSOJa5UyViGPHPeQg8QHxAD+jt7YXw8PArKxPInTY3N2Fra+sFvE1nJJ4xjQfX+YIGra2tIfF5X9be3g5skd9mUCVAk4+urKy8UHIKhtEi1tXV8dvv1AgkstoGGhsbhao4GxoapDKF1Vy13gi8j7hBG5f8XzQrKSnhw4+9EaCqEihsimgGg0GeXHVKAlTP36JSQRTfV1pYWBjQ3mRh/raSADUjIHp3RkUks1IlAeqk+EYR1nJycvjwDSUBShKQkpIiNAFZ0n2VAo5yD0idlchG9Rjr9LSMhJuA1GHFxsYKr0JERUXJW1s3gUi6RERECE8gMjLSIwGXdHG5hCdwcnLCh045ATtdHA6H8ASOjo740CEnYJWEGptNeAJ2u12uK7kJSMLSxsaG8JM/PDzkk7c/R2B1dVVoAsvLbgFvUZnIpA6aFDORbW5ujg9nlATM9FpI7lNIfkKZ2Wzmw1+UBFy83yS5T0QbHx+XtFW22Mue+oFBukxPT8tDlTAmW9gfvDU0xMy0t7cHAwMDQk1+ZmYG5ufnefQZVGvqv6HL0NAQSRnCEBgedqspPZTL1Aj8gTCenp5KmpAIRqrg4qIUNSlJ9fmSVcg+R2zTK2tra7vSydMcRkZGeJB5j9c/z5TYHpS5f1leuIe7/gZVf1fRau7s7EBzczM4ndKcvwQPqpw3AmSkCP+JuLuwsADx8fGQkZER0snTOcTBwQGwiX/itclRkdfpNJHOfsrp0GF3d1euzQTNaMGampr45ClhGXi57y8ByQ0RlD3eslgsYZRI8vLygtb40Ibt6enhbjPMJu9UbTN9EOCFHp3r3tne3o6mg4ezszPIzMwMaJzv6uqCiYkJvmHpIPG+2spz8+eUkpj+yDUkkmAqKiqgtrb2whI8LQZFGZakuNt+COc84POXALd36G1zKYY2OJEpLi6WNrqaNMMOLKSDPCrMWG3DO8JvWZx3+jOZy5zUvwtP1exy5Rfp6emg0WieiyzsPFjpnlQaGJUZNhQE5K5FijDJfXr+ZrwY/c/EY+YiJrjEvxgEkoAn0zPxSRmWA17mXofgmDlUSe8/AQYAdMKRyE17UWIAAAAASUVORK5CYII=" width="2%"> By SKU </label>
        &nbsp;&nbsp;
    @endif

</p>

<table  cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <td style="width: 50%">
            <!--Customer Info-->

            @if($cusInfo && $orderInfo)
               <table class="table-style1" style="float: left; width: 100%" cellspacing="0" cellpadding="0">
                   <thead>
                   <tr>
                       <td colspan="2">&nbsp;Customer Info</td>
                   </tr>
                   </thead>
                   <tbody>
                   <tr>
                       <td class="title">Customer Name</td>
                       <td>{{array_get($cusInfo,'cus_name','')}}</td>
                   </tr>
                   <tr>
                       <td class="title">Ref Code</td>
                       <td>{{array_get($orderInfo,'ref_code','')}}</td>
                   </tr>
                   <tr>
                       <td class="title">Cust Order #</td>
                       <td>{{array_get($orderInfo,'cus_odr_num','')}}</td>
                   </tr>
                   <tr>
                       <td class="title">PO</td>
                       <td>{{array_get($orderInfo,'cus_po','')}}</td>
                   </tr>
                   </tbody>
               </table>
               @else
                   <table class="table-style1" style="float: left; width: 100%" cellspacing="0" cellpadding="0">
                          <thead>
                          <tr>
                              <td colspan="2">&nbsp;Customer Info</td>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>
                              <td class="title">Customer Name</td>
                              <td>{{array_get($cusInfo,'cus_name','')}}</td>
                          </tr>
                          <tr>
                              <td class="title">Ref Code</td>
                              <td>&nbsp;</td>
                          </tr>
                          <tr>
                              <td class="title">Cust Order #</td>
                              <td>&nbsp;</td>
                          </tr>
                          <tr>
                              <td class="title">PO</td>
                              <td>&nbsp;</td>
                          </tr>
                          </tbody>
                      </table>

            @endif
           <!--/Customer Info-->
        </td>
        <td style="width: 50%">
            <!--Order Info-->
            @if($orderInfo)
                <table class="table-style1" style="float: right; width: 100%" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <td colspan="2">&nbsp;Order Info</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="title">Order Type</td>
                        <td>{{array_get($orderInfo,'odr_type','')}}</td>
                    </tr>
                    <tr>
                        <td class="title">Order Status</td>
                        <td>{{array_get($orderInfo,'odr_sts','')}}</td>
                    </tr>
                    <tr>
                        <td class="title">Ref</td>
                        <td>{{array_get($orderInfo,'ref_code','')}}</td>
                    </tr>
                    <tr>
                        <td class="title">CSR</td>
                        <td>{{array_get($orderInfo,'csr','')}}</td>
                    </tr>
                    </tbody>
                </table>
                @else
                    <table class="table-style1" style="float: right; width: 100%" cellspacing="0" cellpadding="0">
                            <thead>
                            <tr>
                                <td colspan="2">&nbsp;Order Info</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="title">Order Type</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="title">Order Status</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="title">Ref</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="title">CSR</td>
                                <td>&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
            @endif
            <!--/Order Info-->
        </td>
    </tr>

</table>


<!--SKU: type=1-->
@if($type)
<table class="table-style2" cellpadding="0" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Item ID</th>
        <th>UPC</th>
        <th>SKU</th>
        <th>Size</th>
        <th>Color</th>
        <th>Lot</th>
        <th>Charge Code</th>
        <th>Desc</th>
        <th>WO QTY</th>
    </tr>
    </thead>

    <tbody>

@if($workOdrHdr)
    @foreach($workOdrHdr['details'] as $wo)
        @foreach($wo['skus'] as $skus)
        <tr>
            <td>{{array_get($skus,'item_id','')}}</td>
            <td>{{array_get($skus,'cus_upc','')}}</td>
            <td>{{array_get($skus,'sku','')}}</td>
            <td>{{array_get($skus,'size','')}}</td>
            <td>{{array_get($skus,'color','')}}</td>
            <td>{{array_get($skus,'lot','')}}</td>
            <td>{{array_get($wo,'chg_code','')}}</td>
            <td>{{array_get($wo,'chg_code_des','')}}</td>
            <td>{{array_get($skus,'wo_qty','')}}</td>
        </tr>
        @endforeach
    @endforeach
@endif

    </tbody>
</table>
<!--/SKU-->

<!--Charge Code: type=0-->
@else
 <table class="table-style2" cellpadding="0" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Charge Code</th>
            <th>Description</th>
            <th>WO QTY</th>
        </tr>
        </thead>
        <tbody>
@if($workOdrHdr)
    @foreach($workOdrHdr['details'] as $wo)
        <tr>
            <td colspan="3" class="title">
                {{array_get($wo,'sku','')}} -
                {{array_get($wo,'size','')}} -
                {{array_get($wo,'color','')}}
            </td>
        </tr>
        @foreach($wo['chargecodes'] as $chargecodes)
            <tr>
                <td>{{array_get($chargecodes,'chg_code','')}}</td>
                <td>{{array_get($chargecodes,'chg_code_des','')}}</td>
                <td>{{array_get($chargecodes,'wo_qty','')}}</td>
            </tr>
        @endforeach
    @endforeach
        </tbody>
    </table>
    @endif
@endif
<!--/Charge Code-->

<table class="table-style3" cellspacing="0" cellpadding="0" width="100%">
    <thead>
    <tr>
        <th>Internal Comments</th>
        <th>External Comments</th>
    </tr>
    </thead>
    <tbody>
    @if($workOdrHdr)
        <tr>
            <td>{{array_get($workOdrHdr,'wo_in_notes','')}}</td>
            <td>{{array_get($workOdrHdr,'wo_ex_notes','')}}</td>
        </tr>
    @endif
    </tbody>
</table>

<footer>
    <table width="100%">
        <tr>
            <td width="50%" style="padding: 0; color: #404040; font-size: 14px">Printed: {{$printedBy}} - {{date("Y/m/d h:i:s ")}} </td>
            <td width="50%" style="text-align: right; padding: 0; font-size: 16px; color: #3f3f3f">&nbsp;</td>
        </tr>
    </table>
</footer>
</body>
</html>
