<?php

namespace App\Api\V2\Inbound\Pallet2\GetInfoByRfid\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Response;

abstract class AbstractController extends Controller
{
    use Helpers;

    private $response = [
        'code' => 200,
        'status' => false,
        'message' => null,
        'data' => null,
    ];

    public function formatArr($arr = [], $check = false){
        if(empty($arr))
            return $arr;

        if(!is_array($arr) && is_object($arr))
            $arr = $arr->toArray();

        $data = [];
        foreach($arr as $k=>$v){
            if($check){
                foreach($v as $t=>$h) {
                    if (is_numeric($t)) {
                        unset($v[$t]);
                    }
                }
                $data[] = $v;
            } else
                if(is_numeric($k)) unset($arr[$k]);
        }
        if($check)
            return $data;

        return $arr;
    }

    final protected function returnError($msg){
        $data = [
            'data'    => null,
            'message' => $msg,
            'status'  => false,
        ];

        return new Response($data, 200, [], null);
    }

    final protected function setStatus($status){
        $this->response['status'] = $status;
    }

    final protected function setData($data = []){
        $this->response['data'] = $data;
    }

    final protected function setMessage($message = ''){
        $this->response['message'] = $message;
    }

    final protected function setCode($code = ''){
        $this->response['code'] = $code;
    }

    final protected function getStatus(){
        return $this->response['status'];
    }

    final protected function getData(){
        return $this->response['data'];
    }

    final protected function getMessage(){
        return $this->response['message'];
    }

    final protected function getCode(){
        return $this->response['code'];
    }

    final protected function getResponseData(){
        return $this->response;
    }
}
