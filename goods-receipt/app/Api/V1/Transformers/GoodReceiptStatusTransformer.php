<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GoodsReceiptStatus;


class GoodReceiptStatusTransformer extends TransformerAbstract
{

    public function transform(GoodsReceiptStatus $goodsReceiptStatus)
    {
        $checked = object_get($goodsReceiptStatus, 'checked', false);

        return [
            'gr_sts_code' => object_get($goodsReceiptStatus, 'gr_sts_code',null),
            'gr_sts_name' => object_get($goodsReceiptStatus, 'gr_sts_name', null),
            'gr_sts_checked' => $checked
        ];
    }
}
