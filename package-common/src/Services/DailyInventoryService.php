<?php

namespace Seldat\Wms2\Services;

use Illuminate\Support\Facades\DB;

class DailyInventoryService
{
    public function correctCustomerDataByDay(int $cusId, array $input)
    {
        $date = strtotime($input['select_day'] . ' UCT');

        if (!$date) {
            return false;
        }

        $dateAfter = $date + 86400;

        DB::statement("DELETE FROM daily_inv_rpt where (`inv_dt` BETWEEN ? and ?) and cus_id = ?;", [$date, $date+86399, $cusId]);

        DB::statement("
        INSERT INTO `daily_inv_rpt` (
            `whs_id`,
            `cus_id`,
            `item_id`,
            `description`,
            `sku`,
            `size`,
            `color`,
            `pack`,
            `uom`,
            `is_rack`,
            `inv_dt`,
            `init_ctns`,
            `init_qty`,
            `init_plt`,
            `init_volume`,
            `in_ctns`,
            `in_qty`,
            `in_plt`,
            `in_volume`,
            `out_ctns`,
            `out_qty`,
            `out_plt`,
            `out_volume`,
            `cur_ctns`,
            `cur_qty`,
            `cur_plt`,
            `cur_volume`,
            `created_at`,
            `updated_at`,
            `lot`,
            `ctn_type`
        )
        SELECT
            c.whs_id,                   #whs_id
            c.cus_id,                   #cus_id
            c.item_id,                  #item_id
            c.des,                      #description
            c.sku,                      #sku
            c.size,                     #size
            c.color,                    #color
            c.ctn_pack_size,            #pack
            c.uom_code,                 #uom
            1,                          #is_rack
            ?,                          #inv_dt
            COUNT(c.ctn_id),            #init_ctns
            IFNULL(SUM(c.piece_remain), 0),        #init_qty
            COUNT(DISTINCT (c.plt_id)), #init_plt
            IFNULL(SUM(c.volume), 0),   #init_volume
            0,                          #in_ctns
            0,                          #in_qty
            0,                          #in_plt
            0,                          #in_volume
            0,                          #out_ctns
            0,                          #out_qty
            0,                          #out_plt
            0,                          #out_volume
            COUNT(c.ctn_id),            #cur_ctns
            IFNULL(SUM(c.piece_remain) , 0),        #cur_qty
            COUNT(DISTINCT (c.plt_id)), #cur_plt
            IFNULL(SUM(c.volume), 0),   #cur_volume
            UNIX_TIMESTAMP(),           #created_at
            UNIX_TIMESTAMP(),           #updated_at
            c.lot,                      #lot
            IFNULL(c.ctn_type, 'null')  #ctn_type
        FROM
            cartons c
        WHERE c.created_at < ?
            AND (
                c.cus_id = ?
            )
            AND (
                (
                    c.shipped_dt >= ?
                    AND c.ctn_sts = 'SH'
                )
                OR c.ctn_sts IN ('AC', 'LK', 'PD')
            )
            AND c.deleted = 0
        GROUP BY c.item_id;
        ", [$date, $dateAfter, $cusId, $dateAfter]);

        DB::statement("
            UPDATE `daily_inv_rpt` i
            JOIN (SELECT ot.whs_id, ot.item_id, IFNULL(SUM(ot.picked_qty), 0) AS qty, IFNULL(SUM(ot.qty), 0) AS ctns, IFNULL(SUM(ot.qty  * i.volume ), 0) AS volume
            FROM odr_dtl ot
            JOIN odr_hdr o ON o.odr_id = ot.odr_id
            JOIN item i ON i.item_id = ot.item_id
            WHERE o.shipped_dt = ? AND o.odr_sts = 'SH' AND o.deleted = 0
            GROUP BY ot.whs_id, ot.item_id) o
            ON o.whs_id = i.whs_id AND o.item_id = i.item_id
            SET i.out_ctns = o.ctns, i.out_qty = o.qty, i.out_volume = o.volume
            WHERE i.inv_dt = ?
        ", [$date, $date]);

        DB::statement("
            UPDATE `daily_inv_rpt` i
            JOIN (
            SELECT o.whs_id, p.item_id, COUNT(DISTINCT p.out_plt_id) AS plt
            FROM pack_hdr p
            JOIN odr_hdr o ON o.odr_id = p.odr_hdr_id
            WHERE o.shipped_dt = ? AND o.odr_sts = 'SH' AND o.deleted = 0
            GROUP BY p.whs_id, p.item_id
            ) o
            ON o.whs_id = i.whs_id AND o.item_id = i.item_id
            SET i.out_plt = o.plt
            WHERE i.inv_dt = ?
        ", [$date, $date] );

        DB::statement("
            UPDATE  `daily_inv_rpt` i
            JOIN (
            SELECT g.whs_id, gt.item_id, SUM(gt.gr_dtl_act_ctn_ttl) AS ctns,
            IFNULL(SUM(gt.gr_dtl_act_ctn_ttl * gt.pack), 0) AS qty,
            IFNULL(SUM(gt.gr_dtl_act_ctn_ttl * gt.volume), 0) AS volume,
            IFNULL(SUM(gt.gr_dtl_plt_ttl), 0) AS plt
            FROM gr_dtl gt
            JOIN gr_hdr g ON gt.gr_hdr_id = g.gr_hdr_id
            WHERE g.gr_hdr_act_dt >= ? AND g.gr_hdr_act_dt < ? AND g.gr_sts = 'RE' AND g.deleted = 0
            GROUP BY g.whs_id, gt.item_id
            ) AS s ON s.whs_id = i.whs_id AND s.item_id = i.item_id
            SET i.in_ctns = s.ctns, i.in_plt = s.plt, i.in_qty = s.qty, i.in_volume = s.volume
            WHERE i.inv_dt = ?;", [$date, $dateAfter, $date]
        );
    }
}
