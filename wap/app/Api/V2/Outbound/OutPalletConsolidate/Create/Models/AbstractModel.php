<?php

namespace App\Api\V2\Outbound\OutPalletConsolidate\Create\Models;


use Illuminate\Support\Facades\DB;

abstract class AbstractModel
{
    protected $model;

    public function __construct($model = null)
    {
        $modelName = substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);
        $modelName = "\\App\\" . str_replace('Model', '', $modelName);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $this->model = ($model) ?: new $modelName();
    }

    /**
     * Get empty model.
     *
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Refresh model to be clean
     */
    public function refreshModel()
    {
        $this->model = $this->model->newInstance();
    }

    /**
     * Get table name.
     *  //show all data in table
     *
     * @return string
     */
    public function getTable()
    {
        return $this->model->getTable();
    }

    /**
     * Make a new instance of the entity to query on.
     *
     * @param array $with
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    /**
     * Find a single entity by key value.
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = [])
    {
        $query = $this->make($with);

        return $query->where($key, '=', $value)->first();
    }

    /**
     * Retrieve model by id
     * regardless of status.
     *
     * @param int $id model ID
     * @param array $with
     *
     * @return Model
     */
    public function byId($id, array $with = [])
    {
        $query = $this->make($with)->where($this->model->getKeyName(), $id);
        $model = $query->firstOrFail();

        return $model;
    }

    /**
     * Get next model.
     *
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function next($model, array $with = [])
    {
        return $this->adjacent(1, $model, $with);
    }

    /**
     * Get prev model.
     *
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function prev($model, array $with = [])
    {
        return $this->adjacent(-1, $model, $with);
    }

    /**
     * Get prev model.
     *
     * @param int $direction
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function adjacent($direction, $model, array $with = [])
    {
        $currentModel = $model;
        $models = $this->all($with);

        foreach ($models as $key => $model) {
            if ($currentModel->{$this->model->getKeyName()} == $model->{$this->model->getKeyName()}) {
                $adjacentKey = $key + $direction;

                return isset($models[$adjacentKey]) ? $models[$adjacentKey] : null;
            }
        }
    }

    /**
     * Get paginated models.
     *
     * @param int $page Number of models per page
     * @param int $limit Results per page
     * @param array $with Eager load related models
     *
     * @return stdClass Object with $items && $totalItems for pagination
     */
    public function byPage($page = 1, $limit = 10, array $with = [])
    {
        $result = new stdClass();
        $result->page = $page;
        $result->limit = $limit;
        $result->totalItems = 0;
        $result->items = [];
        $query = $this->make($with);

        $totalItems = $query->count();
        $query->skip($limit * ($page - 1))
            ->take($limit);
        $models = $query->get();
        // Put items and totalItems in stdClass
        $result->totalItems = $totalItems;
        $result->items = $models->all();

        return $result;
    }

    /**
     * Get all models.
     *
     * @param array $with Eager load related models
     *
     * @return Collection
     */
    public function all(array $with = [])
    {
        $query = $this->make($with);

        // Get
        return $query->get();
    }

    /**
     * Get all models by key/value.
     *
     * @param string $key
     * @param string $value
     * @param array $with
     *
     * @return Collection
     */
    public function allBy($key, $value, array $with = [])
    {
        $query = $this->make($with);

        $query->where($key, $value);
        // Get
        $models = $query->get();

        return $models;
    }

    /**
     * Get latest models.
     *
     * @param int $number number of items to take
     * @param array $with array of related items
     * @param array $with array of columns return
     *
     * @return Collection
     */
    public function latest($number = 10, array $with = [], array $columns = null)
    {
        $query = $this->make($with);

        return $query->take($number)->get($columns);
    }

    /**
     * Get single model by Slug.
     *
     * @param string $slug slug
     * @param array $with related tables
     *
     * @return mixed
     */
    public function bySlug($slug, array $with = [])
    {
        $model = $this->make($with)
            ->where('slug', '=', $slug)
            ->firstOrFail();

        return $model;
    }

    /**
     * Return all results that have a required relationship.
     *
     * @param string $relation
     * @param array $with
     *
     * @return Collection
     */
    public function has($relation, array $with = [])
    {
        $entity = $this->make($with);

        return $entity->has($relation)->get();
    }

    /**
     * Create a new model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function create(array $data)
    {
        // Create the model
        $model = $this->model->fill($data);

        if ($model->save()) {

            return $model;
        }

        return false;
    }

    /**
     * Update an existing model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function update(array $data)
    {
        $model = $this->model->findOrFail($data[$this->model->getKeyName()]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    public function updateWhere($dataUpdated, $where)
    {
        $query = $this->make();
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query->where($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query->where($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query->where($field, '=', $search);
                }
            } else {
                $query->where($field, '=', $value);
            }
        }

        return $query->update($dataUpdated);
    }

    /**
     * Get One collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $orderBy
     * @param array $columns
     * @param bool $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getFirstWhere($where, array $with = [], array $orderBy = [], $columns = ['*'], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {

            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {

                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        foreach ($orderBy as $column => $sortType) {
            $query->orderBy($column, $sortType);
        }

        return $query->first($columns);
    }

    /**
     * Find a collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $orderBy
     * @param array $columns
     * @param bool $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function findWhere($where, array $with = [], array $orderBy = [], $columns = ['*'], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        foreach ($orderBy as $column => $sortType) {
            $query->orderBy($column, $sortType);
        }

        return $query->get($columns);
    }

    /**
     * @param array $where
     * @param bool $or
     *
     * @return mixed
     */
    public function checkWhere($where, $or = false)
    {
        $query = $this->make([]);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        return $query->count();
    }

    /**
     * Sort models.
     *
     * @param array $data updated data
     *
     * @return null
     */
    public function sort(array $data)
    {
        foreach ($data['item'] as $position => $item) {
            $page = $this->model->find($item[$this->model->getKeyName()]);
            $sortData = $this->getSortData($position + 1, $item);
            $page->update($sortData);
        }
    }

    /**
     * Get sort data.
     *
     * @param int $position
     *
     * @return array
     */
    protected function getSortData($position)
    {
        return [
            'position' => $position,
        ];
    }

    /**
     * Delete model.
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete($model)
    {
        return $model->delete();
    }

    /**
     * Delete model By Ids
     *
     * @param array|int $ids
     *
     * @return bool
     */
    public function deleteById($ids)
    {
        $ids = is_array($ids) ? $ids : [$ids];

        return $this->model->destroy($ids);
    }

    /**
     * Sync related items for model.
     *
     * @param Model $model
     * @param array $data
     * @param string $table
     *
     * @return null
     */
    public function syncRelation($model, array $data, $table = null)
    {
        if (!method_exists($model, $table)) {
            return false;
        }
        if (!isset($data[$table])) {
            return false;
        }
        // add related items
        $pivotData = [];
        $position = 0;
        if (is_array($data[$table])) {
            foreach ($data[$table] as $id) {
                $pivotData[$id] = ['position' => $position++];
            }
        }
        // Sync related items
        $model->$table()->sync($pivotData);
    }

    /**
     * Get location by $where and return columns arrays
     *
     * @param string $where
     * @param array $columns
     * @param  array $params
     * @param bool $upperCase
     * @param bool $hashTbl
     *
     * @return array
     */
    public function fetchColumns($columns = [], $where = "", $params = [], $upperCase = false, $hashTbl = false)
    {
        //Hash table have to key , value
        if (count($columns) !== 2 && $hashTbl) {
            return false;
        }

        $tblName = $this->getTable();
        $colNames = implode(', ', $columns);

        $query = "
            SELECT $colNames
            FROM $tblName
        ";
        if (!empty($where)) {
            $query .= " WHERE $where";
        }

        $result = [];

        $db = $this->model->getConnection()->getPdo();

        $statement = $db->prepare($query);
        $statement->execute($params);

        // process return hash table
        if ($hashTbl) {
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                $key = $row[$columns[0]];
                $val = $row[$columns[1]];
                if ($upperCase) {
                    $key = strtoupper($key);
                }

                $result[$key] = $val;
            }
        } else {
            //Init result columns array
            foreach ($columns as $colName) {
                $result[$colName] = [];
            }
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                foreach ($columns as $colName) {
                    if ($upperCase) {
                        $val = strtoupper($row[$colName]);
                    } else {
                        $val = $row[$colNames];
                    }

                    $result[$colName][] = $val;
                }
            }
        }

        return $result;
    }

    /**
     * @param $query
     * @param array $attributes
     *
     * @return bool
     */
    public function sortBuilder(&$query, $attributes = [])
    {
        $validConditions = ['asc', 'desc'];
        $validColumn = $this->model->getTableColumns();

        if (empty($attributes['sort'])) {
            return false;
        }

        foreach ($attributes['sort'] as $key => $value) {

            if (!$value) {
                $value = 'asc';
            }

            if (!in_array($value, $validConditions)) {
                continue;
            }

            if (!in_array($key, $validColumn)) {
                continue;
            }

            $query->orderBy($key, $value);
        }
    }

    /**
     * Update exist. Create if not exist
     *
     * @param array $data
     *
     * @return bool
     */
    public function replace(array $data)
    {
        $model = $this->model->firstOrNew([$this->model->getKeyName() => $data[$this->model->getKeyName()]]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    /*
         * format status WMS to WAP
         * */
    public function formatStatus($array, $status)
    {
        $arr = [
            'default' => [
                'RG' => 'RV',
                'RE' => 'RD',
                'AC' => 'AT',
                'NW' => 'NW'
            ],
            'location' => [
                'RG' => 'RV',
                'RE' => 'RD',
                'AC' => 'ET'
            ],
            'order' => [
                'PK' => 'AG',
                'PD' => 'AD'
            ]
        ];

        if(!isset($arr[$array][$status]))
            return $status;

        return $arr[$array][$status];
    }

}
