<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\JWTUtil;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

class CartonModel extends AbstractModel
{
    /**
     * PalletModel constructor.
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    /**
     * @param $ctn_ids
     *
     * @return bool
     */
    public function checkExistedCartons($ctn_ids)
    {
        $resultCount = $this->model->whereIn('ctn_id', $ctn_ids)->count();

        return $resultCount == count($ctn_ids) ? true : false;
    }

    /**
     * @param array $grDetail
     * @param $info
     *
     * @return array
     */
    public function createCartonNumber(array $grDetail, $info, $cartonStatus = null)
    {
        $cartonCode = str_replace('GDR', 'CTN', $info['gr_hdr_num']);

        if (!empty($grDetail) && is_array($grDetail)) {

            $asnDtlIds = array_pluck($grDetail, 'asn_dtl_id', 'asn_dtl_id');

            // Remove is_damaged
            $this->model->newInstance()->whereIn('asn_dtl_id', $asnDtlIds)->update(['is_damaged' => 0]);

            // Get all Carton by Num
            $allCartons = $this->model->newInstance()
                ->where('ctn_num', 'like', "$cartonCode-%")->orderBy('ctn_id', 'desc')->get()->toArray();

            $cartonByASN = [];
            if (!empty($allCartons)) {
                foreach ($allCartons as $allCarton) {
                    $cartonByASN[$allCarton['asn_dtl_id']][] = $allCarton;
                }
            }
            $maxCtn = 0;
            foreach ($grDetail as $detail) {
                $actualCarton = $detail['gr_dtl_act_ctn_ttl'];

                //init cross dock values
                $crsDoc = empty($detail['crs_doc']) ? 0 : $detail['crs_doc'];
                if ($crsDoc > $actualCarton) {
                    throw new \Exception('Cross dock have to less than or equal actual carton');
                }

                $ctnASN = array_get($cartonByASN, $detail['asn_dtl_id'], null);

                $moreCtn = $actualCarton - count($ctnASN);

                if ($moreCtn > 0) {
                    if (empty($maxCtn)) {
                        $maxCtn = array_get($allCartons, "0.ctn_num", null);
                        $maxCtn = explode("-", $maxCtn);
                        $maxCtn = (int)end($maxCtn);
                    }
                    $dmgTll = (int)array_get($detail, 'gr_dtl_dmg_ttl', 0);
                    // Add more Carton
                    while ($moreCtn > 0) {
                        $maxCtn++;
                        $cartons['add'][$maxCtn] = $cartonCode . "-" . str_pad($maxCtn, 4, "0", STR_PAD_LEFT);
                        $cartonParam = $this->addTime([
                            'asn_dtl_id'    => $detail['asn_dtl_id'],
                            'gr_dtl_id'     => $detail['gr_dtl_id'],
                            'gr_hdr_id'     => $detail['gr_hdr_id'],
                            'is_damaged'    => $dmgTll > 0 ? 1 : 0,
                            'item_id'       => array_get($detail, 'asn_detail.item_id', null),
                            'whs_id'        => $info['whs_id'],
                            'cus_id'        => $info['cus_id'],
                            'ctn_num'       => $cartons['add'][$maxCtn],
                            'ctn_sts'       => $cartonStatus ?: config('constants.ctn_status.ACTIVE'),
                            'ctn_uom_id'    => array_get($detail, 'asn_detail.uom_id', null),
                            'uom_code'      => array_get($detail, 'asn_detail.uom_code', null),
                            'uom_name'      => array_get($detail, 'asn_detail.uom_name', null),
                            'ctn_pack_size' => array_get($detail, 'asn_detail.asn_dtl_pack', null),
                            'piece_remain'  => array_get($detail, 'asn_detail.asn_dtl_pack', null),
                            'piece_ttl'     => array_get($detail, 'asn_detail.asn_dtl_pack', null),
                            'gr_dt'         => null,
                            'sku'           => array_get($detail, 'asn_detail.asn_dtl_sku', ''),
                            'size'          => array_get($detail, 'asn_detail.asn_dtl_size', ''),
                            'color'         => array_get($detail, 'asn_detail.asn_dtl_color', ''),
                            'lot'           => array_get($detail, 'asn_detail.asn_dtl_lot', ''),
                            'po'            => array_get($detail, 'asn_detail.asn_dtl_po', ''),
                            'upc'           => array_get($detail, 'upc', null),
                            'ctnr_id'       => array_get($detail, 'ctnr_id', null),
                            'ctnr_num'      => array_get($detail, 'ctnr_num', null),
                            'length'        => array_get($detail, 'asn_detail.asn_dtl_length', ''),
                            'width'         => array_get($detail, 'asn_detail.asn_dtl_width', ''),
                            'height'        => array_get($detail, 'asn_detail.asn_dtl_height', ''),
                            'weight'        => array_get($detail, 'asn_detail.asn_dtl_weight', ''),
                            'volume'        => array_get($detail, 'asn_detail.asn_dtl_volume', ''),
                            'cube'          => array_get($detail, 'asn_detail.asn_dtl_cube', ''),
                            'expired_dt'    => array_get($detail, 'asn_detail.expired_dt', ''),
                            'ucc128'        => $this->generateUCC128(array_get($detail, 'asn_detail.item_id', null),
                                $info['cus_id']),
                            'des'           => array_get($detail, 'asn_detail.asn_dtl_des', ''),
                            'loc_id'        => null,
                            'loc_code'      => null,
                            'loc_name'      => null
                        ]);

                        $dmgTll--;

                        $cartonParams[] = $cartonParam;
                        if (count($cartonParams) == 100) {
                            $this->model->newInstance()->insert($cartonParams);
                            $cartonParams = [];
                        }
                        $moreCtn--;
                    }
                    if (!empty($cartonParams)) {
                        $this->model->newInstance()->insert($cartonParams);
                        $cartonParams = [];
                    }

                } else if ($moreCtn < 0) {
                    // Remove some Carton
                    $deleteIds = [];
                    for ($i = 0; $i < (-$moreCtn); $i++) {
                        $deleteIds[] = $ctnASN[$i]['ctn_id'];
                        $cartons['del'][] = $ctnASN[$i]['ctn_num'];
                    }

                    if (!empty($deleteIds)) {
                        $this->model->newInstance()->whereIn('ctn_id', $deleteIds)->delete();
                    }
                }
            }
        }

        return $cartons;
    }

    private function generateUCC128($itemId, $cusId)
    {
        return str_pad($itemId, 5, '0', STR_PAD_LEFT) . str_pad($cusId, 8, '0', STR_PAD_LEFT);
    }

    /**
     * @param $attributes
     * @param int $limit
     *
     * @return mixed
     */
    public function assignedPalletToCarton($attributes, $limit = 1)
    {
        return $this->model
            ->where('item_id', $attributes['item_id'])
            ->where('whs_id', $attributes['whs_id'])
            ->where('cus_id', $attributes['cus_id'])
            ->where('asn_dtl_id', $attributes['asn_dtl_id'])
            ->where(function ($sql) {
                $sql->orWhereNull('plt_id')
                    ->orWhere('plt_id', '0');
            })
            ->take($limit)
            ->update(
                [
                    'plt_id' => $attributes['plt_id'],
                    'gr_dt'  => time()
                ]);
    }

    /**
     * @param array $cartonParams
     *
     * @return array
     */
    private function addTime(array $cartonParams)
    {
        $userId = JWTUtil::getPayloadValue('jti') ?: 0;

        $deleted_at = getDefaultDatetimeDeletedAt();
        $created_at = time();
        $time = [
            'created_at' => $created_at,
            'updated_at' => $created_at,
            'deleted_at' => $deleted_at,
            'created_by' => $userId,
            'updated_by' => $userId,
            'deleted'    => 0
        ];

        return array_merge($cartonParams, $time);
    }

    /**
     * @param $ctn_ids
     *
     * @return mixed
     */
    public function getCartonWithNewLocation($ctn_ids)
    {
        $ctn_ids = is_array($ctn_ids) ? $ctn_ids : [$ctn_ids];

        $rows = $this->model
            ->whereIn('ctn_id', $ctn_ids)
            ->get(['ctn_id', 'loc_id', 'loc_name', 'loc_code']);

        return $rows;
    }

    /**
     * @param $ids
     * @param array $with
     *
     * @return mixed
     */
    public function getPalletByAsn($ids, $with = [])
    {
        $query = $this->make($with);

        return $query
            ->whereIn('asn_dtl_id', $ids)
            ->groupBy('plt_id')
            ->get();
    }

    /**
     * @param $itemIds
     * @param array $with
     *
     * @return mixed
     */
    public function getItemLocation($itemIds, $with = [])
    {
        $query = $this->make($with);
        $query->select(['item_id', 'ctn_id', 'loc_id']);
        $query->whereIn('item_id', $itemIds);
        $query->where('loc_id', '>', 0);
        $query->groupBy('item_id');

        return $query->get();
    }

    /**
     * @param $asnDtlIds
     *
     * @return mixed
     */
    public function checkPalletAssigned($asnDtlIds)
    {
        $carton = $this->model->whereIn('asn_dtl_id', $asnDtlIds)
            ->whereNull('plt_id')
            ->whereNull('loc_type_code')
            ->first();

        return $carton ? false : true;
    }

    /**
     * @param $cartonId
     * @param $gr_dtl_id
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getNextCtnId($cartonId, $gr_dtl_id)
    {
        $rs = $this->getFirstWhere(
            [
                "ctn_id" => ['ctn_id', '>', $cartonId],
                ['gr_dtl_id', '=', $gr_dtl_id]
            ],
            [],
            ['ctn_id' => "asc"]);

        return $rs;
    }

    /**
     * @param $locationId
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($locationId, array $with, $limit = null)
    {
        $query = $this->make($with);

        $query->where('ctn_sts', 'AC')->where('loc_id', $locationId);

        $query->whereHas('pallet', function ($query) {
            // search whs id
            $query->whereNotNull('loc_id');
        });

        $query->select(['*', DB::Raw('SUM(piece_remain) as qty')]);
        $query->groupBy('item_id');

        //filterData in getting Carton List By Location
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    /**
     * @param $palletId
     * @param int $transfer
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchPlt($palletId, $transfer = 0, array $with, $limit = null)
    {
        $query = $this->make($with);

        $query->where('loc_id', $palletId);

        if (!$transfer) {
            $query->whereHas('pallet', function ($query) {
                // search whs id
                $query->whereNotNull('loc_id');
            });
        }

        $query->groupBy('item_id');

        //filterData in getting Carton List By Pallet
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    /**
     * @param $lpnCarton
     * @param array $with
     * @param null $limit
     * @return mixed
     */
    public function searchLPNCarton($lpnCarton, array $with, $limit = null)
    {
        $query = $this->make($with)
            ->where('lpn_carton', $lpnCarton)
            ->where('ctn_sts', 'AC')
            ->whereNotNull('lpn_carton')
            ->select(['*', DB::Raw('SUM(piece_remain) as qty')])
            ->groupBy('item_id', 'lpn_carton')
            ->groupBy('item_id');
        $this->model->filterData($query);
        return $query->paginate($limit);
    }

    /**
     * @param $locationId
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchItem($locationId, array $with, $limit = null)
    {
        $query = $this->make($with);
        $query->where('loc_id', $locationId);
        $query->groupBy('item_id');

        $query->whereHas('pallet', function ($query) {
            // search whs id
            $query->whereNotNull('loc_id');
        });

        //filterData in getting Items List for relocation
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    public function searchLPNItem($lpn, array $with, $limit = null)
    {
        $query = $this->make($with);
        $query->where('lpn_carton', $lpn);
        $query->groupBy('item_id');

        $query->whereHas('pallet', function ($query) {
            // search whs id
            $query->whereNotNull('lpn_carton');
        });

        //filterData in getting Items List for relocation
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    /**
     * @param $input
     * @param $palletId
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function getCartonListByPalletForPalletReport($input, $palletId, array $with, $limit = null)
    {
        $query = $this->make($with);

        $query->where('plt_id', $palletId)
            ->where('item_id', array_get($input, 'item_id'))
            ->where('lot', array_get($input, 'lot'))
            ->where('ctn_pack_size', array_get($input, 'ctn_pack_size'))
            ->where('cartons.deleted', 0)
            ->whereIn('cartons.ctn_sts', ['AC', 'LK', 'RG']);

        //filterData in getting Carton List By Pallet
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    /**
     * @param array $param
     */
    public function removeWhereInLocation(array $param)
    {
        $this->model->whereIn('loc_id', $param)->update([
            'loc_id'   => null,
            'loc_name' => null,
            'loc_code' => null
        ]);
    }

    public function getDamageCarton($goodReceiptId)
    {
        $query = $this->make(['goodReceiptDtl.goodsReceipt', 'damageCarton']);

        $query->where('is_damaged', 1);

        $query->whereHas('goodReceiptDtl.goodsReceipt', function ($query) use ($goodReceiptId) {
            $query->where('gr_hdr_id', $goodReceiptId);
        });
        $query->whereHas('damageCarton', function ($query) use ($goodReceiptId) {

        });

        return $query->get()->transform(function ($item) {
            return [
                'ctn_num'  => object_get($item, 'ctn_num', ''),
                'dmg_type' => object_get($item, 'damageCarton.damageType.dmg_name', ''),
                'dmg_note' => object_get($item, 'damageCarton.dmg_note', 'NA')
            ];
        });

    }

    public function updateWhereIn(array $update, array $valueWhere, $columns)
    {
        return $this->model->whereIn($columns, $valueWhere)->update($update);
    }

    /**
     * @param $locationId
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchItemInLocation($locationId, array $with, $limit = null)
    {
        $query = $this->make($with);
        $query->where('loc_id', $locationId);
        $query->groupBy('item_id');

        $query->whereHas('pallet', function ($query) {
            // search whs id
            $query->whereNotNull('loc_id');
        });

        return $query->paginate($limit);
    }

    /**
     * @param $cartonIds
     * @param array $with
     *
     * @return mixed
     */
    public function getDmgQty($cartonIds, $with = [])
    {
        $query = $this->make($with)
            ->select([
                DB::raw("sum(ctn_pack_size) as dmg_qty"),
                "item_id",
                "cus_id",
                "whs_id",
                "gr_dtl_id",
                "ctn_num"
            ])
            ->whereIn("ctn_id", $cartonIds)
            ->groupBy("gr_dtl_id");

        return $query->get();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchCartons($attributes = [], array $with, $limit = null)
    {
        $query = $this->make($with);
        if (!empty($attributes['gr_id'])) {
            $query->where('gr_dtl_id', $attributes);
        }

        return $query->paginate($limit);
    }

    public function searchLpnLoc($attributes = [])
    {
        $whsId = Data::getCurrentWhsId();
        $is_lpn = 0;

        $query = $this->model->where('whs_id', $whsId);

        if(!empty($attributes['to_loc']) && $attributes['to_loc'] == 1){
            $query = $query->whereIn('ctn_sts',['AC','LK','RG']);
        } else {
            $query =$query->where('ctn_sts', 'AC');
        }
        
        if (!empty($attributes['loc_lpn_carton'])) {
            if(strpos($attributes['loc_lpn_carton'], "P-") === false){
                $is_lpn = 0;
                    $query->whereRaw("loc_code like '%{$attributes['loc_lpn_carton']}%'");
                //$query->whereNotNull('loc_code');
                //$query->groupBy('loc_code');

            }
            else {
                $is_lpn = 1;
                $query->where('lpn_carton', 'like', '%'.$attributes['loc_lpn_carton'].'%');
                //$query->groupBy('lpn_carton');
            }
        }
        $query->select(DB::raw('DISTINCT ctn_id,ctn_num,loc_id,loc_code,lpn_carton,' . $is_lpn .' as is_lpn'));
        $limit = $attributes['limit'] ? $attributes['limit'] : 20;
        return $query->paginate($limit);
    }


    /**
     * @param $grId
     *
     * @return mixed
     */
    public function getDistinctPalletIdByGrIds($grId)
    {
        $rows = $this->model
            ->distinct('plt_id')
            ->where('gr_dtl_id', $grId)
            ->get(['plt_id']);

        return $rows;
    }

    /**
     * @param $grNum
     *
     * @return string
     */
    public function getCartonNumber($grNum)
    {
        $cartonCode = str_replace('GDR', 'CTN', $grNum);
        $ctnNumInfo = $this->model
            ->select(DB::raw('ctn_num'))
            ->where('ctn_num', 'like', $cartonCode . "-%")
            ->orderBy('ctn_num', 'desc')
            ->first();

        $index = substr(object_get($ctnNumInfo, 'ctn_num', ''), -4);
        $index++;
        $cartonNum = $cartonCode . "-" . str_pad($index, 4, "0", STR_PAD_LEFT);

        return $cartonNum;
    }

    /**
     * @param array $grDtlIds
     *
     * @return array
     */
    public function getCartonPutAwayIntUpdate($grDtlIds = [])
    {
        $grDtlIds = is_array($grDtlIds) ? $grDtlIds : [$grDtlIds];
        if (empty($grDtlIds)) {
            return [];
        }
        $query = DB::table('cartons')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->join('asn_dtl', 'asn_dtl.asn_dtl_id', '=', 'cartons.asn_dtl_id')
            ->where('cartons.deleted', 0)
            ->where('item.deleted', 0)
            ->where('asn_dtl.deleted', 0)
            ->whereIn('cartons.gr_dtl_id', $grDtlIds)//->whereNotNull('cartons.plt_id')
        ;

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('cartons.whs_id', $currentWH);

        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('cartons.cus_id', $cus_ids);

        $model = $query->get();

        return $model;
    }

    /**
     * @param $grHdrId
     * @param array $with
     *
     * @return mixed
     */
    public function getByGRGroupItem($grHdrId, $with = [])
    {
        $query = $this->make($with)
            ->select([
                DB::raw('sum(piece_remain) as piece_remain_ttl'),
                DB::raw("sum(case when is_damaged = '1' then piece_remain else 0 end) as piece_remain_dmg"),
                'item_id',
                'cus_id',
                'whs_id'
            ])
            ->where('gr_hdr_id', $grHdrId)
            ->groupBy('item_id');

        $model = $query->get();

        return $model;
    }

    public function getCrsDockLoc($grHdr, $grDtlId, $locId)
    {
        $cusId = $grHdr->cus_id;
        $whsId = Data::getCurrentWhsId();
        $userId = Data::getCurrentUserId();
        $loc = Location::join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->leftJoin('pallet', 'pallet.loc_id', '=', 'location.loc_id')
            ->where([
                'loc_type.loc_type_code' => 'XDK',
                'location.loc_whs_id'    => $whsId,
                'location.loc_id'        => $locId,
                //'pallet.deleted'         => 0
            ])
            ->first([
                'location.loc_id',
                'location.loc_code',
                'location.loc_alternative_name',
                'pallet.plt_id',
                'pallet.cus_id',
                'pallet.gr_hdr_id'
            ]);

        if (!$loc) {
            throw new HttpException(403, 'Location: ' . $locId . ' for cross dock not existed');
        }

        if (!$loc->plt_id) {
            //create pallet
            $pltNum = last(explode(' ', microtime()));
            $pallet = [
                'cus_id'         => $cusId,
                'whs_id'         => $whsId,
                'plt_num'        => 'XDK-' . $pltNum,
                'ctn_ttl'        => 0,
                'init_ctn_ttl'   => 0,
                'init_piece_ttl' => 0,
                'is_xdock'       => 1,
                'gr_hdr_id'      => $grHdr->gr_hdr_id,
                'gr_dtl_id'      => $grDtlId,
                'loc_id'         => $loc->loc_id,
                'loc_code'       => $loc->loc_code,
                'loc_name'       => $loc->loc_alternative_name,
                'created_at'     => time(),
                'updated_at'     => time(),
                'deleted'        => 0,
                'deleted_at'     => 915148800,
                'created_by'     => $userId,
                'updated_by'     => $userId,
            ];

            $pltId = DB::table('pallet')->insertGetId($pallet);
            $loc->plt_id = $pltId;
        } elseif ($loc->cus_id != $cusId) {
            throw new HttpException(403, 'Location: ' . $loc->loc_code . ' has pallet not belongs customer');
        } elseif ($loc->gr_hdr_id != $grHdr->gr_hdr_id) {
            throw new HttpException(403, 'Location: ' . $loc->loc_code . ' has pallet not belongs this goods receipts');
        }


        return $loc;
    }

    /**
     * Get cartons or containers, good receipt, damage type, sku have damage cartons
     *
     * @param integer $userId
     * @param array $input
     * @param string $type
     * @param integer $limit
     *
     * @return Pagination
     */
    public function cartonsHaveDamage($userId = null, $input = [], $type = null, $limit = 20)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        if (!$userId) {
            return null;
        }
        $where = [
            'cartons.is_damaged'  => 1,
            'cus_in_user.user_id' => $userId,
            'customer.cus_status' => 'AC',
            'customer.deleted'    => 0,
        ];
        $query = $this->model
            ->where($where)
            ->where('cartons.ctn_sts', '!=', 'CC')
            ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
            ->join('cus_in_user', 'cus_in_user.cus_id', '=', 'customer.cus_id')
            ->join('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'cartons.gr_hdr_id')
            ->join('container', 'container.ctnr_id', '=', 'cartons.ctnr_id')
            ->join('damage_carton', 'damage_carton.ctn_id', '=', 'cartons.ctn_id')
            ->join('damage_type', 'damage_type.dmg_id', '=', 'damage_carton.dmg_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id');

        $select = [
            'cartons.*',
            'cus_in_user.whs_id',
            'customer.cus_code',
            'customer.cus_name',
            'gr_hdr.gr_hdr_id',
            'gr_hdr.gr_hdr_num',
            'gr_hdr.gr_in_note',
            'gr_hdr.ref_code',
            'container.ctnr_id',
            'container.ctnr_num',
            'damage_carton.dmg_id',
            'damage_type.dmg_name',
            'damage_type.dmg_id',
            'damage_type.dmg_code',
            DB::raw('CONCAT(cartons.sku, " - ", cartons.size, " - ", cartons.color, " - ", cartons.lot) AS sku_str'),
            DB::raw('count(cartons.ctn_id) dmg_ctns'),
            DB::raw('sum(`piece_ttl`) dmg_qty')
        ];
        $query->select($select);
        $this->buildWhere($input, $query);
        $this->buildGroupByAndOrder($type, $query);

        //echo $query->toSql();exit();

        return $query->paginate($limit);
    }

    /**
     * Build group by and order by query
     *
     * @param strtring $type
     * @param pointer $query
     */
    private function buildGroupByAndOrder($type, &$query)
    {
        switch ($type) {
            case 'container':
                $query->groupBy('container.ctnr_id')
                    ->orderBy('container.ctnr_id', 'desc');
                break;
            case 'good-receipt':
                $query->groupBy('gr_hdr.gr_hdr_id')
                    ->orderBy('gr_hdr.gr_hdr_id', 'desc');
                break;
            case 'damage-type':
                $query->groupBy('damage_type.dmg_id')
                    ->orderBy('damage_type.dmg_id', 'desc');
                break;
            case 'sku':
                $query->groupBy('cartons.sku')
                    ->orderBy('cartons.sku', 'desc');
                break;
            case 'carton':
            default:
                $query->groupBy('cartons.item_id')
                    ->groupBy('cartons.lot')
                    ->groupBy('gr_hdr.gr_hdr_id')
                    ->groupBy('cartons.po')
                    ->groupBy('cartons.ctn_pack_size')
                    ->groupBy('damage_type.dmg_code')
                    ->orderBy('cartons.ctn_id', 'desc');
                break;
        }
    }

    /**
     * Build extra where for query statement
     *
     * @param array $input
     * @param pointer $query
     */
    private function buildWhere($input, &$query)
    {
        $cusId = array_get($input, 'cus_id', null);
        $ctnrNum = array_get($input, 'ctnr_num', null);
        $grHdrNum = array_get($input, 'gr_hdr_num', null);
        $dmgTypeId = array_get($input, 'dmg_id', null);
        $sku = array_get($input, 'sku', null);
        $whsId = array_get($input, 'whs_id', null);
        $itemId = array_get($input, 'item_id', null);
        $lot = array_get($input, 'lot', null);
        if ($whsId) {
            $query->where('cus_in_user.whs_id', $whsId)
                ->where('gr_hdr.whs_id', $whsId);
        }
        if ($cusId) {
            $query->where('customer.cus_id', $cusId);
        }
        if ($dmgTypeId) {
            $query->where('damage_carton.dmg_id', $dmgTypeId);
        }
        if ($itemId) {
            $query->where('cartons.item_id', $itemId);
        }
        if ($lot) {
            $query->where('cartons.lot', $lot);
        }
        if ($ctnrNum) {
            $query->where('cartons.ctnr_num', 'LIKE', "%{$ctnrNum}%");
        }
        if ($sku) {
            $query->where('cartons.sku', 'LIKE', "%{$sku}%");
        }
        if ($grHdrNum) {
            $query->where('gr_hdr.gr_hdr_num', 'LIKE', "%{$grHdrNum}%");
        }
    }

    /**
     * @param $itemIds
     * @param int $limit
     *
     * @return mixed
     */
    public function findDamagedCartonByItemIds($grDtlIds, $limit)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons')//if use this line then return  array
        ->Join('damage_carton as dc', 'dc.ctn_id', '=', 'cartons.ctn_id')
            ->leftJoin('damage_type as dt', 'dt.dmg_id', '=', 'dc.dmg_id')
            ->leftJoin('users as u', 'u.user_id', '=', 'dc.updated_by')
            ->where('is_damaged', 1)
            ->where('cartons.deleted', 0)
            ->whereIn('gr_dtl_id', $grDtlIds);
        $row = $query->paginate($limit);

        return $row;
    }

    /**
     * @param $itemIds
     *
     * @return mixed
     */
    public function getCartonByGrDtlIds($grDtlIds)
    {
        $grDtlIds = is_array($grDtlIds) ? $grDtlIds : [$grDtlIds];
        $query = $this->model
            ->where('is_damaged', 1)
            ->whereIn('gr_dtl_id', $grDtlIds);

        return $query->pluck('ctn_id');
    }

    public function removeRfidCartonsByCancelAsn($asnHdrId)
    {
        $userId = Data::getCurrentUserId();

        $query = DB::table('cartons')
            ->join('asn_dtl', 'asn_dtl.asn_dtl_id', '=', 'cartons.asn_dtl_id')
            ->where('asn_dtl.asn_hdr_id', $asnHdrId)
            ->where('cartons.deleted', 0)
            ->update([
                'cartons.rfid'       => null,
                'cartons.updated_by' => $userId,
                'cartons.updated_at' => time(),
            ]);

        return $query;
    }

    public function deleteRfidVirtualCartonsByCancelAsn($asnHdrId)
    {
        $userId = Data::getCurrentUserId();

        $query = DB::table('vtl_ctn')
            ->join('asn_dtl', 'asn_dtl.asn_dtl_id', '=', 'vtl_ctn.asn_dtl_id')
            ->where('asn_dtl.asn_hdr_id', $asnHdrId)
            ->where('vtl_ctn.deleted', 0)
            ->update([
                'vtl_ctn.updated_by' => $userId,
                'vtl_ctn.updated_at' => time(),
                'vtl_ctn.deleted_at' => time(),
                'vtl_ctn.deleted'    => 1,
            ]);

        return $query;
    }

    public function removeRfidCartonsByCancelAsnDetail($asnDtlId)
    {
        $query = $this->model
            ->where('asn_dtl_id', $asnDtlId)
            ->update([
                'rfid' => null,
            ]);

        return $query;
    }

    public function deleteRfidVirtualCartonsByCancelAsnDetail($asnDtlId)
    {
        $userId = Data::getCurrentUserId();

        $query = DB::table('vtl_ctn')
            ->where('vtl_ctn.asn_dtl_id', $asnDtlId)
            ->where('vtl_ctn.deleted', 0)
            ->update([
                'vtl_ctn.updated_by' => $userId,
                'vtl_ctn.updated_at' => time(),
                'vtl_ctn.deleted_at' => time(),
                'vtl_ctn.deleted'    => 1,
            ]);

        return $query;
    }

    public function updateCartonsByPltIdAndGrDtlId($grHdr, $pallet, $grDtl, $ctnTtl, $curLPN = null)
    {
        $cartons = $this->model
            ->where('plt_id', $pallet->plt_id)
            ->where('gr_dtl_id', $grDtl->gr_dtl_id)
            ->where('lpn_carton', $curLPN)
            ->get();

        $cartonIds = [];
        foreach ($cartons as $carton) {
            $cartonIds[$carton->ctn_id] = $carton->ctn_id;
        }

        $ctnTtlOld = count($cartonIds);

        if ($grDtl->uom_code == 'KG') {
            $this->model
                ->where('plt_id', $pallet->plt_id)
                ->where('gr_dtl_id', $grDtl->gr_dtl_id)
                ->where('lpn_carton', $curLPN)
                ->update([
                    'inner_pack' => $ctnTtl
                ]);
            return ['ctn_ttl_old' => $ctnTtlOld];
        }

        $result = false;
        $palletRfid = $pallet->rfid ? true : false;
        $ctnTtlOld = $palletRfid ? 0 : $ctnTtlOld;

        $ttl = $ctnTtl - $ctnTtlOld;
        $cartonInserts = [];
        if ($ctnTtl == 0 || $palletRfid) {

            $arrEdit = [
                'deleted_at' => time(),
                'deleted'    => 0,
                'loc_id'     => NULL,
                'loc_code'   => NULL,
                'ctn_sts'    => 'IA'
            ];

            if ($palletRfid) {
                $arrEdit = [
                    'loc_id'     => NULL,
                    'loc_code'   => NULL,
                    'ctn_sts'    => 'AJ'
                ];
            }

            $result = $this->model
                ->where('plt_id', $pallet->plt_id)
                ->where('lpn_carton', $curLPN)
                ->update($arrEdit);
        }

        if ($ctnTtl > $ctnTtlOld) {
            $userId = $this->getUserId();
            $loc_code = array_get($pallet, 'loc_code', null);
            // - CCTC
            // Insert carton
            $curCarton = $cartons->first();
            $ctnData = [
                'plt_id'           => $pallet->plt_id,
                'asn_dtl_id'       => object_get($grDtl, 'asn_dtl_id', null),
                'gr_dtl_id'        => object_get($grDtl, 'gr_dtl_id', null),
                'item_id'          => array_get($grDtl, 'item_id', null),
                'whs_id'           => array_get($pallet, 'whs_id', null),
                'cus_id'           => array_get($pallet, 'cus_id', null),
                'loc_id'           => array_get($pallet, 'loc_id', null),
                'loc_code'         => $loc_code,
                'loc_name'         => $loc_code,
                'loc_type_code'    => $loc_code ? 'RAC' : NULL,
                'ctn_pack_size'    => array_get($grDtl, 'pack', 0),
                'piece_remain'     => array_get($grDtl, 'pack', 0),
                'piece_ttl'        => array_get($grDtl, 'pack', 0),
                'ctn_uom_id'       => array_get($grDtl, 'uom_id', 0),
                'ctn_sts'          => 'RG',
                'gr_dt'            => time(),
                'gr_hdr_id'        => object_get($grDtl, 'gr_hdr_id', null),
                'sku'              => array_get($grDtl, 'sku', 0),
                'size'             => array_get($curCarton, 'size', null),
                'color'            => array_get($curCarton, 'color', null),
                'lot'              => array_get($curCarton, 'lot', 'NA'),
                'po'               => array_get($grDtl, 'po', 'NA'),
                'uom_code'         => array_get($grDtl, 'uom_code', 0),
                'uom_name'         => array_get($grDtl, 'uom_name', 0),
                'upc'              => array_get($grDtl, 'upc', 0),
                'ctnr_id'          => array_get($grDtl, 'ctnr_id', 0),
                'ctnr_num'         => array_get($grDtl, 'ctnr_num', 0),
                'is_ecom'          => 0,
                'picked_dt'        => 0,
                'storage_duration' => 0,
                'expired_dt'       => object_get($grDtl, 'expired_dt', 0),
                'return_id'        => null,
                'length'           => object_get($grDtl, 'length', 0),
                'width'            => object_get($grDtl, 'width', 0),
                'height'           => object_get($grDtl, 'height', 0),
                'weight'           => object_get($grDtl, 'weight', 0),
                'cube'             => object_get($grDtl, 'cube', 0),
                'volume'           => object_get($grDtl, 'volume', 0),
                'cat_code'         => $grDtl->cat_code,
                'cat_name'         => $grDtl->cat_name,
                'spc_hdl_code'     => $grDtl->spc_hdl_code,
                'spc_hdl_name'     => $grDtl->spc_hdl_name,
                'ucc128'           => object_get($grDtl, 'asnDetail.ucc128', 0),
                'created_at'       => time(),
                'updated_at'       => time(),
                'created_by'       => $userId,
                'updated_by'       => $userId,
                'deleted_at'       => 915148800,
                'deleted'          => 0,
            ];

            $maxNum = null;

            for ($i = 0; $i < $ttl; $i++) {
                $maxNum = $this->generateNum(object_get($grHdr, 'gr_hdr_num'), object_get($grHdr, 'gr_hdr_id'),
                    $maxNum);
                //$ctnData['lpn_carton'] = $i < $countCurQty ? $curLPN : null;
                $ctnData['lpn_carton'] = $curLPN;
                $ctnData['is_damaged'] = 0;
                $ctnData['ctn_num'] = $maxNum;
                $ctnData['rfid'] = $palletRfid ? str_replace('CTN', 'CCTC', $maxNum) : null;
                $cartonInserts[] = $ctnData;
            }
            $result = $this->model->insert($cartonInserts);

        } else if (!$palletRfid && $ctnTtl < $ctnTtlOld) {
            $ttl = $ctnTtlOld - $ctnTtl;
            $cartonIds = array_values($cartonIds);
            $cartonIdDelete = [];
            foreach ($cartonIds as $key => $cartonId) {
                if ($key < $ttl) {
                    $cartonIdDelete[] = $cartonId;
                }
            }

            // Delete carton
            $result = $this->model
                ->whereIn('ctn_id', $cartonIdDelete)
                ->update([
                    'deleted_at' => time(),
                    'deleted'    => 1,
                    'loc_id'     => NULL,
                    'loc_code'   => NULL,
                    'ctn_sts'    => 'IA'
                ]);

            DB::table('rpt_carton')
                ->whereIn('ctn_id', $cartonIdDelete)
                ->update([
                    'deleted_at' => time(),
                    'deleted'    => 1,
                    'loc_id'     => NULL,
                    'loc_code'   => NULL,
                    'ctn_sts'    => 'IA'
                ]);
        }

        return [
            'ctn_ttl_old' => $ctnTtlOld,
            'status'      => $result
        ];
    }

    public function deleteCartonsByPltIdAndGrDtlId($grHdr, $pallet, $grDtl, $ctnTtl, $curLpn = null)
    {
        $rfid = $pallet->rfid;
        $cartons = $this->model
            ->where('plt_id', $pallet->plt_id)
            ->where('lpn_carton', $curLpn)
            // ->where('gr_dtl_id', $grDtl->gr_dtl_id)
            ->get();

        $cartonIds = [];
        foreach ($cartons as $carton) {
            $cartonIds[$carton->ctn_id] = $carton->ctn_id;
        }
        // Delete cartons
        $arrEdit = [
            'deleted'    => 1
        ];

        $this->model->where('plt_id', $pallet->plt_id)
            ->where('lpn_carton', $curLpn)
            ->update($arrEdit);

        DB::table('rpt_carton')
            ->where('plt_id', $pallet->plt_id)
            ->update($arrEdit);

        return [
            'ctn_ttl_old' => count($cartonIds)
        ];
    }

    public function getMaxNum($grHdrId, $cartonCode)
    {
        $result = DB::table('cartons')
            ->where('gr_hdr_id', $grHdrId)
            ->where('ctn_num', 'LIKE', "$cartonCode-%")
            ->max('ctn_num');

        return $result;
    }

    public function generateNum($grHdrNum, $grHdrId, $maxNum = null)
    {
        $seq = 1;
        $grHdrNumNew = explode('-', $grHdrNum);
        array_pop($grHdrNumNew);
        $grHdrNumNew[0] = 'CTN';
        $grHdrNumNew = implode('-', $grHdrNumNew);

        if (!$maxNum) {
            $maxNum = $this->getMaxNum($grHdrId, $grHdrNumNew);
        }
        // ctnNum
        $cartonCode = str_replace('GDR', 'CTN', $grHdrNum);

        if ($maxNum) {
            $parts = explode('-', $maxNum);
            $seq = array_pop($parts);
            $seq++;
        }

        $ctnNum = $cartonCode . "-" . str_pad($seq, 4, "0", STR_PAD_LEFT);

        return $ctnNum;
    }

    public function getUserId()
    {
        return JWTUtil::getPayloadValue('jti');
    }

    /**
     * @param $pallet|Pallet Collection
     * @return int
     */
    public function getCartonTotalInPallet($pallet)
    {
        return $this->model->where('whs_id', '=', $pallet->whs_id)
            ->where('cus_id', '=', $pallet->cus_id)
            ->where('plt_id', '=', $pallet->plt_id)
            ->whereIn('ctn_sts', ['AC', 'RG', 'LK'])
            ->count();
    }

    public function getPalletSupportMixSku($whsId, $grHdrId, $limit = 20)
    {
        $pallets = $this->model
            ->join('pal_sug_loc', function ($q) {
                $q->on('cartons.gr_hdr_id', '=', 'pal_sug_loc.gr_hdr_id')
                ->on('cartons.item_id', '=', 'pal_sug_loc.item_id')   ;
            })
            ->join('item',  'item.item_id', '=', 'cartons.item_id')
            ->join('item_status', 'item_status.item_sts_code', '=', 'item.status')
            ->where('cartons.gr_hdr_id', $grHdrId)
            ->where('cartons.whs_id', $whsId)
            ->select([
                'cartons.plt_id',
                'cartons.item_id',
                'cartons.gr_dtl_id',
                'cartons.lpn_carton',
                'cartons.sku',
                'cartons.size',
                'cartons.color',
                'cartons.lot',
                'item.description AS des',
                'item.status AS item_status_code',
                'item_status.item_sts_name AS item_status',
                'cartons.ctn_pack_size as pack',
                'pal_sug_loc.act_loc_code as act_loc_code',
                'pal_sug_loc.ctn_ttl as init_ctns',
                'cartons.uom_code',
                'cartons.uom_name',
                'cartons.weight',
                "cartons.rfid",
                "pal_sug_loc.qty_ttl AS init_qty",
            ])
            ->groupBy('cartons.item_id')
            ->groupBy('cartons.lot')
            ->groupBy('cartons.lpn_carton')
            ->orderBy('cartons.item_id')
            ->paginate($limit);

        return $pallets;
    }

    public function getLocationStillReceiving($grHdrId, $whsId, array $locIds)
    {
        if (! (is_array($locIds) && ! empty($locIds))) {
            return [];
        }

        $result = $this->model->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->where('cartons.gr_hdr_id', '<>', $grHdrId)
            ->where('cartons.ctn_sts', 'RG')
            ->where('cartons.whs_id', $whsId)
            ->whereIn('cartons.loc_id', $locIds)
            ->where('location.deleted', 0)
            ->where('location.loc_sts_code', 'RG')
            ->pluck('location.loc_id')
            ->toArray();

        return $result;
    }
}
