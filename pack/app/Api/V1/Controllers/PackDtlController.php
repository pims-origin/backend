<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\PackRefModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Transformers\PackViewDtlTransformer;
use App\Api\V1\Validators\PackDtlValidator;
use App\Api\V1\Validators\PackHdrValidator;
use App\Jobs\BOLJob;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

/**
 * Class PackDtlController
 *
 * @package App\Api\V1\Controllers
 */
class PackDtlController extends AbstractController
{
    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var PackRefModel
     */
    protected $packRefModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var WavePickDtlModel
     */
    protected $wavePickDtlModel;
    protected $orderHdrMetaModel;

    /**
     * PackDtlController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param PackDtlModel $packDtlModel
     * @param PackHdrModel $packHdrModel
     * @param PackRefModel $packRefModel
     * @param EventTrackingModel $eventTrackingModel
     * @param WavePickDtlModel $wavePickDtlModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        PackDtlModel $packDtlModel,
        PackHdrModel $packHdrModel,
        PackRefModel $packRefModel,
        EventTrackingModel $eventTrackingModel,
        WavePickDtlModel $wavePickDtlModel
    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->packDtlModel = $packDtlModel;
        $this->packHdrModel = $packHdrModel;
        $this->packRefModel = $packRefModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->wavePickDtlModel = $wavePickDtlModel;
        $this->orderHdrMetaModel = new OrderHdrMetaModel();
    }

    /**
     * @param $orderHdrId
     * @param Request $request
     * @param PackViewDtlTransformer $packViewDtlTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(
        $orderHdrId,
        Request $request,
        PackViewDtlTransformer $packViewDtlTransformer
    ) {
        try {
            $input = $request->getQueryParams();
            $orderDtls = '';
            if (!empty($input['odr_dtl_id'])) {
                $input['odr_dtl_id'] = array_filter(explode(",", $input["odr_dtl_id"]));
                $orderDtls = $this->orderDtlModel->getOrderDtlById($input['odr_dtl_id'])->toArray();
            }
            $orderHdr = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderHdrId]);

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            $itemOrder = [];
            if ($orderDtls) {
                foreach ($orderDtls as $orderDtl) {
                    $key = $orderDtl['item_id'] . '-' . $orderDtl['lot'];
                    $itemOrder[$key] = $orderDtl;
                }
            }

            if (!empty($input['pack_hdr_id'])) {
                $packHdr = $this->packHdrModel->getFirstBy("pack_hdr_id", $input['pack_hdr_id'],
                    ['systemUom', 'orderHdr', 'item']);

                //$packDtl = $orderDtls;
                $packDtl = $this->packDtlModel
                    //->findWhere(['pack_hdr_id' => $input['pack_hdr_id'], 'odr_hdr_id' => $orderHdrId], ['item'])
                    ->findWhere(['pack_hdr_id' => $input['pack_hdr_id'], 'odr_hdr_id' => $orderHdrId])
                    ->toArray();
                if (!$packDtl) {
                    throw new \Exception(Message::get("BM017", "Carton"));
                }
            } else if (!empty($input['odr_dtl_id'])) {
                $packHdr = new PackHdr();
                $packHdr->sku_ttl = count($input['odr_dtl_id']);
                //$packDtl = $this->orderDtlModel->loadByItems($orderHdrId, $input['item_id'], ['item'])->toArray();
                $packDtl = $orderDtls;
            } else {
                throw new \Exception(Message::get("BM017", "Order Detail input"));
            }

            $packHdr->order_type_key = $orderHdr->odr_type;
            $packHdr->origin_odr_type = object_get($orderHdr, 'shippingOrder.type', '');
            $packHdr->odr_hdr_num = $orderHdr->odr_num;
            $packHdr->odr_hdr_id = $orderHdr->odr_id;

            /*$itemPieceQty = $this->packDtlModel->loadPieceQty(['odr_hdr_id' => $orderHdrId])->toArray();
            $itemPieceQty = array_pluck($itemPieceQty, "piece_qty_ttl", "item_id");*/

            //  Get wave pick detail by wave pick
            /*$wvDtls = $this->wavePickDtlModel->getActualQtyByWvIds($orderHdr->wv_id);
            $wvDtls = array_pluck($wvDtls, 'act_piece_qty', 'item_id');*/

            $weight = $this->packDtlModel->getWeightByPackHdrId($input['pack_hdr_id']);

            $items = array_map(function ($pack) use ($itemOrder, $weight/*, $itemPieceQty, $wvDtls*/) {
                //$requested = $wvDtls[$pack['item_id']];
                $requested = null;
                $piece_qty = array_get($pack, 'piece_qty', 0);
                $ship_track_id = null;
                $remain = 0;
                if (empty($pack['pack_hdr_id'])) {
                    $odr_hdr_id = $itemOrder[$pack['item_id'] . '-' . $pack['lot']]['odr_id'];
                    $ship_track_id = $itemOrder[$pack['item_id'] . '-' . $pack['lot']]['ship_track_id'];
                    $piece_qty = 0;
                    $packQty = (int)$this->packDtlModel->sumPieceQty($pack['lot'], $pack['item_id'], $odr_hdr_id);
                    $odr_dtl_id = array_get($pack, 'odr_dtl_id', -1);
                    $act_piece_qty = DB::table('odr_cartons')
                        ->select(DB::raw('SUM(piece_qty) AS piece_qty'))
                        ->where('odr_dtl_id', $odr_dtl_id)->first();
                    $remain = $act_piece_qty['piece_qty'] - $packQty;
                    $requested = $act_piece_qty['piece_qty'];

                }


                if ($remain < 0) {
                    $remain = 0;
                }

                $weight = array_get($weight, array_get($pack, 'pack_dtl_id', null), 0);

                return [
                    'odr_dtl_id'    => array_get($pack, 'odr_dtl_id', null),
                    'pack_dtl_id'   => array_get($pack, 'pack_dtl_id', null),
                    'itm_id'        => array_get($pack, 'item_id', null),
                    'sku'           => array_get($pack, 'sku', null),
                    'color'         => array_get($pack, 'color', null),
                    'size'          => array_get($pack, 'size', null),
                    'lot'           => array_get($pack, 'lot', null),
                    'requested'     => $requested,
                    'piece_qty'     => $piece_qty,
                    'remain_qty'    => $remain,
                    'ship_track_id' => $ship_track_id,
                    'weight'        => $weight
                ];
            }, $packDtl);

            $packHdr['items'] = $items;

            return $this->response->item($packHdr, $packViewDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderHdrId
     * @param Request $request
     * @param PackHdrValidator $packHdrValidator
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function store(
        $orderHdrId,
        Request $request,
        PackHdrValidator $packHdrValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['odr_hdr_id'] = $orderHdrId;

        $packHdrValidator->validate($input);

        // validation
        $piece_total = array_sum(array_column($input['items'], 'piece_qty'));
        if (empty($piece_total)) {
            throw new \Exception(Message::get("VR001", "Pack Qty"));
        }

        $orderHdr = $this->orderHdrModel->getFirstBy("odr_id", $orderHdrId, ['details.item'])->toArray();
        $itemData = array_pluck($orderHdr['details'], 'item', 'item.item_id');

        try {
            $numberOfPack = 1;
            $packHdrIds = [];
            if ($input['number_of_pack']) {
                $numberOfPack = $input['number_of_pack'];
            }

            // prepare data for insert batch
            $items = array_column($input['items'], 'itm_id');
            if (!$this->orderDtlModel->checkDtlHasItem($orderHdrId, $items)) {
                throw new \Exception(Message::get("BM017", "Order-Item"));
            }

            $packHdrCreate = [];
            $packRef = $this->packRefModel->getFirstBy("pack_ref_id", $input['pack_ref_id']);
            // dtt pack
            $Pack_Dt_Checksum = '';
            $pacKDtls = [];
            $pacKDtlCreate = [];
            $skuTotal = 0;

            foreach ($input['items'] as $item) {
                if (empty($item['piece_qty'])) {
                    continue;
                }
                if (!$this->orderDtlModel->checkWhere(['odr_id' => $orderHdrId, 'item_id' => $item['itm_id']])) {
                    throw new \Exception(Message::get("BM017", "Order-Item"));
                }

                $paramDtl = [
                    'pack_hdr_id' => 0,
                    'odr_hdr_id'  => $orderHdrId,
                    'whs_id'      => $orderHdr['whs_id'],
                    'cus_id'      => $orderHdr['cus_id'],
                    'item_id'     => $item['itm_id'],
                    'size'        => $item['size'],//array_get($itemData, $item['itm_id'] . ".size", null),
                    'lot'         => $item['lot'],//array_get($itemData, $item['itm_id'] . ".lot", null),
                    'sku'         => $item['sku'],//array_get($itemData, $item['itm_id'] . ".sku", null),
                    'cus_upc'     => array_get($itemData, $item['itm_id'] . ".cus_upc", null),
                    'uom_id'      => array_get($itemData, $item['itm_id'] . ".uom_id", null),
                    'piece_qty'   => $item['piece_qty'],
                    'sts'         => 'i',
                    'created_at'  => time(),
                    'updated_at'  => time(),
                    'deleted'     => 0,
                    'deleted_at'  => 915148800,
                    'created_by'  => $this->packHdrModel->getUserId(),
                    'updated_by'  => $this->packHdrModel->getUserId(),
                    'color'       => array_get($itemData, $item['itm_id'] . ".color", null)
                ];
                $Pack_Dt_Checksum .= $Pack_Dt_Checksum . '_' . $item['itm_id'] . '_' . $item['piece_qty'] . "&";

                $pacKDtls[] = $paramDtl;

                $skuTotal++;
            }

            for ($i = 0; $i < $numberOfPack; $i++) {
                $packHdrCreate[] = $this->createPackHdr($orderHdr, $packRef, $skuTotal, md5($Pack_Dt_Checksum),
                    $piece_total, $pacKDtls, $input['is_new_sku']);
            }

            DB::beginTransaction();

            $isFirst = true;
            foreach (array_chunk($packHdrCreate, 1500) as $key => $phrCreate) {
                if ($key > 0) {
                    $isFirst = false;
                }
                $pacKDtlCreate = [];
                $events = [];
                $pacKHdrInserted = $this->packHdrModel->insertBatch($phrCreate, $orderHdrId);
                foreach ($pacKHdrInserted as $pack) {
                    foreach ($pacKDtls as $pacKDtl) {
                        $pacKDtl['pack_hdr_id'] = object_get($pack, 'pack_hdr_id');
                        $pacKDtlCreate[] = $pacKDtl;
                        $packHdrIds[] = object_get($pack, 'pack_hdr_id');
                    }
                    $this->createEvent($orderHdr, object_get($pack, 'pack_hdr_num'), $events);
                }
                $this->packDtlModel->createBatch($pacKDtlCreate);
                $this->packHdrModel->updateOrderStatus($orderHdrId, $events, $isFirst);
                $this->eventTrackingModel->createBatch($events);
            }

            $this->_updateOrderFlow($orderHdrId, array_get($orderHdr, 'whs_id', 0));

            DB::commit();
            $this->callJobBOL($orderHdrId, array_get($orderHdr, 'whs_id', 0), $request);

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'pack_hdr_id' => $packHdrIds
                ]
            ])->setStatusCode(Response::HTTP_CREATED);
        } catch (\PDOException $e) {
            //    return $this->response->errorBadRequest(
            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $orderHdrId
     * @param $packHdrId
     * @param Request $request
     * @param PackDtlValidator $packDtlValidator
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function update(
        $orderHdrId,
        $packHdrId,
        Request $request,
        PackDtlValidator $packDtlValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['pack_hdr_id'] = $packHdrId;
        $packDtlValidator->validate($input);
        $piece_total = array_sum(array_column($input['items'], 'piece_qty'));

        if (empty($piece_total)) {
            throw new \Exception(Message::get("VR001", "Pack Qty"));
        }

        try {
            $orderHdr = $this->orderHdrModel->getFirstBy("odr_id", $orderHdrId, ['details.item']);
            $itemData = array_pluck($orderHdr->details->toArray(), 'item', 'item.item_id');
            $skuTotal = 0;
            $Pack_Dt_Checksum = '';
            $events = [];
            DB::beginTransaction();
            foreach ($input['items'] as $item) {

                if (!$this->packDtlModel->checkWhere([
                    'pack_dtl_id' => $item['pack_dtl_id'],
                    'pack_hdr_id' => $packHdrId,
                    'odr_hdr_id'  => $orderHdrId
                ])
                ) {
                    throw new \Exception(Message::get("BM017", "Order-Pack-Detail"));
                }

                if (empty($item['piece_qty'])) {
                    $this->packDtlModel->refreshModel();
                    $this->packDtlModel->deleteById($item['pack_dtl_id']);
                    continue;
                }

                $this->packDtlModel->refreshModel();
                $paramDtl = [
                    'pack_dtl_id' => $item['pack_dtl_id'],
                    'pack_hdr_id' => $packHdrId,
                    'odr_hdr_id'  => $orderHdrId,
                    'whs_id'      => $orderHdr->whs_id,
                    'cus_id'      => $orderHdr->cus_id,
                    'item_id'     => $item['itm_id'],
                    'size'        => $item['size'],//array_get($itemData, $item['itm_id'] . ".size", null),
                    'lot'         => $item['lot'],//array_get($itemData, $item['itm_id'] . ".lot", null),
                    'sku'         => $item['sku'],//array_get($itemData, $item['itm_id'] . ".sku", null),
                    'cus_upc'     => array_get($itemData, $item['itm_id'] . ".cus_upc", null),
                    'uom_id'      => array_get($itemData, $item['itm_id'] . ".uom_id", null),
                    'piece_qty'   => $item['piece_qty'],
                    'sts'         => 'u',
                    'color'       => array_get($itemData, $item['itm_id'] . ".color", null)
                ];
                $Pack_Dt_Checksum .= $Pack_Dt_Checksum . '_' . $item['itm_id'] . '_' . $item['piece_qty'] . "&";
                // Update Pack Detail
                $this->packDtlModel->update($paramDtl);
                $skuTotal++;

            }
            // Update Piece Total and Sku Total for Pack Hdr
            $this->packHdrModel->refreshModel();
            $this->packHdrModel->updateWhere([
                'piece_ttl'        => $piece_total,
                'sku_ttl'          => $skuTotal,
                'pack_dt_checksum' => md5($Pack_Dt_Checksum)
            ], [
                'pack_hdr_id' => $packHdrId
            ]);

            // Create update PackDtlChecksum
            $this->createPackDtlChecksum($orderHdrId, $packHdrId);

            // Update status order
            $this->packHdrModel->updateOrderStatus($orderHdrId, $events);

            // Write Event Tracking
            $this->writeEventTracking($events);

            $this->_updateOrderFlow($orderHdrId, $orderHdr->whs_id);

            DB::commit();
            $this->callJobBOL($orderHdrId, $orderHdr->whs_id, $request);

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'pack_hdr_id' => $packHdrId
                ]
            ])->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function callJobBOL($orderId, $whsId, $request)
    {
        $order = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderId]);
        $odr_sts = object_get($order, 'odr_sts', '');

        if ($odr_sts == 'PA') {
            dispatch(new BOLJob($orderId, $whsId, $request));
        }
    }

    /**
     * @param $orderHdr
     * @param $packRef
     * @param $skuTotal
     * @param $pack_dtl_checksum
     * @param $piece_ttl
     * @param array $pacKDtls
     *
     * @return array
     */
    private function createPackHdr($orderHdr, $packRef, $skuTotal, $pack_dtl_checksum, $piece_ttl, $pacKDtls = [], $isNewSku)
    {
        $paramPackHdr = [
            'odr_hdr_id'   => $orderHdr['odr_id'],
            'whs_id'       => $orderHdr['whs_id'],
            'cus_id'       => $orderHdr['cus_id'],
            'carrier_name' => $orderHdr['carrier'],
            'sku_ttl'      => $skuTotal,
            'piece_ttl'    => $piece_ttl,
            'pack_sts'     => "AC",
            'sts'          => "i",
            'ship_to_name' => $orderHdr['ship_to_name'],
            'pack_ref_id'  => object_get($packRef, 'pack_ref_id', null),
            'width'        => object_get($packRef, 'width', null),
            'height'       => object_get($packRef, 'height', null),
            'length'       => object_get($packRef, 'length', null),
            // 'dimension'    => object_get($packRef, 'dimension', 0),
            'created_at'   => time(),
            'updated_at'   => time(),
            'created_by'   => $this->packHdrModel->getUserId(),
            'updated_by'   => $this->packHdrModel->getUserId(),
            'deleted'      => 0,

            'deleted_at'       => 915148800,
            'pack_dt_checksum' => $pack_dtl_checksum,
            'pack_type'        => object_get($packRef, 'pack_type', 0),

            'item_id' => array_get($pacKDtls, '0.item_id', ''),
            'size'    => array_get($pacKDtls, '0.size', ''),
            'lot'     => array_get($pacKDtls, '0.lot', ''),
            'sku'     => array_get($pacKDtls, '0.sku', ''),
            'color'   => array_get($pacKDtls, '0.color', ''),
            'cus_upc' => array_get($orderHdr, "details.0.item.cus_upc", ''),
        ];

        if (!$isNewSku){
            $paramPackHdr = array_merge($paramPackHdr, [
                'item_id'   => null,
                'size'      => null,
                'lot'       => null,
                'sku'       => null,
                'color'     => null,
                'cus_upc'   => null
            ]);
        }

        return $paramPackHdr;
    }

    public function createEvent($orderHdr, $packNum, &$events)
    {
        // Create Event Tracking
        $events[] = [
            'whs_id'     => $orderHdr['whs_id'],
            'cus_id'     => $orderHdr['cus_id'],
            'owner'      => $orderHdr['odr_num'],
            'evt_code'   => Status::getByKey("Event", "PACKING"),
            'trans_num'  => $packNum,
            'created_at' => time(),
            'created_by' => $this->packDtlModel->getUserId(),
            'info'       => sprintf(Status::getByKey("Event-Info", "PACKING"), $packNum),
        ];
        $events[] = [
            'whs_id'     => $orderHdr['whs_id'],
            'cus_id'     => $orderHdr['cus_id'],
            'owner'      => $orderHdr['odr_num'],
            'evt_code'   => Status::getByKey("Event", "PACKING-COMPLETE"),
            'created_at' => time(),
            'created_by' => $this->packDtlModel->getUserId(),
            'trans_num'  => $packNum,
            'info'       => sprintf(Status::getByKey("Event-Info", "PACKING-COMPLETE"), $packNum),
        ];
    }


    public function createPackDtlChecksum($odrHdrId, $packHdrId = null)
    {
        //Create/update Pack_dt_checksum
        //get Distinct PackHdrID By OrderIds
        if ($packHdrId) {
            $packHdrIds[]['pack_hdr_id'] = $packHdrId;
        } else {
            $packHdrIds = $this->packDtlModel->getDistinctPackHdrIDByOrderIds($odrHdrId)->toArray();
        }

        foreach ($packHdrIds as $packHdrId) {
            //get packDtl according to pack_hdr_id
            $packDtlAccordingToPackId = $this->packDtlModel->findWhere(['pack_hdr_id' => $packHdrId['pack_hdr_id']])
                ->toArray();

            $Pack_Dt_Checksum = '';

            if ($packDtlAccordingToPackId) {
                foreach ($packDtlAccordingToPackId as $packDtl) {
                    // Create Pack_Dt_Checksum for a packHdrID
                    $Pack_Dt_Checksum .= md5($Pack_Dt_Checksum . '_' . $packDtl['item_id'] . '_' . $packDtl['piece_qty']) . "&";
                }

            }
            //update Pack_Dt_Checksum to PackHdr
            $dataUpdate = [
                'pack_hdr_id'      => $packHdrId['pack_hdr_id'],
                'pack_dt_checksum' => $Pack_Dt_Checksum,
            ];
            $this->packHdrModel->update($dataUpdate);
        }
    }

    public function createPackDtlChecksumAllOrders()
    {
        //Create/update Pack_dt_checksum
        //get Distinct PackHdrID By OrderIds

        $packHdrIds = $this->packDtlModel->getDistinctPackHdrIDByOrderIds()->toArray();

        foreach ($packHdrIds as $packHdrId) {
            //get packDtl according to pack_hdr_id
            $packDtlAccordingToPackId = $this->packDtlModel->findWhere(['pack_hdr_id' => $packHdrId['pack_hdr_id']])
                ->toArray();

            $Pack_Dt_Checksum = '';

            if ($packDtlAccordingToPackId) {
                foreach ($packDtlAccordingToPackId as $packDtl) {
                    // Create Pack_Dt_Checksum for a packHdrID
                    $Pack_Dt_Checksum .= $Pack_Dt_Checksum . '_' . $packDtl['item_id'] . '_' . $packDtl['piece_qty'] . "&";

                }

            }
            //update Pack_Dt_Checksum to PackHdr
            $dataUpdate = [
                'pack_hdr_id'      => $packHdrId['pack_hdr_id'],
                'pack_dt_checksum' => md5($Pack_Dt_Checksum),
            ];
            $outPallet = $this->packHdrModel->update($dataUpdate);
        }
        if ($outPallet) {
            return $this->response->noContent()
                ->setContent(['data' => ['message' => 'Create checkSum successfully!']])
                ->setStatusCode(Response::HTTP_CREATED);
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _updateOrderFlow($odrId, $whsId)
    {
        $odrObj = $this->orderHdrModel->getFirstWhere(['odr_id' => $odrId]);
        $odrSts = object_get($odrObj, 'odr_sts');

        if ($odrSts == 'PA') {
            $orderFlow = $this->orderHdrMetaModel->getOrderFlow($odrId);
            $isAutoPAM = $this->orderHdrMetaModel->getFlow($orderFlow, 'PAM');
            // $isAutoBOL = $this->orderHdrMetaModel->getFlow($orderFlow, 'BOL');
            $isAutoARS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ARS');
            // $isAutoASS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ASS');

            $flagStatus = $odrObj->odr_sts;
            if (array_get($isAutoPAM, 'usage', -1) == 1) {
                $flagStatus = 'PTD';
            }

            if (array_get($isAutoPAM, 'usage', -1) == 1 &&
                    array_get($isAutoARS, 'usage', -1) == 1) {
                $flagStatus = 'RS';
            }

            if ($odrObj->odr_sts != $flagStatus) {
                $odrObj->odr_sts = $flagStatus;
                $odrObj->save();
            }
        }
    }
}
