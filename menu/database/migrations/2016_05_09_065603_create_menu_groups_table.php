<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_group', function (Blueprint $table) {
            $table->increments('menu_group_id');
            $table->string('name', 50);
            $table->string('description', 255)->nullable()->default('NULL');
            $table->timestamps();

            $table->unique('name');
        });
    }

    /**git
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_group');
    }
}
