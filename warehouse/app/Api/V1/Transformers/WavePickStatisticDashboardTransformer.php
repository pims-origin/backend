<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use App\CustomObject;
use Seldat\Wms2\Models\WavepickHdr;
use League\Fractal\TransformerAbstract;


class WavePickStatisticDashboardTransformer extends TransformerAbstract
{
    /**
     * @param WavepickHdr $wvHdrInfo
     *
     * @return array
     */
    public function transform(WavepickHdr $wvHdrInfo)
    {
        return [
            'wv_id'          => object_get($wvHdrInfo, 'wv_id', 0),
            'wv_num'         => object_get($wvHdrInfo, 'wv_num', ''),
            'wv_sts'         => object_get($wvHdrInfo, 'wv_sts', ''),
            'picker'         => object_get($wvHdrInfo, 'picker', ''),
            'total'          => intval($wvHdrInfo->total),
            'picked'         => intval($wvHdrInfo->picked),
            'percent_picked' => floatval($wvHdrInfo->percent_picked),
            'created_at'     => object_get($wvHdrInfo, 'created_at', '')->format('Y-m-d'),
        ];
    }

}