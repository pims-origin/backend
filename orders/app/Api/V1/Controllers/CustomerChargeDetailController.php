<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerChargeDetailModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\ChargeCodeModel;
use App\Api\V1\Transformers\CustomerChargeDetailTransformer;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Transformers\ChargeCodeTransformer;
use App\Api\V1\Validators\CustomerChargeDetailValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\V1\Validators\CustomerChargeDetailDeleteMassValidator;


class CustomerChargeDetailController extends AbstractController
{
    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var $customerChargeDetailModel
     */
    protected $customerChargeDetailModel;

    /**
     * @var customerChargeDetailValidator
     */
    protected $customerChargeDetailValidator;

    /**
     * @var CustomerModel
     */
    protected $customerModel;

    /**
     * @var chargeCodeModel
     */
    protected $chargeCodeModel;

    /**
     * CustomerChargeDetailController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param CustomerChargeDetailModel $customerChargeDetailModel
     * @param CustomerModel $customerModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        CustomerChargeDetailModel $customerChargeDetailModel,
        CustomerModel $customerModel,
        ChargeCodeModel $chargeCodeModel
    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->customerChargeDetailModel = $customerChargeDetailModel;
        $this->customerModel = $customerModel;
        $this->chargeCodeModel = $chargeCodeModel;
    }




}
