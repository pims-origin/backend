<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        if (! Schema::hasTable('block_dtl')) {
            Schema::create('block_dtl', function (Blueprint $table) {
                $table->increments('block_dtl_id');
                $table->integer('block_hdr_id');
                $table->integer('whs_id');
                $table->integer('cus_id');
                $table->integer('item_id');
                $table->string('sku', 50);
                $table->string('size', 30);
                $table->string('color', 30);
                $table->integer('loc_id');
                $table->string('loc_name', 50);
                $table->char('block_dtl_sts', 2);
                $table->integer('created_by');
                $table->integer('updated_by');
                $table->integer('created_at');
                $table->integer('updated_at');
                $table->char('sts', 1);
                $table->tinyInteger('deleted');
                $table->integer('deleted_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        if (Schema::hasTable('block_dtl')) {
            Schema::drop('block_dtl');
        }
    }
}
