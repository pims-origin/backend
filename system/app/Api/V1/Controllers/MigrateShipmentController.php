<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Validators\BOLHdrValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Helper;


/**
 * Class BOLController
 *
 * @package App\Api\V1\Controllers
 */
class MigrateShipmentController extends AbstractController
{
    CONST HG_ID = 4;//1 //4
    CONST WHS_ID = 1;
    CONST _TABLE = 'nj_vcny_loc_ctnr_ship_tmp'; //loc_ctnr_ship_tmp //nj_vcny_loc_ctnr_ship_tmp
    public function insertItem($items)
    {
        // Get info Items Exists
        $listItems = DB::table('item')
            ->select('item_id', 'sku', 'size', 'color')
            ->whereIn(DB::raw("CONCAT(sku,'-',size,'-',color)"), array_keys($items))->get();

        foreach ($listItems as $item) {
            $key = $item['sku'].'-'.$item['size'].'-'.$item['color'];
            if (!empty($items[$key])) {
                unset($items[$key]);
            }
        }
        $rs = [];
        try {
            if (!empty($items)) {
                foreach ($items as $item) {
                    $rs[] = [
                        'sku'        => $item['sku'],
                        'size'       => $item['size'],
                        'color'      => $item['color'],
                        'length'     => $item['length'],
                        'width'      => $item['width'],
                        'height'     => $item['height'],
                        'weight'     => $item['weight'],
                        'created_at' => time(),
                        'created_by' => 1,
                        'updated_at' => time(),
                        'updated_by' => 1,
                        'deleted_at' => 915148800,
                        'deleted'    => 0,
                        'volume'     => Helper::calculateVolume($item['length'], $item['width'], $item['height']),
                        'cube'       => Helper::calculateCube($item['length'], $item['width'], $item['height']),
                        'uom_name'   => 'EACH',
                        'uom_code'   => 'EA',
                        'uom_id'     => 1,
                        'pack'       => $item['ctn_pack_size'],
                        'cus_id'     => self::HG_ID,
                        'status'     => 'AC',
                    ];
                }
                DB::table('item')->insert($rs);
            }
        } catch (\Exception $exception) {
            return $this->response->errorBadRequest($exception->getMessage());
        }

    }

    public function insertContainers($data)
    {
        $containers = DB::table('container')->select('ctnr_num')
            ->whereIn('ctnr_num', $data)->get();

        foreach ($containers as $item) {
            if (!empty($data[$item['ctnr_num']])) {
                unset($data[$item['ctnr_num']]);
            }
        }
        try {
            if (!empty($data)) {
                $rs = [];
                foreach ($data as $name) {
                    $rs[] = [
                        'ctnr_num'   => $name,
                        'created_at' => time(),
                        'created_by' => 1,
                        'updated_at' => time(),
                        'updated_by' => 1,
                        'deleted_at' => 915148800,
                        'deleted'    => 0
                    ];
                }
                DB::table('container')->insert($rs);
            }
        } catch (\Exception $exception) {
            return $this->response->errorBadRequest($exception->getMessage());
        }
    }

    public function insertCartons($data, $ctnrs, $items)
    {
        try {
            $cartonParams = [];
            $arrError = [];
            $countItems = current(DB::table('cartons')->select(DB::raw('count(1)'))->where('ctn_num', 'like', 'CTN-1612-%')->first());
            $countItems = $countItems + 1;
            foreach ($data as $key => $row) {
                $cartonParams[] = [
                    'asn_dtl_id'    => null,
                    'gr_dtl_id'     => null,
                    'gr_hdr_id'     => null,
                    'is_damaged'    => 0,
                    'item_id'       => $items[$row['sku'].'-'.$row['size'].'-'.$row['color']],
                    'whs_id'        => self::WHS_ID,
                    'cus_id'        => self::HG_ID,
                    'ctn_num'       => 'CTN-1612-' . str_pad($countItems + $key, 4, "0", STR_PAD_LEFT),
                    'ctn_sts'       => $row['ctn_sts'],
                    'ctn_uom_id'    => 1,
                    'uom_code'      => 'EA',
                    'uom_name'      => 'EACH',
                    'loc_id'        => null,
                    'loc_code'      => null,
                    'loc_name'      => null,
                    'loc_type_code' => 'RAC',
                    'ctn_pack_size' => $row['ctn_pack_size'],
                    'piece_remain'  => $row['piece_remain'],
                    'piece_ttl'     => $row['piece_ttl'],
                    'gr_dt'         => strtotime($row['gr_dt']),
                    'sku'           => $row['sku'],
                    'size'          => $row['size'],
                    'color'         => $row['color'],
                    'lot'           => $row['lot'],
                    'po'            => $row['po'],
                    'upc'           => null,
                    'ctnr_id'       => $ctnrs[$row['containerName']],
                    'ctnr_num'      => $row['containerName'],
                    'length'        => $row['length'],
                    'width'         => $row['width'],
                    'height'        => $row['height'],
                    'weight'        => $row['weight'],
                    'volume'        => Helper::calculateVolume($row['length'], $row['width'], $row['height']),
                    'cube'          => Helper::calculateCube($row['length'], $row['width'], $row['height']),
                    'expired_dt'    => null,
                    'created_at'    => time(),
                    'updated_at'    => time(),
                    'deleted'       => 0,
                    'deleted_at'    => 915148800,
                    'updated_by'    => 1,
                    'created_by'    => 1,
                    'plt_id'        => null,
                    'shipped_dt'    => strtotime($row['shipped_dt'])
                ];

            }
            if (!empty($arrError)) {
                dd(implode(', ', $arrError));
            }
            DB::table('cartons')->insert($cartonParams);

        } catch (\Exception $exception) {
            dd($row['sku'].'-'.$row['size'].'-'.$row['color']);
            return $this->response->errorBadRequest($exception->getMessage());
        }
    }

    public function store(Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        //get params
        try {
            $jThis = $this;
            DB::beginTransaction();
            //DB::table('loc_ctnr_ship_tmp')->chunk(300, function($data) use ($jThis) {
            DB::table(self::_TABLE)->chunk(300, function($data) use ($jThis) {
                // 1.0 Get info
                $items = [];
                $ctnrs = [];
                foreach ($data as $key => $item) {
                    $keyItem = $item['sku'] . '-' . $item['size'] . '-' . $item['color'];
                    $items[$keyItem] = [
                        'sku' => $item['sku'],
                        'size' => $item['size'],
                        'color' => $item['color'],
                        'length' => $item['length'],
                        'width' => $item['width'],
                        'height' => $item['height'],
                        'weight' => $item['weight'],
                        'ctn_pack_size' => $item['ctn_pack_size']
                    ];
                    $ctnrs[$item['containerName']] = $item['containerName'];
                }

                // 1.1 Insert Items
                $this->insertItem($items);

                // 1.2 Create Containers
                $this->insertContainers($ctnrs);

            });
            DB::commit();
            return 'Successfully Insert Data';

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function update(Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        //get params
        try {
            //$data = DB::table('loc_ctnr_ship_tmp')->where('cartonID', '<', 7590605)->get();
            //$data = DB::table('loc_ctnr_ship_tmp')->whereBetween('cartonID', [7590779, 7591994])->get();
            //$data = DB::table('loc_ctnr_ship_tmp')->whereBetween('cartonID', [5045970, 5049315])->get();
            //$data = DB::table('loc_ctnr_ship_tmp')->whereBetween('cartonID', [5049316, 5049315])->get();
            //$data = DB::table('loc_ctnr_ship_tmp')->where('cartonID', '>', 5049315)->get();
            //$data = DB::table('nj_vcny_loc_ctnr_ship_tmp')->get();

            /*SELECT * FROM `nj_vcny_loc_ctnr_ship_tmp` WHERE cartonID < 7590779;
            SELECT * FROM `nj_vcny_loc_ctnr_ship_tmp` WHERE cartonID BETWEEN 7590779 AND 7600363
            SELECT * FROM `nj_vcny_loc_ctnr_ship_tmp` WHERE cartonID > 7600363;*/
            $jThis = $this;
            DB::beginTransaction();
            //DB::table('nj_vcny_loc_ctnr_ship_tmp')->where('cartonID', '>', 7635136)->chunk(100, function($data) use ($jThis) {
            //DB::table('nj_vcny_loc_ctnr_ship_tmp')->whereBetween('cartonID', [7600363, 7635136])->chunk(100, function($data) use ($jThis) {
            DB::table(self::_TABLE)->chunk(200, function($data) use ($jThis) {

                // 1.0 Get info
                $items = [];
                $ctnrs = [];
                foreach ($data as $key => $item) {
                    $keyItem = $item['sku'] . '-' . $item['size'] . '-' . $item['color'];
                    $items[$keyItem] = [
                        'sku' => $item['sku'],
                        'size' => $item['size'],
                        'color' => $item['color'],
                        'length' => $item['length'],
                        'width' => $item['width'],
                        'height' => $item['height'],
                        'weight' => $item['weight'],
                        'ctn_pack_size' => $item['ctn_pack_size']
                    ];
                    $ctnrs[$item['containerName']] = $item['containerName'];
                }

                $items = DB::table('item')
                    ->select('item_id', 'sku', 'size', 'color')
                    ->whereIn(DB::raw("CONCAT(sku,'-',size,'-',color)"), array_keys($items))->get();
                $arrItems = [];

                foreach ($items as $item) {
                    $key = $item['sku'] . '-' . $item['size'] . '-' . $item['color'];
                    $arrItems[$key] = $item['item_id'];
                }

                $containers = DB::table('container')->select('ctnr_num', 'ctnr_id')
                    ->whereIn('ctnr_num', $ctnrs)->get();
                $ctnrs = [];
                foreach ($containers as $item) {
                    $ctnrs[$item['ctnr_num']] = $item['ctnr_id'];
                }

                // 1.3 create cartons
                $this->insertCartons($data, $ctnrs, $arrItems);

            });

            DB::commit();
            return 'Successfully Update Data';

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }
}
