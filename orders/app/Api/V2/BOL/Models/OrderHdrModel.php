<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\BOL\Models;

use App\Api\V1\Traits\WorkOrderTrait;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\BOLOrderDetail;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{
    use WorkOrderTrait;

    const DAS_LIMIT = 2;
    const DAS_VALID_DATE = [2, 7];

    protected $statusType = [
        'csr'               => ['NW'],
        'allocate'          => ['NW', 'AL', 'PAL'],
        'wave-pick'         => ['PK', 'PPK', 'PD', 'PIP', 'AL', 'PAL'],
        'packing'           => ['PD', 'PIP', 'PN', 'PPA', 'PA', 'PAP'],
        'pallet-assignment' => ['PA', 'PAP'],
        'shipping'          => ['SH', 'ST', 'PSH'],
        'cancel-order'      => ['CC']
    ];

    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     * @param BOLOrderDetail|null $bolModel
     */
    public function __construct(OrderHdr $model = null, BOLOrderDetail $bolModel = null)
    {
        $this->model = ($model) ?: new OrderHdr();
        $this->bolModel = ($bolModel) ?: new BOLOrderDetail();
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
    }


    /**
     * @param $orderHdrIds
     * @param array $with
     *
     * @return mixed
     */
    public function loadByOrdIds($orderHdrIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('odr_id', $orderHdrIds);
        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }

}
