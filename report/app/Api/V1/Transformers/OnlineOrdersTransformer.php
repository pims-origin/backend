<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\OrderDtl;

class OnlineOrdersTransformer extends AbstractTransformer
{
    public function transform(OrderDtl $order)
    {
        return [
            'cus_name' => object_get($order, 'customer.cus_name', NULL),
            'user_name' => trim(object_get($order, 'orderHdr.csrUser.first_name', NULL) .
                    ' ' . object_get($order, 'orderHdr.csrUser.last_name', NULL)),
            'odr_sts' => object_get($order, 'odr_sts', NULL),
            'odr_num' => object_get($order, 'odr_num', NULL),
            'cus_po' => object_get($order, 'cus_po', NULL),
            'cus_odr_num' => object_get($order, 'cus_odr_num', NULL),
            'ship_to_name' => object_get($order, 'ship_to_name', NULL),
            'ship_to_address' => trim(object_get($order, 'ship_to_add_1', NULL) . ' ' .
                    object_get($order, 'ship_to_add_2', NULL)),
            'ship_to_city' => object_get($order, 'ship_to_city', NULL),
            'ship_to_state' => object_get($order, 'ship_to_state', NULL),
            'ship_to_zip' => object_get($order, 'ship_to_zip', NULL),
            'ship_to_country' => object_get($order, 'ship_to_country', NULL),
            'sku' => $order->sku,
            'upc' => object_get($order, 'item.item_code', ''),
            'product_description' => object_get($order, 'item.description', ''),
            'whs_name' => object_get($order, 'warehouse.whs_name', ''),
            'qty' => $order->qty,
            'pieces' => $order->piece_qty,
            'odr_req_dt' => $this->getDate(object_get($order, 'odr_req_dt', NULL)),
            'ship_by_dt' => $this->getDate(object_get($order, 'ship_by_dt', NULL)),
            'cancel_by_dt' => $this->getDate(object_get($order, 'cancel_by_dt', NULL)),
            'carrier' => object_get($order, 'carrier', NULL),
            'cus_notes' => object_get($order, 'cus_notes', NULL),
            'website' => object_get($order, 'website', NULL),
        ];
    }

    /*
    ***************************************************************************
    */

}
