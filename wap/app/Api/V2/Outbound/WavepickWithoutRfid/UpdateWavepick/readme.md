#########################-START-VERSION-2-##################################
- WAP-707: [Outbound - Wavepick] Modify API update wavepick
    + Completed wave pick detail -> release reserve location
    + Validate location for Update Wave pick

- WAP-623: [Outbound - Wave Pick] Auto assign cartons to Order if 1 Wave Pick just have only 1 Order (Case RFID)

- WAP-432: [Outbound - Wavepick] Create API to pick cartons/pieces for location has no RFID
#########################-END-VERSION-2-##################################