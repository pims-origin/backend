<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\EventTracking;

class EventTrackingListsTransformer extends TransformerAbstract
{
    /**
     * @param EventTracking $eventTracking
     *
     * @return array
     */
    public function transform(EventTracking $eventTracking)
    {
        return [
            'owner'       => object_get($eventTracking, 'owner', null),
            'trans_num'   => object_get($eventTracking, 'trans_num', null),
            'evt_code'    => object_get($eventTracking, 'evt_code', null),
            'description' => object_get($eventTracking, 'event.des', null),
            'evt_type'    => object_get($eventTracking, 'event.type', null),
            'created_at'  => $eventTracking->created_at->format('m/d/Y H:i:s'),
            'info'        => object_get($eventTracking, 'info', null),
            'user'        => object_get($eventTracking, 'createdUser.first_name', null) . ' ' .
                object_get($eventTracking, 'createdUser.last_name', null),

        ];

    }

}
