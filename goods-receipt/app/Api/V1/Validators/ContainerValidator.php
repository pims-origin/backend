<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 01/July/2016
 * Time: 11:05
 */

namespace App\Api\V1\Validators;


class ContainerValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'ctnr_num' => 'required',

        ];
    }
}
