<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;

class TransferWarehouseIMSJob extends IMSJob
{
    protected $orderId;

    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    public function handle()
    {
        try {
            $token = $this->login();

            $endpoint = env('IMS_TRANSFER_WH');

            // Check IMS Configuration
            if (empty($endpoint)) {
                $this->writeLogment('IMS Transfer Warehouse', 'IMS Transfer Warehouse Endpoint is not define!');
                return false;
            }

            $odrHdr = DB::table('odr_hdr')
                ->leftJoin('warehouse', 'warehouse.whs_id', '=', 'odr_hdr.whs_id_to')
                ->where('odr_hdr.odr_id', $this->orderId)
                ->where('odr_hdr.odr_type', 'WHS')
                ->select([
                    'warehouse.whs_code as des_whs_code',
                    'odr_hdr.odr_id',
                    'odr_hdr.whs_id as org_whs_id',
                    'odr_hdr.odr_num as order_num',
                    'odr_hdr.ref_cod as ref',
                    'odr_hdr.odr_type',
                ])
                ->first();

            $items = DB::table('odr_dtl')
                ->select([
                    'item.sku',
                    'item.size',
                    'item.color',
                    'item.description',
                    'item.uom_code as uom',
                    'item.pack',
                    'cartons.po',
                    'cartons.expired_dt as expired_date',                    
                    DB::raw('SUM(is_damaged) as damaged_ctns'),
                    DB::raw('SUM(cartons.is_damaged * odr_dtl.piece_qty) AS damaged_qty'),
                    DB::raw('COUNT(cartons.ctn_id) AS actual_ctns'),
                    // DB::raw('inventory.in_hand_qty AS actual_qty')
                    'odr_dtl.piece_qty AS actual_qty'
                ])
                ->leftJoin('item', 'item.item_id', '=', 'odr_dtl.item_id')
                ->leftJoin('cartons', 'cartons.item_id', '=', 'odr_dtl.item_id')
                // ->leftJoin('inventory', 'inventory.item_id', '=', 'odr_dtl.item_id')
                ->where('odr_dtl.odr_id', $this->orderId)
                ->groupBy('cartons.item_id')
                ->get();

            $items = $this->itemsTransformer($items);

            $odrNum = array_get($odrHdr, 'order_num');

            $data = [
                'type'      => 'warehouse-transfer',
                'id'        => array_get($odrHdr, 'odr_id'),
                'attributes' => [
                    'order_num'     => array_get($odrHdr, 'order_num'),
                    'org_whs_id'    => array_get($odrHdr, 'org_whs_id'),
                    'des_whs_code'  => array_get($odrHdr, 'des_whs_code'),
                    'ref'           => array_get($odrHdr, 'ref'),
                    'expected_date' => null,
                    'actual_date'   => null,
                    'items'         => $items
                ]
            ];

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-type: application/json"
            ];

            $result = $this->call($endpoint, 'POST', json_encode(['data' => $data]), $header);

            $this->writeLogment('IMS Transfer Warehouse ' . $odrNum, 'URL: '. $endpoint .' --- Request: ' . json_encode(['data' => $data]) . ' --- Response: ' . $result);
        } catch (\Exception $e) {
            $this->writeLogment('IMS Transfer Warehouse', $e->getMessage());
        }
    }

    public function itemsTransformer($items)
    {
        $result = [];
        foreach($items as $key => $item) {
            $result[$key] = [
                "po"            => array_get($item, 'po', null),
                "po_date"       => null,
                "sku"           => array_get($item, 'sku', null),
                "size"          => array_get($item, 'size', null),
                "color"         => array_get($item, 'color', null),
                "description"   => array_get($item, 'description', null),
                "uom"           => array_get($item, 'uom', null),
                "pack"          => array_get($item, 'pack', null),
                "actual_qty"    => array_get($item, 'actual_qty', null),
                "actual_ctns"   => array_get($item, 'actual_ctns', null),
                "damaged_qty"   => array_get($item, 'damaged_qty', null),
                "damaged_ctns"  => array_get($item, 'damaged_ctns', null),
                "expired_date"  => !empty(array_get($item, 'expired_date', null)) ? date('Y-m-d', array_get($item, 'expired_date')) : null
            ];
        }

        return $result;
    }
}