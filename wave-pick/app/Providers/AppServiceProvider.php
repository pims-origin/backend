<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Validator::extend('greater_than_zero', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $min_value = 0;
            return $value > $min_value;
        });

        Validator::replacer('greater_than_zero', function($message, $attribute, $rule, $parameters) {
            return str_replace('_', ' ' , 'The '. $attribute .' must be greater than 0');
        });
    }
}
