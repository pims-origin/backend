<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V2\Inbound\Pallet2\ScanPalletOptionYes\Models;

use Seldat\Wms2\Models\Item;

class ItemModel extends AbstractModel
{
    /**
     * ItemModel constructor.
     * @param Item|null $model
     */
    public function __construct(Item $model = null)
    {
        $this->model = ($model) ?: new Item();
    }

    public function checkBySkuSizeColor($skuSizeColor)
    {
        if (count($skuSizeColor)==0) {
            return 0;
        }
        $query = $this->make();

        foreach ($skuSizeColor as $each) {
            $query->orWhere(function ($query) use ($each) {
                $query->where('sku', $each['sku']);
                $query->where('size', $each['size']);
                $query->where('color', $each['color']);
            });
        }
        return $query->count();
    }
}
