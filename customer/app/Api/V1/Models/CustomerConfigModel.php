<?php

namespace App\Api\V1\Models;

use Illuminate\Database\Query\Builder;
use Seldat\Wms2\Models\CustomerConfig;
use Seldat\Wms2\Utils\SelArr;
use Wms2\UserInfo\Data;

class CustomerConfigModel extends AbstractModel
{
    protected $model;

    public function __construct(CustomerConfig $model = null)
    {
        $this->model = ($model) ?: new CustomerConfig();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function getCustomerConfigs($data)
    {
        return $this->model
            ->where('cus_id', $data['cus_id'])
            ->where('whs_id', $data['whs_id'])
            ->where('config_name', $data['config_name'])
            ->where('config_value', $data['config_value'])
            ->first();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateCustomerConfigs($data)
    {
        return $this->model
            ->where('cus_id', $data['cus_id'])
            ->where('whs_id', $data['whs_id'])
            ->where('config_name', $data['config_name'])
            ->where('config_value', $data['config_value'])
            ->update(
                [
                    'ac'  => $data['ac'],
                    'sts' => 'u',
                ]
            );
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->where('config_name', 'edi_integration');

        if (isset($attributes['whs_id'])) {
            $query->where('whs_id', $attributes['whs_id']);
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['edi'])) {
            $query->where('ac', $attributes['edi']);
        }

        $this->sortBuilder($query, $attributes);
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param $edi
     * @param array $with
     *
     * @return mixed
     */
    public function getCustomerEdi($whsId, $cusId, $edi, $with = [])
    {
        $query = $this->make($with);
        $query->where('config_name', 'edi_integration');

        if ($whsId) {
            $query->where('whs_id', $whsId);
        }
        if ($cusId) {
            $query->where('cus_id', $cusId);
        }
        if ($edi) {
            $query->where('ac', $edi);
        }
        $query = $query->get();

        return $query;
    }


}
