<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace Seldat\Wms2\Models;

class DamageType extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'damage_type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'dmg_id';

    /**
     * @var array
     */
    protected $fillable = [
        'dmg_code',
        'dmg_name',
        'dmg_des',
        'created_by',
        'updated_by'
    ];


}
