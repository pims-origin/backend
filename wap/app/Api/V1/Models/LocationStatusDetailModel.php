<?php
/**
 * Created by PhpStorm.
 *
 * Date: 8-august-16
 * Time: 10:29
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\LocationStatusDetail;

class LocationStatusDetailModel extends AbstractModel
{
    /**
     * @var LocationStatusDetail
     */
    protected $model;

    /**
     * @param LocationStatusDetail $model
     */
    public function __construct(LocationStatusDetail $model = null)
    {
        $this->model = ($model) ?: new LocationStatusDetail();
    }

    /**
     * @param array $loc_id
     */
    public function updateLocStatusWhereIn(array $loc_id, $sts_code)
    {
        return $this->model->whereIn('loc_sts_dtl_loc_id', $loc_id)->update(['loc_sts_dtl_sts_code' => $sts_code]);
    }

}
