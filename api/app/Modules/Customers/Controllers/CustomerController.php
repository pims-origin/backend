<?php

namespace App\Modules\Customers\Controllers;

use Illuminate\Support\Facades\Config;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Modules\Customers\Models\CustomerModel;

use App\Modules\Customers\Resources\CustomerReadResource;
use App\Modules\Customers\Resources\CustomerCreatedResource;
use App\Modules\Customers\Resources\CustomerCollection;
use App\Modules\Customers\Validators\CustomerValidator;
use App\Modules\Customers\Validators\CustomerStoreValidator;

class CustomerController extends Controller
{
    protected $customerModel;

    public function __construct(CustomerModel $customerModel)
    {
        $this->customerModel = $customerModel;
    }

    public function index($whsId, Request $request)
    {
        $whsId = array_get($request->all(),'whs_id', $whsId);
        $input = $request->all();

        $limit = array_get($input, 'per_page', 20);

        $baseUrl = str_replace( $request->root() , env('APP_URL'), \URL::current());

        $customers = $this->customerModel->getCustomerForUserByWhs($whsId, $limit, $baseUrl);

        return new CustomerCollection($customers);
    }

    public function read($cusId, Request $request, CustomerValidator $validator)
    {
        $cusId = array_get($request->all(),'cus_id', $cusId);
        $customer = $this->customerModel->getCustomerDetail($cusId);
        $validator->checkCustomer($customer);

        return new CustomerReadResource($customer);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        (new CustomerStoreValidator)->validate($input);

        try {
            DB::beginTransaction();

            $cusId = $this->customerModel->store($input);

            $this->storeCustomerProcess($cusId);

            DB::commit();

            $customer = $this->customerModel->getCustomerById($cusId);

            $response = new CustomerCreatedResource($customer);

            DB::table('sys_bugs')->insert([
                'date'       => time(),
                'api_name'   => 'WMS Create Customer',
                'error'      => 'Request: ' . json_encode($input) . ' --- Response: ' . json_encode($response),
                'created_at' => time(),
                'created_by' => null,
                'updated_at' => time()
            ]);

            return $response;
        } catch (\Exception $e) {
            DB::rollback();

            DB::table('sys_bugs')->insert([
                'date'       => time(),
                'api_name'   => 'WMS Create Customer',
                'error'      => $e->getMessage(),
                'created_at' => time(),
                'created_by' => null,
                'updated_at' => time()
            ]);
        }
    }

    public function storeCustomerProcess($cusId)
    {
        $userId = $this->getCurrentUserId();

        $this->addZoneToCus($cusId, $userId);
        $this->addWhsToCus($cusId, $userId);
        $this->addCusToUser($cusId);
    }

    public function addWhsToCus($cusId, $userId)
    {
        $warehouses = DB::table('warehouse')
            ->select('warehouse.whs_id')
            ->join('user_whs', 'user_whs.whs_id', '=', 'warehouse.whs_id')
            ->groupBy('warehouse.whs_id')
            ->get();

        foreach($warehouses as $whs) {
            DB::table('customer_warehouse')->insert([
                'whs_id'     => $whs->whs_id,
                'cus_id'     => $cusId,
                'deleted_at' => 915148800,
                'created_at' => time(),
                'created_by' => $userId,
                'updated_at' => time(),
                'updated_by' => $userId,
                'deleted'    => 0
            ]);
        }
    }

    public function addZoneToCus($cusId, $userId)
    {
        $vZones = DB::table('zone')
            ->select('zone.zone_id')
            ->join('zone_type', 'zone_type.zone_type_id', '=', 'zone.zone_type_id')
            ->where('zone_type.zone_type_code', 'VZ')
            ->get();

        foreach($vZones as $zone) {
            DB::table('customer_zone')->insert([
                'zone_id'    => $zone->zone_id,
                'cus_id'     => $cusId,
                'deleted_at' => 915148800,
                'created_at' => time(),
                'created_by' => $userId,
                'updated_at' => time(),
                'updated_by' => $userId,
                'deleted'    => 0
            ]);
        }
    }

    public function addCusToUser($cusId)
    {
            $allUser = DB::table('users')
                ->join('user_whs', 'user_whs.user_id', '=', 'users.user_id')
                ->join('warehouse', 'warehouse.whs_id', '=', 'user_whs.whs_id')
                ->select('users.user_id', 'warehouse.whs_id')
                ->groupBy('users.user_id', 'warehouse.whs_id')
                ->get();

            foreach ($allUser as $user) {
                DB::table('cus_in_user')->insert([
                    'cus_id'    => $cusId,
                    'user_id'   => $user->user_id,
                    'whs_id'    => $user->whs_id
                ]);
            }
    }

    protected function getCurrentUserId()
    {
        $key = env("JWT_SECRET");
        $auth = app('request')->header('Authorization');
        $jwt = str_replace("Bearer ", "", $auth);
        $pub_key = openssl_pkey_get_private(FIREBASE_PRIVATE_KEY);
        $keyData = openssl_pkey_get_details($pub_key);
        $decoded =  \Firebase\JWT\JWT::decode($jwt, $keyData['key'], array('RS256'));
        $userId = $decoded->sub;
        
        return $userId;
    }
}
