<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 26-Feb-16
 * Time: 3:00 PM
 */

namespace App\Api\V2\BillOfLadings\CreateBOL\Models;

use Illuminate\Support\Facades\DB;

class ReportModel
{

    /**
     * @param $orderId
     * @return array
     */
    public function shipped($orderId)
    {
        $orderData = [];
        $return = [];
        $results = DB::table('odr_hdr')
            ->whereIn('odr_id', $orderId)->first();
        if (!$results) {
            return false;
        }
        $finalStatuses = ['SH', 'CC'];
        if (!in_array($results['odr_sts'], $finalStatuses)) {
            $orderData['wms_status_code'] = $results['odr_sts'];
            $orderData['wms_status_name'] = $results['odr_sts'];
        }
        if (empty($results)) {
            return $orderData;
        }
        $orderData = (array) $results;

        $odr_dtls = DB::table('odr_hdr')
            ->select('odr_dtl.*')
            ->join('odr_dtl', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')
            ->leftjoin('shipment', 'shipment.ship_id', '=', 'odr_hdr.ship_id')
            ->orderBy('odr_hdr.odr_id')
            ->whereRaw('odr_hdr.odr_num LIKE "'. $results['odr_num']. '%"')
            ->whereIn('odr_hdr.odr_sts', ['SH', 'CC', 'PRC'])
            ->groupBy('odr_dtl.odr_dtl_id', 'odr_hdr.odr_sts')
            ->get();

        foreach ($odr_dtls as $odr_dtl) {
            $orderData['items'][$odr_dtl['odr_dtl_id']]['product_order']= (array) $odr_dtl;
        }

        $orderData['isSuccess'] = true;
        $return ['cus_id'] = $results['cus_id'];
        $return ['whs_id'] = $results['whs_id'];
        $return ['api_app'] = 'edi';
        $return ['transactional_code'] = 'ORD';
        $return ['success_key'] = $results['odr_num'];
        $return ['data'] = [$results['odr_num'] => $orderData];

        return $return;
    }

    /**
     * @param $shipId
     * @return array
     */
    public function shipment($shipId)
    {
        $return = [];

        $shipments = DB::table('shipment')->where('shipment.ship_id', $shipId)->where('ship_sts', 'FN')->first();
        if (! $shipments) {
            return false;
        }

        $shipments['bol_odr_dtl'] = DB::table('shipment')
            ->select('bol_odr_dtl.*')
            ->join('bol_odr_dtl', 'bol_odr_dtl.ship_id', '=', 'shipment.ship_id')
            ->where('shipment.ship_id', $shipId)->get();

        $shipments['bol_carrier_dtl'] = DB::table('shipment')
            ->select('bol_carrier_dtl.*')
            ->join('bol_carrier_dtl', 'bol_carrier_dtl.ship_id', '=', 'shipment.ship_id')
            ->where('shipment.ship_id', $shipId)->get();

        $shipments['isSuccess'] = true;
        $return ['cus_id'] = $shipments['cus_id'];
        $return ['whs_id'] = $shipments['whs_id'];
        $return ['api_app'] = 'edi';
        $return ['transactional_code'] = 'BOL';
        $return ['success_key'] = $shipments['bo_num'];
        $return ['data'] = [$shipments['bo_num'] => $shipments];

        return $return;

    }

}
