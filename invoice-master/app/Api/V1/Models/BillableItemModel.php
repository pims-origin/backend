<?php

namespace App\Api\V1\Models;

use App\BillableDtl;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelStr;

class BillableItemModel extends AbstractModel
{
    /**
     * Constructor.
     */

    public function __construct()
    {
        $this->model = new BillableDtl();
    }

    public function search($input, $with = [], $limit = 10) {
        $query = $this->make($with);
        $query->select([
            'billable_dtl.*',
            'odr_hdr.cus_po'
        ]);

        $query->join('sac', 'sac.sac_id', '=', 'billable_dtl.sac_id');
        $query->leftJoin('odr_hdr', 'billable_dtl.ref_num', '=', 'odr_hdr.odr_num');

        if (!empty($input['cus_id'])) {
            $query->where('billable_dtl.cus_id', $input['cus_id']);
        }

        if (!empty($input['whs_id'])) {
            $query->where('billable_dtl.whs_id', $input['whs_id']);
        }

        if (isset($input['charge_code']) && $input['charge_code'] != '') {
            $query->where('billable_dtl.sac', 'like', "%" . SelStr::escapeLike($input['charge_code']) . "%");
        }

        if (isset($input['types']) && $input['types'] != '') {
            $query->whereIn('billable_dtl.ba_type', $input['types']);
        }

        if (isset($input['end_date']) && $input['end_date'] != '') {
            $ym = date('ym', strtotime($input['end_date']));
            $query->where('billable_dtl.ref_num', 'LIKE', '%-'.$ym.'-%');
        }

        if (isset($input['ref_num']) && $input['ref_num'] != '') {
            $query->where('billable_dtl.ref_num', 'LIKE', '%'.$input['ref_num'].'%');
        }

        $this->sortBuilder($query, $input);
        return $query->paginate($limit);
    }

    public function searchByRefnum($input, $with = [], $limit = 10, $refNum)
    {
        $query = $this->make($with);
        $query->where('ref_num', $refNum)->withTrashed();

        $this->sortBuilder($query, $input);
        return $query->paginate($limit);
    }

    public function validateBillableItemToDelete($ids) {
        $check = DB::table('inv_dtl')
            ->where('deleted', 0)
            ->whereIn('ba_dtl_id', $ids)
            ->first();
        if ($check) {
            return false;
        } else {
            return true;
        }
    }

    public function forceDelete($ids){
        $affectedRows = DB::table('billable_dtl')
                        ->whereIn('ba_dtl_id', $ids)
                        ->delete();

        return $affectedRows;
    }
}

