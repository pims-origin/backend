<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 7/25/2016
 * Time: 6:13 PM
 */

namespace App\Api\V1\Validators;


class PrintDamageCartonValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'ctn_num'            => 'require',
            'dmg_type'            => 'require',
        ];
    }
}