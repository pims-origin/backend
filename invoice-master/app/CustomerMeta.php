<?php

namespace App;


use App\Api\V1\Models\BaseInvoiceModel;

class CustomerMeta extends BaseInvoiceModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_meta';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cus_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'value',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted',
        'deleted_at',
        'qualifier'
    ];


}
