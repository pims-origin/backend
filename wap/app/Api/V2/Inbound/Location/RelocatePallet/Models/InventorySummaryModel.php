<?php

namespace App\Api\V2\Inbound\Location\RelocatePallet\Models;

use Mockery\CountValidator\Exception;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\SelStr;

class InventorySummaryModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        // search sku
        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        // search sku
        if (isset($attributes['color'])) {
            $query->where('color', 'like', "%" . SelStr::escapeLike($attributes['color']) . "%");
        }

        // search sku
        if (isset($attributes['size'])) {
            $query->where('size', 'like', "%" . SelStr::escapeLike($attributes['size']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * @param array $data
     */
    public function updateDmgQty(array $data)
    {
        foreach ($data as $detail) {
            $this->refreshModel();
            $ivt = $this->model
                ->where('item_id', $detail['item_id'])
                ->where('cus_id', $detail['cus_id'])
                ->where('whs_id', $detail['whs_id'])
                ->first()->toArray();

            if ($ivt) {
                $ivt['dmg_qty'] = (int)$ivt['dmg_qty'] + $detail['dmg_qty'];
                $ivt['avail'] = (int)$ivt['ttl'] - (int)$ivt['allocated_qty'] - (int)$ivt['dmg_qty'];
                $this->refreshModel();
                $this->update($ivt);
            }
        }
    }

    public function updateQtyByGRDtl($whsId, $cusId, $grDtl)
    {

        $invItem = $this->model->where([
            'item_id' => $grDtl['item_id'],
            'lot' => $grDtl['lot'],
            'whs_id' => $whsId,
            'cus_id' => $cusId
        ])->first();

        $ctnModel = New CartonModel();
        $availQty = $ctnModel->sumAvailQtyByAsnDtlId($grDtl['asn_dtl_id']);

        $DamagedQty = $ctnModel->sumDamagedQtyByAsnDtlId($grDtl['asn_dtl_id']);

        if ($invItem) {
            $invItem->avail += $availQty;
            $invItem->dmg_qty += $DamagedQty;
            $invItem->save();
        } else {
            //Insert
            $arrInput = [
                'item_id' => $grDtl['item_id'],
                'cus_id' => $cusId,
                'whs_id' => $whsId,
                'color' => $grDtl['color'],
                'size' => $grDtl['size'],
                'lot' => $grDtl['lot'],
                'ttl' => $availQty,
                'picked_qty' => 0,
                'allocated_qty' => 0,
                'dmg_qty' => $DamagedQty,
                'avail' => $availQty,
                'sku' => $grDtl['sku'],
                'back_qty' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ];
            $this->model->create($arrInput);
        }
    }
}
