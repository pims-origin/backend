<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Zone;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class ZoneModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct(Zone $model = null)
    {
        $this->model = ($model) ?: new Zone();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */

}
