<?php

namespace App\Api\V1\Validators;

class WorkOrderValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_id'                    => 'required|integer',
            'tax'                       => 'numeric|between:0,100',
            'subtotal'                  => 'numeric|between:0,999999999999999.99',
            'vas_note'                  => 'string|max:255',
            'items.*.ref_type'          => 'required',
            'items.*.ref_num'           => 'required|string|max:20',
            'items.*.cus_sac_id'        => 'required|integer',
            'items.*.qty'               => 'required|numeric|between:0,99999.99',
            'items.*.vas_item'          => 'string|max:30',
            'items.*.vas_description'   => 'string|max:30',
            'items.*.unit_price'        => 'required|numeric|between:0,99999.99',
        ];
    }
}
