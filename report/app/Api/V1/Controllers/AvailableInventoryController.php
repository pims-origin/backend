<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\AvailableInventoryModel;
use App\Api\V1\Transformers\AvailableInventoryTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class AvailableInventoryController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new AvailableInventoryModel();
        $this->transformer = new AvailableInventoryTransformer();
    }

    /*
    ****************************************************************************
    */

}
