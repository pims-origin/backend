<?php

namespace App;

/**
 * @property mixed sys_country_id
 * @property mixed sys_country_code
 * @property mixed sys_country_name
 */
class SystemCountry extends BaseSoftModel
{
    protected $table = 'system_country';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_country_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sys_country_code',
        'sys_country_name'
    ];
}