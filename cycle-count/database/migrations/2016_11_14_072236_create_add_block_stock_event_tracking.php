<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddBlockStockEventTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('evt_lookup')) {
            DB::table('evt_lookup')->insert([
                ['evt_code'=>'BSC', 'trans'=>'BLK', 'des' => 'Create Block Stock'],
                ['evt_code'=>'CPL', 'trans'=>'BLK', 'des' => 'Complete Block Stock'],
                ['evt_code'=>'RSC', 'trans'=>'RSN', 'des' => 'Create Block Reason']
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
