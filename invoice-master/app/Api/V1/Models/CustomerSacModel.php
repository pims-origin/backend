<?php
namespace App\Api\V1\Models;
use Seldat\Wms2\Utils\SelStr;
/**
 * Created by PhpStorm.
 * User: duong.tran
 * Date: 1/25/2018
 * Time: 10:39 AM
 */
class CustomerSacModel extends AbstractModel
{
    /**
     * Constructor.
     */

    public function __construct()
    {
        $this->model = new CustomerSac();
    }

    public function search($input, $with = [], $limit = 10) {
        $query = $this->make($with);
        if (!empty($input['cus_id'])) {
            $query->where('cus_id', $input['cus_id']);
        }

        if (!empty($input['whs_id'])) {
            $query->where('whs_id', $input['whs_id']);
        }

        if (!empty($input['type'])) {
            $query->where('type', $input['type']);
        }

        if (isset($input['charge_code']) && $input['charge_code'] != '') {
            $query->where(function($q) use ($input) {
                $q->where('sac_name', 'like', "%" . SelStr::escapeLike($input['charge_code']) . "%")
                    ->orWhere('sac', 'LIKE', "%" . SelStr::escapeLike($input['charge_code']) . "%");
            });
        }

        if (array_has($input, 'sort')) {
            if (array_has($input['sort'], 'type_name')) {
                $input['sort']['type'] = $input['sort']['type_name'];
                unset($input['sort']['type_name']);
            }

            if (array_has($input['sort'], 'unit_uom_name')) {
                $input['sort']['unit_uom'] = $input['sort']['unit_uom_name'];
                unset($input['sort']['unit_uom_name']);
            }

            if (array_has($input['sort'], 'range_uom_name')) {
                $input['sort']['range_uom'] = $input['sort']['range_uom_name'];
                unset($input['sort']['range_uom_name']);
            }
        }

        $this->sortBuilder($query, $input);
        return $query->paginate($limit);
    }

    public function createMulti(array $data)
    {
        foreach ($data as $item) {
            $this->model->create($item);
        }
    }

    public function updateMulti(array $data)
    {
        foreach ($data as $item) {
            $this->model->where('cus_sac_id', $item['cus_sac_id'])->update([
                'price' => $item['price'],
                'discount' => $item['discount']
            ]);
        }
    }
}

