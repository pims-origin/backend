<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21/11/2016
 * Time: 13:59
 */

namespace App\Api\Inventory\Models;

use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CycleDis;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\Invt;
use Seldat\Wms2\Models\Pallet;
use Wms2\UserInfo\Data;

class CycleProcess extends AbstractModel
{
    public $errors = [];
    protected $invSumModel;
    protected $inventory;

    public function __construct($model = null)
    {
        $this->model = $model ? $model : new CycleHdr();
        $this->invSumModel = new InventorySummary();
        $this->inventory = new Invt();
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function recount($data)
    {
        $model = new CycleDtlModel();
        $validate = $model->validateCycleDtlApprove($data);

        if (!$validate) {
            $this->errors = $model->errors;

            return false;
        }

        $result = $model->processRecountCycle($data);
        if (!$result) {
            $this->errors = $model->errors;
        }

        return $result;

    }

    public function approve($data)
    {

    }

    /**
     * @param $cycleHdrId
     * @param $cycleDtlIds
     *
     * @return mixed
     */
    public function getCycleDtlDataProcess($cycleHdrId, $cycleDtlIds)
    {
        if (!($cycleHdrId && $cycleDtlIds)) {
            return [];
        }

        $data = CycleDtl::select(
            'cycle_dtl.cycle_dtl_sts',
            'cycle_discrepancy.dicpy_qty',
            'cycle_discrepancy.ctn_id',
            'cycle_dtl.item_id',
            'cycle_dtl.lot',
            'cycle_dtl.whs_id',
            'cycle_dtl.remain',
            'cycle_discrepancy.dicpy_ctn_id',
            'cycle_discrepancy.dicpy_sts',
            'cycle_dtl.sys_loc_id',
            'cycle_dtl.act_loc_id',
            'cycle_dtl.act_loc_name',
            'cycle_hdr.cycle_num',
            'cycle_dtl.cycle_hdr_id',
            'cycle_dtl.cycle_dtl_id',
            'cycle_dtl.act_qty',
            'cycle_dtl.sys_qty'
        )
            ->join('cycle_hdr', 'cycle_hdr.cycle_hdr_id', '=', 'cycle_dtl.cycle_hdr_id')
            ->leftjoin('cycle_discrepancy', function ($join) {
                $join->on('cycle_discrepancy.cycle_dtl_id', '=', 'cycle_dtl.cycle_dtl_id')
                    ->whereNotIn('cycle_discrepancy.dicpy_sts', [
                        CycleDisModel::STATUS_DIS_RECOUNT,
                        CycleDisModel::STATUS_DIS_ADJUSTED
                    ]);
            })
            ->where('cycle_dtl.cycle_hdr_id', $cycleHdrId)
            ->where('cycle_dtl.cycle_dtl_sts', CycleDtlModel::STATUS_CYCLEDTL_OPEN)
            ->whereIn('cycle_dtl.cycle_dtl_id', $cycleDtlIds)
            ->get()
            ->toArray();

        return $data;
    }

    /**
     * @param $cycleHdrId
     * @param $cycleDtlIds
     *
     * @return bool
     */
    public function updateActLocForCycleDtl($cycleHdrId, $cycleDtlIds)
    {
        if (!($cycleHdrId && $cycleDtlIds)) {
            return false;
        }

        $result = DB::update("UPDATE cartons INNER JOIN pallet  ON (cartons.plt_id= pallet.plt_id), 
                (
                    SELECT  item_id, 
                            remain, 
                            sys_loc_id, 
                            act_loc_id, 
                            loc.loc_code, 
                            loc.loc_alternative_name AS loc_name, 
                            pack
                    FROM cycle_dtl 
                    LEFT JOIN location loc ON loc.loc_id = cycle_dtl.act_loc_id
                    WHERE sys_loc_id <> act_loc_id 
                        AND cycle_hdr_id = ? 
                        AND cycle_dtl_id IN (" . implode($cycleDtlIds, ',') . ")
                        AND cycle_dtl_sts = ?
                ) cd
                SET     cartons.loc_id = cd.act_loc_id, 
                        cartons.loc_code = cd.loc_code, 
                        cartons.loc_name = cd.loc_name,
                        pallet.loc_id = cd.act_loc_id, 
                        pallet.loc_code = cd.loc_code, 
                        pallet.loc_name = cd.loc_name
                WHERE cartons.item_id = cd.item_id 
                    AND cartons.ctn_pack_size = cd.pack
                    AND cartons.piece_remain = cd.remain 
                    AND cartons.loc_id = cd.sys_loc_id", [
            $cycleHdrId,
            CycleDtlModel::STATUS_CYCLEDTL_OPEN
        ]);

        return $result;
    }

    /**
     * @param $discreArr
     * @param $oddCartonIds
     * @param $alignCntIds
     */
    private function filterDelCartons($isEach, $discreArr, &$oddCartonIds, &$alignCntIds)
    {
        if ($isEach) {
            foreach ($discreArr as $ctn) {
                if ($ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_DELETE) {
                    $oddCartonIds[] = $ctn['ctn_id'];
                } elseif ($ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_ALIGN) {
                    $alignCntIds[] = $ctn;
                }
            }
        } else {
            $oddCartonIds = array_column($discreArr, 'ctn_id');
        }
    }

    /**
     * @param $oddCartons
     * @param $userId
     * @param $isEach
     */
    public function delCtnDiscrepancy($oddCartons, $userId, $isEach)
    {
        $oddCartonIds = $alignCntIds = [];
        $this->filterDelCartons($isEach, $oddCartons, $oddCartonIds, $alignCntIds);

        if ($oddCartonIds) {
            Carton::whereIn('ctn_id', $oddCartonIds)
                ->update([
                    'ctn_sts'          => LockModel::STATUS_ADJUSTED,
                    'plt_id'           => null,
                    'loc_id'           => null,
                    'loc_code'         => null,
                    'loc_name'         => null,
                    'loc_type_code'    => null,
                    'picked_dt'        => time(),
                    'storage_duration' => CartonModel::getCalculateStorageDurationRaw('gr_dt'),
                    'updated_by'       => $userId,
                    'updated_at'       => time()
                ]);


        }
        $locIds = [];

        if ($alignCntIds) {
            $created = time();
            foreach ($alignCntIds as $alCtn) {
                $locIds[] = $alCtn['act_loc_id'];

                $status = DB::update('UPDATE  cartons 
                            SET     `updated_by` = ?,
                                    `updated_at`= ?,
                                    `piece_remain` = `piece_remain` + (?),
                                    `loc_id` = ?,
                                    `loc_code` = ?,
                                    `loc_name` = ?
                            WHERE   `ctn_id` = ?',
                    [
                        $userId,
                        $created,
                        $alCtn['dicpy_qty'],
                        $alCtn['act_loc_id'],
                        $alCtn['act_loc_name'],
                        $alCtn['act_loc_name'],
                        $alCtn['ctn_id']
                    ]);
            }
        }
        $locIds = array_unique($locIds);

        return $locIds;
    }

    /**
     * @param      $lackCtn
     * @param      $user_id
     * @param bool $isEach
     */
    public function cloneCtnDiscrepancy($lackCtn, $user_id, $isEach = false)
    {
        $cartonModel = new CartonModel();

        $cartonIds = array_column($lackCtn, 'ctn_id');
        $cartons = $cartonModel->getCartons($cartonIds);

        foreach ($lackCtn as $ctn) {

            $ctnArr = $this->getCartonIdInArr($cartons, $ctn['ctn_id']);

            $maxCtnNum = $cartonModel->generateCartonNum($ctn['cycle_num']);

            if ($ctnArr) {
                unset($ctnArr['ctn_id']);

                //discrepancy process
                $disObj = CycleDisModel::getCycleDtl($ctn['dicpy_ctn_id']);
                $disArr = $disObj->toArray();
                $pallet = CycleDisModel::getPalletByLocID($disObj->cycleDtl->act_loc_id);
                if (!$pallet) {
                    $pallet = $this->createPallet($disObj->cycleDtl, $ctn);
                }

                $disArr['dicpy_qty'] = 1;
                $disArr['dicpy_sts'] = CycleDisModel::STATUS_DIS_ADJUSTED;
                unset($disArr['dicpy_ctn_id']);
                unset($disArr['cycle_dtl']);

                for ($i = 1; $i <= $ctn['dicpy_qty']; $i++) {
                    //$max++;
                    $maxCtnNum++;
                    $objCarton = new Carton();
                    $this->setAttributeByArr($ctnArr, $objCarton);
                    //carton clone
                    $objCarton->ctn_num = $maxCtnNum;

                    $objCarton->loc_id = $disObj->cycleDtl->act_loc_id;
                    $objCarton->loc_code = $disObj->cycleDtl->act_loc_name;
                    $objCarton->loc_name = $disObj->cycleDtl->act_loc_name;
                    //ctn process
                    $objCarton->updated_at = $disArr['updated_at'] = time();
                    $objCarton->updated_by = $disArr['updated_by'] = $user_id;
                    $objCarton->gr_dt = time();
                    $objCarton->ctn_sts = LockModel::STATUS_LOCK;
                    $objCarton->is_damaged = null;
                    $objCarton->piece_remain = $objCarton->piece_ttl = $disObj->cycleDtl->remain;
                    $objCarton->ctn_pack_size = $disObj->cycleDtl->pack;
                    $objCarton->is_ecom = 0;
                    $objCarton->plt_id = $pallet->plt_id;
                    $objCarton->gr_hdr_id = null;
                    $objCarton->asn_dtl_id = null;
                    $objCarton->is_damaged = 0;
                    $objCarton->gr_dtl_id = null;
                    $objCarton->return_id = null;
                    $objCarton->po = null;
                    $objCarton->ctnr_num = null;
                    $objCarton->ctnr_id = null;
                    $objCarton->rfid = null;

                    //process for count_by = each
                    if ($isEach === true) {
                        if ($ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_CLONE) {
                            $objCarton->piece_remain = $objCarton->piece_ttl = $disObj->cycleDtl->pack;
                        } elseif ($ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_ALIGN) {
                            $objCarton->piece_remain = $objCarton->piece_ttl = $ctn['dicpy_qty'];
                        }
                    }

                    //discrepacy clone
                    if (!$objCarton->save()) {
                        continue;
                    }

                    $disArr['ctn_id'] = $objCarton->ctn_id;
                    CycleDis::insert([$disArr]);

                    if ($isEach && $ctn['dicpy_sts'] === CycleDisModel::STATUS_DIS_ALIGN) {
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param $arrCartons
     * @param $ctnId
     *
     * @return array
     */
    private function getCartonIdInArr($arrCartons, $ctnId)
    {
        foreach ($arrCartons as $carton) {
            if ($carton['ctn_id'] == $ctnId) {
                return $carton;
            }
        }

        return [];
    }


    /**
     * Get unclock itemIds
     *
     * @param $itemIds
     *
     * @return array. List valid itemIds to unclock.
     */
    public function getUnclockItemIds($cycleHdrId)
    {
        $result = [];

        if (!($cycleHdrId)) {
            return $result;
        }

        $data = CycleDtl::select('item_id')
            ->where('cycle_hdr_id', $cycleHdrId)
            //->whereNotIn('item_id', function ($query) use ($cycleHdrId) {
            //    $query->select('item_id')
            //        ->from('cycle_dtl')
            //        ->where('cycle_hdr_id', $cycleHdrId)
            //        ->where('cycle_dtl_sts', '<>', CycleDtlModel::STATUS_CYCLEDTL_ACCEPTED);
            //})
            ->distinct()->pluck('item_id')->toArray();

        return $data;

        if (!$data) {
            return $itemIds;
        }

        return $result;
    }

    /**
     * @param $arr
     * @param $model
     */
    private function setAttributeByArr($arr, &$model)
    {
        foreach ($arr as $key => $val) {
            $model->$key = $val;
        }
    }

    /**
     * @param $whsId
     * @param $itemId
     * @param $lot
     * @param $discrepancy
     *
     * @return mixed
     */
    public function updateInventory($whsId, $itemId, $lot, $discrepancy)
    {
        $operator = '+';

        if ($discrepancy < 0) {
            $operator = '-';
        }

        //convert to positive number
        $discrepancy = abs($discrepancy);
        $invItem = $this->invSumModel->where(['whs_id' => $whsId, 'item_id' => $itemId, 'lot' => $lot])->first();

        /*
         * 1. If $discrepancy > avail qty,
         * 1.1 avail = 0
         * 1.2 allocated_qty = allocated_qty - ($discrepancy - avail qty)
         * 2. update avail = $discrepancy
         * $discrepancy = 10
         * avail = 5, 0
         * allocated_qty = 15, 10
         */
        $availQty = $discrepancy;
        if ($invItem->avail < $discrepancy && $operator == '-') {
            $allocQty = $invItem->allocated_qty - ($discrepancy - $invItem->avail);
            $availQty = $invItem->avail;
            $invItem->allocated_qty = $allocQty;
        }


        $availQtySql = sprintf('`avail` %s %d', $operator, $availQty);
        $ttlQty = sprintf('`ttl` %s %d', $operator, $discrepancy);

        $res = $this->invSumModel->where(['whs_id' => $whsId, 'item_id' => $itemId, 'lot' => $lot])
            ->update([
                'avail'         => DB::raw($availQtySql),
                'ttl'           => DB::raw($ttlQty),
                'allocated_qty' => $invItem->allocated_qty
            ]);
        $dataInv = $this->invSumModel->where(['whs_id' => $whsId, 'item_id' => $itemId, 'lot' => $lot])->first();
        $this->inventory->where(['whs_id' => $whsId, 'item_id' => $itemId])
            ->update([
                'in_hand_qty'         => array_get($dataInv, 'ttl', 0)
            ]);

        return $res;
    }

    public function generatePLNNum($CycleNum)
    {
        $key = sprintf("%s", $CycleNum) . '%';
        $num = Pallet::select('plt_num')
            ->where('plt_num', 'LIKE', $key)
            ->orderBy('plt_num', 'DESC')
            ->value('plt_num');
        if (empty($num)) {
            $num = $CycleNum . "-000";
        }

        return sprintf('LPN-%s', $num);
    }

    public function createPallet($cycleDtl, $ccHdr)
    {
        $userId = Data::getCurrentUserId();
        $maxPltNum = $this->generatePLNNum($ccHdr['cycle_num']);
        $maxPltNum++;
        $ctnTtl = $ccHdr['dicpy_qty'];

        return Pallet::create([
            'loc_id'     => $cycleDtl->act_loc_id,
            'loc_name'   => $cycleDtl->act_loc_name,
            'loc_code'   => $cycleDtl->act_loc_name,
            'whs_id'     => $cycleDtl->whs_id,
            'cus_id'     => $cycleDtl->cus_id,
            'plt_num'    => $maxPltNum,
            'created_at' => time(),
            'updated_at' => time(),
            'created_by' => $userId,
            'updated_by' => $userId,
            'ctn_ttl'    => $ctnTtl
        ]);


    }
}