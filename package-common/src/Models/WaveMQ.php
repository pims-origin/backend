<?php

namespace Seldat\Wms2\Models;

class WaveMQ extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_mq';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_mq_id';

    protected $fillable = [
        'wv_id',
        'wv_dtl_id',
        'wv_num',
        'item_id',
        'wv_mq_sts'
    ];

}
