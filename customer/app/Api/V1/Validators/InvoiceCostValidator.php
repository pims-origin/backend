<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:00
 */

namespace App\Api\V1\Validators;

class CustomerChargeDetailValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id'                => 'required',
            'chg_code_id'           => 'required',
            'cus_charge_dtl_rate'   => 'required',
            'cus_charge_dtl_des'    => 'array',
            'cus_id.*'              => 'required',
            'whs_id.*'              => 'required',
            'chg_code_id.*'         => 'required',
            'cus_charge_dtl_rate.*' => 'required',
        ];
    }
}
