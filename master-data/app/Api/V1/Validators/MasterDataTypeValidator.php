<?php

namespace App\Api\V1\Validators;

class MasterDataTypeValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'md_type'  => 'required',
            'mdt_name' => 'required',
            'mdt_des'  => 'required',
            'md_sts'   => 'required',
        ];
    }
}
