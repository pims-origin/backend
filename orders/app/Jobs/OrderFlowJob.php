<?php

namespace App\Jobs;


use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Models\OrderHdrModel;
use DB;
use GuzzleHttp\Client;

class OrderFlowJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $orderId;
    protected $dependency;
    protected $cusId;
    protected $request;
    protected $customerMetaModel;
    protected $hdrMetaModel;

    public function __construct(
        $orderId,
        $dependency,
        $cusId,
        $request,
        CustomerMetaModel $customerMetaModel,
        OrderHdrMetaModel $hdrMetaModel = null
    ) {
        //
        $this->orderId = $orderId;
        $this->dependency = $dependency;
        $this->cusId = $cusId;
        $this->request = $request;
        $this->customerMetaModel = $customerMetaModel;
        $this->hdrMetaModel = $hdrMetaModel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        if (is_null($this->hdrMetaModel)) {
            $cus_meta = $this->customerMetaModel->getOrderFlow($this->cusId);
        } else // get customer meta
        {
            $cus_meta = $this->hdrMetaModel->getOrderFlow($this->orderId);
        }

        $orderHdrModel = new OrderHdrModel();
        $csrFlow = $this->customerMetaModel->getFlow($cus_meta, 'CSR');
        $alcFlow = $this->customerMetaModel->getFlow($cus_meta, 'ALC');
        //$NWPFlow = $this->customerMetaModel->getFlow($cus_meta, 'NWP');
        $client = new Client();

        try {

            if ($this->dependency==1) {
                if (array_get($csrFlow, 'usage', -1) == 1) {
                    $odr = $orderHdrModel->getFirstWhere(['odr_id' => $this->orderId]);
                    $csr_default = $this->customerMetaModel->getPNP($this->cusId, 'CSR', object_get($odr, 'whs_id', 0));
                    if ($csr_default != 0) {
                        $odrIds[] = $this->orderId;
                        $client->request('PUT', env('API_ORDER') . 'assign-csr',
                            [
                                'headers'     => ['Authorization' => $this->request->getHeader('Authorization')[0]],
                                'form_params' => ["user_id" => $csr_default, "odr_id" => $odrIds]

                            ]
                        );
                    }
                }
            }

            if ($this->dependency==2) {
                if (array_get($alcFlow, 'usage', -1) == 1) {
                    $arrOrder[] = $this->orderId;
                    $ordDtl = new OrderDtlModel();
                    $listDtl = $ordDtl->loadByOrdIds([$this->orderId]);
                    $param = [];
                    foreach ($listDtl as $dtl) {
                        $dt = $dtl->toArray();
                        $dt['itm_id'] = $dt['item_id'];
                        $param[] = $dt;
                    }

                    $client->request('PUT', env('API_ORDER') . 'allocates/' . $this->orderId,
                        [
                            'headers'     => ['Authorization' => $this->request->getHeader('Authorization')],
                            'form_params' => $param
                        ]
                    );
                }


            }
        } catch (\Exception $exception) {
            // DB::table('sys_bugs')->where('id', 3)->update(['error' => $exception->getMessage()]);
            $dataError = [
                'date'     => time(),
                'api_name' => "Order Flow",
                'error'    => $exception->getMessage(),
            ];

            DB::table('sys_bugs')->insert($dataError);
        }

    }
}
