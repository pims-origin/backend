<?php

namespace App\Api\V2\WavePick\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WaveDtlLoc;
use Wms2\UserInfo\Data;

class WavePickDtlLocModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WaveDtlLoc $model = null)
    {
        $this->model = ($model) ?: new WaveDtlLoc();
    }

    /**
     * @param $wvId
     *
     * @return mixed
     */
    public function getSugLocIdByWvId($wvId)
    {
        return $this->model
            ->where('wv_id', $wvId)
            ->select('sug_loc_ids')
            ->get();
    }

    public function getWavePickDetailLocation($wvId, $with = [])
    {
        return $this->model
            ->when((count($with) > 0), function($q) use($with){
                return $q->with($with);
            })
            ->where('wv_id', $wvId)
            ->get();
    }


}
