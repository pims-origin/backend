<?php

namespace backend\library;

use yii\db\Exception;
use yii\redis\Cache as redisCache;
use yii\redis\Connection;

class MyRedisCache extends redisCache
{
    public function buildKey($key)
    {
        if (!is_string($key)) {
            $key = md5(json_encode($key));
        }

        return $this->keyPrefix . $key;
    }

    public function multipleDelete($keys)
    {
        return (bool)$this->redis->executeCommand('DEL', $keys);
    }
}