<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use \Seldat\Wms2\Models\WorkOrderHdr;
use \Seldat\Wms2\Models\WorkOrderDtl;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class WorkOrderModel extends AbstractModel
{
    /**
     * @param WorkOrder $model
     */
    public function __construct(WorkOrderHdr $model = null)
    {
        $this->model = ($model) ?: new WorkOrderHdr();
    }

    /**
     * Generate new wo number
     *
     * @return string
     */
    public function generateWoNum($odrNum)
    {
        if ($odrNum) {
            return 'WORK-' . $odrNum;
        }

        $currentYearMonth = date('ym');
        $defaultWoNum = "WORK-${currentYearMonth}-00001";

        $lastWoNum = DB::table('settings')
            ->where('setting_key', 'last_wo_num')
            ->value('setting_value');

        if (empty($lastWoNum) || strpos($lastWoNum, "-${currentYearMonth}-") === false) {
            return $defaultWoNum;
        }

        return ++$lastWoNum;
    }

    /**
     * upsert wo number
     *
     * @param $woNum
     *
     * @return mixed
     */
    public function updateWoNum($woNum)
    {
        if (strlen($woNum) > 15) {
            return false;
        }

        $db = DB::connection()->getPdo();
        $query = "REPLACE INTO settings(setting_key, setting_value) VALUES('last_wo_num', '$woNum')";
        $statement = $db->prepare($query);

        return $statement->execute();
    }

    /**
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function search($attributes, array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        // search according to Work Order Number
        if (isset($attributes['wo_hdr_num'])) {
            $query->where('wo_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['wo_hdr_num']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }

        // search according to Order  Number
        if (isset($attributes['odr_hdr_num'])) {
            $query->where('odr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['odr_hdr_num']) . "%");
        }

        // search according to Work Order Status
        if (isset($attributes['wo_sts'])) {
            $query->where('wo_sts', 'like', "%" . SelStr::escapeLike($attributes['wo_sts']) . "%");
        }

        // search according to Pack By
        if (isset($attributes['type'])) {
            $query->where('type', $attributes['type']);
        }

        $this->model->filterData($query);
        $query->groupBy('wo_hdr_id');
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getAllWorkOrderOfOriginAndBackOrder($odrNum)
    {
        $backOrderList = $this->model
            ->where('odr_hdr_num', 'like', substr($odrNum, 0, 14) . "%")
            ->get();

        return $backOrderList;
    }

}
