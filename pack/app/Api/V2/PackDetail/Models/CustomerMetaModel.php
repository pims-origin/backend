<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Nov-16
 * Time: 12:01
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\CustomerMeta;

class CustomerMetaModel extends AbstractModel
{
    /**
     * CustomerMetaModel constructor.
     *
     * @param CustomerMeta|null $model
     */
    public function __construct(CustomerMeta $model = null)
    {
        $this->model = ($model) ?: new CustomerMeta();
    }
    public function getFlow($configs, $flow) {
        foreach ($configs as $config)
        {
            if(array_get($config, 'flow_code') == $flow) {
                return $config;
            }
        }
    }

    public function getDefaultCsr($cusId){
        $csr = false;
        $ret = $this->model
            ->where('cus_id', $cusId)
            ->where('qualifier', 'CSR')
            ->first();
        if (!empty($ret->value)) {
            $arrUser = json_decode($ret->value);
            foreach ($arrUser as $user) {
                $objUser = json_decode($user);
                if ($objUser->set_default == 1) {
                    $csr = $objUser->user_id;
                }
            }
        }
        return $csr;
    }

    public function getPaymentAccountNumber($cusId, $courier_name = 'FDX'){
        $arrPayment = [];
        $ret = $this->model
            ->where('cus_id', $cusId)
            ->where('qualifier', 'ACN')
            ->first();
        
        if (!empty($ret->value)) {
            $arrService = json_decode($ret->value);
            foreach ($arrService as $service) {
                $objService = json_decode($service);
                if ($objService->courier_name == $courier_name) {
                    $arrPayment = array(
                        'PaymentType' => object_get($objService, 'pay_type', ''), // valid values RECIPIENT, SENDER and THIRD_PARTY
                        'Payor' => array(
                            'ResponsibleParty' => array(
                                'AccountNumber' => object_get($objService, 'acc_num', ''),
                                'Contact' => null,
                                'Address' => array(
                                    'CountryCode' => 'US'
                                )
                            )
                        )
                    );
                }
            }
        }

        return $arrPayment;
    }
}
