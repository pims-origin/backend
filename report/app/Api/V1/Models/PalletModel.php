<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-June-2017
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class PalletModel extends AbstractModel
{
    const DAS_LIMIT = 2;
    //const ROLE = ['admin', 'manager'];

    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
    }

    /**
     * @param $whs_id
     * @param array $attributes
     *
     * @return mixed
     */
    public function customers($whs_id, $attributes = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        // Search whs_id
        $query = DB::table('customer AS c')
            ->select(['c.cus_id', 'c.cus_name'])
            ->join('pallet', 'c.cus_id', '=', 'pallet.cus_id')
            ->where('pallet.whs_id', $whs_id);

        $result = $query->groupBy('c.cus_id')->orderBy('c.cus_name')->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function search($whs_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        // DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->select([
                'customer.cus_id',
                'customer.cus_name',
                'customer.cus_code',
                'pallet.plt_id',
                'pallet.plt_num',
                'pallet.loc_id',
                'pallet.loc_name',
                'pallet.loc_code',
                'pallet.init_ctn_ttl',
                'pallet.init_piece_ttl',
                'pallet.created_at',
                'pallet.plt_sts',
                'pallet.rfid',
                'cartons.item_id',
                'cartons.sku',
                'cartons.size',
                'cartons.color',
                'cartons.ctn_pack_size',
                'cartons.lot',
                'cartons.upc',
                'cartons.size',
                'cartons.deleted',
                'location.loc_sts_code',
                DB::raw('SUM(IF(cartons.is_damaged = 1, 1, 0)) AS dmg_ctn_ttl'),
                DB::raw('COUNT(*) AS current_ctn_ttl'),
                DB::raw('SUM(piece_remain) AS current_piece_ttl'),
                'item.description as des',
                'location.row',
                'location.level',
                'location.aisle'
            ]);
        $query->join('customer', 'customer.cus_id', '=', 'pallet.cus_id');
        $query->join('cartons', 'cartons.plt_id', '=', 'pallet.plt_id');
        $query->join('item', 'cartons.item_id', '=', 'item.item_id');
        $query->leftJoin('location', 'location.loc_id', '=', 'pallet.loc_id');

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query->where('pallet.whs_id', $whs_id);
        $query->where('pallet.ctn_ttl', '>', 0);
        $query->where('pallet.plt_sts', '!=', 'AJ');

        //Check on Rack or not
        if (isset($attributes['on_rack']) && ($attributes['on_rack']) == 1) {
            $query->whereNotNull('pallet.loc_id');
        } else if (isset($attributes['on_rack']) && ($attributes['on_rack']) == 0) {
            $query->whereNull('pallet.loc_id');
        }

        if (isset($attributes['loc_code'])) {
            $query->where('pallet.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        if (isset($attributes['rfid'])) {
            $query->where('pallet.rfid', 'like', "%" . SelStr::escapeLike($attributes['rfid']) . "%");
        }

        //Search Location
        if (isset($attributes['row'])){
            $query->where('location.row', 'LIKE', SelStr::escapeLike($attributes['row']) . "%");
        }

        if (isset($attributes['level'])){
            $query->where('location.level', 'LIKE', SelStr::escapeLike($attributes['level']) . "%");
        }

        if (isset($attributes['aisle'])){
            $query->where('location.aisle', 'LIKE', SelStr::escapeLike($attributes['aisle']) . "%");
        }

        // Search Customer
        if (isset($attributes['cus_id'])) {
            $query->where('pallet.cus_id', $attributes['cus_id']);
        }

        $query->where('cartons.deleted', 0);
        $query->whereIn('cartons.ctn_sts', ['AC', 'LK', 'RG']);
        if (isset($attributes['sku'])) {
            $query->where('cartons.sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }
        //search more fields from Sku auto complete
        if (isset($attributes['item_id'])) {
            $query->where('cartons.item_id', $attributes['item_id']);
        }

        if (isset($attributes['lot'])) {
            $query->where('cartons.lot', $attributes['lot']);
        }

        //search according to from date to date
        if (!empty($attributes['created_at_from'])) {
            $query->where('pallet.created_at', ">=", strtotime($attributes['created_at_from']));
        }

        if (!empty($attributes['created_at_to'])) {
            $query->where('pallet.created_at', "<", strtotime($attributes['created_at_to'] . "+1 day"));
        }

        $query->groupBy('pallet.plt_id', 'cartons.item_id', 'cartons.lot', 'cartons.ctn_pack_size');

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'cus_name') {
                    $query->orderBy("customer.cus_name", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'cus_code') {
                    $query->orderBy("customer.cus_code", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'rfid') {
                    $query->orderBy("pallet.rfid", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'plt_sts_name') {
                    $query->orderBy("pallet.plt_sts", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'loc_code') {
                    $query->orderBy("pallet.loc_code", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'loc_sts_name') {
                    $query->orderBy("location.loc_sts_code", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'sku') {
                    $query->orderBy("cartons.sku", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'size') {
                    $query->orderBy("cartons.size", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'color') {
                    $query->orderBy("cartons.color", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ctn_pack_size') {
                    $query->orderBy("cartons.ctn_pack_size", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'lot') {
                    $query->orderBy("cartons.lot", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'upc') {
                    $query->orderBy("cartons.upc", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'init_ctn_ttl') {
                    $query->orderBy("pallet.init_ctn_ttl", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'created_at') {
                    $query->orderBy("pallet.created_at", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'des') {
                    $query->orderBy("item.description", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'dmg_ctns') {
                    $query->orderBy("dmg_ctn_ttl", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'current_ctns') {
                    $query->orderBy("current_ctn_ttl", $val);
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'current_pieces') {
                    $query->orderBy("current_piece_ttl", $val);
                    unset($attributes['sort'][$key]);
                }
            }
        }
        // Get
        $this->sortBuilder($query, $attributes);

        $query->orderBy("pallet.created_at", "desc");

        if ($export) {
            $models = $query->get();
        } else {
            $models = $query->paginate($limit);
        }

        return $models;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function skuPalletAutoComplete($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('cartons')
            ->where('cartons.whs_id', $whs_id);


        $query->select(['cartons.sku', 'cartons.size', 'cartons.color', 'cartons.lot', 'cartons.item_id']);

        // Search sku
        if (isset($attributes['sku'])) {
            $query->where('cartons.sku', 'LIKE', "{$attributes['sku']}%");
        }

        $result = $query->groupBy('cartons.item_id', 'cartons.lot')
            ->orderBy('cartons.sku')->take($limit)->get();

        return $result;
    }


}