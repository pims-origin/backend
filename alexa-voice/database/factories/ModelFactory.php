<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\Status;

$factory->define(Seldat\Wms2\Models\User::class, function ($faker) {
    return [
        'first_name' => $faker->name,
        'last_name'  => $faker->name,
        'email'      => $faker->email,
        'password'   => $faker->password,
        'status'     => "AC",
    ];
});

$factory->define(Seldat\Wms2\Models\Customer::class, function ($faker) {
    return [
        'cus_name'            => $faker->name,
        'cus_code'            => $faker->name,
        'cus_status'          => DB::table('cus_status')->select('cus_sts_code')->first()['cus_sts_code'],
        'cus_billing_account' => $faker->randomLetter,
    ];
});

$factory->define(Seldat\Wms2\Models\Warehouse::class, function ($faker) {
    return [
        'whs_name'       => $faker->name,
        'whs_code'       => $faker->name,
        'whs_status'     => DB::table('whs_status')->select('whs_sts_code')->first()['whs_sts_code'],
        'whs_country_id' => DB::table('system_country')->select('sys_country_id')->first()['sys_country_id'],
        'whs_state_id'   => DB::table('system_state')->select('sys_state_id')->first()['sys_state_id'],
        'whs_short_name' => $faker->randomLetter,
    ];
});

$factory->define(Seldat\Wms2\Models\SystemUom::class, function ($faker) {
    return [
        'sys_uom_code' => $faker->randomLetter . $faker->randomLetter,
        'sys_uom_name' => $faker->name,
    ];
});

$factory->define(Seldat\Wms2\Models\Item::class, function ($faker) {
    return [
        'sku'    => $faker->randomLetter,
        'size'   => $faker->randomNumber,
        'color'  => $faker->randomNumber,
        'uom_id' => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()['sys_uom_id'];
        },
        'cus_id' => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()['cus_id'];
        },
        'width'  => 10,
        'pack'   => 10,
        'length' => 10,
        'height' => 20,
        'weight' => 30,
        'status' => DB::table('item_status')->select('item_sts_code')->first()['item_sts_code']
    ];
});

$factory->define(Seldat\Wms2\Models\ShippingOrder::class, function ($faker) {
    return [
        'whs_id'   => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'cus_id'   => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'ref_num'  => $faker->randomLetter,
        'po_total' => $faker->randomNumber,
        'so_sts'   => Status::getByValue("New", "Order-status"),
        'type'     => Status::getByValue("New", "Order-type"),
    ];
});

$factory->define(Seldat\Wms2\Models\OrderHdr::class, function ($faker) {
    $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();

    return [
        'ship_id'         => null,
        'whs_id'          => $shippingOdr->whs_id,
        'cus_id'          => $shippingOdr->cus_id,
        'so_id'           => $shippingOdr->so_id,
        'csr'             => null,
        'odr_sts'         => $shippingOdr->so_sts,
        'odr_type'        => $shippingOdr->type,
        'ref_cod'         => $faker->postcode,
        'odr_num'         => str_random(10),
        'cus_odr_num'     => str_random(10),
        'cus_po'          => $faker->randomLetter,
        'ship_to_name'    => str_random(10),
        'ship_to_add_1'   => $faker->streetAddress,
        'ship_to_city'    => $faker->city,
        'ship_to_state'   => $faker->state,
        'ship_to_zip'     => $faker->postcode,
        'ship_to_country' => $faker->country,
        'carrier'         => $faker->jobTitle,
        'odr_req_dt'      => $faker->unixTime,
        'req_cmpl_dt'     => $faker->unixTime,
        'act_cmpl_dt'     => $faker->unixTime,
        'ship_by_dt'      => $faker->unixTime,
        'cancel_by_dt'    => $faker->unixTime,
        'act_cancel_dt'   => $faker->unixTime,
        'ship_after_dt'   => $faker->unixTime,
        'shipped_dt'      => $faker->unixTime,
        'cus_pick_num'    => str_random(10),
        'ship_method'     => $faker->randomLetter,
        'cus_dept_ref'    => $faker->randomLetter,
        'rush_odr'        => (int)$faker->boolean,
        'cus_notes'       => str_random(10),
        'in_notes'        => str_random(10),
        'hold_sts'        => (int)$faker->boolean,
        'back_odr'        => (int)$faker->boolean,
        'back_odr_seq'    => $faker->randomNumber,
        'org_odr_id'      => $faker->randomNumber,
        'sku_ttl'         => $faker->randomNumber,
        'wv_num'          => str_random(10),
    ];
});

$factory->define(Seldat\Wms2\Models\OrderDtl::class, function ($faker) {
    return [
        'odr_id'       => function () {
            return factory(Seldat\Wms2\Models\OrderHdr::class)->create()['ord_id'];
        },
        'whs_id'       => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()['whs_id'];
        },
        'cus_id'       => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()['cus_id'];
        },
        'item_id'       => function () {
            return factory(Seldat\Wms2\Models\Item::class)->create()['item_id'];
        },
        'uom_id'       => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()['sys_uom_id'];
        },
        'color'        => uniqid() . $faker->randomLetter . $faker->randomLetter,
        'sku'          => $faker->randomLetter . uniqid() . $faker->randomLetter,
        'size'         => $faker->randomLetter . $faker->randomLetter . uniqid(),
        'lot'          => $faker->randomLetter . uniqid() . uniqid(),
        'qty'          => random_int(1, 100),
        'piece_qty'    => random_int(1, 100),
        'back_odr_qty' => random_int(1, 100),
        'sts'          => $faker->randomLetter,
        'special_hdl'  => (int)$faker->boolean,
        'back_odr'     => (int)$faker->boolean,
    ];
});

$factory->define(Seldat\Wms2\Models\OrderShippingInfo::class, function ($faker) {
    return [
        'ship_to_cus_name' => $faker->name,
        'ship_to_addr'     => $faker->address,
        'ship_to_city'     => $faker->city,
        'ship_to_state'    => $faker->state,
        'ship_to_zip'      => $faker->state,
        'ship_to_country'  => $faker->country,
        'ship_by_dt'       => time(),
        'ship_dt'          => time(),
    ];
});

$factory->define(Seldat\Wms2\Models\InventorySummary::class, function ($faker) {
    return [
        'itm_id'        => function () {
            return factory(Seldat\Wms2\Models\Item::class)->create()['item_id'];
        },
        'cus_id'        => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()['cus_id'];
        },
        'whs_id'        => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()['whs_id'];
        },
        'color'         => str_random(10),
        'size'          => str_random(10),
        'lot'           => str_random(10),
        'ttl'           => 9999,
        'allocated_qty' => 4,
        'dmg_qty'       => 1,
        'avail'         => 9999,
        'sku'           => str_random(10),
    ];
});




