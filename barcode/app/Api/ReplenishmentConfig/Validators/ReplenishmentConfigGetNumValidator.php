<?php

namespace App\Api\ReplenishmentConfig\Validators;

use App\Api\ReplenishmentConfig\Models\CartonModel;
use App\Api\ReplenishmentConfig\Models\LocationModel;
use App\Api\ReplenishmentConfig\Models\PalletModel;
use Dingo\Api\Exception\ValidationHttpException;
use Dingo\Api\Exception\UnknownVersionException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Seldat\Wms2\Utils\Message;

/**
 * Class SourceMessageValidator
 *
 * @package App\Api\V1\Validators
 */
class ReplenishmentConfigGetNumValidator extends AbstractValidator
{
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @return array
     */

    protected function rules()
    {
        return [
            'loc_code' => 'required',
            'item_id' => 'required'


        ];
    }

    /**
     * @param $fromLocId
     *
     * @return mixed
     * @throws \Exception
     */
    public function check($fromLocId)
    {
        $this->palletModel = new PalletModel;
        $this->locationModel = new LocationModel;
        $this->cartonModel = new CartonModel;

        // Check from Location status if is allocated (AL)
        $fromLocInfo = $this->locationModel->getFirstWhere(['loc_id' => $fromLocId]);
        if (empty($fromLocInfo)) {
            throw new HttpException(400, Message::get("BM017", "Picking Location"));
        }
        if ($fromLocInfo->loc_sts_code != 'AC') {
            throw new \Exception('Status of Picking Location is not Active!');
        }

        // Load loction (Current location id) to get name..
        $locationInfo = $this->cartonModel->getFirstWhere(['loc_id' => $fromLocId], ['location']);

        if (empty($locationInfo->location)) {
            //throw new HttpException(400, Message::get("BM017", "Current Location"));
            throw new HttpException(400, "The Picking Location don't have any pallet");
        }

    }

    /**
     * @param $loc_sts_code
     * @param $msgLocation
     *
     * @throws \Exception
     */
    public function checkPalletStatus($loc_sts_code, $msgLocation)
    {
        switch ($loc_sts_code) {
            case config('constants.INACTIVE'):
                throw new UnknownVersionException($msgLocation . ' is not active.', null, 1);

            case config('constants.LOCKED'):
                throw new UnknownVersionException($msgLocation . ' is being locked.', null, 2);

            case config('constants.RESERVED'):
                throw new UnknownVersionException($msgLocation . ' is being reserved.', null, 3);

            case 'RG':
                throw new UnknownVersionException($msgLocation . ' is being receiving.', null, 3);

            default:
                break;
        }
    }


}
