<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>Location Barcode List pdf</title>
    <style type="text/css">
		@page bigger {
				sheet-size: 6in 4in;
			}

			.container {
           /* margin-bottom: 10px;
            padding-bottom: 10px;*/
            /*border-bottom: 1px dashed #000;*/

            /*padding-left: 40px;
            padding-right: 40px;*/
            width: 6in;
            heigh: 4in;

            text-align: center;

        }


        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 36px;
            font-weight: 100;
            margin: 0;
        }

        h2 {
            color: #282828;
            text-transform: uppercase;
            float: left;
            width: 100%;
            margin: 5px 0;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 14px;
            font-family: "Calibri";

            /*width: 21cm;
            height: 29.7cm;*/
			width: 6in;
            height: 4in;

            margin: 0 auto;
            border: 1px solid #ddd;
            padding: 2px;
            margin: 0 auto;
            text-align: center;

        }

        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #a9a9a9;
            font-size: 19px;
        }

        .table-style tbody tr td b {
            font-size: 20px;
            font-weight: 600;
            color: #000;
            margin-bottom: 50px;
        }

        .table-style tbody tr td {
            line-height: 38px;
        }

        .table-style2 {
            border: 0px solid #a9a9a9;
            margin: 15px 0;
            line-height: 30px;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            text-transform: capitalize;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 0px dashed #d5d5d5;
            border-bottom: 0px solid #c0c0c0;
            color: #595959;
            line-height: 36px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }

        .table-style2 tbody tr:nth-child(2n) td {
            background-color: #ffffff;
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }

        footer {
            border-top: 1px solid #ddd;
            margin-top: 50px;
            padding-top: 10px;
        }

        .barcodeborder {
            background-color: #0F6;
            border: 1px solid #ddd;
            width: 350px;
            float: left;
            text-align: center;
            padding: 15px;
        }

    </style>
</head>
<body>

@foreach($locsArr as $locs)
<div class="container" style="width: 100%; height: 100%; text-align: center;">
<table class="" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <tbody>

    	<!--<tr><td>&nbsp;</td></tr>-->
        <tr>

            <td style="text-align: center; padding: -30px; width: 100%; height: 100%;">
                <!-- <?php
//                $licensePlate = 'P-' . date('ym') . '-' . str_pad($i, 6, "0", STR_PAD_LEFT);
                 ?> -->
                <b style="font-size: 50px">{{$locs}}</b> <br>
                <barcode height="1.8" size="1.3"
                         code="{{$locs}}"
                         type="C128A"/>
                <br>
            </td>

        </tr>
       <!-- <tr>
            <td colspan="1" style="text-align: center; padding: 0px; border-top: 0px dashed #ddd;">&nbsp;
            </td>
        </tr>-->
    </tbody>
</table>
</div>
@endforeach

</body>
</html>
