<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 22-Jul-16
 * Time: 3:05
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\DamageType;

class DamageDamageTypeTransformer extends TransformerAbstract
{
    public function transform($data)
    {
        return [
            'dmg_id'        => $data->dmg_id,
            'dmg_code'      => $data->dmg_code,
            'dmg_name'      => $data->dmg_name,
            'ctnr_num'      => $data->ctnr_num,
            'whs_id'      => $data->whs_id,
            'cus_id'      => $data->cus_id,
        ];
    }
}
