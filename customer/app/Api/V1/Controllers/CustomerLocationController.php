<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerZoneModel;
use App\Api\V1\Models\CustomerLocationModel;
use App\Api\V1\Transformers\CustomerLocationTransformer;
use Illuminate\Database\Eloquent\Collection;
use Psr\Http\Message\ServerRequestInterface as Request;
use Swagger\Annotations as SWG;

class CustomerLocationController extends AbstractController
{
    /**
     * @var $customerLocationModel
     */
    protected $customerLocationModel;

    /**
     * @var $customerZoneModel
     */
    protected $customerZoneModel;

    /**
     * @var customerLocationTransformer
     */
    protected $customerLocationTransformer;

    /**
     * CustomerLocationController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->customerLocationModel = new CustomerLocationModel();
        $this->customerZoneModel = new CustomerZoneModel();
        $this->customerLocationTransformer = new CustomerLocationTransformer();
    }

    /**
     * API List Customer Location By Id
     *
     * @param $customerId
     * @param Request $request
     *
     * @return mixed
     */
    public function search($customerId, Request $request)
    {
        $zoneIds = $this->customerZoneModel->getZoneIdsByCustomerId($customerId);

        // get data from HTTP
        $input = $request->getQueryParams();

        $locations = $this->customerLocationModel->search(
            $zoneIds,
            $input,
            ['locationStatus', 'locationType', 'warehouse'],
            array_get($input, 'limit')
        );

        return $this->response->paginator($locations, $this->customerLocationTransformer);
    }
}
