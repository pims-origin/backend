<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\CustomerChargeDetail;

class CustomerChargeDetailModel extends AbstractModel
{
    /**
     * CustomerChargeDetailModel constructor.
     *
     * @param CustomerChargeDetail|null $model
     */
    public function __construct(CustomerChargeDetail $model = null)
    {
        $this->model = ($model) ?: new CustomerChargeDetail();
    }

    /**
     * Find a collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $orderBy
     * @param array $columns
     * @param bool $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function findWhere($where, array $with = [], array $orderBy = [], $columns = ['*'], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }
        $this->model->filterData($query);
        foreach ($orderBy as $column => $sortType) {
            $query->orderBy($column, $sortType);
        }

        return $query->get($columns);
    }


}
