<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 29-August-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Reason;

class ReasonModel extends AbstractModel
{
    /**
     * @param Carton $model
     */
    public function __construct(Reason $model = null)
    {
        $this->model = ($model) ?: new Reason();
    }

    /**
     * Search LocationType
     *
     * @param array $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $keys = [
            'like' => ['r_name'],
            'equal' => ['r_id']
        ];

        return $this->_search($attributes, $with, $limit, $keys);
    }
}
