<?php

namespace App\Api\V1\Transformers;


use App\Api\V1\Models\AsnDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Utils\Status;

class AsnListsCanSortTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(AsnHdr $asnHdr)
    {
        $expDate = object_get($asnHdr, 'asn_hdr_ept_dt', '');
        $expDate = ($expDate) ? date('m/d/Y', $expDate) : '';

        $user = '';
        if (object_get($asnHdr, 'createdUser')) {
            $user = object_get($asnHdr, 'createdUser.first_name', '')
                . ' ' .
                object_get($asnHdr, 'createdUser.last_name', '');
        }

        $asnDetail = new AsnDtlModel();
        $data = $asnHdr->asnDtl()->first();

        /*$xDock = AsnDtl::where('asn_hdr_id', $data->asn_hdr_id)
            ->where('ctnr_id', $data->ctnr_id)->sum('asn_dtl_crs_doc');*/

        /*$grInfo = GoodsReceipt::where('ctnr_id', object_get($data, 'container.ctnr_id', ''))
            ->where('asn_hdr_id', $asnHdr->asn_hdr_id)->first();*/

        //total SKU
        $ttlPiece = $asnDetail->calContainersTtlPieceByAsn($asnHdr->asn_hdr_id);

        //total CTN
        $ttlCtn = $asnDetail->calContainersTtlCtnByAsn($asnHdr->asn_hdr_id);

        $ctnId = object_get($data, 'container.ctnr_id', '');
        $ctnNum = object_get($data, 'container.ctnr_num', '');

        return [
            'asn_hdr_id'      => $asnHdr->asn_hdr_id,
            'asn_hdr_num'     => $asnHdr->asn_hdr_num,
            'asn_hdr_ref'     => $asnHdr->asn_hdr_ref,
            "asn_date"        => $asnHdr->created_at->format('m/d/Y'),
            'cus_name'        => $asnHdr->customer->cus_name,
            'whs_id'          => object_get($asnHdr, 'whs_id', ''),
            'cus_id'          => object_get($asnHdr, 'cus_id', ''),
            'ctnr_id'         => object_get($asnHdr, 'ctnr_id', $ctnId),
            'ctnr_num'        => object_get($asnHdr, 'ctnr_num', $ctnNum),
            //"dtl_crs_doc"     => $xDock,
            'asn_hdr_ept_dt'  => $expDate,
            'dtl_po'          => object_get($data, 'asn_dtl_po', ''),
            'dtl_po_date'     => ($data->asn_dtl_po_dt) ? date("m/d/Y", $data->asn_dtl_po_dt) : "",
            'asn_dtl_pack'    => object_get($data, 'asn_dtl_pack', 0),
            'asn_dtl_cus_upc' => object_get($data, 'asn_dtl_cus_upc', ''),
            'asn_dtl_lot'     => object_get($data, 'asn_dtl_lot', ''),
            'asn_sts'         => $asnHdr->asn_sts,
            'asn_sts_name'    => object_get($asnHdr, 'asnStatus.asn_sts_name', ''),
            'asn_sts_des'     => object_get($asnHdr, 'asnStatus.asn_sts_des', ''),
            'user'            => $user,
            'total-sku'       => object_get($asnHdr, 'asn_hdr_itm_ttl', 0),
            'total-piece'     => $ttlPiece,
            'total-ctn'       => $ttlCtn,
            'updated_at'      => strtotime($asnHdr->updated_at),
            //'gr_sts'          => object_get($grInfo, 'gr_sts', null),
            //'gr_sts_name'     => Status::getByValue(object_get($grInfo, 'gr_sts', null), 'GR_STATUS'),
            'containers'      => $asnDetail->getContainersByAsn($asnHdr->asn_hdr_id),
        ];


    }

}
