<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\Putback\Models;

use App\Api\V1\Traits\WorkOrderTrait;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\BOLOrderDetail;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{
    use WorkOrderTrait;

    const DAS_LIMIT = 2;
    const DAS_VALID_DATE = [2, 7];

    protected $statusType = [
        'csr'               => ['NW'],
        'allocate'          => ['NW', 'AL', 'PAL'],
        'wave-pick'         => ['PK', 'PPK', 'PD', 'PIP', 'AL', 'PAL'],
        'packing'           => ['PD', 'PIP', 'PN', 'PPA', 'PA', 'PAP'],
        'pallet-assignment' => ['PA', 'PAP'],
        'shipping'          => ['SH', 'ST', 'PSH'],
        'cancel-order'      => ['CC', 'PCC', 'RMA']
    ];

    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     * @param BOLOrderDetail|null $bolModel
     */
    public function __construct(OrderHdr $model = null, BOLOrderDetail $bolModel = null)
    {
        $this->model = ($model) ?: new OrderHdr();
        $this->bolModel = ($bolModel) ?: new BOLOrderDetail();
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $orderHdrId
     *
     * @return mixed
     */
    public function deleteOrderHdr($orderHdrId)
    {
        return $this->model
            ->where('odr_id', $orderHdrId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);

        //is_ecom
        $isEcom = 0;

        if (!empty($attributes['type']) && $attributes['type'] == "order") {
            $query->whereHas('shippingOrder', function ($sh) {
                $sh->where('type', '!=', Status::getByValue("Ecomm", "Order-Type"));
            });
        } else if (!empty($attributes['type']) && $attributes['type'] == "online-order") {
            $isEcom = 1;
            $query->whereHas('shippingOrder', function ($sh) {
                $sh->where('type', Status::getByValue("Ecomm", "Order-Type"));
            });
        }

        $query->where('odr_hdr.is_ecom', $isEcom);

        // Order Status
        if (!empty($attributes['odr_sts'])) {
            if ($attributes['odr_sts'] == 'ST') {
                $query->select('odr_hdr.*');
                $query->leftJoin('shipment', 'odr_hdr.ship_id', '=', 'shipment.ship_id');
                $query->whereRaw("odr_hdr.odr_sts = 'ST' AND IFNULL(shipment.`ship_sts` != 'FN', 1)");
            } else {
                $query->where('odr_sts', $attributes['odr_sts']);
            }
        } else if (!empty($attributes['type']) && !empty($this->statusType[$attributes['type']])) {
            $query->whereIn('odr_sts', $this->statusType[$attributes['type']]);
        }

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrEqual = ['cus_id'];

        if (!empty($attributes)) {
            if (!empty($attributes['odr_type'])) {
                $query->where("odr_type", $attributes['odr_type']);
            }

            foreach ($attributes as $key => $value) {
                if ($key === "csr") {
                    $query->whereHas("csrUser", function ($q) use ($attributes) {
                        $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere(
                            DB::raw("concat(first_name, ' ', last_name)"),
                            'like',
                            "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['csr'])) . "%"
                        );
                    });
                    continue;
                }
                if ($key === "odr_num") {
                    $query->where($key, "like", "%" . SelStr::escapeLike($value) . "%");
                    continue;
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        // search dashboard day
        if (isset($attributes['dashboard']) && is_numeric($attributes['dashboard'])) {
            $dashboard = in_array($attributes['dashboard'],
                self::DAS_VALID_DATE) ? $attributes['dashboard'] : self::DAS_VALID_DATE[0];
            if (isset($attributes['dashboard_status'])) {
                if ($attributes['dashboard_status'] == 'shipped') {
                    $query->where('odr_hdr.shipped_dt', '>=',
                        DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $dashboard . ' DAY)'));
                } else {
                    $query->where('odr_hdr.updated_at', '>=',
                        DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $dashboard . ' DAY)'));
                }
            }
        }

        // search dashboard status
        if (isset($attributes['dashboard_status'])) {
            if ($attributes['dashboard_status'] == 'new') {
                $query->where('odr_sts', 'NW');
            }

            if ($attributes['dashboard_status'] == 'inprocess') {
                $query->whereIn('odr_sts', ['AL', 'PK', 'PD', 'PN', 'PA', 'ST']);
                $query->select('odr_hdr.*');
                $query->leftJoin('shipment', 'odr_hdr.ship_id', '=', 'shipment.ship_id');
                $query->whereRaw("IFNULL(shipment.ship_sts != 'FN', 1)");
            }

            if ($attributes['dashboard_status'] == 'ship') {
                $query->where('odr_sts', 'ST');
                $query->select('odr_hdr.*');
                $query->leftJoin('shipment', 'odr_hdr.ship_id', '=', 'shipment.ship_id');
                $query->where('shipment.ship_sts', 'FN');
            }

            if ($attributes['dashboard_status'] == 'back') {
                $query->where('odr_type', 'BAC');
                $query->whereNotIn('odr_sts', ['CC', 'SH']);
            }

            if ($attributes['dashboard_status'] == 'shipped') {
                $query->where('odr_sts', 'SH');
            }

        }

        // search sku
        if (isset($attributes['sku'])) {
            $query->whereHas('details', function ($query) use ($attributes) {
                $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
            });
        }

        $this->sortBuilder($query, $attributes);

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis->getCustomersByWhs($currentWH);

        //$this->model->filterData($query, true, true, false);
        $query->whereIn('odr_hdr.cus_id', $cus_ids)
            ->where('odr_hdr.whs_id', $currentWH);

        $models = $query->paginate($limit);

        return $models;
    }

    public function getOrderNum()
    {
        $currentYearMonth = date('ym');
        $defaultWoNum = "ORD-${currentYearMonth}-00001";
        $order = $this->model
            ->where('odr_type', "<>", 'BAC')
            ->orderBy('odr_id', 'desc')
            ->first();

        $lastNum = object_get($order, 'odr_num', '');

        if (empty($lastNum) || strpos($lastNum, "-${currentYearMonth}-") === false) {
            return $defaultWoNum;
        }

        return ++$lastNum;
    }

    /**
     * @param $odrNum
     *
     * @return string
     */
    public function getBackOrderNum($odrNum)
    {
        $order = $this->model
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "-%")
            ->orderBy('odr_id', 'desc')
            ->first();
        $index = 0;
        $orderNum = object_get($order, "odr_num", null);

        if ($orderNum) {
            $temp = explode("-", $orderNum);
            $index = (int)end($temp);
        } else {
            $orderNum = $odrNum;
        }

        return substr($orderNum, 0, 14) . "-" . str_pad(++$index, 2, "0", STR_PAD_LEFT);
    }

    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function checkWhereIn($orderIds)
    {
        return $this->model
            ->whereIn('odr_id', $orderIds)
            ->count();
    }

    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function getOrderById($order_ids)
    {
        $order_ids = is_array($order_ids) ? $order_ids : [$order_ids];

        $rows = $this->model
            ->whereIn('odr_id', $order_ids)
            ->get();

        return $rows;
    }


    public function fetchColumn($columns = [], $where = "", $params = [], $upperCase = false)
    {
        $tblName = $this->getTable();
        $colNames = implode(', ', $columns);

        $query = "
            SELECT $colNames
            FROM $tblName
        ";
        if (!empty($where)) {
            $query .= " WHERE $where";
        }

        $result = [];

        $db = $this->model->getConnection()->getPdo();

        $statement = $db->prepare($query);
        $statement->execute($params);

        $colKey = $columns;
        end($columns);
        $endKey = key($columns);
        unset($colKey[$endKey]);

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            end($row);
            $end = key($row);
            $val = $row[$columns[$endKey]];
            unset($row[$end]);
            $key = implode("-", $row);

            if ($upperCase) {
                $key = strtoupper($key);
            }

            $result[$key] = $val;
        }

        return $result;
    }

    /**
     * @param $orderHdrIds
     * @param array $with
     *
     * @return mixed
     */
    public function loadByOrdIds($orderHdrIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('odr_id', $orderHdrIds);
        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }

    /**
     * @param $skuTotal
     *
     * @return array
     */
    public function updateImportSku($skuTotal)
    {
        $result = [];
        foreach ($skuTotal as $count => $odrIds) {
            $this->refreshModel();
            $result[] = $this->model
                ->whereIn('odr_id', $odrIds)
                ->update(['sku_ttl' => $count]);
        }

        return $result;
    }

    /**
     * @param $input
     * @param array $with
     *
     * @return mixed
     */
    public function getOrderAddress($input, $with = [])
    {
        $bolOrder = (new BOLOrderDetailModel)->loadExcept()->toArray();

        $exceptIds = array_pluck($bolOrder, 'odr_id');
        if (!empty($input['odr_id'])) {
            $exceptIds = array_diff($exceptIds, $input['odr_id']);
        }

        $query = $this->make($with);
        $query->select([
            DB::raw('group_concat(odr_id) as order_ids'),
            DB::raw('trim(replace(replace(concat(ship_to_name, ", ", ship_to_add_1, ", ", ifnull(ship_to_add_2, ""),
            ", ", ship_to_city, ", ", ship_to_state, " ", ship_to_zip, " ", ship_to_country), ", ,", ","), "  ", " ")) as
            full_address'),
            'carrier',
            'ship_method'
        ]);
        $days = $input['ship_ready'];
        if ($days != 'all') {
            $from = strtotime(date('Y-M-d', strtotime('now')) . '00:00:00');
            $days--;
            $to = strtotime(date('Y-M-d', strtotime('now')) . '23:59:59');
            if ($days > 1) {
                $from = strtotime(date('Y-M-d', strtotime("-$days days")) . ' 00:00:00');
            }

            $query->whereBetween('updated_at', [$from, $to]);
        }

        if (empty($input['t']) || $input['t'] != "l") {
            // Create
            $query->where('cus_id', $input['cus_id']);
            $query->whereNotIn('odr_id', $exceptIds);
        } else {
            // List
            if (!empty($input['cus_id']) && $input['cus_id'] != 'all' && is_numeric($input['cus_id'])) {
                $query->where('cus_id', $input['cus_id']);
            }
            $query->whereIn('odr_id', $exceptIds);
        }

        $query->where(function ($q) {
            $q->orWhere('odr_sts', "ST");
            $q->orWhere('odr_sts', "PSH");
        });

        $query->groupBy('full_address');

        $this->model->filterData($query, true);

        return $query->get();
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getBackOrderList($odrNum)
    {
        $backOrderList = $this->model
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "-%")
            ->get();

        return $backOrderList;
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getOriginOrder($odrNum)
    {
        $backOrderList = $this->model
            ->where('odr_num', substr($odrNum, 0, 14))
            ->first();

        return $backOrderList->odr_id;
    }

    public function validateCancelOdr($odrObj)
    {
        $odrStsNotAllowCancel = [
            Status::getByValue("Partial Shipped", "Order-status"),
            Status::getByValue("Hold", "Order-status"),
            Status::getByValue("Shipped", "Order-status"),
            Status::getByValue("Canceled", "Order-status")
        ];

        $odrStatus = $odrObj->odr_sts;
        if (in_array($odrStatus, $odrStsNotAllowCancel)) {
            return false;
        }

        $chkShipFinal = \DB::table('shipment')
            ->where('deleted', 0)
            ->where('ship_id', $odrObj->ship_id)
            ->where('ship_sts', 'FN')
            ->count();

        if ($chkShipFinal) {
            return false;
        }

        return true;
    }

    /**
     * @param $odrId
     *
     * @return bool
     */
    public function getWorkOrderTrueFalseFromOdrId($odrId)
    {
        $workOrderModel = new WorkOrderModel();
        $work_order = false;
        if ($odrId) {
            if ($wo = $workOrderModel->checkWhere(
                [
                    'odr_hdr_id' => $odrId
                ])
            ) {
                $work_order = true;
            }
        }

        return $work_order;
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getDistinctWavePickByOrderIds($odrNum)
    {
        $rows = $this->model
            ->distinct('wv_id')
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "%")
            ->get(['wv_id']);

        return $rows;
    }

    /**
     * @param $odrNum
     *
     * @return mixed
     */
    public function getAllOrderIdsAccordingToOriginOrdNum($odrNum)
    {
        $rows = $this->model
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "%")
            ->get();

        return $rows;
    }

    /**
     * @return mixed
     */
    public function dashboardOdr($input = [])
    {
        $odrSts = [
            'NW'  => [Status::getByValue("New", "Order-status")],
            'IP'  => [
                Status::getByValue("Allocated", "Order-status"),
                Status::getByValue("Partial Allocated", "Order-status"),
                Status::getByValue("Picking", "Order-status"),
                Status::getByValue("Partial Picking", "Order-status"),
                Status::getByValue("Picked", "Order-status"),
                Status::getByValue("Partial Picked", "Order-status"),
                Status::getByValue("Packing", "Order-status"),
                Status::getByValue("Partial Packing", "Order-status"),
                Status::getByValue("Packed", "Order-status"),
                Status::getByValue("Partial Packed", "Order-status")
            ],
            'OS'  => [
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status")
            ],
            'BAC' => [Status::getByValue("BackOrder", "Order-type")],
            'FN'  => [Status::getByValue("Final", "SHIP-STATUS")]
        ];

        foreach ($odrSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $odrSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $query = DB::table('odr_hdr as odr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['NW']}) THEN 1 ELSE 0 END), 0) AS newOrders,
                COALESCE( SUM(CASE WHEN odr.odr_sts IN ({$odrSts['IP']}) THEN 1
                    WHEN (odr.odr_sts IN ({$odrSts['OS']})
                        AND shipment.`ship_sts` NOT IN ({$odrSts['FN']})) THEN 1
                    ELSE 0 END), 0) AS inprocessOrders,
                COALESCE( SUM(CASE WHEN (odr.odr_sts IN ({$odrSts['OS']})
                        AND shipment.`ship_sts` IN ({$odrSts['FN']})) THEN 1
                    ELSE 0 END), 0) AS orderReadyToShip,
                COALESCE( SUM(CASE WHEN odr.odr_type IN ({$odrSts['BAC']})
                    THEN 1 ELSE 0 END), 0) AS backOrders
            "))
            ->leftJoin('shipment', 'odr.ship_id', '=', 'shipment.ship_id')
            ->where('odr.deleted', 0)
            ->where('odr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr.whs_id', $currentWH);

        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('odr.cus_id', $cus_ids);

        $model = $query->get();

        return $model;
    }

    /**
     * @return mixed
     */
    public function dashboardInprocess($input = [])
    {
        $odrSts = [
            'AL' => [
                Status::getByValue("Allocated", "Order-status"),
                Status::getByValue("Partial Allocated", "Order-status")
            ],
            'PK' => [
                Status::getByValue("Picked", "Order-status"),
                Status::getByValue("Partial Picked", "Order-status"),
                Status::getByValue("Picking", "Order-status"),
                Status::getByValue("Partial Picking", "Order-status")
            ],
            'PN' => [
                Status::getByValue("Packing", "Order-status"),
                Status::getByValue("Partial Packing", "Order-status"),
                Status::getByValue("Packed", "Order-status"),
                Status::getByValue("Partial Packed", "Order-status")
            ],
            'ST' => [
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status")
            ],
            'FN' => [Status::getByValue("Final", "SHIP-STATUS")]
        ];

        foreach ($odrSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $odrSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $query = DB::table('odr_hdr as odr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['AL']})
                    THEN 1 ELSE 0 END), 0) AS allocated,
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PK']})
                    THEN 1 ELSE 0 END), 0) AS picked,
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PN']})
                    THEN 1 ELSE 0 END), 0) AS packing,
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['ST']})
                        AND shipment.`ship_sts` NOT IN ({$odrSts['FN']})
                    THEN 1 ELSE 0 END), 0) AS staging
            "))
            ->leftJoin('shipment', 'odr.ship_id', '=', 'shipment.ship_id')
            ->where('odr.deleted', 0)
            ->where('odr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr.whs_id', $currentWH);

        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('odr.cus_id', $cus_ids);

        $model = $query->get();

        return $model;
    }

    /**
     * @param array $attributes
     *
     * @return mixed
     */
    public function getOrderIdAccordingToOdrNum($attributes = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $odr_num = '';
        if (isset($attributes['odr_num'])) {
            $odr_num = $attributes['odr_num'];
        }

        return $this->model
            ->where('odr_num', 'like', "%" . SelStr::escapeLike($odr_num) . "%")
            ->groupBy('odr_num')
            ->get();
    }

    /**
     * @param $orderIds
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function CanceledOrdersSearch($orderIds, $attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->whereIn('odr_id', $orderIds);

        if (isset($attributes['cus_id'])) {
            $query->whereHas('customer', function ($query) use ($attributes) {
                $query->where('cus_id', $attributes['cus_id']);
            });
        }

        if (isset($attributes['odr_type'])) {
            $query->where('odr_type', $attributes['odr_type']);
        }

        // Check odr_type!='ECO' & odr_sts = cancel & not in Return Order
        $query->whereNotIn('odr_type', ['ECO']);
        $query->whereIn('odr_sts', ['CC']);

        $query->whereHas('oderCarton', function ($query) use ($attributes) {
            $query->where('piece_qty', '>', 0);
        });

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'cancel_date') {
                    $attributes['sort']['updated_at'] = $val;
                    unset($attributes['sort'][$key]);
                }

            }
        }

        $this->sortBuilder($query, $attributes);
        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }

    public function createBackOrder($odrHdr, $odrDtls)
    {
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $odrDtlEntity = new OrderDtl();
        /**
         * 1.check backorder = true
         * 1.2 - query order hdr backorder with status = new or create new with seq num + 1 (nO WV)
         * 1.3 query backorder dtl of backorder ID, item_id, lot
         * 1.4 update backorder dtl
         * 1.5 if backorder dtl  not existed , create new
         */
        $odrId = $odrHdr->odr_id;

        // get order flow origin order

        $OrderHdrMetaModel = new OdrHdrMetaModel();
        $orderFlow = $OrderHdrMetaModel->getFirstWhere(['odr_id' => $odrId, 'qualifier' => 'OFF']);

        $odrHdrBac = $updateBac = null;

        foreach ($odrDtls as $odrDtl) {
            if (!$odrDtl->back_odr) {
                continue;
            }
            //check if order is bac => deny create bac
            if ($odrHdr->org_odr_id) {
                $msg = sprintf("Backorder %s cannot accept to create sub Backorder", $odrHdr->odr_num);
                throw new \Exception($msg);
            }

            if (!$odrHdrBac) {
                //get bac ord HDR
                $odrHdrBac = $this->model
                    ->where('odr_sts', 'NW')
                    ->where('org_odr_id', $odrId)
                    ->first();

                if (!$odrHdrBac) {
                    $odrHdrBac = $odrHdr->replicate();
                    $odrHdrBac->odr_num = $odrHdrBac->odr_num . '-01';
                    $odrHdrBac->org_odr_id = $odrId;
                    $odrHdrBac->odr_id = null;
                    $odrHdrBac->wv_id = null;
                    $odrHdrBac->wv_num = null;
                    $odrHdrBac->odr_sts = 'NW';
                    $odrHdrBac->sku_ttl = 0;
                    $odrHdrBac->back_odr = 0;
                    $odrHdrBac->back_odr_seq = 1;
                    $odrHdrBac->odr_type = Status::getByValue('BackOrder', 'ORDER-type');
                    $odrHdrBac->push();
                } else {
                    $updateBac = true;
                }
            }

            $odrHdrBac->sku_ttl++;

            //get bac order detail
            $bacOdrDtl = $odrDtlEntity
                ->where('odr_id', $odrHdrBac->odr_id)
                ->where('item_id', $odrDtl->item_id)
                ->where('lot', $odrDtl->lot)
                ->first();

            if (!empty($bacOdrDtl)) {
                //update
                $bacOdrDtl->piece_qty = $odrDtl->back_odr_qty;
                $bacOdrDtl->back_odr_qty = 0;
                $bacOdrDtl->back_odr = false;
                $bacOdrDtl->save();
            } else {
                //create
                $bacOdrDtl = null;
                $bacOdrDtl = $odrDtl->replicate();

                $bacOdrDtl->odr_id = $odrHdrBac->odr_id;
                $bacOdrDtl->piece_qty = $odrDtl->back_odr_qty;
                $bacOdrDtl->back_odr_qty = 0;
                $bacOdrDtl->back_odr = false;
                $bacOdrDtl->alloc_qty = 0;
                $bacOdrDtl->itm_sts = 'NW';
                $bacOdrDtl->qty = $odrDtl->pack > 0 ? ceil($odrDtl->back_odr_qty / $odrDtl->pack) : 0;
                $bacOdrDtl->created_at = time();
                $bacOdrDtl->updated_at = time();
                $bacOdrDtl->push();
            }
        }
        if ($odrHdrBac) {
            $odrHdrBac->save();

            if ($updateBac === true) {
                return;
            }

            $OrderHdrMetaModel->insertOdrHdrMeta([
                'odr_id'    => $odrHdrBac->odr_id,
                'qualifier' => 'OFF',
                'value'     => array_get($orderFlow, 'value', 0)
            ]);
            // Insert Event Tracking
            $evtTracking = new EventTrackingModel(new EventTracking());
            $evtTracking->create([
                'whs_id'    => $odrHdrBac['whs_id'],
                'cus_id'    => $odrHdrBac['cus_id'],
                'owner'     => $odrHdr->odr_num,
                'evt_code'  => Status::getByKey("event", "NEW-BACK-ORDER"),
                'trans_num' => $odrHdrBac->odr_num,
                'info'      => sprintf(Status::getByKey("event-info", "BNW"), $odrHdrBac->odr_num, $odrHdr->odr_num)
            ]);
        }

    }

    public function createCancelBacOrder($odrHdrObj, $odrDtlCancels)
    {
        $ttlSku = count($odrDtlCancels);
        $bacOdrHdrObj = $this->createCancelBacOrdHdr($odrHdrObj, $ttlSku);
        $this->createCancelBacOrdDtl($bacOdrHdrObj, $odrDtlCancels);

        return $bacOdrHdrObj;
    }

    public function createCancelBacOrdHdr($originOdrHdrObj, $ttlSku = 0)
    {
        $seq = empty($originOdrHdrObj->back_odr_seq) ? 1 : ++$originOdrHdrObj->back_odr_seq;

        $odrHdrBac = $originOdrHdrObj->replicate();
        $odrHdrBac->odr_num = $originOdrHdrObj->odr_num . '-' . str_pad($seq, 2, "0", STR_PAD_LEFT);;
        $odrHdrBac->org_odr_id = $originOdrHdrObj->odr_id;
        $odrHdrBac->odr_id = null;
        $odrHdrBac->wv_id = $originOdrHdrObj->wv_id;
        $odrHdrBac->wv_num = $originOdrHdrObj->wv_num;
        $odrHdrBac->odr_sts = $originOdrHdrObj->odr_sts;
        $odrHdrBac->sku_ttl = $ttlSku;
        $odrHdrBac->back_odr = 0;
        $odrHdrBac->back_odr_seq = $seq;
        $odrHdrBac->odr_type = 'BAC';
        $odrHdrBac->push();

        return $odrHdrBac;
    }

    public function createCancelBacOrdDtl($bacOdrHdrObj, $orginOdrDtlDatas)
    {
        foreach ($orginOdrDtlDatas as $orginOdrDtlData) {
            $originOdrDtlObj = $orginOdrDtlData['odr_dtl_obj'];
            $cancelQty = $orginOdrDtlData['cancel_qty'];
            $allocQty = $originOdrDtlObj->alloc_qty - $originOdrDtlObj->picked_qty;
            $newPickedQty = $cancelQty > $allocQty ? $cancelQty - $allocQty : 0;

            //create new bac order detail to cancel
            $bacOdrDtl = $originOdrDtlObj->replicate();
            $bacOdrDtl->odr_id = $bacOdrHdrObj->odr_id;
            $bacOdrDtl->piece_qty = $cancelQty;
            $bacOdrDtl->back_odr_qty = 0;
            $bacOdrDtl->back_odr = false;
            $bacOdrDtl->alloc_qty = $originOdrDtlObj->alloc_qty > 0 ? $cancelQty : 0;
            $bacOdrDtl->itm_sts = $originOdrDtlObj->itm_sts;
            $bacOdrDtl->qty = ceil($cancelQty / $originOdrDtlObj->pack);
            $bacOdrDtl->picked_qty = $newPickedQty;
            $bacOdrDtl->created_at = time();
            $bacOdrDtl->updated_at = time();
            $bacOdrDtl->push();

            //update allocate to origin odr dtl
            if ($originOdrDtlObj->alloc_qty > 0) {
                $originOdrDtlObj->alloc_qty -= $cancelQty;
                $originOdrDtlObj->back_odr_qty = $cancelQty;
                $originOdrDtlObj->back_odr = 1;
                $originOdrDtlObj->picked_qty -= $newPickedQty;
                $originOdrDtlObj->save();
            }

            //update odr cartons
            $odrCartonByPieces = $this->sumOdrCartonsByPieceQty($originOdrDtlObj->odr_dtl_id);
            foreach ($odrCartonByPieces as $odrCartonByPiece) {
                if ($odrCartonByPiece['ttl_piece'] < $newPickedQty) {
                    //update date to new odrDtl info
                    $this->updateFullOdrCartons($originOdrDtlObj->odr_dtl_id, $bacOdrDtl->odr_id,
                        $bacOdrDtl->odr_dtl_id, $odrCartonByPiece['piece_qty']);
                    $newPickedQty -= $odrCartonByPiece['ttl_piece'];
                } else {
                    $limit = floor($newPickedQty / $odrCartonByPiece['piece_qty']);
                    $pieceOdrCtn = $newPickedQty % $odrCartonByPiece['piece_qty'];

                    if ($limit > 0) {
                        $this->updateFullOdrCartons($originOdrDtlObj->odr_dtl_id, $bacOdrDtl->odr_id,
                            $bacOdrDtl->odr_dtl_id, $odrCartonByPiece['piece_qty'], $limit);
                    }

                    if ($pieceOdrCtn > 0) {
                        $this->updatePieceOdrCartons($originOdrDtlObj->odr_dtl_id, $bacOdrDtl->odr_id,
                            $bacOdrDtl->odr_dtl_id, $odrCartonByPiece['piece_qty'], $pieceOdrCtn);
                    }

                    break;
                }
            }
        }
    }

    public function sumOdrCartonsByPieceQty($odrDtlId)
    {
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        return OrderCarton::where('odr_dtl_id', $odrDtlId)
            ->select(DB::raw('SUM(piece_qty) AS ttl_piece, piece_qty'))
            ->groupBy('piece_qty')
            ->orderBy('piece_qty', 'desc')
            ->get()
        ;
    }

    public function updateFullOdrCartons($origOdrDtlId, $bacOdrHdrId, $bacOdrDtlId, $pieceQty, $limit = null)
    {
        $limit = empty($limit) ? 10000 : $limit;
        OrderCarton::where([
            'odr_dtl_id' => $origOdrDtlId,
            'piece_qty'  => $pieceQty
        ])
            ->limit($limit)
            ->update([
                'odr_dtl_id' => $bacOdrDtlId,
                'odr_hdr_id' => $bacOdrHdrId
            ])
        ;
    }

    public function updatePieceOdrCartons($origOdrDtlId, $bacOdrHdrId, $bacOdrDtlId, $pieceQty, $pieceOdrCtn)
    {
        $originOdrCtn = OrderCarton::where([
            'odr_dtl_id' => $origOdrDtlId,
            'piece_qty'  => $pieceQty
        ])
           ->first();

        if ($pieceOdrCtn == $originOdrCtn->piece_qty) {
            $originOdrCtn->odr_hdr_id = $bacOdrHdrId;
            $originOdrCtn->odr_dtl_id = $bacOdrDtlId;
        } else {
            $originOdrCtn->piece_qty -= $pieceOdrCtn;

            //clone new odr ctn
            $newOdrCtn = $originOdrCtn->replicate();
            $newOdrCtn->piece_qty = $pieceOdrCtn;
            $newOdrCtn->odr_hdr_id = $bacOdrHdrId;
            $newOdrCtn->odr_dtl_id = $bacOdrDtlId;
            $newOdrCtn->odr_ctn_id = null;
            $newOdrCtn->push();
        }

        $originOdrCtn->save();
    }
}
