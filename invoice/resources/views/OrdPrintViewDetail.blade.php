<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>VIEW ORDER DETAILS PDF</title>
    <style type="text/css">
        /*@page {*/
        /*size: landscape;*/
        /*}*/

        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 32px;
            font-weight: 100;
            margin: 0;
        }

        h2 {
            color: #364150;
            text-transform: uppercase;
            margin: 5px 0;
            font-weight: 500;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 14px;
            font-family: "Calibri";
            width: 29.7cm;
            margin: 0 auto;
            border: 1px solid #ddd;
            padding: 20px;
            margin: 0 auto;

        }

        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #a9a9a9;
            font-size: 19px;
        }

        .table-style tbody tr td {
            padding-top: 5px;
            vertical-align: top;
            padding-bottom: 20px;
        }

        .table-style tbody tr td b {
            font-size: 20px;
            font-weight: 600;
            color: #282828;
        }

        .table-style2 {
            border: 1px solid #a9a9a9;
            margin: 15px 0;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            line-height: 20px;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 1px dashed #d5d5d5;
            border-bottom: 1px dashed #d5d5d5;
            color: #595959;
            line-height: 30px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }

        .table-style2 tbody tr td.title {
            font-weight: 600;
            color: #364150;
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }

    </style>
</head>
<body>

<table style="width: 100%; border-bottom:1px solid #ddd;margin-bottom: 10px" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td width="" style="vertical-align: middle">
            <img src="data:image/png;base64,
            iVBORw0KGgoAAAANSUhEUgAAAXAAAABBCAYAAADISP
            +DAAAACXBIWXMAAC4jAAAuIwF4pT92AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAACl1JREFUeNrsne9x27oSxY81/n75KrhwBUEqCF1BlAI4kiuwXIHsCixXYGlYgOUKTFcQpILgVnD5OngfuMpjFNsiJQAkwfOb0TiJFf4DcLhY7C7OEBilswSAfufXxpq8BCGEkIOcORRmBUABSAH8VRNpDSA58rAlACN/fq393ZvQH3jBxIC1Jrd795x6OldvXsg9btdBGi218e69f454rB58VucnNKAWsf56okh/RCLnQO3n7vwWQCHCvnU4CDSAl4gb/Q7A7d6/+brfS2mjPtDbdlU6+zUga59/5Nn1VeBffAk4gAuO1WZj+bxlR1MA5gBmnhqv1eXItcwBPCqdbQE8W5OvObEiA2Xfql3KuDMi5htrctODF07qcfwrpbM5x3Ezzls02EzEsq9MAUyVzu4BPABY0Z9OIkHLZyEzz03H/Xvp+fgzABTwBkwOCHeidPYo05T5QO4pkQ72U+lsyiYmEVrpSwD/Kp09ih84pPWtsOfO9EAqLlpyrICL+P0ckHC/JeRPSmdPoTs5IYGYi6FyG5H1veOazXukgIsb4gl+FiZDMwXwQhEnkZIAWCqdfRfr2Kf1nQQ06Oa+7ydKAReXySKy+9QUcRI5GsB3jyGh6EAX5mzWFgKudLaI+KFpjDvkiIzDGn9ROvM1hkO7Na5pdDUUcFk0uI/dShH3ECEx8+haxOV4ocU0QeUCJQ0s8LEI24K+NTISEdcOj7fs6D6WbMoDAi4RJ+mI7pudgowBJ+s+nhN3Dp7es19/0OwSeWaBz7tGlYzwq+5Bra6BRuVr89lh5kpnNx0kQlj5dIlltx8NCYBHAN8GbvAs0Z+SDL0U8Gmg85UALt9KBxYxLeSzkgXVJfz53VIA28DPe2NNfstuFx1rVLVLPuITfq/tE4qp0llqTX6UAAZK3Dk4VpXOVNMiV6MS8MAZT5dNazlYk6+UzgoA3z1dy5cOBJzEyaaNQNYKwc0QppreI5oXiOqb9V2/jit2td+ZINzKctG2EI98f+3pejSbn3SBNbmxJl9Zk39GmIqN6pioFIeJO6WjmUTC3rNngQc8l1Y6S47wO2/AgH4Sr5gXAApJh/dp7R5TIGrhSLxvZBZwColcz+3esztz+ZCUzl7gyWVkTX7m+pjnjt6OTRvgRRYPizYdXOnszsfzpHyQHgn5rdJZCX/hvMf4kV0k7mzl8+jgWDP8Wct+3Ba4NbmpFZT3boWLiBsAz6jcKkWTzs2mIiMQ8ZXS2Vf4WzScAlg1tETncONefbAmL6Ve//RUA5m1wv+0wIHKB5cGPK+Wz1JeHkY+P1DtQFKwachIufM4Fr82FXC4cefY2rrXBm6i3a7BWuF/CPgG3YYK7QR99/anqJOxWuGFzFC1h8M3GuMOE3e2tfvaiovoVKtenxIWGaWAW5Ovlc6W6H6btCaiXtRE3Qzsec+Uzr4EFINLdvFBsvEk4Ggofq4WUx/eEPS5i3EEJvb8ZoED1UrxU8+vd1/US/x/Y+NiAIKuevaSJP2k8DyGig8EXjmajZs3FkwfHAn4XOnsjok9tWJW1uRbNPeP9YUElV/tHlUt5J9KZ/csVkWGjGdDJAlkfW/euS9Xossde7BXD9ya/AbDXiBQqGJFfyqdvbAIDhkwvkT8ywfWdwJ3ORfbpsJ+ghWejL2TTN54S16hWgkfOimqkEUKORkiZQfnXLgS7w/cG64MxN3smwL+hojfokrxtRHc407I7/nGJhTwD3Hllnh+7xci7K5mF6MvCz354EEXAD5HYo3vrIvvgYt3EXIsP0KezGHiTonDReIeXF227GVAAX9HxEuxxi9k6lMO/H6VWOMUcUL8WLPbBvWOtg6ve9SLmZMmX7Imt+Ibv0AVbmgHfM8JRZyQ36zvFO7CW58b6EkTK70p6ZjH8qTNl8UiX1mTX6Byr6wGKuYJqj0DEw5f0lM+DdD6LiUcuQkbh9c/Wit8cux/lJrGNzUxv8GwsqM03K26E+LDyAhhfSu4K6OxbqEfW7hzyY42pNBJPXAJ0Ddike+mZKlYESnCbRrR+s2tdLYKuDemBcvYkn7hMpKjrVW9hbu48wVGWGrWy4YOuyL1e295VRN1hX7siJNIB1oFOh/3xCRtZog+eK2Ny8ShgNojMkgfHJ7/mgLuy+ysYj8t9lwstcWTnaXehajPMLwSAiRiRFhDzFoXDo9ljkyYKx3dazLGWuHnXZ58vyqadNwUVd3ieaDL0CCkX6Qej13sGS+umKL7zMglRlYr/FwC+B99dJS25Uxr4UVb2UZtLlMjr9aI0pkeYGlaEi9ffdpN0ufniK8yphpbrfAJ/C2qpafUIJHY81tUKf2l5+eQgJA+KFA1C/VlydpajZJYQ+9GFVI48fy2ejo1vEcs4wcObTISlh4NikJeEinidR1Ox1ROehcH7st9kKDKejy1QxrPz8GCkO6t7zn85iY8j8RKHU2Rq0n9zewJjaqIVHrCMXz6BMGdPUhPxPvRZzeXfSkV4i/DOh1LYk99U2Ofb34llvgawEObBUOlswX8RqQUIL64l23vumAzhJAyEdT7AKK6GZF1miBsfke3Am5NbpTOLPyvSs9Rpb1aVNEmPyDuC9mNO60J/ifp1L6v6RnE5+yrK157LNgKVajgF/gNGfxlfQNYeV4g7RvXoxFw4c7zFG7fIl/sdeyunsE24LmC7ko/dOs0opmGQrchezfW5KXS2S3GE3GlxpDY80vArcnXSmdLjGvX9HVg/7fqyfN9BYllptGkj++MlNnI2mWGyBN79qsRXo2ocUtUFRQJiRWz6+ORJu4cIo19P9zfUunFD73COMqsXgWsQkhIFwbKt1ofdx06WAxkpjpDxIEKf9RCsSa/iTzQHwBWLQrPEzJE8b7cuQc9jOfWZTJaqXd1vS+ODjdXOruLNVT4vQ0dLuE/eaYr1tbkdJ2QWDEAPu+F6rq2vjc+b0Cyw13qzzzWxp688wDLSEV8LXt7EhIj27rlLdasgvvQwRCzV5flM65jTeyZfPAW3In4OpIp5RXFm0RKiSpU8Nsb6zquE3e2IdaOJPzP1XkSRBr/PjnwEEsRvW/wXxHQF4VMKdcc5yRC1gAurMlX+7/wlLgTMvHNpRUeZQZqo02NZcHvAsPKbLKoVuEvWeuERCzcH0VTLeA+cWcb+B5doWIMKWy8K71Y4zc1Ie+rRV6gcpdcMNKERIZBFde9E+5DhonrxJ11yNBbuT+XIh6dFX5+5EO9kR1zpqgqBU570LE3qPxztLZJDJTSr1/lZ9FGPD0l7nRRN2gDd1EkqdKZikkjjt4TUzrTeveGVDqbooo1DVGgp6h17oIJOWSAFHsGyH9Ruf0sAOOgT7u2vssuZrSSXGjgLo59iYgyzs98HbhWdU2j8sP93dIiKFFVK0StY1ta2ISMCw+10v8Ti9F3xu5BCBmAiP8Ldwuyd7Lf7uCZsGsQQgaAy5DCaKoyUsAJIUNg7dKgF7cMBZwQQnwja19bh4eMYmNnCjghZCi4dKPoGBJ7KOCEkKFY4QVkD11HDN4Xfs5uQQgZEJdwl6BUDv1h/G8AtZnd1701xVYAAAAASUVORK5CYII" width="25%">
        </td>
        <td class="text-right">
            <h1>VIEW ORDER DETAILS</h1>
        </td>
    </tr>
    </tbody>
</table>

<!--List-->
<table class="table-style2" cellpadding="0" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th style="width: 16%; text-align: center;">CLIENT</th>
        <th style="width: 14%; text-align: center;">CUSTOMER NAME</th>
        <th style="width: 11%; text-align: center;">CLIENT ORDER #</th>
        <th style="width: 13%; text-align: center;">CUSTOMER ORDER #</th>
        <th style="width: 12%; text-align: center;">ORDER #</th>
        <th style="width: 6%; text-align: center;">CTN</th>
        <th style="width: 7%; text-align: center;">PCS</th>
        <th style="width: 7%; text-align: center;">PLT</th>
        <th style="width: 9%; text-align: center;">DATE</th>
        <th style="width: 5%; text-align: center;">TYPE</th>

    </tr>


    </thead>
    <tbody>

    @if(!empty($details))
        @foreach($details as $detail)
            <tr>
                <td>{{$detail['client']}}</td>
                <td>{{$detail['cus_name']}}</td>
                <td>{{$detail['client_odr_num']}}</td>
                <td>{{$detail['cust_odr_num']}}</td>
                <td>{{$detail['odr_num']}}</td>
                <td>{{$detail['cq']}}</td>
                <td>{{$detail['pq']}}</td>
                <td>{{$detail['plq']}}</td>
                <td>{{$detail['dt']}}</td>
                <td>{{$detail['odr_typ']}}</td>
            </tr>

        @endforeach
    @endif
    </tbody>
</table>
<!--List-->

</body>
</html>