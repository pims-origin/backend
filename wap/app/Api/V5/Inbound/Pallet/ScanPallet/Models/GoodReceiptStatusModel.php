<?php

namespace App\Api\V5\Inbound\Pallet\ScanPallet\Models;

use Seldat\Wms2\Models\GoodsReceiptStatus;

class GoodReceiptStatusModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model =  new GoodsReceiptStatus();
    }



}
