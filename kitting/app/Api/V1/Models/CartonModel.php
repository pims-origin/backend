<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use App\Carton;
use App\Item;
use App\Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\JWTUtil;

class CartonModel extends AbstractModel
{

    protected $model;

    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    /**
     * @param $item_id
     * @param $loc_id
     * @param $type
     * @param $isEcom
     *
     * @return mixed
     */
    public function getAllCartonByItmId($item_id, $loc_id, $type, $isEcom)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        if ($type == "partial") {
            $compare = '!=';
        } else {
            $compare = "=";
        }

        if ($isEcom == true) {
            $yEcom = '(is_ecom <> 0 AND is_ecom is not null)';
        } else {
            $yEcom = '(is_ecom = 0 OR is_ecom is null)';
        }
        $query = $this->make([]);

        return $query
            ->where('item_id', $item_id)
            ->where('loc_id', $loc_id)
            ->where('whs_id', $currentWH)
            ->whereRaw('ctn_pack_size' . $compare . 'piece_remain')
            ->where('piece_remain', '>', 0)
            ->where('ctn_sts', 'AC')
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw($yEcom)
            ->orderBy('piece_remain', 'DESC')
            ->get();
    }


    public function getPickedCartonByWv($wvDtl, $algorithm)
    {
        $query = $this->make([]);
        $pickedQty = $wvDtl['picked_qty'];

        $query
            ->select([
                'sku',
                'lot',
                'size',
                'color',
                'piece_remain',
                'piece_ttl',
                'created_at',
                'expired_dt',
                'ctn_id',
                'ctn_num',
                'rfid',
                'length',
                'width',
                'height',
                'whs_id',
                'cus_id',
                'item_id',
                'ctn_pack_size',
                'upc',
                'ctn_uom_id',
                'uom_code',
                'uom_name'
            ])
            ->whereNotIn('ctn_id', $wvDtl['ctn_ids'])
            ->where('item_id', $wvDtl['itm_id'])
            ->where('loc_id', $wvDtl['act_loc_id'])
            ->where('whs_id', $wvDtl['whs_id'])
            ->where('ctn_sts', 'AC')
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                $query->orderBy('created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                $query->orderBy('created_at', 'ASC');
                break;
        }

        $query->orderBy('lot', 'ASC')
            ->orderBy('piece_remain', 'ASC');

        $carton = $query->first();

        return $carton;
    }

    public function getAvailablePacksizes($whs_id, $itemID, $lot, $locIds, $algorithm)
    {
        $query = $this->getModel()
            ->whereIn('loc_id', $locIds)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('lot', $lot)
            ->groupBy('ctn_pack_size');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        $res = $query->get(['ctn_pack_size'])->toArray();

        return array_column($res, 'ctn_pack_size');
    }

    public function getCartonsByPack($whs_id, $itemID, $lot, $locId, $packSize, $ctns, $algorithm)
    {
        $query = $this->getModel()
            ->where('loc_id', $locId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('lot', $lot)
            ->where('ctn_pack_size', $packSize)
            ->limit($ctns)
            ->orderBy('ctn_pack_size');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        $query->orderBy('cartons.piece_remain', 'DESC');

        $res = $query->get()->toArray();

        return $res;
    }

    public function array_remove($remove, &$array)
    {
        foreach ((array)$array as $key => $value) {
            if ($value == $remove) {
                unset($array[$key]);
                break;
            }
        }

        return $array;
    }

    public function sortCartonsByLoc($cartons)
    {
        $sortCartons = [];
        foreach ($cartons as $carton) {
            $sortCartons[$carton['loc_id']][] = $carton;
        }

        return $sortCartons;
    }

    public function getAllCartonsByWvDtl($wvDtl, $algorithm = 'FIFO', $locIds, $actLocs)
    {
        $packSize = $wvDtl['pack_size'];

        $whsId = $wvDtl['whs_id'];
        $itemId = $wvDtl['item_id'];
        $lot = $wvDtl['lot'];
        $packSizes = $this->getAvailablePacksizes($whsId, $itemId, $lot, $locIds, $algorithm);

        if (!in_array($packSize, $packSizes)) {
            $packSize = array_shift($packSizes);
        } else {
            $packSizes = $this->array_remove($packSize, $packSizes);
        }

        array_unshift($packSizes, $packSize);
        $pickedcartons = [];

        foreach ($actLocs as $actLoc) {
            $pickedQty = $actLoc['picked_qty'];
            foreach ($packSizes as $packSize) {
                $ctns = ceil($pickedQty / $packSize) + 50;
                $cartons = $this->getCartonsByPack($whsId, $itemId, $lot, $actLoc['act_loc_id'], $packSize, $ctns,
                    $algorithm);

                $qtys = array_column($cartons, 'piece_remain');
                $total = array_sum($qtys);
                $pickedcartons = array_merge($pickedcartons, $this->getPickCartons($cartons, $pickedQty));
                $pickedQty = $pickedQty - $total;

                //break if enough cartons
                if ($pickedQty <= 0) {
                    break;
                }
            }
        }

        return $pickedcartons;
    }

    public function getAvailableQuantity($itemID, $lot, $locIds, $whs_id)
    {
        $query = $this->getModel()
            ->whereIn('loc_id', $locIds)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('lot', $lot);

        $qty = $query->sum('piece_remain');

        return $qty;


    }

    /**
     * @param $item_id
     * @param $loc_id
     * @param $type
     * @param $isEcom
     *
     * @return mixed
     */
    public function getCartonByItmId($item_id, $ex = [0])
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $query = $this->make([]);

        $res = $query->where('item_id', $item_id)
            ->select('ctn_id', 'piece_remain')
            ->where('whs_id', $currentWH)
            ->whereNotIn('ctn_id', $ex)
            ->orderBy('piece_remain', 'DESC')
            ->first();

        return $res;
    }


    /**
     * @param $data
     *
     * @return mixed
     */
    public function getNumCartonNearlyPickingQty($data)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return $this->model->where('whs_id', $currentWH)
            ->where('item_id', $data['item_id'])
            ->where('cus_id', $data['cus_id'])
            ->where('piece_remain', '<=', $data['picking_qty'])
            ->orderBy('piece_remain', 'DESC')
            ->get();
    }

    public function getNumCartonNearlyOrderQty($data, $limit = 20, $offset = 0)
    {

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return $this->model->where('whs_id', $currentWH)
            ->where('item_id', $data['item_id'])
            ->where('cus_id', $data['cus_id'])
            ->where('piece_remain', '<=', $data['piece_qty'])
            ->orderBy('piece_remain', 'DESC')
            ->take($limit)->skip($offset)->get();
    }

    /**
     * @param $locIds
     * @param $itemIds
     *
     * @return mixed
     */
    public function getTtlItemInLocation($locIds, $itemIds)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table("cartons")
            ->leftJoin('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->where('cartons.whs_id', '=', $currentWH)
            ->where('cartons.loc_id', $locIds)
            ->where('cartons.item_id', $itemIds)
            ->where('cartons.piece_remain', '>', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('location.loc_sts_code', 'AC')
            ->whereRaw('(cartons.is_damaged = 0 OR cartons.is_damaged is null)')
            ->select([
                'cartons.item_id as ITEM_ID',
                'cartons.loc_id as primary_loc_id',
                DB::raw("(select sum(cartons.piece_remain) from cartons where cartons.item_id =" . $itemIds . " and cartons
                .loc_id = " . $locIds . ") as numItems")
            ])
            ->distinct()
            ->first();
    }

    public function getCartonNearByConsolidate($locIds, $itemIds, $qty, $limit)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $query = $this->make(['location']);

        return $query->where('loc_id', '<>', $locIds)
            ->where('item_id', $itemIds)
            ->where('cartons.whs_id', '=', $currentWH)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->whereRaw('plt_id is not null')
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereHas('location', function ($query) {
                $query->where('loc_sts_code', 'AC');
            })->orderBy('loc_id', 'asc')->take($limit)->get();
        // ->havingRaw('sum(piece_remain) > ' . $qty)->get();

    }

    /**
     * @param $locIds
     * @param $itemIds
     *
     * @return mixed
     */
    public function getCartonForWavePick($locIds, $itemIds)
    {
        // Wv ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];
        // Item ids
        $itemIds = is_array($itemIds) ? $itemIds : [$itemIds];

        $query = DB::table('cartons as C')
            ->join('location as L', 'L.loc_id', '=', 'C.loc_id')
            ->join('pallet as P', 'P.plt_id', '=', 'C.plt_id')
            ->select([
                'C.item_id',
                'C.lot',
                'L.loc_code',
                DB::raw('COUNT(C.ctn_id) as numCarton'),
                DB::raw('SUM(C.piece_remain) as pieceRemain'),
                'P.plt_num'
            ])
            ->whereIn('C.loc_id', $locIds)
            ->whereIn('C.item_id', $itemIds)
            ->where([
                'C.is_damaged' => 0,
                'C.deleted'    => 0
            ])
            ->groupBy('L.loc_id')
            ->orderBy('C.created_at');

        return $query->get();
    }

    /**
     * @param $locIds
     * @param $itemIds
     * @param $wvId
     *
     * @return mixed
     */
    public function getCartonAndWvDtlLocForWavePick($locIds, $itemIds, $wvId)
    {
        // Wv ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];
        // Item ids
        $itemIds = is_array($itemIds) ? $itemIds : [$itemIds];

        $query = DB::table('cartons as C')
            ->join('location as L', 'L.loc_id', '=', 'C.loc_id')
            //->join('pallet as P', 'P.plt_id', '=', 'C.plt_id')
            ->join('pallet as P', 'P.plt_id', '=', 'C.plt_id')
            ->select([
                'C.item_id',
                'C.lot',
                'L.loc_code',
                DB::raw('COUNT(C.ctn_id) as numCarton'),
                'C.piece_remain as pieceRemain',
                'P.plt_num'
            ])
            ->whereIn('C.loc_id', $locIds)
            ->whereIn('C.item_id', $itemIds)
            ->groupBy('L.loc_id')
            ->orderBy('C.created_at');

        return $query->get();
    }

    /**
     * @param $locIds
     * @param $itemIds
     *
     * @return mixed
     */
    public function getSugLocByLocIds($locIds, $item_id, $lot)
    {
        // Loc ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];

        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();


        $cus_id = (new Item())->select('cus_id')
            ->where('item_id', $item_id)
            ->value('cus_id');
        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cus_id);

        $query = DB::table('cartons as C')
            ->select([
                'C.loc_id',
                'C.loc_code',
                DB::raw('sum(C.piece_remain) as avail_qty'),
                'P.plt_num'
            ])
            ->join('pallet as P', 'P.plt_id', '=', 'C.plt_id')
            ->where('C.ctn_sts', 'AC')
            ->where('C.lot', $lot)
            ->where('C.item_id', $item_id)
            ->where('C.is_damaged', 0)
            ->where('C.loc_type_code', 'RAC')
            ->whereIn('C.loc_id', $locIds)
            ->groupBy('C.loc_id');
            // ->orderBy('C.loc_id');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('C.gr_dt', 'DESC');
                $query->orderBy('C.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('C.expired_dt = 0, C.expired_dt'), 'ASC');
                $query->orderBy('C.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('C.gr_dt', 'ASC');
                $query->orderBy('C.created_at', 'ASC');
                break;
        }

        return $query->get();
    }

    /**
     * @param array $locIds
     * @param int $itemId
     *
     * @return mixed
     */
    public function getSugLocByItemIdsAndDiffLocIds($locIds, $itemId)
    {
        // Loc ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];

        $query = DB::table('cartons')
            ->select([
                'loc_id',
                'loc_code',
                DB::raw('sum(piece_remain) as avail_qty')
            ])
            ->where('is_damaged', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('item_id', (int)$itemId)
            ->whereNotIn('loc_id', $locIds)
            ->groupBy('loc_id')
            ->orderBy('loc_id');

        return $query->first();
    }

    /**
     * @param array $locIds
     * @param int $itemId
     *
     * @return mixed
     */
    public function getMoreSugLocByWvDtl($locIds, $data, $take = 1)
    {
        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();
        // Loc ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];
        $itemId = (int)$data['item_id'];
        $is_ecom = (int)$data['is_ecom'];


        $cus_id = (new Item())->select('cus_id')->where('item_id', $itemId)->value('cus_id');
        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cus_id);

        $query = DB::table('cartons')
            ->select([
                'loc_id',
                'loc_code',
                DB::raw('sum(piece_remain) as avail_qty')
            ])
            ->where('is_damaged', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('item_id', (int)$itemId)
            ->where('is_ecom', $is_ecom)
            ->where('lot', $data['lot'])
            ->whereNotIn('loc_id', $locIds)
            ->groupBy('loc_id');
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        if ($take == 1) {
            return $query->first();
        } else {
            return $query->limit($take)->skip(0)->get();

        }
    }


    /**
     * @param array $locIds
     * @param int $itemId
     *
     * @return mixed
     */
    public function suggestActutalLocations($locIds, $data)
    {
        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();
        // Loc ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];
        $itemId = (int)$data['item_id'];
        $is_ecom = (int)$data['is_ecom'];


        $cus_id = (new Item())->select('cus_id')->where('item_id', $itemId)->value('cus_id');
        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cus_id);

        $query = DB::table('cartons')
            ->select([
                'loc_id',
                'loc_code',
                DB::raw('sum(piece_remain) as avail_qty')
            ])
            ->where('is_damaged', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('item_id', (int)$itemId)
            ->where('is_ecom', $is_ecom)
            ->where('lot', $data['lot'])
            ->whereNotIn('loc_id', $locIds)
            ->where('loc_code', 'LIKE', $data['loc_code'] . '%')
            ->groupBy('loc_id')
            ->limit(20);
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        return $query->get();
    }


    public static function getCalculateStorageDurationRaw($startDate = 'created_at')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    public function updatePickedFullCartons($fullCtns)
    {
        $ctnIDs = array_keys($fullCtns);

        $res = DB::table('cartons')->whereIn('ctn_id', $ctnIDs)->update(
            [
                'ctn_sts'          => 'PD',
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'plt_id'           => null,
                'picked_dt'        => time(),
                'storage_duration' => self::getCalculateStorageDurationRaw()
            ]
        );

        return $res;
    }

    public function updatePickedPieceCarton($carton)
    {
        $remainQty = $carton['piece_remain'] - $carton['picked_qty'];
        DB::table('cartons')->where('ctn_id', $carton['ctn_id'])->update(
            ['piece_remain' => $remainQty]
        );

        //change business clone new carton
        $origCtnId = $carton['ctn_id'];
        $origLocId = $carton['loc_id'];
        $maxCtnNum = $this->getMaxCtnNum($carton['ctn_num']);
        unset($carton['ctn_id']);

        $carton['inner_pack'] = 0;

        $newCarton = (new Carton())->fill($carton);
        $newCarton->piece_remain = $carton['picked_qty'];
        $newCarton->origin_id = $origCtnId;
        $newCarton->loc_id = $newCarton->loc_code = null;
        $newCarton->ctn_sts = 'PD';
        $newCarton->picked_dt = time();
        $newCarton->ctn_num = ++$maxCtnNum;
        $newCarton->save();

        $return = $newCarton->toArray();
        $return['pick_piece'] = true;
        $return['loc_id'] = $origLocId;
        $return['loc_code'] = $return['loc_name'];
        $return["picked_qty"] = $carton['picked_qty'];

        return $return;
    }

    public function updateCartonByLocs($cartons)
    {
        /**
         * 1. Update full carton
         * 2. Update piece_remain and piece_ttl
         */
        $fullCtns = [0];
        foreach ($cartons as $idx => $carton) {
            if ($carton['pick_piece']) {
                $cartons[$idx] = $this->updatePickedPieceCarton($carton);
            } else {
                $fullCtns[$carton['ctn_id']] = $carton;
            }
        }

        $this->updatePickedFullCartons($fullCtns);

        return $cartons;
    }

    public function getPickCartons($cartons, $pickedQty)
    {
        $pickedcartons = [];
        foreach ($cartons as $carton) {
            //pick cartons
            $carton['pick_piece'] = false;
            if ($pickedQty < $carton['piece_remain']) {
                $carton['pick_piece'] = true;
                $carton['picked_qty'] = $pickedQty;
                $pickedQty = 0;
            } elseif ($pickedQty == $carton['piece_remain']) {
                $carton['picked_qty'] = $carton['piece_remain'];
                $pickedQty = 0;
            } else {
                $pickedQty = $pickedQty - $carton['piece_remain'];
                $carton['picked_qty'] = $carton['piece_remain'];
            }
            $pickedcartons[] = $carton;

            if ($pickedQty <= 0) {
                break;
            }
        }

        return $pickedcartons;
    }

    public function getMaxCtnNum($ctnNum)
    {
        $maxCtnNum = $this->model->where('ctn_num', 'LIKE', $ctnNum . '-%')->max('ctn_num');
        if ($maxCtnNum) {
            return $maxCtnNum;
        }

        return $ctnNum . '-C00';
    }

    public function cloneKittingCartons($kittingOdrDtls) {


        $ctnNum = $this->generateKitCtnNumAndSeq();
        foreach ($kittingOdrDtls as $kittingOdrDtl) {
            $kittingCartons = [];
            for ($i = 0; $i < $kittingOdrDtl['qty']; $i++) {
                $kittingCartons[] = $this->addTime([
                    'item_id'       => array_get($kittingOdrDtl, 'item_id', null),
                    'whs_id'        => $kittingOdrDtl['whs_id'],
                    'cus_id'        => $kittingOdrDtl['cus_id'],
                    'ctn_num'       => $ctnNum,
                    'ctn_sts'       => 'PD',
                    'ctn_uom_id'    => array_get($kittingOdrDtl, 'uom_id', null),
                    'uom_code'      => isset($kittingOdrDtl->item) ?  $kittingOdrDtl->item->uom_code : null,
                    'uom_name'      => isset($kittingOdrDtl->item) ?  $kittingOdrDtl->item->uom_name : null,
                    'ctn_pack_size' => array_get($kittingOdrDtl, 'pack', null),
                    'piece_remain'  => array_get($kittingOdrDtl, 'pack', null),
                    'piece_ttl'     => array_get($kittingOdrDtl, 'pack', null),
                    'gr_dt'         => null,
                    'sku'           => array_get($kittingOdrDtl, 'sku', ''),
                    'size'          => array_get($kittingOdrDtl, 'size', ''),
                    'color'         => array_get($kittingOdrDtl, 'color', ''),
                    'lot'           => array_get($kittingOdrDtl, 'lot', ''),
                    'length'        => isset($kittingOdrDtl->item) ?  $kittingOdrDtl->item->length : null,
                    'width'         => isset($kittingOdrDtl->item) ?  $kittingOdrDtl->item->width : null,
                    'height'        => isset($kittingOdrDtl->item) ?  $kittingOdrDtl->item->height : null,
                    'weight'        => isset($kittingOdrDtl->item) ?  $kittingOdrDtl->item->weight : null,
                    'volume'        => isset($kittingOdrDtl->item) ?  $kittingOdrDtl->item->volume : null,
                    'cube'          => isset($kittingOdrDtl->item) ?  $kittingOdrDtl->item->cube : null,
                    'ucc128'        => $this->generateUCC128(array_get($kittingOdrDtl, 'item_id', null), $kittingOdrDtl['cus_id']),
                    'loc_id'        => null,
                    'loc_code'      => null,
                    'loc_name'      => null,
                    'loc_type_code' => 'RAC',
                ]);
                $ctnNum++;
            }
            $this->refreshModel();
            $this->model->insert($kittingCartons);
        }
    }

    /**
     * @param array $cartonParams
     *
     * @return array
     */
    private function addTime(array $cartonParams)
    {
        $userId = JWTUtil::getPayloadValue('jti') ?: 0;

        $deleted_at = getDefaultDatetimeDeletedAt();
        $created_at = time();
        $time = [
            'created_at' => $created_at,
            'updated_at' => $created_at,
            'deleted_at' => $deleted_at,
            'created_by' => $userId,
            'updated_by' => $userId,
            'deleted'    => 0
        ];

        return array_merge($cartonParams, $time);
    }

    private function generateUCC128($itemId, $cusId)
    {
        return str_pad($itemId, 5, '0', STR_PAD_LEFT) . str_pad($cusId, 8, '0', STR_PAD_LEFT);
    }

    /**
     * Generate KIT Carton number and sequence
     *
     * @param  string $prefix
     *
     * @return string
     */
    private function generateKitCtnNumAndSeq($prefix = 'KIT-CTN')
    {
        $currentYearMonth = date('ym');

        $lastKitCtn = $this->getLatestKitCtnNumber($prefix . '-' . $currentYearMonth);

        if ($lastKitCtn) {
            $kitCtnNumber = ++$lastKitCtn->ctn_num;
        } else {
            $kitCtnNumber = $prefix . '-' . $currentYearMonth . '-00001';
        }

        return $kitCtnNumber;
    }

    /**
     * @return mixed
     */
    public function getLatestKitCtnNumber($prefix)
    {
        return $this->model
            ->where('ctn_num', 'LIKE', $prefix . '-%')
            ->orderBy('ctn_id', 'desc')->take(1)->first();
    }

    public function updatePickingCartons($pickingOdrDtls) {
        foreach ($pickingOdrDtls as $pickingOdrDtl) {
            $this->model->where('item_id', $pickingOdrDtl['item_id'])
                ->where('lot', $pickingOdrDtl['lot'])
                ->where('ctn_sts', 'PD')
                ->where('whs_id', $pickingOdrDtl['whs_id'])
                ->where('cus_id', $pickingOdrDtl['cus_id'])
                ->limit(round($pickingOdrDtl['qty']/$pickingOdrDtl['pack']))
                ->update(['ctn_sts' => 'PB'])
            ;
        }
    }

    public function updatePutBackCartons($kittingItems) {
        foreach ($kittingItems as $kittingItem) {
            $this->model->where('item_id', $kittingItem['item_id'])
                ->where('lot', $kittingItem['lot'])
                ->where('ctn_sts', 'PD')
                ->where('whs_id', $kittingItem['whs_id'])
                ->where('cus_id', $kittingItem['cus_id'])
                ->limit($kittingItem['picked_ctns'])
                ->update([
                    'ctn_sts'   => 'AC',
                    'plt_id'    => $kittingItem['plt_id'],
                    'loc_id'    => $kittingItem['loc_id'],
                    'loc_code'  => $kittingItem['loc_code'],
                    'loc_name'  => $kittingItem['loc_name'],
                ]);

            $palletModel = new PalletModel();
            $palletModel->updateWhere([
                'loc_id'    => $kittingItem['loc_id'],
                'loc_code'  => $kittingItem['loc_code'],
                'loc_name'  => $kittingItem['loc_name'],
            ],[
                'plt_id'    => $kittingItem['plt_id'],
            ]);
        }
    }
}
