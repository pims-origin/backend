<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class DailyPalletRptInsertCorrect extends AbstractCommand
{
    protected $signature   = 'daily-pallet-rpt-insert-correct {whsId} {fromday?} {today?}';
    protected $description = 'Location Histogram successfully.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {
            $this->logSuccess('start-daily-pallet-rpt-insert-correct');

            $fromday     = $this->argument('fromday') ? strtotime($this->argument('fromday')) : false;

            $today     = $this->argument('today') ? strtotime($this->argument('today')) : false;
            if(!$fromday || !$today){
                return;
            }

            $days = $this->dateFromTo($fromday, $today);

            if(count($days) < 1){
                return;
            }

            $customerWhss = DB::table('customer_warehouse AS cw')
                ->select('cw.cus_id', 'cw.whs_id')
                ->where("cw.deleted", 0)
                ->where('cw.whs_id', $this->argument('whsId'))
                ->orderBy('whs_id')
                ->get();

            if ($customerWhss) {
                foreach ($customerWhss as $cusWhs) {
                    $cusId = $cusWhs['cus_id'];
                    $whsId = $cusWhs['whs_id'];
                    foreach ($days as $value) {
                        $this->insertDailyPalletReport($whsId, $cusId, $value['today'], $value['yesterday']);
                    }
                }
            }

            $this->logSuccess('daily-pallet-rpt-insert-correct');

        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }

    }

    private function insertDailyPalletReport($whsId, $cusId, $today, $yesterday)
    {
        return DB::transaction(function () use ($today, $whsId, $yesterday, $cusId) {
            DB::table('daily_plt_rpt as p')
                ->select(
                    'p.whs_id',
                    'p.cus_id',
                    'p.item_id',
                    'p.description',
                    'p.sku',
                    'p.size',
                    'p.color',
                    'p.pack',
                    'p.uom',
                    'p.lot',
                    'p.plt_id',
                    'p.plt_num',
                    'p.loc_id',
                    'p.loc_code',
                    'p.spc_hdl',
                    'p.gr_dt',
                    'p.exp_dt',
                    DB::raw($today . ' as inv_dt'),
                    'p.is_rack',
                    DB::raw('p.cur_ctns as init_ctns'),
                    DB::raw('p.cur_qty as init_qty'),
                    'p.cur_ctns',
                    'p.cur_qty',
                    DB::raw(time() . ' as created_at'),
                    DB::raw(time() . ' as updated_at')
                )
                ->where('p.whs_id', '=', $whsId)
                ->where('p.cus_id', '=', $cusId)
                ->where('p.inv_dt', '=', $yesterday)
                ->chunk(100, function ($pallets) {
                    foreach ($pallets as $pallet) {
                        $status = $this->updateDailyPalletReport($pallet);
                    }

                    return $status ?? false;
                });
        });
    }

    private function updateDailyPalletReport($pallet)
    {
        return DB::table('daily_plt_rpt')
            ->where('whs_id', '=', $pallet['whs_id'])
            ->where('cus_id', '=', $pallet['cus_id'])
            ->where('inv_dt', '=', $pallet['inv_dt'])
            ->where('item_id', '=', $pallet['item_id'])
            ->where('is_rack', '=', $pallet['is_rack'])
            ->where('plt_id', '=', $pallet['plt_id'])
            ->where('lot', '=', $pallet['lot'])
            ->update([
                'init_qty'  => $pallet['init_qty'],
                'init_ctns' => $pallet['init_ctns'],
            ]);
    }

    private function dateFromTo($from, $to)
    {
        while ($from < $to) {
            $arr[] = [
                'today' => $from,
                'yesterday' => $from - 86400
            ];
            $from += 86400;
        }

        return $arr ?? [];
    }
}
