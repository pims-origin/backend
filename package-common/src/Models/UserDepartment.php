<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:49
 */

namespace Seldat\Wms2\Models;

/**
 * @property mixed usr_dpm_id
 * @property mixed usr_dpm_code
 * @property mixed usr_dpm_name
 * @property mixed usr_dpm_des
 * @property mixed created_by
 * @property mixed updated_by
 */
class UserDepartment extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_department';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'usr_dpm_id';

    /**
     * @var array
     */
    protected $fillable = [
        'usr_dpm_id',
        'usr_dpm_code',
        'usr_dpm_name',
        'usr_dpm_des',
        'created_by',
        'updated_by',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany(__NAMESPACE__ . '\User', 'usr_dpm_id', 'usr_dpm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }

}
