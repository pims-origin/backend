<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 05/05/2016
 * Time: 15:21
 */

namespace App\Api\V1\Validators;


class WarehouseValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_name'       => 'required|max:50',
            'whs_status'     => 'exists:whs_status,whs_sts_code',
            'whs_code'       => 'required|max:3',
            'whs_country_id' => 'required|integer',
            'whs_state_id'   => 'required|integer',
            'whs_city_name'   => 'max:20',
        ];
    }
}
