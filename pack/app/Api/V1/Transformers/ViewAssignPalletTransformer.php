<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\PackHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;

/**
 * Class ViewAssignPalletTransformer
 *
 * @package App\Api\V1\Transformers
 */
class ViewAssignPalletTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $packHdr = new PackHdrModel();
        $packs = $packHdr->getPackDtlByPackId_and_CheckSum(object_get($orderHdr, 'odr_id', 0));

        foreach ($packs as  $pack) {
            $pack['items'] = $pack->details()->get(['sku','size', 'color', 'piece_qty', 'lot', 'item_id', 'cus_upc']);
            $pack['assigned'] = 0;
            unset($pack['pack_hdr_id']);
        }
        return [
            'odr_id'   => array_get($orderHdr, 'odr_id', null),
            'odr_num'  => object_get($orderHdr, 'odr_num', null),
            'total'    => $orderHdr->packHdr()->count(),
            'assigned' => 0,
            'cartons'  => $packs

        ];
    }
}
