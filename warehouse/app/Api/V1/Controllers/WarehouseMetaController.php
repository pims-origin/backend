<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\WarehouseMetaModel;
use App\Api\V1\Transformers\WarehouseMetaTransformer;
use App\Api\V1\Transformers\WarehouseConfigurationMetaTransformer;
use App\Api\V1\Validators\WarehouseConfigurationMetaValidator;
use App\Api\V1\Validators\SaveMassMetaValidator;
use App\Api\V1\Validators\WarehouseMetaValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class WarehouseMetaController extends AbstractController
{
    /*
     * @var $warehouseMetaModel
     * */
    protected $warehouseMetaModel;
    /*
     * @var $warehouseMetaTransformer
     * */
    protected $warehouseMetaTransformer;

    /**
     * @var WarehouseConfigurationMetaTransformer
     */
    protected $warehouseConfigurationMetaTransformer;
    /*
     * @var $warehouseMetaValidator
     * */
    protected $warehouseMetaValidator;

    /**
     * @var WarehouseConfigurationMetaValidator
     */
    protected $warehouseConfigurationMetaValidator;
    /*
     * @var $saveMassWarehouseMetaValidator
     * */
    protected $saveMassWarehouseMetaValidator;


    public function __construct()
    {
        parent::__construct();

        $this->warehouseMetaModel = new WarehouseMetaModel();
        $this->warehouseMetaTransformer = new WarehouseMetaTransformer();
        $this->warehouseConfigurationMetaTransformer = new WarehouseConfigurationMetaTransformer();
        $this->warehouseMetaValidator = new WarehouseMetaValidator();
        $this->saveMassWarehouseMetaValidator = new SaveMassMetaValidator();
        $this->warehouseConfigurationMetaValidator = new WarehouseConfigurationMetaValidator();

    }

    public function index($warehouseId)
    {
        $meta = $this->warehouseMetaModel->getByWarehouseId($warehouseId);

        return $this->response->collection($meta, $this->warehouseMetaTransformer);
    }

    public function store($warehouseId, Request $request)
    {
        // get input from request
        $input = $request->getParsedBody();

        $this->warehouseMetaValidator->validate($input);

        $params = [
            'whs_meta_value' => array_get($input, 'whs_meta_value', ''),
            'whs_qualifier'  => $input['whs_qualifier'],
            'whs_id'         => $warehouseId
        ];

        try {
            if ($warehouse = $this->warehouseMetaModel->create($params)) {
                return $this->response->item($warehouse, $this->warehouseMetaTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function show($warehouseId, $qualifier)
    {
        try {
            $warehouseMeta = $this->warehouseMetaModel->getWarehouseMeta($warehouseId, $qualifier);

            return $this->response->item($warehouseMeta, $this->warehouseMetaTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $warehouseId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function storeConfiguration(
        $warehouseId,
        Request $request
    ) {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        // get input from request
        $input = $request->getParsedBody();

        $this->warehouseConfigurationMetaValidator->validate($input);

        try {
            foreach ($input as $item) {
                $metaValue = array_get($item, 'whs_meta_value', false);
                if (is_array($metaValue)) {
                    $metaValue = json_encode($metaValue);
                }

                $qualifier = array_get($item, 'whs_qualifier', false);
                $qualifierList[] = $qualifier;
                $params = [
                    'whs_meta_value' => $metaValue,
                    'whs_qualifier'  => $qualifier,
                    'whs_id'         => $warehouseId,
                    'created_by'     => $userId,
                    'updated_by'     => $userId,
                    'created_at'     => time(),
                    'updated_at'     => time(),
                    'deleted_at'     => '915148800',
                    'deleted'        => '0',

                ];

                $warehouseConfig = $this->warehouseMetaModel->getWarehouseConfigMeta($warehouseId, $qualifier);
                $warehouseQualifier = DB::table('whs_qualifier')
                    ->where('qualifier', $qualifier)
                    ->first();

                if (!$warehouseQualifier) {
                    return $this->response->errorBadRequest("The qualifier description is not existed!");
                }

                if (!$warehouseConfig) {
                    //create
                    DB::table('whs_meta')->insert($params);

                } else {
                    //update
                    $paramsUpdate = [
                        'whs_meta_value' => $metaValue,
                    ];
                    DB::table('whs_meta')
                        ->where('whs_id', $warehouseId)
                        ->where('whs_qualifier', $qualifier)
                        ->update($paramsUpdate);

                }

            }

            $warehouseConfigInfo = $this->warehouseMetaModel->getWarehouseConfigMeta($warehouseId, $qualifierList);
            return $warehouseConfigInfo;

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("The current server is busy, please wait a few minutes and try again"));
    }

    public function searchTimezone(Request $request)
    {
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', 20);
        $timezones = collect(\DateTimeZone::listIdentifiers());

        if (isset($input['search']) && isset($input['search']) != '') {
            $timezones = $timezones->filter(function ($value, $key) use ($input) {
                return stripos($value, $input['search']) !== false;
            });
        }

        return $timezones->take($limit)->values();
    }

    /**
     * @param $warehouseId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function showConfiguration(
        $warehouseId,
        Request $request
    ) {
        $input = $request->getQueryParams();
        $qualifier = $input['whs_qualifier'];
        try {

            $warehouseConfigInfo = $this->warehouseMetaModel->getWarehouseConfigMeta($warehouseId, $qualifier);
            if (!$warehouseConfigInfo) {
                return [];
            }

            return $warehouseConfigInfo;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function update(
        $warehouseId,
        $qualifier,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['whs_qualifier'] = $qualifier;

        // validation
        $this->warehouseMetaValidator->validate($input);

        $whsMetaVal = array_get($input, 'whs_meta_value', 0);
        $params = [
            'whs_meta_value' => ($whsMetaVal) ? 1 : 0,
            'whs_qualifier'  => $qualifier,
            'whs_id'         => $warehouseId
        ];

        try {
            $getWhsMeta = $this->warehouseMetaModel->getWarehouseMeta($warehouseId, $qualifier);

            //check if not existed, insert whs - meta
            if (empty($getWhsMeta)) {
                if ($warehouse = $this->warehouseMetaModel->create($params)) {
                    return $this->response->item($warehouse, $this->warehouseMetaTransformer)
                        ->setStatusCode(IlluminateResponse::HTTP_CREATED);
                }
            }

            //else update
            if ($warehouseMeta = $this->warehouseMetaModel->update($params)) {
                return $this->response->item($warehouseMeta, $this->warehouseMetaTransformer);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function destroy($warehouseId, $qualifier)
    {
        try {
            $this->warehouseMetaModel->deleteWarehouseMeta($warehouseId, $qualifier);

            return $this->response->noContent();

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function saveMassAndDeleteUnused(
        $warehouseId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->saveMassWarehouseMetaValidator->validate($input);

        $meta = $this->warehouseMetaModel->getByWarehouseId($warehouseId);

        $meta = array_pluck($meta, 'whs_meta_value', 'whs_qualifier');

        foreach ($input['whs_meta_value'] as $key => $val) {
            if (isset($meta[$input['whs_qualifier'][$key]])) {
                $action = 'updateWithoutReturnedModel';
                unset($meta[$input['whs_qualifier'][$key]]);
            } else {
                $action = 'create';
            }

            $params = [
                'whs_meta_value' => $val,
                'whs_qualifier'  => $input['whs_qualifier'][$key],
                'whs_id'         => $warehouseId
            ];

            try {
                $this->warehouseMetaModel->refreshModel();
                $this->warehouseMetaModel->{$action}($params);
            } catch (\PDOException $e) {
                return $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }

        /**
         * delete unused meta
         */
        if ($meta) {
            try {
                $this->warehouseMetaModel->refreshModel();
                $this->warehouseMetaModel->deleteWarehouseMeta($warehouseId, array_keys($meta));
            } catch (\PDOException $e) {
                return $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }

        return $this->response->noContent();
    }
}