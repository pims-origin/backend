<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V5\Inbound\Pallet\ScanPallet\Models;


use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class LocationModel extends AbstractModel
{
    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    /**
     * @param $cus_id
     * @param $whs_id
     * @param array $exceptId
     *
     * @return mixed
     */
    public function loadSuggestedLocation($cus_id, $whs_id, $exceptId = [], $type)
    {
        $query = $this->model->select(['loc_id', 'loc_code', 'loc_alternative_name'])
            ->whereHas('customerZone', function ($q) use ($cus_id) {
                $q->where('cus_id', $cus_id);
            })
            ->whereHas('locationType', function ($q) use ($type) {
                $q->where('loc_type_code', $type);
            })
            ->where('loc_sts_code', config('constants.location_status.ACTIVE'))
            ->where('loc_whs_id', $whs_id);

        if (!empty($exceptId)) {
            $query->whereNotIn('loc_id', $exceptId);
        }

        return $query->first();
    }

    /**
     * @param array $loc_id
     */
    public function countLocationNotHasStatus(array $loc_id, $sts_code)
    {

        return $this->model->where('loc_sts_code', '<>', $sts_code)->whereIn('loc_id', $loc_id)->count();
    }

    /**
     * @param array $loc_id
     */
    public function updateLocStatusWhereIn(array $loc_id, $sts_code)
    {
        return $this->model->whereIn('loc_id', $loc_id)->update(['loc_sts_code' => $sts_code]);
    }

    /**
     * @param $rfid
     * @param $whsId
     * @param bool $locType
     *
     * @deprecated  No longer in use
     * @return mixed
     */
    public function getLocationByRfId($rfid, $whsId, $locType = false)
    {
        $query = $this->model;
        if ($locType) {
            $query = $query->select(
                ['loc_id', 'loc_sts_code', 'loc_code', 'loc_alternative_name', 'loc_type.loc_type_code']
            )
                ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                ->where('rfid', $rfid)
                ->where('loc_whs_id', $whsId);

        } else {
            $query = $query->select(['loc_id', 'loc_sts_code', 'loc_code', 'loc_alternative_name'])
                ->where('rfid', $rfid)
                ->where('loc_whs_id', $whsId);
        }

        return $query->first();
    }

    public function getLocByRfId($whsId, $rfid)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        return $this->model->select(['loc_id', 'loc_sts_code', 'loc_code', 'loc_alternative_name'])
            ->where('rfid', $rfid)
            ->where('loc_whs_id', $whsId)
            ->first();
    }

    public function getEmptyLocationByCusId($whsId, $cusId, $locType = 'RAC')
    {
        $data = $this->model->select(['loc_id', 'loc_sts_code', 'loc_code', 'loc_alternative_name'])
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->join('customer_zone', 'zone.zone_id', '=', 'customer_zone.zone_id')
            ->where('location.loc_whs_id', $whsId)
            ->where('zone.zone_whs_id', $whsId)
            ->where('customer_zone.cus_id', $cusId)
            ->where('customer_zone.deleted', 0)
            ->where('loc_type.loc_type_code', $locType)
            ->where('location.loc_sts_code', 'AC')
            ->whereNotNull('location.rfid')
            ->where('location.rfid', '<>', '')
            ->whereRaw('(Select count(pallet.plt_id) from pallet where pallet.loc_id = location.loc_id) = 0')
            ->where(function ($query) {
                $query->where('loc_code', 'like', '%-%-%-%')
                    ->orWhere('loc_code', 'like', '%-%-%-%-%');
            })
            ->first();

        return $data;
    }

    public function getLocationByLocCode($locCode, $whsId)
    {
        $query = $this->model;

        $query = $query->select(
            ['loc_id', 'loc_sts_code', 'loc_code', 'loc_alternative_name', 'loc_type.loc_type_code']
        )
            ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->where('loc_code', $locCode)
            ->where('loc_whs_id', $whsId);

        return $query->first();
    }

    /**
     * @param $input
     * @param $whsId
     *
     * @return mixed
     * @author:cuongnguyen
     */
    public function getLocationStatusByLocCode($input, $whsId)
    {
        $locCode = array_get($input, 'loc_code', null);
        $locRfid = array_get($input, 'loc_rfid', null);

        $query = $this->model;

        $query = $query->select(
            ['loc_id', 'loc_sts_code', 'loc_code', 'loc_alternative_name', 'loc_type.loc_type_code']
        )
            ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->where('loc_whs_id', $whsId);

        if ($locCode) {
            $query = $query->where('loc_code', $locCode);
        }

        if ($locRfid) {
            $query = $query->where('rfid', $locRfid);
        }

        return $query->first();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchAllLocation($attributes = [], $with = [], $limit = null)
    {
        $pallets = (new PalletModel())->loadAllLoc()->toArray();
        $exceptLocIds = array_pluck($pallets, 'loc_id');

        $query = $this->make($with);

        if (!empty($attributes)) {
            $query->whereHas("locationType", function ($q) use ($attributes) {
                $q->where('loc_type_code', 'like', $attributes['loc_type']);
            });
        }

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        $query->where(function ($q) use ($exceptLocIds) {
            // Status = AC
            $q->orWhere(function ($q2) use ($exceptLocIds) {
                $q2->where('loc_sts_code', 'AC');
                $q2->whereNotIn('loc_id', $exceptLocIds);
            });

            $q->orWhere(function ($q2) use ($exceptLocIds) {
                $q2->where('loc_sts_code', 'RS');
                $q2->whereIn('loc_id', $exceptLocIds);
            });
        });

        $query->whereHas("zone", function ($q) use ($attributes) {
            $q->orderBy('zone_name', 'DESC');
        });

        $query->whereHas("customerZone", function ($q) use ($attributes) {
            $q->orderBy('cus_id', array_get($attributes, 'cusId', null));
        });

        $query->where('loc_whs_id', array_get($attributes, 'whsId', null));

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param array $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function suggestEmptyLocation($attributes = [], $with = [])
    {
        $allExcept = (new PalletModel())->loadLocNotEmptyPallet()->toArray();
        $allExcept = array_pluck($allExcept, 'loc_id');

        $query = $this->make($with);

        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->where('loc_whs_id', array_get($attributes, 'whs_id', ''));
        $query->where('loc_sts_code', config('constants.location_status.ACTIVE'));
        $query->whereNotIn('loc_id', $allExcept);
        if (array_get($attributes, 'type', '') == "PAW") {
            $query->where('loc_code', 'like', '%PAW%');
        } else {
            $query->where('loc_code', 'like', '%RAC%');
        }

        /*$query->whereHas("customerZone", function ($query) use ($attributes) {
            $query->where('cus_id', array_get($attributes, 'cus_id', ''));
        });*/

        $query->whereHas("locationType", function ($query) use ($attributes) {
            $query->where('loc_type_code', array_get($attributes, 'type', ''));
        });

        // Get
        $models = $query->first();

        return $models;
    }

    public function suggestAnEmptyPutAwayLocation($whsId)
    {
        $query = $this->model
            ->select('location.loc_id', 'location.loc_code', 'location.loc_alternative_name', 'location.loc_whs_id',
                'location.rfid')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->leftJoin('vtl_ctn', 'location.loc_id', '=', 'vtl_ctn.loc_id')
            ->where('location.loc_whs_id', $whsId)
            ->where('loc_type.loc_type_code', Status::getByValue('PUT-AWAY', 'LOC_TYPE_CODE'))
            ->whereNull('pallet.loc_id')
            ->whereNull('vtl_ctn.loc_id')
            ->whereNotNull('location.rfid')
            ->first();

        return $query;
    }

    public function getEmptyRackLocation($params = [])
    {
        $query = $this->model
            ->select('location.loc_id', 'location.loc_code', 'location.loc_alternative_name', 'location.loc_whs_id',
                'location.rfid')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->join('customer_zone', 'location.loc_zone_id', '=', 'customer_zone.zone_id')
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->leftJoin('vtl_ctn', 'location.loc_id', '=', 'vtl_ctn.loc_id')
            ->where('location.loc_whs_id', $params['whs_id'])
            ->where('customer_zone.cus_id', $params['cus_id'])
            ->where('loc_type.loc_type_code', Status::getByValue('RACK', 'LOC_TYPE_CODE'))
            ->whereNotNull('location.rfid')
            ->whereNull('vtl_ctn.loc_id')
            ->whereNull('pallet.loc_id')
            ->first();

        return $query;
    }

    public function getAilseRackLocations($params = [])
    {
        $suggestLoc = $params['sug_loc'];
        $cols = explode('-', $suggestLoc);
        $ailse = array_shift($cols);


        $query = $this->model
            ->select('location.loc_id', 'location.loc_code',
                'location.rfid', 'customer_zone.cus_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin('customer_zone', 'location.loc_zone_id', '=', 'customer_zone.zone_id')
            ->where('location.loc_whs_id', $params['whs_id'])
            ->where('loc_type.loc_type_code', Status::getByValue('RACK', 'LOC_TYPE_CODE'))
            ->where('location.loc_code', 'LIKE', "$ailse-%")
            ->orderBy('location.loc_code')
            ->get();

        return $query->toArray();
    }

    public function getLocations($whs_id, $loc_ids)
    {


        $query = $this->model
            ->select('location.loc_id', 'location.loc_code', 'location.loc_alternative_name',
                'location.rfid', 'customer_zone.cus_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin('customer_zone', 'location.loc_zone_id', '=', 'customer_zone.zone_id')
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->leftJoin('vtl_ctn', 'location.loc_id', '=', 'vtl_ctn.loc_id')
            ->where('location.loc_whs_id', $whs_id)
            ->where('loc_type.loc_type_code', Status::getByValue('RACK', 'LOC_TYPE_CODE'))
            ->whereIn('location.loc_id', $loc_ids)
            ->orderBy('location.loc_code')
            ->groupBy('location.loc_id')
            ->get();

        return $query->toArray();
    }

    public function getPalletLocPick($params)
    {
        $query = $this->model
            ->select('location.loc_id', 'location.loc_code', 'location.loc_alternative_name', 'location.loc_whs_id',
                'location.rfid as loc_rfid', 'pallet.rfid as plt_rfid', 'plt_num', 'pallet.plt_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->join('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->where('location.loc_whs_id', $params['whs_id'])
            //->where('location.loc_type_id', 5);;
            ->where('loc_type.loc_type_code', Status::getByValue('PICKING', 'LOC_TYPE_CODE'));

        return $query->get();
    }

    /**
     * @param $attributes
     * @param null $type
     * @param array $with
     *
     * @return mixed
     */
    public function getLocWithType($attributes, $type = null, $with = [])
    {
        $query = $this->make($with);
        if (!empty($type)) {
            $query->whereHas("locationType", function ($q) use ($type) {
                $q->where('loc_type_code', $type);
            });
        }

        if (!empty($attributes['loc_alternative_name'])) {
            $query->where('loc_alternative_name', $attributes['loc_alternative_name']);
        }

        if (!empty($attributes['rfid'])) {
            $query->where('rfid', $attributes['rfid']);
        }

        if (!empty($attributes['loc_whs_id'])) {
            $query->where('loc_whs_id', $attributes['loc_whs_id']);
        }

        $model = $query->first();

        return $model;
    }

    public function getRackLocation($whsId, $rfid)
    {
        $model = $this->getModel()->where(
            [
                'loc_whs_id' => $whsId,
                'rfid'       => $rfid,
                //'loc_sts_code' =>  'AC'
            ]

        )
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('loc_type.loc_type_code', 'RAC')
            ->first();

        return $model;
    }

    public function getRackLocationByCustomer($whsId, $rfid, $cusId)
    {
        $model = $this->getModel()->where([
            'loc_whs_id' => $whsId,
            'rfid'       => $rfid,
        ])
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('loc_type.loc_type_code', 'RAC')
            ->where('customer_zone.cus_id', $cusId)
            ->first();

        return $model;
    }

    /**
     * @param $whsId
     * @param $rfid
     *
     * @return mixed
     */
    public function getRackLocationByLocRFID($whsId, $rfid)
    {
        $model = $this->getModel()->where([
            'loc_whs_id' => $whsId,
            'rfid'       => $rfid,
        ])
            ->first();

        return $model;
    }

    public function readWhsLayout($whsId, $zone, $with = [], $sortBy = "ASC")
    {
        $query = $this->make($with)
            ->where('loc_whs_id', $whsId)
            ->where('loc_alternative_name', 'like', SelStr::escapeLike($zone) . "-%")
            ->orderBy('loc_alternative_name', $sortBy);
        // Get
        $models = $query->get();

        return $models;
    }

    public function readNewWhsLayout($whsId, $locIds, $with = [], $sortBy = "ASC")
    {
        $query = $this->make($with)
            ->where('loc_whs_id', $whsId)
            ->whereIn('loc_id', $locIds)
            ->orderBy('loc_alternative_name', $sortBy);
        // Get
        $models = $query->get();

        return $models;
    }


    public function getByLocCode($locCode, $whsId)
    {
        if (!is_array($locCode)) {
            $locCode = [$locCode];
        }

        return $this->model
            ->select(['loc_id', 'loc_code'])
            ->whereIn('loc_code', $locCode)
            ->where('loc_whs_id', $whsId)
            ->get();
    }

    public function getLocShipLane($whsId, $loc_rfid)
    {
        $query = $this->model
            ->select('location.loc_id', 'location.loc_code', 'location.loc_alternative_name', 'location.loc_whs_id',
                'location.rfid')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            //->where('location.rfid', $loc_rfid)
            ->where('location.loc_whs_id', $whsId)
            ->where('loc_type.loc_type_code', Status::getByValue('SHIPPING', 'LOC_TYPE_CODE'))
            ->whereNull('pallet.loc_id');
        //->whereNotIn('loc_id', DB::table('pallet')->select( DB::raw('COALESCE(loc_id, -1)') ));
        if ($loc_rfid) {
            $query = $query->where('location.rfid', $loc_rfid);
        }

        return $query->get();
    }

    public function getLocShipping($data, $whsId)
    {
        $query = $this->model
            ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->where('loc_type', Status::getByValue('SHIPPING', 'LOC_TYPE_CODE'))
            ->where('loc_cde', $data['loc_code'])
            ->where('loc_whs_id', $whsId);
        // Get
        $models = $query->get();

        return $models;
    }

    /**
     * @param $attributes
     * @param null $type
     * @param array $with
     *
     * @return mixed
     */
    public function getLocShippingLane($whsId, $loc_rfid, $with = [])
    {
        $query = $this->make($with);
        $query = $this->model
            ->select('location.loc_id', 'location.loc_code', 'location.loc_alternative_name', 'location.loc_whs_id',
                'location.rfid')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('location.loc_whs_id', $whsId)
            ->where('location.rfid', $loc_rfid)
            ->where('loc_type.loc_type_code', Status::getByValue('SHIPPING', 'LOC_TYPE_CODE'))
            ->whereNotIn('loc_id', DB::table('out_pallet')->select(DB::raw('COALESCE(loc_id, -1)')));

        $model = $query->first();

        return $model;
    }

    public function getLocRFIDByLocId($locId)
    {
        return $this->model
            ->select('rfid')
            ->where('loc_id', $locId)
            ->first();
    }

    public function sortLocationByLevel($locs)
    {
        $slocs = [];
        foreach ($locs as $loc) {

        }
    }

    public function findSuggestLocationPosition($locs, $sugLoc)
    {

        foreach ($locs as $index => $loc) {
            if ($loc['loc_code'] == $sugLoc) {
                return $index;
            }
        }
    }

    public function getCustomerIdsByLocIds($locIds)
    {
        return $this->model
            ->select('customer_zone.cus_id', 'location.loc_id')
            ->join('customer_zone', 'location.loc_zone_id', '=', 'customer_zone.zone_id')
            ->whereIn('loc_id', $locIds)
            ->get();
    }

    public function getLocByLocCode($locCode, $whsId)
    {
        return $this->model
            ->where('loc_code', $locCode)
            ->where('loc_whs_id', $whsId)
            ->where('deleted', 0)
            ->first();
    }

    public function getLocationList($locTypeId, $limit = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->model
            ->select(['loc_id', 'loc_code', 'rfid', 'location.loc_type_id', 'loc_type_name', 'loc_type_code'])
            ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id');

        if ($locTypeId) {
            $query = $query->where('location.loc_type_id', $locTypeId);
        }

        $models = $query->paginate($limit);

        return $models;
    }

    public function updateLocationSts($asn_hdr_id, $ctnrId)
    {
        return $this->model
            ->whereRaw("EXISTS(SELECT vtl_ctn.loc_id FROM vtl_ctn
                WHERE location.loc_id = vtl_ctn.loc_id
                    AND vtl_ctn.ctnr_id = $ctnrId
                    AND vtl_ctn.asn_hdr_id = $asn_hdr_id)")
            ->where('location.loc_sts_code', 'LK')
            ->update(['location.loc_sts_code' => 'AC']);
    }
}
