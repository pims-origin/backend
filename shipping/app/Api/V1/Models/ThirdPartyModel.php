<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ThirdParty;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ThirdPartyModel extends AbstractModel
{
    /**
     * @param ThirdParty $model
     */
    public function __construct()
    {
        $this->model = new ThirdParty();
    }

    /**
     * @return mixed
     */
    public function search($cusId, $attributes = [], $limit = 20)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query = $this->model
            ->select([
                'tp_id',
                'cus_id',
                'type',
                'name',
                'des',
                'phone',
                'mobile',
                'ct_first_name',
                'ct_last_name',
                'add_1',
                'add_2',
                'city',
                'state',
                'zip',
                'country',
                'created_at',
                'updated_at',
                'deleted_at'
            ])
            ->where('cus_id', $cusId)
            ->where('deleted', 0);
            
        if (isset($attributes['name'])) {
            $query->where('name', 'LIKE', "%" . SelStr::escapeLike($attributes['name']) . "%");
        }

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }
}
