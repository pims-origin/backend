<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:34
 */

namespace Seldat\Wms2\Models;

/**
 * @property mixed whs_add_line_1
 * @property mixed whs_add_line_2
 * @property mixed whs_add_country_code
 * @property mixed whs_add_city_name
 * @property mixed whs_add_state_code
 * @property mixed whs_add_postal_code
 * @property mixed whs_add_whs_id
 * @property mixed whs_add_type
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 *
 */
class WarehouseAddress extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whs_address';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'whs_add_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_add_line_1',
        'whs_add_line_2',
        'whs_add_city_name',
        'whs_add_state_id',
        'whs_add_postal_code',
        'whs_add_whs_id',
        'whs_add_type',
    ];

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_add_whs_id', 'whs_id');
    }

    public function systemState()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemState', 'sys_state_id', 'whs_add_state_id');
    }
}
