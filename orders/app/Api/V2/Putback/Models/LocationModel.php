<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\Putback\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\CustomerZone;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Zone;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

/**
 * Class LocationModel
 *
 * @package App\Api\V1\Models
 */
class LocationModel extends AbstractModel
{
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    public function suggestLocation($cusId, $whsId, $spcHdl, $locCode = null)
    {
        $dynZone = $this->chkDynZone($whsId, $cusId);

        $zoneIds = CustomerZone::select('zone_id')
            ->where('cus_id', $cusId)
            ->get()
            ->toArray();

        $inZoneIds = array_pluck($zoneIds, 'zone_id');

        $dataSuggest = [];
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        foreach ($spcHdl as $spcPlt => $limit) {
            list($pltType, $spcHdlCode) = explode('-', $spcPlt);

            $query = $this->getModel()
                ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
                ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                ->select([
                    'location.loc_id',
                    'location.loc_code',
                    DB::raw("'$spcPlt' AS plt_type_spc_hdl")
                ])
                ->where([
                    'location.loc_sts_code'  => 'AC',
                    'location.loc_whs_id'    => $whsId,
                    'location.spc_hdl_code'  => $spcHdlCode,
                    'location.plt_type'      => $pltType,
                    'pallet.loc_id'          => null,
                    'loc_type.loc_type_code' => 'RAC'
                ])
                ->where(function ($q) use ($inZoneIds, $dynZone) {
                    $q->whereIn('location.loc_zone_id', $inZoneIds);
                    if ($dynZone) {
                        $q->orWhereNull('location.loc_zone_id');
                    }

                    return $q;
                })
                ->limit($limit);

            if ($locCode) {
                $query->where('location.loc_code', 'LIKE', $locCode . "%");
            }

            $rs = $query->get();
            if ($rs) {
                $dataSuggest = array_merge($dataSuggest, $rs->toArray());
            }
        }

        return $dataSuggest;
    }


    public function chkDynZone($whsId, $cusId)
    {
        $chkDynZone = DB::table('cus_config')->where([
            "whs_id"      => $whsId,
            "cus_id"      => $cusId,
            "config_name" => "DNZ",
            "ac"          => "Y",
        ])->value('config_value');

        if (empty($chkDynZone)) {
            return false;
        }

        return true;
    }

    public function getDynZone($whsId, $cusId)
    {
        $chkDynZone = DB::table('cus_config')->where([
            "whs_id"      => $whsId,
            "cus_id"      => $cusId,
            "config_name" => "DNZ",
            "ac"          => "Y",
        ])->value('config_value');

        if (empty($chkDynZone)) {
            return false;
        }

        $customerObj = Customer::where('cus_id', $cusId)->first();

        $zoneCode = $customerObj->cus_code . "-D";
        $zoneName = $customerObj->cus_code . " - Dynamic Zone";
        $zoneObj = Zone::where('zone_code', $zoneCode)->first();

        if ($zoneObj) {
            $zoneObj->zone_num_of_loc += 1;
            $zoneObj->save();

            return $zoneObj->zone_id;
        }

        $colorCode = DB::table('customer_color')->where('cus_id', $cusId)
            ->value('cl_code');

        $arrData = [
            "zone_name"       => $zoneName,
            "zone_code"       => $zoneCode,
            "zone_whs_id"     => $whsId,
            "zone_type_id"    => 5,
            "dynamic"         => 1,
            "zone_min_count"  => 1,
            "zone_max_count"  => 100,
            "zone_color"      => $colorCode ?? '#ffffff',
            "zone_num_of_loc" => 1,
            "created_at"      => time(),
            "updated_at"      => time(),
            "created_by"      => Data::getCurrentUserId(),
            "updated_by"      => Data::getCurrentUserId(),
            "deleted_at"      => 915148800,
            "deleted"         => 0
        ];

        $zoneId = DB::table('zone')->insertGetId($arrData);

        //add customer_zone
        DB::table('customer_zone')->insert([
            'zone_id'    => $zoneId,
            'cus_id'     => $cusId,
            'created_at' => time(),
            'updated_at' => time(),
            "created_by" => Data::getCurrentUserId(),
            "updated_by" => Data::getCurrentUserId(),
            "deleted_at" => 915148800,
            'deleted'    => 0
        ]);

        return $zoneId;
    }

    public function validateLocation($items)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $locs = DB::table('location')
            ->whereIn('loc_id', array_column($items, 'loc_id'))
            ->where('loc_whs_id', Data::getCurrentWhsId())
            ->get([
                'loc_id',
                'spc_hdl_code',
                'plt_type',
                'loc_sts_code',
                'loc_zone_id'
            ]);

        if (empty($locs)) {
            throw new HttpException(400, "Locations are invalid");
        }

        $dynZoneId = $this->getDynZone(Data::getCurrentWhsId(), $items[0]['cus_id']);

        $locs = array_pluck($locs, null, 'loc_id');
        $errors = [];
        foreach ($items as $idx => $item) {
            if ($locs[$item['loc_id']]['loc_sts_code'] !== 'AC') {
                throw new HttpException(400, "Location " . $item['loc_code'] . " is not active");
            }

            if ($locs[$item['loc_id']]['spc_hdl_code'] !== $item['spc_hdl_code']) {
                throw new HttpException(400, "Location's temperature doesn't match: " . $item['loc_code']);
            }

            if ($locs[$item['loc_id']]['plt_type'] !== $item['plt_type']) {
                throw new HttpException(400, "Location's pallet type doesn't match: " . $item['loc_code']);
            }

            if (empty($locs[$item['loc_id']]['loc_zone_id'])) {
                if (!$dynZoneId) {
                    throw new HttpException(400, "Location " . $item['loc_code'] . " don't belong customer");
                }
                $items[$idx]['dynamic_zone_id'] = $dynZoneId;
            }
        }

        return $items;
    }
}
