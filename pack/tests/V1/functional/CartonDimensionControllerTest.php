<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class CartonDimensionControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function getEndPoint()
    {
        return "/v1/carton-dimensions/";
    }

    public function test_Show_Ok()
    {
        // Test Show
        $data = $this->call('GET', $this->getEndPoint());

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);

    }
}
