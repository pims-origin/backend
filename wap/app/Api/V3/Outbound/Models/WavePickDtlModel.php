<?php

namespace App\Api\V3\Outbound\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Status;

class WavePickDtlModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
    }

    public function updateWvDtlPickCtn($data)
    {
        return $this->model->where('wv_dtl_id', $data['wv_dtl_id'])
            ->update([
                'act_piece_qty' => $data['act_piece_qty'],
                'wv_dtl_sts'    => $data['wv_dtl_sts'],
            ]);
    }

    public function getNextWvDtl($wvId, $currentWvDtlID, $whsId = false)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $wvdtls = $this->model
            ->select(
                'wv_dtl_id', 'whs_id', 'wv_id', 'wv_num', 'wv_dtl_sts', 'act_piece_qty', 'piece_qty', 'lot', 'item_id', 'cus_id', 'pack_size', 'sku', 'size', 'color'
            )
            ->where('wv_id', $wvId)
            ->where('wv_dtl_id', '!=', $currentWvDtlID)
            ->whereIn('wv_dtl_sts', ['NW', 'PK']);
        if ($whsId) {
            $wvdtls->where('whs_id', $whsId);
        }

        $wvdtls = $wvdtls->get();

        $result = null ;
        $wvdtls->each(function($wvdtl, $index) use ($wvdtls, $currentWvDtlID, &$result){
            if ($wvdtl->wv_dtl_id > $currentWvDtlID && !$result){
                return $result = $wvdtl;
            }

            if (count($wvdtls) == $index + 1 && !$result) {
                return $result = $wvdtls->first();
            }
        });

        return $result;
    }

    public function getProperlyWvDtl($wvId, $whsId)
    {
        return $this->model
            ->where([
                'wv_dtl.wv_id'  => $wvId,
                'wv_dtl.whs_id' => $whsId,
            ])
            ->join('wv_hdr', 'wv_hdr.wv_id', '=', 'wv_dtl.wv_id')
            ->where('wv_hdr.deleted', 0)
            ->whereIn('wv_hdr.wv_sts'    , ['PK', 'NW'])
            ->whereIn('wv_dtl.wv_dtl_sts', ['PK', 'NW', 'PD'])
            ->get();
    }

    public function getProperlyWvDtlInReturn($wvId, $whsId)
    {
        return $this->model
            ->where([
                'wv_dtl.wv_id'  => $wvId,
                'wv_dtl.whs_id' => $whsId,
            ])
            ->join('wv_hdr', 'wv_hdr.wv_id', '=', 'wv_dtl.wv_id')
            ->where('wv_hdr.deleted', 0)
            // ->whereIn('wv_hdr.wv_sts'    , ['PK', 'NW', 'RT'])
            // ->whereIn('wv_dtl.wv_dtl_sts', ['PK', 'NW', 'PD', 'CC'])
            ->get();
    }

    /**
     * WMS2-4274: [Outbound][Update wave pick] - No message display when scanning over expected carton for the order
     * Get detail total cartons for every wavepick detail
     *
     * @param Integer $whsId
     * @param Integer $wvHdrId
     *
     * @return Collection
     */
    public function getDetailTotalCartons($whsId, $wvHdrId) {
        $query = $this->model
                ->select([
                    //'wv_hdr.wv_id',
                    'wv_dtl.wv_dtl_id',
                    DB::raw('COUNT(odr_cartons.odr_ctn_id) AS total_carton')
                ])
                // ->join('wv_hdr', 'wv_hdr.wv_id', '=', 'wv_dtl.wv_id')
                ->join('odr_cartons', 'wv_dtl.wv_dtl_id', '=', 'odr_cartons.wv_dtl_id')
                ->where('wv_dtl.wv_id', $wvHdrId)
                ->where('wv_dtl.whs_id', $whsId)
                ->where('odr_cartons.deleted', 0)
                ->groupBy('wv_dtl.wv_dtl_id');

        return $query->get();
    }

}
