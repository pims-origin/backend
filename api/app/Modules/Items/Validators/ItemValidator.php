<?php

namespace App\Modules\Items\Validators;

use Validator;

class ItemValidator
{
    public function rules($cusId)
    {
        return [
            'sku'          => 'required|max:30|unique:item,sku',
            'size'         => 'nullable|max:30',
            'color'        => 'nullable|max:30',
            'description'  => 'nullable|max:50',
            'uom'          => 'required|in:ct,pc',
            'pack'         => 'required|integer|min:1',
            'upc'          => 'nullable|digits_between:12,14|unique:item,cus_upc,NULL,item_id,cus_id,' . $cusId,
            'length'       => 'nullable|numeric|between:0,99999999.999',
            'width'        => 'nullable|numeric|between:0,99999999.999',
            'height'       => 'nullable|numeric|between:0,99999999.999',
            'weight'       => 'nullable|numeric|between:0,99999999.999',
            'volume'       => 'nullable',
            'category'     => 'max:30'
        ];
    }

    public function validate($input, $cusId)
    {
        $input['uom'] = strtolower(array_get($input, 'uom', 'CT'));
        $validator = Validator::make($input, $this->rules($cusId), $this->messages());

        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                }
            }
        }

        return $validator;
    }

    public function messages()
    {
        return [
            'sku.required'          => 'SKU is required',
            'sku.max'               => 'SKU max length is 30',
            'sku.unique'            => 'SKU is unique',
            'size.max'              => 'Size max length is 30',
            'color.max'             => 'Color max length is 30',
            'description.max'       => 'Description max length is 50',
            'uom.required'          => 'Uom is required',
            'uom.in'                => 'Uom must be belong to in (Carton(CT), Piece(PC)).',
            'pack.required'         => 'Pack is required',
            'pack.integer'          => 'Pack must be integer type',
            'pack.min'              => 'Pack must be greater than 0',
            'upc.unique'            => 'Upc is unique',
            'upc.digits_between'    => 'Upc must be integer type and min length is 12 and max length is 14',
            'length.required'       => 'Length is required',
            'length.numeric'        => 'Length must be numerice type',
            'length.between'        => 'Length < 1 bilion',
            'width.required'        => 'Width is required',
            'width.numeric'         => 'Width must be numerice type',
            'width.between'         => 'Width < 1 bilion',
            'height.required'       => 'Height is required',
            'height.numeric'        => 'Height must be numerice type',
            'height.between'        => 'Height < 1 bilion',
            'weight.required'       => 'Net weight is required',
            'weight.numeric'        => 'Net weight must be numerice type',
            'weight.between'        => 'Net weight < 1 bilion',
            'category.max'          => 'Category max length is 30'
        ];
    }

    public function checkItem($item)
    {
        if(empty($item))
        {
            throw new \Exception(json_encode(['title' =>'Item is not existed.', 'detail' => 'Please try']));
        }
    }

    public function checkCreateItem($item)
    {
        if(empty($item))
        {
            throw new \Exception(json_encode(['title' =>'Create item unsuccessfully', 'detail' => 'Please try']));
        }
    }

    public function checkUpdateItem($item)
    {
        if(empty($item))
        {
            throw new \Exception(json_encode(['title' =>'Update item unsuccessfully', 'detail' => 'Please try']));
        }
    }
}