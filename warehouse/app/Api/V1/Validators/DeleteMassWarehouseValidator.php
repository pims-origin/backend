<?php

namespace App\Api\V1\Validators;

class DeleteMassWarehouseValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id'   => 'required',
            'whs_id.*' => 'required|exists:warehouse,whs_id'
        ];
    }
}