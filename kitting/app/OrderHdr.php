<?php
namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

/**
 * Class OrderHdr
 *
 * @property mixed ship_id
 * @property mixed cus_id
 * @property mixed whs_id
 * @property mixed so_id
 * @property mixed cus_odr_num
 * @property mixed odr_num
 * @property mixed ref_cod
 * @property mixed cus_po
 * @property mixed odr_type
 * @property mixed odr_sts
 * @property mixed ship_to_name
 * @property mixed ship_to_add_1
 * @property mixed ship_to_add_2
 * @property mixed ship_to_city
 * @property mixed ship_to_state
 * @property mixed ship_to_zip
 * @property mixed ship_to_country
 * @property mixed carrier
 * @property mixed odr_req_dt
 * @property mixed act_cmpl_dt
 * @property mixed req_cmpl_dt
 * @property mixed ship_by_dt
 * @property mixed cancel_by_dt
 * @property mixed act_cancel_dt
 * @property mixed ship_after_dt
 * @property mixed shipped_dt
 * @property mixed cus_pick_num
 * @property mixed ship_method
 * @property mixed cus_dept_ref
 * @property mixed csr
 * @property mixed rush_odr
 * @property mixed cus_notes
 * @property mixed in_notes
 * @property mixed hold_sts
 * @property mixed back_odr_seq
 * @property mixed back_odr
 * @property mixed org_odr_id
 * @property mixed sku_ttl
 * @property mixed audit_sts
 * @property mixed wv_num
 *
 * @package Seldat\Wms2\Models
 */
class OrderHdr extends BaseInvoiceModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'odr_hdr';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_id';
    /**
     * @var array
     */
    protected $fillable = [
        'ship_id',
        'cus_id',
        'whs_id',
        'so_id',
        'odr_num',
        'cus_odr_num',
        'ref_cod',
        'cus_po',
        'odr_type',
        'odr_sts',
        'ship_to_name',
        'ship_to_add_1',
        'ship_to_add_2',
        'ship_to_city',
        'ship_to_state',
        'ship_to_zip',
        'ship_to_country',
        'carrier',
        'odr_req_dt',
        'act_cmpl_dt',
        'req_cmpl_dt',
        'ship_by_dt',
        'cancel_by_dt',
        'act_cancel_dt',
        'ship_after_dt',
        'shipped_dt',
        'cus_pick_num',
        'ship_method',
        'cus_dept_ref',
        'csr',
        'rush_odr',
        'cus_notes',
        'in_notes',
        'hold_sts',
        'back_odr_seq',
        'back_odr',
        'org_odr_id',
        'sku_ttl',
        'audit_sts',
        'wv_num',
        'is_ecom',
        'dlvy_srvc',
        'dlvy_srvc_code',
        'cosignee_sig',
        'carrier_code',
        'shipping_addr_id',
        'ship_to_phone',
    ];

    public $dateFields = [
        'odr_req_dt',
        'req_cmpl_dt',
        'act_cmpl_dt',
        'ship_by_dt',
        'cancel_by_dt',
        'act_cancel_dt',
        'ship_after_dt',
        'shipped_dt',
    ];

    public function shippingOrder()
    {
        return $this->hasOne(__NAMESPACE__ . '\ShippingOrder', 'so_id', 'so_id');
    }

    public function csrUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'csr');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function originalOdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'org_odr_id', 'odr_id');
    }

    public function updatedBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'updated_by');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function details()
    {
        return $this->hasMany(__NAMESPACE__ . '\OrderDtl', 'odr_id', 'odr_id');
    }
}
