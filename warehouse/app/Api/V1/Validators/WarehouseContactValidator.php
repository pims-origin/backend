<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:41
 */

namespace App\Api\V1\Validators;


class WarehouseContactValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_con_fname'      => 'required',
            'whs_con_lname'      => 'required',
            'whs_con_email'      => 'required|email',
            'whs_con_phone'      => 'required',
            'whs_con_position'   => '',
            'whs_con_whs_id'     => 'required|integer',
            'whs_con_department' => '',
        ];
    }
}
