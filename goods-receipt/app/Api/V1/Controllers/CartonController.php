<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 7/25/2016
 * Time: 11:46 AM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\DamageCartonModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\LocationStatusDetailModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Transformers\ConsolidationLocTransformer;
use App\Api\V1\Transformers\LocationTransformer;
use App\Api\V1\Transformers\GoodsReceiptListDamageTransformer;
use App\Api\V1\Transformers\ItemTransformer;
use App\Api\V1\Transformers\LocationStatusDetailTransformer;
use App\Api\V1\Transformers\CartonTransformer;
use App\Api\V1\Transformers\LpnACartonTransformer;
use App\Api\V1\Validators\DamageCartonValidator;
use App\Api\V1\Transformers\NextCartonTransformer;
use App\Api\V1\Validators\PrintDamageCartonValidator;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use mPDF;
use Ouzo\Utilities\Arrays;
use Ouzo\Utilities\Functions;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;

class CartonController extends AbstractController
{
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var DamageCartonModel
     */
    protected $damagedCartonModel;

    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var EventTrackingModel;
     */
    protected $eventTrackingModel;

    /**
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param DamageCartonModel $damageCartonModel
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct
    (
        CartonModel $cartonModel,
        LocationModel $locationModel,
        DamageCartonModel $damageCartonModel,
        GoodsReceiptModel $goodsReceiptModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        InventorySummaryModel $inventorySummaryModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->damagedCartonModel = $damageCartonModel;
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->eventTrackingModel = $eventTrackingModel;
    }

    /**
     * Show Damage Item
     *
     * @param $goodsReceiptId
     * @param Request $request
     * @param GoodsReceiptListDamageTransformer $goodsReceiptListDamageTransformer
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param GoodsReceiptListTransformer $goodsReceiptListTransformer
     *
     */
    public function showDamageItem(
        $goodsReceiptId,
        Request $request,
        GoodsReceiptListDamageTransformer $goodsReceiptListDamageTransformer
    ) {
        try {
            // get data from HTTP
            $input = $request->getQueryParams();

            $goodsReceiptDetail = $this->goodsReceiptDetailModel->search([
                'gr_hdr_id'     => $goodsReceiptId,
                'gr_dtl_is_dmg' => 1
            ], [
                'goodsReceipt',
                'asnDetail',
                'asnDetail.item',

            ], array_get($input, 'limit'));

            return $this->response->paginator($goodsReceiptDetail, $goodsReceiptListDamageTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Save Damage Carton
     *
     * @param Request $request
     * @param DamageCartonValidator $damageCartonValidator
     * @param DamageCartonModel $damageCartonModel
     * @param CartonModel $cartonModel
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveDamageCarton(
        Request $request,
        DamageCartonValidator $damageCartonValidator,
        DamageCartonModel $damageCartonModel,
        CartonModel $cartonModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        //validation
        $damageCartonValidator->validate($input);

        try {
            DB::beginTransaction();

            // Save Mass Damage Carton
            $this->saveMassDamageCarton($input['damage_cartons'], $damageCartonModel, $cartonModel);

            DB::commit();

            return $this->response->noContent()->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function saveMassDamageCarton
    (
        $damageCartons,
        DamageCartonModel $damageCartonModel,
        CartonModel $cartonModel
    ) {
        if (!empty($damageCartons)) {
            // Check duplicate data
            $duplicate = [];
            foreach ($damageCartons as $damageCarton) {
                if (in_array($damageCarton['ctn_id'], $duplicate)) {
                    throw new \Exception(Message::get("BM006", "carton number"));
                }
                $duplicate[] = $damageCarton['ctn_id'];
            }

            // Check existed carton
            $existedCartons = $this->cartonModel->checkExistedCartons($duplicate);
            if (!$existedCartons) {
                throw new \Exception(Message::get("BM017", "carton number"));
            }

            // Check existed damage carton
            $existedDamagedCartons = $this->damagedCartonModel->checkExistedCartons($duplicate);
            if ($existedDamagedCartons) {
                throw new \Exception(Message::get("BM006", "damaged carton"));
            }

            // Save damaged carton
            $cartonIds = array_pluck($damageCartons, "ctn_id");

            $inventoryData = $this->cartonModel->getDmgQty($cartonIds)->toArray();
            $this->inventorySummaryModel->updateDmgQty($inventoryData);

            foreach ($damageCartons as $damageCarton) {
                // data for damage_carton
                $dataDamage = [
                    'ctn_id'   => array_get($damageCarton, 'ctn_id', ''),
                    'dmg_id'   => array_get($damageCarton, 'dmg_id', ''),
                    'dmg_note' => array_get($damageCarton, 'dmg_note', ''),
                ];

                // data for carton
                $dataCarton = [
                    'ctn_id'     => array_get($damageCarton, 'ctn_id', ''),
                    'is_damaged' => 1,
                ];

                // Save to damage_carton & carton
                $cartonModel->refreshModel();
                $damageCartonModel->refreshModel();
                $damageCartonModel->create($dataDamage);
                $cartonModel->update($dataCarton);
            }

            // event tracking for damage carton
            if (isset($inventoryData[0])) {
                $ct = $inventoryData[0];

                $gr_dtl_id = array_get($ct, 'gr_dtl_id', null);
                $gr = $this->goodsReceiptDetailModel->findWhere(['gr_dtl_id' => $gr_dtl_id],
                    ['goodsReceipt'])->toArray();
                $gr_num = array_get($gr, '0.goods_receipt.gr_hdr_num', null);

                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => array_get($gr, '0.goods_receipt.whs_id', null),
                    'cus_id'    => array_get($gr, '0.goods_receipt.cus_id', null),
                    'owner'     => $gr_num,
                    'evt_code'  => config('constants.event.SET-DAMAGED'),
                    'trans_num' => $gr_num,
                    'info'      => sprintf('There are %d carton(s) damaged in %s', count($damageCartons), $gr_num)

                ]);

            }


        } else {
            return $this->response->errorBadRequest(Message::get("BM002", "carton number"));
        }
    }

    /**
     * @param $cartonId
     * @param $goodsReceiptDetailId
     * @param CartonModel $cartonModel
     * @param NextCartonTransformer $nextCartonTransformer
     *
     * @return Response|void
     */
    public function showNextCarton(
        $cartonId,
        $goodsReceiptDetailId,
        CartonModel $cartonModel,
        NextCartonTransformer $nextCartonTransformer
    ) {
        try {
            $nextCarton = $cartonModel->getNextCtnId($cartonId, $goodsReceiptDetailId);
            if (!$nextCarton) {
                throw new \Exception(Message::get("BM017", "carton"));
            }

            return $this->response->item($nextCarton, $nextCartonTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param Excel $excel
     * @param PrintDamageCartonValidator $printDamageCartonValidator
     * @param null $goodsReceiptId
     */
    public function printDamageCarton(
        Request $request,
        Excel $excel,
        PrintDamageCartonValidator $printDamageCartonValidator,
        $goodsReceiptId = null
    ) {

        if ($goodsReceiptId) {
            $input = $this->cartonModel->getDamageCarton($goodsReceiptId);
        } else {

            $input = $request->getParsedBody();
            $printDamageCartonValidator->validate($input);
        }

        try {
            if (count($input)) {

                //Export to Excel file
                //    $excel->create('file', function ($file) use ($input) {
                //        $file->setFilename("Damaged-Carton");
                //        $file->sheet("Template", function ($sheet) use ($input) {
                //            $sheet->mergeCells('A1:E1');
                //            $sheet->cell('A1', function ($cell) {
                //                // Set font
                //
                //                $cell->setFontWeight();
                //                $cell->setAlignment('center');
                //            });
                //            $sheet->setCellValue('A1', "DAMAGED CARTON LIST");
                //            $sheet->setCellValue('A2', "CARTON NUMBER");
                //            $sheet->setCellValue('B2', "DAMAGED TYPE");
                //            $i = 3;
                //            foreach ($input as $row) {
                //                $sheet->setCellValue('A' . $i, $row['ctn_num']);
                //                $sheet->setCellValue('B' . $i, $row['dmg_type']);
                //                $i++;
                //            }
                //
                //        });
                //
                //    })->download('xlsx');

                //add more item to $input
                $userInfo = new \Wms2\UserInfo\Data();
                $userInfo = $userInfo->getUserInfo();
                $firstName = $userInfo['first_name'];
                $lastName = $userInfo['last_name'];

                $ctnNum = array_pluck($input, 'ctn_num', 'ctn_num');
                $cartonData = [];
                foreach ($input as $ctn) {
                    $ctnNum = array_get($ctn, 'ctn_num', '');
                    $cartonInfo = $this->cartonModel->getFirstWhere(
                        ['ctn_num' => $ctnNum]
                    );

                    $cartonData[] = [
                        'sku'      => object_get($cartonInfo, 'sku', null),
                        'size'     => object_get($cartonInfo, 'size', null),
                        'color'    => object_get($cartonInfo, 'color', null),
                        'lot'      => object_get($cartonInfo, 'lot', 'NA'),
                        'ctn_num'  => array_get($ctn, 'ctn_num', ''),
                        'dmg_type' => array_get($ctn, 'dmg_type', ''),
                        'dmg_note' => array_get($ctn, 'dmg_note', ''),
                    ];
                }

                //Export to pdf file
                $this->printPdfData($cartonData, $firstName, $lastName);

            } else {
                return $this->response->errorBadRequest(Message::get("VR029", "data"));
            }


        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param $damagedCarton
     *
     * @throws \MpdfException
     */
    private function printPdfData($damagedCarton, $firstName, $lastName)
    {
        $pdf = new Mpdf();
        $html = (string)view('DamagedCartonListPrintoutTemplate', [
            'damagedCarton' => $damagedCarton,
            'firstName'     => $firstName,
            'lastName'      => $lastName,
        ]);

        $pdf->WriteHTML($html);
        $pdf->Output("", "I");
    }

    /**
     * @param $locationId
     * @param ItemTransformer $itemTransformer
     * @param Request $request
     *
     * @return Response|void
     */
    public function getItemsList($locationId, ItemTransformer $itemTransformer, Request $request)
    {
        try {
            $input = $request->getQueryParams();
            $carton = $this->cartonModel->searchItem($locationId, ['item', 'AsnDtl.systemUom'],
                array_get($input, 'limit'));

            return $this->response->paginator($carton, $itemTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getLPNItemsList($lpn, ItemTransformer $itemTransformer, Request $request)
    {
        try {
            $input = $request->getQueryParams();
            $carton = $this->cartonModel->searchLPNItem($lpn, ['item', 'AsnDtl.systemUom'],
                array_get($input, 'limit'));

            return $this->response->paginator($carton, $itemTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $locationId
     * @param ConsolidationLocTransformer $consolidationLocTransformer
     * @param Request $request
     *
     * @return Response|void
     */
    public function getCartonListByLocation(
        $locationId,
        ConsolidationLocTransformer $consolidationLocTransformer,
        Request $request
    ) {
        try {
            $input = $request->getQueryParams();
            $carton = $this->cartonModel->search($locationId, ['item', 'AsnDtl.systemUom'],
                array_get($input, 'limit'));

            return $this->response->paginator($carton, $consolidationLocTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $locationId
     * @param LocationStatusDetailModel $locationStatusDetailModel
     * @param LocationStatusDetailTransformer $locationStatusDetailTransformer
     *
     * @return Response|void
     */
    public function changeLocationStatus(
        $locationId,
        LocationStatusDetailModel $locationStatusDetailModel,
        LocationStatusDetailTransformer $locationStatusDetailTransformer
    ) {
        $params = [
            'loc_sts_dtl_loc_id'    => $locationId,
            'loc_sts_dtl_sts_code'  => config('constants.location_status.ACTIVE'),
            'loc_sts_dtl_from_date' => time(),
        ];

        try {
            // Update Location status detail
            if ($locationStatusDetailModel->getFirstBy('loc_sts_dtl_loc_id', $locationId)) {
                $result = $locationStatusDetailModel->update($params);
            } else {
                $result = $locationStatusDetailModel->create($params);
            }

            if ($result) {
                // Update Location
                $LocationParams = [
                    'loc_id'       => $locationId,
                    'loc_sts_code' => $params['loc_sts_dtl_sts_code'],
                ];
                $this->locationModel->update($LocationParams);

                return $this->response->item($result, $locationStatusDetailTransformer);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $locationId
     * @param ItemTransformer $itemTransformer
     * @param Request $request
     *
     * @return Response|void
     */
    public function viewLocationDetail($locationId, ItemTransformer $itemTransformer, Request $request)
    {
        try {
            $input = $request->getQueryParams();
            $carton = $this->cartonModel->searchItemInLocation($locationId, ['item', 'AsnDtl.systemUom', 'customer'],
                array_get($input, 'limit'));

            return $this->response->paginator($carton, $itemTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param CartonTransformer $cartonTransformer
     *
     * @return Response|void
     */
    public function cartonList(Request $request, CartonTransformer $cartonTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $cartonInfo = $this->cartonModel->searchCartons($input, [
                'goodReceiptDtl'
            ], array_get($input, 'limit'));

            return $this->response->paginator($cartonInfo, $cartonTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function searchLpnLoc(Request $request, LpnACartonTransformer $lpnACartonTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $cartonInfo = $this->cartonModel->searchLpnLoc($input);
            return $this->response->paginator($cartonInfo, $lpnACartonTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function searchLpnLocation(Request $request,
                                      //LpnACartonTransformer $lpnACartonTransformer,
                                      LocationTransformer $locationTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $locCode = array_get($input, 'loc_lpn_carton');
            //if(strpos($locCode, "P-") === false) {
                $locations = $this->locationModel->search($input);
                return $this->response->paginator($locations, $locationTransformer);
            //}
//            else {
//                $cartonInfo = $this->cartonModel->searchLpnLoc($input, [
//                    'goodReceiptDtl'
//                ], array_get($input, 'limit'));
//                return $this->response->paginator($cartonInfo, $lpnACartonTransformer);
//            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
