<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\AsnDtlModel;
use Dingo\Api\Routing\Helpers;
use App\Api\V1\Transformers\AsnListsCanSortV1Transformer;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CustomerWarehouse;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Utils\Status;
use App\Api\V1\Models\LocationModel;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\AsnDtl;
use \Firebase\JWT\JWT;

class TestInbound extends TestCase
{

    //use DatabaseTransactions;
    use Helpers;
    use TraitTest;

    private $whsId = 99;
    private $cusId = 57;
    private $palletId = 1;
    public static $_caseCreateVTCarton = [];
    private $_token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0ODk1Njg3NzcsImV4cCI6MTQ4OTY1NTE3NywibmFtZSI6IkRhbyBUcmFuIiwidXNlcm5hbWUiOiJjc3JkdHIiLCJqdGkiOjc3fQ.N5pa-dCQ6DGyqg6YLR-iaJpvfIpOAJsnlcoj2PiGWaI";

    public function setUp()
    {
        parent::setUp();
        //prevent Token expire
        JWT::$leeway = 99999999;
    }

    /*
     * Test get ASN list V1
     * URL:     /whs/{whsId:[0-9]+}/asnsV1
     * Method:  GET
     * Action:  AsnController@searchCanSortV1
     */

    public function testAsnsV1()
    {
        echo "\n[STAR] testAsnsV1\n";
        $response = $this->call('GET', "/inbound/whs/{$this->whsId}/asnsV1");
        $content = $response->content();
        $jsonObj = json_decode($content);
        echo "[CALL] /inbound/whs/{$this->whsId}/asnsV1\n";
        echo "[MESS] Asswert status equals 200\n";
        $this->assertEquals(200, $response->status());
        echo "[DONE] testAsnsV1\n";
    }

    /*
     * Test get ASN History
     * URL:     /whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asn-history/{asnDtlId:[0-9]+}
     * Method:  GET
     * Action:  AsnController@getAsnHistory
     */

    public function testAsnHistory()
    {
        echo "\n[STAR] testAsnHistory\n";
        $response = $this->call('GET', "/inbound/whs/{$this->whsId}/cus/{$this->cusId}/asn-history/1");
        $content = $response->content();
        $jsonObj = json_decode($content);
        //dd($jsonObj);
        echo "[RURL] /inbound/whs/{$this->whsId}/cus/{$this->cusId}/asn-history/1\n";
        $this->assertEquals(200, $response->status());
        echo "[DONE] testAsnHistory\n";
    }

    /**
     * Empty location, happy case
     * Test case: submit exist pallet rfid
     */
    public function testGetEmptyLocations()
    {
        echo "\n[STAR] testGetEmptyLocations\n";
        $pallet = Pallet::where([
                    'whs_id' => $this->whsId,
                    'cus_id' => $this->cusId,
                    'plt_sts' => 'AC'
                ])
                ->where('ctn_ttl', '>', 0)
                //->whereNotNull('rfid')
                ->first();
        if (!$pallet) {
            $this->markTestIncomplete("[INCO] No pallet!\n");
        }
        $result = false;
        if (!$pallet->rfid) {
            $pallet->rfid = strtoupper(uniqid("FF"));
            $result = $pallet->save();
        } else {
            $result = true;
        }
        if (!$result) {
            $this->markTestIncomplete("[INCO] Set rfid to pallet for tesing failed!\n");
        }

        $palletRfid = $pallet->rfid;
        $url = "/inbound/whs/{$this->whsId}/pallet/{$palletRfid}/location/rack/get-empty-locations";
        echo "[RURL] {$url}\n";
        $response = $this->call('GET', $url);
        $content = $response->content();
        $jsonObj = json_decode($content);
        if (isset($jsonObj->error)) {
            echo "[INCO] Test incomplete: {$jsonObj->error->message}\n";
            echo "[DONE] testGetEmptyLocations\n";
            $this->markTestIncomplete();
        }
        if (200 != $response->status()) {
            echo "[INCO] Test incomplete status not equal 200, {$response->status()}\n";
            echo "[DONE] testGetEmptyLocations\n";
            $this->markTestIncomplete();
        }

        $content = $response->content();
        $jsonObj = json_decode($content);

        $locModel = new LocationModel();
        $location = $locModel->getEmptyLocationByCusId($this->whsId, $this->cusId);
        /*
         * Verify data return for case no location
         */
        if (empty($location)) {
            echo "[MESS] Verify case\n";
            $verifyMsg = "Current location is not defined in WMS system. Please contact Administrator";
            $errorStatusMsg = "[FAIL] Error MESSAGE return incorrect\n";
            $errorMsg = "[FAIL] Return STATUS in json incorrect!\n";
            $this->assertFalse($jsonObj->status, $errorStatusMsg);
            $this->assertEquals($verifyMsg, $jsonObj->message, $errorMsg);
        }

        $code = $location->loc_alternative_name;
        $locCode = explode("-", $code);

        /*
         * Verify for case incorrect locCode
         */
        if (count($locCode) != 4) {
            $verifyString = "Loc Code is invalid!";
            $failMsg = "[FAIL] ";
            $this->assertStringMatchesFormat($content, $verifyString, $failMsg);
        }
        //dd($content);

        if (isset($jsonObj->status)) {
            $this->assertTrue($jsonObj->status, $response->status());
        }
        echo "[DONE] testGetEmptyLocations\n";
    }

    /**
     * Test create virtual cartons
     * URL:     /whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/add-virtual-cartons
     * Method:  POST
     * Action:  VirtualCartonController@scanCartons
     */
    public function testCreateVirtualCartons()
    {
        echo "\n[STAR] testCreateVirtualCartons\n";
        $asnModel = new AsnDtlModel();
        $input['whs_id'] = $this->whsId;
        $return = $asnModel->getListASN(
                $input, null
        );
        $obj = $return->getCollection()->first();
        if (!$obj) {
            echo "[INCO] No asn data in status NEW or RECEIVING!\n";
            $this->markTestIncomplete();
        }
        $tran = new AsnListsCanSortV1Transformer();
        echo "[MESS] Found ASN {$obj->asn_hdr_id}\n";
        $data = $tran->transform($obj);

        ////////////
        $asnHdrId = $data['asn_hdr_id'];
        //check them item co hay ko
        $asnDtlId = $data['items'][0]['asn_dtl_id'];
        $itemId = $data['items'][0]['item_id'];
        $ctnrId = $data['ctnr_id'];
        $cusId = $data['cus_id'];

        $ctnsRfid = [];
        for ($i = 0; $i < 4; $i++) {
            $ctnsRfid[] = strtoupper(uniqid("TEST-"));
        }
        $params = [
            'asn_hdr_id' => $asnHdrId,
            'asn_dtl_id' => $asnDtlId,
            'ctn_rfid.0' => '',
            'ctn_rfid' => $ctnsRfid[0],
            'ctns_rfid' => $ctnsRfid,
            'type' => 1,
            'item_id' => $itemId,
            'ctnr_id' => $ctnrId,
                //'discrepancy' => '',
        ];

        $url = "/inbound/whs/{$this->whsId}/cus/{$cusId}/add-virtual-cartons";
        echo "[MESS] Data submit " . json_encode($params) . "\n";
        $response = $this->call("POST", $url, $params, [], []
                , ['HTTP_Authorization' => $this->_token]
        );
        $content = $response->content();
        $jsonObj = json_decode($content);
        if (isset($jsonObj->data) && isset($jsonObj->data->status)) {
            $this->assertTrue($jsonObj->data->status, '[FAIL] Result is false: ' . $jsonObj->data->message);
        } else {
            $this->fail("[FAIL] Unknown structure: {$content}\n");
        }

        echo "[DONE] testCreateVirtualCartons\n";
    }

    /**
     * Test complete asn detail api
     * URL:     /whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asn-detail/{asnDtlId:[0-9]+}/complete-sku
     * Method:  PUT
     * Action:  VirtualCartonController@completeASNDetail
     */
    public function testCompleteASNDetail()
    {
        echo "\n[STAR] testCompleteASNDetail\n";
        $asnID = 10;
        $url = "/inbound/whs/{$this->whsId}/cus/{$this->cusId}/asn-detail/{$asnID}/complete-sku";
        $response = $this->call("PUT", $url);
        $content = $response->content();
        $jsonObj = json_decode($content);
        if (isset($jsonObj->data)) {
            $content = 'Complete carton successfully !';
            $this->assertContains($jsonObj->data->message, $content, '"Complete SKU" not complete');
        } else if (isset($jsonObj->error) && isset($jsonObj->error->message)) {
            echo $jsonObj->error->message . "\n";
        }
        echo "[DONE] testCompleteASNDetail\n";
    }

    /**
     * Test set damage carton
     * URL:     /whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/carton/set-damage-carton
     * Method:  PUT
     * Action:  VirtualCartonController@setDamageCarton
     */
    public function testSetDamageCarton()
    {
        echo "\n[STAR] testSetDamageCarton\n";
        $vtnCtn = VirtualCarton::where([
                    'cus_id' => $this->cusId,
                    'whs_id' => $this->whsId
                ])
                ->whereNotNull('ctn_rfid')
                ->first();
        if (!$vtnCtn) {
            $this->markTestSkipped("[SKIP] No data to test");
        }
        $ctnRfid = $vtnCtn->ctn_rfid;
        echo "Start set damage to {$vtnCtn->ctn_rfid}\n";
        $param = [
            'ctn_rfid' => $ctnRfid
        ];
        $url = "/inbound/whs/{$this->whsId}/cus/{$this->cusId}/carton/set-damage-carton";
        $this->call("PUT", $url, $param);

        $newVtnCtn = VirtualCarton::find($vtnCtn->vtl_ctn_id);
        $damage = $newVtnCtn->is_damaged;
        $this->assertEquals(1, $damage, "[FAIL] Set damage in DB incorrect\n");
        echo "[DONE] testSetDamageCarton\n\n";
    }

    /**
     * Test delete virtual carton
     * URL:     /whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/carton/delete-virtual-carton
     * Method:  PUT
     * Action:  VirtualCartonController@deleteVirtualCarton
     */
    public function testDeleteVirtualCarton()
    {
        $vtnCtn = VirtualCarton::where([
                    'cus_id' => $this->cusId,
                    'whs_id' => $this->whsId,
                    'vtl_ctn_sts' => Status::getByValue('NEW', 'VIRTUAL-CARTON-STATUS')
                ])->first();

        if (null === $vtnCtn) {
            echo "\n[SKIP] No virtual carton to test testDeleteVirtualCarton.\n";
            $this->markTestSkipped(
                    "[SKIP] No virtual carton to test testDeleteVirtualCarton.\n"
            );
        }
        $ctnRfid = $vtnCtn->ctn_rfid;
        $param = ['ctn_rfid' => $ctnRfid];
        echo "\n[STAR] testDeleteVirtualCarton {$ctnRfid}\n";
        $res = $this->call("PUT", "/inbound/whs/{$this->whsId}/cus/{$this->cusId}/carton/delete-virtual-carton", $param);
        $content = $res->content();
        $obj = json_decode($content);
        if (isset($obj->status)) {
            $this->assertTrue($obj->status, "[FAIL] Http test delete virtual carton fail\n");
            $newVtnCtn = VirtualCarton::find($vtnCtn->vtl_ctn_id);
            if ($obj->status) {
                $this->assertNull($newVtnCtn, "[FAIL] Virtual carton still exist in database!!!\n");
                echo "[PASS] testDeleteVirtualCarton";
            } else {
                //$this->assert($newVtnCtn, "Virtual carton still exist in database!!!");
            }
        } else {
            echo "[FAIL] testDeleteVirtualCarton\n";
        }

        echo "\n[DONE] testDeleteVirtualCarton\n";
    }

    /**
     * Test scan pallet
     * URL:     /whs/{whsId:[0-9]+}/scan-pallet/
     * Method:  PUT
     * Action:  PalletController@scanPallet
     */
    public function testScanPallet()
    {
        echo "\n[STAR] ".__METHOD__."\n";
        $vtlCtn = VirtualCarton::where([
                    'whs_id' => $this->whsId,
                    'cus_id' => $this->cusId,
                ])
                ->where('ctn_rfid', 'like', "TEST%")
                ->whereNotNull('ctn_rfid')
                ->first();
        if (!$vtlCtn) {
            $this->markTestSkipped("[SKIP] No data to test");
        }
        echo "[MESS] Found virtual carton " . $vtlCtn->vtl_ctn_id . "\n";
        $newPltRfid = strtoupper(uniqid('FFTEST'));
        $res = $this->APIScanPalletPUT($this->whsId, $vtlCtn->ctn_rfid, $newPltRfid);
        $content = $res->content();
        echo "[DATA] Return data {$content}\n";
        $obj = json_decode($content);
        if ($obj->status) {
            $this->assertTrue($obj->status, '[FAIL] Scan pallet fail!');
            $newVtlCtn = VirtualCarton::where([
                        "vtl_ctn_id" => $vtlCtn->vtl_ctn_id
                    ])
                    ->first();
            echo "[CKDB] Pallet rfid in db : {$newVtlCtn->plt_rfid}\n";
            $this->assertEquals($newVtlCtn->plt_rfid, $newPltRfid, '[FAIL] Insert incorrect');
        } else {
            echo "[FAIL] scan pallet fail\n";
            $this->markTestIncomplete();
        }

        echo "[DONE] ".__METHOD__."\n";
    }

    /**
     * Check API PutAwayController@paPutAway
     * URL:     /whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/location/put-away/put-pallet
     * Method:  PUT
     * Action:  PutAwayController@paPutAway
     * CASE:    Pallet EXIST
     */
    public function testPullPallet_CasePallestExist()
    {
        echo "\n[STAR] " . __METHOD__ . "\n";

        /**
         * Get data from DB
         */
        $location = Location::where([
                    'loc_whs_id' => $this->whsId,
                    'loc_type_id' => 2
                ])->whereNotNull('loc_code')
                ->first();
        if (!$location) {
            $this->markTestIncomplete("[INCO] No location in status put way\n");
        }

        $pallet = Pallet::where([
                    'whs_id' => $this->whsId,
                    'cus_id' => $this->cusId
                ])
                ->whereNotNull('rfid')
                ->first();

        if (null === $pallet) {
            $this->markTestSkipped("[SKIP] No pallet have rfid to test. "
                    . "Please add rfid to a pallet!\n");
        }

        $oldCarton = Carton::where([
                    'plt_id' => $pallet->plt_id
                ])->first();

        $oldVirCar = VirtualCarton::where([
                    'plt_rfid' => $pallet->rfid
                ])->first();

        echo "[CALL] Put away Loc code: {$location->loc_code}, Pallet rfid: {$pallet->rfid}\n";
        $param = [
            'pallet-rfid' => $pallet->rfid,
            'loc-code' => $location->loc_code,
            'cus_id' => $this->cusId
        ];
        $res = $this->call("PUT", "/inbound/whs/{$this->whsId}/cus/{$this->cusId}/location/put-away/put-pallet", $param);
        $content = $res->content();
        $obj = json_decode($content);

        $this->assertObjectHasAttribute("Status", $obj, "[FAIL] NOT return status attribute: {$content}\n");
        $newPallet = Pallet::find($pallet->plt_id);
        $newCarton = Carton::where([
                    'plt_id' => $newPallet->plt_id
                ])->first();
        $newVirtualCarton = VirtualCarton::where([
                    'plt_rfid' => $pallet->rfid
                ])->first();
        $newLoc = Location::find($location->loc_id);
        $stsActive = Status::getByKey('LOCATION_STATUS', 'ACTIVE');
        if (isset($obj->Status)) {
            if ($obj->Status == 'OK') {
                /*
                 * Check pallet is updated data
                 */
                $this->assertEquals($location->loc_code, $newPallet->loc_code, "[FAIL] NOT UPDATE  pallet loc_code\n");
                $this->assertEquals($location->loc_id, $newPallet->loc_id, "[FAIL] NOT UPDATE  pallet loc_id\n");
                $this->assertEquals($location->loc_alternative_name, $newPallet->loc_name, "[FAIL] NOT UPDATE  pallet loc_name\n");
                /*
                 * Check carton is updated data
                 */
                $this->assertEquals($location->loc_id, $newCarton->loc_id, "[FAIL] NOT UPDATE carton(loc_id)");
                $this->assertEquals($location->loc_code, $newCarton->loc_code, "[FAIL] NOT UPDATE carton(loc_code)");
                $this->assertEquals($location->loc_alternative_name, $newCarton->loc_code, "[FAIL] NOT UPDATE carton(loc_code)");
                $this->assertEquals(Status::getByValue('PUT-AWAY', 'LOC_TYPE_CODE'), $newCarton->loc_type_code, "[FAIL] NOT UPDATE carton (loc_type_code)");
                /*
                 * Check virtual carton is set null for loc_id and loc_code
                 */
//                $this->assertNull($newVirtualCarton->loc_id, "[FAIL] NOT SET NULL for virtual carton(loc_id)");
//                $this->assertNull($newVirtualCarton->loc_code, "[FAIL] NOT SET NULL for virtual carton(loc_code)");
                /*
                 * Check Location is set ACTIVE
                 */
                $this->assertEquals($stsActive, $newLoc->loc_sts_code, "[FAIL] NOT SET ACTIVE for LOCATION(loc_sts_code)");
            } else {
                /**
                 * Check pallet is changed data, if data change it FAILE
                 */
                $this->assertEquals($pallet->loc_code, $newPallet->loc_code, "[FAIL] WHY UPDATE PALLET(loc_code)\n");
                $this->assertEquals($pallet->loc_id, $newPallet->loc_id, "[FAIL] WHY UPDATE PALLET(loc_id)\n");
                $this->assertEquals($pallet->loc_name, $newPallet->loc_name, "[FAIL] WHY UPDATE PALLET(loc_name)\n");
                /*
                 * Check carton
                 */
                $this->assertEquals($oldCarton->loc_id, $newCarton->loc_id, "[FAIL] WHY UPDATE CARTON loc_id");
                $this->assertEquals($oldCarton->loc_code, $newCarton->loc_code, "[FAIL] WHY UPDATE CARTON loc_id");
                $this->assertEquals($oldCarton->loc_alternative_name, $newCarton->loc_code, "[FAIL] WHY UPDATE CARTON loc_id");
                $this->assertEquals($oldCarton->loc_type_code, $newCarton->loc_type_code, "[FAIL] WHY UPDATE CARTON loc_type_code");
                /*
                 * Check virtual carton is changed
                 */
//                $this->assertEquals($oldVirCar->loc_id, $newVirtualCarton->loc_id, "[FAIL] WHY UPDATE(loc_id)");
//                $this->assertEquals($oldVirCar->loc_code, $newVirtualCarton->loc_code, "[FAIL] WHY UPDATE(loc_code)");
                /*
                 * Check Location data change
                 */
                $this->assertEquals($location->loc_sts_code, $newLoc->loc_sts_code, "[FAIL] WHY UPDATE LOCATION(loc_sts_code)");
            }
            echo "[HTTP] Return message: {$obj->message}\n";
        } else {
            /**
             * Check pallet is changed data, if data change it FAILE
             */
            $this->assertEquals($pallet->loc_code, $newPallet->loc_code, "[FAIL] WHY UPDATE PALLET(loc_code)\n");
            $this->assertEquals($pallet->loc_id, $newPallet->loc_id, "[FAIL] WHY UPDATE PALLET(loc_id)\n");
            $this->assertEquals($pallet->loc_name, $newPallet->loc_name, "[FAIL] WHY UPDATE PALLET(loc_name)\n");
            /*
             * Check carton
             */
            $this->assertEquals($oldCarton->loc_id, $newCarton->loc_id, "[FAIL] WHY UPDATE CARTON loc_id");
            $this->assertEquals($oldCarton->loc_code, $newCarton->loc_code, "[FAIL] WHY UPDATE CARTON loc_id");
            $this->assertEquals($oldCarton->loc_alternative_name, $newCarton->loc_code, "[FAIL] WHY UPDATE CARTON loc_id");
            $this->assertEquals($oldCarton->loc_type_code, $newCarton->loc_type_code, "[FAIL] WHY UPDATE CARTON loc_type_code");
            /*
             * Check virtual carton is changed
             */
            $this->assertEquals($oldVirCar->loc_id, $newVirtualCarton->loc_id, "[FAIL] WHY UPDATE(loc_id)");
            $this->assertEquals($oldVirCar->loc_code, $newVirtualCarton->loc_code, "[FAIL] WHY UPDATE(loc_code)");
            /*
             * Check Location data change
             */
            $this->assertEquals($location->loc_sts_code, $newLoc->loc_sts_code, "[FAIL] WHY UPDATE LOCATION(loc_sts_code)");
        }
        echo "[DONE] " . __METHOD__ . "\n";
    }

    /**
     * Test get customer list
     * Url:     /inbound/whs/{whs_id}/cus-list
     * Method:  GET
     * Action:  CustomerWarehouseController@loadBy
     */
    public function testGetCusList()
    {
        $action = "CustomerWarehouseController@loadBy";
        echo "\n[STAR] {$action}\n";

        /**
         * Get data from DB
         */
        $statusNew = Status::getByKey("ASN_STATUS", "NEW");
        $statusRC = Status::getByKey("ASN_STATUS", "RECEIVING");
        $queryBuilder = CustomerWarehouse::where([
                    'whs_id' => $this->whsId
                ])->whereHas('asnHdr', function ($query) use ($statusNew, $statusRC) {
            $query->where('asn_sts', $statusNew)
                    ->orWhere('asn_sts', $statusRC);
        });
        $cusWhsData = $queryBuilder->get();
        $numDataInDB = $cusWhsData->count();

        /**
         * Request api
         */
        $res = $this->call("GET", "/inbound/whs/{$this->whsId}/cus-list");
        $content = $res->content();
        $jsonObj = json_decode($content);
        $errMsg = "[FAIL] Data return incorrect\n";
        $numData = count($jsonObj->data);
        /**
         * Verify data
         */
        if ($cusWhsData && $numDataInDB > 0) {
            echo "[MESS] Verify have data, in DB: {$numDataInDB}, return: {$numData}\n";
            $this->assertGreaterThan(0, $numData, $errMsg);
        } else {
            echo "[MESS] Verify no data\n";
            $this->assertEquals(0, $numData, $errMsg);
        }

        echo "[DONE] {$action}\n";
    }

    /**
     * Test get containers
     * URL:     /inbound/whs/{whsId:[0-9]+}/containers
     * Method:  GET
     * Action:  ContainerController@search
     */
    public function testGetContainers()
    {
        $action = "ContainerController@search";
        echo "\n[STAR] {$action}\n";
        /*
         * Check data in DB
         */
        $ctnrNum = 'CNTR-001';
        $limit = 10;
        $whsId = $this->whsId;
        $container = Container::where('ctnr_num', 'like', "%" . SelStr::escapeLike($ctnrNum) . "%")
                ->whereHas('asnDtl.asnHdr', function ($query) use ($whsId) {
                    $query->where('whs_id', SelStr::escapeLike($whsId));
                })
                ->limit($limit)
                ->get();
        $numDataInDb = $container->count();
        $params = [
            'ctnr_num' => $ctnrNum,
            'limit' => $limit,
            'whs_id' => $this->whsId
        ];
        echo "[RURL] /inbound/whs/{$this->whsId}/containers\n";
        $res = $this->call("GET", "/inbound/whs/{$this->whsId}/containers", $params);
        $content = $res->content();
        $jsonObj = json_decode($content);

        if (200 != $res->status()) {
            $this->markTestIncomplete("[FAIL] Server or network have problem\n");
        }

        if (isset($jsonObj->data)) {
            $numResponse = count($jsonObj->data);
            echo "[MESS] Start verify data respone({$numResponse}) with data in DB({$numDataInDb})\n";
            $errMsg = "[FAIL] Data not the same with in database\n";
            $this->assertEquals($numDataInDb, $numResponse, $errMsg);
        } else {
            $this->markTestIncomplete("[FAIL] No DATA attribute\n");
        }

        echo "[DONE] {$action}\n";
    }

    /**
     * Test view ASN detail with container_id and asn_id
     * URL:     /whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asns/{asnID:[0-9]+}/containers/{ctnID:[0-9]+}
     * Method:  GET
     * Action:  AsnController@loadASNDtl
     */
    public function testContainers()
    {
        $action = "ContainerController@search";
        echo "\n[STAR] {$action}\n";

        /**
         * Get data from DB
         */
        $asnData = AsnHdr::all()->first();
        $asnDtlData = AsnDtl::where([
                    'asn_hdr_id' => $asnData->asn_hdr_id
                ])->get();
        $num = $asnDtlData->count();
        $asnDtlFirst = $asnDtlData->first();
        $ctnrId = $asnDtlFirst->ctnr_id;
        $asnId = $asnData->asn_hdr_id;
        /**
         * Call api
         */
        $url = "/inbound/whs/{$this->whsId}/cus/{$this->cusId}/asns/{$asnId}/containers/{$ctnrId}";
        $res = $this->call('GET', $url);

        if (200 != $res->status()) {
            $this->markTestIncomplete("[INCO] Test incomplete\n");
        }

        $content = $res->content();
        $jsonObj = json_decode($content);
        if (isset($jsonObj->data)) {
            $this->assertEquals($num, count($jsonObj->data), "[FAIL] Data return incorrect\n");
        }
        echo "[RULR] {$url}\n";
        echo "[DONE] {$action}\n";
    }

    /**
     * Test drop location
     * URL:     /inbound/whs/{whsId:[0-9]+}/pallet/{rfid:[a-zA-Z0-9]+}/drop-location
     * Method:  GET
     * Action:  PalletController@simulatePalletOnRack
     */
    public function testDropLocation()
    {
        $action = "PalletController@simulatePalletOnRack";
        echo "\n[STAR] {$action}\n";

        /**
         * Get data from DB
         */
        $pallet = Pallet::where([
                    'whs_id' => $this->whsId
                ])
                ->whereNotNull('rfid')
                ->get()
                ->first();
        if (!$pallet) {
            $this->markTestIncomplete("[INCO] Not found any pallet to test\n");
        }
        echo "[MESS] Found 1 pallet to test, plt_id: {$pallet->plt_id}\n";

        $locationMd = new LocationModel();
        $location = $locationMd->getLocByRfId($this->whsId, null);

        /**
         * Call api
         */
        $url = "/inbound/whs/{$this->whsId}/pallet/{$pallet->rfid}/drop-location";
        echo "[CALL] {$url}\n";
        $res = $this->call('GET', $url);

        if (200 != $res->status()) {
            echo "[INCO] Request not return status 200, status: {$res->status()}\n";
            $this->markTestIncomplete("[INCO] Test incomplete\n");
        }
        $content = $res->content();
        $jsonObj = json_decode($content);

        if (isset($jsonObj->status) && !$jsonObj->status) {
            echo "[INCO] Test incomplete, message: {$jsonObj->message}\n";
            echo "[DONE] {$action}\n";
            $this->markTestSkipped("[INCO] Test incomplete, message: {$jsonObj->message}\n");
        }

        /*
         * if no location in db, expect return status is false
         */
        if (!$location) {
            $myString = "Current location is not defined in WMS system. Please contact Administrator";
            $this->assertStringMatchesFormat($myString, $jsonObj->message, "[FAIL] Message return incorrect!\n");
            $this->assertFalse($jsonObj->status, "[FAIL] Return status incorrect!\n");
            $this->markTestSkipped("[SKIP] Stop verify other case!\n");
        }

        $code = $location->loc_alternative_name;
        $locCode = explode("-", $code);
        if (count($locCode) != 4) {
            $expectedStr = "Loc Code is invalid!";
            $this->assertContains($expectedStr, $res->content(), "[FAIL] Data return not contain string require!\n");
            $this->markTestSkipped("[SKIP] Stop verify other case\n");
        }

        if (isset($jsonObj->status)) {
            //dd($jsonObj);
            $this->assertGreaterThan(0, count($jsonObj->data->layout), "[MESS] No data found\n");
            $this->assertTrue($jsonObj->status, "[FAIL] Drop Location failed\n");
        }

        echo "[DONE] {$action}\n";
    }

    /**
     * Test Update location for pallet, carton
     * URL:     /whs/{whsId:[0-9]+}/location/rack/put-pallet
     * Method:  GET
     * Action:  PutAwayController@putAway
     */
    public function testRackPutPallet()
    {
        $action = "PutAwayController@putAway";
        echo "\n[STAR] {$action}\n";

        /**
         * Get data from DB
         */
        /*$type = Status::getByValue('RACK', 'LOC_TYPE_CODE');
        $location = Location::where([
                    'loc_whs_id' => $this->whsId,
                    'loc_sts_code' => 'AC'
                ])
                ->whereNotNull('rfid')
                ->whereHas("locationType", function ($q) use ($type) {
                    $q->where('loc_type_code', $type);
                })
                ->first();
        $pallet = Pallet::where([
                    'whs_id' => $this->whsId,
                    'plt_sts' => 'AC'
                ])->whereNull('loc_id')
                ->whereNotNull('rfid')
                ->first();

        if (!$location || !$pallet) {
            echo "[SKIP] No data to test\n";
            echo "[DONE] {$action}\n";
            $this->markTestSkipped();
        }

        echo "[INFO] Start test plt_rdfid:{$pallet->rfid}, loc_rfid: {$location->rfid}\n";*/
        $url = "/inbound/whs/2/location/rack/put-pallet";
        $params = [
            'pallet-rfid' => "FFFFFFFF0002000005041502",
            'loc-rfid' =>"DDDDDDDD0002000004041017"
        ];
        $res = $this->call("PUT", $url, $params);
        $content = $res->content();
        $jsonObj = json_decode($content);
dd($jsonObj);
        if (isset($jsonObj->Status)) {
            $this->assertEquals('OK', $jsonObj->Status);
        } else {
            echo "[INCO] Put away fail: {$jsonObj->message}\n";
            $this->markTestIncomplete("[INCO] Put away fail\n");
            ;
        }

        echo "[DONE] {$action}\n";
    }

    /*
     * Test POST Verify Carton
     * URL:     /inbound/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/verify-cartons
     * Method:  POST
     * Action:  VirtualCartonController@verifyCartonRfids
     */

    public function testPostVerifyCartons()
    {
        echo "\n[STAR] " . __METHOD__ . "\n";

        /*
         * Find data to test
         */
        $data = VirtualCarton::first();
        if (!$data) {
            $this->markTestIncomplete("[INCO] No data to test\n");
        }

        /*
         * Call api
         */
        $url = "/inbound/whs/{$data->whs_id}/cus/{$data->cus_id}/verify-cartons";
        echo "[RURL] $url\n";
        $param = [
            'asn_dtl_id' => $data->asn_dtl_id,
            'ctns_rfid' => [$data->ctn_rfid],
            'asn_hdr_id' => 1,
            'item_id' => 1,
            'ctnr_id' => 1
        ];
        $res = $this->call('POST', $url, $param, [], [], ['HTTP_Authorization' => $this->_token]);
        $content = $res->content();
        $jsonObj = json_decode($content);
        /*
         * Verify
         */
        if (isset($jsonObj->message)) {
            echo "[MESS] Result message: {$jsonObj->message}\n";
        }
        if (isset($jsonObj->data) && isset($jsonObj->data->status)) {
            $this->assertEquals(1, $jsonObj->data->status, "[FAIL] Http failed\n");
        } else if (isset($jsonObj->status)) {
            $this->assertTrue($jsonObj->data->status, "[FAIL] Http failed\n");
        }
        echo "[DATA] {$content}\n";
        echo "[DONE] " . __METHOD__ . "\n";
    }

    /*
     * Test GET List Virtual Carton
     * URL:     /inbound/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asns/{asnId:[0-9]+}/containers/{ctnrId:[0-9]+}/carton/list-virtual-carton
     * Method:  POST
     * Action:  VirtualCartonController@listVirtualCarton
     */

    public function testGetListVirtualCarton()
    {
        echo "\n[STAR] " . __METHOD__ . "\n";

        /*
         * Find data to test
         */
        $data = VirtualCarton::first();

        /*
         * Call api
         */
        $url = "/inbound/whs/{$data->whs_id}/cus/{$data->cus_id}/asns/{$data->asn_hdr_id}/containers/{$data->ctnr_id}/carton/list-virtual-carton";
        echo "[RURL] $url\n";
        $param = [];
        $res = $this->call('GET', $url, $param, [], [], ['HTTP_Authorization' => $this->_token]);
        $content = $res->content();
        $jsonObj = json_decode($content);
        /*
         * Verify
         */
        if (isset($jsonObj->message)) {
            echo "[MESS] Result message: {$jsonObj->message}\n";
        }
        if (isset($jsonObj->data) && isset($jsonObj->data->status)) {
            $this->assertEquals(1, $jsonObj->data->status, "[FAIL] Http failed\n");
            $numData = count($jsonObj->data);
            echo "[DATA] assert num data greater than or equal 0\n";
            $this->assertGreaterThanOrEqual(0, count($jsonObj->data), "[FAIL] No found attribute data\n");
        } else if (isset($jsonObj->status)) {
            $this->assertTrue($jsonObj->data->status, "[FAIL] Http failed\n");
        }
        echo "[DONE] " . __METHOD__ . "\n";
    }

    /*
     * Test Get receiving ASN list
     * URL:     /inbound/whs/{whsId:[0-9]+}/get-receiving-asn
     * Method:  POST
     * Action:  AsnController@getReceivingASN
     */

    public function testGetReceivingASNList()
    {
        echo "\n[STAR] " . __METHOD__ . "\n";

        /*
         * Find data to test
         */
        $data = AsnHdr::where([
                    "asn_sts" => Status::getByKey("ASN_STATUS", "RECEIVING")
                ])->first();
        if (!$data) {
            $this->markTestIncomplete("[INCO] No data to test\n");
        }
        /*
         * Call api
         */
        $url = "/inbound/whs/{$data->whs_id}/get-receiving-asn";
        echo "[RURL] $url\n";
        $param = [];
        $res = $this->call('GET', $url, $param, [], [], ['HTTP_Authorization' => $this->_token]);
        $content = $res->content();
        $jsonObj = json_decode($content);
        /*
         * Verify
         */
        echo "[DATA] {$content}\n";
        $this->assertGreaterThanOrEqual(0, count($jsonObj->data), "[FAIL] No found attribute data\n");
        echo "[DONE] " . __METHOD__ . "\n";
    }

    /*
     * Test Get vtl_ctn by rfid
     * URL:     /inbound/{whsId:[0-9]+}/get-virtual-ctn/{ctn_rfid}
     * Method:  GET
     * Action:  VirtualCartonController@getVirtualCtnRfid
     */

    public function testGetVirtualCartonByRfid()
    {
        echo "\n[STAR] " . __METHOD__ . "\n";

        /*
         * Find data to test
         */
        $data = VirtualCarton::first();
        if (!data) {
            $this->markTestIncomplete("[INCO] No data to test\n");
        }
        /*
         * Call api
         */
        $url = "/inbound/{$data->whs_id}/get-virtual-ctn/{$data->ctn_rfid}";
        echo "[RURL] $url\n";
        $param = [];
        $res = $this->call('GET', $url, $param, [], [], ['HTTP_Authorization' => $this->_token]);
        $content = $res->content();
        $jsonObj = json_decode($content);
        $this->assertJson($content, "[FAIL] Data is not JSON format\n");
        /*
         * Verify
         */
        echo "[DATA] {$content}\n";
        $this->assertGreaterThanOrEqual(0, count($jsonObj->data), "[FAIL] No found attribute data\n");
        echo "[DONE] " . __METHOD__ . "\n";
    }

}
