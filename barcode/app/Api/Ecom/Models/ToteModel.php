<?php

namespace App\Api\Ecom\Models;

use Dingo\Api\Exception\UnknownVersionException;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Tote;
use Seldat\Wms2\Utils\SelArr;
use Wms2\UserInfo\Data;


class ToteModel extends AbstractModel
{
    const WHS_TOTE_QUALIFIER = 'wto';

    public function __construct(Tote $model = null)
    {
        $this->model = ($model) ?: new Tote();
    }

    public function scan($whsId, $request)
    {
        $tote = Tote::where([
            'tote_num'  => $request->tote_num,
            'whs_id'    => $whsId
        ])->first();

        if (!$tote){
            $data = [
                'whs_id'    => $whsId,
                'wv_id'     => NULL,
                'picker_id' => NULL,
                'tote_num'  => $request->tote_num,
                'ctn_ttl'   => 0,
                'tote_sts'  => 'AC'
            ];
            $tote = Tote::create($data);
            return $tote;
        }

        if ( $tote->wv_id && $tote->wv_id != $request->wv_id ){
            throw new \Exception("Tote does not belong to current wave pick");
        }

        return $tote;
    }

    public function getAll($input, $with)
    {
        $query = $this->getSearchQuery($input, $with);
        return $query->get();
    }

    public function getFirst($input, $with)
    {
        $query = $this->getSearchQuery($input, $with);
        return $query->first();
    }

    public function getSearchQuery($input, $with)
    {
        $input = SelArr::clean($input);

        $query = $this->make($with);

        if (!empty($input['whs_id'])){
            $query->where('whs_id', $input['whs_id']);
        }

        if (!empty($input['tote_num'])){
            $query->where('tote_num', $input['tote_num']);
        }

        return $query;
    }

    public function validateToteNum($whsId, $toteNum)
    {
        // return true;

        // $whsMeta = DB::table('whs_meta')->where([
        //                 'whs_qualifier' => self::WHS_TOTE_QUALIFIER,
        //                 'whs_id'        => $whsId
        //             ])->first();

        // if ( !$whsMeta ){
        //     throw new \Exception("Not exists tote in warehouse");
        // }

        // $whsMetaValue = json_decode($whsMeta['whs_meta_value']);

        $toteNum = explode('-', $toteNum);
        // $toteNum = end($toteNum);
        if ($toteNum[0] != "T") {
            throw new \Exception("Tote number is invalid.");
        }

        // if ( $whsMetaValue->max < (int)$toteNum ){
        //     throw new \Exception("Tote number not exits");
        // }
    }
}
