<?php
/**
 * Created by PhpStorm.
 * User: chicu
 * Date: 9/14/2016
 * Time: 8:47 AM
 */
namespace Seldat\Wms2\Utils;

use Illuminate\Support\Facades\DB;

class Helper
{

    const IN_TO_FT = 1728;
    protected static $_stMeasure;

    public static function calculateVolume($lenght, $width, $height)
    {
        //check validate params
        $res = $lenght * $width * $height;

        return $res;
    }

    public static function calculateCube($lenght, $width, $height)
    {
        //check validate params
        $volume = self::calculateVolume($lenght, $width, $height);
        $res = $volume / self::IN_TO_FT;

        return $res;
    }

    public static function cm2Inch($cm)
    {
        return $cm * 0.393701;
    }

    public static function inch2cm($inch)
    {
        return $inch * 2.54;
    }

    public static function kg2Pound($kg)
    {
        return $kg * 2.20462;
    }

    public static function pound2Kg($pound)
    {
        return $pound * 0.453592;
    }

    public static function convertWeight($value, $isMetric = false)
    {
        if ($isMetric) {
            return self::kg2Pound($value);
        }

        return self:: pound2Kg($value);
    }

    public static function convertLeghth($value, $isMetric = false)
    {
        if ($isMetric) {
            return self::cm2Inch($value);
        }

        return self::inch2cm($value);
    }

    /**
     * @param $data
     * @param $measureKind . array contain keys 'length-unit' and 'weight-unit'
     * @param $isMetric
     */
    public static function convertMeasure($data, $measureKind, $isMetric = false, $bySysMeasure = true)
    {
        if ($bySysMeasure) {
            $sysMeasure = self::getSettingsMeasure();
            if (($isMetric && $sysMeasure === 'CM') || !$isMetric && $sysMeasure === 'IN') {
                return $data;
            }
        }

        foreach ($data as $key => $value) {
            if (in_array($key, $measureKind['length-unit'])) {
                $data[$key] = self::convertLeghth($value, $isMetric);
            }

            if (in_array($key, $measureKind['weight-unit'])) {
                $data[$key] = self::convertWeight($value, $isMetric);
            }
        }

        return $data;
    }

    public static function getSettingsMeasure()
    {
        if (self::$_stMeasure) {
            return self::$_stMeasure;
        }
        $arrMeasure = [
            'metric system'   => 'CM',
            'imperial system' => 'IN'
        ];

        $measure = DB::table('settings')->select('setting_value')
            ->where('setting_key', 'MS')
            ->value('setting_value');
        $measure = \GuzzleHttp\json_decode($measure);
        $measure = strtolower($measure);
        self::$_stMeasure = $arrMeasure[$measure];

        return self::$_stMeasure;
    }
}