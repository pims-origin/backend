<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LocationHistogram extends Command
{
    protected $signature = 'location-histogram';
    protected $description = 'Location Histogram successfully.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            DB::beginTransaction();

            $locationUseds = DB::table('pallet')
                ->select([
                    DB::raw('COUNT(1) as loc_used'),
                    'whs_id'
                ])
                ->where('deleted', 0)
                ->whereNotNull('loc_id')
                ->groupBy('whs_id')->get();

            $locationTtl = DB::table('location')
                ->select([
                    DB::raw('COUNT(1) as loc_ttl'),
                    'loc_whs_id'
                ])
                ->where('deleted', 0)
                ->groupBy('loc_whs_id')->get();

            $locPalletByWhs = array_pluck($locationUseds, 'loc_used', 'whs_id');
            $locationByWhs = array_pluck($locationTtl, 'loc_ttl', 'loc_whs_id');
            $arrInsert = [];

            if ($locationByWhs) {

                foreach ($locationByWhs as $whsId => $loc_ttl) {
                    $loc_used_ttl = $locPalletByWhs[$whsId] ?? 0;

                    $arrInsert[] = [
                        'whs_id' => $whsId,
                        'loc_used_ttl' => $loc_used_ttl,
                        'loc_avai_ttl' => $loc_ttl - $loc_used_ttl,
                        'loc_ttl' => $loc_ttl,
                        'his_dt' => date('Y-m-d'),
                        'created_at' => time()
                    ];
                }

                DB::table('loc_his')->insert($arrInsert);
            }

            DB::commit();

        } catch (\PDOException $e) {
            dd($e->getMessage());
            DB::rollBack();

        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollBack();
        }

    }
}
