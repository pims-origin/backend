<?php
namespace App\Api\V4\Inbound\Pallet\ScanPallet\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class OrderCartonModel extends AbstractModel
{

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    public function autoPack($wvHdrId)
    {
        $result = DB::table('odr_hdr as o')
            ->join('odr_hdr_meta as ohm', 'ohm.odr_id', '=', 'o.odr_id')
            ->join('odr_cartons as oc', 'o.odr_id', '=', 'oc.odr_hdr_id')
            ->join('cartons as c', 'c.ctn_id', '=', 'oc.ctn_id')
            ->where('ohm.value', 1)
            ->where('o.odr_sts', 'PD')
            ->where('o.wv_id', $wvHdrId)
            ->groupBy('c.ctn_id')
            ->get();

        return $result;
    }

    public function sumPieceQtyOdrCtnByOdrDtlID($odrDtlId) {
        $sum = DB::table('odr_cartons')
            ->selectRaw('SUM(piece_qty) as piece_qty')
            ->where('odr_dtl_id', $odrDtlId)
            ->get();
        return $sum;
    }

    public function getOrderCT($odrDtlId) {

        $query = $this->model
            ->select( ['odr_num', 'wv_num', 'odr_dtl_id', 'odr_cartons.ctn_num', 'odr_cartons.ctn_id', 'rfid as ctn_rfid'])
            ->join('cartons', 'odr_cartons.ctn_id', '=', 'cartons.ctn_id')
            ->where('odr_dtl_id', $odrDtlId)
            ->groupBy('odr_cartons.ctn_id');

        return $query->get();
    }

}
