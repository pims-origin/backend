<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseStatus;

class WarehouseStatusTransformer extends TransformerAbstract
{
    /**
     * @param WarehouseStatus $warehouseStatus
     *
     * @return array
     */
    public function transform(WarehouseStatus $warehouseStatus)
    {
        return [
            'whs_sts_code' => $warehouseStatus->whs_sts_code,
            'whs_sts_name' => $warehouseStatus->whs_sts_name,
            'whs_sts_desc' => $warehouseStatus->whs_sts_desc,
        ];
    }
}