<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 1-Sep-16
 * Time: 09:25
 */

namespace App\Api\V2\BillOfLadings\UpdateBOL\Models;

use Seldat\Wms2\Models\OutPallet;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;


/**
 * Class OutPalletModel
 *
 * @package App\Api\V1\Models
 */
class OutPalletModel extends AbstractModel
{
    /**
     * @param OutPallet $model
     */
    public function __construct(OutPallet $model = null)
    {
        $this->model = ($model) ?: new OutPallet();
    }

    /**
     * Generate new wo number
     *
     * @return string
     */
    public function generateLPNNum()
    {
        $currentYearMonth = date('ym');
        $defaultWoNum = "LPN-${currentYearMonth}-00001";

        $lastPL = $this->model->orderBy('plt_id', 'desc')->first();
        $lastLPN = object_get($lastPL, 'plt_num', '');

        if (empty($lastLPN) || strpos($lastLPN, "-${currentYearMonth}-") === false) {
            return $defaultWoNum;
        }

        return ++$lastLPN;
    }

    public function getOutPalletByOdrHdrId($odrHdrId, $limit = null)
    {
        $query = $this->model
            ->select([
                'pack_hdr.odr_hdr_id',
                'out_pallet.plt_id',
                'out_pallet.loc_id',
                'out_pallet.loc_code',
                'out_pallet.loc_name',
                'out_pallet.cus_id',
                'out_pallet.whs_id',
                'out_pallet.plt_num',
                'out_pallet.plt_block',
                'out_pallet.plt_tier',
                'out_pallet.ctn_ttl',
                'out_pallet.is_movement',
                'out_pallet.out_plt_sts',
                'out_pallet.created_at'
            ])
            ->join('pack_hdr', 'pack_hdr.out_plt_id', '=', 'out_pallet.plt_id')
            ->where('pack_hdr.odr_hdr_id', $odrHdrId)
            ->where('out_pallet.deleted', 0)
            ->where('out_pallet.out_plt_sts', 'AC');

        if ($limit) {
            $models = $query->paginate($limit);
        } else {
            $models = $query->get();
        }

        return $models;
    }

    public function editOutPallet($location, $pltId)
    {
        $result = $this->model
            ->where('plt_id', $pltId)
            ->where('whs_id', $location->loc_whs_id)
            ->update([
                'loc_id' => $location->loc_id,
                'loc_code' => $location->loc_code,
                'loc_name' => $location->loc_alternative_name
            ]);

        return $result;

    }

    public function updateOutPalletWithBOL($bolId, $odrId, $whsId)
    {
        $sqlPltIds = "plt_id in (select out_plt_id from pack_hdr where odr_hdr_id = {$odrId} and whs_id = {$whsId} and deleted = 0)";
        $query = $this->model
            ->whereRaw(\DB::raw($sqlPltIds))
            ->update([
                    'parent'     => \DB::raw('plt_id'),
                    'bol_id'     => $bolId,
                    'updated_at' => time(),
                    'updated_by' => Data::getCurrentUserId(),
                ]);
    }

}
