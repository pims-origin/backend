<?php

namespace App\Api\V2\Outbound\Wave\GetWavePickDetail\Controllers;

use App\Api\V2\Outbound\Wave\GetWavePickDetail\Models\CartonModel;
use App\Api\V2\Outbound\Wave\GetWavePickDetail\Models\OrderCartonModel;
use App\Api\V2\Outbound\Wave\GetWavePickDetail\Models\LocationModel;
use App\Api\V2\Outbound\Wave\GetWavePickDetail\Models\Log;
use App\Api\V2\Outbound\Wave\GetWavePickDetail\Models\OrderDtlModel;
use App\Api\V2\Outbound\Wave\GetWavePickDetail\Models\OrderHdrModel;
use App\Api\V2\Outbound\Wave\GetWavePickDetail\Models\WavePickDtlModel;
use App\Api\V2\Outbound\Wave\GetWavePickDetail\Models\WavePickHdrModel;
use App\MyHelper;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Seldat\Wms2\Utils\Status;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;

class WavePickController extends AbstractController
{

    const STATUS_ALLOCATE_LOCATION         = 'AL';
    const WAREHOUSE_ALLOCATE_LOCATION_CODE = 'wle';
    const SYSTEM_UOM_CODE_PIECE            = 'PC';

    /**
     * @var Wave pick header
     */
    protected $waveHdrModel;

    /**
     * @var Wave pick detail
     */
    protected $waveDtlModel;

    /**
     * @var OrderHdrModel
     */
    protected $odrHdr;
    protected $odrCtn;

    /**
     * @var Order Detail
     */
    protected $odrDtl;
    protected $ctnModel;

    /**
     * @var Location
     */
    protected $loc;

    protected $STR_PAD_LEFT = 0;
    protected $userId;

    /**
     * WavePickController constructor.
     */
    public function __construct()
    {
        $this->waveHdrModel = new WavePickHdrModel();
        $this->waveDtlModel = new WavePickDtlModel();
        $this->odrHdr = new OrderHdrModel();
        $this->odrDtl = new OrderDtlModel();
        $this->loc = new LocationModel();
        $this->ctnModel = new CartonModel();
        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    public function getSuggestPalletLocation($whsId = false, $wvDtlID = false, Request $request)
    {
        $input = $request->getParsedBody();

        $limit = array_get($input, 'limit', 1);
        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/wave-detail/sku/{$wvDtlID}";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'SPL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get suggest pallet location'
        ]);
        /*
         * end logs
         */

        if (!$whsId || !$wvDtlID) {
            return $this->response->errorNotFound();
        }
        //1. select sug_loc_ids where wv_dtl_id = $wvDtlID on table wv_dt_loc
        /*$location = $this->waveDtlLocModel->getFirstWhere(['wv_dtl_id' => $wvDtlID]);
        if (empty($location)) {
            $msg = sprintf('This wv_dtl_id %s is not exists', $wvDtlID);

            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }*/

        //2. select * from wv_dtl where wv_dt_id =  $wvDtlID,

        try{

            DB::beginTransaction();
            $detail = $this->waveDtlModel->getFirstWhere([
                'wv_dtl_id' => $wvDtlID,
                'whs_id'    => $whsId]
            );

            if (!$detail) {
                $msg = sprintf("This Wavepick detail id: %s doesn't exist", $wvDtlID);
                $data = [
                    'data'    => $wvDtlID,
                    'message' => $msg,
                    'status'  => false,
                ];
                return new Response($data, 200, [], null);
            }

            $actualWvTtl = (new OrderCartonModel())->findWhere(['wv_dtl_id' => $wvDtlID])->count();
            $data = [];
            $t1 = floor($detail->piece_qty / $detail->pack_size);
            $t2 = floor($detail->act_piece_qty / $detail->pack_size);
            $wv_hdr = $this->waveHdrModel->getFirstBy('wv_id', $detail->wv_id);

            $getWvDtlSts = $detail->wv_dtl_sts;
            $getWvId     = $detail->wv_id;
            $getItemId   = $detail->item_id;
            $getLot      = $detail->lot;
            $isProcessing = $this->_isProcessing($whsId, $getWvId, $getItemId, $getLot);
            $qtyLimit     = $detail->piece_qty;
            $qtyCurrent   = $detail->act_piece_qty;
            $itemArr = DB::table('odr_dtl')->where('item_id', $detail->item_id)
                ->select([
                    'system_uom.sys_uom_id as uom_id',
                    'system_uom.sys_uom_code as uom_code',
                ])
                ->join('system_uom', 'system_uom.sys_uom_id', '=', 'odr_dtl.uom_id')
                ->where('odr_dtl.deleted', 0)
                ->where('odr_dtl.wv_id', $detail->wv_id)
                ->first();
            $isPickCtn    = array_get($itemArr, 'uom_code') == self::SYSTEM_UOM_CODE_PIECE ? false : true;

            $detail->load('waveHdr.orderHdr');
            $numOdr = count($detail->waveHdr->orderHdr);
            $cusPo = '';
            if ($numOdr == 1){
                $cusPo = $detail->waveHdr->orderHdr[0]->cus_po;
            }
            if ($numOdr > 1){
                $cusPo = 'multiple';
            }

            $dataWvDtl = [
                'wv_dtl_id'        => $detail->wv_dtl_id,
                'wv_id'            => $getWvId,
                'item_id'          => $getItemId,
                'wv_num'           => $detail->wv_num,
                'color'            => $detail->color,
                'sku'              => $detail->sku,
                'size'             => $detail->size,
                'pack_size'        => $detail->pack_size,
                'lot'              => $detail->lot,
                'wv_dtl_sts'       => $detail->wv_dtl_sts,
                'wv_sts'           => $wv_hdr->wv_sts,
                'allocate_cartons' => $t1 . '/' . $t2,
                'allocate_pieces'  => ceil($detail->piece_qty . '/' . $detail->act_piece_qty),
                'sts_destination'  => $isProcessing,
                'qty_limit'        => $qtyLimit,
                'qty_current'      => $qtyCurrent,
                'ctn_limit'        => $t1,
                'ctn_current'      => $t2,
                'is_pick_ctn'      => $isPickCtn,
                'cus_po'           => $cusPo
            ];

            $loc = [];
//            $getLocId = object_get($detail, 'primary_loc_id');
            $getLocId = null;
            if ($getLocId) {
                $locArr = $this->ctnModel->getSugLocByLocId($whsId, $detail, $getLocId);
                if (count($locArr)) {
                    $loc[] = [
                        'loc_id'         => $getLocId,
                        'loc_code'       => array_get($locArr, 'loc_code', null),
                        'loc_rfid'       => array_get($locArr, 'loc_rfid', null),
                        'sku'            => array_get($locArr, 'sku', null),
                        'lot'            => array_get($locArr, 'lot', null),
                        'cartons'        => array_get($locArr, 'cartons', null),
                        'pieces'         => array_get($locArr, 'avail_qty', null),
                        'is_cycle_count' => $this->_isCycleCount($whsId, $getLocId),
                        'is_rfid'        => $this->_isRfid($whsId, $getLocId),
                    ];

                    // WAP-763 - [Wavepick] Store suggest location by wavepick
                    $dataLoc = [
                        'loc_id'   => $getLocId,
                        'loc_code' => array_get($locArr, 'loc_code', null),
                        'ctns'     => array_get($locArr, 'cartons', 0),
                        'qty'      => array_get($locArr, 'avail_qty', 0),
                    ];
                    $this->_insertWaveSugLoc($detail, $dataLoc);
                }
            }

            if (!$getLocId || !count($loc)) {
                // not exist suggest location
                $whsMeta = DB::table('whs_meta')
                    ->where('whs_id', $whsId)
                    ->where('whs_qualifier', self::WAREHOUSE_ALLOCATE_LOCATION_CODE)
                    ->first();
                $releasedPeriod = array_get($whsMeta, 'whs_meta_value', 0) * 60; //second

                $locIds = [];
                $locObjs = $this->ctnModel->getMoreSugLocByWvDtl($whsId, $detail, $locIds, $limit, $isPickCtn);

                foreach ($locObjs as $locObj) {
                    $getLocId = array_get($locObj, 'loc_id', null);
                    $getLocCode = array_get($locObj, 'loc_code', null);
                    $loc[] = [
                        'loc_id'         => $getLocId,
                        'loc_code'       => $getLocCode,
                        'loc_rfid'       => array_get($locObj, 'loc_rfid', null),
                        'sku'            => array_get($locObj, 'sku', null),
                        'lot'            => array_get($locObj, 'lot', null),
                        'cartons'        => array_get($locObj, 'cartons', null),
                        'pieces'         => array_get($locObj, 'avail_qty', null),
                        'is_cycle_count' => $this->_isCycleCount($whsId, $getLocId),
                        'is_rfid'        => $this->_isRfid($whsId, $getLocId),
                    ];

                    // WAP-763 - [Wavepick] Store suggest location by wavepick
                    $dataLoc = [
                        'loc_id'   => $getLocId,
                        'loc_code' => array_get($locObj, 'loc_code', null),
                        'ctns'     => array_get($locObj, 'cartons', 0),
                        'qty'      => array_get($locObj, 'avail_qty', 0),
                    ];
                    $this->_insertWaveSugLoc($detail, $dataLoc);

                    $userId = Data::getCurrentUserId();
                    // WAP-705 - [Outbound - Wavepick] Update API to get list of locaiton in wavepick detail
                    // Allocate location
                    $this->loc->getModel()
                        ->where('loc_id', $getLocId)
                        ->update([
                            'reserved_at' => time() + $releasedPeriod,
                            'updated_by'   => $userId,
                            'updated_at'   => time(),
                        ]);

                    $this->waveDtlModel->getModel()
                        ->where('wv_dtl_id', $wvDtlID)
                        ->update([
                            'primary_loc_id' => $getLocId,
                            'primary_loc'    => $getLocCode,
                            'updated_by'     => $userId,
                            'updated_at'     => time(),
                        ]);
                }
            }

            DB::commit();

            $data['wv_dtl'] = $dataWvDtl;
            $data['location'] = $loc;
            $data['histories'] = $this->getHistorySKUs($detail->wv_id);

            $array['data'] = $data;

            return new Response($array, 200, [], null);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function getHistorySKUs($wvHdrID)
    {
        $wvDtls = $this->waveDtlModel->findWhere(['wv_id' => $wvHdrID]);
        $data = [];
        if (!is_null($wvDtls)) {
            foreach ($wvDtls as $wvDtl) {
                $item = [];
                $item['sku'] = $wvDtl->sku;
                $item['color'] = $wvDtl->color;
                $item['size'] = $wvDtl->size;
                $item['pack_size'] = $wvDtl->pack_size;
                $t1 = $wvDtl->piece_qty / $wvDtl->pack_size;
                $t2 = $wvDtl->act_piece_qty / $wvDtl->pack_size;
                $item['allocate_cartons'] = $t1 . '/' . $t2;
                $item['allocate_pieces'] = $wvDtl->piece_qty . '/' . $wvDtl->act_piece_qty;
                $data[] = $item;
            }
        }

        return $data;
    }

    public function getNextSKU($whsId, $wv_id, $current_wv_dtl_id, Request $request) {

        $input = $request->getParsedBody();

        $limit = array_get($input, 'limit', 1);
        $pickerId = Data::getCurrentUserId();
        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/wave-detail/{$wv_id}/next-sku/{$current_wv_dtl_id}";
        $owner = "";
        $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GNS',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get next SKU'
        ]);
        /*
         * end logs
         */

        try {
            DB::beginTransaction();

            // WAP-774 - [Wavepick] Limit wavepick list by picker
            $detail = $this->waveDtlModel->getNextWvDtl($wv_id, $current_wv_dtl_id, $whsId, $pickerId);

            if (!$detail) {
                $msg = null;
                $detail = $this->waveDtlModel->getFirstWhere([
                                                    'wv_dtl_id' => $current_wv_dtl_id,
                                                    'wv_id'     => $wv_id,
                                                    'whs_id'    => $whsId,
                                                    ]);
                if (!$detail) {
                    $detail = $this->waveDtlModel->getFirstWhere(['wv_dtl_id' => $current_wv_dtl_id]);
                    $wvNum = object_get($detail, 'wv_num', null);
                    if (!$detail && !$msg) {
                        $msg = sprintf("Wavepick detail doesn't exist!");
                    } else {
                        $detail = $this->waveDtlModel->getFirstWhere(['wv_id' => $wv_id]);
                        if (!$detail && !$msg) {
                            $msg = sprintf("Wavepick doesn't exist!");
                        }
                    }

                    $detail = $this->waveDtlModel->getFirstWhere([
                                                        'wv_dtl_id' => $current_wv_dtl_id,
                                                        'wv_id' => $wv_id,
                                                        ]);
                    if (!$detail && !$msg) {
                        $msg = sprintf("Wavepick detail id: %s doesn't belong to Wavepick num: %s!",
                            $current_wv_dtl_id,
                            $wvNum);
                    }

                    if (!$msg) {
                        $msg = sprintf("Wavepick detail id: %s and Wavepick num: %s don't belong to Wavehouse!",
                            $current_wv_dtl_id,
                            $wvNum);
                    }
                    $data = [
                        'data' => [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false
                        ]
                    ];

                    return new Response($data, 200, [], null);
                }

                $res =  ['data' => [
                    'locations' => [],
                    'wp_dtl_ttl' => new WavePickDtlModel(),
                    'wv_dtl' => null
                ]];

                return $res;
            }

            $wv_hdr = $this->waveHdrModel->getFirstBy('wv_id', $detail->wv_id);
            if (!$wv_hdr) {
                $msg = sprintf("Wavepick is not existed!");
                $data = [
                    'data' => [
                        'data'    => null,
                        'message' => $msg,
                        'status'  => false,
                    ]
                ];

                return new Response($data, 200, [], null);
            }

            $countWPDetails = $this->waveDtlModel->getModel()->where('wv_id', $detail->wv_id)->whereIn('wv_dtl_sts', ['NW', 'PK'])->count();
            $t1 = floor($detail->piece_qty / $detail->pack_size);
            $t2 = floor($detail->act_piece_qty / $detail->pack_size);

            $getWvDtlSts = $detail->wv_dtl_sts;
            $getWvId     = $detail->wv_id;
            $getItemId   = $detail->item_id;
            $getLot      = $detail->lot;
            $isProcessing = $this->_isProcessing($whsId, $getWvId, $getItemId, $getLot);
            $qtyLimit     = $detail->piece_qty;
            $qtyCurrent   = $detail->act_piece_qty;
            $itemArr = DB::table('odr_dtl')->where('item_id', $detail->item_id)
                ->select([
                    'system_uom.sys_uom_id as uom_id',
                    'system_uom.sys_uom_code as uom_code',
                ])
                ->join('system_uom', 'system_uom.sys_uom_id', '=', 'odr_dtl.uom_id')
                ->where('odr_dtl.deleted', 0)
                ->where('odr_dtl.wv_id', $detail->wv_id)
                ->first();

            $isPickCtn    = array_get($itemArr, 'uom_code') == self::SYSTEM_UOM_CODE_PIECE ? false : true;

            $wv_hdr->load('orderHdr');
            $numOdr = count($wv_hdr->orderHdr);
            if ($numOdr == 1){
                $cusPo = $wv_hdr->orderHdr[0]->cus_po;
            }
            if ($numOdr > 1){
                $cusPo = 'multiple';
            }


            $dataWvDtl = [
                'wv_dtl_id'        => $detail->wv_dtl_id,
                'wv_id'            => $getWvId,
                'item_id'          => $getItemId,
                'wv_num'           => $detail->wv_num,
                'color'            => $detail->color,
                'sku'              => $detail->sku,
                'size'             => $detail->size,
                'pack_size'        => $detail->pack_size,
                'lot'              => $detail->lot,
                'wv_dtl_sts'       => $detail->wv_dtl_sts,
                'wv_sts'           => $wv_hdr->wv_sts,
                'allocate_cartons' => $t1 . '/' . $t2,
                'allocate_pieces'  => ceil($detail->piece_qty . '/' . $detail->act_piece_qty),
                'sts_destination'  => $isProcessing,
                'qty_limit'        => $qtyLimit,
                'qty_current'      => $qtyCurrent,
                'ctn_limit'        => $t1,
                'ctn_current'      => $t2,
                'is_pick_ctn'      => $isPickCtn,
                'cus_po'           => $cusPo
            ];

            if($detail){
                $loc = [];
                $getLocId = object_get($detail, 'primary_loc_id');
                if ($getLocId) {
                    $locArr = $this->ctnModel->getSugLocByLocId($whsId, $detail, $getLocId);
                    if (count($locArr)) {
                        $loc[] = [
                            'loc_id'         => $getLocId,
                            'loc_code'       => array_get($locArr, 'loc_code', null),
                            'loc_rfid'       => array_get($locArr, 'loc_rfid', null),
                            'sku'            => array_get($locArr, 'sku', null),
                            'lot'            => array_get($locArr, 'lot', null),
                            'cartons'        => array_get($locArr, 'cartons', null),
                            'pieces'         => array_get($locArr, 'avail_qty', null),
                            'is_cycle_count' => $this->_isCycleCount($whsId, $getLocId),
                            'is_rfid'        => $this->_isRfid($whsId, $getLocId),
                        ];

                        // WAP-763 - [Wavepick] Store suggest location by wavepick
                        $dataLoc = [
                            'loc_id'   => $getLocId,
                            'loc_code' => array_get($locArr, 'loc_code', null),
                            'ctns'     => array_get($locArr, 'cartons', 0),
                            'qty'      => array_get($locArr, 'avail_qty', 0),
                        ];
                        $this->_insertWaveSugLoc($detail, $dataLoc);
                    }
                }

                if (!$getLocId || !count($loc)) {
                    $whsMeta = DB::table('whs_meta')
                        ->where('whs_id', $whsId)
                        ->where('whs_qualifier', self::WAREHOUSE_ALLOCATE_LOCATION_CODE)
                        ->first();
                    $releasedPeriod = array_get($whsMeta, 'whs_meta_value', 0) * 60; //second

                    $locIds = [];
                    $result = $this->ctnModel->getMoreSugLocByWvDtl($whsId, $detail, $locIds, $limit, $isPickCtn);
                    foreach ($result as $locObj) {
                        $getLocId = array_get($locObj, 'loc_id', null);
                        $getLocCode = array_get($locObj, 'loc_code', null);
                        $loc[] = [
                            'loc_id'         => $getLocId,
                            'loc_code'       => array_get($locObj, 'loc_code', null),
                            'loc_rfid'       => array_get($locObj, 'loc_rfid', null),
                            'sku'            => array_get($locObj, 'sku', null),
                            'lot'            => array_get($locObj, 'lot', null),
                            'cartons'        => array_get($locObj, 'cartons', null),
                            'pieces'         => array_get($locObj, 'avail_qty', null),
                            'is_cycle_count' => $this->_isCycleCount($whsId, $getLocId),
                            'is_rfid'        => $this->_isRfid($whsId, $getLocId),

                        ];

                        // WAP-763 - [Wavepick] Store suggest location by wavepick
                        $dataLoc = [
                            'loc_id'   => $getLocId,
                            'loc_code' => array_get($locObj, 'loc_code', null),
                            'ctns'     => array_get($locObj, 'cartons', 0),
                            'qty'      => array_get($locObj, 'avail_qty', 0),
                        ];
                        $this->_insertWaveSugLoc($detail, $dataLoc);

                        $userId = Data::getCurrentUserId();
                        // WAP-705 - [Outbound - Wavepick] Update API to get list of locaiton in wavepick detail
                        // Allocate location
                        $this->loc->getModel()
                            ->where('loc_id', $getLocId)
                            ->update([
                                'reserved_at' => time() + $releasedPeriod,
                                'updated_by'   => $userId,
                                'updated_at'   => time(),
                            ]);

                        $this->waveDtlModel->getModel()
                            ->where('wv_dtl_id', $detail->wv_dtl_id)
                            ->update([
                                'primary_loc_id' => $getLocId,
                                'primary_loc'    => $getLocCode,
                                'updated_by'     => $userId,
                                'updated_at'     => time(),
                            ]);

                    }
                }

                DB::commit();

                $res =  ['data' => [
                    'wv_dtl' => $dataWvDtl,
                    'location' => $loc,
                    'wp_dtl_ttl' => $countWPDetails,
                    'histories' => $this->getHistorySKUs($detail->wv_id)
                ]];
                return $res;
            }

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _isCycleCount($whsId, $locId)
    {
        return false;
        // $query = DB::table('cc_notification')->where([
        //             'whs_id'  => $whsId,
        //             'loc_id'  => $locId,
        //             'deleted' => 0,
        //         ])
        //         ->whereIn('cc_ntf_sts', ['NW'])
        //         ->first();

        // return $query ? true : false;
    }

    private function _isRfid($whsId, $locId)
    {
        $query = DB::table('cartons')->where([
                    'whs_id'  => $whsId,
                    'loc_id'  => $locId,
                    'deleted' => 0,
                ])
                ->whereNotNull('rfid')
                ->where('ctn_sts', '!=', 'AJ')
                ->first();

        return $query ? true : false;
    }

    private function _isProcessing($whsId, $wvId, $itemId, $lot)
    {
        $query = DB::table('odr_hdr')
                    ->join('vas_hdr', 'odr_hdr.odr_id', '=', 'vas_hdr.odr_hdr_id')
                    ->where([
                    'odr_hdr.whs_id'  => $whsId,
                    'odr_hdr.wv_id'   => $wvId,
                    // 'wo_dtl.item_id'  => $itemId,
                    // 'wo_dtl.lot'      => $lot,
                    'odr_hdr.deleted' => 0,
                    'vas_hdr.deleted'  => 0,
                ])
                ->first();

        return $query ? "Work Order" : "Processing";
    }

    private function _insertWaveSugLoc($wvDtl, $dataLoc)
    {
        $data = [
        'wv_dtl_id'  => $wvDtl['wv_dtl_id'],
        'wv_id'      => $wvDtl['wv_id'],
        'whs_id'     => $wvDtl['whs_id'],
        'item_id'    => $wvDtl['item_id'],
        'sku'        => $wvDtl['sku'],
        'size'       => $wvDtl['size'],
        'color'      => $wvDtl['color'],
        'desc'       => '',
        'pack'       => $wvDtl['pack_size'],
        'lot'        => $wvDtl['lot'],
        'ctns'       => $dataLoc['ctns'],
        'qty'        => $dataLoc['qty'],
        'loc_id'     => $dataLoc['loc_id'],
        'loc_code'   => $dataLoc['loc_code'],
        'created_at' => time(),
    ];
        return DB::table('sug_loc')->insert($data);
    }
}
