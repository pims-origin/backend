<?php

namespace App\Api\V1\Validators;

class MasterDataValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'md_code' => 'required',
            'md_type' => 'required|exists:master_data_type,md_type',
            'md_name' => 'required',
            'md_des'  => 'required',
            'md_sts'  => 'required',
        ];
    }
}
