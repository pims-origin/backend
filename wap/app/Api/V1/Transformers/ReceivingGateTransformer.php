<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\VirtualCartonSumModel;
use League\Fractal\TransformerAbstract;

class ReceivingGateTransformer extends TransformerAbstract
{
    //return result
    public function transform(VirtualCartonSumModel $vtlCtnSum)
    {
        return [
            'gate_code'               => array_get($vtlCtnSum, 'gate_code', null)
        ];
    }
}
