<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnStatus;
use Seldat\Wms2\Models\VirtualCarton;

class AsnscanStatusTransformer extends TransformerAbstract
{

    /**
     * ps:Show ASN status
     * @param AsnStatus $asnStatus
     *
     * @return array
     */
    public function transform(AsnDtl $asnDtl)
    {
        return [
            "item_id" =>object_get($asnDtl, 'item_id', 0),
            "sku"  => object_get($asnDtl, "item.sku", ""),
            "size"  => object_get($asnDtl, "item.size", ""),
            "color"  => object_get($asnDtl, "item.color", ""),
            'ctn_ttl' => object_get($asnDtl, "asn_dtl_ctn_ttl", 0),
            'rfid' => object_get($asnDtl, "rfid", null),

        ];
    }
}
