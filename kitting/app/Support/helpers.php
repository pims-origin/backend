<?php

if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if ( ! function_exists('resource_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function resource_path($path = '')
    {
        return app()->basePath() . '/resources' . ($path ? '/' . $path : $path);
    }
}

if ( ! function_exists('escape_like'))
{
    /**
     * escape like query
     *
     * @param  string $path
     * @return string
     */
    function escape_like($string)
    {
        $search = array('%', '_');
        $replace   = array('\%', '\_');
        return str_replace($search, $replace, $string);
    }
}
if (!function_exists('getDefaultDatetimeDeletedAt')) {
    function getDefaultDatetimeDeletedAt()
    {
        return \Illuminate\Support\Carbon::createFromFormat('Y-m-d H:i:s', '1999-01-01 00:00:00', 'UTC')->timestamp;
    }
}