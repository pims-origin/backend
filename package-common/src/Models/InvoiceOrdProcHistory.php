<?php

namespace Seldat\Wms2\Models;


class InvoiceOrdProcHistory extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inv_his_ord_prc';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'inv_id',
        'inv_num',
        'cus_id',
        'whs_id',
        'ord_id',
        'ord_num',
        'inv_sts'
    ];

    public function orderHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\OrderHdr', 'odr_num', 'odr_num');
    }
    
    public function invoiceHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceDetail', 'inv_id', 'inv_id');
    }
}

