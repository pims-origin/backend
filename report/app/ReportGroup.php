<?php

namespace App;

use App\Utils\Database\Eloquent\SoftDeletes;

class ReportGroup extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'report_group';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'grp_id';

    /**
     * @var array
     */
    protected $fillable = [
        'desc',
        'grp_sts',
        'sts',
        'disp_odr',
    ];

    public function report()
    {
        return $this->hasMany(__NAMESPACE__ . '\Report', 'grp_id', 'grp_id');
    }
}
