<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\ReportTransformer;
use App\Api\V1\Models\ReportModel;
use App\Api\V1\Models\CustomersModel;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Utils\Export;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelExport;

class ReportController extends AbstractController
{
    protected $reportModel;

    protected $transformer;

    protected $customersModel;

    /**
     * Init.
     *
     */
    public function __construct()
    {
        $this->reportModel = new ReportModel();
        $this->transformer = new ReportTransformer();
        $this->customersModel = new CustomersModel();
    }

    /**
     * @SWG\Get(
     *     path="/report/search",
     *     tags={"Invoice Report"},
     *     summary="Search Invoice Report",
     *     description="Search Invoice Report",
     *     operationId="getInvoiceIndex",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cus_id",
     *         in="query",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="whs_id",
     *         in="query",
     *         description="ID of Warehouse",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="inv_num",
     *         in="query",
     *         description="Inv Num",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="inv_sts",
     *         in="query",
     *         description="Invoice Status",
     *         enum={
     *             "RE", "PA", "NE", "PE", "AP"
     *         },
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="issue_dt",
     *         in="query",
     *         description="Invoice Issue Date",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Limit",
     *         type="integer",
     *         default="20",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page",
     *         type="integer",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Invoice",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */    
    /**
     * Display a listing of the resource.
     * @param IRequest $request
     * @return mixed
     */
    public function search(IRequest $request)
    {
        $input = $request->getQueryParams();

        try {
            // get list Invocie Header
            $invoiceReport = $this->reportModel->search($input,[], array_get($input, 'limit'));

            return $this->response->paginator($invoiceReport, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param IRequest $request
     *
     * @return array|void
     */
    public function agingSearch(IRequest $request)
    {
        $input = $request->getQueryParams();

        try {
            // get list Invocie Header
            //days: 0 -> 30
            //$invoiceReportThirty = $this->reportModel->agingSearchThirty($input, []);
            $invoiceReportThirty = $this->reportModel->agingSearch($input, [], 0, 30);
            $numOfInvoiceThirty = array_get($invoiceReportThirty, 'num_of_invoice', 0);
            $amountThirty = array_get($invoiceReportThirty, 'amount', 0)?array_get($invoiceReportThirty, 'amount',
                0):0;
            $invoiceReportThirtyDetails = $this->reportModel->agingSearchThirtyDetails($input, [], 0, 30);

            //days: 31 -> 60
            //$invoiceReportSixty = $this->reportModel->agingSearchSixty($input, []);
            $invoiceReportSixty = $this->reportModel->agingSearch($input, [], 31, 60);
            $numOfInvoiceSixty = array_get($invoiceReportSixty, 'num_of_invoice', 0);
            $amountSixty = array_get($invoiceReportSixty, 'amount', 0)? array_get($invoiceReportSixty, 'amount',
                0):0;
            $invoiceReportSixtyDetails = $this->reportModel->agingSearchThirtyDetails($input, [], 31, 60);

            //days: 61 -> 90
            //$invoiceReportNinety = $this->reportModel->agingSearchNinety($input, []);
            $invoiceReportNinety = $this->reportModel->agingSearch($input, [], 61, 90);
            $numOfInvoiceNinety = array_get($invoiceReportNinety, 'num_of_invoice', 0);
            $amountNinety = array_get($invoiceReportNinety, 'amount', 0)?array_get($invoiceReportNinety, 'amount',
                0):0;
            $invoiceReportNinetyDetails = $this->reportModel->agingSearchThirtyDetails($input, [], 61, 90);

            //days: over 90
            //$invoiceReportOverNinety = $this->reportModel->agingSearchOverNinety($input, []);
            $invoiceReportOverNinety = $this->reportModel->agingSearch($input, [], 91, 0);
            $numOfInvoiceOverNinety = array_get($invoiceReportOverNinety, 'num_of_invoice', 0);
            $amountOverNinety = array_get($invoiceReportOverNinety, 'amount', 0)?array_get
            ($invoiceReportOverNinety, 'amount', 0):0;
            $invoiceReportOverNinetyDetails = $this->reportModel->agingSearchThirtyDetails($input, [], 91, 0);

            //Total
            //$invoiceReportTotal = $this->reportModel->agingSearchTotal($input, []);
            $invoiceReportTotal = $this->reportModel->agingSearch($input, [], 0, 0);
            $numOfInvoiceTotal = array_get($invoiceReportTotal, 'num_of_invoice', 0);
            $amountTotal = array_get($invoiceReportTotal, 'amount', 0)?array_get($invoiceReportTotal, 'amount',
                0):0;


            $data = [
                'numOfInvoiceThirty' => $numOfInvoiceThirty,
                'amountThirty'       => $amountThirty,

                'numOfInvoiceSixty' => $numOfInvoiceSixty,
                'amountSixty'       => $amountSixty,

                'numOfInvoiceNinety' => $numOfInvoiceNinety,
                'amountNinety'       => $amountNinety,

                'numOfInvoiceOverNinety' => $numOfInvoiceOverNinety,
                'amountOverNinety'       => $amountOverNinety,

                'numOfInvoiceTotal' => $numOfInvoiceTotal,
                'amountTotal'       => $amountTotal,

                'invoiceReportThirtyDetails'        => $invoiceReportThirtyDetails,
                'invoiceReportSixtyDetails'         => $invoiceReportSixtyDetails,
                'invoiceReportNinetyDetails'        => $invoiceReportNinetyDetails,
                'invoiceReportOverNinetyDetails'    => $invoiceReportOverNinetyDetails
            ];

            if (!empty($input['export']) && $input['export'] == 1) {
                $this->agingExport($input, $data);
                die;
            }

            return ['data' => $data];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $input
     * @param $data
     */
    private function agingExport(
        $input,
        $data
    ) {
        //date_default_timezone_set(config('constants.timezone_utc_zero'));
        $cus_id = array_get($input, 'cus_id', '');
        try {

            $ext = 'xls';
            Excel::create('InvoiceAgingReport', function ($excel) use (
                $data
            ) {
                // Set sheet 1
                $excel->sheet('Invoice Aging Report', function ($sheet) use (
                    $data
                ) {
                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Arial',
                            'size' => 11,
                            'bold' => false,
                        ],
                    ])
                        ->row(1, array(
                            'Summary Aging Invoice Report',
                        ))
                        ->mergeCells('A1:C1')
                        ->cells('A1:C1', function ($cells) {
                            $cells->setFontSize(14)
                                ->setFontColor('#2e3192')
                                ->setFontWeight('bold');
                        })
                        ->row(3, [
                            '30',
                            '',
                            '60',
                            '',
                            '90',
                            '',
                            'Over 90',
                            '',
                            'Total',
                            '',

                        ])
                        ->cells('A3:J4', function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontWeight('bold');
                        })
                        ->mergeCells('A3:B3')
                        ->mergeCells('C3:D3')
                        ->mergeCells('E3:F3')
                        ->mergeCells('G3:H3')
                        ->mergeCells('I3:J3')
                        ->row(4, [
                            '#Invoice',
                            'Amount',
                            '#Invoice',
                            'Amount',
                            '#Invoice',
                            'Amount',
                            '#Invoice',
                            'Amount',
                            '#Invoice',
                            'Amount',
                        ])
                        ->row(5, [
                            array_get($data, 'numOfInvoiceThirty', 0),
                            array_get($data, 'amountThirty', 0),

                            array_get($data, 'numOfInvoiceSixty', 0),
                            array_get($data, 'amountSixty', 0),

                            array_get($data, 'numOfInvoiceNinety', 0),
                            array_get($data, 'amountNinety', 0),

                            array_get($data, 'numOfInvoiceOverNinety', 0),
                            array_get($data, 'amountOverNinety', 0),

                            array_get($data, 'numOfInvoiceTotal', 0),
                            array_get($data, 'amountTotal', 0),

                        ])
                        ->setBorder('A3:J5', 'thin')
                        ->cells('A1:J2', function ($cells) {
                            $cells->setFontWeight('100');//or bold
                        })
                        ->cells('A5:J5', function ($cells) {
                            $cells->setAlignment('right');
                        })
                        ->cells('A3:J4', function ($cells) {
                            $cells->setAlignment('center');
                        });

                });

                // Set sheet 2
                $excel->sheet('Aging Invoice Details Report', function ($sheet) use (
                    $data
                ) {
                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Arial',
                            'size' => 11,
                            'bold' => false,
                        ],
                    ])
                        ->row(1, [
                            ' Aging Invoice Details Report '
                        ])
                        ->mergeCells('A1:C1')
                        ->cells('A1:C1', function ($cells) {
                            $cells->setFontSize(14)
                                ->setFontColor('#2e3192')
                                ->setFontWeight('bold');
                        })
                        ->cells('A3:E3', function ($cells) {
                            $cells->setFontWeight('bold');
                        })
                        ->row(3, [
                            'Invoice',
                            'Customer',
                            'Due Date',
                            'Amount',
                            'Aging'
                        ])
                        ->cells('A3:E3', function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontWeight('bold');
                        });

                    // For data 30 day
                    $row = 4;
                    $invoiceReportThirtyDetails = array_get($data, 'invoiceReportThirtyDetails', 0);
                    if($invoiceReportThirtyDetails){

                        if (array_get($data, 'amountThirty', 0)){
                            $sheet->row($row, [
                                'Sum Amount',
                                '',
                                '',
                                array_get($data, 'amountThirty', 0),
                                ''
                            ]);
                        }

                        $sheet->mergeCells('A' . $row. ':C' . $row)
                        ->cells('A'.$row.':D'.$row, function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontColor('#FF0000')
                                ->setFontWeight('100')
                                ->setAlignment('right');
                        });
                        $row = $row + 1;
                        foreach($invoiceReportThirtyDetails as $inv){
                            $sheet->row($row, [
                                array_get($inv, 'inv_num', ''),
                                array_get($inv, 'cus_name', ''),
                                date('d/m/Y', array_get($inv, 'due_dt', '')),
                                array_get($inv, 'amount', ''),
                                '30'
                            ])
                            ->cells('A5:B' . $row, function ($cells) {
                                $cells->setAlignment('left');
                            });
                            $row = $row + 1;
                        }
                    }
                    //else{
                    //    $sheet->row($row, [
                    //        'Sum Amount',
                    //        '',
                    //        '',
                    //        0,
                    //        ''
                    //    ])
                    //        ->mergeCells('A' . $row. ':C' . $row)
                    //        ->cells('A'.$row.':D'.$row, function ($cells) {
                    //            $cells->setFontSize(12)
                    //                ->setFontColor('#FF0000')
                    //                ->setFontWeight('100')
                    //                ->setAlignment('right');;
                    //        });
                    //    $row = $row + 1;
                    //}

                    // For data 60 day
                    $invoiceReportSixtyDetails = array_get($data, 'invoiceReportSixtyDetails', 0);
                    if($invoiceReportSixtyDetails){
                        if(array_get($data, 'amountSixty', 0)){
                            $sheet->row($row, [
                                'Sum Amount',
                                '',
                                '',
                                array_get($data, 'amountSixty', 0),
                                ''
                            ]);
                        }

                        $sheet->mergeCells('A' . $row. ':C' . $row)
                        ->cells('A'.$row.':D'.$row, function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontColor('#FF0000')
                                ->setFontWeight('100')
                                ->setAlignment('right');;
                        });
                        $row = $row + 1;
                        foreach($invoiceReportSixtyDetails as $inv){
                            $sheet->row($row, [
                                array_get($inv, 'inv_num', ''),
                                array_get($inv, 'cus_name', ''),
                                date('d/m/Y', array_get($inv, 'due_dt', '')),
                                array_get($inv, 'amount', ''),
                                '60'
                            ]);
                            $row = $row + 1;
                        }
                    }
                    //else{
                    //    $sheet->row($row, [
                    //        'Sum Amount',
                    //        '',
                    //        '',
                    //        0,
                    //        ''
                    //    ])
                    //        ->mergeCells('A' . $row. ':C' . $row)
                    //    ->cells('A'.$row.':D'.$row, function ($cells) {
                    //        $cells->setFontSize(12)
                    //            ->setFontColor('#FF0000')
                    //            ->setFontWeight('100')
                    //            ->setAlignment('right');;
                    //    });
                    //    $row = $row + 1;
                    //}

                    // For data 90 day
                    $invoiceReportNinetyDetails = array_get($data, 'invoiceReportNinetyDetails', 0);
                    if($invoiceReportNinetyDetails){
                        if(array_get($data, 'amountNinety', 0)){
                            $sheet->row($row, [
                                'Sum Amount',
                                '',
                                '',
                                array_get($data, 'amountNinety', 0),
                                ''
                            ]) ;
                        }

                        $sheet->mergeCells('A' . $row. ':C' . $row)
                            ->cells('A'.$row.':D'.$row, function ($cells) {
                                $cells->setFontSize(12)
                                    ->setFontColor('#FF0000')
                                    ->setFontWeight('100')
                                    ->setAlignment('right');;
                            });
                        $row = $row + 1;
                        foreach($invoiceReportNinetyDetails as $inv){
                            $sheet->row($row, [
                                array_get($inv, 'inv_num', ''),
                                array_get($inv, 'cus_name', ''),
                                date('d/m/Y', array_get($inv, 'due_dt', '')),
                                array_get($inv, 'amount', ''),
                                '90'
                            ]);
                            $row = $row + 1;
                        }
                    }
                    //else{
                    //    $sheet->row($row, [
                    //        'Sum Amount',
                    //        '',
                    //        '',
                    //        0,
                    //        ''
                    //    ])
                    //        ->mergeCells('A' . $row. ':C' . $row)
                    //        ->cells('A'.$row.':D'.$row, function ($cells) {
                    //            $cells->setFontSize(12)
                    //                ->setFontColor('#FF0000')
                    //                ->setFontWeight('100')
                    //                ->setAlignment('right');;
                    //        });
                    //    $row = $row + 1;
                    //}

                    // For data over 90 day
                    $invoiceReportOverNinetyDetails = array_get($data, 'invoiceReportOverNinetyDetails', 0);
                    if($invoiceReportOverNinetyDetails){
                       if(array_get($data, 'amountOverNinety', 0)){
                           $sheet->row($row, [
                               'Sum Amount',
                               '',
                               '',
                               array_get($data, 'amountOverNinety', 0),
                               ''
                           ]);
                       }
                        $sheet->mergeCells('A' . $row. ':C' . $row)
                        ->cells('A'.$row.':D'.$row, function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontColor('#FF0000')
                                ->setFontWeight('100')
                                ->setAlignment('right');;
                        });
                        $row = $row + 1;
                        foreach($invoiceReportOverNinetyDetails as $inv){
                            $sheet->row($row, [
                                array_get($inv, 'inv_num', ''),
                                array_get($inv, 'cus_name', ''),
                                date('d/m/Y', array_get($inv, 'due_dt', '')),
                                array_get($inv, 'amount', ''),
                                'Over 90'
                            ]);
                            $row = $row + 1;
                        }
                    }
                    //else{
                    //    $sheet->row($row, [
                    //        'Sum Amount',
                    //        '',
                    //        '',
                    //        0,
                    //        ''
                    //    ])
                    //        ->mergeCells('A' . $row. ':C' . $row)
                    //    ->cells('A'.$row.':D'.$row, function ($cells) {
                    //        $cells->setFontSize(12)
                    //            ->setFontColor('#FF0000')
                    //            ->setFontWeight('100')
                    //            ->setAlignment('right');
                    //    });
                    //    $row = $row + 1;
                    //}

                    // Sumary Row
                    $sheet->row($row, [
                        '',
                        '',
                        'Total',
                        array_get($data, 'amountTotal', 0),
                        ''
                    ]);
                    $rowBorder = $row - 1;
                    $sheet
                        ->setBorder('A3:E'.$rowBorder, 'thin')
                        ->cells('A3:E3', function ($cells) {
                            $cells->setAlignment('center');
                        })

                        ->cells('A1:E' . $row, function ($cells) {
                            $cells->setFontWeight('100');
                        })
                        ->cells('A3:E3', function ($cells) {
                            $cells->setFontWeight('bold');
                        });

                });

            })
                ->download($ext);
            //->store($ext, storage_path('excel/exports'));

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }


    /**
     * @SWG\Get(
     *     path="/report/export",
     *     tags={"Invoice Report"},
     *     summary="Export Invoice Report",
     *     description="Export Invoice Report",
     *     operationId="exportInvoiceReport",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cus_id",
     *         in="query",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="whs_id",
     *         in="query",
     *         description="ID of Warehouse",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="inv_num",
     *         in="query",
     *         description="Inv Num",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="inv_sts",
     *         in="query",
     *         description="Invoice Status",
     *         enum={
     *             "RE", "PA", "NE", "PE", "AP"
     *         },
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="issue_dt",
     *         in="query",
     *         description="Invoice Issue Date",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Limit",
     *         type="integer",
     *         default="20",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page",
     *         type="integer",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Invoice",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */    
    public function export(IRequest $request)
    {
        $input = $request->getQueryParams();
        $invoiceReport = $this->reportModel->search($input,[], array_get($input, 'limit'));
        $data[] = [
            'inv_num' => "Invoice Num",
            'cus_name' => "Customer",
            'bill_to_add1' => "Bill To Address I",
            'bill_to_add2' => "Bill To Address II",
            'inv_sts' => "Status",
            'issue_dt' => "Issue Date",
            'due_dt' => "Due Date",
            'paid_dt' => "Paid Date",
            'period' => "Period",
            'total' => "Amount",
            'currency' => "Currency",
            'created_at' => "Created Date"
        ];
        foreach ($invoiceReport as $report) {
            $data[] = [
                $report->inv_num,
                $report->cus_name,
                $report->bill_to_add1,
                $report->bill_to_add2,
                $report->inv_sts_name,
                $report->issue_dt_format,
                $report->due_dt_format,
                $report->paid_dt_format,
                $report->period,
                $report->total,
                $report->currency,
                $report->created_at->format('Y/m/d'),
            ];
        }

        SelExport::exportList("Invoice Report", 'csv', $data);
        die;
    }
}
