<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 3:01 PM
 */

namespace App\Api\V1\Validators;


class ResetPasswordValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
            'token' => 'required',
        ];
    }
}
