<?php

namespace Seldat\Wms2\Models;


class InvoicingSummary extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice_summary';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'inv_sum_id';

    /**
     * @var array
     */
	 
    protected $fillable = [
        'cus_id',
        'whs_id',
        'inv_id',
        'inv_num',
        'chg_code_id',
        'chg_uom_id',
        'chg_type_id',
        'dt',
        'qty',
        'sum_sts'
    ];

    
    public function chargeCode()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ChargeCode', 'chg_code_id', 'chg_code_id');
    }

    public function chargeType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ChargeType', 'chg_type_id', 'chg_type_id');
    }
    
    public function systemUom()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemUom', 'chg_uom_id', 'sys_uom_id');
    }
    
    public function invoiceVolumeRates()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceVolumeRates', 'chg_code_id', 'chg_code_id');
    }
}
