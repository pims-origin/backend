<?php

namespace App\Modules\Orders\Eloquents;

use Illuminate\Support\Facades\Config;
class EventTracking extends BaseModel
{
    public static function boot()
    {
        parent::boot();

        $userId = Config::get('data.userId');

        // create a event to happen on saving
        static::saving(function ($table) use ($userId) {
            $table->created_by = $table->created_by ?: $userId;
        });
    }
    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }
    protected $table = 'evt_tracking';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'evt_code',
        'owner',
        'trans_num',
        'info',
        'whs_id',
        'cus_id'

    ];

    public function odrHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\OdrHdr', 'odr_id', 'odr_id');
    }

    public function odrDtlMeta()
    {
        return $this->hasOne(__NAMESPACE__ . '\OdrDtlMeta', 'odr_dtl_id');
    }

    public function sysUom()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemUom', 'sys_uom_id', 'uom_id');
    }
}
