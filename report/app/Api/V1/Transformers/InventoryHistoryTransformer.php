<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class InventoryHistoryTransformer extends TransformerAbstract
{
    public function transform($data)
    {
        return [
            'cus_name' => $data->cus_name,
            'odr_num' => $data->odr_num,
            'ctnr_num' => $data->ctnr_num,
            'log_date' => strlen($data->log_date) >= 10 ?
                    date('m/d/Y', $data->log_date) : '',
            'total_cartons' => $data->box,
            'total_pieces' => $data->pieces
        ];
      }
}

