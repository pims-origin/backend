<?php
namespace App\Api\V1\Models;

use Psr\Http\Message\ServerRequestInterface as IRequest;

class ReceivingService extends BaseService
{
    /**
     * ItemService constructor.
     * @param IRequest $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_SYSTEM');
        parent::__construct($request, $api);
    }

    /**
     * @param $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function process()
    {
        $params = $this->inputs;

        $uri = env('API_SYSTEM') . 'queue/send';

        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

}
