<?php

namespace App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Controllers;

use App\Api\V1\Models\Log;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\CartonModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\EventTrackingModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\InventorySummaryModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\LocationModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\OrderCartonModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\OrderDtlModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\OrderHdrModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\PalletModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\WaveDtlLocModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\WavePickDtlModel;
use App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models\WavePickHdrModel;
use App\Jobs\AutoAssignRfidsJob;
use App\MyHelper;
use App\libraries\RFIDValidate;
use Dingo\Api\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;

class WavepickController extends AbstractController
{
    const DEFAULT_USER_UPDATE_WAVE       = 'citaus';
    const PREFIX_CARTON_RFID_CYCLE_COUNT = 'CCTC';
    const STATUS_ALLOCATE_LOCATION       = 'AL';
    const LOCATION_TYPE_PROCESSING       = 'PRO';

    protected $orderDtlModel;
    protected $locationModel;
    protected $cartonModel;
    protected $eventTrackingModel;
    protected $orderCartonModel;
    protected $wvDtlModel;
    protected $waveHdrModel;
    protected $orderHdrModel;
    protected $palletModel;
    protected $inventorySummaryModel;

    private $_errorMsgs;
    protected $countWrongCtn;
    protected $countTrueCtn;
    protected $trueSkuInfos;
    protected $rfidWrongCtns;
    protected $pickerId;
    protected $isOverpicking;
    protected $notMatchWave;

    public function __construct(
        CartonModel $cartonModel
    ) {
        $this->cartonModel           = $cartonModel;
        $this->wvDtlModel            = new WavePickDtlModel();
        $this->orderDtlModel         = new OrderDtlModel();
        $this->orderCartonModel      = new OrderCartonModel();
        $this->orderHdrModel         = new OrderHdrModel();
        $this->palletModel           = new PalletModel();
        $this->waveHdrModel          = new WavePickHdrModel();
        $this->eventTrackingModel    = new EventTrackingModel();
        $this->locationModel         = new LocationModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
        $this->_errorMsgs            = [];
        $this->countWrongCtn         = 0;
        $this->countTrueCtn          = 0;
        $this->trueSkuInfos          = [];
        $this->rfidWrongCtns         = [];
        $this->notMatchWave          = [];
    }

    public function updateWavePick(Request $request, $whsId, $wv_id)
    {
        set_time_limit(0);
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v6/whs/{$whsId}/wave/{$wv_id}/update";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'UWP',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Update wave pick'
        ]);

        /*
         * end logs
         */

        $codes = array_get($input, 'code', null);

        if (!$codes) {
            $msg = "Please check input params.";
            return $this->_responseErrorMessage($msg);
        }

        $palletRfid = array_get($codes, 'pallet', null);
        $cartons    = array_get($codes, 'ctns', null);
        $userName   = array_get($codes, 'picker', null);

        // WAP-779 - [Wavepick] Update picker id to wave pick detail
        if ($userName) {
            $checkUser = DB::table('users')->where('username', $userName)->first();
            if (count($checkUser)) {
                $this->pickerId = $checkUser['user_id'];
            } else {
                $this->pickerId = Data::getCurrentUserId();
            }
        } else {
            $this->pickerId = Data::getCurrentUserId();
        }

        //check invalid code carton
        $ctnRfidInValid = [];
        $errorMsg = '';
        if (!empty($cartons)) {
            foreach ($cartons as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }
        } else {
            // $msg = 'Ctns array is required!';
            $msg = 'Please scan carton tags!';
            return $this->_responseErrorMessage($msg);
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            $this->_setErrorMsg($errorMsg);
//            return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
        }

        if ($palletRfid) {
            $pltRFIDValid = new RFIDValidate($palletRfid, RFIDValidate::TYPE_PALLET, $whsId);
            //validate pallet RFID
            if (!$pltRFIDValid->validate()) {
                // $this->_setErrorMsg($pltRFIDValid->error);
                $msg = sprintf("The pallet RFID %s is invalid", $palletRfid);
               return $this->_responseErrorMessage($msg);
            }
        }

        $cartons = array_unique($cartons);

        try {
            \DB::beginTransaction();

            // WAP-646 - [Outbound - Wave pick] Remove Return Cartons Feature
            // picked or Shipped or Canceled or Putback or Inactive
            $pickedCtn = $this->cartonModel->getPickedCartonByCtnRFID($cartons, $whsId);

            $seqCtn = 1;
            foreach($pickedCtn as $ctn){
                if ( !$ctn->odr_ctn_id ){
                    continue;
                }

                if (!$ctn->wv_hdr_id) {
                    // processing location
                    $ctn->update([
                        'ctn_sts'  => 'AC'
                    ]);
                    if ($ctn->odr_ctn_id) {
                        DB::table('odr_cartons')->where('odr_ctn_id', $ctn->odr_ctn_id)->delete();
                    }
                }

                if ($ctn->wv_hdr_id && $ctn->wv_hdr_id != $wv_id) {
                    // case 7: Picked does not belong to current Wave, the process like Shipped
                    $this->generateNewCarton($ctn, $seqCtn++);
                }
            }

//              if (count($pickedCtn) > 0) {
//                  $ctnRfidPick = array_pluck($pickedCtn->toArray(), 'rfid');
//                  $msg = sprintf("Cartons %s already picked.", implode(', ', $ctnRfidPick));
//                  $this->_setErrorMsg($msg);
//                  $this->countWrongCtn += count($ctnRfidPick);
// //                 $msg = sprintf("Unable to update wavepick with mixed statuses {'ACTIVE', 'ADJUSTED'} and 'PICKED'.");
// //                 return $this->_responseErrorMessage($msg);
//              }

             // $returnFlag = false;
            //get carton list by carton RFID
            $ctnLists = $this->cartonModel->getActiveCartonByCtnRFID($cartons)->toArray();

            // WAP-501: Update API update wavepick for processing wrong cartons
            // Throw out error message
            if (count($ctnLists) != count($cartons)) {
                //validate carton RFID
                foreach ($cartons as $carton) {
                    $cartonByRFIDInfo = $this->cartonModel->getCartonByCtnRFID($carton, $whsId);
                    //check carton RFID is existed or not
                    if (!$cartonByRFIDInfo) {
                        $msg = sprintf("Carton RFID %s doesn't exist.", $carton);
                        $cartonObj = $this->cartonModel->getFirstWhere([
                            'rfid' => $carton,
                            ]);
                        if ($cartonObj) {
                            $msg = sprintf("Carton RFID %s doesn't belong to warehouse.", $carton);
                        }

                        $this->_setErrorMsg($msg);
                        continue;
//                            \DB::rollback();
//                            return $this->_responseErrorMessage($msg);
                    }

                    //check carton RFID should be ACTIVE
//                     if ($cartonByRFIDInfo->ctn_sts != 'AC' && $cartonByRFIDInfo->ctn_sts != 'AJ') {
//                         $msg = sprintf("Unable to update wavepick with mixed statuses {'ACTIVE', 'ADJUSTED'} and 'PICKED'.");
//                         if ($cartonByRFIDInfo->ctn_sts == 'SH') {
//                             $msg = sprintf("Carton RFID %s has been shipped.", $carton);
//                         }
//                         if ($cartonByRFIDInfo->ctn_sts == 'LK') {
//                             $msg = sprintf("Carton RFID %s has been locked.", $carton);
//                         }

//                         $this->_setErrorMsg($msg);
// //                            \DB::rollback();
// //                            return $this->_responseErrorMessage($msg);
//                     }

                    //check carton RFID with piece remain must > 0
                    if ($cartonByRFIDInfo->piece_remain < 1) {
                        $msg = sprintf("Piece remain of carton RFID %s should greater than zero.", $carton);

                        $this->_setErrorMsg($msg);
//                            \DB::rollback();
//                            return $this->_responseErrorMessage($msg);
                    }

                    //check carton RFID is damaged or not
//                     if ($cartonByRFIDInfo->is_damaged == 1) {
//                         $msg = sprintf("Carton RFID %s has damaged.", $carton);

//                         $this->_setErrorMsg($msg);
// //                            \DB::rollback();
// //                            return $this->_responseErrorMessage($msg);
//                     }

                    //check is_ecom of carton RFID
                    if ($cartonByRFIDInfo->is_ecom == 1) {
                        $msg = sprintf("Carton RFID %s is only for eCommerce Order.", $carton);

                        $this->_setErrorMsg($msg);
//                            \DB::rollback();
//                            return $this->_responseErrorMessage($msg);
                    }

                    // WAP-646 - [Outbound - Wave pick] Remove Return Cartons Feature
                    //check carton RFID must belong to a location
//                     if (!$cartonByRFIDInfo->loc_id && $cartonByRFIDInfo->ctn_sts != 'AJ') {
//                         $msg = sprintf("Carton RFID %s is not belong to any location.", $carton);

//                         $this->_setErrorMsg($msg);
// //                            \DB::rollback();
// //                            return $this->_responseErrorMessage($msg);
//                     }

                    //check carton RFID must belong to a pallet
//                     if (!$cartonByRFIDInfo->plt_id && $cartonByRFIDInfo->ctn_sts != 'AJ') {
//                         $msg = sprintf("Carton RFID %s is not belong to any pallet.", $carton);

//                         $this->_setErrorMsg($msg);
// //                            \DB::rollback();
// //                            return $this->_responseErrorMessage($msg);
//                     }
                }

                $trueRFIDs = array_pluck($ctnLists, 'rfid', null);
                $wrongRFIDS = array_diff($cartons, $trueRFIDs);
                $rfidExists = $this->cartonModel->getModel()->whereIn('rfid', $wrongRFIDS)->pluck('rfid')->toArray();
                $rfidNotExist = array_diff($wrongRFIDS, $rfidExists);
                if (!empty($rfidNotExist)) {
                    $msg = sprintf("At least 1 carton rfid doesn't exist. %s.", implode(', ', $rfidNotExist));
                    if (count($this->getErrorMsg()) == 0) {
                        $this->_setErrorMsg($msg);
                    }
                }
//                    \DB::rollback();
//                    return $this->_responseErrorMessage($msg, $wrongRFIDS);
            }
            // End WAP-501 */

            // update wave pick normaly
            // check valid wave pick
            $wvDtlData = $this->wvDtlModel->getProperlyWvDtl($wv_id, $whsId);

            // Check wave pick detail existed
            if (empty($wvDtlData) || count($wvDtlData) == 0) {
                $msg = "Wave pick doesn't exist.";
                $wvDtlData = $this->wvDtlModel->getModel()
                    ->where([
                        'wv_id'  => $wv_id,
                        'whs_id' => $whsId,
                    ])
                    ->first();

                if ($wvDtlData) {
                    $wvHdr = $this->waveHdrModel->getFirstWhere(['wv_id' => $wv_id]);
                    $wvSts = object_get($wvHdr, 'wv_sts');

                    $msg = sprintf("Wave pick already %s.", Status::getByKey('WAVEPICK-STATUS', $wvSts));
                } else {
                    $wvDtlData = $this->wvDtlModel->getModel()
                        ->where([
                            'wv_id' => $wv_id,
                        ])
                        ->first();

                    if ($wvDtlData) {
                        $msg = "Wave pick doesn't belong to warehouse.";
                    }
                }

                \DB::rollback();
                return $this->_responseErrorMessage($msg);
            }

            // $userData = new Data();
            // $currentUser = $userData->getUserInfo();
            // if ((array_get($wvDtlData->toArray(), '0.picker') != $currentUser['user_id'])
            //     &&
            //     ($currentUser['username'] != self::DEFAULT_USER_UPDATE_WAVE)) {
            //     $msg = "This wave pick item doesn't belong to current user.";
            //     return $this->_responseErrorMessage($msg);
            // }

            // cartons with Active status on a LOCKED location
            $locIds = array_pluck($ctnLists, 'loc_id');
            $notActiveLocs = $this->locationModel->getNotActiveLocByIds($locIds);
            if (count($notActiveLocs)) {
                $whsMeta = DB::table('whs_meta')
                    ->where('whs_id', $whsId)
                    ->where('whs_qualifier', 'wle')
                    ->where('deleted', 0)
                    ->first();
                $minuteExpired = array_get($whsMeta, 'whs_meta_value', 0);
                foreach ($notActiveLocs as $keyLoc => $notActiveLoc) {
                    // WAP-707 - [Outbound - Wavepick] Modify API update wavepick
                    $locIdCheck = array_get($notActiveLoc, 'loc_id');
                    if (array_get($notActiveLoc, 'loc_sts_code') == self::STATUS_ALLOCATE_LOCATION) {
                        $timeReleased = array_get($notActiveLoc, 'updated_at') + $minuteExpired * 60;
                        if ($timeReleased < time()) {
                            unset($notActiveLocs[$keyLoc]);
                            continue;
                        }
                    }
                    foreach ($ctnLists as $keyCtn => $ctnList) {
                        if (array_get($ctnList, 'loc_id') == $locIdCheck){
                            $this->rfidWrongCtns[] = array_get($ctnList, 'rfid');
                            unset($ctnLists[$keyCtn]);
                            $this->countWrongCtn++;
                            break;
                        }
                    }
                }
                // WAP-501: Processing with wrong cartons
                if (count($notActiveLocs)) {
                    $locRfidNotActive = array_pluck($notActiveLocs, 'rfid');
                    $msg = sprintf("Cannot update wavepick for cartons on locations that are not active: %s.", implode(', ', $locRfidNotActive));

                    $this->_setErrorMsg($msg);
                }

                // \DB::rollback();
                // return $this->_responseErrorMessage($msg);
                //
            }

            $dataWaveByCartons = [
                'whs_id'   => $whsId,
                'cartons'  => $cartons,
                'wv_id'    => $wv_id,
                'ctnLists' => $ctnLists,
                'palletRfid' => $palletRfid,
                'pickerId'  => $this->pickerId
            ];

            $this->_updateWavePickByCarton($dataWaveByCartons, $wvDtlData);
            //update wave header status
            $wvSts = $this->waveHdrModel->updatePicked($wv_id, $this->pickerId);

            foreach($wvDtlData as $wvDtl){
                $skuInfos[] = [
                    'sku'   => $wvDtl->sku,
                    'size'  => $wvDtl->size,
                    'color' => $wvDtl->color,
                    'pack_size' => $wvDtl->pack_size,
                    'lot'   => $wvDtl->lot
                ];
            }

            $wvDtlData = $wvDtlData->first();
            if ($wvSts) {
                $event = [
                    'whs_id'        => $whsId,
                    'cus_id'        => object_get($wvDtlData, 'cus_id'),
                    'owner'         => object_get($wvDtlData, 'wv_num'),
                    'evt_code'      => 'WPC',
                    'trans_num'     => object_get($wvDtlData, 'wv_num'),
                    'info'          => "Wave Pick completed",
                    'created_by'    => $this->pickerId,
                    'created_at'    => time()
                ];
                //  Evt tracking
                DB::table('evt_tracking')->insert($event);
            }

            // WAP-725 - [Outbound - Wavepick] Modify API update wavepick
            if (count($this->rfidWrongCtns)) {
                $this->_updateProcessingLocationForCartons($whsId, $this->rfidWrongCtns);
            }

            if($numSuccessCarton = $this->countTrueCtn){
                $pattern = '%s-%s-%s-%s-%s';
                $skuStrings = [];
                foreach($this->trueSkuInfos as $info){
                    $skuStrings[] = sprintf($pattern, $info['sku'], $info['size'], $info['color'], $info['pack_size'], $info['lot']);
                }
                $dataTracking = [
                    'evt_code'      => 'WPN',
                    'owner'         => object_get($wvDtlData, 'wv_num'),
                    'trans_num'     => object_get($wvDtlData, 'wv_num'),
                    'info'          => sprintf('WAP - %d Carton(s) RFID, belonged to "%s", were picked', $numSuccessCarton ,implode(',', array_unique($skuStrings))),
                    'whs_id'        => $whsId,
                    'cus_id'        => object_get($wvDtlData, 'cus_id'),
                    'created_by'    => $this->pickerId,
                    'created_at'    => time()
                ];
                if($numSuccessCarton > 0){
                    DB::table('evt_tracking')->insert($dataTracking);
                }
            }

            \DB::commit();

            // WAP-623: [Outbound - Wave Pick] Auto assign cartons to Order if 1 Wave Pick just have only 1 Order (Case RFID)
            $isAutoAssign = $this->_isAutoAssignCartonsToOrder($wv_id);
            if ($isAutoAssign && !$this->isOverpicking) {
                dispatch(new AutoAssignRfidsJob($whsId, $wv_id, $cartons, $request, null, null, $this->pickerId));
            }

            if ($this->isOverpicking){
                $randomProcessingLocation = $this->locationModel->getModel()
                                            ->where([
                                                'loc_whs_id'    =>  $whsId,
                                                'loc_type_id'   =>  6
                                            ])->first();

                if ( $randomProcessingLocation ) {
                    $cartonOverPicks = OrderCarton::where([
                        'wv_hdr_id'         =>  $wv_id,
                        'odr_dtl_id'    =>  null,
                    ])->get()->pluck('ctn_id')->toArray();

                    $this->cartonModel->getModel()->whereIn('ctn_id', $cartonOverPicks)->update([
                        'loc_id'        => $randomProcessingLocation->loc_id,
                        'loc_code'      => $randomProcessingLocation->loc_code,
                        // 'loc_name'      => $randomProcessingLocation->loc_alternative_name,
                        'loc_type_code' => 'PRO'
                    ]);
                }
            }

            if(!empty($this->notMatchWave)){
                $msg = sprintf(
                    'The carton rfid %s is not matched with wave'
                    . ' pick: %s', implode(', ', array_unique($this->notMatchWave)), object_get($wvDtlData, 'wv_num'));
                $this->_setErrorMsg($msg);
            }
            $msg = "Update wave pick successfully.";
            if (count($this->getErrorMsg()) > 0) {
                $numberWrongCtn = count($cartons) - count($ctnLists) + $this->countWrongCtn;
                $formatMsg = sprintf("are %s cartons", $numberWrongCtn);
                if ($numberWrongCtn == 1) {
                    $formatMsg = sprintf("is %s carton", $numberWrongCtn);
                }
                $msg = sprintf("Update wave pick successfully. But there %s cannot be updated. ".PHP_EOL."[{%s}].",
                    $formatMsg,
                    implode("}; ".PHP_EOL."{", $this->getErrorMsg()));

                Log::error($request, $whsId, [
                    'evt_code'      => 'ERN',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);
            }
            $data = [
                'message' => $msg,
                'data'    => [
                    'picked'         => $wvSts,
                    'picked_qty_ttl' => $this->countTrueCtn,
                ],
                'status'  => true,
            ];

            //WMS2-4274: [Outbound][Update wave pick] - No message display when scanning over expected carton for the order
            $wvDtlTotalCarton = $this->wvDtlModel->getDetailTotalCartons($whsId, $wv_id);
            if (count($wvDtlTotalCarton) == 0) {
                $wvDtlTotalCarton = $this->wvDtlModel->getModel()
                                        ->select([
                                            'wv_dtl_id',
                                            DB::raw('0 AS total_carton'),
                                            DB::raw('0 AS total_pieces'),
                                        ])
                                        ->where('wv_id', $wv_id)
                                        ->get();
            }
            $data['data']['wavepick_detail'] = $wvDtlTotalCarton;

            return new Response($data, 200, [], null);

        } catch (\Exception $e) {
            \DB::rollback();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    private function _responseErrorMessage($msg, $data = null)
    {
        return new Response([
                        'data'    => $data,
                        'message' => $msg,
                        'status'  => false,
                    ], 200, [], null);
    }

    private function _updateWavePickByCarton($data, $dataWaveDtlInfo)
    {
        $whs_id     = $data['whs_id'];
        $wv_id      = $data['wv_id'];
        $cartons    = $data['cartons'];
        $ctnLists   = $data['ctnLists'];
        $palletRfid = $data['palletRfid'];
        $pickerId   = $data['pickerId'];

        /* WAP-501: Processing with wrong cartons
        // Check active & existed carton
        // $dataCheckActive = [
        //     'ctn-rfid' => $cartons,
        //     'ctnLists' => $ctnLists
        // ];
        // $this->checkActiveCarton($dataCheckActive);
        */

        //check carton Item ID = WV_dtl ID
        $wvDtlCodes = [];

        foreach ($dataWaveDtlInfo as $wvDtl) {
            $wvDtlCodes[] = ($wvDtl['item_id'] . '@^' . strtoupper($wvDtl['lot']));
        }

        $wvDtlCodesAll = $wvDtlCodes;
        $arrCartonProcessed = []; //cartons which are picked

        foreach ($dataWaveDtlInfo as $wvDtl) {
            $pickedQTY = 0;
            $cartonItemID = null;
            $wvDtlCode = ($wvDtl['item_id'] . '@^' . strtoupper($wvDtl['lot']));

            $locIds = [];
            $countAJ = 0;
            $rfidsAJ = [];

            foreach ($ctnLists as $key => $ctn) {
                $cartonCode = ($ctn['item_id'] . '@^' . strtoupper($ctn['lot']));
                // //check cartons QTY is enough to wave pick or not
                // $ctn_Qty_wvDtl = $wvDtl['ctn_qty'];
                // $wvDtlID = $wvDtl['wv_dtl_id'];
                // $wvNum = $wvDtl['wv_num'];
                // $ctn_Qty_odr_ctn = $this->orderCartonModel->countOrderCTByWvDtlId($wvDtlID);
                // if ($ctn_Qty_wvDtl == $ctn_Qty_odr_ctn) {
                //     $msg = sprintf(
                //         'Had already received enough carton!'
                //     );
                //     throw new \Exception($msg);
                // }
                if (!in_array($cartonCode, $wvDtlCodesAll)) {
                    // WAP-501: processing with wrong cartons
                    $this->notMatchWave[] = $ctn['rfid'];
                     /*
                     $msg = sprintf(
                         'The carton rfid %s is not matched with wave'
                         . ' pick: %s', $ctn['rfid'], $wvDtl['wv_num']
                     );
                    $this->_setErrorMsg($msg);
                    */
                    $this->rfidWrongCtns[] = $ctn['rfid'];
                    $this->countWrongCtn++;
                    continue;
                    // throw new \Exception($msg);
                }

                if ($cartonCode == $wvDtlCode) {
                    if ($wvDtl['pack_size'] != $ctn['ctn_pack_size']) {
                        // WAP-501: processing with wrong cartons
                         $msg = sprintf(
                             'The carton rfid %s pack size %d is not same pack size %d in wave pick details of %s!',
                             $ctn['rfid'], $ctn['ctn_pack_size'], $wvDtl['pack_size'], $wvDtl['wv_num']

                         );

                        $this->_setErrorMsg($msg);
                        $this->rfidWrongCtns[] = $ctn['rfid'];
                        $this->countWrongCtn++;
                        continue;
                        // throw new \Exception($msg);
                    }
                    $arrCartonProcessed[$key] = $ctn;
                    $pickedQTY += $ctn['piece_remain'];

                    if (array_get($ctn, 'ctn_sts') == "AJ") {
                        $countAJ++;
                        $rfidsAJ[] = array_get($ctn, 'rfid');
                    }
                }

                $locIds[] = $ctn['loc_id'];
            }
            if (empty($arrCartonProcessed)) {
                continue;
            } else{
                $this->countTrueCtn += count($arrCartonProcessed);
                foreach($arrCartonProcessed as $value){
                    if(!empty($value['sku'])){
                        $arrTemp['sku'] = $value['sku'];
                        $arrTemp['size'] = $value['size'];
                        $arrTemp['color'] = $value['color'];
                        $arrTemp['pack_size'] = $value['ctn_pack_size'];
                        $arrTemp['lot'] = $value['lot'];
                        array_push($this->trueSkuInfos, $arrTemp);
                    }
                }
            }

            //remove wave detail item which processed
            $wvDtlCodes = $this->removeItemFromArray($wvDtlCodes, $cartonCode);

            $actPicked = $wvDtl['act_piece_qty'] + $pickedQTY;

            // update invetory summary
            if ($actPicked > $wvDtl['piece_qty']) {
                $this->isOverpicking = true;
                $this->_updateInvtSmrOverPicking($wvDtl, $pickedQTY);
            } else {
                $this->_updateInventorySummary($wvDtl, $pickedQTY);
            }

            $wvDtlSts = Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS");
            if ($actPicked >= $wvDtl['piece_qty']) {
                $wvDtlSts = Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS");

                // WAP-707 - [Outbound - Wavepick] Modify API update wavepick
                if (count($locIds)) {
                    $this->_updateReleasedLocation($locIds);
                }
            }

            // Update wave pick detail
            $dataUpdateWvDtl = [
                'act_piece_qty' => $actPicked,
                'wv_dtl_id'     => $wvDtl['wv_dtl_id'],
                'wv_dtl_sts'    => $wvDtlSts,
            ];

            //update wave pick detail
            $this->wvDtlModel->updateWvDtlPickCtn($dataUpdateWvDtl, $this->pickerId);

            $this->waveHdrModel->updateWhere([
                'wv_sts'     => 'PK',
                'updated_by' => $this->pickerId,
                'updated_at' => time()
            ], [
                'wv_id' => $wv_id
            ]);

            //insert order carton
            $this->orderCartonModel->insertOrderCartons($arrCartonProcessed, $wvDtl, $this->pickerId);

            //update table carton to pick, pick_date, status, storage_duration
            $this->updateCartonInfoToPicked($arrCartonProcessed);

            // WAP-592 - Change work flow of Cartons with status AJ
            $this->_computeAJCarton($whs_id, $wvDtl, $palletRfid, $countAJ, $rfidsAJ);

            //update pallet ctn_ttl
            // update pallet zero date
            $locIds = array_unique($locIds);
            $this->palletModel->updatePalletCtnTtl($locIds);
            $this->palletModel->updateZeroPallet($locIds);

            /**
             * 1. sort cartons by wave details id
             * 2. sort cartons by locations
             * 3. calculate picked qty and actual location
             * 4. Get wv_dtl_loc
             * 5. if existed, update or create
             */
            // $wdtLoc = $this->cartonModel->updateWvDtlLoc($arrCartonProcessed);

            // if ($wdtLoc) {
            //     $this->_insertWaveDtlLoc($wdtLoc, $wvDtl);
            // }

            //remove array cartons which is processed from cartonList
            foreach ($arrCartonProcessed as $key => $value) {
                unset($ctnLists[$key]);
            }
            unset($arrCartonProcessed);
        }

    }

    private function _updateWavePickReturnPicking($data)
    {
        /**
         * Update inventory Availble QTY if Over Pick QTY/Allocated QTY and reduce picked qty
         * Update Wave pick details to reduce actual qty
         * Delete odr Carton table
         * Create pallet and assign cartons to this pallet
         * IF existed pallet and then assign cartons to this pallet, cc notification
         */

        $whs_id = $data['whs_id'];
        // $wv_id = $data['wv_id'];
        $cartons = $data['cartons'];
        $ctnLists = $data['ctnLists'];
        $palletRfid = $data['palletRfid'];

        $wvIds = $this->orderCartonModel->getModel()
                ->select(\DB::raw('distinct wv_hdr_id'))
                ->whereIn('ctn_rfid', $cartons)
                ->where('deleted', 0)
                ->whereNull('odr_hdr_id')
                ->get()->toArray();

        // if (count($wvIds) == 0) {
        //     $msg = "Cartons don't belong to any wave pick";
        //     throw new \Exception($msg);
        // }
        $wvIds = array_pluck($wvIds, 'wv_hdr_id');
        $wavePickPickeds = [];
        foreach ($wvIds as $keyWv => $wv_id) {
            $dataWaveDtlInfo = $this->wvDtlModel->getProperlyWvDtlInReturn($wv_id, $whs_id);

            //check carton Item ID = WV_dtl ID
            $wvDtlCodes = [];
            foreach ($dataWaveDtlInfo as $wvDtl) {
                $wvDtlCodes[] = ($wvDtl['item_id'] . '@^' . strtoupper($wvDtl['lot']));
            }

            $wvDtlCodesAll = $wvDtlCodes;
            $arrCartonProcessed = []; //cartons which are picked

            foreach ($dataWaveDtlInfo as $wvDtl) {
                $returnQTY = 0;
                $cartonItemID = null;
                $wvDtlCode = ($wvDtl['item_id'] . '@^' . strtoupper($wvDtl['lot']));

                $locIds = [];
                foreach ($ctnLists as $key => $ctn) {
                    $cartonCode = ($ctn['item_id'] . '@^' . strtoupper($ctn['lot']));

                    // if (!in_array($cartonCode, $wvDtlCodesAll)) {
                    //     $msg = sprintf(
                    //         'The carton rfid %s is not matched with wave'
                    //         . ' pick: %s', $ctn['rfid'], $wvDtl['wv_num']
                    //     );
                    //     throw new \Exception($msg);
                    // }

                    if ($cartonCode == $wvDtlCode) {
                        // if ($wvDtl['pack_size'] != $ctn['ctn_pack_size']) {
                        //     $msg = sprintf(
                        //         'The carton rfid %s pack size %d is not same pack size %d in wave pick details of %s!',
                        //         $ctn['rfid'], $ctn['ctn_pack_size'], $wvDtl['pack_size'], $wvDtl['wv_num']

                        //     );
                        //     throw new \Exception($msg);
                        // }
                        $orderCartonCheck = $this->orderCartonModel->getFirstWhere([
                            'ctn_rfid'  => $ctn['rfid'],
                            'wv_dtl_id' => $wvDtl['wv_dtl_id'],
                            ]);
                        if ($orderCartonCheck) {
                            $arrCartonProcessed[$key] = $ctn;
                            $returnQTY += $ctn['piece_remain'];
                        }
                    }

                    $locIds[] = $ctn['loc_id'];
                }

                if (empty($arrCartonProcessed)) {
                    continue;
                }

                //remove wave detail item which processed
                $wvDtlCodes = $this->removeItemFromArray($wvDtlCodes, $cartonCode);

                $actPicked = $wvDtl['act_piece_qty'] - $returnQTY;
                if ($actPicked < 0) {
                    $msg = "Unable return picked QTY greater than actual piece QTY of wavepick detail";
                    throw new \Exception($msg);
                }

                $wvDtlSts = Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS");
                if ($actPicked >= $wvDtl['piece_qty']) {
                    $wvDtlSts = Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS");

                    if ($actPicked == 0 && in_array($wvDtl['wv_sts'], ["RT", "CC"])) {
                        $wvDtlSts = Status::getByValue("Canceled", "WAVEPICK-DETAIL-STATUS");
                    }
                }

                // Update wave pick detail
                $dataUpdateWvDtl = [
                    'act_piece_qty' => $actPicked,
                    'wv_dtl_id'     => $wvDtl['wv_dtl_id'],
                    'wv_dtl_sts'    => $wvDtlSts,
                ];

                //update wave pick detail
                $this->wvDtlModel->updateWvDtlPickCtn($dataUpdateWvDtl);

                $this->waveHdrModel->updateWhere([
                    'wv_sts' => 'PK'
                ], [
                    'wv_id' => $wv_id
                ]);

                // delete order carton
                $this->orderCartonModel->deleteOdrCartonInReturn(
                    array_pluck($arrCartonProcessed, 'rfid')
                );

                // update invetory summary
                $this->_updateInvtSmrReturnPicking($wvDtl, $returnQTY);
                //update pallet and carton
                $ctnRfidsUpdate = array_pluck($arrCartonProcessed, 'rfid');
                if ($palletRfid) {
                    $this->_assignCartonToPallet(
                        $wvDtl,
                        $ctnRfidsUpdate,
                        $palletRfid,
                        $returnQTY
                    );
                } else {
                    // update cartons status
                    $this->cartonModel->getModel()
                                    ->whereIn('rfid', $ctnRfidsUpdate)
                                    ->update([
                                            'ctn_sts'          => Status::getByKey('CTN_STATUS', 'ACTIVE'),
                                            'picked_dt'        => 0,
                                            'storage_duration' => 0,
                                            'loc_id'           => null,
                                            'loc_type_code'    => null,
                                            'loc_code'         => null,
                                            'loc_name'         => null,
                                            'plt_id'           => null,
                                            'updated_at'       => time(),
                                            'updated_by'       => Data::getCurrentUserId(),
                                        ]);
                }

                // update wavepick detail location
                // $this->_updateWvDtlLocInReturn($arrCartonProcessed, $wvDtl);

                //remove array cartons which is processed from cartonList
                foreach ($arrCartonProcessed as $key => $value) {
                    // unset carton that already returned
                    foreach ($cartons as $keyRfid => $rfid) {
                        if (array_get($value, 'rfid') == $rfid){
                            unset($cartons[$keyRfid]);
                            break;
                        }
                    }
                    unset($ctnLists[$key]);
                }
                unset($arrCartonProcessed);
            }

            //update wave header status
            $wvSts = $this->waveHdrModel->updatePicked($wv_id);

            // set wavepick status is cancel
            // 1. Wavepick that  is not overpick will change to Completed when all orders are picked
            // 2. Wavepick that  is not overpick will change to Canceled when all orders are canceled
            // Else wavepick is PICKING
            if ($wvSts == 0) {
                $wvSts = $this->waveHdrModel->updateCanceled($wv_id);
            }

            $wavePickPickeds[] = [
                'wv_id'  => $wv_id,
                'picked' => $wvSts,
            ];
        }
        // WAP-506: assign wrong catons to pallet
        if ($palletRfid) {
            $this->_assignWrongCartonToPallet($whs_id, $cartons, $palletRfid);
        }

        return $wavePickPickeds;
    }

    private function _insertWaveDtlLoc($wdtLocArr, $wvDtl)
    {
        $wvDtlLoc = (new WaveDtlLocModel())->getModel()->where('wv_dtl_id', $wvDtl->wv_dtl_id)->first();

        if ($wvDtlLoc) {
            if ($wvDtlLoc->act_loc_ids) {
                $actLocs = json_decode($wvDtlLoc->act_loc_ids, true);
                foreach ($actLocs as $keyActLoc => $actLoc) {
                    foreach ($wdtLocArr as $keyWdtLoc => $wdtLoc) {
                        // update existing location
                        if ($wdtLoc['loc_id'] == $actLoc['loc_id']) {
                            $actLocs[$keyActLoc]['cartons']     = array_merge($actLocs[$keyActLoc]['cartons'], $wdtLoc['cartons']);
                            $actLocs[$keyActLoc]['picked_qty'] += $wdtLoc['picked_qty'];
                            $actLocs[$keyActLoc]['avail_qty']   = $wdtLoc['avail_qty'];
                            unset($wdtLocArr[$keyWdtLoc]);
                        }
                    }
                }
                $actLocs = array_merge($actLocs, $wdtLocArr);
                $wvDtlLoc->act_loc_ids = json_encode($actLocs);
            } else {
                $wvDtlLoc->act_loc_ids = json_encode($wdtLocArr);
            }

            return $wvDtlLoc->update();
        }

        return;

    }

    private function _updateInventorySummary($wvData, $pickedQTY)
    {
        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $wvData['item_id'],
                'whs_id'  => $wvData['whs_id'],
                'lot'     => $wvData['lot'],
            ]
        );

        $invtAvail     = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;

        $overAvailQTY = 0;
        if ($invtAllocated < $pickedQTY) {
            // if ($invtAllocated + $invtAvail < $pickedQTY) {
            //     $msg = "Not enough allocated QTY to pick";
            //     throw new \Exception($msg);
            // }

            $wvDataArr = json_decode($wvData->data, true);
            if (!$wvDataArr) {
                $wvDataArr['avail']    = 0;
                $wvDataArr['allocated'] = 0;
            }

            $overAvailQTY = $pickedQTY - $invtAllocated;
            $wvDataArr['avail'] += $overAvailQTY;

            $wvData->data = json_encode($wvDataArr);
            $wvData->update();
        }

        $reduceAllocate = $pickedQTY - $overAvailQTY;
        $allocatedSql = sprintf('IF(`allocated_qty` < %d, 0, `allocated_qty` - %d)', $reduceAllocate, $reduceAllocate);
        $pickedSql    = sprintf('`picked_qty` + %d', $pickedQTY);
        $availSql     = sprintf('IF(`avail` < %d, 0, `avail` - %d)', $overAvailQTY, $overAvailQTY);

        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $wvData['item_id'],
            'whs_id'  => $wvData['whs_id'],
            'lot'     => $wvData['lot'],
        ]);
    }

    private function _updateInvtSmrOverPicking($wvData, $pickedQTY)
    {
        $allocatedQTY = $wvData['piece_qty'] - $wvData['act_piece_qty'];
        $overPicking  = $wvData['act_piece_qty'] + $pickedQTY - $wvData['piece_qty'];
        if ($wvData['act_piece_qty'] >= $wvData['piece_qty']) {
            // over in case over picking continuous
            $overPicking = $pickedQTY;
        }

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $wvData['item_id'],
                'whs_id'  => $wvData['whs_id'],
                'lot'     => $wvData['lot'],
            ]
        );

        $invtAvail     = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;

        $wvDataArr = json_decode($wvData->data, true);
        if (!$wvDataArr) {
            $wvDataArr['avail']    = 0;
            $wvDataArr['allocated'] = 0;
        }

        if ($pickedQTY > $invtAvail + $invtAllocated) {
            // $msg = "Not enough available QTY to pick";
            // throw new \Exception($msg);
        }

        $overAllocatedQTY = 0;
        $overAvailQTY     = 0;

        // using inventory available qty to overpicking
        if ($wvData['act_piece_qty'] >= $wvData['piece_qty']) {
            // over in case over picking continuous
            if ($overPicking > $invtAllocated) {
                $overAllocatedQTY = $invtAllocated;
                $overAvailQTY     = $overPicking - $overAllocatedQTY;
            } else {
                $overAllocatedQTY = $overPicking;
            }

        } elseif ($overPicking > $invtAllocated - $allocatedQTY) {
            // over picking > inventory allocated remain
            $overAllocatedQTY = $invtAllocated - $allocatedQTY;
            // using inventory allocated qty when inventory available qty is not enough
            $overAvailQTY     = $overPicking - $overAllocatedQTY;
        } else {
            $overAllocatedQTY = $overPicking;
        }

        $wvDataArr['avail']     += $overAvailQTY;
        $wvDataArr['allocated'] += $overAllocatedQTY;

        $wvData->data = json_encode($wvDataArr);
        $wvData->update();

        // check value less than zero
        if ($invtAvail < $overAvailQTY) {
            // $msg = "Not enough available QTY to pick";
            // throw new \Exception($msg);
        }

        if ($wvData['act_piece_qty'] >= $wvData['piece_qty']) {
            // over in case over picking continuous
            if ($invtAllocated < $overAllocatedQTY) {
                // $msg = "Not enough allocated QTY to pick";
                // throw new \Exception($msg);
            }
        } elseif ($invtAllocated < $allocatedQTY + $overAllocatedQTY) {
            // $msg = "Not enough allocated QTY to pick";
            // throw new \Exception($msg);
        }

        $reduceAllocate = $allocatedQTY + $overAllocatedQTY;
        $allocatedSql = sprintf('IF(`allocated_qty` < %d, 0, `allocated_qty` - %d)', $reduceAllocate, $reduceAllocate);
        if ($wvData['act_piece_qty'] >= $wvData['piece_qty']) {
            // over in case over picking continuous
            $allocatedSql = sprintf('IF(`allocated_qty` < %d, 0, `allocated_qty` - %d)', $overAllocatedQTY, $overAllocatedQTY);
        }
        $pickedSql    = sprintf('`picked_qty` + %d', $pickedQTY);
        $availSql     = sprintf('IF(`avail` < %d, 0, `avail` - %d)', $overAvailQTY, $overAvailQTY);

        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $wvData['item_id'],
            'whs_id'  => $wvData['whs_id'],
            'lot'     => $wvData['lot'],
        ]);
    }

    private function _updateInvtSmrReturnPicking($wvData, $returnQTY)
    {
        $returnOverPicking = 0;
        // piece qty < actual picked
        if ($wvData['piece_qty'] <= $wvData['act_piece_qty'] - $returnQTY) {
            // over picking
            $returnOverPicking = $returnQTY;
        } elseif ($wvData['act_piece_qty'] > $wvData['piece_qty']) {
            // over picking a part
            $returnOverPicking = $wvData['act_piece_qty'] - $wvData['piece_qty'];
        }

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $wvData['item_id'],
                'whs_id'  => $wvData['whs_id'],
                'lot'     => $wvData['lot'],
            ]
        );

        $invtAvail     = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;
        $invtPicked    = (int)$ivt->picked_qty;

        $wvDataArr = json_decode($wvData->data, true);
        $wvDataArr = json_decode($wvData->data, true);
        if (!$wvDataArr) {
            $wvDataArr['avail']    = 0;
            $wvDataArr['allocated'] = 0;
        }

        $returnAllocatedQTY = 0;
        $returnAvailQTY    = 0;

        // return picking < overpicking
        if ($returnOverPicking < $wvDataArr['allocated'] + $wvDataArr['avail']) {
            // return to available is higher priority
            if ($returnOverPicking < $wvDataArr['avail']) {
                $returnAvailQTY = $returnOverPicking;
            } else {
                $returnAllocatedQTY = $returnOverPicking - $wvDataArr['avail'];
                $returnAvailQTY     = $wvDataArr['avail'];
            }
        } else {
            // return to inventory allocated Qty a part
            $returnAllocatedQTY = $wvDataArr['allocated'];
            $returnAvailQTY     = $returnOverPicking - $wvDataArr['allocated'];
        }

        $wvDataArr['allocated'] = $wvDataArr['allocated'] - $returnAllocatedQTY;
        $wvDataArr['avail']     = $wvDataArr['avail'] - $returnAvailQTY;
        $wvDataArr['avail']     = $wvDataArr['avail'] < 0 ? 0 : $wvDataArr['avail'];

        $wvData->data = json_encode($wvDataArr);
        $wvData->update();

        if ($invtPicked < $returnQTY) {
            $msg = "Not enough picked QTY to return picking";
            throw new \Exception($msg);
        }

        $allocatedSql = sprintf('`allocated_qty` + %d', $returnQTY - $returnAvailQTY);
        $pickedSql    = sprintf('`picked_qty` - %d', $returnQTY);
        $availSql     = sprintf('`avail` + %d', $returnAvailQTY);


        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $wvData['item_id'],
            'whs_id'  => $wvData['whs_id'],
            'lot'     => $wvData['lot'],
        ]);

        // correct Actual Allocated Qty
        $this->inventorySummaryModel->correctActualAllocatedQty(
            $wvData['whs_id'],
            $wvData['item_id'],
            $wvData['lot']
        );
    }

    private function removeItemFromArray($arr, $val)
    {
        unset($arr[array_search($val, $arr)]);

        return array_values($arr);
    }

    private function checkActiveCarton($data)
    {
        $ctnRFID = $data['ctn-rfid'];
        $ctnLists = $data['ctnLists'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id' => $e['ctn_id'],
                'rfid'   => $e['rfid'],
            ];
        }, $ctnLists);

        $allCtns = array_pluck($tempCtns, null, "rfid");

        foreach ($ctnRFID as $key => $value) {
            if (!isset($allCtns[$value])) {
                $msg = "Carton " . $value . " is not active or existing";
                throw new \Exception($msg);
            }
        }
    }

    private function updateCartonInfoToPicked($cartonList)
    {
        foreach ($cartonList as $ctn) {
            $created_at = array_get($ctn, 'created_at', 0);

            // Calculate storage_duration
            if (is_string($created_at) || is_int($created_at)) {
                $created_at = (int)$created_at;
            } else {
                $created_at = $created_at->timestamp;
            }

            $storageDate = date("Y-m-d", $created_at);

            $pickedDate = date("Y-m-d");

            $dateDiff = (int)round(abs(strtotime($storageDate) - strtotime($pickedDate)) / 86400);

            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }

            $dataCartonUpdate = [
                'ctn_sts'          => Status::getByKey('CTN_STATUS', 'PICKED'),
                'picked_dt'        => time(),
                'storage_duration' => $storageDuration,
                'loc_id'           => null,
                'loc_type_code'    => null,
                'loc_code'         => null,
//                'loc_name'         => null,
                // 'plt_id'           => null,
            ];

            $this->cartonModel->updateWhere($dataCartonUpdate, [
                'ctn_id' => $ctn['ctn_id']
            ]);

            $this->updatePalletPick($ctn);
        }
    }

    private function updatePalletPick($ctn)
    {
        // Update ctn_ttl in pallet table
        $plt_id = array_get($ctn, 'plt_id', null);

        $palletInfo = $this->palletModel->getFirstWhere(['plt_id' => $plt_id]);

        $this->palletModel->updatePalletCtnTtlByPltId($plt_id);

        //update total of carton on pallet
        if (0 == $palletInfo['ctn_ttl']) {
            $this->palletModel->updatePallet($palletInfo);
        }
    }

    private function _assignCartonToPallet($wvData, $ctnRfids, $palletRfid, $returnQTY)
    {
        $pallet = $this->palletModel->getFirstWhere([
            'whs_id' => $wvData['whs_id'],
            'rfid'   => $palletRfid,
        ]);

        if (!$pallet) {
            $wvNum = object_get($wvData, 'wv_num');

            $pltNum = $this->palletModel->generatePalletNumFromWvNum($wvNum);

            $ctnTtl = count($ctnRfids);
            $data = [
                "cus_id"           => $wvData['cus_id'],
                "whs_id"           => $wvData['whs_id'],
                "plt_num"          => $pltNum,
                "rfid"             => $palletRfid,
                "ctn_ttl"          => $ctnTtl,
                "init_ctn_ttl"     => $ctnTtl,
                "storage_duration" => 0,
                "plt_sts"          => "AC",
            ];
            $pallet = $this->palletModel->create($data);
        } else {
            // update pallet
            $pallet->loc_id           = null;
            $pallet->loc_code         = null;
            $pallet->loc_name         = null;
            $pallet->ctn_ttl          += count($ctnRfids);
            $pallet->updated_at       = time();
            $pallet->zero_date        = time();
            $pallet->storage_duration = 0;

            $pallet->save();
        }

        // update cartons status
        $this->cartonModel->getModel()
                        ->whereIn('rfid', $ctnRfids)
                        ->update([
                                'ctn_sts'          => Status::getByKey('CTN_STATUS', 'ACTIVE'),
                                'picked_dt'        => 0,
                                'storage_duration' => 0,
                                'loc_id'           => null,
                                'loc_type_code'    => null,
                                'loc_code'         => null,
                                'loc_name'         => null,
                                'plt_id'           => object_get($pallet, 'plt_id'),
                                'updated_at'       => time(),
                                'updated_by'       => Data::getCurrentUserId(),
                            ]);
    }

    private function _assignWrongCartonToPallet($whsId, $ctnRfids, $palletRfid)
    {
        $pallet = $this->palletModel->getFirstWhere([
            'whs_id' => $whsId,
            'rfid'   => $palletRfid,
        ]);

        $ctnTtl = count($ctnRfids);
        if (!$pallet) {
            $pltNum = $this->palletModel->generatePalletNumFromRfid($palletRfid);

            $cartonEx = $this->cartonModel->getModel()
                        ->whereIn('rfid', $ctnRfids)->first();
            $data = [
                "cus_id"           => object_get($cartonEx, 'cus_id'),
                "whs_id"           => $whsId,
                "plt_num"          => $pltNum,
                "rfid"             => $palletRfid,
                "ctn_ttl"          => $ctnTtl,
                "init_ctn_ttl"     => $ctnTtl,
                "storage_duration" => 0,
                "plt_sts"          => "AC",
            ];
            $pallet = $this->palletModel->create($data);
        } else {
            // update pallet
            $pallet->loc_id           = null;
            $pallet->loc_code         = null;
            $pallet->loc_name         = null;
            $pallet->ctn_ttl          += $ctnTtl;
            $pallet->init_ctn_ttl     += $ctnTtl;
            $pallet->updated_at       = time();
            $pallet->zero_date        = time();
            $pallet->storage_duration = 0;

            $pallet->save();
        }

        // update cartons status
        $this->cartonModel->getModel()
                        ->whereIn('rfid', $ctnRfids)
                        ->update([
                                // 'ctn_sts'          => Status::getByKey('CTN_STATUS', 'ACTIVE'),
                                'picked_dt'        => 0,
                                'storage_duration' => 0,
                                'loc_id'           => null,
                                'loc_type_code'    => null,
                                'loc_code'         => null,
                                'loc_name'         => null,
                                'plt_id'           => object_get($pallet, 'plt_id'),
                                'updated_at'       => time(),
                                'updated_by'       => Data::getCurrentUserId(),
                            ]);
    }

    private function _updateWvDtlLocInReturn($cartons, $wvDtl)
    {
        $wvDtlLoc = (new WaveDtlLocModel())->getModel()->where('wv_dtl_id', $wvDtl->wv_dtl_id)->first();

        if ($wvDtlLoc && $wvDtlLoc->act_loc_ids) {
            $actLocs = json_decode($wvDtlLoc->act_loc_ids, true);
            $ctnIdsCheck = array_pluck($cartons, 'ctn_id');
            foreach ($actLocs as $keyLoc => $actLoc) {
                foreach ($actLoc['cartons'] as $keyCtn => $cartonOld) {
                    if (in_array($cartonOld['ctn_id'], $ctnIdsCheck)) {
                        // remove carton
                        unset($actLocs[$keyLoc]['cartons'][$keyCtn]);
                        // update location
                        foreach ($cartons as $carton) {
                            if ($cartonOld['ctn_id'] == $carton['ctn_id']) {
                                // return QTY
                                $actLocs[$keyLoc]['picked_qty'] -= $carton['piece_remain'];
                                break;
                            }
                        }

                    }
                }
                // update carton
                $actLocs[$keyLoc]['cartons'] = array_values($actLocs[$keyLoc]['cartons']);
                if (count($actLocs[$keyLoc]['cartons']) == 0) {
                    // remove location
                    unset($actLocs[$keyLoc]);
                }
            }
            $wvDtlLoc->act_loc_ids = json_encode(array_values($actLocs));

            $wvDtlLoc->update();
        }
    }

    private function _setErrorMsg($msg)
    {
        $this->_errorMsgs[] = $msg;
    }

    public function getErrorMsg()
    {
        return $this->_errorMsgs;
    }

    private function _computeAJCarton($whsId, $wvDtl, $palletRfid, $countAJ, $rfidsAJ)
    {
        $itemId   = array_get($wvDtl, 'item_id');
        $packSize = array_get($wvDtl, 'pack_size');
        $lot      = array_get($wvDtl, 'lot');

        /* WAP-592 - Change work flow of Cartons with status AJ*/
        if ($palletRfid != "" && !empty($palletRfid)) {
            $pltObj = $this->palletModel->getFirstWhere(['rfid' => $palletRfid]);

            // get carton that rfid is null and belong to the pallet
            if ($pltObj) {
                // update carton status to AJ and remove location.
                $rawCtn = sprintf("rfid LIKE '%s%s'", self::PREFIX_CARTON_RFID_CYCLE_COUNT, "%");
                $virtualCartons = $this->cartonModel->getModel()
                    ->where('plt_id', $pltObj->plt_id)
                    ->where('whs_id', $whsId)
                    ->where('item_id', $itemId)
                    ->where('ctn_pack_size', $packSize)
                    ->where('ctn_sts', 'AC')
                    ->where('lot', $lot)
                    ->whereRaw($rawCtn)
                    ->get();

                $dataAJUpdateOdrCartons = [];
                foreach ($virtualCartons as $virtualCarton)
                {
                    if ($countAJ > 0){
                        $dataAJUpdateOdrCartons[] = [
                            'ctn_rfid'  => array_shift($rfidsAJ),
                            'plt_id'    => $virtualCarton->plt_id,
                            'loc_id'    => $virtualCarton->loc_id,
                            'loc_code'  => $virtualCarton->loc_code,
                        ];
                        // $virtualCarton->plt_id   = null;
                        $virtualCarton->loc_id   = null;
                        $virtualCarton->loc_code = null;
//                        $virtualCarton->loc_name = null;
                        $virtualCarton->ctn_sts  = Status::getByKey('CTN_STATUS', 'ADJUSTED');
                        $virtualCarton->save();
                        $countAJ--;
                    } else {
                        break;
                    }
                }

                $this->_updateCtnAJAndPltForOdrCartons($dataAJUpdateOdrCartons);

                //remove pallet when all carton rfids are cycle count
//                $this->palletModel->removePalletWhenCtnRfidsAreCC([$pltObj->plt_id], $whs_id, self::PREFIX_CARTON_RFID_CYCLE_COUNT);
            }
            if ($countAJ > 0){
                $this->_removeRandomCartonAJ($whsId, $itemId, $packSize, $lot, $countAJ, $rfidsAJ);
            }
        } else {
            // update carton status to AJ and remove location.
            if ($countAJ > 0){
                $this->_removeRandomCartonAJ($whsId, $itemId, $packSize, $lot, $countAJ, $rfidsAJ);
            }

                //remove pallet when all carton rfids are cycle count
//                $this->palletModel->removePalletWhenCtnRfidsAreCC($pltIds, $whsId, self::PREFIX_CARTON_RFID_CYCLE_COUNT);
        }
        /* END - WAP-592 - Change work flow of Cartons with status AJ*/
    }

    private function _removeRandomCartonAJ($whsId, $itemId, $packSize, $lot, $countAJ, $rfidsAJ)
    {
        // update carton status to AJ and remove location.
        $rawCtn = sprintf("rfid LIKE '%s%s'", self::PREFIX_CARTON_RFID_CYCLE_COUNT, "%");
        $virtualCartons = $this->cartonModel->getModel()
            ->where('whs_id', $whsId)
            ->where('item_id', $itemId)
            ->where('ctn_pack_size', $packSize)
            ->where('ctn_sts', 'AC')
            ->where('lot', $lot)
            ->whereRaw($rawCtn)
            ->get();

        $dataAJUpdateOdrCartons = [];
        foreach ($virtualCartons as $virtualCarton)
        {
            if ($countAJ > 0){
                $dataAJUpdateOdrCartons[] = [
                    'ctn_rfid'  => array_shift($rfidsAJ),
                    'plt_id'    => $virtualCarton->plt_id,
                    'loc_id'    => $virtualCarton->loc_id,
                    'loc_code'  => $virtualCarton->loc_code,
                ];
                // $virtualCarton->plt_id   = null;
                $virtualCarton->loc_id   = null;
                $virtualCarton->loc_code = null;
//                        $virtualCarton->loc_name = null;
                $virtualCarton->ctn_sts  = Status::getByKey('CTN_STATUS', 'ADJUSTED');
                $virtualCarton->save();
                $countAJ--;
            } else {
                break;
            }
        }

        $this->_updateCtnAJAndPltForOdrCartons($dataAJUpdateOdrCartons);

    }

    private function _isAutoAssignCartonsToOrder($wvId)
    {
        $countOdr = $this->orderHdrModel->getModel()
            ->where('wv_id', $wvId)
            ->whereNull('org_odr_id')
            ->count();

        if ($countOdr <= 1) {
            return true;
        }
        return false;
    }

    private function _updateReleasedLocation($locIds)
    {
        return $this->locationModel->getModel()
            ->whereIn('loc_id', $locIds)
            // ->where('loc_sts_code', self::STATUS_ALLOCATE_LOCATION)
            ->update([
                // 'loc_sts_code' => 'AC',
                'reserved_at'  => 0
            ]);
    }

    private function _updateProcessingLocationForCartons($whsId, $rfids)
    {
        $locObj = $this->locationModel->getModel()
            ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->where('loc_type.loc_type_code', self::LOCATION_TYPE_PROCESSING) // processing
            ->where('location.loc_whs_id', $whsId)
            ->first();

        if ($locObj) {
            return $this->cartonModel->getModel()
                ->whereIn('rfid', $rfids)
                ->where('whs_id', $whsId)
                ->update([
                    'loc_id'        => $locObj->loc_id,
                    'loc_code'      => $locObj->loc_code,
                    'loc_name'      => $locObj->loc_alternative_name,
                    'loc_type_code' => self::LOCATION_TYPE_PROCESSING,
                ]);
        }
        return false;
    }

    private function _updateCtnAJAndPltForOdrCartons($datas)
    {
        $pltArr = [];
        foreach ($datas as $data) {
            $ctnRfid = array_get($data, 'ctn_rfid');
            $pltId = array_get($data, 'plt_id');
            $locId = array_get($data, 'loc_id');
            $locCode = array_get($data, 'loc_code');
            if ($pltId) {
                if ($pltId != array_get($pltArr, 'plt_id')) {
                    $pltArr = DB::table('pallet')->where('plt_id', $pltId)->first();
                }
                $pltRfid = array_get($pltArr, 'rfid');
                DB::table('odr_cartons')->where('ctn_rfid', $ctnRfid)
                    ->where('deleted', 0)
                    ->update([
                        'plt_id'   => $pltId,
                        'plt_rfid' => $pltRfid,
                    ]);
            }

            if ($locId) {
                DB::table('odr_cartons')->where('ctn_rfid', $ctnRfid)
                    ->where('deleted', 0)
                    ->whereNull('loc_id')
                    ->update([
                        'loc_id'   => $locId,
                        'loc_code' => $locCode,
                    ]);
            }
        }
    }

    public function generateNewCarton($carton, $seqCtn){
        $cartonObj = $this->cartonModel->getFirstWhere(['rfid' => $carton->rfid]);
        $odrCartonObj = $this->orderCartonModel->getFirstWhere(['odr_ctn_id' => $carton->odr_ctn_id]);
        $realRfid = $carton->rfid;
        $fakeRfid = $this->generateCtnRfidFromWavepick($carton->wv_hdr_id, $seqCtn);
        $cartonObj->rfid = $fakeRfid;
        $cartonObj->save();
        if ($odrCartonObj) {
            $odrCartonObj->ctn_rfid = $fakeRfid;
            $odrCartonObj->save();
        }

        $newCarton = clone $cartonObj;
        $newCarton->rfid = $realRfid;
        $newCarton->ctn_sts = 'AC';
        $newCarton->ctn_num = $this->cartonModel->generateCtnNum();
        $newCarton->exists = false;
        $newCarton->ctn_id = null;
        $newCarton->save();
    }

    public function generateCtnRfidFromWavepick($wvHdrId, $seq = 0)
    {
        $wv = WavepickHdr::find($wvHdrId);
        //create CTN number by Wavepick number number
        $ctnCode = str_replace('WAV', self::PREFIX_CARTON_RFID_CYCLE_COUNT, $wv->wv_num);
        $ctnCode = str_replace('-', '', $ctnCode);
        if (!$seq) {
            //get current carton number, count null is 0 so need add 1 for the first
            $seq = $this->orderCartonModel->where('wv_hdr_id', $wv->wv_id)->count() + 1;
        }

        // max length of carton rfid is 24
        if (strlen($ctnCode) > 10) {
            $ctnCode = substr($ctnCode, 4 + strlen($ctnCode) - 20);
            $ctnCode = self::PREFIX_CARTON_RFID_CYCLE_COUNT . $ctnCode;
        }

        $ctnRfid = $ctnCode . 'O' . time(). str_pad($seq, 24 - 11 - strlen($ctnCode), "0", STR_PAD_LEFT);

        return $ctnRfid;
    }
}
