<?php

namespace Jobs;

use Fluent\Logger\FluentLogger;
use VladimirYuldashev\LaravelQueueRabbitMQ\Queue\Jobs\RabbitMQJob;

class Log {

    function fire($job, $data){
        $logger = FluentLogger::open(\Config::get('log.host'), \Config::get('log.port'));
        $logger->post(\Config::get('log.tag'), $data['payload']);
        $job->delete();
    }

}