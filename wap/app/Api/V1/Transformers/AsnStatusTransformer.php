<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnStatus;

class AsnStatusTransformer extends TransformerAbstract
{

    /**
     * ps:Show ASN status
     * @param AsnStatus $asnStatus
     *
     * @return array
     */
    public function transform(AsnStatus $asnStatus)
    {
        return [
            'asn_sts' => $asnStatus->asn_sts_code,
            'asn_sts_name' => $asnStatus->asn_sts_name,
            'asn_sts_des' => $asnStatus->asn_sts_des,
        ];
    }
}
