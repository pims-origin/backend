<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerLocationModel;
use App\Api\V1\Models\CustomerWarehouseModel;
use App\Api\V1\Models\CustomerZoneModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Transformers\CustomerZoneTransformer;
use App\Api\V1\Validators\SaveMassCustomerZoneValidator;
use App\Api\V1\Validators\CustomerZoneValidator;
use App\Api\V1\Validators\ZoneSaveMassValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\V1\Models\CustomerModel;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;

class CustomerZoneController extends AbstractController
{
    /**
     * @var $customerZoneModel
     */
    protected $customerZoneModel;
    /**
     * @var $customerZoneTransformer
     */
    protected $customerZoneTransformer;
    /**
     * @var $customerZoneValidator
     */
    protected $customerZoneValidator;
    /**
     * @var $saveMassCusZoneValidator
     */
    protected $saveMassCusZoneValidator;
    /**
     * @var $customerModel
     */
    protected $customerModel;

    /**
     * @var $cartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     */
    public function __construct(
        CartonModel $cartonModel,
        LocationModel $locationModel
    ) {
        parent::__construct();

        $this->customerZoneModel = new CustomerZoneModel();
        $this->customerZoneTransformer = new CustomerZoneTransformer();
        $this->customerZoneValidator = new CustomerZoneValidator();
        $this->saveMassCusZoneValidator = new SaveMassCustomerZoneValidator();
        $this->customerModel = new CustomerModel();
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
    }

    /**
     * @param $customerId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function index($customerId, Request $request)
    {
        $input = $request->getQueryParams();

        try {
            $meta = $this->customerZoneModel->searchZoneByCustomer(['cus_id' => $customerId],
                ['zone', 'zone.warehouse'], array_get($input, 'limit'));

            return $this->response->paginator($meta, $this->customerZoneTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Read Customer Zone
     *
     * @param int $customerId
     * @param int $zoneId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(
        $customerId,
        $zoneId
    ) {
        try {
            $customerZone = $this->customerZoneModel->loadBy([
                'zone_id' => $zoneId,
                'cus_id'  => $customerId
            ], ['customer', 'zone'])->first();

            return $this->response->item($customerZone, $this->customerZoneTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Read By Zone
     *
     * @param $zoneId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function loadCustomerByZone(
        $zoneId
    ) {
        try {
            $customerZone = $this->customerZoneModel->getFirstBy('zone_id', $zoneId, ['customer']);

            return $this->response->item($customerZone, $this->customerZoneTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $customerId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(
        $customerId,
        Request $request
    ) {
        // get input from request
        $input = $request->getParsedBody();

        $this->customerZoneValidator->validate($input);

        $params = [
            'cus_id'  => $customerId,
            'zone_id' => $input['zone_id']
        ];

        try {
            if (($customer = $this->customerZoneModel->getFirstBy('zone_id', $params['zone_id']))
                && $customer->cus_id == $customerId
            ) {
                throw new \Exception(Message::get("BM006", "Zone"));
            }

            //save zone id from customer_zone table
            if ($cusZone = $this->customerZoneModel->create($params)) {
                return $this->response->item($cusZone, $this->customerZoneTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $customerId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function saveMassAndDeleteUnused(
        $customerId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->saveMassCusZoneValidator->validate($input);

        // Check Customer not exist
        if (!$this->customerModel->loadBy(['cus_id' => $customerId])->first()) {
            return $this->response->errorBadRequest(Message::get("BM017", "customer"));
        }
        $meta = $this->customerZoneModel->getByCustomerId($customerId);

        //meta[Zone => Customer]
        $meta = array_pluck($meta, 'cus_id', 'zone_id');
        foreach ($input['zone_id'] as $zoneId) {
            if (isset($meta[$zoneId])) {

                unset($meta[$zoneId]);

            } else {
                $params = [
                    'zone_id' => $zoneId,
                    'cus_id'  => $customerId,
                ];
                // check duplicated
                if ($this->customerZoneModel->checkWhere([
                    'zone_id' => $zoneId
                ])
                ) {
                    throw new \Exception(Message::get("BM006", "Zone"));
                }

                try {
                    $this->customerZoneModel->refreshModel();
                    $this->customerZoneModel->create($params);
                    } catch (\PDOException $e) {
                        return  $this->response->errorBadRequest(
                            SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
                        );
                } catch (\Exception $e) {
                    return $this->response->errorBadRequest($e->getMessage());
                }
            }
        }
        /**
         * delete unused meta, $meta[Zone => Customer]
         */
        if ($meta) {
            //Locations cannot be removed while having inventories! (Location has already item)
            $locationInfo = $this->locationModel->loadByZoneIds(array_keys($meta));

            $locationIds = array_pluck($locationInfo, 'loc_id');
            if ($this->cartonModel->checkWhereIn($locationIds)
            ) {
                return $this->response->errorBadRequest(Message::get("BM031"));
            }

            try {
                //Zones cannot be removed while having inventories! (Zone has already item)
                $location = $this->locationModel->loadByZoneIds(array_keys($meta));
                $locationArr = json_decode($location, true);
                $locationIdArr = array_pluck($locationArr, 'loc_id');
                if ($this->cartonModel->checkWhereIn($locationIdArr)) {
                    throw new \Exception("Zones cannot be removed while having inventories!");
                }

                $this->customerZoneModel->refreshModel();
                $this->customerZoneModel->deleteZonesByCustomerId($customerId, array_keys($meta));
            } catch (\PDOException $e) {
                return $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }

        return $this->response->noContent();
    }

    /**
     * @param $customerId
     * @param $zoneId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function destroy($customerId, $zoneId)
    {
        //Locations cannot be removed while having inventories! (Location has already item)
        $locationInfo = $this->locationModel->findWhere([
            'loc_zone_id' => $zoneId,
        ]);
        $locationIds = array_pluck($locationInfo, 'loc_id');
        if ($this->cartonModel->checkWhereIn($locationIds)
        ) {
            return $this->response->errorBadRequest(Message::get("BM031"));
        }

        try {
            $this->customerZoneModel->deleteCustomerZone($customerId, $zoneId);

            return $this->response->noContent();

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}
