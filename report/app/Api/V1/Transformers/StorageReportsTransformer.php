<?php

namespace App\Api\V1\Transformers;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;
use League\Fractal\TransformerAbstract;

class StorageReportsTransformer extends TransformerAbstract
{
    public function transform($model)
    {
        return $model->toArray();
    }

}
