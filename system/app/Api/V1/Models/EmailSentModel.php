<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\EmailSent;


class EmailSentModel extends AbstractModel
{
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new EmailSent();
    }
}
