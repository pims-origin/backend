<?php
namespace common\models;

use yii\db\ActiveRecord;

class Warehouse extends ActiveRecord
{
    public static function tableName()
    {
        return 'warehouse';
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_id' => 'user_id'])
            ->viaTable('user_warehouse', ['whs_id' => 'whs_id']);
    }
}