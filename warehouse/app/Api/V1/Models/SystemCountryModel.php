<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/24/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;



use Seldat\Wms2\Models\SystemCountry;

class SystemCountryModel extends AbstractModel
{
    /**
     *
     */
    public function __construct(SystemCountry $model = null)
    {
        $this->model = ($model) ?: new SystemCountry();
    }
}