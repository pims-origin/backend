<?php
    
    namespace App\Api\V1\Controllers;
    
    use App\Api\V1\Models\ReportConfigsModel;
    use App\Api\V1\Models\ReportSkuSummarysModel;
    use App\Api\V1\Transformers\ReportSkuSummarysTransformer;
    use App\Api\V1\Validators\ReportSkuSummaryValidator;
    use Box\Spout\Common\Type;
    use Box\Spout\Writer\WriterFactory;
    use DB;
    use Illuminate\Http\Response as IlluminateResponse;
    use Illuminate\Support\Facades\Log;
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Seldat\Wms2\Utils\Message;
    use Seldat\Wms2\Utils\SystemBug;
    
    class ReportSkuSummaryController extends AbstractController
    {
        /**
         * @var $validator
         */
        protected $validator;
        /**
         * @var ReportSkuSummarysModel
         */
        protected $reportSkuSummarysModel;
        /**
         * @var ReportConfigsModel
         */
        protected $reportConfigsModel;
        
        public function __construct
        (
            ReportSkuSummarysModel $reportSkuSummarysModel,
            ReportConfigsModel $reportConfigsModel,
            ReportSkuSummaryValidator $validator
        
        ) {
            $this->reportSkuSummarysModel = $reportSkuSummarysModel;
            $this->reportConfigsModel = $reportConfigsModel;
            $this->validator = $validator;
        }
        
        /**
         * @param Request $request
         * @param ReportSkuSummarysTransformer $reportSkuSummaryTransformer
         *
         * @return \Dingo\Api\Http\Response|void
         */
        public function getReportSkuSummary(
            Request $request,
            ReportSkuSummarysTransformer $reportSkuSummaryTransformer
        ) {
            
            //$data = $reportSkuSummarysModel->all();
            //
            //if (empty($data)) {
            //    throw new \Exception("No results");
            //}
            
            /** @var TYPE_NAME $data */
            //return $this->response->collection($data, new ReportSkuSummarysTransformer)
            //    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            
            // get data from HTTP
            $input = $request->getQueryParams();
            
            if (!empty($input['export']) && $input['export'] == 1) {
                $this->export($input);
                die;
            }
            
            try {
                $reportSkuSummary = $this->reportSkuSummarysModel->search($input, [
                
                ], array_get($input, 'limit'));
                
                return $this->response->paginator($reportSkuSummary, $reportSkuSummaryTransformer);
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }
        
        /**
         * @param $input
         *
         * @return $this|void
         * @throws \Exception
         */
        private function export(
            $input
        ) {
            try {
                $reportSkuSummarys = $this->reportSkuSummarysModel->search($input, [
                ], null, true);
                
                //----------get tittle
                $reportSkuSummarysArr = $reportSkuSummarys->toArray();
                
                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $cusMeta = $this->reportConfigsModel->getFirstWhere([
                    'qualifier' => 'SSR'
                ],
                    [],
                    ['level' => "desc"]);
                
                $cusMetaValJson = array_get($cusMeta, 'data', '');
                $cusMetaValues = json_decode($cusMetaValJson, true);
                //-------Sort according to index (SORT_ASC, SORT_DESC)
                $titleOrg = [];
                if (!empty($cusMetaValues)) {
                    foreach ($cusMetaValues as $cusMetaKey => $cusMetaValue) {
                        if ($cusMetaValue['show'] == true) {
                            $titleOrg[] = [
                                'name'               => $cusMetaKey,
                                'show'               => array_get($cusMetaValue, 'show', null),
                                'fixed'              => array_get($cusMetaValue, 'fixed', null),
                                'index'              => array_get($cusMetaValue, 'index', null),
                                'width'              => array_get($cusMetaValue, 'width', null),
                                'condition'          => array_get($cusMetaValue, 'condition', null),
                                'searchType'         => array_get($cusMetaValue, 'search_type', null),
                                'displayName'        => array_get($cusMetaValue, 'display_name', null),
                                'defaultDisplayName' => array_get($cusMetaValue, 'default_display_name', null),
                            
                            ];
                        }
                    }
                }
                $result = $titleOrg;
                
                $cusMetaValues = $this->array_msort($result, ['index' => SORT_ASC]);
                //-------/Sort according to index (SORT_ASC, SORT_DESC)
                
                $titleOrg = [];
                if (!empty($cusMetaValues)) {
                    foreach ($cusMetaValues as $cusMetaValue) {
                        if ($cusMetaValue['show'] == true && !empty($cusMetaValue['displayName'])) {
                            $titleOrg[$cusMetaValue['name']] = $cusMetaValue['displayName'];
                        } elseif ($cusMetaValue['show'] == true && empty($cusMetaValue['displayName'])) {
                            $titleOrg[$cusMetaValue['name']] = $cusMetaValue['defaultDisplayName'];
                        }
                    }
                }
                //----------/get tittle
                $title = $titleOrg;
                //get user Lang
                $userLang = Message::userLang();
                if (!$userLang) {
                    return $this->response->errorBadRequest("User ID not found!");
                }
                if ($userLang == 'he_IL') {
                    $title = array_reverse($title);
                }
                
                $filePath = storage_path() . "/Report_GR.csv";
                $filePath = "Report_GR";
                $this->saveFile($title, $reportSkuSummarysArr, $filePath);
                
                return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);
            } catch (\PDOException $e) {
                return $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }
        
        function array_msort($array, $cols)
        {
            $colarr = [];
            foreach ($cols as $col => $order) {
                $colarr[$col] = [];
                foreach ($array as $k => $row) {
                    $colarr[$col]['_' . $k] = strtolower($row[$col]);
                }
            }
            $params = [];
            foreach ($cols as $col => $order) {
                $params[] =& $colarr[$col];
                $params = array_merge($params, (array)$order);
            }
            call_user_func_array('array_multisort', $params);
            $ret = [];
            $keys = [];
            $first = true;
            foreach ($colarr as $col => $arr) {
                foreach ($arr as $k => $v) {
                    if ($first) {
                        $keys[$k] = substr($k, 1);
                    }
                    $k = $keys[$k];
                    if (!isset($ret[$k])) {
                        $ret[$k] = $array[$k];
                    }
                    $ret[$k][$col] = $array[$k][$col];
                }
                $first = false;
            }
            
            return $ret;
        }
        
        /**
         * @param array $title
         * @param array $data
         * @param $filePath
         *
         * @return bool
         */
        private function saveFile(array $title, array $data, $filePath)
        {
            if (empty($data) || empty($title)) {
                return false;
            }
            
            $writer = WriterFactory::create(Type::CSV);
            $writer->openToBrowser($filePath);
            
            $dataSave[] = array_values($title);
            foreach ($data as $item) {
                $temp = [];
                foreach ($title as $key => $field) {
                    $values = explode("|", $key);
                    $value = "";
                    if (count($values) == 1) {
                        $value = array_get($item, $values[0], null);
                    } else {
                        if (!empty($values[2])) {
                            switch ($values[1]) {
                                case "*":
                                    $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                                    break;
                                case ".":
                                    $value = trim(array_get($item, $values[0], null) . " " .
                                        array_get($item, $values[2], null));
                                    break;
                                case "format()":
                                    $value = date($values[2], array_get($item, $values[0], null));
                                    break;
                            }
                        }
                    }
                    $temp[] = $value;
                }
                $dataSave[] = $temp;
            }
            $writer->addRows($dataSave);
            $writer->close();
        }
        
        public function storeReportSkuSummary(Request $request, ReportSkuSummarysModel $reportSkuSummarysModel)
        {
            // Get data from input
            $input = $request->getParsedBody();
            
            foreach ($input as $key) {
                // Add data input to Array
                $dataSkuSummaryArray = [
                    'whs_code'    => $key['whs_code'],
                    'cus_code'    => $key['cus_code'],
                    'upc'         => array_get($key, 'upc'),
                    'des'         => array_get($key, 'des'),
                    'sku'         => $key['sku'],
                    'size'        => $key['size'],
                    'color'       => $key['color'],
                    'pack'        => $key['pack'],
                    'uom'         => $key['uom'],
                    'in_hand_qty' => array_get($key, 'in_hand_qty'),
                    'in_pick_qty' => array_get($key, 'in_pick_qty'),
                    'total_qty'   => array_get($key, 'total_qty'),
                ];
                
                $checkSkuSummarry = [
                    'whs_code' => $key['whs_code'],
                    'cus_code' => $key['cus_code'],
                    'sku'      => $key['sku'],
                    'size'     => $key['size'],
                    'color'    => $key['color'],
                    'pack'     => $key['pack'],
                ];
                try {
                    if ($reportSkuSummarysModel->findWhere($checkSkuSummarry)->count() == 0) {
                        //  Create
                        Log::info('Insert: ' . $key['sku']);
                        DB::beginTransaction();
                        $reportSkuSummarysModel->refreshModel();
                        $reportSkuSummarysModel->create($dataSkuSummaryArray);
                        DB::commit();
                        Log::info('Insert-End');
                    } else {
                        // Update
                        Log::info('update: ' . $key['sku']);
                        DB::beginTransaction();
                        $reportSkuSummarysModel->updateWhere($dataSkuSummaryArray, $checkSkuSummarry);
                        DB::commit();
                    }
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::info('Insert Error: ' . $key['sku'] . '\n ' . $e->getMessage());
                }
            }
            
            return $this->response->noContent()->setContent(['status' => '201', 'data' => ['Success']])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        }
        
    }