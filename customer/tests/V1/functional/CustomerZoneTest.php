<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use \Seldat\Wms2\Models\Warehouse;
use \Seldat\Wms2\Models\WarehouseMeta;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\Message;

class CustomerZoneTest extends TestCase
{
    use DatabaseTransactions;

    protected $customerId;
    protected $zoneId;

    public function setUp()
    {
        parent::setUp();

        // create data into DB
        $this->customerId = factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        $this->zoneId = factory(\Seldat\Wms2\Models\Zone::class)->create()->zone_id;

    }

    private function getEndPoint()
    {
        return "/v1/customers/{$this->customerId}/zones";
    }

    public function testShow_Ok()
    {
        // make data
        $customerZone = factory(\Seldat\Wms2\Models\CustomerZone::class)->create();
        $this->customerId = $customerZone->cus_id;

        $response = $this->call('GET', $this->getEndPoint() . "/{$customerZone->zone_id}");
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2 Data is not null
        $this->assertNotEmpty($responseData['data']);
    }

    public function testShow_NotExist_Ok()
    {
        $this->customerId = 999999999999999;

        $response = $this->call('GET', $this->getEndPoint() . "/999999999999999999");
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2 Data is not null
        $this->assertEmpty($responseData['data']);
    }

    public function testIndex_Ok()
    {
        // make data
        $customerZone = factory(\Seldat\Wms2\Models\CustomerZone::class)->create();
        $this->customerId = $customerZone->cus_id;

        $response = $this->call('GET', $this->getEndPoint());
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2 Data is not null
        $this->assertNotEmpty($responseData['data']);
    }

    public function testIndex_NotExist_Ok()
    {
        // make data
        $customerZone = factory(\Seldat\Wms2\Models\CustomerZone::class)->create();
        $this->customerId = 9999999999999;

        $response = $this->call('GET', $this->getEndPoint());
        $responseData = json_decode($response->getContent());

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2 Data is not null
        $this->assertEmpty($responseData->data);
    }

    public function testLoadByZone_Ok()
    {
        // make data
        $customerZone = factory(\Seldat\Wms2\Models\CustomerZone::class)->create();
        $zoneId = $customerZone->zone_id;

        $response = $this->call('GET', "v1/customer-zones/{$zoneId}");
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2 Data is not null
        $this->assertNotEmpty($responseData['data']);
    }

    public function testLoadByZone_NotExist_Ok()
    {
        $response = $this->call('GET', "v1/customer-zones/999999999999999999999");
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2 Data is not null
        $this->assertEmpty($responseData['data']);
    }

    public function testStore_Ok()
    {
        $response = $this->call('POST', $this->getEndPoint(), [
            'zone_id' => $this->zoneId,
        ]);
        $responseData = json_decode($response->getContent());

        // Case 1: Response is 201
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        // Case 2 Data is not null
        $this->assertNotEmpty($responseData);
    }

    public function testStore_EmptyField_Ok()
    {
        $response = $this->call('POST', $this->getEndPoint(), []);
        $responseData = json_decode($response->getContent());

        // Case 1: Response is 422
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2 Message is "The zone id field is required."
        $this->assertEquals("The zone id field is required.", $responseData->errors->errors->zone_id[0]);
    }

    public function testStore_Exist_Fail()
    {
        $customerZone = factory(\Seldat\Wms2\Models\CustomerZone::class)->create();
        $this->customerId = $customerZone->cus_id;

        $response = $this->call('POST', $this->getEndPoint(), [
            'zone_id' => $customerZone->zone_id,
        ]);
        $responseData = json_decode($response->getContent());

        // Case 1: Response is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2 Message is "Zone is existed."
        $this->assertEquals(Message::get("BM006", "Zone"), $responseData->errors->message);
    }

    public function testSaveMass_Ok()
    {
        $whs_id = factory(\Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        $customerZone = factory(\Seldat\Wms2\Models\CustomerZone::class)->create();
        $this->customerId = $customerZone->cus_id;

        $response = $this->call('POST', $this->getEndPoint() . "/save-mass-and-delete-unused", [
            'zone_id' => [$customerZone->zone_id],
            'whs_id'  => [$whs_id],
        ]);
        $responseData = json_decode($response->getContent());

        // Case 1: Response is 201
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        // Case 2 Data empty
        $this->assertEmpty($responseData);
    }

    public function testSaveMass_CustomerNotExist_Fail()
    {
        $whs_id = factory(\Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        $customerZone = factory(\Seldat\Wms2\Models\CustomerZone::class)->create();
        $this->customerId = 99999999999999999999;

        $response = $this->call('POST', $this->getEndPoint() . "/save-mass-and-delete-unused", [
            'zone_id' => [$customerZone->zone_id],
            'whs_id'  => [$whs_id],
        ]);

        $responseData = json_decode($response->getContent());

        // Case 1: Response is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2 Message is "This customer is not exist."
        $this->assertEquals(Message::get("BM017", "customer"), $responseData->errors->message);
    }

    public function testDelete_Ok()
    {
        $customerZone = factory(\Seldat\Wms2\Models\CustomerZone::class)->create();

        $response = $this->call('DELETE', $this->getEndPoint() . "/{$customerZone->zone_id}");
        $responseData = json_decode($response->getContent());

        // Case 1: Response is 204
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        // Case 2 Data empty
        $this->assertEmpty($responseData);
    }


}
