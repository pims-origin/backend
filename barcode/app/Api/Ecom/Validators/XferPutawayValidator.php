<?php

namespace App\Api\Ecom\Validators;
use Illuminate\Support\Facades\Input;

class XferPutawayValidator extends AbstractValidator
{

    protected function rules()
    {
        $avail = Input::get('avail_qty');
        return [
            'mez_loc_id'   => 'required|integer',
            'mez_loc_code' => 'required',
            'act_loc_code' => 'required',
            'act_qty' => 'required|integer|max:'.$avail,
            'item_id'  => 'required'
        ];
    }
}
