<?php
/**
 * Created by PhpStorm.
 * user: PhuongHong
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\Status;

class OrderCancelListTransformer extends TransformerAbstract
{

    public function transform(OrderHdr $orderHdr)
    {

        return [
            'odr_id'                => $orderHdr->odr_id,
            'odr_num'               => $orderHdr->odr_num,
            'cus_odr_num'           => $orderHdr->cus_odr_num,
            'total_SKUs'            => object_get($orderHdr, 'odrDtl_NO', 0),
            'cus_po'                => object_get($orderHdr, 'cus_po', null),
            'ref_cod'               => object_get($orderHdr, 'ref_cod', null),
            'rush_odr'              => object_get($orderHdr, 'rush_odr', null),
            'act_cancel_dt'         => (empty(object_get($orderHdr, 'act_cancel_dt', ''))) ? '' :
                date("m/d/Y", object_get($orderHdr, 'act_cancel_dt', '')),
            // Customer
            'cus_id'                => $orderHdr->cus_id,
            'cus_name'              => object_get($orderHdr, 'customer.cus_name', null),
            'cus_code'              => object_get($orderHdr, 'customer.cus_code', null),
            // Order Status
            'odr_sts'               => $orderHdr->odr_sts,
            'odr_sts_name'          => Status::getByKey("Order-Status", $orderHdr->odr_sts),
            // User/CSR
            'csr_id'                => $orderHdr->csr,
            'csr_name'              => trim(object_get($orderHdr, 'csrUser.first_name', null) . " " .
                object_get($orderHdr, 'csrUser.last_name', null)),
            // Order Type
            'origin_odr_type'       => object_get($orderHdr, 'shippingOrder.type', ''),
            'odr_type'              => $orderHdr->odr_type,
            'odr_type_name'         => Status::getByKey("Order-Type", $orderHdr->odr_type),
            //Back Order List
            'backOrderList'         => $orderHdr->backOrderList,
            //Work Order List
            'workOrderList_sku_ttl' => $orderHdr->workOrderList_sku_ttl,
            'workOrderList'         => $orderHdr->workOrderList,
            //WavePickList
            'wavPick_sku_ttl'       => $orderHdr->wavPick_sku_ttl,
            'wavPick_ctn_ttl'       => $orderHdr->wavePick_ctn_ttl,
            'wavePickList'          => $orderHdr->wavePickList,
            //packList
            'carton_ttl'            => $orderHdr->carton_ttl,
            'packList_sku_ttl'      => $orderHdr->packList_sku_ttl,
            'packList'              => $orderHdr->packList,
            //palletList
            'carton_inPallets_ttl'  => $orderHdr->carton_inPallets_ttl,
            'pallet_ttl'            => $orderHdr->pallet_ttl,
            'palletList'            => $orderHdr->palletList,

        ];
    }
}
