<?php

namespace Seldat\Wms2\Models;


class GateDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gate_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gate_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'ctnr_id',
        'whs_id',
        'open',
        'close',
        'duration',
        'cus_name',
        'ctnr_num',
        'gate_dtl_sts',
        'gate_id',
        'gate_name'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function container()
    {
        return $this->hasOne(__NAMESPACE__ . '\Container', 'ctnr_id', 'ctnr_id');
    }

    public function gate()
    {
        return $this->hasOne(__NAMESPACE__ . '\Gate', 'gate_id', 'gate_id');
    }

    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }
}
