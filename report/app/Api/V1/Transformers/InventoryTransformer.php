<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Inventory;
use League\Fractal\TransformerAbstract;

class InventoryTransformer extends TransformerAbstract
{

    /*
    ***************************************************************************
    */

    public function transform(Inventory $inventory)
    {
        return [
            'cus_name' => object_get($inventory, 'customer.cus_name', ''),
            'ctnr_num' => $inventory->ctnr_num,
            'gr_hdr_num' => object_get($inventory, 'goodReceiptDtl.goodsReceipt.gr_hdr_num', ''),
            'sys_mea_name' => object_get($inventory, 'AsnDtl.AsnHdr.SystemMeasurement.sys_mea_name', ''),
            'created_at' => $inventory->created_at->format('m/d/Y'),
            'sku' => $inventory->sku,
            'piece_remain' => $inventory->piece_remain,
            'po' => $inventory->po,
            'lot' => $inventory->lot,
            'length' => $inventory->length,
            'width' => $inventory->width,
            'height' => $inventory->height,
            'weight' => $inventory->weight,
            'upc' => $inventory->upc,
            'size' => $inventory->size,
            'color' => $inventory->color,
            'ctn_pack_size' => $inventory->ctn_pack_size,
            'ctn_num' => $inventory->ctn_num,
            'plt_num' => object_get($inventory, 'pallet.plt_num', ''),
            'loc_code' => $inventory->loc_code,
            'ctn_sts' => $inventory->ctn_sts,
        ];
    }

    /*
    ****************************************************************************
    */

}
