<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\PackRefModel;
use App\Api\V1\Transformers\PackRefTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

/**
 * Class PackRefController
 *
 * @package App\Api\V1\Controllers
 */
class PackRefController extends AbstractController
{
    /**
     * @var PackRefModel
     */
    protected $packRefModel;

    /**
     * PackDimensionController constructor.
     */
    public function __construct(PackRefModel $packRefModel)
    {
        $this->packRefModel = $packRefModel;
    }

    /**
     * @param $packRefId
     * @param PackRefTransformer $packRefTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($packRefId, PackRefTransformer $packRefTransformer)
    {
        try {
            $packRef = $this->packRefModel->getFirstBy('pack_ref_id', $packRefId);

            return $this->response->item($packRef, $packRefTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param Request $request
     * @param PackRefTransformer $packRefTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request, PackRefTransformer $packRefTransformer)
    {

        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $packRef = $this->packRefModel->search($input, []);

            return $this->response->collection($packRef, $packRefTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
