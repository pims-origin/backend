<?php

namespace backend\models;

use Yii;
use common\models\Users;

/**
 * This is the model class for table "last_login".
 *
 * @property integer $user_id
 * @property integer $login_at
 * @property integer $expired_at
 * @property string $status
 *
 * @property Users $user
 */
class LastLogin extends \yii\db\ActiveRecord
{
    const STATUS_ACCEPTED = '01';
    const STATUS_DELETED = '07';

    protected $expiredTime;

    public function init()
    {
        parent::init();
    }

    public function getExpiredTime()
    {
        return 24 * 60 * 60;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%last_login}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login_at', 'expired_at'], 'integer'],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id'    => 'User ID',
            'login_at'   => 'Login At',
            'expired_at' => 'Expired At',
            'status'     => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }

    public function login($loginAt)
    {
        $this->login_at = $loginAt;
        $this->expiredTime = $this->getExpiredTime();
        $this->expired_at = $this->login_at + $this->expiredTime;
        $this->status = self::STATUS_ACCEPTED;

        return $this->save();
    }

    public function logout()
    {
        $this->status = self::STATUS_DELETED;

        return $this->save();
    }
}
