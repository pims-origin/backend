<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WavepickDtl;

class WaveDetailTransformer extends TransformerAbstract
{
    //return result
    public function transform(WavepickDtl $wave)
    {
        $total = object_get($wave, 'piece_qty', 0);
        $actual = object_get($wave, 'act_piece_qty', 0);

        $sku_size_color = object_get($wave, 'sku');

        $size = object_get($wave, 'size', null);
        $color = object_get($wave, 'color', null);
        $size = !empty($size) ? strtoupper($size) : '';
        if ($size != 'NA') {
            $sku_size_color .= '-'.$size;
        }
        $color = !empty($color) ? strtoupper($color) : '';
        if ($color != 'NA') {
            $sku_size_color .= '-'.$color;
        }

        return [
            'wv_id'     => object_get($wave, 'wv_id'),
            'wv_dtl_id' => object_get($wave, 'wv_dtl_id'),
            'wv_num'    => object_get($wave, 'wv_num'),
            'sku'       => $sku_size_color,
            'size'      => $size,
            'color'     => $color,
            'qty'       => $total . "/" . $actual,
            'status'    => $total == $actual ? "DONE" : ($actual == 0 ? "NOT" : "IN")
        ];
    }

}
