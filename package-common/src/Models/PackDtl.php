<?php

namespace Seldat\Wms2\Models;

/**
 * Class PackDtl
 *
 * @package Seldat\Wms2\Models
 */
class PackDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pack_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pack_dtl_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'pack_dtl_id',
        'pack_hdr_id',
        'odr_hdr_id',
        'whs_id',
        'cus_id',
        'item_id',
        'size',
        'lot',
        'sku',
        'color',
        'cus_upc',
        'uom_id',
        'piece_qty',
        'sts',
        'weight',
    ];

    public function packHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\PackHdr', 'pack_hdr_id', 'pack_hdr_id');
    }

    public function orderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_hdr_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function item()
    {
        return $this->hasOne(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function systemUom()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemUom', 'sys_uom_id', 'uom_id');
    }
}
