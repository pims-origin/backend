<?php

namespace App\Jobs;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;

class LocationHistogramJob extends Job
{
    /**
     * LocationHistogramJob constructor.
     * @param Schedule $schedule
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $locationUseds = DB::table('pallet')
            ->select([
                DB::raw('COUNT(1) as loc_used'),
                'whs_id'
            ])
            ->whereNotNull('loc_id')
            ->groupBy('whs_id')->get();

        $locationTtl = DB::table('location')
            ->select([
                DB::raw('COUNT(1) as loc_ttl'),
                'whs_id'
            ])
            ->groupBy('whs_id')->get();

        $locPalletByWhs = array_pluck($locationUseds, 'loc_used', 'whs_id');
        $locationByWhs = array_pluck($locationTtl, 'loc_ttl', 'whs_id');
        $arrInsert = [];

        if ($locationByWhs) {

            foreach ($locationByWhs as $whsId => $loc_ttl) {
                $loc_used_ttl = $locPalletByWhs[$whsId] ?? 0;

                $arrInsert[] = [
                    'whs_id' => $whsId,
                    'loc_used_ttl' => $loc_used_ttl,
                    'loc_avai_ttl' => $loc_ttl - $loc_used_ttl,
                    'loc_ttl' => $loc_ttl,
                    'his_dt' => strtotime(time()),
                    'created_at' => time()
                ];
            }

            DB::table('loc_hist')->insert($arrInsert);
        }
    }
}