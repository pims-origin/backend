<?php

namespace Seldat\Wms2\Models;


class TisDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tis_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'tis_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'tis_hdr_id',
        'tis_dtl_name',
        'whs_id',
        'ctn_ttl',
        'sku_ttl',
        'tis_dtl_sts',
        'description',
        'sts',
        'sys_loc_ttl',
        'sys_ctn_ttl',
        'sys_sku_ttl'
    ];

    public function tisHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\TisHdr', 'tis_hdr_id', 'tis_hdr_id');
    }

    public function reports()
    {
        return $this->hasMany(__NAMESPACE__ . '\TisRpt', 'tis_dtl_id', 'tis_dtl_id');
    }
}
