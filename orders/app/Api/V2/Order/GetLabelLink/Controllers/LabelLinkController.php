<?php

namespace App\Api\V2\Order\GetLabelLink\Controllers;

use App\Api\V2\Order\GetLabelLink\Models\OrderLabelLinkModel;
use App\Api\V2\Order\GetLabelLink\Transformers\LabelLinkTransformer;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Wms2\UserInfo\Data;
use mPDF;

/**
 * Class LabelLinkController
 *
 * @package App\Api\V2\Order\GetLabelLink\Controllers
 */
class LabelLinkController extends AbstractController
{
    protected $orderLabelLinkModel;

    /**
     * LabelLinkController constructor.
     *

     * @param OrderLabelLinkModel $orderLabelLinkModel
     */
    public function __construct(
        OrderLabelLinkModel $orderLabelLinkModel
    ) {
        $this->orderLabelLinkModel = $orderLabelLinkModel;
    }

    /**
     * @param $whsId
     * @param $odrId
     * @param $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getList($whsId, $odrId, Request $request)
    {
        $input = $request->getParsedBody();
        try {
            $labelLinks = $this->orderLabelLinkModel->getLabelLink($whsId, $odrId, array_get($input, 'limit', 20));

            return $this->response->paginator($labelLinks, (new LabelLinkTransformer()));

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}
