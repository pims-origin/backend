<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/8/2016
 * Time: 9:26 AM
 */

namespace tests\codeception\backend\api;

use tests\codeception\backend\ApiTester;


class RoleCest
{
    public function _before(ApiTester $I)
    {
        //$I->getDbh()->beginTransaction();

        // Authority - PARENT - ROLE
        $name1 = "authorityTest1";
        $I->haveInDatabase('authority', [
            'name'        => $name1,
            'type'        => '1',
            'description' => 'unit test 1'
        ]);
        //$this->_name1 = $name1;
        $this->_name1 = $I->grabFromDatabase('authority', 'name', ['name' => $name1]);

        // Authority - PARENT - Permission
        $name2 = 'authorityTest2';
        $I->haveInDatabase('authority', [
            'name'        => $name2,
            'type'        => '2',
            'description' => 'unit test 2'
        ]);
        $this->_name2 = $name2;

        $name3 = 'authorityTest3';
        $I->haveInDatabase('authority', [
            'name'        => $name3,
            'type'        => '2',
            'description' => 'unit test 3'
        ]);
        $this->_name3 = $name3;

        $name4 = 'authorityTest4';
        $I->haveInDatabase('authority', [
            'name'        => $name4,
            'type'        => '2',
            'description' => 'unit test 4'
        ]);
        $this->_name4 = $name4;

        // Authority - CHILD
        $I->haveInDatabase('authority_child', [
            'parent' => $name1,
            'child'  => 'authorityTest2'
        ]);

        $I->haveInDatabase('authority_child', [
            'parent' => 'Admin',
            'child'  => 'authorityTest1'
        ]);

        $I->haveInDatabase('authority_child', [
            'parent' => 'Admin',
            'child'  => 'authorityTest3'
        ]);

        $I->haveInDatabase('authority_child', [
            'parent' => 'Admin',
            'child'  => 'authorityTest4'
        ]);

        // User
        $this->_user_id = $I->haveInDatabase('users', [
            'first_name' => 'fi12T',
            'last_name'  => 'l2sT',
            'email'      => 'test@codecept.com',
            'password'   => '$2y$13$dWhcDzI0Ry.D4sCxME0L3evfLqsjlpt.Ip0MHKoVP.qwECqrExqOa', //Seldat123
            'status'     => 'AC',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->_user_id2 = $I->haveInDatabase('users', [
            'first_name' => 'fi12T2',
            'last_name'  => 'l2sT2',
            'email'      => 'test2@codecept.com',
            'password'   => '$2y$13$dWhcDzI0Ry.D4sCxME0L3evfLqsjlpt.Ip0MHKoVP.qwECqrExqOa', //Seldat123
            'status'     => 'AC',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        // User Role
        $I->haveInDatabase('user_role', [
            'item_name' => 'Admin',
            'user_id'   => $this->_user_id,
        ]);


    }

    public function _after(ApiTester $I)
    {
        //I->getDbh()->rollBack();
        //$I->deleteFromDatabase('users', ['user_id' => $this->_user_id]);
    }

    public function test_view_ok(ApiTester $I)
    {
        $I->sendGET("/roles/" . $this->_name1);
        $I->seeResponseCodeIs(200);
    }

    public function test_permission_ok(ApiTester $I)
    {
        $I->sendGET("/roles?roleName=" . $this->_name1 . "/permissions");
        $I->seeResponseCodeIs(200);
    }

    public function test_byUser_ok(ApiTester $I)
    {
        $I->sendGET("/users/" . $this->_user_id . "/roles");
        $I->seeResponseCodeIs(200);
    }

    public function test_user_ok(ApiTester $I)
    {
        $I->sendGET("/roles?roleName=" . $this->_name1 . "/users");
        $I->seeResponseCodeIs(200);
    }

    public function test_create_ok(ApiTester $I)
    {
        $I->sendPOST("/roles", [
            'name'        => 'zyoloyalayuluz',
            'code'        => 'oau',
            'description' => 'yulu yala yolo',
        ]);
        $I->seeResponseCodeIs(201);
        $I->deleteFromDatabase('authority', ['name' => 'zyoloyalayuluz']);
    }

    public function test_create_fail(ApiTester $I)
    {
        $I->sendPOST("/roles", [
            'name'        => 'zyoloyalayuluzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
            'code'        => 'oau',
            'description' => 'yulu yala yolo',
        ]);
        $I->seeResponseCodeIs(400);
        $I->deleteFromDatabase('authority', ['code' => 'oau']);
    }

    public function test_update_ok(ApiTester $I)
    {
        $I->sendPUT("/roles", [
            'name'        => $this->_name1,
            'update_name' => 'zyoloyalayuluz',
            'code'        => 'oau2',
            'description' => 'yulu yala yolo',
        ]);
        $I->seeResponseCodeIs(200);
        $I->deleteFromDatabase('authority', ['name' => 'zyoloyalayuluz']);
    }

    public function test_update_fail(ApiTester $I)
    {
        $I->sendPUT("/roles", [
            'name'        => $this->_name1,
            'update_name' => 'zyoloyalayuluz',
            'code'        => 'oau2zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
            'description' => 'yulu yala yolo',
        ]);
        $I->seeResponseCodeIs(400);
        $I->deleteFromDatabase('authority', ['code' => 'oau2']);
    }

    public function test_assignPermission_ok(ApiTester $I)
    {
        $I->sendPOST("/roles/" . $this->_name1 . "/permissions", [
            'permissions' => $this->_name2 . ',' . $this->_name3 . ',' . $this->_name4
        ]);
        $I->seeResponseCodeIs(200);
    }

    public function test_assignToUser_ok(ApiTester $I)
    {
        $I->sendPOST("/users/" . $this->_user_id2 . "/roles", [
            'user_id' => $this->_user_id,
            'roles'   => $this->_name1
        ]);
        $I->seeResponseCodeIs(200);
        $I->deleteFromDatabase('user_role', ['user_id' => $this->_user_id2]);
        $I->deleteFromDatabase('users', ['user_id' => $this->_user_id2]);
    }

    public function test_assignToUser_fail(ApiTester $I)
    {
        $I->sendPOST("/users/" . $this->_user_id2 . "/roles", [
            'user_id' => "9999999999999999999999",
            'roles'   => $this->_name1
        ]);
        $I->seeResponseCodeIs(400);
        $I->deleteFromDatabase('user_role', ['user_id' => $this->_user_id2]);
        $I->deleteFromDatabase('users', ['user_id' => $this->_user_id2]);
    }

    public function test_delete_ok(ApiTester $I)
    {
        $I->sendPOST("/roles/delete", [
            'roles' => $this->_name1
        ]);
        $I->seeResponseCodeIs(200);
    }

}