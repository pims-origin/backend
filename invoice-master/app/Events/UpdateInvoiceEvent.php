<?php

namespace App\Events;

use App\InvoiceHeader;

class UpdateInvoiceEvent extends Event
{
    public $invoiceHeader;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(InvoiceHeader $invoiceHeader)
    {
        $this->invoiceHeader = $invoiceHeader;
    }
}
