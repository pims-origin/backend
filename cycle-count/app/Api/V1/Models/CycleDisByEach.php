<?php
/**
 * Created by PhpStorm.
 * User: duy
 * Date: 20/10/2016
 * Time: 14:33
 */

namespace App\Api\V1\Models;

class CycleDisByEach extends CycleDisModel
{
    /**
     * @param $cyleDtl
     */
    public function getDiscrepancyCarton($cyleDtl)
    {
        $result = [];

        if (! $cyleDtl) {
            return $result ;
        }

        $acQty = $cyleDtl['act_qty'] !== null ? $cyleDtl['act_qty']:
            $cyleDtl['sys_qty'];

        $discrepancy = $acQty - $cyleDtl['sys_qty'];

        if ($discrepancy > 0) {
            $result = $this->getCartonToCloneByEach($cyleDtl, $discrepancy);

        } else {
            $result = $this->getCartonToRemoveByEach($cyleDtl, $discrepancy);
        }

        return $result;
    }

    /**
     * @param $cyleDtl
     * @param $discrepancy
     */
    public function getCartonToCloneByEach($cyleDtl, $discrepancy)
    {
        $numberAdd = floor($discrepancy / $cyleDtl['pack']);
        $qtyUpdate = $discrepancy % $cyleDtl['pack'];
        if ($cyleDtl['is_new_sku']) {
            $numberAdd = floor($discrepancy / $cyleDtl['remain']);
            $qtyUpdate = $discrepancy % $cyleDtl['remain'];
        }


        $result = $this->getCartonToClone($cyleDtl, $numberAdd);
        if ($qtyUpdate && count($result)) {
            $alignCarton = $result[0];

            $alignCarton['dicpy_sts'] = self::STATUS_DIS_ALIGN;
            $alignCarton['dicpy_qty'] = $qtyUpdate;

            if ($numberAdd) {
                $result[] = $alignCarton;
            } else {
                $result[0] = $alignCarton;
            }
        }

        return $result;
    }

    /**
     * @param $cyleDtl
     * @param $discrepancy
     *
     * @return array
     */
    public function getCartonToRemoveByEach($cyleDtl, $discrepancy)
    {
        $absDiscrepancy = abs($discrepancy);
        $numberDel = ceil($absDiscrepancy / $cyleDtl['pack']);
        if($cyleDtl['sys_qty'] < $cyleDtl['pack']){
            $numberDel = ceil($cyleDtl['sys_qty']/$cyleDtl['remain']);
        }
        $qtyUpdate = $discrepancy % $cyleDtl['pack'];
        
        $result = $this->getCartonToRemove($cyleDtl ,$numberDel);
        
        if ($qtyUpdate) {
            $result[0]['dicpy_qty'] = $qtyUpdate;
            $result[0]['dicpy_sts'] = self::STATUS_DIS_ALIGN;
        }
        
        return $result;
    }
}