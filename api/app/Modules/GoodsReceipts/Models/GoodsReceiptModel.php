<?php

namespace App\Modules\GoodsReceipts\Models;

use App\Modules\GoodsReceipts\Eloquents\GoodsReceipt;

class GoodsReceiptModel extends AbstractModel
{
    const RECEIVED = 'RE';
    protected $grHdr;

    public function __construct(GoodsReceipt $grHdr)
    {
        $this->grHdr = $grHdr;
    }

    public function getListGR($whsId, $cusId, $limit)
    {
        $grs = $this->grHdr->where('deleted', 0)
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId)
                ->where('gr_sts', self::RECEIVED)
                ->orderBy('created_at', 'DESC');

        if ($this->checkCusOfUser($whsId, $cusId))
        {
            return $grs->paginate($limit);
        }
    }

    public function getGRDetail($whsId, $cusId, $grHdrId)
    {
        $gr = $this->grHdr->where('deleted', 0)
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->where('gr_sts', self::RECEIVED)
            ->find($grHdrId);

        if ($this->checkCusOfUser($whsId, $cusId))
        {
            return $gr;
        }
    }
}