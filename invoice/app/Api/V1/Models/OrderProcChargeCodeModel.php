<?php

namespace App\Api\V1\Models;

use \DateTime;
use App\Invoice;

use Illuminate\Support\Facades\DB;

use App\Api\V1\Models\CommonInvoiceModel;
use App\BaseModel;

use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\WorkOrderHdr;

use Seldat\Wms2\Models\InvoiceVolumeRates;
use Seldat\Wms2\Models\ChargeCode;
use Seldat\Wms2\Models\InvoiceCost;
use Seldat\Wms2\Models\InvoiceOrdProcHistory;


class OrderProcChargeCodeModel extends AbstractModel
{
    /**
    * @var
    */
    protected $work;
    protected $charge;
    protected $cusCharge;
    protected $invHistory;
    protected $volRates;
    protected $uom;
    protected $vol;

    /**
    * Order Processing Charge Code Model constructor.
    */

    public function __construct() {
        $this->model = new OrderHdr();
        $this->work = new WorkOrderHdr();

        $this->volRates = new InvoiceVolumeRates();
        $this->charge = new ChargeCode();
        $this->cusCharge = new InvoiceCost();
        $this->invHistory = new InvoiceOrdProcHistory();

        $this->uom = new CommonInvoiceModel();
        $this->vol = new VolumeRatesModel();

    }

    /**
    * Calculate Order Processing charge codes
    *
    * @param array params
    * @param array recNums
    * @param string returnRecs
    * @return array
    */


    function calculate($data, $model)
    {
        $type = Invoice::OP;
        
        $custID = getDefault($data['custID'], NULL);
        $whsID = getDefault($data['whs_id'], NULL);

        //%orderNumbers changed to $orderIDs
        $orderIDs = $data['orderIDs'] = $data['details'] ?
            getDefault($data['items']['Order Processing']) : [];

        if ($data['details'] && array_filter($data['items']) && ! $orderIDs) {
            return [$custID => 0];
        }

        //get open orders if not select items
        $data['orderIDs'] = $orderIDs ? $orderIDs :
                       $this->getOpenOrders($data, $custID);


        // Set volume rates
        $this->vol->volumeRates($whsID, $model, $type);

        $results = $this->charge
                        ->join('charge_type AS ch', 'ch.chg_type_id', '=', 'charge_code.chg_type_id')
                        ->join('system_uom AS s', 's.sys_uom_id', '=', 'charge_code.chg_uom_id')
                        ->where('sys_uom_name', '!=', Invoice::$unit)
                        ->where('chg_type_name', $type)
                        ->select('chg_code', 'chg_code_name', 
                                 'sys_uom_code', 'sys_uom_name')
                        ->groupBy('chg_code')
                        ->get()
                        ->toArray();
//dd($results);
        $uoms = array_column($results, 'sys_uom_name');
//dd($uoms);
        $uniqueUoms = array_unique($uoms);

        $customerUOMs = $model->customerUOMs($type);

        $data['processedOrderIDs'] =
             $this->getBilledOrders($custID, $orderIDs);

        $data['model'] = $model;

        //order status that needs to be invoiced
        $data['status'] = [
            config('constants.odr_status.SHIPPED')
        ];

        $uomValues = $return = [];
        foreach ($uniqueUoms as $uom) {
            $res = $this->getValues($data, $uom, $customerUOMs);
            if ($res) {
                $uomValues[$uom] = $res;
            }
        }

        $rates = $model->rates();

        $custCounts = [];
        $opRates = getDefault($rates[$type], []);


        foreach ($opRates as $custID => $ccs) {
            foreach ($ccs as $cc => $uom) {
                if ($uom == Invoice::$unit) {
                    continue;
                }
                $uomValue = getDefault($uomValues[$uom][$custID]);
                if ($uomValue) {
                    $custCounts[$cc][$custID] = $uomValue;
                }
            }
        }

        $woValues = $this->getLabor($data, $customerUOMs, Invoice::$unit);

        return $woValues ? $custCounts + $woValues : $custCounts;
    }

    /**
    * @param array  custs
    * @param array params
    *
    * @return array
    */

    private function getOpenOrders($params, $custs)
    {
        $eventCode = [
            config('constants.event.ORD-SHIPPED'), 
            config('constants.event.PR-SHIPPED'),
            config('constants.event.ORD-CANCELLED')
        ];
        
        if (! $custs) {
            return FALSE;
        }

        $custsVal = is_array($custs) ? $custs : [$custs];

        $results = $this->model
                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                        ->select('odr_hdr.whs_id', 'odr_hdr.cus_id', 'odr_hdr.odr_id')
                        ->where(function($q) use($params, $custsVal, $eventCode) {

                            $q->where(DB::raw('IF(act_cmpl_dt, FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"),
                                            FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
                                            )
                                        '), '>=', $params['startDate']
                                );
                            $q->where(DB::raw('IF(act_cmpl_dt, FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"),
                                            FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
                                            )
                                        '), '<=', $params['endDate']
                                );

                            $q->whereIn('evt_code', $eventCode);

                            if ($custsVal) {
                                $q->whereIn('odr_hdr.cus_id', $custsVal);
                            }

                            if ($params['whs_id']) {
                                $q->where('odr_hdr.whs_id', $params['whs_id']);
                            }
                        })
                        ->where(function($q) {
                           $q->where('back_odr', '<>', 1);
                           $q->orWhereNull('back_odr');
                        })
                        ->groupBy('odr_hdr.whs_id', 'odr_hdr.cus_id', 'odr_hdr.odr_id')
                        ->lists('odr_id')
                        ->toArray();

        return $results;
    }


    /**
    * @param mixed custID
    * @param array orderNumbers
    *
    * @return array
    */

    function getBilledOrders($custID, $orderIDs=[])
    {
        $cust = is_array($custID) ? $custID : [$custID];

        $results = $this->invHistory
                        ->select('ord_id')
                        ->where(function($q) use($cust, $orderIDs) {
                            if ($orderIDs) {
                                $q->whereIN('ord_id', $orderIDs);
                            }
                            if ($cust) {
                                $q->whereIN('cus_id', $cust);
                            }
                        })
                        ->where('inv_sts', 1)
                        ->lists('ord_id')
                        ->toArray();

        return $results;
    }

    /**
    * @param array data
    * @param string type
    * @param array customerUOMs
    *
    * @return array
    */

    function getValues($data, $type, $customerUOMs)
    {
        $query = null;

        $custs = getDefault($customerUOMs[$type]) ?
                     array_unique($customerUOMs[$type]) : NULL;

        if (! $custs) {
           return FALSE;
        }

        $orderField = 'odr_hdr.odr_id';
        $volField = 'vol';
        $dateField = 'dt';
        $custField = 'odr_hdr.cus_id';
        $whsField = 'odr_hdr.whs_id';

        $status = $data['status'];
        
        $cube = 'ph.width * ph.height * ph.length';

        //common order query
        $uoms = [
            Invoice::$carton,
            Invoice::$piece,
            Invoice::$volume,
            Invoice::$volumeImperial,
            Invoice::$volumeMetric,
            Invoice::$pallet,
            Invoice::$order,
            Invoice::$bol
        ];

        $eventCode = [
            config('constants.event.ORD-SHIPPED'), 
            config('constants.event.PR-SHIPPED')
        ];


        if (in_array($type, $uoms)) {
            $query = $this->model
                        ->join('bol_odr_dtl AS sh', 'sh.odr_id', '=', 'odr_hdr.odr_id')
                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num');
        }

        switch ($type) {
            case Invoice::$carton:
                $query->select('odr_hdr.whs_id', 'odr_hdr.cus_id', DB::raw('SUM(ctn_qty) AS valueCount'));
                break;
            case Invoice::$piece:
                $query->select('odr_hdr.whs_id', 'odr_hdr.cus_id', DB::raw('SUM(piece_qty) AS valueCount'));
                break;
            case Invoice::$volume:
            case Invoice::$volumeImperial:
                $cubeQty = DB::raw('SUM(cube_qty_ttl) AS valueCount');
                $query->select('odr_hdr.whs_id', 'odr_hdr.cus_id', $cubeQty);
                break;
            case Invoice::$volumeMetric:
                $cubeQty = DB::raw('ROUND(SUM(cube_qty_ttl) * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8) AS valueCount');
                $query->select('odr_hdr.whs_id', 'odr_hdr.cus_id', $cubeQty);
                break;
            case Invoice::$pallet:
                $query->select('odr_hdr.whs_id', 'odr_hdr.cus_id', DB::raw('SUM(plt_qty) AS valueCount'));
                break;
            case Invoice::$order:
                $query->select('odr_hdr.whs_id', 'odr_hdr.cus_id', DB::raw('COUNT(DISTINCT(odr_hdr.odr_num)) AS valueCount'));
                break;
            case Invoice::$bol:
                $query->select('odr_hdr.whs_id', 'odr_hdr.cus_id', DB::raw('COUNT(DISTINCT(odr_hdr.ship_id)) AS valueCount'));
                break;
            case Invoice::$label:
                $query = $this->model
                            ->join('odr_dtl AS od', 'od.odr_id', '=', 'odr_hdr.odr_id')
                            ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                            ->select('odr_hdr.cus_id', 'odr_hdr.whs_id',  DB::raw('COUNT(DISTINCT(ship_track_id)) AS valueCount'));
                break;
            case Invoice::$fedex:
                $query = $this->model
                                ->join('shipment AS sh', 'sh.ship_id', '=', 'odr_hdr.ship_id')
                                ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                                ->select('odr_hdr.cus_id', 'odr_hdr.whs_id',  DB::raw('COUNT(odr_hdr.odr_num) AS valueCount'));
                $carrier = 'FEDEX';
                break;
            case Invoice::$ups:
                $query = $this->model
                                ->join('shipment AS sh', 'sh.ship_id', '=', 'odr_hdr.ship_id')
                                ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                                ->select('odr_hdr.cus_id', 'odr_hdr.whs_id',  DB::raw('COUNT(odr_hdr.odr_num) AS valueCount'));
                  $carrier = 'UPS';
                break;
            case Invoice::$orderCancel:
                $query =  $this->model
                                ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                                ->select('odr_hdr.cus_id', 'odr_hdr.whs_id', DB::raw('COUNT(odr_hdr.odr_num) AS valueCount'));

                $status = [config('constants.odr_status.CANCELLED')];
                
                $eventCode = [config('constants.event.ORD-CANCELLED')];
                break;
            case Invoice::$smallCarton:
            case Invoice::$mediumCarton:
            case Invoice::$largeCarton:
            case Invoice::$smallCartonImperial:
            case Invoice::$mediumCartonImperial:
            case Invoice::$largeCartonImperial:

                $volField = DB::raw('ROUND(' . $cube . ' / 1728, 8)');
                $queryRes = $this->ordRangeQuery($type);
                
                $custField = $queryRes['custField'];
                $query = $queryRes['query'];
                $orderField = $queryRes['orderField'];
                $whsField = $queryRes['whsField'];
                $dateField = $queryRes['dateField'];
                break;
            case Invoice::$smallCartonMetric:
            case Invoice::$mediumCartonMetric:
            case Invoice::$largeCartonMetric:
                
                $volField = DB::raw('ROUND(' . $cube . ' * ' . Invoice::CUBIC_INCH_TO_CUBIC_METER . ', 8)');
                $queryRes = $this->ordRangeQuery($type);
                
                $custField = $queryRes['custField'];
                $query = $queryRes['query'];
                $orderField = $queryRes['orderField'];
                $whsField = $queryRes['whsField'];
                $dateField = $queryRes['dateField'];
                break;
            default:
                return [];
        }

        //status
        $ordQuery = $status ? $query->whereIn('odr_sts', $status) : $query;

        //carrier
        $carrierTypes = [
            Invoice::$ups,
            Invoice::$fedex
        ];

        if (in_array($type, $carrierTypes)) {
            $ordQuery->where(DB::raw('UPPER(sh.carrier)'), $carrier);
        }
        
        $cusQuery = $custs ? $ordQuery->whereIn($custField, $custs) : $ordQuery;


        //$data['orderNumbers']
        $selectQuery = $data['orderIDs']
                            ? $cusQuery->whereIn($orderField, $data['orderIDs']) : $cusQuery;

       //$data['processedOrderIDs']
        $hisQuery = $data['processedOrderIDs']
                            ? $selectQuery->whereNotIn($orderField, $data['processedOrderIDs']) : $selectQuery;

        //monthly carton
        $volParams = [
            'passed' => [
                'uom' => $type,
                'custs' => $custs,
                'model' => $data['model']
            ],
            'volField' => $volField,
            'type' => Invoice::OP
        ];

        $volQuery = $this->vol->volClause($hisQuery, $volParams, $custField);

        $finalQuery = $volQuery ? $volQuery : $hisQuery;

        //results
        $finalResults = $finalQuery
                            ->where(function($q) use( $data, $whsField, $eventCode ) {
                                $q->where(DB::raw('IF(act_cmpl_dt, FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"),
                                                FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
                                                )
                                            '), '>=', $data['startDate']
                                        );
                                $q->where(DB::raw('IF(act_cmpl_dt, FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"),
                                                FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
                                                )
                                            '), '<=', $data['endDate']
                                        );

                                $q->whereIn('evt_code', $eventCode);

                                if ($data['whs_id']) {
                                    $q->where($whsField, $data['whs_id']);
                                }
                            })
                            ->where(function($q) {
                                $q->where('odr_hdr.back_odr', '<>', 1);
                                $q->orWhereNull('odr_hdr.back_odr');
                            })
                            ->groupBy($custField, $whsField)
                            ->get()
                            ->toArray();

        $vendorIDs = array_column($finalResults, 'cus_id');
        $cartonCounts = array_column($finalResults, 'valueCount');

        return array_combine($vendorIDs, $cartonCounts);
    }


    /**
    * @param array data
    * @param array customerUOMs
    *
    * @return array
    */

    function getLabor($data, $customerUOMs)
    {
        $eventCode = [
            config('constants.event.ORD-SHIPPED'),
            config('constants.event.PR-SHIPPED')
        ];
        
        $custs = getDefault($customerUOMs['UNIT']);
        if (! $custs) {
            return FALSE;
        }

        $custID = array_unique($custs);

        $results = $this->model
                        ->join('wo_hdr AS wh', 'wh.odr_hdr_id', '=', 'odr_hdr.odr_id')
                        ->join('wo_dtl AS wd', 'wd.wo_hdr_id', '=', 'wh.wo_hdr_id')
                        ->join('charge_code AS ch', 'ch.chg_code', '=', 'wd.chg_code')
                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                        ->select('odr_hdr.cus_id',
                                'odr_hdr.whs_id',
                                'ch.chg_code_name',
                                 DB::raw('SUM(qty) AS labor')
                                )
                        ->where(function($q) use( $data, $custID, $eventCode ) {
                                $q->where(DB::raw('IF(act_cmpl_dt, FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"),
                                                      FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
                                                      )
                                                 '), '>=', $data['startDate']
                                          );
                                $q->where(DB::raw('IF(act_cmpl_dt, FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"),
                                                      FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
                                                      )
                                                 '), '<=', $data['endDate']
                                        );

                                $q->whereIn('evt_code', $eventCode);

                                $q->whereNotNull('odr_hdr.ship_id');

                                $q->whereIn('odr_id', $data['orderIDs']);

                                $q->whereNotIn('odr_id', $data['processedOrderIDs']);

                                if ($data['whs_id']) {
                                    $q->where('odr_hdr.whs_id', $data['whs_id']);
                                }

                                if ($custID) {
                                    $q->where('odr_hdr.cus_id', $custID);
                                }

                            })
                        ->where(function($q) {
                                $q->where('back_odr', '<>', 1);
                                $q->orWhereNull('back_odr');
                        })
                        ->groupBy('odr_hdr.cus_id', 'odr_hdr.whs_id',
                                  'wd.chg_code_id')
                        ->get()
                        ->toArray();

        $return = [];
        foreach ($results as $values) {

            $chargeCodes = $values['chg_code_name'];
            $vendorID = $values['cus_id'];

            $return[$chargeCodes][$vendorID] = $values['labor'];
        }

        return $return;
    }

    /**
    * @params array
    * @processed array
    *
    * @return array
    */

    function processingQuery($params, $processed)
    {
        $startDate = getDefault($params['startDate']);
        $endDate = getDefault($params['endDate']);
        $orderIDs = getDefault($params['odr_id']);
        $cusID = getDefault($params['cusID']);
        $whsID = getDefault($params['whsID']);
        
        $eventCode = [
            config('constants.event.ORD-SHIPPED'),
            config('constants.event.PR-SHIPPED')
        ];

        $dateClause = '
            IF(act_cmpl_dt,
               FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"),
               FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
            )
        ';

        switch ($processed) {
            case 'shipped':
                $query = $this->model
                    ->join('bol_odr_dtl AS sh', 'sh.odr_id', '=', 'odr_hdr.odr_id')
                    ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                    ->join('customer AS c', 'c.cus_id', '=', 'odr_hdr.cus_id')
                    ->select(
                        'odr_hdr.odr_id AS id',
                        'odr_hdr.odr_num',
                        'cus_name AS clientName',
                        DB::raw('CONCAT("", "Open", "") AS type'),
                        'odr_hdr.ship_to_name AS name',
                        'cus_odr_num',
                        'odr_hdr.cus_po',
                        DB::raw($dateClause . ' AS dt'),
                        DB::raw('SUM(ctn_qty) AS cartonQuantity'),
                        DB::raw('SUM(piece_qty) AS pieceQuantity'),
                        DB::raw('SUM(plt_qty) AS palletQuantity')
                    )
                    ->where(function($q) use( $params, $eventCode ) {

                        $q->whereIn('evt_code', $eventCode);

                        $q->whereNotNull('odr_hdr.ship_id');

                        $q->where('odr_sts', config('constants.odr_status.SHIPPED'));

                        $params['processedOrderIDs'] ? $q->whereNotIn('odr_hdr.odr_id', $params['processedOrderIDs'])
                            : NULL;

                    });

                break;

            case 'cancelled':

                $query = $this->model
                    ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                    ->join('customer AS c', 'c.cus_id', '=', 'odr_hdr.cus_id')
                    ->select(
                            'odr_hdr.odr_id AS id',
                            'odr_hdr.odr_num',
                            'cus_name',
                            DB::raw('CONCAT("", "Cancel", "") AS type'),
                            'odr_hdr.ship_to_name AS name',
                            'cus_odr_num',
                            'cus_po',
                            DB::raw('IF(
                                        act_cancel_dt,
                                        FROM_UNIXTIME(act_cancel_dt, "%Y-%m-%d"),
                                        FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
                                    ) AS dt'),
                            DB::raw('CONCAT("", "NA", "") AS cartonQuantity'),
                            DB::raw('CONCAT("", "NA", "") AS pieceQuantity'),
                            DB::raw('CONCAT("", "NA", "") AS palletQuantity')
                    )
                    ->where(function($q) use($params) {

                        $q->where('evt_code', config('constants.event.ORD-CANCELLED'));

                        $q->where('odr_sts', config('constants.odr_status.CANCELLED'));

                    });

                break;

            default:

                break;
        }

        $startDate ? $query->where(DB::raw($dateClause), '>=', $startDate) : NULL;
        $endDate ? $query->where(DB::raw($dateClause), '<=', $endDate) : NULL;
        $orderIDs ? $query->whereIn('odr_hdr.odr_id', $orderIDs) : NULL;
        $cusID ? $query->where('odr_hdr.cus_id', $cusID) : NULL;
        $whsID ? $query->where('odr_hdr.whs_id', $whsID) : NULL;

        $query->where(function($query) {
                $query->where('back_odr', '<>', 1);
                $query->orWhereNull('back_odr');
            })
            ->groupBy('odr_hdr.cus_id', 'odr_hdr.whs_id', 'odr_hdr.odr_id');

        return $query;
    }

    /**
     * @param array passed
     * @return array
    */

    public function ordRangeQuery($type)
    {
        $custField = 'ph.cus_id';
        $query = $this->model
                    ->join('pack_hdr AS ph', 'ph.odr_hdr_id', '=', 'odr_hdr.odr_id')
                    ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
                    ->join('inv_vol_rates AS iv', 'iv.cus_id', '=', 'ph.cus_id')
                    ->join('charge_type AS ct', 'ct.chg_type_id', '=', 'iv.chg_type_id')
                    ->join('system_uom AS su', 'su.sys_uom_id', '=', 'iv.chg_uom_id')
                    ->where('su.sys_uom_name', $type)
                    ->where('ct.chg_type_name', Invoice::OP)
                    ->select('ph.cus_id', 'ph.whs_id', 
                              DB::raw('COUNT(DISTINCT pack_hdr_id) AS valueCount')
                     );

        $orderField = 'ph.odr_hdr_id';
        $whsField = 'ph.whs_id';
        $dateField = DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")');
        
        return [
            'custField' => $custField,
            'query' => $query,
            'orderField' => $orderField,
            'whsField' => $whsField,
            'dateField' => $dateField
        ];
    }

}
