<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class InventorySummaryModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
    }

    public function searchAutoComplete($input, $limit = null)
    {
        $currentWH = Data::getCurrentWhsId();

        $attributes = SelArr::removeNullOrEmptyString($input);
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $query = $this->model
            ->join('item', 'item.item_id', '=', 'invt_smr.item_id');

        $query->where('invt_smr.whs_id', $currentWH);

        $arrLike = ['sku', 'cus_upc'];
        $arrEqual = ['item_id', 'cus_id'];

        if(!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if(in_array($key, $arrLike)) {
                    $key = ($key == 'cus_upc') ? 'upc' : $key;
                    $query->where('invt_smr.'.$key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if(in_array($key, $arrEqual)) {
                    $query->where('invt_smr.'.$key, $value);
                }
            }
        }

        $query->groupBy('invt_smr.item_id');

        return $query->limit($limit)->get();

    }
}
