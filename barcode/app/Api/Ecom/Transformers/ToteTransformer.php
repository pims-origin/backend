<?php

namespace App\Api\Ecom\Transformers;

use League\Fractal\TransformerAbstract;

class ToteTransformer extends TransformerAbstract
{
    public function transform($tote)
    {
        return [
            'tote_id'   => $tote->tote_id,
            'tote_num'  => $tote->tote_num,
            'tote_sts'  => $tote->tote_sts
        ];
    }
}
