<?php

namespace Seldat\Wms2\Models;

class ReportPallet extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rpt_pallet';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'plr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'loc_id',
        'whs_id',
        'plt_num',
        'ctn_ttl',
        'plt_block',
        'plt_tier',
        'rfid',
        'plt_sts',
        'loc_name',
        'loc_code',
        'created_by',
        'updated_by',
        'zero_date',
        'gr_hdr_id',
        'gr_dtl_id',
        'storage_duration',
        'return_id',
        'is_movement',
        'dmg_ttl',
        'data',
        'init_ctn_ttl',
        'gr_dt',
        'init_piece_ttl',
        'pack',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'is_xdock',
        'weight',
        "odr_id",
    ];
}
