<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Dingo\Api\Exception\UnknownVersionException;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class PalletModel extends AbstractModel
{

    protected $eventTrackingModel;
    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null, EventTrackingModel $eventTrackingModel = null)
    {
        $this->model = ($model) ?: new Pallet();
        $this->eventTrackingModel = $eventTrackingModel;
    }

    public function checkPalletByPalletNum($pltNum, $whsId)
    {
        return $this->model->where('rfid', $pltNum)
            ->where('ctn_ttl', '>', 0)
            ->where('plt_sts', 'NW')
            ->where('whs_id', $whsId)
            ->count('plt_id');
    }

    public function getPalletByPalletNum($pltNum, $whsId)
    {
        return $this->model->where('rfid', $pltNum)
            ->join('gr_dtl', 'pallet.gr_dtl_id', '=', 'gr_dtl.gr_dtl_id')
            ->where('whs_id', $whsId)
            ->where('plt_sts', 'NW')
            ->first();
    }

    public function getHdrPutAwayByHdrDtlID($hdrDtlId, $with = [])
    {
        $sql1 = "select pal_sug_loc.gr_hdr_num, pal_sug_loc.sku, pal_sug_loc.size, pal_sug_loc.color, count(*) as total,( " .
            " select count(*) as actual from pallet where pallet.plt_id = pal_sug_loc.plt_id " .
            " AND pallet.loc_id IS NOT NULL ) as actual";
        $query = $this->make($with)
            ->select([
                DB::raw($sql1),
                "pal_sug_loc.gr_hdr_num",
                "pal_sug_loc.sku",
                "pal_sug_loc.size",
                "pal_sug_loc.color",
            ])
            ->where('pal_sug_loc.gr_dtl_id', $hdrDtlId)
            ->get();

        return $query;
    }

    /**
     * @param $pltInfo
     *
     * @return mixed
     */
    public function updatePallet($pltInfo)
    {
        $pltId = array_get($pltInfo, 'plt_id', null);
        $created_at = array_get($pltInfo, 'created_at', 0);
        $zeroDt = time();

        // Calculate storage_duration
        $date1 = date("Y-m-d",
            is_int($created_at) || is_string($created_at) ? (int)$created_at : $created_at->timestamp);
        $date2 = date("Y-m-d");

        $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
        if ($dateDiff == 0) {
            $storageDuration = 1;
        } else {
            $storageDuration = $dateDiff;
        }

        return $this->model
            ->where('plt_id', $pltId)
            ->update([
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'ctn_ttl'          => 0,
                'zero_date'        => $zeroDt,
                'storage_duration' => $storageDuration
            ]);
    }

    /**
     * @return mixed
     */
    public function getAllPalletHasNoCarton($pltIds)
    {
        return $this->model
            ->select('pallet.plt_id', 'pallet.created_at')
            ->whereIn('pallet.plt_id', $pltIds)
            ->whereRaw('(pallet.loc_id is not null or pallet.ctn_ttl > 0)')
            ->whereRaw('(Select count(cartons.ctn_id) from cartons where cartons.plt_id = pallet.plt_id) = 0')
            ->get();
    }

    /**
     * @param $locIds
     *
     * @return mixed
     */
    public function updateCtnTtl($locIds)
    {
        return $this->model->whereIn('pallet.loc_id', $locIds)
            ->update([
                'ctn_ttl' => DB::raw('(Select count(cartons.ctn_id1) from cartons where cartons.plt_id = pallet
                .plt_id)')
            ]);
    }

    public function updatePalletCtnTtl($locIds)
    {
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            $this->model
                ->whereIn('loc_id', $chunkLocIds)
                ->update([
                    'ctn_ttl' => DB::raw("(
                    SELECT COUNT(c.ctn_id) FROM cartons c
                    WHERE pallet.plt_id = c.plt_id
                        AND c.ctn_sts IN ('AC', 'LK', 'RG')
                        AND c.deleted = 0
                )")
                ]);
        }
    }


    public function updatePalletCtnTtlByPallet($pltIds)
    {
        return $this->model
            ->whereIn('plt_id', $pltIds)
            ->update([
                'ctn_ttl' => DB::raw("(SELECT COUNT(c.ctn_id) FROM cartons c WHERE pallet.plt_id = c.plt_id)")
            ]);
    }

    /**
     * Update pallet when no cartons
     *
     * @param array $locIds
     *
     * @return boolean
     */
    public function updateZeroPallet($locIds)
    {
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            $this->model
                ->whereIn('loc_id', $chunkLocIds)
                ->where('ctn_ttl', 0)
                ->update([
                    'loc_id' => null,
                    'loc_code' => null,
                    'loc_name' => null,
                    'rfid' => null,
                    'zero_date' => time(),
                    'storage_duration' => CartonModel::getCalculateStorageDurationRaw(),
                    'plt_sts' => 'PD'
                ]);
        }
    }

    public function updateZeroPalletByPallet($pltIds)
    {
        return $this->model
            ->whereIn('plt_id', $pltIds)
            ->where('ctn_ttl', 0)
            ->update([
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'rfid'             => null,
                'zero_date'        => time(),
                'storage_duration' => CartonModel::getCalculateStorageDurationRaw(),
                'plt_sts'          => 'PD'
            ]);
    }

    public function removeLocDynZone($locId)
    {
        return DB::table('location')
            ->join('zone', 'location.loc_zone_id', '=', 'zone.zone_id')
            ->leftJoin('pallet', 'pallet.loc_id', '=', 'location.loc_id')
            ->where([
                "location.loc_id" => $locId,
                "pallet.plt_id"   => null,
                "zone.dynamic"    => 1
            ])
            ->update(['location.loc_zone_id' => null]);
    }

    public function generatePltNum($prefix = "V") {
        $currentYearMonth = date('ym');
        $pallet = Pallet::where('plt_num', 'LIKE', '%-' . $currentYearMonth . '-%')
            ->orderBy('plt_id', 'DESC')
            ->first();

        if (!$pallet){
            return $prefix . '-' . $currentYearMonth . '-' . '000001';
        }

        $aNum = explode("-", $pallet['plt_num']);
        $aTemp = array_keys($aNum);
        $end = end($aTemp);
        $index = (int)$aNum[$end];
        $aNum[0] = $prefix;
        $aNum[$end] = str_pad(++$index, 6, "0", STR_PAD_LEFT);
        return implode("-", $aNum);
    }

    public function validateLPNFormat($plt_num){
        if((bool)preg_match("/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }
//        if((bool)preg_match("/^T-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
//            return true;
//        }
//
//        if((bool)preg_match("/^V-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
//            return true;
//        }
        return false;
    }

    public function validatePLTFormat($plt_num){
        return (bool)preg_match("/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num);
    }

    public function validateToTeFormat($plt_num){
        return (bool)preg_match("/^T-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num);
    }

    public function validateVirtualFormat($plt_num){
        return (bool)preg_match("/^V-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num);
    }

    public function checkPltSameCustomer($whsId, $cusId, $pltId)
    {
        return $this->model
            ->where('plt_id', $pltId)
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->first();
    }
    public function checkPalletByPltId($whsId, $pltId)
    {
        return $this->model
            ->where('plt_id', $pltId)
            ->where('whs_id', $whsId)
            ->where('ctn_ttl', '>', 0)
            ->where('deleted', '=', 0)
            ->where('plt_sts', 'AC')
            ->first();
    }

    public function generateEventOwner($type)
    {

        $currentYearMonth = date('ym');

        $lastEvent = $this->eventTrackingModel->getFirstWhere(
            [['owner', 'like', '%' . $type . '%']],
            [],
            ['id' => 'desc']
        );
        $next = 1;
        if ($lastEvent) {
            list(, $yymm, $number) = explode('-', $lastEvent->owner);
            $next = ($yymm != $currentYearMonth) ? $next : intval($number) + 1;
        }

        $result = sprintf('%s-%s-%s',
            $type,
            $currentYearMonth,
            str_pad($next, 6, '0', STR_PAD_LEFT));

        return $result;
    }
    public function updatePalletZeroDateAndDurationDaysAuto()
    {
        $this->model
            ->whereNull('pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)"), 0)
            ->update([
                'pallet.zero_date'        => DB::raw('pallet.updated_at'),
                'pallet.ctn_ttl'          => 0,
                'pallet.loc_id'           => null,
                'pallet.loc_code'         => null,
                'pallet.loc_name'         => null,
                'pallet.storage_duration' =>
                    DB::raw("(IF(DATEDIFF(FROM_UNIXTIME(pallet.updated_at), FROM_UNIXTIME(pallet.created_at)),
					DATEDIFF(FROM_UNIXTIME(pallet.updated_at),FROM_UNIXTIME(pallet.created_at)), 1))")
            ]);

        $this->model
            ->whereNull('pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)"), '>', 0)
            ->update([
                'pallet.ctn_ttl' =>
                    DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)")
            ]);
        DB::table('rpt_pallet')
            ->whereNull('rpt_pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = rpt_pallet.plt_id)"), 0)
            ->update([
                'rpt_pallet.zero_date'        => DB::raw('rpt_pallet.updated_at'),
                'rpt_pallet.ctn_ttl'          => 0,
                'rpt_pallet.loc_id'           => null,
                'rpt_pallet.loc_code'         => null,
                'rpt_pallet.loc_name'         => null,
                'rpt_pallet.storage_duration' =>
                    DB::raw("(IF(DATEDIFF(FROM_UNIXTIME(rpt_pallet.updated_at), FROM_UNIXTIME(rpt_pallet.created_at)),
					DATEDIFF(FROM_UNIXTIME(rpt_pallet.updated_at),FROM_UNIXTIME(rpt_pallet.created_at)), 1))")
            ]);

        DB::table('rpt_pallet')
            ->whereNull('rpt_pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = rpt_pallet.plt_id)"), '>', 0)
            ->update([
                'rpt_pallet.ctn_ttl' =>
                    DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = rpt_pallet.plt_id)")
            ]);

    }

    public function removeWhereInLocation(array $param)
    {
        return $this->model->whereIn('loc_id', $param)->update([
            'loc_id'           => null,
            'loc_name'         => null,
            'loc_code'         => null,
            'zero_date'        => time(),
            'storage_duration' => DB::raw('IF(DATEDIFF(FROM_UNIXTIME(updated_at), FROM_UNIXTIME(created_at)),
                        DATEDIFF(FROM_UNIXTIME(updated_at), FROM_UNIXTIME(created_at)), 1)'),
            'rfid'             => null,
            'deleted'          => 1,
            'deleted_at'       => time(),
        ]);
    }
}
