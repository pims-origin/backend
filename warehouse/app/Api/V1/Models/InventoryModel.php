<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\Inventories;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\Log;

class InventoryModel extends AbstractModel
{
    /**
     * InventoryModel constructor.
     *
     * @param Inventories|null $model
     */
    public function __construct(Inventories $model = null)
    {
        $this->model = ($model) ?: new Inventories();
    }

    public function getItemInProcess($params=[]){
        $statusQuery=(new GoodsReceiptDetail())->getModel()
        ->select(DB::raw('CONCAT(gr_dtl.item_id,".",gr_hdr.cus_id,".",gr_hdr.whs_id) AS `key`'))
        ->join('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'gr_dtl.gr_hdr_id')
        ->whereIn('gr_hdr.gr_sts',['CC','RG'])
        ->groupBy('gr_dtl.item_id','gr_hdr.cus_id','gr_hdr.whs_id');
        if (!empty($params['cus_id'])) {
            $statusQuery->where('gr_hdr.cus_id', $params['cus_id']);
        }
        if (!empty($params['sku'])) {
            $statusQuery->where('gr_dtl.sku', 'like', "%" . SelStr::escapeLike($params['sku']) . "%");
        }
        if (!empty($params['customer_ids'])) {
            $statusQuery->whereIn('gr_hdr.cus_id', $params['customer_ids']);
        }
        if (!empty($params['warehouse_ids'])) {
            $statusQuery->whereIn('gr_hdr.whs_id', $params['warehouse_ids']);
        }
        $statusData=$statusQuery->pluck('key');
        return $statusData;
    }

    /**
     * @param array $params
     * @param array $with
     * @param int $limit
     * @param bool|false $export
     *
     * @return mixed
     */
    public function inventoryReports($params = [], $with = [], $limit = 2000, $export = false)
    {
        $params = SelArr::removeNullOrEmptyString($params);
        $query = (new Inventories())->getModel()
        ->join('customer', 'customer.cus_id', '=', 'inventory.cus_id')
        ->join('item', 'item.item_id', '=', 'inventory.item_id')
        ->select([
            'inventory.item_id',
            'inventory.sku',
            'inventory.size',
            'inventory.color',
            //'inventory.lot',
            'customer.cus_id',
            'customer.cus_name',
            'customer.cus_code',
            'inventory.in_hand_qty as avail',
            //'inventory.allocated_qty',
            'inventory.in_pick_qty as picked_qty',
            //'inventory.lock_qty',
            //'inventory.dmg_qty',
            //'inventory.crs_doc_qty',
            'inventory.total_qty as ttl',
            'inventory.upc',
            'item.pack',
            'item.description',
            'inventory.whs_id',
            DB::raw('round(inventory.total_qty/item.pack, 2) AS ctns'),
        ]);

        if (isset($params['cus_id'])) {
            $query->where('inventory.cus_id', intval($params['cus_id']));
        }

        if (isset($params['sku'])) {
            $query->where('inventory.sku', 'like', "%" . SelStr::escapeLike($params['sku']) . "%");
        }
        //if (isset($params['lot'])) {
        //    $lot = trim($params['lot']);
        //    if (!empty($lot)) {
        //        $query->where('inventory.lot', $lot);
        //    }
        //}

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cusIds = $predis->getCustomersByWhs($currentWH);
        if (!empty($params['cus_id'])) {
            $cusIds=[$params['cus_id']];
        }
        $query->whereIn('inventory.cus_id', $cusIds);
        $query->where('inventory.whs_id', $currentWH);
        $query->where('inventory.total_qty', '!=', 0);
        $query->orderBy('inventory.updated_at', 'desc');

        //$query->groupBy('inventory.item_id', 'inventory.lot');
        $query->groupBy('inventory.item_id');
        $this->sortBuilder($query, $params);

        $statusData=$this->getItemInProcess([
            'sku'=>$params['sku']??null,
            'warehouse_ids'=>[$currentWH],
            'customer_ids'=>$cusIds,
        ]);
        if ($export) {
            return $query->get();
        } else {
            return ['data'=>$query->paginate($limit),'statusData'=>$statusData];
        }
    }

    public function inventoryReportsV2($params = [], $with = [], $limit = 2000, $export = false)
    {
        $params = SelArr::removeNullOrEmptyString($params);
        $query = DB::table("invt_smr AS inv")
            ->join('customer', 'customer.cus_id', '=', 'inv.cus_id')
            ->join('item', 'item.item_id', '=', 'inv.item_id')
            ->select([
                'inv.item_id',
                'inv.sku',
                'inv.size',
                'inv.color',
                //'inv.lot',
                'customer.cus_id',
                'customer.cus_name',
                'customer.cus_code',
                'inv.avail',
                //'inv.allocated_qty',
                'inv.picked_qty',
                //'inv.lock_qty',
                //'inv.dmg_qty',
                //'inv.crs_doc_qty',
                'inv.ttl',
                'inv.upc',
                'item.pack',
                'item.description',
                'inv.whs_id',
                DB::raw('round(inv.ttl/item.pack, 2) AS ctns'),
            ]);

        if (isset($params['cus_id'])) {
            $query->where('inv.cus_id', intval($params['cus_id']));
        }

        if (isset($params['sku'])) {
            $query->where('inv.sku', 'like', "%" . SelStr::escapeLike($params['sku']) . "%");
        }
        //if (isset($params['lot'])) {
        //    $lot = trim($params['lot']);
        //    if (!empty($lot)) {
        //        $query->where('inv.lot', $lot);
        //    }
        //}

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cusIds = $predis->getCustomersByWhs($currentWH);
        if (!empty($params['cus_id'])) {
            $cusIds=[$params['cus_id']];
        }
        $query->whereIn('inv.cus_id', $cusIds);
        $query->where('inv.whs_id', $currentWH);
        $query->where('inv.ttl', '!=', 0);
        $query->orderBy('inv.updated_at', 'desc');

        //$query->groupBy('inv.item_id', 'inv.lot');
        $query->groupBy('inv.item_id');
        $this->sortBuilder($query, $params);

        $statusData=$this->getItemInProcess([
            'sku'=>$params['sku']??null,
            'warehouse_ids'=>[$currentWH],
            'customer_ids'=>$cusIds,
        ]);
        if ($export) {
            return $query->get();
        } else {
            return ['data'=>$query->paginate($limit),'statusData'=>$statusData];
        }
    }


}
