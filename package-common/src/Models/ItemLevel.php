<?php
/**
 * Created by PhpStorm.
 * User: duong.tran
 * Date: 12/26/2017
 * Time: 3:18 PM
 */
namespace Seldat\Wms2\Models;

use Illuminate\Database\Eloquent\Model;

class ItemLevel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'item_level';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'level_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'pkg_code',
        'pkg_name'
    ];

    public function itemHier()
    {
        return $this->hasMany(__NAMESPACE__ . '\ItemHier', 'level_id', 'level_id');
    }
}
