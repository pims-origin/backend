<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => ['log'],
    'modules'             => [],
    'components'          => [
        'httpclient'   => [
            'class'          => 'understeam\httpclient\Client',
            'detectMimeType' => false,
            'requestOptions' => [
                'exceptions' => false,
                'verify'     => false
            ],
            'requestHeaders' => [
                "Accept" => "*/*"
                // specify global request headers (can be overrided with $options on making request)
            ],
        ],
        'user'         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => false,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'   => [
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => false,
            'showScriptName'      => false,
            'cache'               => \yii\redis\Cache::className(),
            'rules'               => [
                'GET customer-csr/<cusIds>/<whsId>'    => 'user/get-customer-csr',
                'GET csr-warehouse/<whsId>'    => 'user/get-csr-by-warehouse',
                'GET customer-picker/<cusIds>/<whsId>' => 'user/get-customer-picker',
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => 'user',
                    'extraPatterns' => [
                        'POST signup'                         => 'signup',
                        'POST login'                          => 'login',
                        'PUT update/<id:\d+>'                 => 'update',
                        'PUT update-profile/<id:\d+>'         => 'update-profile',
                        'GET info/<id:\d*>'                   => 'info',
                        'POST,GET token'                      => 'token',
                        'POST logout'                         => 'logout',
                        'POST change-password'                => 'change-password',
                        'POST update-password'                => 'update-password',
                        'POST setup-password'                 => 'setup-password',
                        'POST get-user-by-ids'                => 'get-user-by-ids',
                        'DELETE multiple'                     => 'delete-multiple',
                        'GET user-cache'                      => 'user-cache',
                        'DELETE clear-user-cache'             => 'clear-user-cache',
                        'GET get-office-loc'                  => 'get-office-loc',
                        'GET get-user-dept'                   => 'get-user-dept',
                        'GET export/<fileType:\w*>'           => 'export',
                        'GET get-csr/<cusIds>/<whsId>'        => 'get-csr',
                        'GET get-picker/<whsId:\d+>'          => 'get-picker',
                        'GET get-putter/<cusIds>/<whsId>'     => 'get-putter',
                        'GET get-put-backer/<cusIds>/<whsId>' => 'get-put-backer',
                    ],
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => 'user-meta',
                    'extraPatterns' => [
                        'GET <qualifier:\w+>' => 'get',
                        'PUT <qualifier:\w+>' => 'set'
                    ],
                ],
            ],
        ],

        'request' => [
			'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],

        'response' => [
            'class'  => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON
        ]
    ],
    'params'              => $params,
];
