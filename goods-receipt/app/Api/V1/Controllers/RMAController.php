<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Transformers\AsnTransformer;
use App\Api\V1\Transformers\GoodsReceiptCartonTransformer;
use App\Api\V1\Transformers\RMAOrderTransformer;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\removeNullOrEmptyString;
use Wms2\UserInfo\Data;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class RMAController extends AbstractController
{
    const ORDER_STATUS_SHIPPED = 'SH';
    const ORDER_STATUS_RMA = 'RMA';

    public function __construct(
        AsnHdrModel $asnHdrModel,
        EventTrackingModel $eventTrackingModel
    ){
        $this->asnHdrModel = $asnHdrModel;
        $this->eventTrackingModel = $eventTrackingModel;
    }

    public function createASN(Request $request)
    {
        \DB::setFetchMode(\PDO::FETCH_OBJ);
        $input = $request->getParsedBody();

        $odrNum = array_get($input, 'odr_num', null);
        if (!$odrNum){
            return $this->response->errorBadRequest('Miss Order Number');
        }

        $checkExistAsn = AsnHdr::where('asn_type', 'RMA')
                                ->where('asn_hdr_ref', $odrNum)
                                ->count();
        if($checkExistAsn){
            return $this->response->errorBadRequest('Order already created RMA');
        }

        $order = OrderHdr::where('odr_num', $odrNum)->first();
        if (!$order){
            return $this->response->errorBadRequest('Order Number Is Missed In Database');
        }
        if (!in_array($order->odr_sts, [self::ORDER_STATUS_SHIPPED, self::ORDER_STATUS_RMA])){
            return $this->response->errorBadRequest('Order Status Must Be Shipped or Canceled RMA');
        }

        try{
            DB::beginTransaction();

            $order->load('details.systemUom', 'details.item', 'waveHdr', 'shipment', 'shippingOrder');
            $asnNumAndSeq = $this->asnHdrModel->generateAsnNumAndSeq('RMA');
            $whsId = Data::getCurrentWhsId();
            $userId = Data::getCurrentUserId();

            $dataAsnHdr = [
                'asn_hdr_seq'       => $asnNumAndSeq['asn_seq'],
                'asn_hdr_ept_dt'    => strtotime(date('Y-m-d', time())),
                'asn_hdr_num'       => $asnNumAndSeq['asn_num'],
                'asn_hdr_ref'       => $odrNum,
                'asn_type'          => 'RMA',
                'cus_id'            => $order->cus_id,
                'whs_id'            => $order->whs_id,
                'asn_sts'           => 'NW',
                'asn_hdr_des'       => '',
                'asn_hdr_ctn_ttl'   => $order->shipment->ctn_qty_ttl ?? 1,
                'asn_hdr_itm_ttl'   => $order->details->count(),
                'sys_mea_code'      => 'IN',
                'asn_hdr_ctnr_ttl'  => 1,
            ];
            $asnHdr = AsnHdr::create($dataAsnHdr);

            $container = Container::where('ctnr_num', $odrNum)->first();
            if (!$container){
                $container = Container::create([
                    'ctnr_num' => $odrNum,
                    'ctnr_note' => '',
                ]);
            }


            for ($i=0; $i < $order->details->count(); $i++){
                if($order->details[$i]->picked_qty > 0){
                    $expectDate = date('ymd', $asnHdr->asn_hdr_ept_dt);
                    $expiredDate = 0;
                    $ucc128 = $this->generateUpc128($order->details[$i]->item_id, $order->cus_id);

                    $ctnTotal =  ceil($order->details[$i]->picked_qty / $order->details[$i]->pack);

                    $asnDtls[] = new AsnDtl([
                        'ctnr_id'               => $container->ctnr_id,
                        'ctnr_num'              => $container->ctnr_num,
                        'item_id'               => $order->details[$i]->item_id,
                        'asn_dtl_lot'           => $order->details[$i]->lot,
                        'asn_dtl_po'            => $order->shippingOrder->po_total,
                        'asn_dtl_po_dt'         => 0,
                        'uom_id'                => $order->details[$i]->uom_id,
                        'uom_code'              => $order->details[$i]->systemUom->sys_uom_code,
                        'uom_name'              => $order->details[$i]->systemUom->sys_uom_name,
                        'asn_dtl_ctn_ttl'       => $ctnTotal,
                        'asn_dtl_crs_doc'       => 0,
                        'asn_dtl_crs_doc_qty'   => 0,
                        'asn_dtl_des'           => '',
                        'asn_dtl_length'        => $order->details[$i]->item->length,
                        'asn_dtl_width'         => $order->details[$i]->item->width,
                        'asn_dtl_height'        => $order->details[$i]->item->height,
                        'asn_dtl_weight'        => $order->details[$i]->item->weight,
                        'asn_dtl_sts'           => 'NW',
                        'asn_dtl_volume'        =>  $order->details[$i]->item->volume,
                        'asn_dtl_pack'          => $order->details[$i]->pack,
                        'asn_dtl_cus_upc'       => $order->details[$i]->cus_upc,
                        'asn_dtl_sku'           => $order->details[$i]->sku,
                        'asn_dtl_size'          => $order->details[$i]->size,
                        'asn_dtl_color'         => $order->details[$i]->color,
                        'asn_dtl_qty_ttl'       => $order->details[$i]->picked_qty,
                        'expired_dt'            => 0,
                        'cat_code'              => $order->details[$i]->item->cat_code,
                        'cat_name'              => $order->details[$i]->item->cat_name,
                        'asn_dtl_cube'          => $order->details[$i]->item->cube,
                        'ucc128'                => $ucc128,
                        'spc_hdl_code'          => $order->details[$i]->item->spc_hdl_code,
                        'spc_hdl_name'          => $order->details[$i]->item->spc_hdl_name
                    ]);
                }
            }
            $asnHdr->asnDtl()->saveMany($asnDtls);

            // event tracking
            $this->eventTrackingModel->create([
                'whs_id'    => $order->whs_id,
                'cus_id'    => $order->cus_id,
                'owner'     => $asnNumAndSeq['asn_num'],
                'evt_code'  => config('constants.event.ASN-NEW'),
                'trans_num' => $asnNumAndSeq['asn_num'],
                'info'      => sprintf(config('constants.event-info.ASN-NEW'), $asnNumAndSeq['asn_num'])
            ]);

            DB::commit();

            return $this->response->item($asnHdr, new AsnTransformer)
                ->setStatusCode(201);
        }catch(\Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getOrdersWithStatusShipped(Request $request, RMAOrderTransformer $transformer)
    {
        $input = $request->getQueryParams();
        $input = SelArr::removeNullOrEmptyString($input);
        $limit = array_get($input, 'limit', 20);

        $orderAlreadyCreatedRMA = AsnHdr::where('asn_type', 'RMA')->pluck('asn_hdr_ref')->toArray();

        $query = OrderHdr::whereIn('odr_sts', [self::ORDER_STATUS_SHIPPED, self::ORDER_STATUS_RMA])
                        ->where('whs_id', Data::getCurrentWhsId())
                        ->whereNotIn('odr_num', $orderAlreadyCreatedRMA);

        if (isset($input['odr_num']) && $input['odr_num']){
            $query->where('odr_num', 'LIKE', '%' . $input['odr_num'] . '%');
        }

        if (isset($input['cus_odr_num']) && $input['cus_odr_num']){
            $query->where('cus_odr_num', 'LIKE', '%' . $input['cus_odr_num'] . '%');
        }

        if (isset($input['cus_po']) && $input['cus_po']){
            $query->where('cus_po', 'LIKE', '%' . $input['cus_po'] . '%');
        }

        $query->orderBy('odr_num', 'DESC');

        $orders = $query->paginate($limit);

        return $this->response->paginator($orders, $transformer);
    }

    private function generateUpc128($itemId, $cusId)
    {
        return str_pad($cusId, 5, '0', STR_PAD_LEFT) . str_pad($itemId, 8, '0', STR_PAD_LEFT);
    }


    public function reGenerateBillableIn(Request $request)
    {
        $client     = new Client();
        $apiInvoice = env('API_INVOICE_MASTER');

        try {
            if (!$apiInvoice) {
                Log::warning('Can\'t found API INVOICE MASTER IN ENV.');
                return false;
            }
            $authorization =  $request->getHeader('Authorization');
            $whsId = 2;
            $cusId = 3;
            $grHdrIds = [

            ] ;
            foreach ($grHdrIds as $grHdrId) {
                $data = [
                    'headers'     => ['Authorization' => $authorization],
                    'form_params' => [
                        'whs_id'    => $whsId,
                        'cus_id'    => $cusId,
                        'gr_hdr_id' => $grHdrId,
                    ],
                ];
                $client->request('POST', $apiInvoice . '/v1/billableitem/goodsreceipt', $data);
            }
            Log::info('Auto create Billable successful.');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function reGenerateBillableOut(Request $request)
    {
        $client     = new Client();
        $apiInvoice = env('API_INVOICE_MASTER');

        try {
            if (!$apiInvoice) {
                Log::warning('Can\'t found API INVOICE MASTER IN ENV.');
                return false;
            }

            $authorization =  $request->getHeader('Authorization');
            $odrIds = [

            ];
            foreach ($odrIds as $odrId) {
                $client->request('POST', $apiInvoice . '/v1/billableitem/outbound',
                    [
                        'headers'     => ['Authorization' => $authorization],
                        'form_params' => [
                            'odr_id' => $odrId
                        ]
                    ]
                );
            }
            Log::info('Auto create Billable successful.');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

}