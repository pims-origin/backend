<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class ClientContactsTransformer extends TransformerAbstract
{
    public function transform($clientContacts)
    {
        return [
            'cus_ctt_id' => $clientContacts->cus_ctt_id,
            'cus_ctt_dft' => $clientContacts->cus_ctt_dft,
            'cus_ctt_fname' => $clientContacts->cus_ctt_fname,
            'cus_ctt_lname' => $clientContacts->cus_ctt_lname,
            'cus_ctt_email' => $clientContacts->cus_ctt_email,
            'cus_ctt_phone' => $clientContacts->cus_ctt_phone,
            'cus_ctt_mobile' => $clientContacts->cus_ctt_mobile,
            'cus_ctt_ext' => $clientContacts->cus_ctt_ext,
            'cus_ctt_position' => $clientContacts->cus_ctt_position,
            'cus_ctt_cus_id' => $clientContacts->cus_ctt_cus_id,
            'cus_ctt_department' => $clientContacts->cus_ctt_department,
        ];
    }

    /*
    ****************************************************************************
    */

}
