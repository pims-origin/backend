<?php

namespace App\Api\V1\Models;

use App\BillableHdr;

class BillableHdrModel extends AbstractModel
{
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new BillableHdr();
    }

    public function deleteMultiByWorkOrderHdrIds($vasNums)
    {
        $this->model->whereIn('ref_num', $vasNums)->delete();
    }

    public function deleteByWorkOrderHdrId($vasNum)
    {
        $this->model->where('ref_num', $vasNum)->delete();
    }

    public function forcedeleteMultiByBillableItemHdrIds($ids)
    {
        $this->model->where('ba_id', $ids)->forceDelete();
    }
}
