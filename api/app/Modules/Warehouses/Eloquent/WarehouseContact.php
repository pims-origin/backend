<?php
namespace App\Modules\Warehouses\Eloquent;

use Illuminate\Database\Eloquent\Model;

class WarehouseContact extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whs_contact';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'whs_con_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_con_fname',
        'whs_con_lname',
        'whs_con_email',
        'whs_con_phone',
        'whs_con_mobile',
        'whs_con_ext',
        'whs_con_position',
        'whs_con_whs_id',
        'whs_con_department',
    ];
    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_con_whs_id', 'whs_id');
    }
}
