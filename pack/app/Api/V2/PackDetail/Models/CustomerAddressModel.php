<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\CustomerAddress;
use Illuminate\Database\Query\Builder;

class CustomerAddressModel extends AbstractModel
{
    protected $model;

    public function __construct(CustomerAddress $model = null)
    {
        $this->model = ($model) ?: new CustomerAddress();
    }

    public function deleteCustomerAddress($customerId, $addressId)
    {
        return $this->model
            ->where('cus_add_id', $addressId)
            ->where('cus_add_cus_id', $customerId)
            ->delete();
    }

    public function getShippingAddressByCustomer($customerId)
    {
        $query = $this->model->where('cus_add_cus_id', $customerId)
            ->where('cus_add_type', 'ship');
        $this->model->filterData($query);
        return $query->first();
    }

    public function getAddressByCustomer($customerId, $addressId)
    {
        $query = $this->model->where('cus_add_cus_id', $customerId)
            ->where('cus_add_id', $addressId);
        $this->model->filterData($query);
        return $query->first();
    }
}
