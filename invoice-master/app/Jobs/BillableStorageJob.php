<?php

namespace App\Jobs;

use App\Helpers\SelExport;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class BillableStorageJob extends IMSJob
{
    public function handle()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $this->writeLogment('Test IMS Billable Storage', 'Test IMS Job');
        try {
            $token = $this->login();

            $endpoint = env('IMS_BILLABLE_STORAGE');

            // Check IMS Configuration
            if(empty($endpoint)) {
                $this->writeLogment('IMS Billable Storage', 'IMS Billable Storage Endpoint is not define');
                return false;
            }

            //$start = new Carbon('first day of last month');
            //$end = new Carbon('last day of last month');

            $start_time = Carbon::yesterday()->startOfMonth()->timestamp;
            $end_time = Carbon::yesterday()->endOfMonth()->timestamp;

            $data = DB::table('billable_hdr')
                ->join('billable_dtl', 'billable_dtl.ba_id', '=', 'billable_hdr.ba_id')
                ->leftJoin('warehouse', 'warehouse.whs_id', '=', 'billable_hdr.whs_id')
                ->leftJoin('customer', 'customer.cus_id', '=', 'billable_hdr.cus_id')
                ->leftJoin('cus_sac', 'cus_sac.sac_id', '=', 'billable_dtl.sac_id')
                ->whereIn('billable_hdr.ba_type', ['INB', 'OUB'])
                ->where('billable_hdr.deleted', 0)
                ->whereBetween('billable_hdr.created_at', [$start_time, $end_time])
                ->select([
                    'customer.cus_name',
                    'warehouse.whs_name',
                    'billable_dtl.sac_name',
                    'billable_dtl.ref_num',
                    DB::raw('SUM(billable_dtl.qty) as total_qty'),
                    'billable_dtl.ba_type',
                    'billable_dtl.description',
                    'cus_sac.unit_uom'
                ])
                ->groupBy('billable_dtl.whs_id')
                ->groupBy('billable_dtl.cus_id')
                ->groupBy('billable_dtl.sac')
                ->groupBy('billable_dtl.ref_num')
                ->get()->toArray();

            if(count($data) == 0) {
                $this->writeLogment('IMS Billable Storage', 'No data available this month');
                return false;
            }

            //$data = ['data' => $data];

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-Type: application/json"
            ];

            $result = $this->call($endpoint, 'POST', json_encode($data), $header);

            $this->writeLogment('IMS Billable Storage - WMS', 'Body: ' . json_encode($data));

            $title = ['cus_name', 'whs_name', 'sac_name', 'ref_num', 'total_qty', 'ba_type', 'description', 'unit_uom'];
            $fileName = date('Y-m-d').'_'.time().'_IMS_Billable_Storage';
            $filePath = storage_path($fileName);
//        $this->saveFile($title, $data, $filePath);
            SelExport::exportList($filePath, 'csv', $data);
            Storage::disk('s3')->put(env('S3_PIMS_DIR').$fileName.'.csv', file_get_contents($filePath.'.csv'));
            //unlink($filePath);

            $this->writeLogment('IMS Billable Storage - IMS', 'Response: ' . $result);

        } catch (\Exception $e) {
            $this->writeLogment('IMS Billable Storage', $e->getMessage());
        }
    }

    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToFile($filePath);

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case "*":
                            $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                            break;
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                        case "-":
                            $value = array_get($item, $values[0], null) - array_get($item, $values[2], null);
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }


        $writer->addRows($dataSave);

        $writer->close();
    }
}
