<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\WaveDtlLocModel;
use App\Api\V1\Models\WaveDtlModel;
use App\Api\V1\Models\WaveHdrModel;
use App\Api\V1\Transformers\WaveDetailTransformer;
use App\Api\V1\Transformers\WaveTransformer;
use App\Api\V1\Validators\WaveValidator;
use App\Jobs\AutoPackJob;
use Carbon\Carbon;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

/**
 * Class WaveController
 *
 * @package App\Api\V1\Controllers
 */
class WaveController extends AbstractController
{

    /**
     * @var int|mixed
     */
    protected $userId;

    /**
     * @var WaveHdrModel
     */
    protected $waveHdrModel;

    /**
     * @var WaveDtlModel
     */
    protected $waveDtlModel;

    /**
     * @var WaveDtlLocModel
     */
    protected $waveDtlLocModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var WaveDtlLocModel
     */
    protected $wavePickDtlLocModel;

    /**
     * @var CartonModel
     */
    protected $carton;

    /**
     * WaveController constructor.
     *
     * @param WaveHdrModel $waveHdrModel
     * @param WaveDtlModel $waveDtlModel
     * @param WaveDtlLocModel $waveDtlLocModel
     * @param CartonModel $cartonModel
     * @param PackDtlModel $packDtlModel
     * @param PackHdrModel $packHdrModel
     * @param ItemModel $itemModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        WaveHdrModel $waveHdrModel,
        WaveDtlModel $waveDtlModel,
        WaveDtlLocModel $waveDtlLocModel,
        CartonModel $cartonModel,
        PackDtlModel $packDtlModel,
        PackHdrModel $packHdrModel,
        ItemModel $itemModel,
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        LocationModel $locationModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->waveHdrModel = $waveHdrModel;
        $this->waveDtlModel = $waveDtlModel;
        $this->waveDtlLocModel = $waveDtlLocModel;
        $this->cartonModel = $cartonModel;
        $this->palletModel = new PalletModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->locationModel = $locationModel;
        $this->packHdrModel = $packHdrModel;
        $this->packDtlModel = $packDtlModel;
        $this->itemModel = $itemModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->customerConfigModel = new CustomerConfigModel();
        $this->wavePickDtlLocModel = new WaveDtlLocModel();

        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param WaveTransformer $waveTransformer
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search($whsId, Request $request, WaveTransformer $waveTransformer, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            $allWaves = $this->waveHdrModel->getModel()
                ->leftJoin('odr_hdr', 'wv_hdr.wv_id', '=', 'odr_hdr.wv_id')
                ->join('wv_dtl', 'wv_hdr.wv_id', '=', 'wv_dtl.wv_id')
                ->select([
                    'wv_hdr.wv_id',
                    'wv_hdr.wv_num',
                    DB::raw('COUNT(DISTINCT odr_hdr.odr_id) AS odr_ttl'),
                    DB::raw('COUNT(DISTINCT wv_dtl.wv_dtl_id) AS total'),
                    DB::raw('(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty = wv_dtl.piece_qty) as actual'),
                    DB::raw("IF(wv_hdr.wv_sts = 'NW', 'NOT', 'IN') AS status")
                ])
                ->where('wv_hdr.is_ecom', 0)
                ->where('wv_hdr.whs_id', $input['whs_id'])
                ->where("wv_dtl.picker_id", $this->userId)
                ->whereIn('wv_hdr.wv_sts', ['NW', 'PK'])
                ->groupBy('wv_hdr.wv_id')
                ->orderBy('wv_hdr.updated_at', 'DESC')
                ->get();

            if (!empty($allWaves)) {
                return $this->response->collection($allWaves, $waveTransformer);
            } else {
                return ['data' => []];
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function searchV2($whsId, Request $request)
    {
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['picker'] = Data::getCurrentUserId();

        try {
            $allWaves = $this->waveHdrModel->getModel()
                ->leftJoin('odr_hdr', 'wv_hdr.wv_id', '=', 'odr_hdr.wv_id')
                ->join('wv_dtl', 'wv_hdr.wv_id', '=', 'wv_dtl.wv_id')
                ->join('wv_assignment', function($join) use ($input){
                    $join->on('wv_assignment.wv_id', '=', 'wv_hdr.wv_id');
                    $join->where('wv_assignment.picker_id', '=', $input['picker']);
                })
                ->select([
                    DB::raw('IF ((SELECT SUM(rush_odr) FROM odr_hdr WHERE odr_hdr.wv_id = wv_hdr.wv_id) > 0, 1, 0) AS rush_odr'),
                    'wv_hdr.wv_id',
                    'wv_hdr.wv_num',
                    DB::raw('COUNT(DISTINCT odr_hdr.odr_id) AS odr_ttl'),
                    DB::raw('COUNT(DISTINCT wv_dtl.wv_dtl_id) AS total'),
                    DB::raw('(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty = wv_dtl.piece_qty) as actual'),
                    DB::raw("IF(wv_hdr.wv_sts = 'NW', 'NOT', 'IN') AS status")
                ])
                ->where('wv_hdr.is_ecom', 0)
                ->where('wv_hdr.whs_id', $input['whs_id'])
                ->whereIn('wv_hdr.wv_sts', ['NW', 'PK'])
//                ->where('wv_hdr.wv_sts', '=', 'NW')
                ->groupBy('wv_hdr.wv_id')
                ->orderBy('rush_odr', 'DESC')
                ->orderBy('wv_hdr.updated_at', 'DESC')
                ->get();

           if (!empty($allWaves)) {
                return $this->response->collection($allWaves, new WaveTransformer());
            } else {
                return ['data' => []];
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getStatusWvDtl($wv_id)
    {
        $statusWvDtl = $this->waveDtlModel->getStatusWvDtl($wv_id);

        $status = 'NOT';
        if (!empty($statusWvDtl[0])) {
            $sumActualWvdtl = array_get($statusWvDtl[0], 'actual', 0);
            $sumPieceQtyWvdtl = array_get($statusWvDtl[0], 'piece_qty', 0);

            if ($sumActualWvdtl === $sumPieceQtyWvdtl) {
                $status = 'DONE';
            } else if ($sumActualWvdtl > 0 && $sumActualWvdtl < $sumPieceQtyWvdtl) {
                $status = 'IN';
            }
        }


        return $status;
    }

    /**
     * @param $whsId
     * @param $wvHdrId
     * @param WaveDetailTransformer $waveDtlTransformer
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($whsId, $wvHdrId, WaveDetailTransformer $waveDtlTransformer, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input['whs_id'] = $whsId;


        $waveValidator->validate($input);

        try {
            $wave = $this->waveDtlModel->make(['customer'])->getModel()
                ->join('wv_assignment', 'wv_assignment.wv_id', '=', 'wv_dtl.wv_id')
                ->where([
                    'wv_dtl.whs_id'    => $input['whs_id'],
                    'wv_dtl.wv_id'     => $wvHdrId,
                    'wv_assignment.picker_id' => Data::getCurrentUserId()
                ])
                ->get();

            if (empty($wave)) {
                return $this->response->errorBadRequest("This wave pick don't belong user");
            }

            return $this->response->collection($wave, $waveDtlTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $wvDtlId
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function detail($whsId, $wvDtlId, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            $wave = $this->waveDtlModel->getFirstWhere([
                'whs_id'    => $whsId,
                'wv_dtl_id' => $wvDtlId,
                'picker_id' => $this->userId
            ]);

            if (empty($wave)) {
                return $this->response->errorBadRequest('Wave dtl not found');
            }

            $sku_size_color = $wave->sku;
            $size = !empty($wave->size) ? strtoupper($wave->size) : '';
            if ($size != 'NA') {
                $sku_size_color .= '-' . $wave->size;
            }
            $color = !empty($wave->color) ? strtoupper($wave->color) : '';
            if ($color != 'NA') {
                $sku_size_color .= '-' . $wave->color;
            }

            $current_time = Carbon::now()->timestamp;
            $order = DB::table("odr_hdr")->where("wv_id", $wave->wv_id)->first();
            $dataLaborTracking = [
                'user_id'       => $this->userId,
                'cus_id'        => object_get($wave, 'cus_id'),
                'whs_id'        => $whsId,
                'trans_num'     => $wave->wv_num,
                'trans_dtl_id'  => $wvDtlId,
                'start_time'    => $current_time,
                'end_time'      => $current_time,
                'lt_type'       => "OB",
                'owner'         => array_get($order, 'odr_num', "")
            ];
            DB::table('rpt_labor_tracking')->insert($dataLaborTracking);
            // Make data of wave pick detail
            $ucc128s = DB::table('cartons')->select(['ucc128'])->where([
                'whs_id'  => $whsId,
                'ctn_sts' => 'AC',
                'item_id' => $wave->item_id,
                'lot'     => $wave->lot
            ])->whereNotNull('loc_id')
                ->distinct()->get();

            $ucc128s = array_pluck($ucc128s, 'ucc128');

            $item = DB::table("item")->where("item_id", $wave->item_id)->select("spc_hdl_code")->first();

            $detail['data'] = [
                'wv_id'         => $wave->wv_id,
                'wv_num'        => $wave->wv_num,
                'wv_dtl_id'     => $wave->wv_dtl_id,
                'sku'           => $sku_size_color,
                'qty'           => $wave->piece_qty . "/" . $wave->act_piece_qty,
                'item_id'       => $wave->item_id,
                'upc'           => $wave->cus_upc,
                'status'        => $this->getStatusWvDtl($wvDtlId),
                'pack_size'     => $wave->pack_size,
                'lot'           => $wave->lot,
                'ucc128s'       => $ucc128s,
                "spc_hdl_code"  => $item['spc_hdl_code'],
                'pick_pallet'   => $wave->pick_pallet,
                'uom_id'        => $wave->uom_id,
                'uom_code'      => $this->_getUomCodeById($wave->uom_id),
            ];

            return $detail;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function detailV2($whsId, $wvDtlId, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $wave = $this->waveDtlModel->getFirstWhere([
                'whs_id'    => $whsId,
                'wv_dtl_id' => $wvDtlId
        ]);

        if (empty($wave)) {
            return $this->response->errorBadRequest('Wave dtl not found');
        }

        $wvAssigns = DB::table('wv_assignment')
                        ->where('wv_id', $wave->wv_id)
                        ->where('picker_id', $this->userId)
                        ->get();
        if ( !$wvAssigns ) {
            $msg = "Picker not assigned to this wave pick";
            return $this->response->errorBadRequest($msg);
        }

        try {
            $sku_size_color = $wave->sku;
            $size = !empty($wave->size) ? strtoupper($wave->size) : '';
            if ($size != 'NA') {
                $sku_size_color .= '-' . $wave->size;
            }
            $color = !empty($wave->color) ? strtoupper($wave->color) : '';
            if ($color != 'NA') {
                $sku_size_color .= '-' . $wave->color;
            }
            $order = DB::table("odr_hdr")->where("wv_id", $wave->wv_id)->first();
            $current_time = Carbon::now()->timestamp;
            $dataLaborTracking = [
                'user_id'       => $this->userId,
                'cus_id'        => object_get($wave, 'cus_id'),
                'whs_id'        => $whsId,
                'trans_num'     => $wave->wv_num,
                'trans_dtl_id'  => $wvDtlId,
                'start_time'    => $current_time,
                'end_time'      => $current_time,
                'lt_type'       => "OB",
                'owner'         => array_get($order, 'odr_num', "")
            ];
            DB::table('rpt_labor_tracking')->insert($dataLaborTracking);

            // Make data of wave pick detail
            $ucc128s = DB::table('cartons')->select(['ucc128'])->where([
                'whs_id'  => $whsId,
                'ctn_sts' => 'AC',
                'item_id' => $wave->item_id,
                'lot'     => $wave->lot
            ])->whereNotNull('loc_id')
                ->distinct()->get();

            $ucc128s = array_pluck($ucc128s, 'ucc128');

            $detail['data'] = [
                'wv_id'       => $wave->wv_id,
                'wv_num'      => $wave->wv_num,
                'po_num'      => array_get($order, 'cus_po', ''),
                'wv_dtl_id'   => $wave->wv_dtl_id,
                'sku'         => $sku_size_color,
                'qty'         => $wave->piece_qty . "/" . $wave->act_piece_qty,
                'item_id'     => $wave->item_id,
                'upc'         => $wave->cus_upc,
                'status'      => $this->getStatusWvDtl($wvDtlId),
                'pack_size'   => $wave->pack_size,
                'lot'         => $wave->lot,
                'ucc128s'     => $ucc128s,
                'pick_pallet' => $wave->pick_pallet,
                'uom_id'      => $wave->uom_id,
                'uom_code'    => $this->_getUomCodeById($wave->uom_id),
            ];

            return $detail;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _getUomCodeById($uomId)
    {
        $systemUom = DB::table('system_uom')
                        ->where('deleted', 0)
                        ->where('sys_uom_id', $uomId)
                        ->first();

        return array_get($systemUom, 'sys_uom_code', '');
    }
    /**
     * @param $whsId
     * @param $wvDtlId
     * @param Request $request
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function suggest($whsId, $wvDtlId, Request $request, WaveValidator $waveValidator)
    {
        $input = $request->getQueryParams();

        // get data from HTTP
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            if (empty($input['type'])) {
                $input['type'] = "CT";
            }

            if (!in_array($input['type'], ["LC", "CT"])) {
                throw new \Exception("'type' is invalid!");
            }

            $waveDtlLoc = $this->waveDtlLocModel->getFirstWhere(['wv_dtl_id' => $wvDtlId], ['waveHdr', 'waveDtl']);

            if (empty($waveDtlLoc->waveHdr)
                || empty($waveDtlLoc->waveDtl)
                || $waveDtlLoc->waveHdr->picker != $this->userId
            ) {
                return $this->response->noContent();
            }

            $itemId = object_get($waveDtlLoc, "waveDtl.item_id", 0);
            $pieceQty = object_get($waveDtlLoc, "waveDtl.piece_qty", 0);
            $actPieceQty = object_get($waveDtlLoc, "waveDtl.act_piece_qty", 0);
            $packSize = object_get($waveDtlLoc, "waveDtl.pack_size", 0);
            $limit = ceil(($pieceQty - $actPieceQty) / $packSize);

            $locIds = explode(",", $waveDtlLoc->sug_loc_ids);
            if ($input['type'] == "CT") {
                $cartons = $this->cartonModel->getSuggest([
                    'whs_id'  => $whsId,
                    'item_id' => $itemId,
                    'loc_id'  => $locIds,
                ], $limit)->toArray();

                $carton['data'] = array_map(function ($e) {
                    return $e['ctn_num'];
                }, $cartons);
            } else {
                $location = $this->locationModel->getSuggest($locIds)->toArray();
                $carton['data'] = array_map(function ($e) {
                    return $e['loc_alternative_name'];
                }, $location);
            }

            return $carton;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $wv_dtl_id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function updateWavepick($whsId, $wv_dtl_id, Request $request)
    {
        set_time_limit(0);

        // get data from HTTP
        $input = $request->getParsedBody();
        $type = $input['type'];
        $codes = $input['codes'];

        try {

            if ($type == "CT") {
                $dataWaveByCartons = [
                    'whs_id'    => $whsId,
                    'wv_dtl_id' => $wv_dtl_id,
                    'codes'     => $codes
                ];
                $this->updateWavePickByCarton($dataWaveByCartons);
            } elseif ($type == "LC") {
                $dataWaveByPallets = [
                    'whs_id'    => $whsId,
                    'wv_dtl_id' => $wv_dtl_id,
                    'codes'     => $codes
                ];

                $cartonCode = $this->updateWavePickByLocation($dataWaveByPallets);
                // check number
                $dataWaveByCartons = $dataWaveByCartons = [
                    'whs_id'    => $whsId,
                    'wv_dtl_id' => $wv_dtl_id,
                    'codes'     => $cartonCode

                ];
                $this->updateWavePickByCarton($dataWaveByCartons);

            } else {

                $dataWaveByEA = [
                    'whs_id'    => $whsId,
                    'wv_dtl_id' => $wv_dtl_id,
                    'codes'     => null
                ];
                $this->updateWavePickByEA($dataWaveByEA);
            }

            $wvDtl = $this->waveDtlModel->getFirstWhere(['wv_dtl_id' => $wv_dtl_id]);

            $event = [
                'whs_id'    => $whsId,
                'cus_id'    => object_get($wvDtl, 'cus_id'),
                'owner'     => object_get($wvDtl, 'wv_num'),
                'evt_code'  => 'RFW',
                'trans_num' => 'RFG',
                'info'      => sprintf("RF Gun -  Update Wave Pick by %s", ($type == 'CT') ? 'Carton' : 'Location'),
            ];

            //  Evt tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($event);

            return $this->response->noContent()
                ->setContent(['data' => ['message' => 'Update wavepick successfully']])
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                "Bad Database connection!, Please ask admin for checking."
            );
        } catch (\Exception $e) {
            throw $e;
            //return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $data
     */
    private function updateWavePickByCarton($data)
    {
        $whs_id = $data['whs_id'];
        $wv_dtl_id = $data['wv_dtl_id'];
        $codes = $data['codes'];

        $ctnLists = $this->cartonModel->checkActiveCartonByCtnNum($codes)->toArray();

        if (count($ctnLists) == 0) {
            throw new \Exception(Message::get("BM155"));
        }

        // Check active & existed carton
        $dataCheckActive = [
            'ctnNums'  => $codes,
            'ctnLists' => $ctnLists
        ];
        $this->checkActiveCarton($dataCheckActive);

        // Get wavepick detail info
        $dataWaveDtlInfo = $this->waveDtlModel->getFirstWhere([
            'wv_dtl_id' => $wv_dtl_id
        ]);

        // Check wave pick detail existed
        if (empty($dataWaveDtlInfo)) {
            throw new \Exception(Message::get("BM017", "Wavepick detail " . $wv_dtl_id));
        }

        $itemId = array_get($dataWaveDtlInfo, 'item_id', null);
        $cusId = array_get($dataWaveDtlInfo, 'cus_id', null);
        $wv_id = array_get($dataWaveDtlInfo, 'wv_id', null);
        $piece_qty = array_get($dataWaveDtlInfo, 'piece_qty', null);
        $act_piece_qty = array_get($dataWaveDtlInfo, 'act_piece_qty', 0);

        // Get all carton by item id
        $dataGetCtnInfo = [
            'whsId'   => $whs_id,
            'itemId'  => $itemId,
            'cusId'   => $cusId,
            'ctnNums' => $codes
        ];

        $dataCartonInfos = $this->cartonModel->getAllCartonByItemId($dataGetCtnInfo)->toArray();

        if (count($dataCartonInfos) == 0) {
            throw new \Exception(Message::get("BM017", "Carton with item " . $itemId));
        }

        $dataCheckCartonHasItem = [
            'item_id' => $itemId,
            'ctnNums' => $codes,
            'cartons' => $dataCartonInfos
        ];

        // Check carton has item_id or not
        $this->checkCartonHasItemId($dataCheckCartonHasItem);

        // Start to handle valid cartons
        $sumPieceRemainFromCtns = 0;
        foreach ($dataCartonInfos as $dataCartonInfo) {
            $sumPieceRemainFromCtns += $dataCartonInfo['piece_remain'];
        }

        $actPicked = $act_piece_qty + $sumPieceRemainFromCtns;

        if ($actPicked > $piece_qty) {
            throw new \Exception(Message::get("BM156", $actPicked, $piece_qty));
        }

        // Update wave pick detail
        $dataUpdateWvDtl = [
            'act_piece_qty' => $actPicked,
            'wv_dtl_id'     => $wv_dtl_id,
            'wv_dtl_sts'    => ($actPicked < $piece_qty
                ? Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS")
                : Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS"))
        ];
        try {
            \DB::beginTransaction();
            $this->waveDtlModel->updateWvDtl($dataUpdateWvDtl);

            /*
             * ==================================================================
             * HANDLE TABLES: CARTONS, PALLET, ORDER CARTON WHEN PICK FULL CARTON
             * ==================================================================
             * */

            // insert order carton
            $dataOrderCarton = [
                'wv_id'           => $wv_id,
                'dataCartonInfos' => $dataCartonInfos
            ];
            $this->processOrderCarton($dataOrderCarton);

            // update carton status, release all carton from pallet & location
            $this->updateCartonStatusToPicked($dataCartonInfos);

            // update total carton in pallet table, also release pallet from location if has no carton
            $this->updateCtnTtl($dataCartonInfos);

            $this->updateOrderPicked($wv_id, $wv_dtl_id);

            $this->updateWaveHeaderPicked($wv_id);


            /*
             * ==================================================================
             * END PROCESS
             * ==================================================================
             * */
            \DB::commit();

        } catch (\Exception $e) {
            \DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param $dataCartonInfos
     */
    private function updateCtnTtl($dataCartonInfos)
    {
        $arrLocIds = [];
        $arrPltIds = [];
        foreach ($dataCartonInfos as $dataCartonInfo) {
            if (!in_array($dataCartonInfo['loc_id'], $arrLocIds)) {
                array_push($arrLocIds, $dataCartonInfo['loc_id']);
            }

            if (!in_array($dataCartonInfo['plt_id'], $arrPltIds)) {
                array_push($arrPltIds, $dataCartonInfo['plt_id']);
            }
        }
        //$this->palletModel->updateCtnTtl($arrLocIds);
        $this->updatePalletWhenPickCarton($arrPltIds);
    }

    /**
     * @param $dataOrderCarton
     */
    private function processOrderCarton($dataOrderCarton)
    {
        $wv_id = $dataOrderCarton['wv_id'];
        $dataCartonInfos = $dataOrderCarton['dataCartonInfos'];
        $ordersDtlInfos = $this->orderDtlModel->getListOrderDtlOfWavePick($wv_id);

        foreach ($ordersDtlInfos as $ordersDtlInfo) {
            foreach ($dataCartonInfos as $dataCartonInfo) {
                $item = [
                    'ctn_num' => $dataCartonInfo['ctn_num'],
                    'ctn_id'  => $dataCartonInfo['ctn_id']
                ];
                $dataOrderCarton = [
                    'odr_dtl'    => $ordersDtlInfo,
                    'item'       => $item,
                    'carton'     => $dataCartonInfo,
                    'picked_qty' => array_get($dataCartonInfo, 'piece_remain', null),
                    'wv_id'      => $wv_id,
                    'is_storage' => 0
                ];
                $this->insertOrderCarton($dataOrderCarton);
            }
        }
    }

    /**
     * @param $data
     */
    private function insertOrderCarton($data)
    {
        $odr_dtl = $data['odr_dtl'];
        $item = $data['item'];
        $carton = $data['carton'];
        $wv_id = $data['wv_id'];
        $isStorage = $data['is_storage'];

        $dataOrderCartonInsert = [
            'odr_hdr_id'   => $odr_dtl['odr_id'],
            'odr_dtl_id'   => $odr_dtl['odr_dtl_id'],
            'wv_hdr_id'    => $wv_id,
            'wv_dtl_id'    => $odr_dtl['wv_dtl_id'],
            'odr_num'      => $odr_dtl['odr_num'],
            'wv_num'       => $odr_dtl['wv_num'],
            'ctnr_rfid'    => '',
            'ctn_num'      => $item['ctn_num'],
            'ctn_id'       => $item['ctn_id'],
            'piece_qty'    => array_get($carton, 'piece_remain', null),
            'ship_dt'      => 0,
            'sts'          => Status::getByKey('CTN_STATUS', 'PICKED'),
            'is_storage'   => $isStorage,
            'ctn_rfid'     => array_get($carton, 'ctnr_num', null),
            'ctn_sts'      => array_get($carton, 'ctn_sts', null),
            'item_id'      => array_get($carton, 'item_id', null),
            'sku'          => array_get($carton, 'sku', null),
            'size'         => array_get($carton, 'size', null),
            'color'        => array_get($carton, 'color', null),
            'lot'          => array_get($carton, 'lot', null),
            'upc'          => array_get($carton, 'upc', null),
            'pack'         => array_get($carton, 'ctn_pack_size', null),
            'uom_id'       => array_get($carton, 'ctn_uom_id', null),
            'uom_code'     => array_get($carton, 'uom_code', null),
            'uom_name'     => array_get($carton, 'uom_name', null),
            'cat_code'     => array_get($carton, 'cat_code', null),
            'cat_name'     => array_get($carton, 'cat_name', null),
            'whs_id'       => array_get($carton, 'whs_id', null),
            'cus_id'       => array_get($carton, 'cus_id', null),
            'spc_hdl_code' => array_get($carton, 'spc_hdl_code', null),
            'spc_hdl_name' => array_get($carton, 'spc_hdl_name', null),
            'loc_id'       => array_get($carton, 'loc_id', null),
            'loc_code'     => array_get($carton, 'loc_code', null),
            'length'       => array_get($carton, 'length', null),
            'width'        => array_get($carton, 'width', null),
            'height'       => array_get($carton, 'height', null),
            'weight'       => array_get($carton, 'weight', null),
            'volume'       => array_get($carton, 'volume', null),
            'plt_id'       => array_get($carton, 'plt_id', null),
            'plt_rfid'     => array_get($carton, 'plt_num', null)
        ];

        $this->orderCartonModel->refreshModel();
        $this->orderCartonModel->create($dataOrderCartonInsert);
    }

    /**
     * @param $pltIds
     */
    private function updatePalletWhenPickCarton($pltIds)
    {
        $allPalletHasNoCartons = $this->palletModel->getAllPalletHasNoCarton($pltIds);
        foreach ($allPalletHasNoCartons as $allPalletHasNoCarton) {
            $this->palletModel->updatePallet($allPalletHasNoCarton);
        }
    }

    /**
     * Pick full carton
     *
     * @param $dataCartonInfos
     */
    private function updateCartonStatusToPicked($dataCartonInfos)
    {
        foreach ($dataCartonInfos as $ctn) {
            $created_at = (int)array_get($ctn, 'created_at', 0);

            $picked_dt = time();

            // Calculate storage_duration
            $date1 = date("Y-m-d", is_int($created_at) || is_string($created_at) ? (int)$created_at :
                $created_at->timestamp);

            $date2 = date("Y-m-d");

            $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }
            $dataCartonUpdate = [
                'ctn_sts'          => Status::getByKey('CTN_STATUS', 'PICKED'),
                'picked_dt'        => $picked_dt,
                'storage_duration' => $storageDuration,
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'plt_id'           => null,
            ];

            $this->cartonModel->updateWhere($dataCartonUpdate, [
                'ctn_num' => $ctn['ctn_num']
            ]);

            // Update ctn_ttl in pallet table
            $plt_id = array_get($ctn, 'plt_id', null);

            $palletInfo = $this->palletModel->getFirstWhere(['plt_id' => $plt_id]);

            $this->palletModel->updateWhere([
                'ctn_ttl' => array_get($palletInfo, 'ctn_ttl', null) - 1
            ], [
                'plt_id' => $plt_id
            ]);
        }
    }

    /**
     * @param $dataCheckActive
     */
    private function checkActiveCarton($dataCheckActive)
    {
        $ctnNums = $dataCheckActive['ctnNums'];
        $ctnLists = $dataCheckActive['ctnLists'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id'  => $e['ctn_id'],
                'ctn_num' => $e['ctn_num'],
            ];
        }, $ctnLists);
        $allCtns = array_pluck($tempCtns, null, "ctn_num");

        foreach ($ctnNums as $key => $value) {
            if (!isset($allCtns[$value])) {
                throw new \Exception("Carton " . $value . " is not active or existed");
            }
        }
    }

    /**
     * @param $dataCheckCarton
     */
    private function checkCartonHasItemId($dataCheckCarton)
    {
        $itemId = $dataCheckCarton['item_id'];
        $ctnNums = $dataCheckCarton['ctnNums'];
        $ctnLists = $dataCheckCarton['cartons'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id'  => $e['ctn_id'],
                'ctn_num' => $e['ctn_num'],
            ];
        }, $ctnLists);
        $allCtns = array_pluck($tempCtns, null, "ctn_num");

        foreach ($ctnNums as $key => $value) {
            if (!isset($allCtns[$value])) {
                throw new \Exception("Carton " . $value . " does not has item " . $itemId);
            }
        }
    }

    private function updateWavePickByPallet($data)
    {
        $whs_id = $data['whs_id'];
        $wv_dtl_id = $data['wv_dtl_id'];
        $codes = $data['codes'];

    }

    /**
     * @param $data
     *
     * @return int
     */
    private function updateWavePickByEA($data)
    {
        $whs_id = $data['whs_id'];
        $wv_dtl_id = $data['wv_dtl_id'];
        $codes = $data['codes'];

        return 0;
    }

    private function updateOrderPicked($wvHdrId, $wvDtlId)
    {
        //  Update Oder Dtl
        $this->orderDtlModel->updatePDOdrDtl($wvHdrId);

        //  Update Oder Hdr is picking
        //$this->orderHdrModel->updatePKOdrHdr($wvHdrId);

        //  Update Oder Hdr is picked
        $this->orderHdrModel->updatePDOdrHdr($wvHdrId);

        //  Order Auto pack
        $odrCartons = $this->orderCartonModel->autoPack($wvHdrId);

        if ($odrCartons) {
            $odrIdAutoPacks = [];
            foreach ($odrCartons as $odrCarton) {

                $odrId = array_get($odrCarton, 'odr_id');
                $odrIdAutoPacks[$odrId] = $odrId;

                $item = $this->itemModel->getFirstBy('item_id', $odrCarton['item_id']);

                //  Update Oder Hdr is picked
                $this->packHdrModel->refreshModel();
                $packHdr = $this->packHdrModel->create([
                    'pack_hdr_num' => str_replace('CTN', 'CNT', $odrCarton['ctn_num']),
                    'seq'          => 1,
                    'odr_hdr_id'   => $odrId,
                    'cnt_id'       => array_get($odrCarton, 'ctn_id'),
                    'whs_id'       => array_get($odrCarton, 'whs_id'),
                    'cus_id'       => array_get($odrCarton, 'cus_id'),
                    'width'        => array_get($item, 'width', null),
                    'height'       => array_get($item, 'height', null),
                    'length'       => array_get($item, 'length', null),
                    'carrier_name' => '',
                    'sku_ttl'      => 1,
                    'piece_ttl'    => array_get($odrCarton, 'piece_remain'),
                    'pack_sts'     => 'AC',
                    'pack_type'    => 'CT',
                    'ship_to_name' => '',
                    'sts'          => 'i'
                ]);

                //  Update Oder Hdr is picked
                $this->packDtlModel->refreshModel();
                $this->packDtlModel->create([
                    'pack_hdr_id' => $packHdr->pack_hdr_id,
                    'odr_hdr_id'  => array_get($odrCarton, 'odr_id'),
                    'whs_id'      => array_get($odrCarton, 'whs_id'),
                    'cus_id'      => array_get($odrCarton, 'cus_id'),
                    'item_id'     => array_get($odrCarton, 'item_id'),
                    'size'        => array_get($item, 'size', null),
                    'lot'         => array_get($item, 'lot', null),
                    'sku'         => array_get($item, 'sku', null),
                    'color'       => array_get($item, 'color', null),
                    'cus_upc'     => array_get($item, 'cus_upc', null),
                    'uom_id'      => array_get($item, 'uom_id', null),
                    'piece_qty'   => array_get($odrCarton, 'piece_remain'),
                    'sts'         => 'i',
                ]);
            }
            //  Update Oder Hdr is packed
            $this->orderHdrModel->updatePAOdrHdr($odrIdAutoPacks);
        }
    }

    private function updateWaveHeaderPicked($wvHdrId)
    {
        $allDetail = DB::table('wv_dtl')->select(['wv_dtl_sts'])->where(['wv_id' => $wvHdrId])->get();
        if (!empty($allDetail)) {
            $picked = 1;
            $picking = 0;
            foreach ($allDetail as $detail) {

                if ($detail['wv_dtl_sts'] == "PK") {
                    $picking = true;
                    break;
                }

                if ($detail['wv_dtl_sts'] != "PD") {
                    $picked = 0;
                }
            }

            if ($picking) {
                $this->waveHdrModel->update(['wv_id' => $wvHdrId, 'wv_sts' => 'PK']);
            } else if ($picked) {
                $this->waveHdrModel->update(['wv_id' => $wvHdrId, 'wv_sts' => 'CO']);
            }
        }
    }


    public function wavepickDashboard($whsId, Request $request, WaveValidator $waveValidator)
    {
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;
        $waveValidator->validate($input);
        try {


            $wavepick = $this->waveHdrModel->getWaveDashBoard($input, ['details']);

            $result = ["wavepick" => 0, "picking" => 0, "picked" => 0];
            if (!empty($wavepick)) {
                foreach ($wavepick as $key => $allWave) {
                    $total = count($allWave['details']);
                    $picking = 0;
                    foreach ($allWave['details'] as $detail) {
                        if (empty($detail['act_piece_qty'])) {
                            $picking++;
                        }
                    }
                    $result['wavepick'] = $result['wavepick'] + $total;
                    $result['picked'] = $result['picked'] + $total - $picking;
                    $result['picking'] = $result['picking'] + $picking;
                }

            }

            return $this->response->array($result);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function updateWavePickByLocation($params)
    {
        // Wave Pick Detail
        $wvDtl = $this->waveDtlModel->getFirstWhere(['wv_dtl_id' => $params['wv_dtl_id']]);

        if (empty($wvDtl)) {
            throw new \Exception("Wave Pick detail is empty!");
        }

        $ctnTotal = ceil(($wvDtl->piece_qty - $wvDtl->act_piece_qty) / $wvDtl->pack_size);

        $locations = $this->locationModel->getByLocCode($params['codes'], $params['whs_id'])->toArray();
        $locationCodes = array_pluck($locations, 'loc_id', 'loc_code');
        foreach ($params['codes'] as $code) {
            if (empty($locationCodes[$code])) {
                return "$code is invalid!";
            }
        }

        $result = $this->cartonModel->getCartonsByLocs($locationCodes)->toArray();

        $result = array_column($result, 'ctn_num');
        if (count($result) == 0) {
            throw new \Exception("There're no cartons in this warehouse!");
        }
        // only pick total cartons of location <= total carton
        if (count($result) > $ctnTotal) {
            throw new \Exception("Total pieces of locations larger needed pick!");
        }

        return $result;
    }


    public function isPalletCode($code)
    {
        if((bool)preg_match("/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $code)) {
            return true;
        }
        if((bool)preg_match("/^T-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $code)) {
            return true;
        }

        if((bool)preg_match("/^V-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $code)) {
            return true;
        }
        return false;
    }


    public function updateWavePickPallet($plt_num, $whsid, $wvDtl, $actQty)
    {
        $pallet = $this->palletModel->getFirstWhere(['rfid' => $plt_num, 'whs_id' => Data::getCurrentWhsId()]);
        if (empty($pallet)) {
            return $this->response->errorBadRequest("Pallet '$plt_num' is not existed");
        }

        //  Not Pick Pallet
        if (!$wvDtl->pick_pallet) {

            $pallocId = object_get($pallet, 'loc_id');
            if ($pallocId) {
                //validate location
                $locPal = Location::where('loc_id', $pallocId)
                    ->where('loc_whs_id', Data::getCurrentWhsId())
                    ->first();
                if ($locPal->loc_sts_code != 'AC') {
                    return $this->response->errorBadRequest("Location of pallet '$plt_num' is not active");
                }
            }

            //validate qty
            if ($wvDtl['piece_qty'] < $actQty) {
                $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s picked Qty QTY %d is greater than Allocated Qty %d',
                    $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['lot'], $wvDtl['piece_qty'], $actQty);

                return $this->response->errorBadRequest($msg);
            }
        }

        //get algorithm
        $algorithm = $this->customerConfigModel->getPickingAlgorithm($wvDtl->whs_id, $wvDtl->cus_id);

        try {

            DB::beginTransaction();

            //  Pick Pallet
            if ($wvDtl->pick_pallet == 1) {
                $this->locationModel->pickPalletUpdateActiveLocationByWvDtlId($whsid, $pallet->loc_id);
            }

            $qty = $this->cartonModel->getAvailableQuantityByPallet($wvDtl['item_id'], $wvDtl['lot'], $pallet->plt_id,
                $whsid);
            if ($qty < $actQty) {
                $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s available QTY %d is less than Picked Qty %d',
                    $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['lot'], $qty, $actQty);
                throw new \Exception($msg);
            }

            $actPickInfo = [
                'picked_qty' => $actQty,
                'plt_id'     => $pallet->plt_id,
                'plt_num'    => $plt_num,
                'avail_qty'  => $qty
            ];

            $cartons = $this->cartonModel->getAllCartonsPalletByWvDtl($wvDtl, $algorithm, $actPickInfo);

            $actLoc = [
                'picked_qty' => $actQty,
                'act_loc_id' => $pallet->plt_id,
                'act_loc'    => $pallet->rfid,
                'loc_id'     => $pallet->plt_id,
                'loc_code'   => $pallet->rfid,
                'avail_qty'  => $qty
            ];

            if (!empty($locPal)) {
                $actLoc = [
                    'picked_qty' => $actQty,
                    'act_loc_id' => $locPal->loc_id,
                    'act_loc'    => $locPal->loc_code,
                    'loc_id'     => $locPal->loc_id,
                    'loc_code'   => $locPal->loc_code,
                    'avail_qty'  => $qty
                ];
            }

            $wvDtlSts = 'PK';
            if ($wvDtl->act_piece_qty + $actQty == $wvDtl->piece_qty) {
                $wvDtlSts = 'PD';
            }
            $this->waveDtlModel->updateWaveDtl($wvDtl->wv_dtl_id, $actQty, $wvDtlSts);
            $data = $this->groupCartonsByLot($cartons);
            $odrCartons = $this->cartonModel->updateCartonByLocs($data['cartons']);

            //DB::commit();
            //return ['msg' => 'successful'];

            $this->updateInventoryByLot($actQty, $wvDtl);

            $this->wavePickDtlLocModel->updateWaveDtlLoc($actLoc, $wvDtl->wv_id, $wvDtl->wv_dtl_id);

            $data = $this->orderDtlModel->updateOdrDtlPickedQty($wvDtl, $odrCartons);

            $eventDatas = [
                'evt_code'   => 'WWP',
                'info'       => sprintf('GUN - %d cartons picked in pallet %s', ceil($actQty / $wvDtl['pack_size']),
                    $plt_num),
                'owner'      => $wvDtl['wv_num'],
                'trans_num'  => $wvDtl['wv_num'],
                'created_at' => time(),
                'created_by' => JWTUtil::getPayloadValue('jti') ?: 1
            ];
            $this->eventTrackingModel->create($eventDatas);
            $this->palletModel->updatePalletCtnTtlByPallet([$pallet->plt_id]);
            $this->palletModel->updateZeroPalletByPallet([$pallet->plt_id]);

            if (!empty($pallet->loc_id)) {
                $this->palletModel->removeLocDynZone($pallet->loc_id);
            }

            //update sts
            $this->orderHdrModel->updateOrderPickedByWv($wvDtl->wv_id);

            $this->waveHdrModel->updateWvPicking($wvDtl->wv_id);

            $this->waveHdrModel->updateWvComplete($wvDtl->wv_id);

            $this->locationModel->pickPalletUpdateActiveLocationByWvDtlId($whsid, $wvDtl, $pallocId);

            DB::commit();

            //process auto pack by order flow


            return ['msg' => 'successful'];
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * url PUT: v1/1/update-wavepick
     * @param integer $whsId
     * @param integer wv_dtl_id
     * @param string loc_code
     * @param integer act_qty
     *
     * @return json object
     */
    public function updateWavePickNoEcom($whs_id, Request $request)
    {
        $input = $request->getParsedBody();

        $wvDtlId = $input['wv_dtl_id'];
        $locCode = $input['loc_code'];
        $lpnCarton = null;

        DB::beginTransaction();


        //get wv_dtl
        $wvDtl = WavepickDtl::where([
            'wv_dtl_id' => $wvDtlId,
            'whs_id'    => $whs_id
        ])->first();
        $current_time = Carbon::now()->timestamp;
        DB::table('rpt_labor_tracking')
            ->where("trans_dtl_id",$wvDtlId)
            ->where("whs_id",$whs_id)
            ->where("user_id",$this->userId)
            ->where("cus_id",$wvDtl->cus_id)
            ->update(['end_time' => $current_time]);
        if (!in_array($wvDtl['wv_dtl_sts'], ['NW', 'PK']) || ($wvDtl['piece_qty'] == $wvDtl['act_piece_qty'])) {
            return $this->response->errorBadRequest("Wavepick detail Picked already!");
        }

        $wvHdr = $this->waveHdrModel->findWhere(['wv_id' => $wvDtl->wv_id])->first();

        if (DB::table('wv_assignment')->where('wv_id', $wvDtl->wv_id)->where('picker_id', Data::getCurrentUserId())->count() == 0) {
            return $this->response->errorBadRequest("This wave pick item don't belong user");
        }

        if (!$wvDtl || !$wvHdr) {
            return $this->response->errorBadRequest("Wave pick is not existed");
        }



        if (!in_array($wvHdr['wv_sts'], ['NW', 'PK'])) {
            return $this->response->errorBadRequest("Wave pick Completed already!");
        }

        $uomId   = object_get($wvDtl, 'uom_id');
        $uomCode = $this->_getUomCodeById($uomId);
        $actQty  = $uomCode == 'KG' ? $input['act_qty'] : (int)$input['act_qty'];

        //  Pick Pallet
        if ($wvDtl->pick_pallet == 1) {

            $query = DB::table('odr_dtl_allocation')
                ->select('qty', 'loc_code', 'plt_rfid')
                ->where('wv_dtl_id', $wvDtlId);

            if ($this->isPalletCode($locCode)) {
                $query->where('plt_rfid', trim($locCode));
            } else {
                $query->where('loc_code', trim($locCode));
            }

            $orderDetailAllocation = $query->first();

            if (empty($orderDetailAllocation['plt_rfid'])) {
                return $this->response->errorBadRequest("Wavepick detail is not pick full pallet");
            }
            $locCode = $orderDetailAllocation['plt_rfid'];
            $actQty = $orderDetailAllocation['qty'];

        }

        if ($this->isPalletCode($locCode)) {
            $lpnCarton = $locCode;

            $wvDtlCarton = $this->cartonModel->getFirstWhere([
                'lpn_carton' => $locCode,
                'whs_id' => $wvDtl->whs_id,
                'cus_id' => $wvDtl->cus_id
            ]);

            $pallet = $this->palletModel->getFirstWhere([
                'plt_id' => $wvDtlCarton->plt_id,
                'whs_id' => $wvDtlCarton->whs_id,
                'cus_id' => $wvDtlCarton->cus_id
            ]);

            if(empty($pallet)) {
                return $this->response->errorBadRequest("Pallet " . $locCode . " is not existed");
            }

            if(empty($pallet->loc_code)) {
                return $this->response->errorBadRequest("Pallet " . $locCode . " is not at any location");
            }

            $locCode = $pallet->loc_code;
        }

        //get location
        $loc = Location::where([
            'loc_code'   => $locCode,
            'loc_whs_id' => $whs_id
        ])->first();
        if (!$loc) {
            return $this->response->errorBadRequest("Location " . $locCode . " is not existed");
        }


        /*
        * Check $loc status = IN, LK ko cho phep tiep tuc pick
        */
        if ($loc->loc_sts_code == 'IA') {
            return $this->response->errorBadRequest("Location " . $locCode . " is inactive");
        }
//        if ($loc->loc_sts_code == 'LK') {
//            return $this->response->errorBadRequest("Location " . $locCode . " is locked");
//        }


        //validate qty
        if ($wvDtl['piece_qty'] < $actQty) {
            $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s picked Qty QTY %d is greater than Allocated Qty %d',
                $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['lot'], $wvDtl['piece_qty'], $actQty);

            return $this->response->errorBadRequest($msg);
        }

        //get algorithm
        $algorithm = $this->customerConfigModel->getPickingAlgorithm($wvDtl->whs_id, $wvDtl->cus_id);

        $qty = $this->cartonModel->getAvailableQuantity($wvDtl['item_id'], $wvDtl['lot'], $loc->loc_id,
            $loc->loc_whs_id, $lpnCarton);
        if ($qty < $actQty) {
            $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s available QTY %d is less than Picked Qty %d',
                $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['lot'], $qty, $actQty);
            throw new \Exception($msg);
        }

        $actLoc = [
            'picked_qty' => $actQty,
            'act_loc_id' => $loc->loc_id,
            'act_loc'    => $loc->loc_code,
            'loc_id'     => $loc->loc_id,
            'loc_code'   => $loc->loc_code,
            'avail_qty'  => $qty,
            'lpn_carton' => $lpnCarton
        ];

        $cartons = $this->cartonModel->getAllCartonsByWvDtl($wvDtl, $algorithm, [$loc->loc_id], [$actLoc]);

        if(! $cartons ){
            return $this->response->errorBadRequest('There is no carton to pick or duplicated submit');
        }

        try {

            $wvDtlSts = 'PK';
            if ($wvDtl->act_piece_qty + $actQty > $wvDtl->piece_qty) {
                $this->response->errorBadRequest('Pick qty greater than remaining qty of wavepick');
            }

            if ($wvDtl->act_piece_qty + $actQty == $wvDtl->piece_qty) {
                $wvDtlSts = 'PD';
            }
            $this->waveDtlModel->updateWaveDtl($wvDtlId, $actQty, $wvDtlSts);
            $data = $this->groupCartonsByLot($cartons);
            $odrCartons = $this->cartonModel->updateCartonByLocs($data['cartons']);

//            $this->updateInventoryByLot($actQty, $wvDtl);

            $this->wavePickDtlLocModel->updateWaveDtlLoc($actLoc, $wvDtl->wv_id, $wvDtlId);

            $data = $this->orderDtlModel->updateOdrDtlPickedQty($wvDtl, $odrCartons);

            $eventDatas = [
                'evt_code'   => 'WWP',
                'info'       => sprintf('GUN - %d cartons picked in location %s', ceil($actQty / $wvDtl['pack_size']),
                    $locCode),
                'owner'      => $wvDtl['wv_num'],
                'trans_num'  => $wvDtl['wv_num'],
                'created_at' => time(),
                'created_by' => JWTUtil::getPayloadValue('jti') ?: 1
            ];

            $this->eventTrackingModel->create($eventDatas);

            $this->palletModel->updatePalletCtnTtl([$loc->loc_id]);
            $this->palletModel->updateZeroPallet([$loc->loc_id]);
            $this->palletModel->removeLocDynZone($loc->loc_id);

            //update sts
            $autoPack = $this->orderHdrModel->updateOrderPickedByWv($wvDtl->wv_id);

            $this->waveHdrModel->updateWvPicking($wvDtl->wv_id);

            $this->waveHdrModel->updateWvComplete($wvDtl->wv_id);

            $this->locationModel->pickPalletUpdateActiveLocationByWvDtlId($whs_id, $wvDtl, $loc->loc_id);

            //Update previous location to wv detail

            $this->waveDtlModel->getModel()->where("wv_dtl_id", $wvDtlId)->update([
                "act_loc_id"        => $loc->loc_id,
                "act_loc"           =>$loc->loc_code
            ]);


//            $test = DB::table("odr_cartons")->where("item_id", $wvDtl->item_id)->get();
//            dd($test);
//            dd(0);
            DB::commit();
            dispatch(new AutoPackJob($wvDtl->wv_id, $request));

            return ['msg' => 'successful'];
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }


    }

    public function validateWvLoc($whs_id, $locCode)
    {
        //get location
        $loc = Location::where([
            'loc_code'   => $locCode,
            'loc_whs_id' => $whs_id
        ])->first();
        if (!$loc) {
            return $this->response->errorBadRequest("Location " . $locCode . " is not existed");
        }

        /*
        * Check $loc status = IN, LK ko cho phep tiep tuc pick
        */
        if ($loc->loc_sts_code == 'IA') {
            return $this->response->errorBadRequest("Location " . $locCode . " is inactive");
        }
        if ($loc->loc_sts_code == 'LK') {
            return $this->response->errorBadRequest("Location " . $locCode . " is locked");
        }

        return ['msg' => 'location is valid'];
    }

    private function groupCartonsByLot($locs)
    {
        $lots = [];
        $cartons = [];
        foreach ($locs as $carton) {
            if (!isset($lots[$carton['lot']])) {
                $lots[$carton['lot']] = 0;
            }
            $lots[$carton['lot']] += $carton['picked_qty'];
            $cartons[] = $carton;
        }

        return [
            'cartons' => $cartons,
            'lots'    => $lots
        ];
    }

    private function updateInventoryByLot($pickedQty, $cond)
    {
        $sqlPickQty = sprintf('`picked_qty` + %d', $pickedQty);
        $sqlAvailQty = sprintf('`allocated_qty` - %d', $pickedQty);
        $res = DB::table('invt_smr')
            ->where([
                'whs_id'  => $cond['whs_id'],
                'item_id' => $cond['item_id'],
                'lot'     => $cond['lot']
            ])
            ->update([
                    'picked_qty'    => DB::raw($sqlPickQty),
                    'allocated_qty' => DB::raw($sqlAvailQty),
                ]
            );

        return $res;
    }

    public function moreSuggestLocation(Request $request)
    {
        $input = $request->getQueryParams();
        try {

            if (!isset($input['wv_dtl_id'])) {
                $this->response->errorBadRequest('Wave detail cannot empty');
            }
            $wvDtl = WavepickDtl::where('wv_dtl_id', $input['wv_dtl_id'])->first();
            if (empty($wvDtl)) {
                $this->response->errorBadRequest('Wave detail is not existed');
            }

            if (!in_array($wvDtl->wv_dtl_sts, ['NW', 'PK'])) {
                $this->response->errorBadRequest('Wave detail is picked');
            }

            $pickFullPallet = array_get($input, 'full_pallet', false);

            $suggestLocation = $this->cartonModel->getMoreSugLocByWvDtl($wvDtl, $pickFullPallet);
            if (count($suggestLocation) > 0) {
                $suggestLocation = $this->cartonModel->sortDeepLocationPicking($suggestLocation);
            }

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function checkWvLocation(Request $request)
    {
        $input = $request->getParsedBody();

        $locCode = $input['loc_code'] ?: null;
        $wvDtlId = $input['wv_dtl_id'] ?: null;

        if (!$locCode || !$wvDtlId) {
            return $this->response->errorBadRequest('Location code or wave detail id is empty');
        }

        $wvDtl = WavepickDtl::where('wv_dtl_id', $input['wv_dtl_id'])->first();
        if (empty($wvDtl)) {
            $this->response->errorBadRequest('Wave detail is not existed');
        }
        if (!in_array($wvDtl->wv_dtl_sts, ['NW', 'PK'])) {
            $this->response->errorBadRequest('Wave detail is picked');
        }
        //validate customer zone
        $cusZones = DB::table('customer_zone')
            ->where('cus_id', $wvDtl->cus_id)
            ->pluck('zone_id');

        if (empty($cusZones)) {
            return $this->response->errorBadRequest('Customer has not location zone');
        }
        if ($this->isPalletCode($locCode)) {
            //validate location
            $pallet = $this->palletModel->getFirstWhere(['rfid' => $locCode, 'whs_id' => Data::getCurrentWhsId()]);
            if (empty($pallet)) {
                return $this->response->errorBadRequest("Pallet '$locCode' is not existed");
            }
            $pallocId = object_get($pallet, 'loc_id');
            if ($pallocId) {
                //validate location
                $locPal = Location::where('loc_id', $pallocId)
                    ->where('loc_whs_id', Data::getCurrentWhsId())
                    ->first();
                $hasCtAC = $this->cartonModel->checkWhere(['cus_id' => $wvDtl->cus_id, 'whs_id' => $wvDtl->whs_id, 'ctn_sts' => 'AC']);

                if ($locPal->loc_sts_code != 'AC' && $hasCtAC == 0) {
                    return $this->response->errorBadRequest("Location of pallet '$locCode' is not active");
                }
            }
            $rs = $this->cartonModel->checkPalletByWvDtl($pallet->plt_id, $wvDtl->item_id, $wvDtl->lot);
            if (empty($rs)) {
                return $this->response->errorBadRequest("Pallet '$locCode' has not cartons to wavepick");
            }

            return ['data' => $rs];

        } else {
            //validate location
            $locObj = Location::where('loc_code', $locCode)
                ->where('loc_whs_id', Data::getCurrentWhsId())
                ->first();
            if (empty($locObj)) {
                return $this->response->errorBadRequest("Location '$locCode' is not existed");
            }

            $hasCtAC = $this->cartonModel->checkWhere(['cus_id' => $wvDtl->cus_id, 'whs_id' => $wvDtl->whs_id, 'ctn_sts' => 'AC']);


            if ($locObj->loc_sts_code != 'AC' && $hasCtAC == 0) {
                return $this->response->errorBadRequest("Location is not active");
            }
            $rs = $this->cartonModel->checkLocByWvDtl($locObj->loc_id, $wvDtl->item_id, $wvDtl->lot);
            if (empty($rs['loc_id'])) {
                return $this->response->errorBadRequest("Location '$locCode' has not cartons to wavepick");
            }

            return ['data' => $rs];
        }

    }
}

