<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Controllers\AbstractController;
use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\UserModel;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;

class AuthenticationController extends AbstractController
{
    protected $service;

    public function __construct(IRequest $request)
    {
        $this->service = new AuthenticationService($request);
    }

    public function actionLogin(IRequest $request)
    {
        $result = $this->service->login($request);
        $content = \json_decode($this->convertResponse($result)->getContent(), true);
        $statusCode = $this->convertResponse($result)->getStatusCode();

        if (!empty($content['code']) && $content['code'] === 405) {

            $body = $request->getParsedBody();
            $userModel = new UserModel();

            //get user information by email
            $user = $userModel->getUserByUsername($body['username']);
            //create token
            $token = $userModel->makeResetPasswordToken($user);
            $result = array_merge($content, [
                    'token' => $token
                ]);
        }

        try {
            // WAP-522 - [System Check] User is logout when login the other place
            if ($statusCode == 200) {
               // push to wap-channel
               DB::setFetchMode(\PDO::FETCH_ASSOC);
               $input = $request->getParsedBody();
               $username = array_get($input, 'username');
               $userModel = new UserModel();
               $user = $userModel->getUserByUsername($username);

               Redis::publish(env('REDIS_CHANNEL_WMS'),
                   json_encode(array_merge($user->toArray(), [
                       'token' => array_get($content, 'data.token')
                   ])));
            }
        }catch (\Exception $exception) {
            $dataError = [
                'date'     => time(),
                'api_name' => "WAP - Cannot connect to Redis.",
                'error'    => $exception->getMessage(),
            ];

            DB::table('sys_bugs')->insert($dataError);
        }

        return $result;
    }

    public function actionLogout(IRequest $request)
    {
        $result = $this->service->logout($request);
        return $result;
    }
}
