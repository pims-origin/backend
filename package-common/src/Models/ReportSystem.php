<?php

namespace Seldat\Wms2\Models;

class ReportSystem extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_report';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_rpt_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sys',
        'url_logo',
        'name_logo'
    ];
}
