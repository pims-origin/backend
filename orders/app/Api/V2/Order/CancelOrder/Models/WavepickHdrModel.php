<?php

namespace App\Api\V2\Order\CancelOrder\Models;

use Seldat\Wms2\Models\WavepickHdr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

/**
 * Class WavepickHdrModel
 *
 * @package App\Api\V1\Models
 */
class WavepickHdrModel extends AbstractModel
{
    /**
     * WavepickHdrModel constructor.
     *
     * @param WavepickHdr|null $model
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $waveHdrId
     *
     * @return mixed
     */
    public function deleteWavepickHdr($waveHdrId)
    {
        return $this->model
            ->where('wv_id', $waveHdrId)
            ->delete();
    }

    /**
     * @param $wvIds
     *
     * @return mixed
     */
    public function getWavePick($wvIds)
    {
        $row = $this->model
            ->whereIn('wv_id', $wvIds)
            ->get();

        return $row;
    }


    /**
     * @param string $wvId
     *
     * @return mixed
     */
    public function updatePicked($wvId)
    {
        $sqlOdrPicked = "(SELECT COUNT(1) FROM odr_dtl WHERE odr_dtl.odr_id = odr_hdr.odr_id AND odr_dtl.itm_sts IN ('NW', 'PK') AND deleted =0) = 0";
        $sqlWvPickedStr = "(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty != wv_dtl.piece_qty AND deleted =0) = 0";
        return \DB::table('wv_hdr')
            ->join('odr_hdr', 'odr_hdr.wv_num', '=', 'wv_hdr.wv_num')
            ->where('wv_hdr.wv_id', $wvId)
            ->where('wv_hdr.deleted', 0)
            ->where('odr_hdr.deleted', 0)
            ->whereRaw(DB::Raw($sqlWvPickedStr))
            ->whereRaw(DB::Raw($sqlOdrPicked))
            ->update([
                'wv_hdr.wv_sts'     => Status::getByValue("Completed", "WAVEPICK-STATUS"),
                'wv_hdr.updated_at' => time(),
                'wv_hdr.updated_by' => Data::getCurrentUserId(),
            ]);
    }

    /**
     * @param string $wvId
     *
     * @return mixed
     */
    public function updateCanceled($wvId)
    {
        $sqlOdrCanceled = "(SELECT COUNT(1) FROM odr_hdr WHERE odr_hdr.wv_id = wv_hdr.wv_id AND deleted = 0) = 0";
        $sqlWvPickedStr = "(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty != 0 AND deleted = 0) = 0";
        return \DB::table('wv_hdr')
            ->where('wv_hdr.wv_id', $wvId)
            ->where('wv_hdr.deleted', 0)
            ->whereRaw(DB::Raw($sqlOdrCanceled))
            ->whereRaw(DB::Raw($sqlWvPickedStr))
            ->update([
                'wv_hdr.wv_sts'     => Status::getByValue("Canceled", "WAVEPICK-STATUS"),
                'wv_hdr.updated_at' => time(),
                'wv_hdr.updated_by' => Data::getCurrentUserId(),
            ]);
    }
}
