<?php

namespace Seldat\Wms2\Models;

class SkuSummary extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sku_summary';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
//    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = [
        'whs_code',
        'cus_code',
        'upc',
        'des',
        'sku',
        'size',
        'color',
        'pack',
        'uom',
        'in_hand_qty',
        'in_pick_qty',
        'total_qty',
        'created_at',
        'updated_at'
    ];
}
