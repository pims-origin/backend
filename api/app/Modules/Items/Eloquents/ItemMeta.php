<?php

namespace App\Modules\Items\Eloquents;

use Illuminate\Database\Eloquent\Model;

class ItemMeta extends Model
{
    protected $table = 'item_meta';

    protected $primaryKey = 'itm_id';

    public $timestamps = false;

    protected $fillable = [
        'qualifier',
        'value',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
        'deleted',
        'deleted_at'
    ];
}