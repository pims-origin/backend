<?php

namespace Seldat\Wms2\Http\Middleware;

use Closure;
use Dingo\Api\Routing\Router;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Namshi\JOSE\JWS;
use PhpParser\Node\Expr\Cast\Array_;
use Seldat\Wms2\Utils\Profiler;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Wms2\UserInfo\Data;

class Authorize
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var
     */
    protected $userInfo;

    /**
     * @var
     */
    protected $permissions;


    /**
     * Authorize constructor.
     */
    public function __construct(Router $router, Data $data)
    {
        $this->router = $router;
        $this->userInfo = $data->getUserInfo();
        Profiler::log('getUserInfo');
        $this->permissions = File::getRequire(__DIR__ . '/../../config/permissions.php');
        Profiler::log('getRequire permissions');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check user login another
        //$this->checkToken();

        $isSuperAdmin = array_get($this->userInfo, 'isSuperAdmin', false);

        // User is Admin, then bypass checking permission
        if ($isSuperAdmin) {
            return $next($request);
        }

        $userHasPermission = array_get($this->userInfo, 'permissions', null);
        $action = array_get($this->router->getCurrentRoute()->getAction(), 'action', null);

        if (!$action) {
            throw new AccessDeniedHttpException('Permission denied!');
        }

        $permissions = array_get($this->permissions, 'rolePermission', []);
        $whsIds = array_column(array_get($this->userInfo, 'user_warehouses', []), 'whs_id');
		
        if (!($roleName = array_get($permissions, $action, []))) {
            if (!in_array($action, $userHasPermission)) {
                throw new AccessDeniedHttpException('Permission denied!');
            } else {
                return $next($request);
            }
        }
        $requestWH = array_get($request->route()[2], 'warehouseId');
        if (str_contains($action, 'Warehouse')) {
            $whPermission = ['editWarehouse', 'createWarehouse', 'deleteWarehouse'];
            if ($action == 'viewWarehouse') {
                $this->checkUserPermission($roleName, $userHasPermission);
                return $next($request);
            }
            if (count(array_intersect($whPermission, $userHasPermission)) == 0) {
                throw new AccessDeniedHttpException('Permission denied!');
            }

            return $next($request);
        }
        // check user has permission access route with list permission
        if (!is_array($roleName[0])) {
            $roleName = [$roleName];
        }
        $this->checkUserPermission($roleName, $userHasPermission);

        //check user can handle request url warehouse
        if (!in_array($requestWH, $whsIds) && $requestWH > 0) {
            throw new AccessDeniedHttpException('Permission denied!');
        }

        $data = $request->all();
        // check user can handle data warehouse
        $this->checkUserAccessWarehouse($data, $whsIds);

        // check user can handle data customer
        $this->checkUserAccessCustomer($data);

        $response = $next($request);

        if (empty($response->getContent())) {
            return $response;
        }

        try {
            $content = \GuzzleHttp\json_decode($response->getContent(), true);
        } catch (\Exception $e) {
            return $response;
        }

        if (!isset($content['errors']['message']) || env('API_DEBUG')) {
            return $response;
        }

        if(str_contains( $content['errors']['message'], 'SQLSTATE')) {
            $content['errors']['message'] = 'Data error, please contact WMS support or email to wms_support@seldatinc.com!';
            $response->setContent(\GuzzleHttp\json_encode($content));
        }

        if(str_contains( $content['errors']['message'], 'Deadlock')) {
            $content['errors']['message'] = 'System is processing other request, please try later!';
            $response->setContent(\GuzzleHttp\json_encode($content));
        }

        return $response;
    }


    /**
     * @param array $data
     * @param $key
     * @param array $listAccess
     *
     * @return bool
     */

    function canHandle(array $data, $key, array $listAccess = [])
    {
        foreach ($data as $k => $val) {
            if ($k === $key && in_array($val, $listAccess) == false) {
                return 1;
            }
            if (is_array($val)) {
                return $this->canHandle($val, $key, $listAccess);
            }
        }

        return true;
    }

    private function checkUserAccessWarehouse($data, $whsIds)
    {
        if (!$this->canHandle($data, 'whs_id', $whsIds)) {
            throw new AccessDeniedHttpException('Invalid data. Has a warehouse you can\'t handle data');
        }
    }

    private function checkUserAccessCustomer($data)
    {
        // check user can handle data customer
        $cusIds = array_column(array_get($this->userInfo, 'user_customers', []), 'cus_id');
        if (!$this->canHandle($data, 'cus_id', $cusIds)) {
            throw new AccessDeniedHttpException('Invalid data. Has a customer you can\'t handle data');
        }
    }

    private function checkUserPermission($permissionNeed, $userPermission)
    {
        foreach ($permissionNeed as $needOne) {
            if (count(array_intersect($needOne, $userPermission)) == count($needOne)) {
                return true;
            }
        }

        throw new AccessDeniedHttpException('Permission denied!');
    }

    private function checkToken()
    {
        $token = JWTAuth::getToken();

        try {
            $jws = JWS::load($token);
            $lastLogin = $jws->getPayload()['iat'];
            if ($lastLogin != $this->userInfo['last_login']) {
                throw new AccessDeniedHttpException('User has already logged in from another browser/device.');
            }
        } catch (Exception $e) {
            throw new AccessDeniedHttpException('Could not decode token: '.$e->getMessage());
        }

    }
}
