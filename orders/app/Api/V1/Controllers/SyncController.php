<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\OrderHdrModel;
use App\Jobs\SyncAsnJob;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ServerRequestInterface as Request;

class SyncController extends AbstractController
{
    protected $odrHdrModel;

    public function __construct(OrderHdrModel $odrHdrModel)
    {
        $this->odrHdrModel = $odrHdrModel;
    }

    public function syncASN ($odrId, Request $request)
    {
        $input = $request->getQueryParams();

        try {
            if (empty($input['queue-whs'])) {
                throw new \Exception("queue-whs field is required.");
            }

            $odrHdr = $this->odrHdrModel->byId($odrId, ['details']);

            if (empty($odrHdr)) {
                throw new \Exception("Order (odr_id = {$odrId}) is not exists!");
            }

            dispatch(new SyncAsnJob($odrHdr, $input['queue-whs'], $request));
            
            Log::info("Sync ASN for order {$odrId} is in Queue");
            return ['msg' => "Sync ASN for order {$odrId} is in Queue"];
        } catch (\Exception $e) {
            Log::error('[sync ASN] '.$e->getMessage());
        }
    }
}