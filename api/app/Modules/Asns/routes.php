<?php

$middleware = ['app', 'user', 'cus', 'whs', 'timezone'];

$router->group([
        'prefix'     => '/rest/wms360/seldat/v1',
        'middleware' => $middleware,
        'namespace'  => 'App\Modules\Asns\Controllers'
    ], function () use ($router) {

    // Get list asn of customer
    $router->get('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/asns', [
        'uses' => 'AsnController@index'
    ]);

    // Get detail asn of customer
    $router->get('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/asns/{asnId:[0-9]+}', [
        'uses' => 'AsnController@read'
    ]);

    // Create asn
    $router->post('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/asns', [
        'uses' => 'AsnController@create'
    ]);

    // Update asn
    $router->put('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/asns/{asnHdrId:[0-9]+}', [
        'uses' => 'AsnController@update'
    ]);
});