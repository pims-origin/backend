<?php

namespace App\Api\V2\Inbound\Location\ListEmptyLocation\Controllers;

use App\Api\V2\Inbound\Location\ListEmptyLocation\Models\LocationModel;
use App\Api\V2\Inbound\Location\ListEmptyLocation\Models\PalletModel;
use App\Api\V2\Inbound\Location\ListEmptyLocation\Models\CustomerModel;
use App\libraries\RFIDValidate;

use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\SelArr;

class LocationController extends AbstractController
{
    const DEFAULT_LOT_PUTBACK = "NA";

    protected $locationModel;
    protected $palletModel;
    protected $customerModel;

    public function __construct()
    {
        $this->locationModel = new LocationModel();
        $this->palletModel   = new PalletModel();
        $this->customerModel = new CustomerModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function getEmptyLocations($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input = SelArr::removeNullOrEmptyString($input);
        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/location/rack/get-empty-by-customer";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'LEL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get List empty Location'
        ]);
        /*
         * end logs
         */


        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        $pltRfid    = array_get($input, 'plt_rfid', null);
        $cusId      = array_get($input, 'cus_id', null);
        $ctnPutback = array_get($input, 'ctn_ttl', 0);
//        $packSize   = array_get($input, 'pack_size', 0); // ignore pack_size
        $itemId     = array_get($input, 'item_id', 0);
        $isEcom     = array_get($input, 'is_ecom', 0);

        DB::setFetchMode(\PDO::FETCH_ASSOC);

        if ($pltRfid) {
            // check pallet in table pallet, vtl_ctn
            $pallet = $this->palletModel->getFirstWhere([
                'whs_id' => $whsId,
                'rfid'   => $pltRfid,
            ]);

            if ($pallet && $pallet->cus_id != $cusId) {
                $msg = sprintf("The Pallet Rfid %s doesn't belong to current customer.",
                    $pltRfid, $cusId);
                return $this->_responseErrorMessage($msg);
            }

            if ($pallet && $pallet->loc_code != null) {
                $msg = sprintf("The Pallet Rfid %s was already on location %s",
                    $pltRfid, $pallet->loc_code);
                return $this->_responseErrorMessage($msg);
            }
        }

        // Check customer
        $cusArray = DB::table('customer_warehouse')
            ->where('cus_id', $cusId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->first();
        if (!count($cusArray)) {
            $cusObj = $this->customerModel->getFirstWhere(['cus_id' => $cusId]);
            if (!$cusObj) {
                $msg = sprintf("The customer Id %d doesn't exist.", $cusId);
            } else {
                $msg = sprintf("The customer %s doesn't belong to current warehouse.", $cusObj->cus_name);
            }
            return $this->_responseErrorMessage($msg);
        }

        try {
            $limit = 6;
            // WAP-584 - Create API get suggest location list by Pallet tag
            if ($pltRfid) {
                //o	Suggest empty locations if WAP send cartons within a pallet
                if($isEcom == 1){
                    $locType = 'ECO';
                }
                else{
                    $locType = 'RAC';
                }
                $locations = $this->locationModel->getMoreEmptyLocationByCusId($whsId, $cusId, $locType, $limit);

                if (!count($locations)) {
                    $msg = sprintf("Current location is not defined in WMS system. Please contact Administrator");
                    return $this->_responseErrorMessage($msg);
                }
            } else {
                //o	Suggest approximately locations if WAP send cartons without a pallet
                $data = [
                    'whs_id'    => $whsId,
                    'cus_id'    => $cusId,
                    'item_id'   => $itemId,
                    'is_picked' => 1,
//                    'lot'       => self::DEFAULT_LOT_PUTBACK,
//                    'pack_size' => $packSize,
//                    'ctn_ttl'   => $ctnPutback,
                ];
                //o	Suggest locations at the level 1 and less cartons and the same item id
                /*
                 * - Cartons is belong to warehouse, customer
                 * - Is the same SKU
                 * - Have picked carton
                 * - Low level
                 */
                $locations = $this->locationModel->getMoreLocationCanPutCarton($data, $limit, $isEcom);
                $countLocs = count($locations);
                if ($countLocs < $limit) {
                    $limit -= $countLocs;
                    $data = [
                        'whs_id'    => $whsId,
                        'cus_id'    => $cusId,
                        'item_id'   => $itemId,
                        'is_picked' => 0,
                    ];
                    //o	Suggest locations at the level 1 and less cartons
                    /*
                     * - Cartons is belong to warehouse, customer
                     * - Is the same SKU
                     * - Low level
                     */
                    $locations2 = $this->locationModel->getMoreLocationCanPutCarton($data, $limit, $isEcom);
                    $countLocs2 = count($locations2);
                    if ($countLocs2 && $countLocs) {
                        $locations = array_merge($locations, $locations2);
                    } elseif ($countLocs2) {
                        $locations = $locations2;
                    }
                    $countLocs = count($locations);
                    if ($countLocs < $limit) {
                        $limit -= $countLocs;
                        $data = [
                            'whs_id'    => $whsId,
                            'cus_id'    => $cusId,
                            'item_id'   => 0,
                            'is_picked' => 0,
                        ];
                        /*
                         * - Cartons is belong to warehouse, customer
                         * - Low level
                         */
                        $locations3 = $this->locationModel->getMoreLocationCanPutCarton($data, $limit, $isEcom);
                        $countLocs3 = count($locations3);
                        if ($countLocs3 && count($locations)) {
                            $locations = array_merge($locations, $locations3);
                        } elseif ($countLocs3) {
                            $locations = $locations3;
                        }
                        $countLocs = count($locations);
                        if (!$countLocs) {
                            $msg = sprintf("Current location is not defined in WMS system. Please scan a Pallet");
                            return $this->_responseErrorMessage($msg);
                        }
                    }
                }
            }

            $locationFirst = array_shift($locations);
            $data = [
                "cus_id"               => $cusId,
                "loc_sts_code"         => array_get($locationFirst, 'loc_sts_code'),
                "loc_alternative_name" => array_get($locationFirst, 'loc_alternative_name'),
                "loc_id"               => array_get($locationFirst, 'loc_id'),
                "loc_rfid"             => array_get($locationFirst, 'rfid'),
                "suggested"            => $locations,
            ];

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data']   = [$data];

            return $msg;

        } catch (\Exception $e) {

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _validateParams($attributes, $whsId)
    {
        $pltRfid  = array_get($attributes, 'plt_rfid');
        $cusId    = array_get($attributes, 'cus_id');
        $ctnTtl   = array_get($attributes, 'ctn_ttl');
//        $packSize = array_get($attributes, 'pack_size');
        $itemId   = array_get($attributes, 'item_id');

        $names = [
            'plt_rfid'    => 'Pallet Rfid',
//            'pack_size'   => 'Pack size',
            'ctn_ttl'     => 'Number of Cartons',
            'item_id'     => 'Item Id',
            'cus_id'      => 'Customer Id',
        ];

        // Check Required
        $requireds = [
//            'plt_rfid' => $pltRfid,
//            'pack_size' => $packSize,
            'item_id'   => $itemId,
            'cus_id'    => $cusId,
            'ctn_ttl'   => $ctnTtl,
        ];

        // Check int type and greater than 0
        $intGreaterThan0 = [
            'cus_id'    => $cusId,
            'item_id'   => $itemId,
            'ctn_ttl'   => $ctnTtl,
//            'pack_size' => $packSize,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet rfid is string
        if ($pltRfid && !is_string($pltRfid)) {
            $msg = $names['plt_rfid'] . " must be string type";
            return $this->_responseErrorMessage($msg);
        }

        foreach ($intGreaterThan0 as $key => $field) {
            if (!is_int($field) || $field < 1) {
                $errorDetail = "{$names[$key]} must be integer type and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        //validate pallet RFID
        if ($pltRfid) {
            $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
            if (!$pltRFIDValid->validate()) {
                return $this->_responseErrorMessage($pltRFIDValid->error);
            }
        }

    }
}