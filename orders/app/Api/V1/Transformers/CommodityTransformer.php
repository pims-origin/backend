<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 27-Sep-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Commodity;

class CommodityTransformer extends TransformerAbstract
{
    public function transform(Commodity $commondityInfo)
    {
        return [
            'cmd_id'         => $commondityInfo->cmd_id,
            'cmd_name'       => $commondityInfo->cmd_name,
            'cmd_des'        => $commondityInfo->cmd_des,
            'commodity_nmfc' => $commondityInfo->commodity_nmfc,
            'cmd_cls'        => $commondityInfo->cmd_cls,
        ];
    }
}
