<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\AsnDtlModel;
use Dingo\Api\Routing\Helpers;
use App\Api\V1\Transformers\AsnListsCanSortV1Transformer;

class TestPutPalletToRACKWithoutCompleteSKU extends TestCase
{

    use TraitTest;
    //use DatabaseTransactions;
    use Helpers;

    private $whsId = 99;
    private $cusId = 57;
    private $palletId = 1;
    private $DB_ROLLBACK = true;
    public static $_caseCreateVTCarton = [];
    private $_token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0ODkzNjk0NDYsImV4cCI6MTQ4OTQ1NTg0NiwibmFtZSI6IkRhbyBUcmFuIiwidXNlcm5hbWUiOiJjc3JkdHIiLCJqdGkiOjc3fQ.gvcMhnHsOLzrVsOkzv2vtI1zsXvOnnnSjsaLqfMcjLI";

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Test Scan pallet without complete SKU
     * 
     * Step to test:
     * - Step 1: /whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/add-virtual-cartons
     * - Step 2: /whs/{$this->whsId}/scan-pallet/
     * - Step 3: /whs/{$this->whsId}/location/rack/put-pallet
     * 
     * Verify: Status = true
     */
    public function testPutPalletToRACKWithoutCompleteSKU_Case()
    {
        /*
         * Prepare data to test
         */
        echo "\n[STAR] " . __METHOD__ . "\n";
        $vtlCtnOnPallet = [];
        for ($i = 0; $i < 4; $i++) {
            $vtlCtnOnPallet[] = strtoupper(uniqid("AA"));
        }
        $pltRfid = strtoupper(uniqid("FF"));

        /*
         * Steps to test
         */
        $this->step1_scanCartons($vtlCtnOnPallet);
        $this->step2_scanPallet($this->whsId, $vtlCtnOnPallet, $pltRfid);
        $result = $this->step3_putAway($pltRfid);
        $content = $result->content();
        $jsonObj = json_decode($content);
        /*
         * Put away
         */
        //Find location with rfid, whs_id, sts: RAC, loc_sts: AC
        echo "[CONT] 3.{$content}\n";
        if (isset($jsonObj->Status)) {
            $this->assertEquals("OK", $jsonObj->Status, "[FAIL] Status not true\n");
        } else {
            $this->fail("[FAIL] Unknown structure\n");
        }

        echo "\n[DONE] " . __METHOD__ . "\n";
    }

    private function step1_scanCartons($vtlCtnOnPallet)
    {
        $asnObj = $this->getAsnInfo();
        if (!$asnObj) {
            $this->markTestIncomplete("[INCO] No ASN data to test!\n");
        }
        $tran = new AsnListsCanSortV1Transformer();
        $data = $tran->transform($asnObj);

        $response = $this->APIScanCartons($this->whsId, $data, $vtlCtnOnPallet);

        $content = $response->content();
        $jsonObj = json_decode($content);
        echo "[CONT] 1.{$content}\n";
        if (!isset($jsonObj->data) || !isset($jsonObj->data->status) || $jsonObj->data->status != 1) {
            $this->fail("[FAIL] Insert carton unsuccessfully!\n");
        }
        $this->assertTrue($jsonObj->data->status, "[FAIL] Scan cartons fail\n");
    }

    private function step2_scanPallet($whsId, $vtlCtnOnPallet, $pltRfid)
    {
        $param = [
            'pallet.ctn-rfid' => $vtlCtnOnPallet,
            'pallet.pallet-rfid' => $pltRfid
        ];
        $res = $this->APIScanPallet($whsId, $param);
        $content = $res->content();
        $jsonObj = json_decode($content);
        echo "[CONT] 2.{$content}\n";
        if (isset($jsonObj->{'gr_status'})) {
            $this->assertFalse($jsonObj->{'gr_status'}, "[FAIL] Good Receipt exist when not complete ASN!\n");
        } else {
            echo "[INCO] Not found gr_status\n";
            $this->markTestIncomplete();
        }
    }

    private function step3_putAway($pltRfid)
    {
        $qBuilder = \Seldat\Wms2\Models\Location::select(['location.*', 'pallet.plt_id'])
                ->leftJoin('pallet', 'pallet.loc_id', '=', 'location.loc_id')
                ->where([
                    'loc_whs_id' => $this->whsId,
                    'loc_type_id' => 3, //RAC
                    'loc_sts_code' => 'AC'
                ])
                ->where(DB::raw("SUBSTR(location.rfid, 1, 2)"),'DD')
                ->whereNull('pallet.plt_id')
                ->whereNotNull('location.rfid')
                ;
//        $mySql = str_replace(array('%', '?'), array('%%', '%s'), $qBuilder->toSql());
//        $myQuery = vsprintf($mySql, $qBuilder->getBindings());
//        echo $myQuery;
        $location = $qBuilder->first();
        if (!$location) {
            echo "[INCO] No location to test\n";
            $this->markTestIncomplete();
        }
        return $this->APIPutAway($this->whsId, $pltRfid, $location->rfid);
    }

}
