<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\AsnStatus;


class AsnStatusModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnStatus();
    }

    public function search($attributes = [])
    {
        $query  = $this->make([]);
        $this->sortBuilder($query,$attributes);
        return $query->get();
    }


}
