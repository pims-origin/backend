<?php

namespace App\Api\V1\Transformers;


use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\ContainerModel;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnHdr;

class AsnListsCanSortTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(AsnHdr $asnHdr)
    {
        $expDate = object_get($asnHdr, 'asn_hdr_ept_dt', '');
        $expDate = ($expDate) ? date('m/d/Y', $expDate) : '';

        $user = '';
        if (object_get($asnHdr, 'createdUser')) {
            $user = object_get($asnHdr, 'createdUser.first_name', '')
                . ' ' .
                object_get($asnHdr, 'createdUser.last_name', '');
        }

        $asnDetail = new AsnDtlModel();
        $data = $asnHdr->asnDtl()->first();
        $container_id = (!empty($data->ctnr_id))?$data->ctnr_id:$asnHdr->gr_hdr_ctnr_id;
        $container_num = (!empty($data->ctnr_num))?$data->ctnr_num:$asnHdr->gr_hdr_ctnr_num;
        //$xDock = AsnDtl::where('asn_hdr_id', object_get($data, 'asn_hdr_id'))->where('ctnr_id', object_get($data, 'ctnr_id'))->sum('asn_dtl_crs_doc');
        //$asnInfo = $asnDetail->getTotalInfoFromAsnDtlByHeader($asnHdr->asn_hdr_id);

        $containers = $asnDetail->getContainersByAsn($asnHdr->asn_hdr_id);
        if(count($containers) == 0) {
            $container_model = new ContainerModel();
            $ctnr = $container_model->getFirstWhere(['ctnr_id' => $container_id]);
            $containers[] = $ctnr;
        }
        //$isDecrepancy = $asnDetail->getAsnIsDiscrepancy($asnHdr->asn_hdr_id);
        //$isDamaged = $asnDetail->getAsnIsDamaged($asnHdr->asn_hdr_id);
        //$isWap= GoodsReceipt::where('asn_hdr_id', $asnHdr->asn_hdr_id)->where('created_from', 'WAP')->count();
        return [
            'asn_hdr_id'      => $asnHdr->asn_hdr_id,
            'asn_hdr_num'     => $asnHdr->asn_hdr_num,
            'asn_hdr_ref'     => $asnHdr->asn_hdr_ref,
            'cus_name'        => object_get($asnHdr , 'customer.cus_name', null),
            'cus_code'        => object_get($asnHdr , 'customer.cus_code', null),
            'ctnr_id'         => $container_id,
            'ctnr_num'        => $container_num,
            "dtl_crs_doc"     => null,//$xDock,
            'asn_hdr_ept_dt'  => $expDate,
            'dtl_po'          => object_get($data, 'asn_dtl_po', ''),
            'dtl_po_date'     => ( object_get($data, 'asn_dtl_po_dt')) ? date("m/d/Y", $data->asn_dtl_po_dt) : "",
            'asn_dtl_pack'    => object_get($data, 'asn_dtl_pack', 0),
            'asn_dtl_cus_upc' => object_get($data, 'asn_dtl_cus_upc', ''),
            'asn_dtl_lot'     => object_get($data, 'asn_dtl_lot', ''),
            'asn_sts'         => $asnHdr->asn_sts,
            'asn_sts_name'    => object_get($asnHdr, 'asnStatus.asn_sts_name', ''),
            'asn_sts_des'     => object_get($asnHdr, 'asnStatus.asn_sts_des', ''),
            'user'            => $user,
            'containers'      => (!empty($containers))?$containers:null,
            'is_discrepancy'  => null,//$isDecrepancy > 0 ? 1 : 0,
            'is_damaged'      => null,//$isDamaged > 0 ? 1 : 0,
            "ctnr_ttl"        => null,//array_get($asnInfo, 'ctnr_ttl', 0),
            "item_ttl"        => null,//array_get($asnInfo, 'itm_ttl', 0),
            "ctn_ttl"         => null,//intval(array_get($asnInfo, 'ctn_ttl', 0)),
            'create_from_wap' => null,//$isWap > 0 ? 1: 0,
            'asn_hdr_act_dt'  =>  !empty($asnHdr->asn_hdr_act_dt) ? date('m/d/Y', $asnHdr->asn_hdr_act_dt) : null,
            'asn_type'        => $asnHdr->asn_type,
            'asn_date' =>  !empty($asnHdr->created_at) ? $asnHdr->created_at->format('m/d/Y') : null,
            'gr_hdr_created_at' =>  !empty($asnHdr->gr_hdr_created_at) ? date('m/d/Y', $asnHdr->gr_hdr_created_at) : null,
            'gr_hdr_act_dt' =>!empty($asnHdr->gr_hdr_act_dt) ? date('m/d/Y', $asnHdr->gr_hdr_act_dt) : null,
        ];
    }
}
