<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerWarehouse;
use League\Fractal\TransformerAbstract;

class CustomerWarehouseDefaultTransformer extends TransformerAbstract
{
    public function transform(CustomerWarehouse $customerWarehouse)
    {
        $firstName = object_get($customerWarehouse, 'customer.customerDefaultContact.cus_ctt_fname', null);
        $lastName = object_get($customerWarehouse, 'customer.customerDefaultContact.cus_ctt_lname', null);
        $customerName = $firstName . ' ' . $lastName;

        return [
            'cus_id'              => $customerWarehouse->cus_id,
            'whs_id'              => $customerWarehouse->whs_id,
            'whs_code'            => object_get($customerWarehouse, 'warehouse.whs_code', null),
            'cus_code'            => object_get($customerWarehouse, 'customer.cus_code', null),
            'cus_billing_account' => object_get($customerWarehouse, 'customer.cus_billing_account', null),
            'cus_name'            => object_get($customerWarehouse, 'customer.cus_name', null),
            'cus_city_name'       => !empty($customerWarehouse->customer->customerAddress->cus_add_city_name)
                ? $customerWarehouse->customer->customerAddress->cus_add_city_name
                : null,
            'cus_state_name'      => !empty($customerWarehouse->customer->customerAddress->systemState->sys_state_name)
                ? $customerWarehouse->customer->customerAddress->systemState->sys_state_name
                : null,
            'cus_postal_code'     => !empty($customerWarehouse->customer->customerAddress->cus_add_postal_code)
                ? $customerWarehouse->customer->customerAddress->cus_add_postal_code
                : null,
            'cus_ctt_full_name'   => $customerName,
            'cus_ctt_email'       => object_get($customerWarehouse, 'customer.customerDefaultContact.cus_ctt_email',
                null),
            'cus_ctt_phone'       => object_get($customerWarehouse, 'customer.customerDefaultContact.cus_ctt_phone',
                null),
            'status'              => object_get($customerWarehouse, 'customer.customerStatus', null),
        ];
    }
}