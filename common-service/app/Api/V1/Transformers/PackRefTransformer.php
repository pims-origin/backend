<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\PackRef;
use League\Fractal\TransformerAbstract;

class PackRefTransformer extends TransformerAbstract
{
    public function transform(PackRef $packRef)
    {
        return [
            'pack_ref_id' => $packRef->pack_ref_id,
            'width'       => (float)$packRef->width,
            'height'      => (float)$packRef->height,
            'length'      => (float)$packRef->length,
            'dimension'   => $packRef->dimension,
            'pack_type'   => $packRef->pack_type,
            'pack_name'   => object_get($packRef, 'packType.pack_name', ''),
        ];
    }
}
