<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerMeta;
use League\Fractal\TransformerAbstract;

class CustomerMetaTransformer extends TransformerAbstract
{
    public function transform(CustomerMeta $customerMeta)
    {
        if(isset($customerMeta->val_json_decode)) {
            return $customerMeta->val_json_decode;
        } else {
            return [
                'cus_id' => $customerMeta->cus_id,
                'qualifier' => $customerMeta->qualifier,
                'value' => $customerMeta->value
            ];
        }
    }
}