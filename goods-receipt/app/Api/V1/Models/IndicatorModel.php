<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Indicator;
use Seldat\Wms2\Utils\SelArr;

class IndicatorModel extends AbstractModel
{
    /**
     * @param Indicator $model
     */
    public function __construct(Indicator $model = null)
    {
        $this->model = ($model) ?: new Indicator();
    }

    /**
     * @param $attributes
     *
     * @return mixed
     */
    public function indicatorDropDown($attributes)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query = $this->model
            ->select([
                'ind_group', 'evt_code'
            ]);
        $query->groupBy('ind_group');

        $row = $query->get();

        return $row;
    }

}
