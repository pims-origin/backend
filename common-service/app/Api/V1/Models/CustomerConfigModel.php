<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\CustomerConfig;
use Seldat\Wms2\Utils\SelArr;

class CustomerConfigModel extends AbstractModel
{
    public function __construct(CustomerConfig $model = null)
    {
        $this->model = ($model) ?: new CustomerConfig();
    }

    /*
    ****************************************************************************
    */

    public function deleteCustomerConfigWarehouse($warehouseId, $customerId)
    {
        return $this->model
            ->where('whs_id', $warehouseId)
            ->where('cus_id', $customerId)
            ->whereIn('config_value', [997, 810, 846, 858, 945, 940, 861, 856, 888])
            ->delete();
    }

    /**
     * @param $warehouseId
     * @param $customerId
     * @param $ediTrans
     *
     * @return mixed
     */
    public function getCusConfigByIds($warehouseId, $customerId, $ediTrans = false)
    {
        $rows = $this->model
            ->where('whs_id', $warehouseId)
            ->where('cus_id', $customerId)
            ->whereIn('config_value', $ediTrans)
            ->get();

        return $rows;
    }

    /**
     * @param $warehouseId
     * @param $customerId
     * @param $configValue
     *
     * @return mixed
     */
    public function getCusConfigId($warehouseId, $customerId, $configValue)
    {
        return $cusConfigId = $this->model
            ->where('whs_id', $warehouseId)
            ->where('cus_id', $customerId)
            ->where('config_value', $configValue)
            ->first(['id']);

    }


}
