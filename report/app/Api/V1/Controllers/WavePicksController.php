<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\WavePicksDtlModel;
use App\Api\V1\Transformers\WavePicksTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class WavePicksController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new WavePicksDtlModel();
        $this->transformer = new WavePicksTransformer();
    }

    /*
    ****************************************************************************
    */

}
