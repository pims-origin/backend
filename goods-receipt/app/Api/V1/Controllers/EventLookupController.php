<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 01-July-16
 * Time: 10:00
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EventLookupModel;
use App\Api\V1\Transformers\EventLookupTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class EventLookupController extends AbstractController
{
    /**
     * @var EventLookupModel
     */
    protected $eventLookupModel;


    /**
     * @param EventLookupModel $eventLookupModel
     */
    public function __construct(EventLookupModel $eventLookupModel)
    {
        $this->eventLookupModel = $eventLookupModel;

    }

    /**
     * @param Request $request
     * @param EventLookupTransformer $eventLookupTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request, EventLookupTransformer $eventLookupTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $eventLookup = $this->eventLookupModel->search($input, [

            ], array_get($input, 'limit'));

            return $this->response->paginator($eventLookup, $eventLookupTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
