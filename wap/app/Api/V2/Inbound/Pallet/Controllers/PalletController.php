<?php

namespace App\Api\V2\Inbound\Pallet\Controllers;

use App\Api\V2\Inbound\Models\PalletModel;
use App\Api\V2\Inbound\Models\CartonModel;
use App\Api\V2\Inbound\Models\CustomerModel;
use App\Api\V2\Inbound\Pallet\Validators\PalletValidator;
use App\libraries\RFIDValidate;
use Dingo\Api\Http\Response as Response;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use App\Api\V2\Inbound\Models\Log;
use Psr\Http\Message\ServerRequestInterface as Request;

class PalletController extends AbstractController
{
    /**
     * @var $palletModel
     */
    protected $palletModel;

    /**
     * @var $cartonModel
     */
    protected $cartonModel;

    /**
     * @var $palletValidator
     */
    protected $palletValidator;

    /**
     * @var $customerModel
     */
    protected $customerModel;

    public function __construct()
    {
        $this->palletModel = new PalletModel();
        $this->cartonModel = new CartonModel();
        $this->customerModel = new CustomerModel();
        $this->palletValidator = new PalletValidator();
    }

    public function getInfoPallet($whsId, $rfid, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['rfid'] = $rfid;

        //valid input
        $this->palletValidator->validate($input);

        $rfidValidate = new RFIDValidate($rfid, 'pallet', $whsId);
        if (! $rfidValidate->validate()) {
            return [
                'status' => false,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => -1,
                        'msg' => $rfidValidate->error
                    ]
                ],
                'data' => []
            ];
        }

        $dataCarton = [];
        $dataSku = [];

        try {
            $url = "/whs/{$whsId}/inbound/pallet/{$rfid}";
            Log:: info($request, $whsId, [
                'evt_code'     => 'ACC',
                'owner'        => '',
                'transaction'  => '',
                'url_endpoint' => $url,
                'message'      => 'Get Pallet Information By Pallet rfid'
            ]);

            $itemPallet = $this->palletModel->getFirstWhere([
                'whs_id'  => $whsId,
                'rfid'    => $rfid
            ]);

            if ($itemPallet) {
                $listCarton = $this->cartonModel->findWhere([
                    'plt_id' => $itemPallet->plt_id,
                    'whs_id' => $whsId
                ]);
                foreach ($listCarton as $carton) {
                    $dataCarton[] = [
                        'rfid'    => $carton->rfid,
                        'ctn_num' => $carton->ctn_num,
                        'ctn_sts' => $carton->ctn_sts,
                        'sku'     => $carton->sku
                    ];
                }

                $itemCustomer = $this->customerModel->getFirstBy('cus_id', $itemPallet->cus_id);
                $dataSku = array_unique(array_pluck($listCarton, 'sku'));

                return [
                    'status' => true,
                    'iat'    => time(),
                    'messages' => [
                        [
                            'status_code' => 1,
                            'msg'         => 'Successfully!'
                        ]
                    ],
                    'data' => [
                        [
                            'customer' => [
                                'cus_name' => $itemCustomer->cus_name,
                                'cus_id'   => $itemCustomer->cus_id
                            ],
                            'pallet' => [
                                'rfid'     => $itemPallet->rfid,
                                'plt_num'  => $itemPallet->plt_num,
                                'loc_name' => $itemPallet->loc_name,
                                'plt_sts' => $itemPallet->plt_sts
                            ],
                            'skus'    => $dataSku,
                            'cartons' => $dataCarton
                        ]
                    ]
                ];
            } else {
                return [
                    'status' => true,
                    'iat'    => time(),
                    'messages' => [
                        [
                            'status_code' => 1,
                            'msg'         => 'OK!'
                        ]
                    ],
                    'data' => []
                ];
            }
        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => "/whs/{$whsId}/inbound/pallet/{$rfid}",
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }
}
