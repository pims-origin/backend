<?php

namespace Seldat\Wms2\Models;

class ReceivingReport extends BaseSoftModel
{
    protected $table = 'gr_hdr';
    
    protected $primaryKey = 'gr_hdr_id';
    
    protected $fillable = [
        'ctnr_id',
        'asn_hdr_id',
        'gr_hdr_seq',
        'gr_hdr_ept_dt',
        'gr_hdr_num',
        'whs_id',
        'cus_id',
        'gr_in_note',
        'gr_ex_note',
        'gr_sts',
        'putter',
        'ctnr_num',
        'ref_code',
        'ctnr_num',
    ];
    
    public function AsnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }
    
    public function Customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
    
    public function Warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }
    
    public function AsnHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnHdr', 'asn_hdr_id', 'asn_hdr_id');
    }
    
    public function SystemMeasurement()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemMeasurement', 'sys_mea_code', 'sys_mea_code');
    }
    
    public function User()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'user_id', 'user_id');
    }
    
    public function GoodsReceiptDetail()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptDetail', 'gr_dtl_id', 'gr_dtl_id');
    }
    
    public function GoodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'gr_hdr_id', 'gr_hdr_id');
    }
    
    public function Users()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'user_id', 'user_id');
    }
}
