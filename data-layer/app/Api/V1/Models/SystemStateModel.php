<?php

namespace App\Api\V1\Models;

use App\SystemState;
use App\Utils\SelStr;
use App\Utils\SelArr;

class SystemStateModel extends AbstractModel
{
    public function __construct(SystemState $model = null)
    {
        $this->model = ($model) ?: new SystemState();
    }

    public function deleteSystemState($systemStateId)
    {
        return $this->model
            ->where('sys_state_id', $systemStateId)
            ->delete();
    }

    public function deleteMassSystemState(array $systemStateIds)
    {
        return $this->model
            ->whereIn('sys_state_id', $systemStateIds)
            ->delete();
    }

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'sys_state_name' || $key === 'sys_state_code' || $key === 'sys_state_desc') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                } else if ($key === 'sys_state_id' || $key === 'sys_state_country_id') {
                    $query->where($key, $value);
                }
            }
        }

        //Implement sort builder for api
        //usage: ?sort[name]=desc&sort[title]=asc
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    public function getStateInCountryByCode($code, $countryId)
    {
        return $this->model
            ->where('sys_state_country_id', $countryId)
            ->where('sys_state_code', $code)
            ->first();
    }

    public function getStateInCountryByName($name, $countryId)
    {
        return $this->model
            ->where('sys_state_country_id', $countryId)
            ->where('sys_state_name', $name)
            ->first();
    }
}
