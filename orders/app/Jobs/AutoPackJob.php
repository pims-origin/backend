<?php

namespace App\Jobs;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Models\OrderHdrModel;
use DB;
use GuzzleHttp\Client;

class AutoPackJob extends Job
{

    protected $wvId;
    protected $request;
    /**
     * AutoPackJob constructor.
     */
    public function __construct($wvId, $request)
    {
        $this->wvId = $wvId;
        $this->request = $request;
    }

    public function handle()
    {


        $orderHdrModel = new OrderHdrModel();
        $orderHdrMetaModel = new OrderHdrMetaModel();
        $orderDtlModel = new OrderDtlModel();
        $orders = $orderHdrModel->findWhere(['wv_id' => $this->wvId, 'odr_sts' => 'PD']);

        try{

            foreach ($orders as $order) {
                $odrId = object_get($order, 'odr_id');
                $orderFlow = $orderHdrMetaModel->getOrderFlow($odrId);
                $packingFLow = $orderHdrMetaModel->getFlow($orderFlow, 'APO');

                if (array_get($packingFLow, 'usage', -1) == 1) {
                    $client = new Client();
                    $odrDtls = $orderDtlModel->findWhere(['odr_id' => $odrId , 'deleted' => 0]);
                    $data = [];
                    foreach ($odrDtls as  $odrDtl) {
                        $data[] = [
                            'odr_dtl_id' => object_get($odrDtl, 'odr_dtl_id'),
                            'itm_id' => object_get($odrDtl, 'item_id')
                        ];

                    }
                    $client->request('POST', env('API_PACKING') . '/v1/packs/auto/' . $odrId,
                        [
                            'headers'     => ['Authorization' => $this->request->getHeader('Authorization')],
                            'form_params' => $data
                        ]
                    );
                }
                DB::table('sys_bugs')->where('id', 4 )->update(['error' =>  env('API_PACKING') . '/v1/packs/auto/' .
                    $odrId]);

            }
            DB::table('sys_bugs')->where('id', 5 )->update(['error' => count($orders)]);

        }catch (\Exception $exception) {
            DB::table('sys_bugs')->where('id', 5 )->update(['error' => $exception->getMessage()]);

        }

    }
}
