<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReturnHdr;
use Seldat\Wms2\Models\WorkOrderHdr;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use App\Api\V1\Models\OrderHdrModel;
use Seldat\Wms2\Utils\Status;

/**
 * Class ReturnHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class ReturnHdrTransformer extends TransformerAbstract
{
    /**
     * @param ReturnHdr $returnHdr
     *
     * @return array
     */
    public function transform(ReturnHdr $returnHdr)
    {
        $createdAt = date("m/d/Y", strtotime($returnHdr->created_at));

        return [
            'return_id'       => $returnHdr->return_id,
            'return_num'      => $returnHdr->return_num,
            'odr_hdr_num'     => $returnHdr->odr_hdr_num,
            'odr_hdr_id'      => $returnHdr->odr_hdr_id,
            'csr'             => $returnHdr->csr,
            'csr_name'        => trim(object_get($returnHdr, "csrUser.first_name", null) . " " .
                object_get($returnHdr, "csrUser.last_name", null)),
            'putter'          => $returnHdr->putter,
            'putter_name'     => trim(object_get($returnHdr, "putterUser.first_name", null) . " " .
                object_get($returnHdr, "putterUser.last_name", null)),
            'return_sts'      => $returnHdr->return_sts,
            'return_sts_name' => Status::getByKey("Return-Order", $returnHdr->return_sts),
            'return_qty'      => $returnHdr->return_qty,
            'sts'             => $returnHdr->sts,
            'created_at'      => $createdAt,
            'created_by'      => trim(object_get($returnHdr, "createdUser.first_name", null) . " " .
                object_get($returnHdr, "createdUser.last_name", null))
        ];
    }
}
