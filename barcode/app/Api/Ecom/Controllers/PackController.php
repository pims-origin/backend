<?php

namespace App\Api\Ecom\Controllers;

use App\Api\Ecom\Models\PackDtlModel;
use App\Api\Ecom\Models\PackHdrModel;
use App\Api\Ecom\Transformers\PackTransformer;
use App\Api\Ecom\Validators\PackDetailValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use App\Api\Ecom\Transformers\PackDetailTransformer;
use App\Api\Ecom\Validators\PackValidator;

/**
 * Class PackController
 *
 * @package App\Api\V1\Controllers
 */
class PackController extends AbstractController
{
    const MAX_LENGTH_UPS_TRACKING_NUM = 18;

    protected $packHdrModel;
    protected $packDtlModel;

    public function __construct(
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel
    ) {
        $this->packHdrModel = $packHdrModel;
        $this->packDtlModel = $packDtlModel;
    }

    public function search($whsId, Request $request, PackTransformer $packTransformer, PackValidator $packValidator)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        $packValidator->validate($input);

        try {
            $packs = $this->packHdrModel->getModel()
                ->with(['details'])
                ->where('whs_id', $whsId)
                ->where('is_scan', 0)
                ->whereNotNull('tracking_number')
                ->orderBy('updated_at', 'desc')
                ->paginate(array_get($input, 'limit', 10));

            return $this->response->paginator($packs, $packTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show($whsId, $trackingNum, Request $request, PackDetailTransformer $packDtlTransformer, PackValidator $packValidator)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        if (strlen($trackingNum) > self::MAX_LENGTH_UPS_TRACKING_NUM) {
            $input['tracking_number'] = substr($trackingNum, strlen($trackingNum) - 12);
        } else {
            $input['tracking_number'] = $trackingNum;
        }

        $packValidator->validate($input);

        try {
            $pack = $this->packHdrModel->getModel()
                ->with(['details'])
                ->where('tracking_number', $input['tracking_number'])
                ->where('whs_id', $input['whs_id'])
                ->first();

            if (empty($pack)) {
                return $this->response->errorBadRequest("This tracking number is not exists!");
            }

            if ($pack->is_scan == 1) {
                return $this->response->errorBadRequest("Order has been scanned and packed.");
            }

            return $this->response->item($pack, $packDtlTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function checkPackDetail($whsId, Request $request, PackDetailValidator $packDetailValidator)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['whs_id'] = $whsId;

        $packDetailValidator->validate($input);

        try {
            $packHdr = $this->packHdrModel->getModel()
                ->with(['details'])
                ->where('tracking_number', $input['tracking_number'])
                ->where('whs_id', $input['whs_id'])
                ->where('is_scan', 0)
                ->first();

            if (empty($packHdr)) {
                return $this->response->errorBadRequest("Pack is not exists!");
            }

            if (empty($packHdr->details)) {
                return $this->response->errorBadRequest("Items are not exists!");
            }

            if (count($packHdr->details) != count($input['details'])) {
                return $this->response->errorBadRequest("Items are not match with Pack Detail!");
            }

            $packItems = array_pluck($packHdr->details, 'item_id');
            $inputItems = array_pluck($input['details'], 'item_id');

            if (!empty(array_diff($packItems, $inputItems))) {
                return $this->response->errorBadRequest("Items are not match with Pack Detail!");
            }

            $checkQty = array_pluck($input['details'], 'pack_qty', 'item_id');

            foreach ($packHdr->details as $item) {
                if ($checkQty[$item->item_id] != $item->piece_qty) {
                    return $this->response->errorBadRequest("Pack Quantity is missing!");
                }
            }

            // update is_scan
            $packHdr->is_scan = 1;
            $packHdr->save();

            return ['message' => 'Pack '. $packHdr->pack_hdr_num .' is checked!'];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}

