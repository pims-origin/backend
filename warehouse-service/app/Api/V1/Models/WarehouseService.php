<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class WarehouseService extends BaseService
{
    /**
     * @param $queryStr
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getList($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/';
        $uri = sprintf('%s?%s', $uri, $queryStr);
        $response = $this->client->get($uri);//$options
        return $response;
    }

    /**
     * @param $id
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDetail($id)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $id;
        $warehouseResponse = $this->client->get($uri);
        $warehouse = $this->responseToArray($warehouseResponse->getBody()->getContents());

        if (!$warehouse) {
            return false;
        }

        $uri = env('API_WAREHOUSE') . 'warehouses/' . $id . '/addresses';
        $addressResponse = $this->client->get($uri);
        $addresses = $this->responseToArray($addressResponse->getBody()->getContents());

        $uri = env('API_WAREHOUSE') . 'warehouses/' . $id . '/contacts';
        $contactResponse = $this->client->get($uri);
        $contacts = $this->responseToArray($contactResponse->getBody()->getContents());


        $uri = env('API_WAREHOUSE') . 'warehouses/' . $id . '/meta';
        $metaResponse = $this->client->get($uri);
        $meta = $this->responseToArray($metaResponse->getBody()->getContents());


        $result = $warehouse;

        if ($addresses) {
            $result += ['billing' => $addresses[1]] + ['shipping' => $addresses[0]];
        }

        if ($contacts) {
            $result += ['contacts' => $contacts[0]];
        }

        if ($meta) {
            $result += ['meta' => $meta[0]];
        }

        return $result;
    }

    /**
     * @param array params
     *
     * @return bool
     */
    public function create($params)
    {
        //  Uri create warehouse
        $uri = env('API_WAREHOUSE') . 'warehouses/';

        //  Call api create warehouse
        $options = [
            'form_params' => !empty($params['whs']) ? $params['whs'] : []
        ];

        $responseWarehouses = $this->client->post($uri, $options);

        //  Check create warehouse success.
        if ($responseWarehouses->getStatusCode() != Response::HTTP_CREATED) {
            return $responseWarehouses;
        }

        //  Get body content
        $warehouse = \GuzzleHttp\json_decode($responseWarehouses->getBody()->getContents());

        //  Id warehouse
        $warehouseId = $warehouse->data->whs_id;

        //----- Call api create contact (array ) + id warehouse created
        $options = [
            'form_params' => !empty($params['contacts'])
                ? ($params['contacts'] + ['whs_con_whs_id' => $warehouseId])
                : []
        ];
        $responseContact = $this->client->post($uri . $warehouseId . '/contacts', $options);
        if ($responseContact->getStatusCode() != Response::HTTP_CREATED) {
            return $responseContact;
        }


        //----- Call api create shipping + id warehouse created
        $options = [
            'form_params' => !empty($params['shipping'])
                ? ($params['shipping'] + ['whs_add_whs_id' => $warehouseId])
                : []
        ];
        $responseShipping = $this->client->post($uri . $warehouseId . '/addresses', $options);
        if ($responseShipping->getStatusCode() != Response::HTTP_CREATED) {
            return $responseShipping;
        }


        //----- Call api create billing + id warehouse created
        $options = [
            'form_params' => !empty($params['billing'])
                ? ($params['billing'] + ['whs_add_whs_id' => $warehouseId])
                : []
        ];
        $responseBilling = $this->client->post($uri . $warehouseId . '/addresses', $options);
        if ($responseBilling->getStatusCode() != Response::HTTP_CREATED) {
            return $responseBilling;
        }

        return ['data' => ['whs_id' => $warehouseId]];
    }

    /**
     * @param integer warehouseId, array $params
     *
     * @return bool
     */
    public function update($warehouseId, $params)
    {
        //  Uri create warehouse
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $warehouseId . '/';

        //  Call api create warehouse
        $options = [
            'form_params' => !empty($params['whs']) ? $params['whs'] : []
        ];
        $responseWarehouses = $this->client->put($uri, $options);

        //  Check create warehouse success.
        if ($responseWarehouses->getStatusCode() != Response::HTTP_OK) {
            return $responseWarehouses;
        }

        //----- Call api create contact (array ) + id warehouse created
        $options = [
            'form_params' => !empty($params['contacts'])
                ? ($params['contacts'] + ['whs_con_whs_id' => $warehouseId])
                : []
        ];
        //  Uri update contacts
        $uriContacts = $uri . 'contacts/'
            . (isset($params['contacts']['whs_con_id']) ? $params['contacts']['whs_con_id'] : '');
        $responseContact = $this->client->put($uriContacts, $options);
        if ($responseContact->getStatusCode() != Response::HTTP_OK) {
            return $responseContact;
        }


        //----- Call api create shipping + id warehouse created
        $options = [
            'form_params' => !empty($params['shipping'])
                ? ($params['shipping'] + ['whs_add_whs_id' => $warehouseId])
                : []
        ];
        //  Uri update shipping
        $uriShipping = $uri . 'addresses/'
            . (isset($params['shipping']['whs_add_id']) ? $params['shipping']['whs_add_id'] : '');
        $responseShipping = $this->client->put($uriShipping, $options);
        if ($responseShipping->getStatusCode() != Response::HTTP_OK) {
            return $responseShipping;
        }


        //----- Call api create billing + id warehouse created
        $options = [
            'form_params' => !empty($params['billing'])
                ? ($params['billing'] + ['whs_add_whs_id' => $warehouseId])
                : []
        ];
        //  Uri update billing
        $uriBilling = $uri . 'addresses/'
            . (isset($params['billing']['whs_add_id']) ? $params['billing']['whs_add_id'] : '');
        $responseBilling = $this->client->put($uriBilling, $options);
        if ($responseBilling->getStatusCode() != Response::HTTP_OK) {
            return $responseBilling;
        }

        //----- Call api create warehouse meta warehouse created
        $options = [
            'form_params' => !empty($params['meta'])
                ? ($params['meta'] + ['whs_id' => $warehouseId])
                : []
        ];

        // Uri update meta
        $uriMeta = $uri . 'meta/'
            . (isset($params['meta']['whs_qualifier']) ? $params['meta']['whs_qualifier'] : '');

        $responseMeta = $this->client->put($uriMeta, $options);
        if ($responseMeta->getStatusCode() != Response::HTTP_OK) {
            return $responseMeta;
        }

        return ['data' => true];
    }

    /**
     * @param integer warehouseId
     *
     * @return bool
     */
    public function destroy($warehouseId)
    {
        //  Uri create warehouse
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $warehouseId . '/';

        $responseWarehouses = $this->client->delete($uri);
        if ($responseWarehouses->getStatusCode() != Response::HTTP_OK) {
            return $responseWarehouses;
        }

        return ['data' => true];
    }

    /**
     * @param integer warehouseId, array $params
     *
     * @return bool
     */
    public function search($queryStr)
    {
        //  Uri create warehouse
        $uri = env('API_WAREHOUSE') . 'warehouses/';
        $uri = sprintf('%s?%s', $uri, $queryStr);
        $response = $this->client->get($uri);

        return $response;
    }

    public function getLocations($warehouseId, $queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $warehouseId . '/locations';
        $uri = sprintf('%s?%s', $uri, $queryStr);
        $response = $this->client->get($uri);

        return $response;
    }

    public function getLocationsByCustomers($whsId, $cusId, $queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $whsId . '/locations-by-customers/' . $cusId;
        $uri = sprintf('%s?%s', $uri, $queryStr);
        $response = $this->client->get($uri);

        return $response;
    }


    public function getFreeLocations($warehouseId, $queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $warehouseId . '/free-locations';
        $uri = sprintf('%s?%s', $uri, $queryStr);
        $response = $this->client->get($uri);

        return $response;
    }

    public function getZone($warehouseId, $queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $warehouseId . '/zones';
        $uri = sprintf('%s?%s', $uri, $queryStr);
        $response = $this->client->get($uri);

        return $response;
    }

    public function getStatuses()
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/statuses';
        $response = $this->client->get($uri);

        return $response;
    }

    public function getLayout($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'whs-layout?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function showLayout($zone, $queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'whs-layout';
        $uri = sprintf('%s/%s?%s', $uri, $zone, $queryStr);
        $response = $this->client->get($uri);

        return $response;
    }

    public function showPopup($locId)
    {
        $uri = env('API_WAREHOUSE') . 'whs-layout';
        $uri = sprintf('%s/%s/show', $uri, $locId);

        $response = $this->client->get($uri);

        return $response;
    }

    public function getInvReport($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/inventory-report?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function shippingReport($whsId, $queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $whsId . '/report/shipping?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function orderReport($whsId, $queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/' . $whsId . '/report/order?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }

    public function orderStatusNotShipping($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses/report/order-status-not-shipping?' . $queryStr;
        $response = $this->client->get($uri);

        return $response;
    }
}
