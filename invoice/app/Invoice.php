<?php

namespace App;

class Invoice
{
    const RC = 'RECEIVING';
    const ST = 'STORAGE';
    const OP = 'ORD_PROC';
    const OS = 'OTHER_SERV';

    const CURRENCY = 'USD';

    const CUBIC_FOOT_TO_CUBIC_METER = 0.028316846592;
    const CUBIC_INCH_TO_CUBIC_METER = 0.0000163870640693;

    static $smallCarton = 'MONTHLY_SMALL_CARTON';
    static $mediumCarton = 'MONTHLY_MEDIUM_CARTON';
    static $largeCarton = 'MONTHLY_LARGE_CARTON';
    static $xlCarton = 'MONTHLY_XL_CARTON';
    static $xxlCarton = 'MONTHLY_XXL_CARTON';

    static $container = 'CONTAINER';
    static $month = 'MONTH';
    static $volume = 'VOLUME';
    static $piece = 'PIECE';
    static $pallet = 'PALLET';
    static $carton = 'CARTON';

    static $monthPallet = 'MONTHLY_PALLET';
    static $monthVolume = 'MONTHLY_VOLUME';

    static $palletCurrent = 'PALLET_CURRENT';
    static $volumeCurrent = 'VOLUME_CURRENT';
    static $cartonCurrent = 'CARTON_CURRENT';

    static $order = 'ORDER';
    static $unit = 'UNIT';
    static $ups = 'UPS_CARTON';
    static $fedex = 'FEDEX_CARTON';
    static $label = 'LABEL';
    static $orderCancel = 'ORDER_CANCEL';
    static $labor = 'LABOR';
    static $bol = 'BOL';

    //Imperial and Metric Measurements UOMS
    static $volumeImperial = 'VOLUME_IMPERIAL';
    static $smallCartonImperial = 'MONTHLY_SMALL_CART_IMPERIAL';
    static $mediumCartonImperial = 'MONTHLY_MEDIUM_CART_IMPERIAL';
    static $largeCartonImperial = 'MONTHLY_LARGE_CART_IMPERIAL';
    static $xlCartonImperial = 'MONTHLY_XL_CART_IMPERIAL';
    static $xxlCartonImperial = 'MONTHLY_XXL_CART_IMPERIAL';
    static $monthVolumeImperial = 'MONTHLY_VOLUME_IMPERIAL';
    static $volumeCurrentImperial = 'VOLUME_CURRENT_IMPERIAL';

    static $volumeMetric = 'VOLUME_METRIC';
    static $smallCartonMetric = 'MONTHLY_SMALL_CART_METRIC';
    static $mediumCartonMetric = 'MONTHLY_MEDIUM_CART_METRIC';
    static $largeCartonMetric = 'MONTHLY_LARGE_CART_METRIC';
    static $xlCartonMetric = 'MONTHLY_XL_CART_METRIC';
    static $xxlCartonMetric = 'MONTHLY_XXL_CART_METRIC';
    static $monthVolumeMetric = 'MONTHLY_VOLUME_METRIC';
    static $volumeCurrentMetric = 'VOLUME_CURRENT_METRIC';

    static $charge_codes_measures = [
        'RLI' => 'REC-LARGE-CART',
        'RLM' => 'REC-LARGE-CART',
        'RMI' => 'REC-MEDIUM-CART',
        'RMM' => 'REC-MEDIUM-CART',
        'RSI' => 'REC-SMALL-CART',
        'RSM' => 'REC-SMALL-CART',
        'RVI' => 'REC-VOL',
        'RVM' => 'REC-VOL',
        'HLI' => 'SHIP-LARGE-CART',
        'HLM' => 'SHIP-LARGE-CART',
        'HMI' => 'SHIP-MEDIUM-CART',
        'HMM' => 'SHIP-MEDIUM-CART',
        'HSI' => 'SHIP-SMALL-CART',
        'HSM' => 'SHIP-SMALL-CART',
        'HVI' => 'SHIP-VOL',
        'HVM' => 'SHIP-VOL',
        'SLI' => 'STOR-LARGE-CART',
        'SLM' => 'STOR-LARGE-CART',
        'SMI' => 'STOR-MEDIUM-CART',
        'SMM' => 'STOR-MEDIUM-CART',
        'SNI' => 'STOR-MONTH-VOL',
        'SNM' => 'STOR-MONTH-VOL',
        'SSI' => 'STOR-SMALL-CART',
        'SSM' => 'STOR-SMALL-CART',
        'SCI' => 'STOR-VOL-CUR',
        'SCM' => 'STOR-VOL-CUR',
        'SVI' => 'STOR-VOL',
        'SVM' => 'STOR-VOL',
        'SGI' => 'STOR-XL-CART',
        'SGM' => 'STOR-XL-CART',
        'STI' => 'STOR-XXL-CART',
        'STM' => 'STOR-XXL-CART',
    ];

}