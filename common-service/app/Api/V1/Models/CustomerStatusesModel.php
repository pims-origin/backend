<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\CustomerStatus;

class CustomerStatusesModel extends AbstractModel
{
    public function __construct(Customers $model = NULL)
    {
        $this->model = ($model) ?: new CustomerStatus();
    }

    /*
    ****************************************************************************
    */

}
