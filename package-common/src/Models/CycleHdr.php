<?php

namespace Seldat\Wms2\Models;

class CycleHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cycle_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cycle_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cycle_name',
        'cycle_num',
        'whs_id',
        'count_by',
        'cycle_method',
        'cycle_type',
        'cycle_detail',
        'description',
        'assignee_id',
        'due_dt',
        'has_color_size',
        'cycle_sts',
        'sts',
        'completed_at'

    ];

    /**
     * @return Warehouse
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'whs_id', 'whs_id');
    }

    /**
     * @return assignee User
     */
    public function assigneeUser()
    {
        return $this->belongsTo(User::class, 'assignee_id', 'user_id');
    }

    /**
     * @return Created User
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return Created User
     */
    public function cycleDtl()
    {
        return $this->hasMany(CycleDtl::class, 'cycle_hdr_id');
    }
}
