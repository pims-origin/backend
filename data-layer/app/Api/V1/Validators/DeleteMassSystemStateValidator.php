<?php

namespace App\Api\V1\Validators;

class DeleteMassSystemStateValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'sys_state_id'   => 'required',
            'sys_state_id.*' => 'required|exists:system_state,sys_state_id'
        ];
    }
}