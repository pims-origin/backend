<?php

namespace App\Modules\Asns\Models;

use App\Modules\Asns\Eloquents\Item;
use Illuminate\Support\Facades\Config;
use App\Modules\Asns\Eloquents\AsnHdr;
use App\Modules\Asns\Eloquents\AsnDtl;
use App\Modules\Asns\Eloquents\Container;
use App\Modules\Asns\Eloquents\SystemUom;

abstract class AbstractModel
{
    public function __construct()
    {
        //
    }

    public function checkCusOfUser($whsId, $cusId)
    {
        $cusOfUser = Config::get('data.cusOfUser');

        foreach ($cusOfUser as $data) {
            if ($data->whs_id == $whsId && $data->cus_id == $cusId)
            {
                return true;
            }
        }

        return false;
    }

    public static function generateAsnNumAndSeq(String $prefix = 'ASN')
    {
        $currentYearMonth = date('ym');
        $asn = AsnHdr::where('asn_hdr_num', 'LIKE', '%-' . $currentYearMonth . '-%')
                        ->orderBy('asn_hdr_seq', 'DESC')
                        ->first();

        if (!$asn){
            return [
                'asn_num' => $prefix . '-' . $currentYearMonth . '-' . '00001',
                'asn_seq' => 1
            ];
        }

        $asn->asn_hdr_seq++;
        return [
            'asn_num' => $prefix . '-' . $currentYearMonth . '-' . sprintf('%05d', $asn->asn_hdr_seq),
            'asn_seq' => $asn->asn_hdr_seq
        ];
    }

    public function volumeAndCube($length, $width, $height)
    {
        return [
            'volume' => $length * $width * $height,
            'cube'   => $length * $width * $height / 1728
        ];
    }

    public function generateUpc128($itemId, $cusId)
    {
        return str_pad($cusId, 5, '0', STR_PAD_LEFT) . str_pad($itemId, 8, '0', STR_PAD_LEFT);
    }

    public function getItemInfo($attributes)
    {
        $item = Item::where('deleted', 0)
            ->where('cus_id', array_get($attributes, 'cus_id'))
            ->where('sku', array_get($attributes, 'sku'))
            ->first();

        return $item;
    }

    public function createNewContainer($attributes)
    {
        $container = Container::where('deleted', 0)
            ->where('ctnr_num', array_get($attributes, 'ctnr_num'))
            ->first();

        if (!$container)
        {
            $container = Container::create($attributes);
        }

        return $container;
    }

    public function getFlagDis($msmCode)
    {
        $result = [
            'flag_dis_h'  => false,
            'flag_dis_l'  => false,
            'flag_dis_we' => false,
            'flag_dis_wi' => false
        ];

        if ($msmCode == 'IN')
        {
            $result = [
                'flag_dis_h'  => true,
                'flag_dis_l'  => true,
                'flag_dis_we' => true,
                'flag_dis_wi' => true
            ];
        }

        return $result;
    }

    public function checkExistItem($items)
    {
        $result = [];

        foreach ($items as $item) {
            $checkItm = Item::where([
                'cus_id'    => array_get($item, 'cus_id'),
                'sku'       => array_get($item, 'sku'),
                'deleted'   => 0
            ])->first();

            if (!$checkItm)
            {
                $result[] = array_get($item, 'sku');
            }
        }

        return $result;
    }

    public function checkCtnrProcessing($data)
    {
        $check = false;
        $status = array_get($data, 'status', ['CC', 'NW', 'RG', 'RE']);

        $asnDtls = AsnDtl::select(['asn_hdr_id', 'ctnr_num'])
            ->where('deleted', 0)
            ->where('ctnr_num', array_get($data, 'ctnr_num'))
            ->where('asn_dtl_sts', '<>', array_get($status, 'cc'))
            ->groupBy('asn_hdr_id', 'ctnr_num')
            ->get();

        foreach ($asnDtls as $asnDtl) {
            $asnHdr = $asnDtl->asnHdr()->where('whs_id', array_get($data, 'whs_id'))->first();

            if ($asnHdr)
            {
                $grHdr = $asnHdr->goodsReceipt()->where('gr_sts', '<>', array_get($status, 're'))->first();
                if ($grHdr)
                {
                    $check = true;
                    break;
                }
            }
        }

        return $check;
    }

    public function validateDuplicateItem($input)
    {
        $result = [];

        if (count($input) >= 2)
        {
            $output = array_map("unserialize", array_unique(array_map("serialize", $input)));
            if (count($output) < count($input))
            {
                $arrDup = array_diff_key($input, $output);
                $result = array_map("unserialize", array_unique(array_map("serialize", $arrDup)));
            }
        }

        return $result;
    }
}