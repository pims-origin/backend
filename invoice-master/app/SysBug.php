<?php
/**
 * Created by PhpStorm.
 * User: chicu
 * Date: 9/14/2016
 * Time: 10:47 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Seldat\Wms2\Utils\JWTUtil;

class SysBug extends BaseModel
{
    protected $table = 'sys_bugs';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'date',
        'api_name',
        'error'
    ];

    public static function boot()
    {
        parent::boot();

        $userId = JWTUtil::getPayloadValue('jti') ?: 1;


        // create a event to happen on saving
        static::creating(function ($table) use ($userId) {
            $table->created_by = $table->created_by ?: $userId;
            $table->created_at = time();
        });

    }
    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }

    public static function writeSysBug($message, $apiName, $function) {
        $arrErrors = [
            'function' => $function,
            'errors' => $message
        ];
        //set array input data
        $data = [
            'date' => date('dmy'),
            'api_name' => $apiName,
            'error' => json_encode($arrErrors)
        ];
        //if is's ok, call insert to DB
        SysBug::create($data);
    }
}