<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\PalletSuggestLocation;
use Wms2\UserInfo\Data;


class PalletSuggestLocationModel extends AbstractModel
{

    /**
     * PalletSuggestLocationModel constructor.
     *
     * @param PalletSuggestLocation|null $model
     */
    public function __construct(PalletSuggestLocation $model = null)
    {
        $this->model = ($model) ?: new PalletSuggestLocation();
    }

    /**
     * @param $hdrDtlId
     *
     * @return mixed
     */
    public function getHdrPutAwayByHdrDtlID($hdrDtlId)
    {
        $sql1 = "pal_sug_loc.gr_hdr_num, pal_sug_loc.sku, pal_sug_loc.size, pal_sug_loc.color, count(*) as total, " .
            " SUM(( select count(*) as actual from pallet where pallet.plt_id = pal_sug_loc.plt_id " .
            " AND pallet.loc_id IS NOT NULL )) as actual";
        $query = $this->model
            ->select([
                DB::raw($sql1),
                "pal_sug_loc.gr_hdr_num",
                "pal_sug_loc.sku",
                "pal_sug_loc.size",
                "pal_sug_loc.color",
            ])
            ->where('pal_sug_loc.gr_dtl_id', $hdrDtlId)
            ->get();

        return $query;
    }

    public function getDtlPutAwayByHdrDtlID($hdrDtlId)
    {

        $query = $this->model
            ->select('location.loc_code', 'pallet.plt_num')
            ->join('location', 'location.loc_id', '=', 'pal_sug_loc.loc_id')
            ->join('pallet', 'pallet.plt_id', '=', 'pal_sug_loc.plt_id')
            ->whereNull('pallet.loc_id')
            ->where('pal_sug_loc.gr_dtl_id', $hdrDtlId)
            ->get();

        return $query;
    }

    /**
     * @param $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function search($attributes, $with = [])
    {
        $query = $this->make($with)
            ->select(['plt_id', 'gr_hdr_id', 'gr_hdr_num'])
            ->where('putter', $attributes['putter'])
            ->where('whs_id', $attributes['whs_id'])
            ->where(
                DB::raw("(select count(*) from pallet p where pal_sug_loc.plt_id = p.plt_id and p.loc_id is null)"),
                ">", 0
            )
            ->groupBy('gr_hdr_id');

        $model = $query->get();

        return $model;
    }

    public function show($attributes, $with = [], $day = null)
    {

        $color = "case when (color is not null AND color != 'NA') then concat('-', color) else '' end";
        $size = "case when (size is not null AND size != 'NA') then concat('-', size) else '' end";
        $query = $this->make($with)
            ->select([
                'gr_hdr_id',
                'gr_hdr_num',
                'gr_dtl_id',
                'sku',
                'color',
                'size',
                DB::raw("concat(sku, $size, $color) as sku_size_color"),
                DB::raw("count(*) as total"),
                DB::raw("SUM((select count(*) from pallet p where p.plt_id = pal_sug_loc.plt_id and p.loc_id is not null)) as actual")
            ]);
        if (!empty($attributes['gr_hdr_id'])) {
            $query->where('gr_hdr_id', $attributes['gr_hdr_id']);
        }
        $query->where('putter', $attributes['putter'])
            ->where('whs_id', $attributes['whs_id'])
            ->groupBy('gr_dtl_id')
            ->orderBy('gr_hdr_id');
        if (!is_null($day)) {
            $limit = (!empty($day) && is_numeric($day)) ? $day : 1;
            $query->where('created_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));
        };
        $model = $query->get();

        return $model;
    }

    public function upsert($pltGrDtl, $grHdr)
    {
        $pltSugLoc = DB::table('pal_sug_loc')->where('plt_id', $pltGrDtl['plt_id'])
            ->first();

        if (empty($pltSugLoc)) {
            $data = [
                'plt_id'       => $pltGrDtl['plt_id'],
                'loc_id'       => $pltGrDtl['loc_id'],
                'data'         => $pltGrDtl['loc_code'],
                'ctn_ttl'      => $pltGrDtl['ctn_ttl'],
                'item_id'      => $pltGrDtl['item_id'],
                'sku'          => $pltGrDtl['sku'],
                'size'         => $pltGrDtl['size'],
                'color'        => $pltGrDtl['color'],
                'lot'          => $pltGrDtl['lot'],
                'putter'       => $grHdr->putter,
                'gr_hdr_id'    => $grHdr->gr_hdr_id,
                'gr_dtl_id'    => $pltGrDtl['gr_dtl_id'],
                'gr_hdr_num'   => $grHdr->gr_hdr_num,
                'whs_id'       => $pltGrDtl['whs_id'],
                'put_sts'      => 'CO',
                'act_loc_id'   => $pltGrDtl['loc_id'],
                'act_loc_code' => $pltGrDtl['loc_code'],
                'deleted_at'   => 915148800,
                'deleted'      => 0,
                'created_by'   => Data::getCurrentUserId(),
                'updated_by'   => Data::getCurrentUserId(),
                'created_at'   => time(),
                'updated_at'   => time(),
            ];

            DB::table('pal_sug_loc')->insert($data);
        } else {
            DB::table('pal_sug_loc')->where('plt_id', $pltGrDtl['plt_id'])
                ->update([
                    'act_loc_id'   => $pltGrDtl['loc_id'],
                    'act_loc_code' => $pltGrDtl['loc_code'],
                    'put_sts'      => 'CO'
                ]);
        }
    }

    public function upsertWithGrDTl($pltGrDtl, $grDtl, $grHdr, $qtyCtn)
    {
        if(empty($grDtl) || empty($grHdr)) {return 0;} else {
            $pltSugLoc = DB::table('pal_sug_loc')->where('plt_id', $pltGrDtl['plt_id'])
                ->where('item_id', $grDtl->item_id)
				->orderBy('created_at', 'desc')
                ->first();
            if (empty($pltSugLoc) || $pltSugLoc['gr_hdr_id'] != $grHdr->gr_hdr_id) {

                $data = [
                    'plt_id' => $pltGrDtl['plt_id'],
                    'loc_id' => $pltGrDtl['loc_id'],
                    'data' => $pltGrDtl['loc_code'],
                    'ctn_ttl' => $pltGrDtl['ctn_ttl'],
                    'qty_ttl' => $pltGrDtl['init_piece_ttl'],
                    'item_id' => $grDtl->item_id,
                    'sku' => $grDtl->sku,
                    'size' => $grDtl->size,
                    'color' => $grDtl->color,
                    'lot' => $grDtl->lot,
                    'putter' => $grHdr->putter,
                    'gr_hdr_id' => $grHdr->gr_hdr_id,
                    'gr_dtl_id' => $grDtl->gr_dtl_id,
                    'gr_hdr_num' => $grHdr->gr_hdr_num,
                    'whs_id' => $pltGrDtl['whs_id'],
                    'put_sts' => 'CO',
                    'act_loc_id' => $pltGrDtl['loc_id'],
                    'act_loc_code' => $pltGrDtl['loc_code'],
                    'deleted_at' => (!empty($pltSugLoc['gr_hdr_id'])) ? $pltSugLoc['gr_hdr_id'] + 1 : 915148800,
                    'deleted' => 0,
                    'created_by' => Data::getCurrentUserId(),
                    'updated_by' => Data::getCurrentUserId(),
                    'created_at' => time(),
                    'updated_at' => time(),
                ];

                /*$date = date("Y_m_d");
                File::append(
                    storage_path("/logs/suggest_location_$date.log"),
                    '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' . PHP_EOL .
                    date("Y-m-d H:i:s") . ': Inserted Data: ' . PHP_EOL .
                    print_r($data,true)	. PHP_EOL
                );*/

                DB::table('pal_sug_loc')->insert($data);

                return 0;
            } else {
                DB::table('pal_sug_loc')->where('plt_id', $pltGrDtl['plt_id'])
                    ->where('item_id', $grDtl->item_id)
                    ->update([
                        'act_loc_id' => $pltGrDtl['loc_id'],
                        'act_loc_code' => $pltGrDtl['loc_code'],
                        'ctn_ttl' => $pltGrDtl['ctn_ttl'],
                        'qty_ttl' => $pltGrDtl['init_piece_ttl'],
                        'put_sts' => 'CO'
                    ]);
                return 0;
            }
        }
    }

    public function getItemByAsn($attribute, $whsId) {
        $grDtls = DB::table("gr_dtl")
            ->join("gr_hdr","gr_hdr.gr_hdr_id","=","gr_dtl.gr_hdr_id")
            ->where("gr_hdr.asn_hdr_id",$attribute['asn_hdr_id'])
            ->where("gr_dtl.deleted",0)
            ->where("gr_hdr.deleted",0)
            ->select("item_id","gr_dtl_id","sku")
            ->get();

        $grDtlIds = array_pluck($grDtls,'gr_dtl_id');

        $query = $this->getModel()->whereIn("gr_dtl_id",$grDtlIds)->get();
        return($query);
    }

}
