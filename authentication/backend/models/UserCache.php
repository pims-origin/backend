<?php

namespace backend\models;

use yii\base\Exception;
use yii\db\Query;
use Yii;

final class UserCache extends BaseUser
{
    protected $_userKey = "",
        $_expired = 3600, //1 hour
        $_userId = "",
        $_prefix = "cache.user.",
        $_cacheServ = null;
    private static $_instance;

    protected function __construct()
    {
        $this->_userId = \Yii::$app->user->getId();
        $this->_userKey = $this->_prefix . $this->_userId;
        $this->_cacheServ = \yii::$app->cache;
        return parent::__construct();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new UserCache();
        }

        return self::$_instance;
    }

    public function deleteUserCache($id = null)
    {
        if (empty($id)) {
            $this->_cacheServ->delete($this->_userKey);
        } else {
            $this->_cacheServ->delete($this->_prefix . $id);
        }
    }

    public function getUserCache()
    {
        $result = $this->_cacheServ->get($this->_userKey);
        if ($result) {
            return $result;
        }

        //get user permission from authorization
		$perms = $this->getUserPermissions();

        //get warehouses, customers, roles
        $user = $this->_getUserInfo($this->_userId);
        //set result
        $user['permissions'] = $perms;
        $user['current_whs'] = $this->_getWID($user['user_warehouses']);
        $user['isSuperAdmin'] = ($this->_userId === 1);

        //set cache
        $this->_cacheServ->set($this->_userKey, $user, $this->_expired);

        return $user;
    }

    /**
     * get warehouse id default in user meta. If not existed get first whs_id in user_whs
     *
     * @param int $whs
     *
     * @return int/null
     */
    protected function _getWID($whs)
    {
        $userId = $this->_userId;
        $whsIds = array_column($whs, "whs_id");
        if (empty($whsIds)) {
            return null;
        }

        $userMeta = new UserMetaProcess();

        $wid = $userMeta->get($userId, 'WID');

        if (empty($wid) || !in_array($wid, $whsIds)) {
            $wid = $whsIds[0]; //get frist whs_id
            $userMeta->set($userId, 'WID', $wid);
        }

        return $wid;
    }

    /**
     * update cache for current_whs
     *
     * @param int $value
     *
     * @return bool
     * @throws Exception
     */
    public function setWID($value)
    {
        $userCache = self::getUserCache();
        $whsIds = array_column($userCache['user_warehouses'], "whs_id");

        if (!in_array($value, $whsIds)) {
            return false;
        }

        $userCache['current_whs'] = $value;
        //update cache
        $this->_cacheServ->set($this->_userKey, $userCache, $this->_expired);

        return true;
    }
}