<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\CusInUserModel;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;

class CustomerInUserModel extends AbstractModel
{
    /**
     * @param CusInUserModel $model
     */
    public function __construct(CusInUserModel $model = null)
    {
        $this->model = ($model) ?: new CusInUserModel();
    }

    public function getUserOfCustomer($customerId){
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return DB::table('cus_in_user as ciu')
            ->select([
                'ciu.cus_id','ciu.user_id'
            ])
            ->join('users as u', 'u.user_id', '=', 'ciu.user_id')
            ->addSelect('u.first_name','u.last_name','u.username')
            ->where('ciu.cus_id', '=', $customerId)
            ->groupBy('user_id')
            ->get();
    }
}