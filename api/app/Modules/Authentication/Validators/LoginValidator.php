<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/13/2018
 * Time: 3:44 PM
 */
namespace App\Modules\Authentication\Validators;
use Validator;

class LoginValidator
{
    public function rules()
    {
        return [
            'username' => 'required|min:6|max:255',
            'password' => 'required|min:6|max:255'
        ];
    }

    public function validate($input)
    {
        $validator = Validator::make($input, $this->rules(), $this->messages());
        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                    /*$listErrors['title'][] = $title;
                    $listErrors['detail'][] = 'Please try';*/
                }
                //throw new \Exception(json_encode($listErrors));
            }
        }
        return $validator;
    }

    public function messages() {
        return [
            'username.required'     => 'Username is required',
            'username.min'          => 'Username min length 6 charater',
            'username.max'          => 'Username max length 255 ',
            'password'              => 'Username is required',
            'password.min'          => 'Password min length 6 charater',
            'password.max'          => 'Password max length 255 ',
        ];
    }

    public function checkUserExistAndActive($user) {
        if(!$user) {
            throw new \Exception(json_encode(['title' =>'Username or password is invalid.', 'detail' => 'Please try']));
        } else if (isset($user->status) && $user->status == "IA") {
            throw new \Exception(json_encode(['title' =>'Username or password is invalid.', 'detail' => 'Please try']));
        }
    }

    public function checkPasswordMatch($requestPass, $passInDB) {

        if (!password_verify($requestPass,$passInDB)) {
            throw new \Exception(json_encode(['title' =>'Username or password is invalid.', 'detail' => 'Please try']));

        }
    }
}