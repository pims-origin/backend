<?php

namespace App\Api\V2\Outbound\Order\GetSkuByRfid\Controllers;

use App\Api\V2\Outbound\Order\GetSkuByRfid\Models\CartonModel;
use App\libraries\RFIDValidate;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class CartonController extends AbstractController
{

    protected $cartonModel;

    public function __construct()
    {
        $this->cartonModel = new CartonModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function getSku($whsId, $rfid, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/v2/{$whsId}/order/sku-by-rfid/{$rfid}";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GLO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get SKU by RFID'
        ]);
        /*
         * end logs
         */

        $cartons = [$rfid];
        //check invalid code carton
        $ctnRfidInValid = [];
        $errorMsg = '';
        if (!empty($cartons)) {
            foreach ($cartons as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }
        } else {
            $msg = 'Ctns array is required!';
            return $this->_responseErrorMessage($msg);
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
        }

        try {

            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $cartonObj = $this->cartonModel->getFirstWhere([
                            'whs_id'     => $whsId,
                            'rfid'       => $rfid,
                            // 'ctn_sts'    => 'PD',
                            'is_damaged' => 0,
                        ]);

            if (!$cartonObj) {
                $msg = sprintf('Carton RFID %s is not scanned through conveyor inbound!', $rfid);
                $cartonObj = $this->cartonModel->getFirstWhere(['rfid' => $rfid]);
                if ($cartonObj) {
                    if ($cartonObj->whs_id != $whsId) {
                        $msg = sprintf('Carton RFID %s does not belong to current Warehouse!', $rfid);
                    }
                    // if ($cartonObj->ctn_sts != 'PD') {
                    //     $msg = sprintf('Carton RFID %s is not picked!', $rfid);
                    // }
                    if ($cartonObj->is_damaged == 1) {
                        $msg = sprintf('Carton RFID %s is damaged!', $rfid);
                    }
                }

                return $this->_responseErrorMessage($msg);
            }

            $data = [
                'rfid'      => object_get($cartonObj,'rfid'),
                'item_id'   => object_get($cartonObj,'item_id'),
                'sku'       => object_get($cartonObj,'sku'),
                'size'      => object_get($cartonObj,'size'),
                'color'     => object_get($cartonObj,'color'),
                'lot'       => object_get($cartonObj,'lot'),
                'pack'      => object_get($cartonObj,'ctn_pack_size'),
                'piece_qty' => object_get($cartonObj,'piece_remain'),
                'ctn_sts'   => object_get($cartonObj,'ctn_sts'),
            ];

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data']   = [$data];

            return $msg;

        } catch (\Exception $e) {

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}