<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\InvoiceCreateData;

use Seldat\Wms2\Utils\JWTUtil;

class InvoiceCreateDataModel extends AbstractModel
{
    protected $model;

    public function __construct(
        InvoiceCost $model = NULL
    )
    {
        $this->model = ($model) ?: new InvoiceCreateData();
    }

    /*
    ****************************************************************************
    */

    public function saveCreateParams($data)
    {
        $results = $this->model->create([
            'data' => json_encode($data),
            'created_by' => JWTUtil::getPayloadValue('jti') ?: 0,
        ]);

        $insertedValues = $results->getAttributes();

        return $insertedValues['inv_cr_id'];
    }

    /*
    ****************************************************************************
    */

    public function getCreateParams($inv_cr_id)
    {
        $results = $this->model
                ->where('inv_cr_id', $inv_cr_id)
                ->first();

        return $results->getOriginal();
    }

    /*
    ****************************************************************************
    */

}

