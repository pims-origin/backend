<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ShippingOrder;
use Seldat\Wms2\Models\Shipment;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class ShippingOrderModel
 *
 * @package App\Api\V1\Models
 */
class ShippingOrderModel extends AbstractModel
{
    /**
     * @var ShippingOrder
     */
    protected $model;

    /**
     * @var ShippingOrder
     */
    protected $shipMentModel;

    /**
     * ShippingOrderModel constructor.
     *
     * @param ShippingOrder|null $model
     */
    public function __construct(ShippingOrder $model = null)
    {
        $this->model = ($model) ?: new ShippingOrder();
    }
}
