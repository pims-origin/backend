<?php

namespace Seldat\Wms2\Models;

class InvoiceDetails extends BaseSoftModel
{
    protected $table = 'invoice_summary';

    protected $primaryKey = 'inv_sum_id';

    protected $fillable = [
        'cus_id',
        'whs_id',
        'inv_id',
    ];

    /*
    ****************************************************************************
    */

}
