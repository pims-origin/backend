<?php

namespace App\Api\V1\Validators;


class SaveMassCustomerZoneValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id'    => 'required|array',
            'whs_id.*'  => 'required|exists:warehouse,whs_id',
            'zone_id'   => 'required|array',
            'zone_id.*' => 'required|exists:zone,zone_id'
        ];
    }
}
