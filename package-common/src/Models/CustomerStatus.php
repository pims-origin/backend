<?php

namespace Seldat\Wms2\Models;

class CustomerStatus extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_status';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cus_sts_code';


    public $incrementing = false;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
        'cus_sts_desc'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_sts_code', 'cus_status');
    }
}
