<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\OrderHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\OrderHdrMeta;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderDtlTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderDtlTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $odrHdrMeta = OrderHdrMeta::where('odr_id', $orderHdr->odr_id)
            ->where('qualifier', 'OFP')
            ->first();
        $partial = $orderHdr->back_odr == 1 ? "Partial " : "";

        $objItems = object_get($orderHdr, 'items', null);
        if (!empty($objItems)) {
            foreach ($objItems as &$item) {
                if (!in_array($item['uom_code'], ['KG', 'PL'])) {
                    $item['uom_qty'] = (int)$item['uom_qty'];
                    $item['piece_qty'] = (int)$item['piece_qty'];
                    $item['qty'] = (int)$item['qty'];
                    $item['avail'] = (int)$item['avail'];
                    $item['allocated_qty'] = (int)$item['allocated_qty'];
                }
                $item['picked_qty'] = (int)$item['picked_qty'];
            }            
        }
        return [
            // Customer
            'whs_id'               => $orderHdr->whs_id,
            'whs_id_to'            => $orderHdr->whs_id_to,
            'cus_id'               => $orderHdr->cus_id,
            'cus_name'             => object_get($orderHdr, 'customer.cus_name', null),
            'cus_odr_num'          => $orderHdr->cus_odr_num,
            'ref_cod'              => $orderHdr->ref_cod,
            'cus_po'               => $orderHdr->cus_po,
            'odr_type_key'         => $orderHdr->odr_type,
            'odr_type'             => Status::getByKey("Order-type", $orderHdr->odr_type),
            'rush_odr'             => $orderHdr->rush_odr,
            'pack_odr'             => array_get($odrHdrMeta, 'value', null),
            'sku_ttl'              => $orderHdr->sku_ttl,
            'odr_sts_key'          => $orderHdr->odr_sts,
            'odr_sts'              => $partial . Status::getByKey("Order-status", $orderHdr->odr_sts),
            'odr_num'              => $orderHdr->odr_num,
            'carrier'              => $orderHdr->carrier,
            'carrier_code'         => $orderHdr->carrier_code,
            'ship_to_phone'        => $orderHdr->ship_to_phone,
            'dlvy_srvc'            => $orderHdr->dlvy_srvc,
            'dlvy_srvc_code'       => $orderHdr->dlvy_srvc_code,
            'cosignee_sig'         => $orderHdr->cosignee_sig,
            'csr'                  => $orderHdr->csr,
            'ship_to_name'         => $orderHdr->ship_to_name,
            'ship_to_add_1'        => $orderHdr->ship_to_add_1,
            'ship_to_city'         => $orderHdr->ship_to_city,
            'in_notes'             => $orderHdr->in_notes,
            'cus_notes'            => $orderHdr->cus_notes,
            'csr_notes'            => $orderHdr->csr_notes,
            'origin_odr_type'      => object_get($orderHdr, 'shippingOrder.type', ''),
            //Customer config
            'partial'              => $orderHdr->partial,
            // Country
            'ship_to_country'      => $orderHdr->ship_to_country,
            'ship_to_country_name' => object_get($orderHdr, 'systemCountry.sys_country_name', null),
            // State
            'ship_to_state'        => $orderHdr->ship_to_state,
            'ship_to_state_name'   => object_get($orderHdr, 'systemState.sys_state_name', null),
            'ship_to_zip'          => $orderHdr->ship_to_zip,
            'ship_by_dt'           => $this->dateFormat($orderHdr->ship_by_dt),
            'shipped_dt'           => empty($orderHdr->shipped_dt) ? null : $this->dateFormat($orderHdr->shipped_dt),
            'cancel_by_dt'         => $this->dateFormat($orderHdr->cancel_by_dt),
            'req_cmpl_dt'          => $this->dateFormat($orderHdr->req_cmpl_dt),
            'act_cmpl_dt'          => $this->dateFormat($orderHdr->act_cmpl_dt),
            'act_cancel_dt'        => $this->dateFormat($orderHdr->act_cancel_dt),
            'schd_ship_dt'         => $this->dateFormat($orderHdr->schd_ship_dt),
            'routed_dt'            => $this->dateFormat($orderHdr->routed_dt),
            'items'                => $objItems,
            'is_final_bol'         => isset($orderHdr->shipment->ship_sts) ? ($orderHdr->shipment->ship_sts == 'FN' ? true : false) : null,
            'spec_instruct'        => object_get($orderHdr, 'spec_instruct', '')
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date ? date("m/d/Y", $date) : null;
    }
}
