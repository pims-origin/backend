<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Edi;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class EdiModel extends AbstractModel
{
    public function __construct(Edi $model = null)
    {
        $this->model = ($model) ?: new Edi();
    }

    public function getEdiInfos()
    {
        $query = $this->model
            ->where('edi_sts', 'AC');

        $query = $query->get();

        return $query;
    }


}
