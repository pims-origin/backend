<?php

namespace App\Modules\Orders\Eloquents;

class OrderHdr extends BaseSoftModel
{
    protected $table = 'odr_hdr';

    protected $primaryKey = 'odr_id';

    protected $fillable = [
        'ship_id',
        'cus_id',
        'whs_id',
        'so_id',
        'odr_num',
        'cus_odr_num',
        'ref_cod',
        'cus_po',
        'odr_type',
        'odr_sts',
        'ship_to_name',
        'ship_to_email',
        'ship_to_add_1',
        'ship_to_add_2',
        'ship_to_city',
        'ship_to_state',
        'ship_to_zip',
        'ship_to_country',
        'carrier',
        'odr_req_dt',
        'act_cmpl_dt',
        'req_cmpl_dt',
        'ship_by_dt',
        'cancel_by_dt',
        'act_cancel_dt',
        'ship_after_dt',
        'shipped_dt',
        'cus_pick_num',
        'ship_method',
        'cus_dept_ref',
        'csr',
        'rush_odr',
        'cus_notes',
        'in_notes',
        'hold_sts',
        'back_odr_seq',
        'back_odr',
        'org_odr_id',
        'sku_ttl',
        'audit_sts',
        'wv_num',
        'is_ecom',
        'dlvy_srvc',
        'dlvy_srvc_code',
        'cosignee_sig',
        'carrier_code',
        'shipping_addr_id',
        'ship_to_phone',
        'schd_ship_dt',
        'routed_dt',
        'csr_notes',
        'spec_instruct',
        'track_num',
    ];

    public $dateFields = [
        'odr_req_dt',
        'req_cmpl_dt',
        'act_cmpl_dt',
        'ship_by_dt',
        'cancel_by_dt',
        'act_cancel_dt',
        'ship_after_dt',
        'shipped_dt',
    ];

    public function shipment()
    {
        return $this->hasOne(__NAMESPACE__ . '\Shipment', 'ship_id', 'ship_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function shippingOrder()
    {
        return $this->hasOne(__NAMESPACE__ . '\ShippingOrder', 'so_id', 'so_id');
    }

    public function csrUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'csr');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function originalOdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'org_odr_id', 'odr_id');
    }

    public function details()
    {
        return $this->hasMany(__NAMESPACE__ . '\OrderDtl', 'odr_id', 'odr_id');
    }

    public function oderCarton()
    {
        return $this->hasMany(__NAMESPACE__ . '\OrderCarton', 'odr_hdr_id', 'odr_id');
    }

    public function systemCountry()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemCountry', 'sys_country_code', 'ship_to_country');
    }

    public function systemState()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemState', 'sys_state_code', 'ship_to_state');
    }

    public function packHdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\PackHdr', 'odr_hdr_id', 'odr_id');
    }

    public function packDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\PackDtl', 'odr_hdr_id', 'odr_id');
    }

    public function OrderDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\OrderDtl', 'odr_id', 'odr_id');
    }

    public function updatedBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'updated_by');
    }

    public function openOrder()
    {
        return $this->hasOne(__NAMESPACE__ . '\OpenOrder', 'ord_id', 'odr_id');
    }

    public function returnHdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\ReturnHdr', 'odr_hdr_id', 'odr_id');
    }

    public function waveHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\WavepickHdr', 'wv_id', 'wv_id');
    }

}
