<?php

define('PAGING_LIMIT', 10);

define('TIME_ZONE', 'America/New_York');

define('DATE_FROMAT', 'm/d/Y');

define('OPEN_ORDERS_FROM_DATE', '06/29/2016');

define('OPEN_ORDERS_DAYS_ADD', 7);

define('OPEN_ORDERS_EXCLUDE_CLIENTS', [
    'Elite Brands',
]);

define('CTN_STS',[
    'Active' =>'AC',
    'Picked' =>'PD',
    'Locked' =>'LK',
    'PutBack'=>'PB'
    ]);
