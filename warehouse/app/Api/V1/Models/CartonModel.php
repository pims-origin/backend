<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 29-August-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class CartonModel extends AbstractModel
{
    /**
     * @param Carton $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    /**
     * @param $locationIds
     *
     * @return mixed
     */
    public function checkWhereIn($locationIds)
    {
        return $this->model
            ->whereIn('loc_id', $locationIds)
            ->count();
    }

    public function loadViewLayout($whsId, $with = [])
    {
        $query = $this->make($with)
            ->select([
                'loc_id',
                DB::raw('sum(piece_ttl) as piece_ttl'),
                DB::raw('sum(piece_remain) as piece_remain'),
                DB::raw('count(ctn_id) as ctn_ttl'),
                DB::raw('count(distinct item_id) as sku_ttl')
            ])
            ->where('whs_id', $whsId)
            ->whereNotNull('loc_id')
            ->groupBy('loc_id')
            ->get();

        return $query;
    }

    public function getLocationItems($whsId, $locId, $with = [])
    {
        $query = $this->make($with)
            ->select([
                'cartons.whs_id',
                'cartons.cus_id',
                'cartons.loc_id',
                'cartons.item_id',
                'cartons.ctn_pack_size',
                'cartons.color',
                'cartons.sku',
                'cartons.size',
                'cartons.lot',
                'cartons.po',
                'cartons.upc',
                'cartons.length',
                'cartons.width',
                'cartons.height',
                'cartons.weight',
                'cartons.cube',
                'cartons.volume',
                'cartons.ctn_uom_id',
                'cartons.uom_code',
                'cartons.uom_name',
                'cartons.lpn_carton',
                DB::raw('sum(piece_ttl) as piece_ttl'),
                DB::raw('sum(piece_remain) as piece_remain'),
                DB::raw('count(ctn_id) as ctn_ttl'),
                'loc.max_lpn',
                'pallet.plt_num',
            ])
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->join('location as loc', 'loc.loc_id', '=', 'cartons.loc_id')
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.ctn_sts', '!=', 'AJ')
            ->whereRaw('(cartons.loc_id = '.$locId.' or loc.parent_id = ' .$locId .' )')
            ->groupBy(['pallet.plt_num', 'item_id', 'lot', 'cartons.lpn_carton'])
            ->get();

        return $query;
    }

    public function cartonsByLocIds($locIds, $with = [])
    {
        $query = $this->make($with)
            ->select([
                'loc_id',
                DB::raw('sum(piece_remain) as ttl_piece_remain'),
                DB::raw('count(item_id) as ttl_item')
            ])
            ->where('deleted', 0)
            ->whereIn('loc_id', $locIds);

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('whs_id', $currentWH);

        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('cus_id', $cus_ids);

        $model = $query->get();

        return $model;
    }

    /**
     * @param $params
     * @param int $limit
     *
     * @return mixed
     */
    public function invReport($warehouseId, $params, $limit = 20)
    {
        $query = $this->make();
        $query->where('whs_id', $warehouseId);
        $query->leftJoin('customer', 'cartons.cus_id', '=', 'customer.cus_id');
        $query->leftJoin('odr_cartons', 'cartons.ctn_id', '=', 'odr_cartons.ctn_id');
        $query->leftJoin('users', 'odr_cartons.updated_by', '=', 'users.user_id');

        $query->where('odr_cartons.ctn_sts', 'SH');
        if (isset($params['cus_id']) && !empty($params['cus_id'])) {
            $query->where('cartons.cus_id', intval($params['cus_id']));
        }
        if (isset($params['odr_num']) && !empty($params['odr_num'])) {
            $query->where('odr_cartons.odr_num', $params['odr_num']);
        }
        if (isset($params['sku']) && !empty($params['sku'])) {
            $query->where('odr_cartons.sku', $params['sku']);
        }

        $query->select([
            'customer.cus_name',
            'odr_cartons.odr_num',
            'odr_cartons.item_id',
            'odr_cartons.sku',
            'odr_cartons.size',
            'odr_cartons.color',
            'odr_cartons.lot',
            'odr_cartons.upc',
            'odr_cartons.piece_qty',
            'odr_cartons.ship_dt',
            'users.first_name',
            'users.last_name',
        ]);

        $query->orderBy('odr_cartons.ship_dt', 'DESC');

//        $this->sortBuilder($query, $params);

        return $query->paginate($limit);

    }

    /**
     * @param int $itemId
     * @param int $locationId
     *
     * @return mixed
     */
    public function checkIsEcomByItemIdAndLocId($itemId, $locationId)
    {
        return $this->model
            ->where('item_id', $itemId)
            ->where('loc_id', '!=', $locationId)
            ->where('is_ecom', 1)
            ->count();
    }

    public function getExistLocId(){
        return $this->model->whereNotNull('loc_id')->get();
    }

    /**
     * @param array $params
     * @param array $with
     * @param int $limit
     * @param bool $export
     * @param $warehouseId
     *
     * @return mixed
     */
    public function skuReports($params = [], $with = [], $limit = 2000, $export = false, $warehouseId)
    {
        $params = SelArr::removeNullOrEmptyString($params);
        $query = $this->make($with)->leftJoin('system_uom', 'system_uom.sys_uom_id', '=', 'cartons.ctn_uom_id')
        //$query = $this->model;
        //$query = DB::table('cartons');
        ->addSelect([
            'item_id',
            'cus_id',
            'sku',
            'ctn_sts',
            'ctn_pack_size',
            'size',
            'color',
            'length',
            'width',
            'height',
            'weight',
            'cube',
            'system_uom.sys_uom_name as uom',
            'volume',
            'warehouse.whs_name',
            'cartons.ctn_pack_size',
            DB::raw("(SUM(cartons.piece_remain) ) AS piece_remain_ttl"),
            DB::raw("ROUND((SUM(cartons.piece_remain) ) / cartons.ctn_pack_size, 0) AS carton_ttl"),
//            DB::raw("(count(cartons.ctn_id) ) AS carton_ttl"),

    ]);
        if (isset($params['cus_id'])) {
            $query->where('cartons.cus_id', intval($params['cus_id']));
        }

        if (isset($params['sku'])) {
            $query->where('cartons.sku', 'like', "%" . SelStr::escapeLike($params['sku']) . "%");
        }

        $query->Join('warehouse', 'cartons.whs_id', '=', 'warehouse.whs_id');

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cusIds = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('cartons.cus_id', $cusIds);
        
        $query->where('cartons.whs_id', $currentWH);
        $query->whereIn('cartons.ctn_sts', ['AC', 'LK', 'PD']);
        $query->orderBy('cartons.updated_at');
        $query->groupBy('cartons.item_id', 'cartons.lot', 'cartons.ctn_sts');
        $this->sortBuilder($query, $params);

        if ($export) {
            return $query->get();
        } else {
            return $query->paginate($limit);
        }
    }
}