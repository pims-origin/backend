<?php

namespace App\Modules\Asns\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;
use App\Modules\Asns\Models\AsnModel;
use App\Modules\Asns\Resources\AsnResource;
use App\Modules\Asns\Resources\AsnCollection;
use App\Modules\Asns\Validators\AsnValidator;
use App\Modules\Asns\Resources\AsnReadResource;
use App\Modules\Asns\Validators\CreateValidator;
use App\Modules\Asns\Validators\UpdateValidator;

class AsnController extends Controller
{
    protected $asnModel;
    protected $asnValidator;
    protected $createValidator;
    protected $updateValidator;

    public function __construct(
        AsnModel $asnModel,
        AsnValidator $asnValidator,
        CreateValidator $createValidator,
        UpdateValidator $updateValidator
    )
    {
        $this->asnModel        = $asnModel;
        $this->asnValidator    = $asnValidator;
        $this->createValidator = $createValidator;
        $this->updateValidator = $updateValidator;
    }

    public function index($whsId, $cusId, Request $request)
    {
        $input = $request->all();

        $limit = array_get($input, 'per_page', 20);

        $baseUrl = str_replace( $request->root() , env('APP_URL'), \URL::current());

        $asns  = $this->asnModel->getListAsnByCus($whsId, $cusId, $limit, $baseUrl);

        $this->asnValidator->checkAsn($asns);

        return new AsnCollection($asns);
    }

    public function read($whsId, $cusId, $asnId, Request $request)
    {
        $asn  = $this->asnModel->getAsnDetail($whsId, $cusId, $asnId);

        $this->asnValidator->checkAsn($asn);

        return new AsnReadResource($asn);
    }

    public function create($whsId, $cusId, Request $request)
    {
        $input = $request->all();
        $input = array_merge($input, ['whs_id' => $whsId, 'cus_id' => $cusId]);

        $this->createValidator->validate($input);

        $asn = $this->asnModel->createAsn($input);

        $this->createValidator->checkCreateAsn($asn);

        return new AsnReadResource($asn);
    }

    public function update($whsId, $cusId, $asnHdrId, Request $request)
    {
        $input = $request->all();
        $input = array_merge($input, ['whs_id' => $whsId, 'cus_id' => $cusId, 'asn_hdr_id' => $asnHdrId]);

        $this->updateValidator->validate($input);

        $asn = $this->asnModel->updateAsn($input);

        $this->updateValidator->checkUpdateAsn($asn);

        return new AsnReadResource($asn);
    }
}