<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Reason;


class ReasonTransformer extends TransformerAbstract
{
    /**
     * @param Reason $reason
     *
     * @return array
     */
    public function transform(Reason $reason)
    {
        return [
            'r_id'   => $reason->r_id,
            'r_name' => $reason->r_name
        ];
    }
}
