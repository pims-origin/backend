<?php

namespace Seldat\Wms2\Models;

class ReportConfigs extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'report_configs';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'qualifier',
        'data',
        'ref',
        'des',
        'name',
        'level',
        'system',
        'sts',
        'url',
        'url_export',
        'tbl_ref'
    ];
}
