<?php

namespace common\models;

use yii\db\ActiveRecord;

class ResetPassword extends ActiveRecord
{
    public static function tableName()
    {
        return 'reset_password';
    }
}