<?php

namespace App\Api\Gate\Validators;


class GateDtlValidator extends AbstractValidator
{

    protected function rules()
    {
        return [
            'gate_id'      => 'integer|exists:gate,gate_id',
            'ctnr_num'     => 'max:50',
            'cus_id'       => 'integer|exists:customer,cus_id',
            'open'         => 'required',
            'gate_dtl_sts' => 'required',
        ];
    }
}
