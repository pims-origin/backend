<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderShippingTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderShippingTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $createdAt = date("m/d/Y", strtotime($orderHdr->created_at));

        return [
            'odr_id'          => $orderHdr->odr_id,
            'so_id'           => $orderHdr->so_id,

            // Warehouse
            'whs_id'          => $orderHdr->whs_id,
            'whs_name'        => object_get($orderHdr, 'warehouse.whs_name', null),
            'whs_code'        => object_get($orderHdr, 'warehouse.whs_code', null),

            // Customer
            'cus_id'          => $orderHdr->cus_id,
            'cus_name'        => object_get($orderHdr, 'customer.cus_name', null),
            'cus_code'        => object_get($orderHdr, 'customer.cus_code', null),

            // Order Status
            'odr_sts'         => $orderHdr->odr_sts,
            'odr_sts_name'    => Status::getByKey("Order-Status", $orderHdr->odr_sts),

            // User
            'csr_id'          => $orderHdr->csr,
            'csr_name'        => trim(object_get($orderHdr, 'csrUser.first_name', null) . " " .
                object_get($orderHdr, 'csrUser.last_name', null)),

            // Order Type
            'origin_odr_type' => object_get($orderHdr, 'shippingOrder.type', ''),
            'odr_type'        => $orderHdr->odr_type,
            'odr_type_name'   => Status::getByKey("Order-Type", $orderHdr->odr_type),

            'odr_num'      => $orderHdr->odr_num,
            'ship_to_name' => $orderHdr->ship_to_name,
            'ship_by_dt'   => $this->dateFormat($orderHdr->ship_by_dt),
            'shipped_dt'   => empty($orderHdr->shipped_dt) ? null : $this->dateFormat($orderHdr->shipped_dt),
            'cancel_by_dt' => $this->dateFormat($orderHdr->cancel_by_dt),
            'back_odr'     => $orderHdr->back_odr,
            'alloc_qty'    => $orderHdr->alloc_qty,
            'bol'          => Status::getByKey("SHIP-STATUS", $orderHdr->ship_sts)
                ? Status::getByKey("SHIP-STATUS", $orderHdr->ship_sts)
                : 'No Bol',
            'created_at'   => $createdAt,
            'created_by'   => trim(object_get($orderHdr, "createdBy.first_name", null) . " " .
                object_get($orderHdr, "createdBy.last_name", null))
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }
}
