<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\EventLookup;

class EventLookupTransformer extends TransformerAbstract
{
    //return result
    public function transform(EventLookup $eventLookup)
    {
        return [
            'evt_code' => $eventLookup->evt_code,
            'trans'    => $eventLookup->trans,
            'des'      => $eventLookup->des,
            'type'     => $eventLookup->type,
        ];
    }

}
