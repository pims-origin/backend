<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 08-Aug-16
 * Time: 14:21
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\UserDepartmentModel;
use App\Api\V1\Transformers\UserDepartmentTransformer;
use App\Api\V1\Validators\UserDepartmentValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class SystemUomController
 *
 * @package App\Api\V1\Controllers
 */
class UserDepartmentController extends AbstractController
{

    /**
     * @var $userDepartmentModel
     */
    protected $userDepartmentModel;

    /**
     * @var $userDepartmentTransformer
     */
    protected $userDepartmentTransformer;

    /**
     * @var $userDepartmentValidator
     */
    protected $userDepartmentValidator;

    /**
     * @var $userDepartmentDeleteMassValidator
     */
    protected $userDepartmentDeleteMassValidator;

    /**
     * UserDepartmentController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->userDepartmentModel = new UserDepartmentModel();
        $this->userDepartmentTransformer = new UserDepartmentTransformer();
        $this->userDepartmentValidator = new UserDepartmentValidator();
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        Request $request
    )
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $limit = array_get($input, 'limit');
            $department = $this->userDepartmentModel->search($input,['user'], $limit);
            if ($limit) {
                return $this->response->paginator($department, $this->userDepartmentTransformer);
            } else {
                return $this->response->collection($department, $this->userDepartmentTransformer);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->userDepartmentValidator->validate($input);

        $params = [
            'usr_dpm_code' => $input['usr_dpm_code'],
            'usr_dpm_name' => $input['usr_dpm_name'],
            'usr_dpm_des'  => !empty($input['usr_dpm_des']) ? $input['usr_dpm_des'] : ''

        ];

        try {
            // check duplicate Department Code
            $userDepartmentCode = $this->userDepartmentModel->checkWhere([
                'usr_dpm_code' => $params['usr_dpm_code']
            ]);
            if (!empty($userDepartmentCode)) {
                return $this->response->errorBadRequest(Message::get("BM006", "User Department Code"));
            }

            // check duplicate User Department Name
            $userDepartmentName = $this->userDepartmentModel->checkWhere([
                'usr_dpm_name' => $params['usr_dpm_name']
            ]);
            if (!empty($userDepartmentName)) {
                return $this->response->errorBadRequest(Message::get("BM006", "User Department Name"));
            }

            if ($userDepartment = $this->userDepartmentModel->create($params)) {
                return $this->response->item($userDepartment,
                    $this->userDepartmentTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $userDepartmentId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($userDepartmentId,  Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $userDepartment = $this->userDepartmentModel->getFirstBy('usr_dpm_id', $userDepartmentId);

            $arrUserDepartment = [
                'usr_dpm_code' => $userDepartment->usr_dpm_code,
                'usr_dpm_name' => $userDepartment->usr_dpm_name,
                'usr_dpm_des'  => $userDepartment->usr_dpm_des,
            ];

            $userOfDepartment = $this->userDepartmentModel->getUserByUserDepartmentId($userDepartmentId, $input);

            $arrUserDepartment['user_of_department'] = $userOfDepartment;

            $result = [
                'data'=> $arrUserDepartment
            ];

            return $result;

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $userDepartmentId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update($userDepartmentId, IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['usr_dpm_id'] = $userDepartmentId;
        // validation
        $this->userDepartmentValidator->validate($input);

        $params = [
            'usr_dpm_id'   => $userDepartmentId,
            'usr_dpm_code' => $input['usr_dpm_code'],
            'usr_dpm_name' => $input['usr_dpm_name'],
            'usr_dpm_des'  => !empty($input['usr_dpm_des']) ? $input['usr_dpm_des'] : ''
        ];

        try {
            // Check not exist
            if (!$this->userDepartmentModel->checkWhere(['usr_dpm_id' => $userDepartmentId])) {
                return $this->response->errorBadRequest(Message::get("BM017", "User Department"));
            }

            // check duplicate User Department Code
            $userDepartment = $this->userDepartmentModel->getFirstWhere([
                'usr_dpm_code' => $params['usr_dpm_code']
            ]);
            if (!empty($userDepartment) && $userDepartment->usr_dpm_id != $userDepartmentId) {
                return $this->response->errorBadRequest(Message::get("BM006", "User Department Code"));
            }

            // check duplicate User Department Name
            $userDepartment = $this->userDepartmentModel->getFirstWhere([
                'usr_dpm_name' => $params['usr_dpm_name']
            ]);
            if (!empty($userDepartment) && $userDepartment->usr_dpm_id != $userDepartmentId) {
                return $this->response->errorBadRequest(Message::get("BM006", "User Department Name"));
            }

            if ($userDepartment = $this->userDepartmentModel->update($params)) {
                return $this->response->item($userDepartment, $this->userDepartmentTransformer);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * API Delete User Department
     *
     * @param int $userDepartmentId
     *
     * @return Response|void
     */
    public function destroy($userDepartmentId)
    {
        //User Department cannot be removed while having users! (User Department has already user)
        if ($this->userDepartmentModel->getUserByUserDepartmentId($userDepartmentId)
        ) {
            return $this->response->errorBadRequest(Message::get("BM098","User Department" ));
        }

        try {
            if ($this->userDepartmentModel->deleteById($userDepartmentId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM017", "User Department"));
    }
}
