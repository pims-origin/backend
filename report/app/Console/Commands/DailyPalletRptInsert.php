<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class DailyPalletRptInsert extends AbstractCommand
{
    protected $signature   = 'daily-pallet-rpt-insert {today?}';
    protected $description = 'Daily Pallet Report Insert.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {
            $this->logSuccess('start-daily-pallet-rpt-insert');

            $today     = $this->argument('today') ? strtotime($this->argument('today')) : strtotime(date('Y-m-d', time()));
            $yesterday = ($today - 86400);

            $customerWhss = DB::table('customer_warehouse AS cw')
                ->select('cw.cus_id', 'cw.whs_id')
                ->where("cw.deleted", 0)
                ->orderBy('whs_id')
                ->get();

            if ($customerWhss) {
                foreach ($customerWhss as $cusWhs) {
                    $cusId = $cusWhs['cus_id'];
                    $whsId = $cusWhs['whs_id'];
                    $this->insertDailyPalletReport($whsId, $cusId, $today, $yesterday);
                }
            }

            $this->logSuccess('daily-pallet-rpt-insert');

        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }

    }

    private function insertDailyPalletReport($whsId, $cusId, $today, $yesterday)
    {
        return DB::transaction(function () use ($today, $whsId, $yesterday, $cusId) {
            $tmp = DB::table('daily_plt_rpt as p')
                ->select(
                    'p.whs_id',
                    'p.cus_id',
                    'p.item_id',
                    'p.description',
                    'p.sku',
                    'p.size',
                    'p.color',
                    'p.pack',
                    'p.uom',
                    'p.lot',
                    'p.plt_id',
                    'p.plt_num',
                    'p.loc_id',
                    'p.loc_code',
                    'p.spc_hdl',
                    'p.gr_dt',
                    'p.exp_dt',
                    DB::raw($today . ' as inv_dt'),
                    'p.is_rack',
                    DB::raw('p.cur_ctns as init_ctns'),
                    DB::raw('p.cur_qty as init_qty'),
                    'p.cur_ctns',
                    'p.cur_qty',
                    DB::raw(time() . ' as created_at'),
                    DB::raw(time() . ' as updated_at')
                )
                ->where('p.whs_id', '=', $whsId)
                ->where('p.cus_id', '=', $cusId)
                ->where('p.inv_dt', '=', $yesterday)
                ->whereNotExists(function ($q) use ($today) {
                    $q->select(DB::raw(1))->from('daily_plt_rpt as c')
                        ->whereRaw('p.whs_id = c.whs_id AND p.item_id = c.item_id AND p.lot = c.lot AND p.is_rack = c.is_rack')
                        ->where('c.inv_dt', '=', $today);
                })
                ->get();
                /*
                ->chunk(100, function ($pallets) {
                    return DB::table('daily_plt_rpt')->insert($pallets);
                });
                */
            return DB::table('daily_plt_rpt')->insert($tmp);
        });
    }
}
