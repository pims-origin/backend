<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportPallet;

class ReportPalletTransformer extends AbstractTransformer
{

    /**
     * @param ReportPallet $reportPallet
     * @return array
     */
    public function transform(ReportPallet $reportPallet)
    {
        return [
        	"plr_id"			=>	array_get($reportPallet, 'plr_id', null),
            "plt_id"			=>	array_get($reportPallet, 'plt_id', null),
            "cus_id"			=>	array_get($reportPallet, 'cus_id', null),
            "aisle"				=>	array_get($reportPallet, 'aisle', null),
            "level"				=>	array_get($reportPallet, 'level', null),
            "row"				=>	array_get($reportPallet, 'row', null),
            "cus_code"			=>	array_get($reportPallet, 'cus_code', null),
            "cus_name"			=>	array_get($reportPallet, 'cus_name', null),
            "gr_hdr_num"		=>	array_get($reportPallet, 'gr_hdr_num', null),
            "whs_id"			=>	array_get($reportPallet, 'whs_id', null),
            "plt_num"			=>	array_get($reportPallet, 'plt_num', null),
            "rfid"				=>	array_get($reportPallet, 'rfid', null),
            "plt_block"			=>	array_get($reportPallet, 'plt_block', null),
            "plt_tier"			=>	array_get($reportPallet, 'plt_tier', null),
            "loc_id"			=>	array_get($reportPallet, 'loc_id', null),
            "loc_name"			=>	array_get($reportPallet, 'loc_name', null),
            "loc_sts_code"		=>	array_get($reportPallet, 'loc_sts_code', null),
            "loc_code"			=>	array_get($reportPallet, 'loc_code', null),
            "created_at"		=>	$this->setTimeZone(array_get($reportPallet, 'whs_id', null), array_get($reportPallet->toArray(), 'created_at', null)),
            "created_by"		=>	array_get($reportPallet, 'created_by', null),
            "updated_at"		=>	$this->setTimeZone(array_get($reportPallet, 'whs_id', null), array_get($reportPallet->toArray(), 'updated_at', null)),
            "updated_by"		=>	array_get($reportPallet, 'updated_by', null),
            "deleted"			=>	array_get($reportPallet, 'deleted', null),
            "deleted_at"		=>	$this->setTimeZone(array_get($reportPallet, 'whs_id', null), array_get($reportPallet, 'deleted_at', null)),
            "gr_hdr_id"			=>	array_get($reportPallet, 'gr_hdr_id', null),
            "gr_dtl_id"			=>	array_get($reportPallet, 'gr_dtl_id', null),
            "ctn_ttl"			=>	array_get($reportPallet, 'ctn_ttl', null),
            "storage_duration"	=>	array_get($reportPallet, 'storage_duration', null),
            "plt_sts"			=>	array_get($reportPallet, 'plt_sts', null),
            "zero_date"			=>	array_get($reportPallet, 'zero_date', null),
            "return_id"			=>	array_get($reportPallet, 'return_id', null),
            "is_movement"		=>	array_get($reportPallet, 'is_movement', null),
            "dmg_ttl"			=>	array_get($reportPallet, 'dmg_ttl', null),
            "data"				=>	array_get($reportPallet, 'data', null),
            "current_ctns"		=>	array_get($reportPallet, 'current_ctns', null),
            "init_ctn_ttl"		=>	array_get($reportPallet, 'init_ctn_ttl', null),
            "ccn"				=>	array_get($reportPallet, 'ccn', null),
            "lot"				=>	array_get($reportPallet, 'lot', null),
            "item_id"			=>	array_get($reportPallet, 'item_id', null),
            "upc"				=>	array_get($reportPallet, 'upc', null),
            "description"		=>	array_get($reportPallet, 'description', null),
            "dmg_ctns"			=>	array_get($reportPallet, 'dmg_ctns', null),
            "sku"				=>	array_get($reportPallet, 'sku', null),
            "size"				=>	array_get($reportPallet, 'size', null),
            "color"				=>	array_get($reportPallet, 'color', null),
            "pack"				=>	array_get($reportPallet, 'pack', null),
            "mixed_sku"			=>	array_get($reportPallet, 'mixed_sku', null),
            "is_full"			=>	array_get($reportPallet, 'is_full', null),
            "current_pieces"	=>	array_get($reportPallet, 'current_pieces', null),
            "init_piece_ttl"	=>	array_get($reportPallet, 'init_piece_ttl', null),
            "weight"			=>	array_get($reportPallet, 'weight', null),
            "uom"				=>	array_get($reportPallet, 'uom', null),
            "spc_hdl_code"		=>	array_get($reportPallet, 'spc_hdl_code', null),
            "gr_dt"				=>	array_get($reportPallet, 'gr_dt', null),
            "ref"				=>	array_get($reportPallet, 'ref', null),
            "is_xdock"			=>	array_get($reportPallet, 'is_xdock', null),
            "expired_date"		=>	$this->setTimeZone(array_get($reportPallet, 'whs_id', null), array_get($reportPallet, 'expired_date', null)),
            "cat_code" 			=>	array_get($reportPallet, 'cat_code', null),
            'timezone'          =>  $this->getWhsTimeZone(array_get($reportPallet, 'whs_id', null)) ?: 'UTC±0',
        ];
    }
}
