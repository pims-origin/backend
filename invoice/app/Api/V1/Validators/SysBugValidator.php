<?php

namespace App\Api\V1\Validators;

class SysBugValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'date' => 'required',
            'api_name' => 'required',
            'error' => 'required',
        ];
    }
}
