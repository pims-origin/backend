<?php

namespace App\Api\V1\Traits;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\ShipmentModel;
use App\Api\V1\Models\OutPalletModel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Jobs\AutoUpdateDailyInventoryAndPalletReport;

use App\Jobs\InvoiceBillableJob;
use App\Jobs\ShippedOrderJob;
use App\Jobs\BillableLaborJob;

trait OrderFlowControllerTrait
{

    protected $req;

    public function orderFlow(
        $order_id,
        $dependency,
        $cus_id,
        $request,
        CustomerMetaModel $customerMetaModel,
        OrderHdrMetaModel $hdrMetaModel = null
    ) {
        $this->req = $request;
        try {
            $dpcy = $dependency;
            // get customer meta
            if (is_null($hdrMetaModel)) {
                $cus_meta = $customerMetaModel->getOrderFlow($cus_id);
            } else // get customer meta
            {
                $cus_meta = $hdrMetaModel->getOrderFlow($order_id);
            }

            $orderHdrModel = new OrderHdrModel();
            $csrFlow = $customerMetaModel->getFlow($cus_meta, 'CSR');
            $alcFlow = $customerMetaModel->getFlow($cus_meta, 'ALC');
            $NWPFlow = $customerMetaModel->getFlow($cus_meta, 'NWP');
            if ($dependency == 1) {
                // csr

                if (array_get($csrFlow, 'usage', -1) == 0) {
                    $odr = $orderHdrModel->getFirstWhere(['odr_id' => $order_id]);
                    $csr_default = $customerMetaModel->getPNP($cus_id, 'CSR', object_get($odr, 'whs_id', 0));
                    if ($csr_default != 0) {
                        $csr_status = $this->CSRFlow($order_id, $csr_default);

                        if ($csr_status != 200) {
                            //$orderHdrModel->deleteOrderHdr($order_id);
                           // throw new \Exception($csr_status);

                        }
                    }


                }
            }

            if ($dependency == 2) {
                // allocated

                if (array_get($alcFlow, 'usage', -1) == 0) {
                    $alc_status = $this->ALCFlow($order_id);
                    if ($alc_status != 200) {
                        if (array_get($csrFlow, 'usage', -1) == 0) {
                            //$orderHdrModel->deleteOrderHdr($order_id);
                        }
                        //throw new \Exception($alc_status);
                    }

                }
            }
            if ($dependency == 3) {

                if (array_get($NWPFlow, 'usage', -1) == 0) {
                    $wp_status = $this->NWPFlow($order_id);
                    if ($wp_status != 200 && $dpcy == 1) {
                        $orderHdrModel->deleteOrderHdr($order_id);
                      //  throw new \Exception('Error occurred during create wave pick or assign picker');
                    }
                }
            }


        } catch (\Exception $exception) {
            throw  new \Exception($exception->getMessage());
        }

    }

    public function CSRFlow($order_id, $user_id)
    {
        $arrOrder[] = $order_id;
        $client = new Client();
        try {
            $client->request('PUT', env('API_ORDER') . 'assign-csr',
                [
                    'headers'     => ['Authorization' => $this->req->getHeader('Authorization')],
                    'form_params' => ["user_id" => $user_id, "odr_id" => $arrOrder]

                ]
            );

        } catch (RequestException $requestException) {
            return $this->getMessageError($requestException->getMessage());

        }

        return 200;
    }


    public function ALCFlow($order_id)
    {
        $arrOrder[] = $order_id;
        $ordDtl = new OrderDtlModel();
        $listDtl = $ordDtl->loadByOrdIds([$order_id]);
        $param = [];
        foreach ($listDtl as $dtl) {
            $dt = $dtl->toArray();
            $dt['itm_id'] = $dt['item_id'];
            $param[] = $dt;
        }

        $client = new Client();

        try {
            $client->request('PUT', env('API_ORDER') . 'allocates/' . $order_id,
                [
                    'headers'     => ['Authorization' => $this->req->getHeader('Authorization')],
                    'form_params' => $param
                ]
            );
        } catch (RequestException $requestException) {
            return $this->getMessageError($requestException->getMessage());

        }

        return 200;
    }

    public function NWPFlow($order_id)
    {
        $param[] = $order_id;

        $client = new Client();
        try {
            $res = $client->request('POST', env('API_WAVEPICK') . 'create-wave-pick',
                [
                    'headers'     => ['Authorization' => $this->req->getHeader('Authorization')],
                    'form_params' => ["odr_ids" => $param]

                ]
            );
        } catch (\Exception $exception) {

            //echo $exception->getMessage();

            return 400;
        }

        return $res->getStatusCode();
    }

    public function shippedFlow($odrIds, $request)
    {
        try {
            $arrOrder = [];
            foreach ($odrIds as $odrId) {
                $hdrMetaModel = new OrderHdrMetaModel();
                $customerMetaModel = new CustomerMetaModel();
                $cus_meta = $hdrMetaModel->getOrderFlow($odrId);
                $Flow = $customerMetaModel->getFlow($cus_meta, 'SH');
                if (array_get($Flow, 'usage', -1) == 0) {
                    $arrOrder[] = $odrId;
                }
            }

            if (!empty($arrOrder)) {
                $this->processShip($arrOrder);
            }

        } catch (\Exception $exception) {
            throw  new \Exception($exception->getMessage());
        }
    }

    public function checkPartial($order_id)
    {
        $hdrMetaModel = new OrderHdrMetaModel();
        $customerMetaModel = new CustomerMetaModel();
        $order_meta = $hdrMetaModel->getOrderFlow($order_id);
        $Flow = $customerMetaModel->getFlow($order_meta, 'POD');
        if (array_get($Flow, 'usage', -1) == 1) {
            return true;
        }

        return false;
    }

    private function processShip($orderIds, $shipDate = null, $request)
    {
        if (!is_array($orderIds)) {
            $orderIds = [$orderIds];
        }
        $orderHdrs = $this->orderHdrModel->loadByOrdIds($orderIds)->toArray();
        $imsShippedOrder = [];

        if (!$orderHdrs) {
            throw new \Exception(Message::get("BM017", "Orders"));
        }

        $stsStg = [
            Status::getByValue("Scheduled to Ship", "Order-Status"),
           // Status::getByValue("Partial Staging", "Order-Status")
        ];

        //$arrShpIds = [];
        //foreach ($orderHdrs as $orderHdr) {
        //    $arrShpIds[] = $orderHdr['ship_id'];
        //}
        //if (empty($arrShpIds)) {
        //    throw new \Exception(Message::get("BM017", "Shipping"));
        //}
        //
        //$shpMnts = (new ShipmentModel())->loadByShpIds($arrShpIds)->toArray();
        //
        //if (!$shpMnts) {
        //    throw new \Exception(Message::get('BM126'));
        //}
        //$arrShpFnlIds = [];
        //foreach ($shpMnts as $shpMnt) {
        //    if ($shpMnt['ship_sts'] === Status::getByValue("Final", "SHIP-STATUS")) {
        //        $arrShpFnlIds[] = $shpMnt['ship_id'];
        //    }
        //}

        try {
            //
            DB::beginTransaction();

            $currentWH = Data::getCurrentWhsId();
            $odrIds = [];

            foreach ($orderHdrs as $orderHdr) {

                if (!in_array($orderHdr['odr_sts'], $stsStg)
                    //|| empty($orderHdr['ship_id'])
                    //|| !in_array($orderHdr['ship_id'], $arrShpFnlIds)
                ) {
                    throw new \Exception(Message::get("BM157", 'Order', 'BOL'));
                }

                $odrSts = Status::getByValue("Shipped", "Order-Status");
                $currentDate = strtotime(date("Y-m-d") .  ' UCT');
                $shipDate = $shipDate ? $shipDate : $currentDate;
                $this->orderHdrModel->updateWhere([
                    'act_cmpl_dt' => time(),
                    'shipped_dt'  => $shipDate,
                    'updated_by'  => Data::getCurrentUserId(),
                    'odr_sts'     => $odrSts
                ], ['odr_id' => $orderHdr['odr_id']]);

                $odrIds[] = $orderHdr['odr_id'];

                $odrNum = array_get($orderHdr, 'odr_num');
                $currentWH = !empty($currentWH) ? $currentWH : array_get($orderHdr, 'whs_id');

                $evtCode = Status::getByKey("event", "PARTIAL-SHIPPED");
                $evtInfo = Status::getByKey("EVENT-INFO", "PRC");

                if ($odrSts === Status::getByValue("Shipped", "Order-Status")) {
                    $evtCode = Status::getByKey("event", "ORDER-SHIPPED");
                    $evtInfo = Status::getByKey("EVENT-INFO", "ORDER-SHIPPED");
                }

                //Remove location of out pallet
                $outPalletModel = new OutPalletModel();
                $listOutPallet = $outPalletModel->getOutPalletByOdrHdrId($orderHdr['odr_id']);
                if (count($listOutPallet) > 0) {
                    foreach ($listOutPallet as $outPallet) {
                        $outPallet->update([
                            'loc_id' => null,
                            'loc_name' => null,
                            'loc_code' => null
                        ]);
                    }
                }
                // Insert Event Tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $currentWH,
                    'cus_id'    => array_get($orderHdr, 'cus_id'),
                    'owner'     => $odrNum,
                    'evt_code'  => $evtCode,
                    'trans_num' => $odrNum,
                    'info'      => sprintf($evtInfo, $odrNum)
                ]);


                //---- Insert Orders Info for report module at status is SH = Shipped
                //insert multi orders
                $odr_Id = $orderHdr['odr_id'];
                $cusId = $orderHdr['cus_id'];
                $cusInfo = DB::Table('customer')
                    ->where('customer.cus_id', $cusId)
                    ->first();
                $orderDtlInfo = DB::Table('odr_dtl')
                    ->where('odr_dtl.odr_id', $odr_Id)
                    ->get();

                if($orderDtlInfo){
                    foreach ($orderDtlInfo as $orderDtl){
                        //to get cube
                        $itemId = array_get($orderDtl,'item_id','');
                        $itemInfo = DB::Table('item')
                            ->where('item.item_id', $itemId)
                            ->first();
                        $length = array_get($itemInfo, 'length', '');
                        $width = array_get($itemInfo, 'width', '');
                        $height = array_get($itemInfo, 'height', '');

                        //get ship_by
                        $updatedBy = array_get($orderHdr,'updated_by','');
                        $userInfo = DB::Table('users')
                            ->where('users.user_id', $updatedBy)
                            ->first();

                        //get sys_state_name
                        $shipToState = array_get($orderHdr,'ship_to_state','');
                        $systemStateInfo = DB::Table('system_state')
                            ->where('system_state.sys_state_code', $shipToState)
                            ->first();

                        //---- /Insert Orders Info for Shipping report at status is SH = Shipped
                        $dataShippingRpt = [
                            'cus_id'   => $cusId,
                            'cus_name' => array_get($cusInfo, 'cus_name', ''),
                            'cus_code' => array_get($cusInfo, 'cus_code', ''),
                            'whs_id'         => array_get($orderHdr, 'whs_id', ''),
                            'odr_id'        => array_get($orderHdr, 'odr_id', ''),
                            'odr_num'  => array_get($orderHdr, 'odr_num', ''),
                            'odr_type' => array_get($orderHdr, 'odr_type', ''),

                            'item_id' => array_get($orderDtl, 'item_id', ''),
                            'sku'     => array_get($orderDtl, 'sku', ''),
                            'size'    => array_get($orderDtl, 'size', ''),
                            'color'   => array_get($orderDtl, 'color', ''),
                            'lot'     => array_get($orderDtl, 'lot', ''),
                            'pack'    => array_get($orderDtl, 'pack', ''),

                            'cus_upc'       => array_get($orderDtl, 'cus_upc', ''),
                            'shipping_ctns' => round(array_get($orderDtl, 'picked_qty', '')/array_get($orderDtl, 'pack', ''),0),

                            'cube' => ROUND( (array_get($orderDtl, 'picked_qty', '')/array_get($orderDtl, 'pack', '')) *
                                ($length*$width*$height)/1728, 2 ),

                            'picked_qty'     => array_get($orderDtl, 'picked_qty', ''),
                            'shipped_dt'     => array_get($orderHdr, 'shipped_dt', ''),
                            'ship_by'        => array_get($userInfo, 'first_name', '').' '.array_get($userInfo, 'last_name', ''),
                            'cus_odr_num'    => array_get($orderHdr, 'cus_odr_num', ''),
                            'cus_po'         => array_get($orderHdr, 'cus_po', ''),
                            'ship_to_name'   => array_get($orderHdr, 'ship_to_name', ''),
                            'ship_to_add_1'  => array_get($orderHdr, 'ship_to_add_1', ''),
                            'ship_to_city'   => array_get($orderHdr, 'ship_to_city', ''),
                            'ship_to_state'  => array_get($orderHdr, 'ship_to_state', ''),
                            'sys_state_name' => array_get($systemStateInfo, 'sys_state_name', ''),
                            'ship_to_zip'    => array_get($orderHdr, 'ship_to_zip', ''),
                            'carrier'        => array_get($orderHdr, 'carrier', ''),

                            'created_by'     => Data::getCurrentUserId(),
                            'updated_by'     => Data::getCurrentUserId(),
                            'deleted_at'     => 915148800,
                            'deleted'        => 0

                        ];
                        $this->shippingReportModel->refreshModel();
                        $this->shippingReportModel->create($dataShippingRpt);
                        //---- /Insert Orders Info for Shipping report at status is SH = Shipped


                        //---- Insert Order Info for SKU Tracking report
                        if (array_get($orderDtl, 'picked_qty', 0) > 0) {
                            $dataSKUTrackingRpt = [
                                'cus_id'   => $cusId,
                                'cus_name' => array_get($cusInfo, 'cus_name', ''),
                                'cus_code' => array_get($cusInfo, 'cus_code', ''),

                                'odr_id'        => array_get($orderHdr, 'odr_id', ''),
                                'trans_num'     => array_get($orderHdr, 'odr_num', ''),
                                'whs_id'        => array_get($orderHdr, 'whs_id', ''),
                                'po_ctnr'       => array_get($orderHdr, 'cus_po', ''),
                                'ref_cus_order' => array_get($orderHdr, 'ref_cod', ''),
                                'actual_date'   => !empty(array_get($orderHdr, 'ship_by_dt', '')) ? array_get($orderHdr,
                                    'ship_by_dt', '') : 0,

                                'item_id' => $itemId,
                                'sku'     => array_get($orderDtl, 'sku', ''),
                                'size'    => array_get($orderDtl, 'size', ''),
                                'color'   => array_get($orderDtl, 'color', ''),
                                'pack'    => empty(array_get($orderDtl, 'pack', '')) ? null : array_get($orderDtl,
                                    'pack', ''),
                                'lot'     => array_get($orderDtl, 'lot', ''),

                                'qty'  => (empty(array_get($orderDtl, 'qty', '')) ? 0 : array_get($orderDtl, 'qty',
                                        '') *
                                    (-1)),
                                'ctns' => ROUND(((empty(array_get($orderDtl, 'qty', '')) ? 0 : array_get($orderDtl,
                                            'qty', 0)) / (empty(array_get($orderDtl, 'pack', '')) ? 0 :
                                            array_get($orderDtl, 'pack', ''))) * (-1), 0),
                                'cube' => ROUND(((empty(array_get($orderDtl, 'qty', '')) ? 0 : array_get($orderDtl,
                                            'qty', '')) / (empty(array_get($orderDtl, 'pack', '')) ? 0 :
                                            array_get($orderDtl, 'pack',
                                                ''))) * (-1) * ($length * $width * $height) / 1728, 2),

                            ];

                            $this->SKUTrackingReportModel->create($dataSKUTrackingRpt)->toArray();
                        }

                        //---- /Insert Order Info for SKU Tracking report


                    }
                }
                //---- /Insert Orders Info for report module at status is SH = Shipped




            }

            //  Get Odr_dtl by OrderIds
            $this->orderDtlModel->refreshModel();
            $orderDtls = $this->orderDtlModel->loadByOrdIds($odrIds)->toArray();
            if (!$orderDtls) {
                throw new \Exception(Message::get("BM017", "Order Details"));
            }

            $arrItms = [];

            foreach ($orderDtls as $orderDtl) {
                $itmId = array_get($orderDtl, 'item_id');
                $Itemlots[$itmId][] = array_get($orderDtl, 'lot');
                $arrItms[$itmId] = array_get($orderDtl, 'alloc_qty', 0);
            }

            // Change request: update order carton & carton status - 25-11-2016
            $this->updateOrderCarton($orderIds, $shipDate);

            // Update inventory summary
            $strOdrIds = implode(',', $odrIds);
            $sqlStr = "IF(`%s` <=
                COALESCE(
                    ( SELECT SUM(odr_cartons.piece_qty)
                    FROM odr_cartons
                    WHERE invt_smr.item_id = odr_cartons.item_id
                        AND odr_cartons.lot = invt_smr.lot
                        AND odr_cartons.ctn_sts = 'SH'
                        AND odr_cartons.odr_hdr_id IN(%s) )
                ,0), 0 , `%s` -
                COALESCE(
                    ( SELECT SUM(odr_cartons.piece_qty)
                    FROM odr_cartons
                    WHERE invt_smr.item_id = odr_cartons.item_id
                        AND odr_cartons.lot = invt_smr.lot
                        AND odr_cartons.ctn_sts = 'SH'
                        AND odr_cartons.odr_hdr_id IN(%s) )
                ,0))
            ";

            $sqlPickQty = sprintf($sqlStr, 'picked_qty', $strOdrIds, 'picked_qty', $strOdrIds);
            $sqlTtl = sprintf($sqlStr, 'ttl', $strOdrIds, 'ttl', $strOdrIds);

            foreach ($Itemlots as $itemId => $lots) {
                $this->invtSmrModel->getModel()
                    ->where('whs_id', $currentWH)
                    ->where('item_id', $itemId)
                    ->whereIn('lot', $lots)
                    ->update([
                        'picked_qty' => DB::raw($sqlPickQty),
                        'ttl'        => DB::raw($sqlTtl)
                    ]);
            }

            /*$this->invtSmrModel->updateWhere([
                'picked_qty' => DB::raw($sqlPickQty),
                'ttl'        => DB::raw($sqlTtl)
            ], [
                'whs_id' => $currentWH
            ]);*/


            DB::commit();
            //Create Billable for Invoice
            if (!empty($orderIds)) {
                foreach ($odrIds as $odrId) {
                    dispatch(new InvoiceBillableJob($odrId, $request));
                }
            }

            if(!empty($orderHdrs)){
                foreach ($orderHdrs as $orderHdr) {
                    //Update Daily Inventory and Pallet report
                    //dispatch(new AutoUpdateDailyInventoryAndPalletReport($currentWH, $orderHdr['cus_id'], strtotime(date('Y-m-d'))));
                }
            }

            //push MQ receiving
            if ($this->customerConfigModel->checkWhere([
                'whs_id'       => $currentWH,
                'cus_id'       => array_get($orderHdrs[0], 'cus_id'),
                // 'config_name'  => config('constants.cus_config_name.EDI_INTEGRATION'),
                'config_value' => config('constants.cus_config_value.EDI945'),
                'ac'           => config('constants.cus_config_active.YES')
            ])
            ) {
                $reportDetail = $this->reportModel->shipped($orderIds);
                $this->reportService->init($reportDetail);
                $this->reportService->process();
            }

            // Send data to IMS
            dispatch(new ShippedOrderJob($orderIds));

            return ['data' => 'Selected Orders are shipped successfully'];

        } catch (\PDOException $e) {
            DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function updateOrderCarton($orderIds, $shipDate)
    {
        if (!is_array($orderIds)) {
            $orderIds = [$orderIds];
        }
        foreach ($orderIds as $orderId) {
            $this->orderCartonModel->refreshModel();
            $this->orderCartonModel->updateWhere([
                'ship_dt' => $shipDate,
                'sts'     => 'u',
                'ctn_sts' => Status::getByKey('CTN_STATUS', 'SHIPPED')
            ], [
                'odr_hdr_id' => $orderId
            ]);

            // check each order in order carton
            $this->orderCartonModel->refreshModel();
            $ctn_ids = $this->orderCartonModel->getModel()
                ->where(['odr_hdr_id' => $orderId])
                ->pluck('ctn_id')
                ->toArray()
            ;
//            if (count($ctn_ids) == 0) {
//                throw new \Exception(Message::get("BM017", "Order Carton"));
//            }
//
//            $this->cartonModel->getModel()
//                ->whereIn('ctn_id', $ctn_ids)
//                ->update([
//                    'ctn_sts'          => Status::getByKey('CTN_STATUS', 'SHIPPED'),
//                    'shipped_dt'       => $shipDate,
//                    'storage_duration' => CartonModel::getCalculateStorageDurationRaw()
//                ]);

            if (count($ctn_ids) > 0) {
                foreach (array_chunk($ctn_ids, 200) as $chunkCtnIds) {
                    $this->cartonModel->getModel()
                        ->whereIn('ctn_id', $chunkCtnIds)
                        ->update([
                            'ctn_sts'          => Status::getByKey('CTN_STATUS', 'SHIPPED'),
                            'shipped_dt'       => $shipDate,
                            'storage_duration' => CartonModel::getCalculateStorageDurationRaw()
                        ]);
                }
            }
        }
    }

    public function getMessageError($message)
    {
        $error = explode('"message":"', $message);
        $error = explode('","', array_get($error, '1', ''));

        return array_get($error, '0', '');
    }
}