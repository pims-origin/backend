<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class LocationModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function listMzLocation($attributes = [], $limit = null)
    {
        $currentWH = Data::getCurrentWhsId();

        $query = $this->model
            ->where('loc_whs_id', $currentWH)
            ->where('loc_sts_code', Status::getByKey("LOCATION_STATUS", "ACTIVE"));

        $query->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('loc_type.loc_type_code', 'ECO');

        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes['loc_code'])) {
            $query->where('loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        if (!empty($attributes['sort']) && is_array($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy('location.'.$key, $order);
                }
            }
        }

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function listActMzLocation($attributes = [], $with = [], $limit = null)
    {
        // get current warehouse
        $currentWH = Data::getCurrentWhsId();

        $query = $this->make($with);

        $query->where('loc_whs_id', $currentWH)
            ->where('loc_sts_code', 'AC');

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['loc_code'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                    break;
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        $query->whereHas("locationType", function ($q) {
            $q->where('loc_type_code', 'like', 'eco');
        });

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function listItemLocation($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            $query->whereHas("pallet", function ($q) use ($attributes) {
                $q->where('cus_id', 'like', '%' . array_get($attributes, 'cus_id', '') . '%');
            });

            $query->whereHas("pallet.carton", function ($q) use ($attributes) {
                $q->where('item_id', 'like', '%' . array_get($attributes, 'item_id', '') . '%');
                $q->where(function ($q1) {
                    $q1->where('is_damaged', 0);
                    $q1->orWhereNull('is_damaged');
                });
            });

            $query->whereHas("pallet.carton.item", function ($q) use ($attributes) {
                $q->where('sku', 'like', '%' . array_get($attributes, 'sku', '') . '%');
                $q->where('cus_upc', 'like', '%' . array_get($attributes, 'cus_upc', '') . '%');
            });
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        $query->where('loc_whs_id', $currentWH);

        $query->whereHas("locationType", function ($q) {
            $q->where('loc_type_code', '<>', 'eco');
        });

        // Get
        $models = $query->paginate($limit);

        return $models;

    }

    public function listLocationforItem($input, $limit = null)
    {
        //get current warehouse of user
        $currentWH = Data::getCurrentWhsId();

        $attributes = SelArr::removeNullOrEmptyString($input);

        $query = $this->model
            ->select(
                'location.loc_sts_code',
                'location.loc_id',
                'location.loc_code',
                DB::raw('SUM(cartons.piece_remain) as ttl_pieces'),
                DB::raw('COUNT(DISTINCT cartons.ctn_id) as ctn_ttl')
            )
            ->where('location.loc_whs_id', $currentWH)
            ->where('location.loc_sts_code', 'AC');

        $query->join('cartons', 'location.loc_id', '=', 'cartons.loc_id');

        // check deleted cartons
        $query->where('cartons.deleted', 0);

        // check damaged cartons
        $query->where('cartons.is_damaged', 0);

        // check loc type is RAC
        $query->where('cartons.loc_type_code', 'RAC');

        // filter by param [cus_id, item_id, sku, cus_upc]
        if (!empty($attributes['cus_id'])) {
            $query->where('cartons.cus_id', $attributes['cus_id']);
        }
        if (!empty($attributes['item_id'])) {
            $query->where('cartons.item_id', $attributes['item_id']);
        }
        if (!empty($attributes['sku'])) {
            $query->where('cartons.sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }
        if (!empty($attributes['cus_upc'])) {
            $query->where('cartons.upc', 'like', "%" . SelStr::escapeLike($attributes['cus_upc']) . "%");
        }

        //ignore location type XDOCK
        $query->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->where('loc_type.loc_type_code', '<>', 'XDK');

        // group by
        $query->groupBy('location.loc_id');

        // sort
        $query->orderBy('location.loc_code');

        // Get
        $models = $query->paginate($limit);

        return $models;

    }

    /**
     * @param $cus_id
     *
     * @return mixed
     */
    public function checkLocationBelongToCustomerAndCurrentWH($cus_id, $loc_id)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table('location')
            ->leftJoin('customer_zone', 'location.loc_zone_id', '=', 'customer_zone.zone_id')
            ->where('customer_zone.cus_id', $cus_id)
            ->where('location.loc_whs_id', $currentWH)
            ->where('location.loc_id', $loc_id)
            ->count();
    }
}
