<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Pallet;

class PutAwayItemUpdateTransformer extends TransformerAbstract
{
    /**
     * @param Pallet $pallet
     *
     * @return array
     */
    public function transform(Pallet $pallet)
    {
        $carton = object_get($pallet, 'carton', null);
        $item = object_get($carton->first(), 'item', null);
        $AsnDtl = object_get($carton->first(), 'AsnDtl', null);

        return [
            'item_id'           => object_get($item, 'item_id', null),
            'plt_id'            => object_get($pallet, 'plt_id', null),
            'plt_num'           => object_get($pallet, 'plt_num', null),
            'ctn_ttl'           => is_null($pallet) ? 0 : $carton->count(),
            'sku'               => object_get($item, 'sku', null),
            'color'             => object_get($item, 'color', null),
            'size'              => object_get($item, 'size', null),
            'lot'               => object_get($AsnDtl, 'asn_dtl_lot', null),
            'uom'               => object_get($item, 'systemUom.sys_uom_name', null),
            'length'            => object_get($AsnDtl, 'asn_dtl_length', null),
            'width'             => object_get($AsnDtl, 'asn_dtl_width', null),
            'height'            => object_get($AsnDtl, 'asn_dtl_height', null),
            'weight'            => object_get($AsnDtl, 'asn_dtl_weight', null),
            'asn_dtl_cus_upc'   => object_get($AsnDtl, 'asn_dtl_cus_upc', null),
            'asn_dtl_pack'      => object_get($AsnDtl, 'asn_dtl_pack', null),
            'assigned_loc_id'   => object_get($pallet, 'palletSuggestLocation.location.loc_id'),
            'assigned_loc_code' => object_get($pallet, 'palletSuggestLocation.location.loc_code'),
            'assigned_loc_name' => object_get($pallet, 'palletSuggestLocation.location.loc_alternative_name'),
            "actual_loc_code"   => object_get($pallet, "loc_code", null),
            "actual_loc_name"   => object_get($pallet, "loc_name", null),
            "actual_loc_id"     => object_get($pallet, "loc_id", null)

        ];
    }
}
