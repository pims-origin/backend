<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Models\GoodsReceiptDtlModel;
use App\Api\V1\Models\GoodsReceiptReportModel;
use App\Api\V1\Models\SKUTrackingReportModel;
use App\Api\V1\Models\LaborTrackingModel;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class AsnDtlModel extends AbstractModel
{

    /**
     * GoodsReceiptModel constructor.
     *
     * @param AsnHdr|null $model
     */
    protected $asnHdrModel;

    protected $itemModel;

    protected $grHdrModel;

    protected $grDtlModel;

    protected $cartonModel;

    protected $palletModel;

    protected $locationModel;

    protected $containerModel;

    protected $palletSuggestLocationModel;

    protected $eventTrackingModel;

    protected $inventoryModel;

    protected $SKUTrackingReportModel;

    protected $goodsReceiptReportModel;

    protected $reportLaborTrackingModel;

    public function __construct(
        AsnDtl $model = null,
        AsnHdrModel $asnHdr,
        ContainerModel $containerModel,
        Item $item,
        GoodsReceiptModel $goodsReceipt,
        GoodsReceiptDtlModel $goodsReceiptDetail,
        CartonModel $carton,
        PalletModel $pallet,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        EventTrackingModel $eventTrackingModel,
        InventoriesModel $inventoriesModel,
        LocationModel $location,
        SKUTrackingReportModel $SKUTrackingReportModel,
        GoodsReceiptReportModel $goodsReceiptReportModel,
        ReportLaborTrackingModel $reportLaborTrackingModel
    )
    {
        $this->model            = ($model) ? : new AsnDtl();
        $this->asnHdrModel      = $asnHdr;
        $this->itemModel        = $item;
        $this->grHdrModel       = $goodsReceipt;
        $this->grDtlModel       = $goodsReceiptDetail;
        $this->cartonModel      = $carton;
        $this->palletModel      = $pallet;
        $this->locationModel    = $location;
        $this->containerModel   = $containerModel;
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->inventoryModel   = $inventoriesModel;
        $this->SKUTrackingReportModel = $SKUTrackingReportModel;
        $this->goodsReceiptReportModel = $goodsReceiptReportModel;
        $this->reportLaborTrackingModel = $reportLaborTrackingModel;
    }

    /*
     * Make ASn Dtl, Gr Hdr, Gr Dtl, Pallet, Carton, Even Tracking
     * @param: asn_hdr_id, cus_id, sku, type, loc_code, qty, item_id
     * Return array [ is_error
     *                      =0. no error
     *                      =1. Error
     *                msg]
     *
     */

    public function createAsnOneStep($attribute) {
        DB::beginTransaction();

        $asnHdr = $this->asnHdrModel->getFirstWhere(['asn_hdr_id' => $attribute['asn_hdr_id']]);

        $user_id = Data::getCurrentUserId();
        $time = time();

        if(empty($asnHdr)) {
            return ["is_error" => 1,"msg" => "This Asn does not exist!"];
        }

        if(!in_array($asnHdr->asn_sts,['RG','NW'])) {
            return ["is_error" => 1,"msg" => "This Asn status is not receiving!"];
        }

//        $ctn_num = (!empty($asnHdr->asn_hdr_ref))?$asnHdr->asn_hdr_ref:$this->generateContainerNum();
//        if(!empty($asnHdr->asn_hdr_ref)) {
        $container = $this->containerModel->getFirstWhere(['ctnr_num' => $attribute['ctnr_num']]);
//        }
        if(empty($container)) {
            return ["is_error" => 1,"msg" => "This Container does not exist!"];
        }

//        if(empty($container) || empty($asnHdr->asn_hdr_ref)) {
//            $container = $this->containerModel->create(['ctnr_num' => $ctn_num]);
//        }

        $asnDtl = $this->getModel()
            ->where("asn_hdr_id", $attribute['asn_hdr_id'])
            ->where("item_id", $attribute['item_id'])
            ->first();

        $item = $this->itemModel->where("item_id", $attribute['item_id'])->where("cus_id", $asnHdr->cus_id)->first();
        if(empty($item)) {
            return ["is_error" => 1,"msg" => "This Item is not belong to this Customer!"];
        }
//        $ctn_qty = 0;
//        if($attribute['qty'] >= $item->pack) {
//            $ctn_qty = $attribute['qty'] / $item->pack;
//            $ctn_qty = (int) $ctn_qty;
//        }
//
//        if(($attribute['qty'] % $item->pack)  > 0) {
//            $ctn_qty ++;
//        }

        $ctn_qty = (!empty($attribute['carton_qty']))?$attribute['carton_qty']:0;
        $attribute['qty'] = $ctn_qty * $item->pack;

        //check ASN type from SIP
        $asn_meta = DB::table("asn_meta")
            ->where("asn_hdr_id", $attribute['asn_hdr_id'])->where("qualifier","SIP")->first();
        if(!empty($asn_meta) && empty($asnDtl)) {
            return ["is_error" => 1,"msg" => "This ASN do not have ASN detail!"];
        }

        if(!empty($asnDtl)) {
            if(empty($asn_meta)) {
                $asnDtl->asn_dtl_ctn_ttl +=  $ctn_qty;
                $asnDtl->asn_dtl_sts =  "RG";
                $asnDtl->asn_dtl_qty_ttl +=  $attribute['qty'];
                $asnDtl->save();
            }
        } else {
            $time = time();
            $asnDtlParam = [
                'asn_hdr_id'        => $asnHdr->asn_hdr_id,
                'item_id'           => $item->item_id,
                'ctnr_id'           => $container->ctnr_id,
                'ctnr_num'          => $container->ctnr_num,
                'asn_dtl_po'        => "PO".$time,
                'uom_id'            => $item->uom_id,
                'asn_dtl_sku'       => $item->sku,
                'asn_dtl_size'      => $item->size,
                'asn_dtl_color'     => $item->color,
                'uom_code'          => $item->uom_id,
                'uom_name'          => $item->uom_id,
                'asn_dtl_ctn_ttl'   => $ctn_qty,
                'asn_dtl_crs_doc'   => 0,
                'asn_dtl_des'       => "",
                'asn_dtl_sts'       => "RG",
                'asn_dtl_lot'       => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                'asn_dtl_length'    => $item->length,
                'asn_dtl_volume'    => $item->volume,
                'asn_dtl_weight'    => $item->weight,
                'asn_dtl_height'    => $item->height,
                'asn_dtl_width'     => $item->width,
                'asn_dtl_pack'      => $item->pack,
                'asn_dtl_qty_ttl'   => $attribute['qty']
            ];

            $asnDtl = $this->create($asnDtlParam);
        }

        //Update Asn type
        $asnHdr->asn_type = "1ST";
        if($asnHdr->asn_sts == "NW") {
            $asnHdr->asn_sts = "RG";
        }
        $asnHdr->save();

        // Do Good Receipt

        $grHdr = $this->grHdrModel->getModel()
            ->where('asn_hdr_id', $asnHdr->asn_hdr_id)
            ->whereIn("gr_sts", ['NW','RG','RE'])
            ->first();
        if(empty($grHdr)) {
            $grNumAndSeq = $this->generateGrNumAndSeq($asnHdr->asn_hdr_num, $asnHdr->asn_hdr_id);

            $grHdr = $this->grHdrModel->create([
                "ctnr_id"           => $container->ctnr_id,
                "asn_hdr_id"        => $asnHdr->asn_hdr_id,
                "gr_hdr_seq"        => $grNumAndSeq['gr_seq'],
                "gr_hdr_ept_dt"     => $asnHdr->asn_hdr_ept_dt,
                "gr_hdr_act_dt"     => $asnHdr->asn_hdr_ept_dt,
                "gr_hdr_num"        => $grNumAndSeq['gr_num'],
                "gr_sts"            => 'RG',
                "whs_id"            => $asnHdr->whs_id,
                "cus_id"            => $asnHdr->cus_id,
                "ctnr_num"          => $container->ctnr_num,
                "ref_code"          => (!empty($asnHdr->asn_hdr_ref))?$asnHdr->asn_hdr_ref:"",
                'created_from'      => 'GUN',
                "putaway"           => 0
            ]);
        }

        $grDtl = $this->grDtlModel->getModel()
            ->where("gr_hdr_id", $grHdr->gr_hdr_id)
            ->where("item_id", $item->item_id)
            ->whereIn("gr_dtl_sts", ['NW','RE','RG'])
            ->first();
        if(empty($grDtl)) {
            $grDtl = $this->grDtlModel->create([
                "asn_dtl_id"            => $asnDtl->asn_dtl_id,
                "gr_hdr_id"             => $grHdr->gr_hdr_id,
                "gr_dtl_ept_ctn_ttl"    => $ctn_qty,
                "gr_dtl_act_ctn_ttl"    => $ctn_qty,
                "gr_dtl_plt_ttl"        => 1,
                "gr_dtl_is_dmg"         => 0,
                "gr_dtl_sts"            => 'RG',
                "item_id"               => $item->item_id,
                "pack"                  => $item->pack,
                "sku"                   => $item->sku,
                "size"                  => $item->size,
                "color"                 => $item->color,
                "lot"                   => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                "po"                    => $asnDtl->asn_dtl_po,
                "uom_code"              => $item->uom_code,
                "uom_name"              => $item->uom_name,
                "uom_id"                => $item->uom_id,
                "gr_dtl_dmg_ttl"        => 0,
                "gr_dtl_act_qty_ttl"    => $attribute['qty'],
                "upc"                   => $item->upc,
                "ctnr_id"               => $container->ctnr_id,
                "ctnr_num"              => $container->ctnr_num,
                "length"                => $item->length,
                "width"                 => $item->width,
                "height"                => $item->height,
                "weight"                => $item->weight,
                "cube"                  => $item->cube,
                "volume"                => $item->volume,
                "crs_doc"               => 0,
                "crs_doc_qty"           => 0
            ]);
        } else {
            $grDtl->gr_dtl_plt_ttl++;
            $grDtl->gr_dtl_ept_ctn_ttl += $ctn_qty;
            $grDtl->gr_dtl_act_ctn_ttl += $ctn_qty;
            $grDtl->gr_dtl_sts = "RG";
            $grDtl->gr_dtl_act_qty_ttl += $attribute['qty'];
            $grDtl->save();
        }

        // make plt
        $plt_num = (!empty($attribute['plt_num']))?$attribute['plt_num']:$this->generatePltNum();
        if(!empty($attribute['loc_code'])) {
            $loc_plt_num = $this->palletModel->getFirstWhere(['loc_code'=>$attribute['loc_code']]);
            $plt_num = (!empty($loc_plt_num))?$loc_plt_num->plt_num:$plt_num;
        }

        $pallet = $this->palletModel->getFirstWhere([
            "plt_num" => $plt_num,
            "whs_id" => $asnHdr->whs_id,
            "loc_code" => $attribute['loc_code'],
            ]);

        if(empty($pallet)) {
            $pallet = $this->palletModel->create([
                "cus_id"            => $asnHdr->cus_id,
                "whs_id"            => $asnHdr->whs_id,
                "plt_num"           => $plt_num,
                "rfid"              => $plt_num,
                "gr_hdr_id"         => $grHdr->gr_hdr_id,
                "gr_dtl_id"         => $grDtl->gr_dtl_id,
                "ctn_ttl"           => $ctn_qty,
                "item_id"           => $item->item_id,
                "pack"              => $item->pack,
                "sku"               => $item->sku,
                "size"              => $item->size,
                "color"             => $item->color,
                "lot"               => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                "storage_duration"  => 0,
                "plt_sts"           => 'RG',
                "is_movement"       => 0,
                "uom"               => $item->uom_id,
                "dmg_ttl"           => 0,
                "init_ctn_ttl"      => $ctn_qty,
                "mixed_sku"         => 0,
                "is_full"           => 0,
                "init_piece_ttl"    => $attribute['qty']
            ]);
        } else {
            // Check pallet
            if(!empty($pallet->gr_hdr_id) != $grHdr->gr_hdr_id) {
                return ["is_error" => 1,"msg" => "This LPN is belong to another Good Receipt!"];
            }

            if($pallet->cus_id != $grHdr->cus_id) {
                return ["is_error" => 1,"msg" => "This LPN is belong to another Customer!"];
            }


            if(!in_array($pallet->plt_sts,['RG','NW'])) {
                return ["is_error" => 1,"msg" => "This LPN is not active!"];
            }

            $pallet->ctn_ttl   += $ctn_qty;
            $pallet->plt_sts    = "RG";
            $pallet->init_piece_ttl += $attribute['qty'];
            $pallet->gr_hdr_id  = $grHdr->gr_hdr_id;
            $pallet->gr_dtl_id  = $grDtl->gr_dtl_id;
            $pallet->cus_id     = $asnHdr->cus_id;
            $pallet->save();
        }

        // Make Carton
        $checkCarton = $this->cartonModel->getFirstWhere(['plt_id' => $pallet->plt_id, 'whs_id' => $asnHdr->whs_id]);
        if(!empty($checkCarton)) {
            if($checkCarton->cus_id != $asnHdr->cus_id) {
                return ["is_error" => 1,"msg" => "This LPN is belong to another Customer!"];
            }
        }

        $cartonList = [];
        $maxCTNnum = $this->cartonModel->_generateCtnNum();
        for($i= 0; $i<$ctn_qty; $i++) {
            $maxCTNnum = $this->cartonModel->getNewCtnNum($maxCTNnum);

            $cartonList[] = [
                "cus_id"                => $asnHdr->cus_id,
                "whs_id"                => $asnHdr->whs_id,
                "plt_id"                => $pallet->plt_id,
                "asn_dtl_id"            => $asnDtl->asn_dtl_id,
                "ctn_num"               => $maxCTNnum,
                "gr_dtl_id"             => $grDtl->gr_dtl_id,
                "gr_hdr_id"             => $grHdr->gr_hdr_id,
                "ctn_sts"               => 'RG',
                "item_id"               => $item->item_id,
                "ctn_pack_size"         => $item->pack,
                "sku"                   => $item->sku,
                "size"                  => $item->size,
                "color"                 => $item->color,
                "lot"                   => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                "ctn_uom_id"            => $item->uom_id,
                "is_damaged"            => 0,
                "piece_remain"          => $item->pack,
                "piece_ttl"             => $item->pack,
                "is_ecom"               => 0,
                "picked_dt"             => 0,
                "storage_duration"      => 0,
                "uom_code"              => $item->uom_code,
                "uom_name"              => $item->uom_name,
                "upc"                   => (!empty($item->cus_upc))?$item->cus_upc:"",
                "ctnr_id"               => $container->ctnr_id,
                "ctnr_num"              => $container->ctnr_num,
                "length"                => $item->length,
                "width"                 => $item->width,
                "height"                => $item->height,
                "weight"                => $item->weight,
                "cube"                  => $item->cube,
                "volume"                => $item->volume,
                "created_at"            => $time,
                "created_by"            => $user_id,
                "updated_at"            => $time,
                "updated_by"            => $user_id,
                "po"                    => $asnDtl->asn_dtl_po,
                "expired_dt"            => (!empty($attribute['expired_dt']))?strtotime($attribute['expired_dt']):null,
                "inner_pack"            => 0,
                "lpn_carton"            => $pallet->plt_num
            ];
        }
        if($attribute['qty'] % $item->pack > 0) {
            $cartonList[$ctn_qty - 1]['piece_remain']   = $attribute['qty'] % $item->pack;
            $cartonList[$ctn_qty - 1]['piece_ttl']      = $attribute['qty'] % $item->pack;
        }
        $this->cartonModel->getModel()->insert($cartonList);

        // Event Tracking
        $event[] = [
            'whs_id'    => $asnHdr->whs_id,
            'cus_id'    => $asnHdr->cus_id,
            'owner'     => $asnHdr->asn_hdr_num,
            'evt_code'  => 'ARV',
            'trans_num' => $asnHdr->asn_hdr_num,
            'info'      => sprintf("GUN - ASN %s receiving Sku %s quality %s", $asnHdr->asn_hdr_num,
                $item->sku, $attribute['qty']),
            "created_at"            => $time,
            "created_by"            => $user_id,
        ];

        $event[] = [
            'whs_id'    => $grHdr->whs_id,
            'cus_id'    => $grHdr->cus_id,
            'owner'     => $grHdr->gr_hdr_num,
            'evt_code'  => 'GRA',
            'trans_num' => $item->sku,
            'info'      => sprintf("GUN - Assign %s cartons of Sku %s to Pallet %s", $ctn_qty,
                $item->sku, $plt_num ),
            "created_at"            => $time,
            "created_by"            => $user_id,
        ];

        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->getModel()->insert($event);

        // PutAway
        $location = $this->locationModel->getFirstWhere([ "loc_code" => $attribute['loc_code'],'loc_whs_id' => $grHdr->whs_id ]);

        if (is_null($location->loc_zone_id)) {

            $zoneId = $this->locationModel->getDynZone($asnHdr->whs_id, $asnHdr->cus_id);
            if (!$zoneId) {
                return ["is_error" => 1,"msg" => "This Customer does not support dynamic zone."];
            }

            $location->loc_zone_id = $zoneId;
            $location->save();

            if(!empty($location->is_block_stack) && $location->is_block_stack == 1) {
                // Update zone to all children location
                $this->locationModel->getModel()->where("parent_id", $location->loc_id)->update(["loc_zone_id" => $zoneId]);
                $location_child = $this->locationModel->getFirstChildEmpty($location);
                if(!empty($location_child)) {
                    $location = $location_child;
                }
            }


            $zone = DB::table("zone")->where("zone_id", $zoneId)->first();
            $zone_num_of_loc = $zone['zone_num_of_loc'] + 1;
            DB::table("zone")->where("zone_id", $location->loc_zone_id)->update(['zone_num_of_loc'=> $zone_num_of_loc]);
        } else {
            $check_zone = DB::table("customer_zone")
                ->where("zone_id", $location->loc_zone_id)
                ->where("cus_id", $asnHdr->cus_id)
                ->where("deleted", 0)
                ->first();
            if(empty($check_zone)) {
                $msg = sprintf("%s is assigned a Zone but not belonged to this customer.",$location->loc_code);
                return ["is_error" => 1,"msg" => $msg];
            }

            if(!empty($location->is_block_stack) && $location->is_block_stack == 1) {
                $location_child = $this->locationModel->getFirstChildEmpty($location);
                if(!empty($location_child)) {
                    $location = $location_child;
                }
            }
        }

        $pal_loc = $this->palletModel->getFirstWhere(['loc_id' => $location->loc_id]);
        if(!empty($pal_loc)) {
            $msg = sprintf("Location %s had already pallet.",$location->loc_code);
            return ["is_error" => 1,"msg" => $msg];
        }

        if (!empty($pallet->loc_id)) {
            $msg = sprintf("Pallet %s has already in a location!", $pallet->plt_num);
            return ["is_error" => 1, "msg" => $msg];
        }

        //update location sts = RG
        $location->loc_sts_code = 'RG';
        $location->save();

        $pallet->loc_id = $location->loc_id;
        $pallet->loc_code = $location->loc_code;
        $pallet->loc_name = $location->loc_code;
        $pallet->plt_sts = 'RG';
        $pallet->save();

        $this->cartonModel->updateWhere(
            [
                'loc_id'        => $location->loc_id,
                'loc_code'      => $location->loc_code,
                'loc_type_code' => 'RAC',
                'loc_name'      => $location->loc_code,
            ],
            [
                'plt_id' => $pallet->plt_id,
            ]);

        // Update Inventory
//        $this->inventoryModel->updateInventory($grHdr->whs_id, $grHdr->cus_id,$item->item_id, $attribute['qty']);

        //add pallet suggest location
        $qtyCtn = [
            "qty"       => $attribute['qty'],
            "ctn_ttl"   => $ctn_qty
        ];
        $this->palletSuggestLocationModel->upsertWithGrDTl($pallet, $grDtl, $grHdr, $qtyCtn);

        $event = [
            'whs_id'    => $pallet['whs_id'],
            'cus_id'    => $pallet['cus_id'],
            'owner'     => $grHdr->gr_hdr_num,
            'evt_code'  => 'RFP',
            'trans_num' => $pallet['plt_num'],
            'info'      => sprintf("GUN - Update Pallet %s on location %s", $pallet['plt_num'],
                $location['loc_code']),
        ];

        //  Evt tracking
        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->create($event);

        if ($grHdr->gr_sts == 'RE') {
            GoodsReceipt::where([
                'gr_hdr_id' => $grHdr->gr_hdr_id
            ])
                ->whereRaw("
                        (
                            SELECT COUNT(pal_sug_loc.act_loc_id) 
                            FROM pal_sug_loc 
                            JOIN gr_dtl ON pal_sug_loc.`gr_dtl_id` = gr_dtl.`gr_dtl_id`
                            WHERE pal_sug_loc.act_loc_id IS NOT NULL
                                AND pal_sug_loc.gr_hdr_id = gr_hdr.gr_hdr_id
                                AND gr_dtl.`gr_dtl_sts` = 'RG'
                        ) = 
                        (
                            SELECT SUM(gr_dtl.gr_dtl_plt_ttl) FROM gr_dtl
                            WHERE gr_dtl.gr_hdr_id = gr_hdr.gr_hdr_id
                            AND gr_dtl.`gr_dtl_sts` = 'RG'
                        )
                    ")
                ->update(['gr_hdr.putaway' => 1]);
        } else {
            GoodsReceipt::where([
                'gr_hdr_id' => $grHdr->gr_hdr_id,
                'created_from' => 'GUN'
            ])
                ->whereRaw("
                        (
                            SELECT COUNT(pal_sug_loc.act_loc_id) 
                            FROM pal_sug_loc 
                            JOIN gr_dtl ON pal_sug_loc.`gr_dtl_id` = gr_dtl.`gr_dtl_id`
                            WHERE pal_sug_loc.act_loc_id IS NOT NULL
                                AND pal_sug_loc.gr_hdr_id = gr_hdr.gr_hdr_id
                                AND gr_dtl.`gr_dtl_sts` = 'RG'
                        ) = 
                        (
                            SELECT SUM(gr_dtl.gr_dtl_plt_ttl) FROM gr_dtl
                            WHERE gr_dtl.gr_hdr_id = gr_hdr.gr_hdr_id
                            AND gr_dtl.`gr_dtl_sts` = 'RG'
                        )
                    ")
                ->update(['gr_hdr.putaway' => 1]);
        }
        DB::commit();

        return [
            "is_error"  => 0,
            "items"     => [
                "sku"   => $item->sku,
                "upc"  => (!empty($item->cus_upc))?$item->cus_upc:$item->item_code,
            ],
            "qty"       => $attribute['qty'],
            "msg"       => "Submit Successfully"
        ];
    }

    public function createAsnTwoStep($attribute) {
        DB::beginTransaction();
        $asnHdr = $this->asnHdrModel->getFirstWhere(['asn_hdr_id' => $attribute['asn_hdr_id']]);

        $user_id = Data::getCurrentUserId();
        $time = time();

        if(empty($asnHdr)) {
            return ["is_error" => 1,"msg" => "This Asn does not exist!"];
        }

        if(!in_array($asnHdr->asn_sts,['RG','NW'])) {
            return ["is_error" => 1,"msg" => "This Asn status is not receiving!"];
        }

//        $ctn_num = (!empty($asnHdr->asn_hdr_ref))?$asnHdr->asn_hdr_ref:$this->generateContainerNum();
//        if(!empty($asnHdr->asn_hdr_ref)) {
        $container = $this->containerModel->getFirstWhere(['ctnr_num' => $attribute['ctnr_num']]);
//        }
        if(empty($container)) {
            return ["is_error" => 1,"msg" => "This Container does not exist!"];
        }

//        if(empty($container) || empty($asnHdr->asn_hdr_ref)) {
//            $container = $this->containerModel->create(['ctnr_num' => $ctn_num]);
//        }

        $asnDtl = $this->getModel()
            ->where("asn_hdr_id", $attribute['asn_hdr_id'])
            ->where("item_id", $attribute['item_id'])
            ->first();

        $item = $this->itemModel->where("item_id", $attribute['item_id'])->where("cus_id", $asnHdr->cus_id)->first();
        if(empty($item)) {
            return ["is_error" => 1,"msg" => "This Item is not belong to this Customer!"];
        }
//        $ctn_qty = 0;
//        if($attribute['qty'] >= $item->pack) {
//            $ctn_qty = $attribute['qty'] / $item->pack;
//            $ctn_qty = (int) $ctn_qty;
//        }
//
//        if(($attribute['qty'] % $item->pack)  > 0) {
//            $ctn_qty ++;
//        }

        $ctn_qty = (!empty($attribute['carton_qty']))?$attribute['carton_qty']:0;
        $attribute['qty'] = $ctn_qty * $item->pack;

        //check ASN type from SIP
        $asn_meta = DB::table("asn_meta")
            ->where("asn_hdr_id", $attribute['asn_hdr_id'])->where("qualifier","SIP")->first();
        if(!empty($asn_meta) && empty($asnDtl)) {
            return ["is_error" => 1,"msg" => "This ASN do not have ASN detail!"];
        }

        if(!empty($asnDtl)) {
            if(empty($asn_meta)) {
                $asnDtl->asn_dtl_ctn_ttl +=  $ctn_qty;
                $asnDtl->asn_dtl_sts =  "RG";
                $asnDtl->asn_dtl_qty_ttl +=  $attribute['qty'];
                $asnDtl->save();
            }
        } else {
            $time = time();
            $asnDtlParam = [
                'asn_hdr_id'        => $asnHdr->asn_hdr_id,
                'item_id'           => $item->item_id,
                'ctnr_id'           => $container->ctnr_id,
                'ctnr_num'          => $container->ctnr_num,
                'asn_dtl_po'        => "PO".$time,
                'uom_id'            => $item->uom_id,
                'asn_dtl_sku'       => $item->sku,
                'asn_dtl_size'      => $item->size,
                'asn_dtl_color'     => $item->color,
                'uom_code'          => $item->uom_id,
                'uom_name'          => $item->uom_id,
                'asn_dtl_ctn_ttl'   => $ctn_qty,
                'asn_dtl_crs_doc'   => 0,
                'asn_dtl_des'       => "",
                'asn_dtl_sts'       => "RG",
                'asn_dtl_lot'       => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                'asn_dtl_length'    => $item->length,
                'asn_dtl_volume'    => $item->volume,
                'asn_dtl_weight'    => $item->weight,
                'asn_dtl_height'    => $item->height,
                'asn_dtl_width'     => $item->width,
                'asn_dtl_pack'      => $item->pack,
                'asn_dtl_qty_ttl'   => $attribute['qty'],
                'prod_line'         => $attribute['product_line'],
                'cmp'               => $attribute['campaign'],
                'asn_dtl_po'        => $attribute['po_num'],
                'asn_dtl_po_dt'     => time()
            ];

            $asnDtl = $this->create($asnDtlParam);
        }
        //Update Asn type
        $asnHdr->asn_type = "2ST";
        if($asnHdr->asn_sts == "NW") {
            $asnHdr->asn_sts = "RG";
        }
        $asnHdr->save();

        // Do Good Receipt

        $grHdr = $this->grHdrModel->getModel()
            ->where('asn_hdr_id', $asnHdr->asn_hdr_id)
            ->whereIn("gr_sts", ['NW','RG','RE'])
            ->first();
        if(empty($grHdr)) {
            $grNumAndSeq = $this->generateGrNumAndSeq($asnHdr->asn_hdr_num, $asnHdr->asn_hdr_id);
           
            $grHdr = $this->grHdrModel->create([
                "ctnr_id"           => $container->ctnr_id,
                "asn_hdr_id"        => $asnHdr->asn_hdr_id,
                "gr_hdr_seq"        => $grNumAndSeq['gr_seq'],
                "gr_hdr_ept_dt"     => $asnHdr->asn_hdr_ept_dt,
                "gr_hdr_act_dt"     => $asnHdr->asn_hdr_ept_dt,
                "gr_hdr_num"        => $grNumAndSeq['gr_num'],
                "gr_sts"            => 'RG',
                "whs_id"            => $asnHdr->whs_id,
                "cus_id"            => $asnHdr->cus_id,
                "ctnr_num"          => $container->ctnr_num,
                "ref_code"          => (!empty($asnHdr->asn_hdr_ref))?$asnHdr->asn_hdr_ref:"",
                'created_from'      => 'GUN',
                "putaway"           => 0
            ]);
        }

        $cus_name = '';
        $cus_code = '';
        if(!empty($asnHdr->cus_id)) {
            $cusInfo = DB::table('customer')->where('cus_id', $asnHdr->cus_id)->first();
            $cus_name = array_get($cusInfo,'cus_name','');
            $cus_code = array_get($cusInfo,'cus_code','');
        }
        $grDtl = $this->grDtlModel->getModel()
            ->where("gr_hdr_id", $grHdr->gr_hdr_id)
            ->where("item_id", $item->item_id)
            ->whereIn("gr_dtl_sts", ['NW','RE','RG'])
            ->first();
        if(empty($grDtl)) {
            $grDtl = $this->grDtlModel->create([
                "asn_dtl_id"            => $asnDtl->asn_dtl_id,
                "gr_hdr_id"             => $grHdr->gr_hdr_id,
                "gr_dtl_ept_ctn_ttl"    => $ctn_qty,
                "gr_dtl_act_ctn_ttl"    => $ctn_qty,
                "gr_dtl_plt_ttl"        => 1,
                "gr_dtl_is_dmg"         => 0,
                "gr_dtl_sts"            => 'RG',
                "item_id"               => $item->item_id,
                "pack"                  => $item->pack,
                "sku"                   => $item->sku,
                "size"                  => $item->size,
                "color"                 => $item->color,
                "lot"                   => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                "po"                    => $asnDtl->asn_dtl_po,
                "uom_code"              => $item->uom_code,
                "uom_name"              => $item->uom_name,
                "uom_id"                => $item->uom_id,
                "gr_dtl_dmg_ttl"        => 0,
                "gr_dtl_act_qty_ttl"    => $attribute['qty'],
                "upc"                   => $item->upc,
                "ctnr_id"               => $container->ctnr_id,
                "ctnr_num"              => $container->ctnr_num,
                "length"                => $item->length,
                "width"                 => $item->width,
                "height"                => $item->height,
                "weight"                => $item->weight,
                "cube"                  => $item->cube,
                "volume"                => $item->volume,
                "crs_doc"               => 0,
                "crs_doc_qty"           => 0
            ]);

            //---- Insert for goods receipt report
            $cusId = $asnHdr->cus_id;
            $cusInfo = DB::Table('customer')
                ->where('customer.cus_id', $cusId)
                ->first();
            $discrepancy = $ctn_qty - $ctn_qty;

            $grSeq = DB::table('gr_hdr')->where('asn_hdr_id', $asnHdr->asn_hdr_id)->count();
            $grSeq++;

            $dataGrRpt = [
                "ctnr_id"           => $container->ctnr_id,
                "asn_hdr_id"        => $asnHdr->asn_hdr_id,
                "gr_sts"            => 'RG',
                "gr_hdr_seq"        => $grSeq,
                'cus_id'   => $cusId,
                'cus_name' => array_get($cusInfo, 'cus_name', ''),
                'cus_code' => array_get($cusInfo, 'cus_code', ''),

                'gr_hdr_id'  => $grHdr->gr_hdr_id,
                'gr_hdr_num' => $grHdr->gr_hdr_num,
                'whs_id'     => $asnHdr->whs_id,
                'ctnr_num'   => $container->ctnr_num,
                'ref_code'   => (!empty($asnHdr->asn_hdr_ref)) ? $asnHdr->asn_hdr_ref : "",

                'item_id'            => $item->item_id,
                'sku'                => $item->sku,
                'size'               => $item->size,
                'color'              => $item->color,
                'pack'               => $item->pack,
                'lot'                => (!empty($attribute['lot'])) ? $attribute['lot'] : 'NA',
                'gr_dtl_act_qty_ttl' => $attribute['qty'],
                'gr_dtl_act_ctn_ttl' => $ctn_qty,
                'gr_dtl_ept_ctn_ttl' => $ctn_qty,
                'gr_dtl_disc'        => $discrepancy,
                'gr_dtl_plt_ttl'     => 1,

                'length'         => $item->length,
                'width'          => $item->width,
                'height'         => $item->height,
                'weight'         => $item->weight,
                'volume'         => $item->volume,
                'crs_doc'        => 0,
                'gr_dtl_dmg_ttl' => 0,
                'asn_hdr_ept_dt' => $asnHdr->asn_hdr_ept_dt,
                'asn_hdr_act_dt' => $asnHdr->asn_hdr_act_dt,
                'gr_hdr_act_dt'  => $asnHdr->asn_hdr_ept_dt ?: time(),
                'cube'           => $item->cube,
            ];

            $grrInfo = $this->goodsReceiptReportModel->create($dataGrRpt)->toArray();
            //---- /Insert for goods receipt report
        } else {
            $grDtl->gr_dtl_plt_ttl++;
            $grDtl->gr_dtl_ept_ctn_ttl += $ctn_qty;
            $grDtl->gr_dtl_act_ctn_ttl += $ctn_qty;
            $grDtl->gr_dtl_act_qty_ttl += $attribute['qty'];
            $grDtl->gr_dtl_sts = "RG";
            $grDtl->save();
        }
        // make plt
        $plt_num = (!empty($attribute['plt_num']))?$attribute['plt_num']:null;
        if(!empty($attribute['loc_code'])) {
            $loc_plt_num = $this->palletModel->getFirstWhere(['loc_code'=>$attribute['loc_code']]);
            $plt_num = (!empty($loc_plt_num))?$loc_plt_num->plt_num:$plt_num;
        }
        $cusInfo = DB::table('customer')->where('cus_id',$asnHdr->cus_id)->first();
        $pallet = null;
        if($plt_num != null) {
            $pallet = $this->palletModel->getFirstWhere([
                "plt_num" => $plt_num,
                "whs_id" => $asnHdr->whs_id
            ]);
            if(empty($pallet)) {
                $pallet = $this->palletModel->create([
                    "cus_id"            => $asnHdr->cus_id,
                    "whs_id"            => $asnHdr->whs_id,
                    "plt_num"           => $plt_num,
                    "rfid"              => $plt_num,
                    "gr_hdr_id"         => $grHdr->gr_hdr_id,
                    "gr_dtl_id"         => $grDtl->gr_dtl_id,
                    "ctn_ttl"           => $ctn_qty,
                    "item_id"           => $item->item_id,
                    "pack"              => $item->pack,
                    "sku"               => $item->sku,
                    "size"              => $item->size,
                    "color"             => $item->color,
                    "lot"               => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                    "storage_duration"  => 0,
                    "plt_sts"           => 'RG',
                    "is_movement"       => 0,
                    "uom"               => $item->uom_id,
                    "dmg_ttl"           => 0,
                    "init_ctn_ttl"      => $ctn_qty,
                    "mixed_sku"         => 0,
                    "is_full"           => 0,
                    "init_piece_ttl"    => $attribute['qty']
                ]);
                if(!empty($grHdr)) {
                    $gr_hdr_num = array_get($grHdr,'gr_hdr_num','');
                }
            } else {
                // Check pallet
                if(!empty($pallet->gr_hdr_id) && $pallet->gr_hdr_id != $grHdr->gr_hdr_id) {
                    return ["is_error" => 1,"msg" => "This LPN is belong to another Good Receipt!"];
                }

                if($pallet->cus_id != $grHdr->cus_id && $pallet->plt_sts != 'NW') {
                    return ["is_error" => 1,"msg" => "This LPN is belong to another Customer!"];
                }


                if(!in_array($pallet->plt_sts,['RG','NW','AC'])) {
                    return ["is_error" => 1,"msg" => "This LPN is not active!"];
                }
                if($pallet->plt_sts == 'NW') {
                   $pallet->cus_id =  $asnHdr->cus_id;
                }
                $pallet->ctn_ttl   += $ctn_qty;
                $pallet->plt_sts    = "RG";
                $pallet->init_piece_ttl += $attribute['qty'];
                $pallet->gr_hdr_id  = $grHdr->gr_hdr_id;
                $pallet->gr_dtl_id  = $grDtl->gr_dtl_id;
                $pallet->cus_id     = $asnHdr->cus_id;
                $pallet->save();
            }
            DB::table("rpt_pallet")->insert([
                "plt_id"            => $pallet->plt_id,
                "cus_id"            => $asnHdr->cus_id,
                "whs_id"            => $asnHdr->whs_id,
                "plt_num"           => $plt_num,
                "rfid"              => $plt_num,
                "gr_hdr_id"         => $grHdr->gr_hdr_id,
                "gr_dtl_id"         => $grDtl->gr_dtl_id,
                "ctn_ttl"           => $ctn_qty,
                "item_id"           => $item->item_id,
                "pack"              => $item->pack,
                "sku"               => $item->sku,
                "size"              => $item->size,
                "color"             => $item->color,
                "lot"               => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                "description"       => $item->description,
                "storage_duration"  => 0,
                "plt_sts"           => 'RG',
                "is_movement"       => 0,
                "uom"               => $item->uom_id,
                "dmg_ttl"           => 0,
                "init_ctn_ttl"      => $ctn_qty,
                "mixed_sku"         => 0,
                "is_full"           => 0,
                "init_piece_ttl"    => $attribute['qty'],
                'cus_name'          => $cus_name,
                'cus_code'          => $cus_code,
                'current_ctns'      => $ctn_qty,                
                'created_at'        => time(),
                'updated_at'        => time(),
                'created_by'        => $user_id,
                'updated_by'        => $user_id,
                'deleted'           => 0,
                'deleted_at'        => 915148800
            ]);
        }


        // Make Carton
        if(!empty($pallet)) {
            $checkCarton = $this->cartonModel->getFirstWhere(['plt_id' => $pallet->plt_id, 'whs_id' => $asnHdr->whs_id]);
            if(!empty($checkCarton)) {
                if($checkCarton->cus_id != $asnHdr->cus_id) {
                    return ["is_error" => 1,"msg" => "This LPN is belong to another Customer!"];
                }
            }
        }
        $cartonList = [];
        $maxCTNnum = $this->cartonModel->_generateCtnNum();
        $current_pieces = 0;
        $listCartonNum = [];
        for($i= 0; $i<$ctn_qty; $i++) {
            $maxCTNnum = $this->cartonModel->getNewCtnNum($maxCTNnum);
            $listCartonNum[]= $maxCTNnum;
            $current_pieces += $item->pack;
            $cartonList[] = [
                "cus_id"                => $asnHdr->cus_id,
                "whs_id"                => $asnHdr->whs_id,
                "plt_id"                => (!empty($pallet->plt_id))?$pallet->plt_id:null,
                "asn_dtl_id"            => $asnDtl->asn_dtl_id,
                "ctn_num"               => $maxCTNnum,
                "gr_dtl_id"             => $grDtl->gr_dtl_id,
                "gr_hdr_id"             => $grHdr->gr_hdr_id,
                "ctn_sts"               => 'RG',
                "item_id"               => $item->item_id,
                "ctn_pack_size"         => $item->pack,
                "sku"                   => $item->sku,
                "size"                  => $item->size,
                "color"                 => $item->color,
                "lot"                   => (!empty($attribute['lot']))?$attribute['lot']:'NA',
                "ctn_uom_id"            => $item->uom_id,
                "is_damaged"            => 0,
                "piece_remain"          => $item->pack,
                "piece_ttl"             => $item->pack,
                "is_ecom"               => 0,
                "picked_dt"             => 0,
                "storage_duration"      => 0,
                "uom_code"              => $item->uom_code,
                "uom_name"              => $item->uom_name,
                "upc"                   => (!empty($item->cus_upc))?$item->cus_upc:"",
                "ctnr_id"               => $container->ctnr_id,
                "ctnr_num"              => $container->ctnr_num,
                "length"                => $item->length,
                "width"                 => $item->width,
                "height"                => $item->height,
                "weight"                => $item->weight,
                "cube"                  => $item->cube,
                "volume"                => $item->volume,
                "created_at"            => $time,
                "created_by"            => $user_id,
                "updated_at"            => $time,
                "updated_by"            => $user_id,
                "po"                    => $asnDtl->asn_dtl_po,
                "expired_dt"            => (!empty($attribute['expired_dt']))?strtotime($attribute['expired_dt']):null,
                "inner_pack"            => 0,
                "lpn_carton"            => $pallet->plt_num
            ];
        }
        if($attribute['qty'] % $item->pack > 0) {
            $cartonList[$ctn_qty - 1]['piece_remain']   = $attribute['qty'] % $item->pack;
            $cartonList[$ctn_qty - 1]['piece_ttl']      = $attribute['qty'] % $item->pack;
        }

        $splitCartons = array_chunk($cartonList, 100);

        foreach($splitCartons as $ctSplited) {
            $this->cartonModel->refreshModel();
            $this->cartonModel->getModel()->insert($ctSplited);
        }

        $cartons = $this->cartonModel->getModel()->select('ctn_id')
            ->where('plt_id', (!empty($pallet->plt_id))?$pallet->plt_id:null)
            ->where("gr_dtl_id", $grDtl->gr_dtl_id)
            ->whereIn('ctn_num',$listCartonNum)
            ->get();

        foreach ($cartons as $key => $carton) {
            $cartonList[$key]['ctn_id']     = $carton->ctn_id;
            $cartonList[$key]['cus_code']   = $cus_code;
            $cartonList[$key]['cus_name']   = $cus_name;
            $cartonList[$key]['gr_hdr_num'] = $grHdr->gr_hdr_num;
            $cartonList[$key]['gr_hdr_id']  = $grHdr->gr_hdr_id;
            $cartonList[$key]["plt_num"]    = $plt_num;
            $cartonList[$key]["plt_rfid"]   = $plt_num;
        }
        foreach ($cartonList as $key => $carton) {
            if (isset($carton['lpn_carton'])) {
                unset($cartonList[$key]["lpn_carton"]);
            }
        }

        $splitCartonRpt = array_chunk($cartonList, 100);

        foreach($splitCartonRpt as $ctSplited) {
            DB::table("rpt_carton")->insert($ctSplited);
        }
        
        DB::table("rpt_pallet")->where('plt_id', $pallet->plt_id)->update([
            'current_pieces'            =>  $current_pieces,
            'dmg_ctns'                  =>  0,
        ]);
        // Event Tracking
        $event[] = [
            'whs_id'    => $asnHdr->whs_id,
            'cus_id'    => $asnHdr->cus_id,
            'owner'     => $asnHdr->asn_hdr_num,
            'evt_code'  => 'ARV',
            'trans_num' => $asnHdr->asn_hdr_num,
            'info'      => sprintf("GUN - ASN %s receiving Sku %s quality %s", $asnHdr->asn_hdr_num,
                $item->sku, $attribute['qty']),
            "created_at"            => $time,
            "created_by"            => $user_id,
        ];

        $event[] = [
            'whs_id'    => $grHdr->whs_id,
            'cus_id'    => $grHdr->cus_id,
            'owner'     => $grHdr->gr_hdr_num,
            'evt_code'  => 'GRA',
            'trans_num' => $item->sku,
            'info'      => sprintf("GUN - Assign %s cartons of Sku %s to Pallet %s", $ctn_qty,
                $item->sku, (!empty($plt_num))?$plt_num:'' ),
            "created_at"            => $time,
            "created_by"            => $user_id,
        ];

        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->getModel()->insert($event);

        $this->setTrackingEndTime($asnHdr, $grHdr, $grDtl);
        
        DB::commit();

        return [
            "is_error"  => 0,
            "items"     => [
                "sku"   => $item->sku,
                "upc"  => (!empty($item->cus_upc))?$item->cus_upc:$item->item_code,
            ],
            "qty"       => $attribute['qty'],
            "msg"       => "Submit Successfully"
        ];
    }

    public function setTrackingEndTime($asnHdr, $grHdr, $grDtl)
    {
        $predis = new Data;
        $userId = $predis->getCurrentUserId();
        $whsId = $predis->getCurrentWhsId();

        $getStartData = [
            'user_id'       =>  $userId,
            'whs_id'        =>  $whsId,
            'cus_id'        =>  $asnHdr['cus_id'],
            'owner'         =>  $asnHdr['asn_hdr_num'],
            'trans_num'     =>  null,
            'trans_dtl_id'  =>  null,
            'end_time'      =>  null,
            'lt_type'       =>  'IB'
        ];

        $getStart = $this->reportLaborTrackingModel->getFirstWhere($getStartData, [], ['lt_id' => 'DESC']);
        if(!empty($getStart)) {
            $getStart->update([
                    'trans_num'     =>  $grHdr['gr_hdr_num'],
                    'trans_dtl_id'  =>  $grDtl['gr_dtl_id'],
                    'end_time'      =>  time()
                ]);
        }
                
    }

    /**
     * @param $asnNum
     * @param $asnHrdId
     *
     * @return array
     */
    private function generateGrNumAndSeq($asnNum, $asnHrdId)
    {
        $numOfGr = $this->grHdrModel->checkWhere(['asn_hdr_id' => $asnHrdId]);

        $result = [
            'gr_num' => '',
            'gr_seq' => '',
        ];

        $result['gr_seq'] = ($numOfGr) ? $numOfGr + 1 : 1;

        $grNum = str_replace(config('constants.asn_prefix'), config('constants.gr_prefix'), $asnNum);

        $result['gr_num'] = sprintf('%s-%s',
            $grNum,
            str_pad($result['gr_seq'], 2, '0', STR_PAD_LEFT));

        return $result;
    }

    protected function generateContainerNum() {
        return $this->containerModel->generateContainerNum();
    }

    protected function generatePltNum() {
        return $this->palletModel->generatePltNum();
    }

    protected function validateLPNFormat() {
        return $this->palletModel->generatePltNum();
    }
}