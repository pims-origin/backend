<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Zone;


class ZoneCapacityTransformer extends TransformerAbstract
{
    /**
     * @param Zone $zone
     *
     * @return array
     */
    public function transform(Zone $zone)
    {
        $totalLocation = object_get($zone, 'totalLocation', 0);
        $usedLocation = object_get($zone, 'usedLocation', 0);
        $percent = 0;
        if ($totalLocation) {
            $percent = $usedLocation / $totalLocation;
        }
        return [
            'zone_id'   => object_get($zone, 'zone_id', null),
            'zone_code' => object_get($zone, 'zone_code', null),
            'zone_name' => object_get($zone, 'zone_name', null),
            'total'     => intval($totalLocation),
            'used'      => intval($usedLocation),
            'percent'   => $percent * 100
        ];
    }
}