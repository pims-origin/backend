<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use \DateTime;
use App\Invoice;

use App\Api\V1\Models\SummaryModel;
use App\Api\V1\Models\InvoicingSummaryModel;
use App\Api\V1\Models\VolumeRatesModel;

use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\InvoicingSummary;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\RcvSum;


class StorageMasterSummaryModel extends AbstractModel
{
    /**
    * @var
    */
    protected $carton;
    protected $sum;
    protected $inv;
    protected $invSum;
    protected $vol;
    protected $plt;
    protected $event;
    protected $ivcSumModel;

    public $volRates = [];
    
    public $totals = [];


    /**
     * Constructor
    */

    public function __construct()
    {
        $this->carton = new Carton();
        $this->inv = new InvoicingSummary();
        $this->plt = new Pallet();
        $this->ivcSumModel = new InvoicingSummaryModel();

        $this->invSum = new SummaryModel();
        $this->vol = new VolumeRatesModel();
        $this->event = new EventTracking();

        $this->ordCtn = new OrderCarton();
        $this->rcvSum = new RcvSum();
    }

    /**
     * Populate Storage Charge codes for each customer on daily
    */

    public function storageChargeCodes()
    {
        $type = Invoice::ST;

        $palletUoms = [
            Invoice::$pallet,
            Invoice::$monthPallet,
            Invoice::$palletCurrent
        ];

        $rcvDate = $chgResults = $allUoms = $rcvRes =
                $uniqueUoms = $pieceRes = $lastSumPcs = $ordCtnRes = [];

        $prevDate = date('Y-m-d', strtotime('-1 days'));
        $curDate = date('Y-m-d');
            
        //get max date from invoice_summary for storage charge codes
        $maxStor = $this->ivcSumModel->getMaxDate($type);

        $lastDate = $maxStor->maxDate;

        if ($lastDate && $lastDate == $prevDate) {
            return [];
        }

        //get rcv_dt from cartons table if no entry for storage charge code in
        //invoice_summary table
        if (! $lastDate) {
            $minRcv = $this->event
                        ->select(DB::raw('MIN(FROM_UNIXTIME(created_at, "%Y-%m-%d")) AS minDate'))
                        ->where('evt_code', '=', config('constants.event.GR-COMPLETE'))
                        ->first();

            $rcvDate = $minRcv->minDate;
        }

        $lastSumDate = $lastDate ? $lastDate : $rcvDate;

        if (! $lastSumDate) {
            return [];
        }

        //get next date to populate
        $dateRes = $rcvDate ? $rcvDate : new \DateTime($lastSumDate . ' +1 day');
        $date = $rcvDate ? $dateRes : $dateRes->format('Y-m-d');

        if ($date == $curDate) {
            return [];
        }

        //get all storage charge codes
        $chgCodes = $this->invSum->getChargeCodes($type);


        $pieceChgCodeID = NULL;
        foreach($chgCodes as $row) {
            $uom = $row['system_uom']['uom'];

                $allUoms[] = $uom;
                $chgResults[$uom]['chg_code_id'][] = $row['chg_code_id'];
                $chgResults[$uom]['chg_type_id'] = $row['charge_type']['chg_type_id'];
                $chgResults[$uom]['sys_uom_id'] = $row['system_uom']['sys_uom_id'];

                if ($uom == Invoice::$piece) {
                    $pieceChgCodeID = $row['chg_code_id'];
                }
        }

        $uniqueUoms = array_unique($allUoms);

        //get the lastSumDate piece qty from invoice_summary table
        if ($lastDate && $pieceChgCodeID) {
            $pieceResult = $this->inv
                                ->select('whs_id', 'cus_id', 'dt', 'qty')
                                ->where('dt', '=', $lastDate)
                                ->where('chg_code_id', $pieceChgCodeID)
                                ->get()
                                ->toArray();


            foreach($pieceResult as $pcs) {
                $whs = $pcs['whs_id'];
                $cus = $pcs['cus_id'];
                $dt = $pcs['dt'];

                $pieceRes[$whs][] = $cus;

                $lastSumPcs[$whs][$cus][$dt] =  $pcs['qty'];
            }
        }

        //get rcv piece qty if any for the date we populating
        $rcvResult = $this->rcvSum
                        ->select('cus_id', 'whs_id', 'dt', 'pcs_val')
                        ->where('dt', '=', $date)
                        ->get()
                        ->toArray();

        foreach($rcvResult as $rcv) {
            $whs = $rcv['whs_id'];
            $cus = $rcv['cus_id'];
            $dt = $rcv['dt'];

            $rcvRes[$whs][$cus][$dt] =
                        isset($rcvRes[$whs][$cus][$dt]) ?
                        $rcvRes[$whs][$cus][$dt] : 0;

            $rcvRes[$whs][$cus][$dt] +=  $rcv['pcs_val'];
        }


        //query from inv_vol_rates  for each vol uom
        $volumeModel = $this->ivcSumModel->volumeRanges($type);

        //get the cartons data
        $cartonRes = $this->cartonsQuery($date);

        //get the cartons pieces ship date in odr_cartons table
        $ordCtnRes = $this->piecesQuery($lastSumDate);

        //get the pallets from pallets table
        $pallets = $this->palletsQuery($date);


        //CARTONS
        if ($cartonRes) {
            $this->getCartonResults([
                'cartonRes' => $cartonRes,
                'date' => $date,
                'uniqueUoms' => $uniqueUoms,
                'pieceRes'  => $pieceRes,
                'volumeModel' => $volumeModel
            ]);
        }

        //PIECES
        $this->getPieceResults([
                'pieceRes' => $pieceRes,
                'date' => $date,
                'ordCtnRes' => $ordCtnRes,
                'lastSumPcs'  => $lastSumPcs,
                'rcvRes' => $rcvRes
        ]);
      
        //PALLETS
        if ($pallets) {
            $this->getPalletResults([
                'pallets' => $pallets,
                'date' => $date,
                'palletUoms' => $palletUoms
            ]);
        }

   
        //populate invoice_summary table
        $this->populateSummary([
            'chgResults' => $chgResults, 
            'date' => $date
        ]);
    }
    
    /**
    * @param array params
    * @return array
    */

    static function uomCases($params)
    {
        $row = $params['row'];

        $cusID = $row['cus_id'];
        $whsID = $row['whs_id'];

        $pieceRes = getDefault($params['pieceRes'][$whsID], []);

        $value = 0;

        switch ($params['uom']) {
            case Invoice::$volume:
            case Invoice::$monthVolume:
            case Invoice::$volumeCurrent:
            case Invoice::$volumeImperial:
            case Invoice::$monthVolumeImperial:
            case Invoice::$volumeCurrentImperial:
                $value = $row['vol'];
                break;
            case Invoice::$volumeMetric:
            case Invoice::$monthVolumeMetric:
            case Invoice::$volumeCurrentMetric:
                $value = $row['volMetric'];
                break;
            case Invoice::$carton:
            case Invoice::$cartonCurrent:
                $value = 1;
                break;
            case Invoice::$piece:
                $value = in_array($cusID, $pieceRes) == 0 ? $row['uom'] : 0;
                break;
            case Invoice::$smallCarton:
            case Invoice::$mediumCarton:
            case Invoice::$largeCarton:
            case Invoice::$xlCarton:
            case Invoice::$xxlCarton:
            case Invoice::$smallCartonImperial:
            case Invoice::$mediumCartonImperial:
            case Invoice::$largeCartonImperial:
            case Invoice::$xlCartonImperial:
            case Invoice::$xxlCartonImperial:
                $row['volField'] = $row['vol'];
                return self::getVolRange($row, $params);
            case Invoice::$smallCartonMetric:
            case Invoice::$mediumCartonMetric:
            case Invoice::$largeCartonMetric:
            case Invoice::$xlCartonMetric:
            case Invoice::$xxlCartonMetric:
                $row['volField'] = $row['volMetric'];
                return self::getVolRange($row, $params);
        }

        return ['value' => $value];
    }

    
    /**
    * @param array row
    *
    * @return array
    */

    static function checkVolRange($row, $param)
    {
        $volModel = $param['volModel'];
        $size = $param['uom'];
        $type = $param['type'];
        $volField = $row['volField'];

        $custVolRate = $volModel->custVolumeRange($type, $row);

        if (! getDefault($custVolRate[$size]['min_vol'])) {
            return ['rate' => 0];
        }

        if ($volField > getDefault($custVolRate[$size]['min_vol'])
            &&  (
                // If less than max or there is no max
                $volField <= getDefault($custVolRate[$size]['max_vol'])
                || ! getDefault($custVolRate[$size]['max_vol'])
            )
        ) {

            return [
                'rangeUOM' => $size,
                'rate' => 1,
            ];
        }

        return ['rate' => 0];
    }

    
    /**
    * @param array row
    *
    * @return array
    */

    static function getVolRange($row, $params)
    {
        $volRes = self::checkVolRange($row, $params);

        return $volRes['rate'] ? [
            'overrideUOM' => $volRes['rangeUOM'],
            'value' => 1,
        ] : ['value' => 0];
    }
    
    
    /**
    * @param array data
    * @param array totals
    * @return array
    */
    
    public function getCartonResults($data)
    {
        $date = $data['date'];
        $uniqueUoms = $data['uniqueUoms'];
        $volumeModel = $data['volumeModel'];
        $type = Invoice::ST;
        $pieceRes = $data['pieceRes'];
      
        //batch process
        $cartonData = $data['cartonRes'];

        if (! $cartonData) {
            return;
        }

        $cartonRes = array_splice($cartonData, 0, 500);

        $data['cartonRes'] = $cartonData;

        //get total qty for each uoms
        foreach ($cartonRes as $row) {
            $custID = $row['cus_id'];
            $whsID = $row['whs_id'];
            $rcvDt = $row['rcv_dt'];
            $lastActiveDt = $row['last_active_dt'];

            if ($rcvDt > $date || $lastActiveDt < $date) {
                continue;
            }

            foreach($uniqueUoms as $uom) {

                $uomRes = self::uomCases([
                    'uom' => $uom,
                    'row' => $row,
                    'volModel' => $volumeModel,
                    'type' => $type,
                    'pieceRes' => $pieceRes
                ]);

                $finalUOM = getDefault($uomRes['overrideUOM'], $uom);

                $this->totals[$custID][$whsID][$finalUOM] =
                    isset($this->totals[$custID][$whsID][$finalUOM]) ?
                    $this->totals[$custID][$whsID][$finalUOM] : 0;

                $this->totals[$custID][$whsID][$finalUOM] += $uomRes['value'];
            }
        }
        
        $this->getCartonResults($data);
    }
    
    /**
    * @param array data
    * @param array totals
    * @return array
    */
    
    public function getPieceResults($data)
    {
        $pieceRes = $data['pieceRes'];
        $date = $data['date'];
        $ordCtnRes = $data['ordCtnRes'];
        $lastSumPcs = $data['lastSumPcs'];
        $rcvRes = $data['rcvRes'];
        
        $prev = new \DateTime($date . ' -1 day');
        $prevDt =  $prev->format('Y-m-d');
        $pcsUOM = Invoice::$piece;

        foreach ($pieceRes as $whsID => $res) {
            foreach ($res as $cusID) {

                $shipTotalQty = getDefault($ordCtnRes[$cusID][$whsID][$prevDt]);

                $lastSumQty = getDefault($lastSumPcs[$whsID][$cusID][$prevDt], 0);

                $rcvQty = getDefault($rcvRes[$whsID][$cusID][$date], 0);

                if (! $lastSumQty) {
                    continue;
                }

                $qty = $shipTotalQty ?  $lastSumQty - $shipTotalQty + $rcvQty
                                : $lastSumQty + $rcvQty;

                $this->totals[$cusID][$whsID][$pcsUOM] =
                        isset($this->totals[$cusID][$whsID][$pcsUOM]) ?
                        $this->totals[$cusID][$whsID][$pcsUOM] : 0;

                $this->totals[$cusID][$whsID][$pcsUOM] = $qty;
            }
        }
    }
    
    /**
    * @param array data
    * @param array totals
    * @return array
    */
    
    public function getPalletResults($data)
    {
       $date = $data['date'];
       $palletUoms = $data['palletUoms'];
       
       //batch process
        $palletsData = $data['pallets'];

        if (! $palletsData) {
            return;
        }

        $pallets = array_splice($palletsData, 0, 500);

        $data['pallets'] = $palletsData;

        //get total qty for pallet uom
        foreach ($pallets as $pal) {

            $custID = $pal['cus_id'];
            $whsID = $pal['whs_id'];
            $rcvDt = $pal['rcv_dt'];
            $lastActiveDt = $pal['last_active'];

            if ($rcvDt > $date || $lastActiveDt < $date) {
                continue;
            }

            foreach ($palletUoms as $pltUom) {
                $this->totals[$custID][$whsID][$pltUom] =
                    isset($this->totals[$custID][$whsID][$pltUom]) ?
                    $this->totals[$custID][$whsID][$pltUom] : 0;

                $this->totals[$custID][$whsID][$pltUom] += 1;
            }
        }
        
        $this->getPalletResults($data);
    }
    
    /**
    * @param date lastSumDate
    * @return array
    */
    
    public function cartonsQuery($sumDate)
    {
        $query = $this->carton
                    ->select('cus_id', 'whs_id',
                            'ctn_id AS ctn', 
                            'shipped_dt', 'ctn_pack_size AS uom',
                            DB::raw('FROM_UNIXTIME(gr_dt, "%Y-%m-%d") AS rcv_dt'),
                            DB::raw('IF(shipped_dt, FROM_UNIXTIME(shipped_dt, "%Y-%m-%d"),
                                         DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                                        ) AS last_active_dt'
                                    ),
                            DB::raw('cube AS vol'),
                            DB::raw('ROUND(cube * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8) AS volMetric')
                            )
                    ->where(DB::raw('IF(shipped_dt, FROM_UNIXTIME(shipped_dt, "%Y-%m-%d"),
                                        DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                                        )'
                                    ), '>=', $sumDate)
                    ->where(DB::raw('FROM_UNIXTIME(gr_dt, "%Y-%m-%d")'), '<=', $sumDate)
                    ->whereNotNull('gr_dt')
                    ->get()
                    ->toArray();
                     
        return $query;
    }
    
    
    /**
    * @param date lastSumDate
    * @return array
    */
    
    public function piecesQuery($sumDate)
    {
        $ordCtnRes = [];
        
        $ordCtnQuery = $this->ordCtn
                            ->join('cartons', 'cartons.ctn_id', '=', 'odr_cartons.ctn_id')
                            ->select('cartons.cus_id', 'cartons.whs_id',
                                      DB::raw('SUM(piece_qty) AS piece_qty'),
                                      DB::raw('FROM_UNIXTIME(ship_dt, "%Y-%m-%d") AS ship')
                                    )
                            ->where(DB::raw('FROM_UNIXTIME(ship_dt, "%Y-%m-%d")'), '>=', $sumDate)
                            ->groupBy('cus_id', 'whs_id', 'ship')
                            ->get();

        foreach($ordCtnQuery->toArray() as $row) {
            $cusID = $row['cus_id'];
            $whsID = $row['whs_id'];
            $shippedDt = $row['ship'];

            $ordCtnRes[$cusID][$whsID][$shippedDt] = $row['piece_qty'];
        }
        
        return $ordCtnRes;
    }
    
    
    /**
    * @param date lastSumDate
    * @return array
    */
    
    public function palletsQuery($sumDate)
    {
        $query = $this->plt
                        ->select('plt_id', 'cus_id', 'whs_id')
                        ->selectRaw('FROM_UNIXTIME(created_at, "%Y-%m-%d") AS rcv_dt,
                                     IF(zero_date, FROM_UNIXTIME(zero_date, "%Y-%m-%d"),
                                     DATE_SUB(CURDATE(), INTERVAL 1 DAY)) AS last_active'
                                   )
                        ->where(DB::raw('IF(zero_date, FROM_UNIXTIME(zero_date, "%Y-%m-%d"),
                                            DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                                            )'
                                       ), '>=', $sumDate)
                        ->where(DB::raw('FROM_UNIXTIME(created_at, "%Y-%m-%d")'), '<=', $sumDate) 
                        ->get()
                        ->toArray();  
        
        return $query;
    }
    
    /**
    * @param date totals
    * @return array
    */
    
    public function populateSummary($param)
    {
        $data = [];
        
        $chgResults = $param['chgResults'];
        $date = $param['date'];
        
        if (! $this->totals) {
            return;
        }
        
        //prepare the data to store into invoice_summary table
        foreach ($this->totals as $cus => $warehouse) {
            foreach ($warehouse as $whs => $charges) {
                foreach ($chgResults as $uom => $val) {
                    
                    foreach ($val['chg_code_id'] as $code) {
                        $data[] = [
                            'whs_id' => $whs,
                            'cus_id' => $cus,
                            'qty' => floatVal(getDefault($charges[$uom])),
                            'chg_type_id' => $val['chg_type_id'],
                            'chg_uom_id' => $val['sys_uom_id'],
                            'chg_code_id' => $code,
                            'dt' => $date,
                        ];
                    }
                }
            }
        }

        //insert into invoice_summary table
        DB::beginTransaction();

        if (! empty($data)) {
            foreach($data as $row) {
                //check integrity vialotion
                if (! $this->invSum->getFirstWhere([
                    'cus_id'      => $row['cus_id'],
                    'whs_id'      => $row['whs_id'],
                    'chg_type_id' => $row['chg_type_id'],
                    'chg_code_id' => $row['chg_code_id'],
                    'dt' => $row['dt']
                ])){
                    $this->inv->create($row);
                }
            }
        }
var_dump('storage charge code inserted for ' . $date);

        DB::commit();
    }
}
