<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;

class CycleCountIMSJob extends IMSJob
{
    protected $whsId;

    protected $cycleId;

    protected $cycleDtlIds;

    public function __construct($whsId, $cycleId, $cycleDtlIds)
    {
        $this->whsId = $whsId;
        $this->cycleId = $cycleId;
        $this->cycleDtlIds = $cycleDtlIds;
    }

    public function handle()
    {
        $endpoint = env('IMS_CYCLE_COUNT');

        // Check IMS Configuration
        if (empty($endpoint)) {
            $this->writeLogment('IMS Cycle Count', 'IMS Cycle Count Endpoint is not define!');

            return false;
        }
        $cycleHdr = DB::table('cycle_hdr')->where('cycle_hdr_id', $this->cycleId)->first();

        $cycleNum = $cycleHdr['cycle_num'];

        $cycleCount = DB::table('cycle_dtl')
            ->select([
                'cycle_dtl.whs_id',
                'cycle_dtl.cus_id',
                'cycle_dtl.sku',
                DB::raw('SUM(ABS(COALESCE(sys_qty,0)-COALESCE(act_qty,0)) * COALESCE(pack,0)) AS quantity_change'),
                DB::raw('SUM(COALESCE(act_qty,0) * COALESCE(pack,0)) AS updated_quantity'),
                //'cycle_dtl.lot',
                //'cycle_dtl.sys_qty',
                //'cycle_dtl.act_qty',
                //'cycle_dtl.pack'
                // 'cartons.expired_dt'
            ])
            // ->leftJoin('cartons', function($join) {
            //     $join->on('cartons.item_id', '=', 'cycle_dtl.item_id');
            //     $join->on('cartons.loc_id', '=', 'cycle_dtl.sys_loc_id');
            // })
            ->where('cycle_dtl.cycle_hdr_id', $this->cycleId)
            ->whereIn('cycle_dtl.cycle_dtl_id', $this->cycleDtlIds)
            ->groupBy('cycle_dtl.item_id')
            //->groupBy('cycle_dtl.lot')
            ->get();

        try {
            $token = $this->login();

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-type: application/json"
            ];

            $data = [
                'cyc_num' => $cycleNum
            ];

            $items = [];
            foreach ($cycleCount as $cc) {
                $items[] = [
                    'cus_id'            => array_get($cc, 'cus_id', null),
                    'sku'               => array_get($cc, 'sku', null),
                    //'lot'               => array_get($cc, 'lot', null),
                    'lot'               => null,
                    //'exp_date'          => !empty(array_get($cc, 'expired_dt', null)) ? date('Y/m/d', array_get($cc, 'expired_dt')) : null,
                    'exp_date'          => null,
                    //'quantity_change'   => abs(array_get($cc, 'sys_qty', 0) - array_get($cc, 'act_qty', 0)) * array_get($cc, 'pack', 0),
                    //'updated_quantity'  => abs(array_get($cc, 'act_qty', 0) ?? array_get($cc, 'sys_qty', 0)) * array_get($cc, 'pack', 0),
                    'quantity_change'   => array_get($cc, 'quantity_change', 0),
                    'updated_quantity'   => array_get($cc, 'updated_quantity', 0),
                ];
            }

            $data['items'] = $items;

            $endpoint = str_replace('{:whsId}', $this->whsId, $endpoint);

            $result = $this->call($endpoint, 'POST', json_encode(['data' => $data]), $header);

            $this->writeLogment('IMS Cycle Count ' . $cycleNum, 'URL: '. $endpoint .' --- Request: ' . json_encode(['data' => $data]) . ' --- Response: ' . $result);

        } catch (\Exception $e) {
            $this->writeLogment('IMS Cycle Count', $e->getMessage());
        }
    }
}