<?php

$router = $app->router;

$router->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['SELDAT KITTING API V1'];
        });

        $api->get('/kitting-statuses', ['action' => 'viewOrder', 'uses' => 'OrderHdrController@getKittingStatus']);
        $api->get('/kitting-putaway-list/{orderId:[0-9]+}', ['action' => 'viewOrder', 'uses' => 'OrderHdrController@getKittingPutAway']);

        // Order
        $api->post('/orders', ['action' => 'createOrder', 'uses' => 'OrderHdrController@store']);
        $api->get('/orders/{orderId:[0-9]+}', ['action' => 'viewOrder', 'uses' => 'OrderHdrController@show']);
        $api->put('/orders/{orderId:[0-9]+}', ['action' => 'editOrder', 'uses' => 'OrderHdrController@update']);
        $api->get('/orders', ['action' => 'viewOrder', 'uses' => 'OrderHdrController@search']);

        //Auto complete SKU
        $api->get('/auto-complete-sku', ['action' => 'viewOrder', 'uses' => 'ItemController@searchItem']);

        //Allocate
        $api->put('/allocate/{orderHdrId:[0-9]+}',
            ['action' => 'allocateOrder', 'uses' => 'OrderHdrController@allocate']);

        //Pack
        $api->put('/packs/auto/{orderHdrId:[0-9]+}', [
            'action' => "orderPacking",
            'uses'   => 'PackHdrController@autoStore'
        ]);

        //Wave Pick
        $api->group(['prefix' => 'wave-pick'], function ($api) {
            $api->get('/order-picking-list', [
                'action' => "updateOrderPicking",
                'uses'   => 'OrderPickingController@odrPickingList'
            ]);

            $api->get('wave-pick-detail/{wvHdrId:[0-9]+}', [
                'action' => "updateOrderPicking",
                'uses'   => 'OrderPickingController@detailWavePick'
            ]);

            $api->get('/list-suggest-location/{wvDtlId:[0-9]+}', [
                'action' => 'createWavePick',
                'uses'   => 'OrderPickingController@suggestLocation'
            ]);

            $api->post('/check-actual-locations', [
                'action' => 'createWavePick',
                'uses'   => 'OrderPickingController@checkActualLocations'
            ]);

            $api->put('/update-wave-pick/{wv_id:[0-9]+}/no-ecom', [
                'action' => "updateOrderPicking",
                'uses'   => 'OrderPickingController@updateWavePickNoEcom'
            ]);

            $api->get('/more-suggest-location', [
                'action' => 'createWavePick',
                'uses'   => 'OrderPickingController@moreSuggestLocation'
            ]);

            $api->get('suggest-actual-locations', [
                'action' => 'createWavePick',
                'uses'   => 'OrderPickingController@suggestActutalLocations'
            ]);

            $api->get('/print', [
                'action' => "createWavePick",
                'uses'   => 'OrderPickingController@printWavepick'
            ]);
        });

        //Put Back
        $api->group(['prefix' => 'put-back'], function ($api) {
            $api->get('/{orderHdrId:[0-9]+}/get-items', [
                'action' => "updatePutback",
                'uses'   => 'PutBackController@putBackDetail'
            ]);

            $api->get('/check-plt-rfid', [
                'action' => "updatePutback",
                'uses'   => 'PutBackController@validationPallet'
            ]);

            $api->put('/{orderHdrId:[0-9]+}', [
                'action' => "updatePutback",
                'uses'   => 'PutBackController@updatePutBack'
            ]);
        });
        $api->get('/location/{whsId:[0-9]+}/locations-by-customers/{cusId:[0-9]+}', [
            'action' => 'viewLocation',
            'uses'   => 'PutBackController@locationsByCustomer'
        ]);
    });
});
