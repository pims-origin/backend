<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Services\DailyInventoryService;

class DailyInventoryRpt extends AbstractCommand
{
    protected $signature   = 'daily-inventory-rpt {today?}';
    protected $description = 'Location Histogram successfully.';

    public function __construct()
    {
        parent::__construct();
        $this->dailyInventoryService = new DailyInventoryService();
    }

    public function handle()
    {
        try {
            $this->logSuccess('start-daily-inventory-rpt');

            $today     = $this->argument('today') ? $this->argument('today') : date('Y-m-d');

            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $customerWhss = DB::table('customer_warehouse AS cw')
                ->select('cw.cus_id', 'cw.whs_id')
                ->join('customer as c', 'c.cus_id', '=', 'cw.cus_id')
                ->where("c.deleted", 0)
                ->where("cw.deleted", 0)
                ->groupBy('cw.cus_id')
                ->get();

            foreach ($customerWhss as $cusWhs) {
                $cusId = $cusWhs['cus_id'];
                $whsId = $cusWhs['whs_id'];
                $this->dailyInventoryService->correctCustomerDataByDay($cusId, ['select_day' => $today]);
            }

            $this->logSuccess('daily-inventory-rpt');
        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }

    }
}
