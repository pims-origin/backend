<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CustomerSac;
use App\VasDtl;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\DB;

class WorkOrderDtlTransformer extends TransformerAbstract
{

    /**
     * ps:Show WorkOrder
     *
     * @param VasDtl $workOrderDtl
     *
     * @return array
     */
    public function transform(VasDtl $workOrderDtl)
    {
        $sac = (new CustomerSac())->getByCusSacId($workOrderDtl->cus_sac_id);
        $unit_uom = DB::table('inv_uom')->where('inv_uom_code', object_get($sac,'unit_uom', null))->first();
        $refTypeArr = array_flip(config('constants.vas_ref_type'));
        return [
            'vas_dtl_id'            => $workOrderDtl->vas_dtl_id,
            'ref_num'               => $workOrderDtl->ref_num,
            'ref_type'              => $refTypeArr[$workOrderDtl->ref_type],
            'vas_item'              => $workOrderDtl->vas_item,
            'vas_description'       => $workOrderDtl->vas_description,
            'qty'                   => $workOrderDtl->qty,
            'unit_price'            => $workOrderDtl->unit_price,
            'currency'              => $workOrderDtl->currency,
            'discount'              => $workOrderDtl->discount,
            'amount'                => $workOrderDtl->amount,
            'whs_id'                => object_get($sac,'whs_id', null),
            'type'                  => object_get($sac,'type', null),
            'cus_sac_id'            => object_get($sac,'cus_sac_id', null),
            'sac'                   => object_get($sac,'sac', null),
            'sac_name'              => object_get($sac,'sac_name', null),
            'description'           => object_get($sac,'description', null),
            'unit_uom'              => object_get($unit_uom,'inv_uom_des', null),
            'base_price'            => object_get($sac,'price', null),
        ];
    }
}
