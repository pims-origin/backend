<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\WarehouseZone;
use App\WarehouseZoneType;
use App\ZoneLocationStatus;
use App\Warehouse;

class ZoneTypeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * ZoneTypeTest constructor.
     */

    protected $zoneTypeParams = [
        'zone_type_name' => 'Zone Type Name',
        'zone_type_code' => 'AB',
        'zone_type_desc' => 'Test'
    ];

    private function getEndPoint()
    {
        return "/v1/zone-types";
    }

    public function testStore_NoParam_Fail()
    {
        $this->call('POST', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_HasParam_Ok()
    {
        // make data
        $endPoint = $this->getEndPoint();

        $response = $this->call('POST', $endPoint, $this->zoneTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
        $responseZone = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('zone_type_name', $responseZone['data']);
        $this->assertArrayHasKey('zone_type_code', $responseZone['data']);
        $this->assertArrayHasKey('zone_type_desc', $responseZone['data']);
    }

    public function testStore_DuplicateZoneTypeCode_Fail()
    {
        $endPoint = $this->getEndPoint();

        $this->call('POST', $endPoint, $this->zoneTypeParams);
        $this->call('POST', $endPoint, $this->zoneTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_DuplicateZoneTypeName_Fail()
    {
        $zoneType = factory(WarehouseZoneType::class)->create();
        $this->zoneTypeParams['zone_type_name'] = $zoneType->zone_type_name;
        $endPoint = $this->getEndPoint();

        $this->call('POST', $endPoint, $this->zoneTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testShow_Ok()
    {
        $zoneType = factory(App\WarehouseZoneType::class)->create();
        $endPoint = $this->getEndPoint() . '/' . $zoneType->zone_type_id;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testShow_ZoneTypeIdNotExisted_Fail()
    {
        $endPoint = $this->getEndPoint() . '/999999999';
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_Ok()
    {
        $zoneTypeId = factory(App\WarehouseZoneType::class)->create()->zone_type_id;
        $endPoint = $this->getEndPoint() . '/' . $zoneTypeId;

        $this->zoneTypeParams['zone_type_id'] = $zoneTypeId;

        $response = $this->call('PUT', $endPoint, $this->zoneTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
        $responseZone = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('zone_type_name', $responseZone['data']);
        $this->assertArrayHasKey('zone_type_code', $responseZone['data']);
        $this->assertArrayHasKey('zone_type_desc', $responseZone['data']);
    }

    public function testUpdate_NoParams_Fail()
    {
        $zoneType = factory(App\WarehouseZoneType::class)->create();
        $endPoint = $this->getEndPoint() . '/' . $zoneType->zone_type_id;
        $this->call('PUT', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_DuplicateZoneTypeCode_Fail()
    {
        $zoneTypeId = factory(App\WarehouseZoneType::class)->create()->zone_type_id;
        $zoneTypeCode = factory(App\WarehouseZoneType::class)->create()->zone_type_code;

        $params = [
            'zone_type_name' => rand(),
            'zone_type_code' => 'Test',
            'zone_type_desc' => 'Test'
        ];

        $this->zoneTypeParams['zone_type_code'] = $zoneTypeCode;

        $this->call('PUT', $this->getEndPoint() . '/' . $zoneTypeId, $this->zoneTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDestroy_SingleItem_Ok()
    {
        $zoneType = factory(App\WarehouseZoneType::class)->create();
        $endPoint = $this->getEndPoint() . '/' . $zoneType->zone_type_id;

        $this->call('DELETE', $endPoint);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDestroy_ZoneTypeIdNotExisted_Fail()
    {
        $endPoint = $this->getEndPoint() . '/999999';

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $ZoneTypes = factory(App\WarehouseZoneType::class, 3)->create();
        $params = [];
        foreach ($ZoneTypes as $key => $ZoneType) {
            $params['zone_type_id'][$key] = $ZoneType->zone_type_id;
        }
        $this->call('DELETE', $this->getEndPoint(), $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDeleteMass_NoParams_Fail()
    {
        $this->call('DELETE',$this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testSearch_NoParams_Fail()
    {
        $endPoint = $this->getEndPoint();
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_Ok()
    {
        $zoneType = factory(App\WarehouseZoneType::class)->create();
        $endPoint = $this->getEndPoint() . '?zone_type_name=' . $zoneType->zone_type_name
            . '&zone_type_code=' . $zoneType->zone_type_code;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }
}
