<?php

namespace App\Api\V1\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;

/**
 * Class BaseController
 *
 * @package App\Api\V1\Controllers
 *
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="",
 *     basePath="/v1",
 *     definitions={},
 *     @SWG\Info(
 *         version="WMS 2.0.0",
 *         title="Items API",
 *         description="There are some API function to view, create, update, delete data for Items",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="dang.dung@seldatinc.com"
 *         ),
 *         @SWG\License(
 *             name="WMS2.0",
 *             url="seldatinc.com"
 *         )
 *     ),
 *     @SWG\Definition(
 *          definition="ErrorModel",
 *          @SWG\Property(
 *              property="errors",
 *              required={"message","status_code"},
 *              @SWG\Property(property="message", type="string", description="Message error"),
 *              @SWG\Property(property="status_code", type="integer", format="int32", description="Status code error"),
 *          ),
 *     ),
 *     @SWG\Definition(
 *         definition="InputItemData",
 *         required={"item_code", "sku", "size", "color", "cus_id"},
 *         @SWG\Property(property="item_code", type="string", description="Item Code", default="testCode"),
 *         @SWG\Property(property="description", type="string", description="Description", default="test description"),
 *         @SWG\Property(property="lot", type="string", description="lot", default="testLot"),
 *         @SWG\Property(property="sku", type="string", description="SKU", default="testSKU"),
 *         @SWG\Property(property="size", type="string", description="Size", default="testSize"),
 *         @SWG\Property(property="color", type="string", description="Color", default="testColor"),
 *         @SWG\Property(property="uom_id", type="integer", format="int32", description="UOM Id", default=1),
 *         @SWG\Property(property="pack", type="integer", format="int32", description="Pack", default=1),
 *         @SWG\Property(property="length", type="number", format="float", description="Length", default=1.00),
 *         @SWG\Property(property="width", type="number", format="float", description="Width", default=1.00),
 *         @SWG\Property(property="height", type="number", format="float", description="Height", default=1.00),
 *         @SWG\Property(property="weight", type="number", format="float", description="Weight", default=1.00),
 *         @SWG\Property(property="cus_id", type="integer", format="int32", description="Customer Id", default=1),
 *         @SWG\Property(property="cus_upc", type="string", description="Customer UPC",
 *              default="test customer UPC"),
 *         @SWG\Property(property="status", type="string", description="Status", default="AC"),
 *         @SWG\Property(property="condition", type="string", description="Condition", default="testCondition")
 *     ),
 *     @SWG\Definition(
 *         definition="ItemData",
 *         @SWG\Property(property="item_id", type="integer", format="int32", description="Item Id"),
 *         @SWG\Property(property="item_code", type="string", description="Item Code"),
 *         @SWG\Property(property="upc", type="string", description="UPC"),
 *         @SWG\Property(property="description", type="string", description="Description"),
 *         @SWG\Property(property="lot", type="string", description="lot"),
 *         @SWG\Property(property="sku", type="string", description="SKU"),
 *         @SWG\Property(property="size", type="string", description="Size"),
 *         @SWG\Property(property="color", type="string", description="Color"),
 *         @SWG\Property(property="uom_id", type="integer", format="int32", description="UOM Id"),
 *         @SWG\Property(property="pack", type="integer", format="int32", description="Pack"),
 *         @SWG\Property(property="length", type="number", format="float", description="Length"),
 *         @SWG\Property(property="width", type="number", format="float", description="Width"),
 *         @SWG\Property(property="height", type="number", format="float", description="Height"),
 *         @SWG\Property(property="weight", type="number", format="float", description="Weight"),
 *         @SWG\Property(property="volume", type="number", format="float", description="Volume"),
 *         @SWG\Property(property="cus_id", type="integer", format="int32", description="Customer Id"),
 *         @SWG\Property(property="cus_upc", type="string", description="Customer UPC"),
 *         @SWG\Property(property="status", type="string", description="Status"),
 *         @SWG\Property(property="condition", type="string", description="Condition"),
 *         @SWG\Property(
 *              property="created_at",
 *              @SWG\Property(property="date", type="string", format="date-time", description="Date"),
 *              @SWG\Property(property="timezone_type", type="integer", format="int32", description="Timezone Type"),
 *              @SWG\Property(property="timezone", type="string", description="Timezone")
 *          ),
 *         @SWG\Property(
 *              property="updated_at",
 *              @SWG\Property(property="date", type="string", format="date-time", description="Date"),
 *              @SWG\Property(property="timezone_type", type="integer", format="int32", description="Timezone Type"),
 *              @SWG\Property(property="timezone", type="string", description="Timezone")
 *         ),
 *         @SWG\Property(property="deleted_at", type="integer", format="int32", description="Deleted at date"),
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */

/**
 * @SWG\Tag(
 *      name="Items",
 *      description="Items API",
 * )
 */
abstract class AbstractController extends Controller
{
    use Helpers;
}
