<?php
/**
 * Created by PhpStorm.
 * User: phuctran
 * Date: 11/05/2017
 * Time: 10:18
 */

namespace App\Api\V1\Controllers;


use Illuminate\Http\Request;

class VersionController
{
    public function index(Request $request){
        $gunVersion = $request->header('gun-version');

        $data = [
            'gun_version' => $gunVersion,
            'min_version' => env('MIN_VERSION'),
            'latest_version' => env('LATEST_VERSION'),
            'features' => explode('|', env('FEATURES')),
            'update_reason' => env('UPDATE_REASON'),
            'min_version_link' => env('MIN_VERSION_LINK'),
            'latest_version_link' => env('LATEST_VERSION_LINK')
        ];

        $data['check'] = ($gunVersion) ? $this->checkVersion($gunVersion, $data['min_version']) : false;

        return $data;
    }

    private function checkVersion($gunVersion, $minVersion){
        $gunVersion = explode('.', $gunVersion);
        $minVersion = explode('.', $minVersion);
        if((int)$gunVersion[0] > (int)$minVersion[0])    return true;
        else{
            if((int)$gunVersion[1] > (int)$minVersion[1])    return true;
            else{
                if((int)$gunVersion[2] > (int)$minVersion[2])    return true;
                else{
                    if((int)$gunVersion[3] >= (int)$minVersion[3])    return true;
                }
            }
        }
        return false;
    }
}