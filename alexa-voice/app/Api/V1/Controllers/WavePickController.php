<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\WavePickDtlLocModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class WavePickController extends AbstractController
{
    protected $wv_hdr;
    protected $wv_dtl;
    protected $odrHdr;
    protected $carton;
    protected $wv_dtl_loc;
    protected $customer_config;

    /**
     * WavePickController constructor.
     */
    public function __construct()
    {
        $this->wv_hdr = new WavePickHdrModel();
        $this->wv_dtl = new WavePickDtlModel();
        $this->odrHdr = new OrderHdrModel();
        $this->carton = new CartonModel();
        $this->wv_dtl_loc = new WavePickDtlLocModel();
        $this->customer_config = new CustomerConfigModel();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateWavePickStatus($wv_id, Request $request)
    {
        $wave = $this->wv_hdr->getFirstBy('wv_id', $wv_id);
        if (empty($wave)) {
            return $this->response->errorBadRequest("The Wave Pick is not existed!");
        }

        $picking = $this->wv_dtl->getPickingWvDtlByWvId($wv_id);
        if (!empty($picking)) {
            return $this->response->errorBadRequest('This wavepick is picking.');
        }

        try {
            DB::beginTransaction();

            $this->wv_dtl->updateWvDtlByWvId($wv_id);

            DB::commit();

            $predis = new Data();
            $userInfo = $predis->getUserInfo();
            $userId = $userInfo['user_id'];
            $whsId = array_get($userInfo, 'current_whs', 0);
            // Log Picking
            file_put_contents(storage_path("wv_{$userId}_{$whsId}.log"),\json_encode(['wv_id' => $wv_id, 'wv_num' => $wave->wv_num]));

            return [
                'data' => [
                    'status' => true,
                    'msg' => 'Update Successfully'
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function checkNewWavepick(Request $request)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = $userInfo['user_id'];
        $whsId = array_get($userInfo, 'current_whs', 0);
        $user_customers = array_get($userInfo, 'user_customers', 0);
        $cusIds = array_pluck($user_customers, 'cus_id');

        $logFile = storage_path("wv_{$userId}_{$whsId}.log");
        if (file_exists($logFile)) {
            $abc = file_get_contents($logFile);
            $data = \json_decode($abc);
            $wv_id = $data->wv_id;
            $wave = $this->wv_hdr->getFirstBy('wv_id', $wv_id);
            if (!empty($wave) && $wave->wv_sts == 'CO') {
                $wave = $this->wv_hdr->getWvHdrFirst($userId, $whsId, $cusIds);
            } else {
                $wave->wv_sts = 'PK';
            }
        } else {
            $wave = $this->wv_hdr->getWvHdrFirst($userId, $whsId, $cusIds);
        }

        $data = [
            'status_code' => false
        ];
        if (!empty($wave)) {
            $data = [
                'wavepick_num' => $wave->wv_num,
                'wavepick_id' => $wave->wv_id,
                'status_code' => $wave->wv_sts,
                'status_name' => Status::getByKey('WAVEPICK-STATUS', $wave->wv_sts),
                'msg'    => 'Wavepick is ' . Status::getByKey('WAVEPICK-STATUS', $wave->wv_sts)
            ];
        }

        return ['data' => $data];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateWavepick($wv_id, Request $request)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = $userInfo['user_id'];
        $whsId = array_get($userInfo, 'current_whs', 0);

        $input = $request->getParsedBody();
        //$input['whs_id'] = $whsId;
        //$input['wv_id'] = $wv_id;

        if (!empty($input)) {
            foreach ($input as $key => $item) {
                $input[$key]['whs_id'] = $whsId;
                $input[$key]['wv_id'] = $wv_id;
            }
        }

        $url = env('API_WAVE_PICK') . "update-wave-pick/$wv_id/no-ecom";
        try {
            $client = new Client();
            $client->request('PUT', $url,
                [
                    'headers' => [
                        'Authorization' => $request->getHeader('Authorization'),
                        'Content-Type' => 'application/json'
                    ],
                    'body' => \GuzzleHttp\json_encode($input)
                ]);

            @unlink(storage_path("wv_{$userId}_{$whsId}.log"));
            return [
                'data' => [
                    'status' => true,
                    'msg'    => 'Update Wave Pick is successfully',
                    'status_code'   => 200
                ]
            ];

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            $data = \json_decode($e->getResponse()->getBody()->getContents(), true);
            return $this->response->errorBadRequest($data['errors']['message']);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function moreSuggestLocation(Request $request)
    {
        $input = $request->getParsedBody();

        try {
            if (!isset($input['item_id']) || empty($input['item_id'])) {
                return $this->response->errorBadRequest("The item_id input is required!");
            }
            if (!isset($input['lot']) || empty($input['lot'])) {
                return $this->response->errorBadRequest("The lot input is required!");
            }
            if (!isset($input['is_ecom'])) {
                return $this->response->errorBadRequest("The is_ecom input is required!");
            }

            if (empty($input['loc_ids'])) {
                $input['loc_ids'] = [];
            } else {
                $input['loc_ids'] = array_filter(explode(',', $input['loc_ids']));
            }

            $result = $this->carton->getMoreSugLocByWvDtl($input['loc_ids'], $input, 5);

            return [
                'data' => $result
            ];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $wvId
     * @param $dataWvDtls
     *
     * @return array
     */
    public function getLoationsNumberCartons($whsId, $dataWvDtls)
    {
        if (empty($dataWvDtls) || count($dataWvDtls) == 0) {
            return [];
        }
        $itemIds = [];
        $itemLots = [];

        foreach ($dataWvDtls as $dataWvDtl) {
            $itemIds[$dataWvDtl['item_id']] = $dataWvDtl['item_id'];
            $itemLots[$dataWvDtl['item_id']] = [
                'item_id'     => $dataWvDtl['item_id'],
                'sku'         => $dataWvDtl['sku'],
                'color'       => $dataWvDtl['color'],
                'size'        => $dataWvDtl['size'],
                'lot'         => $dataWvDtl['lot'],
                'pack_size'   => (int)$dataWvDtl['pack_size'],
                'number_of_cartons' => (int)$dataWvDtl['ctn_qty'],
                'piece_qty' => (int)$dataWvDtl['piece_qty'],
                'cus_id'      => $dataWvDtl['cus_id'],
                'wv_dtl_id'   => $dataWvDtl['wv_dtl_id']
            ];
        }

        // Load data location for excel
        /*$wvDtlLocs = $this->wv_dtl_loc->getSugLocIdByWvId($wvId)->toArray();

        $locIds = [];
        foreach ($wvDtlLocs as $wvDtlLoc) {
            $ids = array_filter(explode(',', $wvDtlLoc['sug_loc_ids']));
            $locIds = array_merge($locIds, $ids);
        }*/

        //$cartons = $this->carton->getCartonForWavePick($locIds, $itemIds);
        $ctns = [];
        if (!empty($itemLots) && count($itemLots) > 0) {
            foreach ($itemLots as $itemId => $item) {
                $lot = $item['lot'];
                $ctns[$itemId . '-' . $lot] = $item;
                $algorithm = $this->customer_config->getPickingAlgorithm($whsId, $item['cus_id']);
                $ctns[$itemId . '-' . $lot]['locations'] = $this->carton->getCartonForItem($whsId, $item['cus_id'], $itemId, $lot, $algorithm);
            }
        }
        return $ctns;
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function firstWavepick(Request $request)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);
        $userId = $userInfo['user_id'];
        $user_customers = array_get($userInfo, 'user_customers', 0);
        $cusIds = array_pluck($user_customers, 'cus_id');

        $logFile = storage_path("wv_{$userId}_{$whsId}.log");
        if (file_exists($logFile)) {
            $abc = file_get_contents($logFile);
            $data = \json_decode($abc);
            $wv_id = $data->wv_id;
            $wave = $this->wv_hdr->getFirstBy('wv_id', (int)$wv_id);
            if ($wave->wv_sts == 'CO') {
                @unlink($logFile);
                $wave = $this->wv_hdr->getWvHdrFirst($userId, $whsId, $cusIds);
            }
        } else {
            $wave = $this->wv_hdr->getWvHdrFirst($userId, $whsId, $cusIds);
        }

        if (empty($wave)) {
            return $this->response->errorBadRequest("New Wavepick is not exists");
        }

        $wvNum = $wave->wv_num;
        $input['wv_id'] = $wave->wv_id;

        $this->odrHdr->refreshModel();
        $orders = $this->odrHdr->allBy('wv_id', $input['wv_id'])->toArray();
        if (empty($orders)) {
            return $this->response->errorBadRequest("The Wavepick Order is not existed!");
        }

        $odrHdrNum = trim(implode(", ", array_pluck($orders, 'odr_num')));

        // Load data for excel
        $dataWvDtls = $this->wv_dtl->loadData($input['wv_id']);

        // Get Location for excel
        $tempCartons = $this->getLoationsNumberCartons($whsId, $dataWvDtls);

        $isStsPicking = 'NW';
        if (!empty($dataWvDtls)) {
            foreach ($dataWvDtls as $dataWvDtl) {
                if ($dataWvDtl->wv_dtl_sts == 'PK') {
                    $isStsPicking = 'PK';
                }
            }
        }

        return [
            'data' => [
                'wavepick_id' => $input['wv_id'],
                'order_number' => $odrHdrNum,
                'wavepick_number' => $wvNum,
                'items'  => !empty($tempCartons) ? array_values($tempCartons) : [],
                'status_code' => $isStsPicking,
                'status_name' => Status::getByKey('WAVEPICK-STATUS', $isStsPicking)
            ]
        ];
    }

    public function removeFile()
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);
        $userId = $userInfo['user_id'];

        $logFile = storage_path("wv_{$userId}_{$whsId}.log");
        $msg = 'Files not exists';
        if (file_exists($logFile)) {
            @unlink($logFile);
            $msg = "File wv_{$userId}_{$whsId}.log removed successfully!";
        }
        return ['data' => [
                'status' => true,
                'msg' => $msg,
            ]
        ];
    }
}
