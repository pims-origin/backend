<?php
/**
 * Created by PhpStorm.
 * User: vinhpham
 * Date: 9/25/17
 * Time: 1:34 PM
 */

namespace App\Jobs;

use DB;
use GuzzleHttp\Client;

class BOLJob extends Job
{

    protected $orderId;
    protected $request;
    protected $whsId;
    /**
     * BOLJob constructor.
     */
    public function __construct($orderId, $whsId, $request)
    {
        $this->orderId = $orderId;
        $this->request = $request;
        $this->whsId = $whsId;

    }

    /*
     *
     */

    public function handle()
    {

        $client = new Client();
        try {
            $client->request('GET', env('API_ORDER') . '/v2/whs/'. $this->whsId .'/auto-bol/' . $this->orderId,
                [
                    'headers'     => ['Authorization' => $this->request->getHeader('Authorization')]
                ]
            );
            DB::table('sys_bugs')->where('id', 6 )->update(['error' =>  env('API_ORDER') . '/v2/whs'. $this->whsId .'/auto-bol/' . $this->orderId ]);

        } catch (\Exception $exception) {
            DB::table('sys_bugs')->where('id', 7 )->update(['error' => $exception->getMessage()]);
        }

    }

}