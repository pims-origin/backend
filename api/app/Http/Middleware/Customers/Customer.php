<?php

namespace App\Http\Middleware\Customers;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cus_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_name',
        'cus_status',
        'cus_code',
        'cus_country_id',
        'cus_state_id',
        'cus_city_name',
        'cus_postal_code',
        'cus_billing_account',
        'cus_des'
    ];
}
