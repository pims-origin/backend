<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WaveDtlLoc;
use Wms2\UserInfo\Data;

class WavePickDtlLocModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WaveDtlLoc $model = null)
    {
        $this->model = ($model) ?: new WaveDtlLoc();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickDtlLoc($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $wvId
     *
     * @return mixed
     */
    public function getSugLocIdByWvId($wvId)
    {
        return $this->model
            ->where('wv_id', $wvId)
            ->select('sug_loc_ids')
            ->get();
    }

    /**
     * @param $wvId
     *
     * @return mixed
     */
    public function getSugLocByWvId($wvId)
    {
        $query = DB::table('wv_dtl_loc as wdl')
            ->select([
                'wdl.sug_loc_ids',
                'wdl.act_loc_ids',
                'wd.item_id',
                'wd.lot',
                'wd.ctn_qty',
                'wd.act_piece_qty',
                'wd.piece_qty',

            ])
            ->where('wdl.wv_id', $wvId)
            ->join('wv_dtl as wd', 'wd.wv_dtl_id', '=', 'wdl.wv_dtl_id')
            ->get();

        return $query;
    }

    /**
     * @param $wvDtlId
     *
     * @return static
     */
    public function getSugLocIdByWvDtlId($wvDtlId)
    {
        return $this->model
            ->where('wv_dtl_id', $wvDtlId)
            ->select(['sug_loc_ids', 'act_loc_ids'])
            ->first();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateWvDtlLoc($data)
    {
        $dataUser = new Data();
        $userInfo = $dataUser->getUserInfo();

        return $this->model
            ->where('wv_dtl_id', '=', $data['wv_dtl_id'])
            ->update([
                'act_loc_ids' => $data['act_loc_ids'],
                'updated_by'  => $userInfo['user_id'],
                'updated_at'  => time()
            ]);
    }

    /**
     * @param $actLoc
     * @param $wvDtlId
     */
    public function updateWaveDtlLoc($actLoc, $wvDtlId)
    {

        $strActLocId = \GuzzleHttp\json_encode($actLoc);

        // update wave pick detail location
        $dataWvDtlLoc = [
            'wv_dtl_id'   => $wvDtlId,
            'act_loc_ids' => $strActLocId
        ];

        $this->updateWvDtlLoc($dataWvDtlLoc);
    }

}
