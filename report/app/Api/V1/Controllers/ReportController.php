<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ReportGroupModel;
use App\Api\V1\Models\ReportModel;
use App\Api\V1\Models\SampleModel;
use App\Api\V1\Transformers\ReportTransformer;
use App\Api\V1\Transformers\SampleTransformer;
use Mockery\CountValidator\Exception;

class ReportController extends AbstractController
{

    /**
     * @var report group model
     */
    protected $rp_grp;

    /**
     * ReportController constructor.
     */
    public function __construct()
    {
        $this->rp_grp = new ReportGroupModel();
    }

    /**
     * @return mixed
     */
    public function getListReport(ReportTransformer $rpTransformer)
    {
        try {
            $rpGrpLs = $this->rp_grp->getListReportGroup(['report']);
            foreach ($rpGrpLs as $key => $group) {
                $reports = array_map(function ($e) {
                    return [
                        "rp_id" => $e["rp_id"],
                        "desc"  => $e["desc"]
                    ];
                }, $group->report->toArray());

                $rpGrpLs[$key]['reports'] = $reports;
            }

            return $this->response->collection($rpGrpLs, $rpTransformer);
        } catch (Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }
}
