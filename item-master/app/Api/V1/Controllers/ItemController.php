<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ItemHierModel;
use App\Api\V1\Models\ItemLevelModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\AsnDtlModel;
//use Ite
use App\Api\V1\Models\SystemUomModel;
use App\Api\V1\Transformers\ItemDetailTransformer;
use App\Api\V1\Transformers\ItemLevelTransformer;
use App\Api\V1\Transformers\ItemTransformer;
use App\Api\V1\Validators\ItemValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Utils\Helper;
use Seldat\Wms2\Utils\Profiler;
use App\Api\V1\Validators\ItemDeleteMassValidator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Dingo\Api\Http\Response;
use mPDF;
use Wms2\UserInfo\Data;

class ItemController extends AbstractController
{
    /**
     * @var object $itemModel
     */
    protected $itemModel;

    protected $itemHierModel;

    protected $systemUomModel;

    protected $itemLevelModel;
    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;

    /**
     * @var $itemTransformer
     */
    protected $itemTransformer;

    /**
     * @var $itemDetailTransformer
     */
    protected $itemDetailTransformer;
    protected $itemDetailTransformerV2;

    /**
     * @var $itemValidator
     */
    protected $itemValidator;

    /**
     * @var $itemDeleteMassValidator
     */
    protected $itemDeleteMassValidator;

    /**
     * @param AsnDtlModel $asnDtlModel
     */
    public function __construct(
        AsnDtlModel $asnDtlModel
    ) {
        Profiler::log('start controller __construct');
        $this->itemModel = new ItemModel();
        $this->itemTransformer = new ItemTransformer();
        $this->itemDetailTransformer = new ItemDetailTransformer();
        $this->itemDetailTransformerV2 = new ItemDetailTransformer(2);
        $this->itemValidator = new ItemValidator();
        $this->itemDeleteMassValidator = new ItemDeleteMassValidator();
        $this->asnDtlModel = $asnDtlModel;
        $this->itemLevelModel = new ItemLevelModel();
        $this->itemHierModel = new ItemHierModel();
        $this->systemUomModel = new SystemUomModel();
    }

    public function show($itemId)
    {
        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $item = $this->itemModel->getFirstBy('item_id', $itemId, ['customer']);
            $spc_hdl_code = DB::table('item_meta')->where('itm_id', '=', $item->item_id)->first();
            $item->spc_hdl_code = json_decode(array_get($spc_hdl_code, 'value'));

            $item['type_flag'] = 'show';

            return $this->response->item($item, $this->itemTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $cusId
     * @param Request $request
     *
     * @return Response|void
     */
    public function validateCusAndSku(
        $cusId,
        Request $request
    ) {
        try {
            $input = $request->getQueryParams();

            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $item = $this->itemModel->getFirstWhere([
                'sku'    => trim(array_get($input, 'sku', '')),
                'cus_id' => $cusId
            ]);

            if ($item) {
                return ["data" => 'true'];
            }

            return ["data" => 'false'];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getItemById($itemId)
    {
        try {
            $item = $this->itemModel->getFirstBy('item_id', $itemId, ['customer']);
            $spc_hdl_code = DB::table('item_meta')->where('itm_id', '=', $item->item_id)->first();
            $item->spc_hdl_code = json_decode(array_get($spc_hdl_code, 'value'));
            return $this->response->item($item, $this->itemDetailTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function search(Request $request)
    {
        Profiler::log('start controller');
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $item = $this->itemModel->search($input, ['customer'], array_get($input, 'limit', 20));
            $data=$this->response->paginator($item, $this->itemDetailTransformerV2);
            //Profiler::end();
            return $data;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage().'.'.$e->getLine());
        }
    }

    public function updateDescription($itemId, Request $request)
    {
        $input = $request->getParsedBody();
        $description = array_get($input, 'description');
        $item = $this->itemModel->getFirstWhere(['item_id' => intval($itemId)]);
        if (!$item) {
            return $this->response()->errorBadRequest('Item master does not exists');
        }
        if (!$description) {
            return $this->response()->errorBadRequest(sprintf('Please fill description of SKU %s', $item->sku));
        }
        $item->description = $description;
        $item->save();
        return response()->json([
            'data' => ['message' => sprintf('Update Description of SKU %s Successfully', $item->sku)]
        ]);
    }

    private function upsert($request, $itemId = false)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['size'] = !empty($input['size']) ? $this->normalizeNA($input['size']) : ' ';
        $input['color'] = !empty($input['color']) ? $this->normalizeNA($input['color']) : ' ';
        $uomId = intval(array_get($input, 'uom_id'));

        // validation
        $this->itemValidator->validate($input);

        $params = [
            'description' => array_get($input, 'description', ''),
            'size'        => array_get($input, 'size', ' '),
            'color'       => array_get($input, 'color', ' '),
            'sku'         => strtoupper(array_get($input, 'sku', '')),
            'cus_upc'     => array_get($input, 'cus_upc', ''),
            'uom_id'      => $uomId,
            'pack'        => empty($input['pack']) ? null : intval($input['pack']),
            'length'      => floatval(array_get($input, 'length')),
            'width'       => floatval(array_get($input, 'width')),
            'height'      => floatval(array_get($input, 'height')),
            'weight'      => floatval(array_get($input, 'weight')),
            'cus_id'      => intval($input['cus_id']),
            'condition'   => array_get($input, 'condition', ''),
            'item_hier'   => array_get($input, 'item_hier', ''),
            'charge_by'   => array_get($input, 'charge_by', NULL),
            //'spc_hdl_code'=> $input['spc_hdl_code'],
        ];

        $item_meta_data = [
            'RAC' => 0,
            'BIN' => 0,
            'SHE' => 0,
            'FRE' => 0
        ];

        $spc_hdl_code_input = explode(',', array_get($input, 'spc_hdl_code', []));

        foreach ($spc_hdl_code_input as $item) {
            if (in_array($item, $item_meta_data)) {
                $item_meta_data[$item] = 1;
            }
        }

        if(array_get($input,'charge_by') == 'CF'){
            if(empty($input['length']) || empty($input['width']) || empty($input['height'])){
                return $this->response->errorBadRequest('Width - length - height must not empty.');
            }
        }

        if ($itemId) {
            $params['item_id'] = $itemId;
        }else{
            $params['itm_img'] = 'NA';
            $params['status'] = 'RG';

            $itemCode = $this->generateItemCode($input['cus_id']);

            $params['item_code'] = $itemCode;
        }

        try {
            DB::beginTransaction();

            // valid exist item in asn => not edit
            $asnDtl = $this->asnDtlModel->getFirstWhere([
                'item_id' => $itemId,
                ['asn_dtl_sts', '!=', 'CC']
            ]);
            $asnDtl = $asnDtl ? $asnDtl->toArray() : [];

            $inventorySummary = InventorySummary::where('item_id', $itemId)->first();
            $inventorySummary = $inventorySummary ? $inventorySummary->toArray() : [];

            // add uom_name & uom_code
            $systemUom = SystemUom::where('sys_uom_id', $params['uom_id'])->first();
            $params['uom_name'] = $systemUom['sys_uom_name'];
            $params['uom_code'] = $systemUom['sys_uom_code'];

            // calculate volume & cube
            $params['volume'] = Helper::calculateVolume($params['length'], $params['width'], $params['height']);
            $params['cube'] = Helper::calculateCube($params['length'], $params['width'], $params['height']);

            $volume = $params['volume'];
            $cube = $params['cube'];

            // check duplicate item
            $item = $this->itemModel->getFirstWhere([
                'sku'    => $params['sku'],
                'size'   => $params['size'],
                'color'  => $params['color'],
                'pack'   => intval($input['pack']),
                'cus_id' => intval($input['cus_id']),
            ]);

            if ($itemId && (count($asnDtl) > 0 || count($inventorySummary) > 0)) {
                $isError = true;
                if (!empty($item) && empty(object_get($item, 'cus_upc'))) {
                    $isError = false;

                    $params = [
                        'cus_upc'       => array_get($input, 'cus_upc', ''),
                        'item_id'       => $itemId,
                        'description'   => array_get($input, 'description', ''),
                        'length'        => floatval(array_get($input, 'length')),
                        'width'         => floatval(array_get($input, 'width')),
                        'height'        => floatval(array_get($input, 'height')),
                        'weight'        => floatval(array_get($input, 'weight')),
                    ];
                }

                if ($item && $item->item_id == $itemId) {
                    $isError = false;
                    //Update Length, width, height, weight, Cube, volume on Item and relative/another tables
                    $inputLength = array_get($input, 'length');
                    $inputWidth = array_get($input, 'width');
                    $inputHeight = array_get($input, 'height');
                    $inputWeight = array_get($input, 'weight');

                    if ($item->length != $inputLength
                        || $item->width != $inputWidth
                        || $item->height != $inputHeight
                        || $item->weight != $inputWeight
                    ) {
                        $this->itemModel->updateItemForRelativeObjects($input, $itemId, $volume, $cube);
                    }
                }

//                if ($isError) {
//                    return $this->response->errorBadRequest('The item has existed on the ASN or Inventory, so do not edit item.');
//                }
            }


            $chkDupItem = true;
            if ($itemId && $item) {
                $chkDupItem = ($item->item_id != $itemId);
            }

            if (!empty($item) && $chkDupItem) {
                return $this->response->errorBadRequest('[SKU - Size - Color] is existed.');
            }

            if (!empty($params['cus_upc'])) {
                //Check duplicate cus_upc on item: "The {1}  for {0} existed!"
                $itemHasUpc = $this->itemModel->getFirstWhere([
                    'cus_upc' => $params['cus_upc'],
                    'cus_id' => intval($input['cus_id'])
                ]);
                $checkUPC = false;
                if ($itemHasUpc) {
                    $checkUPC = ($itemId == $itemHasUpc->item_id) ? false : true;
                }
                if ($checkUPC) {
                    return $this->response->errorBadRequest(Message::get("BM006", "UPC/EAN"));
                }

                //Check duplicate cus_upc on ASNDtl:"The {1}  for {0} existed!"
                $asnDtlHasUpc = $this->asnDtlModel->getFirstWhere([
                    'asn_dtl_cus_upc' => $params['cus_upc']
                ]);
                $checkUPCASN = false;
                if ($asnDtlHasUpc) {
                    $checkUPCASN = ($itemId == $asnDtlHasUpc->item_id) ? false : true;
                }
                if ($checkUPCASN) {
                    return $this->response->errorBadRequest(Message::get("BM006", "UPC/EAN"));
                }
            }

            //set NULL value for cus_upc
            if(empty($params['cus_upc']))
                $params['cus_upc'] = NULL;

            //get level_id
            $levelId = DB::table('system_uom')->where('sys_uom_id', $uomId)->value('level_id');
            $params['level_id'] = $levelId;
            $itemHier = array_get($input, 'item_hier', []);
            if ($itemId) {
                $curItem = $this->itemModel->getFirstWhere([
                    'item_id'    => $itemId,
                ]);
                if($input['sku'] != $curItem->sku ) {
                    if(count($asnDtl) > 0)
                        return $this->response->errorBadRequest('Item cannot be updated SKU. Item already exists in ASN ID: '.$asnDtl['asn_hdr_id'].")");
                    if(count($inventorySummary) > 0)
                        return $this->response->errorBadRequest('Item cannot be updated SKU. Item already exists in Inventory Summary');
                }
                if ($item = $this->itemModel->update($params)) {
                    // Update cus_upc in cartons table
                    DB::table('cartons')
                        ->where('item_id', $itemId)
                        ->update(['upc' => $params['cus_upc']]);
                   // $this->processHierArray($params['item_hier'], $itemId, true);

                    DB::table('item_meta')
                        ->where('qualifier', '=', 946)
                        ->where('itm_id', '=', $itemId)
                        ->update(['value' => json_encode($item_meta_data)]);

                    DB::commit();

                    $item->spc_hdl_code = $item_meta_data;

//                     If Item status is Wait-Approve and Description is changed
//                     Send email to AE
                    // if($item->status == 'RG' && $params['description'] != $curItem->description) {
                    //     \App\Api\V1\Controllers\ImageController::sendNotificationEmail($itemId, true);
                    // }

                    return $this->response->item($item, $this->itemTransformer);
                }
            } else {
                //$params['item_code'] = Item::generateItemCode($input['cus_id']);
                if ($item = $this->itemModel->create($params)) {
                   // $this->processHierArray($params['item_hier'], $item->item_id);
                    $data = [
                        'qualifier' => 946,
                        'itm_id' => $item->item_id,
                        'value' => json_encode($item_meta_data)
                    ];

                    DB::table('item_meta')->insert($data);
                    $item->spc_hdl_code = $item_meta_data;

                    DB::commit();

                    return $this->response->item($item,
                        $this->itemTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
                }
            }

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
        return $this->response->errorBadRequest('There is some thing wrong, please contact admin !');
    }

    // PIMS-396

    /**
     * @param Request $request
     * @param AsnRefCodeListTransformer $asnRefCodeListTransformer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function approveItems(Request $request)
    {
        try {
            $request = $request->getParsedBody();
            $itemIds = array_get($request, 'item_ids');
            if (count($itemIds) < 1) {
                return $this->response()->errorBadRequest('Please choose one or more Item/Sku for approve');
            }

            if (!is_array($itemIds)) {
                $itemIds = [$itemIds];
            }

            $itemIdsApproved = $this->itemModel->getItemIdsApproved($itemIds);
            if (empty($itemIdsApproved)) {
                return $this->response()->errorBadRequest('Item does not exists');
            }

            $itemIdsWaitApprove = array_diff($itemIds, $itemIdsApproved->toArray());
            if (empty($itemIdsWaitApprove)) {
                return $this->response()->errorBadRequest('SKUs have been approved');
            }

            DB::beginTransaction();

            $this->itemModel->getModel()->whereIn('item_id', $itemIdsWaitApprove)->update(['status' => 'AC']);
            $data = [];

            $asnRefCodeList = $this->itemModel->getAsnRefCodeList($itemIdsWaitApprove, Data::getCurrentWhsId());
            if ($asnRefCodeList) {
                foreach ($asnRefCodeList as $asnRef) {
                    $data[] = [
                        'asn_hdr_id' => object_get($asnRef, 'asn_hdr_id'),
                        'asn_hdr_num' => object_get($asnRef, 'asn_hdr_num'),
                        'asn_hdr_ref' => object_get($asnRef, 'asn_hdr_ref')
                    ];
                }
            }

            $response = [
                'message' => 'Approved Successfully',
                'asn_ref' => $data
            ];

            DB::commit();

            return response()->json(['data' => $response]);

        } catch (\PDOException $e) {
            DB::rollback();
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }

    public function approve($itemId)
    {
        DB::table('item')->where('item_id', $itemId)->update(['status' => 'AC']);

        $res['data'] = [
            'message'   => "Item Approved!",
        ];

        return response()->json($res, 201);
    }

    public function processHierArray($arrInput, $item_id, $isEdit = false)
    {
        $createArr = [];
        $arrOutItem = [];
        $arrItem = $this->itemLevelModel->all();

        foreach ($arrItem as $item) {
            $arrOutItem[$item->level_id]['pkg_code'] = $item->pkg_code;
            $arrOutItem[$item->level_id]['pkg_name'] = $item->pkg_name;
        }

        foreach ($arrInput as $input) {
            if (empty($input['level_id']) || empty($input['uom_code']) || empty($input['pack']))
            {
                continue;
            }

            $systemUom = SystemUom::where('sys_uom_code', $input['uom_code'])->first();
            if (empty($systemUom)) {
                continue;
            }
            $input['uom_name'] = $systemUom['sys_uom_name'];
            $input['uom_id']   = $systemUom['sys_uom_id'];
            $input['pkg_code'] = $arrOutItem[$input['level_id']]['pkg_code'];
            $input['pkg_name'] = $arrOutItem[$input['level_id']]['pkg_name'];
            // calculate volume & cube
            /*$input['volume'] = Helper::calculateVolume($input['length'], $input['width'], $input['height']);
            if (empty($input['cube'])) {
                $input['cube'] = Helper::calculateCube($input['length'], $input['width'], $input['height']);
            }*/

            $createArr[] = [
                'level_id'  => array_get($input, 'level_id'),
                'pkg_code'  => array_get($input, 'pkg_code'),
                'pkg_name'  => array_get($input, 'pkg_name'),
                'pack'      => array_get($input, 'pack'),
                'item_id'   => $item_id,
                'parent'    => array_get($input, 'parent', null),
                /*'length'    => array_get($input, 'length'),
                'width'     => array_get($input, 'width'),
                'height'    => array_get($input, 'height'),
                'weight'    => array_get($input, 'weight'),
                'net_weight'=> array_get($input, 'net_weight', null),
                'volume'    => array_get($input, 'volume'),
                'cube'      => array_get($input, 'cube'),*/
                'uom_name'  => array_get($input, 'uom_name'),
                'uom_code'  => array_get($input, 'uom_code'),
                'uom_id'    => array_get($input, 'uom_id'),
                //'upc'       => array_get($input, 'upc', null)
            ];
        }

        //Insert hier items
        $parent_id = null;
        DB::beginTransaction();
        try {
            if ($isEdit) {
                $this->itemHierModel->deleteItem($item_id);
            }
            foreach ($createArr as $key => $item) {
                $item['parent'] = $parent_id;
                $hier_item = $this->itemHierModel->create($item);
                $parent_id = $hier_item->item_hier_id;
                $this->itemHierModel->refreshModel();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function normalizeNA($str)
    {
        $str = trim($str);
        if (strtoupper($str) === 'NA' || empty($str)) {
            return 'NA';
        }

        return $str;
    }

    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    /* Check list Items
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function check(Request $request)
    {
        $inputs = $request->getParsedBody();
        $items['available'] = [];
        $items['notAvailable'] = [];
        try {
            $systemUom = SystemUom::whereIn('sys_uom_code', ['PE'])->first();

            if (!isset($systemUom->sys_uom_id)) {
                $systemUom = SystemUom::first();
            }
            $uom_system_id = isset($systemUom->sys_uom_id) ? $systemUom->sys_uom_id : 'null';

            foreach ($inputs as $row => $input) {
                $sku = array_get($input, 'sku', '');
                $size = array_get($input, 'size', '');
                $color = array_get($input, 'color', '');
                $cusId = array_get($input, 'cus_id', '');
                $uom = array_get($input, 'uom', '');
                $arrParam = ['sku' => $sku, 'cus_id' => $cusId];

                if (!empty($size)) {
                    $arrParam['size'] = $size;
                }

                if (!empty($color)) {
                    $arrParam['color'] = $color;
                }

                $item_uom = SystemUom::where('sys_uom_code', $uom)->first();
                $uom_id = isset($item_uom->sys_uom_id) ? $item_uom->sys_uom_id : $uom_system_id;
                $input['uom_id'] = $uom_id;

                $itemData = $this->itemModel->getFirstWhere($arrParam);

                if ($itemData) {
                    $items['available'][$row] = $itemData->toArray();
                    $items['available'][$row]['uom_id'] = $uom_id;
                } else {
                    $items['notAvailable'][] = $input;
                }
            }

            return $items;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function update($itemId, Request $request)
    {
        return $this->upsert($request, $itemId);
    }

    public function destroy($itemId)
    {
        try {
            $this->itemModel->deleteItem($itemId);
            return new Response([], 200);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Cannot Delete this item');
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['item_id'] = is_array($input['item_id']) ? $input['item_id'] : [$input['item_id']];

        // validation
        $this->itemDeleteMassValidator->validate($input);

        try {
            $this->itemModel->deleteMassItem($input['item_id']);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }

    public function import(Request $request)
    {
        $input = $request->getParsedBody();
        $file_tmp = $request->getUploadedFiles()['file'];
        $filename = sprintf(
            '%s.%s',
            uniqid(),
            pathinfo($file_tmp->getClientFilename(), PATHINFO_EXTENSION)
        );
        $file_tmp->moveTo(storage_path('files/' . $filename));
        $file = storage_path('files/' . $filename);
        $processed = 0;
        $excel = Excel::load($file, function ($reader) use ($input, &$processed) {
            set_time_limit(0);
            $results = $reader->toArray();
            foreach ($results as $key => $value) {
                if ($this->upsertFromExcel($value, $input['cus_id'])) {
                    $processed++;
                }
            }
        });
        $total = count($excel->toArray());
        $collection = [
            'data' => [
                'total'     => $total,
                'processed' => $processed
            ]
        ];

        return new Response($collection, 200);
    }

    private function upsertFromExcel($input, $cus_id)
    {
        $input['cus_id'] = $cus_id;
        if (empty(trim($input['size']))) {
            $input['size'] = 'NA';
        }
        if (empty(trim($input['color']))) {
            $input['color'] = 'NA';
        }
        if (empty(trim($input['uom_code']))) {
            $input['uom_code'] = 'EA';
        }

//        $this->itemValidator->validate($input);

        $params = [
            'cus_id'      => intval($input['cus_id']),
            'description' => trim(array_get($input, 'description', '')),
            'sku'         => trim(array_get($input, 'sku', '')),
            'size'        => trim(array_get($input, 'size', '')),
            'color'       => trim(array_get($input, 'color', '')),
            'uom_code'    => trim(array_get($input, 'uom_code', '')),
            'pack'        => floatval(array_get($input, 'pack')),
            'length'      => floatval(array_get($input, 'length')),
            'width'       => floatval(array_get($input, 'width')),
            'height'      => floatval(array_get($input, 'height')),
            'weight'      => floatval(array_get($input, 'weight')),
            'cube'        => floatval(array_get($input, 'cube')),
            'status'      => array_get($input, 'status', config('constants.item_status.ACTIVE'))
        ];

        try {
            // add uom_name & uom_id
            $systemUom = SystemUom::where('sys_uom_code', $params['uom_code'])->first();
            $params['uom_id'] = $systemUom['sys_uom_id'];
            $params['uom_name'] = $systemUom['sys_uom_name'];

            // calculate volume & cube
            $params['volume'] = Helper::calculateVolume($params['length'], $params['width'], $params['height']);
            if (empty($params['cube'])) {
                $params['cube'] = Helper::calculateCube($params['length'], $params['width'], $params['height']);
            }

            // check duplicate item
            $item = $this->itemModel->updateOrCreate([
                'sku'    => $params['sku'],
                'size'   => $params['size'],
                'color'  => $params['color'],
                'cus_id' => $params['cus_id']
            ], $params);
            if ($item) {
                return true;
            }

            return false;

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
    /**
     * @return mixed
     */
    public function getItemLevel()
    {
        try {
            $item = $this->itemLevelModel->all();
            return $this->response->collection($item, new ItemLevelTransformer());
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function exportToExcel(Request $request)
    {
        try{
            $input = $request->getQueryParams();

            $items = $this->itemModel->getItemForExport($input);

            $dataForExcel = $items->transform(function($item){
                return [
                    'item_id'     => $item->item_id,
                    'cus_code'    => $item->customer->cus_code ?? '',
                    'cus_name'    => $item->customer->cus_name ?? '',
                    'sku'         => $item->sku,
                    'size'        => $item->size,
                    'color'       => $item->color,
                    'description' => $item->description,
                    'uom_name'    => $item->uom_name,
                    'pack'        => $item->pack,
                    'cus_upc'     => $item->cus_upc, //EAN/GTIN
                    'length'      => $item->length,
                    'width'       => $item->width,
                    'height'      => $item->height,
                    'weight'      => $item->weight,
                    'volume'      => $item->volume,
                    'cube'        => round($item->volume / 1728, 2)
                ];
            })->prepend([
                'item_id'      => 'Item ID',
                'cus_code'     => 'Cus Code',
                'cus_name'     => 'Customer',
                'sku'          => 'SKU',
                'size'         => 'Size',
                'color'        => 'Color',
                'description'  => 'Description',
                'uom_name'     => 'UOM',
                'pack'         => 'Pack',
                'cus_upc'      => 'EAN/GTIN',
                'length'       => 'Length',
                'width'        => 'Width',
                'height'       => 'Height',
                'weight'       => 'Weight',
                'volume'       => 'Volume',
                'cube'         => 'Cube'
            ])->toArray();

            return Excel::create('item_'.time(),function($file)use($dataForExcel){
                $file->sheet(date('Y-m-d',time()),function($sheet)use($dataForExcel){
                    $sheet->fromArray($dataForExcel, null, 'A1', false, false);
                });
            })->download('xlsx');
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        }catch(\Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
    public function getItemUom()
    {
        try {
            $item = $this->systemUomModel->allBy('sys_uom_type', 'Item', [] ,  ['level_id' => 'asc']);
            $arrOut = array();
            if (!empty($item)) {
                $arrMasterLevel = $this->itemLevelModel->all()->toArray();
                for ($i = 1; $i <= count($arrMasterLevel); $i++) {
                    $arrOut[$i]['level_id'] = $i;
                    $arrOut[$i]['level_name'] = $arrMasterLevel[$i-1]['pkg_name'];
                    $arrOut[$i]['level_code'] = $arrMasterLevel[$i-1]['pkg_code'];
                    $pos = 0;
                    foreach ($item as $it) {
                        if ($it->level_id == $i) {
                            $arrOut[$i]['details'][$pos]['pkg_code'] = $it->sys_uom_code;
                            $arrOut[$i]['details'][$pos]['pkg_name'] = $it->sys_uom_name;
                            $pos++;
                        }
                    }
                }
            }
            return new Response($arrOut, 200, []);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
     * @return Mixed
     */
    public  function getItemChargeBy(){
        try {
            $charge_by = Status::get('ITEM-CHARGE-BY');

            $results = [];
            if(!empty($charge_by)){
                foreach ($charge_by as $key => $value){
                    $results [] = [
                        'key'       => $key,
                        'value'     => $value
                    ];
                }
            }
            return ["data" => $results];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
      
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function generateSKU($cusId)
    {
        $customer = DB::table('customer')->where('cus_id', $cusId)->first();

        $skuMidCode = '1';

        $skuHeader = $customer['cus_code'] .'-'. date('ym') . '-' . $skuMidCode;
        $skuLasted = DB::table('item')
                    ->where('cus_id', $cusId)
                    ->where('sku', 'LIKE', $skuHeader . '%')
                    ->whereBetween(DB::raw("substr(`sku`, 10, 16)"), [1000000, 9999999])
                    ->orderBy('sku', 'DESC')
                    ->first();

        if($skuLasted) {
            $n = str_replace($skuHeader, '', $skuLasted['sku']);
            $n = substr($n, 0, 6);
            $n = preg_replace("/[^0-9]/", "0", $n);
            $newN = str_pad($n + 1, 6, 0, STR_PAD_LEFT);
        }else{
            $newN = str_pad(1, 6, 0, STR_PAD_LEFT);
        }
        
        $res['data'] = [
            'sku' => $skuHeader.$newN
        ];

        return response()->json($res, 201);
    }

    public function generateItemCode($cusId)
    {
        $customer = DB::table('customer')->where('cus_id', $cusId)->first();

        $skuMidCode = '1';

        $iCodeHeader = $customer['cus_code'] . date('ym') . $skuMidCode;
        $itemLasted = DB::table('item')
                    ->where('cus_id', $cusId)
                    ->where('item_code', 'LIKE', $iCodeHeader . '%')
                    ->whereBetween(DB::raw("substr(`sku`, 10, 16)"), [1000000, 9999999])
                    ->orderBy('item_code', 'DESC')
                    ->first();

        if($itemLasted) {
            $n = str_replace($iCodeHeader, '', $itemLasted['item_code']);
            $n = substr($n, 0, 7);
            $n = preg_replace("/[^0-9]/", "0", $n);
            $newN = str_pad($n + 1, 7, 0, STR_PAD_LEFT);
        }else{
            $newN = str_pad(1, 7, 0, STR_PAD_LEFT);
        }
        
        return $iCodeHeader.$newN;
    }

    public function printSKU(Request $request)
    {
        $input = $request->getQueryParams();
        $numToPrint = array_get($input, 'num_to_print', '');
        /*
        $sku = array_get($input, 'sku', '');

        if (empty($sku)) {
            throw new \Exception("The SKU is required!");
        }
        */

        $itemId = array_get($input, 'itm_id');
        if (!$itemId) {
            throw new \Exception("The Item ID of SKU is required!");
        }

        $item = DB::table('item')
            ->join('customer', 'customer.cus_id', '=', 'item.cus_id')
            ->where('item.deleted', 0)
            ->where('item.item_id', $itemId)
            ->select(['item.sku', 'customer.cus_name'])
            ->first();

        if (empty($item)) {
            throw new \Exception("Item not found!");
        }

        if (empty($numToPrint)) {
            throw new \Exception("The number of SKU is required!");
        }

        if (!is_numeric($numToPrint) || $numToPrint <= 0) {
            throw new \Exception("The number of SKU is invalid!");
        }

        $params = [
//            'sku'           => $sku,
            'item'          => $item,
            'numToPrint'    => $numToPrint,
        ];

        $this->createLPNPdfFile($params);

    }

    private function createLPNPdfFile(
        $params
    ) {
        $pdf = new mPDF(
            '',    // mode - default ''
            [152.4, 101.6],    // format - A4, for example, default '',(WxH)( 6 inch = 152.4 mm
            // X 4 inch = 101.6 mm X)
            0,     // font size - default 0
            '',    // default font family
            5,    // margin_left
            5,    // margin right
            46,     // margin top
            10,    // margin bottom
            1,     // margin header
            1,     // margin footer
            'p' // L - landscape, P - portrait
        );

        $html = (string)view('prints.SKUPrintoutTemplate', [
            'item'          => $params['item'],
            'numToPrint'    => $params['numToPrint'],
        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        $pdf->Output($params['sku'].".pdf", "D");
        //$pdf->Output();
    }

    public function deleteItem($itemId)
    {
        try {
            $whsId = Data::getCurrentWhsId();
            $item = $this->itemModel->getFirstWhere(['item_id' => $itemId]);
            if (empty($item)) {
                return $this->response()->errorBadRequest('Item does not exists');
            }

            $result = $this->itemModel->getItemTransactions($itemId, $whsId);
            if (is_null($result)) {
                return $this->response()->errorBadRequest('ItemId and WhsId can be not null');
            }
            $transNum = array_filter($result);
            if (! empty($transNum)) {
                $transNum = implode(", ", $transNum);
                return $this->response()->errorBadRequest(sprintf('Item %s already exists in %s, ...', $itemId, $transNum));
            }
            DB::beginTransaction();
            $dataUpdate = [
                'deleted' => 1,
                'updated_by' => Data::getCurrentUserId(),
                'updated_at' => time(),
                'deleted_at' => time()
            ];
            $this->itemModel->updateWhere($dataUpdate, ['item_id' => $itemId]);
            DB::commit();
            return [
                'data' => [
                    'message' => sprintf('Delete Item %s successfully', $itemId)
                ]
            ];

        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response()->errorBadRequest(
                SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }
}
