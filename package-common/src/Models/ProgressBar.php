<?php
namespace Seldat\Wms2\Models;

/**
 * Class ProgressBar
 *
 * @package Seldat\Wms2\Models
 */
class ProgressBar extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'progress_bar';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'transaction';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'transaction',
        'total',
        'value',
        'status'
    ];
}
