<?php

namespace backend\controllers;

use backend\forms\DeleteForm;
use backend\forms\DeleteUserForm;
use backend\forms\SetupPasswordForm;
use backend\forms\UpdatePasswordForm;
use backend\forms\ChangePasswordForm;
use backend\forms\SignupForm;
use backend\forms\LoginForm;
use backend\forms\UpdateUserForm;
use backend\forms\UserSearchForm;

use backend\models\BaseUser;
use backend\models\DeleteUser;
use backend\models\LastLogin;
use backend\models\OfficeLocation;
use backend\models\UserCache;
use backend\models\UserDepartment;
use backend\models\UserMetaProcess;
use backend\models\UserSearch;
use backend\models\WarehouseProcess;
use common\models\CusInternalUser;

use common\models\ResetPassword;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use common\models\User;
use yii\web\BadRequestHttpException;
use Yii;

class UserController extends ApiController
{
    public $modelClass = 'common\models\User';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view'], $actions['index'], $actions['create'], $actions['delete'], $actions['update']);

        return $actions;
    }

    public function actionSignup()
    {
        return $this->addUser();
    }

    public function actionCreate()
    {
        return $this->addUser();
    }

    protected function addUser()
    {
        $model = new SignupForm();
        $model->load(Yii::$app->request->post());

        if (!$model->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $model->errors
            ];
        }
        if ($result = $model->createActiveUser()) {
            Yii::$app->response->statusCode = 201;

            return ['data' => $result];
        } else {
            Yii::$app->response->statusCode = 400;

            return ['error' => $model->errors];
        }
    }

    public function actionLogin()
    {
        try {
            $model = new LoginForm();
            $model->load(Yii::$app->request->post());
            $result = $model->login();
            if ($result === false) {
                $errors = $model->getErrors();
                $error = array_shift($errors);
                throw new \Exception(array_shift($error));
            }

            //refresh user cache info
            Yii::$app->user->setIdentity($model->getUser());
            $userCache = UserCache::getInstance();
            $userCache->deleteUserCache();
            $userCache->getUserCache();

            $result = is_array($result) ? $result : ['token' => $result];

            return ['data' => $result];
        }
        catch (\Exception $e)
        {
            throw new BadRequestHttpException($e->getMessage());
        }
    }

    public function actionToken()
    {
        $user = Yii::$app->user->getId();

        return [
            'data' =>
                ['user_id' => $user]
        ];
    }

    public function actionLogout()
    {
        $arrToken = $this->getAuth();
        $userId = Yii::$app->user->getId();
        $lastLogin = LastLogin::findOne(['user_id' => $userId, 'expired_at' => $arrToken['exp']]);
        $result = $lastLogin->logout();

        return [
            'data' =>
                ['success' => $result]
        ];
    }

    public function actionIndex()
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }
        $result = UserSearch::getList($form);

        return $result;
    }

    /**
     * @return Action
     */
    public function actionGetCsr($cusIds, $whsId)
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }
        $result = UserSearch::getCsrByCustomerWarehouse($cusIds, $whsId);

        return $result;
    }

    /**
     * @return Action
     */
    public function actionGetPutter($cusIds, $whsId)
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }
        $result = UserSearch::getPutterByCustomerWarehouse($cusIds, $whsId);

        return $result;
    }

    public function actionGetPicker($whsId)
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }

        $result = UserSearch::getPickerByWarehouse($whsId);

        return $result;
    }

    /**
     * @return Action
     */
    public function actionGetCustomerCsr($cusIds, $whsId)
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }
        $result = UserSearch::getCustomerCsrByCustomer($cusIds, $whsId);

        return $result;
    }

    /**
     * @return Action
     */
    public function actionGetCsrByWarehouse($whsId)
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }
        $result = UserSearch::getCsrByWarehouse($whsId);

        return $result;
    }

    public function actionGetCustomerPicker($cusIds, $whsId)
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }

        $result = UserSearch::getCustomerPickerByCustomerWarehouse($cusIds, $whsId);

        return $result;
    }

    private function array2csv(array &$array)
    {
        if (!is_array($array) || count($array) == 0) {
            return null;
        }
        ob_start();

        $df = fopen("php://output", 'w');
        $title = [
            'Employed Id',
            'Username',
            'First name',
            'Last name',
            'Email',
            'Created',
            'Updated',
            'Status'
        ];
        fputcsv($df, $title);
        foreach ($array as $row) {
            $r = [
                $row['emp_code'],
                $row['username'],
                $row['first_name'],
                $row['last_name'],
                $row['email'],
                date("m/d/Y", $row['created_at']),
                date("m/d/Y", $row['updated_at']),
                $row['status'] == 'AC' ? 'Active' : 'Inactive',
            ];

            fputcsv($df, $r);
        }
        fclose($df);

        return ob_get_clean();
    }

    private function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-type: text/csv; charset=utf-8");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    private function getFormRequest()
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }

        return $form;
    }

    public function actionExport($fileType)
    {
        $form = $this->getFormRequest();
        $dataProvider = UserSearch::getAllUser($form);
        $filename = 'UserList-' . Date('YmdGis');
        if ($fileType === 'csv') {
            // Export csv
            $filename .= '.csv';
            $this->download_send_headers($filename);
            echo $this->array2csv($dataProvider['data']);
        } else {
            // export pdf
            $filename .= '.pdf';
            $mpdf = new \mPDF();
            $mpdf->WriteHTML($this->renderPartial('template', ['dataProvider' => $dataProvider]));
            $mpdf->Output($filename, 'D');
        }
        exit;
    }

    public function actionView($id)
    {
        $fields = [
            'usr_dpm_id',
            'user_id',
            'email',
            'username',
            'first_name',
            'last_name',
            'phone',
            'phone_extend',
            'mobile',
            'created_at',
            'updated_at',
        ];
        $result = UserSearch::getUser($id, $fields);
        if (false === $result) {
            throw new NotFoundHttpException('User not found');
        }

        return [
            'data' => $result
        ];
    }

    /**
     * Get all info of user. Include: roles, warehouse, customes
     *
     * @param $id
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionInfo($id = false)
    {
        $userSearch = UserSearch::getInstance();
        //check permissions editUser
        if (!$id && $this->checkPermission('userAssignAll')) {
            return ['data' => $userSearch->getAssignUserInfo()];
        }

        $result = $userSearch->getFullUserInfo($id);
        if (false === $result) {
            throw new NotFoundHttpException('User not found');
        }

        return [
            'data' => $result
        ];
    }

    private function _update($id, $isProfile = false)
    {
        $user = User::findOne(['user_id' => $id]);
        if (!$user instanceof User) {
            Yii::$app->response->statusCode = 404;

            return [
                'error' => [
                    'id'     => $id,
                    'status' => 404,
                    'code'   => 'user.update.not_found',
                    'title'  => 'User not found',
                ]
            ];
        }
        $input = Yii::$app->request->post();
        $model = new UpdateUserForm();
        $model->load($input);

        if (!$model->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $model->errors
            ];
        }

        //  Check email exists
        $userEmailExists = User::find()
            ->where(['email' => trim($input['email'])])
            ->andWhere(['<>', 'user_id', $id])
            ->count();

        if ($userEmailExists) {
            Yii::$app->response->statusCode = 404;

            return [
                'error' => [
                    'email' => ['This email has already been existed.'],
                ]
            ];
        }

        if ($model->updateUser($user, $isProfile)) {
            return ['data' => ['success' => true]];
        } else {
            Yii::$app->response->statusCode = 400;

            return ['error' => $model->errors];
        }
    }

    public function actionUpdate($id)
    {
        return $this->_update($id);
    }

    public function actionUpdateProfile($id)
    {
        return $this->_update($id, true);
    }

    public function actionDelete($id)
    {
        $user = User::findOne(['user_id' => $id]);
        if (!$user instanceof User) {
            Yii::$app->response->statusCode = 404;

            return [
                'error' => [
                    'id'     => $id,
                    'status' => 404,
                    'code'   => 'user.delete.not_found',
                    'title'  => 'User not found',
                ]
            ];
        }
        $model = new DeleteUser();
        if ($model->deleteUser($user)) {
            return ['data' => ['success' => true]];
        } else {
            Yii::$app->response->statusCode = 400;

            return ['error' => $model->errors];
        }
    }

    public function actionDeleteMultiple()
    {
        $form = new DeleteUserForm();
        $form->load(Yii::$app->request->post());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return ['errors' => $form->errors];
        }

        $model = new DeleteUser();

        $response = $model->deleteMultiple([
            'in',
            'user_id',
            explode(',', $form->user_ids)
        ]);

        if ($response) {
            return ['data' => ['success' => true]];
        }
    }

    public function actionChangePassword()
    {
        return $this->editPassword(new ChangePasswordForm());
    }

    public function actionUpdatePassword()
    {
        return $this->editPassword(new UpdatePasswordForm());
    }

    public function actionSetupPassword()
    {
        $post = Yii::$app->request->post();

        $model = new SetupPasswordForm();

        $userReset = BaseUser::getFullUserIdByToken($post['token']);

        if ($userReset['updated_at'] != $userReset['created_at']
                || $userReset['expired_at'] < date('Y-m-d H:i:s')) {
            throw new BadRequestHttpException('You have already set up your password last time. Password cannot be setup twice.');
        }
        $userId = $userReset['user_id'];

        $model->load($post);

        if (empty($userId)) {
            throw new BadRequestHttpException('User Id Invalid');
        }

        if (!$model->validate()) {
            Yii::$app->response->statusCode = 422;

            return ['error' => $model->errors];
        }
        $user = User::findOne(['user_id' => $userId]);

        if (!$user instanceof User) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => [
                    'id'     => $post['user_id'],
                    'status' => 400,
                    'code'   => 'user.update_password.not_found',
                    'title'  => 'User not found',
                ]
            ];
        }

        if ($model->updatePassword($user)) {
            // Update
            $userReset = ResetPassword::findOne(['user_id' => $userId]);
            $model->updateResetPassword($userReset);

            return ['data' => ['success' => true]];
        } else {
            Yii::$app->response->statusCode = 400;

            return ['error' => $model->errors];
        }
    }

    protected function editPassword($model)
    {
        $post = Yii::$app->request->post();

        if (isset($post['token'])) {
            $userId = BaseUser::getUserIdByToken($post['token'])[0];
        } else if (!empty($post['user_id'])) {
            $userId = $post['user_id'];
        } else {
            $userId = Yii::$app->user->id;
        }
        $post['user_id'] = $userId;

        if (empty($userId)) {
            throw new BadRequestHttpException('User Id Invalid');
        }

        $model->load($post);

        if (!$model->validate()) {
            Yii::$app->response->statusCode = 422;

            return ['error' => $model->errors];
        }
        $user = User::findOne(['user_id' => $userId]);

        if (!$user instanceof User) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => [
                    'id'     => $post['user_id'],
                    'status' => 400,
                    'code'   => 'user.update_password.not_found',
                    'title'  => 'User not found',
                ]
            ];
        }

        if ($model->updatePassword($user)) {
            //  Update time lastlogin for policy password
            // WMS2-5170 - [Authentication] Fix Expired user if last login time is so long
            // if (isset($post['token'])) {
                $lastLogin = LastLogin::findOne(['user_id' => $userId]);
                if (null === $lastLogin) {
                    $lastLogin = new LastLogin([
                        'user_id' => $userId,

                    ]);
                }
                $lastLogin->login(time());
            // }

            return ['data' => ['success' => true]];
        } else {
            Yii::$app->response->statusCode = 400;

            return ['error' => $model->errors];
        }
    }

    public function actionGetUserByIds()
    {
        $post = Yii::$app->request->post();
        $users = UserSearch::getUsers($post['user_ids']);
        if (!$users) {
            throw new BadRequestHttpException('User Or Roles not found');
        }

        return ['data' => $users];
    }

    /**
     * Get user info from cache or db
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUserCache()
    {
        $userCache = UserCache::getInstance();
        $result = $userCache->getUserCache();
        if (false === $result) {
            throw new NotFoundHttpException('User not found');
        }

        return [
            'data' => $result
        ];
    }

    public function actionClearUserCache()
    {
        $userCache = UserCache::getInstance();
        $result = $userCache->deleteUserCache();
        if (false === $result) {
            throw new NotFoundHttpException('User not found');
        }

        return [
            'data' => 'deleted user cache completed'
        ];
    }
    /**
     * Get office location
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetOfficeLoc()
    {
        $result = OfficeLocation::getOfficeLoc();
        if (false === $result) {
            throw new NotFoundHttpException('Office location not found');
        }

        return [
            'data' => $result
        ];
    }

    /**
     * Get user department
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetUserDept()
    {
        $result = UserDepartment::getUserDept();
        if (false === $result) {
            throw new NotFoundHttpException('User Department not found');
        }

        return [
            'data' => $result
        ];
    }

    /**
     * @return Action
     */
    public function actionGetPutBacker($cusIds, $whsId)
    {
        $form = new UserSearchForm();

        $form->load(Yii::$app->request->get());

        if (!$form->validate()) {
            Yii::$app->response->statusCode = 400;

            return [
                'error' => $form->errors
            ];
        }
        $result = UserSearch::getPutBackerByCustomerWarehouse($cusIds, $whsId);

        return $result;
    }
}
