<?php
namespace App\Modules\Warehouses\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WarehouseDetailtResource extends JsonResource
{
    public function toArray($request)
    {
        $addresses = AddressResource::collection($this->warehouseAddress);
        $contacts = ContactResource::collection($this->warehouseContact);

        return [
                    'type' => 'warehouses',
                    'id' => (int) $this->whs_id,
                    'attributes' => [
                        'name'          => $this->whs_name,
                        'code'          => $this->whs_code,
                    ],
                    "relationships"     => [
                        'addresses'   => $addresses,
                        'contacts'    => $contacts
                    ]
                ];

    }
}