<?php
namespace console\rbac;

use yii\helpers\VarDumper;
use  yii\rbac\Rule;
use yii\web\UnauthorizedHttpException;

class AccessWarehouseAndCustomerRule extends Rule
{
    public $name = 'AccessWarehousAndCustomer';

    private $errors = [];
    
    public function execute($userId, $item, $params)
    {

        return true;

        VarDumper::dump($userId);
        exit;
        
        /**
         * query table authority_user by user_id ($userId), permission_name ($item->name)
         * customer_id, warehouse_id, user_id, permission_name (createASN)
         **/

        $authority_user = []; //query  table authority_user by user_id ($userId), permission_name ($item->name)
        $this->checkUserInWarehouses($params, $authority_user);
        $this->checkUserInCustomers($params, $authority_user);
        
        if(empty($this->errors)){
            return true;
        }else{
            //throw new UnauthorizedHttpException();
        }
        
        //return isset($params['obj']) ? $params['obj']['created_by'] == $userId : false;
    }
    
    private function checkUserInWarehouses($params, $authority_user){
        $params['obj']['warehouse_ids'] = [1, 2, 3];
        
       $this->errors = [];
    }

    private function checkUserInCustomers($params, $authority_user){
        $params['obj']['customr_ids']   = [1, 2, 3];
        
        $this->errors = [];
    }
}
