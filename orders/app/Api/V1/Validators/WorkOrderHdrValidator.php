<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Validator;
use Dingo\Api\Exception\ValidationHttpException;

/**
 * Class OrderHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class WorkOrderHdrValidator extends AbstractValidator
{
    protected function rules()
    {}
    
    /**
     * @return array
     */
    protected function myRules($rule = false)
    {
        $rules = [
            "create" => [
                'cus_id'  => 'required|integer|exists:customer,cus_id',
                'odr_num' => 'unique:wo_hdr,odr_hdr_num,NULL,id,deleted,0'
            ],
            "update" => [
                'wo_hdr_id' => 'required|integer|exists:wo_hdr,wo_hdr_id',
                'cus_id'    => 'required|integer|exists:customer,cus_id'
            ]
        ];

        return $rules[$rule];
    }

    public function validate($input, $rule = 'create')
    {
        $validator = Validator::make($input, $this->myRules($rule), $this->messages());

        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->getMessages());
        }

        return $validator;
    }
}
