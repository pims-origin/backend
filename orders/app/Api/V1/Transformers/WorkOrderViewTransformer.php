<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 03-Oct-16
 * Time: 11:04
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WorkOrderHdr;
use Seldat\Wms2\Utils\Status;

class WorkOrderViewTransformer extends TransformerAbstract
{
    public function transform(WorkOrderHdr $workOrderHdr)
    {
        return [
            'wo_hdr_id'   => $workOrderHdr->wo_hdr_id,
            'wo_hdr_num'  => $workOrderHdr->wo_hdr_num,
            'cus_id'      => $workOrderHdr->cus_id,
            'odr_num'     => $workOrderHdr->odr_hdr_num,
            'wo_ex_notes' => $workOrderHdr->wo_ex_notes,
            'wo_in_notes' => $workOrderHdr->wo_in_notes,
            'type'        => $workOrderHdr->type,
            'wo_sts'      => Status::getByKey("wo_status", $workOrderHdr->wo_sts),
            'details'     => $workOrderHdr->details
        ];
    }
}
