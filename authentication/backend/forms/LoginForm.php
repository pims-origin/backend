<?php

namespace backend\forms;

use Yii;
use backend\forms\Form;
use common\models\User;

class LoginForm extends Form
{
    const PASSWORD_MIN_LENGTH = 8;

    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        $denySts = [
            "IA" => "inactive",
            "LO" => "locked"
        ];
        $user = User::findOne(['username' => $this->username]);

        if ($user) {
            if (isset($denySts[$user->status])) {
                $this->addError($attribute, 'User is ' . $denySts[$user->status]);

                return;
            }
        }

        if (!$user || !$user->validatePassword($this->password)) {
            $this->addError($attribute, 'Incorrect username or password.');
        }
        $this->_user = $user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if (!$this->validate()) {
            return false;
        }

        return $this->_user->getJWT();
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
