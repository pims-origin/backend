<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MenuFavoriteModel;
use App\Api\V1\Models\CustomerAddressModel;
use App\Api\V1\Models\MenuModel;
use App\Api\V1\Transformers\CustomerAddressTransformer;
use App\Api\V1\Validators\CustomerAddressValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class CustomerAddressController extends AbstractController
{
    /**
     * @var $customerAddressModel
     */
    protected $customerAddressModel;

    /**
     * @var $customerAddressTransformer
     */
    protected $customerAddressTransformer;

    /**
     * @var $customerAddressValidator
     */
    protected $customerAddressValidator;

    /**
     * CustomerAddressController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->customerAddressModel = new CustomerAddressModel();
        $this->customerAddressTransformer = new CustomerAddressTransformer();
        $this->customerAddressValidator = new CustomerAddressValidator();
    }

    public function index($customerId)
    {
        $customerAddress = $this->customerAddressModel->allBy('cus_add_cus_id', $customerId);

        return $this->response->collection($customerAddress, $this->customerAddressTransformer);
    }

    public function show($customerId, $addressId)
    {
        $customerAddress = $this->customerAddressModel->getAddressByCustomer($customerId, $addressId);

        return $this->response->item($customerAddress, $this->customerAddressTransformer);
    }

    public function store(
        $customerId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation

        $this->customerAddressValidator->validate($input);

        $params = [
            'cus_add_line_1'      => $input['cus_add_line_1'],
            'cus_add_line_2'      => array_get($input, 'cus_add_line_2', ''),
            'cus_add_country_id'  => $input['cus_add_country_id'],
            'cus_add_city_name'   => $input['cus_add_city_name'],
            'cus_add_state_id'    => $input['cus_add_state_id'],
            'cus_add_postal_code' => $input['cus_add_postal_code'],
            'cus_add_cus_id'      => $customerId,
            'cus_add_type'        => $input['cus_add_type'],
        ];

        try {
            if ($customerAddress = $this->customerAddressModel->create($params)) {
                return $this->response->item($customerAddress,
                    $this->customerAddressTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function update(
        $customerId,
        $addressId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation

        $this->customerAddressValidator->validate($input);

        $params = [
            'cus_add_id'          => $addressId,
            'cus_add_line_1'      => $input['cus_add_line_1'],
            'cus_add_line_2'      => array_get($input, 'cus_add_line_2', ''),
            'cus_add_country_id'  => $input['cus_add_country_id'],
            'cus_add_city_name'   => $input['cus_add_city_name'],
            'cus_add_state_id'    => $input['cus_add_state_id'],
            'cus_add_postal_code' => $input['cus_add_postal_code'],
            'cus_add_cus_id'      => $customerId,
            'cus_add_type'        => $input['cus_add_type'],
        ];

        try {
            if ($customerAddress = $this->customerAddressModel->update($params)) {
                return $this->response->item($customerAddress, $this->customerAddressTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function destroy($customerId, $addressId)
    {
        try {
            if ($this->customerAddressModel->deleteCustomerAddress($customerId, $addressId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
