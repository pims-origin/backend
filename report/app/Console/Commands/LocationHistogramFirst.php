<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LocationHistogramFirst extends Command
{
    protected $signature = 'location-histogram-first';
    protected $description = 'Location Histogram First successfully.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            DB::beginTransaction();

            $start_date = DB::table('pallet_storage')
                ->select([
                    DB::raw('MIN(pallet_storage.start_date) AS start_date')
                ])->pluck('start_date');

            $locationTtl = DB::table('location')
                ->select([
                    DB::raw('COUNT(1) as loc_ttl'),
                    'loc_whs_id'
                ])
                ->where('deleted', 0)
                ->groupBy('loc_whs_id')->get();
            $locationByWhs = array_pluck($locationTtl, 'loc_ttl', 'loc_whs_id');

            $now = strtotime(date('Y-m-d 23:59:59',time()));
            $start_date = $start_date[0] ?? null;
            $arrInsert = [];
            if ($start_date) {
                for ($i = $start_date; $i <= $now; $i += 86400) {

                    foreach ($locationByWhs as $whsId => $ttl) {
                        $used_ttl = DB::table('pallet_storage')
                            ->select([
                                DB::raw('COUNT(DISTINCT(pallet_storage.loc_id)) as used_ttl')
                            ])
                            ->whereRaw(DB::raw("
                                pallet_storage.start_date <= $i
                                AND pallet_storage.end_date >= $i
                                AND whs_id = $whsId
                            "))->pluck('used_ttl');

                        $loc_used_ttl = array_get($used_ttl, 0, 0);
                        $arrInsert[] = [
                            'whs_id' => $whsId,
                            'loc_used_ttl' => $loc_used_ttl,
                            'loc_avai_ttl' => $ttl - $loc_used_ttl,
                            'loc_ttl' => $ttl,
                            'his_dt' => date('Y-m-d', $i),
                            'created_at' => $i
                        ];
                    }
                }

                if ($arrInsert) {
                    foreach (array_chunk($arrInsert, 1000) as $locs) {
                        DB::table('loc_his')->insert($locs);
                    }
                }

            }
            DB::commit();

        } catch (\PDOException $e) {
            dd($e->getMessage());
            DB::rollBack();

        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollBack();
        }

    }
}
