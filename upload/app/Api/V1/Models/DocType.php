<?php
/**
 * Created by PhpStorm.
 * User: PhucTran
 * Date: 3/16/2017
 * Time: 10:17 AM
 */

namespace App\Api\V1\Models;


use Illuminate\Database\Eloquent\Model;

class DocType extends Model
{
    protected $table = 'doc_type';
    protected $primaryKey = 'doc_code';

    public $incrementing = false;
    public $timestamps = false;
}