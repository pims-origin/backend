<?php

namespace App\Modules\Asns\Eloquents;

class Container extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'container';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ctnr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'ctnr_num',
        'ctnr_note',
    ];

    public function goodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'ctnr_id', 'ctnr_id');
    }

    //pt: show ASN detail
    public function asnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'ctnr_id', 'ctnr_id');
    }


}
