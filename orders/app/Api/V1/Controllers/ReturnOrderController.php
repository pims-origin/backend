<?php
/**
 * Created by PhpStorm.
 * Phuong Hong
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\ReturnDtl;
use App\Api\V1\Models\BOLOrderDetailModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\OdrHdrMetaModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PutBackModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\OutPalletModel;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Models\WavepickDtlModel;
use App\Api\V1\Models\WavepickHdrModel;
use App\Api\V1\Models\WorkOrderModel;
use App\Api\V1\Models\WorkOrderDtlModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\ReturnOrderModel;
use App\Api\V1\Models\ReturnOrderDtlModel;
use App\Api\V1\Traits\OrderHdrControllerTrait;
use App\Api\V1\Transformers\OrderDtlTransformer;
use App\Api\V1\Transformers\OrderHdrTransformer;
use App\Api\V1\Transformers\PackHdrTransformer;
use App\Api\V1\Transformers\OrderCancelListTransformer;
use App\Api\V1\Transformers\PutBackTransformer;
use App\Api\V1\Transformers\ReturnOrderTransformer;
use App\Api\V1\Validators\OrderHdrValidator;
use App\Api\V1\Validators\CsrValidator;
use App\Api\V1\Validators\OrderImportValidator;
use App\Library\OrderImport;
use Illuminate\Http\Request as IRequest;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;

/**
 * Class OdrCartonsController
 *
 * @package App\Api\V1\Controllers
 */
class ReturnOrderController extends AbstractController
{
    use OrderHdrControllerTrait;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var ShippingOrderModel
     */
    protected $shippingOrderModel;

    /**
     * @var UserModel
     */
    protected $userModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var OrderImportValidator
     */
    protected $orderImportValidator;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var WavepickHdrModel
     */
    protected $wavepickHdrModel;

    /**
     * @var WavepickDtlModel
     */
    protected $wavepickDtlModel;

    /**
     * @var WorkOrderModel
     */
    protected $workOrderModel;

    /**
     * @var WorkOrderDtlModel
     */
    protected $workOrderDtlModel;

    /**
     * @var $customerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var $outPalletModel
     */
    protected $putBackModel;

    /**
     * @var BOLOrderDetailModel
     */
    protected $bolOrderDetailModel;

    /**
     * @var Order Hdr Meta Model
     */
    protected $orderHdrMetaModel;

    /**
     * @var ReturnOrderModel
     */
    protected $returnOrderModel;

    /**
     * @var ReturnOrderDtlModel
     */
    protected $returnOrderDtlModel;

    /**
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param UserModel $userModel
     * @param EventTrackingModel $eventTrackingModel
     * @param WarehouseModel $warehouseModel
     * @param ItemModel $itemModel
     * @param OrderImportValidator $orderImportValidator
     * @param ReturnOrderModel $returnOrderModel
     * @param PutBackModel $putBackModel
     * @param PackHdrModel $packHdrModel
     * @param PackDtlModel $packDtlModel
     * @param WavepickHdrModel $wavepickHdrModel
     * @param WavepickDtlModel $wavepickDtlModel
     * @param WorkOrderDtlModel $workOrderDtlModel
     * @param WorkOrderModel $workOrderModel
     * @param CustomerConfigModel $customerConfigModel
     * @param OutPalletModel $outPalletModel
     * @param BOLOrderDetailModel $bolOrderDetailModel
     * @param ReturnOrderModel $returnOrderModel
     * @param ReturnOrderDtlModel $returnOrderDtlModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        InventorySummaryModel $inventorySummaryModel,
        UserModel $userModel,
        EventTrackingModel $eventTrackingModel,
        WarehouseModel $warehouseModel,
        ItemModel $itemModel,
        OrderImportValidator $orderImportValidator,
        ReturnOrderModel $returnOrderModel,
        PutBackModel $putBackModel,
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel,
        WavepickHdrModel $wavepickHdrModel,
        WavepickDtlModel $wavepickDtlModel,
        WorkOrderDtlModel $workOrderDtlModel,
        WorkOrderModel $workOrderModel,
        CustomerConfigModel $customerConfigModel,
        OutPalletModel $outPalletModel,
        BOLOrderDetailModel $bolOrderDetailModel,
        ReturnOrderDtlModel $returnOrderDtlModel
    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->userModel = $userModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->warehouseModel = $warehouseModel;
        $this->itemModel = $itemModel;
        $this->orderImportValidator = $orderImportValidator;
        $this->orderHdrMetaModel = new OdrHdrMetaModel();
        $this->returnOrderModel = $returnOrderModel;
        $this->putBackModel = $putBackModel;
        $this->returnOrderDtlModel = $returnOrderDtlModel;
    }

    /**
     * @param Request $request
     * @param ReturnOrderTransformer $returnOrderTransformer
     *
     * @return Response|void
     */
    public function ReturnOrders(
        Request $request,
        ReturnOrderTransformer $returnOrderTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $input['whs_id'] = array_get($userInfo, 'current_whs', 0);

        try {
            $returnOrder = $this->returnOrderModel->search($input, [
                'odrHdr',
                'odrHdr.customer',
                'odrHdr.details',
                'putterUser'
            ], array_get($input, 'limit'));

            return $this->response->paginator($returnOrder, $returnOrderTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $returnId
     * @param ReturnOrderTransformer $returnOrderTransformer
     *
     * @return Response|void
     * @throws \Exception
     */
    public function ReturnOrdersDetail(
        $returnId,
        ReturnOrderTransformer $returnOrderTransformer
    ) {
        $returnOrderInfo = $this->returnOrderModel->getFirstBy('return_id', $returnId);
        if (empty($returnOrderInfo)) {
            throw new \Exception(Message::get("BM017", "Return Order"));
        }
        $orderId = object_get($returnOrderInfo, 'odr_hdr_id', '');
        try {
            $items = [];
            if ($orderId) {

                $returnDetails = $this->returnOrderDtlModel->findWhere(
                    ['odr_hdr_id' => $orderId],
                    [
                        'createdUser',
                        'orderDtl.systemUom',
                        'orderDtl'
                    ]
                );
                if (!empty($returnDetails)) {
                    foreach ($returnDetails as $returnDetail) {
                        $items[] = [
                            'item_id'      => array_get($returnDetail, 'orderDtl.item_id', ''),
                            'itm_sts'      => array_get($returnDetail, 'return_dtl_sts', ''),
                            'itm_sts_name' => Status::getByKey("RETURN-ORDER",
                                array_get($returnDetail, 'return_dtl_sts', '')),
                            'sku'          => array_get($returnDetail, 'orderDtl.sku', ''),
                            'size'         => array_get($returnDetail, 'orderDtl.size', ''),
                            'color'        => array_get($returnDetail, 'orderDtl.color', ''),
                            'uom_id'       => array_get($returnDetail, 'orderDtl.uom_id', ''),
                            'uom_name'     => object_get($returnDetail, "orderDtl.systemUom.sys_uom_name", null),
                            'pack'         => array_get($returnDetail, 'orderDtl.pack', ''),
                            'piece_qty'    => ReturnDtl::where('odr_dtl_id',
                                array_get($returnDetail, 'orderDtl.odr_dtl_id', 0))->sum('piece_qty'),
                            'origin_ctns'  => object_get($returnDetail, "original_ctns", 0),
                            'new_ctns'     => object_get($returnDetail, "new_ctns", 0),
                            'ctns'         => object_get($returnDetail, "ctn_ttl", 0),
                            'created_at'   => array_get($returnDetail, 'created_at', '')->format('m/d/Y'),
                            'created_by'   => trim(object_get($returnDetail, "createdUser.first_name", null) . " " .
                                object_get($returnDetail, "createdUser.last_name", null)),

                        ];
                    }
                    $returnOrderInfo->items = $items;
                }

            }

            return $this->response->item($returnOrderInfo, $returnOrderTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getUpdatePutBack($returnId, PutBackTransformer $putBackTransformer)
    {
        $returnOrderInfo = $this->returnOrderModel->getFirstBy('return_id', $returnId);
        if (empty($returnOrderInfo)) {
            throw new \Exception(Message::get("BM017", "Return Order"));
        }
        try {
            $putbackList = $this->putBackModel->findWhere(["return_id" => $returnId], ['pallet', 'returnOrderDtl']);
            $details = [];
            foreach ($putbackList as $putBack) {

                $piece_qty = object_get($putBack, 'returnOrderDtl.piece_qty', null);
                $ctn_ttl = object_get($putBack, 'ctn_ttl', null);
                if (is_null($piece_qty) || is_null($ctn_ttl) || $ctn_ttl == 0) {
                    $packSize = null;
                } else {
                    $packSize = $piece_qty / $ctn_ttl;
                }
                $detail = [
                    'pb_id'              => object_get($putBack, 'pb_id', null),
                    'return_dtl_id'      => object_get($putBack, 'return_dtl_id', null),
                    'item_id'            => object_get($putBack, 'item_id', null),
                    'sku'                => object_get($putBack, 'sku', ''),
                    'size'               => object_get($putBack, 'size', ''),
                    'color'              => object_get($putBack, 'color', ''),
                    'suggested_loc_id'   => object_get($putBack, 'suggest_loc_id', ''),
                    'suggested_loc_code' => object_get($putBack, 'suggest_loc_code', ''),
                    'actual_loc_id'      => object_get($putBack, 'actual_loc_id', null),
                    'actual_loc_code'    => object_get($putBack, 'actual_loc_code', null),
                    'status'             => Status::getByKey('RETURN-ORDER', object_get($putBack, 'pb_sts', '')),
                    'ctn_ttl'            => $ctn_ttl,
                    'piece_qty'          => $piece_qty,
                    'pack_size'          => $packSize,
                    'is_new'             => 1,
                    'uom'                => object_get($putBack, 'returnOrderDtl.orderDtl.systemUom.sys_uom_name',
                        null),
                    'plt_num'            => object_get($putBack, 'pallet.plt_num', '')
                ];
                $details[] = $detail;
            }
            $returnOrderInfo->details = $details;

            return $this->response->item($returnOrderInfo, $putBackTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }


}
