﻿<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>Pallet Print Order</title>
    <style type="text/css">
        @page bigger {
            sheet-size: 4in 7in;
        }

        .container {
            margin-bottom: 10px;
            padding-bottom: 10px;
            /*border-bottom: 1px dashed #000;*/

            /*padding-left: 40px;
            padding-right: 40px;*/
            width: 4in;
            heigh: 7in;

            text-align: center;

        }

        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 32px;
            font-weight: 100;
            margin: 0;
        }

        h2 {
            color: #364150;
            text-transform: uppercase;
            margin: 5px 0;
            font-weight: 500;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 14px;
            font-family: "Calibri";
            /*width: 21cm;
            height: 29.7cm;*/

            width: 4in;
            height: 7in;

            /*margin: 0 auto;*/
            border: 0px solid #ddd;
            padding: 20px;
            /* margin: 0 auto;*/

        }

        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #a9a9a9;
            font-size: 19px;
        }

        .table-style tbody tr td {
            padding-top: 5px;
            vertical-align: top;
            padding-bottom: 20px;
        }

        .table-style tbody tr td b {
            font-size: 20px;
            font-weight: 600;
            color: #282828;
        }

        .table-style1 {
            width: 100%;
            border: 1px solid #ddd;
            line-height: 20px;
        }

        .table-style1 thead tr th {
            color: #333;
            padding: 0 5px;
            border-bottom: 1px dashed #d2d2d2;
            font-size: 16px;
            text-transform: uppercase;
            font-weight: bold;
            text-align: left;
            border-right: 1px solid #e3f1f2;
            line-height: 24px;
        }

        .table-style1 tbody tr td {
            border-bottom: 1px dashed #ddd;
            padding: 0 5px;
            color: #595959;
        }

        .table-style1 tbody tr:last-child td {
            border-bottom: none;
        }

        .table-style1 tbody tr td.title {
            border-right: 1px solid #ddd;
            background-color: #e3f1f2;
            /*width: 40%;*/
        }

        .table-style2 {
            border: 1px solid #ddd;
            line-height: 20px;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            text-transform: capitalize;
            line-height: 24px;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 1px dashed #d5d5d5;
            border-bottom: 1px dashed #d5d5d5;
            color: #595959;
            line-height: 20px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }

        .table-style2 tbody tr td.title {
            font-weight: 600;
            color: #364150;
        }

        .table-style2 tbody tr:nth-child(2n) td {
            /*background-color: #e3f1f2;*/
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }

        .table-style3 thead tr th {
            font-weight: 600;
            text-align: left;
            color: #364150;
            text-transform: capitalize;
        }

        .table-style3 tr td {
            line-height: 20px;
            font-size: 14px;
            color: #595959;
            padding-right: 5px;
        }

        .table-style3 tr td b {
            color: #1c1c1c;
        }

        .table-style4 {
            width: 100%;
            border: 1px solid #ddd;
            line-height: 24px;
        }

        .table-style4 thead tr th {
            color: #36c6d3;
            padding: 0 5px;
            border-bottom: 1px dashed #a9a9a9;
            font-size: 16px;
            text-transform: uppercase;
            font-weight: bold;
            text-align: left;
            border-right: 1px solid #e3f1f2;
        }

        .table-style4 tbody tr td {
            padding: 5px;
            color: #595959;
            line-height: 20px;
            border-bottom: 1px solid #ddd;

        }

        .table-style4 tbody tr td:last-child {
            border-right: none;
        }

        .table-style4 tbody tr:last-child td {
            border-bottom: none;
        }

        .table-style4 tbody tr td.title {
            white-space: nowrap;
            /*width: 15%;*/
            font-weight: 600;
            color: #333;
        }

        #displayed-number {

        }
    </style>
</head>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.4.js"></script>

<script type="application/javascript">

    $(function () {
        var div = $('#displayed-number');
        var fontSize = parseInt(div.css('font-size'));

        do {
            fontSize--;
            div.css('font-size', fontSize.toString() + 'px');
        } while (div.height() >= 177 || div.width() >= 101);
    });

</script>
<body>
<?php
$j = 1;
$ctns_ttl = 0;
?>
@foreach ($orderHdrs as $orderHdr)
    <div class="container">
        <table>
            <tr>
                <td id="displayed-number">
                    <table class="table-style1" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr>
                            <th width="50%">SHIP FROM</th>
                            <th width="50%">SHIP TO</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                {{array_get($orderHdr, 'cus_name', '') . ' c/o Seldat'}}
                                <!-- Seldat -->
                                <br>
                                {{array_get($orderHdr, 'cus_add_line_1', '')}} <br>
                                {{array_get($orderHdr, 'cus_add_city_name', '')}}
                                ,
                                @if(array_get($orderHdr, 'sys_state_code', ''))
                                    {{array_get($orderHdr, 'sys_state_code', '')}}
                                @endif
                                {{array_get($orderHdr, 'cus_add_postal_code', '')}}
                                .

                            </td>
                            <td>
                                {{array_get($orderHdr, 'ship_to_name', '')}}<br>
                                {{array_get($orderHdr, 'ship_to_add_1', '')}}<br>
                                {{array_get($orderHdr, 'ship_to_city', '')}}
                                @if(array_get($orderHdr, 'ship_to_state', ''))
                                    , {{array_get($orderHdr, 'ship_to_state', '')}}
                                @endif
                                {{array_get($orderHdr, 'ship_to_zip', '')}}


                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table-style2" cellpadding="0" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="50%">Ship to Postal Code</th>
                            <th>Carrier</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="vertical-align: middle">
                            <td rowspan="2" style="padding-top: 3px !important; text-align: center">
                                <barcode height="1.5" size="0.9" code="{{array_get($orderHdr, 'ship_to_zip', '')}}"
                                         type="C128A"/>
                                <br>
                                {{array_get($orderHdr, 'ship_to_zip', '')}}              </td>
                            <td>{{array_get($orderHdr, 'carrier_name', '')}}</td>
                        </tr>
                        <tr style="vertical-align: middle">
                            <td>{{array_get($orderHdr, 'bo_num', 'NA')}}</td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table-style4" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                            <td class="title" style="">Cus Name:</td>
                            <td colspan="4"
                                style="white-space: nowrap;padding: 0px !important;">{{array_get($orderHdr, 'cus_name', '')}}</td>
                        </tr>
                        <tr>
                            <td class="title" style="width:27% !important;">CTNS:</td>
                        <!-- <td style="padding: 0px !important;">{{$ctn_Total}}</td> -->
                            <td style="padding: 0px !important; width:23% !important;">{{array_get($orderHdr, 'count_ctns', '')}}</td>
                            <!--<td class="title">&nbsp;</td>-->
                            <td colspan="2" class="title" style="text-align:right; width:27% !important;">Pallet:</td>
                            <td style="padding: 0px !important; text-align:left; width:23% !important;">{{$j++}}
                                of {{!empty($plt_Total) ? $plt_Total: 0}}</td>
                        </tr>
                        <tr>
                            <td class="title">CUS ORD#:</td>
                            <td colspan="4"
                                style="padding: 0px !important;">{{array_get($orderHdr, 'cus_odr_num', '')}}</td>
                        </tr>
                        <tr>
                            <td class="title">PO #:</td>
                            <td colspan="4">{{array_get($orderHdr, 'cus_po', '')}}</td>

                        </tr>
                        <tr>
                            <td class="title">SKU(#):</td>
                            <td colspan="4">
                                <?php
                                $arr = explode(",", array_get($orderHdr, 'sku', ''));
                                $arrVal = array_count_values($arr);
                                $sku_Str = '';
                                foreach ($arrVal as $key => $value) {
                                    $sku_Str .= ($key . '(' . $value . '), ');
                                }
                                $lenStr = strlen(trim($sku_Str));
                                $sku_Str_final = substr($sku_Str, 0, $lenStr - 1);
                                ?>
                                {{$sku_Str_final}}
                                <!--{{--{{array_get($orderHdr, 'sku', '') ? : ''}}--}}-->

                            </td>
                        </tr>
                        <tr>
                            <td class="title">Length:</td>
                            <td>{{array_get($orderHdr, 'length', '') ? : ''}}</td>
                            <td colspan="2" class="title">Width:</td>
                            <td >{{array_get($orderHdr, 'width', '') ? : ''}} </td>
                        </tr>
                        <!--{{-- <tr>
                            <td class="title">Width:</td>
                            <td colspan="4">{{array_get($orderHdr, 'width', '') ? : ''}}</td>

                        </tr>--}}-->
                        <tr>
                            <td class="title">Height:</td>
                            <td >{{array_get($orderHdr, 'height', '') ? : ''}}</td>
                            <td colspan="2" class="title">Weight:</td>
                            <td>{{array_get($orderHdr, 'weight', '') ? : ''}}</td>

                        </tr>
                         <!--{{-- <tr>
                            <td class="title">Weight:</td>
                            <td colspan="4">{{array_get($orderHdr, 'weight', '') ? : ''}}</td>

                        </tr>--}}-->
                        <!-- <tr>
                <td class="title">Item #:</td>
                <td>{{array_get($orderHdr, 'item_id', '')}}</td>

            </tr> -->


                        <!-- <tr>
                <td class="title"></td>
                <td class="title">Lot:</td>
                <td>{{array_get($orderHdr, 'lot', '')}}</td>

            </tr>
             <tr>
                <td class="title"></td>
                <td class="title">SKU:</td>
                <td>{{array_get($orderHdr, 'sku', '')}}</td>

            </tr> -->


                        <tr>

                        </tr>
                        <tr>

                        <!-- </tr>
            <tr>
                <td class="title">UPC:</td>
                <td>
                    @if(array_get($orderHdr, 'checksum_ttl', 0)<=1)
                            {{array_get($orderHdr, 'cus_upc', '')}}
                        @else
                            NA
@endif
                                </td>
                                <td class="title">Size:</td>
                                <td>
@if(array_get($orderHdr, 'checksum_ttl', 0)<=1)
                            {{array_get($orderHdr, 'size', '')}}
                        @else
                            NA
@endif
                                </td>
                                <td class="title">Color:</td>
                                <td>
@if(array_get($orderHdr, 'checksum_ttl', 0)<=1)
                            {{array_get($orderHdr, 'color', '')}}
                        @else
                            NA
@endif
                                </td>
                            </tr> -->

                        <tr>
                            <td colspan="2" class="title">Completed at:</td>
                            <td colspan="3">{{!empty($completed_at) ? $completed_at: ''}}</td>

                        </tr>

                        <tr>
                            <td colspan="2" class="title">Cancel By Date:</td>
                            <td colspan="3">{{array_get($orderHdr, 'cancel_by_dt', '')}}</td>

                        </tr>

                        </tbody>
                    </table>
                    <table class="table-style1" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                            <td style="text-align: center; padding-top: 3px !important;">
                                <barcode height="1.5" size="0.9" code="{{array_get($orderHdr, 'plt_num', '')}}"
                                         type="C128A"/>
                                <br>
                                {{array_get($orderHdr, 'plt_num', '')}}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                   <!-- {{--<table style="width: 100%;" cellspacing="0" cellpadding="0">--}}
                    {{--<tbody>--}}
                    {{--@for($r=1; $r<=3;$r++)--}}
                    {{--<tr>--}}
                    {{--<td style="text-align: center; padding: 5px">&nbsp;--}}

                    {{--</td>--}}
                    {{--</tr>--}}
                    {{--@endfor--}}
                    {{--</tbody>--}}
                    {{--</table>--}}-->

                </td>
            </tr>
        </table>
    </div>
@endforeach

</body>
</html>
