<?php

namespace App\Api\V1\Transformers;

use App\GoodsReceipt;
use League\Fractal\TransformerAbstract;

class GrHdrTransformer extends TransformerAbstract
{

    /**
     * ps:Show GoodsReceipt
     *
     * @param GoodsReceipt $grHdr
     *
     * @return array
     */
    public function transform(GoodsReceipt $grHdr)
    {
        return [
            'gr_hdr_id' => $grHdr->gr_hdr_id,
            'gr_hdr_num' => $grHdr->gr_hdr_num,
        ];
    }
}
