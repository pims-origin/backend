<?php
namespace App\Api\OUTBOUND\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class OutPalletModel extends AbstractModel
{

    /**
     * OutPalletModel constructor.
     *
     * @param OutPallet|null $model
     */
    public function __construct(OutPallet $model = null)
    {
        $this->model = ($model) ?: new OutPallet();
    }

    public function createNewPallet($packHdr, $code, $ctnTtl)
    {
        $userId = Data::getCurrentUserId();

        $data = [
            'loc_id'      => null,
            'cus_id'      => $packHdr->cus_id,
            'whs_id'      => $packHdr->whs_id,
            'plt_num'     => $code,
            'plt_block'   => null,
            'plt_tier'    => null,
            'ctn_ttl'     => $ctnTtl,
            'loc_name'    => null,
            'out_plt_sts' => 'AC',
            'loc_code'    => null,
            'created_by'  => $userId,
            'updated_by'  => $userId,
            'deleted'     => 0,
            'deleted_at'  => $packHdr->deleted_at,
            'created_at'  => time(),
            'updated_at'  => time()
        ];

        return $this->create($data);
    }

    public function createOutPallet($whsId, $cusId, $pltNum)
    {
        $userId = Data::getCurrentUserId();

        $data = [
            'loc_id'      => null,
            'cus_id'      => $cusId,
            'whs_id'      => $whsId,
            'plt_num'     => $pltNum,
            'plt_block'   => null,
            'plt_tier'    => null,
            'ctn_ttl'     => 0,
            'loc_name'    => null,
            'out_plt_sts' => 'AC',
            'loc_code'    => null,
            'created_by'  => $userId,
            'updated_by'  => $userId,
            'deleted'     => 0,
            'deleted_at'  => 915148800,
            'created_at'  => time(),
            'updated_at'  => time()
        ];

        return $this->create($data);
    }

    public function updateOutPalletMovement($whsId, $pltNum) {
        return $this->model
            ->where([
                'whs_id' => $whsId,
                'plt_num' => $pltNum,
            ])
            ->update([
                'is_movement' => 1
            ]);
    }

    public function updateOutPalletPut($whsId, $pltNum) {
        return $this->model
            ->where([
                'whs_id' => $whsId,
                'plt_num' => $pltNum,
            ])
            ->update([
                'is_movement' => 0
            ]);
    }
}
