<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\OutPalletModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Transformers\OrderOutPalletTransformer;
use App\Api\V1\Transformers\OutPalletTransformer;
use App\Api\V1\Validators\OutPalletValidator;
use App\Jobs\BOLJob;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;


class OutPalletController extends AbstractController
{
    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OutPalletModel
     */
    protected $outPalletModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var LocationModel
     */
    protected $packHdrModel;

    /**
     * CustomerChargeDetailController constructor.
     */
    public function __construct()
    {
        $this->orderHdrModel = new OrderHdrModel();
        $this->outPalletModel = new OutPalletModel();
        $this->locationModel = new LocationModel();
        $this->packHdrModel = new PackHdrModel();
    }

    public function search($whsId, $odrHdrId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $odrHdr = $this->orderHdrModel->getFirstBy('odr_id', $odrHdrId);

            if (empty($odrHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            $outPallets = $this->outPalletModel->getOutPalletByOdrHdrId($odrHdrId, array_get($input, 'limit', 9999));

            return $this->response->paginator($outPallets, new OutPalletTransformer());

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function autoLocation($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $loc_code = array_get($input, 'loc_code', null);

        try {
            $locations = $this->locationModel->searchAutoOutPallet($whsId, $loc_code, array_get($input, 'limit', 10));

            return ['data' => $locations];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function editOutPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $out_pallets = array_get($input, 'out_pallets', null);

        if (empty($out_pallets)) {
            throw new \Exception("Out Pallets is required!");
        }

        try {
            DB::beginTransaction();
            $locationAreas = [];
            $locationArea = null;
            foreach ($out_pallets as $outPallet) {
                $loc_code = array_get($outPallet, 'loc_code', null);
                $plt_id = array_get($outPallet, 'plt_id', null);
                if (!$plt_id) {
                    throw new \Exception('Invalid data!');
                }

                if (!$loc_code) {
                    throw new \Exception('Location is required!');
                }

                $location = $this->locationModel->getLocationByLocCode($whsId, $loc_code);

                if (empty(object_get($location, 'loc_id'))
                ) {
                    $location = $this->locationModel->getLocationByLocCodeWithOutType($whsId, $loc_code);

                    $msg = (!empty(object_get($location, 'loc_id'))) ? sprintf("Location %s is not belong shipping lane!",
                        $loc_code) : sprintf("Location %s is not existed!", $loc_code);
                    throw new \Exception($msg);
                } else {

                    if (object_get($location, 'loc_sts_code') != 'AC') {
                        throw new \Exception(sprintf("Location %s is not active!", $loc_code));
                    }
                }


                if ($location) {
                    $this->outPalletModel->editOutPallet($location, $plt_id);
                    $locationArea = $location->area;
                    //Check that all location must have same area
                    $locationAreas[$location->area] = true;
                }
            }

            if (count($locationAreas) > 1) {
                throw new \Exception('All locations must belong to a same area!');
            }

            //Update Order status to Schedule to ship if area = 1
            if (isset($input['odr_id'])) {
                $odrId = array_get($input, 'odr_id');
                if ($odrId) {
                    $odrHdr = $this->orderHdrModel->byId($odrId);
                    if ($locationArea == 1) {
                        if (in_array($odrHdr->odr_sts, ['RS', 'SS'])) {
                            if ($odrHdr->odr_sts == 'SS') {
                                // $odrHdr->odr_sts = 'SS';
                                // $odrHdr->save();
                            } else if ($odrHdr->odr_sts == 'RS') {
                                $odrHdr->odr_sts = 'SS';
                                $odrHdr->save();
                                //if (isset($odrHdr->shipment) && $odrHdr->shipment->ship_sts == 'FN') {
                                //    $odrHdr->odr_sts = 'SS';
                                //    $odrHdr->save();
                                //} else {
                                //    throw new \Exception("Order status must have Final BOL to perform this action!");
                                //}
                            }
                        } else {
                            throw new \Exception("Order status must be Ready to ship or Scheduled to ship to perform this action!");
                        }
                    } else if ($locationArea == 2) {
                        if (in_array($odrHdr->odr_sts, ['PTD', 'RS'])) {
                            $odrHdr->odr_sts = 'RS';
                            $odrHdr->save();
                        } else {
                            throw new \Exception("Order status must be Palletized or Ready to ship to perform this action!");
                        }
                    } else if (!$locationArea){
                        if (in_array($odrHdr->odr_sts, ['RS', 'SS', 'PTD'])) {
                            throw new \Exception("Invalid location!");
                        }
                    }
                }
            }

            DB::commit();

            $this->dispatch(new BOLJob($odrHdr->odr_id, $odrHdr->whs_id, $request));

            return [
                'data' => 'Assigned shipping lane successfully!'
            ];

        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function getOutPalletByOrder($whsId, $odrHdrId, Request $request)
    {
        try {
            $input = $request->getQueryParams();
            $pallets = $this->outPalletModel->getOutPalletByOrder($whsId, $odrHdrId, $input, array_get($input,'limit'));

            return $this->response->paginator($pallets, (new OrderOutPalletTransformer()));

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $pltId
     * @param Request $request
     * @return mixed
     */
    public function updateOutPalletByOrder($whsId, $pltId, Request $request) {
        $validator = new OutPalletValidator();
        $input = array_filter($request->getParsedBody());
        $validator->validate($input);

        try {
            $this->outPalletModel->updateWhere([
                'volume'    => array_get($input, 'volume'),
                'length'    => array_get($input, 'length'),
                'width'     => array_get($input, 'width'),
                'height'    => array_get($input, 'height'),
                'weight'    => array_get($input, 'weight'),
            ], ['plt_id' => $pltId]);
/*
            return [
                'data' => Message::getOnLang('Update Pallet successfully!')
            ];*/

            return [
                'data' => 'Update Pallet successfully!'
            ];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Allow to delete out pallet if odr_sts = PA (Packed)
     * @param $pltId
     * @throws \Exception
     * @return mixed
     */
    public function deleteOutPallet($pltId) {
        $packHdrs = $this->packHdrModel->allBy('out_plt_id', $pltId);
        if (!$packHdrs) {
            throw new \Exception("Out pallet doesn't belong to any packed order");
        }

        $outPallet = OutPallet::find($pltId)
                    ->load('packHdr.orderHdr');
        $order = object_get($outPallet, 'packHdr.orderHdr', null);
        if (!$order){
            throw new \Exception("Out pallet doesn't belong to any order");
        }

        try {
            DB::beginTransaction();
            //Delete out pallet
            $this->outPalletModel->deleteById($pltId);
            $this->packHdrModel->updateWhere([
                'pack_sts'      => 'AC',
                'out_plt_id'    => NULL,
            ], ['out_plt_id' => $pltId]);

            if ($order->odr_sts === Status::getByValue("Palletized", "ORDER-STATUS")){
                $order->update(['odr_sts' => Status::getByValue("Palletizing", "ORDER-STATUS")]);
            }

            DB::commit();
            return [
                'data' => 'Delete Pallet successfully!'
            ];
        }  catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
