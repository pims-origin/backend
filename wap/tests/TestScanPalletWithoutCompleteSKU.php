<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\AsnDtlModel;
use Dingo\Api\Routing\Helpers;
use App\Api\V1\Transformers\AsnListsCanSortV1Transformer;

class TestScanPalletWithoutCompleteSKU extends TestCase
{

    //use DatabaseTransactions;
    use Helpers;

    private $whsId = 1;
    private $cusId = 31;
    private $palletId = 1;
    private $DB_ROLLBACK = true;
    public static $_caseCreateVTCarton = [];
    private $_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0ODg5MzYzNzgsImV4cCI6MTQ4OTAyMjc3OCwibmFtZSI6IkRhbyBUcmFuIiwidXNlcm5hbWUiOiJjc3JkdHIiLCJqdGkiOjc3fQ.un4Knw-WAxBASIqIBCPBLXKEfWBNVyrMkPbUBG7S9QU";

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Test Scan pallet without complete SKU
     */
    public function testPUTScanPalletWithoutCompleteSKU()
    {
        /*
         * Prepare data to test
         */
        $asnModel = new AsnDtlModel();
        $input['whs_id'] = $this->whsId;
        $return = $asnModel->getListASN(
                $input, null
        );
        $obj = $return->getCollection()->first();
        $tran = new AsnListsCanSortV1Transformer();
        $data = $tran->transform($obj);

        $asnHdrId = $data['asn_hdr_id'];
        //check them item co hay ko
        if (!isset($data['items'])) {
            $this->markTestIncomplete("[INCO] No data to test\n");
        }
        $asnDtlId = $data['items'][0]['asn_dtl_id'];
        $itemId = $data['items'][0]['item_id'];
        $ctnrId = $data['ctnr_id'];
        $cusId = $data['cus_id'];
        $vtlCtnOnPallet = [];
        for ($i = 0; $i < 4; $i++) {
            $vtlCtnOnPallet[] = strtoupper(uniqid("DD-"));
        }

        $params = [
            'asn_hdr_id' => $asnHdrId,
            'asn_dtl_id' => $asnDtlId,
            'ctn_rfid.0' => '',
            'ctn_rfid' => "",
            'ctns_rfid' => $vtlCtnOnPallet,
            'gate_code' => '',
            'type' => 1,
            'item_id' => $itemId,
            'ctnr_id' => $ctnrId,
                //'discrepancy' => '',
        ];

        $url = "/inbound/whs/{$this->whsId}/cus/{$cusId}/add-virtual-cartons";
        echo "[RURL] 1.{$url}\n";
        $response = $this->call("POST", $url, $params, [], []
                , ['HTTP_Authorization' => $this->_token]
        );
        $content = $response->content();
        $jsonObj = json_decode($content);
        echo "[CONT] {$content}\n";
        if (!isset($jsonObj->data) || !isset($jsonObj->data->status) || $jsonObj->data->status != 1) {
            $this->fail("[FAIL] Insert carton unsuccessfully!\n");
        }

        $this->assertTrue($jsonObj->data->status, "[FAIL] Scan cartons fail\n");

        //Second call
        $pltRfid = strtoupper(uniqid("FF-"));
        $param = [
            'pallet.ctn-rfid' => $vtlCtnOnPallet,
            'pallet.pallet-rfid' => $pltRfid
        ];

        $url = "/inbound/whs/{$this->whsId}/scan-pallet/";
        echo "[RURL] 2.{$url}\n";
        echo "[DATA] " . json_encode($param) . "\n";
        $this->refreshApplication();
        $res = $this->call('PUT', $url, $param);
        $content = $res->content();
        $jsonObj = json_decode($content);
        echo "[CONT] {$content}\n";
        if (isset($jsonObj->{'gr-status'})) {
            $this->assertFalse($jsonObj->{'gr-status'}, "[FAIL] Good Receipt exist when not complete ASN!\n");
        } else {
            $this->markTestIncomplete("[INCO] Not found gr-status\n");
        }
    }

}
