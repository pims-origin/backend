<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\WorkOrderModel;
use Seldat\Wms2\Models\WorkOrderHdr;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use App\Api\V1\Models\OrderHdrModel;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderHdrTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $createdAt = date("m/d/Y", strtotime($orderHdr->created_at));

        $OrderInfo = new OrderHdrModel();
        $workOrderHdrInfo = WorkOrderHdr::where('odr_hdr_id', $orderHdr->odr_id)->first();

        $statusName = Status::getByKey("Order-Status", $orderHdr->odr_sts);
        if ($orderHdr->back_odr) {
            $statusName = str_replace('Partial ', '', $statusName);
            $statusName = 'Partial ' . $statusName;
        }

        return [
            'odr_id'          => $orderHdr->odr_id,
            'so_id'           => $orderHdr->so_id,
            'work_order'      => $OrderInfo->getWorkOrderTrueFalseFromOdrId($orderHdr->odr_id),
            'wo_hdr_num'      => object_get($workOrderHdrInfo, 'wo_hdr_num', ''),
            // Warehouse
            'whs_id'          => $orderHdr->whs_id,
            'whs_id_to'       => $orderHdr->whs_id_to,
            'whs_name'        => object_get($orderHdr, 'warehouse.whs_name', null),
            'whs_code'        => object_get($orderHdr, 'warehouse.whs_code', null),
            // Customer
            'cus_id'          => $orderHdr->cus_id,
            'cus_name'        => object_get($orderHdr, 'customer.cus_name', null),
            'cus_code'        => object_get($orderHdr, 'customer.cus_code', null),
            // Order Status
            'odr_sts'         => $orderHdr->odr_sts,
            'odr_sts_name'    => $statusName,
            // User
            'csr_id'          => $orderHdr->csr,
            'csr_name'        => trim(object_get($orderHdr, 'csrUser.first_name', null) . " " .
                object_get($orderHdr, 'csrUser.last_name', null)),
            // Order Type
            'origin_odr_type' => object_get($orderHdr, 'shippingOrder.type', ''),
            'odr_type'        => $orderHdr->odr_type,
            'odr_type_name'   => Status::getByKey("Order-Type", $orderHdr->odr_type),
            'odr_num'         => $orderHdr->odr_num,
            'cus_odr_num'     => $orderHdr->cus_odr_num,
            'cus_po'          => $orderHdr->cus_po,
            'rush_odr'        => $orderHdr->rush_odr,
            'ship_to_name'    => $orderHdr->ship_to_name,
            'ship_by_dt'      => $this->dateFormat($orderHdr->ship_by_dt),
            'cancel_by_dt'    => $this->dateFormat($orderHdr->cancel_by_dt),
            //'shipped_dt'      => empty($orderHdr->act_cmpl_dt) ? null : $this->dateFormat($orderHdr->act_cmpl_dt),
            'shipped_dt'      => empty($orderHdr->shipped_dt) ? null : $this->dateFormat($orderHdr->shipped_dt),
            'back_odr'        => $orderHdr->back_odr,
            'alloc_qty'       => $orderHdr->alloc_qty,
            'created_at'      => $createdAt,
            'created_by'      => trim(object_get($orderHdr, "createdBy.first_name", null) . " " .
                object_get($orderHdr, "createdBy.last_name", null)),
            // WavePick
            'wv_id'           => object_get($orderHdr, 'wv_id', null),
            'carrier'         => object_get($orderHdr, 'carrier', null),
            'carrier_code'    => object_get($orderHdr, 'carrier_code', null),
            'dlvy_srvc'       => object_get($orderHdr, 'dlvy_srvc', null),
            'wv_num'          => object_get($orderHdr, 'waveHdr.wv_num', null),
            'bol'             => Status::getByKey("SHIP-STATUS", $orderHdr->ship_sts)
                ? Status::getByKey("SHIP-STATUS", $orderHdr->ship_sts)
                : 'No Bol',
            'new_lpn'         => $orderHdr->new_lpn,
            'schd_ship_dt'    => $this->datetimeFormat($orderHdr->schd_ship_dt),
            'routed_dt'       => $this->dateFormat($orderHdr->routed_dt),

            'act_cancel_dt'         => (empty(object_get($orderHdr, 'act_cancel_dt', ''))) ? '' :
                date("m/d/Y", object_get($orderHdr, 'act_cancel_dt', '')),
            'driver_name'     => object_get($orderHdr, 'driver_name', null),
            'ship_id'         => $orderHdr->ship_id,

        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }

    private function datetimeFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y H:i:s", $date) : null;
    }
}
