<?php


namespace App\Api\ReplenishmentConfig\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class CartonModel extends AbstractModel
{
    const PICK_CARTON = 'CT';
    const PICK_PIECE = 'PC';
    const PICK_PALLET = 'PL';

    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    public function getSuggest($attributes, $limit = null, $with = [])
    {
        if (!is_array($attributes['loc_id'])) {
            $attributes['loc_id'] = [$attributes['loc_id']];
        }

        $query = $this->make($with)
            ->select(['ctn_id', 'ctn_num'])
            ->where('whs_id', $attributes['whs_id'])
            ->where('item_id', $attributes['item_id'])
            ->whereIn('loc_id', $attributes['loc_id'])
            ->where('ctn_sts', "AC");

        if (!empty($limit)) {
            $model = $query->take($limit)->get();
        } else {
            $model = $query->get();
        }

        return $model;
    }

    /**
     * @param $ctnNum
     *
     * @return mixed
     */
    public function checkActiveCartonByCtnNum($ctnNum)
    {
        $ctnNums = is_array($ctnNum) ? $ctnNum : [$ctnNum];

        return $this->model->whereIn('ctn_num', $ctnNums)
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('loc_id')
            ->select('ctn_id', 'ctn_num')
            ->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function getAllCartonByItemId($data)
    {
        return $this->model->select([
                'cartons.*',
                'pallet.plt_num'
            ])
            ->leftJoin('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->whereIn('ctn_num', $data['ctnNums'])
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('cartons.item_id', $data['itemId'])
            ->where('cartons.whs_id', $data['whsId'])
            ->where('cartons.cus_id', $data['cusId'])
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('cartons.loc_id')
            ->get();
    }

    /**
     * @param $locId
     *
     * @return mixed
     */
    public function getAllCartonByLocIds($locId)
    {

        $locIds = is_array($locId) ? $locId : [$locId];

        return $this->model
            ->leftJoin('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->whereIn('cartons.loc_id', $locIds)
            ->get();
    }

    public function getCartonsByLocs($locIds, $limit = null)
    {
        $query = $this->model->select(['ctn_id', 'ctn_num'])
            ->whereIn('loc_id', $locIds)
            ->where('ctn_sts', "AC")
            ->where('loc_type_code', 'RAC')
            ->where(function ($q) {
                $q->orWhere('is_damaged', 0);
                $q->orWhereNull('is_damaged');
            })
            ->where('is_ecom', 0);

        if (!empty($limit)) {
            return $query->limit($limit)->get();
        } else {
            return $query->get();
        }
    }

    public function getAvailableQuantity($itemID, $lot, $locId, $whs_id)
    {
        $query = $this->getModel()
            ->where('loc_id', $locId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('lot', $lot);

        $qty = $query->sum('piece_remain');

        return $qty;


    }

    public function getAllCartonsByWvDtl($wvDtl, $algorithm = 'FIFO', $locIds, $actLocs)
    {
        $packSize = $wvDtl['pack_size'];

        $whsId = $wvDtl['whs_id'];
        $itemId = $wvDtl['item_id'];
        $lot = $wvDtl['lot'];
        $packSizes = $this->getAvailablePacksizes($whsId, $itemId, $lot, $locIds, $algorithm);

        if (!in_array($packSize, $packSizes)) {
            $packSize = array_shift($packSizes);
        } else {
            $packSizes = $this->array_remove($packSize, $packSizes);
        }

        array_unshift($packSizes, $packSize);
        $pickedcartons = [];

        foreach ($actLocs as $actLoc) {
            $pickedQty = $actLoc['picked_qty'];
            foreach ($packSizes as $packSize) {
                $ctns = ceil($pickedQty / $packSize) + 50;
                $cartons = $this->getCartonsByPack($whsId, $itemId, $lot, $actLoc['act_loc_id'], $packSize, $ctns,
                    $algorithm);

                $qtys = array_column($cartons, 'piece_remain');
                $total = array_sum($qtys);
                $pickedcartons = array_merge($pickedcartons, $this->getPickCartons($cartons, $pickedQty));
                $pickedQty = $pickedQty - $total;

                //break if enough cartons
                if ($pickedQty <= 0) {
                    break;
                }
            }
        }

        return $pickedcartons;
    }

    public function getAvailablePacksizes($whs_id, $itemID, $lot, $locIds, $algorithm)
    {
        $query = $this->getModel()
            ->whereIn('loc_id', $locIds)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('lot', $lot)
            ->groupBy('ctn_pack_size');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }


        $res = $query->get(['ctn_pack_size'])->toArray();

        return array_column($res, 'ctn_pack_size');
    }

    public function array_remove($remove, &$array)
    {
        foreach ((array)$array as $key => $value) {
            if ($value == $remove) {
                unset($array[$key]);
                break;
            }
        }

        return $array;
    }


    public function getCartonsByPack($whs_id, $itemID, $lot, $locId, $packSize, $ctns, $algorithm)
    {
        $this->getModel()->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->where('loc_id', $locId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('lot', $lot)
            ->where('ctn_pack_size', $packSize)
            ->limit($ctns)
            ->orderBy('ctn_pack_size');


        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        $query->orderBy('cartons.piece_remain', 'ASC');

        $res = $query->get()->toArray();

        return $res;
    }


    public function getPickCartons($cartons, $pickedQty)
    {
        $pickedcartons = [];
        foreach ($cartons as $carton) {
            //pick cartons
            $carton['pick_piece'] = false;
            if ($pickedQty < $carton['piece_remain']) {
                $carton['pick_piece'] = true;
                $carton['picked_qty'] = $pickedQty;
                $pickedQty = 0;
            } elseif ($pickedQty == $carton['piece_remain']) {
                $carton['picked_qty'] = $carton['piece_remain'];
                $pickedQty = 0;
            } else {
                $pickedQty = $pickedQty - $carton['piece_remain'];
                $carton['picked_qty'] = $carton['piece_remain'];
            }
            $pickedcartons[] = $carton;

            if ($pickedQty <= 0) {
                break;
            }
        }

        return $pickedcartons;
    }

    public function updateCartonByLocs($cartons)
    {
        /**
         * 1. Update full carton
         * 2. Update piece_remain and piece_ttl
         */
        $fullCtns = [0];
        foreach ($cartons as $idx => $carton) {
            if ($carton['pick_piece']) {
                $cartons[$idx] = $this->updatePickedPieceCarton($carton);
            } else {
                $fullCtns[$carton['ctn_id']] = $carton;
            }
        }

        $this->updatePickedFullCartons($fullCtns);

        return $cartons;
    }

    public function updatePickedPieceCarton($carton)
    {
        $remainQty = $carton['piece_remain'] - $carton['picked_qty'];
        $res = DB::table('cartons')->where('ctn_id', $carton['ctn_id'])->update(
            ['piece_remain' => $remainQty]
        );

        //change business clone new carton
        $origCtnId = $carton['ctn_id'];
        $origLocId = $carton['loc_id'];
        $maxCtnNum = $this->getMaxCtnNum($carton['ctn_num']);
        unset($carton['ctn_id']);

        $carton['inner_pack'] = 0;

        $newCarton = (new Carton())->fill($carton);
        $newCarton->piece_remain = $carton['picked_qty'];
        $newCarton->origin_id = $origCtnId;
        $newCarton->loc_id = $newCarton->loc_code = null;
        $newCarton->ctn_sts = 'PD';
        $newCarton->picked_dt = time();
        $newCarton->ctn_num = ++$maxCtnNum;
        $newCarton->save();

        $return = $newCarton->toArray();
        $return['pick_piece'] = true;
        $return['loc_id'] = $origLocId;
        $return['loc_code'] = $return['loc_name'];
        $return["picked_qty"] = $carton['picked_qty'];

        return $return;
    }

    public function updatePickedFullCartons($fullCtns)
    {
        $ctnIDs = array_keys($fullCtns);

        $res = DB::table('cartons')->whereIn('ctn_id', $ctnIDs)->update(
            [
                'ctn_sts'          => 'PD',
                'loc_id'           => null,
                'loc_code'         => null,
                //'loc_name'         => null,
                //'plt_id'           => null,
                'picked_dt'        => time(),
                'storage_duration' => self::getCalculateStorageDurationRaw()
            ]
        );

        return $res;
    }

    public static function getCalculateStorageDurationRaw($startDate = 'created_at')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    public function getFullPallet($whsId, $itemId, $lot)
    {
        $query = DB::table('cartons')
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->select([
                DB::raw('pallet.*')
            ])
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('pallet.ctn_ttl', '>', 0)
            ->whereRaw('pallet.init_ctn_ttl = pallet.ctn_ttl')
            ->orderBy('pallet.init_ctn_ttl', 'DESC');

        //var_dump($query->getBindings());exit($query->toSql());
        return $query->first();
    }

    public function sortDeepLocationPicking($data)
    {
        for ($i = 0; $i < count($data); $i++) {

            if (isset($data[$i + 1])) {

                $next = $data[$i + 1]['loc_code'];
                $cur = $data[$i]['loc_code'];
                $curloc = substr($cur, 0, strlen($cur) - 1);
                $nextLoc = substr($next, 0, strlen($next) - 1);
                if ($curloc == $nextLoc) {
                    if (substr($cur, -1, 1) > substr($next, -1, 1)) {
                        $next = $data[$i + 1];
                        $current = $data[$i];
                        $data[$i + 1] = $current;
                        $data[$i] = $next;
                        $i = $i + 1;
                    }

                }

            }
        }

        return $data;
    }

    public function getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take, $IgnorefirstLevel = true)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $query = DB::table('cartons')
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->select([
                'pallet.loc_id',
                'pallet.loc_code',
                'pallet.rfid',
                DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS ctns')
            ])
            ->where('location.loc_sts_code', 'AC')
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId)
            ->where('pallet.deleted', 0)
            ->whereRaw('pallet.init_ctn_ttl = pallet.ctn_ttl')
            ->groupBy('pallet.loc_id');

        if ($IgnorefirstLevel) {
            //Remove A = Level 1
            $query->whereRaw("pallet.loc_code REGEXP '([B-Z])[1-2]$' ");
        }

        $query->orderBy('pallet.ctn_ttl', 'DESC');
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                // $query->orderBy('cartons.gr_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        };
        $query->orderBy(DB::Raw("pallet.loc_code"), 'DESC');

        return $query->limit($take)->skip(0)->get();
    }

    public function getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl, $firstLevel = true)
    {
        $query = DB::table('cartons')
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->select([
                'pallet.loc_id',
                'pallet.loc_code',
                DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS ctns')
            ])
            ->where('location.loc_sts_code', 'AC')
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId)
            ->where('pallet.deleted', 0)
            ->groupBy('pallet.loc_id');

        if ($pltTtl < 1 && $firstLevel) {
            $query->whereRaw("pallet.loc_code REGEXP '([A])[1-2]$' ");
            $query->whereRaw('pallet.init_ctn_ttl != pallet.ctn_ttl');

        }

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        }


        if ($pltTtl >= 1) {
            $query->orderBy("pallet.ctn_ttl", 'DESC');
            $query->orderBy(DB::Raw("pallet.loc_code"), 'DESC');
        } else {
            $query->orderBy("pallet.ctn_ttl", 'ASC');
            $query->orderBy(DB::Raw("pallet.loc_code"), 'ASC');
        }

        return $query->limit($take)->skip(0)->get();
    }

    /**
     * @param array $locIds
     * @param int $itemId
     *
     * @return mixed
     */
    public function getMoreSugLocByWvDtl($wvDtl, $pickFullPallet, $take = 8)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();
        $itemId = $wvDtl->item_id;
        $lot = $wvDtl->lot;
        $cusId = $wvDtl->cus_id;
        $pickQty = $wvDtl->piece_qty - $wvDtl->act_piece_qty;
        $pickedCTNS = ceil($pickQty / $wvDtl->pack_size);

        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cusId);

        $fullPallet = $this->getFullPallet($whsId, $itemId, $lot);
        $pltTtl = 0;


        if ($fullPallet) {
            $pltTtl = intval($pickedCTNS / $fullPallet['ctn_ttl']);
        }

        if ($pickFullPallet) {
            if ($pltTtl >= 1) {
                $ignoreFirstLevel = true;
                $locs = $this->getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take,
                    $ignoreFirstLevel);
                if (count($locs) == 0) {
                    $locs = $this->getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take,
                        $ignoreFirstLevel);
                }

                if (count($locs) > 0) {
                    return $locs;
                }
            }

        }

        $firstLevel = false;
        $locs = $this->getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl);
        $pickFullPallet = true;
        if (count($locs) == 0) {
            $locs = $this->getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl, $firstLevel);
        }


        return $locs;

    }

    public function checkLocByWvDtl($locId, $itemId, $lot)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        return $this->model->select([
            'cartons.loc_id',
            'cartons.loc_code',
            'pallet.pack',
            DB::raw('COALESCE(SUM(piece_remain), 0) as avail_qty'),
            DB::raw('COUNT(ctn_id) as ctns')
        ])
            ->join('pallet', 'pallet.plt_id', 'cartons.plt_id')
            ->where('cartons.whs_id', Data::getCurrentWhsId())
            ->where('cartons.loc_id', $locId)
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->first();
    }

    public function checkPalletByWvDtl($pltId, $itemId, $lot)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model->select([
            'cartons.loc_id',
            'cartons.loc_code',
            'pallet.pack',
            DB::raw('COALESCE(SUM(piece_remain), 0) as avail_qty'),
            DB::raw('COUNT(ctn_id) as ctns')
        ])
            ->join('pallet', 'pallet.plt_id', 'cartons.plt_id')
            ->where('cartons.whs_id', Data::getCurrentWhsId())
            ->where('cartons.plt_id', $pltId)
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->first()
            ;
    }

    /*
     * update for pick pallet
     *
     */

    public function getAvailableQuantityByPallet($itemID, $lot, $pltId, $whs_id)
    {
        $query = $this->getModel()
            ->where('plt_id', $pltId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('lot', $lot);

        $qty = $query->sum('piece_remain');

        return $qty;


    }


    public function getAllCartonsPalletByWvDtl($wvDtl, $algorithm = 'FIFO', $actLocs)
    {
        $packSize = $wvDtl['pack_size'];

        $whsId = $wvDtl['whs_id'];
        $itemId = $wvDtl['item_id'];
        $lot = $wvDtl['lot'];
        $packSizes = $this->getAvailablePacksizesByPallet($whsId, $itemId, $lot,  $algorithm);

        if (!in_array($packSize, $packSizes)) {
            $packSize = array_shift($packSizes);
        } else {
            $packSizes = $this->array_remove($packSize, $packSizes);
        }

        array_unshift($packSizes, $packSize);
        $pickedcartons = [];


        $pickedQty = $actLocs['picked_qty'];
        foreach ($packSizes as $packSize) {
            $ctns = ceil($pickedQty / $packSize) + 50;
            $cartons = $this->getCartonsPalletByPack($whsId, $itemId, $lot, $actLocs['plt_id'], $packSize, $ctns,
                $algorithm);

            $qtys = array_column($cartons, 'piece_remain');
            $total = array_sum($qtys);
            $pickedcartons = array_merge($pickedcartons, $this->getPickCartons($cartons, $pickedQty));
            $pickedQty = $pickedQty - $total;

            //break if enough cartons
            if ($pickedQty <= 0) {
                break;
            }
        }


        return $pickedcartons;
    }



    public function getAvailablePacksizesByPallet($whs_id, $itemID, $lot,  $algorithm)
    {
        $query = $this->getModel()
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('lot', $lot)
            ->groupBy('ctn_pack_size');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }


        $res = $query->get(['ctn_pack_size'])->toArray();

        return array_column($res, 'ctn_pack_size');
    }


    public function getCartonsPalletByPack($whs_id, $itemID, $lot, $plt_id, $packSize, $ctns, $algorithm)
    {
        $this->getModel()->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->where('plt_id', $plt_id)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('lot', $lot)
            ->where('ctn_pack_size', $packSize)
            ->limit($ctns)
            ->orderBy('ctn_pack_size');


        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        $query->orderBy('cartons.piece_remain', 'ASC');

        $res = $query->get()->toArray();

        return $res;
    }

    public function getMaxCtnNum($ctnNum)
    {
        $maxCtnNum = $this->model->where('ctn_num', 'LIKE', $ctnNum . '-%')->max('ctn_num');
        if ($maxCtnNum) {
            return $maxCtnNum;
        }

        return $ctnNum . '-C00';
    }

    public function getCartonWhereInById($attributes, $with = false, $isNull = true)
    {
        return $this->model
            ->when($with,function($q) use($with) {
                return $q->with($with);
            })
            ->when($isNull, function($q){
                return $q->whereNotNull('loc_id')
                    ->whereNotNull('loc_code');
            })
            ->where('plt_id',$attributes)
            ->get();
    }

    public function updateWhereIn(array $update, array $valueWhere, $columns)
    {
        return $this->model->whereIn($columns, $valueWhere)->update($update);
    }

    /**
     * @param $ctn_ids
     *
     * @return mixed
     */
    public function getCartonWithNewLocation($ctn_ids)
    {
        $ctn_ids = is_array($ctn_ids) ? $ctn_ids : [$ctn_ids];

        $rows = $this->model
            ->whereIn('ctn_id', $ctn_ids)
            ->get(['ctn_id', 'loc_id', 'loc_name', 'loc_code']);

        return $rows;
    }


}
