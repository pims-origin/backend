<?php

namespace App\Api\Ecom\Controllers;

use App\Api\Ecom\Models\CartonModel;
use App\Api\Ecom\Models\WaveDtlModel;
use App\Api\Ecom\Models\WaveHdrModel;
use App\Api\Ecom\Transformers\WaveDetailTransformer;
use App\Api\Ecom\Transformers\WaveTransformer;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\WaveDtlLocModel;
use App\Api\V1\Validators\WaveValidator;
use App\Jobs\AutoPackJob;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\Tote;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

/**
 * Class WaveController
 *
 * @package App\Api\V1\Controllers
 */
class WaveController extends AbstractController
{

    /**
     * @var int|mixed
     */
    protected $userId;

    /**
     * @var WaveHdrModel
     */
    protected $waveHdrModel;

    /**
     * @var WaveDtlModel
     */
    protected $waveDtlModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    protected $customerConfigModel;

    protected $waveDtlLocModel;

    protected $orderDtlModel;

    protected $eventTrackingModel;

    protected $palletModel;

    protected $orderHdrModel;

    protected $locationModel;

    public function __construct(
        WaveHdrModel $waveHdrModel,
        WaveDtlModel $waveDtlModel,
        CartonModel $cartonModel,
        CustomerConfigModel $customerConfigModel,
        WaveDtlLocModel $waveDtlLocModel,
        OrderDtlModel $orderDtlModel,
        EventTrackingModel $eventTrackingModel,
        PalletModel $palletModel,
        OrderHdrModel $orderHdrModel,
        LocationModel $locationModel
    ) {
        $this->waveHdrModel = $waveHdrModel;
        $this->waveDtlModel = $waveDtlModel;
        $this->cartonModel = $cartonModel;
        $this->customerConfigModel = $customerConfigModel;
        $this->waveDtlLocModel = $waveDtlLocModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->palletModel = $palletModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->locationModel = $locationModel;

        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param WaveTransformer $waveTransformer
     * @param WaveValidator $waveValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search($whsId, Request $request, WaveTransformer $waveTransformer, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            $allWaves = $this->waveHdrModel->getModel()
                ->leftJoin('odr_hdr', 'wv_hdr.wv_id', '=', 'odr_hdr.wv_id')
                ->join('wv_dtl', 'wv_hdr.wv_id', '=', 'wv_dtl.wv_id')
                ->select([
                    'wv_hdr.wv_id',
                    'wv_hdr.wv_num',
                    DB::raw('COUNT(DISTINCT odr_hdr.odr_id) AS odr_ttl'),
                    DB::raw('COUNT(DISTINCT wv_dtl.wv_dtl_id) AS total'),
                    DB::raw('(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty = wv_dtl.piece_qty) as actual'),
                    DB::raw("IF(wv_hdr.wv_sts = 'NW', 'NOT', 'IN') AS status")
                ])
                ->where('wv_hdr.is_ecom', 1)
                ->where('wv_hdr.whs_id', $input['whs_id'])
                ->where("wv_dtl.picker_id", $this->userId)
                ->where(function ($query) use ($input) {
                    if (!empty($input['wv_num'])) {
                        $query->where('wv_hdr.wv_num', $input['wv_num']);
                    }
                })
                ->whereIn('wv_hdr.wv_sts', ['NW', 'PK'])
                ->groupBy('wv_hdr.wv_id')
                ->orderBy('wv_hdr.updated_at', 'DESC')
                ->get();

            if (!empty($allWaves)) {
                return $this->response->collection($allWaves, $waveTransformer);
            } else {
                return ['data' => []];
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show(
        $whsId,
        $wvHdrNum,
        Request $request,
        WaveDetailTransformer $waveDtlTransformer,
        WaveValidator $waveValidator
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        $waveValidator->validate($input);

        try {
            $wave = $this->waveDtlModel->getModel()
                ->where([
                    'whs_id'    => $input['whs_id'],
                    'wv_num'    => $wvHdrNum,
                    'picker_id' => Data::getCurrentUserId()
                ])
                ->get();

            if (empty($wave)) {
                return $this->response->errorBadRequest("This wave pick don't belong user");
            }

            return $this->response->collection($wave, $waveDtlTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function detail($whsId, $wvDtlId, WaveValidator $waveValidator)
    {
        // get data from HTTP
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;

        $waveValidator->validate($input);

        try {
            $wave = $this->waveDtlModel->getFirstWhere([
                'whs_id'    => $whsId,
                'wv_dtl_id' => $wvDtlId,
                'picker_id' => $this->userId
            ]);

            if (empty($wave)) {
                return $this->response->errorBadRequest('Wave dtl not found');
            }

            $sku_size_color = $wave->sku;
            $size = !empty($wave->size) ? strtoupper($wave->size) : '';
            if ($size != 'NA') {
                $sku_size_color .= '-' . $wave->size;
            }
            $color = !empty($wave->color) ? strtoupper($wave->color) : '';
            if ($color != 'NA') {
                $sku_size_color .= '-' . $wave->color;
            }

            $detail['data'] = [
                'wv_id'       => $wave->wv_id,
                'wv_num'      => $wave->wv_num,
                'wv_dtl_id'   => $wave->wv_dtl_id,
                'sku'         => $sku_size_color,
                'qty'         => $wave->act_piece_qty . '/' . $wave->piece_qty,
                'status'      => $this->getStatusWvDtl($wvDtlId),
                'pack_size'   => $wave->pack_size,
                'lot'         => $wave->lot,
                'pick_pallet' => $wave->pick_pallet,
                'uom_id'      => $wave->uom_id,
                'uom_code'    => $this->_getUomCodeById($wave->uom_id),
                'cus_upc'     => $wave->cus_upc,
                'ucc128'      => Item::generateUpc128($wave->cus_id, $wave->item_id)
            ];

            return $detail;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function getStatusWvDtl($wv_id)
    {
        $statusWvDtl = $this->waveDtlModel->getStatusWvDtl($wv_id);

        $status = 'NOT';
        if (!empty($statusWvDtl[0])) {
            $sumActualWvdtl = array_get($statusWvDtl[0], 'actual', 0);
            $sumPieceQtyWvdtl = array_get($statusWvDtl[0], 'piece_qty', 0);

            if ($sumActualWvdtl === $sumPieceQtyWvdtl) {
                $status = 'DONE';
            } else if ($sumActualWvdtl > 0 && $sumActualWvdtl < $sumPieceQtyWvdtl) {
                $status = 'IN';
            }
        }


        return $status;
    }

    private function _getUomCodeById($uomId)
    {
        $systemUom = DB::table('system_uom')
            ->where('deleted', 0)
            ->where('sys_uom_id', $uomId)
            ->first();

        return array_get($systemUom, 'sys_uom_code', '');
    }

    public function checkWvLocation(Request $request)
    {
        $input = $request->getParsedBody();

        $locCode = $input['loc_code'] ?: null;
        $wvDtlId = $input['wv_dtl_id'] ?: null;

        if (!$locCode || !$wvDtlId) {
            return $this->response->errorBadRequest('Location code or wave detail id is empty');
        }

        $wvDtl = WavepickDtl::where('wv_dtl_id', $input['wv_dtl_id'])->first();
        if (empty($wvDtl)) {
            $this->response->errorBadRequest('Wave detail is not existed');
        }
        if (!in_array($wvDtl->wv_dtl_sts, ['NW', 'PK'])) {
            $this->response->errorBadRequest('Wave detail is picked');
        }
        //validate customer zone
        $cusZones = DB::table('customer_zone')
            ->where('cus_id', $wvDtl->cus_id)
            ->pluck('zone_id');
        if (empty($cusZones)) {
            return $this->response->errorBadRequest('Customer has not location zone');
        }

        //validate location
        $locObj = Location::where('loc_code', $locCode)
            ->where('loc_whs_id', Data::getCurrentWhsId())
            ->first();
        if (empty($locObj)) {
            return $this->response->errorBadRequest("Location '$locCode' is not existed");
        }
        if ($locObj->loc_sts_code != 'AC') {
            return $this->response->errorBadRequest("Location is not active");
        }
        $rs = $this->cartonModel->checkLocByWvDtl($locObj->loc_id, $wvDtl->item_id, $wvDtl->lot);
        if (empty($rs['loc_id'])) {
            return $this->response->errorBadRequest("Location '$locCode' has not cartons to wavepick");
        }

        return ['data' => $rs];

    }

    public function updateWavePickEcom($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        $wvDtlId = $input['wv_dtl_id'];
        $locCode = $input['loc_code'];
        $toteNum = $input['tote_num'];

        DB::beginTransaction();


        //get wv_dtl
        $wvDtl = WavepickDtl::where([
            'wv_dtl_id' => $wvDtlId,
            'whs_id'    => $whsId
        ])->first();

        if (!in_array($wvDtl['wv_dtl_sts'], ['NW', 'PK']) || ($wvDtl['piece_qty'] == $wvDtl['act_piece_qty'])) {
            return $this->response->errorBadRequest("Wavepick detail Picked already!");
        }

        $wvHdr = $this->waveHdrModel->findWhere(['wv_id' => $wvDtl->wv_id])->first();

        if ($wvDtl->picker_id != Data::getCurrentUserId()) {
            return $this->response->errorBadRequest("This wave pick item don't belong user");
        }

        if (!$wvDtl || !$wvHdr) {
            return $this->response->errorBadRequest("Wave pick is not existed");
        }


        if (!in_array($wvHdr['wv_sts'], ['NW', 'PK'])) {
            return $this->response->errorBadRequest("Wave pick Completed already!");
        }

        $uomId = object_get($wvDtl, 'uom_id');
        $uomCode = $this->_getUomCodeById($uomId);
        $actQty = $uomCode == 'KG' ? $input['act_qty'] : (int)$input['act_qty'];

        //get location
        $loc = Location::where([
            'loc_code'   => $locCode,
            'loc_whs_id' => $whsId
        ])->first();
        if (!$loc) {
            return $this->response->errorBadRequest("Location " . $locCode . " is not existed");
        }

        //get tote
        $tote = Tote::where([
            'whs_id'    => $whsId,
            'tote_num'  => $toteNum
        ])->first();
        if (!$tote){
            return $this->response->errorBadRequest("Tote " . $toteNum . " is not existed");
        }
        if ($tote->wv_id && $tote->wv_id != $wvDtl->wv_id){
            return $this->response->errorBadRequest("Tote " . $toteNum . " does not belong to this wave pick");
        }

        /*
        * Check $loc status = IN, LK ko cho phep tiep tuc pick
        */
        if ($loc->loc_sts_code == 'IA') {
            return $this->response->errorBadRequest("Location " . $locCode . " is inactive");
        }
        if ($loc->loc_sts_code == 'LK') {
            return $this->response->errorBadRequest("Location " . $locCode . " is locked");
        }


        //validate qty
        if ($wvDtl['piece_qty'] < $actQty) {
            $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s picked Qty QTY %d is greater than Allocated Qty %d',
                $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['lot'], $wvDtl['piece_qty'], $actQty);

            return $this->response->errorBadRequest($msg);
        }

        //get algorithm
        $algorithm = $this->customerConfigModel->getPickingAlgorithm($wvDtl->whs_id, $wvDtl->cus_id);

        $qty = $this->cartonModel->getAvailableQuantity($wvDtl['item_id'], $wvDtl['lot'], $loc->loc_id,
            $loc->loc_whs_id);
        if ($qty < $actQty) {
            $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s available QTY %d is less than Picked Qty %d',
                $wvDtl['sku'], $wvDtl['size'], $wvDtl['color'], $wvDtl['lot'], $qty, $actQty);
            throw new \Exception($msg);
        }

        $actLoc = [
            'picked_qty' => $actQty,
            'act_loc_id' => $loc->loc_id,
            'act_loc'    => $loc->loc_code,
            'loc_id'     => $loc->loc_id,
            'loc_code'   => $loc->loc_code,
            'avail_qty'  => $qty
        ];


        $cartons = $this->cartonModel->getAllCartonsByWvDtl($wvDtl, $algorithm, [$loc->loc_id], [$actLoc]);

        if (!$cartons) {
            return $this->response->errorBadRequest('There is no carton to pick or duplicated submit');
        }

        try {


            $wvDtlSts = 'PK';
            if ($wvDtl->act_piece_qty + $actQty > $wvDtl->piece_qty) {
                $this->response->errorBadRequest('Pick qty greater than remaining qty of wavepick');
            }

            if ($wvDtl->act_piece_qty + $actQty == $wvDtl->piece_qty) {
                $wvDtlSts = 'PD';
            }
            $this->waveDtlModel->updateWaveDtl($wvDtlId, $actQty, $wvDtlSts);
            $data = $this->groupCartonsByLot($cartons);
            $odrCartons = $this->cartonModel->updateCartonByLocs($data['cartons']);

            $this->updateInventoryByLot($actQty, $wvDtl);

            $this->waveDtlLocModel->updateWaveDtlLoc($actLoc, $wvDtl->wv_id, $wvDtlId);

            $tote->update([
                'picker_id' => $wvDtl->picker_id,
                'wv_id'     => $wvDtl->wv_id,
                'ctn_ttl'   => DB::raw(sprintf('ctn_ttl + %d', $actQty))
            ]);

            $data = $this->orderDtlModel->updateOdrDtlPickedQty($wvDtl, $odrCartons, $tote);

            $eventDatas = [
                'evt_code'   => 'WWP',
                'info'       => sprintf('GUN - %d cartons picked in location %s', ceil($actQty / $wvDtl['pack_size']),
                    $locCode),
                'owner'      => $wvDtl['wv_num'],
                'trans_num'  => $wvDtl['wv_num'],
                'created_at' => time(),
                'created_by' => JWTUtil::getPayloadValue('jti') ?: 1
            ];

            $this->eventTrackingModel->create($eventDatas);

            $this->palletModel->updatePalletCtnTtl([$loc->loc_id]);
            $this->palletModel->updateZeroPallet([$loc->loc_id]);
            $this->palletModel->removeLocDynZone($loc->loc_id);

            //update sts
            $autoPack = $this->orderHdrModel->updateOrderPickedByWv($wvDtl->wv_id);

            $this->waveHdrModel->updateWvPicking($wvDtl->wv_id);

            $this->waveHdrModel->updateWvComplete($wvDtl->wv_id);

            $this->locationModel->pickPalletUpdateActiveLocationByWvDtlId($whsId, $wvDtl, $loc->loc_id);

            DB::commit();
            dispatch(new AutoPackJob($wvDtl->wv_id, $request));

            return ['msg' => 'successful'];
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }


    }

    private function groupCartonsByLot($locs)
    {
        $lots = [];
        $cartons = [];
        foreach ($locs as $carton) {
            if (!isset($lots[$carton['lot']])) {
                $lots[$carton['lot']] = 0;
            }
            $lots[$carton['lot']] += $carton['picked_qty'];
            $cartons[] = $carton;
        }

        return [
            'cartons' => $cartons,
            'lots'    => $lots
        ];
    }

    private function updateInventoryByLot($pickedQty, $cond)
    {
        $sqlPickQty = sprintf('`picked_qty` + %d', $pickedQty);
        $sqlAvailQty = sprintf('`allocated_qty` - %d', $pickedQty);
        $res = DB::table('invt_smr')
            ->where([
                'whs_id'  => $cond['whs_id'],
                'item_id' => $cond['item_id'],
                'lot'     => $cond['lot']
            ])
            ->update([
                    'picked_qty'    => DB::raw($sqlPickQty),
                    'allocated_qty' => DB::raw($sqlAvailQty),
                ]
            );

        return $res;
    }
}

