<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\Pallet;

class GoodsReceiptDetailTransformer extends TransformerAbstract
{
    public function transform(GoodsReceipt $goodsReceipt)
    {
        $details = object_get($goodsReceipt, 'details', null);

        return [
            'asn_hdr_num'          => object_get($goodsReceipt, 'asnHdr.asn_hdr_num', null),
            'asn_hdr_ref'          => object_get($goodsReceipt, 'asnHdr.asn_hdr_ref', null),
            'asn_sts'              => object_get($goodsReceipt, 'asnHdr.asn_sts', null),
            'sys_measurement_code' => object_get($goodsReceipt, 'asnHdr.sys_mea_code', null),
            'gr_hdr_id'            => object_get($goodsReceipt, 'gr_hdr_id', null),
            "user_id"              => $goodsReceipt->created_by,
            "user_name"            => trim(object_get($goodsReceipt, "user.first_name", null) . " " .
                object_get($goodsReceipt, "user.last_name", null)),

            // Container
            'ctnr_id'              => object_get($goodsReceipt, 'ctnr_id', null),
            'ctnr_num'             => object_get($goodsReceipt, 'container.ctnr_num', ''),
            'ctnr_note'            => object_get($goodsReceipt, 'container.ctnr_note', ''),

            'gr_hdr_ept_dt' => date('Y/m/d', object_get($goodsReceipt, 'gr_hdr_ept_dt', 0)),
            'gr_hdr_act_dt' => date('Y/m/d', object_get($goodsReceipt, 'gr_hdr_act_dt', 0)),
            'gr_hdr_num'    => object_get($goodsReceipt, 'gr_hdr_num', null),
            // Warehouse Info
            'whs_id'        => object_get($goodsReceipt, 'whs_id', null),

            // Customer Info
            'cus_id'        => object_get($goodsReceipt, 'cus_id', null),
            'cus_code'      => object_get($goodsReceipt, 'customer.cus_code', null),
            'cus_name'      => object_get($goodsReceipt, 'customer.cus_name', null),
            'labor_charge'  => object_get($goodsReceipt, 'labor_charge', null),
            'gr_in_note'    => object_get($goodsReceipt, 'gr_in_note', null),
            'gr_ex_note'    => object_get($goodsReceipt, 'gr_ex_note', null),

            // Goods Receipt Status
            'gr_sts_code'   => object_get($goodsReceipt, 'gr_sts', null),
            'gr_sts_name'   => object_get($goodsReceipt, 'goodsReceiptStatus.gr_sts_name', ''),
            'gr_sts_desc'   => object_get($goodsReceipt, 'goodsReceiptStatus.gr_sts_desc', ''),

            'is_pending'   => (int)object_get($goodsReceipt, "is_pending", 0),
            'ttl_item'     => is_null($details) ? 0 : count($details),
            "ttl_pallet"   => Pallet::where('gr_hdr_id', $goodsReceipt->gr_hdr_id)->count(),
            // Goods Receipt Details
            'gr_details'   => $details,
            'created_from' => object_get($goodsReceipt, 'created_from', null),
            'gr_type'      => object_get($goodsReceipt, 'gr_type', null),
            'pallet_ttl'   => object_get($goodsReceipt, 'pallet_ttl', null),
            'cube'         => object_get($goodsReceipt, 'cube', null),
            'ctnr_size'    => $goodsReceipt->ctnr_size ? (int)$goodsReceipt->ctnr_size : null,
        ];
    }
}

