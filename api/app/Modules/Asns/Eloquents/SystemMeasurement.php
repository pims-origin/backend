<?php

namespace App\Modules\Asns\Eloquents;

class SystemMeasurement extends BaseSoftModel
{
    protected $table = 'system_measurement';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_mea_code';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'sys_mea_code',
        'sys_mea_name',
        'sys_mea_desc',
    ];
}
