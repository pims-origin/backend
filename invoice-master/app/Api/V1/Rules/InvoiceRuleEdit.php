<?php 
namespace  App\Api\V1\Rules;

use App\InvoiceHeader;
use Illuminate\Contracts\Validation\Rule;


class InvoiceRuleEdit implements Rule
{
    /**
     * [passes description]
     * @param  [type] $attribute [description]
     * @param  [type] $value     [description]
     * @return [type]            [description]
     */
    public function passes($attribute, $value)
    {
        return InvoiceHeader::whereIn('inv_sts', ['NE', 'PE'])->find($value);
    }
    /**
     * [message description]
     * @return [type] [description]
     */
    public function message()
    {
        return 'The invoice don\'t have status New Or Pending.';
    }
}
