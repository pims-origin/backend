<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use App\SystemUom;
use League\Fractal\TransformerAbstract;

class SystemUomTransformer extends TransformerAbstract
{
    public function transform(SystemUom $systemUom)
    {
        return [
            'sys_uom_id'   => $systemUom->sys_uom_id,
            'level_id'     => $systemUom->level_id,
            'sys_uom_code' => $systemUom->sys_uom_code,
            'sys_uom_name' => $systemUom->sys_uom_name,
            'sys_uom_des'  => $systemUom->sys_uom_des,
            'sys_uom_type' => $systemUom->sys_uom_type,
            'created_at'   => $systemUom->created_at,
            'updated_at'   => $systemUom->updated_at,
            'deleted_at'   => $systemUom->deleted_at,
        ];
    }
}
