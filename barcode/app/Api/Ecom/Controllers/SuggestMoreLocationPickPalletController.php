<?php

namespace App\Api\Ecom\Controllers;

use App\Api\V1\Models\SuggestMoreLocationPickPalletModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\JWTUtil;
use App\Api\Ecom\Models\CartonModel;

/**
 * Class WaveController
 *
 * @package App\Api\V1\Controllers
 */
class SuggestMoreLocationPickPalletController extends AbstractController
{

    /**
     * @var int|mixed
     */
    protected $userId;

    /**
     * @var SuggestMoreLocationPickPalletModel
     */
    protected $cartonsModel;

    /**
     * SuggestMoreLocationPickPalletController constructor.
     */
    public function __construct()
    {
        $this->cartonsModel = new CartonModel();

        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    public function moreSuggestLocation(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            if (!isset($input['wv_dtl_id'])) {
                $this->response->errorBadRequest('Wave detail cannot empty');
            }
            $wvDtl = WavepickDtl::where('wv_dtl_id', $input['wv_dtl_id'])->first();
            if (empty($wvDtl)) {
                $this->response->errorBadRequest('Wave detail is not existed');
            }

            if (!in_array($wvDtl->wv_dtl_sts, ['NW', 'PK'])) {
                $this->response->errorBadRequest('Wave detail is picked');
            }

            $suggestLocation = $this->cartonsModel->getSugLocByWvDtl($wvDtl);

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}

