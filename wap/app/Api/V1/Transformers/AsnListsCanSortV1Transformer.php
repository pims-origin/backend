<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\AsnStatusModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Utils\Status;

class AsnListsCanSortV1Transformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(AsnDtl $asnDtl)
    {
        $asnDetail = new AsnDtlModel();
        $vtlCtn = new VirtualCartonModel();

        $asnHdr = new AsnHdrModel();
        $asnHdr = $asnHdr->getFullASN($asnDtl->asn_hdr_id);

        $expDate = object_get($asnHdr, 'asn_hdr_ept_dt', '');
        $expDate = ($expDate) ? date('m/d/Y', $expDate) : '';

        $ctnr = new ContainerModel();
        $ctnr = $ctnr->getFirstBy('ctnr_id', $asnDtl->ctnr_id);

        return [
            'asn_hdr_id'     => $asnHdr->asn_hdr_id,
            'asn_hdr_num'    => $asnHdr->asn_hdr_num,
            'asn_hdr_ref'    => $asnHdr->asn_hdr_ref,
            "asn_date"       => $asnHdr->created_at->format('m/d/Y'),
            'cus_name'       => $asnHdr->cus_name,
            'whs_id'         => $asnHdr->whs_id,
            'cus_id'         => $asnHdr->cus_id,
            'ctnr_id'        => $ctnr->ctnr_id,
            'ctnr_num'       => $ctnr->ctnr_num,
            'asn_hdr_ept_dt' => $expDate,
            'asn_sts'        => $asnDetail->formatStatus('default', $asnHdr->asn_sts),
            'asn_sts_name'   => $asnHdr->asn_sts_name,
            'total-sku'      => $asnDtl->total_sku,
            'total-cartons'  => $asnDtl->total_cartons,
            'items'          => $asnDtl->items,
        ];

    }

}
