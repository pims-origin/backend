<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class LocationModel extends AbstractModel
{
    protected $warehouseZone;

    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }


    public function loadByZoneIds($zoneIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('loc_zone_id', $zoneIds);
        $this->model->filterData($query);
        // Get
        $models = $query->get();

        return $models;
    }

    public function loadByZoneId($zoneId, $with = [])
    {
        $query = $this->make($with);
        $query->where('loc_zone_id', $zoneId);
        $this->model->filterData($query);
        // Get
        $models = $query->get();

        return $models;
    }

}
