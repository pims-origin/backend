<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Utils\Message;

class ContainerControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function getEndPoint()
    {
        return "/v1/containers/";
    }

    protected $containerParams = [
        'ctnr_num'  => 'CT00111',
        'ctnr_note' => 'This container is created by Functional Test',
    ];

    public function test_Search_Ok()
    {
        // Test Search
        $this->call('GET', $this->getEndPoint());

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }

}
