<?php
/**
 * Created by PhpStorm.
 * User: dai.ho
 * Date: 09/05/2016
 * Time: 09:35
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MenuFavoriteModel;
use App\Api\V1\Models\MenuGroupModel;
use App\Api\V1\Models\MenuModel;
use App\Api\V1\Transformers\MenuGroupTransformer;
use App\Api\V1\Validators\MenuGroupValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Dingo\Api\Exception\ValidationHttpException;

class MenuGroupController extends AbstractController
{
    /**
     * @var $menuGroupModel
     */
    protected $menuGroupModel;
    /**
     * @var $menuGroupTransformer
     */
    protected $menuGroupTransformer;
    /**
     * @var $menuGroupValidator
     */
    protected $menuGroupValidator;
    /**
     * @var $menuModel
     */
    protected $menuModel;
    /**
     * @var $menuFavoriteModel
     */
    protected $menuFavoriteModel;

    /**
     * MenuGroupController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->menuGroupModel = new MenuGroupModel();
        $this->menuGroupTransformer = new MenuGroupTransformer();
        $this->menuGroupValidator = new MenuGroupValidator();
        $this->menuModel = new MenuModel();
        $this->menuFavoriteModel = new MenuFavoriteModel();
    }

    /**
     * @SWG\Get(
     *     tags={"Menu Group"},
     *     path="/v1/groups",
     *     summary="List All Menu Group",
     *     description="List All Menu Group",
     *     operationId="GET_listMenuGroup",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Return array all Menu Group",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_group_id": 1,
     *                     "name": "Header Menu",
     *                     "description": "This is example from Swagger",
     *                 },
     *                 {
     *                     "menu_group_id": 2,
     *                     "name": "Footer Menu",
     *                     "description": "This is example from Swagger",
     *                 },
     *             }
     *         },
     *     ),
     * )
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $menuGroups = $this->menuGroupModel->all();

        return $this->response->collection($menuGroups, $this->menuGroupTransformer);
    }

    /**
     * @SWG\Get(
     *     tags={"Menu Group"},
     *     path="/v1/groups/{menuGroupId}",
     *     summary="Read Menu Group",
     *     description="View Detail Menu Group",
     *     operationId="GET_show",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="menuGroupId",
     *         description="Menu Group Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return item of Menu Group",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_group_id": 1,
     *                     "name": "Header Menu",
     *                     "description": "This is example from Swagger",
     *                 }
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Menu Group Detail
     *
     * @param $menuGroupId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($menuGroupId)
    {
        try {
            $menuGroup = $this->menuGroupModel->byId($menuGroupId);

            return $this->response->item($menuGroup, $this->menuGroupTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     tags={"Menu Group"},
     *     path="/v1/groups",
     *     summary="Create Menu Group",
     *     description="Create Menu Group",
     *     operationId="POST_createMenuGroup",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         description="Menu Group's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="description",
     *         description="Menu Group's Description",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Return item of Menu Group",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_group_id": 1,
     *                     "name": "Header Menu",
     *                     "description": "This is example from Swagger",
     *                 }
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Menu Group Create
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->menuGroupValidator->validate($input);

        $params = [
            'name'        => $input['name'],
            'description' => $input['description']
        ];

        try {
            // check duplicate name
            if ($this->menuGroupModel->getByName($params['name'])) {
                throw new \Exception('Menu Group name is existed.');
            }

            if ($menuGroup = $this->menuGroupModel->create($params)) {
                return $this->response->item($menuGroup,
                    $this->menuGroupTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (ValidationHttpException $e) {
            return $this->response->error(implode("\n", $e->getErrors()->all()),
                IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    /**
     * @SWG\Put(
     *     tags={"Menu Group"},
     *     path="/v1/groups/{menuGroupId}",
     *     summary="Update Menu Group",
     *     description="Update Menu Group",
     *     operationId="PUT_updateMenuGroup",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="menuGroupId",
     *         description="Menu Group Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         description="Menu Group's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="description",
     *         description="Menu Group's Description",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return item of Menu Group",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_group_id": 1,
     *                     "name": "Header Menu",
     *                     "description": "This is example from Swagger",
     *                 }
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * @param $menuGroupId
     * @param Request $request
     *
     * @return mixed
     */
    public function update(
        $menuGroupId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->menuGroupValidator->validate($input);

        $params = [
            'menu_group_id' => $menuGroupId,
            'name'          => $input['name'],
            'description'   => $input['description']
        ];

        try {
            // check duplicate name
            if (($menuGroup = $this->menuGroupModel->getByName($params['name']))
                && $menuGroup->menu_group_id != $menuGroupId
            ) {

                throw new \Exception('Menu Group name is existed.');
            }

            if ($menuGroup = $this->menuGroupModel->update($params)) {
                return $this->response->item($menuGroup, $this->menuGroupTransformer);
            }
        } catch (ValidationHttpException $e) {
            return $this->response->error(implode("\n", $e->getErrors()->all()),
                IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    /**
     * @SWG\Delete(
     *     tags={"Menu Group"},
     *     path="/v1/groups/{menuGroupId}",
     *     summary="Delete Menu Group",
     *     description="Delete a Menu Group",
     *     operationId="menuGroupId",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="menuGroupId",
     *         description="Menu Group Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Menu Group was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param $menuGroupId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($menuGroupId)
    {
        try {
            if ($this->menuGroupModel->deleteWithMenuAndFavorite($menuGroupId, $this->menuModel,
                $this->menuFavoriteModel)
            ) {
                return $this->response->noContent();
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Cannot Delete this item');
    }
}
