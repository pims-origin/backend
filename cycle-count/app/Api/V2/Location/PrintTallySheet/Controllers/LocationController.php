<?php

namespace App\Api\V2\Location\PrintTallySheet\Controllers;

use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use mPDF;
use Ouzo\Utilities\Arrays;
use Ouzo\Utilities\Functions;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;

class LocationController extends AbstractController
{

    public function __construct
    (
    ) {
    }

    public function printTallySheet(
        Request $request
    ) {
        $input = $request->getParsedBody();

        $userInfo = new \Wms2\UserInfo\Data();
        $userInfo = $userInfo->getUserInfo();
        $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
        $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

        //Export to PDF File
        $this->createTallySheet($printedBy);
    }

    /**
     * @param $printedBy
     *
     * @throws \MpdfException
     */
    private function createTallySheet($printedBy)
    {
        $pdf = new Mpdf();
        $html = (string)view('TallySheetPrintoutTemplate', [
            'printedBy' => $printedBy
        ]);

        $pdf->WriteHTML($html);
        $pdf->Output("tallySheet.pdf", "D");
    }

}
