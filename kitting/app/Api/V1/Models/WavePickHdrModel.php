<?php

namespace App\Api\V1\Models;

use App\WavepickHdr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use App\Wms2\UserInfo\Data;

class WavePickHdrModel extends AbstractModel
{

    protected $model;

    /**
     * @param $model
     * WavePickModel constructor.
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickHdr($data){
        return $this->model->create($data);
    }

    /**
     * @return mixed
     */
    public function getLatestWavePickNumber()
    {
        return $this->model->orderBy('wv_num', 'desc')->take(1)->first();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getWvHdrInfo($wv_id){
        $whs_id = self::getCurrentWhsId();
        return $this->model
            ->where('wv_hdr.wv_id','=',$wv_id)
            ->where('wv_hdr.whs_id','=',$whs_id)
            ->select(
                'wv_hdr.wv_id',
                'wv_hdr.wv_num',
                'wv_hdr.wv_sts',
                'wv_hdr.whs_id',
                'wv_hdr.picker'
            )
            ->first();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->whereHas('orderHdr', function ($query) {
            $query->where('odr_type', 'KIT');
        });
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId   = array_get($userInfo, 'current_whs', 0);
        $cusLsts = array_get($userInfo, 'user_customers', 0);
        $cusIds = array_column($cusLsts, 'cus_id');
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;

        $query->where('whs_id', $whsId);

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['wv_num','wv_sts'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }
        // concat all cus_id of order of wave pick
        // select wave has concat column in customer current user
        $query->whereHas("details", function ($query) use ($cusIds, $attributes) {
            $query->whereIn("cus_id", $cusIds);
            //search by cus_id
            if (isset($attributes['cus_id']) && $attributes['cus_id'] != '') {
                $query->where('cus_id', $attributes['cus_id']);
            }
        });
        if (!empty($attributes['picker_id'])) {
            $count = DB::table('user_role')->where('user_id', $userId)->whereIn('item_name', ['Admin', 'manager'])
                ->count();
            if (!$count) {
                $query->where('picker', $userId);
            }
        }
        if (!empty($attributes['picker'])) {

            $query->whereHas("pickerUser", function ($q) use ($attributes) {
                $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['picker']) . "%");
                $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['picker']) . "%");
                $q->orWhere(
                    DB::raw("concat(first_name, ' ', last_name)"),
                    'like',
                    "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['picker'])) . "%"
                );
            });
        }

        //check no-ecom
        $query->where('is_ecom', 0);

        $this->model->filterData($query);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     * @deprecated  DONt USE it
     */
    public function updateStatusWvHdr($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    public function updateWvComplete($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function updateWvPicking($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Picking", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param $wvHdrIds
     *
     * @return mixed
     */
    public function getWvHdrId($wvHdrIds)
    {
        $wvHdrIds = is_array($wvHdrIds) ? $wvHdrIds : [$wvHdrIds];

        $rows = $this->model
            ->whereIn('wv_id', $wvHdrIds)
            ->get();

        return $rows;
    }
}
