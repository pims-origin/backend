<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group([
    'prefix' => 'v1',
    'namespace' => 'App\Api\V1\Controllers',
], function ($app) {
    $app->get('menu/left-menu', 'MenuController@getLeftMenu');
    $app->get('menu/left-full-menu', 'MenuController@getFullLeftMenu');
    $app->get('menu/favourite-menu', 'MenuController@getFavouriteMenu');
    $app->post('menu/favourite-menu', 'MenuController@addFavouriteMenuItem');
    $app->delete('menu/favourite-menu/{id}', 'MenuController@removeFavouriteMenuItem');
    $app->get('menus', 'MenuController@getMenu');
    $app->post('menus', 'MenuController@createMenu');
    $app->post('menus/{groupId}', 'MenuController@createMenuItem');
    $app->get('menus/{groupId}', 'MenuController@getMenuItem');

    // USER MANAGEMENT
    $app->get('users/roles', 'UserController@getRoleByLoggedUser');
    $app->get('users/profile', 'UserController@getProfile');
    $app->put('users/profile', 'UserController@updateProfile');
    $app->get('users', 'UserController@getList');
    $app->get('users/{userId:[0-9]+}', 'UserController@getUserDetail');
    $app->get('users/info/{userId:[0-9]+}', 'UserController@getUserInfo');
    $app->get('users/{userId}/roles', 'UserController@getRoleByUser');

    $app->get('users/permissions', 'UserController@getPermissionByLoggedUser');
    $app->post('users/{userId}/roles', 'UserController@assignUser');
    $app->post('users', 'UserController@createUser');
    $app->post('users/change-password', 'UserController@changePassword');
    $app->post('users/reset-password/{userId:[0-9]+}', 'UserController@resetPassword');
    $app->put('users/{userId}', 'UserController@updateUser');
    $app->delete('users/{userId}', 'UserController@deleteUser');
    $app->delete('users', 'UserController@deleteMultipleUser');
    $app->get('users/export/{type}', 'UserController@userExport');
    $app->post('users/report-issue', 'UserController@reportIssue');

    // ROLE
    $app->get('roles', 'RoleController@getList');
    $app->get('roles/{role}/users', 'RoleController@getUserByRole');
    $app->get('roles/{role}/permissions', 'RoleController@getPermissionByRole');
    $app->post('roles/{role}/permissions', 'RoleController@assignPermission');
    $app->get('roles/{role}', 'RoleController@getRoleDetail');
    $app->post('roles', 'RoleController@createRole');
    $app->put('roles/{roleName}', 'RoleController@updateRole');
    $app->post('roles/delete', 'RoleController@deleteRoles');

        // PERMISSION
        $app->get('permissions', 'PermissionController@getList');
        $app->get('permissions/groups', 'PermissionController@getGrpList');
        $app->get('permission/{permission}/users', 'PermissionController@getUserByPermission');

    //FORGOT PASSWORD
    $app->post('forgot-password', 'FrontEndController@forgotPassword');
    $app->post('reset-password', 'FrontEndController@resetPassword');
    $app->post('setup-password', 'FrontEndController@setupPassword');
    $app->get('check-reset-password-token/{token}', 'FrontEndController@checkResetPasswordToken');

    // AUTHENTICATION
    $app->post('login', 'AuthenticationController@actionLogin');
    $app->get('logout', 'AuthenticationController@actionLogout');

    // SYSTEM CONFIGURATION
    $app->get('system/general', 'SystemController@getGeneralInfo');
    $app->post('system/general', 'SystemController@postSystemInfo');
    $app->get('system/smtp', 'SystemController@getSMTPInfo');
    $app->post('system/smtp', 'SystemController@postSystemInfo');

});
