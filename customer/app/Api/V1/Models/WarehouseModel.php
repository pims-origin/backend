<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:32
 */

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\Warehouse;
use \Seldat\Wms2\Models\CustomerWarehouse;
use Illuminate\Support\Facades\DB;

class WarehouseModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     *
     * @param Warehouse $model
     */
    public function __construct(Warehouse $model = null)
    {
        $this->model = ($model) ?: new Warehouse();
    }

    public function listWhsNotInCus($cusId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model->select([
            'warehouse.whs_id',
            'warehouse.whs_code',
            'warehouse.whs_name'
        ])
            ->whereNotIn('warehouse.whs_id', function($query) use ($cusId){
                $query->select('whs_id')
                    ->from(with(new CustomerWarehouse)->getTable())
                    ->where('cus_id', $cusId)
                    ->where('deleted', 0);
            })
            ->where('warehouse.whs_status', '<>', 'IA')
            ->get();
    }
}
