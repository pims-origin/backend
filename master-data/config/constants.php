<?php

return [
    'master_data_type' => [
        'status_dropdown' => [
            'IA' => 'Inactive',
            'AC' => 'Active',
        ],
        'status' => [
            'inactive' => 'IA',
            'active'   => 'AC',
        ],
    ],
    'master_data' => [
        'status_dropdown' => [
            'IA' => 'Inactive',
            'AC' => 'Active',
        ],
        'status' => [
            'inactive' => 'IA',
            'active'   => 'AC',
        ],
    ]

];
?>