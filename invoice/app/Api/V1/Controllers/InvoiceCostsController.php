<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Response as IlluminateResponse;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\InvoiceCostsModel;
use App\Api\V1\Transformers\InvoiceCostsTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class InvoiceCostsController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new InvoiceCostsModel();
        $this->transformer = new InvoiceCostsTransformer();
    }

    /*
    ****************************************************************************
    */

    public function checkChagreCode(Request $request)
    {
        $input = $request->getQueryParams();

        $results = $this->model->checkChagreCode($input);

        return json_encode($results);
    }

    /*
    ****************************************************************************
    */

    public function updateCosts(Request $request)
    {
        $input = $request->getParsedBody();

        try {

            $results = $this->model->updateCosts($input);

            return $this->response->item($results, $this->transformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
    ****************************************************************************
    */

}
