<?php

namespace App\Modules\Asns\Validators;

use Validator;
use App\Modules\Asns\Models\AbstractModel;

class CreateValidator
{
    public function rules()
    {
        return [
            // Validate asnHdr
            'ctnr_num'          => 'required',
            'ref_code'          => 'required',
            'cus_id'            => 'required',

            // Validate asnDtl
            'items.*.item_id'   => 'required|integer',
            'items.*.sku'       => 'required',
            'items.*.size'      => 'nullable',
            'items.*.color'     => 'nullable',
            'items.*.des'       => 'nullable',
            'items.*.pack'      => 'nullable',
            'items.*.uom'       => 'required|in:ct,pc',
            'items.*.qty'       => 'required|integer|min:1',
            'items.*.lot'       => 'nullable',
            'items.*.ept_dt'    => 'nullable|date|after:' . date('Y/m/d'),
            'items.*.cat'       => 'nullable|max:3'
        ];
    }

    public function validate($input)
    {
        $items = array_get($input, 'items');

        if ($items) {
            foreach ($items as $key => $item) {
                $input['items'][$key]['uom'] = strtolower($item['uom']);
            }
        }

        $validator = Validator::make($input, $this->rules(), $this->messages());

        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                }
            }
        }

        return $validator;
    }

    public function messages()
    {
        return [
            // Validate asnHdr
            'ctnr_num.required'         => 'Container is required.',
            'ref_code.required'         => 'Refcode is required.',
            'cus_id.required'           => 'Customer is required.',

            // Validate asnDtl
            'items.*.item_id.required'  => 'Item ID is required.',
            'items.*.item_id.integer'   => 'Item ID must be number.',
            'items.*.sku.required'      => 'SKU is required.',
            'items.*.uom.required'      => 'UOM is required.',
            'items.*.uom.in'            => 'UOM must belong to (Carton, Piece).',
            'items.*.qty.required'      => 'Quantity is required.',
            'items.*.qty.integer'       => 'Item ID must be number.',
            'items.*.qty.min'           => 'Quantity is invalid ( MIN VALUE IS 1 ).',
            'items.*.ept_dt.date'       => 'Expected date format is invalid.',
            'items.*.ept_dt.after'      => 'Expected date must be after current date.',
            'items.*.cat.max'           => 'Category is invalid ( Max string lenght is 3 ).',
        ];
    }

    public function checkCreateAsn($asn)
    {
        if(empty($asn))
        {
            throw new \Exception(json_encode(['title' =>'Create asn unsuccessfully', 'detail' => 'Please try']));
        }
    }
}