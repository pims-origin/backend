<?php

namespace App\Api\Bay\Controllers;

use App\Api\Bay\Models\BayModel;
use App\Api\Bay\Models\ContainerChargeModel;
use App\Api\Bay\Transformers\BayTransformer;
use App\Api\Bay\Validators\BayValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Bay;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;


/**
 * Class BayController
 *
 * @package App\Api\Bay\Controllers
 */
class BayController extends AbstractController
{

    protected $userId;
    /**
     * @var BayModel
     */
    protected $bayModel;
    /**
     * @var ContainerChargeModel
     */
    protected $ctnrChargeModel;

    /**
     * BayController constructor.
     *
     * @param BayModel $bayModel
     */
    public function __construct(
        BayModel $bayModel,
        ContainerChargeModel $ctnrChargeModel
    ) {
        $this->bayModel = $bayModel;
        $this->ctnrChargeModel = $ctnrChargeModel;
        $this->userId = JWTUtil::getPayloadValue('jti') ? JWTUtil::getPayloadValue('jti') : 0;
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param BayTransformer $bayTransformer
     * @param BayValidator $bayValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        Request $request,
        BayTransformer $bayTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {

            $input['sort']['line'] = 'ASC';
            $input['sort']['pos'] = 'ASC';
            $bays = $this->bayModel->search($input, [], array_get($input, 'limit', 99999999));

            $bayIds = [];
            foreach ($bays as $bay) {
                $bayIds[$bay->bay_id] = $bay->bay_id;
            }

            //  Get Container Charge By BayIds
            $ctnrCharges = $this->ctnrChargeModel->getCtnrChargeOpenByBayIds($bayIds);

            $arrCtnrCharge = [];
            if ($ctnrCharges) {
                foreach ($ctnrCharges as $ctnrCharge) {
                    $arrCtnrCharge[$ctnrCharge->bay_id] = [
                        'cc_id'     => object_get($ctnrCharge, 'cc_id', null),
                        'ctnr_id'   => object_get($ctnrCharge, 'ctnr_id', null),
                        'ctnr_num'  => object_get($ctnrCharge, 'ctnr_num', null),
                        'cus_id'    => object_get($ctnrCharge, 'cus_id', null),
                        'cus_name'  => object_get($ctnrCharge, 'cus_name', null),
                        'temp'      => object_get($ctnrCharge, 'temp', null),
                        'cc_sts'    => object_get($ctnrCharge, 'cc_sts', null),
                        'cc_sts_name' => object_get($ctnrCharge, 'cc_sts', null)
                            ? Status::getByKey('Container_Charge_Status',object_get($ctnrCharge, 'cc_sts', null))
                            : '',
                        'start'     => object_get($ctnrCharge, 'start', null)
                            ? date('m/d/Y H:i', strtotime(object_get($ctnrCharge, 'start', null))) : '',
                        'end'       => object_get($ctnrCharge, 'end', null)
                            ? date('m/d/Y H:i', strtotime(object_get($ctnrCharge, 'end', null))) : '',
                        'duration'  => object_get($ctnrCharge, 'duration', null)
                    ];
                }
            }

            $items = [];
            if ($bays) {
                foreach ($bays as $bay) {
                    $items[$bay->line][$bay->pos] = [
                        'bay_id' => object_get($bay, 'bay_id', null),
                        'bay_name' => object_get($bay, 'bay_name', null),
                        'bay_sts' => object_get($bay, 'bay_sts', null),
                        'bay_sts_name' => Status::getByKey('BAY_STATUS',object_get($bay, 'bay_sts')),
                        'pos' => object_get($bay, 'pos', null),
                        'line' => object_get($bay, 'line', null),
                        'container' => !empty($arrCtnrCharge[$bay->bay_id])
                            ? $arrCtnrCharge[$bay->bay_id] : []
                    ];
                }

                foreach ($items as $key => $item) {
                    $items[$key] = array_values($item);
                }
            }

            return ['data' => $items];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $bayId
     * @param BayTransformer $bayTransformer
     * @return mixed
     */
    public function show($bayId, BayTransformer $bayTransformer)
    {
        try {
            $bays = $this->bayModel->allBy("bay_id", $bayId);

            return $this->response->collection($bays, $bayTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param BayValidator $bayValidator
     * @return mixed
     */
    public function store(
        Request $request,
        BayValidator $bayValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $bayValidator->validate($input);

        try {
            DB::beginTransaction();

            $bay = $this->bayModel->create($input);

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'bay_id' => $bay->bay_id
                ]
            ])->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $bayId
     * @param Request $request
     * @param BayValidator $bayValidator
     * @return mixed
     */
    public function update(
        $bayId,
        Request $request,
        BayValidator $bayValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['bay_id'] = $bayId;
        $bayValidator->validate($input);

        $paramsUpdate = [
            'bay_name' => $input['bay_name'],
            'bay_sts'  => $input['bay_sts'],
            'pos'      => (int)$input['pos'],
            'line'     => (int)$input['line']
        ];

        try {
            DB::beginTransaction();

            // Update Bay Model
            $this->bayModel->refreshModel();
            $this->bayModel->updateWhere($paramsUpdate,['bay_id' => $bayId]);

            DB::commit();
            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'bay_id' => $bayId
                ]
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

}

