<?php

namespace App\Api\V1\Models;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Inventories;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use stdClass;
use Wms2\UserInfo\Data;

class ItemModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    protected $cartonModel;

    protected $carton;

    public function __construct()
    {
        $this->model = new Item();
        $this->inventorySummaryModel = new InventorySummaryModel();
        $this->cartonModel = new CartonModel();
        $this->carton = new Carton();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $whs_id = Data::getCurrentWhsId();

        $query = $this->make($with);

        $arrLike = ['color', 'sku', 'size', 'lot'];
        $arrEqual = [
            'item_id',
            'cus_upc',
        ];

        // filter by request param
        if (!empty($attributes)) {
            if (isset($attributes['cus_id'])) {
                $query = $query->where('item.cus_id', $attributes['cus_id']);
            }
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query = $query->where('item.'.$key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual)) {
                    $query = $query->where('item.'.$key, SelStr::escapeLike($value));
                }
            }
        }

        // join invt_smr to get sum avail qty
        $query->join('invt_smr', 'item.item_id', '=', 'invt_smr.item_id')
            ->where('whs_id', $whs_id);

//        ->where('invt_smr.lot', '<>', 'ECO');

        $query->select(
            'item.*',
            'invt_smr.avail',
            'invt_smr.lot'
        );

        $query->groupBy('item.item_id');
        
        $result = $query->paginate($limit);
        return $result;
        
//         if (!$result->isEmpty()) {
//             $lots = Inventories::whereIn('item_id', $result->pluck('item_id')->toArray())
//                 ->where('whs_id', '=', $whs_id)
//                 ->get([
//                     'item_id',
//                     'lot',
//                     'in_hand_qty as avail'
//                 ])->groupBy('item_id')->toArray();
            
//             $result = $result->map(function ($item) {
//                 return $item;
//             });
//         }
    
//         $page = Paginator::resolveCurrentPage() ?: 1;
    
//         return new LengthAwarePaginator($result, count($result), $limit, $page,[
//             'path' => LengthAwarePaginator::resolveCurrentPath()
//         ]);
    }
    
    /**
     * Rebuild pack size to array on page create order
     * 
     * @param LengthAwarePaginator $models    Pointer
     * @param Array                $itemIdArr Array item id
     */
    private function rebuildPackSizeArr(&$models, $itemIdArr)
    {
        $itemIdArr = array_values($itemIdArr);
        $cartonsModel = new CartonModel();
        $packObj = $cartonsModel->getCartonsByItemId($itemIdArr);
        $packArr = [];

        foreach ($packObj as $key => $value) {
            $packSize = $value['ctn_pack_size'];
            $packArr[$value['item_id']][$packSize] = $packSize;
        }

        foreach ($models as $idx => $val) {
            //$models[$idx]->pack_arr = $packArr[$val['item_id']];
            if (isset($packArr[$val['item_id']])) {
                $arr = array_values($packArr[$val['item_id']]);
                $arrItem = [];
                foreach($arr as $value) {
                    $obj = new stdClass();
                    $obj->pack_size = $value;
                    $arrItem[] = $obj;
                }

                $models[$idx]->pack =$arrItem;
            } else {
                $obj = new \stdClass();
                $obj->pack_size = $val['pack'];
                $tmp = [$obj];
                $models[$idx]->pack = $tmp;
            }
        }
    }

    public function fetchColumn($columns = [], $where = "", $params = [], $upperCase = false)
    {
        $tblName = $this->getTable();
        $colNames = implode(', ', $columns) . ",uom_id,lot";

        $query = "
            SELECT $colNames
            FROM $tblName
        ";
        if (!empty($where)) {
            $query .= " WHERE $where";
        }

        $result = [];

        $db = $this->model->getConnection()->getPdo();

        $statement = $db->prepare($query);
        $statement->execute($params);

        $colKey = $columns;
        end($columns);
        $endKey = key($columns);
        unset($colKey[$endKey]);

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $rowRight = $row;
            $rowLeft = array_splice($rowRight, 0, (count($rowRight) - 2));
            end($rowLeft);
            $end = key($rowLeft);
            $val = [
                $columns[$endKey] => $row[$columns[$endKey]],
                'uom_id'          => $row['uom_id'],
                'lot'             => $row['lot'],
            ];
            unset($rowLeft[$end]);
            $key = implode("-", $rowLeft);

            if ($upperCase) {
                $key = strtoupper($key);
            }

            $result[$key] = $val;
        }

        return $result;
    }

    public static function generateItemCode($cusId, $number = 1)
    {
        return null;
        // DB::setFetchMode(\PDO::FETCH_ASSOC);
        // $cusCode = Customer::where('cus_id', $cusId)
        //     ->value('cus_code');

        // if (empty($cusCode)) {
        //     throw new HttpException(403, 'This customer has customer_code invalid');
        // }

        // $key = '91' . $cusCode;

        // $seq = DB::table('seq')
        //     ->where([
        //         'tbl_name' => 'item',
        //         'key'      => $key
        //     ])
        //     ->value('seq');

        // if (empty($seq)) {
        //     $seq = $key . '000001';
        //     //update
        //     DB::table('seq')
        //         ->insert([
        //             'tbl_name' => 'item',
        //             'key'      => $key,
        //             'seq'      => $seq
        //         ]);
        // } else {
        //     $seq++;
        //     //update
        //     DB::table('seq')
        //         ->where([
        //             'tbl_name' => 'item',
        //             'key'      => $key
        //         ])
        //         ->update(['seq' => $seq]);
        // }

        // return $seq;
    }
}
