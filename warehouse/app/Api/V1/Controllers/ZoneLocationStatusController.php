<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ZoneLocationStatusModel;
use App\Api\V1\Transformers\ZoneLocationStatusTransformer;
use App\Api\V1\Validators\ZoneLocationStatusValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class ZoneLocationStatusController extends AbstractController
{
    /**
     * @var $zoneLocationStatusModel
     */
    protected $zoneLocationStatusModel;

    /**
     * @var $zoneLocationStatusTransformer
     */
    protected $zoneLocationStatusTransformer;

    /**
     * ZoneLocationStatusController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->zoneLocationStatusModel = new ZoneLocationStatusModel();
        $this->zoneLocationStatusTransformer = new ZoneLocationStatusTransformer();
    }

    /**
     * @SWG\Get(
     *     path="/v1/zoneLocationStatus/search",
     *     summary="Search Zone Location Status",
     *     description="Search Zone Location Status",
     *     operationId="searchZoneLocationStatus",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Return all found result of Zone Location Status",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="whs_zone_location_status_id", type="integer", description=""),
     *             @SWG\Property(property="whs_loc_sts_qualifier", type="string", description=""),
     *             @SWG\Property(property="whs_loc_sts_desc", type="string", description=""),
     *             @SWG\Property(property="created_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="updated_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="deleted_at", type="string", format="date-time", description=""),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_zone_location_status_id": 2,
     *                     "whs_loc_sts_qualifier": "ddd",
     *                     "whs_loc_sts_desc": "cccccccccc",
     *                     "created_at": {
     *                         "date": "2016-06-02 14:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "updated_at": {
     *                         "date": "2016-06-02 14:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": 915148800
     *                 },
     *             },
     *         },
     *     ),
     * )
     *
     * Zone Location Status Search
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $zoneLocationStatus = $this->zoneLocationStatusModel->search($input);

            return $this->response->paginator($zoneLocationStatus, $this->zoneLocationStatusTransformer);

        } catch (\Exception $e) {
            return  $this->response->errorBadRequest(SystemBug::writeSysBugs($e, __FUNCTION__));
        }
    }
}
