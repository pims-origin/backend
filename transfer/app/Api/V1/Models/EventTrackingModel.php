<?php

namespace App\Api\V1\Models;


use Seldat\Wms2\Models\EventTracking;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * @param EventTracking $model
     */
    public function __construct(EventTracking $model = null)
    {
        $this->model = ($model) ?: new EventTracking();
    }


}
