<?php

return [
    'loc_status' => [
        'ACTIVE'   => 'AC',
        'INACTIVE' => 'IA',
    ],
    'PAGING_LIMIT' => 20
    ,
    'putaway_status' =>[
        'NEW'          => 'NW',
        'COMPLETED'    => 'CO',
    ]
];