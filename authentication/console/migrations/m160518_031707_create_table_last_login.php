<?php

use yii\db\Migration;

/**
 * Handles the creation for table `last_login`.
 */
class m160518_031707_create_table_last_login extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('last_login', [
            'user_id'    => $this->primaryKey(),
            'login_at'   => $this->integer(),
            'expired_at' => $this->integer(),
            'status'     => $this->string(2),
        ]);
        $this->addForeignKey(
            'fk-last_login-user_id',
            'last_login',
            'user_id',
            'users',
            'user_id',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
//        $this->dropTable('table_last_login');
    }
}
