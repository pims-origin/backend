<?php

namespace App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models;

use Illuminate\Support\Collection;
use Seldat\Wms2\Models\Logs;
use Psr\Http\Message\ServerRequestInterface as Request;

class LogsModel extends AbstractModel
{

    protected $asnHdr;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new Logs();
    }

    public function insertError(Request $request, $whsId, $logsData=[]){

        $resData = $request->getParsedBody();
        $headerData =  $request->getHeaders();

        // Logs Model
        $this->refreshModel();

        return $this->create([
            'whs_id' => $whsId,
            'type' => 'error',
            'evt_code' => $logsData['evt_code'],
            'owner' => $logsData['owner'],
            'transaction' => $logsData['transaction'],
            'url_endpoint' => $logsData['url_endpoint'],
            'message' => $logsData['message'],
            'header' => \GuzzleHttp\json_encode($headerData),
            'request_data' => \GuzzleHttp\json_encode($resData),
            'response_data' => $logsData['response_data']
        ]);

    }

    public function insertInfo(Request $request, $whsId, $logsData=[]){

        if (!env('API_LOG_INFO')) {
            return false;
        }
        $resData = $request->getParsedBody();
        $headerData =  $request->getHeaders();

        // Logs Model
        $this->refreshModel();

        return $this->create([
            'whs_id' => $whsId,
            'type' => 'info',
            'evt_code' => $logsData['evt_code'],
            'owner' => $logsData['owner'],
            'transaction' => $logsData['transaction'],
            'url_endpoint' => $logsData['url_endpoint'],
            'message' => $logsData['message'],
            'header' => \GuzzleHttp\json_encode($headerData),
            'request_data' => \GuzzleHttp\json_encode($resData),
            'response_data' => $logsData['response_data']
        ]);

    }

    public function insertDebug(Request $request, $whsId, $logsData=[]){

        if (!env('API_DEBUG')) {
            return false;
        }
        $resData = $request->getParsedBody();
        $headerData =  $request->getHeaders();

        // Logs Model
        $this->refreshModel();

        return $this->create([
            'whs_id' => $whsId,
            'type' => 'debug',
            'evt_code' => $logsData['evt_code'],
            'owner' => $logsData['owner'],
            'transaction' => $logsData['transaction'],
            'url_endpoint' => $logsData['url_endpoint'],
            'message' => $logsData['message'],
            'header' => \GuzzleHttp\json_encode($headerData),
            'request_data' => \GuzzleHttp\json_encode($resData),
            'response_data' => $logsData['response_data']
        ]);

    }
}
