<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerSacModel;
use App\Api\V1\Transformers\CustomerSacTransformer;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use App\Api\V1\Models\CustomerSac;
use App\Api\V1\Validators\CustomerSacValidator as Validator;
use Illuminate\Support\Facades\DB;

class CustomerSacController extends AbstractController
{
    protected $customerSacModel;

    protected $transformer;

    /**
     * Init.
     *
     */
    public function __construct()
    {
        $this->customerSacModel = new CustomerSacModel();
        $this->transformer = new CustomerSacTransformer();
    }
    
    /**
     * Display a listing of the resource.
     * @param IRequest $request
     * @return mixed
     */
    public function search(IRequest $request)
    {
        $input = $request->getQueryParams();

        try {
            // get list SAC
            $workOrders = $this->customerSacModel->search($input,[], array_get($input, 'limit'));

            return $this->response->paginator($workOrders, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/customersac",
     *     tags={"Customer Sacs"},
     *     summary="Update Sacs for Customer",
     *     description="Post Customer Sacs",
     *     operationId="postCustomerSacs",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Sacs",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    /**
     * Store a newly created data in customer sac.
     *
     * @param  IRequest $request
     * @return Response
     */
    public function store(IRequest $request)
    {
        $input = $request->getParsedBody();
        // if ($this->containtOverlapSac($input['data'])) {
        //     throw new \Exception("There are overlap sacs. Please check and re-update");
        // }
        
        // validation
        // $validator = new Validator;
        // // $validator->validate($input);
        $data = $this->prepareData($input);
        try {
            // Store customer's sac(s)
            if (array_has($data, 'create')) {
                $this->customerSacModel->createMulti($data['create']);
            }
            if (array_has($data, 'update')) {
                $this->customerSacModel->updateMulti($data['update']);
            }

            return $this->response->created(Null, '{}');
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Update the data in customer sac.
     *
     * @param  IRequest $request
     * @return Response
     */
    public function update(IRequest $request)
    {
        $input = $request->getParsedBody();
        try {
            // Update customer's sac(s)
            foreach ($input['data'] as $row) {
                CustomerSac::where('cus_sac_id', $row['cus_sac_id'])->update([
                    'price' => $row['price'],
                    'discount' => $row['discount']
                ]);
            }
            return $this->response->noContent();
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
    /**
     * @SWG\Delete(
     *     path="/customersac",
     *     tags={"Customer Sacs"},
     *     summary="Delete Sacs for Customer",
     *     description="Post Customer Sacs",
     *     operationId="deleteCustomerSacs",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Sacs",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  IRequest  $request
     * @return Response
     */
    public function delete(IRequest $request)
    {
        $input = $request->getParsedBody();
        $ids = $input['ids'];
        try {
            DB::table('cus_sac')->whereIn('cus_sac_id', $ids)->delete();
            return $this->response->noContent();
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * prepare data for store or update Customer Sac.
     *
     * @param  array  $data
     * @return array
     */
    public function prepareData($data)
    {
        $result = [];
        foreach ($data['data'] as $row) {
            if (array_has($row, 'cus_sac_id') && $row['cus_sac_id'] != '') {
                $result['update'][] = [
                    'cus_sac_id' => $row['cus_sac_id'],
                    'price' => $row['price'],
                    'discount' => $row['discount'] ?: 0,
                ];
            } else {
                $result['create'][] = [
                    'whs_id' => $data['whs_id'],
                    'cus_id' => $data['cus_id'],
                    'sac_id' => $row['sac_id'],
                    'sac' => $row['sac'],
                    'sac_name' => $row['sac_name'],
                    'description' => $row['description'],
                    'type' => $row['type'],
                    'range_from' => $row['range_from'] ?: Null,
                    'range_to' => $row['range_to'] ?: Null,
                    'range_uom' => $row['range_uom'],
                    'period_uom' => $row['period_uom'],
                    'block' => $row['block'] ?: Null,
                    'unit_uom' => $row['unit_uom'],
                    'sac_period' => $row['sac_period'],
                    'price' => $row['price'],
                    'discount' => $row['discount'] ?: 0,
                ];
            }
        }

        return $result;
    }

    // public function containtOverlapSac($data)
    // {
    //     $validate = false;
    //     $collection = collect($data);
    //     $grouped = $collection->groupBy('type');
    //     $grouped->each(function($value, $key) use(&$validate) {
    //         $sacGroup = array_unique(array_filter($value->pluck('group')->toArray()));
    //         if (count($sacGroup) > 1) {
    //             $validate = true;
    //         }
    //     });

    //     return $validate;
    // }
}
