<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

class OrderHdrModel extends AbstractModel
{

    protected $model;
    protected $eventTrackingModel;
    protected $orderHdrMetaModel;

    /**
     * WavePickModel constructor.
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->orderHdrMetaModel = new OdrHdrMetaModel();
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getListOrderHdrInfo($odr_ids, $type)
    {
        $whs_id = self::getCurrentWhsId();
        if ($type == "updated") {
            $arr = [
                Status::getByValue('Picking', 'ORDER-STATUS'),
                Status::getByValue('Partial Picking', 'ORDER-STATUS')
            ];
        } else {
            $arr = [
                Status::getByValue('Allocated', 'ORDER-STATUS'),
                Status::getByValue('Partial Allocated', 'ORDER-STATUS')
            ];
        }

        $res = $this->model
            ->leftJoin('shipping_odr', 'shipping_odr.so_id', '=', 'odr_hdr.so_id')
            ->where('csr', '!=', 0)
            ->where('odr_hdr.whs_id', $whs_id)
            ->whereIn('odr_id', $odr_ids)
            ->whereIn('odr_sts', $arr)
            ->get();

        return $res;
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getOrderHdrNums($odr_ids)
    {
        return $this->model
            ->whereIn('odr_id', $odr_ids)
            ->lists('odr_num');
    }

    /**
     * @param $wvId
     *
     * @return mixed
     */
    public function getOrderHdrNumsByWvId($wvId)
    {
        $whs_id = self::getCurrentWhsId();

        return $this->model
            ->where('whs_id', $whs_id)
            ->whereIn('wv_id', $wvId)
            ->get(['odr_num', 'cus_id']);
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateOrdHdr($data)
    {
        // For partial order
        if ($data['odr_sts'] == Status::getByValue("Partial Allocated", "ORDER-STATUS")) {
            $sts = Status::getByValue("Partial Picking", "ORDER-STATUS");
        } // For full order
        elseif ($data['odr_sts'] == Status::getByValue("Allocated", "ORDER-STATUS")) {
            $sts = Status::getByValue("Picking", "ORDER-STATUS");
        }

        return $this->model->where('odr_id', '=', $data['odr_id'])
            ->update([
                'wv_id'   => $data['wv_id'],
                'wv_num'  => $data['wv_num'],
                'odr_sts' => $sts
            ]);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getOdrHdrListByWvId($wv_id)
    {
        return $this->model->where('wv_id', '=', $wv_id)->get();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function updatePickedOrdHdr($wv_id)
    {
        $odrHdrLsts = $this->getOdrHdrListByWvId($wv_id);
        $cancelOdrSts = Status::getByValue("Canceled", "ORDER-STATUS");
        foreach ($odrHdrLsts as $odrHdr) {
            $odrHdrMetaInfos = $this->orderHdrMetaModel->getOdrHdrMetaInfo($odrHdr['odr_id'], 'OFP');
            if ($odrHdr['odr_sts'] == $cancelOdrSts) {
                continue;
            }
            try {
                if ($odrHdr['odr_sts'] == Status::getByValue("Picking", "ORDER-STATUS")) {
                    $this->saveDataPickedOdrHdr([
                        'wv_id'   => $wv_id,
                        'odr_id'  => $odrHdr['odr_id'],
                        'odr_sts' => Status::getByValue("Picked", "ORDER-STATUS"),
                    ]);


                    if ($odrHdrMetaInfos[0]['value'] == 1) {
                        $evt_code = Status::getByKey("EVENT", "ORDER-PICKED");
                        $info = sprintf(Status::getByKey("EVENT-INFO", "ORDER-PICKED"), $odrHdr['odr_num']);

                        $this->eventTrackingModel->refreshModel();
                        $this->eventTrackingModel->create([
                            'whs_id'    => $odrHdr['whs_id'],
                            'cus_id'    => $odrHdr['cus_id'],
                            'owner'     => $odrHdr['odr_num'],
                            'evt_code'  => $evt_code,
                            'trans_num' => $odrHdr['odr_num'],
                            'info'      => $info
                        ]);
                    }
                } elseif ($odrHdr['odr_sts'] == Status::getByValue("Partial Picking", "ORDER-STATUS")) {
                    $this->saveDataPickedOdrHdr([
                        'wv_id'   => $wv_id,
                        'odr_id'  => $odrHdr['odr_id'],
                        'odr_sts' => Status::getByValue("Partial Picked", "ORDER-STATUS"),
                    ]);

                    if ($odrHdrMetaInfos[0]['value'] == 1) {
                        $evt_code = Status::getByKey("EVENT", "PARTIAL-PICKED");
                        $info = sprintf(Status::getByKey("EVENT-INFO", "PIP"), $odrHdr['odr_num']);

                        $this->eventTrackingModel->refreshModel();
                        $this->eventTrackingModel->create([
                            'whs_id'    => $odrHdr['whs_id'],
                            'cus_id'    => $odrHdr['cus_id'],
                            'owner'     => $odrHdr['odr_num'],
                            'evt_code'  => $evt_code,
                            'trans_num' => $odrHdr['odr_num'],
                            'info'      => $info
                        ]);
                    }
                }

            } catch (\Exception $e) {
            }


        }
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function saveDataPickedOdrHdr($data)
    {
        return $this->model->where('wv_id', '=', $data['wv_id'])
            ->where('odr_id', $data['odr_id'])
            ->update([
                'odr_sts' => $data['odr_sts']
            ]);
    }

    /**
     * @param $wvIds
     *
     * @return mixed
     */
    public function getOrderHdrByWavePikcIds($wvIds)
    {
        $whs_id = self::getCurrentWhsId();
        $wvIds = is_array($wvIds) ? $wvIds : [$wvIds];

        return $this->model
            ->where('whs_id', $whs_id)
            ->whereIn('wv_id', $wvIds)
            ->get();

    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getBackOrderInfoByOrgOdrId($id)
    {
        return $this->model->where('org_odr_id', $id)
            ->where('odr_sts', Status::getByValue("New", "Order-Status"))
            ->first();
    }

    /**
     * @param $odrNum
     *
     * @return string
     */
    public function getBackOrderNum($odrNum)
    {
        $order = $this->model
            ->where('odr_num', 'like', substr($odrNum, 0, 14) . "-%")
            ->orderBy('odr_id', 'desc')
            ->first();
        $index = 0;
        $orderNum = object_get($order, "odr_num", null);

        if ($orderNum) {
            $temp = explode("-", $orderNum);
            $index = (int)end($temp);
        } else {
            $orderNum = $odrNum;
        }

        return substr($orderNum, 0, 14) . "-" . str_pad(++$index, 2, "0", STR_PAD_LEFT);
    }

    public function getBackOrderMaxSequenceNum($orderId)
    {
        $res = (int)$this->model->where(['org_odr_id' => $orderId,])->max('back_odr_seq');

        return $res;
    }

    public function supportPartial($order_id)
    {
        $odrMetaModel = new OdrHdrMetaModel();
        $customerMetaModel = new CustomerMetaModel();
        $order_meta = $odrMetaModel->getOrderFlow($order_id);
        $Flow = $customerMetaModel->getFlow($order_meta, 'POD');

        return (bool)array_get($Flow, 'usage', false);
    }


    private function filterBackOrders($orders)
    {
        $backOrders = [];
        foreach ($orders as $odrId => $odrDtls) {
            foreach ($odrDtls as $odrDtl) {
                if ($odrDtl->back_odr) {
                    $backOrders[$odrId] = $odrDtls;
                    break;
                }
            }
        }

        return $backOrders;
    }

    public function createBackOrders($orders)
    {
        $orders = $this->filterBackOrders($orders);

        if (empty($orders)) {
            return;
        }

        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $odrDtlEntity = new OrderDtl();

        foreach ($orders as $odrId => $odrDtls) {
            /**
             * 1.check backorder = true
             * 1.2 - query order hdr backorder with status = new or create new with seq num + 1 (nO WV)
             * 1.3 query backorder dtl of backorder ID, item_id, lot
             * 1.4 update backorder dtl
             * 1.5 if backorder dtl  not existed , create new
             */
            $odrHdr = $this->byId($odrId);
            if ($odrHdr->org_odr_id) {
                $msg = sprintf("Backorder %s cannot accept to create sub Backorder", $odrHdr->odr_num);
                throw new \Exception($msg);
            }
            $odrHdr->back_odr = 1;
            $odrHdr->save();

            //$seq = $this->getBackOrderMaxSequenceNum($odrId);
            $OrderHdrMetaModel = new OdrHdrMetaModel();
            $orderFlow = $OrderHdrMetaModel->getFirstWhere(['odr_id' => $odrId, 'qualifier' => 'OFF']);

            $allowBackOrder = $this->supportPartial($odrId);

            if (!$allowBackOrder) {
                $msg = sprintf("Order ID %s is not allowed to create backorder", $odrHdr->odr_num);
                throw new \Exception($msg);
            }

            $odrHdrBac = null;

            foreach ($odrDtls as $odrDtl) {
                if (!$odrDtl->back_odr) {
                    continue;
                }

                if (!$odrHdrBac) {
                    //get bac ord HDR
                    $odrHdrBac = $this->model
                        ->where('odr_sts', 'NW')
                        ->where('org_odr_id', $odrId)
                        ->first();

                    if (!$odrHdrBac) {
                        $odrHdrBac = $odrHdr->replicate();
                        $odrHdrBac->odr_num = $odrHdrBac->odr_num . '-01';
                        $odrHdrBac->org_odr_id = $odrId;
                        $odrHdrBac->odr_id = null;
                        $odrHdrBac->wv_id = null;
                        $odrHdrBac->wv_num = null;
                        $odrHdrBac->odr_sts = 'NW';
                        $odrHdrBac->back_odr = 0;
                        $odrHdrBac->back_odr_seq = 1;
                        $odrHdrBac->odr_type = Status::getByValue('BackOrder', 'ORDER-type');
                        $odrHdrBac->push();
                        $OrderHdrMetaModel->create([
                            'odr_id'    => $odrHdrBac->odr_id,
                            'qualifier' => 'OFF',
                            'value'     => array_get($orderFlow, 'value', 0)
                        ]);
                    }

                    $bacOrders[$odrId]['hdr'] = $odrHdrBac;
                }

                //get bac order detail
                $bacOdrDtl = $odrDtlEntity
                    ->where('odr_id', $odrHdrBac->odr_id)
                    ->where('item_id', $odrDtl->item_id)
                    ->where('lot', 'ANY')
                    ->first();

                if ($bacOdrDtl) {
                    //update
                    $bacOdrDtl->piece_qty = $odrDtl->back_odr_qty;
                    $bacOdrDtl->back_odr_qty = 0;
                    $bacOdrDtl->back_odr = false;
                    $bacOdrDtl->save();
                } else {
                    //WMS2-5393 - Not allow to create backorder
                    $msg = sprintf('The Order %s should be reverted, Do not allow to create a backorder', $odrHdr->odr_num);
                    throw new Exception($msg);
                    //create
                    /*$bacOdrDtl = null;
                    $bacOdrDtl = $odrDtl->replicate();

                    $bacOdrDtl->odr_id = $odrHdrBac->odr_id;
                    $bacOdrDtl->piece_qty = $odrDtl->back_odr_qty;
                    $bacOdrDtl->picked_qty = 0;
                    $bacOdrDtl->back_odr_qty = 0;
                    $bacOdrDtl->back_odr = false;
                    $bacOdrDtl->alloc_qty = 0;
                    $bacOdrDtl->lot = 'ANY';
                    $bacOdrDtl->itm_sts = 'NW';
                    $bacOdrDtl->qty = $odrDtl->pack_size > 0 ? ceil($odrDtl->back_odr_qty / $odrDtl->pack_size) : 0;
                    $bacOdrDtl->created_at = time();
                    $bacOdrDtl->updated_at = time();
                    $odrDtlData = $bacOdrDtl->toArray();
                    unset($odrDtlData['odr_num']);
                    (new OrderDtl)->insert($odrDtlData);
                    */
                }
            }
        }


    }

    public function updateOrderPickedByWv($wvId)
    {
        $odrSts = 'PD';
        $this->getModel()->where([
            'odr_sts' => 'PK',
            'wv_id'   => $wvId
        ])->update(['odr_sts' => $odrSts, 'sts' => 'u']);
    }
}
