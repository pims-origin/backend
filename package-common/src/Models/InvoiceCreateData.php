<?php

namespace Seldat\Wms2\Models;

class InvoiceCreateData extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice_create_data';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'inv_cr_id';

    protected $fillable = [
        'data',
        'created_by',
        'created_at'
    ];

}
