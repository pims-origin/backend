<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PalletSuggestLocation;

class PalletSuggestLocationModel extends AbstractModel
{

    /**
     * PalletSuggestLocationModel constructor.
     *
     * @param PalletSuggestLocation|null $model
     */
    public function __construct(PalletSuggestLocation $model = null)
    {
        $this->model = ($model) ?: new PalletSuggestLocation();
    }

    /**
     * @param $hdrDtlId
     *
     * @return mixed
     */
    public function getHdrPutAwayByHdrDtlID($hdrDtlId)
    {
        $sql1 = "pal_sug_loc.gr_hdr_num, pal_sug_loc.sku, pal_sug_loc.size, pal_sug_loc.color, count(*) as total, " .
            " SUM(( select count(*) as actual from pallet where pallet.plt_id = pal_sug_loc.plt_id " .
            " AND pallet.loc_id IS NOT NULL )) as actual";
        $query = $this->model
            ->select([
                DB::raw($sql1),
                "pal_sug_loc.gr_hdr_num",
                "pal_sug_loc.sku",
                "pal_sug_loc.size",
                "pal_sug_loc.color",
            ])
            ->where('pal_sug_loc.gr_dtl_id', $hdrDtlId)
            ->get();

        return $query;
    }

    public function getDtlPutAwayByHdrDtlID($hdrDtlId)
    {

        $query = $this->model
            ->select('location.loc_code', 'pallet.plt_num')
            ->join('location', 'location.loc_id', '=', 'pal_sug_loc.loc_id')
            ->join('pallet', 'pallet.plt_id', '=', 'pal_sug_loc.plt_id')
            ->whereNull('pallet.loc_id')
            ->where('pal_sug_loc.gr_dtl_id', $hdrDtlId)
            ->get();

        return $query;
    }

    /**
     * @param $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function search($attributes, $with = [])
    {
        $query = $this->make($with)
            ->select(['plt_id', 'gr_hdr_id', 'gr_hdr_num'])
            ->where('putter', $attributes['putter'])
            ->where('whs_id', $attributes['whs_id'])
            ->where(
                DB::raw("(select count(*) from pallet p where pal_sug_loc.plt_id = p.plt_id and p.loc_id is null)"),
                ">", 0
            )
            ->groupBy('gr_hdr_id');

        $model = $query->get();

        return $model;
    }

    public function show($attributes, $with = [], $day = null)
    {

        $color = "case when (color is not null AND color != 'NA') then concat('-', color) else '' end";
        $size = "case when (size is not null AND size != 'NA') then concat('-', size) else '' end";
        $query = $this->make($with)
            ->select([
                'gr_hdr_id',
                'gr_hdr_num',
                'gr_dtl_id',
                'sku',
                'color',
                'size',
                DB::raw("concat(sku, $size, $color) as sku_size_color"),
                DB::raw("count(*) as total"),
                DB::raw("SUM((select count(*) from pallet p where p.plt_id = pal_sug_loc.plt_id and p.loc_id is not null)) as actual")
            ]);
        if (!empty($attributes['gr_hdr_id'])) {
            $query->where('gr_hdr_id', $attributes['gr_hdr_id']);
        }
        $query->where('putter', $attributes['putter'])
            ->where('whs_id', $attributes['whs_id'])
            ->groupBy('gr_dtl_id')
            ->orderBy('gr_hdr_id');
        if (!is_null($day)) {
            $limit = (!empty($day) && is_numeric($day)) ? $day : 1;
            $query->where('created_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));
        };
        $model = $query->get();

        return $model;
    }


}
