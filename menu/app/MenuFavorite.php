<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 11:04 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed user_id
 * @property mixed menu_id
 * @property mixed display_order
 */
class MenuFavorite extends BaseModel
{
    /**
     * primaryKey
     *
     * @var integer
     * @access protected
     */
    protected $primaryKey = null;

    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'menu_id',
        'display_order',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'favorite_menu';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function menu()
    {
        return $this->hasOne('\App\Menu', 'menu_id', 'menu_id');
    }
}
