<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportWidgets;
use League\Fractal\TransformerAbstract;

class ReportWidgetsTransformer extends TransformerAbstract
{
    public function transform(ReportWidgets $reportWidgets)
    {
        return [
            'id'           => array_get($reportWidgets, 'id', ''),
            'data'         => json_decode(array_get($reportWidgets, 'data', '')),
            'des'          => array_get($reportWidgets, 'des', ''),
            'name'         => array_get($reportWidgets, 'name', ''),
            'max_report'   => array_get($reportWidgets, 'max_report', ''),
            'count_report' => array_get($reportWidgets, 'count_report', ''),
            'ordinal'      => array_get($reportWidgets, 'ordinal', ''),
            'created_at'   => (empty(array_get($reportWidgets, 'created_at'))) ? "" : date("m/d/Y",
                strtotime(array_get($reportWidgets, 'created_at'))),
            'updated_at'   => (empty(array_get($reportWidgets, 'updated_at'))) ? "" : date("m/d/Y",
                strtotime(array_get($reportWidgets, 'updated_at'))),
        ];
    }

}
