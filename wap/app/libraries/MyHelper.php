<?php
namespace App;

use Illuminate\Support\Facades\File;

class MyHelper
{

    public function __construct()
    {

    }

    public static function log($title, $content, $option = NULL) {
        if (!env('API_LOG')) {
            return false;
        }
        $debug = env('APP_LOG');
        $isException = MyHelper::get($option,'type');
        if ($debug || $isException == 'exception')  {
            $fileName = self::get($option, 'fileName', 'DefaultLog');
            $fileName = time(). '-' . $fileName;
            $showStack = self::get($option, "showStack", FALSE);
            $filePath = storage_path() . '/logs/' . $fileName . "-" . date(
                    "d_m"
                ) . ".txt";

            $content = "[" . date("Y-m-d H:i:s") . "]" . " $title : "
                       . (is_string($content)
                    ? $content
                    : json_encode(
                        $content
                    )) . "\n";

            if ($showStack) {
                $json = json_encode(debug_backtrace(TRUE, 8));
                $content .= $json . "\n";
            }

            File::append($filePath, $content);
        }
    }

    /**
     * Log to file
     *
     * @param      $message  content to log
     */
    public static function logException($message, array $option = []) {
        self::log("Error", $message,
            [
                'showStack' => TRUE,
                'fileName' => array_get($option, 'fileName', 'error'),
                'type' => 'exception'
            ]
        );
    }

    public static function get($object, $value, $defaultValue = NULL) {
        if (is_array($value)) {
            $tmpValue = $object;
            for ($i = 0, $len = count($value); $i < $len; $i++) {
                $tmpValue = self::get($tmpValue, $value[$i], $defaultValue);
            }
            return $tmpValue;
        } else {
            if (!isset($object)) {
                return $defaultValue;
            } elseif (is_array($object)) {
                return isset($object[$value]) ?
                    $object[$value] : $defaultValue;
            } elseif (is_object($object)) {
                return isset($object->$value) ?
                    $object->$value : $defaultValue;
            }
        }
    }

    public static function checkRule($code, $type = null)
    {
        switch ($type) {
            case 'P': //PALLET
                return self::checkRulePallet($code);
            case 'L': //LOCATION
                return self::checkRuleLocation($code);
            default: //CARTON
                return self::checkRuleCarton($code);
        }
    }

    protected static function checkRulePallet($pallet)
    {
        $flag = true;
        if (is_array($pallet)) {
            foreach ($pallet as $index => $item) {
                $maxLength = strlen($item);
                $startChar = substr($item, 0, 2);
                if ($startChar != 'FF' || $maxLength > 24) {
                    $flag = false;
                    break;
                }
            }
        } else {
            $maxLength = strlen($pallet);
            $startChar = substr($pallet, 0, 2);
            if ($startChar != 'FF' || $maxLength > 24) {
                $flag = false;
            }
        }
        return $flag;
    }

    protected static function checkRuleLocation($location)
    {
        $flag = true;
        if (is_array($location)) {
            foreach ($location as $index => $item) {
                $maxLength = strlen($item);
                $startChar = substr($item, 0, 2);
                if ($startChar != 'DD' || $maxLength > 24) {
                    $flag = false;
                    break;
                }
            }
        } else {
            $maxLength = strlen($location);
            $startChar = substr($location, 0, 2);
            if ($startChar != 'DD' || $maxLength > 24) {
                $flag = false;
            }
        }
        return $flag;
    }

    public static function isHex($str){
        return preg_match('/([A-F0-9])/', $str);
    }

    protected static function checkRuleCarton($carton)
    {
        $flag = true;
        if (is_array($carton)) {
            foreach ($carton as $index => $item) {
                $maxLength = strlen($item);
                if ($maxLength != 24) {
                    $flag = false;
                    break;
                }
            }
        } else {
            $maxLength = strlen($carton);
            if ($maxLength > 24) {
                $flag = false;
            }
        }
        return $flag;
    }
}