<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\InvoicingSummary;
use Seldat\Wms2\Models\ChargeCode;
use Seldat\Wms2\Models\InvoiceHeader;
use Seldat\Wms2\Models\InvoiceCost;
use Seldat\Wms2\Models\InvoiceList;

class SummaryModel extends AbstractModel
{
    protected $model;
    protected $chgCode;
    protected $invoiceList;
    protected $invoiceHeader;
    protected $invoiceCost;
    
    static $limit = 5;

    /**
    *  Constructor
    */

    public function __construct()
    {
        $this->model = new InvoicingSummary();
        $this->chgCode = new ChargeCode();
        $this->invoiceList = new InvoiceList();
        $this->invoiceHeader = new InvoiceHeader();
        $this->invoiceCost = new InvoiceCost();
    }

    /**
     * @params array
     * return array
    */

    public function getEventID($params)
    {
        $evtCode = $params['evtCode'];

        $maxLogResult = DB::table($params['sumTable'])
                        ->select(DB::raw('MAX( ' . $params['field'] . ') AS maxLogID'))
                        ->first();

        $lastLogID = intVal($maxLogResult->maxLogID);

        $logQuery = DB::table($params['logTable'])
                        ->select('id')
                        ->where(function ($q) use($lastLogID, $evtCode) {
                            if ($lastLogID) {
                                $q->where('id', '>', $lastLogID);
                            }

                            if ($evtCode) {
                                $q->where('evt_code', $evtCode);
                            }
                        });
              
        $eventIDs = $logQuery->lists('id');
      
        return $eventIDs;
    }

    /**
     * @type string
     * return array
    */

    public function getChargeCodes($type)
    {
        $chgQuery = $this->chgCode
                            ->select('chg_code_id', 'chg_code', 'chg_type_id', 'chg_uom_id')
                            ->with([
                                'chargeType' => function ($q) {
                                   $q->select('chg_type_id', 'chg_type_name');
                                },
                               'systemUom' => function ($q) {
                                   $q->select('sys_uom_id', 'sys_uom_name as uom');
                                }
                            ])
                            ->whereHas('chargeType', function ($q) use($type) {
                                 $q->where('chg_type_name', $type);
                            })
                            ->get();

        $result = $chgQuery->toArray();

        return $result;
    }

    /*
    ****************************************************************************
    */

    public function getInvoiceDetails($invoiceNumber)
    {
        $return = [
            'invoiceDate' => NULL,
            'paymentDate' => NULL,
            'checkNumber' => NULL,
            'cancellationNumber' => NULL,
        ];

        $invoiceHeader = $this->invoiceHeader
                ->select('inv_sts', 'inv_dt', 'inv_paid_dt', 'inv_paid_ref')
                ->where('inv_num', $invoiceNumber)
                ->where('inv_sts', '!=', INVOICE_STATUS_OPEN['code'])
                ->first();

        switch ($invoiceHeader['inv_sts']) {
            case INVOICE_STATUS_INVOICED['code']:

                $return['invoiceDate'] = $invoiceHeader['inv_dt'];

                break;
            case INVOICE_STATUS_PAID['code']:

                $return['invoiceDate'] = $invoiceHeader['inv_dt'];
                $return['paymentDate'] = $invoiceHeader['inv_paid_dt'];
                $return['checkNumber'] = $invoiceHeader['inv_paid_ref'];

                break;
            case INVOICE_STATUS_CANCELLED['code']:

                $cancellation = $this->invoiceHeader
                        ->where('inv_org', $invoiceNumber)
                        ->where('inv_typ', INVOICE_TYPE_CANCELLATION['code'])
                        ->select('inv_num')
                        ->first();

                $return['invoiceDate'] = $invoiceHeader['inv_dt'];
                $return['cancellationNumber'] = $cancellation['inv_num'];

                break;
            default:
                break;
        }

        return $return;
    }

    /*
    ****************************************************************************
    */

    public function getAmt($data)
    {
        return $this->getCommonAmt($data) +
               $this->getCurrentAmt($data) +
               $this->getMonthlyAmt($data);
    }

    /*
    ****************************************************************************
    */

    public function getCommonAmt($data)
    {
        $specialUOMs = array_merge(INVOICE_CURRENT_UOM, INVOICE_MONTHLY_UOM);

        $query = $this->invoiceList->select(
                    DB::raw('
                        SUM(
                            ROUND(qty * chg_cd_price, 2)
                        ) AS amt'
                    )
                )
                ->whereNotIn('su.sys_uom_name', $specialUOMs)
                ->where('inv_sts', 1);

        $finalQuery = $this->addAmtClauses($query, $data);

        $results = $finalQuery->get();

        return $results[0]->amt;
    }

    /*
    ****************************************************************************
    */

    public function getCurrentAmt($data)
    {
        $maxDateQuery = $this->invoiceList->select(
                    'chg_code_id',
                    DB::raw('MAX(dt) AS dt')
                )
                ->join('system_uom AS su', 'su.sys_uom_id', '=', 'invoice_summary.chg_uom_id')
                ->whereIn('su.sys_uom_name', INVOICE_CURRENT_UOM)
                ->where('inv_sts', 1)
                ->groupBy('chg_code_id');

        $maxDateQuery->where('invoice_summary.cus_id', $data['clientID']);

        $joinQuery = $this->addSummaryDateClause($maxDateQuery, $data);

        $subQuery = DB::raw("({$joinQuery->toSql()}) as subQuery");

        $sql = $joinQuery->getQuery();

        $query = $this->invoiceList->select(
                    'qty',
                    'chg_cd_price',
                    DB::raw('
                        SUM(
                            ROUND(qty * chg_cd_price, 2)
                        ) AS amt'
                    )
                )
                ->join($subQuery, 'subQuery.chg_code_id', '=', 'invoice_summary.chg_code_id')
                ->mergeBindings($sql);

        $finalQuery = $this->addSummaryJoinClauses($query, $data);

        $results = $finalQuery
                ->where('invoice_summary.cus_id', $data['clientID'])
                ->whereIn('su.sys_uom_name', INVOICE_CURRENT_UOM)
                ->whereRaw('subQuery.dt = invoice_summary.dt')
                ->get();

        return $results[0]->amt;
    }

    /*
    ****************************************************************************
    */

    public function getMonthlyAmt($data)
    {
        $rangeQuery = $this->invoiceList->select(
                    'chg_cd_price',
                    DB::raw('
                        DATEDIFF(
                            LAST_DAY(YEAR(dt)*10000 + MONTH(dt)*100 + 1),
                            YEAR(dt)*10000 + MONTH(dt)*100 + 1
                        ) + 1 AS dayCount'
                    ),
                    DB::raw('SUM(qty) AS totalQty')
                )
                ->whereIn('su.sys_uom_name', INVOICE_MONTHLY_UOM)
                ->where('inv_sts', 1)
                ->groupBy(
                    DB::raw('YEAR(dt)*100 + MONTH(dt)')
                );

        $subQuery = $this->addAmtClauses($rangeQuery, $data);

        $finalQuery = DB::table(
                    DB::raw("({$subQuery->toSql()}) as subQuery")
                )
                ->mergeBindings($subQuery->getQuery())
                ->select(
                    DB::raw('
                        SUM(
                            ROUND(chg_cd_price * totalQty / dayCount, 2)
                        ) as amt'
                    )
                );

        $results = $finalQuery->get();

        return $results[0]->amt;
    }

    /*
    ****************************************************************************
    */

    public function addAmtClauses($query, $data)
    {
        $query->where('invoice_summary.cus_id', $data['clientID']);

        $joinedQuery = $this->addSummaryJoinClauses($query, $data);

        $finalQuery = $this->addSummaryDateClause($joinedQuery, $data);

        return $finalQuery;
    }

    /*
    ****************************************************************************
    */

    public function addSummaryJoinClauses($query, $data)
    {
        $query->join('invoice_cost AS ic', 'ic.chg_code_id', '=', 'invoice_summary.chg_code_id')
                ->join('system_uom AS su', 'su.sys_uom_id', '=', 'invoice_summary.chg_uom_id')
                ->whereRaw('ic.cus_id = invoice_summary.cus_id')
                ->where('ic.cus_id', $data['clientID'])
                ->where('ic.chg_cd_cur', $data['curCode'])
                ->where('ic.chg_cd_price', '>', '0')
                ->where('ic.deleted', 0)
                ->where('su.deleted', 0);

        return $query;
    }

    /*
    ****************************************************************************
    */

    public function addSummaryDateClause($query, $data)
    {
        if ($data['fromDate']) {
            $query->where('invoice_summary.dt', '>=', $data['fromDate']);
        }

        if ($data['toDate']) {
            $query->where('invoice_summary.dt', '<=', $data['toDate']);
        }

        return $query;
    }

    /*
    ****************************************************************************
    */

    public function getQty($clientID, $chargeCodeID)
    {
        $qty = $this->invoiceList
                ->where('cus_id', $clientID)
                ->where('chg_code_id', $chargeCodeID)
                ->sum('qty');

        return $qty;
    }

    /*
    ****************************************************************************
    */

    public function getCost($clientID, $chargeCodeID, $curCode)
    {
        $cost = $this->invoiceCost
                ->where('cus_id', $clientID)
                ->where('chg_code_id', $chargeCodeID)
                ->where('chg_cd_cur', $curCode)
                ->select('chg_cd_price')
                ->first();

        return $cost['chg_cd_price'];
    }

    /*
    ****************************************************************************
    */
    /*
       ****************************************************************************
       */

    public function getCurrentRevenueArray($type)
    {
        return [
            $type => [
                'customer' => 'NULL',
                'currentMonthRevenue' => 0,
                'YTDRevenue' => 0
            ],
        ];
    }
}
