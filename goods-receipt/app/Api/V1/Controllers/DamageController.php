<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Transformers\ContainerTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\DamageTypeModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Transformers\DamageCartonsTransformer;
use App\Api\V1\Transformers\DamageContainerTransformer;
use App\Api\V1\Transformers\DamageGoodReceiptTransformer;
use App\Api\V1\Transformers\DamageDamageTypeTransformer;
use App\Api\V1\Transformers\DamageSKUTransformer;
use Seldat\Wms2\Utils\Export;

class DamageController extends AbstractController
{

    /**
     * Return containers have damage cartons
     * @return type
     */
    public function containers(Request $request)
    {
        
        $cartonModel = new CartonModel();
        $userId = $this->getUserId();
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', PHP_INT_MAX);
        $data = $cartonModel->cartonsHaveDamage($userId, $input, 'container', null);
        
        return $this->response->paginator($data, new DamageContainerTransformer);
        
    }

    /**
     * Search good receipt have damage cartons
     * 
     * @param Request $request
     * 
     * @return Pagination
     */
    public function goodReceipts(Request $request)
    {
        $cartonModel = new CartonModel();
        $userId = $this->getUserId();
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', PHP_INT_MAX);
        $data = $cartonModel->cartonsHaveDamage($userId, $input, 'good-receipt', $limit);
        
        return $this->response->paginator($data, new DamageGoodReceiptTransformer);
    }

    /**
     * Search damage type have damage cartons
     * 
     * @param Request $request
     * 
     * @return Pagination
     */
    public function damageType(Request $request)
    {
        $cartonModel = new CartonModel();
        $userId = $this->getUserId();
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', PHP_INT_MAX);
        $data = $cartonModel->cartonsHaveDamage($userId, $input, 'damage-type', $limit);
        
        return $this->response->paginator($data, new DamageDamageTypeTransformer);
    }

    /**
     * Search sku have damage cartons
     * 
     * @param Request $request
     * 
     * @return Pagination
     */
    public function sku(Request $request)
    {
        $cartonModel = new CartonModel();
        $userId = $this->getUserId();
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', PHP_INT_MAX);
        $data = $cartonModel->cartonsHaveDamage($userId, $input, 'sku', $limit);
        
        return $this->response->paginator($data, new DamageSKUTransformer);
    }

    /**
     * Return current user id
     * 
     * @return integer
     */
    private function getUserId()
    {
        $redis = new Data();
        $userInfo = $redis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', null);

        return $userId;
    }

    /**
     * Search cartons have damage cartons
     * 
     * @param Request $request
     * 
     * @return Pagination
     */
    public function search(Request $request) {
        $cartonModel = new CartonModel();
        $userId = $this->getUserId();
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', 20);
        $data = $cartonModel->cartonsHaveDamage($userId, $input, 'cartons', $limit);
        
        return $this->response->paginator($data, new DamageCartonsTransformer);
        
    }
    
    /**
     * Export data
     * 
     * @param Request $request
     * 
     * @return Pagination
     */
    public function exportCsv(Request $request){
        ini_set('memory_limit', '2G');
        ini_set('max_execution_time', 0);
        $cartonModel = new CartonModel();
        $userId = $this->getUserId();
        $input = $request->getQueryParams();
        $data = $cartonModel->cartonsHaveDamage($userId, $input, 'cartons', PHP_INT_MAX)->toArray();

        $title = [            
            'cus_code' => 'Customer Code',
            'cus_name' => "Customer Name",
            'gr_hdr_num' => "GR Num",
            'ctnr_num' => "CTNR Num",
            'ref_code' => "Ref Code",
            'po' => "PO",
            'dmg_name' => "Damaged Type",
            'dmg_qty' => "Damaged QTY",
            'item_id' => "Item ID",
            'sku' => "SKU",
            'size' => "Size",
            'color' => "Color",
            'lot' => "Lot",
            'ctn_pack_size' => "Pack Size",
            'des' => "Description",
        ];
        Export::showFile("My name", $title, $data['data']);
        die;
    }
}

