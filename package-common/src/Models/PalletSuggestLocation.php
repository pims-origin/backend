<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace Seldat\Wms2\Models;

class PalletSuggestLocation extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pal_sug_loc';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'plt_id',
        'loc_id',
        'data',
        'loc_name',
        'created_by',
        'updated_by',
        'ctn_ttl',
        'item_id',
        'sku',
        'size',
        'color',
        'putter',
        'gr_hdr_id',
        'gr_dtl_id',
        'gr_hdr_num',
        'whs_id',
        'put_sts',
        'act_loc_id',
        'act_loc_code',
        'lot'
    ];

    public function location()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Location', 'loc_id', 'loc_id');
    }

    public function goodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'gr_hdr_id', 'gr_hdr_id');
    }

    public function goodsReceiptDetail()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptDetail', 'gr_dtl_id', 'gr_dtl_id');
    }

    public function pallet()
    {
        return $this->hasOne(__NAMESPACE__ . '\Pallet', 'plt_id', 'plt_id');
    }

    public function item()
    {
        return $this->hasOne(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function putterUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'putter');
    }
}
