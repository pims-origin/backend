<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\SystemUomModel;
use App\Api\V1\Transformers\SystemUomTransformer;
use App\Api\V1\Validators\SystemUomValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\V1\Validators\SystemUomDeleteMassValidator;
use Seldat\Wms2\Utils\Message;

class SystemUomController extends AbstractController
{
    /**
     * @var SystemUomModel
     */
    protected $systemUomModel;
    /**
     * @var $systemUomTransformer
     */
    protected $systemUomTransformer;
    /**
     * @var $systemUomValidator
     */
    protected $systemUomValidator;
    /**
     * @var $systemUomDeleteMassValidator
     */
    protected $systemUomDeleteMassValidator;

    /**
     * SystemUomController constructor.
     *
     * @param SystemUomModel $systemUomModel
     */
    public function __construct(SystemUomModel $systemUomModel)
    {
        $this->systemUomModel = $systemUomModel;
        $this->systemUomTransformer = new SystemUomTransformer();
        $this->systemUomValidator = new SystemUomValidator();
        $this->systemUomDeleteMassValidator = new SystemUomDeleteMassValidator();
    }

    public function index()
    {
        try {
            $systemUom = $this->systemUomModel->all();

            return $this->response->collection($systemUom, $this->systemUomTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/v1/systemUoms/{systemUomId}",
     *     summary="Read SystemUom",
     *     description="View Detail SystemUom",
     *     operationId="showSystemUom",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="systemUomId",
     *         description="SystemUom Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return detail of SystemUom",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\SystemUoms(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "systemUom 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * SystemUom Detail
     *
     * @param int $systemUomId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function show($systemUomId)
    {
        try {
            $systemUom = $this->systemUomModel->getFirstBy('sys_uom_id', $systemUomId);

            return $this->response->item($systemUom, $this->systemUomTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/v1/systemUoms/search",
     *     summary="Search SystemUom",
     *     description="Search SystemUom",
     *     operationId="searchSystemUom",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Return all found result of SystemUom",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\SystemUoms(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "systemUom 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     * )
     *
     * SystemUom Search
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $systemUom = $this->systemUomModel->search($input, [], array_get($input, 'limit', 20));

            return $this->response->paginator($systemUom, $this->systemUomTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/systemUoms/{systemUomId}",
     *     summary="Create SystemUom",
     *     description="Create SystemUom",
     *     operationId="createSystemUom",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="systemUomId",
     *         description="SystemUom Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_name",
     *         description="SystemUom's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_status",
     *         description="SystemUom's State Uom",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_short_name",
     *         description="SystemUom's Short Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of SystemUom",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\SystemUoms(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "SystemUom 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * SystemUom Create
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemUomValidator->validate($input);

        $params = [
            'sys_uom_code' => $input['sys_uom_code'],
            'sys_uom_des'  => !empty($input['sys_uom_des']) ? $input['sys_uom_des'] : '',
            'sys_uom_name' => $input['sys_uom_name'],
        ];

        try {
            // check duplicate Uom Code
            $systemUom = $this->systemUomModel->loadBy([
                'sys_uom_code' => $params['sys_uom_code']
            ])->first();
            if (!empty($systemUom)) {
                return $this->response->errorBadRequest(Message::get("BM006", "UOM Code"));
            }

            // check duplicate Uom Name
            $systemUom = $this->systemUomModel->loadBy([
                'sys_uom_name' => $params['sys_uom_name']
            ])->first();
            if (!empty($systemUom)) {
                return $this->response->errorBadRequest(Message::get("BM006", "UOM Name"));
            }

            if ($systemUom = $this->systemUomModel->create($params)) {
                return $this->response->item($systemUom,
                    $this->systemUomTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Put(
     *     path="/v1/systemUoms/{systemUomId}",
     *     summary="Update SystemUom",
     *     description="Update SystemUom",
     *     operationId="updateSystemUom",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="systemUomId",
     *         description="SystemUom Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_name",
     *         description="SystemUom's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_status",
     *         description="SystemUom's State Uom",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_short_name",
     *         description="SystemUom's Short Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of SystemUom",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\SystemUoms(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "systemUom 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * @param int $systemUomId
     * @param Request $request
     *
     * @return mixed
     * @internal param int $whs_con_id
     */
    public function update(
        $systemUomId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['sys_uom_id'] = $systemUomId;
        // validation
        $this->systemUomValidator->validate($input);

        $params = [
            'sys_uom_id'   => $systemUomId,
            'sys_uom_code' => $input['sys_uom_code'],
            'sys_uom_des'  => !empty($input['sys_uom_des']) ? $input['sys_uom_des'] : '',
            'sys_uom_name' => $input['sys_uom_name'],
        ];

        try {
            // Check not exist
            if (!$this->systemUomModel->loadBy(['sys_uom_id' => $systemUomId])->first()) {
                return $this->response->errorBadRequest(Message::get("BM017", "SystemUom"));
            }

            // check duplicate Uom Code
            $systemUom = $this->systemUomModel->loadBy([
                'sys_uom_code' => $params['sys_uom_code']
            ])->first();
            if (!empty($systemUom) && $systemUom->sys_uom_id != $systemUomId) {
                return $this->response->errorBadRequest(Message::get("BM006", "UOM Code"));
            }

            // check duplicate Uom Name
            $systemUom = $this->systemUomModel->loadBy([
                'sys_uom_name' => $params['sys_uom_name']
            ])->first();
            if (!empty($systemUom) && $systemUom->sys_uom_id != $systemUomId) {
                return $this->response->errorBadRequest(Message::get("BM006", "UOM Name"));
            }

            if ($systemUom = $this->systemUomModel->update($params)) {
                return $this->response->item($systemUom, $this->systemUomTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Delete(
     *     path="/v1/systemUom/{systemUomId}",
     *     summary="Delete SystemUom",
     *     description="Delete a SystemUom",
     *     operationId="updateContact",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="systemUomId",
     *         description="SystemUom Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="SystemUom was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param int $systemUomId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     * @internal param MenuModel $menuModel
     *
     */
    public function destroy($systemUomId)
    {
        try {
            if ($this->systemUomModel->deleteSystemUom($systemUomId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM003", "Charge UOM"));
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->systemUomDeleteMassValidator->validate($input);

        $input['sys_uom_id'] = is_array($input['sys_uom_id']) ? $input['sys_uom_id'] : [$input['sys_uom_id']];

        try {
            $this->systemUomModel->deleteMassSystemUom($input['sys_uom_id']);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }
}
