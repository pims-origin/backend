<?php
/**
 * Created by PhpStorm.
 *
 * Date: loc_id-July-16
 * Time: 10:00
 */

namespace App\Api\V2\Outbound\Item\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\AsnHdrModel;

use App\Api\V1\Models\DamageCartonModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V2\Outbound\Models\ItemModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\Log;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\VirtualCartonSumModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\OutPalletModel;

use App\Api\V1\Validators\ScanPalletValidator;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Validators\PalletValidator;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use TCPDF;

/**
 * Class PalletController
 *
 * @package App\Api\V1\Controllers
 */
class ItemController extends AbstractController
{
    protected $palletModel;
    protected $virtualCartonModel;
    protected $orderDtlModel;
    protected $scanPallet;
    protected $locationModel;
    protected $locationStatusDetailModel;
    protected $cartonModel;
    protected $asnDtlModel;
    protected $eventTrackingModel;
    protected $palletSuggestLocationModel;
    protected $goodsReceiptModel;
    protected $goodsReceiptDetailModel;
    protected $vtlCtnModel;
    protected $orderCartonModel;
    protected $vtlCtnSumModel;
    protected $asnHdrModel;
    protected $wvDtlModel;
    protected $waveHdrModel;
    protected $orderHdrModel;
    protected $itemModel;
    protected $packHdrModel;
    protected $packDtlModel;
    protected $InventorySummaryModel;
    protected $outPalletModel;
    protected $containerModel;
    protected $dmgCtnModel;

    /**
     * PalletController constructor.
     *
     * @param PalletModel $palletModel
     * @param PalletSuggestLocationModel $palletSuggestLocationModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        PalletModel $palletModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        CartonModel $cartonModel,
        LocationModel $locationModel,
        EventTrackingModel $eventTrackingModel
    )
    {
        $this->scanPallet = new ScanPalletValidator();
        $this->palletModel = $palletModel;
        $this->asnDtlModel = new AsnDtlModel();
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->goodsReceiptModel = new GoodsReceiptModel();
        $this->goodsReceiptDetailModel = new GoodsReceiptDetailModel();
        $this->vtlCtnModel = new VirtualCartonModel();
        $this->vtlCtnSumModel = new VirtualCartonSumModel();
        $this->asnHdrModel = new AsnHdrModel();
        $this->virtualCartonModel = new VirtualCartonModel();
        $this->wvDtlModel = new WavePickDtlModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->itemModel = new ItemModel();
        $this->packHdrModel = new PackHdrModel();
        $this->packDtlModel = new PackDtlModel();
        $this->waveHdrModel = new WavePickHdrModel();
        $this->InventorySummaryModel = new InventorySummaryModel();
        $this->outPalletModel = new OutPalletModel();
        $this->containerModel = new ContainerModel();
        $this->dmgCtnModel = new DamageCartonModel();
    }

    public function getItemSku($whsId, $cusId, Request $request)
    {
        $owner = $transaction = "";
        $url = "/whs/{$whsId}/{$cusId}/item/search/sku";
        Log:: info($request, $whsId, [
            'evt_code' => 'GIS',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get Item SKU'
        ]);

        $input = $request->getQueryParams();
        $searchValue = array_get($input, 'key');

        $input['whsId'] = $whsId;
        $input['cusId'] = $cusId;
        $error = $this->_validateParams($input);
        if ($error) {
            return $error;
        }

        try {

            $data = [];
            $result = [];

            $items = $this->itemModel->search(['sku' => $searchValue, 'cus_id' => $cusId], ['customer']);
            foreach ($items as $item) {
                $data[] = [
                    'item_id' => object_get($item, 'item_id'),
                    'sku' => object_get($item, 'sku'),
                    'size' => object_get($item, 'size'),
                    'color' => object_get($item, 'color'),
                    'cus_id' => object_get($item, 'customer.cus_id'),
                    'cus_code' => object_get($item, 'customer.cus_code'),
                    'cus_name' => object_get($item, 'customer.cus_name'),
                    'pack_size' => object_get($item, 'pack'),
                    'full' => object_get($item, 'sku') . " | " . object_get($item, 'size') . " | " . object_get($item, 'color') . " | " . object_get($item, 'pack')
                ];
            }
            $result['skus'] = $data;


            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'] = $result;
            $msg['message'][] = [
                'status_code' => 1,
                'msg' => 'OK'
            ];

            return new Response($msg, 200, [], null);

        } catch (\Exception $e) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'][] = [
                'status_code' => -1,
                'msg' => $e->getMessage()
            ];

            return new Response($msg, 200, [], null);
        }
    }


    public function getItemUpc($whsId, Request $request)
    {
        $owner = $transaction = "";
        $url = "/whs/{$whsId}/item/search/upc";
        Log:: info($request, $whsId, [
            'evt_code' => 'GIU',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get Item UPC'
        ]);

        $input = $request->getQueryParams();
        $searchValue = array_get($input, 'key');

        try {

            if (empty($searchValue) ) {
                $msg = "Value of search upc is required";
                return $this->_responseErrorMessage($msg);
            }

            $data = [];
            $result = [];
            // $item = $this->itemModel->getFirstWhere(['cus_upc' => $searchValue], ['customer']);
            $item = $this->itemModel->getModel()
                ->join('customer as ct', 'ct.cus_id', '=', 'item.cus_id')
                ->join('customer_warehouse as cw', 'cw.cus_id', '=', 'ct.cus_id')
                ->where('ct.deleted', 0)
                ->where('cw.deleted', 0)
                ->where('cw.whs_id', $whsId)
                ->where('item.cus_upc', $searchValue)
                ->first();

            $data = [
                'item_id' => object_get($item, 'item_id'),
                'sku' => object_get($item, 'sku'),
                'size' => object_get($item, 'size'),
                'color' => object_get($item, 'color'),
                'cus_id' => object_get($item, 'cus_id'),
                'cus_code' => object_get($item, 'cus_code'),
                'cus_name' => object_get($item, 'cus_name'),
                'pack_size' => object_get($item, 'pack'),
                'full' => object_get($item, 'sku') . " | " . object_get($item, 'size') . " | " . object_get($item, 'color') . " | " . object_get($item, 'pack')
            ];

            $result['upc'] = $item ? $data : new \stdClass();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'] = $result;
            $msg['message'][] = [
                'status_code' => 1,
                'msg' => 'OK'
            ];

            return new Response($msg, 200, [], null);

        } catch (\Exception $e) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'][] = [
                'status_code' => -1,
                'msg' => $e->getMessage()
            ];

            return new Response($msg, 200, [], null);
        }
    }

    private function _validateParams($input)
    {
        $whsId     = array_get($input, 'whsId');
        $cusId     = array_get($input, 'cusId');
        $keySearch = array_get($input, 'key');
        $names = [
            'whsId'     => 'Warehouse id',
            'cusId'     => 'Customer id',
            'keySearch' => 'Value of search sku',
        ];

        // Check Required
        $requireds = [
            'whsId'     => $whsId,
            'cusId'     => $cusId,
            'keySearch' => $keySearch,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // check exist customer
        $checkCustomer = \DB::table('customer')
                        ->where('cus_id', $cusId)
                        ->where('deleted', 0)
                        ->first();
        if (!$checkCustomer) {
            $msg = sprintf("Customer id %s doesn't exist.", $cusId);
            return $this->_responseErrorMessage($msg);
        }

        // check exist warehouse
        $checkWarehouse = \DB::table('warehouse')
                        ->where('whs_id', $whsId)
                        ->where('deleted', 0)
                        ->first();
        if (!$checkWarehouse) {
            $msg = sprintf("Warehouse id %s doesn't exist.", $cusId);
            return $this->_responseErrorMessage($msg);
        }

        // validate customer belong warehouse
        $checkCusWhs = \DB::table('customer_warehouse')
                        ->where('whs_id', $whsId)
                        ->where('cus_id', $cusId)
                        ->where('deleted', 0)
                        ->first();
        if (!$checkCusWhs) {
            $msg = "The customer doesn't belong to warehouse.";
            return $this->_responseErrorMessage($msg);
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'message' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}
