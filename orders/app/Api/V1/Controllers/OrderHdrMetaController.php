<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\OrderFlowMasterModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Transformers\CustomerMetaTransformer;
use App\Api\V1\Transformers\OrderHdrMetaTransformer;
use App\Api\V1\Transformers\OrderFlowMasterTransformer;
use App\Api\V1\Validators\OrderHdrMetaValidator;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use Wms2\UserInfo\Data;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\OrderHdr;
use Illuminate\Support\Facades\DB;

class OrderHdrMetaController extends AbstractController
{
    /*
     * @var CustomerModel
     * */
    protected $customerModel;

    /**
     * @var $customerMetaModel
     */
    protected $customerMetaModel;

    /*
     * @var CustomerTransformer
     * */
    protected $customerTransformer;

    /**
     * @var OrderFlowMaster
     */
    protected $orderFlowMaster;

    /**
     * @var OrderFlowMasterTransformer
     */
    protected $orderFlowMasterTransformer;

    /**
     * @var OrderHdrMetaValidator
     */
    protected $orderHdrMetaValidator;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrMetaModel;

    /**
     * @var OrderHdrMetaTransformer
     */
    protected $orderHdrMetaTransformer;

    protected $customerMetaTransformer;

    /**
     * @var OrderHdr
     */
    protected $orderHdr;



    /**
     * @param OrderHdrMetaModel $orderHdrMetaModel
     * @param CustomerMetaTransformer $customerMetaTransformer
     * @param OrderFlowMasterModel $orderFlowMasterModel
     * @param OrderFlowMasterTransformer $orderFlowMasterTransformer
     * @param OrderHdrMetaValidator $orderHdrMetaValidator
     * @param OrderHdrMetaTransformer $orderHdrMetaTransformer
     */
    public function __construct(
        OrderHdrMetaModel $orderHdrMetaModel,
        CustomerMetaTransformer $customerMetaTransformer,
        OrderFlowMasterModel $orderFlowMasterModel,
        OrderFlowMasterTransformer $orderFlowMasterTransformer,
        OrderHdrMetaValidator $orderHdrMetaValidator,
        OrderHdrMetaTransformer $orderHdrMetaTransformer,
        OrderHdr $orderHdr
    ) {
        //parent::__construct();
        $this->orderHdrMetaModel = $orderHdrMetaModel;
        $this->customerMetaTransformer = $customerMetaTransformer;
        $this->customerMetaModel = new CustomerMetaModel();
        $this->orderFlowMasterModel = $orderFlowMasterModel;
        $this->orderFlowMasterTransformer = $orderFlowMasterTransformer;
        $this->orderHdrMetaValidator = $orderHdrMetaValidator;
        $this->orderHdrMetaTransformer = $orderHdrMetaTransformer;
        $this->orderHdr = $orderHdr;
    }
    
    /**
     * @param Request $request
     * @param $customerId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getOrderFlow(Request $request, $customerId)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        
        try {
            //if exist odr_id
            if ($input['orderid']) {
                $odrHdr = DB::table('odr_hdr')->where('odr_id', '=', $input['orderid'])->first();
                $qualifier = "OFF";
                if ($orderHdrMeta = $this->orderHdrMetaModel->loadBy(['odr_id' => $input['orderid'],
                                                                      'qualifier' => $qualifier])->first()) {
                    //customerMeta: $customerId and qualifier==off
                    if (object_get($orderHdrMeta, 'qualifier', null) == "OFF" || object_get($orderHdrMeta, 'qualifier', null) == "OSF") {
                        $valueJsons = \GuzzleHttp\json_decode($orderHdrMeta->value);
                        $val_json_decodes = [];
                        if ($valueJsons) {
                            foreach ($valueJsons as $valueJson) {
                                $val_json_decode = [
                                    'odr_flow_id' => object_get($valueJson, 'odr_flow_id', 0),
                                    'step'        => object_get(\GuzzleHttp\json_decode($valueJson), 'step', 0),
                                    'flow_code'   => object_get(\GuzzleHttp\json_decode($valueJson), 'flow_code', ''),
                                    'odr_sts'     => object_get(\GuzzleHttp\json_decode($valueJson), 'odr_sts', ''),
                                    'name'        => object_get(\GuzzleHttp\json_decode($valueJson), 'name', ''),
                                    'description' => object_get(\GuzzleHttp\json_decode($valueJson), 'description', ''),
                                    'dependency'  => object_get(\GuzzleHttp\json_decode($valueJson), 'dependency', 0),
                                    'type'        => object_get(\GuzzleHttp\json_decode($valueJson), 'type', ''),
                                    'usage'       => object_get(\GuzzleHttp\json_decode($valueJson), 'usage', 0),
                                ];
                                array_push($val_json_decodes, $val_json_decode);
                            }
                        }
                        $orderHdrMeta->val_json_decode = $val_json_decodes;
                        
                        return $this->response->item($orderHdrMeta, $this->orderHdrMetaTransformer);
                    }
                }
            }
            
            //if exist cus_id
            $arrQualifiers = ['OFF', 'OSF', 'OEF'];
            $arrQualifierMissing = [];
            $i = 0;
            foreach ($arrQualifiers as $arrQualifier) {
                $cusMeta = $this->customerMetaModel->getFirstWhere(['cus_id' => $customerId, 'qualifier' => $arrQualifier]);
                
                if ($cusMeta) {
                    $valueJsons = json_decode($cusMeta->value);
                    
                    if ($valueJsons) {
                        $val_json_decode = [];
                        foreach ($valueJsons as $valueJson) {
                            $odrflow = json_decode($valueJson);
                            $val_json_decode[] = [
                                'odr_flow_id' => object_get($odrflow, 'odr_flow_id', 0),
                                'step'        => object_get($odrflow, 'step', 0),
                                'flow_code'   => object_get($odrflow, 'flow_code', ''),
                                'odr_sts'     => object_get($odrflow, 'odr_sts', ''),
                                'name'        => object_get($odrflow, 'name', ''),
                                'description' => object_get($odrflow, 'description', ''),
                                'dependency'  => object_get($odrflow, 'dependency', 0),
                                'type'        => object_get($odrflow, 'type', ''),
                                'usage'       => object_get($odrflow, 'usage', 0),
                            ];
                        }
                    }
                    $val_json_decodes[$i]['qualifier'] = $arrQualifier;
                    $val_json_decodes[$i]['value'] = $val_json_decode;
                    $i++;
                }
            }
            $cusMeta->val_json_decode = $val_json_decodes;
            return $this->response->item($cusMeta, $this->customerMetaTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
    
    /**
     * @param $request
     * @param $orderId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    protected function upsert($request, $orderId)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        // $this->orderHdrMetaValidator->validate($input);
        
        // get OrderHdr
        $odrHdr = $this->orderHdr->where('odr_id', $orderId)->first();
        
        $valueArrJson = [];
        foreach ($input['value'] as $getVal) {
            $value = [
                'odr_flow_id' => array_get($getVal, 'odr_flow_id', ''),
                'step'        => array_get($getVal, 'step', ''),
                'flow_code'   => array_get($getVal, 'flow_code', ''),
                'odr_sts'     => array_get($getVal, 'odr_sts', ''),
                'name'        => array_get($getVal, 'name', ''),
                'description' => array_get($getVal, 'description', null),
                'dependency'  => array_get($getVal, 'dependency', 0),
                'type'        => array_get($getVal, 'type', ''),
                'usage'       => array_get($getVal, 'usage', '')
            ];
            
            $valueJson = json_encode($value);
            array_push($valueArrJson, $valueJson);
        }
        
        $valueJsons = json_encode($valueArrJson);
        $params = [
            'odr_id'    => $orderId,
            // 'qualifier' => array_get($input, 'qualifier', ''),
            'value'     => $valueJsons
        ];
        
        // check exist of cus_id
        $OdrHdrMeta = $this->orderHdrMetaModel->getFirstWhere([
            'odr_id' => $orderId,
            'qualifier' => array_get($input, 'qualifier', ''),
        ]);
        
        try {
            DB::beginTransaction();
            
            if ($OdrHdrMeta) {
                //update
                $orderHdrMeta = $this->orderHdrMetaModel->update($params);
            } else {
                $params = [
                    'odr_id'    => $orderId,
                    'qualifier' => array_get($input, 'qualifier', ''),
                    'value'     => $valueJsons
                ];
                //create
                $orderHdrMeta = $this->orderHdrMetaModel->create($params);
            }
            DB::commit();
            
            return ["data" => "successful"];
            
        } catch (\PDOException $e) {
            
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            
            return $this->response->errorBadRequest($e->getMessage());
        }
        
        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function store(Request $request, $cusId)
    {
        return $this->upsert($request, $cusId);
    }

    /**
     * @param Request $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(Request $request, $cusId)
    {
        return $this->upsert($request, $cusId);
    }


}
