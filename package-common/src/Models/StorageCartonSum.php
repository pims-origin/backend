<?php

namespace Seldat\Wms2\Models;


class StorageCartonSum extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ctn_sum';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'carton_id';

    /**
     * @var array
     */
    protected $fillable = [
        'carton_id',
        'cust_id',
        'last_active',
        'recv_dt',
        'vol',
        'uom'
    ];

    public function carton()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Carton', 'carton_id', 'ctn_id');
    }
}