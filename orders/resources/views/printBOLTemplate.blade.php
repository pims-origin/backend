<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>BOL pdf</title>

    <style type="text/css">
        @page {
            margin: 35px;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 12px;
            font-family: "Calibri";
        }

        .title {
            text-align: center;
            text-transform: uppercase;
            background-color: #000;
            color: #fff;
            font-weight: normal;
            padding: 2px;
        }

        .table-order tr th, .odr-tr-td {
            text-align: center;
            font-weight: 300;
            border-right: 1px solid #000;
            border-bottom: 1px solid #000
        }

        .bd-all {
            border: 1px solid #000;
        }

        .bd-top {
            border-top: 1px solid #000;
        }

        .bd-left {
            border-left: 1px solid #000;
        }

        .bd-bt {
            border-bottom: 1px solid #000;
        }

        .bd-right {
            border-right: 1px solid #000;
        }

    </style>
</head>
<body>
<table style="width: 100%; background-color: #fff; border: 1px solid #000;" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td style="text-align: left;border-bottom: 1px solid #000;padding: 2px; width: 20%">
            Date: {{date('m/d/Y', time())}}</td>
        <td colspan="2"
            style="text-align: center;text-transform: uppercase; font-weight: bold;
                border-bottom: 1px solid #000;padding: 2px; width: 60%">
            bill of lading
        </td>
        <td style="text-align: right;border-bottom: 1px solid #000;padding: 2px; width: 20%">Page 1</td>
    </tr>
    </tbody>
</table>
<table style="width: 100%; background-color: #fff; padding: 0; border: 1px solid #000; border-top: none;"
       cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td class="bd-bt bd-right" width="50%" style="vertical-align: top;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td class="title" colspan="2">
                        ship from
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;padding-top: 12px; padding-left: 3px">Name:</td>
                    <td style="font-style: italic;padding-top: 12px; padding-right: 3px">{{$shipment['ship_from_name']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 12px; padding-left: 3px">Address:</td>
                    <td style="font-style: italic; padding-top: 12px; padding-right: 3px">{{$shipment['ship_from_addr_1']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 12px; padding-left: 3px">City/State/Zip:</td>
                    <td style="font-style: italic; padding-top: 12px; padding-right: 3px">{{$shipment['ship_from_city'].", ".(array_get($shipment,'ship_from_state', null))
                        .","
                        .$shipment['ship_from_zip']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 12px; padding-left: 3px">SID#: {{$shipment['ship_from_sid']}}</td>
                    <td style="text-align: right;padding-top: 12px; padding-right: 3px; font-style: italic">
                        FOB: <img src="{{$shipment['ship_from_fob']?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                    </td>
                </tr>
                <tr>
                    <td class="title" colspan="2" style="font-style: italic">
                        ship to
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td class="bd-bt" width="50%" style="vertical-align: top;">
            <table style="width: 100%;border-bottom: 1px solid #000" cellpadding="0"
                   cellspacing="0">
                <tbody>
                <tr>
                    <td style="padding-left: 5px; width: 50%">Bill of Lading Number:</td>
                    <td style="font-weight: bold">{{$shipment['bo_label']}}</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td style="text-transform: uppercase; padding: 3px; width: 40%;">carrier name:</td>
                    <td style="font-style: italic; text-transform: uppercase">{{$shipment['carrier']}}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 25px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding: 3px">Trailer Number:</td>
                    <td style="font-style: italic;padding: 3px">{{$shipment['trailer_num']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 3px;padding-left: 3px">Seal Number(s):</td>
                    <td style="padding-top: 3px;padding-left: 3px">{{$shipment['seal_num']}}</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="bd-bt bd-right" width="50%" style="vertical-align: top;">
            <table style="width: 100%;" cellpadding="0"
                   cellspacing="0">
                <tbody>
                <tr>
                    <td style="width: 30%;padding-top: 12px; padding-left: 3px">Name:</td>
                    <td style="font-style: italic;padding-top: 12px; padding-right: 3px">{{$shipment['ship_to_name']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 12px; padding-left: 3px">Address:</td>
                    <td style="font-style: italic; padding-top: 12px; padding-right: 3px">{{$shipment['ship_to_addr_1']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 12px; padding-left: 3px">City/State/Zip:</td>
                    <td style="font-style: italic; padding-top: 12px; padding-right: 3px">{{$shipment['ship_to_city'].", ".(array_get($shipment,'ship_to_state', null)).", ".$shipment['ship_to_zip']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 12px; padding-left: 3px">TEL#: {{$shipment['ship_to_tel']}}</td>
                    <td style="text-align: right;padding-top: 12px; padding-right: 3px; font-style: italic">FOB:
                        <img src="{{$shipment['ship_to_fob']?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                    </td>
                </tr>
                <tr>
                    <td class="title" colspan="2" style="font-style: italic">
                        Third party freight charges bill to
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td class="bd-bt" width="50%" style="vertical-align: top;">
            <table style="width: 100%" cellpadding="0"
                   cellspacing="0">
                <tbody>
                <tr>
                    <td style="padding-left: 3px;padding-top: 10px; width: 40%; text-transform: uppercase">scac:</td>
                    <td style="text-transform: uppercase; padding-left: 3px;padding-top: 10px; font-style: italic">{{$shipment['scac']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 10px; padding-left: 3px">Pro Number:</td>
                    <td style="font-style: italic; padding-left: 3px; padding-top: 10px">{{$shipment['pro_num']}}</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="bd-bt bd-right" width="50%" style="vertical-align: top;">
            <table style="width: 100%;" cellpadding="0"
                   cellspacing="0">
                <tbody>
                <tr>
                    <td style="width: 30%;padding-top: 12px; padding-left: 3px">Name:</td>
                    <td style="padding-top: 10px; padding-right: 3px; font-style: italic">{{$shipment['bill_to_name']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 12px; padding-left: 3px">Address:</td>
                    <td style="padding-top: 12px; padding-right: 3px; font-style: italic">{{$shipment['bill_to_addr_1']}}</td>
                </tr>
                <tr>
                    <td style="padding-top: 12px; padding-left: 3px;padding-bottom: 10px">City/State/Zip:</td>
                    <td style="padding-top: 12px; padding-left: 3px;padding-bottom: 10px; font-style: italic">{{$shipment['bill_to_city'].", ".(array_get($shipment,'bill_to_state', null)).", ".$shipment['bill_to_zip']}}</td>
                </tr>
                </tbody>
            </table>
        </td>
        <td class="bd-bt" width="50%" style="vertical-align: top;">
            <table style="width: 100%" cellpadding="0"
                   cellspacing="0">
                <tbody>
                <tr>
                    <td colspan="3" style="font-size:10px; padding-bottom: 40px; padding-left: 3px; padding-top: 10px">
                        Freight Charge Terms: (freight
                        charges are prepaid unless marked otherwise)
                    </td>
                </tr>
                <tr>
                    <td style="font-style: italic;text-align: center">
                        <img src="{{$shipment['freight_charge_terms'] == "PR"?URL::asset('img/yes.gif'):URL::asset('img/no.gif')}}" style="width: 13px; height: 13px;"/>
                        Prepaid
                    </td>
                    <td style="font-style: italic; text-align: center">
                        <img src="{{$shipment['freight_charge_terms'] == "CO"?URL::asset('img/yes.gif'):URL::asset('img/no.gif')}}" style="width: 13px; height: 13px;"/>
                        Collect
                    </td>
                    <td style="font-style: italic; text-align: center">
                        <img src="{{$shipment['freight_charge_terms'] == "3P"?URL::asset('img/yes.gif'):URL::asset('img/no.gif')}}" style="width: 13px; height: 13px;"/>
                        3rdParty
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="bd-right" width="50%" style="vertical-align: top;">
            <table style="width: 100%;" cellpadding="0"
                   cellspacing="0">
                <tbody>
                <tr>
                    <td style="text-transform: uppercase; padding-top: 10px; padding-bottom: 20px; padding-left: 3px">
                        special instructions:
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 30px; text-transform: uppercase; padding-bottom: 10px">{{$shipment['special_inst']}}</td>
                </tr>
                <tr style="height: 30px">
                    <td></td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 50%; margin: 0; vertical-align: top">
            <table style="width: 100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="text-align: center; border-right: 1px solid #000; width: 25%; padding-top: 5px; height: 70px">
                        <div style="text-align: center;">
                            <img src="{{$shipment['is_attach']?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                        </div>
                        <br>

                        <p style="font-size: 11px">(checkbox)</p>
                    </td>
                    <td style="vertical-align: top; padding: 3px; width: 75%">Master Bill of Lading with attached
                        underlying
                        Bills of Lading
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<table style="width: 100%; padding: 0;" cellspacing="0" cellpadding="0" class="bd-all">
    <tbody>
    <tr>
        <td colspan="3">
            <table class="table-order" width="100%" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <td class="title" colspan="9" style="background-color:#000; padding: 2px;color: #fff">
                        customer order information
                    </td>
                </tr>
                <tr>
                    <th style="padding: 2px">
                        PO#
                    </th>
                    <th style="padding: 2px">
                        # PKGS
                    </th>
                    <th style="padding: 2px">
                        UNITS
                    </th>
                    <th style="padding: 2px">
                        WEIGHT
                    </th>
                    <th style="padding: 2px">
                        DEPT #
                    </th>
                    <th style="padding: 2px">
                        PLTS
                    </th>
                    <th style="padding: 2px">
                        PICK TICKET
                    </th>
                    <th style="padding: 2px">
                        ORDER REF #
                    </th>
                    <th style="text-align: center; font-weight: 300;border-bottom: 1px solid #000; padding: 2px">
                        ADDITIONAL
                        SHIPPER INFO
                    </th>
                </tr>
                @php
                $pkgs = 0;
                $units = 0;
                $weight = 0;
                @endphp
                @foreach ($shipment['bol_odr_dtl'] as $detail)
                    @php
                    $pkgs += $detail['ctn_qty'];
                    $units += $detail['piece_qty'];
                    $weight += $detail['weight'];
                    @endphp
                    <tr>
                        <td style="padding: 2px; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                            {{$detail['cus_po']}}
                        </td>
                        <td class="odr-tr-td">
                            {{$detail['ctn_qty']}}
                        </td>
                        <td class="odr-tr-td">
                            {{$detail['piece_qty']}}
                        </td>
                        <td class="odr-tr-td">
                            {{$detail['weight']}}
                        </td>
                        <td class="odr-tr-td">
                            {{$detail['cus_dept']}}
                        </td>
                        <td class="odr-tr-td">
                            {{$detail['plt_qty']}}
                        </td>
                        <td class="odr-tr-td">
                            {{$detail['cus_ticket']}}
                        </td>
                        <td class="odr-tr-td">
                            {{$detail['cus_odr']}}
                        </td>
                        <td style="text-align: center; font-weight: 300; border-bottom: 1px solid #000">
                            {{$detail['add_shipper_info']}}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td style="padding: 2px; font-weight: 300; border-right: 1px solid #000;">GRAND TOTAL</td>
                    <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$pkgs}}</td>
                    <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$units}}</td>
                    <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$weight}}</td>
                    <td colspan="5" style="background-color: #7d7d7d"></td>
                </tr>
                </thead>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table style="border: 1px solid #000" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td colspan="9" class="title" style="background-color:#000; padding: 2px;color: #fff">
                        carrier information
                    </td>
                </tr>
                <tr>
                    <th class="odr-tr-td" colspan="2"
                        style="padding:2px;text-transform: uppercase; white-space: nowrap">
                        handling unit
                    </th>
                    <th class="odr-tr-td" colspan="2"
                        style="padding:2px; text-transform: uppercase">
                        package
                    </th>
                    <th class="odr-tr-td" rowspan="2"
                        style="padding:2px;text-transform: uppercase">
                        weight
                    </th>
                    <th class="odr-tr-td" rowspan="2"
                        style="padding:2px;text-transform: uppercase">
                        H.M.<br>(X)
                    </th>
                    <th class="odr-tr-td" style="padding:2px;text-transform: uppercase">
                        commodity description
                    </th>
                    <th colspan="2"
                        style="padding:2px;text-align: center; font-weight: 300; border-bottom: 1px solid #000;text-transform: uppercase">
                        ltl only
                    </th>
                </tr>
                <tr>
                    <th class="odr-tr-td" style="text-transform: uppercase">
                        qty
                    </th>
                    <th class="odr-tr-td" style="text-transform: uppercase">
                        type
                    </th>
                    <th class="odr-tr-td" style="text-transform: uppercase">
                        qty
                    </th>
                    <th class="odr-tr-td" style="text-transform: uppercase">
                        type
                    </th>
                    <th style="padding: 2px; font-size: 15px; font-weight: 500; text-align: left;border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 9px">
                        Commodities requiring special or additional care orattention in handling or stowing must be
                        so marked andpackages as ensure sage transportation with ordinarycare.See Section 2(e) of
                        nmfc Item 360
                    </th>
                    <th class="odr-tr-td" style="padding:5px; text-transform: uppercase;white-space: nowrap">
                        nmfc #
                    </th>
                    <th style="padding:2px; text-align: center; font-weight: 300; border-bottom: 1px solid #000; text-transform: uppercase">
                        class
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="odr-tr-td" style="padding:2px;font-size: 15px">
                        {{(int)array_get($shipment, 'bol_carrier_dtl.hdl_qty', null)}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px; font-size: 15px">
                        {{array_get($shipment, 'bol_carrier_dtl.hdl_type', null)}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px;font-size: 15px">
                        {{(int)array_get($shipment, 'bol_carrier_dtl.pkg_qty', null)}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px; font-size: 15px">
                        {{array_get($shipment, 'bol_carrier_dtl.pkg_type', null)}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px; font-size: 15px">
                        {{(float)array_get($shipment, 'bol_carrier_dtl.weight', null)}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px;font-size: 15px">
                        {{!empty($shipment['bol_carrier_dtl']['hm']) && $shipment['bol_carrier_dtl']['hm'] == 1?'x':''}}
                    </td>
                    <td class="odr-tr-td" style="text-transform:uppercase;padding:5px; font-size: 15px">
                        {{array_get($shipment, 'bol_carrier_dtl.cmd_des', null)}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px; font-size: 15px">
                        {{array_get($shipment, 'bol_carrier_dtl.nmfc_num', null)}}
                    </td>
                    <td style="padding:2px;text-align: center; font-weight: 300; border-bottom: 1px solid #000;font-size: 15px">
                        {{array_get($shipment, 'bol_carrier_dtl.cls_num', null)}}
                    </td>
                </tr>
                <tr>
                    <td class="odr-tr-td" style="padding:2px;font-size: 15px">
                        {{$shipment['plt_qty_ttl']}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px;font-size: 15px; background-color: #7d7d7d"></td>
                    <td class="odr-tr-td" style="padding:2px; font-size: 15px">
                        {{$shipment['ctn_qty_ttl']}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px;font-size: 15px;background-color: #7d7d7d"></td>
                    <td class="odr-tr-td" style="padding:2px;font-size: 15px">
                        {{$shipment['weight_ttl']}}
                    </td>
                    <td class="odr-tr-td" style="padding:2px;font-size: 15px;background-color: #7d7d7d"></td>
                    <td class="odr-tr-td" style="text-transform:uppercase;padding:2px;font-size: 15px">
                        GRAND TOTAL
                    </td>
                    <td colspan="2"
                        style="padding:2px;text-align: center; font-weight: 300;border-bottom: 1px solid #000;font-size: 15px; background-color: #7d7d7d"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" ; class="bd-bt">
                <tr>
                    <td width="50%"
                        style="border-right: 1px solid #000; padding: 2px; text-align: center;font-size: 11px; vertical-align: top">
                        <p style="margin: 0; "> Where the rate is dependent on value, shippers are required to
                            state
                            specially in writing the agreed or declared value of the property as follows:</p>

                        <p style="margin: 0;">"The agreed or declared value of the property is specially stated
                            by the
                            shipper to be not exceeding __________ per __________"</p>
                    </td>
                    <td width="50%" style=" padding: 5px; vertical-align: top; font-size: 15px">
                        <p style="margin: 5px">COD Amount ${{!empty($shipment['freight_charge_cost'])
                        ?$shipment['freight_charge_cost']:"_________________"}}</p>

                        <p style="margin: 5px">Fee terms:
                            <img src="{{$shipment['fee_terms'] == "CO"?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                            Collect &nbsp;&nbsp;
                            <img src="{{$shipment['fee_terms'] == "PR"?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                            Prepaid
                        </p>

                        <p style="padding-left: 30px; margin: 5px">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer check acceptable
                            <img src="{{$shipment['cus_accept']?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border-bottom: 1px solid #000;padding: 5px; font-size: 13px">NOTE: Liability
            Limitation for loss or damage in this shipment may be applicable. See 49 U.S.C.- 14706(c)(1)(A) and (B)
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" ; class="bd-bt">
                <tr>
                    <td width="50%"
                        style="border-right: 1px solid #000; padding: 2px; text-align: center;font-size: 10px; vertical-align: top; text-align: left">
                        <p style="margin: 0;"> &nbsp;&nbsp;RECEIVED, subject to individually determined rates or
                            contracts
                            that have been
                            agreed upon in writing between the carrier and shipper, otherwise to the
                            rates,classifications and rules that have been established by the carrier and are
                            available to the shipper, on request, and to all applicable states and federal
                            regulations</p>
                    </td>
                    <td width="50%" style=" padding: 5px; vertical-align: top; font-size: 12px">
                        <p>&nbsp;The carrier shall not make delivery of this shipment without payment
                            of freight and all other lawful charges</p>

                        <p style="padding-top: 15px"> _____________________________ Shipper Signature</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="33%" style="vertical-align: top; padding-left: 5px; font-size: 11px" class="bd-right">
                        &nbsp;&nbsp;SHIPPER SIGNATURE / DATE
                        <p style="margin: 0; font-size: 10px; padding-bottom: 20px">&nbsp;&nbsp;This is to certify that
                            the above named materials
                            are properly classified, packaged, marked and
                            labeled, and are proper condition for transportation
                            according to the applicable regulations of the DOT.</p>
                        <br>

                        <p>_____________________ / _____________</p>
                    </td>
                    <td width="37%" style=" vertical-align: top; padding: 2px">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th style="font-weight: 500; font-size: 14px;vertical-align: top">Trailer Loaded</th>
                                <th style="font-weight: 500; font-size: 14px;padding-bottom: 10px">Freight Counted</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td width="50%">
                                    <p style="margin: 0;font-size: 14px;">
                                        <img src="{{$shipment['trailer_loaded_by']=="BS"?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                                        By Shipper
                                    </p>

                                    <p style="margin: 0;font-size: 14px;">
                                        <img src="{{$shipment['trailer_loaded_by']=="BD"?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                                        By Driver
                                    </p>
                                </td>
                                <td>
                                    <p style="margin: 0;font-size: 13px;">
                                        <img src="{{$shipment['freight_counted_by']=="BS"?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                                        By Shipper
                                    </p>

                                    <p style="margin: 0;font-size: 13px;">
                                        <img src="{{$shipment['freight_counted_by']=="BD"?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                                        By Driver/pallets said to contain
                                    </p>

                                    <p style="margin: 0;font-size: 13px;">
                                        <img src="{{$shipment['freight_counted_by']=="BP"?URL::asset('img/checked.gif'):URL::asset('img/un-check.gif')}}" style="width: 13px; height: 13px;"/>
                                        By Driver/Pieces
                                    </p>

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="30%"
                        style="border-left: 1px solid #000; vertical-align: top; padding-left: 5px; font-size: 11px">
                        CARRIER SIGNATURE / PICKUP DATE
                        <p style="margin: 0;font-size: 9px">Carrier acknowledges receipt of packaged and
                            required placards. Carrier certifies emergency response
                            information was made available and/or carrier has the
                            U.S. DOT emergency response guidebook or equivalent
                            documentation in the vehicle.</p>
                        <br>

                        <p>_____________________ / _____________</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
