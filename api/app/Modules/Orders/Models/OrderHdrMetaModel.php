<?php

namespace App\Modules\Orders\Models;

use App\Modules\Orders\Eloquents\OdrHdrMeta;

class OrderHdrMetaModel extends AbstractModel
{
    /**
     * @param OdrHdrMeta $model
     */
    public function __construct(OdrHdrMeta $model = null)
    {
        $this->model = ($model) ?: new OdrHdrMeta();
    }

    public function getOrderFlow($odr_id)
    {
        $flow = $this->getFirstWhere([
            'odr_id'    => $odr_id,
            'qualifier' => 'OFF'
        ]);
        $value = object_get($flow, 'value', null);
        $val_json_decodes = [];
        if ($value) {
            $valueJsons = \GuzzleHttp\json_decode($value);
            if ($valueJsons) {
                foreach ($valueJsons as $valueJson) {
                    $val_json_decode = [
                        'odr_flow_id' => object_get(\GuzzleHttp\json_decode($valueJson), 'odr_flow_id', 0),
                        'step'        => object_get(\GuzzleHttp\json_decode($valueJson), 'step', 0),
                        'flow_code'   => object_get(\GuzzleHttp\json_decode($valueJson), 'flow_code', ''),
                        'odr_sts'     => object_get(\GuzzleHttp\json_decode($valueJson), 'odr_sts', ''),
                        'name'        => object_get(\GuzzleHttp\json_decode($valueJson), 'name', ''),
                        'description' => object_get(\GuzzleHttp\json_decode($valueJson), 'description', ''),
                        'dependency'  => object_get(\GuzzleHttp\json_decode($valueJson), 'dependency', 0),
                        'type'        => object_get(\GuzzleHttp\json_decode($valueJson), 'type', ''),
                        'usage'       => object_get(\GuzzleHttp\json_decode($valueJson), 'usage', 0),
                    ];
                    array_push($val_json_decodes, $val_json_decode);
                }
            }
        }

        return $val_json_decodes;
    }

    public function getFlow($configs, $flow) {
        foreach ($configs as $config)
        {
            if(array_get($config, 'flow_code') == $flow) {
                return $config;
            }
        }
    }

    public function insert(array $data)
    {
        return $this->model->insert($data);
    }
}