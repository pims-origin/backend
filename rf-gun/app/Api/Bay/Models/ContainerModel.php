<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\Bay\Models;

use Seldat\Wms2\Models\Container;

class ContainerModel extends AbstractModel
{
    /**
     * ContainerModel constructor.
     *
     * @param Container|null $model
     */
    public function __construct(Container $model = null)
    {
        $this->model = ($model) ?: new Container();
    }

    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $this->sortBuilder($query, $attributes);
        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }

    public function getCtnrByIds($ctnrId)
    {
        $rows = $this->model
            ->where('ctnr_id', $ctnrId)
            ->first();

        return $rows;
    }
}
