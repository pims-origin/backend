<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\Zone;
use Wms2\UserInfo\Data;

class LocationModel extends AbstractModel
{
    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    public function checkLocationByLocCode($locCode)
    {
        return $this->model->where('loc_code', $locCode)->count();
    }

    public function getLocationByLocCode($isFull, $locCode, $whsId, $pltType, $blockStack = false)
    {
        $query = $this->model
            ->leftJoin('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->select([
                'location.loc_id',
                'location.loc_code',
                'location.loc_alternative_name',
                'location.loc_sts_code',
                // 'location.spc_hdl_code',
                'pallet.plt_id as old_plt_id',
                'pallet.rfid',
                'customer_zone.cus_id',
                'location.loc_zone_id',
                // 'zone.cat_code',
            ])
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->whereNull('location.rfid')
            ->where('location.loc_whs_id', $whsId)
            ->where('location.plt_type', $pltType);

        if ($blockStack) {
            $query->where('location.loc_code', 'LIKE', $locCode . "%");
            $query->whereNull('pallet.loc_id');
            $query->where('location.loc_sts_code', 'AC');
            $query->where('is_block_stack', 1);
            //$query->orderBy('location.loc_code', 'ASC');
        } else {
            $query->where('location.loc_code', $locCode);
        }

        //Full pallet suggestion
        if ($isFull) {
            $query->orderBy(DB::Raw("location.loc_code REGEXP '([A])[1-2]$', location.loc_code"), 'ASC');
        } else {
            $query->orderBy(DB::Raw("location.loc_code REGEXP '([B-Z])[1-2]$', location.loc_code"), 'ASC');
        }

        return $query->first();
    }

    public function getSuggest($locIds)
    {
        $query = $this->make(['cartons']);
        $query->whereHas('cartons', function ($q) {
            $q->where('ctn_sts', 'AC');
        });

        return $query->select(['loc_code', 'loc_alternative_name', 'loc_id'])
            ->whereIn('loc_id', $locIds)
            ->whereNull('rfid')
            ->get();
    }

    /**
     * @param $locCode
     * @param $whsId
     *
     * @return mixed
     */
    public function getByLocCode($locCode, $whsId)
    {
        if (!is_array($locCode)) {
            $locCode = [$locCode];
        }

        return $this->model
            ->select(['loc_id', 'loc_code'])
            ->whereIn('loc_code', $locCode)
            ->where('loc_whs_id', $whsId)
            ->get();
    }

    public function getDynZone($whsId, $cusId)
    {
        $chkDynZone = DB::table('cus_config')->where([
            "whs_id"      => $whsId,
            "cus_id"      => $cusId,
            "config_name" => "DNZ",
            "ac"          => "Y",
        ])->value('config_value');

        if (empty($chkDynZone)) {
            return false;
        }

        $customerObj = Customer::where('cus_id', $cusId)->first();

        $zoneCode = $customerObj->cus_code . "-D";
        $zoneName = $customerObj->cus_code . " - Dynamic Zone";
        $zoneObj = Zone::where('zone_code', $zoneCode)->first();

        if ($zoneObj) {
            $zoneObj->zone_num_of_loc += 1;
            $zoneObj->save();

            return $zoneObj->zone_id;
        }

        $colorCode = DB::table('customer_color')->where('cus_id', $cusId)
            ->value('cl_code');

        $arrData = [
            "zone_name"       => $zoneName,
            "zone_code"       => $zoneCode,
            "zone_whs_id"     => $whsId,
            "zone_type_id"    => 5,
            "dynamic"         => 1,
            "zone_min_count"  => 1,
            "zone_max_count"  => 100,
            "zone_color"      => $colorCode ?? '#ffffff',
            "zone_num_of_loc" => 1,
            "created_at"      => time(),
            "updated_at"      => time(),
            "created_by"      => Data::getCurrentUserId(),
            "updated_by"      => Data::getCurrentUserId(),
            "deleted_at"      => 915148800,
            "deleted"         => 0
        ];

        $zoneId = DB::table('zone')->insertGetId($arrData);

        //add customer_zone
        DB::table('customer_zone')->insert([
            'zone_id'    => $zoneId,
            'cus_id'     => $cusId,
            'created_at' => time(),
            'updated_at' => time(),
            "created_by" => Data::getCurrentUserId(),
            "updated_by" => Data::getCurrentUserId(),
            "deleted_at" => 915148800,
            'deleted'    => 0
        ]);

        return $zoneId;
    }


    public function getActQtyOdrDtlAllocation($wvDtlId)
    {
        $result = DB::table('odr_dtl_allocation')
            ->select('qty', 'loc_code', 'plt_rfid')
            ->where('wv_dtl_id', $wvDtlId)
            ->first();

        return $result;

    }

    public function pickPalletUpdateActiveLocationByWvDtlId($whsId, $locId)
    {
        // Update Pallet Active
        Pallet::where('loc_id', $locId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->update([
                'plt_sts'    => 'AC',
                'updated_by' => Data::getCurrentUserId()
            ]);

        // Update Carton To Active
        Carton::where('loc_id', $locId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->update([
                'ctn_sts'    => 'AC',
                'updated_by' => Data::getCurrentUserId()
            ]);

        // Update Location AC
        $result = $this->model
            ->where('loc_id', $locId)
            ->where('loc_whs_id', $whsId)
            ->where('deleted', 0)
            ->update([
                'loc_sts_code' => 'AC',
                'updated_by'   => Data::getCurrentUserId()
            ]);

        return $result;

    }

    public function getLocationByKey($whsId, $key, $locCode)
    {
        return $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where($key, $locCode)
          //  ->where('location.loc_sts_code', 'AC')
            ->where('loc_type.loc_type_code', 'SHP')
            ->where('location.loc_whs_id', $whsId)
            ->first();
    }

    public function getLocationByKeyNoType($whsId, $key, $locCode)
    {
        return $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where($key, $locCode)
          //  ->where('location.loc_sts_code', 'AC')
            ->where('location.loc_whs_id', $whsId)
            ->first();
    }

}
