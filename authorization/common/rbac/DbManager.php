<?php
namespace common\rbac;

use common\models\Authority;
use Yii;
use yii\caching\Cache;
use yii\db\Connection;
use yii\db\Query;
use yii\db\Expression;
use yii\base\InvalidCallException;
use yii\base\InvalidParamException;
use yii\di\Instance;
use yii\rbac\Item;

class DbManager extends \yii\rbac\DbManager
{
    const VIEW_MENU_PREFIX = 'ViewMenu';

    public function getMenuPermissionsByUser($userId)
    {
        if (empty($userId)) {
            return [];
        }

        $directRoles = $this->getDirectMenuPermissionsByUser($userId);
        $inheritedRoles = $this->getInheritedMenuPermissionsByUser($userId);

        return array_merge($directRoles, $inheritedRoles);
    }

    public function getRolesByUserId($userId)
    {
        if (empty($userId)) {
            return [];
        }

        $directPermission = $this->getDirectRolesByUserId($userId);
        $inheritedPermission = $this->getInheritedRolesByUserId($userId);

        return array_merge($directPermission, $inheritedPermission);
    }

    /**
     * Returns all permissions that are directly assigned to user.
     * @param string|integer $userId the user ID (see [[\yii\web\User::id]])
     * @return Permission[] all direct permissions that the user has. The array is indexed by the permission names.
     * @since 2.0.7
     */
    protected function getDirectMenuPermissionsByUser($userId)
    {
        $query = (new Query)->select('b.*')
            ->from(['a' => $this->assignmentTable, 'b' => $this->itemTable])
            ->where('{{a}}.[[item_name]]={{b}}.[[name]]')
            ->andWhere(['a.user_id' => (string) $userId])
            ->andWhere(['b.type' => Item::TYPE_PERMISSION])
            ->andWhere('{{b}}.[[name]] LIKE \'viewMenu%\'');

        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }
        return $permissions;
    }

    protected function getDirectRolesByUserId($userId)
    {
        $roles = $this->getRolesByUser($userId);
        return $roles;
    }

    /**
     * Returns all permissions that the user inherits from the roles assigned to him.
     * @param string|integer $userId the user ID (see [[\yii\web\User::id]])
     * @return Permission[] all inherited permissions that the user has. The array is indexed by the permission names.
     * @since 2.0.7
     */
    protected function getInheritedMenuPermissionsByUser($userId)
    {
        $query = (new Query)->select('item_name')
            ->from($this->assignmentTable)
            ->where(['user_id' => (string) $userId]);

        $childrenList = $this->getChildrenList();
        $result = [];
        foreach ($query->column($this->db) as $roleName) {
            $this->getChildrenRecursive($roleName, $childrenList, $result);
        }

        if (empty($result)) {
            return [];
        }

        $query = (new Query)->from($this->itemTable)->where([
            'type' => Item::TYPE_PERMISSION,
            'name' => array_keys($result),
        ]);
        $query->andWhere('[[name]] LIKE \'viewMenu%\'');

        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }
        return $permissions;
    }

    protected function getInheritedRolesByUserId($userId)
    {
        $query = (new Query)->select('item_name')
            ->from($this->assignmentTable)
            ->where(['user_id' => (string) $userId]);

        $childrenList = $this->getChildrenList();
        $result = [];
        foreach ($query->column($this->db) as $roleName) {
            $this->getChildrenRecursive($roleName, $childrenList, $result);
        }

        if (empty($result)) {
            return [];
        }

        $query = (new Query)->from($this->itemTable)->where([
            'type' => Item::TYPE_ROLE,
            'name' => array_keys($result),
        ]);

        $roles = [];
        foreach ($query->all($this->db) as $row) {
            $roles[$row['name']] = $this->populateItem($row);
        }
        return $roles;
    }

    /**
     * Generate Groups for permissions
     * @param array $perms
     * @return array
     */
    public function getPermWithGrp($perms=[])
    {
        $query = (new Query)->select(['a.parent AS group', 'permissions' => 'GROUP_CONCAT(a.child)'])
            ->from(['a' => $this->itemChildTable])
            ->leftJoin(['b' => $this->itemTable], "a.parent = b.name")
            ->where(['a.child' => $perms])
            ->andWhere('b.type = 3') //hard code type=3 is group perms
            ->andWhere("a.parent != 'Menu Mangement'") //except Menu Mangement
            ->groupBy('a.parent')
            ->orderBy('b.description')
        ;
        $rows = $query->all();

        return self::genGroups($rows);
    }
    protected static function genGroups($rows)
    {
        foreach ($rows as &$row) {
            $row['permissions'] = explode(',', $row['permissions']);
        }

        return $rows;
    }

    public function getGroup($name)
    {
        $item = $this->getItem($name);
        return $item instanceof Item && $item->type == Authority::GROUP_TYPE ? $item : null;
    }

    public function getCSRUser()
    {
        $query = (new Query)
            ->select("ur.user_id", "DISTINCT")
            ->from(['ac' => 'authority_child'])
            ->innerJoin(['a' => 'authority'], 'ac.parent = a.name AND a.type = 1')
            ->innerJoin(['ur' => 'user_role'], 'ur.item_name = a.name')
            ->where("ac.child = 'isCSR'")
            ->column();

        return $query;
    }

    public function getPickerUser()
    {
        $query = (new Query)
            ->select("ur.user_id", "DISTINCT")
            ->from(['ac' => 'authority_child'])
            ->innerJoin(['a' => 'authority'], 'ac.parent = a.name AND a.type = 1')
            ->innerJoin(['ur' => 'user_role'], 'ur.item_name = a.name')
            ->where("ac.child = 'updateOrderPicking'")
            ->column();

        return $query;
    }
    public function getPutterUser()
    {
        $query = (new Query)
            ->select("ur.user_id", "DISTINCT")
            ->from(['ac' => 'authority_child'])
            ->innerJoin(['a' => 'authority'], 'ac.parent = a.name AND a.type = 1')
            ->innerJoin(['ur' => 'user_role'], 'ur.item_name = a.name')
            ->where("ac.child = 'updateInventory'")
            ->column();

        return $query;
    }

    public function getPutBackerUser()
    {
        $query = (new Query)
            ->select("ur.user_id", "DISTINCT")
            ->from(['ac' => 'authority_child'])
            ->innerJoin(['a' => 'authority'], 'ac.parent = a.name AND a.type = 1')
            ->innerJoin(['ur' => 'user_role'], 'ur.item_name = a.name')
            ->where("ac.child = 'updatePutback'")
            ->column();

        return $query;
    }

    public function getAccountUser($isManager = false, $params)
    {
        $query = (new Query)
            ->select("ur.user_id", "DISTINCT")
            ->from(['ac' => 'authority_child'])
            ->innerJoin(['a' => 'authority'], 'ac.parent = a.name AND a.type = 1')
            ->innerJoin(['ur' => 'user_role'], 'ur.item_name = a.name');
        $where = [
            'ac.child' => $isManager ? 'changeInvoiceStatus' : 'createInvoice'
        ];
        if ((isset($params['whs_id']) && $params['whs_id'] != '') || isset($params['cus_id']) && $params['cus_id'] != '') {
            $query->innerJoin(['ciu' => 'cus_in_user'], 'ur.user_id = ciu.user_id');
            if (isset($params['whs_id'])) {
                $where['ciu.whs_id'] = $params['whs_id'];
            }
            if (isset($params['cus_id'])) {
                $where['ciu.cus_id'] = $params['cus_id'];
            }
        }
        $query->where($where);
        return $query->column();
    }
}
