<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycleDiscrepancy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('cycle_discrepancy')) {
            Schema::create('cycle_discrepancy', function (Blueprint $table) {
                $table->increments('dicpy_ctn_id');
                $table->integer('cycle_hdr_id');
                $table->integer('cycle_dtl_id');
                $table->integer('ctn_id');
                $table->integer('dicpy_qty');
                $table->char('dicpy_sts', 2);
                $table->integer('created_by');
                $table->integer('updated_by');
                $table->integer('created_at');
                $table->integer('updated_at');
                $table->char('sts', 1);
                $table->tinyInteger('deleted');
                $table->integer('deleted_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cycle_discrepancy')) {
            Schema::drop('cycle_discrepancy');
        }
    }
}
