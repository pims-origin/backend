<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnMeta;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Models\Setting;
use Seldat\Wms2\Models\SystemMeasurement;

class AsnMetaModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnMeta();
    }

    public function upsertAsnCntr($ctnrId, $asnHdr)
    {
        $asnMetaLpn = $this->getModel()->where([
            'qualifier'  => 'LPN',
            'asn_hdr_id' => $asnHdr->asn_hdr_id
        ])
            ->value('value');

        $grNum = str_replace('ASN', 'GDR', $asnHdr->asn_hdr_num);
        $data = [];
        if (empty($asnMetaLpn)) {
            $grNum = str_replace('ASN', 'GDR', $asnHdr->asn_hdr_num);
            $data[$ctnrId] = ['gr_num' => $grNum . '-01'];

            $this->getModel()->insert([
                'qualifier'  => 'LPN',
                'asn_hdr_id' => $asnHdr->asn_hdr_id,
                'value'      => \GuzzleHttp\json_encode($data)
            ]);
        } else {
            $data = \GuzzleHttp\json_decode($asnMetaLpn, true);
            if (!isset($data[$ctnrId])) {
                $seq = count($data) + 1;
                $ctnrNo = str_pad($seq, 2, '0', STR_PAD_LEFT);
                $data[$ctnrId] = ['gr_num' => $grNum . '-' . $ctnrNo];

                $this->getModel()->where([
                    'qualifier'  => 'LPN',
                    'asn_hdr_id' => $asnHdr->asn_hdr_id
                ])
                    ->update(['value' => \GuzzleHttp\json_encode($data)]);
            }
        }


    }


}
