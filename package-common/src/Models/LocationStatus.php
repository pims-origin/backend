<?php

namespace Seldat\Wms2\Models;


class LocationStatus extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loc_status';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'loc_sts_code';

    public $incrementing = false;

}
