<?php

namespace App\Api\V1\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
class DocumentController extends AbstractController
{
    const ORDER_DOCUMENT_FOLDER = 'order-document';

    public function downloadDocument(Request $request) {
        $input = $request->getQueryParams();

        $docType = $input['type'];
        $fileName = $input['file_name'];

        $filePath = storage_path(self::ORDER_DOCUMENT_FOLDER . "/{$docType}/{$fileName}");

        if (file_exists($filePath)) {
            return response()->download($filePath);
        } else {
            return $this->response->errorBadRequest('File not exist.');
        }
    }
 }