<?php

namespace App\Api\V2\Inbound\Location\GetLocationChino\Controllers;

use App\Api\V2\Inbound\Location\GetLocationChino\Models\CartonModel;
use App\Api\V2\Inbound\Location\GetLocationChino\Models\LocationModel;
use App\Api\V2\Inbound\Location\GetLocationChino\Models\ItemModel;
use App\Api\V2\Inbound\Location\GetLocationChino\Models\Log;

use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\MyHelper;
use Seldat\Wms2\Utils\SelArr;


class PutAwayController extends AbstractController
{

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * PutAwayController constructor.
     *
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param ItemModel $itemModel
     */
    public function __construct(
        LocationModel $locationModel
    ) {
        $this->cartonModel   = new CartonModel();
        $this->locationModel = $locationModel;
        $this->itemModel     = new ItemModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function getSuggestLocations($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input = SelArr::removeNullOrEmptyString($input);
        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/location/rack/list-suggest-chino";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'LEL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get List Suggest Location'
        ]);
        /*
         * end logs
         */

        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        $cusId  = array_get($input, 'cus_id', null);
        $putAll = array_get($input, 'put_all', 0);
        $ctnTtl = array_get($input, 'ctn_ttl', 0);
        // $itemId     = array_get($input, 'item_id', 0);

        try {
            $limit = 6;
            // WAP-584 - Create API get suggest location list by Pallet tag
            // if ($putAll == 1) {
                //o Suggest empty locations if WAP send cartons within a pallet
                $locations = $this->locationModel->getMoreEmptyLocationByCusId($whsId, $cusId, 'RAC', $limit);

                if (!count($locations)) {
                    $msg = sprintf("Current location is not defined in WMS system. Please contact Administrator");
                    return $this->_responseErrorMessage($msg);
                }
//             } else {
//                 //o Suggest approximately locations if WAP send cartons without a pallet
//                 $data = [
//                     'whs_id'    => $whsId,
//                     'cus_id'    => $cusId,
//                     // 'item_id'   => $itemId,
//                     'item_id'   => null,
//                     'is_picked' => 1,
// //                    'lot'       => self::DEFAULT_LOT_PUTBACK,
// //                    'pack_size' => $packSize,
// //                    'ctn_ttl'   => $ctnPutback,
//                 ];
//                 //o Suggest locations at the level 1 and less cartons and the same item id
//                 /*
//                  * - Cartons is belong to warehouse, customer
//                  * - Is the same SKU
//                  * - Have picked carton
//                  * - Low level
//                  */
//                 $locations = $this->locationModel->getMoreLocationCanPutCarton($data, $limit);
//                 $countLocs = count($locations);
//                 if ($countLocs < $limit) {
//                     $limit -= $countLocs;
//                     $data = [
//                         'whs_id'    => $whsId,
//                         'cus_id'    => $cusId,
//                         'item_id'   => $itemId,
//                         'is_picked' => 0,
//                     ];
//                     //o Suggest locations at the level 1 and less cartons
//                     /*
//                      * - Cartons is belong to warehouse, customer
//                      * - Is the same SKU
//                      * - Low level
//                      */
//                     $locations2 = $this->locationModel->getMoreLocationCanPutCarton($data, $limit);
//                     $countLocs2 = count($locations2);
//                     if ($countLocs2 && $countLocs) {
//                         $locations = array_merge($locations, $locations2);
//                     } elseif ($countLocs2) {
//                         $locations = $locations2;
//                     }
//                     $countLocs = count($locations);
//                     if ($countLocs < $limit) {
//                         $limit -= $countLocs;
//                         $data = [
//                             'whs_id'    => $whsId,
//                             'cus_id'    => $cusId,
//                             'item_id'   => 0,
//                             'is_picked' => 0,
//                         ];
//                         /*
//                          * - Cartons is belong to warehouse, customer
//                          * - Low level
//                          */
//                         $locations3 = $this->locationModel->getMoreLocationCanPutCarton($data, $limit);
//                         $countLocs3 = count($locations3);
//                         if ($countLocs3 && count($locations)) {
//                             $locations = array_merge($locations, $locations3);
//                         } elseif ($countLocs3) {
//                             $locations = $locations3;
//                         }
//                         $countLocs = count($locations);
//                         if (!$countLocs) {
//                             $msg = sprintf("Current location is not defined in WMS system. Please scan a Pallet");
//                             return $this->_responseErrorMessage($msg);
//                         }
//                     }
//                 }
//             }

            $locationFirst = array_shift($locations);
            $data = [
                "cus_id"               => $cusId,
                "loc_sts_code"         => array_get($locationFirst, 'loc_sts_code'),
                "loc_alternative_name" => array_get($locationFirst, 'loc_alternative_name'),
                "loc_id"               => array_get($locationFirst, 'loc_id'),
                "loc_rfid"             => array_get($locationFirst, 'rfid'),
                "suggested"            => $locations,
            ];

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data']   = [$data];

            return $msg;

        } catch (\Exception $e) {

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _validateParams($attributes, $whsId)
    {
        $ctnTtl = (int)array_get($attributes, 'carton_total');
        $cusId  = (int)array_get($attributes, 'cus_id');
        $putAll = (int)array_get($attributes, 'put_all');

        $names = [
            'put_all'      => 'Put all',
            // 'carton_total' => 'Carton total',
            'cusId'        => 'Customer',
        ];

        // Check Required
        $requireds = [
            'put_all'      => $putAll,
            // 'carton_total' => $ctnTtl,
            'cus_id'       => $cusId,
        ];

        // Check int type
        $integers = [
            'put_all'       => $putAll,
//            'carton_total' => $ctnTtl,
            'cus_id'        => $cusId,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        foreach ($integers as $key => $field) {
            if (!is_int($field) || $field < 1 || $field > 999999999) {
                $errorDetail = "{$names[$key]} must be integer and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        if (!in_array($putAll, [1, 2])) {
            $msg = "Put all mod is required. Just including 1(All) / 2(Partial)";
            return $this->_responseErrorMessage($msg);
        }

        //validate pallet RFID
        // $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
        // if (!$pltRFIDValid->validate()) {
        //     return $this->_responseErrorMessage($pltRFIDValid->error);
        // }

        // //validate location RFID
        // if ($type == 1) {
        //     $locRFIDValid = new RFIDValidate($location, RFIDValidate::TYPE_LOCATION, $whsId);
        //     if (!$locRFIDValid->validate()) {
        //         return $this->_responseErrorMessage($locRFIDValid->error);
        //     }
        // }

        // Check customer
        $customerWarehouseArr = DB::table('customer_warehouse')
            ->where('cus_id', $cusId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->first();

        if (!count($customerWarehouseArr)) {
            $customerArr = DB::table('customer')
                ->where('cus_id', $cusId)
                ->where('deleted', 0)
                ->first();
            if (!count($customerArr)) {
                $msg = sprintf("The customer doesn't exist!");
                return $this->_responseErrorMessage($msg);
            }

            $msg = sprintf("The customer doesn't belong to current warehouse!");
            return $this->_responseErrorMessage($msg);
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _checkMixSkuOnLoc($whsId, $itemId, $locId)
    {
        $count = DB::table('cartons')
            ->where('whs_id', $whsId)
            ->where('item_id', '!=', $itemId)
            ->where('loc_id', $locId)
            // ->whereIn('ctn_sts', ['AC', 'LK'])
            ->where('deleted', 0)
            ->count();

        return $count > 0 ? true : false;
    }

    private function _checkEmptyLoc($whsId, $locId)
    {
        $count = DB::table('cartons')
            ->where('whs_id', $whsId)
            ->where('loc_id', $locId)
            // ->whereIn('ctn_sts', ['AC', 'LK'])
            ->where('deleted', 0)
            ->count();

        return $count == 0 ? true : false;
    }
}
