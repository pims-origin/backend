#########################-START-VERSION-2-##################################
- WAP-804: [Inbound - PutawayNoPallet] Create API submit full or remain Item tag
    + Put cartons to rack with 2 options:
        * Put all (full)
        * Put apart (partial)
    + Without pallet
#########################-END-VERSION-2-##################################