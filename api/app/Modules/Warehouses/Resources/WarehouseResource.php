<?php
namespace App\Modules\Warehouses\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WarehouseResource extends JsonResource
{
    public function toArray($request)
    {
       // $meta = isset($this->quantity) ? ['meta' => ['quantity' => $this->quantity]] : [];
        return [
                'type' => 'warehouses',
                'id' => (int) $this->whs_id,
                'attributes' => [
                    'name' => $this->whs_name,
                    'code'  => $this->whs_code,
                ],
            ] ;

    }
}