<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class WavePickHdrModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickHdr($data){
        return $this->model->create($data);
    }

    /**
     * @return mixed
     */
    public function getLatestWavePickNumber()
    {
        return $this->model->orderBy('wv_num', 'desc')->take(1)->first();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getWvHdrInfo($wv_id){
        return $this->model
            ->where('wv_hdr.wv_id','=',$wv_id)
            ->leftJoin('odr_hdr','odr_hdr.wv_id','=','wv_hdr.wv_id')
            ->leftJoin('customer','customer.cus_id','=','odr_hdr.cus_id')
            ->select(
                'wv_hdr.wv_id',
                'wv_hdr.wv_num',
                'odr_hdr.cus_id',
                'customer.cus_name',
                'wv_hdr.wv_sts',
                'wv_hdr.whs_id'
            )
            ->first();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['wv_num','wv_sts'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }
        $this->model->filterData($query);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function updateStatusWvHdr($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    public function getWaveList($attributes, array $with, $limit = 100) {
        $query = $this->make($with);

        $query->where('whs_id', (int)$attributes['whs_id']);
        $query->where('picker', $attributes['picker']);

        $query->whereIn('wv_sts', [
            Status::getByValue("New", "WAVEPICK-STATUS"),
            Status::getByValue("Picking", "WAVEPICK-STATUS")
        ]);

        return $query->paginate($limit);
    }

    public function getNumOfOrders($wvId){
        $count = \DB::table('odr_hdr')->where('wv_id', $wvId)->count();
        return $count;
    }

    public function getNumOfSkus($wvId){
        $count = \DB::table('wv_dtl')->where('wv_id', $wvId)->count();
        return $count;
    }

}
