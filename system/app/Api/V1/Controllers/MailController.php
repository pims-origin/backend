<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 06-Jun-16
 * Time: 08:36
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EmailSentModel;
use App\Api\V1\Models\InvEmailedModel;
use App\Api\V1\Models\SettingModel;
use App\Utils\Helpers;
use App\Utils\MyHelpers;
use App\Utils\SendMail;
use JmesPath\Env;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Request as IRequest;
use PhpParser\Serializer;
use Symfony\Component\HttpKernel\Exception as HttpException;

class MailController extends AbstractController
{
    /**
     * @var SendMail
     */
    protected $mail;

    /**
     * @var SettingModel
     */
    protected $settingModel;

    /**
     * MailController constructor.
     *
     */
    public function __construct()
    {
        $this->mail = new SendMail();
        $this->settingModel = new SettingModel();

    }

    /**
     * Reset or Setup password
     *
     * @param Request $request
     * @param $act . Only contain: reset_password_url OR setup_password_url
     */
    private function _resetOrSetupPassword(Request $request, $tpl)
    {
        $params = $request->getParsedBody();
        if (empty($params['email'])
            || empty($params['username'])
            || empty($params['first_name'])
            || empty($params[$tpl])
        ) {
            return $this->response->errorBadRequest('Missed parameter');
        }

        $view = "mailResetPassword";
        $to = $params['email'];
        $subject = "Reset Password!";
        $data = [
            'name'         => $params['first_name'] . (!empty($params['last_name']) ? ' ' . $params['last_name'] : ''),
            'website_name' => 'Seldat',
            'username'     => $params['username'],
            'author'       => 'CS'
        ];

        if ($tpl === 'setup_password_url') {
            $view = "mailSetupPassword";
            $subject = "Setup Password!";
            $data['url_setup'] = $params['setup_password_url'];
        } else {
            $data['url_reset'] = $params['reset_password_url'];
        }

        $this->mail->sendMail($view, $to, $subject, $data);
        $this->response->noContent();
    }

    public function resetPassword(Request $request)
    {
        return $this->_resetOrSetupPassword($request, 'reset_password_url');
    }

    public function setupPassword(Request $request)
    {
        return $this->_resetOrSetupPassword($request, 'setup_password_url');
    }

    public function welcomeUser(Request $request)
    {
        $params = $request->getParsedBody();
        if (empty($params['email'])
            || empty($params['first_name'])
        ) {
            return $this->response->errorBadRequest('Missed parameter');
        }
        $this->mail->sendMail(
            'mailNewUser',
            $params['email'],
            "Account Created!",
            [
                'name'         => $params['first_name']
                    . (!empty($params['last_name']) ? ' ' . $params['last_name'] : ''),
                'website_name' => 'Seldat',
                'url_active'   => 'javascript:;',//$params['url_active'],
                'url_help'     => 'javascript:;',//$params['url_help'],
                'mail'         => 'seldat.noreplpy@gmail.com',
            ]
        );
        $this->response->noContent();
    }

    public function userResetPassword(Request $request)
    {
        $params = $request->getParsedBody();
        if (empty($params['email'])
            || empty($params['first_name'])
        ) {
            return $this->response->errorBadRequest('Missed parameter');
        }
        $this->mail->sendMail(
            'mailUserResetPassword',
            $params['email'],
            "Admin reset password!",
            [
                'name'         => $params['first_name'] . (!empty($params['last_name']) ? ' ' . $params['last_name'] : ''),
                'website_name' => 'Seldat',
                'author'       => 'CS'
            ]
        );
        $this->response->noContent();
    }

    public function reportIssue(IRequest $request)
    {
        $params = $request->all();
        $fileName = isset($params['ext']) ? $params['ext'] : "";
        $image = isset($params['img_attach_screen']) ? $params['img_attach_screen'] : "";
        $otherEmail = isset($params['other_email']) ? $params['other_email'] : "";

        // Get Email invoice to send
        $issueReport = $this->settingModel->getSettingByKey('ER');

        if (!empty($issueReport->setting_value)) {
            $issueReportEmail = current(array_filter(explode('"', $issueReport->setting_value)));
            $file = $params['file'];
            $pathName = !empty($file) ? $file->getPathName() : false;
            $this->mail->sendMail(
                'mailReportIssue',
                $issueReportEmail,
                "User Report Issue",
                [
                    'from'    => $params['email_from'],
                    'phone'   => $params['phone'],
                    'content' => $params['content']
                ],
                $pathName,
                $fileName,
                $image
            );
            if (!empty($otherEmail)) {
                $this->mail->sendMail(
                    'mailReportIssue',
                    $otherEmail,
                    "User Report Issue",
                    [
                        'from'    => $params['email_from'],
                        'phone'   => $params['phone'],
                        'content' => $params['content']
                    ],
                    $pathName,
                    $fileName,
                    $image
                );
            }
        }

        $this->response->noContent();
    }

    public function sendInvoiceEmail()
    {
        $invModel = new InvEmailedModel();
        $invEmailed = $invModel->getInvEmailedNew();

        $fileUrl = object_get($invEmailed, 'file_url', null);

        $tmp = explode('/',$fileUrl);
        $fileName = array_pop($tmp);

        $path = storage_path('logs/'.$fileName);

        MyHelpers::downloadS3($fileUrl, $path);

        if ($invEmailed) {
            // Get Email invoice to send
            $invoiceAlert = $this->settingModel->getSettingByKey('EI');
            if (!empty($invoiceAlert->setting_value)) {
                $invoiceAlertEmail = current(array_filter(explode('"', $invoiceAlert->setting_value)));
                //$fileName = array_pop(explode('/',$path));
                $sent = $this->mail->sendMail(
                    'sendInvoiceEmail',
                    $invoiceAlertEmail,
                    sprintf("For cusomrer %s   invoice# %s  has been created",
                        object_get($invEmailed, 'cus_name', null),
                        object_get($invEmailed, 'inv_num', null)),
                    [
                        'file_url' => object_get($invEmailed, 'file_url', null),
                        'file_name' => $fileName,
                        'inv_num' => object_get($invEmailed, 'inv_num', null),
                        'cus_name' => object_get($invEmailed, 'cus_name', null),
                    ],
                    $path,
                    $fileName,
                    false,
                    true
                );

                (new EmailSentModel())->create([
                    'owner' => object_get($invEmailed, 'inv_email_id', null),
                    'func' => 'INV',
                    'sts' => 'IN',
                    'esent_sts' => ($sent) ? 1 : 0,
                    'created_by' => 1,
                    'updated_by' => 1
                ]);
                //delete file on server
                unlink($path);
                //update inv_email_id
                (new InvEmailedModel())->getModel()
                    ->where([
                        'inv_email_id' => object_get($invEmailed, 'inv_email_id', null)
                    ])
                    ->update([
                        'inv_sts' => 'm' //mailed
                    ]);
                return "Email was sent successfully !!";
            }
        }
    }
}
