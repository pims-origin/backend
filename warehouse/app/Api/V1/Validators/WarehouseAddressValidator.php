<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:41
 */

namespace App\Api\V1\Validators;


class WarehouseAddressValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_add_line_1'      => 'required|max:50',
            'whs_add_city_name'   => 'required|max:20',
            'whs_add_state_id'    => 'required',
            'whs_add_postal_code' => 'required|max:20',
            'whs_add_whs_id'      => 'required|integer',
            'whs_add_type'        => 'required',
        ];
    }
}
