- WAP-501: [Outbound - Wavepick] Update API update wavepick for processing wrong cartons. The rules for this:
    + Ignore wrong cartons are gone through the gateway for a wave pick.
    + Return wrong cartons, WMS just assigns these cartons to the current pallet.

- WAP-506: [Outbound - Wavepick] Update API Return Cartons from Wave pick
    + Required Pallet RFID is optional.

- WAP-592: [Outbound - Wavepick] When Update wavepick, Change work flow of Cartons with status AJ