<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Validator;
use Dingo\Api\Exception\ValidationHttpException;

/**
 * Class WorkOrderSkuValidator
 *
 * @package App\Api\V1\Validators
 */
class WorkOrderSkuValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'wo_hdr_id' => 'required|integer|exists:wo_hdr,wo_hdr_id',
            'cus_id' => 'required|integer|exists:customer,cus_id',
        ];
    }
}
