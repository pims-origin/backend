<?php

namespace App\Modules\GoodsReceipts\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;
use App\Modules\GoodsReceipts\Models\GoodsReceiptModel;
use App\Modules\GoodsReceipts\Resources\GoodsReceiptCollection;
use App\Modules\GoodsReceipts\Validators\GoodsReceiptValidator;
use App\Modules\GoodsReceipts\Resources\GoodsReceiptReadResource;

class GoodsReceiptController extends Controller
{
    protected $grModel;
    protected $grValidator;

    public function __construct(GoodsReceiptModel $grModel, GoodsReceiptValidator $grValidator)
    {
        $this->grModel = $grModel;
        $this->grValidator = $grValidator;
    }

    public function index($whsId, $cusId, Request $request)
    {
        $input = $request->all();

        $limit = array_get($input, 'per_page', 20);

        $grs  = $this->grModel->getListGR($whsId, $cusId, $limit);

        $this->grValidator->checkGR($grs);

        return new GoodsReceiptCollection($grs);
    }

    public function read($whsId, $cusId, $grHdrId, Request $request)
    {
        $gr  = $this->grModel->getGRDetail($whsId, $cusId, $grHdrId);

        $this->grValidator->checkGR($gr);

        return new GoodsReceiptReadResource($gr);
    }
}