<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Warehouse;
use App\WarehouseZone;
use App\LocationType;

class ZoneLocationStatusTest extends TestCase
{
    use DatabaseTransactions;

    protected $endpoint;

    /**
     * ZoneLocationStatusTest constructor.
     */
    public function __construct()
    {
        $this->endpoint = '/v1/location-statuses';
    }

    protected $searchParams = [
        'loc_sts_code' => 'AC',
    ];

    public function testSearch_Ok()
    {
        $this->call('GET', $this->endpoint, $this->searchParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }
}
