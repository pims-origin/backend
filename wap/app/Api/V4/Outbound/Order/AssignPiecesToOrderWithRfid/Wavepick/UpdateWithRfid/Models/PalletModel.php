<?php

namespace App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Wavepick\UpdateWithRfid\Models;

use DB;
use Seldat\Wms2\Models\Pallet;

class PalletModel extends AbstractModel
{
    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
    }

    public function updatePallet($pltInfo)
    {
        $pltId = array_get($pltInfo, 'plt_id', null);
        $created_at = array_get($pltInfo, 'created_at', 0);
        $zeroDt = time();

        // Calculate storage_duration
        if (is_string($created_at) || is_int($created_at)) {
            $created_at = (int)$created_at;
        } else {
            $created_at = $created_at->timestamp;
        }

        $storageDate = date("Y-m-d", $created_at);

        $pickedDate = date("Y-m-d");

        $dateDiff = (int)round(abs(strtotime($storageDate) - strtotime($pickedDate)) / 86400);

        if ($dateDiff == 0) {
            $storageDuration = 1;
        } else {
            $storageDuration = $dateDiff;
        }

        return $this->model
            ->where('plt_id', $pltId)
            ->update([
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'ctn_ttl'          => 0,
                'updated_at'       => time(),
                'zero_date'        => $zeroDt,
                'storage_duration' => $storageDuration
            ]);
    }

    public function updatePalletCtnTtl($locIds)
    {
        return $this->model
            ->whereIn('loc_id', $locIds)
            ->update([
                'ctn_ttl' => DB::raw("(SELECT COUNT(c.ctn_id) FROM cartons c WHERE pallet.plt_id = c.plt_id)")
            ]);
    }

    public function updateZeroPallet($locIds)
    {
        //$strSQL = sprintf("IF(DATEDIFF(NOW(), FROM_UNIXTIME(created_at)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        //(created_at)), 1)", date('Y-m-d'));

        return $this->model
            ->whereIn('loc_id', $locIds)
            ->where('ctn_ttl', 0)
            ->update([
                'deleted'          => 1,
                'deleted_at'       => time(),
                'rfid'             => null,
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'is_movement'      => 0,
                'plt_sts'          => 'PD',
                'zero_date'        => time(),
                'storage_duration' => CartonModel::getCalculateStorageDurationRaw()
            ]);
    }

    public function getPalletIDWhereRFID($rfid)
    {
        return $this->model->where('rfid', $rfid)->value('plt_num');
    }

    public function getPalletMoveWhereRFID($rfid, $whsId)
    {
        return $this->model->where([
            'pallet.whs_id' => $whsId,
            'pallet.rfid'   => $rfid
        ])
            ->Join('location as l', 'l.loc_id', '=', 'pallet.loc_id')
            ->whereNotNull('pallet.loc_id')
            ->where('pallet.ctn_ttl', '>', 0)
            ->first();
    }

    public function getPalletPutBackWhereRFID($rfid, $whsId)
    {
        return $this->model->where([
            'whs_id'      => $whsId,
            'rfid'        => $rfid,
            'is_movement' => 1,
        ])
            ->whereNotNull('loc_id')
            ->where('ctn_ttl', '>', 0)
            ->first();
    }

    /**
     * @param $palletRFID
     * @param $whsId
     *
     * @return mixed
     */
    public function checkPalletPutBackWhereRFID($palletRFID, $whsId)
    {
        return $this->model->where([
            'whs_id' => $whsId,
            'rfid'   => $palletRFID
        ])
            //->whereNotNull('loc_id')
            ->orderBy('plt_id', 'desc')
            ->first();
    }

    public function getLocationIsContainPallet($locID)
    {
        return $this->model
            ->where('loc_id', $locID)
            ->where('plt_sts', 'AC')
            ->first();
    }

    public function updatePalletPutBackNewLoc(int $pltID, array $arrLoc)
    {
        return $this->model->where([
            'plt_id'      => $pltID,
            'is_movement' => 1,
        ])
            ->where('ctn_ttl', '>', 0)
            ->update(
                [
                    'loc_id'      => $arrLoc['loc_id'],
                    'loc_name'    => $arrLoc['loc_alternative_name'],
                    'loc_code'    => $arrLoc['loc_code'],
                    'is_movement' => 0,
                ]
            );
    }

    public function getPalletByGrHdrId($grHdrId)
    {
        return $this->model
            ->where('gr_hdr_id', $grHdrId)
            ->get();
    }

    /**
     * @param $wvNum
     *
     * @return string
     */
    public function generatePalletNumFromWvNum($wvNum)
    {
        $palletCode = "LPN-{$wvNum}";

        $countPallet = \DB::table('pallet')->select('plt_num')
            ->where('plt_num', 'like', $palletCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();
        $max = 0;
        if ($countPallet) {
            $pltCode = array_get($countPallet, 'plt_num', null);
            $arrMax = explode('-', $pltCode);
            $max = (int)end($arrMax);
        }

        return $palletCode . "-" . str_pad($max + 1, 4, "0", STR_PAD_LEFT);
    }

    /**
     * @param $rfid
     *
     * @return string
     */
    public function generatePalletNumFromRfid($rfid)
    {
        $palletNumInfo = $this->model
            ->select(DB::raw('plt_num'))
            ->orderBy('plt_num', 'desc')
            ->where('plt_num', 'like', '%' . $rfid . '%')
            ->first();

        $palletNum = object_get($palletNumInfo, 'plt_num', '');
        if ($palletNumInfo) {
            $palletNum++;
        } else {
            $palletNum = "LPN-{$rfid}-00001";
        }

        return $palletNum;
    }

    /**
     * remove Pallet When all Carton Rfids Are Null
     * @param  array $pltIds
     * @param  integer $whsId
     * @return boolean
     */
    public function removePalletWhenCtnRfidsAreNull($pltIds, $whsId)
    {
        $sqlRaw = sprintf("(SELECT COUNT(1) from cartons where cartons.rfid is not null and cartons.deleted = 0) = 0");

        return \DB::table('pallet')->whereIn('pallet.plt_id', $pltIds)
            ->join('cartons', 'cartons.plt_id', '=', 'pallet.plt_id')
            ->where('pallet.whs_id', $whsId)
            ->where('pallet.deleted', 0)
            ->where('cartons.deleted', 0)
            ->whereRaw($sqlRaw)
            ->update([
                'pallet.rfid'       => null,
                'pallet.deleted'    => 1,
                'pallet.deleted_at' => time(),
                'pallet.loc_id'     => null,
                'pallet.loc_code'   => null,
                'pallet.loc_name'   => null,
            ]);
    }

    /**
     * remove Pallet When all Carton Rfids Are Null
     * @param  array $pltIds
     * @param  integer $whsId
     * @return boolean
     */
    public function removePalletWhenCtnRfidsAreCC($pltIds, $whsId, $prefix)
    {
        $sqlRaw = sprintf("(SELECT COUNT(1) from cartons where cartons.rfid not like '%s%s' and cartons.deleted = 0) = 0", $prefix, "%");

        return \DB::table('pallet')->whereIn('pallet.plt_id', $pltIds)
            ->join('cartons', 'cartons.plt_id', '=', 'pallet.plt_id')
            ->where('pallet.whs_id', $whsId)
            ->where('pallet.deleted', 0)
            ->where('cartons.deleted', 0)
            ->whereRaw($sqlRaw)
            ->update([
                'pallet.rfid'       => null,
                'pallet.deleted'    => 1,
                'pallet.deleted_at' => time(),
                'pallet.loc_id'     => null,
                'pallet.loc_code'   => null,
                'pallet.loc_name'   => null,
            ]);
    }

}
