<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use App\Api\V1\Traits\ChargeCodeSysUomService;
use Seldat\Wms2\Models\ChargeType;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class ChargeTypeModel extends AbstractModel
{
    use ChargeCodeSysUomService;

    /**
     * ChargeTypeModel constructor.
     *
     * @param ChargeType|null $model
     */
    public function __construct(ChargeType $model = null)
    {
        $this->model = ($model) ?: new ChargeType();
    }

    /**
     * @param int $char_type_id
     *
     * @return int
     */
    public function deleteChargeType($char_type_id)
    {
        return $this->model
            ->where('chg_type_id', $char_type_id)
            ->delete();
    }

    public function deleteMassChargeType(array $char_type_ids)
    {
        return $this->model
            ->whereIn('chg_type_id', $char_type_ids)
            ->delete();
    }

    /**
     * Search ChargeType
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        return $this->_search($attributes, $with, $limit);
    }
}
