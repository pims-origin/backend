<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 30-May-16
 * Time: 09:08
 */

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Response as IlluminateResponse;

class WarehouseControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $endpointWarehouse;

    protected $warehouseParams = [
        'whs_name'       => 'warehouse from functional test',
        'whs_status'     => 'IA',
        'whs_code'       => 'ABC',
        'whs_short_name' => 'phone from functional test',
        'whs_country_id' => '2',
        'whs_state_id'   => '1',
        'whs_city_name'  => 'Los Angeles',
    ];

    /**
     * WarehouseControllerTest constructor.
     */
    public function __construct()
    {
        $this->endpointWarehouse = '/v1/warehouses';
    }

    public function testList_Ok()
    {
        // make data
        factory(App\Warehouse::class, 3)->create();

        $response = $this->call('GET', $this->endpointWarehouse);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);
        $this->assertNotEmpty($responseData['data']);
        //$this->assertCount(3, $responseData['data']);
    }

    public function testShow_Ok()
    {
        // make data
        $warehouse = factory(App\Warehouse::class)->create();

        $warehouseId = $warehouse->whs_id;

        $response = $this->call('GET', $this->endpointWarehouse . "/{$warehouseId}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testStore_Ok()
    {
        $this->warehouseParams['whs_code'] = str_random('3');
        $response = $this->call('POST', $this->endpointWarehouse, $this->warehouseParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testStore_DuplicateWarehouseCode_Fail()
    {
        $warehouseCode = factory(App\Warehouse::class)->create()->whs_code;
        $warehouseDeleted = factory(App\Warehouse::class)->create()->deleted_at;

        $this->warehouseParams['whs_code'] = $warehouseCode;
        $this->warehouseParams['deleted_at'] = $warehouseDeleted;

        $this->call('POST', $this->endpointWarehouse, $this->warehouseParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_DuplicateWarehouseName_Fail()
    {
        factory(App\Warehouse::class)->create(['whs_name' => $this->warehouseParams['whs_name']])->whs_id;

        $this->call('POST', $this->endpointWarehouse, $this->warehouseParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_NoParram_Fail()
    {
        $this->call('POST', $this->endpointWarehouse);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        // make data
        $warehouse = factory(App\Warehouse::class)->create();
        $warehouseId = $warehouse->whs_id;

        $this->warehouseParams['whs_code'] = str_random('3');

        $response = $this->call('PUT', $this->endpointWarehouse . "/{$warehouseId}", $this->warehouseParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testUpdate_DuplicateWarehouseCode_Fail()
    {
        $warehouseId1 = factory(App\Warehouse::class)->create()->whs_id;
        $warehouseId2 = factory(App\Warehouse::class)->create()->whs_id;

        $this->call('PUT', $this->endpointWarehouse . "/{$warehouseId1}", $this->warehouseParams);
        $this->call('PUT', $this->endpointWarehouse . "/{$warehouseId2}", $this->warehouseParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_DuplicateWarehouseName_Fail()
    {
        $warehouse = factory(App\Warehouse::class)->create();
        $warehouse2 = factory(App\Warehouse::class)->create();

        $this->warehouseParams['whs_name'] = $warehouse->whs_name;

        $this->call('PUT', $this->endpointWarehouse . "/{$warehouse2->whs_id}", $this->warehouseParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_NotExist_Fail()
    {
        $result = $this->call('PUT', $this->endpointWarehouse . "/999999999999", $this->warehouseParams);
        $this->assertEquals(IlluminateResponse::HTTP_BAD_REQUEST, $result->status());
    }

    public function testUpdate_WithZero_Fail()
    {
        $result = $this->call('PUT', $this->endpointWarehouse . "/0", $this->warehouseParams);
        $this->assertEquals(IlluminateResponse::HTTP_BAD_REQUEST, $result->status());
    }

    public function testDelete_Ok()
    {
        // make data
        $warehouseId = factory(App\Warehouse::class)->create()->whs_id;

        $response = $this->call('DELETE', $this->endpointWarehouse . "/{$warehouseId}");
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 204
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        // Case 2: Data is empty
        $this->assertEmpty($responseData);
    }

    public function testDeleteMass_Ok()
    {
        $warehouses = factory(App\Warehouse::class, 3)->create();
        $params = [];
        foreach ($warehouses as $key => $warehouse) {
            $params['whs_id'][$key] = $warehouse->whs_id;
        }

        $response = $this->call('DELETE', $this->endpointWarehouse, $params);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 204
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        // Case 2: Data is empty
        $this->assertEmpty($responseData);
    }

    public function testDeleteMass_NoParams_Fail()
    {
        $this->call('DELETE', $this->endpointWarehouse);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDelete_NotExit_Fail()
    {
        $result = $this->call('DELETE', $this->endpointWarehouse . "/9999999999999", $this->warehouseParams);

        $this->assertEquals(IlluminateResponse::HTTP_BAD_REQUEST, $result->status());
    }
}