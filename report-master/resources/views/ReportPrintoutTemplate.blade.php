﻿﻿<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>Putaway List pdf</title>
    <style type="text/css">

        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 16px;
            margin: 0;
        }

        h2 {
            color: #282828;
            text-transform: uppercase;
            float: left;
            width: 100%;
            margin: 5px 0;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 14px;
            font-family: "Calibri";
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            border: 1px solid #ddd;
            padding: 10px;
            margin: 0 auto;

        }

        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #626262;
            font-size: 19px;
        }

        .theadClass{
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #626262;
            font-size: 19px;
        }

        .tbodyClass {
            color: #626262;
        }

        .table-style tbody tr td  {
            /*font-size: 20px;*/
            font-weight: 600;
            color: #000;
            margin-bottom: 50px;
        }
        .table-style tbody tr td{
            line-height: 38px;
        }

        .table-style2 {
            border: 1px solid #a9a9a9;
            margin: 15px 0;
            line-height: 30px;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #292929;
            text-transform: capitalize;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 1px dashed #d5d5d5;
            border-bottom: 1px solid #c0c0c0;
            color: #292929;
            line-height: 36px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }

        .table-style2 tbody tr:nth-child(2n) td {
            background-color: #f8f8f8;
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }

        footer {
            border-top: 1px solid #ddd;
            margin-top: 50px;
            padding-top: 10px;
        }

    </style>
</head>
<body>
<?php
date_default_timezone_set(config('app.timezone'));
?>
<table style="width: 100%; border-bottom:1px solid #ddd;margin-bottom: 30px" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td width="30%" style="vertical-align: middle">
            <img src="{{$url_logo}}">
        </td>
        <td class="text-right">
            <h1 style="color:{{object_get($dataConfig, 'pdf.header.color', null)}};font-size: {{object_get($dataConfig, 'pdf.header.size', null)}}; font-family: {{object_get($dataConfig, 'pdf.header.font', null)}}">{{object_get($dataConfig, 'pdf.header.title', null)}}</h1>
        </td>
    </tr>
    </tbody>
</table>

<table class="table-style2" cellpadding="0" cellspacing="0" width="100%" style="color:{{object_get($dataConfig, 'pdf.body.color', null)}};font-size: {{object_get($dataConfig, 'pdf.body.size', null)}}; font-family: {{object_get($dataConfig, 'pdf.body.font', null)}}">
    @foreach ($data as $key => $item)
        @if ($key == 0)
            <thead>
            <tr>
                @foreach ($item as $element)
                <th>{{$element}}</th>
                @endforeach
            </tr>
            </thead>
        @else
            <tbody>
                <tr>
                    @if ($item['is_avg'])
                        <td colspan="{{$item['no_col']}}" style="text-align: center">
                            {{$item['label']}}
                        </td>
                        <?php unset($item['no_col']); unset($item['label']); unset($item['is_avg']);?>
                    @endif
                    @foreach ($item as $element)
                        <td >
                            {{$element}}
                        </td>
                    @endforeach
                </tr>
            </tbody>
        @endif
    @endforeach
</table>
<footer>
    <table width="100%" style="color:{{object_get($dataConfig, 'pdf.footer.color', null)}};font-size: {{object_get($dataConfig, 'pdf.footer.size', null)}}; font-family: {{object_get($dataConfig, 'pdf.footer.font', null)}}">
        <tr>
            <td>{{object_get($dataConfig, 'pdf.footer.text', null)}}</td>
        </tr>
        <tr>
            <td>{{object_get($dataConfig, 'pdf.footer.page_label', null)}}</td>
        </tr>
        <tr>
            <td>{{object_get($dataConfig, 'pdf.footer.show_created_date', null)}}</td>
        </tr>
    </table>
</footer>
</body>
</html>
