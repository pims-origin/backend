<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SystemBug;

class SuggestionPutAwayJob extends Job
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        try {
            DB::beginTransaction();

            $cycleDetails = $this->data;

            $newSkuArray = [];
            $skuCCArray = [];

            array_map(function($value) use (&$newSkuArray, &$skuCCArray) {
                if ($value['is_new_sku'] === 1) {
                    $newSkuArray[] = $value;
                } else {
                    $skuCCArray[] = $value;
                }
            }, $cycleDetails);

            $itemIdsCC = array_pluck($this->data, 'item_id');
            $putAwayHists = DB::table('putaway_hist')->whereIn('item_id', $itemIdsCC)->get();

            foreach ($skuCCArray as $cycleDetail) {
                foreach ($putAwayHists as $putAwayHist) {
                    if (($cycleDetail['item_id'] === $putAwayHist['item_id']) && ($cycleDetail['sys_loc_id'] === $putAwayHist['loc_id'])) {
                        if (($cycleDetail['sys_loc_id'] === $cycleDetail['act_loc_id']) && ($cycleDetail['act_qty'] === 0)) {
                            DB::table('putaway_hist')->where('item_id', $cycleDetail['item_id'])->where('loc_id', $cycleDetail['sys_loc_id'])->delete();
                        }
                    }
                }
            }

            foreach ($newSkuArray as $cycleDetail) {
                $data = DB::table('putaway_hist')->where('item_id', $cycleDetail['item_id'])->where('loc_id', $cycleDetail['sys_loc_id'])->first();
                if (!empty($data)) {
                    continue;
                }

                $location = DB::table('location')
                    ->where('deleted', 0)
                    ->where('loc_whs_id', $cycleDetail['whs_id'])
                    ->where('loc_id', $cycleDetail['sys_loc_id'])
                    ->first();

                $cartonLPN = DB::table('cartons')
                    ->where('loc_id', $location['loc_id'])
                    ->where('item_id', $cycleDetail['item_id'])
                    ->where('deleted', 0)
                    ->whereIn('ctn_sts', ['AC', 'RG', 'LK'])
                    ->groupBy('loc_id', 'item_id')
                    ->selectRaw('COUNT(DISTINCT lpn_carton) as ttl_lpn')
                    ->first();

                $dataInsert = [
                    'item_id' => $cycleDetail['item_id'],
                    'loc_id' => $location['loc_id'],
                    'loc_code' => $location['loc_code'],
                    'loc_sts_code' => $location['loc_sts_code'],
                    'aisle' => $location['aisle'],
                    'row' => $location['row'],
                    'level' => $location['level'],
                    'bin' => $location['bin'],
                    'ttl_lpn' => intval($cartonLPN['ttl_lpn'])
                ];

                DB::table('putaway_hist')->insert($dataInsert);

                $this->updateStatusLocationPutAwayHistory();
            }

            DB::commit();

        } catch (\PDOException $e) {
            DB::rollback();
            SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        } catch (\Exception $e) {
            DB::rollback();
            SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    public function updateStatusLocationPutAwayHistory()
    {
        DB::statement('
            UPDATE putaway_hist
            JOIN location ON location.loc_id = putaway_hist.loc_id 
            SET putaway_hist.loc_sts_code = location.loc_sts_code 
            WHERE location.deleted = 0;
        ');
    }
}