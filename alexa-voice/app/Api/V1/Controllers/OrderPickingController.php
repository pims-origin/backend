<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OdrHdrMetaModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderServiceModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Models\WavePickDtlLocModel;
use App\Api\V1\Traits\OrderFlowControllerTrait;
use App\Api\V1\Transformers\WavePickHdrTransformer;
use App\Api\V1\Transformers\WavePickInfoTransformer;
use App\Api\V1\Validators\WavePickValidator;
use App\Api\V1\Validators\WavePickDtlLocValidator;
use App\Jobs\WavePickJob;
use Dingo\Api\Exception\UnknownVersionException;
use Illuminate\Support\Facades\DB;
use Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\InventorySummary;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class OrderPickingController extends AbstractController
{
    protected $wv_hdr;
    /**
     * OrderPickingController constructor.
     */
    public function __construct()
    {
        $this->wv_hdr = new WavePickHdrModel();

    }

    /**
     * List order picking
     *
     * @param WavePickHdrTransformer $wvHdrTransformer
     *
     * @return \Dingo\Api\Http\Response
     */
    public function odrPickingList(Request $request, WavePickHdrTransformer $wvHdrTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        $input['wv_sts'] = 'NW';
        $input['sort']['created_at'] = 'desc';
        try {
            $wvHdr = $this->wv_hdr->wvHdrFirst($input, [], array_get($input, 'limit', 20));

            return $this->response->item($wvHdr, $wvHdrTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Detail wave pick
     *
     * @param Request $request
     * @param WavePickInfoTransformer $wvInfoTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function detailWavePick(Request $request, WavePickInfoTransformer $wvInfoTransformer)
    {
        $input = $request->getQueryParams();
        $wv_id = $input['wv_id'];

        try {
            //Load wave pick header

            $dataWvDtl = $this->wv_hdr->getWvHdrInfo($wv_id);

            // Load order_num
            $odrHdrs = $this->odrHdr->getOrderHdrNumsByWvId([$wv_id])->toArray();

            $dataWvDtl->odr_num = implode(", ", array_column($odrHdrs, 'odr_num'));

            $dataWvDtl->cust_name = $dataWvDtl->cus_name;
            $dataWvDtl->wvDetail = $this->wv_dtl->loadWvDetail($wv_id);

            return $this->response->item($dataWvDtl, new $wvInfoTransformer);
        } catch (Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $wv_id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function updateWavePick($wv_id, Request $request)
    {
        set_time_limit(0);
        // get data from HTTP
        $items = $request->getParsedBody();
        $params = $request->getQueryParams();
        $isEcom = false;

        // validation
        //$wvValidator->validate($items);

        // get params to update wave pick header
        $activeLoc = array_get($params, 'activeLoc', '');
        $sv_activeLoc = array_get($params, 'sv_activeLoc', '');

        $orders = $this->odrHdr->getOrderHdrByWavePikcIds($wv_id);

        if (!empty($orders)) {
            $cancelOdrSts = Status::getByValue("Canceled", "ORDER-STATUS");
            $count = 0;
            foreach ($orders->toArray() as $order) {
                if ($order['odr_sts'] === $cancelOdrSts) {
                    $count++;
                }

                //check ecom order
                if ($order['odr_type'] === 'EC') {
                    $isEcom = true;
                }
            }
            if (count($orders->toArray()) === $count) {
                throw new Exception(Message::get('BM123', 'Canceled order', 'order picking'));
            }
        }


        //validate ecom
        if (!$isEcom) {
            $query = \DB::table('location')
                ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                ->where('location.deleted', 0)
                ->whereIn('location.loc_id', array_column($items, 'act_loc_id'))
                ->where('loc_type.loc_type_code', 'ECO');
            $chk = $query->pluck('location.loc_code');
            if (!empty($chk)) {
                return $this->response->errorBadRequest(implode(', ', $chk) . ' are Mezzanine location');
            }
        }

        // Load wvhdr info
        $wvHdrInfo = $this->wv_hdr->getWvHdrInfo($wv_id);
        //Check if queue running Picking status
        if ($wvHdrInfo->wv_sts = 'PK') {
            return $this->response
                ->noContent()
                ->setContent("{}")
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        }

        // Check complete wv_hdr
        $this->checkCompleteWvHdr($wvHdrInfo);

        // check active location
        $this->checkAndActiveLoc($items, $activeLoc);

        // Check items exist in location
        $this->checkItemInLoc($items, $sv_activeLoc);

        // Check items enough in location
        $this->checkItemEnoughInLoc($items);

        $pickingAlgorithm = $this->customerConfigModel->getPickingAlgorithm($wvHdrInfo->whs_id, $wvHdrInfo->cus_id);

        try {
            \DB::beginTransaction();

            $actLocIds = [];
            $odrDtlList = [];
            $packs = [];
            $eventDatas = [];

            // Update wavepick detail, inventory summary, carton & pallet
            foreach ((array)$items as $item) {
                $item['item_id'] = $item['itm_id'];
                /**
                 * 1. Get Cartons
                 * 1.1 - get by picking algorithm: FIFO , LIFO, FEFO
                 * - FIFO order by created_at ASC,
                 * - LIFO order by created_at DESC,
                 * - FEFO order by expired_dt ASC,
                 * 2. Update Wave dtl by sum up piece_remain in carton
                 * 2.1 update wwdtl loc
                 * 3. Update Inventory - where item_id, lot, cus_id, whs_id
                 * 4. update cartons
                 * 4. Update order dtl sts. all_qty
                 * 4.1 update order hdr
                 * 5. Insert order carton
                 * 6. Update pallet
                 * 7. update wv_hdr
                 * 8. auto pack
                 */
                $actLocs = (array)$item['act_loc'];
                $locs = [];
                $pickedTtl = 0;

                foreach ($actLocs as $actLoc) {
                    $item['act_loc_id'] = $actLoc['act_loc_id'];
                    $actLocIds[] = $actLoc['act_loc_id'];
                    $item['picked_qty'] = $actLoc['picked_qty'];
                    $data = $this->ctn->getAllCartonByWvDtl($item,
                        object_get($pickingAlgorithm, 'config_value', 'FIFO'));
                    $locs[$actLoc['act_loc_id']] = $data['cartons'];
                    $pickedTtl += $data['picked_ttl'];
                }

                $item['picked_ttl'] = $pickedTtl;

                // update wave pick detail
                $this->wv_dtl->updateWaveDtl($item['wv_dtl_id'], $pickedTtl);
                $data = $this->groupCartonsByLot($locs);
                $this->ctn->updateCartonByLocs($data['cartons']);
                $this->updateInventoryByLot($data['lots'], $item);
                $this->wavePickDtlLocModel->updateWaveDtlLoc($actLocs, $item['wv_dtl_id']);
                $data = $this->odrDtl->updateOdrDtlPickedQty($item, $data['cartons']);
                $odrDtlList[] = $data['odrDtls'];
                $eventDatas = array_merge($eventDatas, $data['eventData']);
                $packs[] = $data['packs'];

                $this->inventorySummaryModel->updateInventoryByLotBackOrder($item, $data['discrepancyTtl']);

                // Update order picked
                $this->odrHdr->refreshModel();
                $odrSts = $item['act_piece_qty'] < $item['piece_qty'] ? 'PIP' : 'PD';
                $this->odrHdr->updateWhereIn(['odr_sts' => $odrSts, 'sts' => 'u'], $data['odrIds'], 'odr_id');

            }

            $this->plt->updatePalletCtnTtl($actLocIds);
            $this->plt->updateZeroPallet($actLocIds);
            $orders = $this->odrDtl->sortOrderDtlByOrderID($odrDtlList);

            // ------------- FOR BACK ORDER ---------------------

            $this->odrHdr->createBackOrders($orders);
            $autoPack_data = $this->packHdrModel->autoPacks($packs);
            $eventDatas = array_merge($eventDatas, $autoPack_data['eventData']);
            $orderIds = $autoPack_data['packedOdrIds'];

            // Update Order Status
            if (!empty($orderIds)) {
                $this->odrHdr->refreshModel();
                $this->odrHdr->updateWhereIn([
                    'odr_sts' => Status::getByValue("Packed", "Order-Status")
                ], $orderIds, 'odr_id');
            }

            //  exit('teddd');

            // Change request: Create back-order when update order picking instead of allocated phase
            // $this->processBackOrder($items, $wv_id, $orderDtls);

            $this->wv_hdr->updateStatusWvHdr($wv_id);

            $this->logEventTracking($eventDatas, $wvHdrInfo);

            \DB::commit();

            return $this->response
                ->noContent()
                ->setContent("{}")
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (Exception $e) {
            \DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function startInsertOrderCartonQueue($whsID, $wvId)
    {
        $job = new WavePickJob($wvId);
        $this->dispatch($job);

        return ['data' => 'Successful'];
    }
}
