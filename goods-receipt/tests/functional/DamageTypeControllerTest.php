<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 7/27/2016
 * Time: 9:02 AM
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class DamageTypeControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * DamageTypeTest constructor.
     */

    public function getEndPoint()
    {
        return "/v1/damage-type";
    }

    protected $searchParams = [
        'dmg_name' => "broken1",
        'dmg_code' => "dm1",
        'dmg_id'   => 1
    ];

    protected $damageTypeParams = [
        'dmg_code' => "dTc",
        'dmg_name' => "3r0k311",
        'dmg_des'  => "43r1p7i0n"
    ];

    public function setUp()
    {
        parent::setUp();
    }

    public function testSearchDamageType_Ok()
    {
        $dmgType = factory(\Seldat\Wms2\Models\DamageType::class)->create();
        $this->searchParams['dmg_name'] = $dmgType->dmg_name;
        $this->searchParams['dmg_code'] = $dmgType->dmg_code;
        $this->searchParams['dmg_id'] = $dmgType->dmg_id;

        $this->call('GET', $this->getEndPoint(), $this->searchParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_NoParam_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testRead_Ok()
    {
        $dmgTypeId = factory(\Seldat\Wms2\Models\DamageType::class)->create()->dmg_id;

        $this->call('GET', $this->getEndPoint() . "/{$dmgTypeId}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $this->call('POST', $this->getEndPoint(), $this->damageTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    public function testStore_EmptyField_Fail()
    {
        unset($this->damageTypeParams['dmg_code']);
        $this->call('POST', $this->getEndPoint(), $this->damageTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_ExistUniqueField_Fail()
    {
        $dmgType1 = factory(\Seldat\Wms2\Models\DamageType::class)->create();
        $dmgType2 = factory(\Seldat\Wms2\Models\DamageType::class)->create();

        $this->damageTypeParams['dmg_code'] = $dmgType1->dmg_code;
        $this->damageTypeParams['dmg_name'] = $dmgType2->dmg_name;
        $this->damageTypeParams['dmg_des'] = $dmgType1->dmg_des;

        $this->call('POST', $this->getEndPoint(), $this->damageTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_DuplicateCode_Fail()
    {
        $dmgType = factory(\Seldat\Wms2\Models\DamageType::class)->create();

        $this->damageTypeParams['dmg_code'] = $dmgType->dmg_code;

        $this->call('POST', $this->getEndPoint(), $this->damageTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_DuplicateName_Fail()
    {
        $dmgType1 = factory(\Seldat\Wms2\Models\DamageType::class)->create();

        $this->damageTypeParams['dmg_code'] = 'd211127c043';
        $this->damageTypeParams['dmg_name'] = $dmgType1->dmg_name;
        $this->damageTypeParams['dmg_des'] = $dmgType1->dmg_des;

        $this->call('POST', $this->getEndPoint(), $this->damageTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_MissingField_Fail()
    {
        $damageTypeParams = [
            'dmg_code' => "dTc",
            'dmg_des'  => 'yolo'
        ];

        $this->call('POST', $this->getEndPoint(), $damageTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        $dmgTypeId = factory(\Seldat\Wms2\Models\DamageType::class)->create()->dmg_id;

        $this->call('PUT', $this->getEndPoint() . "/{$dmgTypeId}", $this->damageTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_NotExist_Fail()
    {
        $dmgTypeId = 999999;
        $this->call('PUT', $this->getEndPoint() . "/$dmgTypeId", $this->damageTypeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_ExistUniqueFields_Fail()
    {
        $dmgType1 = factory(\Seldat\Wms2\Models\DamageType::class)->create();
        $dmgType2 = factory(\Seldat\Wms2\Models\DamageType::class)->create();

        $this->damageTypeParams['dmg_code'] = $dmgType1->dmg_code;
        $this->damageTypeParams['dmg_name'] = $dmgType2->dmg_name;
        $this->damageTypeParams['dmg_des'] = $dmgType1->dmg_des;

        $dmgTypeId = factory(\Seldat\Wms2\Models\DamageType::class)->create()->dmg_id;
        $this->call('PUT', $this->getEndPoint() . "/{$dmgTypeId}", $this->damageTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_DuplicateCode_Fail()
    {
        $dmgType = factory(\Seldat\Wms2\Models\DamageType::class)->create();

        $this->damageTypeParams['dmg_code'] = $dmgType->dmg_code;
        $this->damageTypeParams['dmg_name'] = "Damage type name update";
        $this->damageTypeParams['dmg_des'] = "Damage type name duplicate";

        $dmgTypeId = factory(\Seldat\Wms2\Models\DamageType::class)->create()->dmg_id;
        $this->call('PUT', $this->getEndPoint() . "/{$dmgTypeId}", $this->damageTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_DuplicateName_Fail()
    {
        $dmgType1 = factory(\Seldat\Wms2\Models\DamageType::class)->create();

        $this->damageTypeParams['dmg_code'] = 'd211127c043';
        $this->damageTypeParams['dmg_name'] = $dmgType1->dmg_name;
        $this->damageTypeParams['dmg_des'] = $dmgType1->dmg_des;

        $dmgTypeId = factory(\Seldat\Wms2\Models\DamageType::class)->create()->dmg_id;
        $this->call('PUT', $this->getEndPoint() . "/{$dmgTypeId}", $this->damageTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_EmptyField_Fail()
    {
        $dmgTypeId = factory(\Seldat\Wms2\Models\DamageType::class)->create()->dmg_id;

        unset($this->damageTypeParams['dmg_code']);
        $this->call('PUT', $this->getEndPoint() . "/{$dmgTypeId}", $this->damageTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDelete_Ok()
    {
        $dmgTypeId = factory(\Seldat\Wms2\Models\DamageType::class)->create()->dmg_id;

        $this->call('DELETE', $this->getEndPoint() . "/{$dmgTypeId}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDelete_NotExit_Fail()
    {
        $this->call('DELETE', $this->getEndPoint() . "/9999999999999999");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }
}