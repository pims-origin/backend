<?php
/**
 * Created by PhpStorm.
 * User: duy
 * Date: 20/10/2016
 * Time: 14:10
 */

namespace App\Api\V1\Models;

use App\Api\V1\Services\CycleCountService;
use App\Utils\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Mockery\CountValidator\Exception;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CycleDis;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Wms2\UserInfo\Data;

class CycleDisModel extends AbstractModel
{
    public $cycleDtlData;
    public $cycleHdr;
    public $errors;

    const STATUS_DIS_CLONE = 'CL';
    const STATUS_DIS_RECOUNT = 'RC';
    const STATUS_DIS_DELETE = 'DL';
    const STATUS_DIS_ADJUSTED = 'AJ';
    const STATUS_DIS_ALIGN = 'AL';
    const DISCY_QTY_CLONE = 1;
    const DISCY_QTY_DELETE = -1;

    /**
     * @param $cycleDtl
     */
    protected function isNoDiscrepancy($cycleDtl)
    {
        return (
            ($cycleDtl['sys_qty'] == $cycleDtl['act_qty'] && $cycleDtl['sys_loc_id'] == $cycleDtl['act_loc_id']) ||
            ($cycleDtl['act_loc_id'] === null && $cycleDtl['act_qty'] === null) ||
            ($cycleDtl['act_loc_id'] === null && $cycleDtl['sys_qty'] == $cycleDtl['act_qty']) ||
            ($cycleDtl['act_qty'] === null && $cycleDtl['sys_loc_id'] == $cycleDtl['act_loc_id'])
        );
    }

    /**
     * @param $cyleDtl
     * @param $discrepancy
     */
    protected function getCartonToClone($cyleDtl, $discrepancy = 1)
    {
        $result = [];

        $carton = $cyleDtl['sys_qty'] ?
            $this->getCartonFromLockedCarton($cyleDtl) :
            $this->getCartonByItem($cyleDtl['item_id'], $cyleDtl['lot']);

        if ($carton->isEmpty()) {
            return $result;
        }

        $result[] = [
            'ctn_id'       => $carton[0]->ctn_id,
            'dicpy_qty'    => $discrepancy,
            'dicpy_sts'    => self::STATUS_DIS_CLONE,
            'cycle_dtl_id' => $cyleDtl['cycle_dtl_id'],
            'cycle_hdr_id' => $cyleDtl['cycle_hdr_id'],
            'sts'          => 'I'
        ];

        return $result;
    }

    /**
     * @param $cyleDtl
     * @param $discrepancy
     */
    protected function getCartonToRemove($cyleDtl, $discrepancy = 1)
    {
        $result = [];

        $cartons = $this->getCartonFromLockedCarton($cyleDtl, abs($discrepancy));

        if ($cartons->isEmpty()) {
            return $result;
        }

        foreach ($cartons as $carton) {
            $result[] = [
                'ctn_id'       => $carton->ctn_id,
                'dicpy_qty'    => self::DISCY_QTY_DELETE,
                'dicpy_sts'    => self::STATUS_DIS_DELETE,
                'cycle_dtl_id' => $cyleDtl['cycle_dtl_id'],
                'cycle_hdr_id' => $cyleDtl['cycle_hdr_id'],
                'sts'          => 'I'
            ];
        }

        return $result;
    }

    /**
     * @param $cycleDtls
     */
    public function filterDiscrepancyCycleDtl($cycleDtls)
    {
        $discrepancy = $noDiscrepancy = $processed = [];
        $statusProcess = [
            CycleDtlModel::STATUS_CYCLEDTL_NEW,
            CycleDtlModel::STATUS_CYCLEDTL_RECOUNT
        ];

        foreach ($cycleDtls as $cycleDtl) {
            if (in_array($cycleDtl['cycle_dtl_sts'], $statusProcess)) {
                if ($this->isNoDiscrepancy($cycleDtl)) {
                    $noDiscrepancy[] = $cycleDtl;
                } else {
                    $discrepancy[] = $cycleDtl;
                }
            } else {
                $processed[] = $cycleDtl;
            }
        }

        return [
            'processed'     => $processed,
            'discrepancy'   => $discrepancy,
            'noDiscrepancy' => $noDiscrepancy
        ];
    }

    /**
     * @param     $cycleDtl
     * @param int $limit
     */
    public function getCartonFromLockedCarton($cycleDtl, $limit = 1)
    {
        $cartons = Carton::select(
            'ctn_id'
        )
            ->where('whs_id', $cycleDtl['whs_id'])
            ->where('cus_id', $cycleDtl['cus_id'])
            ->where('item_id', $cycleDtl['item_id'])
            ->where('lot', $cycleDtl['lot'])
            ->where('loc_id', $cycleDtl['sys_loc_id'])
            ->where('piece_remain', $cycleDtl['remain'])
            ->where('ctn_pack_size', $cycleDtl['pack'])
            ->where('ctn_sts', '<>', LockModel::STATUS_INACTIVE);

        if (!empty($cycleDtl['plt_rfid'])) {
            $cartons = $cartons->where('lpn_carton', trim($cycleDtl['plt_rfid']));
        }

        $cartons = $cartons->limit($limit)->get();

        return $cartons;
    }

    /**
     * @param $cycleDtl
     */
    public function getCartonByItem($itemId, $lot, $limit = 1)
    {
        $carton = Carton::select('ctn_id')
            ->where('item_id', $itemId)
            ->where('lot', $lot)
            ->where('whs_id', Data::getCurrentWhsId())
            ->limit($limit)
            ->get();

        return $carton;
    }

    /**
     * @param $data
     */
    public function insertDiscrepancy($data)
    {
        if (!$data) {
            return false;
        }

        $rows = array_chunk($data, 200);
        foreach($rows as $row) {
            CycleDis::insert($row);
        }

        return true;
    }

    /**
     * @param $cycleHdr
     */
    public function processDiscrepancy($cycleHdr, $cycleDtls)
    {
        $cycleDtlData = $this->filterDiscrepancyCycleDtl($cycleDtls);
        $cycleDtlIds = array_column($cycleDtls, 'cycle_dtl_id');

        $discrepancies = $this->getDiscrepancyData($cycleDtlData['discrepancy']);

        try {
            DB::beginTransaction();

            $this->insertDiscrepancy($discrepancies);

            CycleDtlModel::updateCycleDtlStatus($cycleDtlIds, CycleDtlModel::STATUS_CYCLEDTL_OPEN);

            CycleHdrModel::updateCycleHdrStatus($cycleHdr->cycle_hdr_id, CycleHdrModel::STATUS_CYCLE_CYCLED);

            CycleDtlModel::setDefaultActValueCycleDtl($cycleHdr->cycle_hdr_id);

            DB::commit();

        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
            DB::rollback();

            return false;
        }

        return true;
    }

    /**
     * @param $cycleDtls
     *
     * @return array|bool
     */
    public function getDiscrepancyData($cycleDtls)
    {
        $result = [];

        if (!$cycleDtls) {
            return false;
        }

        foreach ($cycleDtls as $cycleDtl) {

            $discrepancy = $this->getDiscrepancyCarton($cycleDtl);
            $result = array_merge($result, $discrepancy);
        }

        $result = $this->setDefaultValueDataDB($result);

        return $result;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function setDefaultValueDataDB($data)
    {
        $result = [];

        $usrId = CycleCountService::getUserId();
        $time = time();
        $deleted_at = SoftDeletes::getDefaultDatetimeDeletedAt();

        foreach ($data as $discrepancy) {
            $discrepancy['created_by'] = $discrepancy['updated_by'] = $usrId;
            $discrepancy['created_at'] = $discrepancy['updated_at'] = $time;
            $discrepancy['deleted_at'] = $deleted_at;
            $discrepancy['deleted'] = 0;
            $result[] = $discrepancy;
        }

        return $result;
    }

    /**
     * @param $cycleDisID
     *
     * @return array
     */
    public static function getCycleDtl($cycleDisID)
    {
        if (!$cycleDisID) {
            return [];
        }

        $result = CycleDis::with('cycleDtl')
            ->select('*')
            ->where('dicpy_ctn_id', $cycleDisID)
            ->first();

        return $result;
    }

    public static function getPalletByLocID($locID, $cusId, $whsId)
    {
        $pallet = Pallet::select([
            'plt_id',
            'ctn_ttl',
            'rfid',
        ])
            ->where('loc_id', $locID)
            ->where('cus_id', $cusId)
            ->where('whs_id', $whsId)
            ->first();

        return $pallet;
    }


}