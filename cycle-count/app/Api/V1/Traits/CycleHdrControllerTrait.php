<?php

namespace App\Api\V1\Traits;

use App\Api\V1\Models\CartonModel;
use App\Jobs\UpdateInventoryJob;
use Seldat\Wms2\Models\CycleCountNotification;
use App\Api\V1\Models\CycleDisModel;
use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\CycleProcess;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LockModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Services\CycleCountService;
use App\Api\V1\Validators\RecountValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\Inventories;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\SystemBug;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Jobs\AutoUpdateDailyInventoryAndPalletReport;
use App\Jobs\CycleCountIMSJob;

trait CycleHdrControllerTrait
{
    /*
     * @var CartonModel
     */
    protected $cartonModel;

    /*
     * @var PalletModel
     */
    protected $palletModel;


    /**
     * @param Request $request
     * @param RecountValidator $validator
     *
     * @return mixed
     */
    public function recount(Request $request, RecountValidator $validator)
    {
        $input = $request->getParsedBody();
        $validator->validate($input);

        $model = new CycleProcess();
        $result = $model->recount($input);

        if ($result) {
            return $this->response->accepted(null, ['message' => 'Recount successfully', 'data' => 'success']);
        } else {
            return $this->response->error(implode($model->errors), IlluminateResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @param RecountValidator $validator
     *
     * @return array
     */
    public function approve(Request $request, RecountValidator $validator)
    {
        $input = $request->getParsedBody();
        $validator->validate($input);

        if (empty($input['cycle_dtl_ids'])) {
            return $this->response->errorBadRequest('cycle_dtl_ids can\'t empty');
        }
        $cycleHdrId = $input['cycle_hdr_id'];
        $cycleDtlIds = $arrCycleDtlId = $input['cycle_dtl_ids'];

        if (!array_filter($cycleDtlIds, 'is_int')) {
            return $this->response->errorBadRequest('cycle_dtl_id must be a integer');
        }

        $userId = CycleCountService::getUserId();

        //get cycleHdrObj
        $cycleHdrObj = $this->model->byId($cycleHdrId);

        if ($cycleHdrObj->assignee_id == $userId) {
            return $this->response->errorBadRequest('Permission denied!');
        }

        $cycleProc = new CycleProcess();
        $cycleDtlArr = $cycleProc->getCycleDtlDataProcess($cycleHdrId, $arrCycleDtlId);

        if (!$cycleDtlArr) {
            return $this->response->errorBadRequest('Data not found.');
        }

        $lackCartons = $oddCartons = [];

        $countBy = $cycleHdrObj->count_by;
        $isEach = strtoupper($countBy) === 'EACH' ? true : false; //current only use 2 types
        try {
            DB::beginTransaction();
            $updatedInv = []; //contain list cycle_dtl_id
            $locIds = [];
            $itemIds = [];
            $lots = [];

            $ctnIds=[];
            foreach ($cycleDtlArr as $rw) {
                $ctnIds[]=$rw['ctn_id'];
            }
            $dmgQTYs=collect();
            if(!empty($ctnIds)) {
                $dmgQTYs = Carton::whereIn('ctn_id', $ctnIds)
                ->where('is_damaged', 1)
                ->select(['ctn_id','piece_remain'])
                ->get()
                ->keyBy('ctn_id');
            }

            foreach ($cycleDtlArr as $rw) {
                $locIds[] = $rw['sys_loc_id'];
                $itemIds[] = $rw['item_id'];
                $lots[] = $rw['lot'];
                if (!$isEach) {
                    $ttlDiscrepancy = $disc = $rw['act_qty'] - $rw['sys_qty'];
                } else {
                    $pack = $rw['remain'] > 0 ? $rw['remain'] : $rw['pack'];
                    $ttlDiscrepancy = $disc = ($rw['act_qty'] - $rw['sys_qty']) / $pack;
                }
                if ($disc < 0) {
                    $oddCartons[] = $rw;
                }
                if ($disc > 0) {
                    if($rw['dicpy_qty'] == null){
                        $rw['dicpy_qty'] = $disc;
                    }
                    $lackCartons[] = $rw;
                }

                //process count_by each
                if (!$isEach) {
                    // $ttlDiscrepancy = $disc * $rw['remain'];
                    $pack = $rw['remain'] ? $rw['remain'] : $rw['pack'];
                    $ttlDiscrepancy = $disc * $pack;
                }

                if ($ttlDiscrepancy != 0 && !array_key_exists($rw['cycle_dtl_id'], $updatedInv)) {
                    //$cycleProc->updateInventory($rw, $ttlDiscrepancy);
                    $updatedInv[$rw['cycle_dtl_id']] = ['data' => $rw, 'disc' => $ttlDiscrepancy, 'dmg_qty' => 0];
                }

                //process dmg qty
                /*$dmgQTY = (int)Carton::where('ctn_id', $rw['ctn_id'])
                    ->where('is_damaged', 1)
                    ->value('piece_remain');
                if ($dmgQTY && $ttlDiscrepancy < 0) {
                    $updatedInv[$rw['cycle_dtl_id']]['disc'] += $dmgQTY;
                    $updatedInv[$rw['cycle_dtl_id']]['dmg_qty'] += $dmgQTY;
                }*/
                if($dmgQTYs->contains($rw['ctn_id']) && $ttlDiscrepancy < 0) {
                    $dmgQTY=$dmgQTYs[$rw['ctn_id']]->piece_remain??0;
                    $updatedInv[$rw['cycle_dtl_id']]['disc'] += $dmgQTY;
                    $updatedInv[$rw['cycle_dtl_id']]['dmg_qty'] += $dmgQTY;
                }
            }

            //sort
            arsort($updatedInv);

            foreach ($updatedInv as $inv) {
                $cycleProc->updateInventory($inv['data'], $inv['disc'], $inv['dmg_qty']);
            }
            unset($updatedInv); //free memory

            if ($lackCartons) {
                //clone cartons, discrepancy
                $errorMessage = $cycleProc->cloneCtnDiscrepancy($lackCartons, $userId, $isEach);
                if (! is_null($errorMessage)) {
                    return $this->response()->errorBadRequest($errorMessage);
                }
            }

            if ($oddCartons) {
                $cycleProc->delCtnDiscrepancy($oddCartons, $userId, $isEach);
                /*
                 * 1. Update pallet, ctn_ttl count from cartons where locids
                 * 2. update zero when ctl = 0
                 */

            }

            //update sts cycle_hdr, cycle_dtl
            $arrCycleDtlId = array_unique($arrCycleDtlId);
            foreach (array_chunk($arrCycleDtlId, 200) as $chunkArrCycleDtlId) {
                CycleDtl::where('cycle_hdr_id', $cycleHdrId)
                    ->whereIn('cycle_dtl_id', $chunkArrCycleDtlId)
                    ->update([
                        'cycle_dtl_sts' => CycleDtlModel::STATUS_CYCLEDTL_ACCEPTED,
                        'updated_by' => $userId
                    ]);
            }
            $arr_locid = array_unique($locIds);

            //update sts CCN
            foreach (array_chunk($arr_locid, 200) as $chunkArrLocId) {
                CycleCountNotification::whereIn('loc_id', $chunkArrLocId)
                    ->where('cc_ntf_sts', '<>', 'CE')
                    ->update([
                        'cc_ntf_sts' => "CP",
                        'updated_by' => $userId
                    ]);
            }

            // if all sku is approved, update CC status to Completed
            $unLockAll = $this->model->updateCycelHdrAfterProcessed($cycleHdrId);

            //unclock carton / location

            $whsId = $cycleHdrObj->whs_id;
            // $itemIds = $cycleProc->getUnclockItemIds($cycleHdrId);
            $sysLocIds = array_unique(array_column($cycleDtlArr, 'sys_loc_id'));
            $sysLocIdUnLock = $this->model->getSysLocIdCanUnLock($whsId, $cycleHdrId, $sysLocIds);
            $sysLocIdUnLock = array_unique($sysLocIdUnLock);

            $locIds = array_unique($locIds);

            $this->palletModel->updatePalletCtnTtl($locIds);

            $this->palletModel->updateZeroPallet($locIds);

            //remove dynmic zone
            $this->palletModel->removeDynmicZone($whsId);

            if ($sysLocIdUnLock) {
                LockModel::unLockLocations($sysLocIdUnLock);
            }

            // if CC status updated to Completed -> unlock All location of sku in CC
            if ($unLockAll) {
                (new LockModel())->unClockAllLoc($cycleHdrObj);
            }

            //update lock_qty in invt_smr
            $allCycleDtlArr = $cycleHdrObj->cycleDtl()->get()->toArray();
            $allItemIds = array_column($allCycleDtlArr, 'item_id');
            $allLocIds = array_column($allCycleDtlArr, 'sys_loc_id');
            LockModel::updateLockQtyInInventorySummary($whsId, $allItemIds, $allLocIds);
            //event tracking
            $data = CycleHdrModel::getCycleHdr($cycleHdrId);
            $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingTransNum(
                $cycleHdrId,
                EventTrackingModel::CYCLE_COMPLETED
            );
            $this->eventTrackingModel->create([
                'whs_id'    => $data['whs_id'],
                'cus_id'    => ucfirst($data['cycle_type']) == 'CS' ? $data['cycle_detail'] : null,
                'owner'     => $cycleNumAndSeq['cycle_num'],
                'evt_code'  => EventTrackingModel::CYCLE_COMPLETED,
                'trans_num' => $cycleNumAndSeq['trans_num'],
                'info'      => $cycleNumAndSeq['trans_num'] . ' ' .
                    EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_COMPLETED]
            ]);
            //(new CycleDtlModel())->updateCartonAcByCycleHdr($cycleHdrId);
            // if($isEach && !empty($lackCartons) || $isEach && !empty($oddCartons)){
            if(!empty($lackCartons) || !empty($oddCartons)){
                $cycleProc->updateInventoryEach($whsId, array_unique($itemIds), array_unique($lots));
            }
//            $this->correctInventory();

            DB::commit();
/*
            if(!empty($lackCartons) || !empty($oddCartons)){
                $cycleDetailFirst = array_first($cycleDtlArr);
                $cus_id = $cycleDetailFirst['cus_id'] ?? null;
                if($cus_id){
                    dispatch(new AutoUpdateDailyInventoryAndPalletReport($data['whs_id'], $cus_id, strtotime(date('Y-m-d'))));
                }
            }
*/
            dispatch(new UpdateInventoryJob(['whs_id' => $whsId]));
            dispatch(new CycleCountIMSJob($whsId, $cycleHdrId, $cycleDtlIds));

            return ["data" => "Accept successful"];
        } catch (\PDOException $e) {
            DB::rollBack();
            SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function correctInventory()
    {
        DB::statement('UPDATE inventory i
            LEFT JOIN (
                SELECT
                    t.whs_id,
                    t.item_id,
                    SUM(t.total_qty) AS total_qty,
                    SUM(t.in_pick_qty) AS in_pick_qty
                FROM
                    (
                        (
                            SELECT
                                d.whs_id,
                                d.item_id,
                                0 AS total_qty,
                                SUM(
                                    IF (
                                        d.alloc_qty < d.picked_qty,
                                        0,
                                        d.alloc_qty - d.picked_qty
                                    )
                                ) AS in_pick_qty
                            FROM
                                odr_dtl d
                            JOIN odr_hdr h ON h.odr_id = d.odr_id
                            WHERE
                                h.odr_sts IN ("AL", "PK") AND d.whs_id = 1
                                AND d.alloc_qty > 0
                                AND d.deleted = 0
                            GROUP BY
                                d.whs_id,
                                d.item_id
                        )
                        UNION
                        (
                            SELECT
                                c.whs_id,
                                c.item_id,
                                SUM(c.piece_remain) AS total_qty,
                                SUM(
                                    IF (
                                        c.ctn_sts = "PD",
                                        c.piece_remain,
                                        0
                                    )
                                ) AS in_pick_qty
                            FROM
                                cartons c
                            WHERE
                                c.ctn_sts IN ("AC", "LK", "PD", "TF") and c.whs_id = 1
                                AND c.deleted = 0
                            GROUP BY
                                c.whs_id,
                                c.item_id
                        )
                    ) AS t
                GROUP BY
                    t.whs_id,
                    t.item_id
            ) AS d ON d.whs_id = i.whs_id
            AND d.item_id = i.item_id
        SET i.in_hand_qty = GREATEST(
            IFNULL(
                d.total_qty - d.in_pick_qty,
                0
            ),
            0
        ),
        i.in_pick_qty = IFNULL(d.in_pick_qty, 0) -
        IF (
            d.total_qty - d.in_pick_qty < 0,
            ABS(d.total_qty - d.in_pick_qty),
            0
        )
        WHERE
            i.type = "I" 
            AND i.whs_id = 1
            AND (
                (
                    d.total_qty IS NULL
                    AND i.total_qty > 0
                )
            OR i.total_qty != d.total_qty
            OR i.in_pick_qty != IFNULL(d.in_pick_qty, 0) -
            IF (
                d.total_qty - d.in_pick_qty < 0,
                ABS(d.total_qty - d.in_pick_qty),
                0
            )
        );');
    }

    public function checkOdrSts(Request $request)
    {
        $input = $request->getParsedBody();

        if (!isset($input['odr_ids'])) {
            throw new HttpException(404, "Order can't empty");
        }

        $orderSts = [
            'NW'  => 'New',
            'AL'  => 'Allocated',
            'PK'  => 'Picking',
            'PD'  => 'Picked',
            'PN'  => 'Packing',
            'PA'  => 'Packed',
            'ST'  => 'Staging',
            'SH'  => 'Shipped',
            'HO'  => 'Hold',
            'CC'  => 'Canceled',

            'PAL' => 'Allocated',
            'PPK' => 'Picking',
            'PIP' => 'Picked',
            'PPA' => 'Packing',
            'PAP' => 'Packed',
            'PSH' => 'Staging',
            'PRC' => 'Shipped',
        ];

        $rs = OrderHdr::select([
            'odr_id',
            'odr_sts'
        ])
            ->whereIn('odr_id', (array)$input['odr_ids'])
            ->get()->toArray();

        foreach ($rs as $idx => $rw) {
            $rs[$idx]['odr_sts_name'] = $orderSts[$rw['odr_sts']];
        }

        return ['data' => $rs];
    }

}