<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/8/2016
 * Time: 9:26 AM
 */

namespace tests\codeception\backend\api;

use tests\codeception\backend\ApiTester;


class PermissionCest
{
    public function _before(ApiTester $I)
    {
        // Authority - PARENT - ROLE
        $name1 = "authorityTest1";
        $I->haveInDatabase('authority', [
            'name'        => $name1,
            'type'        => '1',
            'description' => 'unit test 1'
        ]);
        //$this->_name1 = $name1;
        $this->_name1 = $I->grabFromDatabase('authority', 'name', ['name' => $name1]);

        // Authority - PARENT - Permission
        $name2 = 'authorityTest2';
        $I->haveInDatabase('authority', [
            'name'        => $name2,
            'type'        => '2',
            'description' => 'unit test 2'
        ]);
        $this->_name2 = $name2;

        $name3 = 'authorityTest3';
        $I->haveInDatabase('authority', [
            'name'        => $name3,
            'type'        => '2',
            'description' => 'unit test 3'
        ]);
        $this->_name3 = $name3;

        $name4 = 'authorityTest4';
        $I->haveInDatabase('authority', [
            'name'        => $name4,
            'type'        => '2',
            'description' => 'unit test 4'
        ]);
        $this->_name4 = $name4;

        // Authority - CHILD
        $I->haveInDatabase('authority_child', [
            'parent' => $name1,
            'child'  => 'authorityTest2'
        ]);

        $I->haveInDatabase('authority_child', [
            'parent' => 'Admin',
            'child'  => 'authorityTest1'
        ]);

        $I->haveInDatabase('authority_child', [
            'parent' => 'Admin',
            'child'  => 'authorityTest3'
        ]);

        $I->haveInDatabase('authority_child', [
            'parent' => 'Admin',
            'child'  => 'authorityTest4'
        ]);

        // User
        $this->_user_id = $I->haveInDatabase('users', [
            'first_name' => 'fi12T',
            'last_name'  => 'l2sT',
            'email'      => 'test@codecept.com',
            'password'   => '$2y$13$dWhcDzI0Ry.D4sCxME0L3evfLqsjlpt.Ip0MHKoVP.qwECqrExqOa', //Seldat123
            'status'     => 'AC',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->_user_id2 = $I->haveInDatabase('users', [
            'first_name' => 'fi12T2',
            'last_name'  => 'l2sT2',
            'email'      => 'test2@codecept.com',
            'password'   => '$2y$13$dWhcDzI0Ry.D4sCxME0L3evfLqsjlpt.Ip0MHKoVP.qwECqrExqOa', //Seldat123
            'status'     => 'AC',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        // User Role
        $I->haveInDatabase('user_role', [
            'item_name' => 'Admin',
            'user_id'   => $this->_user_id,
        ]);


    }

    public function _after(ApiTester $I)
    {
        //I->getDbh()->rollBack();
        //$I->deleteFromDatabase('users', ['user_id' => $this->_user_id]);
    }

    /**
     * Test list - ok
     *
     * @param ApiTester $I
     *
     * @return string
     */
    public function test_list_ok(ApiTester $I)
    {
        $I->sendGET("/permissions");
        $I->seeResponseCodeIs(200);
    }

    public function test_byUser_ok(ApiTester $I)
    {
        $I->sendGET("/users/" . $this->_user_id . "/permissions");
        $I->seeResponseCodeIs(200);
    }

    public function test_byUser_fail(ApiTester $I)
    {
        $I->sendGET("/users/999999999999999/permissions");
        $I->seeResponseCodeIs(400);
    }

    public function test_group_ok(ApiTester $I)
    {
        $I->sendGET("/users/" . $this->_user_id . "/groups");
        $I->seeResponseCodeIs(200);
    }

    public function test_create_ok(ApiTester $I)
    {
        $I->sendPOST("/permissions", [
            'name'        => 'zhihihahahohoz',
            'code'        => 'oau',
            'description' => 'hoho haha hihi',
        ]);
        $I->seeResponseCodeIs(201);
        $I->deleteFromDatabase('authority', ['name' => 'zhihihahahohoz']);
    }

    public function test_create_fail(ApiTester $I)
    {
        $I->sendPOST("/permissions", [
            'name'        =>
                'zhihihahahohozzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
            'code'        => 'oau',
            'description' => 'hoho haha hihi',
        ]);
        $I->seeResponseCodeIs(400);
        $I->deleteFromDatabase('authority', ['code' => 'oau']);
    }
}