<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class AsnDtlModel extends AbstractModel
{

    protected $asnHdr;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnDtl();
        $this->asnHdr = new AsnHdr();
    }


    /** get container by asn header id
     *
     * @param $asn_hdr_id
     */
    public function getContainersByAsn($asnHrdId)
    {
        $query = $this->make(['container']);
        $query->where('asn_hdr_id', $asnHrdId);
        $query->groupBy('ctnr_id');
        $asnDtls = $query->get();

        $containers = [];
        if (!$asnDtls->isEmpty()) {
            foreach ($asnDtls as $asnDtl) {
                $containers[] = (!empty($asnDtl->container))?$asnDtl->container:'';
            }
        }

        return collect($containers);
    }

    public function getAsnDtlIdItemIdByHeaderAndContainer($asnHrdId, $ctnrId)
    {
        $collections = $this->model
            ->where('asn_hdr_id', $asnHrdId)
            ->where('ctnr_id', $ctnrId)
            ->select('asn_dtl_id', 'item_id')
            ->get();

        $result = [];
        if (!$collections->isEmpty()) {
            foreach ($collections as $row) {
                $result[$row->asn_dtl_id] = $row->item_id;
            }
        }

        return $result;
    }

    /**
     * @param $asnHrdId
     * @param $asnDtlEptDate
     * @param $mode
     * @param $oldCtnrId
     * @param $ctnrId
     *
     * @return bool
     */
    public function getAsnDtlByCTNRAndASNEptDate($asnHrdId, $asnDtlEptDate, $mode, $oldCtnrId, $ctnrId)
    {
        $query = $this->model
            ->where('asn_hdr_id', $asnHrdId)
            ->where('asn_dtl_ept_dt', strtotime($asnDtlEptDate));
        //if ADD mode
        if(!$mode){
            $result = $query->count();
        } else {
            //if update mode
            $result = $query->where('ctnr_id', '<>', $ctnrId)
                            ->count();
        }
        return ($result > 0) ? false : true;

    }

    public function getTotalItem($item_id)
    {
        return $this->model->where('item_id', $item_id)->sum(DB::raw('asn_dtl_pack * asn_dtl_ctn_ttl'));
    }

    public function getTotalInfoOfAsn($pAsnHrdId)
    {
        $asnHrdId=(array)$pAsnHrdId;
        $count= $this->model->select([
            'asn_hdr_id',
            DB::raw('count(distinct asn_dtl.asn_dtl_id) as itm_ttl'),
            DB::raw('count(distinct asn_dtl.ctnr_id) as ctnr_ttl'),
            DB::raw('COALESCE(sum(asn_dtl.asn_dtl_ctn_ttl),0) as ctn_ttl'),
            DB::raw('count(IF(gr_dtl_disc<>0 AND gr_dtl_disc IS NOT NULL,1,NULL)) AS is_discrepancy'),
            DB::raw('count(IF(gr_dtl_is_dmg>0,1,NULL)) AS is_damaged'),
        ])
        ->leftJoin('gr_dtl','gr_dtl.asn_dtl_id','=','asn_dtl.asn_dtl_id')
        ->whereIn('asn_dtl.asn_hdr_id', $asnHrdId)
        ->groupBy('asn_dtl.asn_hdr_id')
        ->get();
        $data=[];
        foreach ($count as $value){
            $data[]=[
                'asn_hdr_id'=>$value['asn_hdr_id'],
                'itm_ttl'=>$value['itm_ttl'],
                'ctnr_ttl'=>$value['ctnr_ttl'],
                'ctn_ttl'=>$value['ctn_ttl'],
                'is_discrepancy'=>$value['is_discrepancy']>0?1:0,
                'is_damaged'=>$value['is_damaged']>0?1:0,
            ];
        }
        return $data;
    }

    public function getTotalInfoFromAsnDtlByHeader($asnHrdId)
    {
        $row = $this->model
            ->select(DB::raw('count(distinct asn_dtl_id) as itm_ttl,
                                          count(distinct ctnr_id) as ctnr_ttl,
                                          sum(asn_dtl_ctn_ttl) as ctn_ttl'))
            ->where('asn_hdr_id', $asnHrdId)
            ->take(1)
            ->get()
            ->first();

        if (!$row) {
            $row = [
                'itm_ttl'  => 0,
                'ctnr_ttl' => 0,
                'ctn_ttl'  => 0,
            ];
        } else {
            $row = $row->toArray();
        }

        return $row;
    }

    public function countContainerOfAsn($asnHrdId)
    {
        return $this->model->where('asn_hdr_id', $asnHrdId)
            ->select('ctnr_id')
            ->groupBy('ctnr_id')
            ->get()
            ->count();
    }

    public function getAsnIsDiscrepancy($asnHrdId)
    {

        $query = $this->make(['grDtl']);

        $query->whereHas('grDtl', function ($query) {
            $query->where('gr_dtl_disc', '<>', 0)->where('gr_dtl_disc', '<>', null);
        });

        return $query->where('asn_hdr_id', $asnHrdId)->count();

    }

    public function getAsnIsDamaged($asnHrdId)
    {

        $query = $this->make(['grDtl']);

        $query->whereHas('grDtl', function ($query) {
            $query->where('gr_dtl_is_dmg', '>', 0);
        });

        return $query->where('asn_hdr_id', $asnHrdId)->count();

    }

    public function search($attributes, array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        // search asn po
        if (isset($attributes['asn_dtl_po'])) {
            $query->where('asn_dtl_po', 'like', "%" . SelStr::escapeLike($attributes['asn_dtl_po']) . "%");
        }
        $asnHdr = $this->asnHdr;
        $query->whereHas('asnHdr', function ($query) use ($attributes, $asnHdr) {
            // search whs id
            if (isset($attributes['whs_id'])) {
                $query->where('whs_id', (int)$attributes['whs_id']);
            }

            // search asn number
            if (isset($attributes['asn_hdr_num'])) {
                $query->where('asn_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['asn_hdr_num']) . "%");
            }

            // search customer
            if (isset($attributes['cus_id'])) {
                $query->where('cus_id', (int)$attributes['cus_id']);
            }

            // search asn status
            if (isset($attributes['asn_sts'])) {
                $query->where('asn_sts', $attributes['asn_sts']);
            }

            // Search by expected date
            if (isset($attributes['asn_hdr_ept_dt'])) {
                $query->where(
                    DB::raw('DATE_FORMAT(FROM_UNIXTIME(asn_hdr_ept_dt), "%m/%d/%Y")'),
                    $attributes['asn_hdr_ept_dt']
                );
            }

            $asnHdr->filterData($query, true);

        });

        // search container
        $query->whereHas('container', function ($query) use ($attributes) {
            if (isset($attributes['ctnr_num'])) {
                $query->where('ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
            }
        });

        // search item
        $query->whereHas('item', function ($query) use ($attributes) {
            if (isset($attributes['dtl_sku'])) {
                $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['dtl_sku']) . "%");
            }
        });

        // Get X-doc
        if (!empty($attributes['xdoc']) && !empty($attributes['xdoc']) === true) {
            $query->select(DB::raw('concat(asn_hdr_id, "-", ctnr_id) as asn_ctnr, sum(asn_dtl_crs_doc) as xdoc'));
            $query->groupBy(['asn_hdr_id', 'ctnr_id']);
        } else {
            $query->groupBy('asn_hdr_id');
        }
        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }

    /**
     * @param $asnDtlIds
     * @param array $attributes
     *
     * @return mixed
     */
    public function updateStatusIn($asnDtlIds, $attributes = [])
    {
        return $this->model->whereIn('asn_dtl_id', $asnDtlIds)
            ->update($attributes);
    }

    /**
     * @param $asnId
     *
     * @return mixed
     */
    public function getUOMByAsnId($asnId)
    {
        $sysUom = $this->model->select('uom_id', 'uom_code', 'uom_code')
            ->where('uom_id', $asnId)
            ->first();

        return $sysUom;
    }

    /**
     * @param $asnID
     * @param $vtlCtnrId
     *
     * @return mixed
     */
    public function listASNDtlAndCartonRFID($asnID, $vtlCtnrId, $search)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('asn_dtl')//array
        //$query = $this->getModel()   //object
        ->select([
                'asn_dtl.asn_hdr_id',
                'asn_dtl.asn_dtl_id',
                'asn_dtl.item_id',
                'asn_dtl.asn_dtl_sku',
                'asn_dtl.asn_dtl_size',
                'asn_dtl.asn_dtl_color',
                'asn_dtl.asn_dtl_lot',
                'asn_dtl.asn_dtl_pack',
                'asn_dtl.asn_dtl_ctn_ttl',
                'asn_dtl.asn_dtl_cus_upc',

            ]);

        $query->Join('vtl_ctn as ctn', 'ctn.asn_dtl_id', '=', 'asn_dtl.asn_dtl_id')
            ->addSelect([
                'ctn.plt_rfid',
                'ctn.vtl_ctn_id',
                'ctn.ctn_rfid',
                'ctn.ctnr_id',
                DB::raw("DATE_FORMAT(FROM_UNIXTIME(ctn.created_at), '%m/%d/%Y %h:%i:%s') AS created_at"),
                DB::raw("DATE_FORMAT(FROM_UNIXTIME(ctn.updated_at), '%m/%d/%Y %h:%i:%s') AS updated_at"),
                'ctn.vtl_ctn_sts',
                'ctn.scanned',
            ]);
        $query->leftJoin('users', 'users.user_id', '=', 'ctn.updated_by')
            ->addSelect(
                DB::RAW("CONCAT(users.first_name,' ',users.last_name) as updated_by")
            );
        $query->leftJoin('users AS u2', 'u2.user_id', '=', 'ctn.created_by')
            ->addSelect(
                DB::RAW("CONCAT(u2.first_name,' ',u2.last_name) as created_by")
            );

        $query->where('asn_dtl.asn_hdr_id', $asnID);
        $query->where('ctn.vtl_ctn_sts', '!=', 'AD');

        /**
         * WMS2-3738
         * 10-04-2017
         */
        $statusName = $this->buildCaseSql('ASN-DETAIL-CARTON-STATUS', 'sts_name');
        if ($statusName) {
            $query->addSelect(
                DB::RAW($statusName)
            );
        }
        $query->where('ctn.ctnr_id', $vtlCtnrId);
        /**
         * Allow search by rfid
         */
        if (isset($search['ctn_rfid']) && !empty($search['ctn_rfid'])) {
            $query->where('ctn_rfid', 'LIKE', "%{$search['ctn_rfid']}%");
        }

        /**
         * Allow search by sku
         */
        if (isset($search['sku']) && !empty($search['sku'])) {
            $query->where('asn_dtl_sku', 'LIKE', "%{$search['sku']}%");
        }
        /**
         * Check throught gateway condition
         */
        if ($search['is_gateway'] == 'Y') {
            $query->whereNotNull("plt_rfid");
        } else if ($search['is_gateway'] == 'N') {
            $query->whereNull("plt_rfid");
        }

        /**
         * WMS2-3867
         */
        if ($search['scanned'] == 'Y') {
            $query->where('scanned', 1);
        } else if ($search['scanned'] == 'N') {
            $query->where('scanned', 0);
        } else {
            //nothing to do
        }
        $query->orderBy("created_at", 'desc');
        $query->orderBy("asn_dtl.item_id", 'desc');
        $query->orderBy("asn_dtl.asn_dtl_lot", 'desc');
        //$query->orderBy("asn_dtl.updated_at", 'desc');
        //$query->orderBy("ctn.plt_rfid", 'desc');
        $row = $query->get();

        return $row;
    }

    /**
     * @param $asnID
     * @param $vtlCtnrId
     *
     * @return mixed
     */
    public function listASNDtlAndPalletRFID($asnID, $vtlCtnrId, $search)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('asn_dtl')
            ->select([
                'asn_dtl.asn_hdr_id',
                'asn_dtl.asn_dtl_id',
                'asn_dtl.item_id',
                'asn_dtl.asn_dtl_sku',
                'asn_dtl.asn_dtl_size',
                'asn_dtl.asn_dtl_color',
                'asn_dtl.asn_dtl_lot',
                'asn_dtl.asn_dtl_pack',
                'asn_dtl.asn_dtl_ctn_ttl',

            ]);

        $query->Join('vtl_ctn as ctn', 'ctn.asn_dtl_id', '=', 'asn_dtl.asn_dtl_id')
            ->addSelect([
                'ctn.plt_rfid',
                'ctn.vtl_ctn_id',
                'ctn.ctnr_id',
                DB::raw("count(ctn.plt_rfid) as ctn_ttl"),
                DB::raw("count(ctn.plt_rfid) * asn_dtl.asn_dtl_pack as piece_ttl")
            ]);

        $query->groupBy('ctn.plt_rfid');
        $query->groupBy('asn_dtl.asn_dtl_id');

        /**
         * WMS2-3738
         * 10-04-2017
         */

        $query->orderBy("asn_dtl.item_id", 'desc');
        $query->orderBy("asn_dtl.asn_dtl_lot", 'desc');
        $query->orderBy("asn_dtl.updated_at", 'desc');
        $query->where('ctn.ctnr_id', $vtlCtnrId);
        $query->where('ctn.deleted', 0);
        $query->where('asn_dtl.asn_hdr_id', $asnID);
        $query->where('asn_dtl.deleted', 0);
        $query->whereNotNull('ctn.plt_rfid');
        /**
         * Allow search by rfid
         */
        if (isset($search['plt_rfid'])) {
            $query->where('plt_rfid', 'LIKE', "%{$search['plt_rfid']}%");
        }

        /**
         * Allow search by sku
         */
        if (isset($search['sku'])) {
            $query->where('asn_dtl_sku', 'LIKE', "%{$search['sku']}%");
        }

        $row = $query->get();

        return $row;
    }

    /**
     * Build mysql case statement
     * This function is builded for ticket WMS2-3738
     *
     * @param String $type
     * @param String $colName
     *
     * @return String
     */
    private function buildCaseSql($type = null, $colName = 'sts_name')
    {
        if (!$type || empty($type) || !Status::get($type)) {
            return null;
        }

        $arraySts = Status::get($type);
        if (!count($arraySts)) {
            return null;
        }

        $str = "CASE vtl_ctn_sts \n";
        foreach ($arraySts as $key => $value) {
            $str .= "WHEN '{$key}' THEN '{$value}' \n";
        }
        $str .= "END AS {$colName}";

        return $str;
    }

    public function updateAsnDtlsReceivedByCtnrID($cntrId, $asnId)
    {
        $asnDetails=$this->model
        ->select([
            DB::raw('gr_dtl.lot AS asn_dtl_lot'),
            'asn_dtl.asn_dtl_id',
        ])
        ->join('gr_dtl','gr_dtl.asn_dtl_id','=','asn_dtl.asn_dtl_id')
        ->where(['asn_dtl.asn_hdr_id' => $asnId, 'asn_dtl.ctnr_id' => $cntrId])
        ->where('asn_dtl.asn_dtl_sts', '!=', 'CC')
        ->get();
        foreach ($asnDetails as $asnDetail) {
			$this->model->where('asn_dtl_id',$asnDetail->asn_dtl_id)->update([
				'asn_dtl_sts'=>'RE',
				'asn_dtl_lot'=>$asnDetail->asn_dtl_lot,
			]);
        }
    }
}
