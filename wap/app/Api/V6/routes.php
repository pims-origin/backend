<?php

// wave pick
$api->group(['prefix' => '/v6/whs/{whsId:[0-9]+}/wave', 'namespace' => 'App\Api\V6\Outbound\Wavepick'], function ($api) {
    // update wave pick with RFID
    $api->put('/{wvId:[0-9]+}/update',
        [
            'action' => "updateWavepick",
            'uses'   => 'UpdateWithRfid\Controllers\WavepickController@updateWavePick'
        ]
    );
});

