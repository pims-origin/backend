<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Validators;


class GoodsReceiptUpdateValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'asn_dtl_id' => 'integer|greater_equal_than_zero',
        ];
    }
}
