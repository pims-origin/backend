<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\SystemCountry::class, function ($faker) {
    return [
        'sys_country_name' => $faker->name,
        'sys_country_code' => strtoupper($faker->randomLetter . $faker->randomLetter),
    ];
});

$factory->define(App\SystemState::class, function ($faker) {
    return [
        'sys_state_name'       => $faker->name,
        'sys_state_code'       => strtoupper($faker->randomLetter . $faker->randomLetter . $faker->randomLetter),
        'sys_state_desc'       => $faker->text,
        'sys_state_country_id' => function () {
            return factory(\App\SystemCountry::class)->create()->sys_country_id;
        }
    ];
});

$factory->define(App\SystemUom::class, function ($faker) {
    $code = '';
    $systemCode = 1;
    while (!empty($systemCode)) {
        $code = $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+[A-Z]{2,4}');
        $systemCode = DB::table('system_uom')->where('sys_uom_code', $code)->get();
    }

    return [
        'sys_uom_code' => $code,
        'sys_uom_name' => $faker->randomLetter,
        'sys_uom_des'  => $faker->randomLetter,
    ];
});