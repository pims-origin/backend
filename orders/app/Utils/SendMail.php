<?php

namespace App\Utils;

use Illuminate\Support\Facades\Mail;

class SendMail
{
    /**
     * @param $view
     * @param $to
     * @param $subject
     * @param array $data
     * @param bool $attach
     * @param bool $fileName
     * @param bool $image
     * @param bool $cc
     *
     * @return mixed
     */
    public function sendMail($view, $to, $subject, $data = [], $attach = false, $fileName = false, $image = false, $cc = false)
    {
        app('queue.connection');
        return Mail::send(
            $view,
            $data,
            function ($message) use ($to, $subject, $data, $attach, $fileName, $image, $cc) {
                if ($image) {
                    $images = explode("base64,", $image);
                    $image = $images[0];
                    if (count($images) > 1) {
                        $image = $images[1];
                    }
                    $message->attachData(base64_decode($image), 'ScreenShot.jpg');
                }
                $message->to($to, !empty($data['name']) ? $data['name'] : '')->subject($subject);

                if ($cc) {
                    $message->bcc('cuong.nguyen@seldatinc.com', 'Cuong.Nguyen');
                }

                if ($attach) {
                    $message->attach($attach, array(
                            'as' => $fileName,
                            'mime' => 'application/pdf')
                    );
                }
            }
        );
    }
}
