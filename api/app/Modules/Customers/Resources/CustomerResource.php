<?php

namespace App\Modules\Customers\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
            'type' => 'customers',
            'id' => (int) $this->cus_id,
            'attributes' => [
                'code'          => $this->cus_code,
                'name'          => $this->cus_name,
                'description'   => $this->cus_des
            ]
        ];
    }
}