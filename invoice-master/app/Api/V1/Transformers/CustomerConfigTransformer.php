<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\CustomerMeta;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\CustomerContact;

class CustomerConfigTransformer extends TransformerAbstract
{

    /**
     * ps:Show SAC
     *
     * @param
     *
     * @return array
     */
    public function transform(CustomerMeta $cusInfo)
    {
        if ($cusInfo->qualifier == 'CIE') {
            $emailConfig = json_decode($cusInfo->value);

            return [
                "send_to_customer"       => $emailConfig->send_to_customer,
                "account_email"          => $emailConfig->account_email,
                "account_manager_email"  => $emailConfig->account_manager_email,
                "customer_email"         => $emailConfig->customer_email ?? $this->getDefaultCusMail($cusInfo->cus_id),
            ];
        }

        if ($cusInfo->qualifier == 'CIO') {
            $emailConfig = json_decode($cusInfo->value);
            
            return [
                "ask_when_reopen"  => $emailConfig->ask_when_reopen,
                "ask_when_reject"  => $emailConfig->ask_when_reject,
                "create_option"    => $emailConfig->create_option,
                "terms"            => $emailConfig->terms ?? 30
            ];
        }
    }

    public function getDefaultCusMail($cusId)
    {
        $contact = CustomerContact::where('cus_ctt_cus_id', '=', $cusId)->firstOrFail();
        return $contact->cus_ctt_email ?? '';
    }
}
