<?php

namespace App\Api\V2\Outbound\Wave\GetMoreLocation\Controllers;

use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V2\Outbound\Wave\GetMoreLocation\Models\CartonModel;
use App\Api\V2\Outbound\Wave\GetMoreLocation\Models\LocationModel;
use App\Api\V2\Outbound\Wave\GetMoreLocation\Models\WavePickDtlModel;
use Seldat\Wms2\Utils\Status;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Seldat\Wms2\Utils\Message;
use TCPDF;
use App\Api\V2\Outbound\Wave\GetMoreLocation\Models\Log;
use App\MyHelper;
use Wms2\UserInfo\Data;
use App\libraries\RFIDValidate;

/**
 * Class PalletController
 *
 * @package App\Api\V1\Controllers
 */
class LocationController extends AbstractController
{
    const STATUS_ALLOCATE_LOCATION         = 'AL';
    const WAREHOUSE_ALLOCATE_LOCATION_CODE = 'wle';
    const SYSTEM_UOM_CODE_PIECE            = 'PC';

    protected $locationModel;
    protected $cartonModel;
    protected $wavePickDtlModel;

    public function __construct(
        CartonModel $cartonModel,
        LocationModel $locationModel,
        WavePickDtlModel $wavePickDtlModel
    ) {
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->wavePickDtlModel = $wavePickDtlModel;
    }

    public function moreLocation(Request $request, $whsId, $wv_dtl_id)
    {
        $input = $request->getParsedBody();
        $limit   = array_get($input, 'limit', 1);
        $loc_ids = array_get($input, 'loc_ids', null);

        /*
         * start logs
         */
        MyHelper::log("Function : " . __FUNCTION__, $input);

        $url = "/v2/whs/{$whsId}/wave/{$wv_dtl_id}/more-location";
        $owner = "";
        $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'ML',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'More location'
        ]);
        /*
         * end logs
         */

        if (!isset($input['loc_ids']) || empty($input['loc_ids'])) {
            $msg = "The loc_ids input is required!";
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        $arrInvalid = [];
        foreach ($input['loc_ids'] as $item) {
            if (!is_numeric($item)) {
                $arrInvalid[] = $item;
            }
        }
        if (!empty($arrInvalid)) {
            $msg = "The loc_ids input is invalid!";
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        $checkWvDtl = $this->wavePickDtlModel->getModel()->where([
                                                                "whs_id"    => $whsId,
                                                                "wv_dtl_id" => $wv_dtl_id,
                                                            ])->first();
        if (!$checkWvDtl) {
            $msg = sprintf("Wavepick detail %d doesn't exist!", $wv_dtl_id);
            $checkWvDtl = $this->wavePickDtlModel->getModel()->where([
                                                                "wv_dtl_id" => $wv_dtl_id,
                                                            ])->first();
            if ($checkWvDtl) {
                $msg = sprintf("Wavepick detail of %s doesn't belong to warehouse!", object_get($checkWvDtl, 'wv_num'));
            }
            $data = [
                'data'    => $wv_dtl_id,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            DB::beginTransaction();

            $whsMeta = DB::table('whs_meta')
                ->where('whs_id', $whsId)
                ->where('whs_qualifier', self::WAREHOUSE_ALLOCATE_LOCATION_CODE)
                ->first();
            $releasedPeriod = array_get($whsMeta, 'whs_meta_value', 0) * 60; //second

            $itemArr = DB::table('odr_dtl')->where('item_id', $checkWvDtl->item_id)
                ->select([
                    'system_uom.sys_uom_id as uom_id',
                    'system_uom.sys_uom_code as uom_code',
                ])
                ->join('system_uom', 'system_uom.sys_uom_id', '=', 'odr_dtl.uom_id')
                ->where('odr_dtl.deleted', 0)
                ->where('odr_dtl.wv_id', $checkWvDtl->wv_id)
                ->first();

            $isPickCtn    = array_get($itemArr, 'uom_code') == self::SYSTEM_UOM_CODE_PIECE ? false : true;

            $result = $this->cartonModel->getMoreLocation($whsId, $wv_dtl_id, $loc_ids, $limit, $releasedPeriod, $isPickCtn, $checkWvDtl->cus_id);
            if (!$result) {
                $msg = "No more location for suggestion!";
                // WAP-783 - [Wavepick] Update error message of suggest location
                $result = $this->cartonModel->getMoreLocationLock($whsId, $wv_dtl_id, $loc_ids, $limit, $releasedPeriod, $isPickCtn);
                if ($result) {
                    $msg = "All locations locked, see supervisor!";
                }
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $locs = [];
            foreach ($result as $locObj) {
                $getLocId = array_get($locObj, 'loc_id', null);
                $getLocCode = array_get($locObj, 'loc_code', null);
                $locs[] = [
                    'loc_id'         => $getLocId,
                    'loc_code'       => $getLocCode,
                    'loc_rfid'       => array_get($locObj, 'loc_rfid', null),
                    'sku'            => array_get($locObj, 'sku', null),
                    'size'           => array_get($locObj, 'size', null),
                    'color'          => array_get($locObj, 'color', null),
                    'lot'            => array_get($locObj, 'lot', null),
                    'item_id'        => array_get($locObj, 'item_id', null),
                    'cartons'        => array_get($locObj, 'cartons', null),
                    'pieces'         => array_get($locObj, 'avail_qty', null),
                    'is_cycle_count' => $this->_isCycleCount($whsId, $getLocId),
                    'is_rfid'        => $this->_isRfid($whsId, $getLocId),
                ];

                // WAP-763 - [Wavepick] Store suggest location by wavepick
                $dataLoc = [
                    'loc_id'   => $getLocId,
                    'loc_code' => array_get($locObj, 'loc_code', null),
                    'ctns'     => array_get($locObj, 'cartons', 0),
                    'qty'      => array_get($locObj, 'avail_qty', 0),
                ];
                $this->_insertWaveSugLoc($checkWvDtl->toArray(), $dataLoc);

                $userId = Data::getCurrentUserId();
                // WAP-706 - [Outbound - Wavepick] Update API to get more list of locaiton in wavepick detail

                // released location
                $this->locationModel->getModel()
                    ->whereIn('loc_id', $loc_ids)
                    ->update([
                        'reserved_at'  => 0,
                        'updated_by'   => $userId,
                        'updated_at'   => time(),
                    ]);

                // Allocate location
                $this->locationModel->getModel()
                    ->where('loc_id', $getLocId)
                    ->update([
                        'reserved_at'  => time() + $releasedPeriod,
                        'updated_by'   => $userId,
                        'updated_at'   => time(),
                    ]);

                $this->wavePickDtlModel->getModel()
                    ->where('wv_dtl_id', $wv_dtl_id)
                    ->update([
                        'primary_loc_id' => $getLocId,
                        'primary_loc'    => $getLocCode,
                        'updated_by'     => $userId,
                        'updated_at'     => time(),
                    ]);


            }
            DB::commit();

            return ['data' => $locs];

            //return $result;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function locationInfo(Request $request, $whsId, $wv_dtl_id)
    {
        $input = $request->getParsedBody();
        $loc   = array_get($input, 'location', 1);
        $type = array_get($input, 'type', 1);
        // Type = 1 is barcode and Type =2 is rfid

        /*
         * start logs
         */
        MyHelper::log("Function : " . __FUNCTION__, $input);

        $url = "/v2/whs/{$whsId}/wave/location-info";
        $owner = "";
        $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'ML',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Location Information'
        ]);
        /*
         * end logs
         */

        if (!$loc) {
            $msg = "The location is required!";
            return $this->_responseErrorMessage($msg);
        }

        if (!in_array($type, [1, 2])) {
            $msg = "Type is required. Just including 2(RFID) / 1(Barcode)";
            return $this->_responseErrorMessage($msg);
        }

        //validate location RFID
        if ($type == 2) {
            $locRFIDValid = new RFIDValidate($loc, RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                $msg = $locRFIDValid->error;
                return $this->_responseErrorMessage($msg);
            }
        }

        //Check existed for The loc RFID
        $locationInfo = '';
        if ($type == 2) {
            $locationInfo = $this->locationModel->getRackLocationByLocRFID($whsId, $loc);
        } else {
            $locationInfo = $this->locationModel->getFirstWhere([
                                'loc_whs_id' => $whsId,
                                'loc_code'   => $loc]);
        }
        if (empty($locationInfo)) {
            $msg = sprintf("The location %s doesn't exist!", $loc);
            return $this->_responseErrorMessage($msg);
        }

        $checkWvDtl = $this->wavePickDtlModel->getModel()->where([
                                                                "whs_id"    => $whsId,
                                                                "wv_dtl_id" => $wv_dtl_id,
                                                            ])->first();
        if (!$checkWvDtl) {
            $msg = sprintf("Wavepick detail %d doesn't exist!", $wv_dtl_id);
            $checkWvDtl = $this->wavePickDtlModel->getModel()->where([
                                                                "wv_dtl_id" => $wv_dtl_id,
                                                            ])->first();
            if ($checkWvDtl) {
                $msg = sprintf("Wavepick detail of %s doesn't belong to warehouse!", object_get($checkWvDtl, 'wv_num'));
            }
            return $this->_responseErrorMessage($msg);
        }

        try {
            $locId = $locationInfo->loc_id;
            $locCode = $locationInfo->loc_code;
            $cartons = $this->cartonModel->getModel()
                ->where('loc_id', $locationInfo->loc_id)
                ->where('item_id', $checkWvDtl->item_id)
                ->where('ctn_sts', 'AC')
                ->select([
                    'item_id',
                    'sku',
                    'size',
                    'color',
                    'ctn_pack_size',
                    'lot',
                    DB::raw('sum(piece_remain) AS total_piece'),
                    DB::raw('count(*) AS total_carton'),
                ])
                ->groupBy('item_id')
                ->get();

            if (!$cartons->count()) {
                $msg = sprintf("Location %s does not contain any cartons belong to the wave pick.", $locationInfo->loc_code);
                return $this->_responseErrorMessage($msg);
            }
            $skus = [];
            foreach ($cartons as $carton) {
                $skus[] = [
                        'sku'            => array_get($carton, 'sku', null),
                        'size'           => array_get($carton, 'size', null),
                        'color'          => array_get($carton, 'color', null),
                        'lot'            => array_get($carton, 'lot', null),
                        'item_id'        => array_get($carton, 'item_id', null),
                        'total_carton'   => array_get($carton, 'total_carton', null),
                        'total_piece'    => array_get($carton, 'total_piece', null),
                ];
            }

            $locCarton = [
                    'loc_id'         => $locId,
                    'loc_code'       => $locCode,
                    'loc_rfid'       => object_get($locationInfo, 'rfid', null),
                    'status'         => Status::getByValue(object_get($locationInfo, 'loc_sts_code', null), 'LOCATION_STATUS'),
                    'skus'           => $skus,
                    'is_cycle_count' => $this->_isCycleCount($whsId, $locId),
                    'is_rfid'        => $this->_isRfid($whsId, $locId),
                ];

            $msg = [
                'status' => true,
                'iat' => time(),
                'messages' => [[
                    'status_code' => 1,
                    'msg' => "Successfully!"
                ]],
                'data' => [$locCarton],
            ];

            return $msg;
            // return ['data' => [$locCarton]];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    private function _isCycleCount($whsId, $locId)
    {
        return false;
        // $query = DB::table('cc_notification')->where([
        //             'whs_id'  => $whsId,
        //             'loc_id'  => $locId,
        //             'deleted' => 0,
        //         ])
        //         ->whereIn('cc_ntf_sts', ['NW'])
        //         ->first();

        // return $query ? true : false;
    }

    private function _isRfid($whsId, $locId)
    {
        $query = DB::table('cartons')->where([
                    'whs_id'  => $whsId,
                    'loc_id'  => $locId,
                    'deleted' => 0,
                ])
                ->whereNotNull('rfid')
                ->where('ctn_sts', '!=', 'AJ')
                ->first();

        return $query ? true : false;
    }

    private function _insertWaveSugLoc($wvDtl, $dataLoc)
    {
        $data = [
        'wv_dtl_id'  => $wvDtl['wv_dtl_id'],
        'wv_id'      => $wvDtl['wv_id'],
        'whs_id'     => $wvDtl['whs_id'],
        'item_id'    => $wvDtl['item_id'],
        'sku'        => $wvDtl['sku'],
        'size'       => $wvDtl['size'],
        'color'      => $wvDtl['color'],
        'desc'       => '',
        'pack'       => $wvDtl['pack_size'],
        'lot'        => $wvDtl['lot'],
        'ctns'       => $dataLoc['ctns'],
        'qty'        => $dataLoc['qty'],
        'loc_id'     => $dataLoc['loc_id'],
        'loc_code'   => $dataLoc['loc_code'],
        'created_at' => time(),
    ];
        return DB::table('sug_loc')->insert($data);
    }

    private function _responseErrorMessage($msg, $data = null)
    {
        return new Response([
                        'data'    => $data,
                        'message' => $msg,
                        'status'  => false,
                    ], 200, [], null);
    }
}
