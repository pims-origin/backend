<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Transformers\OnlineOrdersTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class OnlineOrdersController extends AbstractController
{
    public $orderDtlModel;

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->orderDtlModel = new OrderDtlModel();
        $this->transformer = new OnlineOrdersTransformer();
    }

    /*
    ****************************************************************************
    */

    public function index(Request $request, $data=[])
    {
        return parent::index($request, [
            'model' => $this->orderDtlModel,
            'method' => 'searchOnlineOrders',
        ]);
    }

    /*
    ****************************************************************************
    */

}
