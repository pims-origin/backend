<?php

namespace App\Modules\Orders\Eloquents;

class ThirdParty extends BaseSoftModel
{
    protected $table = 'third_party';

    protected $primaryKey = 'tp_id';

    protected $fillable = [
        'whs_id',
        'cus_id',
        'type',
        'name',
        'des',
        'phone',
        'mobile',
        'ct_first_name',
        'ct_last_name',
        'longtitude',
        'latitude',
        'add_1',
        'add_2',
        'city',
        'state',
        'zip',
        'country',
        'extns',
        'email',
        'ctt_type',
        'addr_type'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }
}
