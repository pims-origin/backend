<?php

namespace Seldat\Wms2\Models;


class InvoiceReceivingHistory extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inv_his_rcv';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
     protected $fillable = [
        'inv_id',
        'inv_num',
        'cus_id',
        'whs_id',
        'gr_hdr_id',
        'recv_dt',
        'inv_sts'
    ];

    public function goodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'ctnr_id', 'ctnr_id');
    }
    
    public function invoiceHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceDetail', 'inv_id', 'inv_id');
    }
}