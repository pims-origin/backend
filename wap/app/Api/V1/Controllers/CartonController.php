<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 7/25/2016
 * Time: 11:46 AM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\DamageCartonModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\Log;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Transformers\CartonTransformer;
use App\Api\V1\Validators\PickFullCartonValidator;
use App\libraries\RFIDValidate;
use App\Api\V1\Validators\ScanCartonValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Maatwebsite\Excel\Excel;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Wms2\UserInfo\Data;

class CartonController extends AbstractController
{
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var DamageCartonModel
     */
    protected $damagedCartonModel;

    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    protected $odrDtlModel;
    protected $odrHdrModel;
    /**
     * @var orderCarton
     */
    protected $orderCartonModel;
    protected $wavePickHdrModel;
    protected $wavePickDtlModel;
    protected $palletModel;

    protected $virtualCartonModel;

    protected $scanCartonsValidator;

    /**
     * CartonController constructor.
     *
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param DamageCartonModel $damageCartonModel
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param PalletModel $palletModel
     */
    public function __construct
    (
        CartonModel $cartonModel,
        LocationModel $locationModel,
        DamageCartonModel $damageCartonModel,
        GoodsReceiptModel $goodsReceiptModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        InventorySummaryModel $inventorySummaryModel,
        PalletModel $palletModel,
        VirtualCartonModel $virtualCartonModel
    )
    {
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->damagedCartonModel = $damageCartonModel;
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->orderCartonModel = new OrderCartonModel();
        $this->odrDtlModel = new OrderDtlModel();
        $this->odrHdrModel = new OrderHdrModel();
        $this->wavePickHdrModel = new WavePickHdrModel();
        $this->wavePickDtlModel = new WavePickDtlModel();
        $this->scanCartonsValidator = new ScanCartonValidator();
        $this->palletModel = $palletModel;
        $this->virtualCartonModel = $virtualCartonModel;
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response
     */
    public function getCtnByRFID($whsId, $rfid, Request $request)
    {
        /*$input = $request->getQueryParams();
        $validate = new GetCartonByRFIDValidator();
        $validate->validate($input);*/

        $ctnData = $this->cartonModel->getFirstWhere(
            [
                'rfid' => $rfid
            ]
        );

        $cartonTransformer = new CartonTransformer();

        return $this->response->item($ctnData, $cartonTransformer);
    }

    public function pickFullCarton($whsId, $wvDtlID, Request $request)
    {
        //ctn_num
        $input = $request->getQueryParams();

        $validate = new PickFullCartonValidator();
        //$validate->validate($input);
        $requireFieldsValidate = $validate->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        DB::beginTransaction();
        //transaction
        try {
            // check exists ctn_num
            $check = $this->cartonModel->getFirstWhere([
                'ctn_num' => $input['ctn_num']
            ]);
            if (empty($check)) {
                $msg = sprintf("This ctn_num %s doesn't exist", $input['ctn_num']);
                $data = [
                    'data' => null,
                    'message' => $msg,
                    'status' => false,
                ];

                return new Response($data, 200, [], null);
            }

            //get created_at of ctn_num on carton table,
            $ctnCreatedDate = $check->created_at;
            $ctnCreatedDate = is_int($ctnCreatedDate) ?: $ctnCreatedDate->timestamp;

            // caculate date storage on WH
            $storageDuration = date('d/m/Y') - date('d/m/Y', $ctnCreatedDate);

            //update table carton: plt_id =null, loc_id=null, loc_code=null, loc_name=null, picked_dt= updated_at = time(),
            //loc_type_code =null,storage_duration = 0, where ctn_num, updated_at = time()
            $this->cartonModel->updateWhere(
                [
                    'plt_id' => null,
                    'loc_id' => null,
                    'loc_code' => null,
                    'loc_name' => null,
                    'ctn_sts' => 'PK',
                    'loc_type_code' => null,
                    'storage_duration' => $storageDuration,
                    'picked_dt' => time(),
                    'updated_at' => time()
                ],
                [
                    'ctn_num' => $input['ctn_num']
                ]
            );

            //update table odr_cartons: sts= PK, updated_at = time(), is_storage = 0,
            $this->orderCartonModel->updateWhere(
                [
                    'sts' => 'PK',
                    'updated_at' => time(),
                    'is_storage' => 0
                ],
                [
                    'ctn_num' => $input['ctn_num']
                ]
            );

            DB::commit();
            $msg = sprintf("Updated carton has num %s successfully.", $input['ctn_num']);

            return $this->response->noContent()
                ->setContent(['message' => $msg, 'Status' => 'OK'])
                ->setStatusCode(Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function assignCartonToOrder($whsId, $orderDtlId, Request $request)
    {
        $input = $request->getParsedBody();

        $ctnRFID = array_get($input, 'ctn_rfid', null);

        //get order_dtl table
        $odrDtl = $this->odrDtlModel->getOdrDtlInfoByOdrDtlId($orderDtlId, $whsId);

        if (empty($odrDtl)) {
            $msg = sprintf("This order detail %d doesn't exist", $orderDtlId);
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];

            return new Response($data, 200, [], null);
        }

        $existCtn = $this->checkOdrCtnExist($ctnRFID);

        if (empty($existCtn)) {
            $msg = sprintf("This carton RFID %s doesn't exist", $ctnRFID);
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];

            return new Response($data, 200, [], null);
        }

        $ctnObj = $this->cartonModel->getFirstWhere(
            [
                'rfid' => $ctnRFID,
                'ctn_sts' => Status::getByKey('CTN_STATUS', 'ACTIVE')
            ]
        );

        //check item in carton the same with odr dtl item
        if ($odrDtl->item_id != $ctnObj->item_id) {
            $msg = sprintf("The carton's SKU is not in order detail's SKU.");
            throw new Exception($msg);
        }

        //get wv_num and wv_dt_id
        $wvObj = $this->wavePickDtlModel->getWavePickInfo($odrDtl->wv_id, $odrDtl->item_id);

        //update table odr_ctn where ctnRFID = $ctnRFID
        try {
            DB::beginTransaction();
            $data = [
                'odr_hdr_id' => $odrDtl->odr_id,
                'odr_num' => $odrDtl->odr_num,
                'odr_dtl_id' => $orderDtlId,
                'wv_hdr_id' => $odrDtl->wv_id,
                'wv_dtl_id' => array_get($wvObj, '0.wv_dtl_id', null),
                'wv_num' => array_get($wvObj, '0.wv_num', null),
                'ctn_rfid' => $ctnRFID,
                'ctn_sts' => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
            ];

            $this->updateOrderCarton($data);

            /*update sts of odr_dt*/
            $pickedQty = $this->orderCartonModel->sumPieceQtyOdrCtnByOdrDtlID($orderDtlId);
            if ($pickedQty[0]['piece_qty'] == $odrDtl->piece_qty) {
                //update odr dtl sts to PICKED
                $stsPicked = Status::getByValue('Picked', 'ORDER-STATUS');
                $this->updateOdrDtlStatus($orderDtlId, $stsPicked);
            } else {
                //update odr dtl sts to PICKING
                $stsPicking = Status::getByValue('Picking', 'ORDER-STATUS');
                $this->updateOdrDtlStatus($orderDtlId, $stsPicking);
            }

            //update sts of odr_hdr
            $this->updateOdrHdrStatus($odrDtl->odr_id);

            DB::commit();
            $msg = sprintf("Assigned carton %s to order successfully.", $input['ctn_rfid']);

            return $this->response->noContent()
                ->setContent(['message' => $msg, 'Status' => 'OK'])
                ->setStatusCode(Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $ctnRFID
     *
     * @return bool
     */
    private function checkOdrCtnExist($ctnRFID)
    {
        $odrCtn = $this->orderCartonModel->getFirstWhere(
            [
                'ctn_rfid' => $ctnRFID,
                'ctn_sts' => Status::getByValue('PICKING', 'ODR_CTN_STATUS')
            ]
        );


        return $odrCtn;
    }

    private function updateOdrDtlStatus($orderDtlId, $sts)
    {
        return $this->odrDtlModel->updateWhere(
            [
                'itm_sts' => $sts
            ],
            [
                'odr_dtl_id' => $orderDtlId
            ]
        );
    }

    private function updateOdrHdrStatus($orderHdrId)
    {
        //check all odr dtl is PICKED
        $check = $this->odrDtlModel->checkAllOdrDtlStsPicked($orderHdrId);

        if (!empty($check)) {
            //update order hdr sts is Picked
            return $this->odrHdrModel->updateWhere(
                [
                    'odr_sts' => Status::getByValue('Picked', 'ORDER-STATUS')
                ],
                [
                    'odr_id' => $orderHdrId
                ]
            );
        } else {
            //update order hdr sts is Picking
            return $this->odrHdrModel->updateWhere(
                [
                    'odr_sts' => Status::getByValue('Picking', 'ORDER-STATUS')
                ],
                [
                    'odr_id' => $orderHdrId
                ]
            );
        }
    }

    public function assignPieceToOrder($whsId, $orderDtlId, Request $request)
    {
        $input = $request->getParsedBody();
        $ctnRFID = array_get($input, 'ctn_rfid', null);
        $pieceQty = array_get($input, 'qty', 0);

        //get order carton by ctn_num
        $odrCtn = $this->orderCartonModel->getFirstWhere(
            [
                'ctn_rfid' => $ctnRFID,
                'ctn_sts' => Status::getByValue('PICKING', 'ODR_CTN_STATUS'),
            ]
        );

        if (empty($odrCtn)) {
            $msg = sprintf("This carton %s doesn't exist", $ctnRFID);

            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];

            return new Response($data, 200, [], null);
        }

        //if input $pieceQty > current piece qty
        if ($pieceQty > $odrCtn->piece_qty) {
            $msg = sprintf('The input piece quantity more than current carton quantity.', $pieceQty);

            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];

            return new Response($data, 200, [], null);
        }

        $flag = false;
        if ($pieceQty == $odrCtn->piece_qty) {
            $flag = true;
        }

        //get order_dtl table
        $odrDtl = $this->odrDtlModel->getOdrDtlInfoByOdrDtlId($orderDtlId, $whsId);

        if (empty($odrDtl)) {
            $msg = sprintf("This order detail %d doesn't exist", $orderDtlId);

            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];

            return new Response($data, 200, [], null);
        }

        $ctnObj = $this->cartonModel->getFirstWhere(
            [
                'ctn_id' => $odrCtn->ctn_id,
            ]
        );

        if (empty($ctnObj)) {
            $msg = sprintf("This carton %s doesn't exist", $ctnRFID);

            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];

            return new Response($data, 200, [], null);
        }

        //check item in carton the same with odr dtl item
        if ($odrDtl->item_id != $ctnObj->item_id) {
            $msg = sprintf("The carton's SKU is not in order detail's SKU.");
            throw new Exception($msg);
        }

        //get wv_num and wv_dt_id
        $wvObj = $this->wavePickDtlModel->getWavePickInfo($odrDtl->wv_id, $odrDtl->item_id);

        //update table odr_ctn where ctnRFID = $ctnRFID
        try {
            DB::beginTransaction();
            //update full carton
            if ($flag) {
                $data = [
                    'odr_hdr_id' => $odrDtl->odr_id,
                    'odr_num' => $odrDtl->odr_num,
                    'odr_dtl_id' => $orderDtlId,
                    'wv_hdr_id' => $odrDtl->wv_id,
                    'wv_dtl_id' => array_get($wvObj, '0.wv_dtl_id', null),
                    'wv_num' => array_get($wvObj, '0.wv_num', null),
                    'ctn_rfid' => $ctnRFID,
                    'ctn_sts' => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                ];

                $this->updateOrderCarton($data);
            } else {
                //create new order carton data
                $dataOrdCtn = [
                    'odr_hdr_id' => $odrDtl->odr_id,
                    'odr_dtl_id' => $orderDtlId,
                    'wv_hdr_id' => $odrCtn->wv_hdr_id,
                    'wv_dtl_id' => $odrCtn->wv_dtl_id,
                    'odr_num' => $odrDtl->odr_num,
                    'wv_num' => $odrCtn->wv_num,
                    'ctn_rfid' => $odrCtn->ctn_rfid,
                    'ctn_num' => $odrCtn->ctn_num,
                    'piece_qty' => $pieceQty,
                    'ctn_id' => $odrCtn->ctn_id,
                    'ctnr_rfid' => $odrCtn->ctn_rfid,
                    'created_at' => time(),
                    'sts' => 'i',
                    'is_storage' => 0,
                    'ctn_sts' => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                ];
                $this->orderCartonModel->create($dataOrdCtn);

                //update current carton
                $newPiece = $odrCtn->piece_qty - $pieceQty;
                $this->orderCartonModel->updateWhere(
                    [
                        'piece_qty' => $newPiece,
                        'is_storage' => 1,
                    ],
                    [
                        'ctn_num' => $odrCtn->ctn_num,
                        'ctn_sts' => Status::getByValue('PICKING', 'ODR_CTN_STATUS')
                    ]
                );
            }

            /*update sts of odr_dt*/
            $pickedQty = $this->orderCartonModel->sumPieceQtyOdrCtnByOdrDtlID($orderDtlId);
            if ($pickedQty[0]['piece_qty'] == $odrDtl->piece_qty) {
                //update odr dtl sts to PICKED
                $stsPicked = Status::getByValue('Picked', 'ORDER-STATUS');
                $this->updateOdrDtlStatus($orderDtlId, $stsPicked);
            } else {
                //update odr dtl sts to PICKING
                $stsPicking = Status::getByValue('Picking', 'ORDER-STATUS');
                $this->updateOdrDtlStatus($orderDtlId, $stsPicking);
            }

            //update sts of odr_hdr
            $this->updateOdrHdrStatus($odrDtl->odr_id);

            DB::commit();
            $msg = sprintf("Assigned carton %s to order successfully.", $input['ctn_rfid']);

            return $this->response->noContent()
                ->setContent(['message' => $msg, 'Status' => 'OK'])
                ->setStatusCode(Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function updateOrderCarton($data)
    {
        return $this->orderCartonModel->updateWhere(
            [
                'odr_hdr_id' => $data['odr_hdr_id'],
                'odr_num' => $data['odr_num'],
                'odr_dtl_id' => $data['odr_dtl_id'],
                'wv_hdr_id' => $data['wv_hdr_id'],
                'wv_dtl_id' => $data['wv_dtl_id'],
                'wv_num' => $data['wv_num'],
                'updated_at' => time(),
                'ctn_sts' => $data['ctn_sts'],
                'sts' => 'u',
            ],
            [
                'ctn_rfid' => $data['ctn_rfid'],
                'ctn_sts' => Status::getByValue('PICKING', 'ODR_CTN_STATUS')
            ]
        );
    }

    /**
     * @param $orderDtlId
     *
     * @return data
     */
    public function assignCartonsToOrder($whsId, $orderDtlId, Request $request)
    {
        $input = $request->getParsedBody();

        $ctnRFID = array_get($input, 'arr_ctn_rfid', null);
        //get order_dtl table
        $odrDtl = $this->odrDtlModel->getOdrDtlInfoByOdrDtlId($orderDtlId, $whsId);

        if (empty($odrDtl)) {
            $msg = sprintf("This order detail %d doesn't exist", $orderDtlId);
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];

            return new Response($data, 200, [], null);
        }

        $check = true;
        $arRfid = '';
        $arSku = '';
        foreach ($ctnRFID as $rfid) {
            $ctn = isset($rfid['ctn_rfid']) ? $rfid['ctn_rfid'] : null;
            $existCtn = $this->checkOdrCtnExist($ctn);
            if (empty($existCtn)) {
                $check = false;
                $arRfid = $arRfid . ', ' . $ctn;
            } //check item in carton the same with odr dtl item
            else {
                $ctnObj = $this->cartonModel->getFirstWhere([
                        'ctn_id' => $existCtn->ctn_id,
                        'ctn_sts' => Status::getByKey('CTN_STATUS', 'ACTIVE')
                    ]
                );
                if (empty($ctnObj) || $odrDtl->item_id != $ctnObj->item_id) {
                    $check = false;
                    $arSku = $arSku . ', ' . $ctn;
                }
            }
        }
        if (!$check) {
            if ($arRfid != '') {
                $arRfid = trim(substr($arRfid, 1));
                $msg = sprintf("The RFIDs %s doesn't exist", $arRfid);
                throw new Exception($msg);
            } elseif ($arSku != '') {
                $arSku = trim(substr($arSku, 1));
                $msg = sprintf("The RFIDs %s carton's SKU is not in order detail's SKU.", $arSku);
                throw new Exception($msg);
            }
        }

        //get wv_num and wv_dt_id
        $wvObj = $this->wavePickDtlModel->getWavePickInfo($odrDtl->wv_id, $odrDtl->item_id);

        //update table odr_ctn where ctnRFID = $ctnRFID
        try {
            DB::beginTransaction();

            foreach ($ctnRFID as $ctn) {
                $ctnRFID = $ctn['ctn_rfid'];
                if (!isset($ctn['qty'])) {
                    $data = [
                        'odr_hdr_id' => $odrDtl->odr_id,
                        'odr_num' => $odrDtl->odr_num,
                        'odr_dtl_id' => $orderDtlId,
                        'wv_hdr_id' => $odrDtl->wv_id,
                        'wv_dtl_id' => array_get($wvObj, '0.wv_dtl_id', null),
                        'wv_num' => array_get($wvObj, '0.wv_num', null),
                        'ctn_rfid' => $ctnRFID,
                        'ctn_sts' => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                    ];

                    $this->updateOrderCarton($data);
                } else {
                    $pieceQty = $ctn['qty'];
                    //get order carton by ctn_num
                    $odrCtn = $this->orderCartonModel->getFirstWhere(
                        [
                            'ctn_rfid' => $ctnRFID,
                            'ctn_sts' => Status::getByValue('PICKING', 'ODR_CTN_STATUS'),
                        ]
                    );

                    //if input $pieceQty > current piece qty
                    if ($pieceQty > $odrCtn->piece_qty) {
                        $msg = sprintf('The input piece quantity more than current carton quantity.', $pieceQty);
                        $data = [
                            'data' => null,
                            'message' => $msg,
                            'status' => false,
                        ];

                        return new Response($data, 200, [], null);
                    }

                    $flag = false;
                    if ($pieceQty == $odrCtn->piece_qty) {
                        $flag = true;
                    }

                    //update table odr_ctn where ctnRFID = $ctnRFID
                    if ($flag) {
                        $data = [
                            'odr_hdr_id' => $odrDtl->odr_id,
                            'odr_num' => $odrDtl->odr_num,
                            'odr_dtl_id' => $orderDtlId,
                            'wv_hdr_id' => $odrDtl->wv_id,
                            'wv_dtl_id' => array_get($wvObj, '0.wv_dtl_id', null),
                            'wv_num' => array_get($wvObj, '0.wv_num', null),
                            'ctn_rfid' => $ctnRFID,
                            'ctn_sts' => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                        ];

                        $this->updateOrderCarton($data);
                    } else {
                        //create new order carton data
                        $dataOrdCtn = [
                            'odr_hdr_id' => $odrDtl->odr_id,
                            'odr_dtl_id' => $orderDtlId,
                            'wv_hdr_id' => $odrCtn->wv_hdr_id,
                            'wv_dtl_id' => $odrCtn->wv_dtl_id,
                            'odr_num' => $odrDtl->odr_num,
                            'wv_num' => $odrCtn->wv_num,
                            'ctn_rfid' => $odrCtn->ctn_rfid,
                            'ctn_num' => $odrCtn->ctn_num,
                            'piece_qty' => $pieceQty,
                            'ctn_id' => $odrCtn->ctn_id,
                            'ctnr_rfid' => $odrCtn->ctn_rfid,
                            'created_at' => time(),
                            'sts' => 'i',
                            'is_storage' => 0,
                            'ctn_sts' => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                        ];
                        $this->orderCartonModel->create($dataOrdCtn);

                        //update current carton
                        $newPiece = $odrCtn->piece_qty - $pieceQty;
                        $this->orderCartonModel->updateWhere(
                            [
                                'piece_qty' => $newPiece,
                                'is_storage' => 1,
                            ],
                            [
                                'ctn_num' => $odrCtn->ctn_num,
                                'ctn_sts' => Status::getByValue('PICKING', 'ODR_CTN_STATUS')
                            ]
                        );
                    }
                }
            }

            /*update sts of odr_dt*/
            $pickedQty = $this->orderCartonModel->sumPieceQtyOdrCtnByOdrDtlID($orderDtlId);
            if ($pickedQty[0]['piece_qty'] == $odrDtl->piece_qty) {
                //update odr dtl sts to PICKED
                $stsPicked = Status::getByValue('Picked', 'ORDER-STATUS');
                $this->updateOdrDtlStatus($orderDtlId, $stsPicked);
            } else {
                //update odr dtl sts to PICKING
                $stsPicking = Status::getByValue('Picking', 'ORDER-STATUS');
                $this->updateOdrDtlStatus($orderDtlId, $stsPicking);
            }

            //update sts of odr_hdr
            $this->updateOdrHdrStatus($odrDtl->odr_id);

            DB::commit();
            $msg = sprintf("Assigned carton to order successfully.");

            return $this->response->noContent()
                ->setContent(['message' => $msg, 'Status' => 'OK'])
                ->setStatusCode(Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response
     */
    public function consolidatePallet_noUse($whsId, Request $request)
    {
        /**
         * consolidate pallet
         */
        // get data from HTTP
        $input = $request->getParsedBody();

        $requireFieldsValidate = $this->scanCartonsValidator->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'] = [
                'status_code' => -1,
                'msg' => $requireFieldsValidate
            ];

            return $msg;

        }
        //get an ctn rfid to get grdetail from table ctn
        $randRfid = array_get($input, 'ctn-rfid', null);
        $arrCtnRfid = array_get($input, 'ctn-rfid', null);
        $pltRfid = array_get($input, 'pallet-rfid', null);

        $owner = $transaction = "";
        try {

            $url = "/whs/{$whsId}/consolidate_plt/";
            Log:: info($request, $whsId, [
                'evt_code' => 'ACC',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => 'Consolidate Pallet'
            ]);

            if (!$randRfid) {
                throw new \Exception(Message::get('BM142'));
            }

            //validate pallet RFID
            $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
            if (!$pltRFIDValid->validate()) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = $pltRfid;
                $msg['message'] = [
                    'status_code' => -1,
                    'msg' => $pltRFIDValid->error
                ];

                return $msg;

            }

            //check invalid code carton
            $ctnRfidInValid = [];
            $errorMsg = '';
            foreach ($arrCtnRfid as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }

            //if has errors, return all error data and message
            if ($ctnRfidInValid) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = $ctnRfidInValid;
                $msg['message'] = [
                    'status_code' => -6,
                    'msg' => $errorMsg
                ];

                return $msg;

            }

            //check exist of carton
            $noExistCartons = '';
            foreach ($arrCtnRfid as $ctnRfid) {
                $checkCartonsExist = $this->cartonModel->checkExistedCartonAccordingToRFID($ctnRfid, $whsId);
                if (!$checkCartonsExist) {
                    $noExistCartons .= $ctnRfid . ' ';
                }

            }
            if ($noExistCartons) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = [];
                $msg['message'] = [
                    'status_code' => -1,
                    'msg' => sprintf("These cartons: %s are not existed.", $noExistCartons)
                ];

                return $msg;

            }

            //check exist of pallet
            $checkPalletExist = $this->palletModel->checkExistedPalletAccordingToRFID($pltRfid, $whsId);

            DB::beginTransaction();
            if ($checkPalletExist) {
                $this->cartonModel->updatePalletID($checkPalletExist->plt_id, $whsId, $arrCtnRfid);
                $ctnTtl = count($arrCtnRfid);
                $this->palletModel->updateCtnTtlForPallet($checkPalletExist->plt_id, $ctnTtl);
            } else {
                $predis = new Data();
                $userInfo = $predis->getUserInfo();
                $currentWH = array_get($userInfo, 'current_whs', 0);
                $cus_ids = $predis->getCustomersByWhs($currentWH);

                //get cus_id according to whs_id that number of cartons is largest
                $countCartonsByCusIds = $this->cartonModel->countCartonsByCusIds($cus_ids);
                $countCartonsByCusIds_array = array_pluck($countCartonsByCusIds, 'numberOfCarton', 'cus_id');
                $maxCountCartonsByCusId = max($countCartonsByCusIds_array);

                foreach ($countCartonsByCusIds_array as $key => $value) {
                    if ($value == $maxCountCartonsByCusId) {
                        $cus_id = $key;
                    }

                }

                $newPalletId = $this->palletModel->createNewPallet($pltRfid, $whsId, $cus_id)->plt_id;
                $this->cartonModel->updatePalletID($newPalletId, $whsId, $arrCtnRfid);
                $ctnTtl = count($arrCtnRfid);
                $this->palletModel->updateCtnTtlForPallet($newPalletId, $ctnTtl);
            }
            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'] = [
                'status_code' => 1,
                'msg' => sprintf("Pallet %s consolidated successfully!", $pltRfid)
            ];

            return $msg;

        } catch (\Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => "/whs/{$whsId}/consolidate_plt/",
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }


    public function getStatusCarton($whsId, $rfid)
    {

        try {

            $ctnRFIDValid = new RFIDValidate($rfid, RFIDValidate::TYPE_CARTON, $whsId);
            if (!$ctnRFIDValid->validate()) {
                throw  new  \Exception("Carton's RFID must be valid pattern '/^([A-F0-9]{24})$/' and length 24");
            }
            $data = [];
            $items = $this->cartonModel->getFirstWhere(['whs_id' => $whsId, 'rfid' => $rfid]);
            if ($items) {
                $data = [
                    'rfid' => object_get($items, 'rfid'),
                    'ctn_sts' => object_get($items, 'ctn_sts')
                ];
            }
            else {

                $vtlCarton = $this->virtualCartonModel->getFirstWhere(['ctn_rfid' => $rfid, 'whs_id' => $whsId]);
                if($vtlCarton) {
                    $data = [
                        'rfid' => object_get($items, 'rfid'),
                        'ctn_sts' => 'NW'
                    ];
                }
                else {
                    $data = [
                        'rfid' => object_get($items, 'rfid'),
                        'ctn_sts' => 'NE'
                    ];
                }

            }

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'][] = $data;
            $msg['message']['ctns'] = [
                'status_code' => 1,
                'msg' => 'OK'
            ];

            return new Response($msg, 200, [], null);

        } catch (\Exception $e) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'][] = [
                'status_code' => -1,
                'msg' => $e->getMessage()
            ];

            return new Response($msg, 200, [], null);
        }
    }


}
