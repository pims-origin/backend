<?php

namespace Seldat\Wms2\Utils;

class SelArr
{
    public static function removeNullOrEmptyString($array)
    {
        return array_filter($array, function($v) {
            return $v !== null && $v !== '';
        }, ARRAY_FILTER_USE_BOTH);
    }

    public static function trim($input)
    {
        if (!is_array($input)){
            return trim($input);
        }

        return array_map([__CLASS__, 'trim'], $input);
    }

    public static function clean(array $input)
    {
        $input = self::removeNullOrEmptyString($input);
        $input = self::trim($input);
        return $input;
    }
}