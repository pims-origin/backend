<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CustomerMetaModel;
use App\VasHdr;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Models\PackRef;

class WorkOrderHdrTransformer extends TransformerAbstract
{

    public $getDetail = false;

    /**
     * ps:Show WorkOrder
     *
     * @param VasHdr $workOrderHdr
     *
     * @return array
     */
    public function transform(VasHdr $workOrderHdr)
    {
        $workOrderStatusArr = array_flip(config('constants.vas_hdr_sts'));
        $workOrderDtl = isset($workOrderHdr->vasDtl) ? $workOrderHdr->vasDtl : null;
        $createdBy = isset($workOrderHdr->createdBy) ? $workOrderHdr->createdBy : null;
        $orderHdr = isset($workOrderHdr->order) ? $workOrderHdr->order : null;

        // show dimension for WorkOrderHdr
        /*if ($workOrderHdr->odr_hdr_id) {
            $dimension = PackHdr::select(['length', 'width', 'height'])
                            ->where('odr_hdr_id', $workOrderHdr->odr_hdr_id)
                            ->first();
                            
            $dimension = (int)$dimension->length . 'x' . (int)$dimension->width . 'x' . (int)$dimension->height;
        }*/

        $result = [
            'vas_id'     => $workOrderHdr->vas_id,
            'vas_num'    => $workOrderHdr->vas_num,
            'cus_id'     => $workOrderHdr->cus_id,
            'subtotal'   => $workOrderHdr->subtotal,
            'tax'        => $workOrderHdr->tax,
            'amount_ttl' => $workOrderHdr->amount_ttl,
            'vas_sts'    => ucfirst(strtolower($workOrderStatusArr[$workOrderHdr->vas_sts])),
            'vas_note'   => $workOrderHdr->vas_note,
            'cus_name'   => $workOrderHdr->customer()->first()->cus_name,
            'ref_qty'    => $workOrderHdr->ref_qty,
            'item_qty'   => $workOrderHdr->item_qty,
            'is_order'   => $workOrderHdr->is_order == 1 ? true : false,
            'odr_id'     => $workOrderHdr->odr_hdr_id,
            'odr_num'    => !empty($orderHdr) ? $orderHdr->odr_num : null,
            'is_rush'    => !empty($orderHdr) ? ($orderHdr->rush_odr == 1 ? true : false) : null,
            'created_at' => $workOrderHdr->created_at->format('m/d/Y'),
            'created_by' => !empty($createdBy) ? $createdBy->first_name . " " . $createdBy->last_name : "",

            'cus_odr_num' => !empty($orderHdr) ? $orderHdr->cus_odr_num : null,
            'cus_po'      => !empty($orderHdr) ? $orderHdr->cus_po : null,

            // 'dimension'  => !empty($dimension) ? $dimension : null,

        ];
        //Get Customer currency
        $currency = (new CustomerMetaModel())->getCurrencyByCurrentWhs($workOrderHdr->cus_id);
        //Get WorkOrderDtl
        if ($this->getDetail && !empty($workOrderDtl)) {
            $arr = [];
            foreach ($workOrderDtl as $detail) {
                $detail = (new WorkOrderDtlTransformer())->transform($detail);
                $detail['currency'] = $currency;
                $arr[] = $detail;
            }
            $result['items'] = $arr;
        }

        return $result;
    }
}
