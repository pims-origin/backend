<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/time', function () use ($app) {
    return date('Y-m-d H:i:s');
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'setWarehouseTimezone';
    $middleware[] = 'verifySecret';
    // $middleware[] = 'authorize';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () {
            return ['Seldat API RF GUNS V1'];
        });

        // get gr_dtl_id from pal_sug_loc
        $api->get('/{whsId:[0-9]+}/put-away/suggest/{grDtlId:[0-9]+}',
            ['action' => "viewPutaway", 'uses' => 'PalletController@getSuggestLocation']);

        //  Update loc_id to pallet table
        $api->put('/{whsId:[0-9]+}/put-away',
            ['action' => "saveAssignPallet", 'uses' => 'PalletController@updatePalletToLocation']);

        // RF Gun
        $api->get('/{whsId:[0-9]+}/put-away', ['action' => "viewPallet", 'uses' => 'PalletController@search']);
        $api->get('/{whsId:[0-9]+}/put-away/{grHdrId:[0-9]+}', [
            'action' => "viewPallet",
            'uses' => 'PalletController@show'
        ]);

        $api->get('/pallet-list', ['action' => "viewPallet", 'uses' => 'PalletController@palletList']);

        // Wave
        $api->get('/{whsId:[0-9]+}/wave', ['action' => "viewWavepick", 'uses' => 'WaveController@search']);
        $api->get('/{whsId:[0-9]+}/wave/{wvHdrId:[0-9]+}',
            ['action' => "viewWavepick", 'uses' => 'WaveController@show']);
        $api->get('/{whsId:[0-9]+}/wave/sku/{wvDtlId:[0-9]+}', [
            'action' => "viewWavepick",
            'uses' =>
                'WaveController@detail'
        ]);
        $api->get('/{whsId:[0-9]+}/wave/sku/{wvDtlId:[0-9]+}/suggest', [
            'action' => "viewWavepick",
            'uses' =>
                'WaveController@suggest'
        ]);

        //relocation
        $api->put('/{whsId:[0-9]+}/relocation', ['action' => "relocation", 'uses' => 'LocationController@relocation']);


        // update wave pick
        $api->put('/{whsId:[0-9]+}/wave/sku/{wv_dtl_id}', ['action' => "viewWavepick", 'uses' =>
            'WaveController@updateWavepick']);

        $api->get('/{whsId:[0-9]+}/putawayDashboard',
            ['action' => "putawayDashboard", 'uses' => 'PalletController@putawayDashboard']);


        $api->get('/{whsId:[0-9]+}/wavepickDashboard',
            ['action' => "wavepickDashboard", 'uses' => 'WaveController@wavepickDashboard']);
    });

    //  Bay
    $api->group(['prefix' => 'bay', 'namespace' => 'App\Api\Bay\Controllers'], function ($api) {

        $api->get('/', function () {
            return ['Seldat API RF GUNS BAY'];
        });

        // Get List
        $api->get('/bays', 'BayController@search');
        // Show
        $api->get('/bays/{bayId:[0-9]+}', 'BayController@show');
        // Store
        $api->post('/bays', 'BayController@store');
        // Update
        $api->put('/bays/{bayId:[0-9]+}', 'BayController@update');

        //  Container Charge
        // Get List
        $api->get('/container-charges', 'BayController@search');
        // Show
        $api->get('/container-charges/{ccId:[0-9]+}', 'ContainerChargeController@show');
        // Store
        $api->post('/container-charges/{bayId:[0-9]+}', 'ContainerChargeController@store');
        // Update
        $api->put('/container-charges/{ccId:[0-9]+}', 'ContainerChargeController@update');

        // Report
        $api->get('/container-charges/report', 'ContainerChargeController@reportContainer');
    });

    // Gate
    $api->group(['prefix' => 'gate', 'namespace' => 'App\Api\Gate\Controllers'], function ($api) {

        $api->get('/', function () {
            return ['Seldat API RF GUNS GATE'];
        });

        // Get List
        $api->get('/gates', 'GateController@search');
        // Show
        $api->get('/gates/{gateId:[0-9]+}', 'GateController@show');
        // Store
        $api->post('/gates', 'GateController@store');
        // Update
        $api->put('/gates/{gateId:[0-9]+}', 'GateController@update');

        //  Gate Detail
        // Get List
        $api->get('/gate-dtls', 'GateController@search');
        // Show
        $api->get('/gate-dtls/{ccId:[0-9]+}', 'GateDtlController@show');
        // Store
        $api->post('/gate-dtls/{gateId:[0-9]+}', 'GateDtlController@store');
        // Update
        $api->put('/gate-dtls/{ccId:[0-9]+}', 'GateDtlController@update');

        // Report
        $api->get('/gate-dtls/report', 'GateDtlController@reportGateDtl');
        // Gate List
        $api->get('/list', 'GateController@index');

    });
});

$api->version('v1', [], function ($api) {
    $api->group(['prefix' => 'mq', 'namespace' => 'App\Api\MQ\Controllers'], function ($api) {

        include_once 'MQ/routes.php';

    });
});