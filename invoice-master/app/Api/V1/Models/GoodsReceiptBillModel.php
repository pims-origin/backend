<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\BillableHdrModel;
use App\Api\V1\Models\BillableDtlModel;
use Seldat\Wms2\Models\AsnDtl;

use Illuminate\Support\Facades\DB;

class GoodsReceiptBillModel extends AbstractModel
{
    protected $grHdrId;
    protected $cusId;
    protected $whsId;
    protected $type;
    protected $quantity;
    protected $amount;
    protected $billableHdr;
    protected $billableDtl;
    protected $grHdrObj;

    public function __construct($cusId, $whsId, $grHdrId)
    {
        $this->grHdrId = $grHdrId;
        $this->grHdrObj = $this->getGrHdrObj();
        $this->cusId = $this->grHdrObj->cus_id;
        $this->whsId = $this->grHdrObj->whs_id;
        $this->refNum = $this->getRefNum();
        $this->type = 'INB'; // Inbound
        $this->billableHdr = new BillableHdrModel();
        $this->billableDtl = new BillableDtlModel();
    }

    public function getRefNum()
    {
        return DB::table('gr_hdr')
            ->select('gr_hdr_num')
            ->where('gr_hdr_id', $this->grHdrId)
            ->first()->gr_hdr_num;
    }

    public function getCtnrFeet()
    {
        return DB::table('gr_hdr')
            ->select('container.value as value')
            ->join('container', 'gr_hdr.ctnr_id', 'container.ctnr_id')
            ->where('gr_hdr.gr_hdr_id', $this->grHdrId)
            ->first()->value;
    }

    public function getChargeCodeList()
    {
        $items = DB::table('cus_sac')
            ->select(DB::raw('sac_id, sac, sac_name, range_from, range_to, unit_uom, price, discount, ROUND((price * (100 - discount)/100), 2) AS discount_price, block, range_uom, LOCATE("DOMESTIC", description) as domestic'))
            ->where([
                ['cus_id', $this->cusId],
                ['whs_id', $this->whsId],
                ['type', $this->type],
                ['deleted', 0],
            ])
            ->get();

        return $items;
    }

    public function getItem()
    {
        return DB::table('gr_dtl')
            ->select(DB::raw('gr_dtl_id, gr_dtl_act_ctn_ttl as ctn, gr_dtl_plt_ttl as pl, gr_dtl_act_qty_ttl as pc, updated_at, item_id, asn_dtl_id'))
            ->where('gr_hdr_id', $this->grHdrId)
            ->where('gr_dtl_sts', 'RE')
            ->get();
    }

    public function getGrHdrObj()
    {
        return DB::table('gr_hdr')
            ->select(DB::raw('asn_hdr.asn_hdr_num, gr_hdr.ctnr_num, gr_hdr.whs_id, gr_hdr.cus_id, gr_hdr.pallet_ttl, gr_hdr.gr_type,  gr_hdr.cube, gr_hdr.ctnr_size'))
            ->where('gr_hdr.gr_hdr_id', $this->grHdrId)
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'gr_hdr.asn_hdr_id')
            ->first();
    }

    public function generate()
    {
        $items = $this->getItem();

        if (!$items->count()) {
            return [];
        }

        $charges = $this->getChargeCodeList();

        if (!$charges->count()) {
            return [];
        }

        $charges = $this->convertChargeCode($charges);
        $data = [];
        foreach ($items as $item) {
            $cube = AsnDtl::findOrFail($item->asn_dtl_id)->asn_dtl_cube ?? 0;
            $weight = AsnDtl::findOrFail($item->asn_dtl_id)->asn_dtl_weight ?? 0;
            $data['CTN'][] = $item->ctn; //carton
            $data['PL'][] = $item->pl; // pallet
            $data['PC'][] = $item->pc ?? 0; // piece
            $data['UNI'][] = $item->pc ?? 0; // unit == piece
            $data['CUB'][] = $cube; // unit == piece
            $data['KG'][] = $weight; // unit == piece
        }
        $data['CTNR'] = $this->getCtnrFeet() ?? 0; // container
        $detail = [];
        $i = 0;
        foreach ($charges as $charge) {
            $qty = 0;
            //[W360-551]Ignore INB_CUBIC FEET IF IMPORTED charge code if complete GR on WEB
            if ($charge->sac == 'I020') {
                continue;
            }

            if ($charge->sac == 'I025') {
                foreach($this->countIBLabor($charge) as $labor) {
                    $qty = round($labor['qty'], 4);
                    if ($qty > 0) {
                        $detail[$i] = [
                            'ba_id' => '',
                            'cus_id' => $this->cusId,
                            'whs_id' => $this->whsId,
                            'ba_type' => $this->type,
                            'sac_id' => $charge->sac_id,
                            'sac' => $charge->sac,
                            'sac_name' => $charge->sac_name,
                            'ref_num' => $this->refNum,
                            'qty' => $qty,
                            'unit_price' => $charge->price,
                            'discount' => $charge->discount,
                            'amount' => $qty * $charge->discount_price,
                            'item' => $this->grHdrObj->asn_hdr_num,
                            'description' => $this->grHdrObj->ctnr_num,
                            'ba_user_id' => $labor['user_id']
                        ];
                        $this->quantity += $qty;
                        $this->amount += $detail[$i]['amount'];
                        $i++;
                    }
                }

                continue;
            }


            if ($charge->unit_uom == 'CTN') {
                if($charge->range_uom == 'SKU') {
                    $qty = $this->groupCtnBySku($data['CUB'], $data['CTN'], $charge->range_from, $charge->range_to, $this->grHdrObj->cube);
                } else {
                    $qty = $this->groupQtyBasedOnRangeUom($data['CTN'], $charge->range_from, $charge->range_to);
                }
            }

            if ($charge->unit_uom == 'PL') {
                if ($this->grHdrObj->gr_type == 'TRK' && $charge->domestic > 0) {
                    $qty = $this->getTotalPallet();
                }

                if ($this->grHdrObj->gr_type !== 'TRK' && $charge->domestic < 1) {
                    $qty = $this->getTotalPallet();
                }
            }

            if ($charge->unit_uom == 'PC') {
                $qty = $this->groupQtyBasedOnRangeUom($data['PC'], $charge->range_from, $charge->range_to);
            }

            if ($charge->unit_uom == 'UNI') {
                $qty = $this->groupQtyBasedOnRangeUom($data['UNI'], $charge->range_from, $charge->range_to);
            }

            if (strtolower($charge->unit_uom) == strtolower($this->grHdrObj->gr_type)) {
                $containerData = $this->grHdrObj->ctnr_size;
                if (strpos($charge->sac_name, 'INB_CONT SKU CHARGE') !== false) {
                    $skuQty = count(array_unique(array_column($items->toArray(), 'item_id')));
                    $containerData = $skuQty;
                }
                $qty = $this->applyChargeCodeContainer($containerData, $charge->range_from, $charge->range_to);
            }

            if ($charge->unit_uom == 'CUB') {
                $qty = $this->grHdrObj->cube ?? $this->groupCubeQtyBasedOnRangeUom($data['CUB']);
            }

            if ($charge->unit_uom == 'KG') {
                $qty = $this->groupCubeQtyBasedOnRangeUom($data['KG']);
            }
            $qty = round($qty, 4);
            if ($qty > 0) {
                $detail[$i] = [
                    'ba_id' => '',
                    'cus_id' => $this->cusId,
                    'whs_id' => $this->whsId,
                    'ba_type' => $this->type,
                    'sac_id' => $charge->sac_id,
                    'sac' => $charge->sac,
                    'sac_name' => $charge->sac_name,
                    'ref_num' => $this->refNum,
                    'qty' => $qty,
                    'unit_price' => $charge->price,
                    'discount' => $charge->discount,
                    'amount' => $qty * $charge->discount_price,
                    'item' => $this->grHdrObj->asn_hdr_num,
                    'description' => $this->grHdrObj->ctnr_num
                ];
                $this->quantity += $qty;
                $this->amount += $detail[$i]['amount'];
                $i++;
            }
        }

        return $detail;
    }

    public function countIBLabor($charge)
    {
        $labor = DB::table('rpt_labor_tracking')
            ->select([
                'rpt_labor_tracking.user_id',
                'rpt_labor_tracking.owner',
                'rpt_labor_tracking.trans_num',
                DB::raw('SUM(end_time - start_time) as total_time')
            ])
            ->leftJoin('users', 'users.user_id', '=', 'rpt_labor_tracking.user_id')
            ->where('whs_id', $this->whsId)
            ->where('trans_num', $this->refNum)
            ->where('lt_type', 'IB')
            ->whereNotNull('end_time')
            ->groupBy('user_id')
            ->get();

        $result = [];
        if(!empty($labor)) {
            foreach($labor as $lb) {
                $result[] = [
                    'qty' => $lb->total_time/3600,
                    'user_id' => $lb->user_id
                ];
            }
        }

        return $result;
    }

    public function applyChargeCodeContainer($data, $from = null, $to = null)
    {
        $total = 0;
        $from = $from ?? 0;
        $to = $to ?? 0;
        if ($from == 0 && $to == 0) {
            $total = 1;   
        }

        if ($data == $from && $data == $to) {
            $total = 1;
        }

        if ($data >= $from && $data <= $to) {
            $total = 1;
        }

        if ($data >= $from && $to == 0) {
            $total = 1;
        }

        return $total;
    }
    public function groupQtyBasedOnRangeUom($data, $from = null, $to = null)
    {
        $total = 0;
        $from = $from ?? 0;
        $to = $to ?? 0;
        foreach ($data as $qty) {
            if ($from == $to) {
                $total += $qty;
            } else if ($from == 0) {
                if ($qty <= $to) {
                    $total += $qty - $from;
                } else {
                    $total += $to;
                }                
            } else if ($from != 0 && $qty >= $from) {
                if ($to != 0 && $qty >= $to) {
                    $total += $to - $from + 1;
                }

                if ($to != 0 && $qty < $to) {
                    $total += $qty - $from + 1;
                }
                
                if ($to == 0) {
                    $total += $qty - $from + 1;
                }
            }
        }

        return $total;
    }

    public function groupCtnBySku($cube, $data, $from = null, $to = null, $masterCube)
    {
        $total = 0;

        if (!empty($masterCube)) {
            $checkRangeFrom = !is_null($from) ? $masterCube >= $from : true;
            $checkRangeTo = !is_null($to) ? $masterCube <= $to : true;
            if (($checkRangeFrom && $checkRangeTo) || $from == $to) {
                $total = array_sum($data);
            }
        } else {
            foreach ($data as $key => $qty) {
                $checkRangeFrom = !is_null($from) ? $cube[$key] >= $from : true;
                $checkRangeTo = !is_null($to) ? $cube[$key] <= $to : true;
                if (($checkRangeFrom && $checkRangeTo) || $from == $to) {
                    $total += $qty;
                }
            }
        }

        return $total;
    }

    public function groupPalletQtyBasedOnRangeUom($data, $from = 0, $to = 0)
    {
        $total = 0;
        $from = $from ?? 0;
        $to = $to ?? 0;
        foreach ($data as $qty) {
            if ($from == $to) {
                $total += $qty;
            }
        }

        return $total;
    }

    public function groupCubeQtyBasedOnRangeUom($data, $from = 0, $to = 0)
    {
        $total = 0;
        $from = $from ?? 0;
        $to = $to ?? 0;
        foreach ($data as $qty) {
            if ($from == $to) {
                $total += $qty;
            }
        }

        return $total;
    }

    public function groupKgQtyBasedOnRangeUom($data, $from = 0, $to = 0)
    {
        $total = 0;
        $from = $from ?? 0;
        $to = $to ?? 0;
        foreach ($data as $qty) {
            if ($from == $to) {
                $total += $qty;
            }
        }

        return $total;
    }

    public function prepareHeaderData()
    {
        # billable header
        return [
            'cus_id' => $this->cusId,
            'whs_id' => $this->whsId,
            'ref_num' => $this->refNum, //gr_hdr_num
            'ba_from' => time(),
            'ba_to' => time(),
            'ba_ttl' => $this->quantity,
            'amount_ttl' => $this->amount,
            'ba_type' => $this->type,
            'ba_period' => 'DAY',
        ];
    }

    public function prepareDetailData($items, $headerId)
    {
        # billalbe detail
        foreach ($items as &$item) {
            $item['ba_id'] = $headerId;
        }
        return $items;
    }
    
    public function saveBillableItem()
    {
        $detail = $this->generate();
        if(!empty($detail)) {
            $headerData = $this->prepareHeaderData();
            return DB::transaction(function() use($detail, $headerData){
                $header = $this->billableHdr->create($headerData);
                $header->detail()->createMany($detail);
                return $header;
            });
        }
    }

    public function convertChargeCode($charges)
    {
        $charges = $charges->each(function ($item, $key){
            if ($item->block) {
                $item = data_fill($item, 'range_from', $item->block);
                $item = data_fill($item, 'range_to', $item->block);
            }
        });
        return $charges;
    }

    public function getTotalPallet()
    {
        $total = $this->grHdrObj->pallet_ttl;
        if (!$total) {
            $total = DB::table('pallet')
                ->where('pallet.gr_hdr_id', $this->grHdrId)
                ->count();
        }

        return $total;
    }
}
