<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class GetTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->call('GET', '/v1/sample');

        $this->assertJson($response->getContent());
    }
}
