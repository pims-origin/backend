<?php

return [
    'item_status' => [
        'ACTIVE'   => 'AC',
        'INACTIVE' => 'IA',
    ],
];