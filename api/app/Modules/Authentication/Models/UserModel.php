<?php
namespace App\Modules\Authentication\Models;

use App\Modules\Authentication\Eloquent\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserModel
{
    protected $model;

    public function __construct(User $model = null)
    {
        $this->model = ($model) ?: new User();
    }

    public function getUserbyUsername($username) {
        $result = DB::table($this->model->getTable())->where('username',$username)->first();
        return $result;
    }

}