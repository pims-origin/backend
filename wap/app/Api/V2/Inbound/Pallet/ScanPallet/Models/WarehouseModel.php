<?php

namespace App\Api\V2\Inbound\Pallet\ScanPallet\Models;

use Seldat\Wms2\Models\Warehouse;

class WarehouseModel extends AbstractModel
{
    protected $model;

    public function __construct(Warehouse $model = null)
    {
        $this->model = ($model) ?: new Warehouse();
    }


}
