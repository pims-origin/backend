<?php
/**
 * Created by PhpStorm.
 *
 * Date: loc_id-July-16
 * Time: 10:00
 */

namespace App\Api\OUTBOUND\Controllers;

use App\Api\OUTBOUND\Transformers\ActiveLocationTransformer;
use App\Api\OUTBOUND\Transformers\MoreLocationTransformer;
use App\libraries\RFIDValidate;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\OUTBOUND\Models\CartonModel;
use App\Api\OUTBOUND\Models\LocationModel;
use App\Api\OUTBOUND\Models\WavePickDtlModel;
use Seldat\Wms2\Utils\Status;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Seldat\Wms2\Utils\Message;
use TCPDF;
use App\Api\V1\Models\Log;
use App\MyHelper;

/**
 * Class PalletController
 *
 * @package App\Api\V1\Controllers
 */
class LocationController extends AbstractController
{
    protected $locationModel;
    protected $cartonModel;
    protected $wavePickDtlModel;

    public function __construct(
        CartonModel $cartonModel,
        LocationModel $locationModel,
        WavePickDtlModel $wavePickDtlModel
    ) {
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->wavePickDtlModel = $wavePickDtlModel;
    }

    public function activeLocation(Request $request, $whsId, $wv_dtl_id)
    {
        $input = $request->getParsedBody();
        $loc_rfid = array_get($input, 'loc_rfid', null);

        /*
         * start logs
         */
        MyHelper::log("Function : " . __FUNCTION__, $input);

        $url = '/whs/{$whsId}/wave/{$wv_dtl_id}/active-location';
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'AL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Active Location'
        ]);
        /*
         * end logs
         */

        //validate location RFID
        $locRFIDValid = new RFIDValidate($loc_rfid, RFIDValidate::TYPE_LOCATION, $whsId);
        if (! $locRFIDValid->validate()) {
            $data = [
                'data' => null,
                'message' => $locRFIDValid->error,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        // check exists wave pick detail
        $existWvPickDtl = (new WavePickDtlModel())->getFirstWhere([
                                                                    'whs_id'    => $whsId,
                                                                    'wv_dtl_id' => $wv_dtl_id,
                                                                    ]);
        if (!$existWvPickDtl) {
            $msg = sprintf("The wave pick detail %s doesn't exist", $wv_dtl_id);
            $data = [
                'data'    => $wv_dtl_id,
                'message' => $msg,
                'status'  => false,
            ];
            return new Response($data, 200, [], null);
        }

        try {
            $result = $this->locationModel->getActiveLocation($whsId, $wv_dtl_id, $loc_rfid);
            // $arrLocation = $result->toArray();
            // if( empty($arrLocation) ){
            //     $result = $this->locationModel->getActiveLocationWithoutWavePick($whsId, $loc_rfid);
            // }

            $arrLocation = $result->toArray();
            if( empty($arrLocation) ){
                $msg = sprintf('The location rfid %s is not found', $loc_rfid);
                $data = [
                    'data' => null,
                    'message' => $msg,
                    'status' => false,
                ];
                return new Response($data, 200, [], null);
            }

            return $this->response->collection($result, new ActiveLocationTransformer())
                ->setStatusCode(Response::HTTP_OK);;

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * This api called for show location incorrect on wavepick detail outbound page
     *
     * @param Request $request
     * @param Integer $whsId
     *
     * @return Response
     */
    public function getLocationWhenWrong(Request $request, $whsId)
    {
        $input = $request->getParsedBody();
        $locRfid = array_get($input, 'loc_rfid', null);

        /*
         * start logs
         */
        MyHelper::log("Function : " . __FUNCTION__, $input);

        $url = '/whs/{whsId:[0-9]+}/location-when-wrong';
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'LWW',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get location when forklift put wrong location'
        ]);

        //validate location RFID
        $locRFIDValid = new RFIDValidate($locRfid, RFIDValidate::TYPE_LOCATION, $whsId);
        if (! $locRFIDValid->validate()) {
            $data = [
                'data' => null,
                'message' => $locRFIDValid->error,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        try {
            $result = $this->locationModel->getExistLocationForWavePickWhenWrong($whsId, $locRfid);

            $arrLocation = $result->toArray();
            if( empty($arrLocation) ){
                $msg = sprintf('The location rfid %s is not found', $locRfid);
                $data = [
                    'data' => null,
                    'message' => $msg,
                    'status' => false,
                ];
                return new Response($data, 200, [], null);
            }

            return $this->response->collection($result, new ActiveLocationTransformer())
                ->setStatusCode(Response::HTTP_OK);;

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function moreLocation(Request $request, $whsId, $wv_dtl_id)
    {
        $input = $request->getParsedBody();

        $loc_ids = array_get($input, 'loc_ids', null);

        /*
         * start logs
         */
        MyHelper::log("Function : " . __FUNCTION__, $input);

        $url = '/whs/{$whsId}/wave/{$wv_dtl_id}/more-location';
        $owner = "";
        $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'ML',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'More location'
        ]);
        /*
         * end logs
         */

        if (!isset($input['loc_ids']) || empty($input['loc_ids'])) {
            $msg = "The loc_ids input is required!";
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        $arrInvalid = [];
        foreach ($input['loc_ids'] as $item) {
            if (!is_numeric($item)) {
                $arrInvalid[] = $item;
            }
        }
        if (!empty($arrInvalid)) {
            $msg = "The loc_ids input is invalid!";
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        $checkWvDtl = $this->wavePickDtlModel->getModel()->where([
                                                                "whs_id"    => $whsId,
                                                                "wv_dtl_id" => $wv_dtl_id,
                                                            ])->first();
        if (!$checkWvDtl) {
            $msg = sprintf("Wavepick detail %d doesn't exist!", $wv_dtl_id);
            $checkWvDtl = $this->wavePickDtlModel->getModel()->where([
                                                                "wv_dtl_id" => $wv_dtl_id,
                                                            ])->first();
            if ($checkWvDtl) {
                $msg = sprintf("Wavepick detail of %s doesn't belong to warehouse!", object_get($checkWvDtl, 'wv_num'));
            }
            $data = [
                'data'    => $wv_dtl_id,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            $result = $this->cartonModel->getMoreLocation($whsId, $wv_dtl_id, $loc_ids);
            if (!$result) {
                    $msg = "No more location for suggestion!";
                    $data = [
                        'data'    => null,
                        'message' => $msg,
                        'status'  => false,
                    ];

                    return new Response($data, 200, [], null);
                }
            $locs = [];
            foreach ($result as $locObj) {
                $getLocId = array_get($locObj, 'loc_id', null);
                $locs[] = [
                    'loc_id'         => $getLocId,
                    'loc_code'       => array_get($locObj, 'loc_code', null),
                    'loc_rfid'       => array_get($locObj, 'loc_rfid', null),
                    'sku'            => array_get($locObj, 'sku', null),
                    'size'           => array_get($locObj, 'size', null),
                    'color'          => array_get($locObj, 'color', null),
                    'lot'            => array_get($locObj, 'lot', null),
                    'item_id'        => array_get($locObj, 'item_id', null),
                    'cartons'        => array_get($locObj, 'cartons', null),
                    'pieces'         => array_get($locObj, 'avail_qty', null),
                    'is_cycle_count' => $this->_isCycleCount($whsId, $getLocId),
                    'is_rfid'        => $this->_isRfid($whsId, $getLocId),
                ];
            }

            return ['data' => $locs];

            //return $result;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    private function _isCycleCount($whsId, $locId)
    {
        $query = DB::table('cc_notification')->where([
                    'whs_id'  => $whsId,
                    'loc_id'  => $locId,
                    'deleted' => 0,
                ])
                ->whereIn('cc_ntf_sts', ['NW'])
                ->first();

        return $query ? true : false;
    }

    private function _isRfid($whsId, $locId)
    {
        $query = DB::table('cartons')->where([
                    'whs_id'  => $whsId,
                    'loc_id'  => $locId,
                    'deleted' => 0,
                ])
                ->whereNotNull('rfid')
                ->where('ctn_sts', '!=', 'AJ')
                ->first();

        return $query ? true : false;
    }
}
