<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 20-Jul-2016
 * Time: 2:50
 */

namespace App\Api\V1\Validators;


class DamageTypeValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'dmg_code'        => 'required',
            'dmg_name'        => 'required',
        ];
    }
}