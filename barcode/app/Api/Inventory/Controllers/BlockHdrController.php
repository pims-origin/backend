<?php

namespace App\Api\Inventory\Controllers;

use App\Api\Inventory\Models\BlockDtlModel;
use App\Api\Inventory\Models\BlockHdrModel;
use App\Api\Inventory\Models\CycleDtlModel;
use App\Api\Inventory\Models\CycleHdrModel;
use App\Api\Inventory\Models\EventTrackingModel;
use App\Api\Inventory\Models\LocationModel;
use App\Api\Inventory\Models\LockModel;
use App\Api\Inventory\Services\CycleCountService;
use App\Api\Inventory\Transformers\BlockHdrTransformer;
use App\Api\Inventory\Validators\BlockHdrValidator;
use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\View;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\Inventory\Validators\CycleHdrValidator;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\Inventory\Transformers\CycleHdrTransformer;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\Inventory\Validators\CusInUserValidator;
use Wms2\UserInfo\Data;

class BlockHdrController extends AbstractController
{
    protected $model;
    protected $validator;
    protected $blockHdrTransform;
    protected $eventTrackingModel;
    protected $locationModel;


    /**
     * BlockHdrController constructor.
     *
     * @param BlockHdrModel $model
     * @param BlockHdrValidator $validator
     * @param EventTrackingModel $eventTrackingModel
     */

    public function __construct(
        BlockHdrModel $model,
        LocationModel $locationModel,
        BlockHdrValidator $validator,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->model = $model;
        $this->validator = $validator;
        $this->blockHdrTransform = new BlockHdrTransformer();
        $this->locationModel = $locationModel;
        $this->eventTrackingModel = $eventTrackingModel;
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */

    public function store(Request $request)
    {
        $input = $request->getParsedBody();

        $this->validator->validate($input);

        $data = $this->model->processCreateBlockHdr($input);

        if (!$data) {
            return $this->response->errorBadRequest(implode($this->model->errors));
        }

        return $this->response->item($data, $this->blockHdrTransform);
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|string
     */

    public function index(Request $request)
    {
        $input = $request->getQueryParams();

        try {

            if (!isset($input['sort'])) {
                $input['sort'] = [
                    'block_hdr_id' => 'desc'
                ];
            }

            $data = $this->model->search($input, ['warehouse', 'createdBy'], array_get($input, 'limit', 20));

            return $this->response->paginator($data, $this->blockHdrTransform);

        } catch (\Exception $e) {

            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);

        }
    }

    /**
     * @param $blockHdrId
     */
    public function view($blockHdrId)
    {
        try {
            $data = $this->model->getBlockStockInfo($blockHdrId);

            return $data ? $data->toArray() : null;

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param Request $request
     * @param $blockHdrId
     *
     * @return array|void
     * @throws \Exception
     */
    public function unblock(Request $request, $blockHdrId)
    {
        $input = $request->getParsedBody();
        $blkDtlIds = $input['block_dtl_ids'];
        $userInfo = new Data();

        $acSts = BlockDtlModel::STATUS_ITEM_ACCEPTED;
        $lkSts = BlockDtlModel::STATUS_ITEM_BLOCKED;

        //validate blockDtlId
        $valid = $this->validateBlkDtl($blkDtlIds);
        if (!$valid) {
            return $this->response->errorBadRequest("Block Detail Ids Invalid");
        }

        $blkHdrObj = $this->model->byId($blockHdrId);

        $query = \DB::table('block_dtl')
            ->where('deleted', 0)
            ->where('block_hdr_id', $blockHdrId)
            ->whereIn('block_dtl_id', $blkDtlIds);

        $blkDtl = $query->pluck('item_id');
        $locIds = array_unique($query->pluck('loc_id'));

        $whsId = $blkHdrObj->whs_id;

        if (!$blkDtl) {
            return $this->response->errorBadRequest('Block detail not existed');
        }

        try {
            \DB::beginTransaction();

            $itemIds = array_unique($blkDtl);
            $query = \DB::table('block_dtl')
                ->where('deleted', 0)
                ->where('whs_id', $whsId)
                ->where('block_hdr_id', $blockHdrId)
                ->where('block_dtl_sts', $lkSts)
                ->whereIn('item_id', $itemIds);
            $lockItemIds = $query->pluck('item_id');

            //update block dtl sts
            \DB::table('block_dtl')
                ->whereIn('block_dtl_id', $blkDtlIds)
                ->update(['block_dtl_sts' => $acSts]);

            //process unlock cartons

            $lockItemIds = array_unique($lockItemIds);
            $unlockItemIds = array_intersect($itemIds, $lockItemIds);

            if (!empty($unlockItemIds)) {
                \DB::table("cartons")
                    ->whereIn('item_id', $unlockItemIds)
                    ->where("whs_id", $whsId)
                    ->where("ctn_sts", $lkSts)
                    ->update(["ctn_sts" => $acSts]);
            }

            //process unclock locations

            $sql = sprintf("(SELECT COUNT(1) FROM cartons WHERE cartons.whs_id = %d AND cartons.ctn_sts = 'LK' AND cartons.loc_id = location.loc_id) = 0",
                $whsId, implode
                ($itemIds));

            $this->locationModel->getModel()
                ->whereIn('loc_id', $locIds)
                ->whereraw(
                    $sql
                )->update(
                    ['loc_sts_code' => 'AC']
                );

            //process update sts block_hdr
            $chk = \DB::table('block_dtl')
                ->where('block_hdr_id', $blockHdrId)
                ->where('block_dtl_sts', $lkSts)
                ->where('deleted', 0)
                ->count();

            if ($chk === 0 AND $blkHdrObj->block_sts === BlockHdrModel::STATUS_BLOCKED) {
                $blkHdrObj->block_sts = BlockHdrModel::STATUS_UNBLOCKED;
                $blkHdrObj->save();

                //process block_hdr detail
                $this->processBlkHdrDtl($blkHdrObj, $whsId);
            }

            \DB::commit();

            return ["data" => "Unclock successful"];
        } catch (\Exception $ex) {
            \DB::rollback();
            throw new \Exception($ex->getMessage());
        }
    }

    protected function processBlkHdrDtl($blkHdrObj, $whsId)
    {
        $acSts = LockModel::STATUS_ACTIVE;
        $lcSts = LockModel::STATUS_LOCK;
        $unClkLocs = '';
        $condition = '';
        if (!empty($blkHdrObj->block_detail)) {
            if ($blkHdrObj->block_type === BlockHdrModel::BLOCK_TYPE_LOCATION) {
                $unClkLocs = $blkHdrObj->block_detail;
                $condition = 'loc_id';
            }

            if ($blkHdrObj->block_type === BlockHdrModel::BLOCK_TYPE_CUSTOMER) {
                $cusId = (int)$blkHdrObj->block_detail;
                $unClkLocs = "
                    SELECT z.zone_id
                    FROM customer_zone cz
                    INNER JOIN zone z ON z.zone_id = cz.zone_id
                    where cz.cus_id = $cusId 
                        AND cz.deleted = 0
                        AND z.deleted = 0
                        AND z.zone_whs_id = $whsId
                ";

                $condition = 'loc_zone_id';
            }

            //run update
            if (!empty($condition)) {
                $query = "
                    UPDATE location 
                    SET loc_sts_code = '$acSts'
                    WHERE $condition IN ($unClkLocs) AND loc_sts_code = '$lcSts';
                ";

                \DB::update($query);

                return true;
            }
        }

        return false;
    }

    protected function validateBlkDtl($blkDtlIds)
    {
        foreach ($blkDtlIds as $v) {
            if (!is_numeric($v)) {
                return false;
            }
        }

        return true;
    }

}
