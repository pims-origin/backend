<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class DailyInventoryRptInsert extends AbstractCommand
{
    protected $signature   = 'daily-inventory-rpt-insert {today?}';
    protected $description = 'Daily Inventory Report Insert.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {
            $this->logSuccess('start-daily-inventory-rpt-insert');
            $today     = $this->argument('today') ? strtotime($this->argument('today')) : strtotime(date('Y-m-d', time()));
            $yesterday = ($today - 86400);

            $customerWhss = DB::table('customer_warehouse AS cw')
                ->select('cw.cus_id', 'cw.whs_id')
                ->where("cw.deleted", 0)
                ->orderBy('whs_id')
                ->get();

            if ($customerWhss) {
                foreach ($customerWhss as $cusWhs) {
                    $cusId = $cusWhs['cus_id'];
                    $whsId = $cusWhs['whs_id'];
                    $this->insertDailyInventoryReport($cusId, $whsId, $today, $yesterday);
                }
            }

            $this->logSuccess('End daily-inventory-rpt-insert');
        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }

    }

    private function insertDailyInventoryReport($cusId, $whsId, $today, $yesterday)
    {
        return DB::transaction(function () use ($cusId, $whsId, $today, $yesterday) {
            $inventories = DB::table('daily_inv_rpt as i')->select(
                'i.whs_id',
                'i.cus_id',
                'i.item_id',
                'i.description',
                'i.sku',
                'i.size',
                'i.color',
                'i.pack',
                'i.uom',
                'i.lot',
                'i.is_rack',
                DB::raw($today . ' as inv_dt'),
                'i.cur_ctns as init_ctns',
                'i.cur_qty as init_qty',
                'i.cur_plt as init_plt',
                'i.cur_ctns',
                'i.cur_qty',
                'i.cur_plt',
                'i.cur_volume',
                DB::raw(time() . ' as created_at'),
                DB::raw(time() . ' as updated_at'),
                'i.ctn_type'
            )
                ->where('i.whs_id', '=', $whsId)
                ->where('i.cus_id', '=', $cusId)
                ->where('i.inv_dt', '=', $yesterday)
                ->whereNotExists(function ($q) use ($today) {
                    $q->select(DB::raw(1))->from('daily_inv_rpt as c')
                        ->whereRaw('i.whs_id = c.whs_id AND i.item_id = c.item_id AND i.is_rack = c.is_rack')
                        ->where('c.inv_dt', '=', $today);
                })
                /* This chunk wrong because it effect the master query because the whereNotExists condition
                 * ->chunk(100, function ($inventories) {
                    return DB::table('daily_inv_rpt')->insert($inventories);
                })*/
                ->get();
            DB::table('daily_inv_rpt')->insert($inventories);
        });
    }
}
