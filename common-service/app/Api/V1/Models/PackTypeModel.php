<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\PackType;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class PackTypeModel extends AbstractModel
{
    public function __construct(PackType $model = null)
    {
        $this->model = ($model) ?: new PackType();
    }




}
