<?php

namespace App\Api\V1\Validators;

class WavePickDtlLocValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_id'     => 'required|integer',
            'picked_qty' => 'required|integer'
        ];
    }
}
