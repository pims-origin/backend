<?php

namespace App\Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderShippedCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => OrderShippedResource::collection($this->collection)
        ];
    }
}