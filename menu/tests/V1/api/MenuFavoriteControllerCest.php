<?php

use App\MenuGroup;
use App\MenuFavorite;
use App\Menu;
use Illuminate\Http\Response as IlluminateResponse;

class MenuFavoriteControllerCest
{
//    protected $endpoint = '/v1';
//
//    /**
//     * @var array
//     */
//    protected $dataUserId = [
//        'logic' => 999999999,
//        'type' => ['1'],
//        'empty' => '',
//        'null' => null,
//        'max' => 9999999999999999999999,
//        'min' => -1,
//    ];
//
//    /**
//     * @var array
//     */
//    protected $dataMenuId = [
//        'logic' => 99999999,
//        'type' => ['1'],
//        'empty' => '',
//        'null' => null,
//        'max' => 9999999999999999999999,
//        'min' => -1,
//    ];
//
//    /**
//     * @var array
//     */
//    protected $dataDisplayOrder = [
//        'logic' => 999999,
//        'type' => ['1'],
//        'empty' => '',
//        'null' => null,
//        'max' => 9999999999999999999999,
//        'min' => -1,
//    ];
//
//    public function listFavorite_Ok(ApiTester $I, MenuFavorite $menuFavorite)
//    {
//        $userId = 1;
//
//        $uri = $this->endpoint . "/favorites/$userId";
//        $I->sendGET($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//    }
//
//    public function storeFavorite_AllCase_Fail(ApiTester $I)
//    {
//        $userId = 1;
//        $uri = $this->endpoint . "/favorites/$userId";
//
//        foreach ($this->dataUserId as $keyUserId => $valueUserId) {
//            foreach ($this->dataMenuId as $keyMenuId => $valueMenuId) {
//                foreach ($this->dataDisplayOrder as $keyDisplayOrder => $valueDisplayOrder) {
//                    $I->sendPOST($uri, [
//                        'user_id' => $valueUserId,
//                        'menu_id' => $valueMenuId,
//                        'display_order' => $valueDisplayOrder,
//                    ]);
//
//                    $I->seeResponseCodeIs(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
//                }
//            }
//        }
//    }
//
//
//    public function destroyFavorite_Ok(ApiTester $I, Menu $menu, MenuGroup $menuGroup, MenuFavorite $menuFavorite)
//    {
//        $userId = 1;
//
//        // Add Menu Group
//        $paramMenuGroup = [
//            'name' => 'Name Test',
//            'description' => 'Description Test'
//        ];
//        $menuGroupId = $I->haveRecord($menuGroup->getTable(), $paramMenuGroup);
//
//        // Add Menu
//        $paramMenu = [
//            'name' => 'Name Test',
//            'menu_group_id' => $menuGroupId,
//            'display_order' => 1,
//        ];
//        $menuId = $I->haveRecord($menu->getTable(), $paramMenu);
//
//        // Add Menu Favorite
//        $paramMenuFavorite = [
//            'user_id' => 1,
//            'menu_id' => $menuId,
//            'display_order' => 1,
//        ];
//
//        $I->haveRecordNoPrimary($menuFavorite->getTable(), $paramMenuFavorite);
//
//        $uri = $this->endpoint . "/favorites/$userId/$menuId";
//        $I->sendDELETE($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_NO_CONTENT);
//    }
//
//    public function destroyFavorite_NotExist_Fail(ApiTester $I)
//    {
//        $uri = $this->endpoint . '/favorite/999999/999999';
//        $I->sendDELETE($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_NOT_FOUND);
//    }
}