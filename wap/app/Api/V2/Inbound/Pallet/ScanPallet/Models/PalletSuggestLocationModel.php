<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Inbound\Pallet\ScanPallet\Models;

use Seldat\Wms2\Models\PalletSuggestLocation;

class PalletSuggestLocationModel extends AbstractModel
{
    /**
     * PalletSuggestLocationModel constructor.
     *
     * @param PalletSuggestLocation|null $model
     */
    public function __construct(PalletSuggestLocation $model = null)
    {
        $this->model = ($model) ?: new PalletSuggestLocation();
    }

    /**
     * Create if not exist
     *
     * @param array $data
     *
     * @return bool
     */
    public function replace(array $data)
    {
        $model = $this->model->firstOrNew(['plt_id' => $data['plt_id']]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    public function getPalletSugLocByPltId($pltId, $grDtlId, $grHdrId)
    {
        $res = $this->model
            ->where('plt_id', $pltId)
            ->where('gr_dtl_id', $grDtlId)
            ->where('gr_hdr_id', $grHdrId)
            ->first();

        return $res;
    }
}
