<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\XferDtlModel;
use App\Api\V1\Models\XferHdrModel;
use App\Api\V1\Models\XferTicketModel;
use App\Api\V1\Traits\XferControllerTrait;
use App\Api\V1\Transformers\DetailTicketTransformer;
use App\Api\V1\Transformers\DetailXferTransformer;
use App\Api\V1\Transformers\ListTicketTransformer;
use App\Api\V1\Transformers\ListWxferTransformer;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

class XferController extends AbstractController
{

    protected $xferHdrModel;
    protected $xferDtlModel;
    protected $xferTicketModel;

    protected $cartonModel;

    protected $locModel;

    protected $eventTrackingModel;

    protected $palletModel;

    use XferControllerTrait;

    /**
     * WvTransferController constructor.
     */
    public function __construct()
    {
        $this->cartonModel = new CartonModel();
        $this->locModel = new LocationModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->palletModel = new PalletModel();
        $this->xferHdrModel = new XferHdrModel();
        $this->xferDtlModel = new XferDtlModel();
        $this->xferTicketModel = new XferTicketModel();
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @throws Exception
     */
    public function createXferRecord(Request $request)
    {
        $input = $request->getParsedBody();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $items = $input['items'];
        $ttl_piece = 0;
        foreach ($items as $item) {
            $ttl_piece += $item['ttl_pieces'];
        }

        // Total Pieces cannot be less than Requested Pieces/XFER
        if (!($ttl_piece >= $input['request_qty'])) {
            throw new HttpException(400, Message::get("BM055"));
        }

        // only get enough location for request qty
        $reqQty = $input['request_qty'];
        foreach ($items as $key => $item) {
            if ($reqQty > 0) {
                if ($item['ttl_pieces'] < $reqQty) {
                    $items[$key]['alloc_qty'] = $item['ttl_pieces'];
                    $reqQty -= $item['ttl_pieces'];
                } elseif ($item['ttl_pieces'] >= $reqQty) {
                    $items[$key]['alloc_qty'] = $reqQty;
                    $reqQty = 0;
                }
            } else {
                $items[$key]['alloc_qty'] = 0;
            }
        }

        $this->validateXfer($items, $input, true);

        // Generate wv_tf_num
        $wvTfNum = $this->xferHdrModel->generateWvTfNum();

        DB::beginTransaction();
        // Save xferHdr
        $xferHdr = $this->saveXferHdr($wvTfNum, $currentWH, $input);

        $arrLocCode = [];
        // Save xferDtl
        foreach ($items as $item) {
            if (isset($item['alloc_qty']) && $item['alloc_qty'] > 0) {
                $dataXferDtl = [
                    'whs_id'        => $currentWH,
                    'cus_id'        => $input['cus_id'],
                    'item_id'       => $input['item_id'],
                    'sku'           => $input['sku'],
                    'size'          => $input['size'],
                    'color'         => $input['color'],
                    'lot'           => $input['lot'],
                    'cus_upc'       => $input['cus_upc'],
                    'allocated_qty' => $item['alloc_qty'],
                    'req_loc_id'    => $item['loc_id'],
                    'req_loc'       => $item['loc_code'],
                    'mez_loc_id'    => $input['loc_id'],
                    'mez_loc'       => $input['loc_code'],
                    'xfer_dtl_sts'  => Status::getByValue("Pending", "XFER-STATUS"),
                    'sts'           => 'i',
                    'xfer_hdr_id'   => $xferHdr->xfer_hdr_id
                ];

                $xferHdr = $this->xferDtlModel->saveXferDtl($dataXferDtl);
                array_push($arrLocCode, $item['loc_code']);
            }
        }

        $locCodeStr = implode(", ", $arrLocCode);
        $dataEvtTracking = [
            'to_loc_code' => $locCodeStr,
            'currentWh'   => $currentWH,
            'wv_tf_num'   => $wvTfNum,
        ];

        $this->logEventTrackingWhenCreateXferRecord($dataEvtTracking, $input);
        DB::commit();

        return $this->response
            ->noContent()
            ->setContent("{}")
            ->setStatusCode(IlluminateResponse::HTTP_CREATED);
    }

    /**
     * @param $items
     * @param $input
     * @param $checkCreate
     *
     * @throws Exception
     */
    private function validateXfer($items, $input, $checkCreate)
    {
        // check active location
        foreach ($items as $item) {
            if ($item['loc_sts'] != Status::getByKey("LOCATION_STATUS", "ACTIVE")) {
                throw new HttpException(400, Message::get('BM136'));
            }
        }

        // check to location belong to customer + current warehouse
        $locCusWh = $this->locModel->checkLocationBelongToCustomerAndCurrentWH($input['cus_id'], $input['loc_id']);
        if ($locCusWh < 0) {
            throw new HttpException(400, Message::get("BM017", "To Location"));
        }

        // check sku belong to customer + current warehouse
        $skuCusWh = $this->cartonModel->checkSkuBelongToCustomerAndWH($input['sku'], $input['cus_id']);
        if ($skuCusWh < 0) {
            throw new HttpException(400, Message::get("BM017", "SKU"));
        }

        // check item id belong to customer + current warehouse
        $itemCusWh = $this->cartonModel->checkItemIdBelongToCustomerAndWH($input['item_id'], $input['cus_id']);
        if ($itemCusWh < 0) {
            throw new HttpException(400, Message::get("BM017", "Item Id"));
        }

        // check cus_upc belong to customer + current warehouse
        $cusUpcWh = $this->cartonModel->checkCusUpcBelongToCustomerAndWH($input['cus_upc'], $input['cus_id']);
        if ($cusUpcWh < 0) {
            throw new HttpException(400, Message::get("BM017", "UPC"));
        }

        if ($checkCreate == true) {
            // check item is not complete
            $itemXfer = $this->xferHdrModel->checkXferAlreadyHasItem($input['item_id'], $input['cus_id']);
            if (!empty($itemXfer)) {
                throw new HttpException(400,
                    Message::get("BM060", $input['item_id'], array_get($itemXfer, 'xfer_hdr_num', '')));
            }
        }

        // Only pending can be edited
        if ($checkCreate == false) {
            if ($input['xfer_sts'] == Status::getByValue("Picking", "XFER-STATUS") ||
                $input['xfer_sts'] == Status::getByValue("Complete", "XFER-STATUS")
            ) {
                throw new HttpException(400, Message::get('BM140', 'Pending', 'edited'));
            }
        }

        // Check other locations already has item
        $dataLocCheck = [
            'item_id' => $input['item_id'],
            'loc_id'  => $input['loc_id'],
        ];

        $checkCtn = $this->cartonModel->checkLocationAlreadyHasItem($dataLocCheck, "create");

        if (count($checkCtn) > 0) {
            $arrLoc = [];
            foreach ($checkCtn as $check) {
                array_push($arrLoc, $check['loc_code']);
            }
            $strLoc = implode(', ', $arrLoc);
            throw new HttpException(400, "Item is currently processing in location " . $strLoc);
        }
    }

    /**
     * @param $wvTfNum
     * @param $currentWH
     * @param $input
     *
     * @return static
     */
    private function saveXferHdr($wvTfNum, $currentWH, $input)
    {
        // Save xferHdr
        $dataXferHdr = [
            'xfer_hdr_num' => $wvTfNum,
            'whs_id'       => $currentWH,
            'cus_id'       => $input['cus_id'],
            'item_id'      => $input['item_id'],
            'sku'          => $input['sku'],
            'size'         => $input['size'],
            'color'        => $input['color'],
            'lot'          => $input['lot'],
            'cus_upc'      => $input['cus_upc'],
            'piece_qty'    => $input['request_qty'],
            'xfer_sts'     => Status::getByValue("Pending", "XFER-STATUS"),
            'sts'          => 'i'
        ];
        $xferHdr = $this->xferHdrModel->saveXferHdr($dataXferHdr);

        return $xferHdr;
    }

    /**
     * @param $currentWH
     * @param $input
     *
     * @return mixed
     */
    private function editXferHdr($currentWH, $input)
    {
        // Edit xferHdr
        $dataXferHdr = [
            'whs_id'    => $currentWH,
            'cus_id'    => $input['cus_id'],
            'item_id'   => $input['item_id'],
            'sku'       => $input['sku'],
            'size'      => $input['size'],
            'color'     => $input['color'],
            'lot'       => $input['lot'],
            'cus_upc'   => $input['cus_upc'],
            'piece_qty' => $input['request_qty'],
            'xfer_sts'  => Status::getByValue("Pending", "XFER-STATUS"),
            'sts'       => 'u'
        ];
        $xferHdr = $this->xferHdrModel->editXferHdr($dataXferHdr, $input['xfer_hdr_id']);

        return $xferHdr;
    }

    /**
     * @param $data
     * @param $input
     */
    private function logEventTrackingWhenCreateXferRecord($data, $input)
    {
        $items = $input['items'];
        $arrLocCode = [];
        foreach ($items as $item) {
            array_push($arrLocCode, $item['loc_code']);
        }
        $locCode = implode(", ", $arrLocCode);

        // Event tracking - WHS Transfer Created
        $evt_code = Status::getByKey("event", "NEW-WHS-XFER");
        $info = sprintf(Status::getByKey("event-info", "NEW-WHS-XFER"),
            $input['request_qty'],
            $input['item_id'],
            $input['sku'],
            $locCode,
            $input['loc_code']
        );
        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->create([
            'whs_id'    => $data['currentWh'],
            'cus_id'    => $input['cus_id'],
            'owner'     => $data['wv_tf_num'],
            'evt_code'  => $evt_code,
            'trans_num' => $data['wv_tf_num'],
            'info'      => $info
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createXferTicket(Request $request)
    {
        $input = $request->getParsedBody();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $xfers = $input['xfers'];

        // Validate Xfer Ticket
        $this->validateXferTicket($xfers);

        // Create Xfer Ticket
        $wv_tk_num = $this->xferTicketModel->generateWvTKNum();
        $dataXferTicket = [
            'wv_tk_num'      => $wv_tk_num,
            'currentWH'      => $currentWH,
            'numXferRecords' => count($xfers),
        ];
        $xferTicket = $this->saveXferTicket($dataXferTicket);

        // Update xfer dtl
        $this->updateXferDtl($xfers, $xferTicket);

        $dataXferHdr = [
            'xfers'      => $xfers,
            'xferTicket' => $xferTicket,
            'currentWH'  => $currentWH,
            'wv_tk_num'  => $wv_tk_num
        ];

        // Update Xfer hdr
        $this->updateXferHdr($dataXferHdr);

        // Event tracking for Xfer ticket
        $evt_code = Status::getByKey("event", "NEW-WHS-XFER-TICKET");
        $info = sprintf(Status::getByKey("event-info", "NEW-WHS-XFER-TICKET"), count($xfers));
        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->create([
            'whs_id'    => $currentWH,
            'cus_id'    => null,
            'owner'     => $wv_tk_num,
            'evt_code'  => $evt_code,
            'trans_num' => $wv_tk_num,
            'info'      => $info
        ]);

        // Print PDF File
        $this->printXferPdf($dataXferHdr);

        return $this->response
            ->noContent()
            ->setContent("{}")
            ->setStatusCode(IlluminateResponse::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function downloadTicket(Request $request)
    {
        $input = $request->getParsedBody();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $xferTicketId = $input['xfer_ticket_id'];
        $xferTicketSts = $input['xfer_ticket_sts'];

        // Validate download Xfer Ticket
        $this->validateDownloadXferTicket($xferTicketSts);

        //load xfer ticket info
        $xferTicketInfo = $this->xferTicketModel->getTicketInfoById($xferTicketId);
        $xferHdrLsts = $this->xferHdrModel->getALLXferHdrByTicketId($xferTicketId);

        $xfers['xfers'] = [];

        // Prepare data for download xfer ticket
        foreach ($xferHdrLsts as $xferHdrLst) {
            $dt = [
                'wv_tf_id'  => $xferHdrLst->xfer_hdr_id,
                'wv_tf_sts' => $xferHdrLst->xfer_sts
            ];
            array_push($xfers['xfers'], $dt);
        }

        $dataXferHdr = [
            'xfers'      => $xfers['xfers'],
            'xferTicket' => $xferTicketInfo,
            'currentWH'  => $currentWH,
            'wv_tk_num'  => array_get($xferTicketInfo, 'xfer_ticket_num', null)
        ];

        // Print PDF File
        $this->printXferPdf($dataXferHdr);

        return $this->response
            ->noContent()
            ->setContent("{}")
            ->setStatusCode(IlluminateResponse::HTTP_OK);
    }

    /**
     * @param $xfers
     */
    private function validateXferTicket($xfers)
    {
        // check pending status or not
        foreach ($xfers as $xfer) {
            if ($xfer['wv_tf_sts'] == Status::getByValue("Picking", "XFER-STATUS") ||
                $xfer['wv_tf_sts'] == Status::getByValue("Picking", "XFER-STATUS")
            ) {
                throw new HttpException(400, Message::get("BM056"));
            }
        }
    }

    /**
     * @param $xfersTicketSts
     */
    private function validateDownloadXferTicket($xfersTicketSts)
    {
        // check pending status or not
        if ($xfersTicketSts == Status::getByValue("Pending", "XFER-STATUS")
        ) {
            throw new HttpException(400, Message::get('BM140', 'Picking/Complete', 'downloaded ticket'));
        }
    }

    /**
     * @param $data
     *
     * @return static
     */
    private function saveXferTicket($data)
    {
        $dataXferTicket = [
            'xfer_ticket_num' => $data['wv_tk_num'],
            'whs_id'          => $data['currentWH'],
            'xfer_ttl'        => $data['numXferRecords'],
            'xfer_ticket_sts' => Status::getByValue("Picking", "XFER-STATUS"),
            'sts'             => 'i'
        ];
        $xferTicket = $this->xferTicketModel->saveXferTicket($dataXferTicket);

        return $xferTicket;
    }

    /**
     * @param $xfers
     * @param $xferTicket
     */
    private function updateXferDtl($xfers, $xferTicket)
    {
        foreach ($xfers as $xfer) {
            $dataXferDtl = [
                'xfer_dtl_sts'   => Status::getByValue("Picking", "XFER-STATUS"),
                'sts'            => 'u',
                'xfer_ticket_id' => $xferTicket->xfer_ticket_id
            ];

            $this->xferDtlModel->updateWhere(
                $dataXferDtl,
                ['xfer_hdr_id' => $xfer['wv_tf_id']]
            );
        }
    }

    /**
     * @param $data
     */
    private function updateXferHdr($data)
    {
        $xferTicket = $data['xferTicket'];
        foreach ($data['xfers'] as $xfer) {
            $dataXferHdr = [
                'xfer_sts'       => Status::getByValue("Picking", "XFER-STATUS"),
                'sts'            => 'u',
                'xfer_ticket_id' => $xferTicket->xfer_ticket_id
            ];

            $this->xferHdrModel->updateWhere(
                $dataXferHdr,
                ['xfer_hdr_id' => $xfer['wv_tf_id']]
            );

            // Event tracking
            $xferHdr = $this->xferHdrModel->loadWvTfInfo($xfer['wv_tf_id']);
            $wvTfObj = $this->xferDtlModel->loadXferDtlInfo($xfer['wv_tf_id']);
            $request_qty = $this->xferDtlModel->getReqQty($xfer['wv_tf_id']);

            $info = sprintf(Status::getByKey("event-info", "WHS-XFER-PICKING"),
                $request_qty,
                array_get($wvTfObj, 'item_id', 0),
                array_get($wvTfObj, 'sku', null),
                array_get($wvTfObj, 'size', null),
                array_get($wvTfObj, 'color', null),
                array_get($wvTfObj, 'req_loc', null),
                array_get($wvTfObj, 'mez_loc', null)
            );

            // Event tracking
            $evt_code = Status::getByKey("event", "WHS-XFER-PICKING");
            $owner = array_get($xferHdr, 'xfer_hdr_num', '');
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $data['currentWH'],
                'cus_id'    => array_get($wvTfObj, 'cus_id', null),
                'owner'     => $owner . ', ' . $data['wv_tk_num'],
                'evt_code'  => $evt_code,
                'trans_num' => $owner,
                'info'      => $info
            ]);
        }
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @throws Exception
     */
    public function editXfer(Request $request)
    {
        $input = $request->getParsedBody();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $items = $input['items'];
        $ttl_piece = 0;
        foreach ($items as $item) {
            $ttl_piece += $item['ttl_pieces'];
        }

        // Total Pieces cannot be less than Requested Pieces/XFER
        if (!($ttl_piece >= $input['request_qty'])) {
            throw new HttpException(400, Message::get("BM055"));
        }

        // only get enough location for request qty
        $reqQty = $input['request_qty'];
        foreach ($items as $key => $item) {
            if ($reqQty > 0) {
                if ($item['ttl_pieces'] < $reqQty) {
                    $items[$key]['alloc_qty'] = $item['ttl_pieces'];
                    $reqQty -= $item['ttl_pieces'];
                } elseif ($item['ttl_pieces'] >= $reqQty) {
                    $items[$key]['alloc_qty'] = $reqQty;
                    $reqQty = 0;
                }
            } else {
                $items[$key]['alloc_qty'] = 0;
            }
        }

        $this->validateXfer($items, $input, false);
        // Load xfer Hdr
        $xferHdrInfo = $this->xferHdrModel->getXferHdrById($input['xfer_hdr_id']);

        if (empty($xferHdrInfo)) {
            throw new HttpException(400, Message::get("BM017", "Xfer"));
        }

        // Save xferHdr
        $this->editXferHdr($currentWH, $input);

        // Remove all old xfer dtl
        $this->xferDtlModel->removeXferDtlById($input['xfer_hdr_id']);

        $arrLocCode = [];
        // Save xferDtl
        foreach ($items as $item) {
            if (isset($item['alloc_qty']) && $item['alloc_qty'] > 0) {
                $dataXferDtl = [
                    'whs_id'        => $currentWH,
                    'cus_id'        => $input['cus_id'],
                    'item_id'       => $input['item_id'],
                    'sku'           => $input['sku'],
                    'size'          => $input['size'],
                    'color'         => $input['color'],
                    'lot'           => $input['lot'],
                    'cus_upc'       => $input['cus_upc'],
                    'allocated_qty' => $item['alloc_qty'],
                    'req_loc_id'    => $item['loc_id'],
                    'req_loc'       => $item['loc_code'],
                    'mez_loc_id'    => $input['loc_id'],
                    'mez_loc'       => $input['loc_code'],
                    'xfer_dtl_sts'  => Status::getByValue("Pending", "XFER-STATUS"),
                    'sts'           => 'u',
                    'xfer_hdr_id'   => $input['xfer_hdr_id']
                ];

                $this->xferDtlModel->saveXferDtl($dataXferDtl);
                array_push($arrLocCode, $item['loc_code']);
            }
        }

        $locCodeStr = implode(", ", $arrLocCode);
        $dataEvtTracking = [
            'to_loc_code' => $locCodeStr,
            'currentWh'   => $currentWH,
            'wv_tf_num'   => array_get($xferHdrInfo, 'xfer_hdr_num', null),
        ];
        $this->logEventTrackingWhenCreateXferRecord($dataEvtTracking, $input);

        return $this->response
            ->noContent()
            ->setContent("{}")
            ->setStatusCode(IlluminateResponse::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param ListWxferTransformer $vTransferTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function listAllWvXfer(Request $request, ListWxferTransformer $vTransferTransformer)
    {
        $input = $request->getQueryParams();

        try {
            $wvTfs = $this->xferHdrModel->searchAllXferHdr($input, ['xferDtl'], array_get($input, 'limit', 20));

            return $this->response->paginator($wvTfs, $vTransferTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param ListTicketTransformer $vTransferTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function listAllTicket(Request $request, ListTicketTransformer $vTransferTransformer)
    {
        $input = $request->getQueryParams();

        try {
            $wvTfs = $this->xferHdrModel->searchAllTicket($input, ['xferTicket'], array_get($input, 'limit', 20));

            return $this->response->paginator($wvTfs, $vTransferTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param DetailTicketTransformer $detailTicketTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function detailTicket(Request $request, DetailTicketTransformer $detailTicketTransformer)
    {
        $input = $request->getQueryParams();

        try {
            $xferTicket = $this->xferDtlModel->getXferTicketDetail($input, ['xferHdr', 'xferTicket'], array_get($input,
                'limit',
                20));
            if (empty($xferTicket)) {
                throw new HttpException(400, Message::get("BM017", "Xfer Ticket"));
            }

            return $this->response->paginator($xferTicket, $detailTicketTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param DetailXferTransformer $detailWvTransferTransformer
     *
     * @return mixed
     * @throws Exception
     */
    public function detailXfer(Request $request, DetailXferTransformer $detailWvTransferTransformer)
    {
        $input = $request->getQueryParams();

        $xferHdrID = array_get($input, 'wv_tf_id', null);
        try {
            $xferHdrModel = new XferHdrModel();
            $xferHdrObj = $xferHdrModel->getXferHdrById($xferHdrID);

            if (empty($xferHdrObj)) {
                throw new HttpException(400, Message::get("BM017", "Xfer"));
            }

            $wvTfs = $this->xferDtlModel->loadXferDetail($input, ['reqLocation', 'reqLocation.locationStatus'],
                array_get($input,
                    'limit', 20));

            if (empty($wvTfs)) {
                throw new HttpException(400, Message::get("BM017", "Xfer"));
            }

            $cusId = array_get($xferHdrObj, 'cus_id', null);
            $cus_info = DB::table('customer')->where('cus_id', '=', $cusId)->select('cus_name')->first();

            $details = [];
            if ($wvTfs) {
                foreach ($wvTfs as $xferDtl) {
                    $ctn_info = DB::table('cartons')
                        ->where('loc_id', $xferDtl->req_loc_id)
                        ->where('item_id', $xferDtl->item_id)
                        ->where('whs_id', $xferDtl->whs_id)
                        ->where(function ($q) {
                            $q->where('is_damaged', 0);
                            $q->orWhereNull('is_damaged');
                        })
                        ->where('cus_id', $xferDtl->cus_id);

                    $ctn_qty = $ctn_info->count();
                    $ttl_pieces = $ctn_info->sum('piece_ttl');

                    $details[] = [
                        'xfer_dtl_id'  => $xferDtl->xfer_dtl_id,
                        'loc_sts'      => object_get($xferDtl, 'reqLocation.loc_sts_code', ''),
                        'loc_sts_name' => object_get($xferDtl, 'reqLocation.locationStatus.loc_sts_name', ''),
                        'loc_id'       => $xferDtl->req_loc_id,
                        'loc_code'     => $xferDtl->req_loc,
                        'ctn_ttl'      => $ctn_qty,
                        'ttl_pieces'   => $ttl_pieces
                    ];
                }
            }

            $xferHdrObj->details = $details;

            return $this->response->item($xferHdrObj, $detailWvTransferTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function updateXfer(Request $request)
    {
        $input = $request->getParsedBody();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = $predis->getCurrentWhs();
        $userId = $userInfo['user_id'];
        $xferHdrNums = [];
        $xferHdrEvent = [];

        try {
            \DB::beginTransaction();
            $xferTicketObj = $this->xferTicketModel->byId($input['xfer_ticket_id']);
            if ($xferTicketObj->xfer_ticket_sts !== 'PK') {
                return $this->response->errorBadRequest('Ticket status isn\'t Picking');
            }

            $xferHdrUpdate = "";
            $cus_id = null;
            foreach ($input['xfer_tickets'] as $xferDtl) {
                $itemId = $xferDtl['item_id'];
                $cusId = $xferDtl['cus_id'];
                $cus_id = $cusId ? $cusId : $cus_id;
                $actQty = $xferDtl['act_piece_qty'];
                $fromLoc = $xferDtl['act_req_loc_id'] ?: $xferDtl['req_loc_id'];
                $fromLocCode = $xferDtl['act_req_loc'] ?: $xferDtl['req_loc'];
                $toLoc = $xferDtl['act_mez_loc_id'] ?: $xferDtl['mez_loc_id'];
                $toLocCode = $xferDtl['act_mez_loc_id'] ? $xferDtl['act_mez_loc'] : $xferDtl['mez_loc'];

                //validate xfer
                $this->validateUpdateTicketXfer([
                    'item_id'       => $itemId,
                    'cus_id'        => $cusId,
                    'act_qty'       => $actQty,
                    'from_loc'      => $fromLoc,
                    'from_loc_code' => $fromLocCode,
                    'to_loc'        => $toLoc,
                    'to_loc_code'   => $toLocCode,
                    'mez_loc'       => $xferDtl['mez_loc']
                ]);

                $iaCtn = [];

                //update xferHdr
                if ($xferHdrUpdate !== $xferDtl['xfer_hdr_id']) {
                    //current xfer_hdr_id
                    $xferHdrUpdate = $xferDtl['xfer_hdr_id'];
                    $xferHdrObj = $this->xferHdrModel->byId($xferHdrUpdate);
                    if ($xferHdrObj->xfer_sts !== 'PK') {
                        return $this->response->errorBadRequest($xferHdrObj->xfer_hdr_num . ' status isn\'t Picking');
                    }

                    //update sts
                    $xferHdrObj->xfer_sts = "CO";
                    $xferHdrObj->save();

                    //event tracking
                    $xferHdrNums[] = $xferHdrObj->xfer_hdr_num;

                    //add event tracking
                    $itemObj = (new Item())->where('item_id', $itemId)->first();
                    $xferHdrEvent[$xferHdrObj->xfer_hdr_id]['from_loc'] = [];
                    $xferHdrEvent[$xferHdrObj->xfer_hdr_id]['to_loc'] = $toLocCode;
                    $xferHdrEvent[$xferHdrObj->xfer_hdr_id]['ttl'] = 0;
                    $xferHdrEvent[$xferHdrObj->xfer_hdr_id]['num'] = 0;
                    $xferHdrEvent[$xferHdrObj->xfer_hdr_id]['item-info'] = $itemObj->item_id . ", " .
                        $itemObj->sku . "," . $itemObj->size . "," . $itemObj->color;
                }

                $chkCondition = "
                    item_id = $itemId AND whs_id = $whsId AND cus_id = $cusId AND is_damaged = 0
                    AND deleted = 0 AND loc_id = $fromLoc AND ctn_sts = 'AC' AND COALESCE(is_ecom, 0) <> 1
                ";

                $condition = $chkCondition . " AND ctn_pack_size = piece_remain";

                //get cloneCtn
                $query = "
                    SELECT * FROM cartons
                    WHERE $condition
                    LIMIT 1
                ";

                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $cloneCtn = DB::selectOne($query);

                if (!$cloneCtn) {
                    $cloneCtn = DB::selectOne("SELECT * FROM cartons WHERE $chkCondition");
                }
                if (!$cloneCtn) {
                    return $this->response->errorBadRequest(Message::get('BM017', 'Carton'));
                }

                $packSize = $cloneCtn['ctn_pack_size'];

                $oddItem = $actQty % $packSize;
                $ctnNum = ($actQty - $oddItem) / $packSize;
                $query = "
                    SELECT ctn_id FROM cartons
                    WHERE $condition
                    LIMIT $ctnNum
                ";
                $evenCtn = \DB::select($query);
                $oddItem += ($ctnNum - count($evenCtn)) * $packSize;

                //process odd items
                $query = "
                    SELECT * FROM cartons
                    WHERE item_id = $itemId AND whs_id = $whsId AND cus_id = $cusId AND COALESCE(is_ecom, 0) <> 1
                        AND ctn_pack_size <> piece_remain AND deleted = 0 AND loc_id = $fromLoc
                ";
                $oddCtn = \DB::select($query);
                $iaCtn2 = $this->processOddItem($oddCtn, $oddItem);
                $iaCtn = array_merge($iaCtn, $iaCtn2);

                //process even ctns
                $iaCtn2 = array_column($evenCtn, 'ctn_id');
                $iaCtn = array_merge($iaCtn, $iaCtn2);
                $pltIds = $this->getPltFromCtn($iaCtn);

                //remove cartons and update pallet cnt_ttl
                $this->rmCntNupdatePltTtl($iaCtn);

                if ($oddItem > 0) {
                    if (intval($cloneCtn['ctn_pack_size']) !== $cloneCtn['piece_remain']) {
                        return $this->response->errorBadRequest("Location $fromLocCode is insufficient to transfer.
                        Please select another");
                    }
                    $query = "
                        UPDATE cartons 
                        SET piece_remain = piece_remain - $oddItem, piece_ttl = piece_remain 
                        WHERE $condition LIMIT 1
                    ";
                    \DB::update($query);
                }
                //ia plt
                $this->iaPlt($pltIds);

                //clone to mez loc or update
                $toLocArr = ['loc_id' => $toLoc, 'loc_code' => $toLocCode];
                $this->processMezLoc($userId, $cloneCtn, $toLocArr, $actQty);

                //update xfer_dtl
                $xferDtlObj = $this->xferDtlModel->byId($xferDtl['xfer_dtl_id']);
                $xferDtlObj->act_req_loc_id = $xferDtl['act_req_loc_id'] ?: null;
                $xferDtlObj->act_req_loc = $xferDtl['act_req_loc'] ?: null;
                $xferDtlObj->act_mez_loc_id = $toLoc;
                $xferDtlObj->act_mez_loc = $toLocCode;
                $xferDtlObj->act_piece_qty = $actQty;
                $xferDtlObj->xfer_dtl_sts = 'CO';
                $xferDtlObj->save();

                //add event xfer hdr
                $xferHdrEvent[$xferHdrObj->xfer_hdr_id]['from_loc'][] = $fromLocCode;
                $xferHdrEvent[$xferHdrObj->xfer_hdr_id]['ttl'] += $xferDtl['act_piece_qty'];
                $xferHdrEvent[$xferHdrObj->xfer_hdr_id]['num']++;
            }

            //update xfer ticket
            $xferTicketObj->xfer_ticket_sts = "CO";
            $xferTicketObj->save();

            // Event tracking xfer_hdr
            foreach ($xferHdrEvent as $xferHdrId => $event) {
                $evt = [
                    'whs_id'     => $xferTicketObj->whs_id,
                    'cus_id'     => $xferTicketObj->cus_id,
                    'owner'      => $xferTicketObj->xfer_ticket_num,
                    'evt_code'   => Status::getByKey("event", "WHS-XFER-COMPLETE"),
                    'trans_num'  => $xferHdrId,
                    'info'       => sprintf(Status::getByKey("event-info", "WHS-XFER-COMPLETE"), $event['num'],
                        $event['item-info'], implode(',', $event['from_loc']), $event['to_loc']),
                    'created_at' => time(),
                    'created_by' => JWTUtil::getPayloadValue('jti')
                ];
                DB::table('evt_tracking')->insert($evt);
            }
            $this->eventTrackingModel->refreshModel();
            // Event tracking
            $this->eventTrackingModel->create([
                'whs_id'    => $xferTicketObj->whs_id,
                'cus_id'    => $cus_id,
                'owner'     => $xferTicketObj->xfer_ticket_num,
                'evt_code'  => Status::getByKey("event", "WHS-XFER-TICKET-COMPLETE"),
                'trans_num' => $xferTicketObj->xfer_ticket_num,
                'info'      => sprintf(Status::getByKey("event-info", "WHS-XFER-TICKET-COMPLETE"), implode(',',
                    $xferHdrNums))
            ]);

            \DB::commit();

            return $this->response
                ->noContent()
                ->setContent("{}")
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\Exception $e) {
            \DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $dataXferHdr
     *
     * @throws \MpdfException
     */
    public function printXferPdf($dataXferHdr)
    {
        $xferTK = $dataXferHdr['xferTicket'];
        $xferTicketInfo = $this->xferTicketModel->getTicketInfoById($xferTK->xfer_ticket_id)->toArray();
        $created_at = gmdate('Y-m-d h:i:s', array_get($xferTicketInfo, 'created_at', null));
        $wv_tf_num = array_get($dataXferHdr, 'wv_tk_num', null);

        $strHtml = "";
        $ttol = 0;

        foreach ($dataXferHdr['xfers'] as $xfer) {
            $xferDtls = $this->xferDtlModel->loadAllXferDtlByHdrId($xfer['wv_tf_id']);
            foreach ($xferDtls as $xferDtl) {
                $cus_name = $xferDtl['cus_name'];
                $cus_upc = !empty($xferDtl['cus_upc']) ? $xferDtl['cus_upc'] :
                    str_pad($xferDtl['item_id'], 5, '0', STR_PAD_LEFT) . str_pad($xferDtl['cus_id'], 8, '0', STR_PAD_LEFT);
                $mez_loc = $xferDtl['mez_loc'];
                $piece_qty = $xferDtl['allocated_qty'];
                $sku = $xferDtl['sku'];
                $color = $xferDtl['color'];
                $size = $xferDtl['size'];
                $req_loc = $xferDtl['req_loc'];

                $strHtml .= <<<EOL
                            <tr>
                            <th>$cus_name<br/>$cus_upc<br/>$mez_loc</th>
                            <th>$piece_qty</th>
                            <th>$sku<br/>$color <br/>$size</th>
                            <th>$req_loc</th>
                        </tr>
EOL;
                $ttol += $piece_qty;
            }
        }


        $html = <<<EOL
        <html>
            <body  style="font-family:sans-serif;font-size:12px;">

                <style>
                    td { text-align: center; }
                    .upercase { text-transform: uppercase; font-weight: bold; }
                </style>
                    <div style="font-family: ocrb; font-size: 14px;">TransferID: $wv_tf_num - Created
                    $created_at</div>
                    <barcode code='$wv_tf_num' type="C128B" text="1" height="2.5" size="0.5"
                    style="margin-left:
                    -5px"/>
                    <div style="font-family: ocrb; font-size: 12px;">$wv_tf_num</div>
                    <br /><br />
                    <table cellspacing="0" cellpadding="10" border="1" style="width:100%;">
                        <tr>
                            <th>Customer<br/>UPC<br/>To Location</th>
                            <th>Pieces</th>
                            <th>SKU <br/>Color <br/>Size</th>
                            <th>From Location</th>
                        </tr>

                        $strHtml
                        <tr>
                            <th>Total</th>
                            <th>$ttol</th>
                            <th colspan="2"></th>
                        </tr>
                    </table>
            </body>
            </html>
EOL;

        $mpdf = new \mPDF('', '', '', '', 20, 15, 25, 25, 10, 10);
        $mpdf->WriteHTML($html);
        $mpdf->Output("XFER Ticket-" . $wv_tf_num, "D");
    }

    public function getAvailQty($itemId, $locId, $cusId)
    {
        return ['data' => ['piece_remain' => $this->cartonModel->getAvailQty($itemId, $locId, $cusId)]];
    }
}
