<?php

namespace Seldat\Wms2\Models;

class WarehouseMeta extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whs_meta';

    /**
     * No primary key in this table
     *
     * @var bool
     */
    protected $primaryKey = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'whs_qualifier',
        'whs_meta_value'
    ];
}
