<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () {
            return ['Seldat API V1'];
        });
        //Report config
        $api->post('/report-configs/create-report', ['action' => "report",
            'uses'      => 'ReportConfigsController@createReport'
        ]);
        $api->put('/report-configs/{qualifier}/fields',
            ['action' => "report", 'uses' => 'ReportConfigsController@updateFieldsReportConfigs']);
        $api->put('/report-configs/{qualifier}/search',
            ['action' => "report", 'uses' => 'ReportConfigsController@updateSearchReportConfigs']);
        $api->get('/report-configs/{qualifier}',
            ['action' => "report", 'uses' => "ReportConfigsController@viewReportConfigs"]);

        // $api->get('report-configs/details-total',
        //     ['action' => "viewReportDetailTotals", 'uses' => "ReportConfigsController@viewReportDetailTotals"]);
        // $api->post('report-configs/import-report',
        //     ['action' => "importReport", 'uses' => "ReportConfigsController@importReport"]);
        $api->put('report-configs/reset/{qualifier}',
            ['action' => "resetReportConfigs", 'uses' => "ReportConfigsController@resetReportConfigs"]);

        //Show Report
        $api->get('/report/{qualifier}/show', [
            'action'    => 'report',
            'uses'      => 'ReportController@show'
        ]);

        $api->get('/report/{qualifier}/export', [
            'action'    => 'report',
            'uses'      => 'ReportController@export'
        ]);


        //Widget
        $api->get('/config-reports/list', ['action' => "report", 'uses' =>'ReportConfigsController@getReportList']);
        $api->get('/config-widgets-reports/list', ['action' => "report", 'uses' =>'ReportWidgetsController@getReportList']);
        $api->post('/config-reports/widgets', ['action' => "report", 'uses' =>'ReportWidgetsController@store']);
        $api->delete('/config-reports/widget/{id:[0-9]+}', ['action' => "report", 'uses' =>'ReportWidgetsController@delete']);
        $api->get('/config-reports/widgets/{reportWidgetsId:[0-9]+}', ['action' => "report", 'uses' =>'ReportWidgetsController@viewReportWidgets']);
        $api->get('/report-widgets/list',['action' => "report", 'uses' => "ReportWidgetsController@getReportWidgets"]);

        // SKU Summary
        $api->get('/report-sku-sumary/list',['action' => "report", 'uses' => "ReportSkuSummaryController@getReportSkuSummary"]);
        $api->post('/report-sku-sumary', ['action' => "report", 'uses' => "ReportSkuSummaryController@storeReportSkuSummary"]);

        //get reports table name
        $api->get('/report-tables-name/list', ['action' => "report", 'uses' =>'ReportConfigsController@getReportTableNameList']);

        //update print pdf
        $api->put('/pdf-config/{qualifier}', ['action' => "report", 'uses' =>'ReportConfigsController@updatePdfConfig']);

        $api->get('/report-pdf/{qualifier}/export', ['action' => "report", 'uses' =>'ReportController@printPdfReport']);

        //logo
        $api->post('/logo-gallery/{idSystem}/upload', ['action' => "report", 'uses' =>'ReportConfigsController@uploadLogo']);
        $api->get('/logo-gallery/{idSystem}', ['action' => "report", 'uses' =>'ReportConfigsController@getLogo']);
        $api->get('/logo-gallery', ['action' => "report", 'uses' =>'ReportConfigsController@getListLogo']);
        $api->post('/logo-gallery', ['action' => "report", 'uses' =>'ReportConfigsController@createLogo']);
        $api->delete('/logo-gallery/{idSystem}', ['action' => "report", 'uses' =>'ReportConfigsController@deleteLogo']);
        $api->get('/logo-gallery/view/{url}', ['action' => "report", 'uses' =>'ReportConfigsController@view']);

        //my report
        $api->get('/config-reports/my-report', ['action' => "report", 'uses' =>'ReportConfigsController@getMyReportList']);
    });
});
