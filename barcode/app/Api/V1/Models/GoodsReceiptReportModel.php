<?php
namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\ReportReceiving;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class GoodsReceiptReportModel extends AbstractModel
{
    /**
     * GoodsReceiptReportModel constructor.
     *
     * @param ReportReceiving|null $model
     */
    public function __construct(ReportReceiving $model = null)
    {
        $this->model = ($model) ?: new ReportReceiving();
    }


}
