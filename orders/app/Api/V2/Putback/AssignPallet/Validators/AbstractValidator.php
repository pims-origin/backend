<?php

namespace App\Api\V2\Putback\AssignPallet\Validators;

use Validator;
use Dingo\Api\Exception\ValidationHttpException;

abstract class AbstractValidator
{
    /**
     * @return array
     */
    abstract protected function rules();


    /**
     * @param $input
     * @return
     * @throws ApiValidateException
     */
    public function validate($input)
    {
        $validator =  Validator::make($input, $this->rules(), $this->messages());

        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->getMessages());
        }

        return $validator;
    }

    /**
     * @return array
     */
    protected function messages()
    {
        return [];
    }
}
