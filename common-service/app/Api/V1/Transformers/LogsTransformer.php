<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Logs;
use League\Fractal\TransformerAbstract;

class LogsTransformer extends TransformerAbstract
{
    public function transform(Logs $logs)
    {
        return [
            'whs_id'        => object_get($logs, 'whs_id', ''),
            'type'          => object_get($logs, 'type', ''),
            'evt_code'      => object_get($logs, 'evt_code', ''),
            'owner'         => object_get($logs, 'owner', ''),
            'transaction'   => object_get($logs, 'transaction', ''),
            'url_endpoint'  => object_get($logs, 'url_endpoint', ''),
            'message'       => object_get($logs, 'message', ''),
            'header'        => object_get($logs, 'header', ''),
            'request_data'  => object_get($logs, 'request_data', ''),
            'response_data' => object_get($logs, 'response_data', ''),
//            'created_by'    => trim(object_get($logs, "createdUser.first_name", null) . " " .
//                object_get($logs, "createdUser.last_name", null)),
            'created_by'    => object_get($logs, 'created_by', ''),
            'created_at'    => date("m/d/Y h:i:s", strtotime(object_get($logs, 'created_at', ''))),
            'logs_id'            => object_get($logs, 'id', ''),
            'des'           => object_get($logs, 'des', ''),
        ];
    }
}
