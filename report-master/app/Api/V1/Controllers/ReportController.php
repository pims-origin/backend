<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ReportConfigsModel;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\ReportSystem;
use Seldat\Wms2\Utils\Export;
use mPDF;


class ReportController extends AbstractController
{

    protected $reportConfigsModel;

    public function __construct(ReportConfigsModel $reportConfigsModel)
    {
        $this->reportConfigsModel = $reportConfigsModel;
    }

    public function show(Request $request, string $qualifier)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $input      = $request->getQueryParams();
        $searches   = array_get($input, 'searches', []);
        $filter_searches  = array_get($input, 'filter_searches', []);
        $ref        = array_get($input, 'ref', 0);
        $level      = array_get($input, 'level', 1);
        $system     = array_get($input, 'system', 'WMS360');
        $limit      = array_get($input, 'limit', 20);

        $config = $this->reportConfigsModel->getModel()
                    ->where('qualifier', $qualifier)
                    ->where([
                        'ref'       => $ref,
                        'level'     => $level,
                        'system'    => $system
                    ])
                    ->first();
        if ( !$config ) {
            $msg = "Config not found";
            throw new \Exception($msg);
        }

        $keyReport = str_replace("_", "", ucwords(str_replace("rpt_", "", $config->tbl_ref), "_"));
        $reportStructure = [
            "Report". $keyReport . "Model",
            "Report" . $keyReport . "Transformer",
            'App\\Api\\V1\\Models\\Report' . $keyReport . 'Model',
            'App\\Api\\V1\\Transformers\\Report' . $keyReport . 'Transformer'
        ];
        list($model, $transformer, $modelClassName, $transformerClassName) = $reportStructure;

        if ( !file_exists(__DIR__ . '/../Models/' . $model . '.php') ) {
            $msg = "Model not found";
            throw new \Exception($msg);
        }

        if ( !file_exists(__DIR__ . '/../Transformers/' . $transformer . '.php') ) {
            $msg = "Transformer not found";
            throw new \Exception($msg);
        }

        try {
            $modelInstance  = new $modelClassName;
            $configData     = json_decode($config->data);
            $query = $modelInstance->filter($configData->filter);
            if ( $searches ) {
                $query = $modelInstance->searches($query, $searches, $configData->default_search);
            }
            if ( $filter_searches ) {
                $query = $modelInstance->filter_searches($query, $filter_searches, $configData->fields);
            }
            $aggregateQuery = $modelInstance->groupBy($query, $configData);
            $ordering = array_get($input, 'ordering', []);

            if(!empty($ordering)) {
                foreach ($ordering as $keyOrderBy => $valueOrderBy) {
                    $configData->order_by[] = (object)['ordering' => $valueOrderBy, 'field_name' =>$keyOrderBy ];
                }
            }
            $listFiedName = array_pluck( $configData->order_by, 'field_name' );
            $flipped = array_flip($listFiedName);
            $orderby = [];
            foreach ($flipped as $keyOrderby) {
                $orderby[] = $configData->order_by[$keyOrderby];
            }
            $query = $modelInstance->orderBy($query, $orderby);
            $query = $modelInstance->sortBuilder($query, $input);
            if(!empty($configData->aggregate)) {
                $aggregates = $modelInstance->aggregate($aggregateQuery, $configData);
                $data = $aggregates->transform(function ($item) use ($configData) {
                    $key = [];
                    if ($configData->group_by && !empty($configData->group_by)) {
                        foreach ($configData->group_by as $value) {
                            $key[] = $item->$value;
                        }
                    }
                    $item->key = implode('|', $key);
                    return $item;
                })->groupBy('key');
                $sumRow = [];
                $data->map(function (&$item, $key) use ($aggregates, $configData, &$sumRow) {

                    $aggregate = $aggregates[$key];
                    $aggregateFields = collect($configData->aggregate)->pluck('field_name')->toArray();
                    $row = $item->first()->toArray();
                    $aggregateRow = [];
                    $countNo = 0;

                    foreach ($row as $fieldName => $value) {
                        if (in_array($fieldName, $aggregateFields)) {
                            $aggregateRow['no_col'] = $countNo;
                            $aggregateRow[$fieldName] = $aggregate[$fieldName];
                        }
                        if (!isset($aggregateRow['no_col'])) {
                            $countNo++;
                        }
                        /*if ( !in_array($fieldName, $aggregateFields) ) {
                            $aggregateRow[$fieldName] = null;
                        }*/

                    }

                    $aggregateRow['label'] = object_get($configData, 'subtotal_label', 'Sub total');

                    $aggregateRow['is_avg'] = true;
                    if (!$sumRow) {
                        $sumRow = $aggregateRow;

                        foreach ($aggregateFields as $field) {
                            $sumRow[$field] = $aggregates->sum($field);
                        }

                        $sumRow['label'] = object_get($configData, 'total_label', 'Sub total');
                    }

                    $item->push($aggregateRow);
                });

                $data = $data->flatten(1)->toArray();
                $data[] = $sumRow;
                return response(['data' => $data
                ], 200);
            } else {

                $data = $query->paginate($limit);
                return $this->response->paginator($data, new $transformerClassName);
            }
            return $this->response->paginator($data, new $transformerClassName);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        /*try {
            $modelInstance  = new $modelClassName;
            $configData     = json_decode($config->data);

            $query = $modelInstance->filter($configData->filter);
            if ( $searches ) {
                $query = $modelInstance->searches($query, $searches, $configData->default_search);
            }

            $query = $modelInstance->orderBy($query, $configData->order_by);

            $query = $modelInstance->sortBuilder($query, $input);

            $data = $query->paginate($limit);

            return $this->response->paginator($data, new $transformerClassName);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }*/
    }

    public function export(Request $request, string $qualifier)
    {
        $input      = $request->getQueryParams();
        $searches   = array_get($input, 'searches', []);
        $filter_searches  = array_get($input, 'filter_searches', []);
        $ref        = array_get($input, 'ref', 0);
        $level      = array_get($input, 'level', 1);
        $system     = array_get($input, 'system', 'WMS360');

        $config = $this->reportConfigsModel->getModel()
                    ->where('qualifier', $qualifier)
                    ->where([
                        'ref'       => $ref,
                        'level'     => $level,
                        'system'    => $system
                    ])
                    ->first();
        if ( !$config ) {
            $msg = "Config not found";
            throw new \Exception($msg);
        }
        $keyReport = str_replace("_", "", ucwords(str_replace("rpt_", "", $config->tbl_ref), "_"));
        $reportStructure = [
            "Report". $keyReport . "Model",
            "Report" . $keyReport . "Transformer",
            'App\\Api\\V1\\Models\\Report' . $keyReport . 'Model',
            'App\\Api\\V1\\Transformers\\Report' . $keyReport . 'Transformer'
        ];
        list($model, $transformer, $modelClassName, $transformerClassName) = $reportStructure;

        try {
            $modelInstance  = new $modelClassName;
            $configData     = json_decode($config->data);

            $query = $modelInstance->filter($configData->filter);

            if ( $searches ) {
                $query = $modelInstance->searches($query, $searches, $configData->default_search);
            }
            if ( $filter_searches ) {
                $query = $modelInstance->filter_searches($query, $filter_searches, $configData->fields);
            }
            $aggregateQuery = $modelInstance->groupBy($query, $configData);

            $aggregates = $modelInstance->aggregate($aggregateQuery, $configData);

            $ordering = array_get($input, 'ordering', []);

            if(!empty($ordering)) {
                foreach ($ordering as $keyOrderBy => $valueOrderBy) {
                    $configData->order_by[] = (object)['ordering' => $valueOrderBy, 'field_name' =>$keyOrderBy ];
                }
            }
            $listFiedName = array_pluck( $configData->order_by, 'field_name' );
            $flipped = array_flip($listFiedName);
            $orderby = [];
            foreach ($flipped as $keyOrderby) {
                $orderby[] = $configData->order_by[$keyOrderby];
            }
            $query = $modelInstance->orderBy($query, $orderby);

            $data = $query->get();

            $data = $data->transform(function ( $item ) use ($configData) {
                $key = [];
                foreach ( $configData->group_by as $value ) {
                    $key[]  = $item->$value;
                }
                $item->key = implode('|', $key);
                return $item;
            })->groupBy('key');
            $sumRow = [];
            $data->map(function(&$item, $key) use ($aggregates, $configData, &$sumRow) {
                $aggregate = $aggregates[$key];
                $aggregateFields = collect($configData->aggregate)->pluck('field_name')->toArray();
                $row = $item->first()->toArray();
                $aggregateRow = [];
                $countNo = 0;
                /*if(!empty($configData->fields)) {
                    foreach ($configData->fields as $key => $value) {
                        $configData->fields[$key]
                    }
                }*/
                foreach ( $row as $fieldName => $value ) {
                    if ( in_array($fieldName, $aggregateFields) ) {
                        $aggregateRow['no_col'] = $countNo;
                        $aggregateRow[$fieldName] = $aggregate[$fieldName];
                    }
                    $listFields = array_column($configData->fields, 'show','field_name');

                    if(!isset($aggregateRow['no_col']) && $listFields[$fieldName] == true) {
                        $countNo++;
                    }
                }
                if(!empty($configData->aggregate)) {
                    $aggregateRow['label'] = object_get($configData, 'subtotal_label', 'Sub total');

                    $aggregateRow['is_avg'] = true;
                }

                if ( !$sumRow ) {
                    $sumRow = $aggregateRow;

                    foreach ( $aggregateFields as $field ) {
                        $sumRow[$field] = $aggregates->sum($field);
                    }
                    if(!empty($configData->aggregate)) {
                        $sumRow['label'] = object_get($configData, 'total_label', 'Sub total');
                    }
                }

                $item->push($aggregateRow);
            });

            $data = $data->flatten(1)->toArray();
            $data[] = $sumRow;

            $title = [];
            foreach ( $configData->fields as $field ) {
                $field = new \ArrayObject($field);
                $key = $field['field_name'];

                if ( $field['data_type'] === 'date' ) {
                    $key .= '|format()|m/d/Y';
                }

                if ( $field['show'] ) {
                    $title[$key] = $field['display_name'];
                }
            }

            Export::exportCsv(array_get($config, 'name', 'Report_' . time()), $title, $data);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function printPdfReport(Request $request, string $qualifier)
    {
        /*$ref    = array_get($_GET, 'ref');
        $level  = array_get($_GET, 'level');
        $system = array_get($_GET, 'system');*/
        $input      = $request->getQueryParams();
        $searches   = array_get($input, 'searches', []);
        $filter_searches  = array_get($input, 'filter_searches', []);
        $ref        = array_get($input, 'ref', 0);
        $level      = array_get($input, 'level', 1);
        $system     = array_get($input, 'system', 'WMS360');
        $limit     = array_get($input, 'limit', null);
        if($limit == "") {
            $limit = null;
        }
        if (!$level || !$system || !is_numeric($ref) ) {
            $msg = 'Fields, ref, level and system is required';
            throw new \Exception($msg);
        }
        $userInfo = new \Wms2\UserInfo\Data();
        $userInfo = $userInfo->getUserInfo();
        $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
        $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];
        $pdf = new Mpdf(

        );
        $config = $this->reportConfigsModel->getModel()
            ->where('qualifier', $qualifier)
            ->where([
                'ref'       => $ref,
                'level'     => $level,
                'system'    => $system
            ])
            ->first();
        if ( !$config ) {
            $msg = "Config not found";
            throw new \Exception($msg);
        }
        $keyReport = str_replace("_", "", ucwords(str_replace("rpt_", "", $config->tbl_ref), "_"));
        $reportStructure = [
            "Report". $keyReport . "Model",
            "Report" . $keyReport . "Transformer",
            'App\\Api\\V1\\Models\\Report' . $keyReport . 'Model',
            'App\\Api\\V1\\Transformers\\Report' . $keyReport . 'Transformer'
        ];
        list($model, $transformer, $modelClassName, $transformerClassName) = $reportStructure;

        if ( !file_exists(__DIR__ . '/../Models/' . $model . '.php') ) {
            $msg = "Model not found";
            throw new \Exception($msg);
        }

        if ( !file_exists(__DIR__ . '/../Transformers/' . $transformer . '.php') ) {
            $msg = "Transformer not found";
            throw new \Exception($msg);
        }

        try {
            $modelInstance  = new $modelClassName;
            $configData     = json_decode($config->data);
            $sys_rpt_id = object_get($configData, 'pdf.header.sys_rpt_id', 0);
            if($sys_rpt_id > 0) {
                $rpt_sys = new ReportSystem();
                $dataSystem = $rpt_sys->getModel()->where('sys_rpt_id', $sys_rpt_id)->first();
                $url_logo = array_get($dataSystem, "url_logo", "");
                $url_logo = env('API_RPT') . '/v1/logo-gallery/view/' . $url_logo;
            } else {
                $url_logo = "";
            }
            $query = $modelInstance->filter($configData->filter);

            if ( $searches ) {
                $query = $modelInstance->searches($query, $searches, $configData->default_search);
            }
            if ( $filter_searches ) {
                $query = $modelInstance->filter_searches($query, $filter_searches, $configData->fields);
            }
            $aggregateQuery = $modelInstance->groupBy($query, $configData);
            $aggregates = $modelInstance->aggregate($aggregateQuery, $configData);
            $ordering = array_get($input, 'ordering', []);

            if(!empty($ordering)) {
                foreach ($ordering as $keyOrderBy => $valueOrderBy) {
                    $configData->order_by[] = (object)['ordering' => $valueOrderBy, 'field_name' =>$keyOrderBy ];
                }
            }
            $listFiedName = array_pluck( $configData->order_by, 'field_name' );
            $flipped = array_flip($listFiedName);
            $orderby = [];
            foreach ($flipped as $keyOrderby) {
                $orderby[] = $configData->order_by[$keyOrderby];
            }
            $query = $modelInstance->orderBy($query, $orderby)->limit($limit);
            $data = $query->get();
            $data = $data->transform(function ( $item ) use ($configData) {
                $key = [];
                if($configData->group_by && !empty($configData->group_by)){
                    foreach ( $configData->group_by as $value ) {
                        $key[]  = $item->$value;
                    }
                }
                $item->key = implode('|', $key);
                return $item;
            })->groupBy('key');

            $sumRow = [];
            $data->map(function(&$item, $key) use ($aggregates, $configData, &$sumRow) {
                $aggregate = $aggregates[$key];
                $aggregateFields = collect($configData->aggregate)->pluck('field_name')->toArray();
                $row = $item->first()->toArray();
                $aggregateRow = [];
                $countNo = 0;
                foreach ( $row as $fieldName => $value ) {
                    if ( in_array($fieldName, $aggregateFields) ) {
                        $aggregateRow['no_col'] = $countNo;
                        $aggregateRow[$fieldName] = $aggregate[$fieldName];
                    }
                    $listFields = array_column($configData->fields, 'show','field_name');

                    if(!isset($aggregateRow['no_col']) && $listFields[$fieldName] == true) {
                        $countNo++;
                    }
                    /*if ( !in_array($fieldName, $aggregateFields) ) {
                        $aggregateRow[$fieldName] = null;
                    }*/

                }
                if(!empty($configData->aggregate)) {
                    $aggregateRow['label'] = object_get($configData, 'subtotal_label', 'Sub total');

                    $aggregateRow['is_avg'] = true;
                }
                if ( !$sumRow ) {
                    $sumRow = $aggregateRow;

                    foreach ( $aggregateFields as $field ) {
                        $sumRow[$field] = $aggregates->sum($field);
                    }
                    if(!empty($configData->aggregate)) {
                        $sumRow['label'] = object_get($configData, 'total_label', 'Sub total');
                    }
                }

                $item->push($aggregateRow);
            });

            $data = $data->flatten(1)->toArray();
            $data[] = $sumRow;
            $title = [];
            if(!empty($configData->fields)) {
                foreach ($configData->fields as $field) {
                    $field = new \ArrayObject($field);
                    $key = $field['field_name'];

                    if ($field['data_type'] === 'date') {
                        $key .= '|format()|m/d/Y';
                    }

                    if ($field['show']) {
                        $title[$key] = $field['display_name'];
                    }
                }
            }

            $dataConfig = json_decode($config->data);
            $data = Export::convertData($title, $data);

            $html = (string)view('ReportPrintoutTemplate', [
                'data'    => $data,
                'dataConfig'=>$dataConfig,
                'url_logo' => $url_logo
            ]);

            $pdf->WriteHTML($html);

            //$dir = storage_path("PutAway/$whsId");
            //$pdf->Output("$dir/$grHdrNum.pdf", 'F');
            $pdf->Output("WMS360_" . $qualifier."_" .$ref . "_report.pdf", "D");
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}