<?php

/*INBOUND PROCESS*/
/*$api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/inbound/scan-pallet', 'namespace' => 'App\Api\V2\Inbound\ScanPallet\Controllers'], function ($api) {
    $api->put('/assign-carton-to-pallet',
        ['uses' => 'PalletController@assignCartonsToPallet']);
});*/

$api->group(['prefix' => '/v3/whs/{whsId:[0-9]+}/', 'namespace' => 'App\Api\V3\Inbound\ScanPallet\Controllers'], function ($api) {
    $api->put('/scan-pallet',
        ['uses' => 'PalletController@assignCartonsToPallet']);
});

$api->group(['prefix' => '/v3/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/', 'namespace' => 'App\Api\V3\Inbound\ScanCartons\Controllers'], function ($api) {
    $api->post('/add-virtual-cartons',
        ['action' => "scanCartons", 'uses' => 'CartonsController@scanCartons']);
});

$api->group(['prefix' => '/v3/whs/{whsId:[0-9]+}/inbound/scan-cartons', 'namespace' => 'App\Api\V3\Inbound\ScanCartons\Controllers'], function ($api) {
    $api->put('/scan-pallet',
        ['action' => "saveAssignPallet", 'uses' => 'CartonsController@assignCartonsToPallet']);
});

// Outbound
$api->group(['prefix' => '/v3/whs/{whsId:[0-9]+}/outbound', 'namespace' => 'App\Api\V3\Outbound\AssignPallet\Controllers'], function ($api) {
    $api->put('/assign-outbound-pallet', ['uses' => 'PalletController@assignOutboundPallet']);
    $api->get('/{locCode}/shipping-lane-pallet', ['uses' => 'PalletController@getShippingLanePallet']);
});

// wave pick
$api->group(['prefix' => '/v3/whs/{whsId:[0-9]+}/wave', 'namespace' => 'App\Api\V3\Outbound\Wavepick'], function ($api) {
    // update wave pick
    $api->put('/{wvId:[0-9]+}/update',
        [
            'action' => "updateWavepick",
            'uses'   => 'UpdateWavepick\Controllers\WavepickController@updateWavePick'
        ]
    );
});

// Location
$api->group(['prefix' => '/v3/whs/{whsId:[0-9]+}/location', 'namespace' => 'App\Api\V3\Inbound\Location'], function ($api) {
    $api->put('/rack/put-pallet',
        [
            'action' => "updateInventory",
            'uses'   => 'Putaway\Controllers\PutAwayController@putAway'
        ]
    );

    $api->post('/rack/check-valid-by-customer',
        [
            'action' => "putaway",
            'uses'   => 'CheckValidLocationPutback\Controllers\LocationController@checkValidLocation'
        ]
    );
});
