<?php

namespace App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models;

use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ItemModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new Item();
    }
}
