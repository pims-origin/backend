<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CartonModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Carton;

class ItemTransformer extends TransformerAbstract
{
    //return result
    public function transform(Carton $carton)
    {
        $cartonsItem = object_get($carton, 'item.carton', null);
        $loc_id = object_get($carton, 'loc_id', 0);

        return [
            //carton
            'item_id'         => object_get($carton, 'item_id', null),
            'cus_id'          => object_get($carton, 'cus_id', null),
            'ctn_ttl'         => is_null($cartonsItem) ? 0 : $cartonsItem->where('loc_id', $loc_id)->count(),
            'piece_remain'    => is_null($cartonsItem) ? 0 : $cartonsItem->where('loc_id',
                $loc_id)->sum('piece_remain'),
            'piece_ttl'       => is_null($cartonsItem) ? 0 : $cartonsItem->where('loc_id', $loc_id)->sum('piece_ttl'),
            //item
            'sku'             => array_get($carton, 'item.sku', null),
            'size'            => array_get($carton, 'item.size', null),
            'color'           => array_get($carton, 'item.color', null),
            //asnDtl
            'asn_dtl_lot'     => array_get($carton, 'AsnDtl.asn_dtl_lot', null),
            'asn_dtl_pack'    => array_get($carton, 'AsnDtl.asn_dtl_pack', null),
            'asn_dtl_cus_upc' => array_get($carton, 'AsnDtl.asn_dtl_cus_upc', null),
            'uom_name'        => array_get($carton, 'AsnDtl.systemUom.sys_uom_name', null),
            'uom_id'          => array_get($carton, 'AsnDtl.systemUom.sys_uom_id', null),
            'asn_dtl_weight'  => array_get($carton, 'AsnDtl.asn_dtl_weight', null),
            'asn_dtl_height'  => array_get($carton, 'AsnDtl.asn_dtl_height', null),
            'asn_dtl_width'   => array_get($carton, 'AsnDtl.asn_dtl_width', null),
            'asn_dtl_length'  => array_get($carton, 'AsnDtl.asn_dtl_length', null),
            //customer
            'cus_name'        => array_get($carton, 'customer.cus_name')
        ];
    }


}
