<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 7-Sep-16
 * Time: 09:28
 */

namespace App\Api\V2\WavePickDetails\WvDtlList\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class WaveDtlListValidator
 *
 * @package App\Api\V1\Validators
 */
class WaveDtlListValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'wv_id'  => 'required|integer',
        ];
    }

}
