<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 31-Oct-16
 * Time: 16:36
 */

namespace App\Api\V2\Order\CancelOrder\Models;

use Seldat\Wms2\Models\WavepickDtl;

/**
 * Class OrderDtlModel
 *
 * @package App\Api\V1\Models
 */
class WavepickDtlModel extends AbstractModel
{
    /**
     * WavepickDtlModel constructor.
     *
     * @param WavepickDtl|null $model
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $wvIds
     *
     * @return mixed
     */
    public function countSku($wvIds)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->whereIn('wv_id', $wvIds)
            ->count(['item_id']);

        return $skuCount;
    }


}
