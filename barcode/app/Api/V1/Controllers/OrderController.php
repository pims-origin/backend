<?php
/**
 * Created by PhpStorm.
 * User: admin
<<<<<<< HEAD
 * Date: 12/13/2018
 * Time: 3:04 PM
=======
 * Date: 1/8/2019
 * Time: 9:20 AM
>>>>>>> PIMS-3
 */

namespace App\Api\V1\Controllers;


use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\ShipmentModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Transformers\OrderDtlTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Models\SysBug;

class OrderController extends AbstractController
{

    protected $ordrHdrModel;
    /**
     * @var $customerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var ShippingOrderModel
     */
    protected $shippingOrderModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    public function __construct(
        OrderHdrModel $orderHdrModel,
        CustomerConfigModel $customerConfigModel,
        ShippingOrderModel $shippingOrderModel,
        OrderDtlModel $orderDtlModel
    )
    {
        $this->ordrHdrModel = $orderHdrModel;
        $this->customerConfigModel = $customerConfigModel;
        $this->shippingOrderModel = $shippingOrderModel;
        $this->orderDtlModel = $orderDtlModel;

    }

    public function search(Request $request, $whsId)
    {
        try {
            $input = $request->getQueryParams();
            $input['limit'] = (!empty($input['limit'])) ? $input['limit'] : 20;
            $input['page'] = (!empty($input['page'])) ? $input['page'] : 1;

            $result = $this->ordrHdrModel->search($input, $whsId);
            $res = json_decode(json_encode($result), true);
            $res['data'] = $result->getCollection()->transform(function ($value) {
                return [
                    "odr_id" => $value->odr_id,
                    "num_sku" => $value->num_sku,
                    "cus_odr_num" => $value->cus_odr_num,
                    "cus_po" => $value->cus_po,
                    "odr_sts" => $value->odr_sts
                ];
            })->toArray();
            return response()->json($res);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getShippedOrders(Request $request, $whsId)
    {
        try {
            $input = $request->getQueryParams();
            $input['limit'] = (!empty($input['limit'])) ? $input['limit'] : 20;
            $input['page'] = (!empty($input['page'])) ? $input['page'] : 1;

            $result = $this->ordrHdrModel->searchShippedOrders($input, $whsId);
            $res = json_decode(json_encode($result), true);
            $res['data'] = $result->getCollection()->transform(function ($value) {
                return [
                    "odr_id" => $value->odr_id,
                    "num_sku" => $value->num_sku,
                    "cus_odr_num" => $value->cus_odr_num,
                    "cus_po" => $value->cus_po,
                    "odr_sts" => $value->odr_sts
                ];
            })->toArray();
            return response()->json($res);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__));
        }
    }

    public function orderDetail($whs_id,
        $orderId,
        OrderDtlTransformer $orderDtlTransformer
    ) {
        try {
            $orderHdr = $this->orderHdrModel->getFirstWhere([
                'odr_id' => $orderId,
                'whs_id' => $whs_id
            ]);

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            //validate whs cus
            $this->chkWhsAndCus($orderHdr['whs_id'], $orderHdr['cus_id']);

            //get partial from customer config
            $customerConfigInfo2 = $this->customerConfigModel->getPartialCustomerConfigs(object_get($orderHdr, 'cus_id',
                ''));
            if ($customerConfigInfo2->toArray()) {
                $partial = true;
            } else {
                $partial = false;
            }
            $orderHdr->partial = $partial;
            $so_id = object_get($orderHdr, 'so_id', 0);
            $shippingInfo = $this->shippingOrderModel->getFirstWhere(['so_id' => $so_id]);
            $isEcoOrder = object_get($shippingInfo, 'type', '') == Status::getByValue('Ecomm',
                'ORDER-TYPE') ? true : false;

            $orderDtl = $this->orderDtlModel
                ->allBy('odr_id', $orderHdr->odr_id, ['inventorySummary.item', 'systemUom'])
                ->toArray();

            $cartonModel = new CartonModel();
            foreach ($orderDtl as $key => $item) {
                $sumField = 'avail';
                if ($orderHdr->odr_type === 'XDK') {
                    $sumField = 'crs_doc_qty';
                }
                /*$invtSum = $this->inventorySummaryModel->getInvSumbyOdrDlt(
                    ['item_id' => $item['item_id']],
                    $item['lot'],
                    $orderHdr->whs_id,
                    $sumField
                );*/
                $inventory = DB::table("inventory")->where("whs_id",$orderHdr->whs_id)
                    ->where("cus_id",$orderHdr->cus_id)->where("item_id", $item['item_id'])
                    ->where("type","I")->select("in_hand_qty")->first();
                $avail = (!empty($inventory['in_hand_qty']))?$inventory['in_hand_qty']:0;
                $cartonEcom = $cartonModel->getFirstWhere([
                    'item_id' => $item['item_id'],
                    'is_ecom' => 1
                ]);

                //get is_ecom from carton
                $ecom_qty = object_get($cartonEcom, 'piece_remain', 0);
                //$avail = $invtSum;
                //$avail = (!empty($inventory['in_hand_qty']))?$inventory['in_hand_qty']:0;
                $level = intval(array_get($item, 'system_uom.level_id', 0));
                $uom_qty = null;
                switch ($level) {
                    case 1:
                    case 2:
                        $uom_qty = $item['piece_qty'];
                        break;
                    case 3:
                        $uom_qty = $item['qty'];
                        break;
                    default:
                        break;
                }
                $onHandQty = (int)array_get($item, 'inventory_summary.avail', 0) - (int)array_get($item, 'inventory_summary.lock_qty', 0);
                $temp[$key] = [
                    'odr_dtl_id'    => $item['odr_dtl_id'],
                    'itm_id'        => $item['item_id'],
                    'sku'           => $item['sku'],
                    'color'         => $item['color'],
                    'size'          => $item['size'],
                    'lot'           => $item['lot'],
                    'uom_qty'       => $uom_qty,
                    'uom_id'        => $item['uom_id'],
                    'uom_code'      => array_get($item, 'system_uom.sys_uom_code', null),
                    'uom_name'      => array_get($item, 'system_uom.sys_uom_name', null),
                    'piece_qty'     => $item['piece_qty'],
                    'pack'          => $item['pack'],
                    'qty'           => $item['qty'],
                    'back_odr'      => $item['back_odr'],
                    'avail'         => $avail > 0 ? $avail : 0,
                    'allocated_qty' => $item['alloc_qty'],
                    'cus_upc'       => $item['cus_upc'],
                    'ship_track_id' => $item['ship_track_id'],
                    'uom_level'     => $level,
                    'picked_qty'    => $item['picked_qty'],
                    'on_hand_qty'   => $onHandQty > 0 ? $onHandQty : 0,
                ];

                $orderHdr['items'] = $temp;
            }

            return $this->response->item($orderHdr, $orderDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function submitKitDtl(Request $request, $whsId) {
        try {
            $input = $request->getParsedBody();
            $input['whs_id'] = $whsId;

            $this->validateKitDtl($input);

            $result = $this->ordrHdrModel->submitKitDtl($input);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function validateKitDtl($attribute) {
        if(empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest("This field cus_id is required!");
        }

        if(empty($attribute['odr_id'])) {
            return $this->response->errorBadRequest("This field odr_hdr_id is required!");
        }

        if(empty($attribute['item_id'])) {
            return $this->response->errorBadRequest("This field item_id is required!");
        }

        if(empty($attribute['odr_dtl_id'])) {
            return $this->response->errorBadRequest("This field odr_dtl_id is required!");
        }
        return 0;
    }
}