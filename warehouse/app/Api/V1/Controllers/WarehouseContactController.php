<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MenuFavoriteModel;
use App\Api\V1\Models\WarehouseContactModel;
use App\Api\V1\Models\MenuModel;
use App\Api\V1\Transformers\WarehouseContactTransformer;
use App\Api\V1\Validators\WarehouseContactValidator;
use App\Api\V1\Validators\SaveMassContactValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class WarehouseContactController extends AbstractController
{
    /**
     * @var $warehouseContactModel
     */
    protected $warehouseContactModel;

    /**
     * @var $warehouseContactTransformer
     */
    protected $warehouseContactTransformer;

    /**
     * @var $warehouseContactValidator
     */
    protected $warehouseContactValidator;

    /**
     * @var $saveMassWarehouseContactValidator
     */
    protected $saveMassWarehouseContactValidator;

    /**
     * WarehouseContactController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseContactModel = new WarehouseContactModel();
        $this->warehouseContactTransformer = new WarehouseContactTransformer();
        $this->warehouseContactValidator = new WarehouseContactValidator();
        $this->saveMassWarehouseContactValidator = new SaveMassContactValidator();
    }

    /**
     * @SWG\Get(
     *     path="/v1/warehouses/{warehouseId}/contacts",
     *     summary="List All Warehouse Contact",
     *     description="List All Warehouse Contact",
     *     operationId="listWarehouseContact",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array all Warehouse Contact",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="whs_con_fname", type="string", description=""),
     *             @SWG\Property(property="whs_con_lname", type="string", description=""),
     *             @SWG\Property(property="whs_con_email", type="string", description=""),
     *             @SWG\Property(property="whs_con_phone", type="string", description=""),
     *             @SWG\Property(property="whs_con_mobile", type="string", description=""),
     *             @SWG\Property(property="whs_con_ext", type="string", description=""),
     *             @SWG\Property(property="whs_con_position", type="string", description=""),
     *             @SWG\Property(property="whs_con_whs_id", type="integer", description=""),
     *             @SWG\Property(property="whs_con_department", type="string", description=""),
     *             @SWG\Property(property="created_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="updated_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="deleted_at", type="string", format="date-time", description=""),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_con_id": 1,
     *                     "whs_con_fname": "contac11111",
     *                     "whs_con_lname": "lname 2222222222",
     *                     "whs_con_email": "email@email.com",
     *                     "whs_con_phone": "phone nef",
     *                     "whs_con_mobile": "mobile nè",
     *                     "whs_con_ext": "ext là gì ",
     *                     "whs_con_position": "v? trí nè",
     *                     "whs_con_whs_id": 999,
     *                     "whs_con_department": "department nè",
     *                     "created_at": {
     *                         "date": "2016-05-30 01:50:23.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "updated_at": {
     *                         "date": "2016-05-30 01:50:23.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": "1975-01-01 01:00:00"
     *                 },
     *                 {
     *                     "whs_con_id": 1,
     *                     "whs_con_fname": "contactttttt",
     *                     "whs_con_lname": "bbbbbbbbbbbbbb",
     *                     "whs_con_email": "cccccccccccc",
     *                     "whs_con_phone": "ddddddddddd",
     *                     "whs_con_mobile": "eeeeeeeeee",
     *                     "whs_con_ext": "ffffffffff",
     *                     "whs_con_position": "ggggggggg",
     *                     "whs_con_whs_id": 999,
     *                     "whs_con_department": "hhhhhhhhh",
     *                     "created_at": {
     *                         "date": "2016-05-26 04:29:18.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": null,
     *                 }
     *             }
     *         },
     *     ),
     * )
     *
     * @param int $warehouseId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($warehouseId)
    {
        $warehouseContacts = $this->warehouseContactModel->getByWarehouseId($warehouseId);

        return $this->response->collection($warehouseContacts, $this->warehouseContactTransformer);
    }

    /**
     * @SWG\Get(
     *     path="/v1/warehouses/{warehouseId}/contacts/{contactId}",
     *     summary="Read Warehouse Contact",
     *     description="View Detail Warehouse Contact",
     *     operationId="showWarehouseContact",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="contactId",
     *         description="Warehouse Contact Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Warehouse Contact",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="whs_con_fname", type="string", description=""),
     *             @SWG\Property(property="whs_con_lname", type="string", description=""),
     *             @SWG\Property(property="whs_con_email", type="string", description=""),
     *             @SWG\Property(property="whs_con_phone", type="string", description=""),
     *             @SWG\Property(property="whs_con_mobile", type="string", description=""),
     *             @SWG\Property(property="whs_con_ext", type="string", description=""),
     *             @SWG\Property(property="whs_con_position", type="string", description=""),
     *             @SWG\Property(property="whs_con_whs_id", type="integer", description=""),
     *             @SWG\Property(property="whs_con_department", type="string", description=""),
     *             @SWG\Property(property="created_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="updated_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="deleted_at", type="string", format="date-time", description=""),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_con_id": 1,
     *                     "whs_con_fname": "contactttttt",
     *                     "whs_con_lname": "bbbbbbbbbbbbbb",
     *                     "whs_con_email": "cccccccccccc",
     *                     "whs_con_phone": "ddddddddddd",
     *                     "whs_con_mobile": "eeeeeeeeee",
     *                     "whs_con_ext": "ffffffffff",
     *                     "whs_con_position": "ggggggggg",
     *                     "whs_con_whs_id": 999,
     *                     "whs_con_department": "hhhhhhhhh",
     *                     "created_at": {
     *                         "date": "2016-05-26 04:29:18.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": null
     *                 },
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Warehouse Contact Detail
     *
     * @param int $warehouseId
     * @param int $contactId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function show($warehouseId, $contactId)
    {
        try {
            $warehouseContact = $this->warehouseContactModel->loadBy([
                'whs_con_id'     => $contactId,
                'whs_con_whs_id' => $warehouseId
            ]);

            return $this->response->item($warehouseContact, $this->warehouseContactTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/warehouses/{warehouseId}/contacts",
     *     summary="Create Warehouse Contact",
     *     description="Create Warehouse Contact",
     *     operationId="createWarehouseContact",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_line1",
     *         description="Warehouse Contact's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_line2",
     *         description="Warehouse Contact's Description",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_countrycode",
     *         description="Warehouse Contact's Country Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_cityname",
     *         description="Warehouse Contact's City Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_statecode",
     *         description="Warehouse Contact's State Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_postalcode",
     *         description="Warehouse Contact's Postal Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_con_whs_id",
     *         description="Warehouse Contact's Warehouse Id",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_type",
     *         description="Warehouse Contact's Type",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Warehouse Contact",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="whs_con_fname", type="string", description=""),
     *             @SWG\Property(property="whs_con_lname", type="string", description=""),
     *             @SWG\Property(property="whs_con_email", type="string", description=""),
     *             @SWG\Property(property="whs_con_phone", type="string", description=""),
     *             @SWG\Property(property="whs_con_mobile", type="string", description=""),
     *             @SWG\Property(property="whs_con_ext", type="string", description=""),
     *             @SWG\Property(property="whs_con_position", type="string", description=""),
     *             @SWG\Property(property="whs_con_whs_id", type="integer", description=""),
     *             @SWG\Property(property="whs_con_department", type="string", description=""),
     *             @SWG\Property(property="created_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="updated_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="deleted_at", type="string", format="date-time", description=""),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_con_id": 1,
     *                     "whs_con_fname": "contactttttt",
     *                     "whs_con_lname": "bbbbbbbbbbbbbb",
     *                     "whs_con_email": "cccccccccccc",
     *                     "whs_con_phone": "ddddddddddd",
     *                     "whs_con_mobile": "eeeeeeeeee",
     *                     "whs_con_ext": "ffffffffff",
     *                     "whs_con_position": "ggggggggg",
     *                     "whs_con_whs_id": 999,
     *                     "whs_con_department": "hhhhhhhhh",
     *                     "created_at": {
     *                         "date": "2016-05-26 04:29:18.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": null
     *                 },
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Warehouse Contact Create
     *
     * @param int $warehouseId
     * @param Request $request
     *
     * @return mixed
     */
    public function store(
        $warehouseId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseContactValidator->validate($input);

        $params = [
            'whs_con_fname'      => $input['whs_con_fname'],
            'whs_con_lname'      => !empty($input['whs_con_lname']) ? $input['whs_con_lname'] : '',
            'whs_con_email'      => $input['whs_con_email'],
            'whs_con_phone'      => $input['whs_con_phone'],
            'whs_con_mobile'     => array_get($input, 'whs_con_mobile', ''),
            'whs_con_ext'        => array_get($input, 'whs_con_ext', ''),
            'whs_con_position'   => $input['whs_con_position'],
            'whs_con_whs_id'     => $warehouseId,
            'whs_con_department' => $input['whs_con_department'],
        ];

        try {
            if ($warehouseContact = $this->warehouseContactModel->create($params)) {
                return $this->response->item($warehouseContact,
                    $this->warehouseContactTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Put(
     *     path="/v1/warehouses/{warehouseId}/contacts/{contactId}",
     *     summary="Update Warehouse Contact",
     *     description="Update Warehouse Contact",
     *     operationId="updateWarehouseContact",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="contactId",
     *         description="Warehouse Contact Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_line1",
     *         description="Warehouse Contact's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_line2",
     *         description="Warehouse Contact's Description",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_countrycode",
     *         description="Warehouse Contact's Country Code",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_cityname",
     *         description="Warehouse Contact's City Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_statecode",
     *         description="Warehouse Contact's State Code",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_postalcode",
     *         description="Warehouse Contact's Postal Code",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_con_whs_id",
     *         description="Warehouse Contact's Warehouse Id",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_type",
     *         description="Warehouse Contact's Type",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Warehouse Contact",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="whs_con_fname", type="string", description=""),
     *             @SWG\Property(property="whs_con_lname", type="string", description=""),
     *             @SWG\Property(property="whs_con_email", type="string", description=""),
     *             @SWG\Property(property="whs_con_phone", type="string", description=""),
     *             @SWG\Property(property="whs_con_mobile", type="string", description=""),
     *             @SWG\Property(property="whs_con_ext", type="string", description=""),
     *             @SWG\Property(property="whs_con_position", type="string", description=""),
     *             @SWG\Property(property="whs_con_whs_id", type="integer", description=""),
     *             @SWG\Property(property="whs_con_department", type="string", description=""),
     *             @SWG\Property(property="created_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="updated_at", type="object", description="", properties={
     *                 @SWG\Property(property="date", type="string", format="date-time", description=""),
     *                 @SWG\Property(property="timezone_type", type="integer", description=""),
     *                 @SWG\Property(property="timezone", type="string", description=""),
     *             }),
     *             @SWG\Property(property="deleted_at", type="string", format="date-time", description=""),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_con_id": 1,
     *                     "whs_con_fname": "contactttttt",
     *                     "whs_con_lname": "bbbbbbbbbbbbbb",
     *                     "whs_con_email": "cccccccccccc",
     *                     "whs_con_phone": "ddddddddddd",
     *                     "whs_con_mobile": "eeeeeeeeee",
     *                     "whs_con_ext": "ffffffffff",
     *                     "whs_con_position": "ggggggggg",
     *                     "whs_con_whs_id": 999,
     *                     "whs_con_department": "hhhhhhhhh",
     *                     "created_at": {
     *                         "date": "2016-05-26 04:29:18.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": null
     *                 },
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * @param int $warehouseId
     * @param int $contactId
     * @param Request $request
     *
     * @return mixed
     * @internal param int $whs_con_id
     */
    public function update(
        $warehouseId,
        $contactId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseContactValidator->validate($input);

        $params = [
            'whs_con_id'         => $contactId,
            'whs_con_lname'      => $input['whs_con_lname'],
            'whs_con_fname'      => $input['whs_con_fname'],
            'whs_con_email'      => $input['whs_con_email'],
            'whs_con_phone'      => $input['whs_con_phone'],
            'whs_con_mobile'     => array_get($input, 'whs_con_mobile', ''),
            'whs_con_ext'        => array_get($input, 'whs_con_ext', ''),
            'whs_con_position'   => $input['whs_con_position'],
            'whs_con_whs_id'     => $warehouseId,
            'whs_con_department' => $input['whs_con_department'],
        ];

        try {
            if ($warehouseContact = $this->warehouseContactModel->update($params)) {
                return $this->response->item($warehouseContact, $this->warehouseContactTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Delete(
     *     path="/v1/warehouses/{warehouseId}/contacts/{contactId}",
     *     summary="Delete Warehouse Contact",
     *     description="Delete a Warehouse Contact",
     *     operationId="contactId",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="contactId",
     *         description="Warehouse Contact Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Warehouse Contact was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param int $warehouseId
     * @param int $contactId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     * @internal param MenuModel $menuModel
     * @internal param MenuFavoriteModel $menuFavoriteModel
     *
     */
    public function destroy($warehouseId, $contactId)
    {
        try {
            if ($this->warehouseContactModel->deleteWarehouseContact($warehouseId, $contactId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function saveMassAndDeleteUnused(
        $warehouseId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->saveMassWarehouseContactValidator->validate($input);

        $contacts = $this->warehouseContactModel->getByWarehouseId($warehouseId);

        $contacts = array_pluck($contacts, 'whs_con_lname', 'whs_con_id');

        foreach ($input['whs_con_fname'] as $key => $val) {
            if (isset($contacts[$input['whs_con_id'][$key]])) {
                $action = 'update';
                unset($contacts[$input['whs_con_id'][$key]]);
            } else {
                $action = 'create';
            }

            $params = [
                'whs_con_id'         => array_get($input, 'whs_con_id.' . $key, null),
                'whs_con_fname'      => array_get($input, 'whs_con_fname.' . $key, ''),
                'whs_con_lname'      => array_get($input, 'whs_con_lname.' . $key, ''),
                'whs_con_email'      => array_get($input, 'whs_con_email.' . $key, ''),
                'whs_con_phone'      => array_get($input, 'whs_con_phone.' . $key, ''),
                'whs_con_mobile'     => array_get($input, 'whs_con_mobile.' . $key, ''),
                'whs_con_ext'        => array_get($input, 'whs_con_ext.' . $key, ''),
                'whs_con_position'   => array_get($input, 'whs_con_position.' . $key, ''),
                'whs_con_whs_id'     => $warehouseId,
                'whs_con_department' => array_get($input, 'whs_con_department.' . $key, ''),
            ];

            try {
                $this->warehouseContactModel->refreshModel();
                $this->warehouseContactModel->{$action}($params);
            } catch (\PDOException $e) {
                return  $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }

        /**
         * delete unused
         */
        if ($contacts) {
            try {
                $this->warehouseContactModel->refreshModel();
                $this->warehouseContactModel->deleteWarehouseContact($warehouseId, array_keys($contacts));
            } catch (\PDOException $e) {
                return  $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
        }

        return $this->response->noContent();
    }
}
