<?php

namespace App\Api\V2\Inbound\ScanPallet\Validators;


class WavePickEAValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'ctn_id' => 'required|integer',
            'ctnr_rfid' => 'required|integer',
            'piece_qty' => 'required|integer',
        ];

    }
}
