<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\UserDepartment;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Utils\Status;

class UserDepartmentTransformer extends TransformerAbstract
{
    public function transform(UserDepartment $userDepartment)
    {
        $countUser = $userDepartment->user()->count();
        return [
            'usr_dpm_id'   => $userDepartment->usr_dpm_id,
            'usr_dpm_code' => $userDepartment->usr_dpm_code,
            'usr_dpm_name' => $userDepartment->usr_dpm_name,
            'usr_dpm_des'  => $userDepartment->usr_dpm_des,
            'num_of_staff' => $countUser,
            'created_at'   => ($userDepartment->created_at) ?
                date('m/d/Y', strtotime($userDepartment->created_at)) : '',
            'updated_at'   => ($userDepartment->updated_at) ?
                date('m/d/Y', strtotime($userDepartment->updated_at)) : '',
            'created_by'   => trim(object_get($userDepartment, "createdBy.first_name", null) . " " .
                object_get($userDepartment, "createdBy.last_name", null))

        ];
    }
}
