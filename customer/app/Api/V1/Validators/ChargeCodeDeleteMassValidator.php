<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:00
 */

namespace App\Api\V1\Validators;

class ChargeCodeDeleteMassValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'chg_code_id'   => 'required',
            'chg_code_id.*' => 'required|exists:charge_code,chg_code_id'
        ];
    }
}
