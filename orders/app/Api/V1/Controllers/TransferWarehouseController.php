<?php

namespace App\Api\V1\Controllers;


use App\Api\V1\Models\AbstractModel;
use App\Api\V1\Models\BOLOrderDetailModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\OdrHdrMetaModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\OutPalletModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\ShipmentModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Models\ThirdPartyModel;
use App\Api\V1\Models\UserMetaModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Models\WavepickDtlModel;
use App\Api\V1\Models\WavepickHdrModel;
use App\Api\V1\Models\WorkOrderDtlModel;
use App\Api\V1\Models\WorkOrderModel;
use App\Api\V1\Traits\OrderFlowControllerTrait;
use App\Api\V1\Traits\OrderHdrControllerTrait;
use App\Api\V1\Transformers\OrderCancelListTransformer;
use App\Api\V1\Transformers\OrderDtlTransformer;
use App\Api\V1\Transformers\OrderHdrTransformer;
use App\Api\V1\Transformers\PackHdrTransformer;
use App\Api\V1\Validators\CsrValidator;
use App\Api\V1\Validators\OrderHdrValidator;
use App\Api\V1\Validators\OrderImportValidator;
use App\Api\V1\Validators\OrderMultiCSRValidator;
use App\Jobs\OrderFlowJob;
use App\Jobs\SyncAsnJob;
use App\Library\OrderImport;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Dingo\Api\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Http\Request as IRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WorkOrderHdr;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use TCPDF;
use Tymon\JWTAuth\Utils;
use Wms2\UserInfo\Data;
use mPDF;

class TransferWarehouseController extends AbstractController
{
    use OrderHdrControllerTrait;
    use OrderFlowControllerTrait;

    const ORDER_DOCUMENT_FOLDER = 'order-document';

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var ShippingOrderModel
     */
    protected $shippingOrderModel;

    /**
     * @var UserModel
     */
    protected $userModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var OrderImportValidator
     */
    protected $orderImportValidator;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var WavepickHdrModel
     */
    protected $wavepickHdrModel;

    /**
     * @var WavepickDtlModel
     */
    protected $wavepickDtlModel;

    /**
     * @var WorkOrderModel
     */
    protected $workOrderModel;

    /**
     * @var WorkOrderDtlModel
     */
    protected $workOrderDtlModel;

    /**
     * @var $customerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var $outPalletModel
     */
    protected $outPalletModel;

    /**
     * @var BOLOrderDetailModel
     */
    protected $bolOrderDetailModel;

    /**
     * @var Order Hdr Meta Model
     */
    protected $orderHdrMetaModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    protected $shipmentModel;

    /**
     * @var ThirdPartyModel
     */
    protected $thirdPartyModel;

    protected $userMetaModel;


    /**
     * OrderHdrController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param ShippingOrderModel $shippingOrderModel
     * @param UserModel $userModel
     * @param EventTrackingModel $eventTrackingModel
     * @param WarehouseModel $warehouseModel
     * @param ItemModel $itemModel
     * @param OrderImportValidator $orderImportValidator
     * @param PackHdrModel $packHdrModel
     * @param PackDtlModel $packDtlModel
     * @param WavepickHdrModel $wavepickHdrModel
     * @param WavepickDtlModel $wavepickDtlModel
     * @param WorkOrderDtlModel $workOrderDtlModel
     * @param WorkOrderModel $workOrderModel
     * @param CustomerConfigModel $customerConfigModel
     * @param OutPalletModel $outPalletModel
     * @param BOLOrderDetailModel $bolOrderDetailModel
     * @param ShipmentModel $shipmentModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        InventorySummaryModel $inventorySummaryModel,
        ShippingOrderModel $shippingOrderModel,
        UserModel $userModel,
        EventTrackingModel $eventTrackingModel,
        WarehouseModel $warehouseModel,
        ItemModel $itemModel,
        OrderImportValidator $orderImportValidator,
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel,
        WavepickHdrModel $wavepickHdrModel,
        WavepickDtlModel $wavepickDtlModel,
        WorkOrderDtlModel $workOrderDtlModel,
        WorkOrderModel $workOrderModel,
        CustomerConfigModel $customerConfigModel,
        OutPalletModel $outPalletModel,
        BOLOrderDetailModel $bolOrderDetailModel,
        ShipmentModel $shipmentModel
    ) {

        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->shippingOrderModel = $shippingOrderModel;
        $this->userModel = $userModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->warehouseModel = $warehouseModel;
        $this->itemModel = $itemModel;
        $this->orderImportValidator = $orderImportValidator;
        $this->packHdrModel = $packHdrModel;
        $this->wavepickHdrModel = $wavepickHdrModel;
        $this->wavepickDtlModel = $wavepickDtlModel;
        $this->workOrderDtlModel = $workOrderDtlModel;
        $this->workOrderModel = $workOrderModel;
        $this->packDtlModel = $packDtlModel;
        $this->customerConfigModel = $customerConfigModel;
        $this->outPalletModel = $outPalletModel;
        $this->bolOrderDetailModel = $bolOrderDetailModel;
        $this->orderHdrMetaModel = new OdrHdrMetaModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->shipmentModel = $shipmentModel;
        $this->thirdPartyModel = new ThirdPartyModel();
        $this->userMetaModel = new UserMetaModel();
    }


    public function store(
        Request $request,
        OrderHdrValidator $orderHdrValidator,
        OrderHdrTransformer $orderHdrTransformer,
        CustomerMetaModel $customerMetaModel,
        OrderHdrMetaModel $orderHdrMetaModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['whs_id'] = (isset($input['edi']) && isset($input['whs_id'])) ? $input['whs_id'] : AbstractModel::getCurrentWhsId();
        $input['whs_id_to'] = (isset($input['whs_id_to'])) ? $input['whs_id_to'] : null;
        $csrId = $customerMetaModel->getPNP($input['cus_id'], 'CSR', $input['whs_id']) ?: Data::getCurrentUserId();
        // validation
        if (!isset($input['edi'])) {
            $orderHdrValidator->validate($input);
            if($input['odr_type'] == 'RTL'){
                if(empty($input['carrier_code'])){
                    throw new \Exception("Carrier Code is required");
                }
                if(empty($input['ship_to_phone'])){
                    throw new \Exception("Ship to phone is required");
                }
                if(empty($input['dlvy_srvc_code'])){
                    throw new \Exception("Delievry Service Code is required");
                }
            }
        }
        try {
            DB::beginTransaction();

            // Update Or Insert Third party
            $thirdParty = $this->createOrUpdateThirdParty($input);

            //value to save
            $status = Status::getByValue("New", "Order-Status");

            $params = [
                // Order Hdr
                'carrier'          => $input['carrier'],
                'ship_to_name'     => $input['ship_to_name'],
                'ship_to_add_1'    => $input['ship_to_add_1'],
                'ship_to_add_2'    => array_get($input, 'ship_to_add_2', null),
                'ship_to_city'     => $input['ship_to_city'],
                'ship_to_country'  => $input['ship_to_country'],
                'ship_to_state'    => $input['ship_to_state'],
                'ref_cod'          => array_get($input, 'ref_cod', null),
                'ship_to_zip'      => $input['ship_to_zip'],
                'ship_by_dt'       => !empty($input['ship_by_dt']) ? strtotime($input['ship_by_dt']) : 0,
                'cancel_by_dt'     => !empty($input['cancel_by_dt']) ? strtotime($input['cancel_by_dt']) : 0,
                'req_cmpl_dt'      => $this->getCompleteDateByShipDate($input['ship_by_dt']),
                'act_cmpl_dt'      => strtotime(array_get($input, 'act_cmpl_dt', null)),
                'act_cancel_dt'    => strtotime(array_get($input, 'act_cancel_dt', null)),
                'in_notes'         => array_get($input, 'in_notes', null),
                'cus_notes'        => array_get($input, 'cus_notes', null),
                'csr_notes'        => array_get($input, 'csr_notes', null),
                // Order Info
                'cus_id'           => $input['cus_id'],
                'whs_id'           => $input['whs_id'],
                'whs_id_to'        => $input['whs_id_to'],
                'cus_odr_num'      => $input['cus_odr_num'],
                'cus_po'           => $input['cus_po'],
                'odr_type'         => $input['odr_type'],
                'rush_odr'         => (int)array_get($input, 'rush_odr', null),
                'odr_sts'          => $status,
                'sku_ttl'          => count($input['items']),
                'is_ecom'          => $input['odr_type'] == 'EC' ? 1 : (int)array_get($input, 'is_ecom', 0),
                'shipping_addr_id' => $thirdParty->tp_id
            ];

            if($input['odr_type'] == 'RTL'){
                $params['carrier_code'] = array_get($input, 'carrier_code', null);
                $params['ship_to_phone'] = array_get($input, 'ship_to_phone', null);
                $params['dlvy_srvc'] = array_get($input, 'dlvy_srvc', null);
                $params['dlvy_srvc_code'] = array_get($input, 'dlvy_srvc_code', null);
                $params['cosignee_sig'] = array_get($input, 'cosignee_sig', 0);
                $params['csr'] = $csrId;
            }

            $odrNum = $this->orderHdrModel->getOrderNum();
            $params['odr_num'] = $odrNum;

            $params['so_id'] = $this->getShippingOdr($input);

            // Check duplicate
            $this->orderHdrModel->refreshModel();
            if ($this->orderHdrModel->checkWhere([
                'whs_id'      => $input['whs_id'],
                'cus_id'      => $input['cus_id'],
                'cus_odr_num' => $input['cus_odr_num'],
                'cus_po'      => $input['cus_po'],
                'odr_type'    => $input['odr_type'],
                'odr_sts'    => array('odr_sts', '<>', 'CC')
            ])
            ) {
                if (isset($input['edi'])) {
                    $return = [
                        'data'    => null,
                        'message' => Message::get("BM006", "Warehouse-Customer-OrderID-PO-Type")
                    ];

                    return json_encode($return);
                } else {
                    throw new \Exception(Message::get("BM006", "Warehouse-Customer-OrderID-PO-Type"));
                }
            }

            // Insert Order Header
            $this->orderHdrModel->refreshModel();
            $orderHdr = $this->orderHdrModel->create($params);

            // Insert Order Detail
            if (!empty($input['items'])) {
                foreach ($input['items'] as $detail) {
                    if ($input['odr_type'] == 'EC'){
                        $detail['itm_id'] = $this->_createNewItemEcom($detail['itm_id'], $input['whs_id']);
                    }

                    $this->orderDtlModel->refreshModel();
                    $detail['whs_id'] = $input['whs_id'];
                    $detail['cus_id'] = $input['cus_id'];
                    $detail['odr_id'] = $orderHdr->odr_id;
                    $detail['item_id'] = $detail['itm_id'];
                    $detail['pack'] = empty($detail['pack']) ? null : $detail['pack'];
                    $detail['uom_id'] = empty($detail['uom_id']) ? null : $detail['uom_id'];
                    $detail['uom_code'] = empty($detail['uom_code']) ? null : $detail['uom_code'];
                    $detail['qty'] = empty($detail['qty']) ? null : $detail['qty'];
                    // $detail['ship_track_id'] = $input['odr_type'] == 'EC' ? $detail['ship_track_id'] : null;
                    $detail['sts'] = "i";
                    $this->orderDtlModel->create($detail);
                }
            }

            // Insert Event Tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $input['whs_id'],
                'cus_id'    => $input['cus_id'],
                'owner'     => $odrNum,
                'evt_code'  => Status::getByKey("event", "Order-New"),
                'trans_num' => $odrNum,
                'info'      => sprintf(Status::getByKey("event-info", "Order-New"), $odrNum)

            ]);

            // Change request for Indo Demo - 7/12/2016
            // Insert data to table odr_hdr_meta
            $this->orderHdrMetaModel->insertOdrHdrMeta([
                'odr_id'     => $orderHdr->odr_id,
                'qualifier'  => 'OFP',
                'value'      => array_get($input, 'pack_odr', 0),
                'created_at' => time()
            ]);
            // get oder flow from order meta
            $oderFlow = array_get($input, 'qualifier.value', []);
            if (!empty($oderFlow)) {
                $valueArrJson = [];
                foreach ($oderFlow as $getVal) {
                    $value = [
                        'odr_flow_id' => array_get($getVal, 'odr_flow_id', ''),
                        'step'        => array_get($getVal, 'step', ''),
                        'flow_code'   => array_get($getVal, 'flow_code', ''),
                        'odr_sts'     => array_get($getVal, 'odr_sts', ''),
                        'name'        => array_get($getVal, 'name', ''),
                        'description' => array_get($getVal, 'description', null),
                        'dependency'  => array_get($getVal, 'dependency', 0),
                        'type'        => array_get($getVal, 'type', ''),
                        'usage'       => array_get($getVal, 'usage', '')
                    ];
                    if($orderHdr->odr_type == 'RTL' && in_array(array_get($getVal, 'odr_flow_id', ''), [7, 11, 12])){
                        $value['usage'] = 1;
                    }
                    $valueJson = json_encode($value);
                    array_push($valueArrJson, $valueJson);
                }
                $valueJsons = json_encode($valueArrJson);
                $this->orderHdrMetaModel->insertOdrHdrMeta([
                    'odr_id'    => $orderHdr->odr_id,
                    'qualifier' => 'OFF',
                    'value'     => $valueJsons
                ]);

            } else {
                $cus_meta = $customerMetaModel->getFirstWhere([
                    'cus_id'    => intval($input['cus_id']),
                    'qualifier' => 'OFF'
                ]);
                $this->orderHdrMetaModel->insertOdrHdrMeta([
                    'odr_id'    => $orderHdr->odr_id,
                    'qualifier' => 'OFF',
                    'value'     => object_get($cus_meta, 'value', '')
                ]);
            }

            DB::commit();

            if ($orderHdr->odr_type != 'XDK') {
                dispatch(new OrderFlowJob($orderHdr->odr_id, 1, $input['cus_id'], $request, $customerMetaModel,
                    $orderHdrMetaModel));
            }

            // sync asn to transfer warehouse
//            if ($input['odr_type'] == "WHS") {
//                if (is_null($input['whs_id_to']) || empty($input['whs_id_to'])) {
//                    throw new \Exception("Warehouse transfer ID is required");
//                }
//                dispatch(new SyncAsnJob($orderHdr, $input['whs_id_to'], $request));
//            }

            return $this->response->item($orderHdr, $orderHdrTransformer)->setStatusCode(Response::HTTP_CREATED);
        } catch (\PDOException $e) {
            DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function createOrUpdateThirdParty(
        $input
    ) {
        $whs_id = array_get($input, 'whs_id', null);
        $name = array_get($input, 'ship_to_name', null);
        $add_1 = array_get($input, 'ship_to_add_1', null);
        $add_2 = array_get($input, 'ship_to_add_2', null);
        $city = array_get($input, 'ship_to_city', null);
        $state = array_get($input, 'ship_to_state', null);
        $zip = array_get($input, 'ship_to_zip', null);
        $country = array_get($input, 'ship_to_country', null);
        $cus_id = array_get($input, 'cus_id');

        $thirdParty = $this->thirdPartyModel->getFirstBy(DB::raw("UPPER(REPLACE(name, ' ', ''))"),
            strtoupper(preg_replace('/\s+/', '', $name)));

        // Update
        if ($thirdParty) {

            $thirdParty->whs_id = $whs_id;
            $thirdParty->cus_id = $cus_id;
            $thirdParty->name = $name;
            $thirdParty->add_1 = $add_1;
            $thirdParty->add_2 = $add_2;
            $thirdParty->city = $city;
            $thirdParty->state = $state;
            $thirdParty->zip = $zip;
            $thirdParty->country = $country;
            $thirdParty->save();

        } else {
            // Create
            $thirdParty = $this->thirdPartyModel->create([
                'type'    => 'CUS',
                'name'    => $name,
                'whs_id'  => $whs_id,
                'cus_id'  => $cus_id,
                'add_1'   => $add_1,
                'add_2'   => $add_2,
                'city'    => $city,
                'state'   => $state,
                'zip'     => $zip,
                'country' => $country
            ]);
        }

        return $thirdParty;

    }
    private function getCompleteDateByShipDate($shipDate)
    {
        if (!$shipDate) {
            return null;
        }
        $currentDateTimestamp = strtotime(date('Y-m-d'));
        $shipDateTimestamp = strtotime($shipDate);

        if ($shipDateTimestamp < $currentDateTimestamp) {
            return $shipDateTimestamp;
        }

        if ($shipDateTimestamp - $currentDateTimestamp >= 3 * 86400) {
            return $shipDateTimestamp - 3 * 86400;
        } else {
            return $currentDateTimestamp;
        }
    }

    /**
     * @param $input
     * @param bool $isUpdate
     *
     * @return mixed
     */
    private function getShippingOdr(
        $input,
        $oldRef = null,
        $isUpdate = false
    ) {
        $this->shippingOrderModel->refreshModel();

        $shipParams = [
            'cus_id'      => $input['cus_id'],
            'whs_id'      => $input['whs_id'],
            'cus_odr_num' => $input['cus_odr_num'],
            'so_sts'      => Status::getByValue("New", "Order-Status"),
            'type'        => $input['odr_type'],
        ];

        // Create Shipping Order.
        $so = $this->shippingOrderModel->getFirstWhere([
            'cus_odr_num' => $shipParams['cus_odr_num'],
            'whs_id'      => $shipParams['whs_id'],
            'cus_id'      => $shipParams['cus_id']
        ]);

        if (empty($so)) {
            // Create Shipping Odr
            $shipParams['po_total'] = 1;
            $soId = $this->shippingOrderModel->create($shipParams)->so_id;
        } else {
            // Update po_total
            $soId = $so->so_id;
            if ($isUpdate) {
                if ($oldRef !== $shipParams['cus_odr_num']) {
                    $shipParams['po_total'] = (int)$so->po_total + 1;
                    $shipParams['so_id'] = $soId;
                    $soId = $this->shippingOrderModel->update($shipParams)->so_id;
                }
            }
        }

        return $soId;
    }

    private function _createNewItemEcom($itemId, $whsId)
    {
        $iTemp = DB::table('item')->where('item_id', $itemId)->first();
        $tempInvtSmr = DB::table('invt_smr')->where('item_id', $itemId)->first();
        if(!empty($iTemp) && $iTemp['pack'] != 1){
            unset($iTemp['item_id']);
            $iTemp['pack'] = 1;
            $nTemp = $this->itemModel->create($iTemp);
            $itemId = $nTemp->item_id;
        }
        // insert new invt-smr
        if(!empty($tempInvtSmr) && $tempInvtSmr['lot'] != 'ECO'){
            unset($tempInvtSmr['inv_sum_id']);
            $tempInvtSmr['item_id'] = $itemId;
            $tempInvtSmr['lot'] = 'ECO';
            $tempInvtSmr['ttl'] = 0;
            $tempInvtSmr['picked_qty'] = 0;
            $tempInvtSmr['allocated_qty'] = 0;
            $tempInvtSmr['dmg_qty'] = 0;
            $tempInvtSmr['avail'] = 0;
            $tempInvtSmr['back_qty'] = 0;
            $tempInvtSmr['ecom_qty'] = 0;
            $tempInvtSmr['lock_qty'] = 0;
            $tempInvtSmr['crs_doc_qty'] = 0;
            $tempInvtSmr['created_at'] = time();
            $tempInvtSmr['updated_at'] = time();
            $nInvtSmr = DB::table('invt_smr')->insert($tempInvtSmr);
        } elseif (!empty($iTemp)) {
            $tempInvtSmr['item_id'] = $itemId;
            $tempInvtSmr['cus_id'] = $iTemp['cus_id'];
            $tempInvtSmr['whs_id'] = $whsId;
            $tempInvtSmr['sku'] = $iTemp['sku'];
            $tempInvtSmr['color'] = $iTemp['color'];
            $tempInvtSmr['size'] = $iTemp['size'];
            $tempInvtSmr['upc'] = $iTemp['cus_upc'];
            $tempInvtSmr['lot'] = 'ECO';
            $tempInvtSmr['ttl'] = 0;
            $tempInvtSmr['picked_qty'] = 0;
            $tempInvtSmr['allocated_qty'] = 0;
            $tempInvtSmr['dmg_qty'] = 0;
            $tempInvtSmr['avail'] = 0;
            $tempInvtSmr['back_qty'] = 0;
            $tempInvtSmr['ecom_qty'] = 0;
            $tempInvtSmr['lock_qty'] = 0;
            $tempInvtSmr['crs_doc_qty'] = 0;
            $tempInvtSmr['created_at'] = time();
            $tempInvtSmr['updated_at'] = time();
            $nInvtSmr = DB::table('invt_smr')->insert($tempInvtSmr);
        }

        return $itemId;
    }

    /**
     * @param $input
     *
     * @return $this|void
     */
    private function exportGRCSV($input)
    {
        try {
            $orderHdr = $this->orderHdrModel->search($input, [
                'shippingOrder',
                'waveHdr',
                'details'
            ], null, true);

            $arrShipIds = array_pluck($orderHdr, 'ship_id');
            $arrShipIds = array_filter($arrShipIds);
            if ($arrShipIds) {
                $shipMents = $this->shippingOrderModel->getShipStsByShipIds($arrShipIds);
                if ($shipMents) {
                    $arrShipSts = [];
                    foreach ($shipMents as $shipMent) {
                        $arrShipSts[$shipMent->ship_id] = [
                            'ship_sts' => $shipMent->ship_sts,
                            'bo_num'   => $shipMent->bo_num
                        ];
                    }

                    foreach ($orderHdr as $order) {
                        if ($order->ship_id
                            && !empty($arrShipSts[$order->ship_id])
                        ) {
                            $order->ship_sts = array_get($arrShipSts[$order->ship_id], 'ship_sts', null);
                            $order->bo_num = array_get($arrShipSts[$order->ship_id], 'bo_num', null);
                        }
                    }
                }
            }

            $orderinfos = [];
            if ($orderHdr) {
                foreach ($orderHdr as $orderHd) {

                    $createdAt = date("m/d/Y", strtotime($orderHd->created_at));
                    $OrderInfo = new OrderHdrModel();
                    $workOrderHdrInfo = WorkOrderHdr::where('odr_hdr_id', $orderHd->odr_id)->first();
                    $statusName = Status::getByKey("Order-Status", $orderHd->odr_sts);
                    if ($orderHd->back_odr) {
                        $statusName = str_replace('Partial ', '', $statusName);
                        $statusName = 'Partial ' . $statusName;
                    }

                    $orderinfos[] = [
                        'odr_sts_name'  => $statusName,
                        'odr_num'       => object_get($orderHd, 'odr_num', null),
                        'cus_name'      => object_get($orderHd, 'customer.cus_code', null),
                        'odr_type_name' => Status::getByKey("Order-Type", object_get($orderHd, 'odr_type', null)),
                        'back_odr'      => object_get($orderHd, 'back_odr', null) ? 'YES' : "NO",
                        'bol'           => Status::getByKey("SHIP-STATUS", $orderHd->ship_sts)
                            ? Status::getByKey("SHIP-STATUS", $orderHd->ship_sts)
                            : 'No Bol',
                        'cus_odr_num'   => object_get($orderHd, 'cus_odr_num', null),
                        'cus_po'        => object_get($orderHd, 'cus_po', null),
                        'ship_to_name'  => $orderHd->ship_to_name,
                        'ship_by_dt'    => empty($orderHd->ship_by_dt) ? null :
                            date("m/d/Y", $orderHd->ship_by_dt),
                        'cancel_by_dt'  => empty($orderHd->cancel_by_dt) ? null :
                            date("m/d/Y", $orderHd->cancel_by_dt),
                        // 'act_cancel_dt' => empty($orderHd->act_cancel_dt) ? null :
                        // date("m/d/Y", $orderHd->act_cancel_dt),
                        'shipped_dt'    => empty($orderHd->shipped_dt) ? null :
                            date("m/d/Y", $orderHd->shipped_dt),
                        'csr_name'      => trim(object_get($orderHd, 'csrUser.first_name', null) . " " .
                            object_get($orderHd, 'csrUser.last_name', null)),
                        'created_by'    => trim(object_get($orderHd, "createdBy.first_name", null) . " " .
                            object_get($orderHd, "createdBy.last_name", null)),
                        'created_at'    => $createdAt,

                    ];
                }
            }

            $title = [
                'odr_sts_name'  => 'Status',
                'odr_num'       => 'Order Number',
                'cus_name'      => 'Customer',
                'odr_type_name' => 'Order Type',
                'back_odr'      => 'Back Order',
                'bol'           => 'BOL',
                'cus_odr_num'   => 'Customer Order',
                'cus_po'        => 'PO',
                'ship_to_name'  => 'Shipping To',
                'ship_by_dt'    => 'Ship By Date',
                'cancel_by_dt'  => 'Cancel By Date',
                // 'act_cancel_dt' => 'Actual Cancel Date',
                'shipped_dt'    => 'Shipped Date',
                'csr_name'      => 'CSR',
                'created_by'    => 'User',
                'created_at'    => 'Created Date',
            ];

            // $filePath = storage_path() . "/Report_PBPM_{$warehouse->whs_name}.csv";

            $today = time();
            $filePath = "Report_Order_{$today}";
            $this->saveFile($title, $orderinfos, $filePath);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
