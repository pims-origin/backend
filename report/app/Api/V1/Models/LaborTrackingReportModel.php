<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ReportLaborTracking;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;

class LaborTrackingReportModel extends AbstractModel
{
    public function __construct(ReportLaborTracking $model=null)
    {
        $this->model = ($model) ?: new ReportLaborTracking();
    }

    public function popupDetail($whs_id,$owner,$trans_num,$user_id)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $data = $this->getModel()
        ->select([
            'start_time',
            'end_time',
        ])
        ->where('rpt_labor_tracking.whs_id',$whs_id)
        ->where('rpt_labor_tracking.owner',$owner)
        ->where('rpt_labor_tracking.trans_num',$trans_num)
        ->where('rpt_labor_tracking.user_id',$user_id)
        ->get();
        foreach ($data as $row){
            $row->total_time=($row->end_time-$row->start_time)/3600;
            $row->total_time=number_format(max($row->total_time,0),3);
            $row->start_time=date('Y/m/d H:i:s',$row->start_time);
            $row->end_time=empty($row->end_time)?null:date('Y/m/d H:i:s',$row->end_time);
        }
        return $data;
    }

    public function search($attributes = [], $with =[], $limit = 20)
    {
        $query = $this->getModel()
            ->select([
                DB::raw('CONCAT_WS(" ",first_name,last_name) as fullname'),
                'customer.cus_name',
                'owner',
                'trans_num',
                DB::raw('MIN(start_time) as start_time'),
                DB::raw('MAX(end_time) as end_time'),
                DB::raw('SUM(rpt_labor_tracking.end_time - rpt_labor_tracking.start_time)/3600 as total_time'),
                'lt_type',
                'odr_hdr.cus_po as po_num',
                'rpt_labor_tracking.user_id',
            ])
            ->leftJoin('users', 'users.user_id', '=', 'rpt_labor_tracking.user_id')
            ->leftJoin('customer', 'customer.cus_id', '=', 'rpt_labor_tracking.cus_id')
            ->leftJoin('odr_hdr', 'odr_hdr.odr_num', '=', 'rpt_labor_tracking.owner')
            ->where('rpt_labor_tracking.whs_id', $attributes['whs_id'])
            ->whereNotNull('rpt_labor_tracking.start_time')
            ->whereNotNull('rpt_labor_tracking.end_time')
            ->whereRaw('rpt_labor_tracking.start_time <> rpt_labor_tracking.end_time');

        if(!empty($attributes['cus_id'])) {
            $query->where('rpt_labor_tracking.cus_id', $attributes['cus_id']);
        }

        if(!empty($attributes['username'])) {
            $query->where(function($where) use ($attributes) {
                $where->where('users.first_name', 'like', '%'.$attributes['username'].'%');
                $where->orWhere('users.last_name', 'like', '%'.$attributes['username'].'%');
            });
        }

        if(!empty($attributes['owner'])) {
            $query->where(function($where) use ($attributes) {
                $where->where('rpt_labor_tracking.owner', $attributes['owner']);
                $where->orWhere('rpt_labor_tracking.trans_num', $attributes['owner']);
            });
        }

        if(!empty($attributes['start_date'])) {
            $query->where('rpt_labor_tracking.start_time', '>=', strtotime($attributes['start_date']));
        }

        if(!empty($attributes['end_date'])) {
            $query->where('rpt_labor_tracking.end_time', '<=', strtotime($attributes['end_date'] . ' 23:59:59'));
        }

        if(!empty($attributes['type'])) {
            $query->where('rpt_labor_tracking.lt_type', $attributes['type']);
        }

        $query->groupBy('rpt_labor_tracking.owner');
        $query->groupBy('rpt_labor_tracking.trans_num');
        $query->groupBy('rpt_labor_tracking.user_id');

        if (isset($attributes['sort']['total_hours']) && !empty($attributes['sort']['total_hours'])) {
            $query->orderBy('total_time', $attributes['sort']['total_hours']);
        }
        if (isset($attributes['sort']['customer']) && !empty($attributes['sort']['customer'])) {
            $query->orderBy('customer.cus_name', $attributes['sort']['customer']);
        }
        if (isset($attributes['sort']['fullname']) && !empty($attributes['sort']['fullname'])) {
            $query->orderBy('fullname', $attributes['sort']['fullname']);
        }

        $this->sortBuilder($query, $attributes);

//        $query->orderBy("rpt_labor_tracking.lt_id", "desc");
//        $query->orderBy("rpt_labor_tracking.end_time", "desc");

        if (!empty($attributes['export']) && $attributes['export'] == 1) {
            $models = $query->get();
        } else {
            $models = $query->paginate($limit);
        }

        return $models;
    }
}
