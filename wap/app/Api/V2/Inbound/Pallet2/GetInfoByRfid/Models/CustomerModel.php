<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 8/19/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Inbound\Pallet2\GetInfoByRfid\Models;

use Seldat\Wms2\Models\Customer;


class CustomerModel extends AbstractModel
{
    /**
     * @param Customer $model
     */
    public function __construct(Customer $model = null)
    {
        $this->model = ($model) ?: new Customer();
    }


}