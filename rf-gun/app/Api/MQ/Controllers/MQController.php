<?php

namespace App\Api\MQ\Controllers;
use App\Api\MQ\Jobs\ExampleJob;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\Process\Process;

/**
 * Created by PhpStorm.
 * User: phuctran
 * Date: 12/12/2016
 * Time: 11:39
 */
class MQController
{
    public function autoUpdateWave(){
//        dispatch(new ExampleJob());
//        Queue::push(new ExampleJob('a1.log'));
//        Queue::push(new ExampleJob('a2.log'));

        $processes = array();
        $pathToArtisan = base_path('artisan');
//        echo $pathToArtisan;exit;
        $processes[] = new Process('php ' . $pathToArtisan . " mq:example-command '". json_encode(['file'=>'a1.log'])."'");
        $processes[] = new Process('php ' . $pathToArtisan . " mq:example-command '". json_encode(['file'=>'a2.log'])."'");
//        print_r($processes);exit;
//        while (count($processes) > 0) {
//            foreach ($processes as $i => $process) {
//                if (!$process->isStarted()) {
//                    echo "Process starts\n";
//
//                    $process->start();
//
//                    continue;
//                }
//                echo $process->getIncrementalOutput();
//                echo $process->getIncrementalErrorOutput();
//
//                if (!$process->isRunning()) {
//                    echo "Process stopped\n";
//
//                    unset($processes[$i]);
//                }
//            }
//        }


        foreach ($processes as $i => $process) {
            if (!$process->isStarted()) {
                echo "Process starts\n";

                $process->start();

                continue;
            }
        }

        echo 'Done';
    }

    public function autoRelocate(){}

    public function autoPack(){}

    public function autoPick(){}
}