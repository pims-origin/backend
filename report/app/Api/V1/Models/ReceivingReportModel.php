<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ReceivingReport;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;

class ReceivingReportModel extends AbstractModel
{
    public function __construct(ReceivingReport $model=null)
    {
        $this->model = ($model) ?: new ReceivingReport();
    }

    /*
    ***************************************************************************
    */

    public function search($attributes = [], $with =[], $limit = PAGING_LIMIT)
    {
        $with[] = 'AsnDtl';
        $with[] = 'GoodsReceipt';
        $with[] = 'AsnDtl.Item';
        $query = $this->make($with);
//                ->where('gr_hdr_num', $attributes['gr_hdr_num']);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $attributes['compare_operator']['ctnr_num'] = 'like';

        //Searching by  GR Start Date
        if (getDefault($attributes['created_gr_start'])) {
            $query
                ->where(DB::raw('DATE_FORMAT(FROM_UNIXTIME(gr_hdr.created_at), "%Y-%m-%d")'), ">=", $attributes['created_gr_start']);
        }

        //Searching by GR Finish Date
        if (getDefault($attributes['created_gr_finish'])){
            $query
                ->where(DB::raw('DATE_FORMAT(FROM_UNIXTIME(gr_hdr.created_at), "%Y-%m-%d")'), "<=", $attributes['created_gr_finish']);
        }

//        if (getDefault($attributes['ctnr_num'])){
//            $query
//                ->where(DB::raw('ctnr_num'), "=", $attributes['ctnr_num']);
//        }


        $this->addSearches($query,[
            'searchAttr' =>$searchAttr,
            'searches' =>[
                'whs_id',
                'cus_id',
                'ctnr_num'
            ],
            'attributes' => $attributes,
            'tablePreface' =>[
                'cus_id' => 'gr_hdr',
                'whs_id' => 'gr_hdr',
                'ctnr_num' => 'gr_hdr'
            ]
        ]);


        //Format is table_name, primary_key, foreign_key
        $query->join('gr_dtl AS gd', 'gd.gr_hdr_id', '=', 'gr_hdr.gr_hdr_id');
        $query->join('asn_dtl AS ad', 'ad.asn_dtl_id', '=', 'gd.asn_dtl_id');
        $query->join('asn_hdr AS ah', 'ah.asn_hdr_id', '=', 'ad.asn_hdr_id');
        $query->join('system_measurement AS s', 's.sys_mea_code', '=', 'ah.sys_mea_code');
        $query->join('customer AS c', 'c.cus_id', '=', 'gr_hdr.cus_id');
        $query->join('warehouse AS w', 'w.whs_id', '=', 'gr_hdr.whs_id');
        $query->join('users AS u', 'u.user_id', '=', 'ad.created_by');
        $query->join('users AS us', 'us.user_id', '=', 'gd.created_by');
        $query->join('item AS i', 'i.item_id', '=', 'gd.item_id');
        $query->select([
            "*",
            DB::raw("sum(gd.gr_dtl_plt_ttl) as gr_hdr_plt_ttl")

        ]);

        if (isset($attributes['autocomplete'])){

            $autocomplete = $attributes['autocomplete'];

            $attributes['autocomplete'] = $autocomplete == 'ctnr_num' ?
                    'gr_hdr.' . $autocomplete : $autocomplete;
        } else{
            $query->groupBy([
                'gr_hdr.gr_hdr_id',
            ]);
        }

        return $this->runSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

    /*
    ***************************************************************************
    */

}
