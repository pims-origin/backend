<?php

namespace App\Modules\Items\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Modules\Items\Models\ItemModel;
use App\Modules\Items\Resources\ItemCollection;
use App\Modules\Items\Resources\ItemResource;
use App\Modules\Items\Validators\ItemValidator;

class ItemController extends Controller
{
    protected $itemModel;
    protected $itemValidator;

    public function __construct(ItemModel $itemModel, ItemValidator $itemValidator)
    {
        $this->itemModel        = $itemModel;
        $this->itemValidator    = $itemValidator;
    }

    public function index($cusId, Request $request)
    {
        $input = $request->all();

        $limit = array_get($input, 'per_page', 20);

        $baseUrl = str_replace( $request->root() , env('APP_URL'), \URL::current());

        $items = $this->itemModel->getListItemByCus($cusId, $limit, $baseUrl);

        $this->itemValidator->checkItem($items);

        return new ItemCollection($items);
    }

    public function read($cusId, $itemId, Request $request)
    {
        $item = $this->itemModel->getItemDetail($itemId, $cusId);

        $this->itemValidator->checkItem($item);

        return new ItemResource($item);
    }

    public function create($cusId, Request $request)
    {
        $input = $request->all();

        $this->itemValidator->validate($input, $cusId);

        $item = $this->itemModel->createItem($cusId, $input);

        $this->itemValidator->checkCreateItem($item);

        $response = new ItemResource($item);

        DB::table('sys_bugs')->insert([
            'date'       => time(),
            'api_name'   => 'WMS Create Item',
            'error'      => 'Params: ' . json_encode($input) . ' --- Response: ' . json_encode($response),
            'created_at' => time(),
            'created_by' => null,
            'updated_at' => time()
        ]);

        return $response;
    }

    public function update($cusId, $itemId, Request $request)
    {
        $input = $request->all();

        $this->itemValidator->validate($input, $itemId);

        $item = $this->itemModel->updateItem($cusId, $itemId, $input);

        $this->itemValidator->checkUpdateItem($item);

        return new ItemResource($item);

    }
}
