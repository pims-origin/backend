<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use App\Wms2\UserInfo\Data;
use App\OrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{

    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }

    public function getOrderNum()
    {
        $currentYearMonth = date('ym');
        $defaultWoNum = "KIT-${currentYearMonth}-00001";
        $order = $this->model
            ->where('odr_type', 'KIT')
            ->orderBy('odr_id', 'desc')
            ->first();

        $lastNum = object_get($order, 'odr_num', '');

        if (empty($lastNum) || strpos($lastNum, "-${currentYearMonth}-") === false) {
            return $defaultWoNum;
        }

        return ++$lastNum;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->where("odr_type", 'KIT');
        // Order Status
        if (!empty($attributes['odr_sts'])) {
            $query->where('odr_sts', $attributes['odr_sts']);
        }

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrEqual = ['cus_id'];
        $arrLike = ['ref_cod', 'odr_num'];

        if (!empty($attributes)) {

            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query->where($key, "like", "%" . SelStr::escapeLike($value) . "%");
                    continue;
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        // search sku
        if (isset($attributes['sku'])) {
            $query->whereHas('details', function ($query) use ($attributes) {
                $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
            });
        }

        $this->sortBuilder($query, $attributes);

        $redis = new Data();
        $userInfo = $redis->getUserInfo();

        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr_hdr.whs_id', $currentWH);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $odrId
     *
     * @return bool
     */
    public function getWorkOrderTrueFalseFromOdrId($odrId)
    {
        $workOrderModel = new WorkOrderModel();
        $work_order = false;
        if ($odrId) {
            if ($wo = $workOrderModel->checkWhere(
                [
                    'odr_hdr_id' => $odrId
                ])
            ) {
                $work_order = true;
            }
        }

        return $work_order;
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getListOrderHdrInfo($odr_ids, $type)
    {
        $whs_id = self::getCurrentWhsId();
        if ($type == "updated") {
            $arr = [
                Status::getByValue('Picking', 'ORDER-STATUS'),
                Status::getByValue('Partial Picking', 'ORDER-STATUS')
            ];
        } else {
            $arr = [
                Status::getByValue('Allocated', 'ORDER-STATUS'),
                Status::getByValue('Partial Allocated', 'ORDER-STATUS')
            ];
        }

        $res = $this->model
            ->leftJoin('shipping_odr', 'shipping_odr.so_id', '=', 'odr_hdr.so_id')
//            ->where('csr', '!=', 0)
            ->where('odr_hdr.whs_id', $whs_id)
            ->whereIn('odr_id', $odr_ids)
            ->whereIn('odr_sts', $arr)
            ->get();

        return $res;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateOrdHdr($data)
    {
        // For partial order
        if ($data['odr_sts'] == Status::getByValue("Partial Allocated", "ORDER-STATUS")) {
            $sts = Status::getByValue("Partial Picking", "ORDER-STATUS");
        } // For full order
        elseif ($data['odr_sts'] == Status::getByValue("Allocated", "ORDER-STATUS")) {
            $sts = Status::getByValue("Picking", "ORDER-STATUS");
        }

        return $this->model->where('odr_id', '=', $data['odr_id'])
            ->update([
                'wv_id'   => $data['wv_id'],
                'wv_num'  => $data['wv_num'],
                'odr_sts' => $sts
            ]);
    }

    /**
     * @param $wvId
     *
     * @return mixed
     */
    public function getOrderHdrNumsByWvId($wvId, $whsId)
    {
        return $this->model
            ->join('odr_dtl', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->where('odr_hdr.whs_id', $whsId)
            ->where('odr_dtl.wv_id', $wvId)
            ->get(['odr_num', 'odr_hdr.cus_id']);
    }

    /**
     * @param $wvIds
     *
     * @return mixed
     */
    public function getOrderHdrByWavePikcIds($wvIds)
    {
        $whs_id = self::getCurrentWhsId();
        $wvIds = is_array($wvIds) ? $wvIds : [$wvIds];

        return $this->model
            ->where('whs_id', $whs_id)
            ->whereIn('wv_id', $wvIds)
            ->get();

    }

    public function updateOrderPickedByWv($wvId)
    {
        $odrSts = 'PD';
        $this->getModel()->where([
            'odr_sts' => 'PK',
            'wv_id'   => $wvId
        ])->update(['odr_sts' => $odrSts, 'sts' => 'u']);
    }
}
