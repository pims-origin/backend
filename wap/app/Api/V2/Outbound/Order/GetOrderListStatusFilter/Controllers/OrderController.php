<?php

namespace App\Api\V2\Outbound\Order\GetOrderListStatusFilter\Controllers;

use App\Api\V2\Outbound\Order\GetOrderListStatusFilter\Models\OrderHdrModel;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OrderController extends AbstractController
{

    protected $orderHdrModel;

    public function __construct()
    {
        $this->orderHdrModel = new OrderHdrModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function getListStatusFilter($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/v2/{$whsId}/order/status-filter";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GLO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get List Status Filter'
        ]);
        /*
         * end logs
         */
        try {

            $data = [
                'NW'  => Status::getByKey('ORDER-STATUS', 'NW'),
                'PK'  => Status::getByKey('ORDER-STATUS', 'PK'),
                'PD'  => Status::getByKey('ORDER-STATUS', 'PD'),
                'PN'  => Status::getByKey('ORDER-STATUS', 'PN'),
                'PA'  => Status::getByKey('ORDER-STATUS', 'PA'),
                'PTG' => Status::getByKey('ORDER-STATUS', 'PTG'),
            ];

            $status = [];
            foreach ($data as $code => $name) {
                $tmp['code'] = $code;
                $tmp['name'] = $name;
                $status[] = $tmp;
            }

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data'] = [["status" => $status]];

            return $msg;

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _isAssignedLabel($odrId, $odrSts)
    {
        if (!in_array($odrSts, ['PN', 'PA'])) {
            return false;
        }

        $isCompleteLable = DB::table('odr_hdr_meta')->where('odr_id', $odrId)
                                ->where('qualifier', 'LBL')
                                ->first();

        if (count($isCompleteLable) > 0) {
            return false;
        }

        $isFinalWorkOrder = DB::table('vas_hdr')->where('odr_hdr_id', $odrId)
                                ->where('deleted', 0)
                                // ->where('vas_sts', 'FN')
                                ->first();

        if (!count($isFinalWorkOrder) || array_get($isFinalWorkOrder, 'vas_sts') == "FN") {
            return false;
        }

        return true;
    }

    private function _isAssignedPallet($odrId, $odrSts)
    {
        if (!in_array($odrSts, ['PN', 'PA'])) {
            return false;
        }

        return true;
    }
}