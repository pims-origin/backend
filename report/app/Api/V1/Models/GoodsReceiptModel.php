<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 03-Mar-2017
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class GoodsReceiptModel extends AbstractModel
{
    const DAS_LIMIT = 2;
    //const ROLE = ['admin', 'manager'];

    /**
     * GoodsReceiptModel constructor.
     *
     * @param GoodsReceipt|null $model
     */
    public function __construct(GoodsReceipt $model = null)
    {
        $this->model = ($model) ?: new GoodsReceipt();
    }

    /**
     * @param $whs_id
     * @param array $attributes
     *
     * @return mixed
     */
    public function customers($whs_id, $attributes = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        // Search whs_id
        $query = DB::table('customer AS c')
            ->select(['c.cus_id', 'c.cus_name'])
            ->join('gr_hdr', 'c.cus_id', '=', 'gr_hdr.cus_id')
            ->where('gr_hdr.whs_id', $whs_id);

        $result = $query->groupBy('c.cus_id')->orderBy('c.cus_name')->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function autoGrNum($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('gr_hdr AS grHdr')
            ->select(['grHdr.gr_hdr_id', 'grHdr.gr_hdr_num']);

        $query->leftJoin('gr_dtl AS grDtl', 'grHdr.gr_hdr_id', '=', 'grDtl.gr_hdr_id')
            ->where('grHdr.whs_id', $whs_id);

        if (isset($attributes['cus_id'])) {
            $query->where('grHdr.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('grDtl.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['ref_code'])) {
            $query->where('grHdr.ref_code', 'like', "%" . SelStr::escapeLike($attributes['ref_code']) . "%");
        }

        if (isset($attributes['gr_hdr_num'])) {
            $query->where('grHdr.gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['gr_hdr_num']) . "%");
        }

        $result = $query->groupBy('grHdr.gr_hdr_id', 'grHdr.gr_hdr_num')
            ->orderBy('grHdr.gr_hdr_num')->take($limit)->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function autoRefCode($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('gr_hdr AS grHdr')
            ->select(['grHdr.ref_code']);

        $query->leftJoin('gr_dtl AS grDtl', 'grHdr.gr_hdr_id', '=', 'grDtl.gr_hdr_id')
            ->where('grHdr.whs_id', $whs_id);

        if (isset($attributes['cus_id'])) {
            $query->where('grHdr.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('grDtl.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['ref_code'])) {
            $query->where('grHdr.ref_code', 'like', "%" . SelStr::escapeLike($attributes['ref_code']) . "%");
        }


        $result = $query->groupBy('grHdr.ref_code')
            ->orderBy('grHdr.ref_code')->take($limit)->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function autoCtnrNum($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('gr_hdr AS grHdr')
            ->select(['grHdr.ctnr_num']);

        if (isset($attributes['cus_id'])) {
            $query->where('grHdr.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('grHdr.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        $result = $query->groupBy('grHdr.ctnr_num')
            ->orderBy('grHdr.ctnr_num')->take($limit)->get();

        return $result;
    }

    public function getContainerQtyByDateRange($data, $dateRange) {
        return $this->model
            ->where('whs_id', $data['whs_id'])
            ->where('cus_id', $data['cus_id'])
            ->whereBetween('created_at', $dateRange)
            ->count();
    }

    public function getForecastInboundActualData($dateArr, $data) {
        return DB::table('gr_dtl')
            ->select([
                DB::raw('IFNULL(SUM(gr_dtl_act_ctn_ttl), 0) as in_carton'),
                DB::raw('IFNULL(SUM(gr_dtl_plt_ttl), 0) as in_pallet'),
                DB::raw('IFNULL(SUM(gr_dtl.cube * gr_dtl_act_ctn_ttl), 0) as in_cube'),
                DB::raw('COUNT(DISTINCT gr_hdr.gr_hdr_id, gr_dtl.ctnr_id) as in_container'),
                DB::raw('IFNULL(SUM(gr_dtl_act_qty_ttl), 0) as in_unit'),
                DB::raw(' (SELECT 0) AS in_shipment '),
                DB::raw(' (SELECT 0) AS in_location '),
                DB::raw(' (SELECT 0) AS in_order ')
            ])
            ->join('gr_hdr', 'gr_dtl.gr_hdr_id', '=', 'gr_hdr.gr_hdr_id')
            ->where('gr_hdr.whs_id', $data['whs_id'])
            ->where('gr_hdr.cus_id', $data['cus_id'])
            ->where('gr_hdr.deleted', 0)
            ->where('gr_dtl.deleted', 0)
            ->whereBetween('gr_hdr.gr_hdr_act_dt', $dateArr)
            ->where('gr_hdr.gr_sts', 'RE')
            ->where('gr_dtl.gr_dtl_sts', 'RE')
            ->first();
    }

}