<?php

namespace App\Api\V2\Outbound\PackCarton\SkuList\Controllers;

use App\Api\V2\Outbound\PackCarton\SkuList\Models\PackHdrModel;
use App\Api\V2\Outbound\PackCarton\SkuList\Transformers\SkuTransformer;
use App\Api\V1\Models\Log;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;

class OrderController extends AbstractController
{

    protected $packHdrModel;

    public function __construct()
    {
        $this->packHdrModel = new PackHdrModel();
    }

    /**
     * Get sku suggestion for order list page
     * Note: this api don't need paging, because this only suggestion, not for list data
     *
     * @param Integer $whsId
     * @param Request $request
     * @param SkuTransformer $skuTransformer
     *
     * @return Response
     */
    public function skuListSuggestion($whsId, Request $request, SkuTransformer $skuTransformer) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        $url = "/v2/whs/{$whsId}/pack/get-skus-by-order";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'SKS',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Load SKU list for suggestion'
        ]);

        try {
            $sku    = array_get($input, 'sku', '');
            $odrNum = array_get($input, 'ord_num', '');
            if(trim($sku) === '') {
                $skus = new \Illuminate\Support\Collection();
                return $this->response->collection($skus, $skuTransformer);
            }
            //not require limit and prevent submit string
            $limit = intval(array_get($input, 'limit', ''));
            if(!$limit) {
                $limit = null;
            }
            $skus = $this->packHdrModel->getSkuForSuggestion($whsId, $sku, $limit, $odrNum);
            Log::respond($request, $whsId, [
                'evt_code' => 'SKS',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'response_data' => $this->response->collection($skus, $skuTransformer)
            ]);

            return $this->response->collection($skus, $skuTransformer);

        } catch (\PDOException $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'SKS',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($this->getResponseData());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}
