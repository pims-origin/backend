<?php

namespace App\Api\OUTBOUND\Transformers;

use League\Fractal\TransformerAbstract;

class SkuTransformer extends TransformerAbstract
{
    public function transform($item)
    {
        return [
            'item_id' => object_get($item, 'item_id', ''),
            'sku'     => object_get($item, 'sku', ''),
            'size'    => object_get($item, 'size', ''),
            'color'   => object_get($item, 'color', ''),
        ];
    }
}