<?php
namespace backend\models;

use yii\base\Model;

abstract class Form extends Model
{
    public function load($data, $formName = null)
    {
        if (!empty($data)) {
            $this->setAttributes($data);

            return true;
        } else {
            return false;
        }
    }
}