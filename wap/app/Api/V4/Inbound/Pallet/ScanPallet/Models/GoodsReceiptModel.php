<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V4\Inbound\Pallet\ScanPallet\Models;

use App\Utils\JWTUtil;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class GoodsReceiptModel extends AbstractModel
{
    protected $asnDtlModel;

    protected $virtualCartonModel;

    protected $eventTrackingModel;

    protected $goodsReceiptDetailModel;

    protected $palletModel;

    protected $cartonModel;

    protected $palletSuggestLocationModel;

    protected $asnHdrModel;

    protected $inventorySummaryModel;

    protected $locationModel;
    /**
     * GoodsReceiptModel constructor.
     *
     * @param GoodsReceipt|null $model
     */
    public function __construct(GoodsReceipt $model = null)
    {
        $this->model = ($model) ?: new GoodsReceipt();
        $this->asnDtlModel = new AsnDtlModel();
        $this->virtualCartonModel= new VirtualCartonModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->goodsReceiptDetailModel = new GoodsReceiptDetailModel();
        $this->palletModel = new PalletModel();
        $this->cartonModel = new CartonModel();
        $this->asnHdrModel = new AsnHdrModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
        $this->palletSuggestLocationModel = new PalletSuggestLocationModel();
        $this->locationModel = new LocationModel();

        DB::setFetchMode(\PDO::FETCH_ASSOC);
    }

    /**
     * @param int $goodsReceiptId
     *
     * @return int
     */
    public function deleteGoodsReceipt($goodsReceiptId)
    {
        return $this->model
            ->where('gr_hdr_id', $goodsReceiptId)
            ->delete();
    }

    /**
     * @param array $goodsReceiptIds
     *
     * @return mixed
     */
    public function deleteMassGoodsReceipt(array $goodsReceiptIds)
    {
        return $this->model
            ->whereIn('gr_hdr_id', $goodsReceiptIds)
            ->delete();
    }


    /**
     * Search GoodsReceipt
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);


        if (isset($attributes['gr_hdr_id'])) {
            $query->where('gr_hdr_id', $attributes['gr_hdr_id']);
        }

        if (isset($attributes['gr_hdr_num'])) {
            $query->where('gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['gr_hdr_num']) . "%");
        }
        // Search Customer
        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', $attributes['cus_id']);
        }
        // Search whs_id
        if (isset($attributes['whs_id'])) {
            $query->where('whs_id', $attributes['whs_id']);
        }

        // Search gr_status
        if (isset($attributes['gr_sts'])) {
            $query->where('gr_sts', $attributes['gr_sts']);
        }

        $query->whereHas('container', function ($query) use ($attributes) {
            if (isset($attributes['ctnr_num'])) {
                $query->where('ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
            }
        });

        // search item
        $query->whereHas('asnHdr', function ($query) use ($attributes) {

            if (isset($attributes['asn_hdr_ref'])) {
                $query->where('asn_hdr_ref', 'like', "%" . SelStr::escapeLike($attributes['asn_hdr_ref']) . "%");
            }
        });
        // search item
        // Get
        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }

    public function getGrByCtnrAndAsn($data){

        return $this->model
            ->where('asn_hdr_id', $data['asn_hdr_id'])
            ->where('ctnr_id', $data['ctnr_id'])
            ->first();
    }

    public function getGrHdrByAsnHdrId($asnHdrId){
        return $this->model
            ->where('asn_hdr_id', $asnHdrId)
            ->count();
    }

    public function createGRWhenScanAllCartons($asnDtl){
        $asnHdrId = array_get($asnDtl, 'asn_hdr_id', '');
        $ctnrId  = array_get($asnDtl, 'ctnr_id', '');
        $whs_id     = array_get($asnDtl, 'whs_id', null);
        $cus_id     = array_get($asnDtl, 'cus_id', null);
        $ctnr_num   = array_get($asnDtl, 'ctnr_num', null);

        //check all ASN detail were completed or not
        $countLatestVtlCtn = $this->asnDtlModel->countAllAsnDtlReceived($asnHdrId, $ctnrId);

        // check all virtual carton have assigned to pallet
        $countCtnHaveNoAssignedToPallet = $this->virtualCartonModel->countVtlCtnNotHavePltByAsnHdrId($asnHdrId, $ctnrId);
        $returnData = [];
        if ($countLatestVtlCtn == 0 && $countCtnHaveNoAssignedToPallet == 0) {
            //call create GR
            $paramsGR = [
                'asn_hdr_id' => $asnHdrId,
                'ctnr_id'    => $ctnrId,
                'whs_id'     => $whs_id,
                'cus_id'     => $cus_id,
                'ctnr_num'   => $ctnr_num
            ];

            $returnData = $this->createGR($paramsGR);
            return $returnData;
        }

        return $returnData;
    }

    /**
     * @param $params
     *
     * @return array|void
     */
    public function createGR($params)
    {

        //check ASN detail is existed
        $asnDetails = $this->asnDtlModel->findWhere([
            'asn_hdr_id' => $params['asn_hdr_id'],
            'ctnr_id'    => $params['ctnr_id'],
        ]);

        //insert GR
        if ($grHrd = $this->createGRHdr($params)) {
            $asn_hdr_num = object_get($grHrd, 'asnHdr.asn_hdr_num', null);
            $ctnrId = object_get($grHrd, 'ctnr_id', null);
            $grHdrNum = object_get($grHrd, 'gr_hdr_num', null);

            $whsId = object_get($grHrd, 'whs_id', null);
            $cusId = object_get($grHrd, 'cus_id', null);

            // event tracking good receipt
            $evtGRReceiving = [
                'whs_id'    => object_get($grHrd, 'whs_id', null),
                'cus_id'    => object_get($grHrd, 'cus_id', null),
                'owner'     => $asn_hdr_num,
                'evt_code'  => Status::getByKey('EVENT', 'GR-RECEIVING'),
                'trans_num' => $asn_hdr_num,
                'info'      => sprintf(Status::getByKey('EVENT-INFO', 'GR-RECEIVING'), $asn_hdr_num),
            ];

            //call event tracking
            $this->eventTracking($evtGRReceiving);

            // Insert goods receipt detail
            foreach ($asnDetails as $asnDetail) {
                $actCtn = $this->getActCtn($asnDetail['asn_dtl_id']);
                $pltTtl = $this->getActPlt($asnDetail['asn_dtl_id']);
                $discTtl = $actCtn - $asnDetail->asn_dtl_ctn_ttl;
                $isDamage = $this->checkDamage($asnDetail['asn_dtl_id']);
                $paramsDetail = [
                    'asn_dtl_id'         => $asnDetail['asn_dtl_id'],
                    'gr_hdr_id'          => $grHrd->gr_hdr_id,
                    'gr_dtl_ept_ctn_ttl' => $asnDetail->asn_dtl_ctn_ttl,
                    'gr_dtl_act_ctn_ttl' => $actCtn,
                    'gr_dtl_plt_ttl'     => $pltTtl,
                    'gr_dtl_disc'        => $discTtl,
                    'gr_dtl_is_dmg'      => $isDamage,
                    'sku'                => $asnDetail['asn_dtl_sku'],
                    'size'               => $asnDetail['asn_dtl_size'],
                    'color'              => $asnDetail['asn_dtl_color'],
                    'lot'                => $asnDetail['asn_dtl_lot'],
                    'po'                 => $asnDetail['asn_dtl_po'],
                    'uom_code'           => $asnDetail['uom_code'],
                    'uom_name'           => $asnDetail['uom_name'],
                    'uom_id'             => $asnDetail['uom_id'],
                    'upc'                => $asnDetail['asn_dtl_cus_upc'],
                    'ctnr_id'            => $asnDetail['ctnr_id'],
                    'ctnr_num'           => $asnDetail['ctnr_num'],
                    'item_id'            => $asnDetail['item_id'],
                    'pack'               => $asnDetail['asn_dtl_pack'],
                    'length'             => array_get($asnDetail, 'asn_dtl_length', 0),
                    'width'              => array_get($asnDetail, 'asn_dtl_width', 0),
                    'height'             => array_get($asnDetail, 'asn_dtl_height', 0),
                    'weight'             => array_get($asnDetail, 'asn_dtl_weight', 0),
                    'cube'               => array_get($asnDetail, 'asn_dtl_cube', 0),
                    'volume'             => array_get($asnDetail, 'asn_dtl_volume', 0),
                    'expired_dt'         => array_get($asnDetail, 'expired_dt', 0),
                ];

                if (is_null($pltTtl)) {
                    throw new \Exception("Total of pallet can not be null.");
                }

                //add size, color, sku, etc
                $grDtl = $this->createGrDtl($paramsDetail);

                $this->createPallet($grDtl, $grHrd, $asnDetail);

                $this->updateASNDtlReceived($asnDetail['asn_dtl_id']);

                $this->inventorySummaryModel->updateQtyByGRDtl($whsId, $cusId, $paramsDetail);
            }

            // event tracking good receipt received
            $evtGRReceived = [
                'whs_id'    => object_get($grHrd, 'whs_id', null),
                'cus_id'    => object_get($grHrd, 'cus_id', null),
                'owner'     => $grHdrNum,
                'evt_code'  => Status::getByKey('EVENT', 'GR-COMPLETE'),
                'trans_num' => $grHdrNum,
                'info'      => sprintf(Status::getByKey('EVENT-INFO', 'GR-RECEIVED'), $grHdrNum),
            ];

            //call event tracking
            $this->eventTracking($evtGRReceived);

            //params for ASN event tracking receiving
            $evtASNReceived = [
                'whs_id'    => object_get($grHrd, 'whs_id', null),
                'cus_id'    => object_get($grHrd, 'cus_id', null),
                'owner'     => $asn_hdr_num,
                'evt_code'  => Status::getByKey('EVENT', 'ASN-COMPLETE'),
                'trans_num' => $asn_hdr_num,
                'info'      => sprintf(Status::getByKey('EVENT-INFO', 'ASN-COMPLETE'), $asn_hdr_num),
            ];

            $this->updateASNReceived($params['asn_hdr_id'], $evtASNReceived);

            //update all vitual ctn status
            $this->updateVtlCtnStatusGRCreated($params['asn_hdr_id'], $ctnrId);

            //update location is using by this GR's pallet
            $this->locationModel->updateLocationSts($params['asn_hdr_id'], $ctnrId);

            //remove location info on virtual carton
            $this->removeLocationsOnVtlCtnWhenGRCreated($params['asn_hdr_id'], $ctnrId);

            $returnData = [
                'gr_hdr_num'  => $grHdrNum,
                'asn_hdr_num' => $asn_hdr_num,
                'gr_hdr_id' => $grHrd->gr_hdr_id
            ];

            return $returnData;
        }
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function createGRHdr($params)
    {
        //get ASN header info
        $asnHeader = $this->asnHdrModel->getFirstBy('asn_hdr_id', $params['asn_hdr_id']);

        // generate GR number
        $grNumAndSeq = $this->generateGrNumAndSeq($asnHeader->asn_hdr_num, $asnHeader->asn_hdr_id);

        //param GR header
        $paramsGRHeader = [
            'ctnr_id'       => array_get($params, 'ctnr_id', null),
            'asn_hdr_id'    => array_get($params, 'asn_hdr_id', null),
            'gr_hdr_seq'    => $grNumAndSeq['gr_seq'],
            'gr_hdr_ept_dt' => $asnHeader->asn_hdr_ept_dt,
            'gr_hdr_num'    => $grNumAndSeq['gr_num'],
            'whs_id'        => array_get($params, 'whs_id', null),
            'cus_id'        => array_get($params, 'cus_id', null),
            'gr_sts'        => Status::getByKey('GR_STATUS', 'RECEIVED'),
            'ctnr_num'      => array_get($params, 'ctnr_num', null),
            'gr_in_note'    => object_get($asnHeader, 'asn_hdr_des', ''),
            'ref_code'      => object_get($asnHeader, 'asn_hdr_ref', ''),
        ];

        //miss 'labor_charge'
        return $this->create($paramsGRHeader);
    }

    /**
     * @param $asnNum
     * @param $asnHrdId
     *
     * @return array
     */
    public function generateGrNumAndSeq($asnNum, $asnHrdId)
    {
        $numOfGr = $this->checkWhere(['asn_hdr_id' => $asnHrdId]);

        $result = [
            'gr_num' => '',
            'gr_seq' => '',
        ];

        $result['gr_seq'] = ($numOfGr) ? $numOfGr + 1 : 1;

        $grNum = str_replace(config('constants.asn_prefix'), config('constants.gr_prefix'), $asnNum);

        $result['gr_num'] = sprintf('%s-%s',
            $grNum,
            str_pad($result['gr_seq'], 2, '0', STR_PAD_LEFT));

        return $result;
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function eventTracking($params)
    {

        // event tracking asn
        $this->eventTrackingModel->refreshModel();

        return $this->eventTrackingModel->create([
            'whs_id'    => $params['whs_id'],
            'cus_id'    => $params['cus_id'],
            'owner'     => $params['owner'],
            'evt_code'  => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info'      => $params['info'],
        ]);
    }

    /**
     * @param $asnDtlId
     *
     * @return mixed
     */
    public function getActCtn($asnDtlId)
    {
        return $this->virtualCartonModel->countCtnByASNDtlId($asnDtlId);
    }

    /**
     * @param $asnDtlId
     *
     * @return mixed
     */
    public function getActPlt($asnDtlId)
    {
        return $this->virtualCartonModel->countPltByASNDtlId($asnDtlId);
    }

    /**
     * @param $asnDtlId
     *
     * @return bool
     */
    public function checkDamage($asnDtlId)
    {
        return (bool)$this->virtualCartonModel->countCtnIsDamageByASNDtlId($asnDtlId);
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function createGrDtl($params)
    {
        $this->goodsReceiptDetailModel->refreshModel();

        return $this->goodsReceiptDetailModel->create($params);
    }

    /**
     * @param $grDtl
     * @param $grHrd
     * @param $asnDetail
     */
    public function createPallet($grDtl, $grHrd, $asnDetail)
    {
        $grHdrNum = object_get($grHrd, 'gr_hdr_num', null);
        $ansDtlId = array_get($grDtl, 'asn_dtl_id', 0);
        $grDtlId = array_get($grDtl, 'gr_dtl_id', 0);
        $cusId = array_get($grHrd, 'cus_id', 0);
        $whsId = array_get($grHrd, 'whs_id', 0);
        $grHdrId = array_get($grHrd, 'gr_hdr_id', 0);

        $vtlCtns = $this->virtualCartonModel->getAllCtnByASNDtlId($ansDtlId);
        $pallets = [];
        foreach ($vtlCtns as $vtlCtn) {
            $pltRfid = array_get($vtlCtn, 'plt_rfid', 0);
            $pallets[$pltRfid][] = $vtlCtn;
        }

        foreach ($pallets as $pltRfid => $vtlCtns) {
            $ctnFirst = array_shift($vtlCtns);
            $locId = array_get($ctnFirst, 'loc_id', null);
            $locCode = array_get($ctnFirst, 'loc_code', null);
            array_push($vtlCtns, $ctnFirst);

            $pltNum = $this->generatePalletNum($grHdrNum);

            $palletData = [
                'whs_id'    => $whsId,
                'cus_id'    => $cusId,
                'plt_num'   => $pltNum,
                'rfid'      => $pltRfid,
                'loc_id'    => $locId,
                'loc_code'  => $locCode,
                'loc_name'  => $locCode,
                'gr_hdr_id' => $grHdrId,
                'gr_dtl_id' => $grDtlId,
                'ctn_ttl'   => count($vtlCtns)
            ];

            //check pallet's RFID is using or free
            $pallet = $this->palletModel->findWhere(
                [
                    'rfid' => $pltRfid
                ]
            );

            if ($pallet) {
                foreach ($pallet as $plt)
                if ($plt['plt_sts'] == 'AC' && $plt['ctn_ttl'] > 0) {
                    $msg = sprintf("The pallet's existed or using", $pltRfid);

                    throw new \Exception($msg);
                }
            }

            $this->palletModel->refreshModel();
            $palletObj = $this->palletModel->create($palletData);
            $params = [
                'ctns'      => $vtlCtns,
                'plt'       => $palletObj,
                'grDtl'     => $grDtl,
                'gr_hdr_id' => $grHdrId,
                'asnDtl'    => $asnDetail,
                'grHdrNum'  => $grHdrNum,
            ];

            $this->createCtns($params);
            $this->initSugPalletLoc($params);
        }
    }

    /**
     * @param $grHdrNum
     *
     * @return string
     */
    public function generatePalletNum($grHdrNum)
    {
        $palletCode = str_replace('GDR', 'LPN', $grHdrNum);

        $countPallet = \DB::table('pallet')->select('plt_num')
            ->where('plt_num', 'like', $palletCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();
        $max = 0;
        if ($countPallet) {
            $pltCode = array_get($countPallet, 'plt_num', null);
            $arrMax = explode('-', $pltCode);
            $max = (int)end($arrMax);
        }

        return $palletCode . "-" . str_pad($max + 1, 3, "0", STR_PAD_LEFT);
    }

    /*public function checkPltRFIDIsUsing($pltRFID)
    {
        $data = $this->palletModel->findWhere(
            [
                'rfid' => $pltRFID
            ]
        );
    }*/

    /**
     * @param $params
     */
    public function createCtns($params)
    {
        $vtlCtns = $params['ctns'];
        $pallet = $params['plt'];
        $grDtl = $params['grDtl'];
        $asnDetail = $params['asnDtl'];
        $grHdrNum = $params['grHdrNum'];
        $grHdrId = $params['gr_hdr_id'];

        $ctnData = [
            'plt_id'        => array_get($pallet, 'plt_id', null),
            'asn_dtl_id'    => object_get($grDtl, 'asn_dtl_id', null),
            'gr_dtl_id'     => object_get($grDtl, 'gr_dtl_id', null),
            'item_id'       => array_get($asnDetail, 'item_id', null),
            'whs_id'        => array_get($pallet, 'whs_id', null),
            'cus_id'        => array_get($pallet, 'cus_id', null),
            'loc_id'        => array_get($pallet, 'loc_id', null),
            'loc_code'      => array_get($pallet, 'loc_code', null),
            'loc_name'      => array_get($pallet, 'loc_name', null),
            'ctn_pack_size' => array_get($asnDetail, 'asn_dtl_pack', 0),
            'piece_remain' => array_get($asnDetail, 'asn_dtl_pack', 0),
            'piece_ttl' => array_get($asnDetail, 'asn_dtl_pack', 0),
            'ctn_uom_id'    => array_get($asnDetail, 'uom_id', 0),
            'ctn_sts'       => Status::getByKey('CTN_STATUS', 'ACTIVE'),
            'gr_dt'         => time(),
            'gr_hdr_id'     => $grHdrId,
            'sku'           => array_get($asnDetail, 'asn_dtl_sku', 0),
            'size'          => array_get($asnDetail, 'asn_dtl_size', 'NA'),
            'color'         => array_get($asnDetail, 'asn_dtl_color', 'NA'),
            'lot'           => array_get($asnDetail, 'asn_dtl_lot', 'NA'),
            'po'            => array_get($asnDetail, 'asn_dtl_po', 'NA'),
            'uom_code'      => array_get($asnDetail, 'uom_code', 0),
            'uom_name'      => array_get($asnDetail, 'uom_name', 0),
            'upc'           => array_get($asnDetail, 'asn_dtl_cus_upc', 0),
            'ctnr_id'       => array_get($asnDetail, 'ctnr_id', 0),
            'ctnr_num'      => array_get($asnDetail, 'ctnr_num', 0),
            'is_ecom'       => 0,
            'picked_dt'     =>0,
            'storage_duration' => 0,
            'loc_type_code' => 'RAC',
            'expired_dt'    =>object_get($grDtl, 'expired_dt', 0),
            'return_id' => null,
            'length' => object_get($grDtl, 'length', 0),
            'width'=> object_get($grDtl, 'width', 0),
            'height'=> object_get($grDtl, 'height', 0),
            'weight'=> object_get($grDtl, 'weight', 0),
            'cube'=> object_get($grDtl, 'cube', 0),
            'volume'=> object_get($grDtl, 'volume', 0),
        ];

        $grDtlId = object_get($grDtl, 'gr_dtl_id', null);
        $ctnID = [];
        foreach ($vtlCtns as $vtlCtn) {
            $ctnNum = $this->generateCtnNum($grHdrNum);
            $ctnData['rfid'] = array_get($vtlCtn, 'ctn_rfid', null);
            $ctnData['is_damaged'] = array_get($vtlCtn, 'is_damaged', 0);
            $ctnData['ctn_num'] = $ctnNum;
            $this->cartonModel->refreshModel();
            $ctnModel = $this->cartonModel->create($ctnData);

            //if this carton is damage, add to ctnID array
            if (array_get($vtlCtn, 'is_damaged', null)) {
                $ctnID[] = $ctnModel->ctn_id;
            }
        }

        //check if $ctnID <> null, insert to table damage_carton
        $this->createDamageCarton($ctnID);
    }

    public function createDamageCarton (array $ctnIDs) {
        foreach ($ctnIDs as $ctnID) {
            $data = [
                'ctn_id' => $ctnID,
                'dmg_id' => 1,
                'dmg_note' => 'WAP set carton damage default.',
            ];
            (new DamageCartonModel())->getModel()->create($data);
        }
        return true;
    }

    public function initSugPalletLoc($params)
    {
        $userId = JWTUtil::getPayloadValue('jti');

        $vtlCtns = $params['ctns'];
        $pallet = $params['plt'];
        $grDtl = $params['grDtl'];
        $asnDetail = $params['asnDtl'];
        $grHdrNum = $params['grHdrNum'];
        $grHdrId = $params['gr_hdr_id'];

        //if this pallet not put to RACK
        if (!array_get($pallet, 'loc_id', null)) {
            return;
        }

        $suPltLocData = [
            'plt_id'       => array_get($pallet, 'plt_id', null),
            'loc_id'       => array_get($pallet, 'loc_id', null),
            'data'         => array_get($pallet, 'loc_code', null),
            'ctn_ttl'      => count($vtlCtns),
            'item_id'      => array_get($asnDetail, 'item_id', null),
            'sku'          => array_get($asnDetail, 'asn_dtl_sku', 0),
            'size'         => array_get($asnDetail, 'asn_dtl_size', 'NA'),
            'color'        => array_get($asnDetail, 'asn_dtl_color', 'NA'),
            'lot'          => array_get($asnDetail, 'asn_dtl_lot', 'NA'),
            'putter'       => $userId,
            'gr_hdr_id'    => $grHdrId,
            'gr_dtl_id'    => object_get($grDtl, 'gr_dtl_id', null),
            'gr_hdr_num'   => $grHdrNum,
            'whs_id'       => array_get($pallet, 'whs_id', null),
            'put_sts'      => "CO",
            'act_loc_id'   => array_get($pallet, 'loc_id', null),
            'act_loc_code' => array_get($pallet, 'loc_code', null),
        ];

        $this->palletSuggestLocationModel->refreshModel();
        $this->palletSuggestLocationModel->create($suPltLocData);
    }

    /**
     * @param $grHdrNum
     *
     * @return string
     */
    public function generateCtnNum($grHdrNum)
    {
        // ctnNum
        $cartonCode = str_replace('GDR', 'CTN', $grHdrNum);
        $seq = $this->cartonModel->getCountCTNNum($grHdrNum) + 1;
        $ctnNum = $cartonCode . "-" . str_pad($seq, 3, "0", STR_PAD_LEFT);

        return $ctnNum;
    }

    /**
     * @param $ansDtlId
     *
     * @return mixed
     */
    public function updateASNDtlReceived($ansDtlId)
    {
        return $this->asnDtlModel->updateWhere(
            [
                'asn_dtl_sts' => Status::getByKey('ASN_DTL_STS', 'RECEIVED')
            ],
            [
                'asn_dtl_id' => $ansDtlId
            ]
        );
    }

    /**
     * @param $asnHdrId
     * @param $evtASNReceived
     */
    public function updateASNReceived($asnHdrId, $evtASNReceived)
    {
        $checkStsASNDtl = $this->asnDtlModel->countAsnDtlNotReceived($asnHdrId);

        if (!$checkStsASNDtl || empty($checkStsASNDtl)) {
            // Change Status ASN
            $this->asnHdrModel->updateWhere(
                [
                    'asn_sts' => Status::getByKey('ASN_STATUS', 'RECEIVED')
                ],
                ['asn_hdr_id' => $asnHdrId]
            );

            //call save event tracking for ASN receiving
            $this->eventTracking($evtASNReceived);
        }
    }

    /**
     * @param $ansHdrId
     * @param $ctnId
     *
     * @return mixed
     */
    public function updateVtlCtnStatusGRCreated($ansHdrId, $ctnId)
    {
        return $this->virtualCartonModel->updateWhere(
            ['vtl_ctn_sts' => Status::getByValue('GOODS-RECEIPT-COMPLETE', 'VIRTUAL-CARTON-STATUS')],
            [
                'asn_hdr_id' => $ansHdrId,
                'ctnr_id'    => $ctnId
            ]
        );
    }

    /**
     * [WMS2-3762] 6. Update vitual cartons
     *
     * @param String $rfid
     *
     * @return Object
     */
    public function updateVtlCtnStatusGRCreatedByRfid($rfid)
    {
        return $this->virtualCartonModel->updateWhere(
            ['vtl_ctn_sts' => Status::getByValue('GOODS-RECEIPT-COMPLETE', 'VIRTUAL-CARTON-STATUS')],
            [
                'rfid' => $rfid
            ]
        );
    }

    /**
     * @param $ansHdrId
     * @param $ctnId
     *
     * @return mixed
     */
    public function removeLocationsOnVtlCtnWhenGRCreated($ansHdrId, $ctnId)
    {
        return $this->virtualCartonModel->updateWhere(
            [
                'loc_rfid' => null,
                'loc_code' => null,
                'loc_id'   => null,
            ],
            [
                'asn_hdr_id' => $ansHdrId,
                'ctnr_id'    => $ctnId,
            ]
        );
    }

    public function isGoodsReceiptExist($asnHdrId, $ctnrId) {
        $grHdr = $this->getFirstWhere([
            'asn_hdr_id'=>$asnHdrId,
            'ctnr_id'=>$ctnrId
        ]);

        return $grHdr;
    }


    /**
     * @param null $asnHdrId
     * @param null $containerInfo
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|null
     * @throws \Exception
     */
    public function createGRHeader($asnHdr, $ctnrId, $ctnrNum)
    {
        //get ASN HDR info to create GR HDR
        //$asnHdr = $this->asnHdrModel->getModel()->find($asnHdrId);
        $params = [
            'asn_hdr_id' => $asnHdr->asn_hdr_id,
            'ctnr_id' => $ctnrId,
            'whs_id' => $asnHdr->whs_id,
            'cus_id' => $asnHdr->cus_id,
            'ctnr_num' => $ctnrNum,
        ];

        //create GR HDR
        // generate GR number
        $grNumAndSeq = $this->generateGrNumAndSeq($asnHdr->asn_hdr_num, $asnHdr->asn_hdr_id);

        //param GR header
        $paramsGRHeader = [
            'ctnr_id' => array_get($params, 'ctnr_id', null),
            'asn_hdr_id' => array_get($params, 'asn_hdr_id', null),
            'gr_hdr_seq' => $grNumAndSeq['gr_seq'],
            'gr_hdr_ept_dt' => $asnHdr->asn_hdr_ept_dt,
            'gr_hdr_num' => $grNumAndSeq['gr_num'],
            'whs_id' => array_get($params, 'whs_id', null),
            'cus_id' => array_get($params, 'cus_id', null),
            'gr_sts' => Status::getByKey('GR_STATUS', 'RECEIVING'),
            'ctnr_num' => array_get($params, 'ctnr_num', null),
            'gr_in_note' => object_get($asnHdr, 'asn_hdr_des', ''),
            'ref_code' => object_get($asnHdr, 'asn_hdr_ref', ''),
            'created_from' => "WAP",
        ];  //miss 'labor_charge'

        $this->refreshModel();
        $goodsReceipt = $this->create($paramsGRHeader);

        if (!$goodsReceipt) {
            $msg = "Create GR header failed.";
            throw new \Exception($msg);
        }

        return $goodsReceipt;
    }

    public function createEVT($data) {
        $evtGRCreated = [
            'whs_id' => array_get($data, 'whs_id', null),
            'cus_id' => array_get($data, 'cus_id', null),
            'owner' => array_get($data, 'owner', null),
            'evt_code' => array_get($data, 'evt_code', null),
            'trans_num' => array_get($data, 'trans_num', null),
            'info' => array_get($data, 'info', null),
        ];
        return $this->eventTracking($evtGRCreated);
    }


    /**
     * 6. Check If not define cartons, insert virtual/real cartons
     * Create miss cartons, call from pallet controller
     *
     * @param type $pltRfid
     * @param type $wrongRFIDS
     * @param type $palletArr
     * @param type $grDtlObj
     * @param type $asnDetailObj
     * @param type $grHdrNum
     */
    public function createCtnIfNotDefined($whsId, $pltRfid, $wrongRFIDS, $grDtlObj, $asnDetailObj, $grHdrNum)
    {
        $asnDetailArr = $asnDetailObj->toArray();
        //Create virtual carton
        $user = (new Data())->getUserInfo();
        $grHdrId = object_get($grDtlObj, 'gr_hdr_id');
        $grHdr = $this->getModel()->find($grHdrId);

        $pallet = (new PalletModel())->getModel()
                ->where([
                    'rfid' => $pltRfid,
                    'gr_hdr_id' => object_get($grDtlObj, 'gr_hdr_id')
                ])
                ->first();
        if(!$pallet) {

            return false;
        }
        //not define cartons, create its
        $cusId = object_get($pallet, 'cus_id');
        $asnDtlId = array_get($asnDetailArr, 'asn_dtl_id', null);
        if (count($wrongRFIDS)) {

            $vtlCtnSum = new VirtualCartonSumModel();
            $dataVtlCtnSum = [
                'asn_hdr_id'      => array_get($asnDetailArr, 'asn_hdr_id', null),
                'asn_dtl_id'      => array_get($asnDetailArr, 'asn_dtl_id', null),
                'cus_id'          => $cusId,
                'whs_id'          => $whsId,
                'item_id'         => array_get($asnDetailArr, 'item_id', null),
                'ctnr_id'         => array_get($asnDetailArr, 'ctnr_id', null),
                //'discrepancy'     => array_get($input, 'discrepancy', null),
                'discrepancy'     => 0,
                'vtl_ctn_sum_sts' => Status::getByValue('RECEIVING', 'VIRTUAL-CARTON-SUMMARY-STATUS'),
                //'gate_code'       => $gateCode,
            ];
            $insertVtlCtnSumId = $vtlCtnSum->create($dataVtlCtnSum)->vtl_ctn_sum_id;

            $vtlCtnDataCommon['asn_hdr_id'] = array_get($asnDetailArr, 'asn_hdr_id');
            $vtlCtnDataCommon['asn_dtl_id'] = array_get($asnDetailArr, 'asn_dtl_id');
            $vtlCtnDataCommon['item_id'] = array_get($asnDetailArr, 'item_id');
            $vtlCtnDataCommon['ctnr_id'] = array_get($asnDetailArr, 'ctnr_id');
            $vtlCtnDataCommon['cus_id'] = array_get($grHdr, 'cus_id');
            $vtlCtnDataCommon['whs_id'] = array_get($grHdr, 'whs_id', null);
            //$vtlCtnDataCommon['is_damaged'] = object_get($grDtlObj, 'is_damaged', 0);
            $vtlCtnDataCommon['is_damaged'] = 0;
            $vtlCtnDataCommon['vtl_ctn_sts'] = Status::getByValue('NEW', 'VIRTUAL-CARTON-STATUS');
            $vtlCtnDataCommon['vtl_ctn_sum_id'] = $insertVtlCtnSumId;

            foreach ($wrongRFIDS as $key => $ctn) {
                $vtlCtnDataCommon['ctn_rfid'] = $ctn;
                $vtlCtnDataCommon['created_at'] = time();
                $vtlCtnDataCommon['updated_at'] = time();
                $vtlCtnDataCommon['created_by'] = $user['user_id'];
                $vtlCtnDataCommon['updated_by'] = $user['user_id'];
                $vtlCtnDataCommon['deleted_at'] = 915148800;
                $vtlCtnDataCommon['deleted'] = 0;
                //init data array
                $dataVtlCtnDtl[] = $vtlCtnDataCommon;
            }
            DB::table('vtl_ctn')->insert($dataVtlCtnDtl);
        }

        $ctnsExisted = $this->cartonModel->getModel()
                ->where([
                    'plt_id'=>object_get($pallet, 'plt_id'),
                    'asn_dtl_id' => array_get($asnDetailArr, 'asn_dtl_id', null),
                    'ctnr_id' => array_get($asnDetailArr, 'ctnr_id', null)
                ])
                ->get()
                ->toArray();
        $ctnsArr = array_pluck($ctnsExisted, 'rfid');
        $vtlCtnOnPallet = $this->virtualCartonModel->getModel()
                ->where([
                    'asn_dtl_id' => array_get($asnDetailArr, 'asn_dtl_id', null),
                    'ctnr_id' => array_get($asnDetailArr, 'ctnr_id', null)
                ])
                ->whereNotIn('ctn_rfid', $ctnsArr)
                ->get()
                ->toArray();
        if(!count($vtlCtnOnPallet)){
            return false;
        }

        $params = [
            'ctns' => $vtlCtnOnPallet,
            'plt' => $pallet->toArray(),
            'grDtl' => $grDtlObj,
            'gr_hdr_id' => object_get($grDtlObj, 'gr_hdr_id'),
            'asnDtl' => $asnDetailArr,
            'grHdrNum' => $grHdrNum,
        ];
        $this->createCtns($params);

        /**
         * 14. Create Event Tracking for Assign Cartons to Pallet
         */
        $evtAssignCtn2Plt = [
            'whs_id' => object_get($grHdr, 'whs_id', null),
            'cus_id' => object_get($grHdr, 'cus_id', null),
            'owner' => object_get($grHdr, 'gr_hdr_num', null),
            'evt_code' => 'WAP',
            'trans_num' => '',
            'info' => sprintf(Status::getByKey('EVENT-INFO', 'GR-ASSIGN-PALLET'), count($vtlCtnOnPallet), $pltRfid),
        ];
        $this->eventTracking($evtAssignCtn2Plt);
    }

    public function countDamageTtl($asnDtlId) {
        $ttl = $this->virtualCartonModel->getModel()
                ->where([
                    'asn_dtl_id' =>$asnDtlId,
                    'is_damaged' => 1,
                    'deleted' => 0
                ])
                ->count();

        return $ttl;
    }

    /**
     * 9. Set real damaged Cartons if is_damaged = 1, insert damaged_cartons
     *
     * @param String $pltRfid
     * @param Object $asnDtl
     *
     * @return boolean
     */
    public function setRealDamageCarton($grDtlId)
    {
        $cartons = $this->cartonModel->getModel()
                ->where('gr_dtl_id', $grDtlId)
                ->where('is_damaged', 1)
                ->get();
        $ctnId = [];
        foreach($cartons as $carton) {
            $ctnId[] = $carton->ctn_id;
        }

        return $this->createDamageCarton($ctnId);
    }

    /**
     * 10. Update Virtual Carton status is Goods Receipt Created (carton not allowed to rescan)
     * @param type $ansHdrId
     * @param type $pltRfid
     * @return boolean
     */
    public function updateVtlCtnGRCreated($asnDtlId, $pltRfid)
    {
        $vtlDamage = $this->virtualCartonModel
                ->getModel()
                ->where([
                    'asn_dtl_id' => $asnDtlId,
                    'plt_rfid' => $pltRfid
                ])
                ->update([
                    'vtl_ctn_sts' => Status::getByValue('GOODS-RECEIPT-COMPLETE', 'VIRTUAL-CARTON-STATUS')
                ]);

        return true;
    }

    /**
     * 11. Update Pal_sug_loc, LPN, total Cartons per pallet(inconsistent data for each time printing)
     *
     * @param type $palletObj
     * @param type $vtlCtns
     * @param type $grDtl
     * @param type $asnDetail
     * @param type $grHdrNum
     *
     * @return boolean
     */
    public function updateSugPltLoc($palletObj, $vtlCtns, $grDtl, $asnDetail, $grHdrNum)
    {
        $sugPltLoc = $this->palletSuggestLocationModel->getModel()
                        ->where([
                            'plt_id' => object_get($palletObj, 'plt_id', null),
                            'gr_hdr_id' => object_get($grDtl, 'gr_hdr_id', null),
                            'gr_dtl_id' => object_get($grDtl, 'gr_dtl_id', null),
                            'whs_id' => array_get($palletObj, 'whs_id', null),
                        ])->get();
        if (!$sugPltLoc->count()) {
            $params = [
                'ctns' => $vtlCtns,
                'plt' => $palletObj->toArray(),
                'grDtl' => $grDtl,
                'gr_hdr_id' => object_get($grDtl, 'gr_hdr_id', null),
                'asnDtl' => $asnDetail,
                'grHdrNum' => $grHdrNum,
            ];
            $this->initSugPalletLoc($params);

            return true;
        }

        $sugPltLoc = $sugPltLoc[0];
        $sugPltLoc->update([
            'ctn_ttl' => count($vtlCtns)
        ]);
    }

    /**
     * @param int $grHdrId
     *
     * @return bool
     */
    public function updateGrHdrStatus($grHdrId)
    {
        $ttlAsnDtlRESql = "(SELECT COUNT(asn_dtl_id) FROM asn_dtl WHERE asn_dtl.`ctnr_id` = gr_hdr.`ctnr_id` AND asn_dtl.`asn_dtl_sts` = 'RE')";
        $ttlGrDtlRESql = "(SELECT COUNT(gr_dtl_id) FROM gr_dtl WHERE gr_dtl.`gr_hdr_id` = gr_hdr.`gr_hdr_id` AND gr_dtl.`gr_dtl_sts` = 'RE' )";

        $allAsnDtlRESql = "(SELECT COUNT(asn_dtl_id) FROM asn_dtl WHERE asn_dtl.`ctnr_id` = gr_hdr.`ctnr_id`)";
        $retsult = $this->getModel()
                ->where(['gr_hdr.gr_hdr_id' => $grHdrId])
                ->whereRaw(sprintf("%s=%s", $ttlAsnDtlRESql, $allAsnDtlRESql))
                ->whereRaw(sprintf("%s=%s", $ttlAsnDtlRESql, $ttlGrDtlRESql))
                ->update([
                        'gr_hdr.gr_sts' =>'RE',
                        'updated_by'    => $this->getUserId(),
                    ]
                );

        return (bool) $retsult;
    }

    /**
     * 13. Create Event Tracking for Create/Update Goods Receipt
     * @param type $grHdr
     */
    public function updateEventTracking($grHdr, $ttlCartons, $ttlDamageCtn)
    {
        $evtGRCreated = [
            'whs_id' => object_get($grHdr, 'whs_id', null),
            'cus_id' => object_get($grHdr, 'cus_id', null),
            'owner' => object_get($grHdr, 'gr_hdr_num', null),
            'evt_code' => 'WGU',
            'trans_num' => '',
            'info' => sprintf(Status::getByKey('EVENT-INFO', 'GR-UPDATED'), $ttlCartons, $ttlDamageCtn),
        ];
        $this->eventTracking($evtGRCreated);
    }

    /**
     * 12. Update Inventory with available qty and damaged qty
     * Update Inventory with available qty and damaged qty
     *
     * @param type $whsId
     * @param type $cusId
     * @param type $grDtl
     * @param type $availQty
     * @param type $dmgQty
     */
    public function updateInventory($whsId, $cusId, $grDtl, $availQty, $dmgQty) {

        $model = $this->inventorySummaryModel->getModel();
        $invt = $model
                ->where([
                    'item_id' => $grDtl['item_id'],
                    'lot' => $grDtl['lot'],
                    'whs_id' => $whsId,
                    'cus_id' => $cusId
                ])
                ->first();

        $ttl = ($availQty + $dmgQty);

        if($invt) {
            $invt->avail += $availQty;
            $invt->dmg_qty += $dmgQty;
            $invt->ttl +=  $ttl;

           return  $invt->save();
        } else {
            $arrInput = [
                'item_id' => $grDtl['item_id'],
                'cus_id' => $cusId,
                'whs_id' => $whsId,
                'color' => $grDtl['color'],
                'size' => $grDtl['size'],
                'lot' => $grDtl['lot'],
                'ttl' => $ttl,
                'picked_qty' => 0,
                'allocated_qty' => 0,
                'dmg_qty' => $dmgQty,
                'avail' => $availQty,
                'sku' => $grDtl['sku'],
                'back_qty' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ];
           return $model->create($arrInput);
        }
    }


    public function reduceInventoryWhenRescanPallet($whsId, $cusId, $grDtl, $availQty, $dmgQty) {

        $model = $this->inventorySummaryModel->getModel();
        $invt = $model
            ->where([
                'item_id' => $grDtl['item_id'],
                'lot' => $grDtl['lot'],
                'whs_id' => $whsId,
                'cus_id' => $cusId
            ])
            ->first();
        $ttl = ($availQty + $dmgQty);
        if($invt) {
            $invt->avail -= $availQty;
            $invt->dmg_qty -= $dmgQty;
            $invt->ttl -=  $ttl;
              $invt->save();
            return $invt;
        }
    }



    /**
     * 17. Put pallet and cartons on Rack and update pal_sug_loc with actual location at step 11, pallet is not existed if not passed step 3
     *
     * @param type $pltRfid
     * @param type $grDtlId
     *
     * @return boolean
     */
    public function putPalletAndCtnOnRack($pltRfid, $grDtlId)
    {
        $pallet = $this->palletModel->getModel()
                ->where([
                    'rfid' => $pltRfid,
                    'gr_dtl_id' => $grDtlId
                ]);
        $palletObj = $pallet->first();
        if (!$palletObj) {
            return false;
        }
        $pltId = object_get($palletObj, 'plt_id', null);
        $grHdrId = object_get($palletObj, 'gr_hdr_id', null);
        $grDtlId = object_get($palletObj, 'gr_dtl_id', null);
        $findLocationInfo = $this->palletSuggestLocationModel->getModel()
                ->where([
                    'plt_id' => $pltId,
                    'gr_dtl_id' => $grDtlId
                ])
                ->first();
        $locId = object_get($findLocationInfo, 'act_loc_id');
        $locCode = object_get($findLocationInfo, 'act_loc_code');
        $locName = object_get($findLocationInfo, 'act_loc_code');
        try {

            $pallet->update([
                'loc_id' => $locId,
                'loc_name' => $locName,
                'loc_code' => $locCode,
                'plt_sts' => 'AC'
            ]);
            $this->cartonModel->getModel()
                    ->where([
                        'gr_hdr_id' => $grHdrId,
                        'gr_dtl_id' => $grDtlId,
                        'plt_id' => $pltId
                    ])
                    ->update([
                        'loc_id' => $locId,
                        'loc_name' => $locName,
                        'loc_code' => $locCode,
                        'loc_type_code' => Status::getByValue('RACK', 'LOC_TYPE_CODE')
            ]);

            $this->palletSuggestLocationModel->getModel()
                    ->where([
                        'gr_hdr_id' => $grHdrId,
                        'gr_dtl_id' => $grDtlId,
                        'plt_id' => $pltId
                    ])
                    ->update([
                        'act_loc_id' => $locId,
                        'act_loc_code' => $locCode,
                        'put_sts' => Status::getByValue('Completed', 'PUT-BACK-STATUS'),
                        'put_sts' => "CO",
            ]);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }


    public function createGRWhenScanCartons($params) {

        //lập lại qui trình của hàm createGR()
        //tách ra nhiều hàm con
        //mỗi hàm con follow tham số của hàm cũ và thêm điều kiện của yêu cầu

        //error_reporting(E_ALL);

        $asnHdrId = array_get($params, 'asn_hdr_id', null);
        $ctnrId = array_get($params, 'ctnr_id', null);
        $pltRfid = array_get($params, 'plt_rfid', null);
        $whsId = array_get($params, 'whs_id', null);
        $cusId = array_get($params, 'cus_id', null);
        $wrongRFIDS = array_get($params, 'wrong_rfids', null);
        //1. Create Goods Receipt
        $asnHdr = $this->asnHdrModel->getModel()
                ->find($asnHdrId);
        //2. Create One Goods Receipt Details
        $grHdr = $this->createGRHdrNew($asnHdrId, $ctnrId);
        if(!$asnHdr || !$grHdr) {

            return false;
        }
        $grHdrId = object_get($grHdr,'gr_hdr_id');
        $grHdrNum = object_get($grHdr, 'gr_hdr_num');
        $asnHdrNum = object_get($asnHdr, 'asn_hdr_num');
        //find asn_dtl_id on this pallet
        $asnDtlIds = $this->virtualCartonModel->getAsnDtlIdByRfid($whsId, $cusId, $pltRfid, $ctnrId);

        $vtnCtnFirst = $this->virtualCartonModel->getModel()
                ->whereNotNull('asn_dtl_id')
                ->where('plt_rfid', $pltRfid)
                ->first();
        $asnDetail = $this->asnDtlModel->getModel()
                ->find(object_get($vtnCtnFirst, "asn_dtl_id"));
        $ctnsByRfidAsnDtlId = $this->virtualCartonModel->getModel()
                ->where(
                    'plt_rfid',$pltRfid
                )
                ->whereIn('asn_dtl_id', $asnDtlIds)
                ->get();
        $vtlCtnInAsnDtl = [];
        foreach($ctnsByRfidAsnDtlId as $row) {
            $vtlCtnInAsnDtl[$row->asn_dtl_id][] = $row;
        }

        $asnDtlId = object_get($asnDetail, 'asn_dtl_id');
        $asnDtlTtlCtn = object_get($asnDetail, 'asn_dtl_ctn_ttl', 0);

        //2. Create One Goods Receipt Details
        $grDtl = $this->goodsReceiptDetailModel->createGrDtlNew($grHdr, $asnDetail);
        $grDtlId = object_get($grDtl, 'gr_dtl_id');

        //4. Check Pallet if not existed, create a new pallet and update carton totalupdateGrDtlWhenScanPallet
        $palletObj = $this->palletModel->createPalletIfNotExist($pltRfid, $grDtl, $grHdr, $asnDetail);

        $this->createCtnIfNotDefined($whsId, $pltRfid, $wrongRFIDS, $grDtl, $asnDetail, $grHdrNum);

        $numVirtualCartonHavePallet = $this->virtualCartonModel->countVtlCtnHavePltByAsnDtlId($asnDtlId, $ctnrId, $pltRfid);

        //scan pallet multitime
        //7. Update GR Dtl: real actual cartons, real damaged cartons, real pallet total
        $grDtlNew = $this->goodsReceiptDetailModel->updateGrDtlWhenScanPallet($grDtlId);

        //9. Set real damaged Cartons if is_damaged = 1, insert damaged_cartons
        $this->setRealDamageCarton($pltRfid, $asnDetail);

        //11. Update Pal_sug_loc, LPN, total Cartons per pallet(inconsistent data for each time printing)
        $vtlCtns = $vtlCtnInAsnDtl[$asnDtlId];
        $this->updateSugPltLoc($palletObj, $vtlCtns, $grDtl, $asnDetail, $grHdrNum);

        //received all cartons
        if ($asnDtlTtlCtn == $numVirtualCartonHavePallet) {
            //scan pallet complete, update goods receipt
            //10. Update Virtual Carton status is Goods Receipt Created (carton not allowed to rescan)
            $this->updateVtlCtnGRCreated($asnHdrId, $pltRfid);
            $this->updateASNDtlReceived($asnDtlId);
            $paramsDetail = [];
            //12. Update Inventory with available qty and damaged qty

            $actCtnTtl = object_get($grDtlNew, 'gr_dtl_act_ctn_ttl');
            $pack = object_get($grDtlNew, 'pack');
            $availQty = $actCtnTtl * $pack;
            $dmgTtl = object_get($grDtlNew, 'gr_dtl_dmg_ttl');
            $dmgQty = $dmgTtl * $pack;
            $this->updateInventory($whsId, $cusId, $grDtl, $availQty, $dmgQty);

            //15. Update Goods receipt dtl Received when asn_dtl Received and count all virtual carton status GR Created
            $this->asnDtlModel->updateAsnDtlStatus($asnDtlId, Status::getByKey('GR_STATUS', 'RECEIVED'));
            $this->goodsReceiptDetailModel->updateGoodsReceiptReceived($grDtlId);

            //16. Update Goods receipt Header when all ASN dtls of one container are received and all gr dtls are received too
            $this->updateGrHdrStatus($grHdrId);
            $this->updateEventTracking($grHdr, $availQty, $dmgQty);

            //17. Put pallet and cartons on Rack and update pal_sug_loc with actual location at step 11, pallet is not existed if not passed step 3
            $this->putPalletAndCtnOnRack($pltRfid, $grDtlId);
        }

        $returnData = [
            'gr_hdr_num' => $grHdrNum,
            'asn_hdr_num' => $asnHdrNum
        ];
        return $returnData;
    }

    public function updateQtyByGRDtl($whsId, $cusId, $asnDetail, $grHrd) {
        $actCtn = $this->getActCtn($asnDetail['asn_dtl_id']);
        $pltTtl = $this->getActPlt($asnDetail['asn_dtl_id']);
        $discTtl = $actCtn - $asnDetail->asn_dtl_ctn_ttl;
        $isDamage = $this->checkDamage($asnDetail['asn_dtl_id']);
        $paramsDetail = [
            'asn_dtl_id'         => $asnDetail['asn_dtl_id'],
            'gr_hdr_id'          => $grHrd->gr_hdr_id,
            'gr_dtl_ept_ctn_ttl' => $asnDetail->asn_dtl_ctn_ttl,
            'gr_dtl_act_ctn_ttl' => $actCtn,
            'gr_dtl_plt_ttl'     => $pltTtl,
            'gr_dtl_disc'        => $discTtl,
            'gr_dtl_is_dmg'      => $isDamage,
            'sku'                => $asnDetail['asn_dtl_sku'],
            'size'               => $asnDetail['asn_dtl_size'],
            'color'              => $asnDetail['asn_dtl_color'],
            'lot'                => $asnDetail['asn_dtl_lot'],
            'po'                 => $asnDetail['asn_dtl_po'],
            'uom_code'           => $asnDetail['uom_code'],
            'uom_name'           => $asnDetail['uom_name'],
            'uom_id'             => $asnDetail['uom_id'],
            'upc'                => $asnDetail['asn_dtl_cus_upc'],
            'ctnr_id'            => $asnDetail['ctnr_id'],
            'ctnr_num'           => $asnDetail['ctnr_num'],
            'item_id'            => $asnDetail['item_id'],
            'pack'               => $asnDetail['asn_dtl_pack'],
            'length'             => array_get($asnDetail, 'asn_dtl_length', 0),
            'width'              => array_get($asnDetail, 'asn_dtl_width', 0),
            'height'             => array_get($asnDetail, 'asn_dtl_height', 0),
            'weight'             => array_get($asnDetail, 'asn_dtl_weight', 0),
            'cube'               => array_get($asnDetail, 'asn_dtl_cube', 0),
            'volume'             => array_get($asnDetail, 'asn_dtl_volume', 0),
            'expired_dt'         => array_get($asnDetail, 'expired_dt', 0),
        ];

        if (is_null($pltTtl)) {
            throw new \Exception("Total of pallet can not be null.");
        }

        $this->inventorySummaryModel->updateQtyByGRDtl($whsId, $cusId, $paramsDetail);
    }

    public function createPalletSuggestLocation($params)
    {
        list ($pallet, $grHdr, $grDtl, $asnDetail, $vtlCtns) = array_values($params);

        $grHdrNum = $grHdr->gr_hdr_num;
        $grHdrId = $grHdr->gr_hdr_id;
        $whsId = array_get($pallet, 'whs_id', null);
        $cusId = $grHdr->cus_id;
        $sugLocation = (new LocationModel())->getEmptyLocationByCusId($whsId, $cusId);

        if (! $sugLocation) {
            return;
        }

        $suPltLocData = [
            'plt_id'       => array_get($pallet, 'plt_id', null),
            'loc_id'       => array_get($sugLocation, 'loc_id', null),
            'data'         => array_get($sugLocation, 'loc_code', null),
            'ctn_ttl'      => count($vtlCtns),
            'item_id'      => array_get($asnDetail, 'item_id', null),
            'sku'          => array_get($asnDetail, 'asn_dtl_sku', 0),
            'size'         => array_get($asnDetail, 'asn_dtl_size', 'NA'),
            'color'        => array_get($asnDetail, 'asn_dtl_color', 'NA'),
            'lot'          => array_get($asnDetail, 'asn_dtl_lot', 'NA'),
            'gr_hdr_id'    => $grHdrId,
            'gr_dtl_id'    => object_get($grDtl, 'gr_dtl_id', null),
            'gr_hdr_num'   => $grHdrNum,
            'whs_id'       => $whsId,
            'put_sts'      => "NW"
        ];

        $this->palletSuggestLocationModel->refreshModel();
        $sugLoc = $this->palletSuggestLocationModel->create($suPltLocData);

        return $sugLoc;
    }
}