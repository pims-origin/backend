<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerChargeDetail;
use League\Fractal\TransformerAbstract;

class CustomerChargeDetailTransformer extends TransformerAbstract
{
    public function transform(CustomerChargeDetail $customerChargeDetail)
    {
        return [
            //'whs_id'              => $customerChargeDetail->whs_id,
            //'chg_code_id'         => $customerChargeDetail->chg_code_id,
            //'cus_charge_dtl_rate' => $customerChargeDetail->cus_charge_dtl_rate,
            //'cus_charge_dtl_des'  => $customerChargeDetail->cus_charge_dtl_des,
            //'created_at'          => $customerChargeDetail->created_at,
            //'updated_at'          => $customerChargeDetail->updated_at,
            //'deleted_at'          => $customerChargeDetail->deleted_at,
        ];
    }
}
