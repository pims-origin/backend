<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Seldat\Wms2\Models\Item;

class ItemTest extends TestCase
{
    use DatabaseTransactions;

    protected $warehouseId;
    protected $zoneId;
    protected $typeId;
    protected $statusCode;

    /**
     * ItemTest constructor.
     */

    public function getEndPoint()
    {
        return "/v1/items";
    }


    protected $searchParams = [
        'sku'    => 'abc',
        'size'   => 1,
        'color'  => 2,
        'cus_id' => 3,
        'size'   => 4,
    ];

    protected $itemParams = [
        'description' => 'Item from unit test',
        'sku'         => '@S@K@U@',
        'size'        => 1267,
        'color'       => 252342,
        'uom_id'      => 33,
        'pack'        => 44,
        'length'      => 55,
        'width'       => 66,
        'height'      => 77,
        'weight'      => 88,
        'volume'      => 99,
        'cus_id'      => 10,
        'cus_upc'     => 'll',
        'status'      => 'AC',
        'condition'   => 13,
        'created_by'  => 14,
        'updated_by'  => 15,
    ];

    public function setUp()
    {
        parent::setUp();
    }

    public function testSearchItem_Ok()
    {
        $this->call('GET', $this->getEndPoint(), $this->searchParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_NoParam_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testRead_Ok()
    {
        $itemId = factory(Item::class)->create()->item_id;

        $this->call('GET', $this->getEndPoint() . "/{$itemId}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $this->call('POST', $this->getEndPoint(), $this->itemParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    //public function testStore_EmptyField_Fail()
    //{
    //    unset($this->itemParams['item_code']);
    //    $this->call('POST', $this->getEndPoint(), $this->itemParams);
    //
    //    $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    //}

    public function testStore_ExistUniqueField_Fail()
    {
        $item = factory(Item::class)->create();
        $item1 = factory(Item::class)->create();

        //$this->itemParams['item_code'] = $item1->item_code;
        $this->itemParams['sku'] = $item->sku;
        $this->itemParams['size'] = $item->size;
        $this->itemParams['color'] = $item->color;
        $this->itemParams['uom_id'] = $item->uom_id;
        $this->itemParams['pack'] = $item->pack;

        $this->call('POST', $this->getEndPoint(), $this->itemParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_LessZone_Fail()
    {
        $this->itemParams['length'] = -12;
        $this->call('POST', $this->getEndPoint(), $this->itemParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    //public function testStore_ExistUpc_Fail()
    //{
    //    $item = factory(Item::class)->create();
    //    //$this->itemParams['item_code'] = $item->item_code;
    //
    //    $this->call('POST', $this->getEndPoint(), $this->itemParams);
    //    $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    //}

    public function testStore_InvalidField_Fail()
    {
        $this->itemParams['cus_id'] = "abc";

        $this->call('POST', $this->getEndPoint(), $this->itemParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_FieldToLong_Fail()
    {
        $this->itemParams['cus_upc'] = 'll111111111111111111111111111111111111111111111111111';
        $this->call('POST', $this->getEndPoint(), $this->itemParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_FieldIsNotNumber_Fail()
    {
        $this->itemParams['pack'] = 'qwerty';
        $this->itemParams['size'] = 'qwerty';
        $this->itemParams['length'] = '-5';
        $this->itemParams['width'] = '0.1';
        $this->itemParams['height'] = '10.1';
        $this->itemParams['weight'] = 'qwerty';
        $this->call('POST', $this->getEndPoint(), $this->itemParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        $itemId = factory(Item::class)->create()->item_id;

        $this->call('PUT', $this->getEndPoint() . "/{$itemId}", $this->itemParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_NotExist_Fail()
    {
        $itemId = 999999;
        $this->call('PUT', $this->getEndPoint() . "/$itemId", $this->itemParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_ExistUniqueFields_Fail()
    {
        $item = factory(Item::class)->create();
        $item1 = factory(Item::class)->create();

        //$this->itemParams['item_code'] = $item1->item_code;
        $this->itemParams['sku'] = $item->sku;
        $this->itemParams['size'] = $item->size;
        $this->itemParams['color'] = $item->color;
        $this->itemParams['uom_id'] = $item->uom_id;
        $this->itemParams['pack'] = $item->pack;

        $itemId = factory(Item::class)->create()->item_id;
        $this->call('PUT', $this->getEndPoint() . "/{$itemId}", $this->itemParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    //public function testUpdate_EmptyField_Fail()
    //{
    //    $itemId = factory(Item::class)->create()->item_id;
    //
    //    unset($this->itemParams['item_code']);
    //    $this->call('PUT', $this->getEndPoint() . "/{$itemId}", $this->itemParams);
    //
    //    $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    //}

    public function testUpdate_InvalidField_Fail()
    {
        $itemId = factory(Item::class)->create()->item_id;

        $this->itemParams['cus_id'] = "abc";
        $this->call('PUT', $this->getEndPoint() . "/{$itemId}", $this->itemParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDelete_Ok()
    {
        $itemId = factory(Item::class)->create()->item_id;

        $this->call('DELETE', $this->getEndPoint() . "/{$itemId}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDelete_NotExit_Fail()
    {
        $this->call('DELETE', $this->getEndPoint() . "/9999999999999999");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $itemId1 = factory(Item::class)->create()->item_id;
        $itemId2 = factory(Item::class)->create()->item_id;
        $itemId3 = factory(Item::class)->create()->item_id;
        $itemId4 = factory(Item::class)->create()->item_id;
        $itemId5 = factory(Item::class)->create()->item_id;

        $this->call('DELETE', $this->getEndPoint(), ['item_id' => [$itemId1, $itemId2, $itemId3, $itemId4, $itemId5]]);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }
}