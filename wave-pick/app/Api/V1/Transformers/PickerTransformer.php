<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class PickerTransformer extends TransformerAbstract
{
    public static $statuses = [
        'AC'    => 'Active',
        'IA'    => 'Inactive'
    ];

    public function transform($picker)
    {
        return [
            'user_name'     => $picker->username,
            'first_name'    => $picker->first_name,
            'last_name'     => $picker->last_name,
            'department'    => $picker->dept,
            'office'        => $picker->off_loc,
            'email'         => $picker->email,
            'phone_number'  => $picker->phone,
            'status'        => self::$statuses[$picker->status]
        ];
    }
}
