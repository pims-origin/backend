<?php

namespace App\Api\V1\Validators;


class ZoneSaveMassValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id'   => 'required',
            'zone_id'  => 'required',
            'loc_id'   => 'required',
            'loc_id.*' => 'required|exists:location,loc_id',
        ];
    }
}
