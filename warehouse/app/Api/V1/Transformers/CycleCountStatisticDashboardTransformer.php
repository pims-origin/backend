<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use App\CustomObject;
use Seldat\Wms2\Models\CycleHdr;
use League\Fractal\TransformerAbstract;


class CycleCountStatisticDashboardTransformer extends TransformerAbstract
{
    /**
     * @param CycleHdr $ccHdrInfo
     *
     * @return array
     */
    public function transform(CycleHdr $ccHdrInfo)
    {
        return [
            'cycle_hdr_id'          => object_get($ccHdrInfo, 'cycle_hdr_id', 0),
            'cycle_num'         => object_get($ccHdrInfo, 'cycle_num', ''),
            'cycle_sts'         => object_get($ccHdrInfo, 'cycle_sts', ''),
            'loc_total'          => intval($ccHdrInfo->loc_total),
        ];
    }

}