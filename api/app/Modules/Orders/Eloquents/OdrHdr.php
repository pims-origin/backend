<?php

namespace App\Modules\Orders\Eloquents;

class OdrHdr extends BaseSoftModel
{
    protected $table = 'odr_hdr';

    protected $primaryKey = 'odr_id';

    protected $fillable = [
        'ship_id',
        'cus_id',
        'whs_id',
        'so_id',
        'odr_num',
        'cus_odr_num',
        'ref_cod',
        'cus_po',
        'odr_type',
        'odr_sts',
        'ship_to_name',
        'ship_to_email',
        'ship_to_add_1',
        'ship_to_add_2',
        'ship_to_city',
        'ship_to_state',
        'ship_to_zip',
        'ship_to_country',
        'carrier',
        'odr_req_dt',
        'act_cmpl_dt',
        'req_cmpl_dt',
        'ship_by_dt',
        'cancel_by_dt',
        'act_cancel_dt',
        'ship_after_dt',
        'shipped_dt',
        'cus_pick_num',
        'ship_method',
        'cus_dept_ref',
        'csr',
        'rush_odr',
        'cus_notes',
        'in_notes',
        'hold_sts',
        'back_odr_seq',
        'back_odr',
        'org_odr_id',
        'sku_ttl',
        'audit_sts',
        'wv_num',
        'is_ecom',
        'dlvy_srvc',
        'dlvy_srvc_code',
        'cosignee_sig',
        'carrier_code',
        'shipping_addr_id',
        'ship_to_phone',
        'schd_ship_dt',
        'routed_dt',
        'csr_notes',
        'spec_instruct',
    ];

    public $dates = [
        'odr_req_dt',
        'req_cmpl_dt',
        'act_cmpl_dt',
        'ship_by_dt',
        'cancel_by_dt',
        'act_cancel_dt',
        'ship_after_dt',
        'shipped_dt',
    ];

    public function odrDtls()
    {
        return $this->hasMany(__NAMESPACE__ . '\OdrDtl', 'odr_id', 'odr_id');
    }

    public function thirdParty()
    {
        return $this->hasOne(__NAMESPACE__ . '\ThirdParty', 'tp_id', 'shipping_addr_id');
    }

    public function odrHdrMeta()
    {
        return $this->hasOne(__NAMESPACE__ . '\OdrHdrMeta', 'odr_id');
    }

    public function packHdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\PackHdr', 'odr_hdr_id', 'odr_id');
    }

    public function odrHdrSts()
    {
        return $this->hasOne(__NAMESPACE__ . '\Status', 'sts', 'odr_sts');
    }

    public function details()
    {
        return $this->hasMany(__NAMESPACE__ . '\OdrDtl', 'odr_id', 'odr_id');
    }
}
