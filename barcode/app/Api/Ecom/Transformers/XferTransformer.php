<?php

namespace App\Api\Ecom\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\XferTicket;

class XferTransformer extends TransformerAbstract
{
    public function transform(XferTicket $xferTicket)
    {
        return [
            'xfer_ticket_id'  => object_get($xferTicket, 'xfer_ticket_id'),
            'xfer_ticket_num' => object_get($xferTicket, 'xfer_ticket_num'),
            'sku'             => object_get($xferTicket, 'co_item', 0). '/' .object_get($xferTicket, 'ttl_item', 0)
        ];
    }
}
