<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportLaborTracking;

class ReportLaborTrackingTransformer extends AbstractTransformer
{

    /**
     * @param ReportLaborTracking $reportSKUSummary
     *
     * @return array
     */
    public function transform(ReportLaborTracking $reportLaborTracking)
    {
        return [
            'lt_id'         => array_get($reportLaborTracking, 'lt_id'),
            'user_id'       => array_get($reportLaborTracking, 'user_id', null),
            'cus_id'        => array_get($reportLaborTracking, 'cus_id', null),
            'whs_id'        => array_get($reportLaborTracking, 'whs_id', null),
            'owner'         => array_get($reportLaborTracking, 'owner', null),
            'trans_num'     => array_get($reportLaborTracking, 'trans_num', null),
            'trans_dtl_id'  => array_get($reportLaborTracking, 'trans_dtl_id', null),            
            'start_time'    => $this->setTimeZone(array_get($reportLaborTracking, 'whs_id', null), array_get($reportLaborTracking, 'start_time', null)),
            'end_time'      => $this->setTimeZone(array_get($reportLaborTracking, 'whs_id', null), array_get($reportLaborTracking, 'end_time', null)),
            'timezone'      => $this->getWhsTimeZone(array_get($reportLaborTracking, 'whs_id', null)) ?: 'UTC±0',
            'lt_type'       => array_get($reportLaborTracking, 'lt_type', null)
        ];
    }
}
