<?php

namespace App;

class CycleHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cycle_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cycle_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cycle_name',
        'cycle_num',
        'whs_id',
        'count_by',
        'cycle_method',
        'cycle_type',
        'cycle_detail',
        'description',
        'assignee_id',
        'due_dt',
        'has_color_size',
        'cycle_sts',
        'sts'
    ];

    /**
     *
     */
    public function assigneeUser()
    {
        return $this->belongsTo(User::class, 'assignee_id', 'user_id');
    }

    /**
     *
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     */
    public function cycleDtl()
    {
        return $this->hasMany(CycleDtl::class, 'cycle_hdr_id');
    }
}
