<?php

namespace App\Api\Inbound2\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\ReportLaborTracking;

class ReportLaborTrackingModel extends AbstractModel
{
    /**
     * ReportLaborTrackingModel constructor.
     */
    public function __construct(ReportLaborTracking $model = null)
    {
        $this->model = ($model) ?: new ReportLaborTracking();
    }


}