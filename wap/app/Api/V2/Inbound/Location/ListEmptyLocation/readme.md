- WAP-584: [Inbound - Putback] Create API get suggest location list by Pallet tag & Customer.
    + Support 2 option :
      *Has pallet*
      -  Empty Location
      *Empty pallet*
      - Be able contain more cartons
      *Location follow*
       - Low level and Nearly Location

- WMS2-5074: [WAP API-PUTBACK] Improvement Putpack function 
    + *Step*
        # Run API Get empty location in case no pallet and validate : https://{{url}}/core/wap/v2/whs/7/location/rack/get-empty-by-customer
        - Cartons is belong to warehouse, customer
        - Is the same SKU 
        - Have picked carton
        - Low level
        *Note* limit 5 location and order by these conditions
        # Run APi validate location : https://{{url}}/core/wap/v2/whs/7/location/rack/check-valid-by-customer
        - Cartons is belong to warehouse, customer
        # Run APi Submit putback: https://{{url}}/core/wap/v2/whs/7/location/rack/putback
        -  Cartons is belong to warehouse, customer