<?php

namespace App\Modules\Orders\Models;

use App\Modules\Orders\Eloquents\OdrHdr;
use App\Modules\Orders\Eloquents\OdrDtl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Modules\Orders\Lib\BulkOrderImport;
use App\Modules\Orders\Lib\EcomOrderImport;
use App\Modules\Orders\Lib\RetailOrderImport;

class OrderModel extends AbstractModel
{
    const STATUS_NEW = 'NW';
    const STATUS_ALLOCATED = 'AL';
    const STATUS_CANCEL = 'CC';
    const STATUS_SHIPPED = 'SH';

    public function __construct(OdrHdr $odrHdr)
    {
        parent::__construct($odrHdr);
    }

    public function search($input, $whsId, $cusId, $limit, $baseUrl)
    {
        $cusOfUser = Config::get('data.cusOfUser');

        $check = false;
        foreach ($cusOfUser as $data) {
            if ($data->whs_id == $whsId && $data->cus_id == $cusId)
            {
                $check = true;
                break;
            }
        }

        if ($check)
        {
            $query = OdrHdr::where('deleted', 0)
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId);

            $query->orderBy('created_at', 'DESC');

            $orders = $query->paginate($limit)->withPath($baseUrl);

            return $orders;
        }
    }

    public function show($whsId, $cusId, $odrHdrId)
    {
        $cusOfUser = Config::get('data.cusOfUser');
        
        $order = OdrHdr::where('deleted', 0)
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->find($odrHdrId);

        foreach ($cusOfUser as $data) {
            if ($data->whs_id == $whsId && $data->cus_id == $cusId)
            {
                return $order;
            }
        }
    }

    public function cancelOrder($whsId, $cusId, $odrNum)
    {
        $cusOfUser = Config::get('data.cusOfUser');
        $curUserId = Config::get('data.user');

        $ordHdr = OdrHdr::where('deleted', 0)
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->where('odr_num', $odrNum)
            ->first();

        if (!$ordHdr)
        {
            throw new \Exception('Order not existed.');
        }
        else if($ordHdr->odr_sts != self::STATUS_NEW && $ordHdr->odr_sts != self::STATUS_ALLOCATED)
        {
            throw new \Exception('Just New Orders and Allocated Orders can be cancel!');
        }

        DB::beginTransaction();
        // Return avail qty (invt_smr) and in_hand_qty (inventory) if Order Status is Allocated
        if($ordHdr->odr_sts == self::STATUS_ALLOCATED) {
            $this->correctInvtSmrTable($ordHdr->odr_id);
            $this->correctInventoryTable($ordHdr->odr_id);
        }

        $ordHdr->cancel_by_dt = time();
        $ordHdr->odr_sts = self::STATUS_CANCEL;
        $ordHdr->updated_at = time();
        $ordHdr->updated_by = $curUserId;

        foreach ($cusOfUser as $data) {
            if ($data->whs_id == $whsId && $data->cus_id == $cusId)
            {
                

                $ordHdr->save();

                DB::commit();

                return $ordHdr;
            }
        }
    }

    public function correctInvtSmrTable($odrId)
    {
        $odrDtls = OdrDtl::where('odr_id', $odrId)->get();
        foreach($odrDtls as $odrDtl) {
            $allocatedQty = $odrDtl->alloc_qty;
            $itemId = $odrDtl->item_id;
            DB::table('invt_smr')->where('item_id', $itemId)->update([
                'allocated_qty' => DB::raw('allocated_qty - ' . $allocatedQty),
                'avail'         => DB::raw('avail + ' . $allocatedQty)
            ]);
        }
    }

    public function correctInventoryTable($odrId)
    {
        $odrDtls = OdrDtl::where('odr_id', $odrId)->get();
        foreach($odrDtls as $odrDtl) {
            $allocatedQty = $odrDtl->alloc_qty;
            $itemId = $odrDtl->item_id;
            DB::table('inventory')->where('item_id', $itemId)->update([
                'in_pick_qty'   => DB::raw('in_pick_qty - ' . $allocatedQty),
                'in_hand_qty'   => DB::raw('in_hand_qty + ' . $allocatedQty)
            ]);
        }
    }

    public function getOrderShipped($input, $whsId, $cusId)
    {
        $cusOfUser = Config::get('data.cusOfUser');

        $check = false;
        $result = NULL;
        foreach ($cusOfUser as $data) {
            if ($data->whs_id == $whsId && $data->cus_id == $cusId)
            {
                $check = true;
                break;
            }
        }

        if ($check)
        {
            $query = OdrHdr::where('deleted', 0)
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId)
                ->where('odr_sts', self::STATUS_SHIPPED);

            if (array_get($input, 'customer_order_number', NULL))
            {
                $query->where('cus_odr_num', array_get($input, 'customer_order_number', NULL));
            }

            if (array_get($input, 'po', NULL))
            {
                $query->where('cus_po', array_get($input, 'po', NULL));
            }

            $query->orderBy('created_at', 'DESC');
            $result = $query->get();
        }

        return $result;
    }

//    public function update($input)
//    {
//        $whsId = array_get($input, 'whs_id');
//        $cusId = array_get($input, 'cus_id');
//        $odrId = array_get($input, 'odr_id');
//        $orderType = array_get($input, 'order_type');
//
//        if (!$this->checkCusOfUser($whsId, $cusId))
//        {
//            throw new \Exception('Customer don\'t belong to this User.');
//        }
//
//        if (!$odrHdr = OdrHdr::find($odrId))
//        {
//            throw new \Exception('Order is not existed.');
//        }
//
//        if ($odrHdr->odr_sts != self::STATUS_NEW)
//        {
//            throw new \Exception('Only New Order can be updated!');
//        }
//
//        switch ($orderType) {
//            case 'Bulk':
//                $parentModel = new BulkOrderImport;
//                break;
//
//            case 'Retail':
//                $parentModel = new RetailOrderImport;
//                break;
//
//            case 'Ecom':
//                $parentModel = new EcomOrderImport;
//                break;
//
//            default:
//                throw new \Exception('Order type does not support');
//                break;
//        }
//
//        $formatData = $parentModel->formatData($input);
//
//        DB::transaction(function() use($parentModel, $formatData, &$odrHdr){
//            $thirdParty = $parentModel->createOrUpdateThirdParty($formatData);
//            $shippingOdr = $parentModel->createOrUpdateShippingOdr($formatData);
//
//            $formatData['shipping_addr_id'] = $thirdParty->tp_id;
//            $formatData['so_id'] = $shippingOdr->so_id;
//
//            $paramOdrHeader = $parentModel->getDataHeader($formatData);
//            $odrHdr = $this->updateOrderHdr($paramOdrHeader, $odrHdr);
//
//            $paramOdrDetails = $parentModel->getDataDetails($formatData, $odrHdr);
//            $odrDtls = $this->updateOrderDtl($paramOdrDetails, $odrHdr);
//
//            $odrHdrMeta = $parentModel->createOrUpdateOdrHdrMeta($formatData, $odrHdr);
//            $odrDtlMetas = $parentModel->createOrUpdateOdrDtlMeta($formatData, $odrDtls);
//        });
//
//        return $odrHdr;
//    }

    private function updateOrderHdr($paramOdrHeader, $odrHdr)
    {
        if ($odrHdr->odr_type == 'EC' && $paramOdrHeader['odr_type'] != 'EC')
        {
            throw new \Exception('Ecom order can\'t change to a different order type');
        } else
        {
            $odrHdr->update($paramOdrHeader);
            $odrHdr = $odrHdr->fill($paramOdrHeader);
        }

        return $odrHdr;
    }

    private function updateOrderDtl($data, $odrHdr)
    {
        $arrOdrDtl = $removeItems = [];
        $removeItems = $this->removeItems($data, $odrHdr);
        foreach ($data as $item) {
            $odrDtl = $odrHdr->odrDtls()->withTrashed()->where('sku', $item['sku'])->first();
            if (!$odrDtl)
            {
                $arrOdrDtl[] = $odrHdr->odrDtls()->create($item);
            } else
            {
                $odrDtl->update($item);
                $odrDtl->restore();
                $arrOdrDtl[] = $odrDtl->fill($item);
            }
        }

        return $arrOdrDtl;
    }

    private function removeItems($data, $odrHdr)
    {
        $odrDtls = $odrHdr->odrDtls;
        $oldItem = array_pluck($odrDtls, 'sku');
        $newItem = array_column($data, 'sku');
        $arrItem = array_diff($oldItem, $newItem);

        if (count($arrItem) > 0)
        {
            foreach ($odrDtls as $odrDtl) {
                if (in_array($odrDtl->sku, $arrItem))
                {
                    $odrDtl->delete();
                    if ($odrDtlMeta = $odrDtl->odrDtlMeta)
                    {
                        $odrDtlMeta->delete();
                    }
                }
            }
        }

        return $arrItem;
    }
}
