﻿<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>ASN-CartonBarcode</title>
    <style type="text/css">
	@page bigger {
            sheet-size: 6in 4in;
        }
		
		 .container {
            margin-bottom: 10px;
            padding-bottom: 10px;
            /*border-bottom: 1px dashed #000;*/

            /*padding-left: 40px;
            padding-right: 40px;*/
            width: 6in;
            heigh: 4in;

            text-align: center;

        }
		 body {
            background-color: #fff;
            color: #000;
            font-size: 20px;
            font-family: "Calibri";
			
			width: 6in;
            height: 4in;
			
            /*width: 21cm;
            height: 29.7cm;*/
			
            margin: 0 auto;
            border: 1px solid #ddd;
            padding: 20px;
            margin: 0 auto;

        }

        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 32px;
            font-weight: 100;
            margin: 0;
        }

        h2 {
            color: #364150;
            text-transform: uppercase;
            margin: 5px 0;
            font-weight: 500;
        }

        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #a9a9a9;
            font-size: 19px;
        }

        .table-style tbody tr td {
            padding-top: 5px;
            vertical-align: top;
            padding-bottom: 20px;
        }

        .table-style tbody tr td b {
            font-size: 20px;
            font-weight: 600;
            color: #282828;
        }

        .table-style1 {
            width: 100%;
            border: 1px solid #ddd;
            line-height: 30px;
            margin-bottom: 15px;
        }

        .table-style1 thead tr th {
            color: #36c6d3;
            padding: 0 5px;
            border-bottom: 1px dashed #d2d2d2;
            font-size: 16px;
            text-transform: uppercase;
            font-weight: bold;
            text-align: left;
            border-right: 1px solid #e3f1f2;
        }

        .table-style1 tbody tr td {
            border-bottom: 1px dashed #ddd;
            padding: 0 5px;
            color: #595959;
        }

        .table-style1 tbody tr:last-child td {
            border-bottom: none;
        }

        .table-style1 tbody tr td.title {
            border-right: 1px solid #ddd;
            background-color: #e3f1f2;
            width: 30%;
        }

        .table-style2 {
            border: 1px solid #a9a9a9;
            margin: 15px 0;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            /*text-transform: capitalize;*/
            line-height: 20px;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 1px dashed #d5d5d5;
            border-bottom: 1px dashed #d5d5d5;
            color: #595959;
            line-height: 30px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }

        .table-style2 tbody tr td.title {
            font-weight: 600;
            color: #364150;
        }

        .table-style2 tbody tr:nth-child(2n) td {
            background-color: #e3f1f2;
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }

        .table-style3 thead tr th {
            font-weight: 600;
            text-align: left;
            color: #364150;
            text-transform: capitalize;
        }

        .table-style3 tr td {
            line-height: 20px;
            font-size: 16px;
            color: #595959;
            padding-right: 5px;
        }

        .table-style3 tr td b {
            color: #1c1c1c;
        }

        footer {
            border-top: 1px dashed #b7bbc1;
            margin-top: 20px;
            padding-top: 5px;
        }

        .img-style {
            width: 16px;
            float: left;
            margin-right: 5px;
        }

        .uppercase-class{
            text-transform: uppercase;
        }

    </style>
</head>
<body>
<?php
date_default_timezone_set(config('app.timezone'));
?>
@foreach($asnDtl as $asnD)
<?php $totalPrint = array_get($qty, $asnD->asn_dtl_id, 1); ?>
    @for($i=1; $i<=$totalPrint; $i++)
    <div class="container">
        <table class="table-style3" cellspacing="0" cellpadding="0" width="100%">
            <!--{{--<thead>--}}-->
            <tr>
                <td colspan="7" style="text-align:center; border-bottom: 0px dashed #b7bbc1; font-weight: 600;
                font-size: 19px; line-height:30px">{{object_get($asnD,
                'ctnr_num','')}}</td>
              </tr>
            <!--{{--</thead>--}}-->
            <tbody>
                <tr>
                    <td style="width:5%">&nbsp;</td>
                    <td style="width:9%">SKU</td>
                    <td style="width:1%">:</td>
                    <td style="width:23%">{{object_get($asnD,'asn_dtl_sku','')}}</td>
                    <td style="width:9%">&nbsp;</td>
                    <td style="width:1%">&nbsp;</td>
                    <td style="width:26%">&nbsp;</td>
                    <!--<td style="width:10%">&nbsp;</td>-->
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>Size</td>
                    <td>:</td>
                    <td colspan="4">{{object_get($asnD,'asn_dtl_size','')}}</td>
                    <!--{{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}-->
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>Color</td>
                    <td>:</td>
                    <td colspan="4">{{object_get($asnD,'asn_dtl_color','')}}</td>
                    <!--{{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}-->
                </tr>
                 <tr>
                    <td>&nbsp;</td>
                    <td>Description</td>
                    <td>:</td>
                    <td colspan="4">{{object_get($asnD,'asn_dtl_des','')}}</td>
                    <!--{{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}
                    {{--<td>&nbsp;</td>--}}-->
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Lot</td>
                    <td>:</td>
                    <td>{{object_get($asnD,'asn_dtl_lot','')}}</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <!--<td>&nbsp;</td>-->
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Pack Size</td>
                    <td>:</td>
                    <td>{{object_get($asnD,'asn_dtl_pack','')}}</td>
                    <td><span style="width:9%">UOM</span></td>
                    <td>:</td>
                    <td><span style="width:35%">{{object_get($asnD,'uom_name','')}}</span></td>
                   <!-- <td>&nbsp;</td>-->
                </tr>
                <tr>
                    <td colspan="7" style="text-align:center;">
                        <table style="width: 100%;">
                            <tr>
                            <?php
                                 $ucc128_origin = object_get($asnD,'ucc128','');
                                // $ucc128_origin = '410899999904899017180507';
                                 //$ucc128_origin ='91PTU00000517170608';
                                 //$ucc128_origin ='410PTU00000517170608';
                                 //$ucc128_origin ='LPN-1705-00035-01-001';

                                 //$ucc128 ='91PTU00000517170608';
                                 $ucc128 = object_get($asnD,'ucc128','');
                                //$ucc128 = object_get($asnD,'ucc128','');
                                //$ucc128 = '410899999904899017180507';  //'(410) 8999999 048990 (17) 180507';

                                $ucc128New1 = str_replace('410', '(410) ', $ucc128);
                                $subUcc128New1 = substr($ucc128, 0, 3);
                                $len = 3;
                                if($subUcc128New1 != 410){
                                    $subUcc128New1 = substr($ucc128, 0, 2);
                                    $len = 2;
                                }

                                if ($subUcc128New1 == 91) {
                                    $cusCode = substr($ucc128New1, 2, 3);
                                    $ucc128 = str_replace($cusCode, " $cusCode ", $ucc128);
                                }

//                                $ucc128New1 = substr_replace($ucc128, '('.$subUcc128New1.') ', 0, $len);
                                $ucc128New1 = substr_replace($ucc128, ''.$subUcc128New1.'', 0, $len);
                                $subUcc128New2 = substr($ucc128New1, -8, 2);
                                $endSub = substr($ucc128New1, -6);
//                                $endStr = " ($subUcc128New2) " . $endSub;
                                $endStr = " $subUcc128New2" . $endSub;
                                $ucc128New2 = substr($ucc128New1, 0, -8) . $endStr;
                            ?>
                                <td style="text-align:center;">
                                    <barcode height="1.6" size="0.8" code="{{$ucc128_origin}}" type="EAN128B"/>
<!-- {{--                                    <barcode height="1.6" size="0.9" code="{{$ucc128_origin}}" type="C128A"/>--}}-->
                                </td>
                            </tr>
                            <tr>
<!--{{--                                <td style="text-align:center;">{{object_get($asnD,'ucc128','')}}</td>--}}
{{--                                    <td style="text-align:center;">{{$ucc128_origin}}</td>--}}-->
                                <td style="text-align:center;">{{$ucc128New2}}</td>
                            </tr>
                        </table>
                    </td>

                </tr>

                <!--be Used when have UPC barcode-->
                <?php
                     $upc = $asnD->asn_dtl_cus_upc;
                ?>
                <!--{{--@if (!empty($upc))--}}
                {{--<tr style="">--}}
                    {{--<td colspan="8" style="text-align:center;">--}}
                        {{--<table style="width: 100%;">--}}
                            {{--<tr>--}}
                                {{--<td style="text-align:center;">--}}
                                    {{--<barcode height="1.6" size="1.4" code="{{$upc}}" type="C128A"/>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                             {{--<tr>--}}
                                {{--<td style="text-align:center;">{{$upc}}</td>--}}
                            {{--</tr>--}}
                        {{--</table>--}}
                    {{--</td>--}}

                {{--</tr>--}}

                {{--@endif--}} -->

                <!--Used when not have UPC barcode-->
                <!--<tr>
                    <td colspan="6" style="text-align:center; line-height: 50px;">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align:center;">&nbsp;</td>
                </tr>-->
                <tr>
                    <td colspan="7" style="text-align:center; line-height: 85px;">&nbsp;</td>
                </tr>
                <!--/Used when not have UPC barcode-->

                <!--Used when have UPC barcode-->
               <!-- {{--<tr>--}}
                    {{--<td colspan="8" style="text-align:center; line-height: 55px;">&nbsp;</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td colspan="8" style="text-align:center; line-height: 25px; border-bottom: 1px dashed #d2d2d2;--}}
                    {{--">&nbsp;</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td colspan="8" style="text-align:center; line-height: 85px;">&nbsp;</td>--}}
                {{--</tr>--}}-->
                <!--/Used when have UPC barcode-->

            </tbody>
        </table>
        </div>
    @endfor
@endforeach

   <!-- {{--<footer>--}}
        {{--<table width="100%">--}}
            {{--<tr>--}}
                {{--<td width="50%" style="padding: 0; color: #404040; font-size: 14px">--}}
                    {{--{{date("Y-m-d h:i:s a")}}--}}
                    {{--By--}}
                    {{--{{$printedBy}}--}}
                {{--</td>--}}
                {{--<td width="50%" style="text-align: right; padding: 0; font-size: 16px; color: #3f3f3f">&nbsp;</td>--}}
            {{--</tr>--}}
        {{--</table>--}}
    {{--</footer>--}}-->


</body>
</html>
