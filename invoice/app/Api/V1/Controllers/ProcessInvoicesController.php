<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ProcessInvoiceModel;
use App\Api\V1\Transformers\DisplayErrorsTransformer;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use App\Api\V1\Models\AuthenticationService;
use Illuminate\Support\Facades\DB;

use Psr\Http\Message\ServerRequestInterface as Request;

class ProcessInvoicesController extends AbstractController
{
    /**
    * @var
    */
    protected $process;
    protected $cancelInvoiceTransformer;

    /**
    *  Constructor
    */

    public function __construct(IRequest $request)
    {
//        $object = new AuthenticationService($request);
//
//        parent::__construct($request, $object);

        $this->process = new ProcessInvoiceModel();
        $this->errorTransformer = new DisplayErrorsTransformer();
    }

    /**
     * Get the charge code details - Invoice Details page
     *
     * @param IRequest $request
     *
     * @return array
     */

    public function processInvoice(IRequest $request)
    {
        $params = $request->getParsedBody();

        //Example-1 Post Params - No client selected and select "open" invoice
//        $params = [
//            'custID' => '',
//            'whs_id' => 83417,
//            'fromDate' => '2015-08-01',
//            'toDate' => '2016-10-30',
//            'openCust' => 42262,
//            'items' => NULL,
//        ];

          //Example-2 Post Params - client selected and check the items to be invoiced
//        $params = [
//            'custID' => 42262,
//            'whs_id' => 83417,
//            'fromDate' => '2015-08-01',
//            'toDate' => '2016-10-30',
//            'openCust' => 42262,
//            'items' => [
//                'Receiving' => [
//                    35346 => 'on',
//                    35347 => 'on'
//                ],
//                'Order Processing' => [
//                    'ORD-1610-00345' => 'on',
//                    'ORD-1610-00346' => 'on'
//                ]
//            ],
//        ];

        //Example-3 Post Params - client selected and select "open" invoice
//        $params = [
//            'custID' => 42262,
//            'whs_id' => 83417,
//            'fromDate' => '2015-08-01',
//            'toDate' => '2016-10-30',
//            'openCust' => 42262,
//            'items' => NULL,
//        ];

        $result = $this->process->getProcessData($params);

        return $result;
    }

    /*
    ****************************************************************************
    */

    public function checkSummaryDate(IRequest $request)
    {
        $input = $request->getQueryParams();

        $results = $this->process->checkSummaryDate($input);

        $isValid = ! $results->isEmpty();

        return json_encode($isValid);
    }

    /**
     * Display the Invoiced invoice
     *
     * @param IRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function displayInvoicedInvoice(IRequest $request, $invoiceNo)
    {
        //Example  -  /admin/invoice/process/inv/100

        $input = $request->getQueryParams();

        if ($invoiceNo) {

            $result = $this->process->getInvoicedData($invoiceNo);

            return $result;
        }

        return FALSE;
    }

    /**
     * Cancel invoice
     *
     * @param IRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function cancelInvoice($inv_num, Request $request)
    {
        try {
            if ($results = $this->process->cancelInvoice(['invoiceNo' => $inv_num])) {
                return $this->response->item($results, $this->errorTransformer);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
