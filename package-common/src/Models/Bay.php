<?php

namespace Seldat\Wms2\Models;


class Bay extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bay';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'bay_id';

    /**
     * @var array
     */
    protected $fillable = [
        'bay_name',
        'bay_sts',
        'pos',
        'line'
    ];
    
    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }

}
