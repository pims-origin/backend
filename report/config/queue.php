<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 06-Jun-16
 * Time: 08:40
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Default Queue Driver
    |--------------------------------------------------------------------------
    |
    | The Laravel queue API supports a variety of back-ends via an unified
    | API, giving you convenient access to each back-end using the same
    | syntax for each one. Here you may set the default queue driver.
    |
    | Supported: "null", "sync", "database", "beanstalkd", "sqs", "redis"
    |
    */
    'default' => env('QUEUE_DRIVER', 'sync'),
    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */
    'connections' => [
        'sync' => [
            'driver' => env('QUEUE_SYNC_DRIVER'),
        ],
        'database' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'default',
            'expire' => 60,
        ],
        'beanstalkd' => [
            'driver' => env('QUEUE_BEANSTALKD_DRIVER'),
            'host' => env('QUEUE_BEANSTALKD_HOST'),
            'queue' => env('QUEUE_BEANSTALKD_QUEUE'),
            'ttr' => env('QUEUE_BEANSTALKD_TTR'),
        ],
        'sqs' => [
            'driver' => env('QUEUE_SQS_DRIVER'),
            'key' => env('QUEUE_SQS_KEY'),
            'secret' => env('QUEUE_SQS_SECRET'),
            'prefix' => env('QUEUE_SQS_PREFIX'),
            'queue' => env('QUEUE_SQS_QUEUE'),
            'region' => env('QUEUE_SQS_REGION'),
        ],
        /*'redis' => [
            'driver' => env('QUEUE_REDIS_DRIVER'),
            'connection' => env('QUEUE_REDIS_CONNECTION'),
            'queue' => env('QUEUE_REDIS_QUEUE'),
            'expire' => env('QUEUE_REDIS_EXPIRE'),
        ],*/
        'redis' => [
            'driver'     => 'redis',
            'connection' => 'default',
            'queue'      => env('REDIS_PREFIX', 'default'),
            'password'   => env('REDIS_PWD', ''),
            'expire'     => 60,
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */
    'failed' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => 'failed_jobs',
    ],
];
