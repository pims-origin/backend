<?php

namespace App\Api\V1\Validators;


class LaborValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            '*.urgency'         => 'required|in:overtime,rush,labor',
            '*.expected_amount' => 'numeric|between:0,999999.99',
            '*.actual_amount'   => 'numeric|between:0,999999.99',
        ];
    }
}
