<?php

namespace App\Api\V4\Inbound\Pallet\ScanPallet\Models;

use Seldat\Wms2\Models\LocationType;

class LocationTypeModel extends AbstractModel
{
    /**
     * LocationTypeModel constructor.
     *
     * @param LocationType|null $model
     */
    public function __construct(LocationType $model = null)
    {
        $this->model = ($model) ?: new LocationType();
    }

    public function getLocTypeList($limit = null)
    {
        \DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->model->select(['loc_type_id','loc_type_name','loc_type_code']);

        $models = $query->paginate($limit);

        return $models;
    }
}
