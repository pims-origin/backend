<?php

namespace App\Api\V2\Putback\AssignPallet\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Wms2\UserInfo\Data;

abstract class AbstractController extends Controller
{
    use Helpers;

    protected final function chkWhsAndCus($whsId, $cusId)
    {
        if (!Data::isAccessWhsAndCus($cusId, $whsId)) {
            $msg = sprintf("You cannot access Warehouse %d and Customer %d! Please contact with admin",
                $cusId, $cusId);
            return $this->response->errorUnauthorized($msg);
        }
    }
}
