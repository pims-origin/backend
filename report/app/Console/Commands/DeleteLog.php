<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Config\Definition\Exception\Exception;

class DeleteLog extends AbstractCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Log';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        $fiveDateAgo = date('Y-m-d', time() - 86400 );
        @unlink(storage_path("logs/success_{$fiveDateAgo}.log"));
        @unlink(storage_path("logs/log_pdo_{$fiveDateAgo}.log"));
        @unlink(storage_path("logs/lumen-{$fiveDateAgo}.log"));
    }
}
