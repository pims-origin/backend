<?php

namespace Seldat\Wms2\Models;


class ContainerCharge extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ctnr_charge';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cc_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'ctnr_id',
        'whs_id',
        'start',
        'end',
        'duration',
        'cus_name',
        'ctnr_num',
        'cc_sts',
        'temp',
        'bay_id',
        'bay_name'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function container()
    {
        return $this->hasOne(__NAMESPACE__ . '\Container', 'ctnr_id', 'ctnr_id');
    }

    public function bay()
    {
        return $this->hasOne(__NAMESPACE__ . '\Bay', 'bay_id', 'bay_id');
    }

    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }
}
