<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\EventTracking;

class EventTrackingReportsTransformer extends TransformerAbstract
{
    /**
     * @param EventTracking $eventTracking
     *
     * @return array
     */
    public function transform(EventTracking $eventTracking)
    {

        return [
            'owner'       => object_get($eventTracking, 'owner', null),
            'description' => object_get($eventTracking, 'event.des', null),
            'created_at'  => $eventTracking->created_at->format('m/d/Y h:i')
        ];

    }

}
