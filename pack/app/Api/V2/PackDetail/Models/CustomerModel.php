<?php

namespace App\Api\V2\PackDetail\Models;

use \Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;
use \Seldat\Wms2\Models\CustomerAddress;

class CustomerModel extends AbstractModel
{

    /**
     * EloquentMenuGroup constructor.
     *
     * @param Customer $model
     */
    public function __construct(Customer $model = null, CustomerAddress $customerAddressModel = null)
    {
        $this->model = ($model) ?: new Customer();
        $this->customerAddressModel = ($customerAddressModel) ?: new CustomerAddress();
    }
}
