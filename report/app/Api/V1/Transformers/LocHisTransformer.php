<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-June-2017
 * Time: 09:27
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;


class LocHisTransformer extends TransformerAbstract
{
    //return result
    public function transform($locHis)
    {
        return [
            'loc_used_ttl'   => object_get($locHis, 'loc_used_ttl', null),
            'loc_avai_ttl'   => object_get($locHis, 'loc_avai_ttl', null),
            'loc_ttl'   => object_get($locHis, 'loc_ttl', null),
            'cur_his'   => object_get($locHis, 'cur_his', null),
            'cur_day'   => object_get($locHis, 'cur_day', null),
            'whs_id'    => object_get($locHis, 'whs_id', null)
        ];
    }

}
