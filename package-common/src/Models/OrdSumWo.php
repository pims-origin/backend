<?php

namespace Seldat\Wms2\Models;


class OrdSumWo extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ord_sum_wo';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_num';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'event_id',
        'odr_num',
        'dt',
        'chg_code_id',
        'labor_val'
    ];

    public function orderHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\OrderHdr', 'odr_num', 'odr_num');
    }
    
    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
    
    public function eventTracking()
    {
        return $this->belongsTo(__NAMESPACE__ . '\EventTracking', 'event_id', 'id');
    }
}

