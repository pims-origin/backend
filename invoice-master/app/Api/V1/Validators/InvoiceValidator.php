<?php

namespace App\Api\V1\Validators;

class InvoiceValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id'             => 'required|integer|exists:warehouse,whs_id,whs_status,AC',
            'cus_id'             => 'required|integer|exists:customer,cus_id,cus_status,AC',
            'due_dt'             => 'required',
            'discount'           => 'numeric|between:0,99999.99',
            'tax'                => 'numeric|between:0,99999.99',
            'subtotal'           => 'numeric|between:0,999999999999999.99',
            'total'              => 'numeric|between:0,999999999999999.99',
            'inv_sts'            => 'required|string|in:NE,PE',
            'note'               => 'string|max:255',
            'items.*.ba_id'      => 'required|integer|exists:billable_hdr,ba_id',
            'items.*.ba_dtl_id'  => 'required|integer|exists:billable_dtl,ba_dtl_id',
            'items.*.ref_num'    => 'required|string',
            'items.*.sac'        => 'required|max:4',
            'items.*.uom'        => 'max:4',
            'items.*.qty'        => 'required|numeric',
            'items.*.unit_price' => 'required|numeric|between:0,99999.99',
            'items.*.currency'   => 'required|string',
            'items.*.amount'     => 'numeric|between:0,999999999999999.99',
        ];
    }
}
