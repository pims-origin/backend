<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\SysBugModel;
use App\Api\V1\Transformers\CRUDTransformer;
use App\Api\V1\Validators\SysBugValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;

class CRUDController extends AbstractController
{
    protected $sysBugModel;
    protected $sysBugTransformer;
    protected $sysBugValidator;

    public function __construct()
    {
        $this->sysBugModel = new SysBugModel();
        $this->sysBugTransformer = new CRUDTransformer();
        $this->sysBugValidator = new SysBugValidator();
    }

    /**
     * @return \Dingo\Api\Http\Response|void
     */
    public function getInfo()
    {
        try {
            //get all row on table sys_bugs
            $sysBug = $this->sysBugModel->getAllSysBug();

            //return content through off sys bug transformer
            return $this->response->item($sysBug, $this->sysBugTransformer);
        } catch (\Exception $e) {
            //catch errors
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function storeInfo(Request $request)
    {
        //get data from HTTP
        $input = $request->getParsedBody();

        //validator
        $this->sysBugValidator->validate($input);

        // values to store
        $params = [
            'date' => array_get($input, 'date', null),
            'api_name' => array_get($input, 'api_name', ''),
            'error' => array_get($input, 'error', ''),
        ];

        // store data and check it status
        try {
            if ($results = $this->sysBugModel->storeSysBug($params)) {
                return $this->response->item($results, $this->sysBugTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest("Errors messages !");
    }

    /**
     * @param $sysBugId
     * @param Request $request
     * @return \Dingo\Api\Http\Response|void
     */
    public function updateInfo($sysBugId, Request $request)
    {

        //get data from HTTP
        $input = $request->getParsedBody();

        //validator
        $this->sysBugValidator->validate($input);

        // values to update
        $params = [
            'id' => $sysBugId,
            'date' => array_get($input, 'date', 0),
            'api_name' => array_get($input, 'api_name', ''),
            'error' => array_get($input, 'error', ''),
        ];

        //update data
        try {
            if ($results = $this->sysBugModel->updateSysBug($params)) {
                return $this->response->item($results, $this->sysBugTransformer);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $sysBugID
     * @return \Dingo\Api\Http\Response|void
     */
    public function deleteInfo($sysBugID)
    {
        try {
            if ($results = $this->sysBugModel->deleteSysBug($sysBugID)) {
                return $this->response->item($results, $this->sysBugTransformer);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
