<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CustomerMetaModel;
use App\BillableDtl;
use League\Fractal\TransformerAbstract;

class BillableTransformer extends TransformerAbstract
{

    /**
     * ps:Show SAC
     *
     * @param BillalbeDtl $sac
     *
     * @return array
     */
    public function transform(BillableDtl $billableDtl)
    {
        $cusPo  = object_get($billableDtl, 'orderHdr.cus_po', '');
        $refNum = object_get($billableDtl, 'header.ref_num', '');

        return [
            "ba_dtl_id"    => $billableDtl->ba_dtl_id,
            "ba_id"        => $billableDtl->ba_id,
            "cus_id"       => $billableDtl->cus_id,
            "whs_id"       => $billableDtl->whs_id,
            "sac_id"       => $billableDtl->sac_id,
            "sac"          => $billableDtl->sac,
            "sac_name"     => $billableDtl->sac_name,
            "ref_num"      => $refNum,
            "qty"          => $billableDtl->qty,
            "ba_type"      => $billableDtl->ba_type,
            "unit_price"   => $billableDtl->unit_price,
            "discount"     => $billableDtl->discount,
            "amount"       => $billableDtl->amount,
            "created_at"   => $billableDtl->created_at->format('m/d/Y'),
            "updated_at"   => $billableDtl->updated_at->format('m/d/Y'),
            "created_by"   => $billableDtl->created_name,
            "updated_by"   => $billableDtl->updated_by,
            'currency'     => (new CustomerMetaModel())->getCurrencyByCurrentWhs($billableDtl->cus_id),
            'unit_uom'     => $billableDtl->sac_unit_uom_name,
            'item'         => $billableDtl->item,
            'description'  => $billableDtl->description,
            'inv_num'      => $billableDtl->inv_num,
            'inv_dt'       => $billableDtl->inv_dt_format,
            'cus_po'       => $cusPo
        ];
    }
}
