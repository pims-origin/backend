<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LocationModel;
use App\Api\V1\Transformers\ItemLocationTransformer;
use App\Api\V1\Transformers\LocationTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;

class LocationController extends AbstractController
{

    protected $loc;

    /**
     * LocationController constructor.
     */
    public function __construct()
    {
        $this->loc = new LocationModel();
    }

    /**
     * @param Request $request
     * @param LocationTransformer $locationTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function listMzLocation(Request $request, LocationTransformer $locationTransformer)
    {
        $input = $request->getQueryParams();

        try {
            $mzLoc = $this->loc->listMzLocation($input, array_get($input, 'limit', 20));

            return $this->response->paginator($mzLoc, $locationTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param LocationTransformer $locationTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function listActMzLocation(Request $request, LocationTransformer $locationTransformer)
    {
        $input = $request->getQueryParams();

        try {
            $mzLoc = $this->loc->listActMzLocation($input, ['locationType'],
                array_get($input, 'limit', 20));

            return $this->response->paginator($mzLoc, $locationTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param ItemLocationTransformer $itemLocationTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function listItemLocation(Request $request, ItemLocationTransformer $itemLocationTransformer)
    {
        $input = $request->getQueryParams();

        try {
            $itemLoc = $this->loc->listLocationforItem($input, array_get($input, 'limit', 20));

            return $this->response->paginator($itemLoc, $itemLocationTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
