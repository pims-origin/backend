<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\AvailableInventory;
use Seldat\Wms2\Utils\SelArr;

class AvailableInventoryModel extends AbstractModel
{
    public function __construct(AvailableInventory $model = null)
    {
        $this->model = ($model) ?: new AvailableInventory();
    }

    /*
    ***************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with)
            ->where('cartons.whs_id', $attributes['whs_id']);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $attributes['compare_operator']['ctnr_num'] = 'like';
        $attributes['compare_operator']['upc'] = 'like';
        $attributes['compare_operator']['sku'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'cus_id',
                'whs_id',
                'ctnr_num',
                'item_code',
                'sku',
            ],
            'attributes' => $attributes,
            'tablePreface' => [
                'cus_id' => NULL,
                'sku' => 'item',
            ],
        ]);

        $query->join('item AS item', 'item.item_id', '=', 'cartons.item_id');

        if (isset($attributes['autocomplete'])) {

            $autocomplete = $attributes['autocomplete'];

            $attributes['autocomplete'] = $autocomplete == 'sku' ?
                    'cartons.' . $autocomplete : $autocomplete;
        } else {
            $query->groupBy([
                'cartons.cus_id',
                'ctnr_ID',
                'cartons.item_id',
                'cartons.po',
                'cartons.lot',
                'cartons.length',
                'cartons.width',
                'cartons.height',
                'cartons.weight',
            ]);
        }

        return $this->runSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

    /*
    ***************************************************************************
    */

}
