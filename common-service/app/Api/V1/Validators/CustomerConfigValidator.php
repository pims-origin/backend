<?php
/**
 * Created by PhpStorm.
 * User: phuong Hong
 * Date: 26-May-16
 * Time: 11:41
 */

namespace App\Api\V1\Validators;


class CustomerConfigValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_id'       => 'required|integer',
            'whs_id'       => 'required|integer',
            'config_name'  => 'required',
            'config_value' => 'required',
            'ac'           => 'required|string',
        ];
    }
}
