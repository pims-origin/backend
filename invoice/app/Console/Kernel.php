<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\TestDemo'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //populating receiving summary - rcv_sum TABLE
        $schedule->call('App\Api\V1\Models\PopulateSummaryModel@getReceivingSummary')
                 ->dailyAt('1:00');
        
        //populating receiving charge codes to invoice_summary table
        $schedule->call('App\Api\V1\Models\ReceivingMasterSummaryModel@receivingChargeCodes')
                 ->dailyAt('1:30');
        
        //populating order processing charge codes to invoice_summary table
        $schedule->call('App\Api\V1\Models\OrderProcessingMasterSummaryModel@orderProcessingChargeCodes')
                  ->dailyAt('2:00');

        //Regular storage summary code - populating storage charge codes to invoice_summary table 
        $schedule->call('App\Api\V1\Models\StorageMasterSummaryModel@storageChargeCodes')
                 ->hourly();

        //Migrating storage summary code - populating storage charge codes to invoice_summary table 
        $schedule->call('App\Api\V1\Models\MigrateStorageSummaryModel@storageChargeCodes')
                 ->everyFiveMinutes();
    }
}
