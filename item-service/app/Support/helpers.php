<?php

/**
 * Generate random token based on app key
 *
 * @return string
 */
function randomToken()
{
    return sha1(time() . env('APP_KEY'));
}
