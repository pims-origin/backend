<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 20/10/2016
 * Time: 14:13
 */

namespace App\Api\V1\Models;

use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;

class CycleDescrepancy extends AbstractModel
{
    /**
     * @var CycleHdrModel
     */
    protected $cycleHdrModel;

    /**
     * @var CycleDtlModel
     */
    protected $cycleDtlModel;

    public function __construct(CycleHdrModel $cycleHdrModel, CycleDtlModel $cycleDtlModel)
    {
        $this->cycleHdrModel = $cycleHdrModel;
        $this->cycleDtlModel = $cycleDtlModel;
    }

}