<?php

namespace App\Api\V1\Models;

use App\Api\V1\Services\CycleCountService;
use Seldat\Wms2\Models\BlockDtl;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\BlockHdr;
use Seldat\Wms2\Models\CustomerWarehouse;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Warehouse;
use Seldat\Wms2\Models\CusInUserModel;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\Paginator;
use Illuminate\Support\Facades\DB;
use App\Api\V1\Validators\Block;
use Wms2\UserInfo\Data;

class BlockHdrModel extends AbstractModel
{
    private $arrNumberFields = [];

    private $arrStringFields = [];

    const STATUS_BLOCKED = 'LK';
    const STATUS_UNBLOCKED = 'UL';

    const STATUS_INSERT = 'I';
    const STATUS_UPDATE = 'U';
    const STATUS_DELETE = 'D';

    const BLOCK_TYPE_CUSTOMER = 'CS';
    const BLOCK_TYPE_SKU = 'SK';
    const BLOCK_TYPE_LOCATION = 'LC';
    const BLOCK_TYPE_ITEM_ID = 'IT';

    public static $arrBlockStatus = [
        self::STATUS_BLOCKED   => 'Locked',
        self::STATUS_UNBLOCKED => 'Unblock',
    ];

    public $errors = [];
    public $waring = [];
    protected $locIdsRange = [];
    protected $blockDtlData = [];

    public static $arrBlockType = [
        self::BLOCK_TYPE_CUSTOMER => 'Customer',
        self::BLOCK_TYPE_SKU      => 'Sku',
        self::BLOCK_TYPE_LOCATION => 'Location',
        self::BLOCK_TYPE_ITEM_ID  => 'Item',
    ];

    private $blockTypeArr = ['LC', 'CS', 'SK', 'IT'];

    const CREATE_BLOCK_STOCK = 'CRA';
    const COMPLETE_BLOCK_STOCK = 'CYD';
    const CREATE_BLOCK_REASON = 'CYR';

    public static $blockEvtTracking = [
        self::CREATE_BLOCK_STOCK   => 'Create Block Stock',
        self::COMPLETE_BLOCK_STOCK => 'Complete Block Stock',
        self::CREATE_BLOCK_REASON  => 'Create Block Reason',
    ];

    /**
     * BlockHdrModel constructor.
     *
     * @param BlockHdr|NULL $model
     */
    public function __construct(BlockHdr $model)
    {
        $this->model = $model;
    }

    /**
     * @param $blockId
     *
     * @return bool
     */
    public function deleteCycleHdr($blockId)
    {
        if (!$blockId) {
            return false;
        }

        return $this->model
            ->where('block_hdr_id', $blockId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */

    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes)) {

            foreach ($attributes as $key => $value) {
                if (in_array($key, $this->arrNumberFields)) {
                    $query->where($key, $value);
                } elseif (in_array($key, $this->arrStringFields)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $this->model->filterData($query, true, true);
        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $input
     *
     * @return bool
     */

    public function processCreateBlockHdr($input)
    {
        $validate = $this->validateCreateBlockHdr($input);

        if (!$validate) {
            return false;
        }


        $evt = new EventTrackingModel();

        //generate event code
        $result = $evt->generateBlockNum();
        $input['block_num'] = $result['block_num'];
        $input['block_seq'] = $result['block_seq'];

        $itemIds = array_column($this->blockDtlData, 'item_id');
        $locIds = array_column($this->blockDtlData, 'loc_id');
        $lockDetail = $input['block_type'] == self::BLOCK_TYPE_LOCATION ?
            $this->locIdsRange : $input['block_detail'];

        DB::beginTransaction();

        try {

            $model = $this->model->create([
                'block_rsn_id' => $input['block_rsn_id'],
                'whs_id'       => $input['whs_id'],
                'block_num'    => $input['block_num'],
                'block_type'   => $input['block_type'],
                'block_detail' => implode(',', (array)$lockDetail),
                'block_sts'    => self::STATUS_BLOCKED,
                'sts'          => self::STATUS_INSERT
            ]);

            $this->createBlockDtls($model, $this->blockDtlData);;

            if ($input['block_type'] == self::BLOCK_TYPE_LOCATION) {
                // update cartons status
                DB::table('cartons')
                    ->whereIn('loc_id', $this->locIdsRange)
                    ->where('whs_id', $input['whs_id'])
                    ->where('ctn_sts', 'AC')
                    ->where('deleted', 0)
                    ->update(['ctn_sts' => self::STATUS_BLOCKED]);
            }

            $this->processLockData($model, $itemIds, $locIds);

            $this->createEventTracking($input);

            DB::commit();

            return $model;

        } catch (Exception $e) {

            DB::rollback();
            throw HttpException(400, 'There is some error when load data, please contact admin !');
        }
    }

    /**
     * @param $data
     *
     * @return bool
     */

    public function validateCreateBlockHdr(&$data)
    {
        $results = $this->validateInputCreateBlockHdr($data);

        if (!$results) {
            return false;
        }

        $itemModel = new ItemModel();

        $blockDetail = $data['block_type'] == self::BLOCK_TYPE_LOCATION ?
            $this->locIdsRange : $data['block_detail'];

        $loccationLocked = LocationModel::getLocationLockByBlockType(
            $data['whs_id'],
            $data['block_type'],
            $blockDetail
        );

        if ($loccationLocked) {
            $this->errors['sku'] = 'This Block Stock can not create. Locations locked: ' . implode(',',
                    $loccationLocked);

            return false;
        }

        $blockDtls = $this->getBlockData(
            $data['whs_id'],
            $data['block_type'],
            $blockDetail
        );

        if (!$blockDtls && $data['block_type'] != 'LC') {
            $this->errors['cus_id'] = $this->getErrorMsgWithNoInventory($data['block_type'], $blockDetail);

            return false;
        }
        //WMS2-4886 => can block location not belong to customer zone
        $itemIds = array_unique(array_column($blockDtls, 'item_id'));

        $itemsLock = $itemModel->getItemLocked($data['whs_id'], $itemIds);

        if ($itemsLock) {
            $this->errors[] = $itemModel->getErrorMsgWithSKUExist($itemsLock);

            return false;
        }

        $this->blockDtlData = $blockDtls;

        return true;
    }

    /**
     * @param $blockType
     * @param $blockDetail
     *
     * @return string
     */

    private function getErrorMsgWithNoInventory($blockType, $blockDetail)
    {
        $tmpError = 'This customer';

        switch ($blockType) {
            case self::BLOCK_TYPE_SKU:
                $tmpError = count($blockDetail) > 1 ? 'SKUs are' : 'SKU is';
                break;
            case self::BLOCK_TYPE_ITEM_ID:
                $tmpError = 'Item is';
                break;
        }

        $result = sprintf("%s out of Inventory or is not in Rack for Block Stock", $tmpError);

        return $result;
    }

    /**
     * @param $whsId
     * @param $blockType
     * @param $blockDetail
     *
     * @return mixed
     */

    public function getBlockData($whsId, $blockType, $blockDetail)
    {
        $status = LockModel::STATUS_ACTIVE;
        $field = $this->getBlockFieldByType($blockType);
        $locTypeRacId = LockModel::getLocTypeId();
        if ($blockType == 'LC') {
            return Location::select(
                'location.loc_id',
                'location.loc_code',
                'location.loc_alternative_name as loc_name',
                'location.loc_whs_id as whs_id',
                'customer_zone.cus_id'
            )
                ->leftJoin('customer_zone', function($q){
                    $q->on('location.loc_zone_id', '=', 'customer_zone.zone_id')
                        ->where('customer_zone.deleted', '=',0);
                })
                ->where('location.loc_sts_code', $status)
                ->where('location.loc_type_id', $locTypeRacId)
                ->whereIn('loc_id', $blockDetail)
                ->get()
                ->toArray();
        } else {


            return Carton::select(
                'cartons.whs_id',
                'cartons.cus_id',
                'cartons.item_id',
                'cartons.sku',
                'cartons.size',
                'cartons.color',
                'cartons.lot',
                'cartons.loc_id',
                'cartons.loc_code',
                'cartons.loc_name'
            )
                ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
                ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
                ->where('location.loc_sts_code', $status)
                ->where('cartons.ctn_sts', $status)
                ->where('location.loc_type_id', $locTypeRacId)
                ->where('cartons.whs_id', $whsId)
                ->whereIn($field, $blockDetail)
                ->groupBy(
                    'cartons.whs_id',
                    'cartons.cus_id',
                    'cartons.item_id',
                    'cartons.loc_id'
                )
                ->get()
                ->toArray();
        }
    }

    /**
     * @param $blockType
     *
     * @return string
     */

    public function getBlockFieldByType($blockType)
    {
        switch ($blockType) {
            case self::BLOCK_TYPE_CUSTOMER:
                $field = 'cartons.cus_id';
                break;
            case self::BLOCK_TYPE_SKU:
                $field = 'cartons.sku';
                break;
            case self::BLOCK_TYPE_ITEM_ID:
                $field = 'cartons.item_id';
                break;
            default:
                $field = 'cartons.loc_id';
                break;
        }

        return $field;
    }

    /**
     * @param $blockHdr
     * @param $data
     *
     * @return bool
     */

    private function createBlockDtls($blockHdr, $data)
    {
        if (!$data) {
            return false;
        }

        $blockDtl = new BlockDtl();

        foreach ($data as $blockItem) {
            $blockItem['block_hdr_id'] = $blockHdr->block_hdr_id;
            $blockItem['block_dtl_sts'] = BlockDtlModel::STATUS_ITEM_BLOCKED;
            $blockItem['sts'] = self::STATUS_INSERT;

            $blockDtl->create($blockItem);
        }

        return true;
    }

    /**
     * @param $blockHdr
     * @param $itemIds
     * @param $locIds
     *
     * @return bool
     */

    public function processLockData($blockHdr, $itemIds, $locIds)
    {


        if (!($blockHdr && $locIds)) {
            return false;
        }

        switch ($blockHdr->block_type) {
            case self::BLOCK_TYPE_CUSTOMER:
                //lock carton
                $locIds = LocationModel::getLocationIdsByCustomer($blockHdr->whs_id, $blockHdr->block_detail);
                break;
            case self::BLOCK_TYPE_LOCATION:

                $locIds = explode(',', $blockHdr->block_detail);
                break;
            case self::BLOCK_TYPE_SKU:
            case self::BLOCK_TYPE_ITEM_ID:
                break;
        }

        LockModel::lockCartonsByCycleHdr($blockHdr->whs_id, $itemIds, $locIds);
        LockModel::lockLocationByIds($blockHdr->whs_id, $locIds);
        LockModel::updateLockQtyInInventorySummary($blockHdr->whs_id, $itemIds, $locIds);
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function validateInputCreateBlockHdr(&$data)
    {
        if (!$data) {
            return false;
        }

        $warehouseInfo = $this->checkExitsWarehouse($data['whs_id']);

        if (!$data['whs_id']) {
            $this->errors['whs_id'] = 'Warehouse ID by empty.';
        } else if (!$warehouseInfo) {
            $this->errors['whs_id'] = 'Warehouse does not exist.';
        }

        $data['whs_name'] = $warehouseInfo['whs_name'];

        if (!$data['block_detail']) {
            $this->errors['block_detail'] = 'Block detail be empty';
        } else {
            $data['block_detail'] = array_map('trim', explode(',', $data['block_detail']));
        }

        if (!$data['block_type']) {
            $this->errors['block_type'] = 'Block type be empty';
        } else if (!in_array(strtoupper($data['block_type']), $this->blockTypeArr)) {
            $this->errors['block_type'] = 'Block type invalid';
        } else {
            $data['block_type'] = strtoupper($data['block_type']);
            $this->validateDataByType($data);
        }

        return $this->errors ? false : true;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function validateDataByType($data)
    {
        $result = false;

        switch (strtoupper($data['block_type'])) {
            case self::BLOCK_TYPE_CUSTOMER:
                $result = $this->checkValidCusInput($data);
                break;
            case self::BLOCK_TYPE_SKU:
                $result = $this->checkValidSKUInput($data);
                break;
            case self::BLOCK_TYPE_ITEM_ID:
                $result = $this->checkValidItemInput($data);;
                break;
            case self::BLOCK_TYPE_LOCATION:
                $result = $this->checkValidLocIdsInput($data);
                break;
        }

        return $result;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function checkValidItemInput($data)
    {
        $itemId = isset($data['block_detail']) ? $data['block_detail'] : null;
        $whsId = isset($data['whs_id']) ? $data['whs_id'] : null;

        if (!($itemId && $whsId)) {
            $this->errors['itemId'] = Message::get('BM081', 'Data');

            return false;
        }

        $itemModel = new ItemModel();
        $itemInfo = $itemModel->getItemInfo($itemId);


        if (!$itemInfo) {
            $this->errors['itemId'] = 'ItemID: ' . array_get($itemId, '0', '') . ' invalid';

            return false;
        }
        $itemWhsIds = array_pluck($itemInfo, 'whs_id');

        if (!in_array($whsId, $itemWhsIds)) {
            $this->errors['sku'] = 'ItemID: ' . array_get($itemInfo, 'item_id',
                    '') . ' has not exists on warehouse : ' . $whsId;

            return false;
        }

        return true;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function checkValidSKUInput($data)
    {
        $skus = isset($data['block_detail']) ? $data['block_detail'] : null;
        $whsId = isset($data['whs_id']) ? $data['whs_id'] : null;

        if (!($skus && $whsId)) {
            $this->errors['sku'] = Message::get('BM081', 'Data');

            return false;
        }

        $skus = array_unique($skus);

        $data = ItemModel::getItemBySKUs($skus);

        //check sku valid on database
        $skuValids = array_column($data->toArray(), 'sku');

        //upper case before diff
        $skus = array_map('strtoupper', $skus);
        $skuValids = array_map('strtoupper', $skuValids);

        $skuInValid = array_diff($skus, $skuValids);

        if ($skuInValid) {
            $this->errors['sku'] = Message::get('BM089', 'SKU', implode(',', $skuInValid));

            return false;
        }

        //get SKU valid in warehouse
        $items = ItemModel::getInfoItemBySkus($skus);
        $skuValids = ItemModel::getSKUInWarehouse($whsId, $items);
        $skuNotInWarehouse = array_diff($skus, array_keys($skuValids));

        if ($skuNotInWarehouse) {
            $this->errors['sku'] = Message::get('BM091', implode(',', $skuNotInWarehouse));

            return false;
        }

        return true;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function checkValidLocIdsInput($data)
    {
        $locIds = isset($data['block_detail']) ? $data['block_detail'] : null;
        $whsId = isset($data['whs_id']) ? $data['whs_id'] : null;

        if (!$locIds || !$whsId) {
            $this->errors['locId'] = 'Location invalid.';

            return false;
        }

        if(is_array($locIds)){
            $locIds = array_map('intval',$locIds);
        }

        $locInfos       = LocationModel::getLocationByLocIds($locIds);
        if($locInfos->isEmpty()){
            if($locations = LocationModel::getLocationByLocIdsWithTrashed($locIds)){
                $this->errors['locId'] = Message::get('BM094', $locations->pluck('loc_code')->implode(', '));
                return false;
            }
            $this->errors['locId'] = 'Location invalid.';
            return false;
        }

        $locCodeValid   = $locInfos->pluck('loc_id');
        if (($locCodeValid->count() <=> count($locIds)) !== 0) {
            $locInValid = array_diff($locIds, $locCodeValid->toArray());
            if($locations = LocationModel::getLocationByLocIdsWithTrashed($locInValid)){
                $this->errors['locId'] = Message::get('BM094', $locations->pluck('loc_code')->implode(', '));
                return false;
            }
            $this->errors['locId'] = 'Location invalid.';
            return false;
        }

        $this->locIdsRange = $locIds;

        return true;
    }

    /**
     * @param $whsId
     *
     * @return mixed
     */

    public function checkExitsWarehouse($whsId)
    {
        return Warehouse::where('whs_id', $whsId)->first();
    }

    /**
     * @param $data
     *
     * @return mixed
     */

    public function checkDataHaveLockFunction($data)
    {
        return Warehouse::where('whs_id', $data)->first();
    }

    /**
     * @param $data
     *
     * @return bool
     */

    public function checkValidCusInput($data)
    {
        $cusIds = isset($data['block_detail']) ? $data['block_detail'] : null;
        $whsId = isset($data['whs_id']) ? $data['whs_id'] : null;
        $customerModel = new CustomerModel();
        //get customer by cusId
        $customer = $customerModel->getCustomerInfo($cusIds);

        if ($customer->isEmpty()) {
            $this->errors['cus_id'] = Message::get('BM089', 'Customer', implode(',', $cusIds));

            return false;
        }

        $warehouseCus = $customerModel->getWarehouseHasCusIds($whsId, $cusIds);

        if ($warehouseCus->isEmpty()) {
            $this->errors['cus_id'] = Message::get('BM090', implode(',', $cusIds));

            return false;
        }

        $cusIdValid = array_column($warehouseCus->toArray(), 'cus_id');
        $cusInvalid = array_diff($cusIds, $cusIdValid);

        // exists cusId invalid
        if ($cusInvalid) {
            $this->errors['cus_id'] = Message::get('BM090', implode(',', $cusInvalid));

            return false;
        }

        $isUserHasCus = $this->checkUserInCus([
            'cus_id'  => $cusIds,
            'user_id' => CycleCountService::getUserId()
        ]);

        if (!$isUserHasCus) {
            $this->errors['cus_id'] = Message::get('BM076');

            return false;
        }

        return true;
    }

    /**
     * @param $data
     *
     * @return bool
     */

    public function checkUserInCus($data)
    {
        $userId = $data['user_id'];
        $cusIds = $data['cus_id'];

        $result = DB::table('cus_in_user')
            ->whereIn('cus_id', $cusIds)
            ->where('user_id', $userId)
            ->count();

        return $result > 0 ? true : false;
    }

    /**
     * @param $blockHdrId
     */
    public function getBlockStockInfo($blockHdrId)
    {
        $data = [];

        if (!$blockHdrId) {
            return [];
        }
        $query = $this->make(['blockReason']);
        $data = $query->where('block_hdr_id', $blockHdrId)->first();

        if (!$data) {
            return [];
        }

        $data->block_sts_name = self::$arrBlockStatus[$data->block_sts];
        $data->block_type_name = self::$arrBlockType[$data->block_type];
        $data->reason_name = object_get($data, 'blockReason.block_rsn_name', '');

        return $data;
    }

    /**
     * @param $data
     *
     * @return bool
     */

    public function createEventTracking($data)
    {
        $evt = new EventTrackingModel();

        $evt->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => isset($data['cus_id']) ? $data['cus_id'] : null,
            'owner'     => $data['block_num'],
            'evt_code'  => self::CREATE_BLOCK_STOCK,
            'trans_num' => $data['block_num'],
            'info'      => self::$blockEvtTracking[self::CREATE_BLOCK_STOCK]
        ]);
    }

    /**
     * @param $whsId
     *
     * @return array
     */
    private function _getCustomerInUser($whsId)
    {
        $ciuObj = (new CusInUserModel())->getModel()
                                        ->where([
                                            'user_id' => Data::getCurrentUserId(),
                                            'whs_id'  => $whsId,
                                            ])->get()->toArray();
        $ciu = array_column($ciuObj, "cus_id");

        return $ciu;
    }
}
