<?php

$middleware = ['app', 'user', 'cus', 'whs'];

$router->group([
    'prefix'        => '/rest/wms360/seldat/v1',
    'middleware'    => $middleware,
    'namespace'     => 'App\Modules\Inventories\Controllers'
], function () use ($router) {

    // Get list inventory of customer
    $router->get('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/inventory', [
        'uses' => 'InventoryController@index'
    ]);
});