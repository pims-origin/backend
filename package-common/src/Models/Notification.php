<?php
/**
 * Created by Duy Nguyen.
 * User: Duy Nguyen
 * Date: 02/12/2016
 * Time: 16:04
 */

namespace Seldat\Wms2\Models;

class Notification extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notification';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ntf_id';

    /**
     * @var array
     */
    protected $fillable = [
        'ntf_title',
        'ntf_desc',
        'ntf_start_date',
        'ntf_expired_date',
        'ntf_sts',
        'sts'
    ];

    /**
     * @return create by
     */
    public function createBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return update by
     */
    public function updateBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'user_id');
    }
}