<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\PackDtl;

/**
 * Class PackDtlModel
 *
 * @package App\Api\V1\Models
 */
class PackDtlModel extends AbstractModel
{
    /**
     * @param PackDtl $model
     */
    public function __construct(PackDtl $model = null)
    {
        $this->model = ($model) ?: new PackDtl();
    }

    /**
     * @param $packHdrIds
     *
     * @return mixed
     */
    public function countSku($packHdrIds)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->whereIn('pack_hdr_id', $packHdrIds)
            ->count(['item_id']);

        return $skuCount;
    }

}
