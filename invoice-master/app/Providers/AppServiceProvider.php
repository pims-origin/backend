<?php

namespace App\Providers;

use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Library\MySqlConnection;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Connection::resolverFor('mysql', function($connection, $database, $prefix, $config){
            return new MySqlConnection($connection, $database, $prefix, $config);

        });

        $this->app->singleton('mailer', function ($app) {
            return $app->loadComponent('mail', 'Illuminate\Mail\MailServiceProvider', 'mailer');
        });
        $this->app->alias('Mailer', \Illuminate\Contracts\Mail\Mailer::class);
        $this->app->instance('StatusCode', 'Illuminate\Http\Response');

        if(env('APP_ENV') === 'testing'){
            DB::connection()->enableQueryLog();
        }


    }
}
