<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use \Seldat\Wms2\Models\WorkOrderSku;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class WorkOrderSkuModel extends AbstractModel
{
    /**
     * @param WorkOrderSku $model
     */
    public function __construct(WorkOrderSku $model = null)
    {
        $this->model = ($model) ?: new WorkOrderSku();
    }

    /**
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['wo_hdr_id'])) {
            $query->where('wo_hdr_id', (int)$attributes['wo_hdr_id']);
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }

        $this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    public function getLikeSkuByOrderNum($cusId, $odrNum, $sku = null)
    {
        $usrInfo = new Data();
        $curWhs = $usrInfo->getCurrentWhs();

        DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('odr_hdr')
                ->select([
                    'odr_hdr.odr_id',
                    'odr_dtl.odr_dtl_id',
                    'item.item_id',
                    'item.sku',
                    'item.description',
                    'item.pack',
                    'item.size',
                    'item.color',
                    'item.length',
                    'item.width',
                    'item.height',
                    'item.weight',
                    'item.uom_code',
                    'item.uom_name',
                    'item.cus_upc as upc',
                ])
                ->leftJoin('odr_dtl', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
                ->join('item', 'odr_dtl.item_id', '=', 'item.item_id')
                ->where('odr_hdr.deleted', 0)
                ->where('odr_dtl.deleted', 0)
                ->where('item.deleted', 0)
                ->where('odr_dtl.odr_dtl_id', '!=', null)
                ->where('odr_hdr.cus_id', $cusId)
                ->where('odr_hdr.whs_id', $curWhs)
                ->where('odr_hdr.odr_num', $odrNum)
                ->where('item.sku', 'like', '%' . $sku . '%')
                ->groupBy('odr_dtl.sku', 'odr_dtl.size', 'odr_dtl.color');
        return $query->get();
    }

    public function getFirstByOldIdAndWOId($oldItemId, $workOrderId)
    {
        $query = $this->model
                    ->select(
                        'wo_sku_id',
                        'wo_sku.item_id',
                        'item.sku',
                        'item.description',
                        'item.pack',
                        'item.uom_code',
                        'item.uom_name',
                        'item.uom_id'
                    )
                    ->leftJoin('item', 'wo_sku.item_id', '=', 'item.item_id')
                    ->where('old_item_id', $oldItemId)
                    ->where('wo_hdr_id', $workOrderId);
        return $query->first();
    }
}
