<?php

namespace App\Modules\Orders\Models;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

abstract class AbstractModel
{
    protected $model;

    public function __construct($model = null)
    {
        $modelName = substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);
        $modelName = "\\App\\" . str_replace('Model', '', $modelName);

        $this->model = ($model) ?: new $modelName();
    }

    /**
     * Get empty model.
     *
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Refresh model to be clean
     */
    public function refreshModel()
    {
        $this->model = $this->model->newInstance();
    }

    /**
     * Get table name.
     *  //show all data in table
     *
     * @return string
     */
    public function getTable()
    {
        return $this->model->getTable();
    }

    /**
     * Find a single entity by key value.
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = [])
    {
        $query = $this->make($with);

        return $query->where($key, '=', $value)->first();
    }

    /**
     * Retrieve model by id
     * regardless of status.
     *
     * @param int $id model ID
     * @param array $with
     *
     * @return Model
     */
    public function byId($id, array $with = [])
    {
        $query = $this->make($with)->where($this->model->getKeyName(), $id);
        $model = $query->firstOrFail();

        return $model;
    }

    /**
     * Get next model.
     *
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function next($model, array $with = [])
    {
        return $this->adjacent(1, $model, $with);
    }

    /**
     * Get prev model.
     *
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function prev($model, array $with = [])
    {
        return $this->adjacent(-1, $model, $with);
    }

    /**
     * Get prev model.
     *
     * @param int $direction
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function adjacent($direction, $model, array $with = [])
    {
        $currentModel = $model;
        $models = $this->all($with);

        foreach ($models as $key => $model) {
            if ($currentModel->{$this->model->getKeyName()} == $model->{$this->model->getKeyName()}) {
                $adjacentKey = $key + $direction;

                return isset($models[$adjacentKey]) ? $models[$adjacentKey] : null;
            }
        }
    }

    /**
     * Get paginated models.
     *
     * @param int $page Number of models per page
     * @param int $limit Results per page
     * @param array $with Eager load related models
     *
     * @return stdClass Object with $items && $totalItems for pagination
     */
    public function byPage($page = 1, $limit = 10, array $with = [])
    {
        $result = new stdClass();
        $result->page = $page;
        $result->limit = $limit;
        $result->totalItems = 0;
        $result->items = [];
        $query = $this->make($with);

        $totalItems = $query->count();
        $query->skip($limit * ($page - 1))
            ->take($limit);
        $models = $query->get();
        // Put items and totalItems in stdClass
        $result->totalItems = $totalItems;
        $result->items = $models->all();

        return $result;
    }

    /**
     * Get all models.
     *
     * @param array $with Eager load related models
     *
     * @return Collection
     */
    public function all(array $with = [])
    {
        $query = $this->make($with);

        // Get
        return $query->get();
    }

    /**
     * Get all models by key/value.
     *
     * @param string $key
     * @param string $value
     * @param array $with
     *
     * @return Collection
     */
    public function allBy($key, $value, array $with = [])
    {
        $query = $this->make($with);

        $query->where($key, $value);
        // Get
        $models = $query->get();

        return $models;
    }

    /**
     * Get latest models.
     *
     * @param int $number number of items to take
     * @param array $with array of related items
     * @param array $with array of columns return
     *
     * @return Collection
     */
    public function latest($number = 10, array $with = [], array $columns = null)
    {
        $query = $this->make($with);

        return $query->take($number)->get($columns);
    }

    /**
     * Get single model by Slug.
     *
     * @param string $slug slug
     * @param array $with related tables
     *
     * @return mixed
     */
    public function bySlug($slug, array $with = [])
    {
        $model = $this->make($with)
            ->where('slug', '=', $slug)
            ->firstOrFail();

        return $model;
    }

    /**
     * Return all results that have a required relationship.
     *
     * @param string $relation
     * @param array $with
     *
     * @return Collection
     */
    public function has($relation, array $with = [])
    {
        $entity = $this->make($with);

        return $entity->has($relation)->get();
    }

    /**
     * Create a new model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function create(array $data)
    {
        // Create the model
        $model = $this->model->fill($data);

        if ($model->save()) {

            return $model;
        }

        return false;
    }

    /**
     * Update an existing model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function update(array $data)
    {
        $model = $this->model->findOrFail($data[$this->model->getKeyName()]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    public function updateWhere($dataUpdated, $where)
    {
        $query = $this->make();
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query->where($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query->where($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query->where($field, '=', $search);
                }
            } else {
                $query->where($field, '=', $value);
            }
        }

        return $query->update($dataUpdated);
    }

    /**
     * Get One collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $orderBy
     * @param array $columns
     * @param bool $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getFirstWhere($where, array $with = [], array $orderBy = [], $columns = ['*'], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {

            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {

                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        foreach ($orderBy as $column => $sortType) {
            $query->orderBy($column, $sortType);
        }

        return $query->first($columns);
    }

    /**
     * Find a collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $orderBy
     * @param array $columns
     * @param bool $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function findWhere($where, array $with = [], array $orderBy = [], $columns = ['*'], $or = false)
    {
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        foreach ($orderBy as $column => $sortType) {
            $query->orderBy($column, $sortType);
        }

        return $query->get($columns);
    }

    /**
     * @param array $where
     * @param bool $or
     *
     * @return mixed
     */
    public function checkWhere($where, $or = false)
    {
        $query = $this->make([]);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        return $query->count();
    }

    /**
     * Make a new instance of the entity to query on.
     *
     * @param array $with
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    public function checkCusOfUser($whsId, $cusId)
    {
        $cusOfUser = Config::get('data.cusOfUser');

        foreach ($cusOfUser as $data) {
            if ($data->whs_id == $whsId && $data->cus_id == $cusId)
            {
                return true;
            }
        }

        return false;
    }
}