<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31/10/2016
 * Time: 17:10
 */

namespace App\Api\V1\Validators;

class CycleCountReasonValidator extends AbstractValidator
{

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'r_name' => 'required|unique:reason,r_name',
            // 'cc_rsn_des' => 'required'
        ];
    }
}