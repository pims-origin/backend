<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Validators;


class GoodsReceiptValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'ctnr_id' => 'required|integer',
            'asn_hdr_id' => 'required|integer',
        ];
    }
}
