<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\WarehousesystemCountry;
use App\WarehousesystemCountryType;
use App\systemCountryLocationStatus;
use App\SystemCountry;

class SystemCountryTest extends TestCase
{
    use DatabaseTransactions;

    protected $systemCountryParams = [
        'sys_country_code' => 'Code',
        'sys_country_name' => 'US'
    ];

    private function getEndPoint()
    {
        return "/v1/system-countries";
    }

    public function testStore_NoParam_Fail()
    {
        $endPoint = $this->getEndPoint();
        $this->call('POST', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_HasParam_Ok()
    {
        $response = $this->call('POST', $this->getEndPoint(), $this->systemCountryParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
        $response = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('sys_country_id', $response['data']);
        $this->assertArrayHasKey('sys_country_code', $response['data']);
        $this->assertArrayHasKey('sys_country_name', $response['data']);
    }

    public function testStore_DuplicateSystemCountryCode_Fail()
    {
        // make data
        $systemCountryCode = factory(SystemCountry::class)->create()->sys_country_code;

        $this->systemCountryParams['sys_country_code'] = $systemCountryCode;

        $this->call('POST', $this->getEndPoint(), $this->systemCountryParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testShow_Ok()
    {
        $systemCountryId = factory(SystemCountry::class)->create()->sys_country_id;
        $endPoint = $this->getEndPoint() . '/' . $systemCountryId;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testShow_SystemCountryNotExisted_Fails()
    {
        $endPoint = $this->getEndPoint() . '/999999999';
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testUpdate_Ok()
    {
        // make data
        $systemCountry = factory(SystemCountry::class)->create();

        $endPoint = $this->getEndPoint() . '/' . $systemCountry->sys_country_id;

        $this->systemCountryParams['sys_country_code'] = rand();
        $this->systemCountryParams['sys_country_name'] = rand();

        $response = $this->call('PUT', $endPoint, $this->systemCountryParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $response = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('sys_country_name', $response['data']);
        $this->assertArrayHasKey('sys_country_code', $response['data']);
        $this->assertArrayHasKey('sys_country_id', $response['data']);
    }

    public function testUpdate_DupliacteSystemCountryCode_Fail()
    {
        // make data
        $sysCountryCode = factory(SystemCountry::class)->create()->sys_country_code;
        $systemCountryId = factory(SystemCountry::class)->create()->sys_country_id;

        $this->systemCountryParams['sys_country_code'] = $sysCountryCode;

        $this->call('PUT', $this->getEndPoint() . '/' . $systemCountryId, $this->systemCountryParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDestroy_SingleItem_Ok()
    {
        $sysCountryId = factory(SystemCountry::class)->create()->sys_country_id;
        $endPoint = $this->getEndPoint() . '/' . $sysCountryId;

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDestroy_systemCountryIdNotExisted_Fail()
    {
        $endPoint = $this->getEndPoint() . '/999999';

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $sysCountries = factory(SystemCountry::class, 3)->create();
        $params = [];
        foreach ($sysCountries as $key => $sysCountry) {
            $params['sys_country_id'][$key] = $sysCountry->sys_country_id;
        }
        $this->call('DELETE', $this->getEndPoint(), $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDeleteMass_NoParams_Fail()
    {
        $this->call('DELETE', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testSearch_NoParams_Fail()
    {
        $endPoint = $this->getEndPoint();
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_Ok()
    {
        $systemCountry = factory(SystemCountry::class)->create();

        $endPoint = $this->getEndPoint() . '?sys_country_id=' . $systemCountry->sys_country_id
            . '&sys_country_name=' . $systemCountry->sys_country_name
            . '&sys_country_code=' . $systemCountry->sys_country_code;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }
}
