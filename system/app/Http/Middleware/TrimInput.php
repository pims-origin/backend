<?php

namespace App\Http\Middleware;

use Closure;

class TrimInput
{
    public function handle($request, Closure $next)
    {
        // Perform action
        $input = $request->all();

        $input = $this->trimArray($input);

        $request->merge($input);

        return $next($request);
    }

    /**
     * Trims a entire array recursivly.
     *
     * @param array $input
     *
     * @return array
     */
    function trimArray($input)
    {

        if (!is_array($input)) {
            return trim($input);
        }

        return array_map([$this, 'trimArray'], $input);
    }
}