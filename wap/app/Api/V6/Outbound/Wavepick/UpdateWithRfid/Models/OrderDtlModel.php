<?php

namespace App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\Status;

class OrderDtlModel extends AbstractModel
{

    protected $model;

    /**
     * OrderDtlModel constructor.
     *
     * @param OrderDtl|null $model
     */
    public function __construct(OrderDtl $model = null)
    {
        $this->model = ($model) ?: new OrderDtl();
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getListOrderDtlInfo($odr_ids)
    {
        return $this->model
            ->whereIn('odr_id', $odr_ids)
            ->get();
    }

    /**
     * @param $itm_id
     * @param $wv_id
     *
     * @return mixed
     */
    public function getOrderDtlOfWavePick($itm_id, $wv_id)
    {
        return $this->model
            ->where('item_id', $itm_id)
            ->where('wv_id', $wv_id)
            ->first();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateOrtDtl($data)
    {
        return $this->model->where('odr_id', '=', $data['odr_id'])
            ->update([
                'wv_id'  => $data['wv_id'],
            ]);
    }

    public function getListOrderDtlOfWavePick($wv_id)
    {
        return $this->model
            ->leftJoin('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->leftJoin('wv_dtl', 'wv_dtl.wv_id', '=', 'odr_hdr.wv_id')
            ->where('odr_dtl.wv_id', $wv_id)
            ->get();
    }

    public function updatePDOdrDtl($wvHdrId)
    {
        $result = DB::table('odr_dtl as o')
            ->where('o.wv_id', $wvHdrId)
            ->where('o.alloc_qty',
                DB::raw('(SELECT SUM(oc.piece_qty) FROM odr_cartons oc WHERE oc.odr_dtl_id = o.odr_dtl_id)')
            )
            ->update(['o.itm_sts' => Status::getByValue('Picked', 'ORDER-STATUS')]);

        return $result;
    }

    public function getOdrDtlInfoByOdrDtlId($odrDtlId, $whsId)
    {
        return $this->model
            ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->where('odr_dtl_id', $odrDtlId)
            ->where('odr_dtl.whs_id', $whsId)
            ->first();
    }

    public function updatePDOdrDtlStatus($odrDtlId)
    {
        $result = DB::table('odr_dtl as o')
            ->where('o.odr_dtl_id', $odrDtlId)
            ->where('o.alloc_qty',
                DB::raw('(SELECT SUM(oc.piece_qty) FROM odr_cartons oc WHERE oc.odr_dtl_id = o.odr_dtl_id)')
            )
            ->update(['o.itm_sts' => Status::getByValue('Picked', 'ORDER-STATUS')]);

        return $result;
    }

    public function checkAllOdrDtlStsPicked($odrHdrId) {
        return $this->model
            ->where('odr_id', $odrHdrId)
            ->whereNotIn('itm_sts', [Status::getByValue('Picked', 'ORDER-STATUS')])
            ->first();
    }

    public function countSKUInOdr($odrID) {
        return $this->model
            ->where('odr_id', $odrID)
            /*->groupBy('sku')*/
            ->count();
    }

    public function countOdrDtlStsNotPD($odrID) {
        return $this->model
            ->where('odr_id', $odrID)
            ->where('itm_sts', '<>', Status::getByKey('ORDER-STATUS','Picked'))
            ->count();
    }

    public function updateOrderDetailPicked($odr_id)
    {
        $sql = "(SELECT SUM(odr_cartons.piece_qty) FROM odr_cartons WHERE odr_cartons.odr_dtl_id = odr_dtl.odr_dtl_id)";
        $result =$this->model
            ->where('odr_id', $odr_id)
            ->whereIn('itm_sts', ['PK', 'NW'])
            ->where('alloc_qty', DB::raw($sql))->update(["itm_sts" => 'PD']);
        return $result;

    }
    public function getTotalCarton($odrID)
    {
        return $this->model
            ->where('odr_id', $odrID)
            ->sum('qty')
            ;

    }

}
