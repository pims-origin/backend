<?php

namespace App\Api\V1\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\Invoice;
use Illuminate\Support\Facades\DB;

class ExampleController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function test(Request $request)
    {
        $start_time = Carbon::now()->startOfMonth()->timestamp;
        $end_time = Carbon::now()->endOfMonth()->timestamp;

        $data = DB::table('billable_hdr')
            ->join('billable_dtl', 'billable_dtl.ba_id', '=', 'billable_hdr.ba_id')
            ->leftJoin('warehouse', 'warehouse.whs_id', '=', 'billable_hdr.whs_id')
            ->leftJoin('customer', 'customer.cus_id', '=', 'billable_hdr.cus_id')
            ->leftJoin('cus_sac', 'cus_sac.sac_id', '=', 'billable_dtl.sac_id')
            ->whereIn('billable_hdr.ba_type', ['INB', 'OUB'])
            ->where('billable_hdr.deleted', 0)
            ->whereBetween('billable_hdr.created_at', [$start_time, $end_time])
            ->select([
                'customer.cus_name',
                'warehouse.whs_name',
                'billable_dtl.sac_name',
                'billable_dtl.ref_num',
                DB::raw('SUM(billable_dtl.qty) as total_qty'),
                'billable_dtl.ba_type',
                'billable_dtl.description',
                'cus_sac.unit_uom'
            ])
            ->groupBy('billable_dtl.whs_id')
            ->groupBy('billable_dtl.cus_id')
            ->groupBy('billable_dtl.sac')
            ->groupBy('billable_dtl.ref_num')
            ->orderBy('billable_hdr.created_at', 'DESC')
            ->get();

        if(count($data) == 0) {
            return ['message' => 'IMS Billable Storage, No data available this month'];
        }

        $data = ['data' => $data];

        return json_encode($data);
    }
}
