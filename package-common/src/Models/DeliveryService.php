<?php

namespace Seldat\Wms2\Models;

class DeliveryService extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'delivery_service';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ds_id';

    /**
     * @var array
     */
    protected $fillable = [
        'ds_key',
        'ds_value'
    ];
}
