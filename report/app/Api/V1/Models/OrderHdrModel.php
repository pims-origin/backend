<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\OpenOrder;

use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SelArr;

use Carbon\Carbon;

class OrderHdrModel extends AbstractModel
{
    public function __construct(OrderHdr $model = NULL)
    {
        $this->model = ($model) ?: new OrderHdr();
        $this->openOrderModel = ($model) ?: new OpenOrder();
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with)
            ->where('odr_hdr.whs_id', $attributes['whs_id']);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $orderType = array_get($searchAttr, 'odr_type');

        if ($orderType) {
           $searchAttr['odr_type'] = Status::getByValue($orderType, 'Order-Type');
        }

        $attributes['compare_operator']['cus_odr_num'] = 'like';
        $attributes['compare_operator']['odr_num'] = 'like';
        $attributes['compare_operator']['cus_po'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'cus_id',
                'odr_type',
                'cus_odr_num',
                'odr_num',
                'cus_po',
                'odr_req_dt',
            ],
            'attributes' => $attributes,
        ]);

        return $this->runSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

    /*
    ***************************************************************************
    */

    public function searchOpenOrders($attributes=[], $with=[], $limit=PAGING_LIMIT)
    {
        $query = $this->make($with)
            ->where('odr_hdr.whs_id', $attributes['whs_id']);

        $query->whereHas('customer', function ($query) {
            if (OPEN_ORDERS_EXCLUDE_CLIENTS) {
                $query->whereNotIn('cus_name', OPEN_ORDERS_EXCLUDE_CLIENTS);
            }
        });

        $fromTime = Carbon::createFromFormat(DATE_FROMAT . ' H', OPEN_ORDERS_FROM_DATE . ' 0')
            ->timestamp;
        $toTime = Carbon::today()->addDays(OPEN_ORDERS_DAYS_ADD)->addSeconds(-1)
            ->timestamp;
        $today = Carbon::today()->addDays(1)->addSeconds(-1)->timestamp;

        if (OPEN_ORDERS_FROM_DATE) {
            $query->whereBetween('odr_hdr.created_at', [$fromTime, $today]);
        } else {
            $query->where('odr_hdr.created_at', '<=', $today);
        }

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $query->with([
                'openOrder' => function($query) {
                    $query->select(
                        'ord_id',
                        'sts',
                        'created_at',
                        'created_by'
                    );
                },
                'details' => function($query) {
                    $query->select(
                        'odr_id',
                        DB::raw('COUNT(DISTINCT odr_dtl.sku) AS sku_count'),
                        DB::raw('SUM(qty) AS ctn_ttl'),
                        DB::raw('SUM(piece_qty) AS pcs_ttl')
                    )
                    ->with([
                        'orderHdr.packHdr' => function($query) {
                            $query->select(
                                'odr_hdr_id',
                                DB::raw('COUNT(DISTINCT pack_hdr.out_plt_id) AS plt_count')
                            )
                            ->groupBy('odr_hdr_id');
                        }
                    ])
                    ->groupBy('odr_id');
                },
            ])
            ->whereNotIn('odr_sts', [
                Status::getByValue('Canceled', 'ORDER-STATUS'),
                Status::getByValue('Shipped', 'ORDER-STATUS'),
            ])
            ->whereNotIn('odr_id', function($query) {
                $query->select('ord_id')
                    ->from('open_ord')
                    ->where('sts', '=', 'Closed');
            })
            ->where('cancel_by_dt', '<=', $toTime);

        $attributes['compare_operator']['cus_odr_num'] = 'like';
        $attributes['compare_operator']['odr_num'] = 'like';

        $cancelDate = array_get($searchAttr, 'cancel_by_dt', NULL);

        if ($cancelDate) {

            $cancelTime = Carbon::createFromFormat('Y-m-d H', $cancelDate . ' 0')
                    ->timestamp;

            if ($cancelTime <= $today) {
                $attributes['compare_operator']['cancel_by_dt'] = 'lessOrEqual';
            }
        }

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'cus_id',
                'cus_odr_num',
                'odr_num',
                'created_at',
                'cancel_by_dt',
            ],
            'attributes' => $attributes,
            'tablePreface' => [
                'cus_id' => NULL,
                'created_at' => NULL,
            ],
            'dateFields' => [
                'created_at',
            ],
        ]);

        if (isset($attributes['autocomplete'])) {
            return $this->searchAutocomplete($query, $attributes['autocomplete'],
                    $limit);
        } else {

            $this->sortBuilder($query, $searchAttr);

            $models = $query->paginate($limit, ['*'], 'page', $searchAttr['page']);

            return $models;
        }
    }

    /*
    ***************************************************************************
    */

    function updateOpenOrderStatus($orderID, $userID, $value)
    {
        $this->openOrderModel
            ->updateOrCreate(
                [
                    'ord_id' => $orderID,
                ],
                [
                    'ord_id' => $orderID,
                    'sts' => $value,
                    'created_at' => Carbon::now(TIME_ZONE)->timestamp,
                    'created_by' => $userID,
                ]
            );

        return $this->model;
    }

    /*
    ****************************************************************************
    */

    public function getForecastActualData($dateRange, $data) {
        return $this->model
            ->select([
                DB::raw('IFNULL(COUNT(*),0) as orders'),
                DB::raw("IFNULL(SUM(IF(odr_sts = 'SH', 1, 0)),0) as shipments"),
            ])
            ->whereBetween('shipped_dt', $dateRange)
            ->where('whs_id', $data['whs_id'])
            ->where('cus_id', $data['cus_id'])
            ->first();
    }

    public function getForecastOutboundDataV2($dateRange, $data){
        $orders = $this->model->where('whs_id', $data['whs_id'])
                            ->where('odr_hdr.odr_sts', 'SH')
                            ->where('odr_hdr.whs_id', $data['whs_id'])
                            ->where('odr_hdr.cus_id', $data['cus_id'])
                            ->whereBetween('odr_hdr.shipped_dt', $dateRange)
                            ->get();

        $data = [
            'out_shipment'  => 0,
            'out_order'     => 0
        ];

        $orders->each(function($order) use(&$data, $dateRange){
            if ( self::isShippedInRange($order, $dateRange) ){
                $data['out_shipment']++;
            }
            $data['out_order']++;
        });

        return $data;
    }

    public function getForecastOutboundData($dateRange, $data){
        $outbound = $this->model->select([
                    DB::raw('COUNT(pack_hdr.pack_hdr_num) AS out_carton'),
                    DB::raw('COUNT(DISTINCT(out_pallet.plt_num)) AS out_pallet')
                ])
                ->leftJoin('pack_hdr', function($join){
                    $join->on('odr_hdr.odr_id', '=', 'pack_hdr.odr_hdr_id')
                         ->where('pack_sts', '=', 'AS');
                })
                ->leftJoin('out_pallet', function($join){
                    $join->on('pack_hdr.out_plt_id', '=', 'out_pallet.plt_id')
                        ->where('out_plt_sts', '=', 'AC');
                })
                ->where('odr_hdr.odr_sts', 'SH')
                ->where('odr_hdr.whs_id', $data['whs_id'])
                ->where('odr_hdr.cus_id', $data['cus_id'])
                ->whereBetween('odr_hdr.shipped_dt', $dateRange)
                ->get()
                ->toArray();

        $outUnit = $this->model->select([
                    DB::raw('SUM(odr_dtl.picked_qty) AS out_unit')
                ])
                ->join('odr_dtl', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
                ->where('odr_hdr.odr_sts', 'SH')
                ->where('odr_hdr.whs_id', $data['whs_id'])
                ->where('odr_hdr.cus_id', $data['cus_id'])
                ->whereBetween('odr_hdr.shipped_dt', $dateRange)
                ->get()
                ->toArray();

        $outCube = DB::select('
                SELECT
                    SUM(cube * cartons_quantity) as out_cube
                FROM
                    (
                        SELECT
                            pack_hdr.item_id,
                            ( pack_hdr.length * pack_hdr.width * pack_hdr.height ) / 1728 AS cube,
                            COUNT(pack_hdr_id) AS cartons_quantity
                        FROM
                            `odr_hdr`
                        LEFT JOIN `pack_hdr` ON `odr_hdr`.`odr_id` = `pack_hdr`.`odr_hdr_id`
                        AND pack_sts = "AS"
                        WHERE odr_hdr.odr_sts = "SH"
                        AND  odr_hdr.deleted = 0
                        AND `odr_hdr`.`shipped_dt` BETWEEN ? AND ?
                        AND `odr_hdr`.`whs_id` = ?
                        AND `odr_hdr`.`cus_id` = ?
                        GROUP BY
                            pack_hdr.item_id
                    ) C;
            ', [
            $dateRange['fromDate'],
            $dateRange['toDate'],
            $data['whs_id'],
            $data['cus_id']
        ]);

        $outShipAndOrder[] = $this->getForecastOutboundDataV2($dateRange, $data);

        $outData = collect($outbound)
                ->merge($outUnit)
                ->merge($outCube)
                ->merge($outShipAndOrder)
                ->flatten(1);

        return $outData;
    }

    public static function isShippedInRange(OrderHdr $order, $dateRange){
        return $order->odr_sts === 'SH' &&
                $dateRange['fromDate'] <= $order->shipped_dt &&
                $order->shipped_dt <= $dateRange['toDate'];
    }
}
