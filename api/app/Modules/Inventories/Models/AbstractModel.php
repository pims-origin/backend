<?php

namespace App\Modules\Inventories\Models;

use Illuminate\Support\Facades\DB;
use Firebase\JWT\JWT;

abstract class AbstractModel
{
    public function __construct()
    {
        //
    }

    protected function getCurrentUserId()
    {
        $key = env("JWT_SECRET");
        $auth = app('request')->header('Authorization');
        $jwt = str_replace("Bearer ", "", $auth);
        define("FIREBASE_PRIVATE_KEY",file_get_contents(base_path() . "/config/jwtRS256.key" ));
        $pub_key = openssl_pkey_get_private(FIREBASE_PRIVATE_KEY);
        $keyData = openssl_pkey_get_details($pub_key);
        $decoded =  JWT::decode($jwt, $keyData['key'], array('RS256'));
        $userId = $decoded->sub;
        
        return $userId;
    }
}