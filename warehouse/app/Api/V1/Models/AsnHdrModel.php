<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;


class AsnHdrModel extends AbstractModel
{

    const DAS_LIMIT = 0;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnHdr();
    }

    /**
     * @return mixed
     */
    public function asnDashboard($input = [])
    {
        $asnSts = [
            'RG' => [Status::getByKey("ASN_STATUS", 'RECEIVING')],
            'RE' => [Status::getByKey("ASN_STATUS", 'RECEIVED')],
            'NW' => [Status::getByKey("ASN_STATUS", 'NEW')],
        ];

        foreach ($asnSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $asnSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId =  array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis->getCustomersByWhs($currentWH);

        $query = DB::table('asn_hdr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['NW']}) 
                    THEN 1 ELSE 0 END), 0) AS newAsn,
	            COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['RE']}) 
	                THEN 1 ELSE 0 END), 0) AS received,
	            COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['RG']}) 
	                THEN 1 ELSE 0 END), 0) AS receiving
            "))
            ->where('deleted', 0)
            ->where('updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->whereIn('cus_id', $cus_ids)
            ->where('whs_id', $currentWH)
            ->where(function ($query) use ($userId) {
                $query->where('created_by', $userId)
                    ->orWhere('updated_by', $userId);
            });

        $this->model->filterData($query, true);

        $model = $query->first();
        return [
            'newAsn' => !empty($model['newAsn']) ? $model['newAsn'] : 0,
            'received' => !empty($model['received']) ? $model['received'] : 0,
            'receiving' => !empty($model['receiving']) ? $model['receiving'] : 0
        ];
    }

    /**
     * @return mixed
     */
    public function asnAdminDashboard($input = [])
    {
        $asnSts = [
            'RG' => [Status::getByKey("ASN_STATUS", 'RECEIVING')],
            'RE' => [Status::getByKey("ASN_STATUS", 'RECEIVED')],
            'NW' => [Status::getByKey("ASN_STATUS", 'NEW')],
        ];

        foreach ($asnSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $asnSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis->getCustomersByWhs($currentWH);

        $query = DB::table('asn_hdr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['NW']}) 
                    THEN 1 ELSE 0 END), 0) AS newAsn,
	            COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['RE']}) 
	                THEN 1 ELSE 0 END), 0) AS received,
	            COALESCE(SUM(CASE WHEN asn_sts IN ({$asnSts['RG']}) 
	                THEN 1 ELSE 0 END), 0) AS receiving
            "))
            ->where('deleted', 0)
            ->where('updated_at', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $limit . ' DAY)'))
            ->whereIn('cus_id', $cus_ids)
            ->where('whs_id', $currentWH);

        $this->model->filterData($query, true);

        $model = $query->first();
        return [
            'newAsn' => !empty($model['newAsn']) ? (int)$model['newAsn'] : 0,
            'received' => !empty($model['received']) ? (int)$model['received'] : 0,
            'receiving' => !empty($model['receiving']) ? (int)$model['receiving'] : 0
        ];
    }

    public function getAsnLate($cus_ids, $whsId){
        $count = $this->model->whereIn('cus_id', $cus_ids)
            ->where('deleted', 0)
            ->where('whs_id', $whsId)
            ->whereRaw('DATE(FROM_UNIXTIME(asn_hdr_ept_dt)) < DATE(NOW())')
            ->whereNotIn('asn_sts', ['RE', 'CC'])
            ->count();
        return $count;
    }

}
