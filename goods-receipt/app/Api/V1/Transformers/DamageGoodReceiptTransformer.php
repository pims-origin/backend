<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\AsnDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;

class DamageGoodReceiptTransformer extends TransformerAbstract
{

    /**
     * @param $sample
     *
     * @return array
     */
    public function transform($data)
    {

        return [
            'cus_id' => object_get($data, 'cus_id', null),
            'whs_id' => object_get($data, 'whs_id', null),
            'gr_hdr_num' => object_get($data, 'gr_hdr_num', null),
            'gr_in_note' => object_get($data, 'gr_in_note', null),
            'gr_hdr_id' => object_get($data, 'gr_hdr_id', null),
            'ctnr_num' => object_get($data, 'ctnr_num', null),
        ];
    }

}
