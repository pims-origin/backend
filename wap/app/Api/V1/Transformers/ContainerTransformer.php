<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Container;

class ContainerTransformer extends TransformerAbstract
{
    //return result
    public function transform(Container $container)
    {
        return [
            'ctnr_id' => $container->ctnr_id,
            'ctnr_num' => $container->ctnr_num,
        ];
    }

}
