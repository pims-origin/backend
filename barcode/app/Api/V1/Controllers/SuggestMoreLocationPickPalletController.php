<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\SuggestMoreLocationPickPalletModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\JWTUtil;

/**
 * Class WaveController
 *
 * @package App\Api\V1\Controllers
 */
class SuggestMoreLocationPickPalletController extends AbstractController
{

    /**
     * @var int|mixed
     */
    protected $userId;

    /**
     * @var SuggestMoreLocationPickPalletModel
     */
    protected $suggestMoreLocationPickPallet;

    /**
     * SuggestMoreLocationPickPalletController constructor.
     */
    public function __construct()
    {
        $this->suggestMoreLocationPickPallet = new SuggestMoreLocationPickPalletModel();

        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    public function moreSuggestLocation(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            if (!isset($input['wv_dtl_id'])) {
                $this->response->errorBadRequest('Wave detail cannot empty');
            }
            $wvDtl = WavepickDtl::where('wv_dtl_id', $input['wv_dtl_id'])->first();
            if (empty($wvDtl)) {
                $this->response->errorBadRequest('Wave detail is not existed');
            }

            if (!in_array($wvDtl->wv_dtl_sts, ['NW', 'PK'])) {
                $this->response->errorBadRequest('Wave detail is picked');
            }

            if ($wvDtl->pick_pallet == 1) {
                // Show list pallet
                $suggestLocation = $this->suggestMoreLocationPickPallet->pickPallet($wvDtl);

            } else {
                $pickFullPallet = array_get($input, 'full_pallet', false);

                $suggestLocation = $this->suggestMoreLocationPickPallet->getMoreSugLocByWvDtl($wvDtl, $pickFullPallet);
                if (count($suggestLocation) > 0) {
                    $suggestLocation = $this->suggestMoreLocationPickPallet->sortDeepLocationPicking($suggestLocation);
                }

            }

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}

