<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SysBug;

class SysBugModel extends AbstractModel
{
    /**
     * SysBug constructor.
     * @param SysBug|null $model
     */
    public function __construct(SysBug $model = null)
    {
        $this->model = ($model) ?: new  SysBug();
    }

    /**
     * @return mixed
     */
    public function getAllSysBug() {
        return $this->model->get();
    }

    /**
     * @param $params
     * @return static
     */
    public function storeSysBug($params) {
        return $this->model->create($params);
    }

    /**
     * @param $params
     * @return bool|int
     */
    public function updateSysBug($params) {
        $this->model->update($params);
    }
    
    /**
     * @param $sysBugId
     * @return mixed
     */
    public function deleteSysBug($sysBugId)
    {
        return $this->model->where('id', $sysBugId)->delete();
    }
}
