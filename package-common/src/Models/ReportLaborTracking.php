<?php

namespace Seldat\Wms2\Models;

class ReportLaborTracking extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rpt_labor_tracking';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'lt_id';

    public $timestamps = false;

    protected $fillable = [
        'lt_id',
        'user_id',
        'cus_id',
        'whs_id',
        'owner',
        'trans_num',
        'trans_dtl_id',
        'start_time',
        'end_time',
        'lt_type'
    ];

}
