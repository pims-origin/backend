<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/10/2016
 * Time: 4:00 PM
 */

namespace App\Api\V1\Traits;

use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

trait ChargeCodeSysUomService
{
    /**
     * Search ChargeType
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function _search(
        $attributes = [],
        $with = [],
        $limit = null,
        $keyword = ['chg_type_code', 'chg_type_name']
    ) {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $keyword)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }
        $this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }
}