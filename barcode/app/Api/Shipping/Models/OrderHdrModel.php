<?php
namespace App\Api\Shipping\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderHdr;

class OrderHdrModel extends AbstractModel
{

    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }

}
