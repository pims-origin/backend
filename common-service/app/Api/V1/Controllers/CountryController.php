<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Transformers\SystemCountryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use App\Api\V1\Validators\SystemCountryValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class CountryController
 *
 * @package App\Api\V1\Controllers
 */
class CountryController extends AbstractController
{

    /**
     * @var SystemStateModel
     */
    protected $systemStateModel;

    /**
     * @var SystemCountryModel
     */
    protected $systemCountryModel;

    /**
     * @var SystemCountryValidator
     */
    protected $systemCountryValidator;

    /**
     * @var SystemCountryTransformer
     */
    protected $systemCountryTransformer;

    /**
     * @var SystemStateTransformer
     */
    protected $systemStateTransformer;

    protected $model;

    /**
     * CountryController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->model = new SystemCountryModel();
        $this->systemCountryModel = new SystemCountryModel();
        $this->systemStateModel = new SystemStateModel();
        $this->systemCountryValidator = new SystemCountryValidator();
        $this->systemCountryTransformer = new SystemCountryTransformer();
        $this->systemStateTransformer = new SystemStateTransformer();
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function index(Request $request)
    {
        $input = $request->getQueryParams();

        try {
            $systemCountry = $this->systemCountryModel->search($input, [], array_get($input, 'limit', PAGING_LIMIT));

            return $this->response->paginator($systemCountry, $this->systemCountryTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $countryId
     *
     * @return \Dingo\Api\Http\Response
     */
    public function getStates(Request $request, $countryId)
    {
        $input = $request->getQueryParams();
        $systemState = $this->systemStateModel->allBy('sys_state_country_id', $countryId, [], $input);
        if ($systemState) {
            return $this->response->collection($systemState, $this->systemStateTransformer);
        } else {
            return $this->response->noContent();
        }
    }

    /**
     * @param Request $request
     * @param $countryCode
     *
     * @return array|\Dingo\Api\Http\Response
     */
    public function getStatesByCode(Request $request, $countryCode)
    {
        $input = $request->getQueryParams();

        $systemCountry = $this->systemCountryModel->getFirstBy('sys_country_code', $countryCode);
        $countryId = object_get($systemCountry, 'sys_country_id', 0);
        $systemState = $this->systemStateModel->allBy('sys_state_country_id', $countryId, [], $input)->toArray();
        $result = [];
        foreach ($systemState as $state) {
            $result[] = [
                'sys_state_id'         => array_get($state, 'sys_state_id', null),
                'sys_state_code'       => trim(array_get($state, 'sys_state_code', null)),
                'sys_state_name'       => trim(array_get($state, 'sys_state_name', null)),
                'sys_state_country_id' => array_get($state, 'sys_state_country_id', null),
                'sys_state_desc'       => trim(array_get($state, 'sys_state_desc', null)),
                'sys_country_name'     => object_get($systemCountry, 'sys_country_name', null)
            ];
        }

        // Sort by name
        if (!empty($input['sort']) && !empty($input['sort']['sys_state_name'])) {
            usort($result, function ($a, $b) use ($input) {
                if ($a == $b) {
                    return 0;
                }
                if (strtoupper($input['sort']['sys_state_name']) == "ASC") {
                    return ($a['sys_state_name'] < $b['sys_state_name']) ? -1 : 1;
                } else {
                    return ($a['sys_state_name'] > $b['sys_state_name']) ? -1 : 1;
                }
            });
        }

        if ($systemState) {
            return ['data' => $result];
        } else {
            return $this->response->noContent();
        }
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemCountryValidator->validate($input);

        $params = [
            'sys_country_name' => $input['sys_country_name'],
            'sys_country_code' => $input['sys_country_code']
        ];
        try {
            // check duplicate code
            if ($this->systemCountryModel->checkWhere(['sys_country_code' => $params['sys_country_code']])) {
                throw new \Exception(Message::get("BM006", "Country Code"));
            }
            // check duplicate name
            if ($this->systemCountryModel->checkWhere(['sys_country_name' => $params['sys_country_name']])) {
                throw new \Exception(Message::get("BM006", "Country Name"));
            }

            if ($SystemCountry = $this->systemCountryModel->create($params)) {
                return $this->response->item($SystemCountry, $this->systemCountryTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $systemCountryId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function show($systemCountryId)
    {
        $SystemCountry = $this->systemCountryModel->getFirstBy('sys_country_id', $systemCountryId);
        if ($SystemCountry) {
            return $this->response->item($SystemCountry, $this->systemCountryTransformer);
        } else {
            return $this->response->noContent();
        }
    }

    /**
     * @param $systemCountryId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function update($systemCountryId, IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemCountryValidator->validate($input);

        $params = [
            'sys_country_id'   => $systemCountryId,
            'sys_country_code' => $input['sys_country_code'],
            'sys_country_name' => $input['sys_country_name'],
        ];
        try {
            // Check exist
            if (!$this->systemCountryModel->checkWhere(['sys_country_id' => $systemCountryId])) {
                throw new \Exception(Message::get("BM017", "Country Code"));
            }

            // check duplicate code
            $systemCountry = $this->systemCountryModel->getFirstBy('sys_country_code', $params['sys_country_code']);

            if (($systemCountry) && $systemCountry->sys_country_id != $systemCountryId) {
                throw new \Exception(Message::get("BM006", "Country Code"));
            }

            // check duplicate name
            $systemCountry = $this->systemCountryModel->getFirstBy('sys_country_name', $params['sys_country_name']);
            if ($systemCountry && $systemCountry->sys_country_id != $systemCountryId) {

                throw new \Exception(Message::get("BM006", "Country Name"));
            }

            if ($systemCountry = $this->systemCountryModel->update($params)) {
                return $this->response->item($systemCountry, $this->systemCountryTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
