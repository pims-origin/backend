<?php

namespace Seldat\Wms2\Models;

class StyleLocations extends BaseSoftModel
{
    protected $table = 'cartons';

    protected $primaryKey = 'ctn_id';

    protected $fillable = [
        'sku',
        'plt_num',
        'loc_name',
        'ctn_sts',
        'ctnr_num',
        'piece_remain',
        'total_cartons',
        'total_pieces',
        'cus_name',
        'item_code',
        'size',
        'color',
        'prefix',
        'suffix',
        'length',
        'width',
        'height',
        'weight',
        'created_at',
    ];

    public function AsnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function pallet()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Pallet', 'plt_id', 'plt_id');
    }
}
