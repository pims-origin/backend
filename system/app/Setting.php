<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Utils\Database\Eloquent\SoftDeletes;

class Setting extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'setting_id';

    protected $fillable = [
        'setting_key',
        'setting_value'
    ];
}
