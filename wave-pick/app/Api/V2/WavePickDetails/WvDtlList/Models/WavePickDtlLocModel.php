<?php

namespace App\Api\V2\WavePickDetails\WvDtlList\Models;

use Seldat\Wms2\Models\WaveDtlLoc;

class WavePickDtlLocModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WaveDtlLoc $model = null)
    {
        $this->model = ($model) ?: new WaveDtlLoc();
    }
}
