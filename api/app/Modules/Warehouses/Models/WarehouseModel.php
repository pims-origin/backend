<?php
namespace App\Modules\Warehouses\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Modules\Warehouses\Eloquent\Warehouse;
use App\Modules\Warehouses\Eloquent\UserWarehouse;

class WarehouseModel
{
    protected $whs;
    protected $whsModel;
    protected $userWhsModel;

    public function __construct(Warehouse $whsModel)
    {
        $this->whs = Config::get('data.whsOfUser');
        $this->whsModel = $whsModel ? $whsModel : new Warehouse;
        $this->userWhsModel = new UserWarehouse;
    }

    public function getListWarehouse($userId, $perPage=10, $baseUrl)
    {
        $listWhs = $this->whsModel->whereIn('whs_id', json_decode(json_encode($this->whs), true))
            ->where('deleted' , 0)
            ->paginate($perPage)->withPath($baseUrl);

        return $listWhs;
    }

    public function getWarehouseById($user_id, $whs_id)
    {
        $whsOfUser = json_decode(json_encode($this->whs), true);
        $listWhs = [];
        foreach ($whsOfUser as $data) {
            $listWhs[] = $data['whs_id'];
        }

        if (!in_array($whs_id, $listWhs))
        {
            throw new \Exception(json_encode(['title' =>'Warehouse is not belong to this user.', 'detail' => 'Please try']));
        }

        $whs = $this->whsModel->where('deleted', 0)->find($whs_id);

        return $whs;
    }
}