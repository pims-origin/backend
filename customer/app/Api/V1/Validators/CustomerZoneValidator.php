<?php

namespace App\Api\V1\Validators;


class CustomerZoneValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'zone_id' => 'required|exists:zone,zone_id'
        ];
    }
}
