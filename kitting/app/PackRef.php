<?php
namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

/**
 * Class PackRef
 *
 * @package Seldat\Wms2\Models
 */
class PackRef extends BaseInvoiceModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pack_ref';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pack_ref_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'width',
        'height',
        'length',
        'dimension',
        'pack_type',
        'created_at',
        'updated_at'
    ];

    public function packType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\PackType', 'pack_type', 'pack_code');
    }

}
