<?php

namespace App\Modules\Orders\Eloquents;

use App\Modules\Orders\Eloquents\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class BaseSoftModel extends Model
{
    use SoftDeletes;

    public static function boot()
    {
        parent::boot();

        $userId = Config::get('data.userId');

        // create a event to happen on updating
        static::updating(function ($table) use ($userId) {
            $table->updated_by = $userId;
        });

        // create a event to happen on saving
        static::saving(function ($table) use ($userId) {
            $table->created_by = $table->created_by ?: $userId;
            $table->updated_by = $userId;
        });
    }

    public function getDateFormat()
    {
        return 'U';
    }
}