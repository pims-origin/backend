<?php


namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Logs;

class LogsTransformer extends TransformerAbstract
{
    //return result
    public function transform(Logs $logs)
    {
        return [
            'id' => object_get($logs,'id'),
            'whs_id' => object_get($logs,'whs_id'),
            'type' => object_get($logs,'type'),
            'evt_code' => object_get($logs,'evt_code'),
            'owner' => object_get($logs,'owner'),
            'transaction' => object_get($logs,'transaction'),
            'url_endpoint' => object_get($logs,'url_endpoint'),
            'message' => object_get($logs,'message'),
            'header' => object_get($logs,'header'),
            'request_data' => object_get($logs,'request_data'),
            'response_data' => object_get($logs,'response_data'),
            'created_at' => object_get($logs,'created_at'),
            'created_by' => object_get($logs,'created_by'),
            'created_at_str' => date("m/d/Y h:i:s", object_get($logs,'created_at_str')),
            'des' => object_get($logs,'des'),
        ];
    }

}
