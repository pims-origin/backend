<?php

namespace common\models;

use backend\models\LastLogin;
use Firebase\JWT\JWT;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UnauthorizedHttpException;

/**
 * User model
 *
 * @property integer $id
 * @property integer $type
 * @property string $first_name
 * @property string $last_name
 * @property string $password
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    // Use the trait in your User model
    use \damirka\JWT\UserTrait;

    const STATUS_DELETED = '07';
    const STATUS_PENDING = '30';
    const STATUS_ACTIVE = 'AC';
    const STATUS_INACTIVE = 'IA';
    const STATUS_LOCKED = 'LO';
    const PW_POLICY_TIME = 90;
    public $last_login;


    public function init()
    {
        parent::init();
        //disable session save
        \Yii::$app->user->enableSession = false;
        \Yii::$app->user->enableAutoLogin = false;
    }

    public function getUserWarehouse()
    {
        return $this->hasMany(UserWarehouse::className(), ['user_id' => 'user_id']);
    }

    public function getCusInUser()
    {
        return $this->hasMany(CusInUser::className(), ['user_id' => 'user_id']);
    }

    public function getCusExUser()
    {
        return $this->hasMany(CusInUser::className(), ['user_id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();

        // remove fields that contain sensitive information
        unset($fields['password']);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['user_id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $username
     *
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status'               => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    // Override this method
    public static function getSecretKey()
    {
        // get secret key from DB
        return 'CGunzjKYdoErvLvz83pk0ucNTnRL9sC5';
    }

    // And this one if you wish
    protected static function getHeaderToken()
    {
        return [];
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $secret = static::getSecretKey();
        $errorText = 'Invalid token';

        // Decode token and transform it into array.
        // Firebase\JWT\JWT throws exception if token can not be decoded
        try {
            $decoded = JWT::decode($token, $secret, [static::getAlgo()]);
        } catch (\Exception $e) {
            if ($e->getMessage() === 'Expired token') {
                $errorText = 'Login expire time';
                throw new UnauthorizedHttpException($errorText, 403);
            }
            throw new UnauthorizedHttpException($errorText);
        }

        $decodedArray = (array)$decoded;

        // If there's no jti param - exception
        if (!isset($decodedArray['jti'])) {
            throw new UnauthorizedHttpException($errorText);
        }

        // JTI is unique identifier of user.
        // For more details: https://tools.ietf.org/html/rfc7519#section-4.1.7
        $user_id = (int)$decodedArray['jti'];

        $lastLogin = LastLogin::findOne([
            'user_id'    => $user_id,
            'expired_at' => (int)$decodedArray['exp'],
            'status'     => LastLogin::STATUS_ACCEPTED
        ]);

        if (null === $lastLogin) {
            throw new UnauthorizedHttpException($errorText);
        }

        if ($lastLogin->login_at != $decodedArray['iat']) {
            $time = $lastLogin->expired_at - $decodedArray['exp'];

            if ($time > 0) {
                $errorText = "User has already logged in from another browser/device.";
                throw new UnauthorizedHttpException($errorText, 402);
            } else {
                $errorText = "Login expire time";
                throw new UnauthorizedHttpException($errorText, 403);
            }

            throw new UnauthorizedHttpException($errorText);
        }

        return new User(['user_id' => $user_id]);
    }

    /**
     * Encodes model data to create custom JWT with model.id set in it
     *
     * @return string encoded JWT
     */
    public function getJWT()
    {
        // Collect all the data
        $secret = static::getSecretKey();
        $loginAt = time();
        $request = Yii::$app->request;
        $hostInfo = '';

        // There is also a \yii\console\Request that doesn't have this property
        if ($request instanceof WebRequest) {
            $hostInfo = $request->hostInfo;
        }

        // update Last Login
        //$lastLogin = LastLogin::findOne(['user_id' => $this->user_id]);
        //if (null === $lastLogin) {
        //    $lastLogin = new LastLogin([
        //        'user_id' => $this->user_id,
        //
        //    ]);
        //} else {
        //    //  Check password policy
        //    $loginPolicyTime = $lastLogin->login_at + (self::PW_POLICY_TIME * 86400);
        //    if ($loginPolicyTime < time()) {
        //        throw new UnauthorizedHttpException(
        //            'Your password has been expired', 405
        //        );
        //    }
        //}

        //VIET multiple login
        $lastLogin = new LastLogin([
            'user_id' => $this->user_id,
        ]);

        $lastLoginResult = $lastLogin->login($loginAt);
        $this->last_login = $loginAt;
        if (!$lastLoginResult) {
            // throw exception not insert into DB

        }

        // Merge token with presets not to miss any params in custom
        // configuration
        $token = array_merge([
            'iss'  => $hostInfo,
            'iat'  => $loginAt,
            'exp'  => $lastLogin->expired_at,
            'fullname' => sprintf('%s %s', $this->first_name, $this->last_name),
            'name' => sprintf('%s %s', $this->first_name, $this->last_name),
            'username' => sprintf('%s',$this->username),
        ], static::getHeaderToken());

        // Set up id
        $token['jti'] = $this->user_id;

        return JWT::encode($token, $secret, static::getAlgo());
    }

}
