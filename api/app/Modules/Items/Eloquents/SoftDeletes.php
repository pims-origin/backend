<?php

namespace App\Modules\Items\Eloquents;

use Carbon\Carbon;

trait SoftDeletes
{
    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootSoftDeletes()
    {
        static::creating(function($item){
            $item->deleted_at = static::getDefaultDatetimeDeletedAt();
            $item->deleted    = 0;
        });
    }

    public static function getDefaultDatetimeDeletedAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', '1999-01-01 00:00:00', 'UTC')->timestamp;
    }
}