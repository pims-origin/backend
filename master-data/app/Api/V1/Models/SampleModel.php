<?php

namespace App\Api\V1\Models;

class SampleModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
//        $this->model = new Menu;
    }

    /**
     * @return object
     */
    public function get()
    {
        return (object)[
            'title' => 'sample 1',
            'description' => 'sample 2',
        ];
    }
}
