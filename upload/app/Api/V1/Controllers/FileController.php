<?php
/**
 * Created by PhpStorm.
 * User: PhucTran
 * Date: 3/16/2017
 * Time: 9:20 AM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\DocType;
use App\Api\V1\Models\File;
use App\Api\V1\Transformers\FileTransformer;
use App\Utils\JWTUtil;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Wms2\UserInfo\Data;

class FileController extends AbstractController
{
    protected $_request;

    /**
     * FileController constructor.
     */
    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function postUpload()
    {
        $userCache = $this->getUserCache();
        $idUser = $userCache['user_id'];
        $whID = $userCache['current_whs'];
        $fullName = $userCache['first_name'] . ' ' . $userCache['last_name'];

        $data = $this->_request->only(
            'doc_type',
            'transaction'
        );

        $validator = $this->getValidationFactory()->make($data, [
            'doc_type' => 'required|max:50',
            //'doc_type' => 'required|max:50|regex:/(^[A-Za-z0-9]+$)+/',
            'transaction' => 'required|max:50',
//            'doc_num' => 'required|max:50',
//            'external' => 'required|numeric|in:0,1',
//            'doc_date' => 'date'
        ]);

        if($validator->fails()){
            $this->response()->errorBadRequest($validator->getMessageBag()->first());
        }


        if (is_null($idUser)) {
            throw new \Exception("Unknown user");
        }

        $files = $this->_request->file('files');
        $arrSuccess = [];
        $arrFail = [];
        $insertFiles = [];
        foreach ($files as $file) {
            $fileName = $file->getClientOriginalName();
            $ext = $file->clientExtension();
            $pathServer = 'upload/' . uniqid() . '/' . $fileName;
            $fileInfo = [
                'name' => $fileName,
                'size' => $file->getClientSize(),
                'mime' => $ext
            ];
            try{
                Storage::put($pathServer, file_get_contents($file->path()));

                $insertFiles[] = [
                    'whs_id' => $whID,
                    'doc_type' => $data['doc_type'],
                    'transaction' => $data['transaction'],
//                    'doc_num' => $data['doc_num'],
//                    'external' => $data['external'],
                    'doc_date' => time(),
                    'filename' => $fileName,
                    'created_at' => time(),
                    'created_by' => $idUser,
                    'fullname' => $fullName,
                    'file_path' => $pathServer,
                    'mime' => $ext,
                ];
                $fileInfo['key'] = base64_encode($pathServer);
                $arrSuccess[] = $fileInfo;
            }
            catch (\Exception $e){
                $arrFail[] = [
                    'file' => $fileInfo,
                    'error' => $e->getMessage()
                ];
            }
        }

        File::insert($insertFiles);
        return response()->json([
            'data' => [
                'success' => $arrSuccess,
                'fail' => $arrFail
            ]
        ]);
    }

    public function getList(){
        $transaction = $this->_request->get('transaction');
        $docCode = $this->_request->get('doc_type');

        $userCache = $this->getUserCache();
        $whID = $userCache['current_whs'];

        $query = File::where('whs_id', $whID);
        if($transaction){
            $query->where('transaction', $transaction);
        }

        if($docCode){
            $query->where('doc_type', $docCode);
        }

//        $query->join('doc_type', 'doc_type.doc_code', '=', 'files.doc_type');
        $query->select([
            'files.*'
        ]);

        return $this->response->collection($query->get(), new FileTransformer());
    }

    public function delete($idFile){

        $userCache = $this->getUserCache();
        $whID = $userCache['current_whs'];

        $file = File::find($idFile);

        if(!$file){
            return $this->response->errorNotFound('File not found');
        }

        if($file->whs_id != $whID){
            return $this->response->errorBadRequest('File not belongs to selected warehouse');
        }

        $file->delete();
        return response()->json([
            'data' => $file
        ]);
    }

//    public function getListDocType(){
//        $group = $this->_request->get('group');
//        $query = DocType::orderBy('group');
//        if($group){
//            $query->where('group', strtoupper($group));
//        }
//        return response()->json([
//            'data' => $query->get()
//        ]);
//    }
//
//    public function getDetailDocType($docCode){
//
//        $docType = DocType::find($docCode);
//        if(!$docType){
//            return $this->response->errorNotFound('Doc type not found');
//        }
//        return response()->json([
//            'data' => $docType
//        ]);
//    }
//
//    public function deleteDocType($docCode){
//
//        $docType = DocType::find($docCode);
//        if(!$docType){
//            return $this->response->errorNotFound('Doc type not found');
//        }
//        $docType->delete();
//        return response()->json([
//            'data' => $docType
//        ]);
//    }
//
//    public function postAddEditDocType(){
//
//        $data = $this->_request->only(
//            'doc_code',
//            'doc_name',
//            'des',
//            'group'
//        );
//
//        $validator = $this->getValidationFactory()->make($data, [
//            'doc_code' => 'required|max:3',
//            'doc_name' => 'required|max:50',
//            'des' => 'max:255',
//            'group' => 'required|max:10'
//        ]);
//
//        if($validator->fails()){
//            $this->response()->errorBadRequest($validator->getMessageBag()->first());
//        }
//
//        $docType = DocType::find($data['doc_code']);
//        if(!$docType){
//            $docType = new DocType();
//        }
//
//        foreach ($data as $key => $value) {
//            $docType->$key = $value;
//        }
//        $docType->save();
//
//        return response()->json([
//            'data' => $docType
//        ]);
//    }
}