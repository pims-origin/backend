<?php

/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 25-May-16
 * Time: 15:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class MenuTest extends TestCase
{
    use DatabaseTransactions;

    protected $urlMenu;

    protected $urlList;

    protected $urlStore;

    /**
     * MenuTest constructor.
     */
    public function __construct()
    {
        $this->urlMenu = '/v1/menus';
        $this->urlList = "/v1/menu-list";
        $this->urlStore = "/v1/groups";
    }

    /**
     * Test List Menu
     *
     * No Input
     * AssertEqual
     */
    public function testList_Ok()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();

        $menuGroupId = $menuGroup->menu_group_id;

        $url = "{$this->urlList}?menu_group_id={$menuGroupId}";

        $response = $this->call('GET', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testList_HasMenuGroupName_Ok()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();

        $url = "{$this->urlList}?menu_group_name={$menuGroup->name}";

        $response = $this->call('GET', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testList_NoParams_Fail()
    {
        $url = "{$this->urlList}";

        $response = $this->call('GET', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testList_NoExistMenuGroupNameInDb_Fail()
    {
        $url = "{$this->urlList}?menu_group_name=Foo";

        $response = $this->call('GET', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NOT_FOUND);
    }

    public function testListWithPermission_Ok()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();

        $menuGroupId = $menuGroup->menu_group_id;

        $url = "{$this->urlList}?menu_group_id={$menuGroupId}";

        $params = [
            'permission' => 'viewMenuDashboard, viewMenuHome',
        ];

        $response = $this->call('POST', $url, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testListWithPermission_HasMenuGroupName_Ok()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();

        $url = "{$this->urlList}?menu_group_name={$menuGroup->name}";

        $params = [
            'permission' => 'viewMenuDashboard, viewMenuHome',
        ];

        $response = $this->call('POST', $url, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testListWithPermission_NoParams_Fail()
    {
        $url = "{$this->urlList}";

        $params = [
            'permission' => 'viewMenuDashboard, viewMenuHome',
        ];

        $response = $this->call('POST', $url, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testListWithPermission_NoPermission_Fail()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();

        $url = "{$this->urlList}?menu_group_name={$menuGroup->name}";

        $response = $this->call('POST', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testListWithPermission_NoExistMenuGroupNameInDb_Fail()
    {
        $url = "{$this->urlList}?menu_group_name=Foo";

        $params = [
            'permission' => 'viewMenuDashboard, viewMenuHome',
        ];

        $response = $this->call('POST', $url, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NOT_FOUND);
    }

    public function testCreate_Ok()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();
        $authority1 = factory(\App\Authority::class)->create();
        $authority2 = factory(\App\Authority::class)->create();
        $authority3 = factory(\App\Authority::class)->create();
        $authority4 = factory(\App\Authority::class)->create();
        $authority5 = factory(\App\Authority::class)->create();
        $authority6 = factory(\App\Authority::class)->create();
        $authority7 = factory(\App\Authority::class)->create();
        $authority8 = factory(\App\Authority::class)->create();
        $authority9 = factory(\App\Authority::class)->create();
        $authority10 = factory(\App\Authority::class)->create();
        $authority11 = factory(\App\Authority::class)->create();
        $authority12 = factory(\App\Authority::class)->create();
        $menuGroupId = $menuGroup->menu_group_id;

        $param = '[{"name": "Dashboard",
                        "description": "Dashboard",
                        "url": "#",
                        "target": "",
                        "icon_class": "icon-dashboard",
                        "permission_name": "'.$authority1->name.'"'.',
                        "nodes": []
                    }, {
                        "name": "Home",
                        "description": "Home",
                        "url": "#",
                        "target": "",
                        "icon_class": "icon-home",
                        "permission_name": "'.$authority2->name.'"'.',
                        "nodes": []
                    }, {
                        "name": "Receiving",
                        "description": "Receiving",
                        "url": "#",
                        "target": "",
                        "icon_class": "icon-receiving",
                        "permission_name": "'.$authority3->name.'"'.',
                        "nodes": []
                    }, {
                        "name": "Shipping Order",
                        "description": "Shipping Order",
                        "url": "#",
                        "target": "",
                        "icon_class": "icon-shipping-order",
                        "permission_name": "'.$authority4->name.'"'.',
                        "nodes": []
                    }, {
                        "name": "System Configuration",
                        "description": "System Configuration",
                        "url": "/system-configuration",
                        "target": "",
                        "icon_class": "icon-system-configuration",
                        "permission_name": "'.$authority5->name.'"'.',
                        "nodes": []
                    }, {
                        "name": "User Management",
                        "description": "User Management",
                        "url": "/user-management",
                        "target": "",
                        "icon_class": "icon-user-management",
                        "permission_name": "'.$authority6->name.'"'.',
                        "nodes": [{
                            "name": "Create User",
                            "description": "Create User",
                            "url": "/user-management/create-user",
                            "target": "",
                            "icon_class": "icon-create-user",
                            "permission_name": "'.$authority7->name.'"'.',
                            "nodes": []
                        }, {
                            "name": "User List",
                            "description": "User List",
                            "url": "/user-management/user-list",
                            "target": "",
                            "icon_class": "icon-user-list",
                            "permission_name": "'.$authority8->name.'"'.',
                            "nodes": []
                        }, {
                            "name": "Role List",
                            "description": "Role List",
                            "url": "/user-management/role-list",
                            "target": "",
                            "icon_class": "icon-role-list",
                            "permission_name": "'.$authority9->name.'"'.',
                            "nodes": []
                        }, {
                            "name": "Create Role",
                            "description": "Create Role",
                            "url": "/user-management/create-role",
                            "target": "",
                            "icon_class": "icon-create-role",
                            "permission_name": "'.$authority10->name.'"'.',
                            "nodes": []
                        }, {
                            "name": "Permission List",
                            "description": "Permission List",
                            "url": "/user-management/permission-list",
                            "target": "",
                            "icon_class": "icon-permission-list",
                            "permission_name": "'.$authority11->name.'"'.',
                            "nodes": []
                        }, {
                            "name": "Create Permission",
                            "description": "Create Permission",
                            "url": "/user-management/create-permission",
                            "target": "",
                            "icon_class": "icon-create-permission",
                            "permission_name": "'.$authority12->name.'"'.',
                            "nodes": []
                        }]
                    }]';

        $url = "{$this->urlStore}/$menuGroupId/list_menu";

        $response = $this->call('POST', $url, [
            'param' => $param,
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testCreate_NotExistMenuGroup_Fail()
    {
        $menuGroupId = 999999999999999;
        $param = '[{
                        "name": "Dashboard",
                        "description": "Dashboard",
                        "url": "#",
                        "target": "",
                        "icon_class": "icon-dashboard",
                        "permission_name": "viewMenuDashboard",
                        "nodes": []
                    }, {
                        "name": "Home",
                        "description": "Home",
                        "url": "#",
                        "target": "",
                        "icon_class": "icon-home",
                        "permission_name": "viewMenuHome",
                        "nodes": []
                    }, {
                        "name": "Receiving",
                        "description": "Receiving",
                        "url": "#",
                        "target": "",
                        "icon_class": "icon-receiving",
                        "permission_name": "viewMenuReceiving",
                        "nodes": []
                    }, {
                        "name": "Shipping Order",
                        "description": "Shipping Order",
                        "url": "#",
                        "target": "",
                        "icon_class": "icon-shipping-order",
                        "permission_name": "viewMenuShippingOrder",
                        "nodes": []
                    }, {
                        "name": "System Configuration",
                        "description": "System Configuration",
                        "url": "/system-configuration",
                        "target": "",
                        "icon_class": "icon-system-configuration",
                        "permission_name": "viewMenuSystemConfiguration",
                        "nodes": []
                    }, {
                        "name": "User Management",
                        "description": "User Management",
                        "url": "/user-management",
                        "target": "",
                        "icon_class": "icon-user-management",
                        "permission_name": "viewMenuUser",
                        "nodes": [{
                            "name": "Create User",
                            "description": "Create User",
                            "url": "/user-management/create-user",
                            "target": "",
                            "icon_class": "icon-create-user",
                            "permission_name": "viewMenuUser_Create",
                            "nodes": []
                        }, {
                            "name": "User List",
                            "description": "User List",
                            "url": "/user-management/user-list",
                            "target": "",
                            "icon_class": "icon-user-list",
                            "permission_name": "viewMenuUser_List",
                            "nodes": []
                        }, {
                            "name": "Role List",
                            "description": "Role List",
                            "url": "/user-management/role-list",
                            "target": "",
                            "icon_class": "icon-role-list",
                            "permission_name": "viewMenuRole_List",
                            "nodes": []
                        }, {
                            "name": "Create Role",
                            "description": "Create Role",
                            "url": "/user-management/create-role",
                            "target": "",
                            "icon_class": "icon-create-role",
                            "permission_name": "viewMenuRole_Create",
                            "nodes": []
                        }, {
                            "name": "Permission List",
                            "description": "Permission List",
                            "url": "/user-management/permission-list",
                            "target": "",
                            "icon_class": "icon-permission-list",
                            "permission_name": "viewMenuPermission_List",
                            "nodes": []
                        }, {
                            "name": "Create Permission",
                            "description": "Create Permission",
                            "url": "/user-management/create-permission",
                            "target": "",
                            "icon_class": "icon-create-permission",
                            "permission_name": "viewMenuPermission_Create",
                            "nodes": []
                        }]
                    }]';
        $url = "{$this->urlStore}/$menuGroupId/list_menu";

        $response = $this->call('POST', $url, [
            'param' => $param,
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function show_Ok()
    {
        // create data into DB
        $menu = factory(App\Menu::class)->create();

        $url = "{$this->urlMenu}/{$menu->menu_id}";

        $response = $this->call('GET', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseMenu = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('menu_id', $responseMenu['data']);
        $this->assertArrayHasKey('menu_group_id', $responseMenu['data']);
        $this->assertArrayHasKey('name', $responseMenu['data']);
        $this->assertArrayHasKey('description', $responseMenu['data']);
        $this->assertArrayHasKey('url', $responseMenu['data']);
        $this->assertArrayHasKey('target', $responseMenu['data']);
        $this->assertArrayHasKey('icon_class', $responseMenu['data']);
        $this->assertArrayHasKey('permission_name', $responseMenu['data']);
        $this->assertArrayHasKey('display_order', $responseMenu['data']);
    }

    /**
     * @test
     */
    public function show_Fail()
    {
        $url = "{$this->urlMenu}/999999";

        $response = $this->call('GET', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @test
     */
    public function store_Ok()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();

        $params = [
            'menu_group_id'   => $menuGroup->menu_group_id,
            'name'            => "menu 1",
            'description'     => "description menu 1",
            'url'             => '#',
            'target'          => '',
            'icon_class'      => '',
            'permission_name' => 'viewMenu',
            'display_order'   => 1,
        ];

        $response = $this->call('POST', $this->urlMenu, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        $responseMenu = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('menu_id', $responseMenu['data']);
        $this->assertArrayHasKey('menu_group_id', $responseMenu['data']);
        $this->assertArrayHasKey('name', $responseMenu['data']);
        $this->assertArrayHasKey('description', $responseMenu['data']);
        $this->assertArrayHasKey('url', $responseMenu['data']);
        $this->assertArrayHasKey('target', $responseMenu['data']);
        $this->assertArrayHasKey('icon_class', $responseMenu['data']);
        $this->assertArrayHasKey('permission_name', $responseMenu['data']);
        $this->assertArrayHasKey('display_order', $responseMenu['data']);
    }

    /**
     * @test
     */
    public function store_MenuGroupNotExisted_Fail()
    {
        $params = [
            'menu_group_id'   => 9999,
            'name'            => "menu 1",
            'description'     => "description menu 1",
            'url'             => '#',
            'target'          => '',
            'icon_class'      => '',
            'permission_name' => 'viewMenu',
            'display_order'   => 1,
        ];

        $response = $this->call('POST', $this->urlMenu, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function store_EmptyName_Fail()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();

        $params = [
            'menu_group_id'   => $menuGroup->menu_group_id,
            'name'            => "",
            'description'     => "description menu 1",
            'url'             => '#',
            'target'          => '',
            'icon_class'      => '',
            'permission_name' => 'viewMenu',
            'display_order'   => 1,
        ];

        $response = $this->call('POST', $this->urlMenu, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function update_Ok()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();
        $menu = factory(App\Menu::class)->create(['menu_group_id' => $menuGroup->menu_group_id]);

        $params = [
            'menu_group_id'   => $menuGroup->menu_group_id,
            'name'            => "new menu 1",
            'description'     => "description menu 1",
            'url'             => '#',
            'target'          => '',
            'icon_class'      => '',
            'permission_name' => 'viewMenu',
            'display_order'   => 1,
        ];

        $url = "{$this->urlMenu}/{$menu->menu_id}";

        $response = $this->call('PUT', $url, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseMenu = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('menu_id', $responseMenu['data']);
        $this->assertArrayHasKey('menu_group_id', $responseMenu['data']);
        $this->assertArrayHasKey('name', $responseMenu['data']);
        $this->assertArrayHasKey('description', $responseMenu['data']);
        $this->assertArrayHasKey('url', $responseMenu['data']);
        $this->assertArrayHasKey('target', $responseMenu['data']);
        $this->assertArrayHasKey('icon_class', $responseMenu['data']);
        $this->assertArrayHasKey('permission_name', $responseMenu['data']);
        $this->assertArrayHasKey('display_order', $responseMenu['data']);
    }

    /**
     * @test
     */
    public function update_MenuGroupNotExisted_Fail()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();
        $menu = factory(App\Menu::class)->create(['menu_group_id' => $menuGroup->menu_group_id]);

        $params = [
            'menu_group_id'   => 9999,
            'name'            => "menu 1",
            'description'     => "description menu 1",
            'url'             => '#',
            'target'          => '',
            'icon_class'      => '',
            'permission_name' => 'viewMenu',
            'display_order'   => 1,
        ];

        $url = "{$this->urlMenu}/{$menu->menu_id}";

        $response = $this->call('PUT', $url, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function update_EmptyName_Fail()
    {
        // create data into DB
        $menuGroup = factory(App\MenuGroup::class)->create();
        $menu = factory(App\Menu::class)->create(['menu_group_id' => $menuGroup->menu_group_id]);

        $params = [
            'menu_group_id'   => $menuGroup->menu_group_id,
            'name'            => "",
            'description'     => "description menu 1",
            'url'             => '#',
            'target'          => '',
            'icon_class'      => '',
            'permission_name' => 'viewMenu',
            'display_order'   => 1,
        ];

        $url = "{$this->urlMenu}/{$menu->menu_id}";

        $response = $this->call('PUT', $url, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function destroy_SingleItem_Ok()
    {
        // create data into DB
        $menu = factory(App\Menu::class)->create();

        $url = "{$this->urlMenu}/{$menu->menu_id}";

        $response = $this->call('DELETE', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    /**
     * @test
     */
    public function destroy_ThreeLevels_Ok()
    {
        // create data into DB
        $menu10 = factory(App\Menu::class)->create();
        $menu20 = factory(App\Menu::class)->create(['parent_id' => $menu10->menu_id, 'display_order' => 1]);
        $menu30 = factory(App\Menu::class)->create(['parent_id' => $menu20->menu_id, 'display_order' => 1]);
        $menu31 = factory(App\Menu::class)->create(['parent_id' => $menu20->menu_id, 'display_order' => 2]);

        $url = "{$this->urlMenu}/{$menu20->menu_id}";

        $response = $this->call('DELETE', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        $menuModel = new App\Menu();
        $this->seeInDatabase($menuModel->getTable(),
            ['menu_id' => $menu30->menu_id, 'parent_id' => 0, 'display_order' => 0]);
        $this->seeInDatabase($menuModel->getTable(),
            ['menu_id' => $menu31->menu_id, 'parent_id' => 0, 'display_order' => 0]);
    }
}
