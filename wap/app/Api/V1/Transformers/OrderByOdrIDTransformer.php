<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderCartonModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

class OrderByOdrIDTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $ordDtl = new OrderDtlModel();
        $ordCtn = new OrderCartonModel();
        $total = $ordDtl->sumCartonOdr($orderHdr->odr_id);

        $ttCtn = $total['total_qty'] . ' / 0';
        $ctns = $ordCtn->findWhere([
            'odr_hdr_id' => $orderHdr->odr_id
        ]);

        if(!empty($ctns)){
            $ctns = $ctns->toArray();
            $ttCtn = $total['total_qty'] . ' / ' . count($ctns);
        }

        $dtCtn = []; $ctnOrd = [];
        foreach ($ctns as $dt) {
            $dtCtn['ctn_id'] = $dt['ctn_id'];
            $dtCtn['ctn_rfid'] = $dt['ctn_rfid'];
            $dtCtn['ctn_num'] = $dt['ctn_num'];
            $dtCtn['ctn_sts'] = Status::getByKey('ORDER-STATUS', $dt['ctn_sts']);
            $ctnOrd[] = $dtCtn;
        }


        return [
            'odr_id'       => $orderHdr->odr_id,
            'odr_num'      => $orderHdr->odr_num,
            'ctn_qty'    => $ttCtn,
            'ctn_detail'   => $ctnOrd
        ];

    }
}
