<?php
namespace backend\models;

class AssignRoleToUserForm extends Form
{
    public $roles;
    public $user_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'roles'], 'required']
        ];
    }
}
