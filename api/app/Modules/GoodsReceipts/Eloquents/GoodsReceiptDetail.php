<?php

namespace App\Modules\GoodsReceipts\Eloquents;

class GoodsReceiptDetail extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gr_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gr_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'asn_dtl_id',
        'gr_hdr_id',
        'gr_dtl_ept_ctn_ttl',
        'gr_dtl_act_ctn_ttl',
        'gr_dtl_cus_note',
        'gr_dtl_disc',
        'gr_dtl_plt_ttl',
        'gr_dtl_is_dmg',
        'gr_dtl_sts',
        'item_id',
        'pack',
        'sku',
        'size',
        'color',
        'lot',
        'po',
        'uom_id',
        'uom_code',
        'uom_name',
        'expired_dt',
        'gr_dtl_dmg_ttl',
        'upc',
        'ctnr_id',
        'ctnr_num',
        'length',
        'width',
        'height',
        'weight',
        'cube',
        'volume',
        'gr_dtl_des',
        'crs_doc',
        'ucc128',
        'spc_hdl_code',
        'spc_hdl_name',
        'cat_code',
        'cat_name'
    ];

    public function goodsReceiptStatus()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptStatus', 'gr_sts', 'gr_sts_code');
    }

    public function goodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'gr_hdr_id', 'gr_hdr_id');
    }

    public function asnDetail()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }

    /**
     *  itemCat
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itemCat()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ItemCat', 'cat_code', 'cat_code');
    }
}
