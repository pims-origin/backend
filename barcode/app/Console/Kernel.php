<?php

namespace App\Console;

use App\Console\Commands\CorrectCarton;
use App\Console\Commands\CorrectPalletCarton;
use App\Console\Commands\UpdateOrderFlow;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // For command vendor:publish
        'BasicIT\LumenVendorPublish\VendorPublishCommand',
      //  ExampleCommand::class
        CorrectCarton::class,
        CorrectPalletCarton::class,
        UpdateOrderFlow::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
