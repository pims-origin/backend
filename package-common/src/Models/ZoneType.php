<?php

namespace Seldat\Wms2\Models;

use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

class ZoneType extends BaseSoftModel
{


    protected $table = 'zone_type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'zone_type_id';

    /**
     * @var array
     */
    protected $fillable = [
        'zone_type_name',
        'zone_type_code',
        'zone_type_desc'
    ];

    public function zone()
    {
        return $this->hasOne(__NAMESPACE__ . '\Zone', 'zone_type_id', 'zone_type_id');
    }
}
