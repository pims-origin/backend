<?php

namespace App\Api\V2\Outbound\OutPallet\UnassignCartonRfid\Controllers;

use App\Api\V2\Outbound\OutPallet\UnassignCartonRfid\Models\OrderHdrModel;
use App\Api\V2\Outbound\OutPallet\UnassignCartonRfid\Models\EventTrackingModel;
use App\Api\V2\Outbound\OutPallet\UnassignCartonRfid\Models\OutPalletModel;
use App\Api\V2\Outbound\OutPallet\UnassignCartonRfid\Models\PackHdrModel;
use App\Api\V2\Outbound\OutPallet\UnassignCartonRfid\Models\CartonModel;
use App\Api\V2\Outbound\OutPallet\UnassignCartonRfid\Validators\OutPalletValidator;
use App\libraries\RFIDValidate;

use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OutPalletController extends AbstractController
{

    protected $orderHdrModel;
    protected $eventTrackingModel;
    protected $outPalletModel;
    protected $packHdrModel;
    protected $cartonModel;

    public function __construct()
    {
        $this->orderHdrModel      = new OrderHdrModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->outPalletModel     = new OutPalletModel();
        $this->packHdrModel       = new PackHdrModel();
        $this->cartonModel        = new CartonModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function unassignPackCarton($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $palletCode = array_get($input, 'pallet', null);
        $ctnRfid    = array_get($input, 'pack_code', null);

        /*
         * start logs
         */

        $url = "/v2/{$whsId}/outpallet/unassign-pack-carton";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'UBP',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Unassign Barcode from Out Pallet'
        ]);

        /*
         * end logs
         */

        $packs = [$ctnRfid];
        if (!$ctnRfid || !$palletCode) {
            $msg = sprintf("Pallet and Pack code field are required");

            return $this->_responseErrorMessage($msg);
        }

        if (!OutPalletValidator::validate($palletCode)) {
            $msg = OutPalletValidator::ERROR_MSG;

            return $this->_responseErrorMessage($msg);
        }
        // Check exists Order hdl num
        $pltNumArr = explode('-', $palletCode);
        if (count($pltNumArr) < 4) {
            $msg = sprintf("The pallet num %s is invalid", $palletCode);

            return $this->_responseErrorMessage($msg);
        }
        $odrNum = "ORD-" . $pltNumArr[1] . "-" . $pltNumArr[2];
        $order = $this->orderHdrModel->getFirstWhere(['odr_num' => $odrNum]);
        if (!$order) {
            $msg = sprintf("No order has been found for the pallet number %s", $palletCode);

            return $this->_responseErrorMessage($msg);
        } else {
            $status = [
                Status::getByValue("Staging", "ORDER-STATUS"),
                Status::getByValue("Packing", "ORDER-STATUS"),
                Status::getByValue("Packed", "ORDER-STATUS"),
            ];
            $odrSts = object_get($order, 'odr_sts', '');
            if (!in_array($odrSts, $status)) {
                $msg = sprintf("Unable to unassign cartons from out pallet for Order is %s!", Status::getByKey('ORDER-STATUS', $odrSts));
                return $this->_responseErrorMessage($msg);
            }
        }

        // check pallet num
        $pallet = $this->outPalletModel->getFirstWhere([
            'whs_id'  => $whsId,
            'plt_num' => $palletCode,
        ]);

        if (!$pallet) {
            $msg = sprintf("The pallet number %s doesn't exist. You must assign Pack cartons to Out pallet before doing this step!", $palletCode);
            $pallet = $this->outPalletModel->getFirstWhere(['plt_num' => $palletCode]);

            if ($pallet) {
                $msg = sprintf("The pallet number %s doesn't belong to current warehouse", $palletCode);
            }
            return $this->_responseErrorMessage($msg);
        }

        if ($pallet && $pallet->out_plt_sts != 'AC') {
            $msg = sprintf("The pallet number %s is %s and not active", $palletCode, $pallet->out_plt_sts);

            return $this->_responseErrorMessage($msg);
        }

        if ($pallet && $pallet->bol_id != null) {
            $msg = sprintf("Unable to unassign cartons from out pallet for Order is already created BOL!", $odrNum);

            return $this->_responseErrorMessage($msg);
        }
        // Check duplicate Pack hdl num
        $uniquePacks = array_unique($packs);
        if (count($uniquePacks) != count($packs)) {
            $duplicatePack = array_unique(array_diff_assoc($packs, $uniquePacks));
            $msg = 'Duplicate Pack: ' . implode(', ', $duplicatePack);

            return $this->_responseErrorMessage($msg);
        }

        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($packs as $key => $pack) {
            //check invalid carton rfid when auto packs
            $checkRfid = substr($pack, 0, 3);
            if (!in_array($checkRfid, ["CTN", "CYC"])) {
                $ctnRFIDValid = new RFIDValidate($pack, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $pack;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }

                $ctnObj = $this->cartonModel->getFirstWhere(['rfid' => $pack]);
                $ctnId = object_get($ctnObj, 'ctn_id', null);
                if (!$ctnId) {
                    $msg = sprintf("Carton Rfid %s doesn't exist.", $pack);
                    return $this->_responseErrorMessage($msg);
                }
                $packHdr = $this->packHdrModel->getFirstWhere(['cnt_id' => $ctnId]);

                $packs[$key] = object_get($packHdr, 'pack_hdr_num', null);

                if ($packHdr->out_plt_id) {
                    $pltId = object_get($pallet, 'plt_id', -1);
                    if ($packHdr->out_plt_id != $pltId) {
                        $msg = sprintf("The carton Rfid %s doesn't belong to Pallet number %s ", $pack, $palletCode);

                        return $this->_responseErrorMessage($msg);
                    }

                }
                if ($packHdr->out_plt_id == null) {
                    $msg = sprintf("The carton Rfid %s doesn't belong to any Out pallet", $pack);
                    return $this->_responseErrorMessage($msg);
                }
            }
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {

            return $this->_responseErrorMessage($errorMsg);
        }

        try {
            // start transaction
            DB::beginTransaction();

            // update location in out_pallet
            $ttlCtn = 0;
            $odrId = $order->odr_id;
            $odrNum = $order->odr_num;

            $sts = false;

            foreach ((array)$packs as $packCode) {
                $packHdr = $this->packHdrModel->getFirstWhere([
                    'pack_hdr_num' => $packCode,
                    'whs_id'       => $whsId,
                ]);

                if (!$packHdr) {
                    $msg = sprintf("Packed Carton %s doesn't exist!", $packCode);

                    return $this->_responseErrorMessage($msg);
                }

                if ($packHdr->out_plt_id) {
                    $pltId = object_get($pallet, 'plt_id', -1);
                    if ($packHdr->out_plt_id != $pltId) {
                        $msg = sprintf("The carton %s doesn't belong to Pallet number %s ", $packCode, $palletCode);

                        return $this->_responseErrorMessage($msg);
                    }

                }
                if ($packHdr->out_plt_id == null) {
                    $msg = sprintf("Pack carton %s doesn't belong to any Out pallet", $packHdr->pack_hdr_num);
                    return $this->_responseErrorMessage($msg);
                }

                if ($odrId != $packHdr->odr_hdr_id) {
                    $msg = sprintf("Please scan cartons the same as order", $odrNum);

                    return $this->_responseErrorMessage($msg);
                }

                //Update Pack Status after asigned Pallet
                $packHdr->out_plt_id = null;
                $packHdr->pack_sts = 'NW';
                $packHdr->save();
                $ttlCtn--;

                $sts = $ttlCtn;
            }

            // Update total pack carton
            $this->outPalletModel->updatePalletCtnTtl($pallet->plt_id);

            $staging = $this->orderHdrModel->rollBackOrderStaging($odrId);

            // if ($odrId) {
            //     $odrObj = $this->orderHdrModel->getFirstWhere(
            //         [
            //             'odr_id' => $odrId
            //         ]
            //     );
            //     $this->eventTrackingModel->eventTracking([
            //         'whs_id'    => $whsId,
            //         'cus_id'    => object_get($odrObj, 'cus_id', null),
            //         'owner'     => object_get($odrObj, 'odr_num', null),
            //         'evt_code'  => 'WOP',
            //         'trans_num' => object_get($odrObj, 'odr_num', null),
            //         'info'      => sprintf("WAP - Remove Carton %s from Pallet", $ctnRfid)
            //     ]);
            // }

            DB::commit();

            $msg = sprintf("%d carton(s) unassigned from pallet %s successfully!", count($packs),
                $palletCode);

            return [
                "iat"     => time(),
                "status"  => true,
                "messages" => [[
                    "msg"         => $msg,
                    "status_code" => 1,
                ]],
                "data"    => []
            ];

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());

        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}