<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 25-Jul-2016
 * Time: 2:50
 */

namespace App\Api\V1\Validators;


class InsertVirtualCartonValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            "asn_hdr_id" => 'required|integer',
            "asn_dtl_id" => 'required|integer',
            "item_id"    => 'required|integer',
            "ctnr_id"    => 'required|integer',
            //"gate_code" => 'required',
        ];
    }

}