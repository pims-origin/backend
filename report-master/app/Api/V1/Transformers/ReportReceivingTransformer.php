<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportReceiving;

class ReportReceivingTransformer extends AbstractTransformer
{

    /**
     * @param ReportReceiving $reportReceiving
     * @return array
     */
    public function transform(ReportReceiving $reportReceiving)
    {
        return [
            "id"                    =>  array_get($reportReceiving, 'id', null),
            "cus_id"                =>  array_get($reportReceiving, 'cus_id', null),
            "cus_name"              =>  array_get($reportReceiving, 'cus_name', null),
            "cus_code"              =>  array_get($reportReceiving, 'cus_code', null),
            "gr_hdr_id"             =>  array_get($reportReceiving, 'gr_hdr_id', null),
            "gr_hdr_num"            =>  array_get($reportReceiving, 'gr_hdr_num', null),
            "whs_id"                =>  array_get($reportReceiving, 'whs_id', null),
            "ctnr_num"              =>  array_get($reportReceiving, 'ctnr_num', null),
            "ref_code"              =>  array_get($reportReceiving, 'ref_code', null),
            "item_id"               =>  array_get($reportReceiving, 'item_id', null),
            "sku"                   =>  array_get($reportReceiving, 'sku', null),
            "size"                  =>  array_get($reportReceiving, 'size', null),
            "color"                 =>  array_get($reportReceiving, 'color', null),
            "lot"                   =>  array_get($reportReceiving, 'lot', null),
            "pack"                  =>  array_get($reportReceiving, 'pack', null),
            "upc"                   =>  array_get($reportReceiving, 'upc', null),
            "gr_dtl_act_qty_ttl"    =>  array_get($reportReceiving, 'gr_dtl_act_qty_ttl', null),
            "gr_dtl_act_ctn_ttl"    =>  array_get($reportReceiving, 'gr_dtl_act_ctn_ttl', null),
            "gr_dtl_ept_qty_ttl"    =>  array_get($reportReceiving, 'gr_dtl_ept_qty_ttl', null),
            "gr_dtl_ept_ctn_ttl"    =>  array_get($reportReceiving, 'gr_dtl_ept_ctn_ttl', null),
            "gr_dtl_disc_qty"       =>  array_get($reportReceiving, 'gr_dtl_disc_qty', null),
            "gr_dtl_plt_ttl"        =>  array_get($reportReceiving, 'gr_dtl_plt_ttl', null),
            "length"                =>  array_get($reportReceiving, 'length', null),
            "width"                 =>  array_get($reportReceiving, 'width', null),
            "height"                =>  array_get($reportReceiving, 'height', null),
            "weight"                =>  array_get($reportReceiving, 'weight', null),
            "volume"                =>  array_get($reportReceiving, 'volume', null),
            "crs_doc_qty"           =>  array_get($reportReceiving, 'crs_doc_qty', null),
            "gr_dtl_dmg_ttl"        =>  array_get($reportReceiving, 'gr_dtl_dmg_ttl', null),
            "asn_hdr_ept_dt"        =>  $this->setTimeZone(array_get($reportReceiving, 'whs_id', null), array_get($reportReceiving, 'asn_hdr_ept_dt', null)),
            "asn_hdr_act_dt"        =>  $this->setTimeZone(array_get($reportReceiving, 'whs_id', null), array_get($reportReceiving, 'asn_hdr_act_dt', null)),
            "gr_hdr_act_dt"         =>  $this->setTimeZone(array_get($reportReceiving, 'whs_id', null), array_get($reportReceiving, 'gr_hdr_act_dt', null)),
            "updated_by"            =>  array_get($reportReceiving, 'updated_by', null),
            "cube"                  =>  array_get($reportReceiving, 'cube', null),
            "created_at"            =>  $this->setTimeZone(array_get($reportReceiving, 'whs_id', null), array_get($reportReceiving->toArray(), 'created_at', null)),
            "updated_at"            =>  $this->setTimeZone(array_get($reportReceiving, 'whs_id', null), array_get($reportReceiving->toArray(), 'updated_at', null)),
            "created_by"            =>  array_get($reportReceiving, 'created_by', null),
            "deleted_at"            =>  $this->setTimeZone(array_get($reportReceiving, 'whs_id', null), array_get($reportReceiving, 'deleted_at', null)),
            "deleted"               =>  array_get($reportReceiving, 'deleted', null),
            'timezone'              => $this->getWhsTimeZone(array_get($reportReceiving, 'whs_id', null)) ?: 'UTC±0',
        ];
    }
}
