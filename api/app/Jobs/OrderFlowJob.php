<?php

namespace App\Jobs;


use App\Modules\Orders\Models\CustomerMetaModel;
use App\Modules\Orders\Models\OrderDtlModel;
use App\Modules\Orders\Models\OrderHdrMetaModel;
use App\Modules\Orders\Models\OrderHdrModel;
use DB;
use GuzzleHttp\Client;
use Psr\Http\Message\ServerRequestInterface as Request;

class OrderFlowJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $orderId;
    protected $cusId;
    protected $request;
    protected $customerMetaModel;
    protected $hdrMetaModel;

    public function __construct(
        $orderId,
        $cusId,
        $request
    ) {
        $this->orderId = $orderId;
        $this->cusId = $cusId;
        $this->request = $request;
        $this->customerMetaModel = new CustomerMetaModel;
        $this->hdrMetaModel = new OrderHdrMetaModel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(empty(env('API_ORDER'))) {
            $this->writeLogment('Order Flow Job', "API_ORDER is not define!");
            return false;
        }

        if (is_null($this->hdrMetaModel)) {
            $cus_meta = $this->customerMetaModel->getOrderFlow($this->cusId);
        } else {
            // Get customer meta
            $cus_meta = $this->hdrMetaModel->getOrderFlow($this->orderId);
        }

        $orderHdrModel = new OrderHdrModel();
        $csrFlow = $this->customerMetaModel->getFlow($cus_meta, 'CSR');
        $client = new Client();

        try {
            $token = "Bearer " . $this->wmsLogin();

            // Auto assign CSR
            if (array_get($csrFlow, 'usage', -1) == 1) {
                $odr = $orderHdrModel->getFirstWhere(['odr_id' => $this->orderId]);
                $csr_default = $this->customerMetaModel->getPNP($this->cusId, 'CSR', object_get($odr, 'whs_id', 0));
                if ($csr_default != 0) {
                    $odrIds[] = $this->orderId;
                    $client->request('PUT', env('API_ORDER') . 'assign-csr',
                        [
                            'headers'     => ['Authorization' => $token],
                            'form_params' => ["user_id" => $csr_default, "odr_id" => $odrIds]
                        ]
                    );
                }
            }

            $this->writeLogment('Order Flow Job', "Auto Order Flow success.");
        } catch (\Exception $e) {
            $this->writeLogment('Order Flow Job', $e->getMessage());
        }

    }
}
