<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 5:00 PM
 */

namespace App\Api\V1\Validators;


class MenuFavoriteValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'menu_id'       => 'required|integer|exists:menu',
            'display_order' => 'required|integer',
        ];
    }
}
