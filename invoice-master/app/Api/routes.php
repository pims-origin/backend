<?php

$router = $app->router;

$router->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Handle application Route
$api->version('v1', [], function ($api) {

    $api->group(['prefix' => 'public', 'middleware' => [], 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
        $api->get('/download-document', 'DocumentController@downloadDocument');
    });

    // Bypass middleware when testing
    $middleware = ['trimInput', 'setWarehouseTimezone'];
    if (env('APP_ENV') != 'testing') {
        $middleware[] = 'verifySecret';
    }

    $api->group(['prefix' => 'v1', 'middleware' => $middleware, 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['SELDAT INVOICE API V1'];
        });

        $api->get('test', [
            'as' => 'test', 'uses' => 'ExampleController@test'
        ]);

        $api->get('list/{type}', 'PrepareController@getList');
        //Sac
        $api->group(['prefix' => 'sac'], function ($api) {
            $api->get('customer/{search?}', 'SacController@index');
            $api->get('/search', 'SacController@search');
            $api->get('/{sac_id}', 'SacController@show');
            $api->put('/{sacId}', 'SacController@update');
        });
        //Charge Code
        $api->group(['prefix' => 'charge-code'], function($api) {
            $api->post('/create', 'SacController@createChargeCode');
            $api->put('/{sacId:[0-9]+}/', 'SacController@updateChargeCode');
            $api->get('/list-unit-uom', 'SacController@getListUnitUOM');
        });
        //Customer Sac
        $api->group(['prefix' => 'customersac'], function ($api) {
            $api->get('/search', 'CustomerSacController@search');
            $api->post('/', 'CustomerSacController@store');
            $api->put('/', 'CustomerSacController@update');
            $api->delete('/{cus_id?}', 'CustomerSacController@delete');
        });
        //Work Order
        $api->group(['prefix' => 'work-order'], function($api){
            $api->post('/', 'WorkOrderController@store');
            $api->put('/{workOrderId:[0-9]+}', 'WorkOrderController@update');
            $api->get('/{workOrderId:[0-9]+}', 'WorkOrderController@show');
            $api->get('/get-next-wo-num', 'WorkOrderController@getNextWONum');
            $api->get('/search', 'WorkOrderController@search');
            // $api->delete('/delete', 'WorkOrderController@delete');
            $api->delete('/delete', 'WorkOrderController@deleteWithNewStatus');
            $api->delete('/delete-multi', 'WorkOrderController@deleteMulti');
            $api->get('/auto-complete/{type}', 'WorkOrderController@autoComplete');
            $api->get('/orders-by-po', 'WorkOrderController@getOrdersByPO');
            $api->get('/{workOrderId:[0-9]+}/print', 'WorkOrderController@printWorkOrder');

            $api->put('/upload-dms', 'WorkOrderController@uploadFileToDMS');
        });
        //Customer Profile
        $api->group(['prefix' => 'customer-profile'], function($api){
            $api->post('/update-currency-by-warehouse', 'CustomerProfileController@updateCurrencyByWarehouse');
            $api->get('/get-currency-by-warehouse/{cusId:[0-9]+}', 'CustomerProfileController@getCurrencyByWarehouse');

            //update Tax for customer config
            $api->post('/update-tax-by-warehouse', 'CustomerProfileController@updateTaxByWarehouse');
            $api->get('/get-tax-by-warehouse/{cusId:[0-9]+}', 'CustomerProfileController@getTaxByWarehouse');
        });
        //Billable Item
        $api->group(['prefix' => 'billableitem'], function ($api) {
            // queue job
            $api->post('/goodsreceipt', 'BillableItemController@storeGoodReceipt');
            $api->post('/storage', 'BillableItemController@storeStorage');
            $api->get('/search', 'BillableItemController@search');
            $api->get('/timerange', 'BillableItemController@getTimeRange');
            $api->post('/outbound', 'BillableItemController@storeOutbound');
            $api->post('/transfer', 'BillableItemController@storeTransfer');
            $api->delete('/delete-multi', 'BillableItemController@deleteMulti');
            //Queue Put Pack
            $api->post('/put-back', 'BillableItemController@createBillableItemViaPutPack');
            $api->get('/search/refnum/{refNum}', 'BillableItemController@searchByRefNum');


            $api->post('/store-na', 'BillableItemController@storeNA');
            $api->get('/autocomplete-sac', 'BillableItemController@autocompleteSac');


            $api->delete('/delete-billable-by-month', 'BillableItemController@deleteStorageBillableByMonth');

        });

        //Invoice
        $api->group(['prefix' => 'invoice'], function($api){
            $api->post('/create', 'InvoiceController@store');
            $api->get('/create', 'InvoiceController@create');
            $api->get('list', 'InvoiceController@index');
            $api->get('show/{invId}', 'InvoiceController@show');
            $api->get('edit/{invId}', 'InvoiceController@edit');
            $api->put('update/{invId}', 'InvoiceController@update');
            $api->put('delete/{invId}', 'InvoiceController@destroyInvoiceDetail');
            $api->put('destroy', 'InvoiceController@destroy');
            $api->get('billable-item/{cusId}', 'InvoiceController@getBillableItem');
            $api->get('customer', 'InvoiceController@getCreatedByInInvoiceHeader');
            $api->put('approve', 'InvoiceController@approve');
            $api->get('get-total','InvoiceController@getTotal');
            //$api->get('print/{invId}', 'InvoiceController@print');
            $api->get('print/{invId:[0-9]+}', 'InvoiceController@printInvoice');
            $api->put('change-status/{invId}', 'InvoiceController@changeStatus');
            $api->post('send-email/{invId}', 'InvoiceController@sendEmail');
            $api->get('term/{cusId}', 'InvoiceController@getInvoiceTerm');
        });

        // Report
        $api->group(['prefix' => 'report'], function ($api) {
            //Invoice Report
            $api->get('/search', 'ReportController@search');
            $api->get('/export', 'ReportController@export');

            //Invoice Aging Report
            $api->get('/aging/search', 'ReportController@agingSearch');
        });

        // Transaction
        $api->group(['prefix' => 'transaction'], function($api){
            $api->get('/search', 'TransactionController@search');
            $api->get('/method', 'TransactionController@method');
            $api->post('/', 'TransactionController@add');
        });

        // Email Smtp
        $api->group(['prefix' => 'customer'], function($api){
            $api->get('/{type}/{cusId}', 'CustomerConfigController@showConfig');
            $api->put('/{type}/{cusId}', 'CustomerConfigController@updateConfig');
        });

        // Email Smtp
        $api->group(['prefix' => 'email'], function($api){
            $api->get('/{whsId}', 'EmailConfigurationController@show');
            $api->put('/{whsId}', 'EmailConfigurationController@update');
        });



        $api->get("/export-pdf", "InvoicePdfController@exportPdf");
        $api->get("/test-template", function () {
            return view('pdf.invoice');
        });
    });
});
