<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PutBackModel;
use App\Api\V1\Models\ReturnDtlModel;
use App\Api\V1\Models\ReturnHdrModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Traits\ReturnControllerTrait;
use App\Api\V1\Transformers\ItemAssignTransformer;
use App\Api\V1\Transformers\PalletSuggestTransformer;
use App\Api\V1\Transformers\ReturnHdrTransformer;
use App\Api\V1\Validators\PalletAssignValidator;
use App\Api\V1\Validators\ReturnHdrValidator;
use App\Api\V1\Validators\UpdatePutBackValidator;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;

/**
 * Class ReturnHdrController
 *
 * @package App\Api\V1\Controllers
 */
class ReturnHdrController extends AbstractController
{
    use ReturnControllerTrait;

    /**
     * @var ReturnHdrModel
     */
    protected $returnHdrModel;

    /**
     * @var ReturnDtlModel
     */
    protected $returnDtlModel;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var PutBackModel
     */
    protected $putbackModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var InventorySummaryModel
     */
    protected $invtSmrModel;

    /**
     * ReturnHdrController constructor.
     *
     * @param ReturnHdrModel $returnHdrModel
     * @param ReturnDtlModel $returnDtlModel
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param OrderCartonModel $orderCartonModel
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param PutBackModel $putBackModel
     * @param ItemModel $itemModel
     * @param InventorySummaryModel $inventorySummaryModel
     */
    public function __construct(
        ReturnHdrModel $returnHdrModel,
        ReturnDtlModel $returnDtlModel,
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        OrderCartonModel $orderCartonModel,
        PalletModel $palletModel,
        CartonModel $cartonModel,
        LocationModel $locationModel,
        PutBackModel $putBackModel,
        ItemModel $itemModel,
        InventorySummaryModel $inventorySummaryModel
    ) {
        $this->returnHdrModel = $returnHdrModel;
        $this->returnDtlModel = $returnDtlModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->orderCartonModel = $orderCartonModel;
        $this->palletModel = $palletModel;
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->putbackModel = $putBackModel;
        $this->itemModel = $itemModel;
        $this->invtSmrModel = $inventorySummaryModel;
    }

    /**
     * @param Request $request
     * @param ReturnHdrValidator $returnHdrValidator
     * @param ReturnHdrTransformer $returnHdrTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(
        Request $request,
        ReturnHdrValidator $returnHdrValidator,
        ReturnHdrTransformer $returnHdrTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $returnHdrValidator->validate($input);

        try {
            $returnHdrMax = $this->returnHdrModel->getFirstMax();
            $returnNum = SelStr::getHdrNum(object_get($returnHdrMax, 'return_num'), "PBR", 5);

            $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $input['odr_hdr_id']);
            $returnModel2 = new \App\Api\V2\Putback\Models\ReturnHdrModel();
            $returnData = $returnModel2->getItemForAssign($orderHdr->odr_id);
            $returnQty = array_column($returnData, 'picked_qty');

            // WMS2-5062 - [Outbound - Putback][WEB] Show error message if it exist RFID carton
            $this->_checkOrderHasRfidCarton($orderHdr->odr_id);
            $params = [
                'return_num'  => $returnNum,
                'odr_hdr_num' => $orderHdr->odr_num,
                'odr_hdr_id'  => $orderHdr->odr_id,
                'return_sts'  => 'NW',
                'return_qty'  => array_sum($returnQty),
                'sts'         => 'i',
            ];

            DB::beginTransaction();
            $returnHdr = $this->returnHdrModel->create($params);

            // Update Order Cartons Status to RETURNING
            $this->orderCartonModel->refreshModel();
            $this->orderCartonModel->updateWhere(['ctn_sts' => "RT"], ["odr_hdr_id" => $orderHdr->odr_id]);
            DB::commit();

            return $this->response->item($returnHdr, $returnHdrTransformer)->setStatusCode(Response::HTTP_CREATED);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param PalletSuggestTransformer $palletSuggestTransformer
     *
     * @return Response|void
     */
    public function suggestLpn(
        Request $request,
        PalletSuggestTransformer $palletSuggestTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        try {

            $suggests = $this->palletModel->suggestPallet($input);

            return $this->response->collection($suggests, $palletSuggestTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $returnId
     * @param Request $request
     * @param PalletAssignValidator $palletAssignValidator
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function assignPallet(
        $returnId,
        Request $request,
        PalletAssignValidator $palletAssignValidator
    ) {
        set_time_limit(0);
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['return_id'] = $returnId;

        $palletAssignValidator->validate($input);

        try {

            $returnHdr = $this->returnHdrModel->getFirstBy("return_id", $input['return_id'], [
                'createdUser',
                'odrHdr.customer',
                'putterUser'
            ]);

            // Assign
            DB::beginTransaction();
            $this->assignPB($returnHdr, $input);
            DB::commit();

            // Print
            $printed = $this->printPB($returnHdr);
            if ($printed !== true) {
                return $this->response->noContent()->setContent([
                    'message' => 'Assign Successful!. But ' . $printed,
                    'data'    => []
                ])->setStatusCode(Response::HTTP_OK);
            }

            return $this->response->noContent()->setContent([
                'message' => 'Assign Successful!',
                'data'    => []
            ])->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function assignPB($returnHdr, $input)
    {
        if ($returnHdr->return_sts == 'AS') {
            throw new \Exception("PutBack Assigned!");
        }

        $pltIds = [];
        foreach ($input['items'] as $itemInput) {

            $item = $this->itemModel->getFirstBy('item_id', $itemInput['item_id']);

            foreach ($itemInput['pallets'] as $detail) {
                // Create Detail
                $returnDtl = $this->createReturnDtl($returnHdr, $detail);
                $itemInput['piece_qty'] = $returnDtl->piece_qty;

                // Assign Carton to Pallet
                $pallet = $this->createCartonAndAssign($returnHdr, $detail, $itemInput, $returnDtl, $item);
                $pltIds = array_merge($pallet, $pltIds);
            }
        }

        // Update Return
        $this->returnHdrModel->refreshModel();
        $this->returnHdrModel->update([
            'return_id'  => $returnHdr->return_id,
            'return_sts' => 'AS',
        ]);

        // Update ctn_ttl into Pallet
        $this->updatePalletCtnTtl($pltIds);
    }

    private function printPB($returnHdr)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        $info = [
            'cus_name'    => object_get($returnHdr, 'odrHdr.customer.cus_name'),
            'user_name'   => trim(object_get($returnHdr, 'createdUser.first_name') . " " .
                object_get($returnHdr, 'createdUser.last_name')),
            'whs_id'      => $whsId,
            'return_num'  => $returnHdr->return_num,
            'return_id'   => $returnHdr->return_id,
            'putter_id'   => object_get($returnHdr, 'putterUser.user_id'),
            'putter_name' => trim(object_get($returnHdr, 'putterUser.first_name') . " " .
                object_get($returnHdr, 'putterUser.last_name')),
        ];

        $putBacks = $this->putbackModel->findWhere(['return_id' => $returnHdr->return_id], ['pallet'])->toArray();

        $pallets = [];
        if (!empty($putBacks[0]) && $putBacks[0]['sts'] == 'u') {
            foreach ($putBacks as $putBack) {
                $pallets[] = [
                    'plt_num'          => array_get($putBack, 'pallet.plt_num'),
                    'ctn_ttl'          => array_get($putBack, 'pallet.ctn_ttl'),
                    'suggest_loc_code' => array_get($putBack, 'suggest_loc_code'),
                    'actual_loc_code'  => array_get($putBack, 'actual_loc_code')
                ];
            }
        } else {
            // Suggest Data
            $dataSuggest = $this->suggestLocation(object_get($returnHdr, 'odrHdr.cus_id'), $whsId);

            $pallets = $this->palletModel->findWhere(['return_id' => $returnHdr->return_id])->toArray();

            if (count($dataSuggest) < count($pallets)) {
                return "There are not enough locations to suggest! Please add more location.";
            }

            DB::beginTransaction();
            // Suggest Location
            foreach ($pallets as $key => $pallet) {
                // Pallet get one location.
                foreach ($dataSuggest as $keyLoc => $location) {
                    $pallets[$key]['location'] = $location;
                    unset($dataSuggest[$keyLoc]);
                    break;
                }

                // Update PutBack Data Suggested
                $this->putbackModel->refreshModel();
                $pubBack = $this->putbackModel->getFirstWhere([
                    'return_id' => $returnHdr->return_id,
                    'pallet_id' => $pallet['plt_id']
                ]);
                $this->putbackModel->refreshModel();
                $this->putbackModel->update([
                    'pb_id'            => $pubBack->pb_id,
                    'suggest_loc_id'   => array_get($pallets[$key]['location'], 'loc_id', null),
                    'suggest_loc_code' => array_get($pallets[$key]['location'], 'loc_code', null),
                ]);
            }

            // Update Return
            $this->returnHdrModel->refreshModel();
            $this->returnHdrModel->update([
                'return_id'  => $returnHdr->return_id,
                'return_sts' => 'SG',
                'sts'        => 'u',
            ]);

            DB::commit();
        }

        // Print PDF
        $this->printPDF($pallets, $info);

        return true;
    }

    /**
     * @param $returnId
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function printPutBack($returnId)
    {
        try {
            $returnHdr = $this->returnHdrModel->getFirstBy("return_id", $returnId, ['putterUser']);

            if (empty($returnHdr->return_id)) {
                throw new \Exception(Message::get("BM017", "PutBack"));
            }

            // Print
            $printed = $this->printPB($returnHdr);
            if ($printed !== true) {
                return $this->response->errorBadRequest($printed);
            }

            return $this->response->noContent()->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $returnId
     * @param Request $request
     * @param UpdatePutBackValidator $updatePutBackValidator
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function updatePutBack(
        $returnId,
        Request $request,
        UpdatePutBackValidator $updatePutBackValidator
    ) {
        set_time_limit(0);
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['return_id'] = $returnId;

        $updatePutBackValidator->validate($input);

        $returnHdr = $this->returnHdrModel->getFirstBy("return_id", $input['return_id']);

        try {

            DB::beginTransaction();
            $locIds = array_pluck($input['items'], 'loc_id');
            $pallets = DB::table('pallet')->whereIn('loc_id', $locIds)->first();

            if (!empty($pallets)) {
                throw new \Exception("Some locations already have pallets.");
            }

            $dataUpdateIvt = [];
            foreach ($input['items'] as $item) {
                $data = $item;
                // Update PutBack
                $putBack = $this->putbackModel->getFirstWhere(['pb_id' => $data['pb_id']]);

                if (!empty($putBack)) {

                    $putBackParam = [
                        'pb_id'           => $putBack->pb_id,
                        'actual_loc_id'   => $data['loc_id'],
                        'actual_loc_code' => $data['loc_code'],
                        'sts'             => 'u',
                        'pb_sts'          => 'CO',
                    ];

                    $this->putbackModel->refreshModel();
                    $this->putbackModel->update($putBackParam);

                    // Update Carton
                    $data['plt_id'] = $putBack->pallet_id;
                    $data['item_id'] = $putBack->item_id;
                    $data['lot'] = object_get($putBack, 'lot', 'NA');
                    $this->updateCarton($data);

                    // Update Pallet
                    $this->updatePallet($data);
                }


                // Update Status Return Detail
                $this->returnDtlModel->refreshModel();
                $returnDtl = $this->returnDtlModel->update([
                    'return_dtl_id'  => $data['return_dtl_id'],
                    'return_dtl_sts' => 'CO'
                ]);

                $data['return_qty'] = $returnDtl->piece_qty;
                $dataUpdateIvt[$data['item_id'] . "-" . $data['lot']] = $data;
            }

            // Update Inventory Summary
            if (!empty($dataUpdateIvt)) {
                foreach ($dataUpdateIvt as $item => $data) {
                    $this->updateInvtSmr($data);
                }
            }

            // Update Return Hdr
            $this->returnHdrModel->refreshModel();
            $this->returnHdrModel->update([
                'return_id'  => $returnId,
                'return_sts' => 'CO'
            ]);

            // Update Order Cartons
            $this->orderCartonModel->refreshModel();
            $this->orderCartonModel->updateWhere(['ctn_sts' => 'PB'], ['odr_hdr_id' => $returnHdr->odr_hdr_id]);
            DB::commit();

            return $this->response->noContent()->setContent(["message" => "Update Successful!"])->setStatusCode
            (Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $returnId
     * @param ItemAssignTransformer $itemAssignTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function getItem(
        $returnId,
        ItemAssignTransformer $itemAssignTransformer
    ) {
        $returnHdr = $this->returnHdrModel->getFirstBy("return_id", $returnId);

        if (empty($returnHdr)) {
            return $this->response->noContent()->setStatusCode(Response::HTTP_OK);
        }

        try {
            $cartons = $this->cartonModel->getItemForAssign($returnHdr->odr_hdr_id, ['item']);

            return $this->response->collection($cartons, $itemAssignTransformer)->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param array $pltIds
     *
     * @return bool
     */
    private function updatePalletCtnTtl(array $pltIds)
    {
        if (empty($pltIds)) {
            return false;
        }

        $cartons = DB::table('cartons')
            ->select([DB::raw("count(ctn_id) as ctn_ttl"), 'plt_id'])
            ->whereIn('plt_id', $pltIds)
            ->where(['deleted_at' => 915148800, 'deleted' => 0])
            ->groupBy('plt_id')
            ->get();

        $cartons = array_pluck($cartons, 'ctn_ttl', 'plt_id');
        $cartonKeys = array_keys($cartons);
        $set = array_map(function ($plt_id, $ctn_ttl) {
            return "WHEN $plt_id THEN $ctn_ttl";
        }, $cartonKeys, $cartons);
        $set = implode(" ", $set);
        $in = implode(",", $cartonKeys);

        $sql = "UPDATE pallet set ctn_ttl = CASE plt_id $set END WHERE plt_id in ($in)";

        DB::statement($sql);

        return false;
    }

    private function _checkOrderHasRfidCarton($odrId)
    {
        $isExistRfid = DB::table('odr_hdr_meta')
            ->where('odr_id', $odrId)
            ->where('qualifier', 'ORC')
            ->first();

        if (count($isExistRfid)) {
            $msg = 'Unable return Order exist RFID carton on WEB. You should use Putback feature on WAP or GUN.';
            return $this->response->errorBadRequest($msg);
        }

    }
}
