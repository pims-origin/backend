<?php

// Shipping Lane

// suggest more location for shipping lane
$api->get('/more-shipping-lane-loc', [
    'action' => 'shippingLane',
    'uses'   => 'SugMoreLocShippingLaneController@moreSuggestShippingLocation'
]);

// Assign Order to Shipping lane
$api->put('/order-shipping-lane', [
    'action' => 'order',
    'uses' => 'OrderToShippingLaneController@assignShippingLane'
]);

// Order Shipping
$api->post('/shipped-order', [
    'action' => 'order',
    'uses' => 'OrderToShippingLaneController@shippedOrder'
]);

$api->post('/shipping-lane', [
    'action' => 'order',
    'uses' => 'OrderToShippingLaneController@shippingLane'
]);

$api->get('/suggest-shipping-lane-loc', [
    'action' => 'shippingLane',
    'uses'   => 'SugMoreLocShippingLaneController@suggestShippingLocation'
]);