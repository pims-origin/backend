<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\ReturnHdr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class OrderCartonModel extends AbstractModel
{

    protected $model;

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    /**
     * @param $data
     */
    public function getOrderCarton($data)
    {
        return $this->model
            ->where('odr_hdr_id', $data['odr_hdr_id'])
            ->first();
    }

    /**
     * @param $odr_id
     * @param $whs_id
     *
     * @return mixed
     */
    public function getOrderCartonBy($odr_id, $whs_id)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $order_cartons = DB::table('odr_cartons as oc')->select([
//            'oc.odr_dtl_id',
            'op.plt_num',
            'oc.sku',
            'c.des',
            'oc.lot',
            'oc.uom_name',
            'oc.pack',
            'oc.loc_id',
            'oc.pack',
            'oc.loc_code',
            DB::raw("COUNT(oc.ctn_id) AS ctns_ttl"),
            DB::raw("SUM(oc.piece_qty) AS piece_qty_ttl"),
            'c.updated_at',
            'c.created_by',
            'u.first_name',
            'u.last_name',
        ])
            ->leftJoin('cartons as c', 'c.ctn_id', '=', 'oc.ctn_id')
            //->leftJoin('pallet as p', 'p.plt_id', '=', 'c.plt_id')
            ->leftJoin('users as u', 'u.user_id', '=', 'oc.created_by')
            ->leftJoin('pack_hdr', 'pack_hdr.odr_hdr_id', '=', 'oc.odr_hdr_id')
            ->leftJoin('out_pallet as op', 'op.plt_id', '=', 'pack_hdr.out_plt_id')
            ->where('oc.odr_hdr_id', $odr_id)
            ->where('oc.whs_id', $whs_id)
            ->groupBy('oc.odr_dtl_id');
//        ->get();

        $new_sku = DB::table('odr_cartons as oc')
            ->select([
                DB::raw('NULL'),
                'i.sku',
                DB::raw('NULL'),
                DB::raw('NULL'),
                'i.uom_name',
                'i.pack',
                DB::raw('NULL'),
                'i.pack',
                DB::raw('NULL'),
                DB::raw('NULL'),
                DB::raw('NULL'),
                'i.updated_at as updated_at',
                'i.created_by as created_by',
                'u.first_name',
                'u.last_name'
            ])
            ->leftJoin('item_child as ic', 'ic.origin', '=', 'oc.item_id')
            ->leftJoin('item as i', 'i.item_id', '=', 'ic.parent')
            ->leftJoin('users as u', 'u.user_id', '=', 'i.created_by')
            ->where('oc.odr_hdr_id', $odr_id)
            ->where('oc.whs_id', $whs_id)
            ->groupBy('oc.odr_dtl_id');

        $query = $order_cartons->union($new_sku)->get();

        return $query;
    }

    /**
     * @param $odr_id
     *
     * @return mixed
     */
    public function loadAllByOdrId($odr_id)
    {
        return $this->model
            ->select([DB::raw('sum(piece_qty) as piece_ttl'), DB::raw('count(odr_ctn_id) as ctn_ttl')])
            ->where('odr_hdr_id', $odr_id)
            ->groupBy('odr_hdr_id')
            ->first();
    }

    /**
     * @param $odr_dtl_id
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function loadCartonToAssign($odr_dtl_id, $with = [], $limit = null)
    {
        $query = $this->make($with)
            ->where('ctn_sts', 'RT')
            ->select(['odr_ctn_id', 'ctn_id'])
            ->where('odr_dtl_id', $odr_dtl_id);

        if (!empty($limit)) {
            $query->take($limit);
        }

        return $query->get();
    }

    public function getOdrDtlPickedQty($odrDtl) {
        return $this->model
            ->select([
                DB::raw('SUM(piece_qty) as picked_qty')
            ])
            ->where('odr_dtl_id',$odrDtl['odr_dtl_id'])
            ->where('wv_hdr_id',$odrDtl['wv_id'])
            ->groupBy('odr_dtl_id')
            ->first();
    }

    /**
     * @param $attributes
     * @param array $with
     * @param null $limit
     * @return mixed
     */
    public function loadCartonNotAssign($attributes, $with = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $rfid = array_get($attributes, 'rfid', '');
        $wv_id = array_get($attributes, 'wv_id');
        // Get current whs_id
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $query = $this->make($with)
            ->leftJoin('customer', 'odr_cartons.cus_id', '=', 'customer.cus_id')
            ->leftJoin('wv_dtl', 'odr_cartons.wv_dtl_id', '=', 'wv_dtl.wv_dtl_id')
            ->leftJoin('users', 'wv_dtl.updated_by', '=', 'users.user_id')
            ->leftJoin('pallet', 'odr_cartons.plt_id', '=', 'pallet.plt_id')
            ->leftJoin('item', 'odr_cartons.item_id', '=', 'item.item_id')
            ->where('odr_hdr_id', NULL)
            ->where('odr_dtl_id', NULL)
            ->where('odr_cartons.whs_id', $currentWH)
            ->where('odr_cartons.wv_hdr_id', $wv_id)
            ->select([
                'customer.cus_name',
                'odr_cartons.wv_num',
                'odr_cartons.ship_dt',
                'odr_cartons.sku',
                'odr_cartons.loc_code',
                'odr_cartons.lot',
                'odr_cartons.uom_name',
                'odr_cartons.pack',
                'odr_cartons.ctn_id',
                'odr_cartons.ctn_num',
                'odr_cartons.ctn_rfid',
                'odr_cartons.piece_qty',
                'item.description',
                'pallet.plt_num',
                // DB::raw("FROM_UNIXTIME(wv_dtl.updated_at) as picked_at"),
                'wv_dtl.updated_at as picked_at',
                DB::raw('concat(first_name, " ", last_name) as picked_by')
            ]);

        if(!empty($rfid)){
            $query->where('ctn_rfid', '!=', NULL);
        }else{
            $query->where('ctn_rfid', NULL);
        }

        $this->sortBuilder($query, $attributes);
//        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }
}
