<?php
/**
 * Created by PhpStorm.
 *
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\OrderHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\OrderHdrMeta;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderDtlTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderDtlTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderDtl
     *
     * @return array
     */
    public function transform(OrderHdr $orderDtl)
    {
        $odrHdrMeta = OrderHdrMeta::where('odr_id', $orderDtl->odr_id)
            ->where('qualifier', 'OFP')
            ->first();

        return [
            // Customer
            'cus_id'               => $orderDtl->cus_id,
            'cus_name'             => object_get($orderDtl, 'customer.cus_name', null),
            'cus_odr_num'          => $orderDtl->cus_odr_num,


        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date ? date("m/d/Y", $date) : null;
    }
}
