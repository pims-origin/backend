<?php

namespace App\Api\V1\Models;

use \DateTime;
use App\Invoice;

use Seldat\Wms2\Models\InvoiceCost;
use Seldat\Wms2\Models\InvoiceVolumeRates;
use Seldat\Wms2\Models\InvoiceHistoryMonth;
use Seldat\Wms2\Models\ChargeCode;

use App\Api\V1\Models\InvoicingSummaryModel;

use Illuminate\Support\Facades\DB;


class CommonInvoiceModel extends AbstractModel
{

    /**
    * @var
    */

    public $customerUOMs = [];
    public $rates = [];
    public $custUOMs = [];
    public $volRates = [];

    protected $charge;
    protected $volume;
    protected $hisMonth;
    protected $invSumModel;

    static $dateTimes = [];

    /**
     * InvoiceCostModel ChargeCodeModel constructor.
     * @param InvoiceCost|null $model
     */

    public function __construct(
        InvoiceCost $model = null,
        ChargeCode $charge = null,
        InvoiceVolumeRates $volume = null,
        InvoiceHistoryMonth $hisMonth = null
    ) {
        $this->model = ($model) ?: new InvoiceCost();
        $this->charge = ($charge) ?: new ChargeCode();
        $this->volume = ($volume) ?: new InvoiceVolumeRates();
        $this->hisMonth = ($hisMonth) ?: new InvoiceHistoryMonth();
        
        $this->invSumModel = new InvoicingSummaryModel();
    }


    /**
     * @param array uomRes
     *
     * @return array
    */

    public function setRates($uomRes)
    {
        // Get each customers charge codes
        $this->rates = getDefault($uomRes['rates'], []);

        return $this;
    }

    public function rates()
    {
        return $this->rates;
    }

    /**
    * @param string category
    * @param int custID
    * @return array
    */

    public function custVolumeRates($category, $custID)
    {
        return getDefault($this->volRates[$category][$custID], []);
    }

    /**
    * @param string getCat
    *
    * @return array
    */

    public function customerUOMs($getCat=FALSE)
    {
        // Get all UOMs for all customers
        if ($getCat) {
            return getDefault($this->customerUOMs[$getCat], []);
        }

        foreach ($this->rates as $type => $custs) {
            foreach ($custs as $custID => $uoms) {
                foreach ($uoms as $uom) {
                    $this->customerUOMs[$type][$uom][] = $custID;
                }
            }
        }
        return $this;
    }

    /**
    * @param string getCat
    *
    * @return array
    */

    public function custUOMs($getCat=FALSE)
    {
        // Get all customers for each UOM
        if (isset($this->custUOMs[$getCat])) {
            return $this->custUOMs[$getCat];
        }

        foreach ($this->customerUOMs as $cat => $uomCusts) {
            if (! $uomCusts) {
                continue;
            }

            foreach ($uomCusts as $uom => $custs) {
                if (! $custs) {
                    continue;
                }

                foreach ($custs as $custID) {
                    $this->custUOMs[$cat][$custID][$uom] = TRUE;
                }
            }
        }

        return getDefault($this->custUOMs[$getCat], []);
    }

    /**
    * @param array  custIDs
    * @param bool uoms
    *
    * @return array
    */

    public function getCosts($whsID, $custIDs=[], $uoms=FALSE)
    {
       $query = $this->make(['chargeCode', 'chargeCode.chargeType', 'chargeCode.systemUom']);

       $cusCharge = $query->where(function ($q) use ($custIDs, $whsID) {
                if ($custIDs) {
                    $q->whereIn('cus_id', $custIDs);
                }

                if ($whsID) {
                    $q->where('whs_id', $whsID);
                }

                $q->where('chg_cd_price', '!=', 0);
       });


        $results = $cusCharge->get()->transform(function ($cusCharge) {
            return [
                'chg_cd' =>  object_get($cusCharge, 'chargeCode.chg_code', null),
                'chg_cd_name' =>  object_get($cusCharge, 'chargeCode.chg_code_name', null),
                'chg_cd_id' => object_get($cusCharge, 'chargeCode.chg_code_id', null),
                'chg_uom_id' => object_get($cusCharge, 'chargeCode.chg_uom_id', null),
                'chg_cd_desc' =>  object_get($cusCharge, 'chargeCode.chg_code_des', null),
                'chg_cd_price' => $cusCharge->chg_cd_price,
                'cust_id' =>  $cusCharge->cus_id,
                'sys_uom_code' => object_get($cusCharge, 'chargeCode.systemUom.sys_uom_code', null),
                'sys_uom_name' => object_get($cusCharge, 'chargeCode.systemUom.sys_uom_name', null),
                'chg_type_id' =>  object_get($cusCharge, 'chargeCode.chargeType.chg_type_id', null),
                'chg_cd_type' =>  object_get($cusCharge, 'chargeCode.chargeType.chg_type_name', null)

            ];
        });

        $return = [];

        foreach ($results as $value) {

            $vendorID = $value['cust_id'];
            $type = $value['chg_cd_type'];
            $chargeCode = $value['chg_cd_name'];

            $return['rates'][$type][$vendorID][$chargeCode] = $uoms ?
                $value['sys_uom_name'] : $value['chg_cd_price'];
            $return['info'][$type][$chargeCode] = $value;
        }

        return $return;
    }


    /**
    *
    * @return array
    */

    function getLaborCharge($cusId)
    {
        $results = [];
        $queryRes = $this->model
            ->select([
                'charge_code.chg_code',
                'charge_code.chg_code_name',
                'charge_code.chg_code_id',
                'charge_code.chg_uom_id',
                'charge_code.chg_code_des',
                'charge_type.chg_type_id',
                'charge_type.chg_type_name',
                'system_uom.sys_uom_code',
                'system_uom.sys_uom_name',
                'invoice_cost.chg_cd_price',
            ])
            ->join('charge_code', 'invoice_cost.chg_code_id', '=', 'charge_code.chg_code_id')
            ->join('charge_type', 'charge_code.chg_type_id', '=', 'charge_type.chg_type_id')
            ->join('system_uom', 'system_uom.sys_uom_id', '=', 'charge_code.chg_uom_id')
            ->where('system_uom.sys_uom_name', Invoice::$labor);
        if (!empty($cusId)) {
            $queryRes->where('invoice_cost.cus_id','=', $cusId);
        }
        $queryRes = $queryRes->get();
        $return = [];

        foreach ($queryRes as $value)
        {
            $results[] = [
                'chg_cd'        => $value->chg_code,
                'chg_cd_name'   => $value->chg_code_name,
                'chg_cd_id'     => $value->chg_code_id,
                'chg_uom_id'    => $value->chg_uom_id,
                'sys_uom_code'  => $value->sys_uom_code,
                'sys_uom_name'  => $value->sys_uom_name,
                'chg_type_id'   => $value->chg_type_id,
                'chg_cd_type'   => $value->chg_type_name,
                'chg_cd_desc'   => $value->chg_code_des,
                'chg_cd_price'  => $value->chg_cd_price,
                'cust_id' => 0,
            ];
        }

        foreach ($results as $value) {
            $chargeCode = $value['chg_cd_name'];

            $return['info'][$chargeCode] = $value;
        }

        return $return;
    }


    /**
     * get Billable dates
     *
     * @param array params
     * @param array recNums
     * @param string returnRecs
     * @return array
     */

    public function getBillableDts($params)
    {
        $custs = $params['custs'];
      
        $whsID = getDefault($params['dates']['whs_id']) ? $params['dates']['whs_id'] :
                        getdefault($params['whs_id']);

        $details = getDefault($params['details']);
        
        $sDate = $params['dates']['startDate'];

        if (! $custs) {
            return [];
        }

        $curDate = self::getDateTime('date');

        $curDtObj = new DateTime($curDate);
        $endDtObj = new DateTime($params['dates']['endDate']);

        $pInitial = $params['period'] == 'daily' ? 'D' : 'M';
        $format = $params['period'] == 'daily' ? 'Y-m-d' : 'Y-m';

        //check Startdate with rcvDate
        $rcvDate = $this->invSumModel->getRcvDate($custs, $whsID);
        
        $startDate = $sDate >= $rcvDate ? $sDate : $rcvDate;
        
        $daterange = new \DatePeriod(
            new DateTime($startDate),
            new \DateInterval('P1'.$pInitial),
            $endDtObj
        );


        $months = [];
        foreach($daterange as $date){
            $months[] = $date->format($format);
        }

        // Incase end date is the first of a month
        $months[] = $endDtObj->format($format);

        $curDTFormatted = $curDtObj->format($format);
        $diff = array_diff($months, [$curDTFormatted]);

        $unique = array_unique($diff);
        if (! $unique) {
            return [];
        }
        
        $select = $params['period'] == 'daily' ?
            'inv_date' : DB::raw('DATE_FORMAT(inv_date, "%Y-%m")');

        $query = $this->hisMonth
                        ->select('cus_id')
                        ->selectRaw($select . ' AS dt')
                        ->where('whs_id', $whsID)
                        ->whereIn($select, $unique)
                        ->where('type', $params['cat'])
                        ->where('inv_sts', 1);

        $monthResults = $custs ? $query->whereIn('cus_id', $custs)->get() : $query->get();

        $billedCustDts = [];
        foreach ($monthResults->toArray() as $row) {
            $custID = $row['cus_id'];
            $billedCustDts[$custID][] = $row['dt'];
        }


        if (isset($params['getBilled'])) {
            return $billedCustDts;
        }

        $day = $params['period'] == 'daily' ? NULL : '-01';

        $sums = $dts = [];
        foreach ($custs as $custID) {
            $custBilled = getDefault($billedCustDts[$custID], []);
            $found = array_diff($unique, $custBilled);

            $sums[$custID]['rcvCount'] = 0;

            foreach ($found as $dt) {
                $dt = $dt.$day;
                $dts[$custID][$dt] = 1;
                $sums[$custID]['rcvCount'] += 1;
            }
        }

        return $details ? $dts : $sums;
    }


    /**
    * @custDts array
    * @return array
    */

    static function formatDtsQty(&$custDts)
    {
        if (! $custDts) {
            return;
        }

        foreach ($custDts as &$details) {
            array_walk($details, function (&$row) {
                if (is_numeric($row['quantity'])) {
                    $row['quantity'] = number_format($row['quantity'], 3);
                }
                $row['ccTotal'] = round($row['ccTotal'], 2);
           });
        }
    }

    /*
    * getDatetime
    */

    static function getDateTime($name)
    {
        if (! isset(self::$dateTimes[$name])) {
            $format = NULL;
            switch ($name) {
                case 'currentTime':
                    self::$dateTimes[$name] = time();
                    return self::$dateTimes[$name];
                case 'date':
                    $format = 'Y-m-d';
                    break;
                case 'dateTime':
                    $format = 'Y-m-d H:i:s';
                    break;
                case 'time':
                    $format = 'H:i:s';
                    break;
                case 'numberDate':
                    $format = 'mdy';
                    break;
                default:
                    die('Invalid Date/Time');
            }

            self::$dateTimes[$name] = date($format);
        }

        return self::$dateTimes[$name];
    }

    /**
    * @chargeCodes array
    * @return array
    */

    public function getChargIDsByCodes($chargeCodes)
    {
        if (! $chargeCodes) {
            return [];
        }

        $results = $this->charge
                        ->select('chg_code_id', 'chg_code_name')
                        ->whereIn('chg_code_name', $chargeCodes)
                        ->get()
                        ->toArray();

        return $results;
    }
}

