<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class ReasonService extends BaseService
{
    public function search($queryStr)
    {
        $uri = $this->getUri();
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }

    protected function getUri()
    {
        return env('API_WAREHOUSE') . 'reasons';
    }

    /**
     * @param $params
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create($params)
    {
        $uri = $this->getUri();
        $options = [
            'form_params' => $params
        ];
        return $this->client->post($uri, $options);
    }

    /**
     * @param $params
     * @param $reasonId
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update($params, $reasonId)
    {
        $uri = $this->getUri() . '/' . $reasonId;
        $options = [
            'form_params' => $params
        ];
        return $this->client->put($uri, $options);
    }
}
