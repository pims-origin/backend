<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ChargeCodeModel;
use App\Api\V1\Models\ChargeTypeModel;
use App\Api\V1\Transformers\ChargeTypeTransformer;
use App\Api\V1\Validators\ChargeTypeValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\V1\Validators\ChargeTypeDeleteMassValidator;
use Seldat\Wms2\Utils\Message;

class ChargeTypeController extends AbstractController
{
    /**
     * @var $chargeTypeModel
     */
    protected $chargeTypeModel;

    /**
     * @var chargeTypeTransformer
     */
    protected $chargeTypeTransformer;

    /**
     * @var chargeTypeValidator
     */
    protected $chargeTypeValidator;

    /**
     * @var chargeTypeDeleteMassValidator
     */
    protected $chargeTypeDeleteMassValidator;

    /**
     * ChargeTypeController constructor.
     *
     */
    public function __construct()
    {
        $this->chargeTypeModel = new ChargeTypeModel();
        $this->chargeTypeTransformer = new ChargeTypeTransformer();
        $this->chargeTypeValidator= new ChargeTypeValidator();
        $this->chargeTypeDeleteMassValidator = new ChargeTypeDeleteMassValidator();
    }

    public function index()
    {
        try {
            $chargeType = $this->chargeTypeModel->all();

            return $this->response->collection($chargeType, $this->chargeTypeTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/v1/chargeTypes/{chargeTypeId}",
     *     summary="Read ChargeType",
     *     description="View Detail ChargeType",
     *     operationId="showChargeType",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="chargeTypeId",
     *         description="ChargeType Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return detail of ChargeType",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\ChargeTypes(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "chargeType 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * ChargeType Detail
     *
     * @param int $chargeTypeId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function show($chargeTypeId)
    {
        try {
            $chargeType = $this->chargeTypeModel->getFirstBy('chg_type_id', $chargeTypeId);

            return $this->response->item($chargeType, $this->chargeTypeTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/v1/chargeTypes/search",
     *     summary="Search ChargeType",
     *     description="Search ChargeType",
     *     operationId="searchChargeType",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Return all found result of ChargeType",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\ChargeTypes(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "chargeType 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     * )
     *
     * ChargeType Search
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     */
    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $chargeType = $this->chargeTypeModel->search($input, [], array_get($input, 'limit', 20));

            return $this->response->paginator($chargeType, $this->chargeTypeTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/chargeTypes/{chargeTypeId}",
     *     summary="Create ChargeType",
     *     description="Create ChargeType",
     *     operationId="createChargeType",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="chargeTypeId",
     *         description="ChargeType Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_name",
     *         description="ChargeType's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_status",
     *         description="ChargeType's State Type",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_short_name",
     *         description="ChargeType's Short Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of ChargeType",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\ChargeTypes(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "ChargeType 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * ChargeType Create
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->chargeTypeValidator->validate($input);

        $params = [
            'chg_type_code' => $input['chg_type_code'],
            'chg_type_des'  => array_get($input, 'chg_type_des', ''),
            'chg_type_name' => $input['chg_type_name'],
        ];

        try {
            // check duplicate chargeType
            if ($this->chargeTypeModel->loadBy(['chg_type_code' => $params['chg_type_code']])->first()) {
                return $this->response->errorBadRequest(Message::get("BM006", "Charge Type Code"));
            }

            if ($this->chargeTypeModel->loadBy(['chg_type_name' => $params['chg_type_name']])->first()) {
                return $this->response->errorBadRequest(Message::get("BM006", "Charge Type Name"));
            }

            if ($chargeType = $this->chargeTypeModel->create($params)) {
                return $this->response->item($chargeType,
                    $this->chargeTypeTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));

    }

    /**
     * @SWG\Put(
     *     path="/v1/chargeTypes/{chargeTypeId}",
     *     summary="Update ChargeType",
     *     description="Update ChargeType",
     *     operationId="updateChargeType",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="chargeTypeId",
     *         description="ChargeType Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_name",
     *         description="ChargeType's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_status",
     *         description="ChargeType's State Type",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_short_name",
     *         description="ChargeType's Short Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of ChargeType",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\ChargeTypes(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_id": 2,
     *                     "whs_name": "chargeType 2",
     *                     "whs_status": "IA",
     *                     "whs_short_name": null
     *                 },
     *             },
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * @param int $chargeTypeId
     * @param Request $request
     *
     * @return mixed
     * @internal param int $whs_con_id
     */
    public function update(
        $chargeTypeId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['chg_type_id'] = $chargeTypeId;
        // validation
        $this->chargeTypeValidator->validate($input);

        $params = [
            'chg_type_id'   => $chargeTypeId,
            'chg_type_code' => $input['chg_type_code'],
            'chg_type_des'  => !empty($input['chg_type_des']) ? $input['chg_type_des'] : '',
            'chg_type_name' => $input['chg_type_name'],
        ];

        try {

            // Check not exist
            if (!$this->chargeTypeModel->loadBy(['chg_type_id' => $chargeTypeId])->first()) {
                return $this->response->errorBadRequest(Message::get("BM017", "Charge Type"));
            }

            // check duplicate chargeType
            $chargeType = $this->chargeTypeModel->loadBy(['chg_type_code' => $params['chg_type_code']])->first();
            if (!empty($chargeType) && $chargeType->chg_type_id != $chargeTypeId) {
                return $this->response->errorBadRequest(Message::get("BM006", "Charge Type Code"));
            }

            $chargeType = $this->chargeTypeModel->loadBy(['chg_type_name' => $params['chg_type_name']])->first();
            if (!empty($chargeType) && $chargeType->chg_type_id != $chargeTypeId) {
                return $this->response->errorBadRequest(Message::get("BM006", "Charge Type Name"));
            }

            if ($chargeType = $this->chargeTypeModel->update($params)) {
                return $this->response->item($chargeType, $this->chargeTypeTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Delete(
     *     path="/v1/chargeType/{chargeTypeId}",
     *     summary="Delete ChargeType",
     *     description="Delete a ChargeType",
     *     operationId="updateContact",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="chargeTypeId",
     *         description="ChargeType Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="ChargeType was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param int $chargeTypeId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_con_id
     * @internal param MenuModel $menuModel
     *
     */
    public function destroy($chargeTypeId)
    {
        try {
            // Check ChargeType exist in ChargeCode.
            $chargeCodeModel = new ChargeCodeModel();
            $chargeCode = $chargeCodeModel->loadBy(['chg_type_id' => $chargeTypeId])->first();
            if (!empty($chargeCode)) {
                return $this->response->errorBadRequest('Please Delete ChargeCode first.');
            }

            if ($this->chargeTypeModel->deleteChargeType($chargeTypeId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM003", "Charge Type"));
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->chargeTypeDeleteMassValidator->validate($input);

        $input['chg_type_id'] = is_array($input['chg_type_id']) ? $input['chg_type_id'] : [$input['chg_type_id']];

        try {
            // Check ChargeType exist in ChargeCode.
            $chargeCodeModel = new ChargeCodeModel();
            $chargeCode = $chargeCodeModel->loadByMany(['chg_type_id' => $input['chg_type_id']])->first();
            if (!empty($chargeCode)) {
                return $this->response->errorBadRequest('Please Delete ChargeCode first.');
            }

            $this->chargeTypeModel->deleteMassChargeType($input['chg_type_id']);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }
}
