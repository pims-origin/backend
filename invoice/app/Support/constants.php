<?php

define('CURRENCY_CODE', 'USD');


define('INVOICE_TYPES', [
    'o' => 'Original',
    'c' => 'Cancellation',
]);

define('INVOICE_TYPE_ORIGINAL', [
    'code' => 'o',
    'name' => 'Original',
]);

define('INVOICE_TYPE_CANCELLATION', [
    'code' => 'c',
    'name' => 'Cancellation',
]);


define('INVOICE_STATUS_OPEN', [
    'code' => 'o',
    'name' => 'Open',
]);

define('INVOICE_STATUS_INVOICED', [
    'code' => 'i',
    'name' => 'Invoiced',
]);

define('INVOICE_STATUS_PAID', [
    'code' => 'p',
    'name' => 'Paid',
]);

define('INVOICE_STATUS_CANCELLED', [
    'code' => 'c',
    'name' => 'Cancelled',
]);

define('INVOICE_STATUSES', [
    'o' => 'Open',
    'i' => 'Invoiced',
    'p' => 'Paid',
    'c' => 'Cancelled',
]);

define('INVOICE_MONTHLY_UOM', [
    'MONTHLY_LARGE_CARTON',
    'MONTHLY_MEDIUM_CARTON',
    'MONTHLY_PALLET',
    'MONTHLY_SMALL_CARTON',
    'MONTHLY_VOLUME',
    'MONTHLY_XL_CARTON',
    'MONTHLY_XXL_CARTON',
]);

define('INVOICE_CURRENT_UOM', [
    'CARTON_CURRENT',
    'PALLET_CURRENT',
    'VOLUME_CURRENT',
]);

define('PAGING_LIMIT', 20);

define('MEASURE', 1);

