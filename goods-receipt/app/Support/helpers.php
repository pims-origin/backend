<?php

use Carbon\Carbon;

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     *
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}
if (!function_exists('getDefaultDatetimeDeletedAt')) {
    function getDefaultDatetimeDeletedAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', '1999-01-01 00:00:00', 'UTC')->timestamp;
    }
}

if(!function_exists('public_path'))
{
    /**
     * Return the path to public dir
     * @param null $path
     * @return string
     */
    function public_path($path=null)
    {
        return rtrim(app()->basePath('public/'.$path), '/');
    }
}

if (! function_exists('convertDateByFormat'))
{
    function convertDateByFormat($date, $format = 'Y-m-d')
    {
        if (!$date) {
            $date = Carbon::now()->toDateString();
        }
        return Carbon::parse($date)->format($format);
    }
}

if (! function_exists('convertTimestampToDate'))
{
    function convertTimestampToDate($timestamp, $format = 'Y-m-d')
    {
        if (!$timestamp) {
            $timestamp = Carbon::now()->timestamp;
        }
        return Carbon::createFromTimestamp($timestamp)->format($format);
    }
}