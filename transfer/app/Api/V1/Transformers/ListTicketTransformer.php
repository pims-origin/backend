<?php

namespace App\Api\V1\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseTransfer;
use Seldat\Wms2\Models\XferDtl;
use Seldat\Wms2\Models\XferHdr;
use Seldat\Wms2\Models\XferTicket;
use Seldat\Wms2\Utils\Status;

class ListTicketTransformer extends TransformerAbstract
{

    /**
     * @param XferHdr $XferHdr
     *
     * @return array
     */
    public function transform(XferHdr $XferHdr)
    {
        $newLoc = $XferHdr->xferDtl()->first();
        $cus_info = DB::table('customer')->where('cus_id', '=', $XferHdr->cus_id)->select('cus_name')->first();

        return [
            'xfer_ticket_sts'      => object_get($XferHdr, 'xferTicket.xfer_ticket_sts', ''),
            'xfer_ticket_sts_name' => Status::getByKey("XFER-STATUS",
                object_get($XferHdr, 'xferTicket.xfer_ticket_sts', '')),
            'xfer_ticket_id'       => object_get($XferHdr, 'xfer_ticket_id', ''),
            'xfer_ticket_num'      => object_get($XferHdr, 'xferTicket.xfer_ticket_num', ''),
            'xfer_hdr_id'          => object_get($XferHdr, 'xfer_hdr_id', ''),
            'xfer_hdr_num'         => object_get($XferHdr, 'xfer_hdr_num', ''),
            'xfer_sts'             => object_get($XferHdr, 'xfer_sts', ''),
            'item_id'              => object_get($XferHdr, 'item_id', ''),
            'cus_upc'              => object_get($XferHdr, 'cus_upc', ''),
            'cus_name'             => array_get($cus_info, 'cus_name', null),
            'sku'                  => object_get($XferHdr, 'sku', ''),
            'size'                 => object_get($XferHdr, 'size', ''),
            'color'                => object_get($XferHdr, 'color', ''),
            'lot'                  => object_get($XferHdr, 'lot', ''),
            'pieces'               => object_get($XferHdr, 'piece_qty', ''),
            'nw_loc'               => object_get($newLoc, 'mez_loc', null),
            'nw_loc_id'            => object_get($newLoc, 'mez_loc_id', null),
        ];
    }
}
