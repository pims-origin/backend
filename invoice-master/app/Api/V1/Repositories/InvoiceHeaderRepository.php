<?php

namespace App\Api\Respositories;

use App\InvoiceDetail;
use App\InvoiceHeader;
use App\BillableDtl;
use Illuminate\Http\Request as IlluminateRequest;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\Warehouse;
use App\Api\V1\Models\CustomerConfigModel;

class InvoiceHeaderRepository extends BaseRepository
{
    protected $customerConfig;

    public function __construct(InvoiceHeader $model)
    {
        $this->model = $model;
        $this->customerConfig = new CustomerConfigModel;
    }
    public function createInvoice(IlluminateRequest $request)
    {
        $bills = $this->mapAddressFill($request->get('whs_id'), $request->get('cus_id'));

        $request->merge(array_merge([
            'inv_dt'  => $request->get('inv_dt', ''),
            'inv_sts' => $request->get('inv_sts', 'NE'),
        ], $bills));

        return DB::transaction(function () use ($request) {
            if (!$invoiceHeader = $this->model->create($request->all())) {
                throw new \Exception('Can\'t create Invoice Detail!');
            }
            $invDetails    = $this->appendInvoiceDatail($request->get('items'), $request->get('inv_sts'));
            $invoiceDetail = $invoiceHeader->detail()->createMany($invDetails);

            if ($invoiceDetail->isEmpty()) {
                throw new \Exception('Can\'t create Invoice Detail!');
            } else {
                BillableDtl::whereIn('ba_dtl_id', $invoiceDetail->pluck('ba_dtl_id'))->delete();
            }

            return true;
        });

    }

    public function destroyInvoice($request)
    {
        return DB::transaction(function () use ($request) {
            $invIds = $request->get('inv_ids');
            foreach ($invIds as $id) {
                $inv = InvoiceHeader::where([
                    ['inv_id', $id],
                    ['inv_sts', 'NE']
                ])->first();

                if (!$inv) {
                    throw new \Exception("Please choose New Invoice(s) to delete.");
                }

                $invoiceDetail = $inv->detail()->get();
                BillableDtl::whereIn('ba_dtl_id', $invoiceDetail->pluck('ba_dtl_id'))->restore();
                InvoiceDetail::whereIn('inv_dtl_id', $invoiceDetail->pluck('inv_dtl_id'))->delete();
            }

            return $this->model->whereIn('inv_id', $invIds)->delete();
        });
    }

    public function destroyInvoiceDetail($invHdrId, IlluminateRequest $request)
    {
        return DB::transaction(function () use ($invHdrId, $request) {
            $invoiceHeader = $this->model->findOrFail($invHdrId);
            $invoiceDetail = $invoiceHeader->detail()->get()->whereIn('inv_dtl_id', $request->get('inv_dtl_ids'));
            BillableDtl::whereIn('ba_dtl_id', $invoiceDetail->pluck('ba_dtl_id'))->restore();
            return $invoiceHeader->detail()
                ->whereIn('inv_dtl_id', $request->get('inv_dtl_ids'))
                ->delete();
        });
    }

    public function updateInvoiceDetail($invHdrId, IlluminateRequest $request)
    {
        return DB::transaction(function () use ($invHdrId, $request) {
            $invoiceHeader = $this->model->findOrFail($invHdrId);
            foreach ($request->get('items') as $invoiceDetail) {
                $invDtlId = ['inv_dtl_id' => $invoiceDetail['inv_dtl_id'] ?? ''];
                if (!empty($invoiceDetail['inv_dtl_id'])) {
                    $invoiceHeader->detail()
                        ->where('inv_dtl_id', $invoiceDetail['inv_dtl_id'])
                        ->update([
                            'qty'    => $invoiceDetail['qty'],
                            'amount' => $invoiceDetail['amount'],
                        ]);
                } else {
                    $invoiceHeader->detail()->create($invoiceDetail);
                    BillableDtl::where('ba_dtl_id', $invoiceDetail['ba_dtl_id'])->delete();
                }
            }
            $invoiceHeaderData = [
                'total'    => $request->get('total'),
                'subtotal' => $request->get('subtotal'),
                'note'     => $request->get('note'),
                'discount' => $request->get('discount'),
                'tax'      => $request->get('tax'),
                'inv_sts'  => $request->get('inv_sts', 'NE'),
            ];

            return $invoiceHeader->update($invoiceHeaderData);

        });
    }

    private function appendInvoiceDatail($params, $invStatus)
    {
        foreach ($params as $param) {
            $data[] = array_merge($param, ['inv_sts' => $invStatus]);
        }

        return $data ?? [];
    }

    private function mapAddressFill($whsId, $cusId)
    {
        $customer  = $this->getCustomerAndCustomerAddress($cusId);
        $warehouse = $this->getWareHouseAndWareHouseAddress($whsId);

        return [
            'bill_from_name'      => $warehouse->whs_name ?? '',
            'bill_from_add1'      => $warehouse->warehouseAddress->whs_add_line_1 ?? '',
            'bill_from_add2'      => $warehouse->warehouseAddress->whs_add_line_2 ?? '',
            'bill_from_city_name' => $warehouse->warehouseAddress->whs_add_city_name ?? '',
            'bill_from_state'     => $warehouse->systemState->sys_state_name ?? '',
            'bill_from_zip'       => $warehouse->warehouseAddress->whs_add_postal_code ?? '',
            'bill_from_country'   => $warehouse->systemCountry->sys_country_name ?? '',

            'bill_to_name'        => $customer->cus_name ?? '',
            'bill_to_add1'        => $customer->customerAddress->cus_add_line_1 ?? '',
            'bill_to_add2'        => $customer->customerAddress->cus_add_line_2 ?? '',
            'bill_to_city_name'   => $customer->customerAddress->cus_add_city_name ?? '',
            'bill_to_state'       => $customer->systemState->sys_state_name ?? '',
            'bill_to_zip'         => $customer->customerAddress->cus_add_postal_code ?? '',
            'bill_to_country'     => $customer->systemCountry->sys_country_name ?? '',
        ];
    }

    private function getCustomerAndCustomerAddress($cusId)
    {
        return Customer::with(['customerAddress' => function ($q) {
            return $q->where('cus_address.cus_add_type', 'bill');
        }, 'systemCountry', 'systemState'])->findOrFail($cusId);
    }

    private function getWareHouseAndWareHouseAddress($whsId)
    {
        return Warehouse::with(['warehouseAddress' => function ($q) {
            return $q->where('whs_address.whs_add_type', 'BI');
        }, 'systemCountry', 'systemState'])
            ->findOrFail($whsId);
    }

    public function getCustomerInvoiceHeader(IlluminateRequest $request)
    {
        return $this->model
            ->when($request->get('cus_id'), function ($q) use ($request) {
                return $q->where('cus_id', $request->get('cus_id'));
            })
            ->when($request->get('whs_id'), function ($q) use ($request) {
                return $q->where('whs_id', $request->get('whs_id'));
            })
            ->has('createdBy', '>', 0)
            ->with('createdBy')
            ->groupBy('created_by')
            ->get();
    }

    public function approve($ids = [])
    {
        $invs = InvoiceHeader::findOrFail($ids);
        foreach ($invs as $inv) {
            if ($inv->inv_sts == 'AP') {
                throw new \Exception("Can\'t approve {$inv->inv_num}");
            } else {
                $header = $inv->update(['inv_sts' => 'AP']);
                if (!$header) {
                    throw new \Exception("Can\'t update the {$inv->inv_num}");
                }
            }
        }
    }

    public function getTotal($request)
    {
        return $this->model
            ->when($request->get('cus_id', false), function ($q) use ($request) {
                return $q->where('cus_id', $request->get('cus_id'));
            })
            ->where('inv_sts', '<>', 'RE')
            ->get();
    }

    public function changeStatus($id, $input)
    {
        $inv = InvoiceHeader::findOrFail($id);

        $trans = $inv->transaction()->get();

        if ($inv->inv_sts != 'AP' && $input['value'] == 're-open') {
            throw new \Exception("Can\'t {$input['value']} {$inv->inv_num}");
        }

        if ($inv->inv_sts == 'AP' && $trans->count()) {
            throw new \Exception("Can\'t {$input['value']} {$inv->inv_num}. It has payment!");
        }

        if ($inv->inv_sts != 'PE' && $input['value'] == 'reject') {
            throw new \Exception("Can\'t {$input['value']} {$inv->inv_num}");
        }

        return DB::transaction(function () use ($id, $input, $inv) {
            if ($input['value'] == 'approve') {
                $header = $inv->update([
                    'inv_sts' => $this->convertStsCode($input['value'])
                ]);
            } else {
                $header = $inv->update([
                    'inv_sts' => $this->convertStsCode($input['value']),
                    'note' => $input['note'],
                ]);
            }
            if (!$header) {
                throw new \Exception("Can\'t update the {$inv->inv_num}");
            }
            
            if ($input['value'] == 'reject') {
                $invoiceDetail = $inv->detail()->get();
                BillableDtl::whereIn('ba_dtl_id', $invoiceDetail->pluck('ba_dtl_id'))->restore();
            }

            return $header;
        });
    }

    public function convertStsCode($value)
    {
        $data = [
            're-open' => 'PE',
            'reject' => 'RE',
            'approve' => 'AP',
        ];

        return $data[$value];
    }
}
