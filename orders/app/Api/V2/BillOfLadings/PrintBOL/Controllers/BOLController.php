<?php

namespace App\Api\V2\BillOfLadings\PrintBOL\Controllers;

use App\Api\V2\BillOfLadings\PrintBOL\Models\BOLCarrierDetailModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\BOLOrderDetailModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\CartonModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\CommodityModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\CustomerConfigModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\EventTrackingModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\InventorySummaryModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\OrderCartonModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\OrderDtlModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\OrderHdrModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\ReportModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\ReportService;
use App\Api\V2\BillOfLadings\PrintBOL\Models\ShipmentModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\WarehouseModel;
use App\Api\V2\BillOfLadings\PrintBOL\Models\OutPalletModel;
use App\Api\V2\BillOfLadings\PrintBOL\Traits\OrderFlowControllerTrait;
use App\Api\V2\BillOfLadings\PrintBOL\Transformers\BOLListTransformer;
use App\Api\V2\BillOfLadings\PrintBOL\Transformers\CommodityTransformer;
use App\Api\V2\BillOfLadings\PrintBOL\Validators\BOLHdrValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Wms2\UserInfo\Data;

/**
 * Class BOLController
 *
 * @package App\Api\V2\BillOfLadings\PrintBOL\Controllers
 */
class BOLController extends AbstractController
{
    // use OrderFlowControllerTrait;

    protected $orderHdrModel;
    protected $shipmentModel;
    protected $bolCarrierDtlModel;
    protected $bolOdrDtlModel;
    protected $commodityModel;
    protected $warehouseModel;
    protected $reportModel;
    protected $reportService;
    protected $customerConfigModel;
    protected $eventTrackingModel;
    protected $orderDtlModel;
    protected $invtSmrModel;
    protected $outPalletModel;
    protected $pdfBolFileName;
    const ORDER_DOCUMENT_FOLDER = 'order-document';

    /**
     * BOLController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param ShipmentModel $shipmentModel
     * @param BOLCarrierDetailModel $bolCarrierDetailModel
     * @param BOLOrderDetailModel $bolOrderDetailModel
     * @param CommodityModel $commodityModel
     * @param WarehouseModel $warehouseModel
     * @param ReportModel $reportModel
     * @param ReportService $reportService
     * @param CustomerConfigModel $customerConfigModel
     * @param EventTrackingModel $eventTrackingModel
     * @param OrderDtlModel $orderDtlModel
     * @param InventorySummaryModel $invtSmrModel
     * @param OrderCartonModel $orderCartonModel
     * @param CartonModel $cartonModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        ShipmentModel $shipmentModel,
        BOLCarrierDetailModel $bolCarrierDetailModel,
        BOLOrderDetailModel $bolOrderDetailModel,
        CommodityModel $commodityModel,
        WarehouseModel $warehouseModel,
        ReportModel $reportModel,
        ReportService $reportService,
        CustomerConfigModel $customerConfigModel,
        EventTrackingModel $eventTrackingModel,
        OrderDtlModel $orderDtlModel,
        InventorySummaryModel $invtSmrModel,
        OrderCartonModel $orderCartonModel,
        CartonModel $cartonModel,
        OutPalletModel $outPalletModel
    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->shipmentModel = $shipmentModel;
        $this->bolCarrierDtlModel = $bolCarrierDetailModel;
        $this->bolOdrDtlModel = $bolOrderDetailModel;
        $this->commodityModel = $commodityModel;
        $this->warehouseModel = $warehouseModel;
        $this->reportModel = $reportModel;
        $this->reportService = $reportService;
        $this->customerConfigModel = $customerConfigModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->invtSmrModel = $invtSmrModel;
        $this->orderCartonModel = $orderCartonModel;
        $this->cartonModel = $cartonModel;
        $this->outPalletModel = $outPalletModel;
    }

    /**
     * @param $whsId
     * @param $shipId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function printBOL($whsId, $shipId, $isDmsIntegration = false, Request $request)
    {
        $input = $request->getParsedBody();

        if (empty($input['cus_id'])) {
            throw new \Exception(Message::get('BM088', 'Customer'));
        }

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        // $whsId = array_get($userInfo, 'current_whs', 0);
        $cusIds = array_column(array_get($userInfo, 'user_customers', []), 'cus_id');

        if (empty($whsId) || !in_array($input['cus_id'], $cusIds)) {
            throw new \Exception(Message::get('BM081', 'Customer'));
        }

        if (!file_exists(storage_path("BOL/$whsId/{$input['cus_id']}"))) {
            mkdir(storage_path("BOL/$whsId/{$input['cus_id']}"), 0777, true);
        }

        $file = "BOL-$whsId-{$input['cus_id']}-$shipId.pdf";

        /*if (file_exists(storage_path("BOL/$whsId/{$input['cus_id']}/$file"))) {
            header('Content-type: application/pdf');
            header("Content-Disposition: attachment; filename=$file");
            readfile(storage_path("BOL/$whsId/{$input['cus_id']}/$file"));

            return $this->response->noContent()->setContent([
                'status' => 'OK',
            ])->setStatusCode(Response::HTTP_OK);
        }*/

        $shipment = $this->shipmentModel->getFirstWhere(['ship_id' => $shipId, 'cus_id' => $input['cus_id']], [
            'fromState',
            'toState',
            'bolOdrDtl',
            'bolCarrierDtl'
        ]);

        if (!empty($shipment)) {
            $shipmentPrint = $shipment->toArray();
            $bolCarrierDtls = array_get($shipmentPrint, 'bol_carrier_dtl');
            $bolOdrDtls = array_get($shipmentPrint, 'bol_odr_dtl');
            // $pltTtl = 0;
            foreach ($bolOdrDtls as $keyBol => $bolOdrDtl) {
                $odrId = array_get($bolOdrDtl, 'odr_id');
                // $outPalletQty = \DB::table('out_pallet as op')
                //     ->join('pack_hdr as ph', 'ph.out_plt_id', '=', 'op.plt_id')
                //     ->where('op.deleted', 0)
                //     ->where('ph.deleted', 0)
                //     ->where('ph.odr_hdr_id', $odrId)
                //     ->groupBy('op.parent')
                //     ->get();

                $bolOdrDtls[$keyBol]['plt_qty'] = $bolOdrDtl['plt_qty'];
                $bolOdrDtls[$keyBol]['weight'] = round($bolOdrDtls[$keyBol]['weight'], 1);
                $bolOdrDtls[$keyBol]['vol_qty_ttl'] = round($bolOdrDtls[$keyBol]['vol_qty_ttl'], 2);
                $bolOdrDtls[$keyBol]['cube_qty_ttl'] = round($bolOdrDtls[$keyBol]['cube_qty_ttl'], 2);
                // $pltTtl += $outPalletQty;
            }

            $shipmentPrint['bol_odr_dtl'] = $bolOdrDtls;

            // $shipmentPrint['ship_from_name'] = 'Seldat';
            $shipmentPrint['vol_qty_ttl'] = round($shipmentPrint['vol_qty_ttl'], 2);
            $shipmentPrint['cube_qty_ttl'] = round($shipmentPrint['cube_qty_ttl'], 2);
            $shipmentPrint['weight_ttl'] = round($shipmentPrint['weight_ttl'], 1);

            // get total master pallet
            $countMasterPallet = $this->outPalletModel->countMasterPallet($whsId, array_get($shipmentPrint, 'ship_id'));
            // $shipmentPrint['plt_qty_ttl'] = $countMasterPallet;
            // $shipmentPrint['bol_carrier_dtl']['hdl_qty'] = $countMasterPallet;
            $shipmentPrint['plt_qty_ttl'] = $shipmentPrint['plt_qty_ttl'];
            $shipmentPrint['bol_carrier_dtl']['hdl_qty'] = $shipmentPrint['plt_qty_ttl'];
            $shipmentPrint['bol_carrier_dtl']['weight'] = round($shipmentPrint['bol_carrier_dtl']['weight'], 1);
            // $shipmentPrint['ship_to_name'] = $shipmentPrint['ship_to_name'] . ' c/o Seldat';
            if(strcmp($shipmentPrint['ship_from_name'], 'Seldat') != 0){
                $shipmentPrint['ship_from_name'] = $shipmentPrint['ship_from_name'] . ' c/o Seldat';
            }
            //Get Order Detail list
            $OrderDetailInfos = $this->shipmentModel->getOrderDetailInfos($shipId);
            if (empty($OrderDetailInfos)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }
            $OrderInfos = $this->shipmentModel->getOrderInfos($shipId);

            // return data for updateBOL
            if($isDmsIntegration == true){
                $this->printBOLPdf($shipmentPrint, $OrderInfos, $OrderDetailInfos, true);
                return $this->pdfBolFileName;
            }
            else{
                $this->printBOLPdf($shipmentPrint, $OrderInfos, $OrderDetailInfos);
            }
        }

        return $this->response->noContent();
    }

    /**
     * @param $shipment
     * @param $OrderInfos
     * @param $OrderDetailInfos
     */
    private function printData($shipment, $OrderInfos, $OrderDetailInfos, $isDmsIntegration = false)
    {
        $html = (string)view('printBOLTemplate', [
            'shipment' => $shipment,
            'OrderInfos'       => $OrderInfos,
            'OrderDetailInfos' => $OrderDetailInfos
        ]);

        $pdf = new mPDF('utf-8', 'A4');

        $htmlOdrDTL = (string)view('BolLpnDetailOrderListPrintoutTemplate', [
            'OrderInfos'       => $OrderInfos,
            'OrderDetailInfos' => $OrderDetailInfos
        ]);

        // output the HTML content
        $pdf->writeHTML($html);
        $pdf->AddPage();
        $pdf->WriteHTML($htmlOdrDTL);

        //Close and output PDF document
        $dir = storage_path("BOL/{$shipment['whs_id']}/{$shipment['cus_id']}");
        $pdf->Output("$dir/BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf", 'F');

        if($isDmsIntegration == true) {
            // Copy file for DMS integration
            $this->pdfBolFileName = "BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf";
            $dmsFolderPath = storage_path(self::ORDER_DOCUMENT_FOLDER . "/dms");
            if (!file_exists($dmsFolderPath)) {
                mkdir($dmsFolderPath, 0777, true);
            }
            $filePath = "{$dmsFolderPath}/{$this->pdfBolFileName}";
            $pdf->Output($filePath, 'F');
        }

        if($isDmsIntegration != true){
            $pdf->Output("BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf", 'D');
        }
    }

    private function printBOLPdf($shipment, $OrderInfos, $OrderDetailInfos, $isDmsIntegration = false)
    {
        $html = (string)view('printBOLTemplate', [
            'shipment' => $shipment,
            'OrderInfos'       => $OrderInfos,
            'OrderDetailInfos' => $OrderDetailInfos
        ])->render();

        $pdf = new \Mpdf\Mpdf(['utf-8', 'A4']);

        $htmlOdrDTL = (string)view('BolLpnDetailOrderListPrintoutTemplate', [
            'OrderInfos'       => $OrderInfos,
            'OrderDetailInfos' => $OrderDetailInfos
        ]);

        // output the HTML content
        $pdf->writeHTML($html);
        $pdf->AddPage();
        $pdf->WriteHTML($htmlOdrDTL);

        //Close and output PDF document
        $dir = storage_path("BOL/{$shipment['whs_id']}/{$shipment['cus_id']}");
        $pdf->Output("$dir/BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf", 'F');

        if($isDmsIntegration) {
            // Copy file for DMS integration
            $this->pdfBolFileName = "BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf";
            $dmsFolderPath = storage_path(self::ORDER_DOCUMENT_FOLDER . "/dms");
            if (!file_exists($dmsFolderPath)) {
                mkdir($dmsFolderPath, 0777, true);
            }
            $filePath = "{$dmsFolderPath}/{$this->pdfBolFileName}";
            $pdf->Output($filePath, 'F');
        }

        if($isDmsIntegration != true){
            $pdf->Output("BOL-{$shipment['whs_id']}-{$shipment['cus_id']}-{$shipment['ship_id']}.pdf", 'D');
        }
    }
}
