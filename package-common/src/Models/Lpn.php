<?php

namespace Seldat\Wms2\Models;

use Seldat\Wms2\Utils\JWTUtil;

class Lpn extends BaseModel
{
    protected $table = 'lpn';

    protected $primaryKey = 'lpn_id';

    const UPDATED_AT = null;

    protected $fillable = [
        'type',
        'month',
        'from',
        'to',
        'lpn_qty',
    ];

    public static function boot()
    {
        parent::boot();

        $userId = JWTUtil::getPayloadValue('jti') ?: 1;

        // create a event to happen on saving
        static::saving(function ($table) use ($userId) {
            $table->created_by = $table->created_by ?: $userId;
        });
    }

    public function scopePallet($query)
    {
        return $query->where('lpn.type', 'P');
    }

    public function scopeTote($query)
    {
        return $query->where('lpn.type', 'T');
    }

    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }
}
