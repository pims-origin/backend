<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use App\InvoiceHeader;

class InvoiceTransaction extends BaseInvoiceModel
{
    protected $table = 'inv_trans';
    protected $primaryKey = 'inv_tran_id';
    protected $dateFormat = 'Y-m-d';
    protected $fillable = [
        'inv_tran_num',
        'inv_tran_dt',
        'inv_num',
        'ref_num',
        'inv_tran_note',
        'amount',
        'method',
    ];

    public function invoice()
    {
        return $this->belongsTo(InvoiceHeader::class, 'inv_id');
    }

    public function setInvTranNumAttribute($value)
    {
        if (empty($value)) {
            $tranCode = 'TRA-' . $this->inv_num;
            $transaction = $this->where('inv_tran_num', 'LIKE', $tranCode . '-%')
                ->withTrashed()
                ->latest()
                ->first();
            $invTranNum = $transaction ? $transaction->inv_tran_num : null;
            return $this->attributes['inv_tran_num'] = $invTranNum ? ++$invTranNum : $this->generateInvTranNum($tranCode);
        }
        
        return $this->attributes['inv_tran_num'] = $value;
    }

    public function setInvTranDtAttribute($value)
    {
        if (empty($value)) {
            return $this->attributes['inv_tran_dt'] = time();
        } else {
            return $this->attributes['inv_tran_dt'] = strtotime($value);
        }
    }

    public function getInvTranDtFormatAttribute($value)
    {
        return date($this->dateFormat, $this->inv_tran_dt); 
    }

    private function generateInvTranNum($tranCode = null)
    {
        return ($tranCode ?: 'TRA-' . $this->inv_num) . '-' . str_pad(1, 3, '0', STR_PAD_LEFT);
    }
}
