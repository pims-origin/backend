<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ReasonService;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use function GuzzleHttp\json_decode;


class ReasonController extends AbstractController
{
    /**
     * @var MenuService
     */
    protected $service;

    /**
     * MenuController constructor.
     *
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        $this->service = new ReasonService($request);
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     *
     * @return \Illuminate\Http\Response
     */
    public function search(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->search($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(IRequest $request)
    {
        $input = $request->getParsedBody();

        $result = $this->service->create($input);

        if ($result instanceof IResponse) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param IRequest $request
     * @param integer $reasonId
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function update(IRequest $request, $reasonId)
    {
        $input = $request->getParsedBody();

        $result = $this->service->update($input, $reasonId);

        if ($result instanceof IResponse) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }
}
