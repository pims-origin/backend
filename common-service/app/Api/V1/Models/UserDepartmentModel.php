<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\UserDepartment;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;

class UserDepartmentModel extends AbstractModel
{
    /**
     * UserDepartmentModel constructor.
     *
     * @param UserDepartment|null $model
     */
    public function __construct(UserDepartment $model = null)
    {
        $this->model = ($model) ?: new UserDepartment();
    }

    /**
     * @param $userDepartmentId
     *
     * @return mixed
     */
    public function getUserByUserDepartmentId($userDepartmentId, $attributes = [])
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('users')
            ->select('user_id','username','emp_code','first_name','last_name','email','status')
            ->addSelect(
                DB::raw("DATE_FORMAT(FROM_UNIXTIME(created_at), '%m/%d/%Y') AS created_at"),
                DB::raw("DATE_FORMAT(FROM_UNIXTIME(updated_at), '%m/%d/%Y') AS updated_at")
            )
            ->where('usr_dpm_id', (int)$userDepartmentId);

            if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
                foreach ($attributes['sort'] as $key => $order) {
                    if (!empty($key)) {
                        $query->orderBy($key, $order);
                    }
                }
            }

        $rows = $query->get();

        return $rows;
    }

    /**
     * Search UserDepartment
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['usr_dpm_code', 'usr_dpm_name'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        // Get
        if ($limit) {
            $models = $query->paginate($limit);
        } else {
            $models = $query->get();
        }

        return $models;
    }
}
