<?php


namespace App\Modules\Orders\Eloquents;


class WavepickHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_hdr';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_id';
    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'wv_num',
        'wv_sts',
        'seq',
        'picker',
        'is_ecom'
    ];

    public function orderHdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\OdrHdr', 'wv_id', 'wv_id');
    }

    public function userCreated()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function details()
    {
        return $this->hasMany(__NAMESPACE__ . '\WavepickDtl', 'wv_id', 'wv_id');
    }

    public function pickerUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'picker');
    }
}
