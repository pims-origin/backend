<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15/August/2017
 * Time: 11:05
 */

namespace App\Api\V1\Validators;

class ScanCartonValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'pallet-rfid' => 'required',
            'ctn-rfid'    => 'required',
        ];
    }
}
