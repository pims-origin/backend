<?php
/**
 * Created by PhpStorm.
 *
 * Date: loc_id-July-16
 * Time: 10:00
 */

namespace App\Api\V3\Outbound\AssignPallet\Controllers;

use App\Api\V3\Outbound\Models\Log;
use App\libraries\RFIDValidate;
use App\Api\V3\Outbound\Models\EventTrackingModel;
use App\Api\V3\Outbound\Models\LocationModel;
use App\Api\V3\Outbound\Models\OutPalletModel;
use App\Api\V3\Outbound\AssignPallet\Validators\AssignPalletValidator;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use App\Jobs\BOLJob;

/**
 * Class PalletController
 *
 * @package App\Api\V3\Outbound\AssignPallet\Controllers
 */
class PalletController extends AbstractController
{
    protected $AssignPallet;
    protected $outPalletModel;
    protected $locationModel;
    protected $eventTrackingModel;

    /**
     * PalletController constructor.
     */
    public function __construct()
    {
        $this->AssignPallet = new AssignPalletValidator();
        $this->outPalletModel = new OutPalletModel();
        $this->locationModel = new LocationModel();
        $this->eventTrackingModel = new EventTrackingModel();
    }

    public function assignOutboundPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $requireFieldsValidate = $this->AssignPallet->validateRequireFields($input);

        if ($requireFieldsValidate) {
            $msgString = $requireFieldsValidate;
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['message'][] = [
                'status_code' => -1,
                'msg' => $msgString,
            ];

            return new Response($msg, 200, [], null);
        }

        //get an ctn rfid to get grdetail from table ctn
        $pltNums = array_get($input, 'pallet', null);
        $locCode = array_get($input, 'loc_code', null);

        $owner = $transaction = "";

        $url = "/whs/{$whsId}/outbound/assign-outbound-pallet/";
        Log:: info($request, $whsId, [
            'evt_code' => 'ACC',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Assign Outbound Pallet'
        ]);

        if (empty($pltNums) || !$locCode) {
            throw new \Exception(Message::get('BM142'));
        }

        //validate pallet RFID
        $locRFIDValid = new RFIDValidate($locCode, RFIDValidate::TYPE_LOCATION, $whsId);
        $keyLoc = 'loc_code';
        if ($locRFIDValid->validate()) {
            $keyLoc = 'rfid';
        }

        $location = $this->locationModel->getLocationByKey($whsId, $keyLoc, $locCode);
        if (!$location) {
            $location =  $location = $this->locationModel->getLocationByKeyNoType($whsId, $keyLoc, $locCode);
            $errStr = (!$location) ? 'Location is not existed' : 'Location is not Shipping type';
            return $this->_responseErrorMessage($errStr);
        }
        else {
            if(object_get($location, 'loc_sts_code') != 'AC') {
                $msg = 'Location is not active';
                return $this->_responseErrorMessage($msg, [$locCode]);
            }
        }

        $outPallets = $this->outPalletModel->getOutPalletByPltNum($whsId, $pltNums);

        if (count($outPallets) != count($pltNums)) {
            $palletNumExists = array_pluck($outPallets, 'plt_num', 'plt_num');
            $arrNotExist = array_diff($pltNums, $palletNumExists);
            $msg = 'Pallets are not existed';

            return $this->_responseErrorMessage($msg, array_values($arrNotExist));
        }
        // else {
        //     $wrongPalletRfid = [];
        //     foreach ($outPallets as $outPallet) {
        //         if(object_get($outPallet, 'out_plt_sts') != 'AC') {
        //             $wrongPalletRfid[] = object_get($outPallet, 'plt_num');
        //         }
        //     }
        //     if(!empty($wrongPalletRfid)) {
        //         $msg = sprintf('Pallet(s) %s is/are not active', implode(', ', $wrongPalletRfid));

        //         return $this->_responseErrorMessage($msg);
        //     }
        // }

        $areaShipping = object_get($location, 'area', 2); // Ready to Ship
        // 1:SS: Schedule to Ship, 2:RS: Ready to Ship
        if ($areaShipping == 1) {
            // Schedule to Ship
            // Check order was shipped
            $checkOdrsShipped = $this->outPalletModel->checkOdrsByPltNums($pltNums, $whsId);
            $countTruePltNum = array_unique(array_pluck($checkOdrsShipped->toArray(), 'plt_num'));
            if (!count($checkOdrsShipped) || $countTruePltNum < count($pltNums)) {
                $msg = sprintf('Unable to assign out-pallet %s of a order is not created BOL yet to shipping lane', implode(', ', $pltNums));

                return $this->_responseErrorMessage($msg);
            }

            $pltRfidShipped = [];
            $pltRfidNotFinalBol = [];
            $pltRfidMoveBack = [];
            foreach ($checkOdrsShipped as $checkOdrShipped) {
                if (object_get($checkOdrShipped, 'odr_sts') == 'SH') {
                    $pltRfidShipped[] = object_get($checkOdrShipped, 'plt_num');
                }

                if (object_get($checkOdrShipped, 'ship_sts') != 'FN') {
                    $pltRfidNotFinalBol[] = object_get($checkOdrShipped, 'plt_num');
                }

                if ($location->area == 2 && object_get($checkOdrShipped, 'odr_sts') == 'SS') {
                    $pltRfidMoveBack[] = object_get($checkOdrShipped, 'plt_num');
                }

            }
            if(!empty($pltRfidShipped)) {
                $msg = sprintf('Unable to assign out-pallet %s of a shipped order to shipping lane.', implode(', ', $pltRfidShipped));

                return $this->_responseErrorMessage($msg);
            }

            if(!empty($pltRfidNotFinalBol)) {
                $msg = sprintf('Unable to assign out-pallet %s of a BOL is not final yet to shipping lane.', implode(', ', array_unique($pltRfidNotFinalBol)));

                return $this->_responseErrorMessage($msg);
            }

            if(!empty($pltRfidMoveBack)) {
                $msg = sprintf('Unable to assign out-pallet %s of a order is "Schedule to Ship" to "Ready to Ship".', implode(', ', $pltRfidMoveBack));

                return $this->_responseErrorMessage($msg);
            }

        } else {
            // Schedule to Ship – Assign all pallets to shipping lane – Area 2 (A, B, C, D)
            // Check order was shipped
            $checkOdrsShipped = $this->outPalletModel->getOdrsByPltNums($pltNums, $whsId);
            $pltRfidShipped = [];
            $pltRfidMoveBack = [];
            foreach ($checkOdrsShipped as $checkOdrShipped) {
                if (object_get($checkOdrShipped, 'odr_sts') == 'SH') {
                    $pltRfidShipped[] = object_get($checkOdrShipped, 'plt_num');
                }

                // if ($location->area == 2 && in_array(object_get($checkOdrShipped, 'odr_sts'), ['RS', 'SS'])) {
                if ($location->area == 2 && in_array(object_get($checkOdrShipped, 'odr_sts'), ['SS'])) {
                    $pltRfidMoveBack[] = object_get($checkOdrShipped, 'plt_num');
                }

            }
            if(!empty($pltRfidShipped)) {
                $msg = sprintf('Unable to assign out-pallet %s of a shipped order to shipping lane.', implode(', ', $pltRfidShipped));

                return $this->_responseErrorMessage($msg);
            }

            if(!empty($pltRfidMoveBack)) {
                $msg = sprintf('Unable to assign out-pallet %s of a order is "Schedule to ship" to "Ready to ship".', implode(', ', $pltRfidMoveBack));

                return $this->_responseErrorMessage($msg);
            }
        }

        try {
            // start transaction
            DB::beginTransaction();

            $this->outPalletModel->updateOutPalletLocation($location, $pltNums);

            // WAP-724 - [Outbound - Shipping Lane] Modify API assign Out pallet to Shipping Lane
            $this->_updateOrderStatusShipping($checkOdrsShipped, $location);

            DB::commit();

            // WMS2-5337 - [BOL] Change the work flow when create BOL
            foreach ($checkOdrsShipped as $checkOdrShipped) {
                if ($odrId = object_get($checkOdrShipped, 'odr_id')) {
                    dispatch(new BOLJob($odrId, $whsId, $request));
                }
            }

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'][] = [
                'status_code' => 1,
                'msg' => 'OK'
            ];

            return new Response($msg, 200, [], null);

        } catch (\Exception $e) {
            DB::rollBack();

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    public function utf8_urldecode($str) {
        $str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
        return $str;
        // return html_entity_decode($str,null,'UTF-8');
      }

    public function getShippingLanePallet($whsId, $locCode)
    {
        try {
            // $locCode = $this->utf8_urldecode($locCode);
            // $locCode = urldecode($locCode);
            // $locCode = urldecode($locCode);
            $locCode = str_replace(['%20', '%2520'], ' ', $locCode);

            $loc = $this->locationModel->getFirstWhere(['loc_code' => $locCode, 'loc_whs_id' => $whsId], ['locationType']);

            if(empty($loc)) {
                throw  new  \Exception('Location is not existed');
            }
            if(object_get($loc, 'locationType.loc_type_code') != 'SHP') {
                throw  new  \Exception('Location is not belong to shipping lane!');
            }

            $dbRawOh = '(select * from odr_hdr where deleted = 0 and odr_sts in ("SS", "RS")) as oh';
//            $pallets = $this->outPalletModel->findWhere(['loc_code' => $locCode, 'whs_id' => $whsId], [], [], ['plt_num'])->pluck('plt_num');
            $pallets = $this->outPalletModel->getModel()
                ->join('pack_hdr as ph', 'ph.out_plt_id', '=', 'out_pallet.plt_id')
                ->join(DB::raw($dbRawOh), 'ph.odr_hdr_id', '=', 'oh.odr_id')
                ->where('out_pallet.whs_id', $whsId)
                ->where('out_pallet.loc_code', $locCode)
                ->where('ph.deleted', 0)
                ->groupBy('plt_num')
                ->pluck('plt_num');

            $data = [
                'loc_code' => $locCode,
                'pallet' => $pallets
            ];

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'][] = $data;
            $msg['message'][] = [

                'status_code' => 1,
                'msg' => 'OK'
            ];
            return new Response($msg, 200, [], null);

        } catch (\Exception $e) {
            DB::rollBack();

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'message' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _updateOrderStatusShipping($checkOdrsShipped, $locObj)
    {
        $areaShipping = object_get($locObj, 'area', 2); // Ready to Ship
        // RS: Ready to Ship, SS: Schedule to Ship
        $odrSts = $areaShipping == 1 ? 'SS' : 'RS';

        foreach ($checkOdrsShipped as $checkOdrShipped) {
                if ($odrId = object_get($checkOdrShipped, 'odr_id')) {
                    $isUpdated = "(SELECT COUNT(1) FROM pack_hdr
                        LEFT JOIN out_pallet ON out_pallet.plt_id = pack_hdr.out_plt_id
                        WHERE pack_hdr.odr_hdr_id = odr_hdr.odr_id AND pack_hdr.deleted = 0
                            AND out_pallet.loc_id IS NULL) = 0";
                    $updateSts = sprintf("(SELECT MAX(location.area) FROM pack_hdr
                        INNER JOIN out_pallet ON pack_hdr.out_plt_id = out_pallet.plt_id
                        INNER JOIN location ON location.loc_id = out_pallet.loc_id
                        WHERE pack_hdr.odr_hdr_id = odr_hdr.odr_id AND out_pallet.deleted = 0
                        ) = %s", $areaShipping);
                    $result = DB::table('odr_hdr')
                        ->where('odr_id', $odrId)
                        ->where('deleted', 0)
                        ->whereRaw($isUpdated)
                        ->whereRaw($updateSts)
                        ->update(["odr_sts" => $odrSts]);
                }
            }
    }
}
