<?php

namespace common\models;

use yii\db\ActiveRecord;

class UserWarehouse extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_whs';
    }

    public function getUsers()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    public function getWarehouses()
    {
        return $this->hasOne(Warehouse::className(), ['whs_id' => 'whs_id']);
    }

}