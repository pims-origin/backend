<?php

namespace App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Models;

use Seldat\Wms2\Models\CustomerConfig;
use Wms2\UserInfo\Data;

class CustomerConfigModel extends AbstractModel
{
    protected $model;

    public function __construct(CustomerConfig $model = null)
    {
        $this->model = ($model) ?: new CustomerConfig();
    }

    /**
     * @param $customerId
     *
     * @return mixed
     * @throws \Exception
     */

    public function getPartialCustomerConfigs($customerId)
    {
        $query = $this->make();
        $this  -> model->filterData($query); //add more where
        $query->where('cus_id',$customerId);
        $query->where('config_name','allocation');
        $query->where('config_value','partial');
        $query->where('ac','Y');
        $models = $query->get();

        return $models;
    }

    public function getConfigValue($warehouseId, $customerId){
        $query = $this->make();
        $this -> model->filterData($query); //add more where
        $query->select('config_value');
        $query->where('whs_id',$warehouseId);
        $query->where('cus_id',$customerId);
        $query->where('config_name','picking_algorithm');
        $query->where('ac','Y');
        $models = $query->first();
        return $models;
    }

    public function getPickingAlgorithm($warehouseId, $customerId){
        $query = $this->make();
        $this -> model->filterData($query); //add more where
        $query->select('config_value');
        $query->where('whs_id',$warehouseId);
        $query->where('cus_id',$customerId);
        $query->where('config_name','picking_algorithm');
        $query->where('ac','Y');
        $models = $query->first();
        $algorithm = object_get($models,'config_value', 'FIFO');
        $algorithm = strtoupper($algorithm);
        return $algorithm;
    }
}
