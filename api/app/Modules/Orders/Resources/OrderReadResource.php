<?php

namespace App\Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderReadResource extends JsonResource
{
    public function toArray($request)
    {
        $odrReqDt   = ($this->odr_req_dt && strtotime($this->odr_req_dt) != 0) ? $this->odr_req_dt->format('Y-m-d') : NULL;
        $shipByDt   = ($this->ship_by_dt && strtotime($this->ship_by_dt) != 0) ? $this->ship_by_dt->format('Y-m-d') : NULL;
        $cancleByDt = ($this->cancle_by_dt && strtotime($this->cancle_by_dt) != 0) ? $this->cancle_by_dt->format('Y-m-d') : NULL;

        return [
            'type' => 'orders',
            'id' => (int) $this->odr_id,
            'attributes' => [
                'order_type'                => $this->odr_type,
                'order_num'                 => $this->odr_num,
                'cus_order_num'             => $this->cus_odr_num,
                'po_num'                    => $this->cus_po,
                'pick_ref'                  => $this->cus_pick_num,
                'dept_ref'                  => $this->cus_dept_ref,
                'rush_priority'             => $this->rush_odr ?: 0,
                'order_date'                => $odrReqDt,
                'ship_date'                 => $shipByDt,
                'cancel_date'               => $cancleByDt,
                'additional_information'    => $this->cus_notes,
                'special_instruction'       => $this->spec_instruct,

                'meta'                      => $this->odrHdrMeta ? json_decode($this->odrHdrMeta->value) : NULL,

                'carrier' => [
                    'carrier'               => $this->carrier,
                    'delivery_service'      => $this->dlvy_srvc
                ],

                'items'                     => ItemResource::collection($this->odrDtls()->where('deleted', 0)->get()),

                'contacts' => [
                    [
                        'contact_type'          => $this->thirdParty->ctt_type ?: 'Receiver',
                        'name'                  => $this->thirdParty->name,
                        'first_name'            => $this->thirdParty->ct_first_name,
                        'last_name'             => $this->thirdParty->ct_last_name,
                        'phone'                 => $this->thirdParty->phone ? $this->thirdParty->phone : $this->ship_to_phone,
                        'extension'             => $this->thirdParty->extns,
                        'mobile'                => $this->thirdParty->mobile,
                        'email'                 => $this->thirdParty->email,
                        'description'           => $this->thirdParty->des
                    ]
                ],

                'addresses' => [
                    [
                        'address_type'          => $this->thirdParty->addr_type ?: 'Ship_To',
                        'address_line_1'        => $this->thirdParty->add_1,
                        'address_line_2'        => $this->thirdParty->add_2,
                        'city'                  => $this->thirdParty->city,
                        'state'                 => $this->thirdParty->state,
                        'zip'                   => $this->thirdParty->zip,
                        'country'               => $this->thirdParty->country
                    ]
                ]
            ]
        ];
    }
}