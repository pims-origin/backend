<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\Status;

class WavePickInfoTransformer extends TransformerAbstract
{

    /**
     * @param WavepickHdr $wvHdr
     *
     * @return array
     */
    public function transform(WavepickHdr $wvHdr)
    {
        $wvDtl = [];
        foreach ($wvHdr->wvDetail as $detail) {
            $sysUom = SystemUom::where('sys_uom_id', $detail['uom_id'])
                ->select('sys_uom_name')->first();
            $detail['uom_name'] = object_get($sysUom, "sys_uom_name", null);
            $detail['wv_dtl_sts'] = Status::getByKey("WAVEPICK-DETAIL-STATUS", $detail['wv_dtl_sts']);
            $detail['itm_id'] = $detail['item_id'];
            $detail['piece_qty'] = $detail['s_piece_qty'];
            $detail['act_loc'] = json_decode($detail['act_loc_ids'],true);
            unset($detail['item_id']);
            unset($detail['s_piece_qty']);
            unset($detail['act_loc_ids']);
            $wvDtl[] = $detail;
        }

        $odrHdr = object_get($wvHdr, 'orderHdr', null);
        $odrHdrType = array_get($odrHdr, '0.shippingOrder.type', null);
        return [
            'wv_id'             => $wvHdr->wv_id,
            'wv_num'            => $wvHdr->wv_num,
            'cus_name'          => object_get($wvHdr, "cust_name", null),
            'odr_num'           => object_get($wvHdr, 'odr_num', null),
            'org_odr_type'      => $odrHdrType,
            'org_odr_type_name' => Status::getByKey('ORDER-TYPE', $odrHdrType),
            'details'           => $wvDtl,
            'picker_id'          => $wvHdr->picker,
            'picker_name'        => trim(object_get($wvHdr, 'pickerUser.first_name', null) . " " .
                object_get($wvHdr, 'pickerUser.last_name', null)),
            'is_ecom'            => (int)$wvHdr->is_ecom
        ];
    }
}
