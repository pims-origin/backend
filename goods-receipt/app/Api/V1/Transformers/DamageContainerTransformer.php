<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\AsnDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;

class DamageContainerTransformer extends TransformerAbstract
{

    /**
     * @param $sample
     *
     * @return array
     */
    public function transform($data)
    {

        return [
            'ctnr_id' => object_get($data, 'ctnr_id', null),
            'ctnr_num' => object_get($data, 'ctnr_num', null),
            'cus_id' => object_get($data, 'cus_id', null),
            'whs_id' => object_get($data, 'whs_id', null),
        ];
    }

}
