<?php

namespace Seldat\Wms2\Models;

/**
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property mixed chg_code_id
 * @property mixed code
 * @property mixed description
 * @property mixed chg_uom_id
 * @property mixed chg_type_id
 */
class ChargeCode extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'charge_code';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'chg_code_id';

    /**
     * @var array
     */
    protected $fillable = [
        'chg_code',
        'chg_code_des',
        'chg_uom_id',
        'chg_type_id',
        'created_by',
        'updated_by',
    ];

    public function systemUom()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemUom', 'chg_uom_id', 'sys_uom_id');
    }

    public function chargeType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ChargeType', 'chg_type_id', 'chg_type_id');
    }

    public function customerChargeDetails()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerChargeDetail', 'chg_code_id', 'chg_code_id');
    }
}
