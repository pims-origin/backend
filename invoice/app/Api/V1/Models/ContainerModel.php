<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\InvoiceReceivingHistory;


class ContainerModel extends AbstractModel
{
    /**
    * @var
    */
    
     protected $invRcv;
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new Container();
        $this->invRcv = new InvoiceReceivingHistory();
    }

    /**
     * @recNum array
     * return mixed
    */

    public function getContainerName($ctnrID)
    {
        $result = $this->model
                        ->where('ctnr_id', $ctnrID)
                        ->value('ctnr_num');

        return $result;
    }
    
      
    /**
     * @invNum array
     * return mixed
    */

    public function getInvoicedContainers($invNum)
    {
        $results = $this->invRcv
                        ->where('inv_num', $invNum)
                        ->lists('gr_hdr_id')
                        ->toArray();

        return $results;
        
    }

}
