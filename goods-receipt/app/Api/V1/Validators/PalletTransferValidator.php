<?php

namespace App\Api\V1\Validators;


class PalletTransferValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'plt_id' => 'required|integer',
            'rfid'   => 'required',

        ];

    }


}
