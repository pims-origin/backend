<?php
/**
 * Created by PhpStorm.
 * User: PCDell76_ThienNguyen
 * Date: 2/10/2017
 * Time: 7:19 PM
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Utils\Status;

class OrderCtnTransformer extends TransformerAbstract
{
    public function transform(OrderCarton $carton)
    {
        return [
            'cus_name'              => object_get($carton, 'cus_name', ''),
            'cus_code'              => object_get($carton, 'cus_code', ''),
            'odr_num'               => object_get($carton, 'odr_num', ''),
            'odr_type_code'         => object_get($carton, 'odr_type', ''),
            'odr_type'              => Status::getByKey("ORDER-TYPE", object_get($carton, 'odr_type', '')),
            'item_id'               => object_get($carton, 'item_id', ''),
            'sku'                   => object_get($carton, 'sku', ''),
            'size'                  => object_get($carton, 'size', ''),
            'color'                 => object_get($carton, 'color', ''),
            'lot'                   => object_get($carton, 'lot', ''),
            'upc'                   => object_get($carton, 'upc', ''),
            'piece_qty'             => object_get($carton, 'piece_qty', ''),
            'ship_dt'               => $this->dateFormat(object_get($carton, 'shipped_dt', '')),
            'ship_by'               => object_get($carton, 'first_name', '') . ' ' . object_get($carton, 'last_name', ''),
            'pack'                  => object_get($carton, 'pack', ''),
            'shipping_ctns'         => round(object_get($carton, 'piece_qty', '') / object_get($carton, 'pack', '')),
            'cus_odr_num'           => object_get($carton, 'cus_odr_num', ''),
            'cus_po'                => object_get($carton, 'cus_po', ''),
            'ship_to_name'          => object_get($carton, 'ship_to_name', ''),
            'carrier'               => object_get($carton, 'carrier', ''),
            'ship_to_add_1'         => object_get($carton, 'ship_to_add_1', ''),
            'ship_to_state'         => object_get($carton, 'ship_to_state', ''),
            'sys_state_name'        => object_get($carton, 'sys_state_name', ''),
            'ship_to_city'          => object_get($carton, 'ship_to_city', ''),
            'ship_to_zip'           => object_get($carton, 'ship_to_zip', ''),

            'act_cancel_dt'         => (empty(object_get($carton, 'act_cancel_dt', ''))) ? '' :
                date("m/d/Y", object_get($carton, 'act_cancel_dt', '')),
        ];
    }

    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date('m/d/Y H:i', $date) : null;
    }
}