<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\SettingModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V1\Transformers\SettingTransformer;
use Symfony\Component\HttpKernel\Exception as HttpException;
use Illuminate\Http\Response as IlluminateResponse;
use App\Setting;

class SettingController extends AbstractController
{
    /**
     * @var $settingModel
     */
    protected $settingModel;

    /**
     * @var $settingTransformer
     */
    protected $settingTransformer;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->settingModel = new SettingModel();
        $this->settingTransformer = new SettingTransformer();
    }

    public function settings()
    {
        $settings = $this->settingModel->all();

        return $this->response->collection($settings, $this->settingTransformer);
    }

    public function instance($key)
    {
        $item = $this->settingModel->getSettingByKey($key);

        if (!$item) {
            return $this->response->noContent();
        }

        return $this->response->item($item, $this->settingTransformer);
    }

    public function create(Request $request)
    {
        $params = $request->getParsedBody();
        if (empty($params['setting_key'])) {
            return $this->response->errorBadRequest('Missed parameter');
        }

        $key = $params['setting_key'];
        $item = $this->settingModel->getSettingByKey($key);
        if ($item instanceof Setting) {
            // conflict
            throw new HttpException\ConflictHttpException('\'' . $key . '\'' . ' setting is existed');
        }

        $data = [
            'setting_key'   => $key,
            'setting_value' => json_encode(array_get($params, 'setting_value', ''))
        ];
        $item = $this->settingModel->create($data);

        return $this->response->item($item, $this->settingTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
        /*
         * [setting_key]   : abc
         * [setting_value][value_key_1] : value key 1
         * [setting_value][value_key_2] : value key 2
         * [setting_value][value_key_3] : value key 3
        */
    }

    public function createByKey($key, Request $request)
    {
        $params = $request->getParsedBody();
        $item = $this->settingModel->getSettingByKey($key);
        if ($item instanceof Setting) {
            // conflict
            throw new HttpException\ConflictHttpException('\'' . $key . '\'' . ' setting is existed');
        }
        $data = [
            'setting_key'   => $key,
            'setting_value' => json_encode(array_get($params, 'setting_value', null))
        ];
        $item = $this->settingModel->create($data);

        return $this->response->item($item, $this->settingTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
    }

    public function update($key, Request $request)
    {
        $params = $request->getParsedBody();
        $item = $this->settingModel->updateSettingByKey($key, array_get($params, 'setting_value', null));
        if (!$item instanceof Setting) {
            return $this->response->errorNotFound('Item not found');
        }

        return $this->response->item($item, $this->settingTransformer);
    }

    public function delete($key)
    {
        $item = $this->settingModel->getSettingByKey($key);
        if (!$item instanceof Setting) {
            return $this->response->errorNotFound('Setting not found');
        }
        $result = $this->settingModel->delete($item);
        if (false === $result) {
            return $this->response->errorBadRequest('Item couldn\'t be removed');
        }

        return $this->response->noContent();
    }
}
