<?php

namespace App\Modules\Orders\Eloquents;

class PackHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pack_hdr';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pack_hdr_id';
    /**
     * @var array
     */
    protected $fillable = [
        'pack_hdr_num',
        'seq',
        'odr_hdr_id',
        'whs_id',
        'cus_id',
        'carrier_name',
        'sku_ttl',
        'piece_ttl',
        'pack_sts',
        'sts',
        'ship_to_name',
        'pallet_id',
        'pack_type',
        'width',
        'height',
        'length',
        'pack_ref_id',
        'pack_dt_checksum'
    ];
}
