<?php

namespace App\Api\V1\Traits;

use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\OdrHdrMetaModel;
use App\Api\V1\Models\OrderDtlModel;
use GuzzleHttp\Client;
use Seldat\Wms2\Models\OrderHdrMeta;

trait OrderFlowControllerTrait
{

    protected $req;

    public function orderFlow($wv_id, $odr_id, $dependency = 5,  $cus_id, $request, OdrHdrMetaModel $orderHdrMeta,
CustomerMetaModel
    $customerMetaModel)
    {
        $this->req = $request;
        try {

            // get customer meta
            $cus_meta = $orderHdrMeta->getOrderFlow($odr_id);

            // csr
            $csrFlow = $customerMetaModel->getFlow($cus_meta, 'APK');

            if (array_get($csrFlow, 'usage', -1) == 0 &&  $dependency == 5) {
                $csr_default = $customerMetaModel->getPNP($cus_id, 'PKR');
                $csr_status = $this->PKRFlow($wv_id, $csr_default);
                if ($csr_status != 200) {
                    throw new \Exception('Error occurred during assign Picker');
                }
            }

        } catch (\Exception $exception) {
            throw  new \Exception($exception->getMessage());
        }

    }

    public function PKRFlow($wv_id, $user_id)
    {
        $wvIds[] = $wv_id;

        $client = new Client();
        $res = $client->request('PUT', env('API_WAVEPICK') .'assign-picker',
            [
                'headers'     => ['Authorization' => $this->req->getHeader('Authorization')],
                'form_params' => ["user_id" => $user_id, "wv_id" => $wvIds]
            ]
        );
        return $res->getStatusCode();
    }

    public function checkPacking($order_id) {
        $hdrMetaModel = new OdrHdrMetaModel();
        $customerMetaModel = new CustomerMetaModel();
        $order_meta = $hdrMetaModel->getOrderFlow($order_id);
        $Flow = $customerMetaModel->getFlow($order_meta, 'PAK');
        if (array_get($Flow, 'usage', -1) == 0) {
            return true;
        }
        return false;
    }
}