<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\Gate\Models;

use Seldat\Wms2\Models\Gate;
use Seldat\Wms2\Utils\SelArr;

class GateModel extends AbstractModel
{
    /**
     * GateModel constructor.
     *
     * @param Gate|null $model
     */
    public function __construct(Gate $model = null)
    {
        $this->model = ($model) ?: new Gate();
    }

    public function search($attributes, array $with = [], $limit = 99999999999)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    public function getGateByIds($gateId)
    {
        $rows = $this->model
            ->where('gate_id', $gateId)
            ->first();

        return $rows;
    }
}
