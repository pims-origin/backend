<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportInventory;

class ReportInventoryTransformer extends AbstractTransformer
{

    /**
     * @param ReportInventory $reportInventory
     * @return array
     */
    public function transform(ReportInventory $reportInventory)
    {
        return [
        	"id"			=>	array_get($reportInventory, 'id', null),
            "whs_id"		=>	array_get($reportInventory, 'whs_id', null),
            "item_id"		=>	array_get($reportInventory, 'item_id', null),
            "cus_id"		=>	array_get($reportInventory, 'cus_id', null),
            "whs_code"		=>	array_get($reportInventory, 'whs_code', null),
            "cus_code"		=>	array_get($reportInventory, 'cus_code', null),
            "type"			=>	array_get($reportInventory, 'type', null),
            "upc"			=>	array_get($reportInventory, 'upc', null),
            "des"			=>	array_get($reportInventory, 'des', null),
            "sku"			=>	array_get($reportInventory, 'sku', null),
            "size"			=>	array_get($reportInventory, 'size', null),
            "color"			=>	array_get($reportInventory, 'color', null),
            "pack"			=>	array_get($reportInventory, 'pack', null),
            "uom"			=>	array_get($reportInventory, 'uom', null),
            "in_hand_qty"	=>	array_get($reportInventory, 'in_hand_qty', null),
            "in_pick_qty"	=>	array_get($reportInventory, 'in_pick_qty', null),
            "total_qty"		=>	array_get($reportInventory, 'total_qty', null),
            "total_ctns"	=>	array_get($reportInventory, 'total_ctns', null),
            "created_at"	=>	$this->setTimeZone(array_get($reportInventory, 'whs_id', null), array_get($reportInventory->toArray(), 'created_at', null)),
            "updated_at"	=>	$this->setTimeZone(array_get($reportInventory, 'whs_id', null), array_get($reportInventory->toArray(), 'updated_at', null)),
            'timezone'      => 	$this->getWhsTimeZone(array_get($reportInventory, 'whs_id', null)) ?: 'UTC±0',
        ];
    }
}
