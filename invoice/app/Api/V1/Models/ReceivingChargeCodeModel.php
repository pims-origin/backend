<?php

namespace App\Api\V1\Models;

use \DateTime;
use App\Invoice;

use Illuminate\Support\Facades\DB;

use App\BaseModel;
use App\Api\V1\Models\CommonInvoiceModel;
use App\Api\V1\Models\VolumeRatesModel;
use App\Api\V1\Models\InvoicingSummaryModel;

use Seldat\Wms2\Models\RcvSum;
use Seldat\Wms2\Models\InvoiceCost;
use Seldat\Wms2\Models\InvoiceReceivingHistory;
use Seldat\Wms2\Models\InvoiceHistoryMonth;
use Seldat\Wms2\Models\GoodsReceipt;


class ReceivingChargeCodeModel extends AbstractModel
{
    /**
    * @var
    */
    protected $cusCharge;
    protected $uom;
    protected $vol;
    protected $invHistory;
    protected $hisMonth;
    protected $invSum = NULL;
    protected $invSumModel;

    /**
    *  Constructor
    */

    public function __construct()
    {
        $this->model =  new RcvSum();
        $this->cusCharge = new InvoiceCost();
        $this->invHistory = new InvoiceReceivingHistory();
        $this->hisMonth =  new  InvoiceHistoryMonth();
        $this->goodsReceipt = new GoodsReceipt();

        $this->uom = new CommonInvoiceModel();
        $this->vol = new VolumeRatesModel();
        
        $this->invSumModel = new InvoicingSummaryModel();
    }

    /**
    * Calculate Receiving charge codes
    *
    * @param params array
    * @param model mixed
    * @return array
    */


    public function calculate($params, $model)
    {
        $type = Invoice::RC;
        
        $custID = getDefault($params['custID'], NULL);
        $whsID = getDefault($params['whs_id'], NULL);

        $recNums = $params['details'] ?
            getDefault($params['items']['Receiving'], []) : [];

        if ($params['details'] && array_filter($params['items']) && ! $recNums) {
            return [$custID => 0];
        }

        // Get receiving history from inv_his_rcv table
        $hisCartonIDs = $this->getReceivingHisCarton($params, $recNums);

        // Calculate based on UOM
        $uomCusts = $model->customerUOMs($type);

        $custUOMs = $model->custUOMs($type);

        $allUOMs = array_keys($uomCusts);
        $allCusts = array_keys($custUOMs);

        // Don't run if there are no customers with charge codes
        if (! $allCusts) {
            return [];
        }

        // Set volume rates
        $this->vol->volumeRates($whsID, $model, $type);
        
        $chargeCodeValues = [];

        foreach ($allUOMs as $uom) {

            $custs = getDefault($uomCusts[$uom]);

            if (! $custs) {
                continue;
            }

            $results = $this->rcvQuery([
                'uom' => $uom,
                'custs' => $custs,
                'model' => $model,
                'params' => $params,
                'recNums' => $recNums,
                'hisCartonIDs' => $hisCartonIDs,
            ]);

            $vendorIDs = array_keys($results);
            $cartonCounts = array_column($results, 'rcvCount');

            $chargeCodeValues[$uom] = array_combine($vendorIDs, $cartonCounts);
        }

        //get vendors/qty for each charge code of Receiving
        $resultCosts = $this->getReceivingUOM($whsID, $custID, $type);

        $chargeDtls = [];
        foreach ($resultCosts as $row) {
            $cc = $row['chg_cd_name'];
            $uom = $row['uom'];
            $custID = $row['cust_id'];
            $count = getDefault($chargeCodeValues[$uom][$custID]);
            if ($count) {
                $chargeDtls[$cc][$custID] = $count;
            }
        }

        return $chargeDtls;
    }

    /**
     * Receiving charge codes
     *
     * @param array params
     * @param array recNums
     * @param string returnRecs
     * @return array
     */

    public function rcvQuery($passed)
    {
        $results = [];

        $query = null;

        $custs = getDefault($passed['custs']);
        
        if ($passed['uom'] == Invoice::$month && $passed['params']['checkItems']) {
            return $this->getReceivingMonth($passed['params'], $custs);
        }

        $recField = 'gr_hdr_id';
        $volField = 'vol';
        $dateField = 'dt';
        $custField = 'cus_id';
        $whsField = 'whs_id';

        $startDate = $passed['params']['startDate'];
        $endDate = $passed['params']['endDate'];

        switch ($passed['uom']) {
            case Invoice::$container:
                $query = $this->model
                               ->select('cus_id', DB::raw('SUM(cntr_val) AS rcvCount'));
                break;
            case Invoice::$smallCarton:
            case Invoice::$mediumCarton:
            case Invoice::$largeCarton:
            case Invoice::$smallCartonImperial:
            case Invoice::$mediumCartonImperial:
            case Invoice::$largeCartonImperial:

                $volField = DB::raw('ca.cube');
                $queryRes = $this->rcvRangeQuery($passed);
                
                $custField = $queryRes['custField'];
                $query = $queryRes['query'];
                $recField = $queryRes['recField'];
                $whsField = $queryRes['whsField'];
                break;
            case Invoice::$smallCartonMetric:
            case Invoice::$mediumCartonMetric:
            case Invoice::$largeCartonMetric:
                
                $volField = DB::raw('ROUND(ca.cube * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8)');
                $queryRes = $this->rcvRangeQuery($passed);

                $custField = $queryRes['custField'];
                $query = $queryRes['query'];
                $recField = $queryRes['recField'];
                $whsField = $queryRes['whsField'];
                break;
            case Invoice::$volume:
            case Invoice::$volumeImperial:
                $query = $this->model
                              ->select('cus_id', DB::raw('SUM(vol_val) AS rcvCount'));
                break;
            case Invoice::$volumeMetric:
                $selectVol = DB::raw('ROUND(SUM(vol_val) * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8) AS rcvCount');
                $query = $this->model
                              ->select('cus_id', $selectVol);
                break;
            case Invoice::$piece:
                $query = $this->model
                              ->select('cus_id', DB::raw('SUM(pcs_val) AS rcvCount'));
                break;
            case Invoice::$pallet:
                $query = $this->model
                              ->select('cus_id', DB::raw('SUM(plt_val) AS rcvCount'));
                break;
            case Invoice::$carton:
            default:
                $query = $this->model
                              ->select('cus_id', DB::raw('SUM(ctn_val) AS rcvCount'));
                break;
        }

        $cusQuery = $custs ? $query->whereIn($custField, $custs) : $query;

        $recQuery = $passed['recNums'] ? $cusQuery->whereIn($recField, $passed['recNums']) : $cusQuery;

        $resQuery = $passed['hisCartonIDs'] ? $recQuery->whereNotIn($recField, $passed['hisCartonIDs']) : $recQuery;

        $volParams = [
            'passed' => $passed,
            'volField' => $volField,
            'type' => Invoice::RC
        ];

        $volQuery = $this->vol->volClause($resQuery, $volParams, $custField);

        $rcvQuery = $volQuery ? $volQuery : $resQuery;

        $param = [
          'field'  => $dateField,
          'startDate' => $startDate,
          'endDate'  => $endDate,
          'whsID'  => $passed['params']['whs_id'],
          'whsField'  => $whsField
        ];

        $rcvResults = $rcvQuery
                        ->where(function($q) use( $param ) {
                            $q->where($param['field'], '>=', $param['startDate']);
                            $q->where($param['field'], '<=', $param['endDate']);

                            if ($param['whsID']) {
                                $q->where($param['whsField'], $param['whsID']);
                            }
                        })
                        ->groupBy($custField, $whsField);

        foreach ($rcvResults->get() as $value) {
            $cust = $value->cus_id;
            $results[$cust]['rcvCount'] = $value->rcvCount;
        }

        return $results;
    }

    /**
     * Receiving History Month
     *
     * @param array dates
     * @param array custs
     * @param mixed details
     * @return array
     */

    private function getReceivingMonth($dates, $custs, $details=FALSE)
    {
        if (! $custs) {
            return [];
        }

        $whsID = $dates['whs_id'];

        $sDate = $dates['startDate'];
        
        $curDate = CommonInvoiceModel::getDateTime('date');

        $curDtObj = new DateTime($curDate);
        $endDtObj = new DateTime($dates['endDate']);

        //check startdate with rcvDate
        $rcvDate = $this->invSumModel->getRcvDate($custs, $whsID);

        $startDate = $sDate >= $rcvDate ? $sDate : $rcvDate;
       
        $daterange = new \DatePeriod(
            new DateTime($startDate),
            new \DateInterval('P1M'),
            $endDtObj
        );

        $months = [];
        foreach($daterange as $date){
            $months[] = $date->format('Y-m');
        }

        // Incase end date is the first of a month
        $months[] = $endDtObj->format('Y-m');

        $curDTFormatted = $curDtObj->format('Y-m');
        
        $diff = array_diff($months, [$curDTFormatted]);

        $unique = array_unique($diff);

        if (! $unique) {
            return [];
        }

        $query = $this->hisMonth
                       ->select('cus_id', DB::raw('DATE_FORMAT(inv_date, "%Y-%m") AS dt'))
                       ->where('inv_sts', '=', 1)
                       ->where('type', 'r')
                       ->whereIn(DB::raw('DATE_FORMAT(inv_date, "%Y-%m")'), $unique);

        $monthResults = $query->where(function ($q) use ($custs, $whsID) {
                            if ($custs) {
                                $q->whereIn('cus_id', $custs);
                            }

                            if ($whsID) {
                                $q->where('whs_id', $whsID);
                            }
                        })
                        ->get();

        $billedCustDts = [];
        foreach ($monthResults as $row) {
            $custID = $row['cus_id'];
            $billedCustDts[$custID][] = $row['dt'];
        }

        $sums = $dts = [];
        foreach ($custs as $custID) {
            $custBilled = getDefault($billedCustDts[$custID], []);
            $found = array_diff($unique, $custBilled);

            $sums[$custID]['rcvCount'] = 0;

            foreach ($found as $dt) {
                $dt = $dt.'-01';
                $dts[$custID][$dt] = 1;
                $sums[$custID]['rcvCount'] += 1;
            }
        }

        return $details ? $dts : $sums;
    }


    /**
    * @param array $custs
    *
    * @return array
    */

    private function getReceivingUOM($whsID, $custs, $type=FALSE)
    {
        $query = $this->cusCharge
                        ->with([
                            'chargeCode' => function($q)
                                {
                                 $q->select('chg_code_id', 'chg_code', 'chg_code_name',
                                            'chg_type_id', 'chg_uom_id');
                                },
                            'chargeCode.chargeType' => function($q) {
                                   $q->select('chg_type_id', 'chg_type_name');
                            },
                            'chargeCode.systemUom' => function($q) {
                                    $q->select('sys_uom_id', 'sys_uom_name');
                            },
                            ])
                        ->whereHas('chargeCode.chargeType', function ($q) use($type) {
                                $q->where('chg_type_name', $type);
                          });

        $cusCharges = $query->where(function($q) use($custs, $whsID) {
                                if ($custs) {
                                    $q->where('cus_id', $custs);
                                }

                                if ($whsID) {
                                    $q->where('whs_id', $whsID);
                                }
                                
                                $q->where('chg_cd_price', '>', '0');
                        })
                        ->get();

        $results = [];

        foreach ($cusCharges as $value)
        {
            $results[] = [
                'chg_cd' =>  $value->chargeCode->chg_code,
                'chg_cd_name' =>  $value->chargeCode->chg_code_name,
                'uom' => $value->chargeCode->systemUom->sys_uom_name,
                'cust_id' => $value->cus_id,
                'price' => $value->chg_cd_price
            ];
        }
        return $results;
    }

     /**
     * get Receiving History cartons IDs
     *
     * @param array params
     * @param array recNums
     * @param string returnRecs
     * @return array
    */

    public function getReceivingHisCarton($params, $recNums=[])
    {
        $custID = getDefault($params['custID']);
        $whsID = getDefault($params['whs_id']);
        $startDate = getDefault($params['startDate']);
        $endDate = getDefault($params['endDate']);

        $query = $this->invHistory
                        ->select('gr_hdr_id')
                        ->where('inv_sts', '=', 1);
       
        if ($startDate) {
            $query->where('recv_dt', '>=', $params['startDate']);
        }

        if ($endDate) {
            $query->where('recv_dt', '<=', $params['endDate']);
        }

        $hisResults = $query->where(function ($q) use ($custID, $whsID, $recNums) {
                            if ($custID) {
                                $q->where('cus_id', $custID);
                            }

                            if ($whsID) {
                                $q->where('whs_id', $whsID);
                            }

                            if ($recNums) {
                                $q->whereIn('gr_hdr_id', $recNums);
                            }

                        })
                       ->get();

        $rec = [];
        if (!$hisResults->isEmpty()) {
            foreach ($hisResults as $row) {
                $rec[] = $row->gr_hdr_id;
            }
        }

        $hisCartonIDs = $hisResults ? array_values($rec) : [];

        return $hisCartonIDs;
    }
    
    /**
     * @param array passed
     * @return array
    */

    public function rcvRangeQuery($passed)
    {
        $custField = 'ca.cus_id';
        $query = DB::table('rcv_sum AS rs')
                    ->join('cartons AS ca', 'ca.gr_hdr_id', '=', 'rs.gr_hdr_id')
                    ->join('inv_vol_rates AS iv', 'iv.cus_id', '=', 'ca.cus_id')
                    ->join('charge_type AS ct', 'ct.chg_type_id', '=', 'iv.chg_type_id')
                    ->join('system_uom AS su', 'su.sys_uom_id', '=', 'iv.chg_uom_id')
                    ->where('su.sys_uom_name', $passed['uom'])
                    ->where('ct.chg_type_name', Invoice::RC)
                    ->select('ca.cus_id', 'ca.whs_id', DB::raw('COUNT(DISTINCT ctn_id) AS rcvCount'));

        $recField = 'rs.gr_hdr_id';
        $whsField = 'ca.whs_id';
  
        return [
            'custField' => $custField,
            'query' => $query,
            'recField' => $recField,
            'whsField' => $whsField
        ]; 
    }
}


