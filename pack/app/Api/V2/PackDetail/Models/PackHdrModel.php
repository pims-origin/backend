<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\PackDetail\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class PackHdrModel extends AbstractModel
{
    protected $packRef;

    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
        $this->packRef = [];
    }

    /**
     * @param $packHdrId
     *
     * @return mixed
     */
    public function deletePackHdr($packHdrId)
    {
        return $this->model
            ->where('ord_hdr_id', $packHdrId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrEqual = ['cus_id', 'odr_sts', 'odr_type'];

        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === "csr") {
                    $query->whereHas("csrUser", function ($q) use ($attributes) {
                        $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere(
                            DB::raw("concat(first_name, ' ', last_name)"),
                            'like',
                            "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['csr'])) . "%"
                        );
                    });
                    continue;
                }
                if ($key === "odr_num") {
                    $query->where($key, "like", "%" . SelStr::escapeLike($value) . "%");
                    continue;
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }

    public function getPackNum()
    {
        $pack = $this->model
            ->where('odr_num', 'like', 'ORD-' . (date("ym", time())) . "-%")
            ->packBy('odr_id', 'desc')
            ->first();
        $index = 0;
        $packNum = object_get($pack, "odr_num", null);

        if ($packNum) {
            $temp = explode("-", $packNum);
            $index = (int)end($temp);
        }

        return 'ORD-' . (date("ym", time())) . "-" . str_pad(++$index, 5, "0", STR_PAD_LEFT);
    }

    /**
     * @param $packIds
     *
     * @return mixed
     */
    public function checkWhereIn($packIds)
    {
        return $this->model
            ->whereIn('odr_id', $packIds)
            ->count();
    }

    /**
     * @param $pack_ids
     * @param null $odrHdrId
     * @param array $with
     *
     * @return mixed
     */
    public function getPackById($pack_ids, $odrHdrId = null, $with = [])
    {
        $pack_ids = is_array($pack_ids) ? $pack_ids : [$pack_ids];
        $query = $this->make($with);
        if (!empty($odrHdrId)) {
            $query->where('odr_hdr_id', $odrHdrId);
        }
        $query->whereIn('pack_hdr_id', $pack_ids);

        return $query->get();
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getPackOdrId($odr_ids)
    {
        $odr_ids = is_array($odr_ids) ? $odr_ids : [$odr_ids];

        $rows = $this->model
            ->select('SUM(sku_ttl) as sku_ttl')
            ->whereIn('odr_hdr_id', $odr_ids)
            ->get();

        return $rows;
    }

    public function getAllDistinctOrderIds()
    {
        $rows = $this->model
            ->distinct('odr_hdr_id')
            ->get(['odr_hdr_id']);

        return $rows;
    }

    public function getPackDtlByPackId_and_CheckSum(
        $pack_ids,
        $with = [],
        array $orderBy = [],
        $columns = ['*'],
        $or = false
    ) {
        $pack_ids = is_array($pack_ids) ? $pack_ids : [$pack_ids];
        $query = $this->make($with);
        $query->whereIn('odr_hdr_id', $pack_ids);
        $query->groupBy('pack_dt_checksum')
            ->select(DB::raw('count(pack_hdr_id) as total, pack_hdr_id, pack_dt_checksum'));

        return $query->get();
    }

    public function getFistPackNotAssignByCheckSum($checksum, $odr_id, $with = [])
    {
        $query = $this->make($with);
        $result = $query->where('pack_dt_checksum', $checksum)
            ->where('odr_hdr_id', $odr_id)
            ->whereNull('out_plt_id')
            ->first();

        return $result;
    }


    public function insertBatch($packDatas, $odr_id)
    {
        $numPack = count($packDatas);
        $this->model->insert($packDatas);
        $checkSum = array_pluck($packDatas, 'pack_dt_checksum');
        $result = $this->model->where('odr_hdr_id', $odr_id)
            ->where('created_by', $this->getUserId())
            ->whereIn('pack_dt_checksum', $checkSum)
            ->take($numPack)->skip(0)
            ->orderBy('pack_hdr_id', 'DESC')
            ->get(['pack_hdr_id', 'pack_hdr_num']);

        return $result;
    }

    public function getSumPieceByOrder($odr_ids)
    {
        $rows = $this->model
            ->select(DB::raw('SUM(piece_ttl) as pieces_ttl'))
            ->whereIn('odr_hdr_id', $odr_ids)
            ->first();

        return $rows;
    }

    public function autoPack($odrDtlIds, $odrId)
    {
        /*
         * get order carton with pack = picked qty by item
         * create pack
         * create event
         * save pack
         * save event
         * change order status
         */

        $orderCarton = new OrderCartonModel();
        $OrderHdrModel = new OrderHdrModel();
        $eventTrackingModel = new EventTrackingModel(new EventTracking());
        $orderHdr = $OrderHdrModel->getFirstWhere(['odr_id' => $odrId]);

        $data = $orderCarton->getCartonByOdrDtl($odrDtlIds);
        $packHdr = [];
        $eventData = [];
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        $seq = 0;

        foreach ($data as $carton) {

            $eventData[] = [
                'whs_id'    => $carton['whs_id'],
                'cus_id'    => $carton['cus_id'],
                'owner'     => $carton['odr_num'],
                'evt_code'  => Status::getByKey("Event", "PACKING"),
                'trans_num' => $carton['ctn_num'],
                'created_at' => time(),
                'created_by' => $userId,
                'info'      => sprintf(Status::getByKey("Event-Info", "PACKING"), $carton['ctn_num']),
            ];
            $eventData[] = [
                'whs_id'    => $carton['whs_id'],
                'cus_id'    => $carton['cus_id'],
                'owner'     => $carton['odr_num'],
                'evt_code'  => Status::getByKey("Event", "PACKING-COMPLETE"),
                'created_at' => time(),
                'created_by' => $userId,
                'trans_num' => $carton['ctn_num'],
                'info'      => sprintf(Status::getByKey("Event-Info", "PACKING-COMPLETE"), $carton['ctn_num']),
            ];

            $pack_type = "CT";
            $width = $carton['width'];
            $height = $carton['height'];
            $length = $carton['length'];
            $pack_ref_id = $this->insertDimension($length, $width, $height, $pack_type);

            $packHdr[] = [
                'pack_hdr_num'     => $carton['ctn_num'],
                'odr_hdr_id'       => $odrId,
                'cnt_id'           => $carton['ctn_id'],
                'whs_id'           => $carton['whs_id'],
                'cus_id'           => $carton['cus_id'],
                'seq'              => ++$seq,
                'carrier_name'     => '',
                'sku_ttl'          => 1,
                'piece_ttl'        => $carton['piece_qty'],
                'pack_sts'         => 'NW',
                'created_at'       => time(),
                'updated_at'       => time(),
                'created_by'       => $carton['created_by'],
                'updated_by'       => $carton['updated_by'],
                'deleted_at'       => $carton['deleted_at'],
                'sts'              => 'I',
                'ship_to_name'     => '',
                'out_plt_id'       => null,
                'pack_type'        => $pack_type,
                'width'            => $carton['width'],
                'height'           => $carton['height'],
                'length'           => $carton['length'],
                'pack_ref_id'      => $pack_ref_id,
                'deleted'          => 0,
                'is_print'         => 0,
                //'pack_dt_checksum' => $carton['item_id'],
                'pack_dt_checksum' => $carton['item_id']."-".$carton['lot'],
            ];
        }

        $size = 1500;
        //auto pack process
        if (!empty($packHdr)) {
            $isFirst = true;
            foreach (array_chunk($packHdr, $size) as $key => $pack) {
                if($key>0) $isFirst = false;
                DB::table('pack_hdr')->insert($pack);
                $ctnIds = array_column($pack, 'cnt_id');
                $ctnIds = array_unique($ctnIds);
                $event = array_slice($eventData, $key * $size, $size);
                // get pack hdr
                $phList = DB::table('pack_hdr as ph')
                    ->join('cartons as c', 'c.ctn_id', '=', 'ph.cnt_id')
                    ->join('odr_cartons as oc', 'oc.ctn_id', '=', 'c.ctn_id')
                    ->whereIn('c.ctn_id', $ctnIds)
                    ->groupBy('c.ctn_id')
                    ->select('ph.pack_hdr_id', 'ph.odr_hdr_id', 'c.whs_id', 'c.item_id', 'c.cus_id',
                        'c.lot', 'c.sku', 'c.size', 'c.color', 'oc.upc', 'c.ctn_uom_id',
                        'oc.piece_qty', 'ph.created_by', 'ph.updated_by'
                    )->get();
                $packDtls = [];

                foreach ($phList as $ph) {
                    $packDtls[] = [
                        'pack_hdr_id' => array_get($ph, 'pack_hdr_id'),
                        'odr_hdr_id'  => array_get($ph, 'odr_hdr_id'),
                        'whs_id'      => array_get($ph, 'whs_id'),
                        'cus_id'      => array_get($ph, 'cus_id'),
                        'item_id'     => array_get($ph, 'item_id'),
                        'lot'         => array_get($ph, 'lot'),
                        'sku'         => array_get($ph, 'sku'),
                        'size'        => array_get($ph, 'size'),
                        'color'       => array_get($ph, 'color'),
                        'cus_upc'     => array_get($ph, 'upc'),
                        'uom_id'      => array_get($ph, 'ctn_uom_id'),
                        'piece_qty'   => array_get($ph, 'piece_qty'),
                        'sts'         => 'i',
                        'created_at'  => time(),
                        'updated_at'  => time(),
                        'deleted'     => 0,
                        'deleted_at'  => 915148800,
                        'created_by'  => array_get($ph, 'created_by'),
                        'updated_by'  => array_get($ph, 'updated_by'),

                    ];
                }

                if (!empty($packDtls)) {
                    DB::table('pack_dtl')->insert($packDtls);
                }
                $this->updateOrderStatus($odrId, $event, $isFirst);
                $eventTrackingModel->createBatch($event);
            }
        }
    }

    public function insertDimension($length, $width, $height, $pack_type)
    {
        $key = $pack_type . $length . $width . $height;
        if (!array_key_exists($key, $this->packRef)) {
            $dimension = sprintf("%sx%sx%s", $length, $width, $height);
            $packRefModel = new PackRefModel();
            $result = $packRefModel->getFirstWhere([
                'dimension' => $dimension,
                'pack_type' => $pack_type

            ]);
            if (empty($result)) {
                return null;
                $packRefModel = new PackRefModel();
                $result = $packRefModel->create([
                    'width'      => $width,
                    'height'     => $height,
                    'length'     => $length,
                    'dimension'  => $dimension,
                    'created_at' => time(),
                    "updated_at" => time(),
                    'pack_type'  => $pack_type
                ]);
            }
            $this->packRef[$key] = object_get($result, 'pack_ref_id');
        }

        return $this->packRef[$key];
    }

    /**
     * @param $orderHdrId
     * @param $events
     * @param bool|true $isCreate
     */
    public function updateOrderStatus($orderHdrId, &$events, $isCreate = false)
    {
        $orderHdrModel = new OrderHdrModel();
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        // Order
        $orderHdr = $orderHdrModel->getFirstBy("odr_id", $orderHdrId, ['details'])->toArray();

        // Create Event Tracking
        if ($isCreate) {
            array_unshift($events, [
                'whs_id'     => $orderHdr['whs_id'],
                'cus_id'     => $orderHdr['cus_id'],
                'owner'      => $orderHdr['odr_num'],
                'evt_code'   => Status::getByKey("Event", "ORDER-PACKING"),
                'trans_num'  => $orderHdr['odr_num'],
                'created_at' => time(),
                'created_by' => $userId,
                'info'       => sprintf(Status::getByKey("Event-Info", "ORDER-PACKING"), $orderHdr['odr_num']),
            ]);
        }

        // Update sts odr_dtl
        $sqlStr = "
            odr_dtl.picked_qty <= (
                SELECT SUM(piece_qty)
                FROM pack_dtl
                WHERE pack_dtl.odr_hdr_id = odr_dtl.odr_id
                    AND pack_dtl.item_id = odr_dtl.item_id
                    AND pack_dtl.lot = odr_dtl.lot
            )

        ";
        DB::table('odr_dtl')
            ->where('odr_dtl.odr_id', $orderHdrId)
            ->where('odr_dtl.picked_qty', ">", 0)
            ->where('odr_dtl.deleted', 0)
            ->whereRaw($sqlStr)
            ->update(['odr_dtl.itm_sts' => 'PA']);

        // Update sts odr_hdr
        $sqlStr = "
            0 = (
                SELECT COUNT(1) FROM odr_dtl
                WHERE odr_hdr.odr_id = odr_dtl.odr_id
                    AND odr_dtl.deleted = 0
                    AND odr_dtl.picked_qty > 0
                    AND odr_dtl.itm_sts != 'PA'
            )
        ";
        $isPa = DB::table('odr_hdr')
            ->where('odr_hdr.odr_id', $orderHdrId)
            ->whereRaw($sqlStr)
            ->update(['odr_hdr.odr_sts' => 'PA']);

        //event complete pack
        if ($isPa) {
            $events[] = [
                'whs_id'     => $orderHdr['whs_id'],
                'cus_id'     => $orderHdr['cus_id'],
                'owner'      => $orderHdr['odr_num'],
                'evt_code'   => Status::getByKey("Event", "ORDER-PACKING-COMPLETE"),
                'trans_num'  => $orderHdr['odr_num'],
                'created_at' => time(),
                'created_by' => $userId,
                'info'       => sprintf(Status::getByKey("Event-Info", "ORDER-PACKING-COMPLETE"),
                    $orderHdr['odr_num']),
            ];
        }

        return $isPa;
    }

    public function updatePackedStatus($orderHdrId){
        // Update sts odr_dtl
        $sqlStr = "
            odr_dtl.alloc_qty = (
                SELECT SUM(piece_qty)
                FROM pack_dtl
                WHERE pack_dtl.odr_hdr_id = odr_dtl.odr_id
                    AND pack_dtl.item_id = odr_dtl.item_id
                    AND pack_dtl.lot = odr_dtl.lot
            )

        ";
        DB::table('odr_dtl')
            ->where('odr_dtl.odr_id', $orderHdrId)
            ->where('odr_dtl.deleted', 0)
            ->whereRaw($sqlStr)
            ->update(['odr_dtl.itm_sts' => 'PA']);

        // Update sts odr_hdr
        $sqlStr = "
            0 = (
                SELECT COUNT(1) FROM odr_dtl
                WHERE odr_hdr.odr_id = odr_dtl.odr_id
                    AND odr_dtl.deleted = 0
                    AND odr_dtl.itm_sts != 'PA'
                    AND odr_dtl.lot!='Any'
            )
        ";
        DB::table('odr_hdr')
            ->where('odr_hdr.odr_id', $orderHdrId)
            ->whereRaw($sqlStr)
            ->update(['odr_hdr.odr_sts' => 'PA']);
    }

    /**
     * @param $orderHdrId
     *
     * @return mixed
     */
    public function countPacksByOdrHdrId($orderHdrId)
    {
        return $this->model
            ->where('odr_hdr_id', $orderHdrId)
            ->count();
    }
}
