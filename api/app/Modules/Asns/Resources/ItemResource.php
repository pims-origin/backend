<?php

namespace App\Modules\Asns\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    public function toArray($request)
    {
        $poDate = ($this->asn_dtl_po_dt && $this->asn_dtl_po_dt != 0) ? date('Y-m-d', $this->asn_dtl_po_dt) : NULL;
        $exPiredDate = ($this->expired_dt && $this->expired_dt != 0) ? date('Y-m-d', $this->expired_dt) : NULL;

        return [
            'PO'            => $this->asn_dtl_po,
            'PO_date'       => $poDate,
            'SKU'           => $this->asn_dtl_sku,
            'Size'          => $this->asn_dtl_size,
            'Color'         => $this->asn_dtl_color,
            'Lot'           => $this->asn_dtl_lot,
            'Description'   => $this->asn_dtl_des,
            'UOM'           => $this->uom_code,
            'Pack'          => $this->asn_dtl_pack,
            'expected_qty'  => $this->asn_dtl_qty_ttl,
            'expected_ctns' => $this->asn_dtl_ctn_ttl,
            'xdock_qty'     => $this->asn_dtl_crs_doc_qty,
            'xdock_ctns'    => $this->asn_dtl_crs_doc,
            'expired_date'  => $exPiredDate,
            'meta'          => $this->asnDtlMeta ? json_decode($this->asnDtlMeta->value) : NULL
        ];
    }
}