<?php

namespace App\Api\V1\Traits;

use App\Api\V1\Models\AbstractModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\InventoryModel;
use App\Jobs\AutoCreateBillable;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Jobs\AutoUpdateDailyInventoryAndPalletReport;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use App\Api\V1\Models\inventoriesModel;

use App\Jobs\CorrectDataJob;
use App\Jobs\GoodsReceiptRecevingJob;

/**
 * Class GoodsReceiptControllerTrait
 *
 * @package App\Api\V1\Traits
 * @property $goodsReceiptModel
 */
trait GoodsReceiptControllerTrait
{
    /**
     * @param $whsId
     * @param $grHdrId
     * @param Request $request
     *
     * @return array
     */


    public function complete($whsId, $grHdrId, Request $request)
    {
        // Get data from HTTP
        $input = $request->getQueryParams();
        $inputPost = $request->getParsedBody();
        $agree   = array_get($input, 'agree', false);
        $forceGr = array_get($input, 'force_gr', false);
        $inNote  = array_get($inputPost, 'in_note', null);
        $exNote  = array_get($inputPost, 'ex_note', null);
        $actGrDt = date("Y-m-d H:i:s");
        
        try {
            $grHdr = $this->goodsReceiptModel->getGoodsReceiptById($grHdrId);
            
            if (!$this->checkVaildItemReceived($grHdrId)) {
                return $this->response->errorBadRequest("Please approve the items in the list before complete!");
            }
            
            if (!$grHdr) {
                throw new \Exception(Message::get("BM017", "Goods Receipt"));
            }
            
            if ($grHdr->gr_sts == 'RE') {
                throw new \Exception('Goods Receipt is already Received');
            }

            if ($grHdr->created_from == 'GUN') {
                return $this->gunComplete($whsId, $grHdr, $request);
            }

            if($grHdr->created_from == 'WMS'){
                if(!$grHdr->putaway){
                    return $this->response->errorBadRequest("Goods Receipt Pallets don't putaway all yet");
                }

                return $this->wmsComplete($whsId, $grHdr, $request);
            }
            
            // Created fom WAP
            if ($grHdr->created_from == 'WAP') {
                $chkPalletPutaways = $this->palletModel->getModel()
                    ->where(['gr_hdr_id' => $grHdrId])
                    ->whereNotIn('plt_sts', ['IA', 'CC', 'AJ'])
                    ->where('ctn_ttl', '>', 0)
                    ->get();
                foreach ($chkPalletPutaways as $chkPalletPutaway)
                {
                    if (!$chkPalletPutaway->loc_id) {
                        $msgError = sprintf('The Pallet %s belong to this Goods Receipt has not putaway yet.', $chkPalletPutaway->rfid);
                        throw new \Exception($msgError);
                    }
                }

                // WMS2-5581 - [Inbound - Goods Receipt] Unable To Completed The Good Receipt When Cartons do not putaway yet.
                // $chkCartonsPutaways = $this->cartonModel->getModel()
                //     ->where(['gr_hdr_id' => $grHdrId])
                //     ->whereNotIn('ctn_sts', ['IA', 'CC', 'AJ'])
                //     ->get();
                // foreach ($chkCartonsPutaways as $chkCartonsPutaway) {
                //         if (!$chkCartonsPutaway->loc_id) {
                //             $msgError = sprintf('The Carton %s belong to this Goods Receipt has not putaway yet.', $chkCartonsPutaway->rfid);
                //             throw new \Exception($msgError);
                //         }
                //     }
            }

            if ($grHdr->created_from != 'WAP') {
                throw new \Exception('Goods Receipt has not been created from WAP');
            }

            $grDtls = $this->goodsReceiptDetailModel->getGoodsReceiptDetailByGrHdrId($grHdrId);

            if (!$grDtls) {
                throw new \Exception('There is no Goods Receipt Details');
            }

            if ( !$this->checkValidDateCompleteGr($grHdr, $actGrDt) ){
                throw new \Exception('The actual date must be in last month or sooner. Please contact to admin.');
            }

            DB::beginTransaction();

            $this->palletModel->getModel()->where('gr_hdr_id', $grHdrId)->where('ctn_ttl', 0)
            ->update([
                'plt_sts'    => 'IA',
                'deleted'    => 1,
                'deleted_at' => time()
            ]);
            // Update damaged cartons (cartons.is_damage = 1) with lot = 'DAMAGE'
            // $cartonsOfGrHdr = $grHdr->cartons;
            // foreach ($cartonsOfGrHdr as $item) {
            //     if($item->is_damaged == 1){
            //         $item->is_damaged = 0;
            //         $item->lot = 'DAMAGE';
            //         $item->save();
            //     }
            // }

            $vtlCtns = DB::table('vtl_ctn')
                ->where('ctnr_id', $grHdr->ctnr_id)
                ->where('asn_hdr_id', $grHdr->asn_hdr_id)
                ->whereNull('plt_rfid')
                ->where('deleted', 0)
                ->get();

            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $asnDtls = DB::table('asn_dtl')
                ->where('ctnr_id', $grHdr->ctnr_id)
                ->where('asn_hdr_id', $grHdr->asn_hdr_id)
                ->where('deleted', 0)
                ->get();

            $asnCtns = [];
            if (!empty($vtlCtns)) {

                // Error 1

                foreach ($grDtls->toArray() as $grDtl) {
                    /**
                     * get
                     */
                    foreach ($vtlCtns as $index => $ctn) {
                        if ($ctn['asn_dtl_id'] == $grDtl['asn_dtl_id']) {
                            $grDtl['ctn_rfid'] = $ctn['ctn_rfid'];
                            $asnCtns[] = [
                                'item_id'       => array_get($grDtl, 'item_id'),
                                'asn_dtl_sku'   => array_get($grDtl, 'sku', null),
                                'asn_dtl_size'  => array_get($grDtl, 'size', null),
                                'asn_dtl_color' => array_get($grDtl, 'color', null),
                                'asn_dtl_lot'   => array_get($grDtl, 'lot', 'NA'),
                                'asn_dtl_pack'  => array_get($grDtl, 'pack', null),
                                'ctn_rfid'      => array_get($grDtl, 'ctn_rfid', null)
                            ];
                        }
                    }
                }

                if ($forceGr) {
                    $this->_forceGrWithVtlCtnNotScannedGW($vtlCtns);
                } else {
                    return [
                        'data'    => $asnCtns,
                        'error'   => 1,
                        'message' => 'Cartons are not assigned to pallet'
                    ];
                }

            } else {

                // If ans dtl not in gr_dtl return these asn_dtl
                $asnDtls = array_filter($asnDtls, function ($asnDtl) use ($grDtls) {
                    $asnDtlIds = array_pluck($grDtls->toArray(), 'asn_dtl_id');
                    return (!in_array($asnDtl['asn_dtl_id'], $asnDtlIds) && $asnDtl['asn_dtl_sts'] != "CC");
                });

                if (count($asnDtls)) {
                    if ($agree) {
                        /// Create all gr dtls from $asnDtls with actual ctn ttl = 0
                        $this->createGoodsReceiptDetails($grHdr->gr_hdr_id, $asnDtls);
                    } else {
                        // Error 2
                        $arr = [];
                        foreach ($asnDtls as $item) {
                            $arr[] = $item;
                        }

                        return [
                            'data'    => $arr,
                            'error'   => 2,
                            'message' => 'Missing SKUs in GR'
                        ];
                    }
                }
            }

            // WMS2-5190 - Save gr_dtl_act_qty_ttl,gr_dtl_ept_qty_ttl  when Complete Goods Receipt
            $this->goodsReceiptDetailModel->computeGrDtlQty($grHdrId);

            // WAP-583 - [Inbound - Pallet] Update Scan Pallet about Inventory and Status
            $this->_updatePalletAndCartonStatusAndInventory($grHdrId, $grHdr, $request);

            $this->goodsReceiptDetailModel->updateGoodsReceiptDetailReceived($grHdrId);

            $res = $this->goodsReceiptModel->updateGRRecevied($grHdrId, $inNote, $exNote, $actGrDt);

            $this->goodsReceiptModel->updateContainerInfo($grHdrId, $inputPost);
            if ($res) {
                $gr_hdr_num = $grHdr->gr_hdr_num;
                // Add Event Tracking Goods Receipt
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $whsId,
                    'cus_id'    => $grHdr->cus_id,
                    'owner'     => $gr_hdr_num,
                    'evt_code'  => Status::getByKey("Event", "GR-COMPLETE"),
                    'trans_num' => $gr_hdr_num,
                    'info'      => 'GR Received - Complete GR'
                ]);

                // Turn on putaway when enough pallets have put on rack
                $this->_turnOnPutAway($grHdrId, $grHdr);

                // Checking when it is just only support WAP
                if (object_get($grHdr, 'created_from') == "WAP") {
                    // Create pallet suggest location
                    $this->_palletSuggestLocation($grHdrId, $grHdr);
                }
            }

            $this->asnDtlModel->updateAsnDtlsReceivedByCtnrID($grHdr->ctnr_id, $grHdr->asn_hdr_id);
            $resAsn = $this->asnHdrModel->updateAsnHdrReceived($grHdr->asn_hdr_id);
            if ($resAsn) {
                $ansHdr = $this->asnHdrModel->getAsnHdrById($grHdr->asn_hdr_id);
                $asnNum = $ansHdr->asn_hdr_num;
                // Add Event Tracking Asn received
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $whsId,
                    'cus_id'    => $grHdr->cus_id,
                    'owner'     => $asnNum,
                    'evt_code'  => Status::getByKey("Event", "ASN-COMPLETE"),
                    'trans_num' => $asnNum,
                    'info'      => sprintf(Status::getByKey("Event-info", "ASN-COMPLETE"), $asnNum)
                ]);
            }
            // Update gr_dt in pallet table
            DB::table('pallet AS p')
                ->where('p.gr_hdr_id', $grHdrId)
                ->where('p.plt_sts', 'RG')
                ->update([
                    'p.plt_sts'      => 'AC',
                    'p.gr_dt'        => time(),
                ]);



            $this->inventoryModel->updateInvent($grHdrId, $whsId);

            $this->correctInventoryReportByGR($grHdrId);

            DB::commit();

            // Correct Data
            $userId = Data::getCurrentUserId();
            dispatch(new CorrectDataJob($request, $grHdrId, $userId));

            // Auto create Billable
            dispatch(new AutoCreateBillable($whsId, $grHdr->cus_id, $grHdr->gr_hdr_id, $request));

            // Send data to IMS
            dispatch(new GoodsReceiptRecevingJob($grHdr->gr_hdr_id));
            
            $invDate = strtotime(date('Y-m-d'));
            // Dispatch(new AutoUpdateDailyInventoryAndPalletReport($whsId, $grHdr->cus_id, $invDate));

            $this->dispatchEdiQueue($grHdr);

            return [
                'message' => 'Update complete Goods Receipt successfully!'
            ];

        } catch (\PDOException $e) {

            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function dispatchEdiQueue($grHdr) {
        //push MQ receiving
        $receivingReports = $this->receivingModel->receiving($grHdr->asn_hdr_id);
        if ($receivingReports && $this->customerConfigModel->checkWhere([
                'whs_id'       => $grHdr->whs_id,
                'cus_id'       => $grHdr->cus_id,
                'config_name'  => config('constants.cus_config_name.EDI_INTEGRATION'),
                'config_value' => config('constants.cus_config_value.EDI861'),
                'ac'           => config('constants.cus_config_active.YES')
            ])
        ) {
            $this->receivingService->init($receivingReports);
            $this->receivingService->process();
        }
    }

    private function insertInvtSmr($grHdr, $grDtls)
    {
        if (!empty($grDtls)) {

            $whsId = $grHdr->whs_id;
            $cusId = $grHdr->cus_id;
            $insertInvtSmr = [];

            $created_at = time();

            foreach ($grDtls as $grDtl)
            {
                if ($grDtl->gr_dtl_sts == 'CC') {
                    continue;
                }
                $ivt = $this->inventorySummaryModel
                    ->getFirstWhere([
                        'item_id' => $grDtl['item_id'],
                        'lot'     => $grDtl['lot'],
                        'whs_id'  => $whsId
                    ]);

                if (!$ivt) {

                    $insertInvtSmr[$grDtl['item_id'].'-'.$grDtl['lot'].'-'.$whsId] = [
                        'item_id'       => $grDtl['item_id'],
                        'cus_id'        => $cusId,
                        'whs_id'        => $whsId,
                        'color'         => array_get($grDtl, "color", null),
                        'sku'           => array_get($grDtl, "sku", null),
                        'size'          => array_get($grDtl, "size", null),
                        'lot'           => $grDtl['lot'],
                        'ttl'           => 0,
                        'allocated_qty' => 0,
                        'dmg_qty'       => 0,
                        'avail'         => 0,
                        'back_qty'      => 0,
                        'upc'           => array_get($grDtl, "upc", null),
                        'crs_doc_qty'   => 0,
                        'created_at'    => $created_at,
                        'updated_at'    => $created_at
                    ];
                }

                //---- Insert Inbound data for SKU Tracking report
                $cusId = $grHdr->cus_id;
                $cusInfo = DB::Table('customer')
                    ->where('customer.cus_id', $cusId)
                    ->first();

                $itemId = $grDtl['item_id'];
                $itemInfo = DB::Table('item')
                    ->where('item.item_id', $itemId)
                    ->first();
                $length = array_get($itemInfo, 'length', '');
                $width = array_get($itemInfo, 'width', '');
                $height = array_get($itemInfo, 'height', '');

                $dataSKUTrackingRpt = [
                    'cus_id'   => $cusId,
                    'cus_name' => array_get($cusInfo, 'cus_name', ''),
                    'cus_code' => array_get($cusInfo, 'cus_code', ''),

                    'gr_hdr_id'     => $grHdr->gr_hdr_id,
                    'trans_num'     => $grHdr->gr_hdr_num,
                    'whs_id'        => $grHdr->whs_id,
                    'po_ctnr'       => $grHdr->ctnr_num,
                    'ref_cus_order' => (!empty($grHdr->ref_code)) ? $grHdr->ref_code : "",
                    'actual_date'   => $grHdr->gr_hdr_act_dt ?: time(),

                    'item_id' => array_get($itemInfo, 'item_id', ''),
                    'sku'     => array_get($itemInfo, 'sku', ''),
                    'size'    => array_get($itemInfo, 'size', ''),
                    'color'   => array_get($itemInfo, 'color', ''),
                    'pack'    => array_get($itemInfo, 'pack', ''),
                    'lot'     => (!empty(array_get($itemInfo, 'lot', ''))) ? array_get($itemInfo, 'lot', '') : 'NA',
                    'qty'     => array_get($grDtl, 'gr_dtl_act_qty_ttl', ''),
                    'ctns'    => array_get($grDtl, 'gr_dtl_act_ctn_ttl', ''),

                    'cube' => array_get($itemInfo, 'cube', ''),
                ];
                $this->SKUTrackingReportModel->refreshModel();
                $this->SKUTrackingReportModel->create($dataSKUTrackingRpt)->toArray();
                //---- /Insert Inbound data for SKU Tracking report


            }

            if (!empty($insertInvtSmr)) {
                foreach (array_chunk($insertInvtSmr, 200) as $data) {
                    $this->inventorySummaryModel->refreshModel();
                    $this->inventorySummaryModel->getModel()->insert(array_values($data));
                }
            }
        }
    }

    private function insertInventory($grHdr, $grDtls)
    {
        if (!empty($grDtls)) {

            $whsId = $grHdr->whs_id;
            $cusId = $grHdr->cus_id;

            foreach ($grDtls as $grDtl) {
                if ($grDtl->gr_dtl_sts == 'CC') {
                    continue;
                }
                $this->inventoriesModel->insertInventory($whsId, $cusId, $grDtl['item_id']);
            }
        }
        return 0;
    }

    /**
     * @param $grHdr
     * @param $grDtls
     */
    private function insertInvtSmrDamageCtn($grHdr, $grDtls)
    {
        if (!empty($grDtls)) {

            $whsId = $grHdr->whs_id;
            $cusId = $grHdr->cus_id;
            $insertInvtSmr = [];

            $created_at = time();

            foreach ($grDtls as $grDtl)
            {
                if ($grDtl->gr_dtl_is_dmg != 1) {
                    continue;
                }
                $ivt = $this->inventorySummaryModel
                    ->getFirstWhere([
                        'item_id' => $grDtl['item_id'],
                        'lot'     => 'DAMAGE',
                        'whs_id'  => $whsId
                    ]);

                if (!$ivt && $grDtl->gr_dtl_is_dmg == 1) {

                    $insertInvtSmr[$grDtl['item_id'].'-'.'DAMAGE'.'-'.$whsId] = [
                        'item_id'       => $grDtl['item_id'],
                        'cus_id'        => $cusId,
                        'whs_id'        => $whsId,
                        'color'         => array_get($grDtl, "color", null),
                        'sku'           => array_get($grDtl, "sku", null),
                        'size'          => array_get($grDtl, "size", null),
                        'lot'           => 'DAMAGE',
                        'ttl'           => 0,
                        'allocated_qty' => 0,
                        'dmg_qty'       => 0,
                        'avail'         => 0,
                        'back_qty'      => 0,
                        'upc'           => array_get($grDtl, "upc", null),
                        'crs_doc_qty'   => 0,
                        'created_at'    => $created_at,
                        'updated_at'    => $created_at
                    ];
                }
            }

            if (!empty($insertInvtSmr)) {
                $this->inventorySummaryModel->refreshModel();
                $this->inventorySummaryModel->getModel()->insert(array_values($insertInvtSmr));
            }
        }
    }

    /**
     * pallet suggestion location
     *
     * @param $goodsReceiptId
     *
     * @throws \Exception
     */
    private function _palletSuggestLocation($goodsReceiptId, $goodsReceipt)
    {
        try {
            $dataSuggest = $this->palletSuggestLocationModel->GetPalSugLoc($goodsReceiptId)->toArray();

            // if (empty($dataSuggest)) {
            // $grData = $this->palletModel->FindWhere(
            //     ['gr_hdr_id' => $goodsReceiptId],
            //     ['grDtl']
            // );

            // Get pallets exect someone that has not suggested location
            $grData = $this->palletModel->getPalletNotExistOnPalSugLoc($goodsReceiptId);
            if ($grData) {
                foreach ($grData as $pallet) {
                    $grDtl = $this->goodsReceiptDetailModel->getFirstBy('gr_dtl_id', $pallet->gr_dtl_id);
                    $pallet['gr_dtl'] = $grDtl;
                }

                $numOfPallets = count($grData);

                $locations = $this->palletModel->suggestLocations($goodsReceipt->cus_id, $goodsReceipt->whs_id,
                    $numOfPallets);

                if (count($locations) < $numOfPallets) {
                    $msg = sprintf("Number of locations is insufficient to assign to %d pallets.",
                        $numOfPallets);

                    return $this->response
                        ->errorBadRequest($msg);
                }
                $this->palletSuggestLocationModel->createSuggestionLocation($goodsReceipt, $grData,
                    $locations);

                // $dataSuggest = $this->palletSuggestLocationModel->GetPalSugLoc($goodsReceiptId)->toArray();

                /**
                 * event tracking Put Away update here
                 */
                $gr_HdrNum = $goodsReceipt->gr_hdr_num;
                $this->eventTrackingModel->refreshModel();
                $data = [
                    'whs_id'    => $goodsReceipt->whs_id,
                    'cus_id'    => $goodsReceipt->cus_id,
                    'owner'     => $gr_HdrNum,
                    //'evt_code'  => config('constants.event.SUGGEST-LOCATION'),
                    'evt_code'  => Status::getByKey('EVENT', 'SUGGEST-LOCATION'),
                    'trans_num' => $gr_HdrNum,
                    'info'      => sprintf('Suggest  %d location(s) for %d pallet(s)', count($locations), $numOfPallets)
                ];
                $this->eventTrackingModel->create($data);

            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * checking enough pallets have put on rack then turn on putaway within gr_hdr table
     *
     * @param $grHdrId
     *
     * @throws \Exception
     */
    private function _turnOnPutAway($grHdrId, $grHdrObj)
    {
        try {

            $totalPallet = $this->goodsReceiptDetailModel->palletTotalOfGrHdr($grHdrId);
            $countPallet = $this->palletModel->countPalletOnRackByGRHdrId($grHdrId);

            if ($totalPallet == $countPallet) {
                $grHdrObj->putaway = 1;
                $grHdrObj->save();
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function createGoodsReceiptDetails($gr_hdr_id, $asnDtls)
    {

        foreach ($asnDtls as $asnDtl) {

            $packCtn = array_get($asnDtl, 'asn_dtl_pack', 0);
            $dataGrDtl = [
                'asn_dtl_id'         => array_get($asnDtl, 'asn_dtl_id'),
                'gr_hdr_id'          => $gr_hdr_id,
                'gr_dtl_act_ctn_ttl' => 0,
                'gr_dtl_ept_ctn_ttl' => intval(array_get($asnDtl, "asn_dtl_ctn_ttl", 0)),
                'gr_dtl_disc'        => intval(array_get($asnDtl, "asn_dtl_ctn_ttl", 0) * -1),
                'gr_dtl_plt_ttl'     => 0,
                'gr_dtl_dmg_ttl'     => 0,
                'gr_dtl_is_dmg'      => 0,
                'gr_dtl_sts'         => "RE",
                'item_id'            => array_get($asnDtl, 'item_id', ''),
                'sku'                => array_get($asnDtl, 'asn_dtl_sku', ''),
                'size'               => array_get($asnDtl, 'asn_dtl_size', ''),
                'color'              => array_get($asnDtl, 'asn_dtl_color', ''),
                'pack'               => $packCtn,
                'lot'                => array_get($asnDtl, 'asn_dtl_lot', ''),
                'po'                 => array_get($asnDtl, 'asn_dtl_po', ''),
                'uom_id'             => array_get($asnDtl, 'uom_id', ''),
                'uom_code'           => array_get($asnDtl, 'uom_code', ''),
                'uom_name'           => array_get($asnDtl, 'uom_name', ''),
                'upc'                => array_get($asnDtl, 'asn_dtl_cus_upc', ''),
                'ctnr_id'            => array_get($asnDtl, 'ctnr_id', ''),
                'ctnr_num'           => array_get($asnDtl, 'ctnr_num', ''),
                'length'             => array_get($asnDtl, 'asn_dtl_length', ''),
                'width'              => array_get($asnDtl, 'asn_dtl_width', ''),
                'height'             => array_get($asnDtl, 'asn_dtl_height', ''),
                'weight'             => array_get($asnDtl, 'asn_dtl_weight', ''),
                'cube'               => array_get($asnDtl, 'asn_dtl_cube', ''),
                'volume'             => array_get($asnDtl, 'asn_dtl_volume', ''),
                'expired_dt'         => array_get($asnDtl, 'expired_dt', ''),
                'gr_dtl_des'         => array_get($asnDtl, 'asn_dtl_des', ''),
                'crs_doc'            => $asnDtl['asn_dtl_crs_doc'] ?: 0
            ];

            // Check duplicate unique fields
            $this->goodsReceiptDetailModel->refreshModel();
            $this->goodsReceiptDetailModel->create($dataGrDtl);
        }
    }

    private function gunComplete($whsId, $grHdr, $request)
    {
        // get data from HTTP
        $inputQuery = $request->getQueryParams();
        $inputPost = $request->getParsedBody();
        $agree = array_get($inputQuery, 'agree', false);
        $inNote = array_get($inputPost, 'in_note', null);
        $exNote = array_get($inputPost, 'ex_note', null);
        $actGrDt = date("Y-m-d H:i:s");
        try {
            $grHdrId = $grHdr->gr_hdr_id;

            $grDtls = $this->goodsReceiptDetailModel->getGoodsReceiptDetailByGrHdrId($grHdrId);
            $arrItemids = array_pluck($grDtls,"item_id");
            $checkItemRecei = DB::table("item")->whereIn("item_id", $arrItemids)->where("status","RG")->first();
            if(!empty($checkItemRecei)) {
                throw new \Exception('This Good Receipt have item status Receiving!');
            }
            if (!$grDtls) {
                throw new \Exception('There is no Goods Receipt Details');
            }

//            $noPutaway = DB::table('pallet')
//                ->where([
//                    'pallet.gr_hdr_id' => $grHdrId,
//                    'pallet.deleted'   => 0,
//                    'pallet.loc_id'    => null,
//                ])
//                ->whereNotNull('pallet.rfid')
//                ->count();

            $noPutaway = DB::table('cartons')
                ->join('gr_dtl', 'gr_dtl.gr_dtl_id', '=', 'cartons.gr_dtl_id')
                ->where([
                    'gr_dtl.gr_hdr_id' => $grHdrId,
                    'cartons.deleted'  => 0,
                    'cartons.loc_id'   => null,
                ])//->where('cartons.ctn_sts', '<>', 'AJ')->where('cartons.ctn_sts', '<>', 'IA')
                ->whereNotIn('cartons.ctn_sts', ['AJ', 'IA', 'CC'])
                ->count();

            if ($noPutaway) {
                return $this->response->errorBadRequest("Goods Receipt Pallets don't putaway all yet");
            }
            DB::beginTransaction();
            //create ref
            $ref = md5('GR'.time());
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $asnDtls = DB::table('asn_dtl')
                ->where('ctnr_id', $grHdr->ctnr_id)
                ->where('asn_hdr_id', $grHdr->asn_hdr_id)
                ->where('asn_dtl_sts', '<>','CC')
                ->where('deleted', 0)
                ->get();

            // If ans dtl not in gr_dtl return these asn_dtl
            $grDtlAsnDtlIds = array_pluck($grDtls->toArray(), 'asn_dtl_id');
            $asnDtls = array_filter($asnDtls, function ($asnDtl) use ($grDtlAsnDtlIds) {
                return !in_array($asnDtl['asn_dtl_id'], $grDtlAsnDtlIds);
            });

            if (count($asnDtls)) {
                if ($agree) {
                    ///create all gr dtls from $asnDtls with actual ctn ttl = 0
                    $this->createGoodsReceiptDetails($grHdr->gr_hdr_id, $asnDtls);
                } else {
                    //Error 2
                    $arr = [];
                    foreach ($asnDtls as $item) {
                        $arr[] = $item;
                    }

                    return [
                        'data'    => $arr,
                        'error'   => 2,
                        'message' => 'Missing SKUs in GR'
                    ];
                }
            }

            // WMS2-5190 - Save gr_dtl_act_qty_ttl,gr_dtl_ept_qty_ttl  when Complete Goods Receipt
            $this->goodsReceiptDetailModel->computeGrDtlQty($grHdrId);

            $this->goodsReceiptModel->updateContainerInfo($grHdrId, $inputPost);

            $res = $this->goodsReceiptModel->updateGRRecevied($grHdrId, $inNote, $exNote, $actGrDt);
            if ($res) {
                $gr_hdr_num = $grHdr->gr_hdr_num;
                // Add Event Tracking Goods Receipt
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $whsId,
                    'cus_id'    => $grHdr->cus_id,
                    'owner'     => $gr_hdr_num,
                    'evt_code'  => Status::getByKey("Event", "GR-COMPLETE"),
                    'trans_num' => $gr_hdr_num,
                    'info'      => 'GR Received - Complete GR'
                ]);
            }
            $this->asnDtlModel->updateAsnDtlsReceivedByCtnrID($grHdr->ctnr_id, $grHdr->asn_hdr_id);
            $resAsn = $this->asnHdrModel->updateAsnHdrReceived($grHdr->asn_hdr_id);
            if ($resAsn) {
                $ansHdr = $this->asnHdrModel->getAsnHdrById($grHdr->asn_hdr_id);
                $asnNum = $ansHdr->asn_hdr_num;
                // Add Event Tracking Asn received
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $whsId,
                    'cus_id'    => $grHdr->cus_id,
                    'owner'     => $asnNum,
                    'evt_code'  => Status::getByKey("Event", "ASN-COMPLETE"),
                    'trans_num' => $asnNum,
                    'info'      => sprintf(Status::getByKey("Event-info", "ASN-COMPLETE"), $asnNum)
                ]);
            }

            $this->insertInvtSmr($grHdr, $grDtls);
//            $this->insertInventory($grHdr, $grDtls);

            //update inventory
//            DB::table('invt_smr AS iv')
//                ->join(DB::raw("(
//                    SELECT item_id, lot,
//                        SUM(IF(!is_damaged, piece_remain, 0)) AS avail,
//                        SUM(IF(loc_type_code = 'XDK', piece_remain, 0)) AS xdock_qty,
//                        SUM(IF(is_damaged, piece_remain, 0)) AS dmg_qty
//                    FROM cartons
//                    WHERE gr_hdr_id = $grHdrId
//                        AND deleted = 0
//                        AND ctn_sts = 'RG'
//                    GROUP BY item_id,lot
//                ) tt"), function ($join) {
//                    $join->on('iv.item_id', '=', 'tt.item_id')
//                        ->on('iv.lot', '=', 'tt.lot');
//
//                })
//                ->where([
//                    'iv.whs_id' => Data::getCurrentWhsId()
//                ])
//                ->update([
//                    "iv.ttl"         => DB::raw("iv.ttl + tt.avail + tt.dmg_qty"),
//                    "iv.avail"       => DB::raw("iv.avail + tt.avail - tt.xdock_qty"),
//                    "iv.dmg_qty"     => DB::raw("iv.dmg_qty + tt.dmg_qty"),
//                    "iv.crs_doc_qty" => DB::raw("iv.crs_doc_qty + tt.xdock_qty"),
//                ]);
            $this->updateInventory($grHdrId);


//            $this->updateNewInventoryTable($grHdrId);

            //change status cartons, pallet, location to AC
            DB::table('cartons')
                ->where([
                    'deleted'   => 0,
                    'ctn_sts'   => 'RG',
                    'gr_hdr_id' => $grHdrId
                ])
                ->update([
                    'ctn_sts' => 'AC',
                    'gr_dt'   => strtotime($actGrDt),
                    'ref'     => $ref
                ]);

            DB::table('pallet AS p')
                ->join('location AS l', 'l.loc_id', '=', 'p.loc_id')
                ->join('cartons AS c', 'c.plt_id', '=', 'p.plt_id')
                ->where('c.gr_hdr_id', $grHdrId)
                ->where('p.plt_sts', 'RG')
                ->where('l.loc_sts_code', 'RG')
                ->groupBy('p.plt_id')
                //->whereNotNull('p.rfid')
                ->update([
                    'l.loc_sts_code' => 'AC',
                    'p.plt_sts'      => 'AC',
                    'p.gr_dt'        => strtotime($actGrDt),
                    'p.ref'            => $ref
                ]);


            $this->inventoryModel->updateInvent($grHdrId, $whsId);

            $this->correctInventoryReportByGR($grHdrId);

            DB::commit();

            //Correct Data
            $userId = Data::getCurrentUserId();
            dispatch( new CorrectDataJob($request, $grHdrId, $userId) );

            $this->dispatchEdiQueue($grHdr);
            dispatch(new AutoCreateBillable($whsId, $grHdr->cus_id, $grHdr->gr_hdr_id, $request));
            // add to queue VIET temporary disable
            //dispatch(new AutoReceivingSummary($whsId, $grHdr->cus_id, $grHdrId, $request));
            //dispatch(new AutostorageChargeCode($whsId, $grHdr->cus_id, $ref, $request));
                
            // Send data to IMS
            dispatch(new GoodsReceiptRecevingJob($grHdr->gr_hdr_id));
            
            //Update Daily Inventory and Pallet report
            dispatch(new AutoUpdateDailyInventoryAndPalletReport($whsId, $grHdr->cus_id, strtotime(date('Y-m-d'))));
            return [
                'data'    => null,
                'error'   => null,
                'message' => 'Update complete Goods Receipt successfully!'
            ];

        } catch (\PDOException $e) {

            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * [wmsComplete description]
     * @param  [type] $whsId   [description]
     * @param  [type] $grHdr   [description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    private function wmsComplete($whsId, $grHdr, $request)
    {
        // get data from HTTP
        $inputQuery = $request->getQueryParams();
        $inputPost = $request->getParsedBody();
        $agree = array_get($inputQuery, 'agree', false);
        $inNote = array_get($inputPost, 'in_note', null);
        $exNote = array_get($inputPost, 'ex_note', null);
        $actGrDt = date("Y-m-d H:i:s");

        try {
            $grHdrId = $grHdr->gr_hdr_id;

            $grDtls = $this->goodsReceiptDetailModel->getGoodsReceiptDetailByGrHdrId($grHdrId);

            if (!$grDtls) {
                throw new \Exception('There is no Goods Receipt Details');
            }

            $noPutaway = DB::table('pallet')->where([
                'gr_hdr_id' => $grHdrId,
                'deleted'   => 0,
                'loc_id'    => null,
            ])
                ->whereNotNull('rfid')
                ->count();

            if ($noPutaway) {
                return $this->response->errorBadRequest("Goods Receipt Pallets don't putaway all yet");
            }

            DB::beginTransaction();
            //create ref
            $ref = md5('GR'.time());
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $asnDtls = DB::table('asn_dtl')
                ->where('ctnr_id', $grHdr->ctnr_id)
                ->where('asn_hdr_id', $grHdr->asn_hdr_id)
                ->where('asn_dtl_sts', '<>','CC')
                ->where('deleted', 0)
                ->get();

            // If ans dtl not in gr_dtl return these asn_dtl
            $grDtlAsnDtlIds = array_pluck($grDtls->toArray(), 'asn_dtl_id');
            $asnDtls = array_filter($asnDtls, function ($asnDtl) use ($grDtlAsnDtlIds) {
                return !in_array($asnDtl['asn_dtl_id'], $grDtlAsnDtlIds);
            });

            if (count($asnDtls)) {
                if ($agree) {
                    ///create all gr dtls from $asnDtls with actual ctn ttl = 0
                    $this->createGoodsReceiptDetails($grHdr->gr_hdr_id, $asnDtls);
                } else {
                    //Error 2
                    $arr = [];
                    foreach ($asnDtls as $item) {
                        $arr[] = $item;
                    }

                    return [
                        'data'    => $arr,
                        'error'   => 2,
                        'message' => 'Missing SKUs in GR'
                    ];
                }
            }

            // WMS2-5190 - Save gr_dtl_act_qty_ttl,gr_dtl_ept_qty_ttl  when Complete Goods Receipt
            $this->goodsReceiptDetailModel->computeGrDtlQty($grHdrId);

            $this->goodsReceiptModel->updateContainerInfo($grHdrId, $inputPost);

            $res = $this->goodsReceiptModel->updateGRRecevied($grHdrId, $inNote, $exNote, $actGrDt);
            if ($res) {
                $gr_hdr_num = $grHdr->gr_hdr_num;
                // Add Event Tracking Goods Receipt
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $whsId,
                    'cus_id'    => $grHdr->cus_id,
                    'owner'     => $gr_hdr_num,
                    'evt_code'  => Status::getByKey("Event", "GR-COMPLETE"),
                    'trans_num' => $gr_hdr_num,
                    'info'      => 'GR Received - Complete GR'
                ]);
            }
            $this->asnDtlModel->updateAsnDtlsReceivedByCtnrID($grHdr->ctnr_id, $grHdr->asn_hdr_id);
            $resAsn = $this->asnHdrModel->updateAsnHdrReceived($grHdr->asn_hdr_id);
            if ($resAsn) {
                $ansHdr = $this->asnHdrModel->getAsnHdrById($grHdr->asn_hdr_id);
                $asnNum = $ansHdr->asn_hdr_num;
                // Add Event Tracking Asn received
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $whsId,
                    'cus_id'    => $grHdr->cus_id,
                    'owner'     => $asnNum,
                    'evt_code'  => Status::getByKey("Event", "ASN-COMPLETE"),
                    'trans_num' => $asnNum,
                    'info'      => sprintf(Status::getByKey("Event-info", "ASN-COMPLETE"), $asnNum)
                ]);
            }

            $this->insertInvtSmr($grHdr, $grDtls);
//            $this->insertInventory($grHdr, $grDtls);

            //update inventory
            $this->updateInventory($grHdrId);

//            $this->updateNewInventoryTable($grHdrId);


            //change status cartons, pallet, location to AC
            $this->updateCartonStatus($grHdrId, $actGrDt, $ref);

            //Update status pallet
            $this->updatePalletStatus($grHdrId, $actGrDt, $ref);

//            $this->updateNewInventoryTable($grHdrId);

            $this->inventoryModel->updateInvent($grHdrId, $whsId);

            DB::commit();

            //Correct Data
            $userId = Data::getCurrentUserId();
            dispatch( new CorrectDataJob($request, $grHdrId, $userId) );

            $this->dispatchEdiQueue($grHdr);
            //Update Daily Inventory and Pallet report
            dispatch(new AutoUpdateDailyInventoryAndPalletReport($whsId, $grHdr->cus_id, strtotime(date('Y-m-d'))));
        } catch (\PDOException $e) {

            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return [
            'data'    => null,
            'error'   => null,
            'message' => 'Update complete Goods Receipt successfully!'
        ];
    }
    /**
     * [updatePalletStatus description]
     * @param  [type] $grHdrId [description]
     * @param  [type] $actGrDt [description]
     * @param  [type] $ref     [description]
     * @return [type]          [description]
     */
    private function updatePalletStatus($grHdrId, $actGrDt, $ref)
    {
        return DB::table('pallet AS p')
                ->join('location AS l', 'l.loc_id', '=', 'p.loc_id')
                ->where('p.gr_hdr_id', $grHdrId)
                ->where('p.plt_sts', 'RG')
                ->where('l.loc_sts_code', 'RG')
                ->update([
                    'l.loc_sts_code' => 'AC',
                    'p.plt_sts'      => 'AC',
                    'p.gr_dt'        => strtotime($actGrDt),
                    'ref'            => $ref
                ]);
    }
    /**
     * [updateCartonStatus description]
     * @param  [type] $grHdrId [description]
     * @param  [type] $actGrDt [description]
     * @param  [type] $ref     [description]
     * @return [type]          [description]
     */
    private function updateCartonStatus($grHdrId, $actGrDt, $ref)
    {
        return DB::table('cartons')->where([
                'deleted'   => 0,
                'ctn_sts'   => 'RG',
                'gr_hdr_id' => $grHdrId
            ])->update([
                'ctn_sts' => 'AC',
                'gr_dt'   => strtotime($actGrDt),
                'ref'     => $ref
            ]);
    }
    /**
     * [updateInventory description]
     * @param  [type] $grHdrId [description]
     * @return [type]          [description]
     */
    private function updateInventory($grHdrId)
    {
        return DB::table('invt_smr AS iv')
        // SUM(IF(!is_damaged, piece_remain, 0)) AS avail,
            ->join(DB::raw("(
                SELECT item_id,
                    whs_id,
                    SUM(piece_remain) AS avail,
                    SUM(IF(loc_type_code = 'XDK', piece_remain, 0)) AS xdock_qty,
                    SUM(IF(is_damaged, piece_remain, 0)) AS dmg_qty
                FROM cartons
                WHERE gr_hdr_id = $grHdrId
                    AND deleted = 0
                    AND ctn_sts = 'RG'
                GROUP BY item_id, whs_id
            ) tt"), function ($join) {
                $join->on('iv.item_id', '=', 'tt.item_id')
                    // ->on('iv.lot', '=', 'tt.lot');
                    ->on('iv.whs_id', '=', 'tt.whs_id');
            })
            ->where([
                'iv.whs_id' => Data::getCurrentWhsId()
            ])
            ->update([
                // "iv.ttl"         => DB::raw("iv.ttl + tt.avail + tt.dmg_qty"),
                "iv.ttl"         => DB::raw("iv.ttl + tt.avail"),
                "iv.avail"       => DB::raw("iv.avail + tt.avail - tt.xdock_qty"),
                "iv.dmg_qty"     => DB::raw("iv.dmg_qty + tt.dmg_qty"),
                "iv.crs_doc_qty" => DB::raw("iv.crs_doc_qty + tt.xdock_qty"),
            ]);
    }

    private function updateNewInventoryTable($grHdrId)
    {
        return DB::table('inventory AS iv')
            // SUM(IF(!is_damaged, piece_remain, 0)) AS avail,
            ->join(DB::raw("(
                SELECT item_id,
                    whs_id,
                    SUM(piece_remain) AS avail,
                    SUM(IF(loc_type_code = 'XDK', piece_remain, 0)) AS xdock_qty,
                    SUM(IF(is_damaged, piece_remain, 0)) AS dmg_qty
                FROM cartons
                WHERE gr_hdr_id = $grHdrId
                    AND deleted = 0
                    AND ctn_sts = 'AC'
                GROUP BY item_id, whs_id
            ) tt"), function ($join) {
                $join->on('iv.item_id', '=', 'tt.item_id')
                    // ->on('iv.lot', '=', 'tt.lot');
                    ->on('iv.whs_id', '=', 'tt.whs_id');
            })
            ->where([
                'iv.whs_id' => Data::getCurrentWhsId(),
                'iv.type'   => 'I'
            ])
            ->update([
                // "iv.ttl"         => DB::raw("iv.ttl + tt.avail + tt.dmg_qty"),
//                "iv.total_qty"      => DB::raw("iv.total_qty + tt.avail"),
                "iv.in_hand_qty"    => DB::raw("iv.in_hand_qty + tt.avail"),
            ]);
    }

    private function _updatePalletAndCartonStatusAndInventory($grHdrId, $grHdr, $request)
    {
        $grDtls = $this->goodsReceiptDetailModel->getGoodsReceiptDetailByGrHdrId($grHdrId);
        //insert inventory
        $this->insertInvtSmr($grHdr, $grDtls);
//        $this->insertInventory($grHdr, $grDtls);
        //insert inventory for damage carton
        // $this->insertInvtSmrDamageCtn($grHdr, $grDtls);

        //update inventory
        $this->updateInventory($grHdrId);

//        $this->updateNewInventoryTable($grHdrId);


        //create ref
        $ref = md5('GR'.time());
        $inputPost = $request->getParsedBody();
        $actGrDt = array_get($inputPost, 'gr_hdr_act_dt', date("Y-m-d H:i"));

        //change status cartons, pallet, location to AC
        $this->updateCartonStatus($grHdrId, $actGrDt, $ref);

        //Update status pallet
        $this->updatePalletStatus($grHdrId, $actGrDt, $ref);

        $this->updateNewInventoryTable($grHdrId);

    }

    private function _forceGrWithVtlCtnNotScannedGW($vtlCtns)
    {
        $userId = Data::getCurrentUserId();
        $ctnRfids = array_pluck($vtlCtns, 'ctn_rfid');

        $cartonExists = DB::table('cartons')
            ->whereIn('rfid', $ctnRfids)
            ->where('deleted', 0)
            ->get();

        $rfidExists = array_pluck($cartonExists, 'rfid');
        $ctnRfidsFocus = array_diff($ctnRfids, $rfidExists);

        $dataOriginal = [
            'created_at' => time(),
            'created_by' => $userId,
            'updated_at' => time(),
            'updated_by' => $userId,
            'deleted' => 0,
            'deleted_at' => 915148800,
        ];

        $dataInsert = [];
        $asnDtls = [];
        $maxNum = null;
        $ctnNumMax = $this->generateCtnNum();
        foreach ($vtlCtns as $vtlCtn) {
            if (in_array($vtlCtn['ctn_rfid'], $ctnRfidsFocus)) {
                $itemId   = $vtlCtn['item_id'];
                $asnDtlId = $vtlCtn['asn_dtl_id'];
                $asnDtl = array_get($asnDtls, $asnDtlId);
                if (!$asnDtl) {
                    $asnDtl = DB::table('asn_dtl')->where('asn_dtl_id', $asnDtlId)
                        ->where('deleted', 0)
                        ->first();
                    if (!count($asnDtl)) {
                        continue;
                    }
                    $asnDtls[$asnDtlId] = $asnDtl;
                }

                $dataOriginal['asn_dtl_id']    = $asnDtlId;
                $dataOriginal['whs_id']        = $vtlCtn['whs_id'];
                $dataOriginal['cus_id']        = $vtlCtn['cus_id'];
                $dataOriginal['ctn_num']       = ++$ctnNumMax;
                $dataOriginal['rfid']          = $vtlCtn['ctn_rfid'];
                $dataOriginal['ctn_sts']       = 'AJ';
                $dataOriginal['ctnr_id']       = $asnDtl['ctnr_id'];
                $dataOriginal['ctnr_num']      = $asnDtl['ctnr_num'];
                $dataOriginal['ctn_uom_id']    = $asnDtl['uom_id'];
                $dataOriginal['uom_code']      = $asnDtl['uom_code'];
                $dataOriginal['uom_name']      = $asnDtl['uom_name'];
                $dataOriginal['piece_remain']  = $asnDtl['asn_dtl_pack'];
                $dataOriginal['piece_ttl']     = $asnDtl['asn_dtl_pack'];
                $dataOriginal['item_id']       = $itemId;
                $dataOriginal['sku']           = $asnDtl['asn_dtl_sku'];
                $dataOriginal['size']          = $asnDtl['asn_dtl_size'];
                $dataOriginal['color']         = $asnDtl['asn_dtl_color'];
                $dataOriginal['lot']           = $vtlCtn['is_damaged'] == 1 ? "DAMAGE" : $asnDtl['asn_dtl_lot'];
                $dataOriginal['ctn_pack_size'] = $asnDtl['asn_dtl_pack'];
                $dataOriginal['length']        = $asnDtl['asn_dtl_length'];
                $dataOriginal['width']         = $asnDtl['asn_dtl_width'];
                $dataOriginal['height']        = $asnDtl['asn_dtl_height'];
                $dataOriginal['weight']        = $asnDtl['asn_dtl_weight'];
                $dataOriginal['volume']        = $asnDtl['asn_dtl_volume'];
                $dataOriginal['des']           = $asnDtl['asn_dtl_des'];
                $dataOriginal['upc']           = $asnDtl['asn_dtl_cus_upc'];
                $dataOriginal['cube']          = $asnDtl['asn_dtl_cube'];
                $dataOriginal['volume']        = $asnDtl['asn_dtl_volume'];
                $dataOriginal['is_damaged']    = $vtlCtn['is_damaged'];

                $dataInsert[] = $dataOriginal;
            }
        }

        DB::table('cartons')->insert($dataInsert);
        DB::table('cartons')
            ->whereIn('rfid', $ctnRfids)
            ->where('ctn_sts', '!=', 'AJ')
            ->update([
                'ctn_sts'    => 'AJ',
                'updated_at' => time(),
                'updated_by' => $userId,
            ]);
    }

    public function generateCtnNum()
    {
        $prefix = 'CTN-' . date('ym') . '-FGR'; //
        $maxNum = DB::table('cartons')->where('ctn_num', 'LIKE', "%" . $prefix . '-%')
            ->max('ctn_num');

        if (empty($maxNum)) {
            return $prefix . '-0000001';
        }

        return ++$maxNum;
    }

    public function correctGoodReceiptAndPalletInfo($grHdr)
    {
        DB::statement("
            UPDATE gr_dtl d
            JOIN
            (SELECT
                COUNT(c.ctn_id) AS ctn_ttl,
                sum(c.piece_remain) AS qty_ttl,
                COUNT(DISTINCT(p.plt_id)) AS plt_ttl,
                SUM(IF(c.is_damaged = 1, 1, 0)) AS ctn_dmg_ttl,
                d.gr_dtl_plt_ttl,
                d.gr_dtl_id
            FROM
                gr_dtl d
            JOIN gr_hdr h ON h.gr_hdr_id = d.gr_hdr_id
            JOIN cartons c ON d.gr_dtl_id = c.gr_dtl_id
            JOIN pallet p ON p.plt_id = c.plt_id
            WHERE
                h.whs_id = ?
                AND d.gr_hdr_id = ?
                AND p.deleted = 0
                AND c.deleted = 0
                AND d.deleted = 0
            GROUP BY
                d.gr_dtl_id
            ) AS t ON d.gr_dtl_id = t.gr_dtl_id
            SET
                d.gr_dtl_act_ctn_ttl = t.ctn_ttl,
                d.gr_dtl_plt_ttl = t.plt_ttl,
                d.gr_dtl_dmg_ttl = t.ctn_dmg_ttl,
                d.gr_dtl_act_qty_ttl = t.qty_ttl;
        ", [$grHdr->whs_id, $grHdr->gr_hdr_id]);

        DB::statement("
            UPDATE gr_dtl d
            SET d.gr_dtl_disc_qty = d.gr_dtl_ept_ctn_ttl/(-1)*(-1) - d.gr_dtl_act_ctn_ttl,
                d.gr_dtl_disc = d.gr_dtl_ept_ctn_ttl/(-1)*(-1) - d.gr_dtl_act_ctn_ttl
            WHERE d.gr_hdr_id = ?;
        ", [$grHdr->gr_hdr_id]);

        DB::statement("
            UPDATE pallet
            SET pallet.ctn_ttl =
            IFNULL((
             SELECT count(*)
             FROM cartons
             WHERE cartons.whs_id = pallet.whs_id
             AND cartons.plt_id = pallet.plt_id
             AND cartons.ctn_sts IN ( 'AC', 'LK', 'AL', 'RG')
             AND cartons.deleted = 0
            ), 0)
            WHERE pallet.whs_id = ? AND pallet.gr_hdr_id= ?;
        ", [$grHdr->whs_id, $grHdr->gr_hdr_id]);

        DB::statement("
            UPDATE pallet
            SET pallet.dmg_ttl =
            IFNULL((
             SELECT count(*)
             FROM cartons
             WHERE cartons.whs_id = pallet.whs_id
             AND cartons.plt_id = pallet.plt_id
             AND cartons.ctn_sts IN ( 'AC', 'LK', 'AL', 'RG')
             AND cartons.deleted = 0
             AND cartons.is_damaged = 1
            ), 0)
            WHERE pallet.whs_id = ? AND pallet.gr_hdr_id = ?;
        ", [$grHdr->whs_id, $grHdr->gr_hdr_id]);
    }

    public function correctPalSugLocInfo($grHdr)
    {
        $userId = Data::getCurrentUserId();
        $time = time();

        DB::statement("
            INSERT INTO pal_sug_loc (
                id,
                plt_id,
                loc_id,
                ctn_ttl,
                qty_ttl,
                item_id,
                sku,
                size,
                color,
                lot,
                putter,
                gr_hdr_id,
                gr_dtl_id,
                gr_hdr_num,
                whs_id,
                created_at,
                updated_by,
                updated_at,
                created_by,
                deleted_at,
                deleted,
                put_sts,
                act_loc_id,
                act_loc_code
            )
            SELECT
                NULL as pal_sug_loc_id,
                p.plt_id as plt_id,
                p.loc_id as loc_id,
                COUNT(DISTINCT(c.ctn_id)) AS ctn_ttl,
                COUNT(DISTINCT(c.ctn_id)) * c.ctn_pack_size AS qty_ttl,
                c.item_id as item_id,
                c.sku as sku,
                c.size as size,
                c.color as color,
                c.lot as lot,
                NULL as putter,
                c.gr_hdr_id as gr_hdr_id,
                c.gr_dtl_id as gr_dtl_id,
                NULL as gr_hdr_num,
                p.whs_id as whs_id,
                ? as created_at,
                ? as updated_by,
                ? as updated_at,
                ? as created_by,
                ? as deleted_at,
                0 as deleted,
                'CO' as put_sts,
                p.loc_id as act_loc_id,
                p.loc_code as act_loc_code
            FROM
                pallet p
                JOIN cartons c ON c.plt_id = p.plt_id
            WHERE
                p.whs_id = ?
                AND p.cus_id = ?
                AND p.gr_hdr_id = ?
                AND p.deleted = 0
                AND c.deleted = 0
            GROUP BY
                c.item_id,
                c.lot
            ON DUPLICATE KEY UPDATE
                ctn_ttl = VALUES(ctn_ttl),
                qty_ttl = VALUES(qty_ttl),
                act_loc_id = VALUES(act_loc_id),
                act_loc_code = VALUES(act_loc_code)
        ", [$time, $userId, $time, $userId, 915148800, $grHdr->whs_id, $grHdr->cus_id, $grHdr->gr_hdr_id]);
    }

    public function checkValidDateCompleteGr($goodReceipt, $actGrDt)
    {
        $completeDate = new Carbon($actGrDt);
        // $grDate = $goodReceipt->created_at;
        $currentDate = new Carbon();
        if ($completeDate->month === $currentDate->month){
            return true;
        }

        if (($currentDate->month - $completeDate->month) == 1 && $currentDate->day < 8) {
            return true;
        }

        // if ($completeDate->diffInDays($currentDate) <= 7){
        //     return true;
        // }

        return false;
    }

    public function checkVaildItemReceived($grHdr_id)
    {
        $itemIds = DB::table('gr_dtl')
            ->where('gr_hdr_id', $grHdr_id)
            ->pluck('item_id');

        if(!empty($itemIds)) {

            $itemStatusArr = DB::table('item')
                ->whereIn('item_id', $itemIds)
                ->where('status', 'RG')
                ->get();

            if(count($itemStatusArr) > 0) {
                return false;
            }
        }

        return true;
    }

    public function correctInventoryReportByGR($grHdrId)
        {
            $grHdr = $this->goodsReceiptModel->getGoodsReceiptById($grHdrId);
            $itemIds = $this->goodsReceiptDetailModel->getGoodsReceiptDetailByGrHdrId($grHdrId)->pluck('item_id');
            foreach($itemIds as $itemId) {
                $ivtr = $this->inventoriesModel->getFirstWhere([
                    "whs_id"    => $grHdr['whs_id'],
                    "cus_id"    => $grHdr['cus_id'],
                    "item_id"   => $itemId,
                    "type"      => "I",
                ]);
                if(!empty($ivtr)) {
                    $ivtrReport = $this->reportInventoryModel->getFirstWhere([
                        "whs_id"    => $ivtr['whs_id'],
                        "cus_id"    => $ivtr['cus_id'],
                        "item_id"   => $ivtr['item_id'],
                    ]);

                    $data = [
                        "whs_id"        => $ivtr['whs_id'],
                        "cus_id"        => $ivtr['cus_id'],
                        "item_id"       => $ivtr['item_id'],
                        "whs_code"      => $ivtr['whs_code'],
                        "cus_code"      => $ivtr['cus_code'],
                        "type"          => $ivtr['type'],
                        "upc"           => $ivtr['upc'],
                        "des"           => $ivtr['des'],
                        "sku"           => $ivtr['sku'],
                        "size"          => $ivtr['size'],
                        "color"         => $ivtr['color'],
                        "pack"          => $ivtr['pack'],
                        "uom"           => $ivtr['uom'],
                        "in_hand_qty"   => $ivtr['in_hand_qty'],
                        "in_pick_qty"   => $ivtr['in_pick_qty'],
                    ];

                    if(empty($ivtrReport)) {
                        $data['created_at'] = time();
                        $data['updated_at'] = time();
                        DB::table($this->reportInventoryModel->getTable())->insert($data);
                    }else{
                        $ivtrReport->update($data);
                    }
                }
            }
        }
}