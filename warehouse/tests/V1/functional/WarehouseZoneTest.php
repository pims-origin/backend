<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\WarehouseZone;
use App\WarehouseZoneType;
use App\Warehouse;

class WarehouseZoneTest extends TestCase
{
    use DatabaseTransactions;

    protected $warehouseId;

    protected $zoneParams = [
        'zone_name'        => 'Zone',
        'zone_code'        => 'AB',
        'zone_min_count'   => 5,
        'zone_max_count'   => 20,
        'zone_description' => 'Zone Test',
        'zone_num_of_loc'  => 10,
    ];

    public function setUp()
    {
        parent::setUp();

        // create data into DB
        $warehouse = factory(Warehouse::class)->create();

        $this->warehouseId = $warehouse->whs_id;
        $this->zoneParams['zone_whs_id'] = $this->warehouseId;
    }

    private function getEndPoint()
    {
        return "/v1/warehouses/{$this->warehouseId}/zones";
    }

    public function testStore_NoParam_Fail()
    {
        $endPoint = $this->getEndPoint();
        $response = $this->call('POST', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_HasParam_Ok()
    {
        // make data
        $zoneType = factory(WarehouseZoneType::class)->create();
        $endPoint = $this->getEndPoint();

        $this->zoneParams['zone_type_id'] = $zoneType->zone_type_id;

        $response = $this->call('POST', $endPoint, $this->zoneParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
        $responseZone = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('zone_name', $responseZone['data']);
        $this->assertArrayHasKey('zone_code', $responseZone['data']);
        $this->assertArrayHasKey('zone_whs_id', $responseZone['data']);
        $this->assertArrayHasKey('zone_type_id', $responseZone['data']);
        $this->assertArrayHasKey('zone_min_count', $responseZone['data']);
        $this->assertArrayHasKey('zone_max_count', $responseZone['data']);
        $this->assertArrayHasKey('zone_description', $responseZone['data']);
        $this->assertArrayHasKey('zone_num_of_loc', $responseZone['data']);
    }

    public function testStore_DuplicateZoneCode_Fail()
    {
        // make data
        $zone_code = factory(WarehouseZone::class)->create()->zone_code;

        $this->zoneParams['zone_code'] = $zone_code;

        $this->call('POST', $this->getEndPoint(), $this->zoneParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_ZoneTypeNotExisted_Fail()
    {
        // make data
        $endPoint = $this->getEndPoint();

        $this->zoneParams['zone_type_id'] = 9999999999;

        $this->call('POST', $endPoint, $this->zoneParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testShow_Ok()
    {
        $zone = factory(App\WarehouseZone::class)->create(['zone_whs_id' => $this->warehouseId]);
        $endPoint = $this->getEndPoint() . '/' . $zone->zone_id;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testShow_QualifierNotExisted_Fails()
    {
        $endPoint = $this->getEndPoint() . '/999999999';
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testUpdate_Ok()
    {
        // make data
        $zone = factory(App\WarehouseZone::class)->create(['zone_whs_id' => $this->warehouseId]);
        $endPoint = $this->getEndPoint() . '/' . $zone->zone_id;

        $zoneType = factory(WarehouseZoneType::class)->create();

        $this->zoneParams['zone_id'] = $zone->zone_id;
        $this->zoneParams['zone_type_id'] = $zoneType->zone_type_id;

        $response = $this->call('PUT', $endPoint, $this->zoneParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseZone = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('zone_name', $responseZone['data']);
        $this->assertArrayHasKey('zone_code', $responseZone['data']);
        $this->assertArrayHasKey('zone_whs_id', $responseZone['data']);
        $this->assertArrayHasKey('zone_type_id', $responseZone['data']);
        $this->assertArrayHasKey('zone_min_count', $responseZone['data']);
        $this->assertArrayHasKey('zone_max_count', $responseZone['data']);
        $this->assertArrayHasKey('zone_description', $responseZone['data']);
        $this->assertArrayHasKey('zone_num_of_loc', $responseZone['data']);
    }

    public function testUpdate_ZoneTypeNotExisted_Fail()
    {
        // make data
        $zone = factory(App\WarehouseZone::class)->create(['zone_whs_id' => $this->warehouseId]);
        $endPoint = $this->getEndPoint() . '/' . $zone->zone_id;

        $this->zoneParams['zone_id'] = $zone->zone_id;
        $this->zoneParams['zone_type_id'] = 9999999999;

        $this->call('PUT', $endPoint, $this->zoneParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_ZoneCodeAlreadyExists_Fail()
    {
        // make data
        $zendCode = factory(App\WarehouseZone::class)->create(['zone_whs_id' => $this->warehouseId])->zone_code;
        $zoneId = factory(App\WarehouseZone::class)->create(['zone_whs_id' => $this->warehouseId])->zone_id;
        $zoneType = factory(WarehouseZoneType::class)->create();

        $this->zoneParams['zone_code'] = $zendCode;
        $this->zoneParams['zone_type_id'] = $zoneType->zone_type_id;

        $this->call('PUT', $this->getEndPoint() . '/' . $zoneId, $this->zoneParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDestroy_SingleItem_Ok()
    {
        $warehouseZone = factory(App\WarehouseZone::class)->create(['zone_whs_id' => $this->warehouseId]);
        $endPoint = $this->getEndPoint() . '/' . $warehouseZone->zone_id;

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDestroy_ZoneIdNotExisted_Fail()
    {
        $endPoint = $this->getEndPoint() . '/999999';

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $warehouses = factory(App\WarehouseZone::class, 3)->create(['zone_whs_id' => $this->warehouseId]);
        $params = [];
        foreach ($warehouses as $key => $warehouse) {
            $params['zone_id'][$key] = $warehouse->zone_id;
        }
        $this->call('DELETE', $this->getEndPoint(), $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDeleteMass_NoParams_Fail()
    {
        $this->call('DELETE', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testSearch_NoParams_Fail()
    {
        $endPoint = $this->getEndPoint();
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_Ok()
    {
        $warehouseZone = factory(App\WarehouseZone::class)->create(['zone_whs_id' => $this->warehouseId]);

        $endPoint = $this->getEndPoint() . '?zone_code=' . $warehouseZone->zone_code
            . '&zone_name=' . $warehouseZone->zone_name
            . '&zone_description=' . $warehouseZone->zone_description
            . '&zone_id=' . $warehouseZone->zone_id
            . '&zone_type_id=' . $warehouseZone->zone_type_id;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }
}
