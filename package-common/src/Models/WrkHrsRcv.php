<?php

namespace Seldat\Wms2\Models;

class WrkHrsRcv extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wrk_hrs_rcv';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'gr_hdr_num',
        'dt',
        'type',
        'amount',
        'cat',
        'inv_sts'
    ];

    
    public function goodsReceipt()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceipt', 'gr_hdr_num', 'gr_hdr_num');
    }
    
    public function invoiceHeader()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceHeader', 'inv_id', 'inv_id');
    }
}


