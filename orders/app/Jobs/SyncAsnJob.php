<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\Item;

class SyncAsnJob extends Job
{
    protected $odrHdr;
    protected $whsTo;
    protected $header;
    protected $client;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($odrHdr, $whsTo, $request)
    {
        $this->odrHdr = $odrHdr;
        $this->whsTo  = $whsTo;
        $token = $request->getHeader('Authorization');
        /*$tokenSwitch = app(UserSwitch::class)->getToken();
        if(!empty($tokenSwitch)){
            $token = $tokenSwitch;
        }*/
        $this->header = [
            'Authorization' => $token,
            'content-type'  => 'application/json'
        ];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('[Sync ASN] '. date('Y-m-d H:i:s') . 'Header: ' . json_encode($this->header));
        Log::info('[Sync ASN] '. date('Y-m-d H:i:s') . 'Warehouse To: ' . $this->whsTo);
        Log::info('[Sync ASN] '. date('Y-m-d H:i:s') . 'Order: ' . json_encode($this->odrHdr));

        try {
            \DB::setFetchMode(\PDO::FETCH_ASSOC);
            // get or create container for transfer: WAREHOUSE_{whs_to_id}
            $ctnrNum = $this->odrHdr->odr_num;
            if (empty($container = Container::where('ctnr_num', $ctnrNum)->first())) {
                $container = Container::create([
                    'ctnr_num' => $ctnrNum,
                    'created_at' => time(),
                    'created_by' => 1,
                    'updated_at' => time(),
                    'updated_by' => 1,
                    'deleted_at' => 915148800,
                    'deleted'    => 0
                ]);
            }

            $params = [
                'cus_id'           => $this->odrHdr->cus_id,
                'asn_hdr_num'      => null,
                'asn_ref'          => $this->odrHdr->odr_num,
                'ctnr_num'         => $container['ctnr_num'],
                'ctnr_id'          => $container['ctnr_num'],
                'ctnr_id_preview'  => 0,
                'asn_hdr_itm_ttl'  => null,
                'exp_date'         => date('m/d/Y'),
                'measurement_code' => 'IN',
                'whs_id'           => $this->whsTo,
                'asn_sts_name'     => 'New',
                'asn_sts'          => 'NW',
                'asn_type'         => 'WHS',
                'ctnr_sts'         => 0,
                'asn_dtl_ept_dt'   => date('m/d/Y', strtotime("+7 day", time()))
            ];

            if (empty($this->odrHdr->details)) {
                throw new \Exception("Order details are not exists.");
            }

            foreach ($this->odrHdr->details as $odrDtl) {
                $item = Item::where('item_id', $odrDtl['item_id'])->first();
                if (empty($item)) {
                    throw new \Exception("Item (item_id: {$odrDtl['item_id']}) is not exists.");
                }

                $params['details'][] = [
                    "dtl_itm_id"       => $odrDtl['item_id'],
                    "asn_dtl_sts"      => null,
                    "asn_dtl_sts_code" => null,
                    "gr_hdr_num"       => null,
                    "asn_dtl_id"       => null,
                    "asn_dtl_cus_upc"  => $odrDtl['cus_upc'],
                    "dtl_sku"          => $odrDtl['sku'],
                    "dtl_size"         => $odrDtl['size'],
                    "dtl_color"        => $odrDtl['color'],
                    "dtl_des"          => $odrDtl['des'],
                    "dtl_uom_id"       => $odrDtl['uom_id'],
                    "dtl_uom_name"     => $odrDtl['uom_code'],
                    "dtl_pack"         => $odrDtl['pack'],
                    "dtl_ctn_ttl"      => (int)$odrDtl['piece_qty'],
                    "dtl_length"       => $item['length'] ?? 1,
                    "dtl_width"        => $item['width'] ?? 1,
                    "dtl_height"       => $item['height'] ?? 1,
                    "dtl_weight"       => $item['weight'] ?? 1,
                    "dtl_crs_doc"      => 0,
                    "dtl_po"           => 'NA',
                    "dtl_po_date"      => null,
                    "created_from"     => null,
                    "tmp_asn_dtl_sts_code"=> null,
                    "flag_dis_we"      => true,
                    "flag_dis_wi"      => true,
                    "flag_dis_h"       => true,
                    "flag_dis_l"       => true,
                    "expired_dt"       => null,
                ];
            }
            Log::info(json_encode($params));
            $this->client = new Client();
            $this->client->request('POST',
                env('API_GOODS_RECEIPT') . 'asns?queue-whs=' . $this->whsTo,
                [
                    'headers' => $this->header,
                    'body'    => \GuzzleHttp\json_encode($params)
                ]
            );

            Log::info('[Job Sync ASN] sync asn successfully.');
        } catch (\Exception $e) {
            Log::error('[Job Sync ASN] '.$e->getMessage());
        }
    }
}
