<?php

namespace App\Api\V1\Transformers;

use App\InvoiceHeader;
use App\VasHdr;
use League\Fractal\TransformerAbstract;

class InvoiceShowTransform extends TransformerAbstract
{

    /**
     * ps:Show WorkOrder
     *
     * @param VasHdr $workOrderHdr
     *
     * @return array
     */
    public function transform(InvoiceHeader $invoiceHeader)
    {
        return [
            'inv_id'         => $invoiceHeader->inv_id,
            'inv_num'        => $invoiceHeader->inv_num,
            'cus_id'         => $invoiceHeader->cus_id,
            'cus_name'       => $invoiceHeader->cus_name,
            'due_dt'         => $invoiceHeader->due_dt_format,
            'discount'       => $invoiceHeader->discount,
            'subtotal'       => $invoiceHeader->subtotal,
            'tax'            => $invoiceHeader->tax,
            'total'          => $invoiceHeader->total,
            'note'           => $invoiceHeader->note,
            'paid_dt'        => $invoiceHeader->paid_dt_format,
            'paid_amount'    => $invoiceHeader->paid_amount,
            'items'          => $this->transformInvoiceDetail($invoiceHeader->detail),
            'customer'       => [],
            'contact'        => [],
            'billing_to'     => $invoiceHeader->bill_to_add1 ?? $invoiceHeader->bill_to_add2,
            'payment'        => [],
            'document'       => [],
            'event_tracking' => [],
            'inv_sts'        => $invoiceHeader->inv_sts,
            'currency'       => $invoiceHeader->currency
        ];
    }

    private function transformInvoiceDetail($invoiceDetails)
    {
        return $invoiceDetails->transform(function ($item) {
            return [
                'inv_dtl_id'   => $item->inv_dtl_id,
                'inv_id'       => $item->inv_id,
                'sac'          => $item->sac,
                'sac_name'     => $item->sac_name,
                'ref_num'      => $item->ref_num,
                'description'  => $item->description,
                'uom'          => $item->uom,
                'unit_uom'     => $item->uom,
                'uom_name'     => $item->uom_name,
                'unit_price'   => $item->unit_price,
                'qty'          => $item->qty,
                'amount'       => $item->amount,
                'discount'     => $item->discount,
                'currency'     => $item->currency,
                'inv_sts'      => $item->inv_sts,
                'inv_sts_name' => $item->inv_sts_name,
                'ba_id'        => $item->ba_id,
                'ba_dtl_id'    => $item->ba_dtl_id,
                'item'         => $item->item,
                'is_charge'    => $item->is_charge
            ];
        });
    }
}
