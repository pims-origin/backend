<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DailyPalletRptCorrect extends AbstractCommand
{
    protected $signature = 'daily-pallet-rpt-correct';
    protected $description = 'Location Histogram successfully.';
    protected $date_correct = '';

    public function __construct()
    {
        parent::__construct();
        $this->date_correct = env('PLT_RPT_DATE_CORRECT') ? env('PLT_RPT_DATE_CORRECT') : date('Y-m-d');
    }

    public function handle()
    {
        try {
            $this->logSuccess('start-daily-pallet-rpt-correct');

            // Empty Loc His Data
            DB::table('daily_plt_rpt')
                ->where(DB::raw("DATE(FROM_UNIXTIME(inv_dt))"), '>=', $this->date_correct)
                ->delete();

            $customerWhss = DB::table('customer_warehouse AS cw')
                ->select('cw.cus_id', 'cw.whs_id')
                ->join('customer as c', 'c.cus_id', '=', 'cw.cus_id')
                ->where("c.deleted", 0)
                ->where("cw.deleted", 0)
                ->get();

            $now = date('Y-m-d');
            $start_date = $this->date_correct;

            if ($customerWhss) {
                while ($start_date <= $now) {

                    $today = strtotime($start_date);
                    $yesterday  = strtotime(date('Y-m-d', strtotime($start_date . " -1 days")));

                    foreach ($customerWhss as $cusWhs) {
                        $cusId = $cusWhs['cus_id'];
                        $whsId = $cusWhs['whs_id'];
                        DB::statement("CALL insert_daily_plt_rpt({$whsId}, {$cusId}, {$today})");
                        DB::statement("CALL update_daily_plt_rpt({$whsId}, {$cusId}, {$yesterday})");
                    }

                    $start_date = date('Y-m-d', strtotime($start_date . " +1 days"));
                }
            }

            $this->logSuccess('daily-pallet-rpt-correct');

        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }

    }
}
