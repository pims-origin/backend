<?php

namespace App\Api\V4\Outbound\Order\AssignPiecesToOrderWithRfid\Wavepick\UpdateWithRfid\Models;


use Seldat\Wms2\Models\EventTracking;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * EventTrackingModel constructor.
     */
    public function __construct()
    {
        $this->model = new EventTracking();
    }


    /**
     * @param $params
     *
     * @return mixed
     */
    public function eventTracking($params)
    {
        // event tracking asn
        $this->refreshModel();

        return $this->create([
            'whs_id' => $params['whs_id'],
            'cus_id' => $params['cus_id'],
            'owner' => $params['owner'],
            'evt_code' => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info' => $params['info'],
        ]);
    }
}
