<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 11:08 AM
 */

namespace App\Api\V1\Models;

use App\MenuFavorite;

class MenuFavoriteModel extends AbstractModel
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @param MenuFavorite $model
     */
    public function __construct(MenuFavorite $model = null)
    {
        $this->model = ($model) ?: new MenuFavorite();
    }

    public function deleteUserMenu($userId, $menuId)
    {
        $model = $this->model->where([
            'user_id' => $userId,
            'menu_id' => $menuId,
        ]);

        // Update display_order first
        $newModel = $model;
        $dataDelete = $newModel->getQuery()->get();
        if (!empty($dataDelete[0]) && !empty($dataDelete[0]->display_order)) {
            $display_order = $dataDelete[0]->display_order;

            $this->model
                ->where('user_id', $userId)
                ->where('display_order', '>=', $display_order)
                ->decrement('display_order');
        }
        if ($model->delete()) {
            return true;
        }

        return false;
    }

    /**
     * Delete Menu Favorite by Ids
     *
     * @param $menuIds
     *
     * @return int
     * @throws \Exception
     */
    public function deleteByMenuIds($menuIds)
    {
        return $this->model
            ->whereIn('menu_id', $menuIds)
            ->delete();
    }

    /**
     * @param array $data
     *
     * @return $this|bool
     */
    public function create(array $data)
    {
        // Update display_order first
        $this->model
            ->where('user_id', $data['user_id'])
            ->where('display_order', '>=', $data['display_order'])
            ->increment('display_order');

        // Create
        $model = $this->model->fill($data);

        if ($model->save()) {

            return $model;
        }

        return false;
    }

    public function getByUserIdAndMenuId($userId, $menuId)
    {
        return $this->model->where('user_id', $userId)
            ->where('menu_id', $menuId)
            ->first();
    }

    public function getAllMenuDetailByUserId($userId)
    {
        return $this->model->with(['menu'])
            ->where('user_id', $userId)
            ->orderBy('display_order', 'asc')
            ->get();
    }
}