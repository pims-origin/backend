<?php

namespace App\Api\V2\Inbound\Location\GetLocationInfo\Controllers;

use App\Api\V2\Inbound\Location\GetLocationInfo\Models\CartonModel;
use App\Api\V2\Inbound\Location\GetLocationInfo\Models\LocationModel;
use App\Api\V2\Inbound\Location\GetLocationInfo\Models\PalletModel;
use App\Api\V2\Inbound\Location\GetLocationInfo\Models\ItemModel;
use App\Api\V2\Inbound\Location\GetLocationInfo\Models\InventorySummaryModel;
use App\Api\V2\Inbound\Location\GetLocationInfo\Models\Log;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\MyHelper;

class PutAwayController extends AbstractController
{

    const PREFIX_CARTON_RFID_RELOCATE = 'CCTC';

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * PutAwayController constructor.
     *
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param ItemModel $itemModel
     */
    public function __construct(
        PalletModel $palletModel,
        LocationModel $locationModel
    ) {
        $this->palletModel   = $palletModel;
        $this->cartonModel   = new CartonModel();
        $this->locationModel = $locationModel;
        $this->itemModel     = new ItemModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function validateLocation($whsId, Request $request)
    {
        // WAP-736 - [Inbound - Relocate] Create API to check & get location code

        $url = "/v2/whs/{$whsId}/location/rack/relocate-valid-location";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'PAR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Relocate Pallet - Validate location'
        ]);

        $input = $request->getParsedBody();
        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }
        $pltRfid  = array_get($input, 'pallet_rfid');
        $location = array_get($input, 'location');
        $type     = (int)array_get($input, 'type');
        $itemId   = (int)array_get($input, 'item_id');

        // Get Pallet by Rfid
        $pallet = $this->palletModel->getFirstWhere([
            'rfid' => $pltRfid,
        ]);

        if (!$pallet) {
            $msg = "Pallet does not exist.";
            return $this->_responseErrorMessage($msg);
        }

        if ($pallet && object_get($pallet, 'whs_id') != $whsId) {
            $msg = "Pallet does not belong to current warehouse.";
            return $this->_responseErrorMessage($msg);
        }

        $cusId = object_get($pallet, 'cus_id');

        //Check existed for The loc RFID
        $locationInfo = '';
        if ($type == 1) {
            $locationInfo = $this->locationModel->getRackLocationByLocRFID($whsId, $input['location']);
        } else {
            $locationInfo = $this->locationModel->getFirstWhere([
                    'loc_whs_id' => $whsId,
                    'loc_code'   => $location
                ]);
        }
        if (empty($locationInfo) || !count($locationInfo)) {
            $msg = "Location does not exist.";
            if ($type == 1) {
                $locationInfo = $this->locationModel->getFirstWhere([
                        'rfid'   => $location
                    ]);
            } else {
                $locationInfo = $this->locationModel->getFirstWhere([
                        'loc_code'   => $location
                    ]);
            }

            if (count($locationInfo)) {
                $msg = "Location does not belong to current warehouse.";
            }

            return $this->_responseErrorMessage($msg);
        }

        $locationObj = $this->locationModel->getRackLocationByCustomerById($whsId, $locationInfo->loc_id, $cusId);

        if (empty($locationObj)) {
            $msg = "Location does not belong to this customer.";
            return $this->_responseErrorMessage($msg);
        } elseif ($locationObj->loc_sts_code != 'AC') {
            $msg = sprintf("Location %s is not active. Current Status %s",
                $locationObj->loc_code,
                $locationObj->loc_sts_code);
            return $this->_responseErrorMessage($msg);
        }

        if ($locationObj && !$locationObj->rfid) {
            $msg = sprintf("Location %s does not be Rfid",
                $locationObj->loc_code);
            return $this->_responseErrorMessage($msg);
        }

        //WAP-742 - [Migration data] Unable relocate when location is mixed SKU
        $checkMixSku = $this->_checkMixSkuOnLoc($whsId, $itemId, $locationObj->loc_id);

        if ($checkMixSku) {
            $msg = sprintf("Unable relocate on location contains cartons are mixed SKU.");
            return $this->_responseErrorMessage($msg);
        }

        //WAP-749 - [Migration data] Unable relocate when location is empty
        $checkEmptyLoc = $this->_checkEmptyLoc($whsId, $locationObj->loc_id);

        if ($checkEmptyLoc) {
            $msg = sprintf("Unable relocate on location does not contain any cartons.");
            return $this->_responseErrorMessage($msg);
        }

        try {
            $msg = sprintf("Successfully!");

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => $msg
                    ]
                ],
                'data' => [[
                    'loc_id'   => $locationObj->loc_id,
                    'loc_code' => $locationObj->loc_code,
                    'rfid'     => $locationObj->rfid,
                ]]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _validateParams($attributes, $whsId)
    {
        $pltRfid  = array_get($attributes, 'pallet_rfid');
        $location = array_get($attributes, 'location');
        $type     = (int)array_get($attributes, 'type');
        // $ctnTtl   = array_get($attributes, 'carton_total');
        $itemId   = (int)array_get($attributes, 'item_id');

        $names = [
            'pallet_rfid'  => 'Pallet rfid',
            'location'     => 'Location',
            'type'         => 'Type',
            // 'carton_total' => 'Carton total',
            'item_id'      => 'SKU',
        ];

        // Check Required
        $requireds = [
            'pallet_rfid'  => $pltRfid,
            'location'     => $location,
            'type'         => $type,
            // 'carton_total' => $ctnTtl,
            'item_id'      => $itemId,
        ];

        // Check int type
        $integers = [
            'type'         => $type,
//            'carton_total' => $ctnTtl,
           'item_id'      => $itemId,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        foreach ($integers as $key => $field) {
            if (!is_int($field) || $field < 1 || $field > 999999999) {
                $errorDetail = "{$names[$key]} must be integer and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        if (!in_array($type, [1, 2])) {
            $msg = "Type is required. Just including 1(RFID) / 2(Barcode)";
            return $this->_responseErrorMessage($msg);
        }

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            return $this->_responseErrorMessage($pltRFIDValid->error);
        }

        //validate location RFID
        if ($type == 1) {
            $locRFIDValid = new RFIDValidate($location, RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                return $this->_responseErrorMessage($locRFIDValid->error);
            }
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _createCtnRfidFromPltNum($pltObj, $seq = 0)
    {
        //create CTN number by Pallet number
        $ctnCode = str_replace('LPN', self::PREFIX_CARTON_RFID_RELOCATE, $pltObj->plt_num);
        $ctnCode = str_replace('-', '', $ctnCode);
        if (!$seq) {
            //get current carton number, count null is 0 so need add 1 for the first
            $seq = $this->model->where('plt_id', $pltObj->plt_id)->count() + 1;
        }

        // max length of carton rfid is 24
        if (strlen($ctnCode) > 20) {
            $ctnCode = substr($ctnCode, 4 + strlen($ctnCode) - 20);
            $ctnCode = self::PREFIX_CARTON_RFID_RELOCATE . $ctnCode;
        }

        $ctnRfid = $ctnCode . str_pad($seq, 24 - strlen($ctnCode), "0", STR_PAD_LEFT);

        return $ctnRfid;
    }

    private function _insertInvtSmr($whsId, $itemObj, $lot)
    {
        $itemId = $itemObj->item_id;

        $ivt = $this->inventorySummaryModel
            ->getFirstWhere([
                'item_id' => $itemId,
                'lot'     => $lot,
                'whs_id'  => $whsId
            ]);

        if (!$ivt) {
            $insertInvtSmr[] = [
                'item_id'       => $itemId,
                'cus_id'        => $itemObj->cus_id,
                'whs_id'        => $whsId,
                'color'         => $itemObj->color,
                'sku'           => $itemObj->sku,
                'size'          => $itemObj->size,
                'lot'           => $lot,
                'ttl'           => 0,
                'allocated_qty' => 0,
                'dmg_qty'       => 0,
                'avail'         => 0,
                'back_qty'      => 0,
                'upc'           => $itemObj->cus_upc,
                'crs_doc_qty'   => 0,
                'created_at'    => time(),
                'updated_at'    => time(),
            ];

            DB::table('invt_smr')->insert($insertInvtSmr);
        }

    }

    private function _updateInventory($itemId, $whsId)
    {
        return DB::table('invt_smr AS iv')
            ->join(DB::raw("(
                SELECT item_id, lot,
                    SUM(IF(!is_damaged, piece_remain, 0)) AS avail,
                    SUM(IF(loc_type_code = 'XDK', piece_remain, 0)) AS xdock_qty,
                    SUM(IF(is_damaged, piece_remain, 0)) AS dmg_qty
                FROM cartons
                WHERE item_id = {$itemId}
                    AND deleted = 0
                    AND ctn_sts = 'AC'
                GROUP BY item_id,lot
            ) tt"), function ($join) {
                $join->on('iv.item_id', '=', 'tt.item_id')
                    ->on('iv.lot', '=', 'tt.lot');
            })
            ->where([
                // 'iv.whs_id' => Data::getCurrentWhsId()
                'iv.whs_id' => $whsId
            ])
            ->update([
                "iv.ttl"         => DB::raw("tt.avail + tt.dmg_qty"),
                "iv.avail"       => DB::raw("tt.avail - tt.xdock_qty"),
                "iv.dmg_qty"     => DB::raw("tt.dmg_qty"),
                "iv.crs_doc_qty" => DB::raw("tt.xdock_qty"),
            ]);
    }

    private function _reduceInventory($pltId, $whsId)
    {
        $prefix = self::PREFIX_CARTON_RFID_RELOCATE;
        return DB::table('invt_smr AS iv')
            ->join(DB::raw("(
                SELECT item_id, lot,
                    SUM(IF(!is_damaged, piece_remain, 0)) AS avail,
                    SUM(IF(loc_type_code = 'XDK', piece_remain, 0)) AS xdock_qty,
                    SUM(IF(is_damaged, piece_remain, 0)) AS dmg_qty
                FROM cartons
                WHERE plt_id = {$pltId}
                    AND deleted = 0
                    AND ctn_sts = 'AC'
                    AND rfid like '{$prefix}%'
                GROUP BY item_id,lot
            ) tt"), function ($join) {
                $join->on('iv.item_id', '=', 'tt.item_id')
                    ->on('iv.lot', '=', 'tt.lot');
            })
            ->where([
                // 'iv.whs_id' => Data::getCurrentWhsId()
                'iv.whs_id' => $whsId
            ])
            ->update([
                "iv.ttl"         => DB::raw("iv.ttl - tt.avail - tt.dmg_qty"),
                "iv.avail"       => DB::raw("iv.avail - tt.avail + tt.xdock_qty"),
                "iv.dmg_qty"     => DB::raw("iv.dmg_qty - tt.dmg_qty"),
                "iv.crs_doc_qty" => DB::raw("iv.crs_doc_qty - tt.xdock_qty"),
            ]);
    }

    private function _checkMixSkuOnLoc($whsId, $itemId, $locId)
    {
        $count = DB::table('cartons')
            ->where('whs_id', $whsId)
            ->where('item_id', '!=', $itemId)
            ->where('loc_id', $locId)
            // ->whereIn('ctn_sts', ['AC', 'LK'])
            ->where('deleted', 0)
            ->count();

        return $count > 0 ? true : false;
    }

    private function _checkEmptyLoc($whsId, $locId)
    {
        $count = DB::table('cartons')
            ->where('whs_id', $whsId)
            ->where('loc_id', $locId)
            // ->whereIn('ctn_sts', ['AC', 'LK'])
            ->where('deleted', 0)
            ->count();

        return $count == 0 ? true : false;
    }
}
