<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

use Seldat\Wms2\Models\GoodsReceiptDetail;

class GoodsReceiptListDamageTransformer extends TransformerAbstract
{
    public function transform(GoodsReceiptDetail $goodsReceiptDetail)
    {

        return [
            'dtl_gr_dtl_id'          => object_get($goodsReceiptDetail, 'gr_dtl_id', null),
            'gr_hdr_num'             => object_get($goodsReceiptDetail, 'goodsReceipt.gr_hdr_num', null),
            'ctnr_id'                => object_get($goodsReceiptDetail, 'goodsReceipt.ctnr_id', null),
            'ctnr_num'               => object_get($goodsReceiptDetail, 'goodsReceipt.container.ctnr_num', ''),
            'dtl_item_id'            => object_get($goodsReceiptDetail, 'asnDetail.item.item_id', ''),
            'dtl_sku'                => object_get($goodsReceiptDetail, 'asnDetail.item.sku', ''),
            'dtl_gr_dtl_act_ctn_ttl' => object_get($goodsReceiptDetail, 'gr_dtl_act_ctn_ttl', ''),
        ];
    }
}
