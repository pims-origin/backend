<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SystemUomTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * SystemUomTest constructor.
     */

    public function getEndPoint()
    {
        return "/v1/system-uoms";
    }


    protected $searchParams = [
        'sys_uom_code' => 'abc',
        'sys_uom_name' => '11a111e',
    ];

    protected $systemUomParams = [
        'sys_uom_code' => 'c0d3',
        'sys_uom_name' => 'sys_uom_name',
        'sys_uom_des'  => 'this is desc',
    ];

    public function setUp()
    {
        parent::setUp();
    }

    public function testSearchSystemUom_Ok()
    {
        $this->call('GET', $this->getEndPoint(), $this->searchParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_NoParam_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testRead_Ok()
    {
        $systemUomId = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;

        $this->call('GET', $this->getEndPoint() . "/{$systemUomId}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $this->call('POST', $this->getEndPoint(), $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    public function testStore_EmptyField_Fail()
    {
        unset($this->systemUomParams['sys_uom_code']);
        $this->call('POST', $this->getEndPoint(), $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        $sys_uom_id = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;

        $this->call('PUT', $this->getEndPoint() . "/{$sys_uom_id}", $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_NotExist_Fail()
    {
        $abc = $this->call('PUT', $this->getEndPoint() . "/999999", $this->systemUomParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

//    public function testUpdate_ExistUniqueFields_Fail()
//    {
//        $systemUom = factory(\Seldat\Wms2\Models\SystemUom::class)->create();
//        $this->systemUomParams['sys_uom_code'] = $systemUom->sys_uom_code;
//
//        $sys_uom_id = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
//        $this->call('PUT', $this->getEndPoint() . "/{$sys_uom_id}", $this->systemUomParams);
//
//        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
//    }

    public function testUpdate_EmptyField_Fail()
    {
        $sys_uom_id = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;

        unset($this->systemUomParams['sys_uom_code']);
        $this->call('PUT', $this->getEndPoint() . "/{$sys_uom_id}", $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDelete_Ok()
    {
        $sys_uom_id = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;

        $this->call('DELETE', $this->getEndPoint() . "/{$sys_uom_id}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDelete_NotExit_Fail()
    {
        $this->call('DELETE', $this->getEndPoint() . "/9999999999999999");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $sys_uom_id1 = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        $sys_uom_id2 = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        $sys_uom_id3 = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        $sys_uom_id4 = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        $sys_uom_id5 = factory(\Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;

        $this->call('DELETE', $this->getEndPoint(),
            ['sys_uom_id' => [$sys_uom_id1, $sys_uom_id2, $sys_uom_id3, $sys_uom_id4, $sys_uom_id5]]);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }
}