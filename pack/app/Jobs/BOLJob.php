<?php
/**
 * Created by PhpStorm.
 * User: vinhpham
 * Date: 9/25/17
 * Time: 1:34 PM
 */

namespace App\Jobs;

use DB;
use GuzzleHttp\Client;
use Wms2\UserInfo\Data;

class BOLJob extends Job
{

    protected $orderId;
    protected $request;
    protected $whsId;
    /**
     * BOLJob constructor.
     */
    public function __construct($orderId, $whsId, $request)
    {
        $this->orderId = $orderId;
        $this->request = $request;
        $this->whsId = $whsId;

    }

    /*
     *
     */

    public function handle()
    {
        $client = new Client();
        try {
            $client->request('GET', env('API_ORDER_V2').'/whs/' . $this->whsId . '/auto-bol/' . $this->orderId,
                [
                    'headers' => ['Authorization' => $this->request->getHeader('Authorization')]
                ]
            );
            
            DB::table('sys_bugs')->insert([
                'date'       => time(),
                'api_name'   => 'Auto Create BOL',
                'created_at' => time(),
                'created_by' => Data::getCurrentUserId(),
                'updated_at' => time(),
                'error'      => env('API_ORDER_V2') . '/whs/' . $this->whsId . '/auto-bol/' . $this->orderId
            ]);
            return true;
        } catch (\Exception $exception) {
            DB::table('sys_bugs')->insert([
                'date'       => time(),
                'api_name'   => 'Auto Create BOL',
                'created_at' => time(),
                'created_by' => Data::getCurrentUserId(),
                'updated_at' => time(),
                'error'      => $exception->getMessage()
            ]);
            return false;
        }
    }
}