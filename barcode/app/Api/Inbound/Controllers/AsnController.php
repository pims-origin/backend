<?php

namespace App\Api\Inbound\Controllers;

use App\Api\Inbound\Models\CartonModel;
use App\Api\Inbound\Models\ContainerModel;
use App\Api\Inbound\Models\CustomerModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\Inbound\Models\GoodsReceiptDetailModel;
use App\Api\Inbound\Models\GoodsReceiptModel;
use App\Api\Inbound\Models\ItemModel;
use App\Api\Inbound\Models\AsnDtlModel;
use App\Api\Inbound\Models\AsnHdrModel;
use App\Api\Inbound\Models\SystemUomModel;
use App\Api\Inbound\Models\VirtualCartonModel;
use App\Api\Inbound\Models\VirtualCartonSumModel;
use App\Api\Inbound\Transformers\AsnDetailTransformer;
use App\Api\Inbound\Transformers\AsnListsCanSortTransformer;
use App\Api\Inbound\Transformers\AsnListsCanSortV1Transformer;
use App\Api\Inbound\Transformers\AsnListVirtualCartonTransformer;
use App\Api\Inbound\Transformers\GoodsReceiptTransformer;
use App\Api\Inbound\Validators\AsnValidator;
use App\Api\Inbound\Validators\GoodsReceiptValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Validator;
use Dingo\Api\Http\Response;
use Wms2\UserInfo\Data;

class AsnController extends AbstractController
{

    /**
     * @var AsnHdrModel
     */
    protected $asnHdrModel;
    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;
    /**
     * @var ItemModel
     */
    protected $itemModel;
    /**
     * @var AsnValidator
     */
    protected $validator;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var GoodsReceiptTransformer
     */
    protected $goodsReceiptTransformer;

    /**
     * @var GoodsReceiptValidator
     */
    protected $goodsReceiptValidator;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var SystemUomModel
     */
    protected $systemUomModel;

    /**
     * @var CustomerModel
     */
    protected $customerModel;

    /**
     * @var VirtualCartonModel
     */
    protected $vtlCtnModel;

    /**
     * @var VirtualCartonSumModel
     */
    protected $vtlCtnSumModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var ContainerModel
     */
    protected $containerModel;

    /**
     * AsnController constructor.
     *
     * @param Item $itemModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(Item $itemModel, EventTrackingModel $eventTrackingModel)
    {
        //$this->validator = new AsnValidator();
        //$this->itemModel = new ItemModel();
        //$this->eventTrackingModel = $eventTrackingModel;
        //$this->goodsReceiptTransformer = new GoodsReceiptTransformer();
        //$this->goodsReceiptValidator = new GoodsReceiptValidator();
        $this->asnHdrModel = new AsnHdrModel();
        //$this->goodsReceiptModel = new GoodsReceiptModel();
        $this->asnDtlModel = new AsnDtlModel();
        //$this->goodsReceiptDetailModel = new GoodsReceiptDetailModel();
        //$this->systemUomModel = new SystemUomModel();
        //$this->customerModel = new CustomerModel();
        //$this->vtlCtnModel = new VirtualCartonModel();
        //$this->vtlCtnSumModel = new VirtualCartonSumModel();
        //$this->cartonModel = new CartonModel();
        //$this->containerModel = new ContainerModel();
    }

    public function searchCanSortV1(
        Request $request,
        $whsId,
        AsnListsCanSortV1Transformer $asnListsCanSortTransformerV1
    ) {
        set_time_limit(0);
        $input = $request->getQueryParams();
        //get needed params
        $input['whs_id'] = $whsId;

        try {
            $asns = $this->asnDtlModel->getListASN($input, array_get($input, 'limit'));

            return $this->response->paginator($asns, $asnListsCanSortTransformerV1);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param $asnID
     * @param $ctnID
     * @param AsnDetailTransformer $asnDetailTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function loadASNDtl(
        $whsId,
        $cusId,
        $asnID,
        $ctnID,
        AsnDetailTransformer $asnDetailTransformer,
        Request $request
    ) {

        if (!$asnHrd = $this->asnHdrModel->getFirstBy('asn_hdr_id', $asnID)) {
            $msg = Message::get("BM017", "ASN");
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $asnDtls = $this->asnDtlModel->findWhere(
            [
                'asn_hdr_id'  => $asnID,
                'ctnr_id'     => $ctnID,
                'asn_dtl_sts' => ['asn_dtl_sts', '!=', 'CC']
            ],
            ['container', 'grDtl', 'grDtl.goodsReceipt', 'item', 'systemUom', 'virtualCarton', 'virtualCartonSummary'],
            ['asn_dtl_id' => 'asc']
        );

        $details = [];
        if ($asnDtls) {
            foreach ($asnDtls as $asnDtl) {
                $details[] = [
                    'asn_dtl_id'             => $asnDtl->asn_dtl_id,
                    'asn_hdr_id'             => $asnDtl->asn_hdr_id,
                    'ctnr_id'                => $asnDtl->ctnr_id,
                    'ctnr_num'               => $asnDtl->container->ctnr_num,
                    'dtl_item_id'            => $asnDtl->item_id,
                    'asn_dtl_lot'            => $asnDtl->asn_dtl_lot,
                    'asn_dtl_cus_upc'        => $asnDtl->asn_dtl_cus_upc,
                    'dtl_sku'                => $asnDtl->item->sku,
                    'dtl_size'               => $asnDtl->item->size,
                    'dtl_color'              => $asnDtl->item->color,
                    'dtl_uom_id'             => $asnDtl->uom_id,
                    'dtl_uom_code'           => object_get($asnDtl, 'systemUom.sys_uom_code', ''),
                    'dtl_uom_name'           => object_get($asnDtl, 'systemUom.sys_uom_name', ''),
                    'dtl_po'                 => $asnDtl->asn_dtl_po,
                    'dtl_po_date'            => ($asnDtl->asn_dtl_po_dt) ? date('m/d/Y', $asnDtl->asn_dtl_po_dt) : '',
                    'dtl_ctn_ttl'            => $asnDtl->asn_dtl_ctn_ttl - $asnDtl->asn_dtl_crs_doc,
                    'gr_hdr_num'             => object_get($asnDtl, 'grDtl.goodsReceipt.gr_hdr_num', ''),
//                    'dtl_gr_dtl_act_ctn_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_act_ctn_ttl', 0),
                    'dtl_gr_dtl_act_ctn_ttl' => intval(object_get($asnDtl, 'grDtl.gr_dtl_act_ctn_ttl', 0)) -
                        intval(object_get($asnDtl, 'grDtl.crs_doc', 0)),
                    'gr_dtl_plt_ttl'         => object_get($asnDtl, 'grDtl.gr_dtl_plt_ttl', 0),
                    'dtl_gr_dtl_is_dmg'      => object_get($asnDtl, 'grDtl.gr_dtl_is_dmg', 0),
                    'dtl_gr_dtl_disc'        => object_get($asnDtl, 'grDtl.gr_dtl_disc', 0),
//                    'dtl_gr_dtl_act_qty_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_act_qty_ttl', 0),
                    'dtl_gr_dtl_act_qty_ttl' => intval(object_get($asnDtl, 'grDtl.gr_dtl_act_qty_ttl', 0)) -
                        intval(object_get($asnDtl, 'grDtl.crs_doc_qty', 0)),
//                    'asn_dtl_qty_ttl'        => object_get($asnDtl, 'asn_dtl_qty_ttl', 0),
                    'asn_dtl_qty_ttl'        => $asnDtl->asn_dtl_pack * ($asnDtl->asn_dtl_ctn_ttl - $asnDtl->asn_dtl_crs_doc),
                    'dtl_crs_doc'            => $asnDtl->asn_dtl_crs_doc,
                    'dtl_des'                => $asnDtl->asn_dtl_des,
                    'dtl_length'             => $asnDtl->asn_dtl_length,
                    'dtl_width'              => $asnDtl->asn_dtl_width,
                    'dtl_height'             => $asnDtl->asn_dtl_height,
                    'dtl_weight'             => $asnDtl->asn_dtl_weight,
                    'asn_dtl_pack'           => $asnDtl->asn_dtl_pack,
                    'dtl_lot'                => $asnDtl->asn_dtl_lot,
                    'dtl_ttl_piece'          => $asnDtl->asn_dtl_pack * ($asnDtl->asn_dtl_ctn_ttl - $asnDtl->asn_dtl_crs_doc),
                    'scan_vir_ctn_ttl'       => $asnDtl->virtualCarton()->count(),
                    'asn_dtl_status'         => object_get($asnDtl, 'virtualCartonSummary.vtl_ctn_sum_sts', ''),
                    'gate_code'              => object_get($asnDtl, 'virtualCartonSummary.gate_code', ''),
                ];
            }
        }

        $asnHrd->details = $details;

        return $this->response->item($asnHrd, $asnDetailTransformer);
    }

    /**
     * @param $whsId
     * @param $asnDtlId
     *
     * @return array
     */
    public function getAsnDetail2($whsId, $asnDtlId)
    {
        $data = AsnDtl::select([
            'asn_dtl.asn_dtl_id',
            'asn_dtl.asn_hdr_id',
            'asn_dtl.ctnr_num',
            'asn_dtl.ctnr_id',
            'asn_dtl.item_id',
            'asn_dtl.asn_dtl_sku',
            'asn_dtl.asn_dtl_size',
            'asn_dtl.asn_dtl_color',
            'asn_dtl.asn_dtl_lot',
            'asn_dtl.asn_dtl_des',
            'asn_dtl.ucc128',
            DB::raw('(asn_dtl.asn_dtl_ctn_ttl - asn_dtl.asn_dtl_crs_doc) AS asn_dtl_ctn_ttl'),
            'asn_dtl.asn_dtl_cus_upc AS upc',
            'asn_dtl.expired_dt',
            'asn_dtl.uom_code',
            DB::raw('(gr_dtl.gr_dtl_act_ctn_ttl - gr_dtl.crs_doc) AS act_ctn'),
            DB::raw('gr_dtl.gr_dtl_plt_ttl AS plts'),
            DB::raw('gr_dtl.gr_dtl_dmg_ttl AS dmg_ttl'),
            //DB::raw('(asn_dtl.asn_dtl_qty_ttl - asn_dtl.asn_dtl_crs_doc) AS asn_dtl_qty_ttl'),
            'gr_dtl.gr_dtl_plt_ttl',
            DB::raw('(gr_dtl.gr_dtl_act_qty_ttl - gr_dtl.crs_doc_qty) AS gr_dtl_act_qty_ttl')
        ])
            ->leftJoin('gr_dtl', 'gr_dtl.asn_dtl_id', '=', 'asn_dtl.asn_dtl_id')
            ->where([
                'asn_dtl.asn_dtl_id' => $asnDtlId,
            ])
            ->whereIn('asn_dtl.asn_dtl_sts', ['NW', 'RG'])
            ->first();;
        $data = $data ? $data->toArray() : [];

        if (!empty($data['expired_dt'])) {
            $data['expired_dt'] = date('ymd', $data['expired_dt']);
        }

        return (array)$data;
    }
}
