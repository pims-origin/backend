<?php

namespace App\Api\V2\Inbound\Containers\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;

class SkuTransformer extends TransformerAbstract
{
    public function transform(AsnDtl $asnDtl)
    {
        return [
            'item_id' => object_get($asnDtl, 'item_id', ''),
            'sku'     => object_get($asnDtl, 'asn_dtl_sku', ''),
            'size'    => object_get($asnDtl, 'asn_dtl_size', ''),
            'color'   => object_get($asnDtl, 'asn_dtl_color', ''),
            'po'      => object_get($asnDtl, 'asn_dtl_po', ''),
        ];
    }

}