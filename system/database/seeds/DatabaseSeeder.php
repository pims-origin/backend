<?php

use Illuminate\Database\Seeder;
use App\Api\V1\Entities\Status;
use App\Api\V1\Entities\Link;
use App\Api\V1\Entities\Project;
use App\Api\V1\Entities\Note;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UserTableSeeder');

        $pending = ['name' => 'Pending'];
        $doing = ['name' => 'Doing'];
        $completed = ['name' => 'Completed'];
        $statusPending = Status::create($pending);
        $statusDoing = Status::create($doing);
        $statusCompleted = Status::create($completed);

        $project1 = ['name'         => 'Project 1',
                     'url'          => 'http://project1.com',
                     'completed_at' => '',
                     'description'  => 'Description for project 1',
                     'repository'   => '',
                     'status_id'    => $statusCompleted->id
        ];
        $project2 = ['name'         => 'Project 2',
                     'url'          => 'http://project2.com',
                     'completed_at' => '',
                     'description'  => 'Description for project 2',
                     'repository'   => '',
                     'status_id'    => $statusCompleted->id
        ];
        $project3 = ['name'         => 'Project 3',
                     'url'          => 'http://project3.com',
                     'completed_at' => '',
                     'description'  => 'Description for project 3',
                     'repository'   => '',
                     'status_id'    => $statusDoing->id
        ];
        $project4 = ['name'         => 'Project 4',
                     'url'          => 'http://project4.com',
                     'completed_at' => '',
                     'description'  => 'Description for project 4',
                     'repository'   => '',
                     'status_id'    => $statusDoing->id
        ];
        $project5 = ['name'         => 'Project 5',
                     'url'          => 'http://project5.com',
                     'completed_at' => '',
                     'description'  => 'Description for project 5',
                     'repository'   => '',
                     'status_id'    => $statusPending->id
        ];
        $project6 = ['name'         => 'Project 6',
                     'url'          => 'http://project6.com',
                     'completed_at' => '',
                     'description'  => 'Description for project 6',
                     'repository'   => '',
                     'status_id'    => $statusPending->id
        ];
        $rsProject1 = Project::create($project1);
        $rsProject2 = Project::create($project2);
        $rsProject3 = Project::create($project3);
        $rsProject4 = Project::create($project4);
        $rsProject5 = Project::create($project5);
        $rsProject6 = Project::create($project6);

        $note1 = ['stamp'       => '11/05/2015',
                  'title'       => 'Note 1',
                  'description' => 'Description for note 1',
                  'project_id'  => $rsProject1->id
        ];
        $note2 = ['stamp'       => '12/11/2015',
                  'title'       => 'Note 2',
                  'description' => 'Description for note 2',
                  'project_id'  => $rsProject1->id
        ];
        $note3 = ['stamp'       => '20/01/2016',
                  'title'       => 'Note 3',
                  'description' => 'Description for note 3',
                  'project_id'  => $rsProject2->id
        ];
        $rsNote1 = Note::create($note1);
        $rsNote2 = Note::create($note2);
        $rsNote3 = Note::create($note3);


        $link1 = ['name' => 'Link 1', 'href' => 'http://link1.com', 'note_id' => $rsNote1->id];
        $link2 = ['name' => 'Link 2', 'href' => 'http://link2.com', 'note_id' => $rsNote1->id];
        $link3 = ['name' => 'Link 3', 'href' => 'http://link3.com', 'note_id' => $rsNote2->id];
        $rsLink1 = Link::create($link1);
        $rsLink2 = Link::create($link2);
        $rsLink3 = Link::create($link3);

        $this->command->info('Seed DB data successfully.');
    }
}
