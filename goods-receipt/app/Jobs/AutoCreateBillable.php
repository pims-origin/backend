<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use App\Jobs\BillableLaborJob;
use App\Jobs\BillableStorageJob;

class AutoCreateBillable extends Job
{
    protected $whsId;
    protected $cusId;
    protected $grHdrId;
    protected $request;
    public function __construct($whsId, $cusId, $grHdrId, $request)
    {
        $this->whsId   = $whsId;
        $this->cusId   = $cusId;
        $this->grHdrId = $grHdrId;
        $this->request = $request;
    }

    public function handle()
    {
        $client     = new Client();
        $apiInvoice = env('API_INVOICE_MASTER');

        try {
            if (!$apiInvoice) {
                $this->writeLogment('Goods Receipt: Auto create Billable', 'Can\'t found API INVOICE MASTER in ENV.');
                return false;
            }
            $data = $this->buildQuery();
            $client->request('POST', $apiInvoice . '/v1/billableitem/goodsreceipt', $data);
            
            $this->writeLogment('Goods Receipt: Auto create Billable', 'Auto create Billable successfully.');

            // Send IMS Billable Labor
            dispatch(new BillableLaborJob($this->whsId, $this->grHdrId));
            // Send IMS Billable Storage
            dispatch(new BillableStorageJob($this->whsId, $this->grHdrId));
            
        } catch (\Exception $e) {
            $this->writeLogment('Goods Receipt: Auto create Billable', $e->getMessage());
        }
    }

    private function buildQuery()
    {
        return [
            'headers'     => ['Authorization' => $this->request->getHeader('Authorization')],
            'form_params' => [
                'whs_id'    => $this->whsId,
                'cus_id'    => $this->cusId,
                'gr_hdr_id' => $this->grHdrId,
            ],
        ];
    }
}
