<?php

namespace App\Api\V1\Transformers;

use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\Status;
use App\Api\V1\Models\WavePickDtlModel;

class WvHdrDetailsTransformer extends TransformerAbstract
{
    protected $defaultIncludes  = ['details'];
    public function transform(WavepickHdr $wvHdr)
    {
        $wv = new WavePickDtlModel();
        $wv_sts = $wv->formatStatus('wvpick', object_get($wvHdr, 'wv_sts', null));
        $arrWvHdr = [
            'wv_id' => object_get($wvHdr, 'wv_id', null),
            'wv_num' => object_get($wvHdr, 'wv_num', null),
            'wv_sts' => $wv_sts,
            'wv_sts_name' => Status::getByKey('WAVE_HDR_STATUS', $wv_sts),
            'of_order' => $wvHdr->OdrTtl,
            'total_sku' =>$wvHdr->SkuTtl,
            'created_at' => object_get($wvHdr, 'created_at', null),
        ];
        return $arrWvHdr;

    }

    public function includeDetails(WavepickHdr $wvHdr, ParamBag $params = null)
    {
        $details = $wvHdr->details()->get();

        return $this->collection($details, new WvDetailsTransformer());
    }
}
