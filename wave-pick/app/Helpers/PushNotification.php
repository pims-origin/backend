<?php

namespace App\Helpers;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Illuminate\Support\Facades\DB;

class PushNotification
{
    public static function alertToPicker($picker_id, $title, $content)
    {
        $device_info = DB::table('user_meta')->where('user_id', $picker_id)->where('qualifier', 'DTK')->value('value');
        $device_info = json_decode($device_info, true);
        $device_token = array_get($device_info, 'device_token');

        if (!$device_token) {
            return false;
        }

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($content)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);

    }
}