<?php

namespace App\Api\Inventory\Models;

use App\Api\Inventory\Services\CycleCountService;
use Carbon\Carbon;
use Mockery\CountValidator\Exception;
use phpDocumentor\Reflection\Types\Null_;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Warehouse;
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\Paginator;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\CustomerWarehouse;
use Seldat\Wms2\Models\Customer;
use App\Api\Inventory\Validators\CycleDtlValidator;
use Wms2\UserInfo\Data;

class CycleHdrModel extends AbstractModel
{

    const COUNT_BY_EACH = 'EACH';
    const COUNT_BY_CARTON = 'CARTON';
    const STATUS_INSERT = 'I';
    const STATUS_UPDATE = 'U';
    const STATUS_DELETE = 'D';
    const CYCLE_TYPE_CUSTOMER = 'CS';
    const CYCLE_TYPE_SKU = 'SK';
    const CYCLE_TYPE_LOCATION = 'LC';
    const NA_VALUE = 'NA';

    const STATUS_CYCLE_OPEN = 'OP';
    const STATUS_CYCLE_ASSIGNED = 'AS';
    const STATUS_CYCLE_CYCLED = 'CC';
    const STATUS_CYCLE_RECOUNT = 'RC';
    const STATUS_CYCLE_COMPLETED = 'CP';
    const STATUS_CYCLE_DELETED = 'DL';

    const CYCLE_METHOD_RFGUN = 'rfgun';
    const CYCLE_METHOD_PAPER = 'paper';

    public static $arrCycleType = [
        self::CYCLE_TYPE_CUSTOMER => 'Customer',
        self::CYCLE_TYPE_SKU => 'SKU',
        self::CYCLE_TYPE_LOCATION => 'Location',
    ];

    public static $arrCycleStatus = [
        self::STATUS_CYCLE_OPEN => 'Open',
        self::STATUS_CYCLE_ASSIGNED => 'Assigned',
        self::STATUS_CYCLE_CYCLED => 'Cycled',
        self::STATUS_CYCLE_RECOUNT => 'Recount',
        self::STATUS_CYCLE_COMPLETED => 'Completed',
        self::STATUS_CYCLE_DELETED=> 'Deleted',
    ];

    public static $arrCycleMethod = [
        self::CYCLE_METHOD_RFGUN => 'rfgun',
        self::CYCLE_METHOD_PAPER => 'paper',
    ];

    private $arrNumberFields = [
        'cycle_hdr_id',
        'whs_id',
        'assignee_id',
        'created_by',
        'updated_by'
    ];

    private $arrStringFields = [
        'cycle_name',
        'count_by',
        'cycle_method',
        'cycle_type',
        'cycle_detail',
        'description',
        'due_dt',
        'cycle_sts',
        'sts'
    ];

    public $cycleType = ['CS', 'SK', 'LC'];

    public $errors = [];
    public $waring = [];
    public $cycleDtlData = [];
    private $locIdsRange = [];

    /**
     * CycleHdrModel constructor.
     * @param CycleHdr|NULL $model
     */
    public function __construct(CycleHdr $model = NULL)
    {
        $this->model = $model ? $model : new CycleHdr();
    }

    /**
     * @param $cycleId
     * @return bool
     */
    public function deleteCycleHdr($cycleId)
    {
        if (! $cycleId) {
            return false;
        }

        return $this->model
            ->where('cycle_hdr_id', $cycleId)
            ->delete();
    }

    /**
     * Search Cycle
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return object|null $models
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $userId = Data::getCurrentUserId();
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes)) {

            foreach ($attributes as $key => $value) {
                if (in_array($key, $this->arrNumberFields)) {
                    $query->where($key, $value);
                } elseif (in_array($key, $this->arrStringFields)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $query->whereRaw("(cycle_hdr.created_by = $userId OR cycle_hdr.assignee_id = $userId)")
            ->whereIn('cycle_sts', [self::STATUS_CYCLE_ASSIGNED, self::STATUS_CYCLE_RECOUNT])
            ->where('cycle_hdr.whs_id', Data::getCurrentWhsId())
        ;

        $this->model->filterDataIn($query, false, false, false);
        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);
        Log::info();
        return $models;
    }

    /**
     * @param $cycleIds
     * @return bool
     */
    public function deleteCycleHdrs($cycleIds)
    {
        if (! ($cycleIds && is_array($cycleIds))) {
            return false;
        }

        return $this->model
            ->whereIn('cycle_hdr_id', $cycleIds)
            ->delete();
    }

    /**
     * @param $input
     *
     * @return bool
     */
    public function processCreateCycleHdr($input)
    {
        $cycleHdr = new CycleHdr();

        $validate = $this->validateCreateCycleHdr($input);

        if (! $validate) {
            return false;
        }

        $itemIds = array_column($this->cycleDtlData, 'item_id');
        $locIds = array_column($this->cycleDtlData, 'sys_loc_id');
        $cycleDetail =  $input['cycle_type'] == self::CYCLE_TYPE_LOCATION ?
            $this->locIdsRange : $input['cycle_detail'];

        try {

            DB::beginTransaction();

            $model = $cycleHdr->create([
                'cycle_name' => $input['cycle_name'],
                'cycle_num' => $input['cycle_num'],
                'whs_id' => $input['whs_id'],
                'count_by' => $input['cycle_count_by'],
                'cycle_method' => $input['cycle_method'],
                'cycle_type' => $input['cycle_type'],
                'cycle_detail' =>  implode(',', $cycleDetail),
                'description' => $input['cycle_des'],
                'assignee_id' => $input['cycle_assign_to'],
                'due_dt' => $input['cycle_due_date'],
                'has_color_size' => $input['cycle_has_color_size'],
                'cycle_sts' => self::STATUS_CYCLE_ASSIGNED,
                'sts' => self::STATUS_INSERT
            ]);

            $this->createCycleDtls($model, $this->cycleDtlData);

            $this->processLockWithCycleHdr($model, $itemIds, $locIds);

            DB::commit();
        } catch (Exception $e) {

            DB::rollback();
            $this->errors[] = $e->getMessage();
            return false;
        }

        return $model;
    }

    /**
     * @param $cycleHdr
     * @param $cycleDtls
     */
    public function processLockWithCycleHdr($cycleHdr, $itemIds, $locIds)
    {
        if (! ($cycleHdr && $itemIds && $locIds)) {
            return false;
        }

        switch ($cycleHdr->cycle_type)
        {
            case self::CYCLE_TYPE_CUSTOMER:
                //lock carton
                $locIds = LocationModel::getLocationIdsByCustomer($cycleHdr->whs_id, $cycleHdr->cycle_detail);
                
                break;
            case self::CYCLE_TYPE_LOCATION:
                
                $locIds = explode(',', $cycleHdr->cycle_detail);
                break;
            case self::CYCLE_TYPE_SKU:

                break;
        }

        LockModel::lockCartonsByCycleHdr($cycleHdr->whs_id, $itemIds, $locIds);
        LockModel::lockLocationByIds($cycleHdr->whs_id, $locIds);
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function validateCreateCycleHdr(&$data)
    {
        $validInput = $this->validateInputCreateCycleHdr($data);

        if (! $validInput) {
            return $validInput;
        }

        $itemModel = new ItemModel();

        $cycleDetail = $data['cycle_type'] == self::CYCLE_TYPE_LOCATION ?
            $this->locIdsRange : $data['cycle_detail'];

        $loccationLockOnCycle = LocationModel::getLocationLockByCycleType(
                $data['whs_id'],
                $data['cycle_type'],
                $cycleDetail
            );

        if ($loccationLockOnCycle) {
            $this->errors['sku'] = 'This Cycle count can not create. Locations locked: '. implode(',', $loccationLockOnCycle);
            return false;
        }

        $cycleDtls = $this->getCycleData(
                $data['whs_id'],
                $data['cycle_type'],
                $cycleDetail
            );

        if (! $cycleDtls) {
            $this->errors['cus_id'] = $this->getErrorMsgWithNoInventory($data['cycle_type'], $cycleDetail);
            return false;
        }

        $itemIds = array_unique(array_column($cycleDtls, 'item_id'));
        $itemsLock = $itemModel->getItemLocked($data['whs_id'], $itemIds);

        if ($itemsLock) {
            $this->errors[] =$itemModel->getErrorMsgWithSKUExist($itemsLock);
            return false;
        }

        if (! $data['cycle_has_color_size']) {
            $hasColorSize = $this->checkHasColorSize($cycleDtls);
            if ($hasColorSize) {
                $this->errors['sku'] = Message::get('BM071', implode(',', array_keys($hasColorSize))) ;
                return false;
            }
        }

        $this->cycleDtlData = $cycleDtls;

        return true;
    }

    /**
     * @param $cycleType
     * @param $cycleDetail
     *
     * @return string
     */
    private function getErrorMsgWithNoInventory($cycleType, $cycleDetail)
    {
        $tmpError = 'This customer';

        switch ($cycleType)
        {
            case self::CYCLE_TYPE_LOCATION:
                $tmpError = count($cycleDetail) > 1 ? 'Locations are' : 'Location is';
                //$result = sprintf($result, $tmpError);
                break;
            case self::CYCLE_TYPE_SKU:
                $tmpError = count($cycleDetail) > 1 ? 'SKUs are' : 'SKU is';
                //$result = sprintf($result, $tmpError);
                break;
            case  self::CYCLE_TYPE_CUSTOMER:
                //$result = sprintf($result, $tmpError);
                break;
        }
        $result = Message::get('BM087', $tmpError);
        return $result;
    }

    /**
     * @param $data
     */
    public function validateInputCreateCycleHdr(&$data)
    {
        if (! $data) {
            return false;
        }

        if (! $data['whs_id']) {
            $this->errors['whs_id'] = Message::get('VR029', 'Warehouse');
        } else if (! $this->checkExitsWarehouse($data['whs_id'])) {
            $this->errors['whs_id'] = Message::get('BM067', 'Warehouse');
        }

        if (! $data['cycle_detail']) {
            $this->errors['cycle_detail'] = Message::get('VR029', 'Cycle detail');
        } else {
            $data['cycle_detail'] = array_map('trim', explode(',', $data['cycle_detail']));
        }

        if (! isset($data['cycle_has_color_size'])) {
            $this->errors['cycle_has_color_size'] = Message::get('BM088', 'has_color_size');
        } else {
            $data['cycle_has_color_size'] = $data['cycle_has_color_size'] == 'true' ? 1 : 0;
        }

        if (! isset($data['cycle_count_by'])) {
            $this->errors['cycle_count_by'] = Message::get('BM088', 'count_by');
        } else {
            $data['cycle_count_by'] = strtoupper($data['cycle_count_by']);
        }

        if (! $data['cycle_type']) {
            $this->errors['cycle_type'] = Message::get('VR029', 'Cycle type');
        } else if (! in_array(strtoupper($data['cycle_type']), $this->cycleType)) {
            $this->errors['cycle_type'] = Message::get('BM081', 'Cycle type');
        } else {
            $data['cycle_type'] = strtoupper($data['cycle_type']);
            //validate by cycle type
            $this->validateDataByCycleType($data);
        }

        if ($data['cycle_due_date']) {
            $data['cycle_due_date'] = $this->reFormatDueDate($data['cycle_due_date']);
        }

        return $this->errors ? false : true;
    }

    /**
     * @param $string
     */
    private function reFormatDueDate($string) 
    {
        $result = NULL;
        
        if (! $string) {
            return $result;
        }
        
        $arr = explode('/', $string);

        if (count($arr) != 3) {
            return $result;
        }

        $result = $arr[2] . '-' . $arr[0] . '-' . $arr[1];

        return $result;
    }

    /**
     * @param $cycleType
     * @param $cycleDetail
     */
    public function validateDataByCycleType($data)
    {
        $result = false;

        switch (strtoupper($data['cycle_type'])) {
            case self::CYCLE_TYPE_CUSTOMER:
                $result = $this->checkValidCusInput($data);
                break;
            case self::CYCLE_TYPE_SKU:
                $result = $this->checkValidSKUInput($data);
                break;
            case self::CYCLE_TYPE_LOCATION:
                $result = $this->checkValidLocIdsInput($data);
                break;
        }

        return $result;
    }

    /**
     * @param      $whsId
     * @param      $cycleType
     * @param      $cycleDetail
     * @param bool $checkHasData
     */
    public function getCycleData($whsId, $cycleType, $cycleDetail, $checkHasData = false)
    {
        $status = LockModel::STATUS_ACTIVE;
        $field = $this->getFieldCycleByType($cycleType);
        $locTypeRacId = LockModel::getLocTypeId();

        $obj = Carton::select(
                'cartons.whs_id',
                'cartons.cus_id',
                'cartons.item_id',
                'cartons.sku',
                'cartons.size',
                'cartons.color',
                'cartons.lot',
                'cartons.ctn_pack_size AS pack_size',
                'cartons.piece_remain AS piece_remain',
                DB::raw('SUM(cartons.piece_remain) AS sys_pieces_qty'),
                'cartons.ctn_uom_id',
                DB::raw('COUNT(cartons.ctn_id) AS sys_carton_qty'),
                'cartons.loc_id AS sys_loc_id',
                'cartons.loc_name AS sys_loc_name'
            )
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
            //->join('item', 'item.item_id', '=', 'cartons.item_id')
            //->where('item.status', $status)
            ->where('location.loc_sts_code', $status)
            ->where('cartons.ctn_sts', $status)
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.piece_remain', '>', 0)
            ->whereIn($field, $cycleDetail)
            ->groupBy(
                'cartons.whs_id',
                'cartons.cus_id',
                'cartons.item_id',
                'cartons.piece_remain',
                //'cartons.ctn_pack_size',
                'cartons.loc_id',
                'cartons.lot'
            )
        ;

        $data = $checkHasData ? $obj->count() : $obj->get()->toArray();

        return $data;
    }

    /**
     * @param $input
     */
    public function prepareDataCycle($input)
    {
        return $this->getCycleData($input['whs_id'], $input['cycle_type'], $input['cycle_detail']);
    }

    /**
     * @param $cycleType
     */
    public function getFieldCycleByType($cycleType)
    {
        $field = '';

        switch ($cycleType) {
            case self::CYCLE_TYPE_CUSTOMER:
                $field = 'cartons.cus_id';
                break;
            case self::CYCLE_TYPE_SKU:
                $field = 'cartons.sku';
                break;
            default:
                $field = 'cartons.loc_id';
                break;
        }

        return $field;
    }

    /**
     * @param $whsId
     */
    public function checkExitsWarehouse($whsId)
    {
        return Warehouse::where('whs_id', $whsId)->count();
    }

    /**
     * @param $cusIds
     */
    public function getCustomerInfo($cusIds, $select = ['cus_id'])
    {
        $cusIds = (array) $cusIds;

        $result = Customer::select($select)
            ->whereIn('cus_id', $cusIds)
            ->get();

        return $result;
    }

    /**
     * @param $cusId
     */
    public function checkExistCustomer($cusId)
    {
        return Customer::where('cus_id', $cusId)->count();
    }

    /**
     * @param $whsId
     * @param $cusId
     */
    public function getWarehouseHasCusIds($whsId, $cusIds)
    {
        $cusIds = (array) $cusIds;

        $result = CustomerWarehouse::where('whs_id', $whsId)
            ->whereIn('cus_id', $cusIds)
            ->get();

        return $result;
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function checkHasColorSize($data)
    {
        $result = [];

        if (! $data) {
            return $result;
        }

        foreach ($data as $item) {
            if ($item['size'] != self::NA_VALUE || $item['color'] == self::NA_VALUE) {
                $result[$item['sku']] = true;
            }
        }

        return $result;
    }

    /**
     * @param $userId
     *
     * @return mixed
     */
    public function checkExistUser($userId)
    {
        return User::where('user_id', $userId)->count();
    }

    /**
     * @param $cusIds
     */
    public function checkValidCusInput($data)
    {
        $cusIds = isset($data['cycle_detail']) ? $data['cycle_detail']: NULL;
        $whsId = isset($data['whs_id']) ? $data['whs_id']: NULL;
        //get customer by cusId
        $customer= $this->getCustomerInfo($cusIds);

        $cusInvalid = $cusIdValid = [];

        if ($customer->isEmpty()) {
            $this->errors['cus_id'] = Message::get('BM089', 'Customer', implode(',', $cusIds));
            return false;
        }

        //cusId valid
        $cusIdValid = array_column($customer->toArray(), 'cus_id');
        $cusInvalid = array_diff($cusIds, $cusIdValid);

        // exists cusId invalid
        if ($cusInvalid) {
            $this->errors['cus_id'] = Message::get('BM089', 'Customer', implode(',', $cusInvalid));
            return false;
        }

        $warehouseCus = $this->getWarehouseHasCusIds($whsId, $cusIds);

        if ($warehouseCus->isEmpty()) {
            $this->errors['cus_id'] = Message::get('BM090', implode(',', $cusIds));
            return false;
        }

        $cusIdValid = array_column($warehouseCus->toArray(), 'cus_id');
        $cusInvalid = array_diff($cusIds, $cusIdValid);

        // exists cusId invalid
        if ($cusInvalid) {
            $this->errors['cus_id'] = Message::get('BM090', implode(',', $cusInvalid));
            return false;
        }

        $isCusInWarehouse = $this->checkCusInUser([
            'cus_id' => $cusIds,
            'user_id' => $data['cycle_assign_to']
        ]);

        if (! $isCusInWarehouse) {
            $this->errors['cus_id'] = Message::get('BM076');
            return false;
        }

        return true;
    }

    /**
     * @param $skus
     * @param $whsId
     */
    private function checkValidSKUInput($data)
    {
        $skus = isset($data['cycle_detail']) ? $data['cycle_detail']: NULL;
        $whsId = isset($data['whs_id']) ? $data['whs_id']: NULL;

        if (! ($skus && $whsId)) {
            $this->errors['sku'] = Message::get('BM081', 'Data');
            return false;
        }

        $skus = array_unique($skus);

        $data = ItemModel::getItemBySKUs($skus);

        //check sku valid on database
        $skuValids = array_column($data->toArray(), 'sku');
        $skuInValid = array_diff($skus, $skuValids);

        if ($skuInValid) {
            $this->errors['sku'] = Message::get('BM089', 'SKU', implode(',', $skuInValid));
            return false;
        }

        //get SKU valid in warehouse
        $items = ItemModel::getInfoItemBySkus($skus);
        $skuValids = ItemModel::getSKUInWarehouse($whsId, $items);
        $skuNotInWarehouse = array_diff($skus, array_keys($skuValids));

        if ($skuNotInWarehouse ) {
            $this->errors['sku'] = Message::get('BM091', implode(',', $skuNotInWarehouse));
            return false;
        }

        return true;
    }

    /**
     * @param $locIds
     * @param $whsId
     */
    private function checkValidLocIdsInput($data)
    {
        $locCodes = isset($data['cycle_detail']) ? $data['cycle_detail']: NULL;
        $whsId = isset($data['whs_id']) ? $data['whs_id']: NULL;

        if (! ($locCodes && $whsId)) {
            $this->errors['locId'] = Message::get('BM081', 'Locations');
            return false;
        }

        if (count($locCodes) != 2) {
            $this->errors['locId'] = Message::get('BM092');
            return false;
        }

        $locFrom = $locCodes[0] ;
        $locTo = $locCodes[1];

        if ($locFrom > $locTo) {
            $this->errors['locId'] = Message::get('BM093');
            return false;
        }

        $locInfos = LocationModel::getLocationByCodes($locCodes)->toArray();
        $locCodeValid = array_column($locInfos, 'loc_code');

        $locInValid = array_diff($locCodes, $locCodeValid);

        if ($locInValid) {

            $this->errors['locId'] = Message::get('BM094', implode(', ', array_unique($locInValid)));
            return false;
        }

        $locRange = LocationModel::getRangeLocationByFromTo($whsId, $locFrom, $locTo)->toArray();

        if (! $locRange) {
            $this->errors['locId'] = 'No location from '. $locFrom. ' to '. $locTo;
            return false;
        }
        
        $this->locIdsRange = array_column($locRange, 'loc_id');

        return true;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function createCycleDtls($cycleHdr, $data)
    {
        if (! $data) {
            return false;
        }

        $validator = new CycleDtlValidator();

        $cycleDtl = new CycleDtl();

        $countBy = strtoupper($cycleHdr->count_by);

        foreach ($data as $countItem) {
            $countItem['cycle_hdr_id'] = $cycleHdr->cycle_hdr_id;
            $countItem['pack'] = $countItem['pack_size'];
            $countItem['remain'] = $countItem['piece_remain'];
            $countItem['sys_qty'] = $countBy == self::COUNT_BY_EACH ?
                $countItem['sys_pieces_qty'] : $countItem['sys_carton_qty'];
            $countItem['cycle_dtl_sts'] = CycleDtlModel::STATUS_CYCLEDTL_NEW;
            $countItem['sts'] = self::STATUS_INSERT;

            $validator->validate($countItem);
            $cycleDtl->create($countItem);
        }

        return true;
    }

    /**
     * @param $cycleID
     * @param bool $cycleDtl
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getCycleInfo($cycleID, $cycleDtl = false)
    {
        $cycleHdr = CycleHdr::with('assigneeUser')->with('createdBy');
        if ($cycleDtl) {
            $cycleHdr->with('cycleDtl');
        }
        $data = $cycleHdr->where('cycle_hdr_id', $cycleID)->first();
        if (! $data) {
            return false;
        }
        $data->assignee_to_name = $data->assigneeUser->first_name . ' ' . $data->assigneeUser->last_name;
        $data->assignee_by_name = $data->createdBy->first_name . ' ' . $data->createdBy->last_name;
        $data->cycle_has_size_color_name = $data->has_color_size == 0 ? 'No' : 'Yes';
        $data->cycle_sts_name = CycleHdrModel::$arrCycleStatus[$data->cycle_sts];
        $data->cycle_type_name = CycleHdrModel::$arrCycleType[$data->cycle_type];
        $data->cycle_method_name = CycleHdrModel::$arrCycleMethod[$data->cycle_method];

        return $data;
    }

    /**
     * Get type list
     */
    public function getCycleTypes()
    {
        $types = self::$arrCycleType;

        $data = [];

        foreach ($types as $key => $val) {
            $data[] = [
                'cycle_type' => $key,
                'cycle_type_name' => $val,
                'cycle_type_des' => NULL
            ];
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getCycleStatus()
    {
        $statuses = self::$arrCycleStatus;

        $data = [];

        foreach ($statuses as $key => $val) {
            $data[] = [
                'cycle_sts' => $key,
                'cycle_sts_name' => $val,
                'cycle_sts_des' => NULL
            ];
        }

        return $data;
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    public function getCycleHdrByIds($ids)
    {
        $result = [];

        if (! $ids) {
            return $result;
        }

        $result = CycleHdr::whereIn('cycle_hdr_id', $ids)
            ->get();

        return $result;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public static function getCycleHdr($id)
    {
        return CycleHdr::where('cycle_hdr_id', $id)
            ->first();
    }

    /**
     * @param $cycleHdrId
     * @param $status
     *
     * @return bool
     */
    public static function updateCycleHdrStatus($cycleHdrId, $status)
    {
        if (! $cycleHdrId) {
            return false;
        }

        return CycleHdr::where('cycle_hdr_id', $cycleHdrId)
                ->update([
                    'cycle_sts' => $status
                ]);
    }

    public function getCycleStatusByCycleId($cycleHdrs, $status = null)
    {
        $isAssigned = false;

        foreach ($cycleHdrs as $cycleHdr) {
            if ($cycleHdr->cycle_sts != CycleHdrModel::STATUS_CYCLE_ASSIGNED) {
                $isAssigned = $cycleHdr->cycle_name;
            }

        }
        return $isAssigned;
    }

    /**
     * @param $messages
     */
    public function setErrors($messages)
    {
        $this->errors = $messages;
    }

    /**
     * @param null $key
     * @return array|mixed
     */
    public function getErrors($key = null){

        return isset($this->errors[$key]) ? $this->errors[$key] : $this->errors;
    }

    /**
     * @param $cycleHdrId
     */
    public function updateCycelHdrAfterProcessed($cycleHdrId)
    {
        //check accept status for cycleDtl
        $query = "
            SELECT cycle_dtl_id FROM cycle_dtl 
            WHERE cycle_hdr_id = $cycleHdrId AND deleted = 0 AND cycle_dtl_sts <> 'AC' 
            LIMIT 1
        ";
        $rs = $this->naturalExec($query);

        if (empty($rs)) {
            return self::updateCycleHdrStatus($cycleHdrId, CycleHdrModel::STATUS_CYCLE_COMPLETED);
        }

        return false;
    }

    /**
     * @param $whsId
     * @param $items
     *
     * @return int
     */
    public function checkCartonLockByItems($whsId, $items)
    {
        $result = 0;

        if (! ($items && $whsId)) {
            return $result;
        }

        $result = DB::table('cartons')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('location.deleted', 0)
            ->where('cartons.ctn_sts', '=', LockModel::STATUS_LOCK)
            ->whereIn('cartons.item_id', $items)
            ->count();

        return $result;
    }

    /**
     * @param $data
     */
    public function checkCusInUser($data)
    {
        $userId = $data['user_id'];
        $cusId = $data['cus_id'];

        $result = DB::table('cus_in_user')
            ->where('cus_id', $cusId)
            ->where('user_id', $userId)
            ->count();

        return $result > 0 ? true : false;
    }

    /**
     * @param $whsId
     */
    public function getUserInWarehouse($whsId)
    {
        $userId = Data::getCurrentUserId();
        return DB::table('users')
            ->select([
                "users.user_id",
                "users.first_name",
                "users.last_name",
                "users.email",
                "users.created_at",
                "users.updated_at",
                "emp_code",
                "status"
            ])
            ->join('user_whs', "users.user_id", "=", "user_whs.user_id")
            ->where("user_whs.whs_id", $whsId)
            ->where("user_whs.user_id", '!=', $userId)
            ->orderBy('users.first_name')
            ->get();
    }

    /**
     * @param $cycleDtls
     */
    private function getItemLockByAnotherCycle($whsId, $cycleDtls)
    {
        $result = CycleDtl::select(
                'cycle_dtl.sku',
                'cycle_hdr.cycle_hdr_id',
                'cycle_hdr.cycle_name'
            )
            ->join('cycle_hdr', 'cycle_hdr.cycle_hdr_id', '=', 'cycle_dtl.cycle_hdr_id')
            ->whereNotIn('cycle_hdr.cycle_sts', [
                self::STATUS_CYCLE_COMPLETED,
                self::STATUS_CYCLE_DELETED
            ])
            ->whereIn('cycle_dtl.item_id', $cycleDtls)
            ->where('cycle_hdr.whs_id', $whsId)
            ->distinct()
            ->get()
            ->toArray();

        return $result;
    }

    /**
     * @param $cycleHdrs
     * @param $currentUserId
     */
    public function getCycleAssigToUserId($cycleHdrs, $userId)
    {
        $result = [];

        foreach($cycleHdrs as $cycleHdr) {
            if ($cycleHdr->assignee_id == $userId) {
                $result[] = $cycleHdr->cycle_hdr_id;
            }
        }

        return $result;
    }

    /**
     * @param $cycleHdrID
     * @param $locIds
     *
     * @return array
     */
    public function getSysLocIdCanUnLock($cycleHdrID, $locIds)
    {
        $result = [];

        if (! ($cycleHdrID && $locIds)) {
            return $result;
        }

        $result  = CycleDtl::select('sys_loc_id')
            ->whereIn('sys_loc_id', $locIds)
            ->where('cycle_hdr_id', $cycleHdrID)
            ->where('cycle_dtl_sts', '<>', CycleDtlModel::STATUS_CYCLEDTL_ACCEPTED)
            ->distinct()
            ->get()
            ->toArray();

        $locIdsLock = $result ? array_column($result, 'sys_loc_id') : [];

        $locCanUnLock = array_diff($locIds, $locIdsLock);

        return $locCanUnLock;
    }
}
