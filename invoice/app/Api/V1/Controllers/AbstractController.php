<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthorizationService;
use App\Api\V1\Models\BaseService;
use Dingo\Api\Routing\Helpers;
use Laravel\Lumen\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;
use Illuminate\Http\Response as LumenResponse;

abstract class AbstractController extends Controller
{
    use Helpers;
    protected $userId;

    public function __construct(IRequest $request, BaseService $service = null)
    {
        $token = $request->getHeader('Authorization');
        if(empty($token[0])) {
            throw new UnauthorizedHttpException('Unauthorized');
        }
        $result = $service->getUserIdByToken($token[0]);

        if (! $result) {
            throw new UnauthorizedHttpException('Unauthorized');
        }

        $this->userId = $result;
    }
    /**
     * Check permission for every action request
     *
     * @param IRequest $request
     * @param $permission
     * @param array $data
     */
    public function checkPermission(IRequest $request, $permission, $data = [])
    {
        $token = $request->getHeader('Authorization');
        if(empty($token[0])) {
            throw new UnauthorizedHttpException('Token is empty');
        }
        $client = new AuthorizationService($request);
        $client->check($permission, $data);
    }

    /**
     * @param IResponse $response
     * @return LumenResponse
     */
    protected function convertResponse(IResponse $response)
    {
        $luResponse = new LumenResponse($response->getBody(), $response->getStatusCode(), []);
        return $luResponse;
    }

    /*
    ****************************************************************************
    */

    public function index(IRequest $request, $data=[])
    {
        $model = array_get($data, 'model', NULL);
        $method = array_get($data, 'method', NULL);
        $with = array_get($data, 'with', []);

        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', PAGING_LIMIT);

        $searchModel = $model ?: $this->model;

        try {

            $results = $method && method_exists($searchModel, $method) ?
                $searchModel->$method($input, $with, $limit) :
                $searchModel->search($input, $with, $limit);

            return $results ?
                $this->response->paginator($results, $this->transformer) :
                $this->response->errorBadRequest(Message::get('VR003'));

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /*
    ****************************************************************************
    */

}
