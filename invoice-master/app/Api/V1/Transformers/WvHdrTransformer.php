<?php

namespace App\Api\V1\Transformers;

use App\WavepickHdr;
use League\Fractal\TransformerAbstract;

class WvHdrTransformer extends TransformerAbstract
{

    /**
     * ps:Show GoodsReceipt
     *
     * @param WavepickHdr $wvHdr
     *
     * @return array
     */
    public function transform(WavepickHdr $wvHdr)
    {
        return [
            'wv_id' => $wvHdr->wv_id,
            'wv_num' => $wvHdr->wv_num,
        ];
    }
}
