<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 06-Jan-17
 * Time: 15:20
 */

namespace App\Api\V1\Traits;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PutBackModel;
use App\Api\V1\Models\ReturnDtlModel;
use App\Api\V1\Models\ReturnHdrModel;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CustomerZone;
use Seldat\Wms2\Models\Pallet;
use mPDF;
use App\Api\V1\Validators\PutterValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V1\Transformers\ReturnHdrTransformer;
use App\Api\V1\Models\UserModel;
use Seldat\Wms2\Utils\Helper;
use Seldat\Wms2\Utils\Message;

trait ReturnControllerTrait
{
    /**
     * @var ReturnHdrModel
     */
    protected $returnHdrModel;

    /**
     * @var ReturnDtlModel
     */
    protected $returnDtlModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var PutBackModel
     */
    protected $putbackModel;

    /**
     * @var InventorySummaryModel
     */
    protected $invtSmrModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @param $returnHdr
     * @param $detail
     *
     * @return mixed
     */
    private function createReturnDtl($returnHdr, $detail)
    {
        $odrCtn = DB::table('odr_cartons')
            ->select([
                DB::raw('sum(piece_qty) as piece_qty_ttl'),
                'sku',
                'size',
                'color',
                'lot',
                'pack',
                'item_id',
                'uom_id',
                'uom_code',
                'uom_name'
            ])
            ->where('odr_dtl_id', $detail['odr_dtl_id'])
            ->groupBy('odr_dtl_id')
            ->first();

        $paramReturnDtl = [
            'return_id'      => $returnHdr->return_id,
            'return_num'     => $returnHdr->return_num,
            'odr_hdr_num'    => $returnHdr->odr_hdr_num,
            'odr_hdr_id'     => $returnHdr->odr_hdr_id,
            'odr_dtl_id'     => $detail['odr_dtl_id'],
            'ctn_ttl'        => $detail['original_ctns'] + $detail['new_ctns'],
            'original_ctns'  => $detail['original_ctns'],
            'new_ctns'       => $detail['new_ctns'],
            'return_dtl_sts' => 'NW',
            'piece_qty'      => array_get($odrCtn, 'piece_qty_ttl', null),
            'sku'            => array_get($odrCtn, 'sku', null),
            'size'           => array_get($odrCtn, 'size', null),
            'color'          => array_get($odrCtn, 'color', null),
            'lot'            => array_get($odrCtn, 'lot', 'NA'),
            'pack'           => array_get($odrCtn, 'pack'),
            'item_id'        => array_get($odrCtn, 'item_id'),
            'uom_id'         => array_get($odrCtn, 'uom_id'),
            'uom_code'       => array_get($odrCtn, 'uom_code'),
            'uom_name'       => array_get($odrCtn, 'uom_name'),
            'sts'            => 'i',
        ];

        $this->returnDtlModel->refreshModel();

        return $this->returnDtlModel->create($paramReturnDtl);
    }

    /**
     * @param $returnHdr
     * @param $ctnTtl
     * @param $returnDtl
     * @param $item
     *
     * @return mixed
     */
    private function createPallet($returnHdr, $ctnTtl, $returnDtl, $item)
    {
        $maxPallet = (new Pallet())->where([
            'deleted'    => 0,
            'deleted_at' => 915148800
        ])->orderBy('plt_id', 'desc')->first();

        $aHdrNum = explode("-", $maxPallet->plt_num);
        $index = (int)end($aHdrNum);
        $aHdrNum[0] = "LPN";
        $temp = array_keys($aHdrNum);
        $key = end($temp);
        $aHdrNum[$key] = str_pad(++$index, 3, "0", STR_PAD_LEFT);

        $pltNum = implode("-", $aHdrNum);

        $palletParram = [
            'cus_id'    => object_get($returnHdr, 'odrHdr.cus_id'),
            'whs_id'    => object_get($returnHdr, 'odrHdr.whs_id'),
            'plt_num'   => $pltNum,
            'ctn_ttl'   => $ctnTtl,
            'return_id' => $returnHdr->return_id,
        ];

        $this->palletModel->refreshModel();

        $pallet = $this->palletModel->create($palletParram);

        $this->createPutBack($returnHdr, $returnDtl, $pallet);

        return $pallet;
    }

    private function updatePallet($pallet)
    {
        $palletParram = [
            'plt_id'   => $pallet['plt_id'],
            'loc_id'   => $pallet['loc_id'],
            'loc_code' => $pallet['loc_code']
        ];

        $this->palletModel->refreshModel();
        $this->palletModel->update($palletParram);
    }

    /**
     * @param $returnHdr
     * @param $detail
     * @param $input
     * @param $returnDtl
     * @param $item
     *
     * @return array
     */
    private function createCartonAndAssign($returnHdr, $detail, $input, $returnDtl, $item)
    {
        $ctnTtl = $detail['original_ctns'] + $detail['new_ctns'];
        $pltTtl = $detail['plt_ttl'];
        $ctnPerPlt = ceil($ctnTtl / $pltTtl);

        // Create Pallet
        $pallets = [];
        $pltIds = [];

        while ($ctnTtl >= $ctnPerPlt) {
            $pallet = $this->createPallet($returnHdr, $ctnPerPlt, $returnDtl, $item)->toArray();
            $pallets[] = $pallet;
            $pltIds[] = $pallet['plt_id'];
            $ctnTtl -= $ctnPerPlt;
        }

        if ($ctnTtl > 0) {
            // Add Last Pallet
            $pallet = $this->createPallet($returnHdr, $ctnTtl, $returnDtl, $item)->toArray();
            $pallets[] = $pallet;
            $pltIds[] = $pallet['plt_id'];
        }

        $this->orderCartonModel->refreshModel();
        $orderCartons = $this->orderCartonModel->loadCartonToAssign($detail['odr_dtl_id'], ['carton']);

        // Assign Original Carton
        $palletAssigned = $pallets;

        $this->assignOriginalCarton($returnHdr, $orderCartons, $palletAssigned, $input['item_id'],
            $detail['original_ctns']);

        // Update Old Carton Stauts of New Carton
        $cartons = $orderCartons->toArray();
        if (!empty($cartons)) {
            $ctnIds = array_pluck($cartons, 'ctn_id');
            DB::table('cartons')
                ->whereIn('ctn_id', $ctnIds)
                ->where('ctn_sts', 'PD')
                ->update(['ctn_sts' => 'PB']);
        }
        $cartons = array_pluck($cartons, 'carton');

        // Create new Carton
        if (!empty($palletAssigned)) {
            $this->createNewCarton($returnHdr, $palletAssigned, $input, $detail, $cartons);
        }

        return $pltIds;
    }

    /**
     * @param $returnHdr
     * @param $orderCartons
     * @param $palletAssigned
     * @param $itemId
     * @param $oriTtl
     */
    private function assignOriginalCarton($returnHdr, &$orderCartons, &$palletAssigned, $itemId, $oriTtl)
    {
        $n = 0;
        foreach ($palletAssigned as $key => $pallet) {
            $ctnTtl = $pallet['ctn_ttl'];
            $palletAssigned[$key]['ctn_ttl'] = $ctnTtl;
            $palletAssigned[$key]['ctn_cur'] = 0;
            foreach ($orderCartons as $kOdr => $orderCarton) {
                $n++;
                if ($oriTtl < $n) {
                    break;
                }
                $ctnTtl--;
                $this->cartonModel->refreshModel();
                $this->cartonModel->update([
                    'ctn_id'    => $orderCarton->ctn_id,
                    'plt_id'    => $pallet['plt_id'],
                    'item_id'   => $itemId,
                    'ctn_sts'   => 'AC',
                    'return_id' => $returnHdr->return_id
                ]);

                $palletAssigned[$key]['ctn_cur'] += 1;

                unset($orderCartons[$kOdr]);

                if ($ctnTtl <= 0) {
                    unset($palletAssigned[$key]);
                    break;
                }
            }
            if ($n >= $oriTtl) {
                break;
            }
        }
    }

    /**
     * @param $returnHdr
     * @param $palletAssigned
     * @param $input
     * @param $detail
     * @param $cartons
     */
    private function createNewCarton($returnHdr, $palletAssigned, $input, $detail, $cartons)
    {
        $uom = $this->itemModel->getFirstBy('item_id', $input['item_id'], ['systemUom']);
        $newCreating = $detail['new_ctns'];

        foreach ($palletAssigned as $key => $pallet) {
            $needToCreate = $pallet['ctn_ttl'] - array_get($pallet, 'ctn_cur', 0);
            $pieceTtl = $input['pack_size'];
            while ($needToCreate > 0) {
                $maxCarton = (new Carton())->where([
                    'deleted'    => 0,
                    'deleted_at' => 915148800
                ])->orderBy('ctn_id', 'desc')->first();

                $aNum = explode("-", $maxCarton->ctn_num);
                $aTemp = array_keys($aNum);
                $end = end($aTemp);
                $index = (int)$aNum[$end];
                $aNum[0] = "CTB";
                $aNum[$end] = str_pad(++$index, 5, "0", STR_PAD_LEFT);
                $ctnNum = implode("-", $aNum);

                if ($newCreating == 1) {
                    $pieceTtl = $input['piece_qty'] - $input['pack_size'] *
                        ($detail['original_ctns'] + $detail['new_ctns'] - 1);
                }

                $this->cartonModel->refreshModel();
                $index = $detail['new_ctns'] - $needToCreate;
                $length = array_get($cartons, "$index.length", 0);
                $width = array_get($cartons, "$index.width", 0);
                $height = array_get($cartons, "$index.height", 0);
                $volume = Helper::calculateVolume($length, $width, $height);
                $cube = Helper::calculateCube($length, $width, $height);
                $grDate = array_get($cartons, "$index.gr_dt", null);
                $ctnPackSize = array_get($cartons, "$index.ctn_pack_size", null);
                $pieceRemain = array_get($cartons, "$index.piece_remain", null);
                if ($ctnPackSize != $pieceRemain) {
                    $grDate = time();
                }
                $this->cartonModel->create([
                    'plt_id'        => $pallet['plt_id'],
                    'item_id'       => $input['item_id'],
                    'cus_id'        => object_get($returnHdr, 'odrHdr.cus_id'),
                    'whs_id'        => object_get($returnHdr, 'odrHdr.whs_id'),
                    'ctn_num'       => $ctnNum,
                    'ctn_sts'       => 'AC',
                    'ctn_uom_id'    => object_get($uom, 'systemUom.sys_uom_id', 0),
                    'uom_name'      => object_get($uom, 'systemUom.sys_uom_name', ''),
                    'uom_code'      => object_get($uom, 'systemUom.sys_uom_code', ''),
                    'return_id'     => $returnHdr->return_id,
                    'piece_remain'  => $pieceTtl,
                    'piece_ttl'     => $pieceTtl,
                    'sku'           => array_get($cartons, "$index.sku", null),
                    'size'          => array_get($cartons, "$index.size", null),
                    'color'         => array_get($cartons, "$index.color", null),
                    'lot'           => array_get($cartons, "$index.lot", "NA"),
                    'upc'           => array_get($cartons, "$index.upc", null),
                    'length'        => $length,
                    'width'         => $width,
                    'height'        => $height,
                    'weight'        => array_get($cartons, "$index.weight", null),
                    'ctn_pack_size' => array_get($cartons, "$index.ctn_pack_size", null),
                    'is_damaged'    => 0,
                    'cube'          => $cube,
                    'volume'        => $volume,
                    'gr_dt'         => $grDate,
                ]);
                $needToCreate--;
                $newCreating--;
            }
        }
    }

    /**
     * @param $data
     */
    private function updateCarton(&$data)
    {
        $carton = $this->cartonModel->getFirstWhere([
            'plt_id' => $data['plt_id']
        ]);
        $data['whs_id'] = $carton['whs_id'];
        $data['cus_id'] = $carton['cus_id'];

        $this->cartonModel->refreshModel();
        $this->cartonModel->updateWhere([
            'loc_id'        => $data['loc_id'],
            'loc_code'      => $data['loc_code'],
            'loc_name'      => $data['loc_code'],
            'ctn_sts'       => 'AC',
            'loc_type_code' => "RAC",
        ], ['plt_id' => $data['plt_id']]);

    }

    /**
     * @param $cusId
     * @param $whsId
     *
     * @return mixed
     */
    private function suggestLocation($cusId, $whsId)
    {
        $allExceptLocId = (new Pallet())
            ->select('loc_id')
            ->where('loc_id', '>', 0)
            ->where(['deleted' => 0, 'deleted_at' => 915148800])
            ->groupBy('loc_id')->get()
            ->toArray();
        $exceptLocIds = array_pluck($allExceptLocId, 'loc_id');

        $zoneIds = (new CustomerZone())
            ->select('zone_id')
            ->where('cus_id', $cusId)
            ->get()->toArray();

        $inZoneIds = array_pluck($zoneIds, 'zone_id');

        $dataSuggest = $this->locationModel->getSuggestData($inZoneIds, $exceptLocIds, $whsId)->toArray();

        return $dataSuggest;
    }

    /**
     * @param $pallet
     * @param $info
     *
     * @throws \MpdfException
     */
    private function printPDF($pallet, $info)
    {
        $pdf = new Mpdf();
        $html = (string)view('PalletAssignPrint', [
            'dataSuggest' => $pallet,
            'info'        => $info
        ]);

        $pdf->WriteHTML($html);
        $pdf->Output("PalletAssign.pdf", "D");

        $dir = storage_path("PutBack/{$info['whs_id']}/{$info['return_id']}");
        $pdf->Output("$dir/{$info['return_num']}.pdf", 'F');
        $pdf->Output("{$info['return_num']}.pdf", 'D');
    }

    /**
     * @param $returnHdr
     * @param $returnDtl
     * @param $pallet
     *
     * @return mixed
     */
    private function createPutBack($returnHdr, $returnDtl, $pallet)
    {
        $odrCarton = $this->orderCartonModel->getFirstWhere(['odr_dtl_id' => $returnDtl->odr_dtl_id]);
        $ctnTtl = array_get($pallet, 'ctn_ttl', 0);
        $putBackParam = [
            'return_id'     => object_get($returnHdr, 'return_id'),
            'return_num'    => object_get($returnHdr, 'return_num'),
            'return_dtl_id' => $returnDtl->return_dtl_id,
            'item_id'       => object_get($odrCarton, 'item_id'),
            'sku'           => object_get($odrCarton, 'sku'),
            'size'          => object_get($odrCarton, 'size'),
            'color'         => object_get($odrCarton, 'color'),
            'lot'           => object_get($odrCarton, 'lot', "NA"),
            'pallet_id'     => array_get($pallet, 'plt_id'),
            'is_new'        => 1,
            'sts'           => "i",
            'pb_sts'        => "NW",
            'ctn_ttl'       => $ctnTtl,
            'pack_size'     => null,
        ];

        $this->putbackModel->refreshModel();

        return $this->putbackModel->create($putBackParam);
    }

    /**
     * @param $data
     */
    private function updateInvtSmr($data)
    {
        $this->invtSmrModel->refreshModel();
        $invtSmr = $this->invtSmrModel->getFirstWhere([
            'item_id' => $data['item_id'],
            'whs_id'  => $data['whs_id'],
            'cus_id'  => $data['cus_id'],
            'lot'     => $data['lot']
        ]);

        if (!empty($invtSmr)) {
            // Update
            $this->invtSmrModel->refreshModel();
            $this->invtSmrModel->updateWhere([
                'picked_qty' => $invtSmr->picked_qty - $data['return_qty'],
                'avail'      => $invtSmr->avail + $data['return_qty']
            ], [
                'item_id' => $data['item_id'],
                'whs_id'  => $data['whs_id'],
                'cus_id'  => $data['cus_id'],
                'lot'     => $data['lot']
            ]);
        }
    }

    /**
     * @param Request $request
     * @param PutterValidator $putterValidator
     * @param ReturnHdrTransformer $returnHdrTransformer
     * @param UserModel $userModel
     *
     * @return mixed
     */
    public function updatePutter(
        Request $request,
        PutterValidator $putterValidator,
        ReturnHdrTransformer $returnHdrTransformer,
        UserModel $userModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $putterValidator->validate($input);

        try {
            DB::beginTransaction();
            // Load User
            $userInfo = $userModel->byId($input['user_id']);
            if (empty($userInfo)) {
                throw new \Exception(Message::get("BM017", "User"));
            }

            //check user status
            $putterValidator->checkUserStatus(object_get($userInfo, 'status', null));

            // Check Exist Return Header
            if (!is_array($input['return_id'])) {
                $input['return_id'] = [$input['return_id']];
            }

            $returnHdrs = DB::table('return_hdr')->whereIn('return_id', $input['return_id'])->get();
            if (empty($returnHdrs)) {
                throw new \Exception(Message::get("BM017", "Return Header"));
            }
            
            foreach ($returnHdrs as $returnHdr) {
                $this->orderCartonModel->refreshModel();
                $this->orderCartonModel->updateWhere([
                    'ctn_sts' => 'PT',
                    'sts'     => 'u'
                ], [
                    'odr_hdr_id' => $returnHdr['odr_hdr_id']
                ]);

                $this->returnHdrModel->updateWhere(
                    [
                        'putter' => $input['user_id']
                    ],
                    [
                        'return_id' => $returnHdr['return_id']
                    ]
                );
            }

            DB::commit();
            return [
                'data' => 'Assign Handler Successful.'
            ];
            
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}