<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerWarehouseModel;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Transformers\CustomerWarehouseDefaultTransformer;
use App\Api\V1\Transformers\CustomerWarehouseTransformer;
use App\Api\V1\Validators\SaveMassMetaValidator;
use App\Api\V1\Validators\CustomerWarehouseValidator;
use App\WarehouseQualifier;
use Dingo\Api\Facade\Route;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\CusInUserModel;
use Seldat\Wms2\Models\CustomerWarehouse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class CustomerWarehouseController extends AbstractController
{
    /**
     * @var $customerWarehouseModel
     */
    protected $customerWarehouseModel;
    /**
     * @var $customerWarehouseTransformer
     */
    protected $customerWarehouseTransformer;
    /**
     * @var $customerWarehouseValidator
     */
    protected $customerWarehouseValidator;
    /**
     * @var $customerWarehouseDefaultTransformer
     */
    protected $customerWarehouseDefaultTransformer;

    protected $warehouseModel;

    public function __construct()
    {
        parent::__construct();

        $this->customerWarehouseModel = new CustomerWarehouseModel();
        $this->customerWarehouseTransformer = new CustomerWarehouseTransformer();
        $this->customerWarehouseValidator = new CustomerWarehouseValidator();
        $this->customerWarehouseDefaultTransformer = new CustomerWarehouseDefaultTransformer();
        $this->warehouseModel = new WarehouseModel();
    }

    public function search($customerId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        $paginator = $this->customerWarehouseModel->search(['cus_id' => $customerId], [], array_get($input, 'limit'));

        return $this->response->paginator($paginator, $this->customerWarehouseTransformer);
    }

    public function store(
        $customerId,
        Request $request
    ) {
        // get input from request
        $input = $request->getParsedBody();

        $this->customerWarehouseValidator->validate($input);

        $params = [
            'cus_id' => $customerId,
            'whs_id' => $input['whs_id']
        ];

        try {
            if (($customer = $this->customerWarehouseModel->getFirstBy('whs_id', $params['whs_id']))
                && $customer->cus_id == $customerId
            ) {
                throw new \Exception(Message::get("BM006", "Warehouse"));
            }

            if ($warehouse = $this->customerWarehouseModel->create($params)) {
                return $this->response->item($warehouse, $this->customerWarehouseTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function destroy($warehouseId, $qualifier)
    {
        try {
            $this->customerWarehouseModel->deleteCustomerWarehouse($warehouseId, $qualifier);

            return $this->response->noContent();

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Load By any field.
     *
     * @param Request $request
     * @param CustomerWarehouseDefaultTransformer $customerWarehouseDefaultTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function loadBy(
        Request $request
    ) {
        $input = $request->getQueryParams();

        $params = [];
        $currentAction = array_get(Route::getCurrentRoute()->getAction(), 'as', '');
        if ($currentAction == 'cusWH') {
            $input['mode'] = 1;
        }

        if (!empty($input['whs_id'])) {
            $params['whs_id'] = is_array($input['whs_id']) ? $input['whs_id'] : [$input['whs_id']];
        }
        try {
            $meta = $this->customerWarehouseModel->searchCustomerByWarehouse(
                $input,
                ['customer', 'customer.customerAddress', 'customer.customerAddress.systemState'],
                array_get($input, 'limit'));

            return $this->response->paginator($meta, $this->customerWarehouseDefaultTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function listWhsNotInCus($cusId)
    {
        try {
            return [
                'data' => $this->warehouseModel->listWhsNotInCus($cusId)
            ];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function whsToCus(Request $request, $cusId)
    {
        // get input from request
        $input = $request->getParsedBody();
        $listWhs = $input['whs_ids'];
        $userId = Data::getCurrentUserId();
        $params = $dataUser = [];

        try {
            DB::beginTransaction();
            if ($listWhs) {
                foreach ($listWhs as $whsId) {
                    if ($this->customerWarehouseModel->getCustomerWarehouse($cusId, $whsId)) {
                        $this->response->errorBadRequest(Message::get("BM006", "Warehouse"));
                    }

                    $params[] = [
                        'cus_id'     => $cusId,
                        'whs_id'     => $whsId,
                        'deleted_at' => 915148800,
                        'created_at' => time(),
                        'updated_at' => time(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'deleted'    => 0
                    ];

                    // assign customer to user
                    // $listUser = DB::table('user_whs')
                    //     ->join('users', 'users.user_id', '=', 'user_whs.user_id')
                    //     ->selectRaw('users.user_id')
                    //     ->where('whs_id', $whsId)
                    //     ->get();

                    // if (count($listUser) > 0) {
                    // 
                    $existData = DB::table('cus_in_user')
                        ->where('cus_id', $cusId)
                        ->where('whs_id', $whsId)
                        ->where('user_id', $userId)
                        ->get();

                    if(count($existData) > 0) {
                        DB::table('cus_in_user')
                        ->where('cus_id', $cusId)
                        ->where('user_id', $userId)
                        ->update(['whs_id' => $whsId]);
                    }else{
                        $dataUser = [
                            'cus_id'  => $cusId,
                            'whs_id'  => $whsId,
                            'user_id' => $userId
                        ];

                        CusInUserModel::insert($dataUser);
                    }

                    // }
                }
                // assign customer to warehouse
                CustomerWarehouse::insert($params);
            }
            DB::commit();

            return [
                'data' => [
                    'message' => 'Add Warehouse to Customer Successfully.',
                ]
            ];

        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}