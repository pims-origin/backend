<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 29-Jul-16
 * Time: 3:05
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;

class NextCartonTransformer extends TransformerAbstract
{
    public function transform(Carton $carton)
    {
        return [
            'ctn_id'  => $carton->ctn_id,
            'ctn_num' => $carton->ctn_num

        ];
    }
}
