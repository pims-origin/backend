<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\WarehouseLocationsModel;
use App\Api\V1\Transformers\WarehouseLocationsTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class WarehouseLocationsController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new WarehouseLocationsModel();
        $this->transformer = new WarehouseLocationsTransformer();
    }

    /*
    ****************************************************************************
    */

}
