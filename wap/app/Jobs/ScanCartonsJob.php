<?php

namespace App\Jobs;

use App\Api\V2\Inbound\Models\VirtualCartonModel;
use App\Api\V2\Inbound\Models\VirtualCartonSumModel;
use App\Api\V2\Inbound\Models\AsnHdrModel;
use App\Api\V2\Inbound\Models\AsnDtlModel;
use App\Api\V2\Inbound\Models\EventTrackingModel;
use App\Api\V2\Inbound\Models\Log;
use Illuminate\Http\Response;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use App\MessageCode;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class ScanCartonsJob extends Job
{
    protected $whsId;
    protected $cusId;
    protected $asnHdrId;
    protected $asnDtlId;
    protected $arrCtnsRfid;
    protected $userId;

    protected $dataVtlCtnSum;
    protected $vtlCtnDataCommon;

    protected $vtlCtn;
    protected $vtlCtnSum;
    protected $asnHdrModel;

    public function __construct($input, $dataVtlCtnSum, $vtlCtnDataCommon)
    {
        $this->whsId       = array_get($input, 'whs_id', 0);
        $this->cusId       = array_get($input, 'cus_id', 0);
        $this->asnHdrId    = array_get($input, 'asn_hdr_id', 0);
        $this->asnDtlId    = array_get($input, 'asn_dtl_id', 0);
        $this->arrCtnsRfid = array_get($input, 'ctns_rfid', 0);
        $this->userId      = array_get($input, 'user_id', 0);

        $this->dataVtlCtnSum    = $dataVtlCtnSum;
        $this->vtlCtnDataCommon = $vtlCtnDataCommon;

        $this->vtlCtn      = new VirtualCartonModel();
        $this->vtlCtnSum   = new VirtualCartonSumModel();
        $this->asnHdrModel = new AsnHdrModel();
    }

    public function handle()
    {
        try {
            \DB::beginTransaction();

            $existVtlCtnSum = $this->vtlCtnSum->getFirstWhere(
                [
                    'asn_hdr_id' => $this->asnHdrId,
                    'asn_dtl_id' => $this->asnDtlId,
                ]
            );
            $insertVtlCtnSumId = null;

            if (empty($existVtlCtnSum)) {
                //insert virtual carton summary
                $insertVtlCtnSumId = $this->vtlCtnSum->create($this->dataVtlCtnSum)->vtl_ctn_sum_id;
            } else {
                $insertVtlCtnSumId = object_get($existVtlCtnSum, 'vtl_ctn_sum_id', null);
            }

            $timeStamp     = time();
            $dataVtlCtnDtl = [];
            foreach ($this->arrCtnsRfid as $key => $ctnsRfid) {
                $this->vtlCtnDataCommon['vtl_ctn_sum_id'] = $insertVtlCtnSumId;
                $this->vtlCtnDataCommon['ctn_rfid']       = $ctnsRfid;
                $this->vtlCtnDataCommon['created_at']     = $timeStamp;
                $this->vtlCtnDataCommon['updated_at']     = $timeStamp;
                $this->vtlCtnDataCommon['created_by']     = $this->userId;
                $this->vtlCtnDataCommon['updated_by']     = $this->userId;
                $this->vtlCtnDataCommon['deleted_at']     = 915148800;
                $this->vtlCtnDataCommon['deleted']        = 0;

                //init data array
                $dataVtlCtnDtl[] = $this->vtlCtnDataCommon;
            }

            //insert virtual carton
            $insertVtlCtn = DB::table('vtl_ctn')->insert($dataVtlCtnDtl);

            if ($insertVtlCtn) {
                //get asn hdr num by asn hdr id for EVT
                $asnHdrObj = $this->asnHdrModel->getASNHdrById($this->asnHdrId);
                //check ANS Status is Receiving or not, if not go to update ASN Status
                if (!empty($asnHdrObj) && $asnHdrObj->asn_sts != Status::getByKey('ASN_STATUS', 'RECEIVING')) {
                    //update ASN detail status
                    $this->updateASNReceiving($this->asnHdrId);

                    //params for ASN event tracking receiving
                    $evtASNReceiving = [
                        'whs_id'    => $this->whsId,
                        'cus_id'    => $this->cusId,
                        'owner'     => $asnHdrObj->asn_hdr_num,
                        'evt_code'  => Status::getByKey('EVENT', 'ASN-RECEIVING'),
                        'trans_num' => $asnHdrObj->asn_hdr_num,
                        'info'      => sprintf(Status::getByKey('EVENT-INFO', 'ASN-RECEIVING'),
                            $asnHdrObj->asn_hdr_num),
                    ];

                    //call save event tracking for ASN receiving
                    $this->eventTracking($evtASNReceiving);
                }

                $this->updateASNDtlReceiving($this->asnDtlId);

                \DB::commit();
            }
        } catch (\PDOException $e) {
            DB::rollBack();
            $msg = MessageCode::get('WAP002');
            $data = [
                'data'    => null,
                'message' => $msg,
                'code'    => 'WAP002',
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 200, null);
        }
    }

    /**
     * @param $asnHdrId
     */
    private function updateASNReceiving($asnHdrId)
    {
        $this->asnHdrModel->updateWhere(
            [
                'asn_sts' => Status::getByKey('ASN_STATUS', 'RECEIVING')
            ],
            ['asn_hdr_id' => $asnHdrId]
        );
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    private function eventTracking($params)
    {
        $eventTrackingModel = new EventTrackingModel();

        $eventTrackingModel->refreshModel();

        return $eventTrackingModel->create([
            'whs_id'    => $params['whs_id'],
            'cus_id'    => $params['cus_id'],
            'owner'     => $params['owner'],
            'evt_code'  => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info'      => $params['info'],
        ]);
    }

    /**
     * @param $ansDtlId
     *
     * @return mixed
     */
    private function updateASNDtlReceiving($ansDtlId)
    {
        $status = Status::getByKey('ASN_DTL_STS', 'RECEIVING');

        return $this->updateASNDtl($ansDtlId, $status);
    }

    /**
     * @param $ansDtlId
     * @param $status
     *
     * @return mixed
     */
    private function updateASNDtl($ansDtlId, $status)
    {
        return (new AsnDtlModel())->updateWhere(
            [
                'asn_dtl_sts' => $status
            ],
            [
                'asn_dtl_id' => $ansDtlId
            ]
        );
    }
}