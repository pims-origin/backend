<?php

namespace App\Api\V1\Validators;

class MakePaymentValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'date' => 'required',
            'type' => 'required',
            'reference' => 'required',
            'action' => 'required',
            'invoice' => 'required',
            'userID' => 'required'
        ];
    }
}
