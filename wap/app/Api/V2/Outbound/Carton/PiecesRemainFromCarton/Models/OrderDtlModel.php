<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09/10/2018
 * Time: 2:17 PM
 */

namespace App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Models;

use App\MessageCode;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class OrderDtlModel extends AbstractModel
{
    /**
     * WavepickDtlModel constructor.
     * @param OrderDtl |null $model
     */
    public function __construct(OrderDtl $model = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $this->model = ($model) ?: new OrderDtl();
    }

}