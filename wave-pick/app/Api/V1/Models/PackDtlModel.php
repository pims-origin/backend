<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\PackDtl;
use Seldat\Wms2\Models\PackHdr;

class PackDtlModel extends AbstractModel
{

    protected $model;

    /**
     * PackDtlModel constructor.
     *
     * @param PackDtl|null $model
     */
    public function __construct(PackDtl $model = null)
    {
        $this->model = ($model) ?: new PackDtl();
    }
}
