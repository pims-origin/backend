<div>Hello,</div>
<div>An item was {{ $data['is_edit'] ? 'edited' : 'created' }} in Item Master Details:</div>
<ul>
	<li>Special Handling: {{ $data['spc_hdl_code'] }}</li>
	<li>SKU: {{ $data['sku'] }}</li>
	<li>Pack Size: {{ $data['pack'] }}</li>
	<li>Description: {{ $data['description'] }}</li>
</ul>
<div>
	You may approve or reject this description for the newly created sku.<br/>
	Follow: <a href="{{ $data['link'] }}">{{ $data['link'] }}</a>
</div>