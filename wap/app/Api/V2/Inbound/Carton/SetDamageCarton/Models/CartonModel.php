<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Inbound\Carton\SetDamageCarton\Models;

use App\MessageCode;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Symfony\Component\VarDumper\Cloner\Data;

class CartonModel extends AbstractModel
{
    /**
     * VirtualCartonModel constructor.
     *
     * @param VirtualCarton|null $model
     */
    public function __construct(Carton $model = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $this->model = ($model) ?: new Carton();
    }

}
