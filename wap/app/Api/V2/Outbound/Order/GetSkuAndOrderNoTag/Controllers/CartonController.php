<?php

namespace App\Api\V2\Outbound\Order\GetSkuAndOrderNoTag\Controllers;

use App\Api\V2\Outbound\Order\GetSkuAndOrderNoTag\Models\CartonModel;
use App\Api\V2\Outbound\Order\GetSkuAndOrderNoTag\Models\OrderHdrModel;
use App\Api\V2\Outbound\Order\GetSkuAndOrderNoTag\Models\OrderCartonModel;
use App\Api\V2\Outbound\Order\GetSkuAndOrderNoTag\Models\OutPalletModel;
use App\Api\V2\Outbound\Order\GetSkuAndOrderNoTag\Models\PackHdrModel;
use App\libraries\RFIDValidate;

use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class CartonController extends AbstractController
{

    protected $cartonModel;
    protected $orderHdrModel;
    protected $orderCartonModel;
    protected $outPalletModel;
    protected $packHdrModel;

    public function __construct()
    {
        $this->cartonModel      = new CartonModel();
        $this->orderHdrModel    = new OrderHdrModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->outPalletModel   = new OutPalletModel();
        $this->packHdrModel     = new PackHdrModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function getLpn($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $odrNum = array_get($input, 'data.odr_num', null);
        /*
         * start logs
         */

        $url = "/v2/{$whsId}/order/get-lpn-by-order-no-tag";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GLO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get LPN and Order information'
        ]);
        /*
         * end logs
         */

        if (empty($odrNum)) {
            $msg = sprintf("Order num is required.");
            return $this->_responseErrorMessage($msg);
        }

        $odrObj = $this->orderHdrModel->getFirstWhere(['odr_num' => $odrNum]);

        if (!$odrObj) {
            $msg = sprintf("Order num does not exist.");
            return $this->_responseErrorMessage($msg);
        }

        if ($odrObj && $odrObj->whs_id != $whsId) {
            $msg = sprintf("Order num does not belong to current warehouse.");
            return $this->_responseErrorMessage($msg);
        }

        if ($odrObj && $odrObj->odr_sts != "PA" && $odrObj->odr_sts != "PTG") {
            $msg = sprintf("Only assign pack cartons to pallet when order status are Packed or Palletizing.");
            return $this->_responseErrorMessage($msg);
        }

        try {

            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $odrId  = $odrObj->odr_id;
            $odrNum = $odrObj->odr_num;
            $cusPo  = $odrObj->cus_po;

            $lpnNumNext = null;
            $outPltIds = $this->packHdrModel->getModel()
                ->where('odr_hdr_id', $odrId)
                ->pluck('out_plt_id')->toArray();
            if (count($outPltIds)) {
                $outPltObjFirst = $this->outPalletModel->getModel()
                    ->whereIn('plt_id', $outPltIds)
                    ->where('deleted', 0)
                    ->orderBy('plt_num', 'DESC')
                    ->first();
                if (count($outPltObjFirst)) {
                     $lpnNumLast = object_get($outPltObjFirst, 'plt_num');
                     $lpnNumNext = ++$lpnNumLast;
                } else {
                    $lpnNumNext = $this->_getLpnByOdrNum($odrNum);
                }
            } else {
                $lpnNumNext = $this->_getLpnByOdrNum($odrNum);
            }

            $packRemain = $this->packHdrModel->getModel()
                ->where('odr_hdr_id', $odrId)
                ->whereNull('out_plt_id')
                ->count();


            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data'] = [[
                'order' => [
                    'odr_id'  => $odrId,
                    'odr_num' => $odrNum,
                    'cus_po'  => $cusPo,
                ],
                // 'skus' => $ctnInfos,
                'lpn_num'     => $lpnNumNext,
                'pack_remain' => $packRemain,
            ]];

            return $msg;

        } catch (\Exception $e) {

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _getLpnByOdrNum($odrNum)
    {
        $licensePlate = $odrNum . "-" . str_pad(1, 4, "0", STR_PAD_LEFT);
        return str_replace("ORD", "LPN", $licensePlate);
    }

}