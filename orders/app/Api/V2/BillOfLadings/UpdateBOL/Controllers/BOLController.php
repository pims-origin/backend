<?php

namespace App\Api\V2\BillOfLadings\UpdateBOL\Controllers;

use App\Api\V1\Models\ThirdPartyModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\BOLCarrierDetailModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\BOLOrderDetailModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\CartonModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\CommodityModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\CustomerConfigModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\EventTrackingModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\InventorySummaryModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\OrderCartonModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\OrderDtlModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\OrderHdrModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\ReportModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\ReportService;
use App\Api\V2\BillOfLadings\CreateBOL\Models\ShipmentModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\WarehouseModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\CustomerModel;
use App\Api\V2\BillOfLadings\CreateBOL\Models\OutPalletModel;
use App\Api\V2\BillOfLadings\CreateBOL\Traits\OrderFlowControllerTrait;
use App\Api\V2\BillOfLadings\CreateBOL\Transformers\BOLListTransformer;
use App\Api\V2\BillOfLadings\CreateBOL\Transformers\CommodityTransformer;
use App\Api\V2\BillOfLadings\CreateBOL\Validators\BOLHdrValidator;
use App\Api\V2\BillOfLadings\UpdateBOL\Models\OrderHdrMetaModel;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Wms2\UserInfo\Data;
use mPDF;
use GuzzleHttp\Client;
use App\Api\V1\Models\Log;
use App\Utils\SendMail;

/**
 * Class BOLController
 *
 * @package App\Api\V2\BillOfLadings\CreateBOL\Controllers
 */
class BOLController extends AbstractController
{
    use OrderFlowControllerTrait;

    protected $orderHdrModel;
    protected $shipmentModel;
    protected $bolCarrierDtlModel;
    protected $bolOdrDtlModel;
    protected $commodityModel;
    protected $warehouseModel;
    protected $customerModel;
    protected $reportModel;
    protected $reportService;
    protected $customerConfigModel;
    protected $eventTrackingModel;
    protected $orderDtlModel;
    protected $invtSmrModel;
    protected $outPalletModel;
    protected $thirdPartyModel;

    /**
     * BOLController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param ShipmentModel $shipmentModel
     * @param BOLCarrierDetailModel $bolCarrierDetailModel
     * @param BOLOrderDetailModel $bolOrderDetailModel
     * @param CommodityModel $commodityModel
     * @param WarehouseModel $warehouseModel
     * @param ReportModel $reportModel
     * @param ReportService $reportService
     * @param CustomerConfigModel $customerConfigModel
     * @param EventTrackingModel $eventTrackingModel
     * @param OrderDtlModel $orderDtlModel
     * @param InventorySummaryModel $invtSmrModel
     * @param OrderCartonModel $orderCartonModel
     * @param CartonModel $cartonModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        ShipmentModel $shipmentModel,
        BOLCarrierDetailModel $bolCarrierDetailModel,
        BOLOrderDetailModel $bolOrderDetailModel,
        CommodityModel $commodityModel,
        WarehouseModel $warehouseModel,
        CustomerModel $customerModel,
        ReportModel $reportModel,
        ReportService $reportService,
        CustomerConfigModel $customerConfigModel,
        EventTrackingModel $eventTrackingModel,
        OrderDtlModel $orderDtlModel,
        InventorySummaryModel $invtSmrModel,
        OrderCartonModel $orderCartonModel,
        CartonModel $cartonModel,
        OutPalletModel $outPalletModel
    ) {
        $this->orderHdrModel       = $orderHdrModel;
        $this->shipmentModel       = $shipmentModel;
        $this->bolCarrierDtlModel  = $bolCarrierDetailModel;
        $this->bolOdrDtlModel      = $bolOrderDetailModel;
        $this->commodityModel      = $commodityModel;
        $this->warehouseModel      = $warehouseModel;
        $this->customerModel       = $customerModel;
        $this->reportModel         = $reportModel;
        $this->reportService       = $reportService;
        $this->customerConfigModel = $customerConfigModel;
        $this->eventTrackingModel  = $eventTrackingModel;
        $this->orderDtlModel       = $orderDtlModel;
        $this->invtSmrModel        = $invtSmrModel;
        $this->orderCartonModel    = $orderCartonModel;
        $this->cartonModel         = $cartonModel;
        $this->outPalletModel      = $outPalletModel;
        $this->thirdPartyModel = new ThirdPartyModel();
    }

    /**
     * @param $whsId
     * @param $shipId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function update($whsId, $shipId, Request $request)
    {
        //get params
        $input = $request->getParsedBody();
        $input['ship_id'] = $shipId;
        (new BOLHdrValidator())->validate($input);

        $shipment = $this->shipmentModel->getFirstWhere(['ship_id' => $shipId]);
        if (empty($shipment->ship_id)) {
            throw new \Exception(Message::get("BM017", "Shipment"));
        }

        $bol_num = array_get($shipment, 'bo_num', '');
        $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $input['items'][0]['odr_id'])->toArray();
        $volumes = DB::table('odr_hdr')
            ->select(['p.odr_hdr_id', DB::raw('sum(p.length * p.width * p.height) as vol_ttl')])
            ->leftJoin("pack_hdr as p", "p.odr_hdr_id", "=", 'odr_hdr.odr_id')
            ->whereIn('odr_id', array_column($input['items'], 'odr_id'))
            ->where([
                'odr_hdr.deleted'    => 0,
                'odr_hdr.deleted_at' => 915148800
            ])->groupBy('p.odr_hdr_id')->get();

        $volumes = array_pluck($volumes, "vol_ttl", "odr_hdr_id");

        $bol_type = array_get($input, 'bol_type');
        if($bol_type == 1){
            $customer = $this->customerModel->getFirstBy('cus_id', $input['cus_id'], [
                'customerAddress',
                'systemCountry',
                'systemState'
            ])->toArray();
            $customer_address = DB::table('cus_address')
                ->join('system_state', 'cus_address.cus_add_state_id', '=', 'system_state.sys_state_id')
                ->join('system_country', 'cus_address.cus_add_country_id', '=', 'system_country.sys_country_id')
                ->where('cus_add_type', 'ship')->where('cus_add_cus_id', $input['cus_id'])->first();
            // $customer_address = array_get($customer, 'customer_address', '');
            $ship_from_name = $customer['cus_name'];
            $ship_from_addr_1 = array_get($customer_address, 'cus_add_line_1', null);
            $ship_from_addr_2 = array_get($customer_address, 'cus_add_line_2', null);
            $ship_from_city = array_get($customer_address, 'cus_add_city_name', '');
            $ship_from_state = array_get($customer_address, 'sys_state_code', '');
            $ship_from_zip = array_get($customer_address, 'cus_add_postal_code', null);
            $ship_from_country = array_get($customer_address, 'sys_country_code', 'US');
        }
        else{
            $warehouse = $this->warehouseModel->getFirstBy('whs_id', $whsId, [
                'warehouseAddress',
                'systemCountry',
                'systemState'
            ])->toArray();
            $ship_from_name = 'Seldat';
            $ship_from_addr_1 = array_get($warehouse, 'warehouse_address.whs_add_line_1', null);
            $ship_from_addr_2 = array_get($warehouse, 'warehouse_address.whs_add_line_2', null);
            $ship_from_city = $warehouse['whs_city_name'];
            $ship_from_state = array_get($warehouse, 'system_state.sys_state_code', '');
            $ship_from_zip = array_get($warehouse, 'warehouse_address.whs_add_postal_code', null);
            $ship_from_country = array_get($warehouse, 'system_country.sys_country_code', 'US');
        }

        /**
         * 5997 - [BOL] Need to be able to create master BOL without editing or matching the ship to address
         */
        $odrIDList = array_pluck($input['items'], 'odr_id');
        $odrHdrList = $this->orderHdrModel->getOrderAddressByOdrIdList($odrIDList);

        $shipmentParam = [
            'whs_id'          => $shipment->whs_id,
            'cus_id'          => $shipment->cus_id,
            'bo_num'          => $shipment->bo_num,
            'bo_label'        => $input['bo_label'],
            'ship_dt'         => $orderHdr['ship_by_dt'],

            'ship_from_name'    => $ship_from_name,
            'ship_from_addr_1'  => $ship_from_addr_1,
            'ship_from_addr_2'  => $ship_from_addr_2,
            'ship_from_city'    => $ship_from_city,
            'ship_from_state'   => $ship_from_state,
            'ship_from_zip'     => $ship_from_zip,
            'ship_from_country' => $ship_from_country,

            'ship_to_name'    => array_get($input, 'ship_to_name', null),
            'ship_to_addr_1'  => array_get($input, 'ship_to_addr_1', null),
            'ship_to_addr_2'  => array_get($input, 'ship_to_addr_2', null),
            'ship_to_city'    => array_get($input, 'ship_to_city', null),
            'ship_to_state'   => array_get($input, 'ship_to_state', null),
            'ship_to_zip'     => array_get($input, 'ship_to_zip', null),
            'ship_to_country' => array_get($input, 'ship_to_country', null),

            /**
             * 5997 - [BOL] Need to be able to create master BOL without editing or matching the ship to address
             */
//            'ship_to_name'    => (count(array_unique(array_pluck($odrHdrList, 'ship_to_name'))) == 1) ?
//                array_get($odrHdrList[0], 'ship_to_name') : 'NA',
//            'ship_to_addr_1'  => (count(array_unique(array_pluck($odrHdrList, 'ship_to_add_1'))) == 1) ?
//                array_get($odrHdrList[0], 'ship_to_add_1') : 'NA',
//            'ship_to_addr_2'  => (count(array_unique(array_pluck($odrHdrList, 'ship_to_add_2'))) == 1) ?
//                array_get($odrHdrList[0], 'ship_to_add_2') : 'NA',
//            'ship_to_city'    => (count(array_unique(array_pluck($odrHdrList, 'ship_to_city'))) == 1) ?
//                array_get($odrHdrList[0], 'ship_to_city') : 'NA',
//            'ship_to_state'   => (count(array_unique(array_pluck($odrHdrList, 'ship_to_state'))) == 1) ?
//                array_get($odrHdrList[0], 'ship_to_state') : 'NA',
//            'ship_to_zip'     => (count(array_unique(array_pluck($odrHdrList, 'ship_to_zip'))) == 1) ?
//                array_get($odrHdrList[0], 'ship_to_zip') : 'NA',
//            'ship_to_country' => (count(array_unique(array_pluck($odrHdrList, 'ship_to_country'))) == 1) ?
//                array_get($odrHdrList[0], 'ship_to_country') : 'NA',

            'bill_to_name'    => array_get($input, 'bill_to_name', null),
            'bill_to_addr_1'  => array_get($input, 'bill_to_addr_1', null),
            'bill_to_city'    => array_get($input, 'bill_to_city', null),
            'bill_to_state'   => array_get($input, 'bill_to_state', null),
            'bill_to_zip'     => array_get($input, 'bill_to_zip', null),
            'bill_to_country' => "US",

            'special_inst'         => $input['special_inst'],
            'carrier'              => $input['carrier'],
            'trailer_num'          => array_get($input, 'trailer_num', null),
            'seal_num'             => array_get($input, 'seal_num', null),
            'scac'                 => array_get($input, 'scac', null),
            'pro_num'              => array_get($input, 'pro_num', null),
            'freight_charge_terms' => $input['freight_charge_terms'],
            'freight_charge_cost'  => array_get($input, 'freight_charge_cost', 0),
            'freight_counted_by'   => $input['freight_counted_by'],
            'po_qty_ttl'           => count($input['items']),
            'weight_ttl'           => (float)array_sum(array_column($input['items'], 'weight')),
            'ctn_qty_ttl'          => array_sum(array_column($input['items'], 'pkgs')),
            'piece_qty_ttl'        => array_sum(array_column($input['items'], 'units')),
            'vol_qty_ttl'          => array_sum($volumes),
            'cube_qty_ttl'         => array_sum($volumes) / 1728,
            'plt_qty_ttl'          => array_sum(array_column($input['items'], 'plts')),
            'fee_terms'            => $input['fee_terms'],
            'cus_accept'           => $input['cus_accept'],
            'trailer_loaded_by'    => $input['trailer_loaded_by'],
            'sts'                  => 'u',
            'party_acc'            => $input['party_acc'],
            'deli_service'         => $input['deli_service'],
            'is_attach'            => array_get($input, 'is_attach', 0),
            'ship_method'          => array_get($input, 'ship_method', null),
            'ship_sts'             => !empty($input['ship_sts']) ?
                $input['ship_sts'] : Status::getByValue("New", "Ship-Status"),
        ];

        try {
            DB::beginTransaction();
            // Update Or Insert Third party
            $thirdParty = $this->createOrUpdateThirdParty($input);
            $shipmentParam['shipping_addr_id'] = $thirdParty->tp_id;

            $this->shipmentModel->refreshModel();
            $this->shipmentModel->updateWhere($shipmentParam, ['ship_id' => $shipId]);
            $allOrder = $this->orderHdrModel->findWhere(['ship_id' => $shipId])->toArray();
            $deleteIds = array_pluck($allOrder, 'odr_id', 'odr_id');

            foreach ($input['items'] as $item) {
                $volume = !empty($volumes[$item['odr_id']]) ? $volumes[$item['odr_id']] : 0;
                $bolOdrDtlParam = [
                    'ship_id'          => $shipId,
                    'ctn_qty'          => $item['pkgs'], // pkgs
                    'piece_qty'        => $item['units'], // units
                    'weight'           => (float)array_get($item, 'weight', 0),
                    'plt_qty'          => $item['plts'], //plts
                    'cus_dept'         => $item['cus_dept'],
                    'cus_ticket'       => array_get($item, 'cus_ticket', null),
                    'cus_odr'          => $item['cus_odr'],
                    'add_shipper_info' => $item['add_shipper_info'],
                    'odr_hdr_num'      => $item['odr_hdr_num'],
                    'vol_qty_ttl'      => $volume,
                    'cube_qty_ttl'     => $volume / 1728,
                    'odr_id'           => $item['odr_id'],
                    'cus_po'           => $item['cus_po'],
                ];

                $this->bolOdrDtlModel->refreshModel();
                $odr = $this->bolOdrDtlModel->getFirstBy('odr_id', $item['odr_id']);
                if (empty($odr->odr_id)) {
                    // Create
                    $this->bolOdrDtlModel->refreshModel();
                    $bolOdrDtlObj = $this->bolOdrDtlModel->create($bolOdrDtlParam);

                    // WMS2-4363 - update Out Pallet
                    $bolId = object_get($bolOdrDtlObj, 'bol_id');
                    $this->outPalletModel->updateOutPalletWithBOL($shipment->ship_id, $item['odr_id'], $whsId);
                } else {
                    // Update or Fail
                    $this->bolOdrDtlModel->refreshModel();
                    $this->bolOdrDtlModel->updateWhere($bolOdrDtlParam, ['odr_id' => $item['odr_id']]);
                }

                // Update Order Header ship_id and Order Status
                if (in_array($item['odr_id'], $deleteIds)) {
                    unset($deleteIds[$item['odr_id']]);
                } else {
                    $this->orderHdrModel->refreshModel();
                    $this->orderHdrModel->updateWhere(['ship_id' => $shipId], ['odr_id' => $item['odr_id']]);
                }
                // Update Order Header ship_id and Order Status
                $orderHdrMetaModel = new OrderHdrMetaModel();
                $orderFlow = $orderHdrMetaModel->getOrderFlow($item['odr_id']);
                $skipShippingLaneFlow = $orderHdrMetaModel->getFlow($orderFlow, 'ASS');
                if ( (array_get($skipShippingLaneFlow, 'usage', -1) == 1) && ($input['ship_sts'] == 'FN') ) {
                    $this->orderHdrModel->updateWhere(
                        [
                            'odr_sts' => 'SS'
                        ],
                        ['odr_id' => $item['odr_id']]
                    );
                }
            }

            if (!empty($deleteIds)) {
                foreach ($deleteIds as $deleteId) {
                    $this->orderHdrModel->refreshModel();
                    $this->orderHdrModel->updateWhere(['ship_id' => null], ['odr_id' => $deleteId]);

                    $this->bolOdrDtlModel->refreshModel();
                    //$this->bolOdrDtlModel->updateWhere(['ship_id' => null], ['odr_id' => $deleteId]);
                    DB::table('bol_odr_dtl')->where('odr_id', $deleteId)
                        ->delete();
                        //->update([
                        //    'deleted'    => 1,
                        //    'deleted_at' => time()
                        //]);
                }
            }

            $carrierDtlParam = [
                'hdl_qty' => array_sum(array_column($input['items'], 'plts')), // sum(plts)
                'pkg_qty' => array_sum(array_column($input['items'], 'pkgs')),// sum(pkgs)
                'weight'  => array_sum(array_column($input['items'], 'weight')),
                'cmd_id'  => $input['cmd_des'],
            ];

            $this->bolCarrierDtlModel->updateWhere($carrierDtlParam, ['ship_id' => $shipId]);

            $arrOdNum = array_column($input['items'], 'odr_hdr_num');

            $this->_uploadPdfToDMS($input, $whsId, $shipId, $shipment->bo_num, $request);
            $this->_uploadPdfToDMSV2($input, $whsId, $shipId, $arrOdNum, $request);

            DB::commit();

            //push MQ receiving
            if ($this->customerConfigModel->checkWhere([
                'whs_id'       => $shipment->whs_id,
                'cus_id'       => $shipment->cus_id,
                'config_name'  => config('constants.cus_config_name.EDI_INTEGRATION'),
                'config_value' => config('constants.cus_config_value.EDI858'),
                'ac'           => config('constants.cus_config_active.YES')
            ])
            ) {

                $reportDetail = $this->reportModel->shipment($shipment->ship_id);

                if ($reportDetail) {
                    $this->reportService->init($reportDetail);
                    $this->reportService->process();
                }

            }
            //get csr email and send mail to CSRs new: WMS2-6298, old: 6042
            /*if($input['ship_sts'] == 'FN') {
                $this->_sendMail2CSRs($shipment->ship_id, $shipment->bo_num, $shipment->cus_id);
            }*/

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'ship_id' => $shipId
                ]
            ])->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            \DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            \DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $input
     *
     * @return mixed
     */
    private function createOrUpdateThirdParty(
        $input
    ) {
        $whs_id = array_get($input, 'whs_id', null);
        $name = array_get($input, 'ship_to_name', null);
        $add_1 = array_get($input, 'ship_to_addr_1', null);
        $add_2 = array_get($input, 'ship_to_addr_2', null);
        $city = array_get($input, 'ship_to_city', null);
        $state = array_get($input, 'ship_to_state', null);
        $zip = array_get($input, 'ship_to_zip', null);
        $country = array_get($input, 'ship_to_country', null);
        $cus_id = array_get($input, 'cus_id');

        $thirdParty = $this->thirdPartyModel->getFirstBy(DB::raw("UPPER(REPLACE(name, ' ', ''))"),
            strtoupper(preg_replace('/\s+/', '', $name)));

        // Update
        if ($thirdParty) {

            $thirdParty->whs_id = $whs_id;
            $thirdParty->cus_id = $cus_id;
            $thirdParty->name = $name;
            $thirdParty->add_1 = $add_1;
            $thirdParty->add_2 = $add_2;
            $thirdParty->city = $city;
            $thirdParty->state = $state;
            $thirdParty->zip = $zip;
            $thirdParty->country = $country;
            $thirdParty->save();

        } else {
            // Create
            $thirdParty = $this->thirdPartyModel->create([
                'type'    => 'CUS',
                'name'    => $name,
                'whs_id'  => $whs_id,
                'cus_id'  => $cus_id,
                'add_1'   => $add_1,
                'add_2'   => $add_2,
                'city'    => $city,
                'state'   => $state,
                'zip'     => $zip,
                'country' => $country
            ]);
        }

        return $thirdParty;

    }

    private function _uploadPdfToDMS($input, $whsId, $shipId, $bolNum, $request)
    {
        // Present file BOL pdf for DMS integration
        try {
            if (!empty($input['ship_sts']) && ($input['ship_sts'] == 'FN')) {
                $pdfBolFileName = app('App\Api\V2\BillOfLadings\PrintBOL\Controllers\BOLController')->printBOL($whsId, $shipId, true, $request);
                $fileUrl = env('SYSTEM_BASE_URL') . "/core/orders/public/download-document?type=dms&file_name={$pdfBolFileName}";
                // Post to DMS
                $client = new Client();
                $dmsApiUrl = env('DMS_DOCUMENT_API');

                $owner = $transaction = "";
                Log:: info($request, $whsId, [
                    'evt_code'     => 'PBD',
                    'owner'        => $owner,
                    'transaction'  => $transaction,
                    'url_endpoint' => $fileUrl,
                    'message'      => 'Start Post BOL file to DMS. '
                ]);
                $res = $client->request('POST', $dmsApiUrl, [
                    'http_errors' => false,
                    'form_params' => [
                        'sys'           => env('SYSTEM_NAME', 'wms360.dev'),
                        'jwt'           => str_replace('Bearer ', '', $request->getHeaders()['authorization'][0]),
                        'file_url'      => $fileUrl,
                        'sts'           => 1,
                        'transaction'   => $bolNum,
                        'document_type' => 'BOL',
                        'doc_date'      => !empty($input['doc_date']) ? $input['doc_date'] : date('m/d/Y'),
                    ]
                ]);
                // Log:: info($request, $whsId, [
                //     'evt_code'     => 'PBD',
                //     'owner'        => $owner,
                //     'transaction'  => $transaction,
                //     'url_endpoint' => $fileUrl,
                //     'message'      => 'Post BOL file to DMS. '. json_encode($res)
                // ]);

            }
        }catch (\Exception $e) {
            SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__);
        }
    }

    private function _uploadPdfToDMSV2($input, $whsId, $shipId, $arrOdNum, $request)
    {
        // Present file BOL pdf for DMS integration
        try {
            if (!empty($input['ship_sts']) && ($input['ship_sts'] == 'FN')) {
                $pdfBolFileName = app('App\Api\V2\BillOfLadings\PrintBOL\Controllers\BOLController')->printBOL($whsId, $shipId, true, $request);
                $fileUrl = env('SYSTEM_BASE_URL') . "/core/orders/public/download-document?type=dms&file_name={$pdfBolFileName}";
                // Post to DMS
                $client = new Client();
                $dmsApiUrl = env('DMS_DOCUMENT_API');

                $owner = $transaction = "";
                Log:: info($request, $whsId, [
                    'evt_code'     => 'PBD',
                    'owner'        => $owner,
                    'transaction'  => $transaction,
                    'url_endpoint' => $fileUrl,
                    'message'      => 'Start Post BOL file to DMS. '
                ]);

                // add transaction odNum
                foreach ($arrOdNum as $odNum) {
                    $res = $client->request('POST', $dmsApiUrl, [
                        'http_errors' => false,
                        'form_params' => [
                            'sys'           => env('SYSTEM_NAME', 'wms360.dev'),
                            'jwt'           => str_replace('Bearer ', '', $request->getHeaders()['authorization'][0]),
                            'file_url'      => $fileUrl,
                            'sts'           => 1,
                            'transaction'   => $odNum,
                            'document_type' => 'order_doc',
                            'doc_date'      => !empty($input['doc_date']) ? $input['doc_date'] : date('m/d/Y'),
                        ]
                    ]);
                }

                // Log:: info($request, $whsId, [
                //     'evt_code'     => 'PBD',
                //     'owner'        => $owner,
                //     'transaction'  => $transaction,
                //     'url_endpoint' => $fileUrl,
                //     'message'      => 'Post BOL file to DMS. '. json_encode($res)
                // ]);

            }
        }catch (\Exception $e) {
            SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__);
        }
    }

     /**
      * this function is used to get corresponding CSR emails and send emails to them
      *
      * @param [type] $shipId
      * @param [type] $boNum
      * @param [type] $cusId
      * @return void
      */
      private function _sendMail2CSRs($shipId, $boNum, $cusId)
      {
          $params['email']  = [];
          $params['data']  = [];

          //get all email of CSR from current customer
          $cusMeta = DB::table('cus_meta')->select('value')
                              ->where('cus_id', $cusId)
                              ->where('qualifier', 'CSR')->first();
          $valueJsons = \GuzzleHttp\json_decode($cusMeta['value']);
          $wh_ids = [];
          if ($valueJsons) {
              foreach ($valueJsons as $valueJson) {
                  $user_id = object_get(\GuzzleHttp\json_decode($valueJson), 'user_id', 0);
                  $whs_id = object_get(\GuzzleHttp\json_decode($valueJson), 'whs_id', 0);

                  $userInfo = DB::table('users')->select('email', 'first_name', 'last_name')
                  ->where('user_id', $user_id)
                  ->first();
                  if(!empty($userInfo)) {
                      $params['email'][] = $userInfo['email'];
                      $params['username'] = $userInfo['first_name'] .' '. $userInfo['last_name'];
                  }
              }
          }

          //get order number
          $getOrderNum = DB::table(DB::raw('bol_odr_dtl as bod'))
                  ->select([
                      DB::raw('oh.odr_num, oh.odr_id')
                  ])
                  ->join("odr_hdr as oh", "oh.odr_id", "=", 'bod.odr_id')
                  ->leftJoin("users as u", "oh.csr", "=", 'u.user_id')
                  ->where('bod.ship_id', $shipId)
                  ->where('bod.deleted', 0)
                  ->get();

          if( !empty($getOrderNum) ) {
              foreach($getOrderNum as $key => $item)
              {
                  $params['data'][$key]['odr_num'] = $item['odr_num'];
                  $params['data'][$key]['odr_id'] = $item['odr_id'];
              }
          }

          $mail = new SendMail();

          //send email
          $mail->sendMail(
              'mailBOLCreated',
              $params['email'],
              "BOL was created with BOL : ". $boNum,
                  [
                      'data'         => $params['data'],
                      'ship_id'      => $shipId,
                      'bo_num'       => $boNum,
                      'website_name' => 'Seldat',
                      'servername' =>  env('API_DOMAIN', $_SERVER['SERVER_NAME']),
                      // 'username'     => $params['username'],
                      'username'     => "all",
                  ]
              );
          $this->response->noContent();
      }
}
