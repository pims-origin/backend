<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\CustomerMeta;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class CustomerMetaModel extends AbstractModel
{
    /**
     * @param CustomerMeta $model
     */
    public function __construct(CustomerMeta $model = null)
    {
        $this->model = ($model) ?: new CustomerMeta();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateCustomerMeta($data)
    {
        return $this->model
            ->where('cus_id', '=', $data['cus_id'])
            ->where('qualifier', '=', $data['qualifier'])
            ->update(['value' => $data['value']]);
    }

    /**
     * @param $dataUpdated
     * @param $where
     *
     * @return mixed
     */
    public function updateWhere($dataUpdated, $where)
    {
        $query = $this->make();
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query->where($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query->where($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query->where($field, '=', $search);
                }
            } else {
                $query->where($field, '=', $value);
            }
        }

        return $query->update($dataUpdated);
    }


}