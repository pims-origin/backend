<?php

return [
    'ctn_status' => [
        'ACTIVE'   => 'AC',
        'INACTIVE' => 'IA',
        'RACKED'   => 'RK',
        'RECEIVED' => 'RC',
        'SHIPPED'   => 'SH'
    ],
    'odr_status' => [
        'NEW'        => 'NW',
        'ALLOCATED'  => 'AL',
        'STAGING' =>    'ST',
        'PICKING'    => 'PK',
        'PICKED'    => 'PD',
        'PACKED'    => 'PA',
        'SHIPPED'   => 'SH',
        'HOLD'      => 'HO',
        'CANCELLED' => 'CC'
    ],
    'charge_codes_measures' => [],
    'gr_status'           => [
        'RECEIVING'       => 'RG',
        'RECEIVED'        => 'RE',
        'RCVD-DISCREPANT' => 'RD',
    ],
    'event'  => [
        'GR-COMPLETE'  => 'GRC',
        'ORD-SHIPPED' => 'OSH',
        'ORD-CANCELLED' => 'OCN',
        'PR-SHIPPED' => 'PRC'
    ],
    'odr_type' => [
        'ECOM' => 'EC',
    ],
    'labor_type' => [
        'EXPECTED' => 'expected',
        'ACTUAL' => 'actual',
    ],
    'labor_cat' => [
        'OP' => 'op',
        'RCV' => 'rcv',
        'WO'  => 'wo'
    ],
];
