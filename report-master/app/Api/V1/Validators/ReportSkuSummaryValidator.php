<?php

namespace App\Api\V1\Validators;


class ReportSkuSummaryValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'whs_code' => 'required',
            'cus_code' => 'required',
            'sku' => 'required',
            'size' => 'required',
            'color' => 'required',
            'pack' => 'required',
            'uom' => 'required',
            'in_hand_qty' => 'required',
            'in_pick_qty' => 'required',
            'total_qty' => 'required',
        ];
    }
}
