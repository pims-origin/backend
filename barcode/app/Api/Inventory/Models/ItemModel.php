<?php

namespace App\Api\Inventory\Models;

use Seldat\Wms2\Models\BlockDtl;
use Seldat\Wms2\Models\BlockHdr;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Message;

class ItemModel extends AbstractModel
{
    const  LIMIT_SEARCH = 10;
    const TYPE_CYCLE = 'cycle';
    const TYPE_BLOCK = 'block';

    public function __construct($model = null)
    {
        $this->model = $model ? $model : new Item();
    }

    /**
     * @param $whsId
     * @param $items
     */
    public static function getSKUInWarehouse($whsId, $items)
    {
        $result = [];

        if (!($whsId && $items)) {
            return $result;
        }

        foreach ($items as $item) {
            if ($item['whs_id'] == $whsId) {
                $result[$item['sku']] = true;
            }
        }

        return $result;
    }

    /**
     * @param $skus
     */
    public static function getInfoItemBySkus($skus)
    {
        $result = [];

        if (!$skus) {
            return $result;
        }

        $result = Item::select(
            'item.item_id',
            'item.sku',
            'item.size',
            'item.color',
            'item.cus_id',
            'customer_warehouse.whs_id'
        )
            ->join('customer', 'customer.cus_id', '=', 'item.cus_id')
            ->join('customer_warehouse', 'customer_warehouse.cus_id', '=', 'customer.cus_id')
            ->whereIn('item.sku', $skus)
            ->get();

        return $result;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */

    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->leftJoin('invt_smr', 'invt_smr.item_id', '=', 'item.item_id');
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'cus_id') {
                    $query->where('item.' . $key, $value);
                } elseif ($key === 'sku') {
                    $query->where('item.' . $key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }
        $this->model->filterDataIn($query, true, false);
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $skus
     */
    public static function getItemBySKUs($skus)
    {
        if (!$skus) {
            return [];
        }

        $result = Item::select(
            'item_id',
            'item_code',
            'sku',
            'size',
            'color'
        )
            ->whereIn('sku', $skus)
            ->where('status', LockModel::STATUS_ACTIVE)
            ->get();

        return $result;
    }

    /**
     * @param $itemID
     *
     * @return array
     */
    public function getItemInfo($itemID)
    {
        $result = [];

        if (!$itemID) {
            return $result;
        }

        $result = $this->model->select(
            'customer_warehouse.whs_id'
        )
            ->join('customer', 'customer.cus_id', '=', 'item.cus_id')
            ->join('customer_warehouse', 'customer_warehouse.cus_id', '=', 'customer.cus_id')
            ->where('item.item_id', $itemID)
            ->groupBy('customer_warehouse.whs_id')
            ->get(['customer_warehouse.whs_id'])->toArray();

        return $result;
    }

    /**
     * @param $whsId
     * @param $itemIds
     *
     * @return mixed
     */
    public function getItemLockedByCycle($whsId, $itemIds)
    {
        $result = [];

        if (!($whsId && $itemIds)) {
            return $result;
        }

        $result = CycleDtl::select(
            'cycle_dtl.sku',
            'cycle_hdr.cycle_hdr_id',
            'cycle_hdr.cycle_name'
        )
            ->join('cycle_hdr', 'cycle_hdr.cycle_hdr_id', '=', 'cycle_dtl.cycle_hdr_id')
            ->whereNotIn('cycle_hdr.cycle_sts', [
                CycleHdrModel::STATUS_CYCLE_COMPLETED,
                CycleHdrModel::STATUS_CYCLE_DELETED
            ])
            ->whereIn('cycle_dtl.item_id', $itemIds)
            ->where('cycle_hdr.whs_id', $whsId)
            ->distinct()
            ->get()
            ->toArray();

        return $result;
    }

    /**
     * @param $whsId
     * @param $itemIds
     *
     * @return array
     */
    public function getItemLockedByBlock($whsId, $itemIds)
    {
        $result = [];

        if (!($whsId && $itemIds)) {
            return $result;
        }

        $result = BlockDtl::select(
            'block_dtl.sku',
            'block_dtl.block_hdr_id',
            'block_hdr.block_rsn_id'
        )
            ->join('block_hdr', 'block_hdr.block_hdr_id', '=', 'block_dtl.block_hdr_id')
            ->whereNotIn('block_hdr.block_sts', [
                BlockHdrModel::STATUS_UNBLOCKED
            ])
            ->whereIn('block_dtl.item_id', $itemIds)
            ->where('block_hdr.whs_id', $whsId)
            ->distinct()
            ->get()
            ->toArray();

        return $result;
    }

    /**
     * @param $whsId
     * @param $itemIds
     *
     * @return array
     */
    public function getItemLocked($whsId, $itemIds)
    {
        $itemLockeds = $this->getItemLockedByCycle($whsId, $itemIds);

        if ($itemLockeds) {
            return [
                'type'       => self::TYPE_CYCLE,
                'itemLocked' => $itemLockeds
            ];
        }

        $itemLockeds = $this->getItemLockedByBlock($whsId, $itemIds);

        if ($itemLockeds) {
            return [
                'type'       => self::TYPE_BLOCK,
                'itemLocked' => $itemLockeds
            ];
        }

        return [];
    }

    /**
     * @param $data
     *
     * @return array|string
     */
    public function getErrorMsgWithSKUExist($data)
    {
        $field = $data['type'] == 'block' ? 'block_hdr_id' : 'cycle_hdr_id';
        $type = $data['type'] == 'block' ? 'block stock' : 'cycle count';

        $result = [];

        foreach ($data['itemLocked'] as $item) {
            $result[] = Message::get('BM095', 'SKU: ' . $item['sku'], $item[$field] . $type);
        }

        $result = implode('<br>', $result);
        $result .= '<br>.Please complete them before';

        return $result;
    }

    /**
     * @param $whsId
     * @param null $search
     * @param null $limit
     *
     * @return array
     */
    public function searchByWarehouse($whsId, $search = null, $limit = null)
    {
        $result = [];

        if (!$whsId) {
            return $result;
        }

        $limit = $limit ? $limit : self::LIMIT_SEARCH;

        $query = $this->model->select(
            'item.item_id',
            'item.item_code',
            'item.sku',
            'item.size',
            'item.color',
            'item.cus_id',
            'customer_warehouse.whs_id'
        )
            ->join('customer', 'customer.cus_id', '=', 'item.cus_id')
            ->join('customer_warehouse', 'customer_warehouse.cus_id', '=', 'customer.cus_id')
            ->join('cartons', 'cartons.item_id', '=', 'item.item_id')
            ->where('customer_warehouse.whs_id', $whsId)
            ->where('customer_warehouse.deleted', 0);

        if ($search) {
            $query->where('item.item_id', "LIKE", '%' . trim($search) . '%');
        }

        $result = $query->limit($limit)
            ->orderBy('item.item_id', 'desc')
            ->groupBy('item.item_id')
            ->get();

        return $result;
    }

    /**
     * @param $cycleHdrId
     * @param $itemId
     */
    public function checkItemExistOnAnotherCycleOrBlock($whsId, $cycleHdrId, $itemId)
    {
        $resutl = ['status' => true];
        $data = $this->getItemLocked($whsId, [$itemId]);

        if (!$data) {
            return $resutl;
        }

        if ($data['type'] == self::TYPE_BLOCK) {
            return [
                'status' => false,
                'type'   => self::TYPE_BLOCK,
                'header' => $data['itemLocked'][0]['block_hdr_id']
            ];
        }

        if ($data['type'] == self::TYPE_CYCLE) {
            foreach ($data['itemLocked'] as $item) {
                if ($item['cycle_hdr_id'] != $cycleHdrId) {
                    return [
                        'status' => false,
                        'type'   => self::TYPE_CYCLE,
                        'header' => $item['cycle_hdr_id']
                    ];
                }
            }
        }

        return $resutl;
    }
}