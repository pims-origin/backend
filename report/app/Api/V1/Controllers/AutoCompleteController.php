<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\LocationUtilizationModel;
use Psr\Http\Message\ServerRequestInterface as Request;

class AutoCompleteController extends AbstractController
{
    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);
        parent::__construct($request, $object);

        $this->locationUtillizationModel = new LocationUtilizationModel();
    }

    public function locationRow(Request $request)
    {
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', 20);
        $row = array_get($input, 'row', '');

        $query = $this->locationUtillizationModel->getColumn('row', $row);
        $locRows = $query->take($limit)->get();

        return ['data' => $locRows];
    }

    public function locationLevel(Request $request)
    {
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', 20);
        $level = array_get($input, 'level', '');

        $query = $this->locationUtillizationModel->getColumn('level', $level);
        $locLevels = $query->take($limit)->get();

        return ['data' => $locLevels];
    }

    public function locationAisle(Request $request)
    {
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', 20);
        $aisle = array_get($input, 'aisle', '');

        $query = $this->locationUtillizationModel->getColumn('aisle', $aisle);
        $locAisles = $query->take($limit)->get();

        return ['data' => $locAisles];
    }

}
