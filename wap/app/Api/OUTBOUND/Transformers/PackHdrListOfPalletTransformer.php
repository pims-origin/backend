<?php

namespace App\Api\OUTBOUND\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\PackHdr;

class PackHdrListOfPalletTransformer extends TransformerAbstract
{
    /**
     * @param PackHdr $packHdr
     * @return array
     */
    public function transform(PackHdr $packHdr)
    {
        return [
            'pack_hdr_id'       => $packHdr->pack_hdr_id,
            'pack_hdr_num'    => $packHdr->pack_hdr_num,
            'seq'    => $packHdr->seq,
            'odr_hdr_id'    => $packHdr->odr_hdr_id,
            'whs_id'    => $packHdr->whs_id,
            'cus_id'    => $packHdr->cus_id,
            'carrier_name'    => $packHdr->carrier_name,
            'sku_ttl'    => $packHdr->sku_ttl,
            'piece_ttl'    => $packHdr->piece_ttl,
            'pack_sts'    => $packHdr->pack_sts,
            'sts'    => $packHdr->sts,
            'ship_to_name'    => $packHdr->ship_to_name,
            'pallet_id'    => $packHdr->pallet_id,
            'pack_type'    => $packHdr->pack_type,
            'width'    => $packHdr->width,
            'height'    => $packHdr->height,
            'length'    => $packHdr->length,
            'pack_ref_id'    => $packHdr->pack_ref_id,
            'pack_dt_checksum'    => $packHdr->pack_dt_checksum,
        ];

    }
}
