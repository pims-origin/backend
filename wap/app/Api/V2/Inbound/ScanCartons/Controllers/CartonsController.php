<?php

namespace App\Api\V2\Inbound\ScanCartons\Controllers;

use App\Api\V2\Inbound\Models\AsnDtlModel;
use App\Api\V2\Inbound\Models\AsnHdrModel;
use App\Api\V2\Inbound\Models\EventTrackingModel;
use App\Api\V2\Inbound\Models\LocationModel;
use App\Api\V2\Inbound\Models\Log;
use App\Api\V2\Inbound\Models\VirtualCartonModel;
use App\Api\V2\Inbound\Models\VirtualCartonSumModel;
use App\Api\V2\Inbound\ScanCartons\Validators\InsertVirtualCartonValidator;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\Jobs\ScanCartonsJob;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use Wms2\UserInfo\Data;
use TCPDF;

/**
 * Class CartonsController
 *
 * @package App\Api\V2\Inbound\ScanCartons\Controllers
 */
class CartonsController extends AbstractController
{
    protected $asnHdrModel;
    protected $asnDtlModel;
    protected $insertVtlCtn;
    protected $vtlCtn;
    protected $vtlCtnSum;

    /**
     * CartonsController constructor.
     *
     * @param VirtualCartonModel $virtualCartonModel
     * @param VirtualCartonSumModel $virtualCartonSumModel
     * @param AsnDtlModel $AsnDtlModel
     */
    public function __construct(
        VirtualCartonModel $vtlCtn,
        VirtualCartonSumModel $vtlCtnSum,
        AsnDtlModel $asnDtlModel
    ) {
        $this->vtlCtn       = $vtlCtn;
        $this->vtlCtnSum    = $vtlCtnSum;
        $this->asnDtlModel  = $asnDtlModel;
        $this->insertVtlCtn = new InsertVirtualCartonValidator();
        $this->asnHdrModel  = new AsnHdrModel();
    }

    /**
     * @param Request $request
     * @param $whsId
     * @param $cusId
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function scanCartons(Request $request, $whsId, $cusId)
    {
        $timeStart = microtime(true);

        // get data from HTTP
        $input = $request->getParsedBody();

        //$this->insertVtlCtn->validate($input);
        $requireFieldsValidate = $this->insertVtlCtn->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        $asnDtlId    = array_get($input, 'asn_dtl_id', 0);
        $arrCtnsRfid = array_get($input, 'ctns_rfid', null);

        $asnDtl = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $asnDtlId]);
        if (empty($asnDtl)) {
            return $this->response->errorBadRequest("ASN detail doesn't exist!");
        }

        $asnHdrId = object_get($asnDtl, 'asn_hdr_id', 0);
        $itemId   = object_get($asnDtl, 'item_id', 0);
        $ctnrId   = object_get($asnDtl, 'ctnr_id', 0);

        // Check duplicate cartons rfid
        $uniqueRFIDs = array_unique($arrCtnsRfid);
        if(count($uniqueRFIDs) != count($arrCtnsRfid)){
            $duplicateRFID = array_unique(array_diff_assoc($arrCtnsRfid, $uniqueRFIDs));
            $msg = 'Duplicate Carton: ' . implode(', ', $duplicateRFID);
            $data = [
                'data'    => null,
                'message' => $msg,
                'code'    => 'WAP002',
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $ansHdr = $this->asnHdrModel->getFirstWhere([
                                                        'asn_hdr_id' => $asnHdrId,
                                                        'whs_id'     => $whsId,
                                                        'cus_id'     => $cusId,
                                                    ]);
        if(!$ansHdr){
            $msg = sprintf('The Asn detail %s is not belong to warehouse %s and customer %s', $asnDtlId, $whsId, $cusId);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }
        else {
            $asnSts = object_get($ansHdr, 'asn_sts', '');
            if ($asnSts == 'RE') {
                $msg = sprintf('Unable to scan new cartons for completed ASN!');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }

        $grHdr = $this->grHdrModel->getFirstWhere(['asn_hdr_id' => $asnHdrId , 'ctnr_id' => $ctnrId]);

        $grSts = object_get($grHdr, 'gr_sts', '');
        if($grSts == 'RE') {
            $msg = sprintf('Unable to scan new cartons for completed Good Receipt!');
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $owner  = array_get($ansHdr, 'asn_hdr_num', '');

        $transaction = $ctnrId;
        try {
            /**
             * Log
             */
            $url = "inbound/whs/{$whsId}/cus/{$cusId}/scan-cartons";
            Log::info($request, $whsId, [
                'evt_code'     => 'ACC',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Scan Cartons'
            ]);

            //validate carton RFID
            $ctnRfidInValid = [];
            $errorMsg = '';
            foreach ($arrCtnsRfid as $ctnsRfid) {
                $ctnRFIDValid = new RFIDValidate($ctnsRfid, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $ctnsRfid;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }

            //if has errors, return all error data and message
            if ($ctnRfidInValid) {
                $data = [
                    'data'    => $ctnRfidInValid,
                    'message' => $errorMsg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $vtlCtnDataCommon = [
                'asn_hdr_id'  => $asnHdrId,
                'asn_dtl_id'  => $asnDtlId,
                'item_id'     => $itemId,
                'ctnr_id'     => $ctnrId,
                'cus_id'      => $cusId,
                'whs_id'      => $whsId,
                'scanned'     => '1',
                'is_damaged'  => '0',
                'vtl_ctn_sts' => Status::getByValue('NEW', 'VIRTUAL-CARTON-STATUS'),
            ];

            $dataVtlCtnSum = [
                'asn_hdr_id'      => $asnHdrId,
                'asn_dtl_id'      => $asnDtlId,
                'cus_id'          => $cusId,
                'whs_id'          => $whsId,
                'item_id'         => $itemId,
                'ctnr_id'         => $ctnrId,
                'discrepancy'     => null,
                'vtl_ctn_sum_sts' => Status::getByValue('RECEIVING', 'VIRTUAL-CARTON-SUMMARY-STATUS'),
            ];

            //get vtl ctn by ans hdr id
            $existAsnHdr = $this->vtlCtnSum->getFirstWhere(
                [
                    'asn_hdr_id' => $asnHdrId,
                    'asn_dtl_id' => $asnDtlId,
                ]
            );

            $vtlCtnSumSts = object_get($existAsnHdr, 'vtl_ctn_sum_sts', '');

            if (trim($vtlCtnSumSts) == Status::getByValue('RECEIVED', 'VIRTUAL-CARTON-SUMMARY-STATUS')) {
                $msg = sprintf(MessageCode::get('WAP005'), $asnDtlId);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'code'    => 'WAP005',
                    'status'  => false,
                ];

                $this->setData($data);
                $this->setMessage($msg);
                Log::error($request, $whsId, [
                    'evt_code'      => 'EAN',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);

                throw new \Exception($this->getMessage());
            }

            //get vtl ctn by vtl ctn ctn_rfid
            $vtlCtnObj = $this->vtlCtn->getModel()->whereIn('ctn_rfid', $arrCtnsRfid)->get();

            if (count($vtlCtnObj) > 0) {
                //insert virtual carton summary
                $existedVtlCtn = array_pluck($vtlCtnObj, 'ctn_rfid', null);
                $duplicateCtn  = array_diff($arrCtnsRfid, array_diff($arrCtnsRfid, $existedVtlCtn));
                $this->setData(['ctns_rfid' => $duplicateCtn]);
                $msg = 'There are cartons existed.';
                $this->setMessage($msg);

                Log::error($request, $whsId, [
                    'evt_code'      => 'EDC',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);

                $data = [
                    'data'    => ['ctns_rfid' => $duplicateCtn],
                    'message' => $msg,
                    'code'    => 'WAP002',
                    'status'  => false,
                ];

                return $this->response->noContent()
                    ->setContent([
                        'data' => [
                            'status'  => false,
                            'message' => $msg
                        ]
                    ])
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }

            $user = (new Data())->getUserInfo();

            $input['whs_id']     = $whsId;
            $input['cus_id']     = $cusId;
            $input['asn_hdr_id'] = $asnHdrId;
            $input['user_id']    = $user['user_id'];

            //add Queue
            $this->dispatch(new ScanCartonsJob($input, $dataVtlCtnSum, $vtlCtnDataCommon));

            $timeEnd = microtime(true);
            Log::info($request, $whsId, [
                'evt_code'     => 'EXE',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => $timeEnd - $timeStart,
            ]);

            $msg = sprintf("Add new cartons has been scanned successfully!");

            return $this->response->noContent()
                ->setContent([
                    'data' => [
                        'status'  => true,
                        'message' => $msg
                    ]
                ])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => "inbound/whs/{$whsId}/cus/{$cusId}/add-virtual-cartons",
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return ['data' => $this->getResponseData()];
            //return new Response($this->getResponseData(), 200, [], null);
        }
    }
}