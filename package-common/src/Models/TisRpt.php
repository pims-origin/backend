<?php

namespace Seldat\Wms2\Models;


class TisRpt extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tis_rpt';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'tis_rpt_id';

    /**
     * @var array
     */
    protected $fillable = [
        'tis_rpt_id',
        'tis_dtl_id',
        'tis_dtl_name',
        'rfid',
        'whs_id',
        'sku',
        'size',
        'color',
        'lot',
        'item_id',
        'sys_qty',
        'act_qty',
        'sts',
    ];

    public function tisDtl()
    {
        return $this->hasOne(__NAMESPACE__ . '\TisDtl', 'tis_dtl_id', 'tis_dtl_id');
    }
}
