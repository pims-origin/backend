<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;

class VasDtl extends BaseInvoiceModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vas_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'vas_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'vas_id',
        'cus_sac_id',
        'cus_id',
        'whs_id',
        'ref_num',
        'ref_type',
        'vas_item',
        'vas_description',
        'qty',
        'unit_price',
        'currency',
        'discount',
        'amount',
    ];

    public function vasHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\VasHdr', 'vas_id', 'vas_id');
    }

    public function scopeOrder($query)
    {
        return $query->where('ref_type', 'ORD');
    }
}
