<?php

namespace App\Api\ReplenishmentConfig\Controllers;

//use Seldat\Wms2\Models\PdHdr;
use App\Api\ReplenishmentConfig\Models\PalletModel;
use App\Api\ReplenishmentConfig\Models\CartonModel;
use App\Api\ReplenishmentConfig\Models\LocationModel;
use App\Api\ReplenishmentConfig\Models\ReplenishmentConfigModel;
use App\Api\ReplenishmentConfig\Validators\ReplenishmentConfigValidator;
use App\Api\ReplenishmentConfig\Validators\ReplenishmentConfigGetNumValidator;
use App\Api\ReplenishmentConfig\Transformers\ReplenishmentConfigTransformer;
use App\Api\ReplenishmentConfig\Transformers\SuggestLocationForRepConfTransformer;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Psr\Http\Message\ServerRequestInterface as Request;
use \Predis\Client;
use Wms2\UserInfo\Data;

class ReplenishmentConfigController extends AbstractController
{
    protected $_redis;
    protected $_cacheKey;
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var ReplenishmentConfigModel
     */
    protected $replenishmentConfigModel;


    /**
     * ReplenishmentConfigController constructor.
     *
     * @param ReplenishmentConfigModel $replenishmentConfigModel
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     */
    public function __construct(
        ReplenishmentConfigModel $replenishmentConfigModel,
        PalletModel $palletModel,
        CartonModel $cartonModel,
        LocationModel $locationModel
    ) {
        $this->replenishmentConfigModel = $replenishmentConfigModel;
        $this->palletModel = $palletModel;
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;

    }


    /**
     * @param Request $request
     * @param ReplenishmentConfigTransformer $replenishmentConfigTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getList(
        Request $request,
        ReplenishmentConfigTransformer $replenishmentConfigTransformer
    ) {
        $input = $request->getQueryParams();
        try {
            // get list replenishment config
            $replenishmentConfig = $this->replenishmentConfigModel->search($input,
                [],
                array_get($input, 'limit'))->toArray();

            $arrItems = [];
            if ($replenishmentConfig){
                foreach($replenishmentConfig as $replenishmentCon){
                    $updateAt = array_get($replenishmentCon, 'updated_at', '');
                    $min = array_get($replenishmentCon, 'min', 0);
                    $max = array_get($replenishmentCon, 'max', 0);
                    $locId = array_get($replenishmentCon, 'loc_id', '');
                    //$currentQty = object_get($replenishmentCon, 'current_qty', 0);
                    $locWithOneSKU = DB::table('cartons')
                        ->select([
                            DB::raw("COUNT(ctn_id) AS ctnNo")
                        ])
                        ->where('loc_id', $locId)
                        ->first();
                    $currentQty = array_get($locWithOneSKU, 'ctnNo', 0);

                    $transferQty = 0;
                    if ($currentQty < $min) {
                        $transferQty = $max - $currentQty;
                    }
                    if($min > $currentQty){
                        $arrItems[] = [
                            'id'           => array_get($replenishmentCon, 'id', ''),
                            'cus_id'       => array_get($replenishmentCon, 'cus_id', ''),
                            'cus_name'     => array_get($replenishmentCon, 'cus_name', ''),
                            'whs_id'       => array_get($replenishmentCon, 'whs_id', ''),
                            'loc_id'       => $locId,
                            'loc_code'     => array_get($replenishmentCon, 'loc_code', ''),
                            'item_id'      => array_get($replenishmentCon, 'item_id', ''),
                            'sku'          => array_get($replenishmentCon, 'sku', ''),
                            'min'          => $min,
                            'max'          => $max,
                            'current_qty'  => $currentQty,
                            //'transfer_qty' => object_get($replenishmentCon, 'transfer_qty', 0),
                            'transfer_qty' => $transferQty,
                            'uom_id'     => array_get($replenishmentCon, 'uom_id', ''),
                            'uom_code'     => array_get($replenishmentCon, 'uom_code', ''),
                            'uom_name'     => array_get($replenishmentCon, 'uom_name', ''),
                            'des'          => array_get($replenishmentCon, 'des', ''),
                            'updated_at'   => (empty($updateAt)) ? '' : date("Y-m-d", strtotime($updateAt)),
                            'updated_by'   => array_get($replenishmentCon, 'updated_by', ''),
                        ];
                    }

                }
            }

            return [
                'data' => $arrItems
            ];


            //return $this->response->collection($replenishmentConfig, $replenishmentConfigTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_SYSTEM, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $repConfId
     * @param ReplenishmentConfigTransformer $replenishmentConfigTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(
        Request $request,
        $repConfId,
        ReplenishmentConfigTransformer $replenishmentConfigTransformer
    ) {
        try {
            // get list Message
            $replenishmentConfig = $this->replenishmentConfigModel->showDetail($repConfId);
            // check Special Handle of Location and SKU
            if (empty($replenishmentConfig)) {
                throw new \Exception(Message::get("BM017", 'Replenishment Config'));
            }

            return $this->response->item($replenishmentConfig, $replenishmentConfigTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_SYSTEM, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function getCartonNumberOfLoc(
        Request $request
    ) {
        try {
            $predis = new Data();
            $userInfo = $predis->getUserInfo();
            $userId = array_get($userInfo, 'user_id', 0);
            $currentWH = array_get($userInfo, 'current_whs', 0);

            $input = $request->getQueryParams();
            // validation
            $replenishmentConfigGetNumValidator = new ReplenishmentConfigGetNumValidator();
            $replenishmentConfigGetNumValidator->validate($input);

            $locCode = array_get($input, 'loc_code', '');
            $itemId = array_get($input, 'item_id', '');
            //get loc_id from
            $locFromInfo = DB::table('location')
                ->select([
                    'location.loc_id'
                ])
                ->where('loc_code', array_get($input, 'loc_code', ''))
                ->where('loc_whs_id', $currentWH)
                ->first();
            $locCodeId_from = array_get($locFromInfo, 'loc_id', '');

            //check
            $newLocationInfo = $replenishmentConfigGetNumValidator->check($locCodeId_from);

            $cartonInfoOfLoc = $this->replenishmentConfigModel->getCartonNumberOfLoc($locCode, $currentWH, $itemId);
            $cartonNumberOfLoc = array_get($cartonInfoOfLoc, 'ctnNo', 0);

            return ['data' => $cartonNumberOfLoc];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_SYSTEM, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param SuggestLocationForRepConfTransformer $suggestLocationForRepConfTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function suggestLocation(
        Request $request,
        SuggestLocationForRepConfTransformer $suggestLocationForRepConfTransformer
    ) {
        try {
            $predis = new Data();
            $userInfo = $predis->getUserInfo();
            $userId = array_get($userInfo, 'user_id', 0);
            $currentWH = array_get($userInfo, 'current_whs', 0);

            $input = $request->getQueryParams();

            $cusId = array_get($input, 'cus_id', '');
            $itemId = array_get($input, 'item_id', '');
            $locIdTo = array_get($input, 'loc_id_to', '');
            $suggestLocationList = $this->replenishmentConfigModel->suggestLocation($cusId, $currentWH, $itemId, $locIdTo);

            return $this->response->collection($suggestLocationList, $suggestLocationForRepConfTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_SYSTEM, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    //private function generatePullDownNum()
    //{
    //    $this->model = new PdHdr;
    //    $currentYearMonth = date('ym');
    //    $defaultWoNum = "PD-${currentYearMonth}-00001";
    //    $pullDownLatest = $this->model->latest()->first();
    //
    //    $lastNum = $pullDownLatest->pd_hdr_num ?? '';
    //
    //    if (empty($lastNum) || strpos($lastNum, "-${currentYearMonth}-") === false) {
    //        return $defaultWoNum;
    //    }
    //
    //    return ++$lastNum;
    //
    //}

    /**
     * @param $request
     *
     * @return array|void
     */
    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $replenishmentConfigValidator = new ReplenishmentConfigValidator();
        $replenishmentConfigValidator->validate($input);

        try {
            DB::beginTransaction();

            $predis = new Data();
            $userInfo = $predis->getUserInfo();
            $userId = array_get($userInfo, 'user_id', 0);
            $currentWH = array_get($userInfo, 'current_whs', 0);

            //Create pd_hdr
            //$pd_hdr_params = [
            //    'picker_id'  => $userId,
            //    'loc_ttl'    => 1,
            //    'type'       => 'LOC',
            //    'pd_sts'     => 'COM',
            //    'created_at' => time(),
            //    'updated_at' => time(),
            //    'created_by' => $userId,
            //    'updated_by' => $userId,
            //    'deleted_at' => 915148800,
            //    'deleted'    => 0,
            //    'due_dt'     => time(),
            //    'pd_hdr_num' => $this->generatePullDownNum(),
            //    'whs_id'     => $currentWH,
            //    'pd_type'    => 'WAT',
            //];
            //$pd_hdr_Id = DB::table('pd_hdr')->insertGetId($pd_hdr_params);

            //Create pd_dtl
            //get loc_id from
            $locFromInfo = DB::table('location')
                ->select([
                    'location.loc_id'
                ])
                ->where('loc_code', array_get($input, 'loc_code_from', ''))
                ->where('loc_whs_id', $currentWH)
                ->first();
            //get pack from item
            $itemInfo = DB::table('item')
                ->select([
                    'item.pack'
                ])
                ->where('item_id', array_get($input, 'item_id', ''))
                ->first();
            //get lot from carton
            $cartonInfo = DB::table('cartons')
                ->select([
                    'cartons.lot',
                    'cartons.cus_id'
                ])
                ->where('loc_id', array_get($input, 'loc_id_to', ''))
                ->where('item_id', array_get($input, 'item_id', ''))
                ->first();

            //$pd_dtl_params = [
            //    'pd_hdr_id'     => $pd_hdr_Id,
            //    'item_id'       => array_get($input, 'item_id', ''),
            //    'sku'           => array_get($input, 'sku', ''),
            //    'size'          => 'NA',
            //    'color'         => 'NA',
            //    'lot'           => array_get($cartonInfo, 'lot', 'NA'),
            //    'pack'          => array_get($itemInfo, 'pack', ''),
            //    'des'           => array_get($input, 'des', ''),
            //    'from_loc_id'   => array_get($locFromInfo, 'loc_id', ''),
            //    'from_loc_code' => array_get($input, 'loc_code_from', ''),
            //    'to_loc_id'     => array_get($input, 'loc_id_to', ''),
            //    'to_loc_code'   => array_get($input, 'loc_code_to', ''),
            //    'created_by'    => $userId,
            //    'updated_by'    => $userId,
            //    'created_at'    => time(),
            //    'updated_at'    => time(),
            //    'deleted_at'    => 915148800,
            //    'deleted'       => 0,
            //    'req_ctns'      => array_get($input, 'transfer_qty', 0),
            //    'req_qty'       => array_get($input, 'transfer_qty', '') * array_get($itemInfo, 'pack', 0),
            //    'act_ctns'      => array_get($input, 'picking_qty', 0),
            //    'act_qty'       => array_get($input, 'picking_qty', 0) * array_get($itemInfo, 'pack', 0),
            //
            //
            //];

            //------------update Current Qty on Replenishment Config
            //$cusId = array_get($cartonInfo, 'cus_id', 'NA');
            //$locId = array_get($input, 'loc_id_to', 0);
            //$itemId = array_get($input, 'item_id', 0);
            //$pickingQty = array_get($input, 'picking_qty', 0);
            //
            ////get replenishment_config_ID
            //$replenishmentConfigInfo = DB::table('replenishment_config')
            //    ->select([
            //            'replenishment_config.id',
            //            'replenishment_config.current_qty',
            //
            //        ]
            //    )
            //    ->where('cus_id', $cusId)
            //    ->where('loc_id', $locId)
            //    ->where('item_id', $itemId)
            //    ->where('whs_id', $currentWH)
            //    ->where('deleted', 0)
            //    ->first();
            //$currentQty = array_get($replenishmentConfigInfo, 'current_qty', 0);
            //$id = array_get($replenishmentConfigInfo, 'id', 0);
            //
            //$RepConf_params = [
            //    'current_qty'  => $currentQty + $pickingQty
            //
            //];
            //
            //DB::table('replenishment_config')
            //    ->where('id', $id)
            //    ->update($RepConf_params);
            //------------/update Current Qty on Replenishment Config

            $fromLocId = array_get($locFromInfo, 'loc_id', '');
            $toLocId = array_get($input, 'loc_id_to', '');

            //1.------check validate
            $newLocationInfo = $replenishmentConfigValidator->check($fromLocId, $toLocId);
            $toLocName = object_get($newLocationInfo, 'loc_alternative_name', '');
            $toLocCode = object_get($newLocationInfo, 'loc_code', null);

            //2.------Create Pull Down
            //$pd_dtlId = DB::table('pd_dtl')->insertGetId($pd_dtl_params);

            //3.------Update carton
            // pallet from Location
            $palletFromLoc = $this->palletModel->getFirstWhere(['loc_id' => $fromLocId]);
            $CurrentWhsId = object_get($palletFromLoc, 'whs_id', 0);
            $CurrentCusId = object_get($palletFromLoc, 'cus_id', 0);

            //get PalletId according to LocationID and check The new location does not have any pallet!
            $palletToLoc = $this->palletModel->getFirstWhere(['loc_id' => $toLocId]);
            if (empty($palletToLoc)
            ) {
                throw new \Exception(Message::get("BM024"));
            }
            $plt_id = object_get($palletToLoc, 'plt_id', 0);
            $data = [
                'loc_id'   => $toLocId,
                'loc_name' => $toLocName,
                'plt_id'   => $plt_id,
                'loc_code' => $toLocCode,
            ];

            // Get customer the same zone from location
            $cusIds = $this->locationModel->getCusIdsByLocIds($CurrentWhsId, $fromLocId)->toArray();
            $cusIds = array_column($cusIds, 'cus_id');

            $locations = $this->locationModel->getLocationByCus($CurrentWhsId, $toLocId, $cusIds)->toArray();
            if (empty($locations)) {
                //$msg = sprintf('The location %s not belonged to this customer', $input['to_loc_code']);
                $msg = 'Current Location and New Location do not belong to the same customer';
                throw new \InvalidArgumentException($msg);
            }

            //get carton_IDs according to Tranfer_num of loc_from
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $pickingQty = array_get($input, 'picking_qty', '');

            $cartonAccLocFrom = DB::table('cartons')
                ->select([
                    'cartons.ctn_id'
                ])
                ->where('loc_id', array_get($locFromInfo, 'loc_id', ''))
                ->orderBy('cartons.ctn_id')
                ->take($pickingQty)->get();

            $ctnFromIds = array_pluck($cartonAccLocFrom, 'ctn_id');

            //$this->cartonModel->updateWhereIn($data, $input['ctn_id'], 'ctn_id');
            $this->cartonModel->updateWhereIn($data, $ctnFromIds, 'ctn_id');

            //4.------Update Pallet
            // update total carton pallet_to
            $this->palletModel->updateCartonTotalByPalletId($plt_id);
            // update total carton pallet_from
            $this->palletModel->updateCartonTotalByLocId($fromLocId);

            $carton = $this->cartonModel->getCartonWithNewLocation($ctnFromIds);
            $this->palletModel->updateZeroPalletReplenishConfig($fromLocId);
            $this->locationModel->removeDynamicZone($fromLocId);


            DB::commit();

            return ["data" => 'Successful'];

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                $e->getMessage() . " " . $e->getLine()
            //SystemBug::writeSysBugs($e, SysBug::API_WORK_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage() . " ");
        }
    }


}
