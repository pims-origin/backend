<?php

namespace App\Api\V1\Controllers;

use App\Api\Respositories\BillableDetailRepository;
use App\Api\Respositories\InvoiceHeaderRepository;
use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Rules\InvoiceRuleAddPayment;
use App\Api\V1\Rules\InvoiceRuleEdit;
use App\Api\V1\Rules\InvoiceRuleSendEmail;
use App\Api\V1\Transformers\BillableItemTransform;
use App\Api\V1\Transformers\InvoiceEditTransform;
use App\Api\V1\Transformers\InvoiceIndexTransform;
use App\Api\V1\Transformers\InvoiceShowTransform;
use App\Api\V1\Validators\InvoiceValidator;
use App\Wms2\UserInfo\Data;
use Illuminate\Http\Request as IlluminateRequest;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\V1\Models\InvoiceHdrModel;
use Validator;
use Mpdf\Mpdf;
use App\InvoiceHeader;
use Illuminate\Support\Facades\Mail;
use App\Mail\Invoice as InvoiceMail;
use Seldat\Wms2\Models\CustomerContact;
use App\Api\V1\Models\EmailConfigurationModel;
use App\Helpers\SettingMailHelper;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CustomerMeta;
use App\Api\V1\Models\CustomerConfigModel;
use Seldat\Wms2\Utils\Message;

class InvoiceController extends AbstractController
{

    /**
     * [$invoiceHeaderRepository description]
     *
     * @var [type]
     */
    protected $invoiceHeaderRepository;
    /**
     * [$billableDetailRepository description]
     *
     * @var [type]
     */
    protected $billableDetailRepository;
    /**
     * [$invoiceValidator description]
     *
     * @var [type]
     */
    protected $invoiceValidator;
    protected $settingMailHelper;
    protected $invoiceHdrModel;

    /**
     * InvoiceController constructor.
     *
     * @param InvoiceHeaderRepository $invoiceHeaderRepository
     * @param BillableDetailRepository $billableDetailRepository
     * @param InvoiceValidator $invoiceValidator
     * @param InvoiceHdrModel $invoiceHdrModel
     */
    public function __construct(
        InvoiceHeaderRepository $invoiceHeaderRepository,
        BillableDetailRepository $billableDetailRepository,
        InvoiceValidator $invoiceValidator,
        InvoiceHdrModel $invoiceHdrModel
    ) {
        $this->invoiceHeaderRepository = $invoiceHeaderRepository;
        $this->billableDetailRepository = $billableDetailRepository;
        $this->invoiceValidator = $invoiceValidator;
        $this->emailConfiguration = new EmailConfigurationModel;
        $this->settingMailHelper = new SettingMailHelper;
        $this->invoiceHdrModel = $invoiceHdrModel;
        $this->customerConfig = new CustomerConfigModel;        
    }

    /**
     * @SWG\Post(
     *     path="/invoice/create",
     *     tags={"Invoice"},
     *     summary="Create Invoice",
     *     description="Create Invoice",
     *     operationId="createInvoice",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    /**
     * [store description]
     *
     * @param  IlluminateRequest $request [description]
     *
     * @return [type]                     [description]
     */
    public function store(IlluminateRequest $request)
    {
        $validator = $this->invoiceValidator->makeValidator($request->all());
        // $validator->after(function ($validator) {
        //     if ($this->validateSacUnique($validator->getData())) {
        //         $validator->errors()->add('sac_unique', 'Service Accounting code must be unique in Invoice.');
        //     }
        // });
        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->errors()->first());
        }

        try {
            if (!$this->invoiceHeaderRepository->createInvoice($request)) {
                return $this->response->errorBadRequest('Can\'t create invoice');
            }

            return new IlluminateResponse(['data' => 'success.'], 200);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * [validateSacUnique description]
     *
     * @param  [type] $attributes [description]
     *
     * @return [type]             [description]
     */
    // private function validateSacUnique(array $attributes)
    // {
    //     $counter = 0;
    //     $items      = array_get($attributes, 'items', []);
    //     $sacUniques    = array_filter(array_pluck($items, 'sac_unique'));
    //     $sacFilter = [];
    //     $sacGroupFilter = [];
    //     foreach ($sacUniques as $value) {
    //         $array = explode(':', $value);
    //         $sacFilter[$array[0]][] = $array[1];
    //         if (count($array) > 2) {
    //             $sacGroupFilter[$array[0]][] = $array[2]; 
    //         }
    //     }

    //     foreach ($sacGroupFilter as $value) {
    //         if(count(array_unique($value)) > 1)
    //         return true;
    //     } 

    //     foreach ($sacFilter as $value) {
    //         if (count(array_unique($value)) > 1) {
    //             return true;
    //         }
    //     }

    //     return false;
    // }

    /**
     * @SWG\Get(
     *     path="/invoice/show/{invId}",
     *     tags={"Invoice"},
     *     summary="Invoice Info",
     *     description="Get Invoice Info",
     *     operationId="getInvoiceInfo",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="invId",
     *         in="path",
     *         description="ID of Invoice",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Invoice Object",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    /**
     * [show description]
     *
     * @param  InvoiceShowTransform $invoiceShowTransform [description]
     * @param  IlluminateRequest $request [description]
     * @param  [type]               $invHdrId             [description]
     *
     * @return [type]                                     [description]
     */
    public function show(InvoiceShowTransform $invoiceShowTransform, IlluminateRequest $request, $invHdrId)
    {
        try {
            if (!$invoiceHeader = $this->invoiceHeaderRepository->getById($invHdrId, ['detail'])) {
                return $this->response->errorBadRequest('Invoice is not exist!');
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->item($invoiceHeader, $invoiceShowTransform);
    }

    /**
     * @SWG\Get(
     *     path="/invoice/list",
     *     tags={"Invoice"},
     *     summary="Search Invoice",
     *     description="Search Invoice",
     *     operationId="getInvoiceIndex",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cus_id",
     *         in="query",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="whs_id",
     *         in="query",
     *         description="ID of Warehouse",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="inv_num",
     *         in="query",
     *         description="Inv Num",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="issue_dt",
     *         in="query",
     *         description="Invoice Issue Date",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Limit",
     *         type="integer",
     *         default="20",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page",
     *         type="integer",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Invoice",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    /**
     * [index description]
     *
     * @param  InvoiceIndexTransform $invoiceIndexTransform [description]
     * @param  IlluminateRequest $request [description]
     *
     * @return [type]                                       [description]
     */
    public function index(InvoiceIndexTransform $invoiceIndexTransform, IlluminateRequest $request)
    {
        try {
            $invoiceHeaders = $this->invoiceHeaderRepository->getPagination($request);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->paginator($invoiceHeaders, $invoiceIndexTransform);

    }

    /**
     * [destroy description]
     *
     * @param  IlluminateRequest $request [description]
     *
     * @return [type]                     [description]
     */
    public function destroy(IlluminateRequest $request)
    {
        try {
            $invoiceHeader = $this->invoiceHeaderRepository->destroyInvoice($request);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return new IlluminateResponse(['data' => $invoiceHeader], 200);
    }

    /**
     * [getBillableItem description]
     *
     * @param  IlluminateRequest $request [description]
     * @param  BillableItemTransform $billableItemTransform [description]
     * @param  [type]                $cusId                 [description]
     *
     * @return [type]                                       [description]
     */
    public function getBillableItem(IlluminateRequest $request, BillableItemTransform $billableItemTransform, $cusId)
    {
        try {
            $request->merge(['cus_id' => $cusId]);
            if (!$billableItems = $this->billableDetailRepository->getPagination($request, ['sacModel'])) {
                return $this->response->errorBadRequest('Can\'t found!');
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->paginator($billableItems, $billableItemTransform);
    }

    /**
     * @SWG\Put(
     *     path="/invoice/delete/{invId}",
     *     tags={"Invoice"},
     *     summary="Delete Invoice Detail(s)",
     *     description="Delete Invoice Detail(s)",
     *     operationId="deleteInvoice",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="invId",
     *         in="path",
     *         description="ID of Invoice",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    /**
     * [destroyInvoiceDetail description]
     *
     * @param  IlluminateRequest $request [description]
     * @param  [type]            $invHdrId [description]
     *
     * @return [type]                      [description]
     */
    public function destroyInvoiceDetail(IlluminateRequest $request, $invHdrId)
    {
        $request->merge(['inv_id' => $invHdrId]);
        $validator = Validator::make($request->all(), [
            'inv_dtl_ids' => 'required',
            'inv_id'      => ['required', new InvoiceRuleEdit],
        ]);

        if ($validator->fails()) {
            $this->response->errorBadRequest($validator->errors()->first());
        }

        try {
            $invoiceHeader = $this->invoiceHeaderRepository->destroyInvoiceDetail($invHdrId, $request);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return new IlluminateResponse(['data' => $invoiceHeader], 200);
    }

    /**
     * [edit description]
     *
     * @param  IlluminateRequest $request [description]
     * @param  InvoiceEditTransform $invoiceEditTransform [description]
     * @param  [type]               $invHdrId             [description]
     *
     * @return [type]                                     [description]
     */
    public function edit(IlluminateRequest $request, InvoiceEditTransform $invoiceEditTransform, $invHdrId)
    {
        $request->merge(['inv_id' => $invHdrId]);
        $validator = Validator::make($request->all(), [
            'inv_id' => ['required', new InvoiceRuleEdit],
        ]);

        if ($validator->fails()) {
            $this->response->errorBadRequest($validator->errors()->first());
        }

        try {
            if (!$invoiceHeader = $this->invoiceHeaderRepository->getById($invHdrId, ['detail'])) {
                return $this->response->errorBadRequest('Invoice is not exist!');
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->item($invoiceHeader, $invoiceEditTransform);
    }

    /**
     * @SWG\Put(
     *     path="/invoice/update/{invId}",
     *     tags={"Invoice"},
     *     summary="Update Invoice",
     *     description="Update Invoice",
     *     operationId="updateInvoice",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="invId",
     *         in="path",
     *         description="ID of Invoice",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    /**
     * [update description]
     *
     * @param  IlluminateRequest $request [description]
     * @param  [type]            $invHdrId [description]
     *
     * @return [type]                      [description]
     */
    public function update(IlluminateRequest $request, $invHdrId)
    {
        $validator = $this->invoiceValidator->makeValidator($request->all());

        // $validator->after(function ($validator) {
        //     if ($this->validateSacUnique($validator->getData())) {
        //         $validator->errors()->add('sac_unique', 'Service Accounting code must be unique in Invoice.');
        //     }
        // });

        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->errors()->first());
        }

        try {
            $invoiceHeader = $this->invoiceHeaderRepository->updateInvoiceDetail($invHdrId, $request);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return new IlluminateResponse(['data' => $invoiceHeader], 200);
    }

    /**
     * [getCreatedByInInvoiceHeader description]
     *
     * @param  IlluminateRequest $request [description]
     *
     * @return [type]                     [description]
     */
    public function getCreatedByInInvoiceHeader(IlluminateRequest $request)
    {
        try {
            $invoiceHeaders = $this->invoiceHeaderRepository->getCustomerInvoiceHeader($request);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        $invoiceData = $invoiceHeaders->transform(function ($item) {
            return [
                'created_by'   => $item->created_by,
                'created_name' => $item->created_name,
            ];
        })->toArray();

        return new IlluminateResponse(['data' => $invoiceData], 200);
    }

    /**
     * @SWG\Post(
     *     path="/invoice/send-email/{indId}",
     *     tags={"Invoice"},
     *     summary="Send Invoice to Customer via Email",
     *     description="Send Invoice Email",
     *     operationId="sendInvoiceEmail",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="invId",
     *         in="path",
     *         description="ID of Invoice",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    public function sendEmail(IlluminateRequest $request, $invHdrId)
    {
        $invHdr = $this->invoiceHeaderRepository->getById($invHdrId);
        $to = array_map('trim', explode(',', $request->get('emails', '')));
        $content = $request->get('content', '');
        if (!$invHdr) {
            return new IlluminateResponse(['data' => 'Invoice is not exist.'], 500);
        }
        
        try {
            $file = storage_path() . '/pdf/' . $invHdr->inv_num . '.pdf';
            if (!file_exists($file)) {
                $this->generatePdf($invHdr);
            }

            $message = new InvoiceMail($invHdr, $file, $content);

            $whsConfig = $this->emailConfiguration->getFirstWhere([
                'whs_id'        => $invHdr->whs_id,
                'whs_qualifier' => 'wie'
            ]);

            if ($whsConfig) {
                $whsConfig = json_decode($whsConfig->whs_meta_value);
                if (!data_get($whsConfig, 'use_system_email', '')) {
                    $this->$this->settingMailHelper->setCustomConfig($whsConfig);
                }
            }
            
            // Send mail
            Mail::to($to)
                ->send($message);
            if (Mail::failures()) {
                return new IlluminateResponse(['data' => 'Delivery failed. Please try again!'], 500);
            }

            $invHdr->email_sts = 2;
            $invHdr->save();

            return new IlluminateResponse(['data' => 'Email sent.'], 200);
        } catch (\Exception $e) {
            return new IlluminateResponse(['data' => 'Delivery failed. Please check your stmp configuration'], 500);
        }


    }

    public function addPayment(IlluminateRequest $request, $invHdrId)
    {
        $request->merge(['inv_id' => $invHdrId]);

        $validator = Validator::make($request->all(), [
            'inv_id' => ['required', new InvoiceRuleAddPayment],
        ]);

        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->errors()->first());
        }

        try {
            $invoiceHeader = $this->invoiceHeaderRepository->getById($invHdrId);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return new IlluminateResponse(['data' => 'Email send success.'], 200);
    }

    public function approvement(IlluminateRequest $request, $invHdrId)
    {
        $request->merge(['inv_id' => $invHdrId]);

        $validator = Validator::make($request->all(), [
            'inv_id' => ['required', new InvoiceRuleAddPayment],
        ]);

        if ($validator->fails()) {
            return $this->response->errorBadRequest($validator->errors()->first());
        }

        try {
            $invoiceHeader = $this->invoiceHeaderRepository->getById($invHdrId);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return new IlluminateResponse(['data' => 'Email send success.'], 200);
    }

    public function approve(IlluminateRequest $request)
    {
        $ids = $request->all()['inv_ids'];
        try {
            $this->invoiceHeaderRepository->approve($ids);

            return new IlluminateResponse(['data' => 'success.'], 200);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @SWG\Get(
     *     path="/invoice/get-total",
     *     tags={"Invoice"},
     *     summary="Invoice Info",
     *     description="Get Invoice Total Amount",
     *     operationId="getInvoiceTotalAmount",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="invId",
     *         in="path",
     *         description="ID of Invoice",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Invoice Object",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    public function getTotal(IlluminateRequest $request)
    {
        $dueStart = date('Y-m-d');
        $dueEnd = date('Y-m-d', strtotime('+2 Week'));
        try {
            $invoiceHeaders = $this->invoiceHeaderRepository->getTotal($request);
            return new IlluminateResponse([
                'data' => [
                    'amount'     => $invoiceHeaders->sum('total'),
                    'amount_due' => $invoiceHeaders->where('due_dt_format', '>=', $dueStart)
                        ->where('due_dt_format', '<=', $dueEnd)
                        ->where('inv_sts', '=', 'AP')
                        ->sum('balance_amount'),
                    'total'      => $invoiceHeaders->count()
                ]
            ], 200);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function generatePdf(InvoiceHeader $invInfo)
    {
        $html = view('pdf.invoice', [
            'header'  => $invInfo,
            'details' => $invInfo->detail()->get()
        ])->render();
        // mpdf config
        $mpdf = new Mpdf([
            'margin_left'   => 20,
            'margin_right'  => 15,
            'margin_top'    => 35,
            'margin_bottom' => 25,
            'margin_header' => 10,
            'margin_footer' => 10
        ]);
        $mpdf->SetProtection(['print']);
        $mpdf->SetTitle("Seldat Inc. - Invoice");
        $mpdf->SetAuthor("Seldat Inc.");
        $mpdf->showWatermarkText = true;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output(storage_path() . '/pdf/' . $invInfo->inv_num . '.pdf', \Mpdf\Output\Destination::FILE);
    }

    public function print($invId)
    {
        $invInfo = $this->invoiceHeaderRepository->getById($invId);
        if ($invInfo) {
            $file = storage_path() . '/pdf/' . $invInfo->inv_num . '.pdf';
            if (!file_exists($file)) {
                $this->generatePdf($invInfo);
            }

            header("Content-type:application/pdf");
            header("Content-Disposition:attachment;filename='download.pdf'");
            readfile($file);
        }
    }

    /**
     * @SWG\Put(
     *     path="/invoice/change-status/{invId}",
     *     tags={"Invoice"},
     *     summary="Update Invoice Status",
     *     description="Update Status of Invoice",
     *     operationId="updateInvoiceStatus",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="invId",
     *         in="path",
     *         description="ID of Invoice",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    public function changeStatus(IlluminateRequest $request, $invId)
    {
        $input = $request->all();
        try {
            $this->invoiceHeaderRepository->changeStatus($invId, $input);
            $header = InvoiceHeader::findOrFail($invId);

            return new IlluminateResponse(['data' => 'success.'], 200);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/invoice/print/{invId}",
     *     tags={"Invoice"},
     *     summary="Print Invoice Info",
     *     description="Print Invoice Info Into PDF",
     *     operationId="getInvoiceInfoPDF",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="invId",
     *         in="path",
     *         description="ID of Invoice",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Invoice Object",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    /**
     * @param $invId
     * @param Request $request
     *
     * @return bool
     * @throws \Exception
     */
    public function printInvoice(
        $invId,
        Request $request
    ) {

        if (empty($invId)) {
            return false;
        }
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

        $invInfo = $this->invoiceHdrModel->printInvoice($invId);
        $invInfoDetails = $this->invoiceHdrModel->InvoiceDetail($invId);
        $cus_id = array_get($invInfo, 'cus_id', '');
        $custome_meta = new CustomerMetaModel();
        $invInfo['currency'] = $custome_meta->getCurrencyByCurrentWhs($cus_id);

        if (empty($invInfo)) {
            throw new \Exception(Message::getOnLang("The {0} is not existed!", "invoice"));
        }
        $dueDt = array_get($invInfo, 'due_dt', '') ? date('Y-m-d', array_get($invInfo, 'due_dt', '')) : '';
        $invoiceDt = array_get($invInfo, 'inv_dt', '') ? date('Y-m-d', array_get($invInfo, 'inv_dt', '')) : '';
        $uom_list = config('constants.unit_uom_name');

        $this->createInvoicePdfFile($invInfo, $invInfoDetails, $printedBy, $dueDt, $invoiceDt, $uom_list);

    }

    /**
     * @param $invInfo
     * @param $invInfoDetails
     * @param $printedBy
     * @param $dueDt
     * @param $invoiceDt
     */
    private function createInvoicePdfFile($invInfo, $invInfoDetails, $printedBy, $dueDt, $invoiceDt, $uom_list)
    {
        $pdf = new Mpdf();
        $html = (string)view('InvoivePrintOutTemplate', [
            'invInfo'        => $invInfo,
            'invInfoDetails' => $invInfoDetails,
            'printedBy'      => $printedBy,
            'dueDt'          => $dueDt,
            'uom_list'       => $uom_list,
            'invoiceDt'      => $invoiceDt
        ]);

        $pdf->WriteHTML($html);
        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        //$pdf->Output("$odrNum.pdf", "D");
        $pdf->Output();
    }

    /**
     * @SWG\Get(
     *     path="/invoice/term/{cusId}",
     *     tags={"Invoice"},
     *     summary="Invoice Term of Customer",
     *     description="Get Invoice Term of Customer",
     *     operationId="getInvoiceTerm",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cusId",
     *         in="path",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Invoice Object",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    public function getInvoiceTerm($cusId)
    {
        $term = CustomerMeta::where([
            'cus_id' => $cusId,
            'qualifier' => 'CIO'
        ])->first();

        
        $data =  $term ? json_decode($term->value)->terms : 30;
        return new IlluminateResponse(['data' => $data], 200);
    }

    public function getCustomerEmailById($cusId)
    {
        if(!$cusId)
            return '';

        $config = $this->customerConfig->getInfo('email', $cusId);
        $value = $config ? json_decode($config->value) : false;

        return $value->customer_email ?? '';
    }
}
