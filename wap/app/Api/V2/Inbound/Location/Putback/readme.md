#########################-START-VERSION-2-##################################
- WAP-586: [Inbound - Putback] Create API submit putback carton
    + Support 2 option :
        * Has pallet
        * Empty pallet
    + Location follow 3 things:
        * Empty Location
        * Be able contain more cartons
        * Nearly Location
    + Just support location rfid and carton rfid.
#########################-END-VERSION-2-##################################