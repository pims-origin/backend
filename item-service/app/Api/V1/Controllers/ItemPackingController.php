<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\ItemService;
use Illuminate\Http\Request as Request;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class ItemPackingController extends AbstractController
{
    /**
     * @var ItemService
     */
    protected $service;

    /**
     * ItemController constructor.
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new ItemService($request);
    }


    public function create(Request $request)
    {
        $inputs = $request->all();
        $result = $this->service->createItemPacking($inputs);

        return $response = $this->convertResponse($result);
    }

    public function show($itemId)
    {
        $result = $this->service->showItemPacking($itemId);
        return $response = $this->convertResponse($result);
    }

}
