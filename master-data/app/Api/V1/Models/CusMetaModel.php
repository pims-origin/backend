<?php

namespace App\Api\V1\Models;

use Illuminate\Database\Eloquent\Model;

class CusMetaModel extends Model
{
    protected $table = 'cus_meta';
    protected $primaryKey = false;
    protected $fillable = [
        'cus_id',
        'value',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted',
        'deleted_at',
        'qualifier',
    ];
}
