<?php
namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

/**
 * Class WavepickHdr
 *
 * @property mixed cus_id
 * @property mixed whs_id
 * @property mixed wv_num
 * @property mixed wv_sts
 * @property mixed seq
 *
 * @package Seldat\Wms2\Models
 */
class WavepickHdr extends BaseInvoiceModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_hdr';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_id';
    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'wv_num',
        'wv_sts',
        'seq',
        'picker',
        'is_ecom'
    ];

    public $odrTtl ;
    public $SkuTtl ;

    public function orderHdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\OrderHdr', 'wv_id', 'wv_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function userCreated()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function details()
    {
        return $this->hasMany(__NAMESPACE__ . '\WavepickDtl', 'wv_id', 'wv_id');
    }

    public function pickerUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'picker');
    }
}
