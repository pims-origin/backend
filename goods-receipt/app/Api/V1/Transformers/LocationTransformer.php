<?php


namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Location;

class LocationTransformer extends TransformerAbstract
{
    //return result
    public function transform(Location $location)
    {
        return [
            'ctn_id' => null,
            'ctn_num' => null,
            'loc_id' => object_get($location, 'loc_id'),
            'loc_name' => object_get($location, 'loc_code'),
            'lpn_carton' => null,
            'is_lpn'  => 0,
        ];
    }

}
