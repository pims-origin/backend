<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 3:01 PM
 */

namespace App\Api\V1\Validators;


class SetupPasswordValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'email' => 'required|email',
            'setup_password_url' => 'required'
        ];
    }
}
