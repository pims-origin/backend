<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AbstractModel;
use App\Api\V1\Models\BOLOrderDetailModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\OdrHdrMetaModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\OutPalletModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\ShipmentModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Models\ThirdPartyModel;
use App\Api\V1\Models\UserMetaModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Models\WavepickDtlModel;
use App\Api\V1\Models\WavepickHdrModel;
use App\Api\V1\Models\WorkOrderDtlModel;
use App\Api\V1\Models\WorkOrderModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Traits\OrderFlowControllerTrait;
use App\Api\V1\Traits\OrderHdrControllerTrait;
use App\Api\V1\Transformers\OrderCancelListTransformer;
use App\Api\V1\Transformers\OrderDtlTransformer;
use App\Api\V1\Transformers\OrderHdrTransformer;
use App\Api\V1\Transformers\PackHdrTransformer;
use App\Api\V1\Validators\CsrValidator;
use App\Api\V1\Validators\OrderHdrValidator;
use App\Api\V1\Validators\OrderImportValidator;
use App\Api\V1\Validators\OrderMultiCSRValidator;
use App\Jobs\OrderFlowJob;
use App\Library\OrderImport;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Dingo\Api\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Http\Request as IRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WorkOrderHdr;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Profiler;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use TCPDF;
use Tymon\JWTAuth\Utils;
use Wms2\UserInfo\Data;
use mPDF;

use App\Jobs\TransferWarehouseIMSJob;

/**
 * Class OrderHdrController
 *
 * @package App\Api\V1\Controllers
 */
class OrderHdrController extends AbstractController
{
    use OrderHdrControllerTrait;
    use OrderFlowControllerTrait;

    const DELIVERY_ORDER_QUALIFIER = 'DLO';
    const ORDER_DOCUMENT_FOLDER = 'order-document';

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var ShippingOrderModel
     */
    protected $shippingOrderModel;

    /**
     * @var UserModel
     */
    protected $userModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var OrderImportValidator
     */
    protected $orderImportValidator;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var WavepickHdrModel
     */
    protected $wavepickHdrModel;

    /**
     * @var WavepickDtlModel
     */
    protected $wavepickDtlModel;

    /**
     * @var WorkOrderModel
     */
    protected $workOrderModel;

    /**
     * @var WorkOrderDtlModel
     */
    protected $workOrderDtlModel;

    /**
     * @var $customerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var $outPalletModel
     */
    protected $outPalletModel;

    /**
     * @var BOLOrderDetailModel
     */
    protected $bolOrderDetailModel;

    /**
     * @var Order Hdr Meta Model
     */
    protected $orderHdrMetaModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    protected $shipmentModel;

    /**
     * @var ThirdPartyModel
     */
    protected $thirdPartyModel;

    protected $userMetaModel;

    /**
     * OrderHdrController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param ShippingOrderModel $shippingOrderModel
     * @param UserModel $userModel
     * @param EventTrackingModel $eventTrackingModel
     * @param WarehouseModel $warehouseModel
     * @param ItemModel $itemModel
     * @param OrderImportValidator $orderImportValidator
     * @param PackHdrModel $packHdrModel
     * @param PackDtlModel $packDtlModel
     * @param WavepickHdrModel $wavepickHdrModel
     * @param WavepickDtlModel $wavepickDtlModel
     * @param WorkOrderDtlModel $workOrderDtlModel
     * @param WorkOrderModel $workOrderModel
     * @param CustomerConfigModel $customerConfigModel
     * @param OutPalletModel $outPalletModel
     * @param BOLOrderDetailModel $bolOrderDetailModel
     * @param ShipmentModel $shipmentModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        InventorySummaryModel $inventorySummaryModel,
        ShippingOrderModel $shippingOrderModel,
        UserModel $userModel,
        EventTrackingModel $eventTrackingModel,
        WarehouseModel $warehouseModel,
        ItemModel $itemModel,
        OrderImportValidator $orderImportValidator,
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel,
        WavepickHdrModel $wavepickHdrModel,
        WavepickDtlModel $wavepickDtlModel,
        WorkOrderDtlModel $workOrderDtlModel,
        WorkOrderModel $workOrderModel,
        CustomerConfigModel $customerConfigModel,
        OutPalletModel $outPalletModel,
        BOLOrderDetailModel $bolOrderDetailModel,
        ShipmentModel $shipmentModel
    ) {

        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->shippingOrderModel = $shippingOrderModel;
        $this->userModel = $userModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->warehouseModel = $warehouseModel;
        $this->itemModel = $itemModel;
        $this->orderImportValidator = $orderImportValidator;
        $this->packHdrModel = $packHdrModel;
        $this->wavepickHdrModel = $wavepickHdrModel;
        $this->wavepickDtlModel = $wavepickDtlModel;
        $this->workOrderDtlModel = $workOrderDtlModel;
        $this->workOrderModel = $workOrderModel;
        $this->packDtlModel = $packDtlModel;
        $this->customerConfigModel = $customerConfigModel;
        $this->outPalletModel = $outPalletModel;
        $this->bolOrderDetailModel = $bolOrderDetailModel;
        $this->orderHdrMetaModel = new OdrHdrMetaModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->shipmentModel = $shipmentModel;
        $this->thirdPartyModel = new ThirdPartyModel();
        $this->userMetaModel = new UserMetaModel();
    }


    /**
     * @param $orderId
     * @param OrderDtlTransformer $orderDtlTransformer
     * @param WarehouseModel $warehouseModel
     * @param CustomerModel $customerModel
     * @param SystemStateModel $systemStateModel
     * @param SystemCountryModel $systemCountryModel
     * @param WavepickHdrModel $wavepickHdrModel
     */
    public function printOrderPickingSlip(
        $orderId,
        OrderDtlTransformer $orderDtlTransformer,
        WarehouseModel $warehouseModel,
        CustomerModel $customerModel,
        SystemStateModel $systemStateModel,
        SystemCountryModel $systemCountryModel,
        WavepickHdrModel $wavepickHdrModel
    ) {

        try {
            $orderHdr = $this->orderHdrModel->getOrderHdrBy($orderId, AbstractModel::getCurrentWhsId());

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            //validate whs cus
            $this->chkWhsAndCus($orderHdr['whs_id'], $orderHdr['cus_id']);

            //get partial from customer config
            $customerConfigInfo2 = $this->customerConfigModel->getPartialCustomerConfigs(object_get($orderHdr, 'cus_id',
                ''));
            if ($customerConfigInfo2->toArray()) {
                $partial = true;
            } else {
                $partial = false;
            }
            $orderHdr->partial = $partial;
            $so_id = object_get($orderHdr, 'so_id', 0);
            $shippingInfo = $this->shippingOrderModel->getFirstWhere(['so_id' => $so_id]);
            $isEcoOrder = object_get($shippingInfo, 'type', '') == Status::getByValue('Ecomm',
                'ORDER-TYPE') ? true : false;

            $orderCartonInfos = $this->orderCartonModel->getOrderCartonBy(
                $orderHdr->odr_id,
                AbstractModel::getCurrentWhsId()
            );

            date_default_timezone_set('UTC');

            foreach ($orderCartonInfos as $key => $item) {
                $temp[$key] = [
                    'plt_num'       => array_get($item, 'plt_num', null),
                    'sku'           => array_get($item, 'sku', null),
                    'des'           => array_get($item, 'des', null),
                    'color'         => array_get($item, 'color', null),
                    'lot'           => array_get($item, 'lot', null),
                    'size'          => array_get($item, 'size', null),
                    'uom_name'      => array_get($item, 'uom_name', null),
                    'pack'          => array_get($item, 'pack', null),
                    'loc_code'      => array_get($item, 'loc_code', null),
                    'ctns_ttl'      => array_get($item, 'ctns_ttl', null),
                    'piece_qty_ttl' => array_get($item, 'piece_qty_ttl', null),
                    'updated_at'    => date("m/d/Y", array_get($item, 'updated_at', null)),
                    'picked_by'     => array_get($item, 'first_name', '') . ' ' . array_get($item, 'last_name', ''),

                ];

                $orderHdr['items'] = $temp;
            }

            $orderHdrArr = $orderHdr->toArray();

            $userInfo = new \Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            $totalSku = count(array_get($orderHdrArr, 'items', ''));
            $orderNum = array_get($orderHdrArr, 'odr_num', '');
            $odrSts = Status::getByKey("Order-status", array_get($orderHdrArr, 'odr_sts', ''));
            $odrType = Status::getByKey("Order-type", array_get($orderHdrArr, 'odr_type', ''));

            $warehouseInfo = $warehouseModel->getFirstBy('whs_id', $orderHdrArr['whs_id']);
            $warehouse = $warehouseInfo->whs_code . ' - ' . $warehouseInfo->whs_name;

            $customer = '';
            if ($customerInfo = $customerModel->getFirstBy('cus_id', $orderHdrArr['cus_id'])) {
                $customer = $customerInfo->cus_code . ' - ' . $customerInfo->cus_name;
            }

            $state = '';
            if ($stateInfo = $systemStateModel->getFirstBy('sys_state_code', $orderHdrArr['ship_to_state'])) {
                $state = $stateInfo->sys_state_name;
            }

            $country = '';
            if ($countryInfo = $systemCountryModel->getFirstBy('sys_country_code', $orderHdrArr['ship_to_country'])) {
                $country = $countryInfo->sys_country_name;
            }

            $wv_num = '';
            if ($wavePickInfo = $wavepickHdrModel->getFirstBy('wv_id', $orderHdrArr['wv_id'])) {
                $wv_num = $wavePickInfo->wv_num;
            }

            $this->printOrderPickingSlipPDFData($orderHdrArr, $totalSku, $orderNum, $printedBy,
                $odrSts, $warehouse, $customer, $odrType, $state, $country, $wv_num);

        } catch
        (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderHdrArr
     * @param $totalSku
     * @param $orderNum
     * @param $printedBy
     * @param $odrSts
     * @param $warehouse
     * @param $customer
     * @param $odrType
     * @param $state
     * @param $country
     *
     * @throws \MpdfException
     */
    private function printOrderPickingSlipPDFData(
        $orderHdrArr,
        $totalSku,
        $orderNum,
        $printedBy,
        $odrSts,
        $warehouse,
        $customer,
        $odrType,
        $state,
        $country,
        $wv_num
    ) {
        $pdf = new Mpdf('c', 'A4-L', $default_font_size = 0, $default_font = '', $mgl = 15, $mgr = 15, $mgt = 5,
            $mgb = 5, $mgh = 9, $mgf = 9, $orientation = 'P');
        $html = (string)view('OrderPickingSlipPrintoutTemplate', [
            'orderHdrArr' => $orderHdrArr,
            'totalSku'    => $totalSku,
            'orderNum'    => $orderNum,
            'printedBy'   => $printedBy,
            'odrSts'      => $odrSts,
            'warehouse'   => $warehouse,
            'customer'    => $customer,
            'odrType'     => $odrType,
            'state'       => $state,
            'country'     => $country,
            'wv_num'      => $wv_num
        ]);

        $pdf->WriteHTML($html);
        //$dir = storage_path("PutAway/$whsId");
        //$pdf->Output("$dir/$grHdrNum.pdf", 'F');
        $pdf->Output("$orderNum.pdf", "D");
    }

    /**
     * @param $orderId
     * @param OrderDtlTransformer $orderDtlTransformer
     * @param WarehouseModel $warehouseModel
     * @param CustomerModel $customerModel
     * @param SystemStateModel $systemStateModel
     * @param SystemCountryModel $systemCountryModel
     * @param WavepickHdrModel $wavepickHdrModel
     */
    public function printOrderPackingSlip(
        $orderId,
        OrderDtlTransformer $orderDtlTransformer,
        WarehouseModel $warehouseModel,
        CustomerModel $customerModel,
        SystemStateModel $systemStateModel,
        SystemCountryModel $systemCountryModel,
        WavepickHdrModel $wavepickHdrModel
    ) {

        try {
            $orderHdr = $this->orderHdrModel->getOrderHdrBy($orderId, AbstractModel::getCurrentWhsId());

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            //validate whs cus
            $this->chkWhsAndCus($orderHdr['whs_id'], $orderHdr['cus_id']);

            //get partial from customer config
            $customerConfigInfo2 = $this->customerConfigModel->getPartialCustomerConfigs(object_get($orderHdr, 'cus_id',
                ''));
            if ($customerConfigInfo2->toArray()) {
                $partial = true;
            } else {
                $partial = false;
            }
            $orderHdr->partial = $partial;
            $so_id = object_get($orderHdr, 'so_id', 0);
            $shippingInfo = $this->shippingOrderModel->getFirstWhere(['so_id' => $so_id]);
            $isEcoOrder = object_get($shippingInfo, 'type', '') == Status::getByValue('Ecomm',
                'ORDER-TYPE') ? true : false;

            $orderCartonInfos = $this->orderCartonModel->getOrderCartonBy(
                $orderHdr->odr_id,
                AbstractModel::getCurrentWhsId()
            );

            /** NOTE: Current timezone in another api is UTC, not same warehouse config */

            date_default_timezone_set('UTC');

            foreach ($orderCartonInfos as $key => $item) {
                $evt = DB::table('evt_tracking')->where('owner', $orderHdr->odr_num)->where('evt_code', 'OPS')->first();
                $temp[$key] = [
                    'plt_num'       => array_get($item, 'plt_num', null),
                    'sku'           => array_get($item, 'sku', null),
                    'des'           => array_get($item, 'des', null),
                    'color'         => array_get($item, 'color', null),
                    'lot'           => array_get($item, 'lot', null),
                    'size'          => array_get($item, 'size', null),
                    'uom_name'      => array_get($item, 'uom_name', null),
                    'pack'          => array_get($item, 'pack', null),
                    'loc_code'      => array_get($item, 'loc_code', null),
                    'ctns_ttl'      => ceil(array_get($item, 'piece_qty_ttl', null)),
                    'piece_qty_ttl' => ceil(array_get($item, 'piece_qty_ttl', null)),
                    'updated_at'    => date("m/d/Y", array_get($evt, 'created_at', null)),
                    'picked_by'     => array_get($item, 'first_name', '') . ' ' . array_get($item, 'last_name', ''),

                ];

                $orderHdr['items'] = $temp;
            }

            $orderHdrArr = $orderHdr->toArray();

            $userInfo = new \Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            $totalSku = count(array_get($orderHdrArr, 'items', ''));
            $orderNum = array_get($orderHdrArr, 'odr_num', '');
            $odrSts = Status::getByKey("Order-status", array_get($orderHdrArr, 'odr_sts', ''));
            $odrType = Status::getByKey("Order-type", array_get($orderHdrArr, 'odr_type', ''));

            $warehouseInfo = $warehouseModel->getFirstBy('whs_id', $orderHdrArr['whs_id']);
            $warehouse = $warehouseInfo->whs_code . ' - ' . $warehouseInfo->whs_name;

            $customer = '';
            if ($customerInfo = $customerModel->getFirstBy('cus_id', $orderHdrArr['cus_id'])) {
                $customer = $customerInfo->cus_code . ' - ' . $customerInfo->cus_name;
            }

            $state = '';
            if ($stateInfo = $systemStateModel->getFirstBy('sys_state_code', $orderHdrArr['ship_to_state'])) {
                $state = $stateInfo->sys_state_name;
            }

            $country = '';
            if ($countryInfo = $systemCountryModel->getFirstBy('sys_country_code', $orderHdrArr['ship_to_country'])) {
                $country = $countryInfo->sys_country_name;
            }

            $wv_num = '';
            if ($wavePickInfo = $wavepickHdrModel->getFirstBy('wv_id', $orderHdrArr['wv_id'])) {
                $wv_num = $wavePickInfo->wv_num;
            }

            $this->printOrderPackingSlipPDFData($orderHdrArr, $totalSku, $orderNum, $printedBy,
                $odrSts, $warehouse, $customer, $odrType, $state, $country, $wv_num);

        } catch
        (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderHdrArr
     * @param $totalSku
     * @param $orderNum
     * @param $printedBy
     * @param $odrSts
     * @param $warehouse
     * @param $customer
     * @param $odrType
     * @param $state
     * @param $country
     *
     * @throws \MpdfException
     */
    private function printOrderPackingSlipPDFData(
        $orderHdrArr,
        $totalSku,
        $orderNum,
        $printedBy,
        $odrSts,
        $warehouse,
        $customer,
        $odrType,
        $state,
        $country,
        $wv_num
    ) {
        $pdf = new \Mpdf\Mpdf(['c', 'A4-L', $default_font_size = 0, $default_font = '', $mgl = 15, $mgr = 15, $mgt = 5,
            $mgb = 5, $mgh = 9, $mgf = 9, $orientation = 'P']);
        $html = (string)view('OrderPackingSlipPrintoutTemplate', [
            'orderHdrArr' => $orderHdrArr,
            'totalSku'    => $totalSku,
            'orderNum'    => $orderNum,
            'printedBy'   => $printedBy,
            'odrSts'      => $odrSts,
            'warehouse'   => $warehouse,
            'customer'    => $customer,
            'odrType'     => $odrType,
            'state'       => $state,
            'country'     => $country,
            'wv_num'      => $wv_num
        ]);

        $pdf->WriteHTML($html);
        //$dir = storage_path("PutAway/$whsId");
        //$pdf->Output("$dir/$grHdrNum.pdf", 'F');
        $pdf->Output("$orderNum.pdf", "D");
    }

    /**
     * @param $orderId
     * @param OrderDtlTransformer $orderDtlTransformer
     * @param WarehouseModel $warehouseModel
     * @param CustomerModel $customerModel
     * @param SystemStateModel $systemStateModel
     * @param SystemCountryModel $systemCountryModel
     * @param WavepickHdrModel $wavepickHdrModel
     */
    public function printOrderRequestSlip(
        $orderId,
        OrderDtlTransformer $orderDtlTransformer,
        WarehouseModel $warehouseModel,
        CustomerModel $customerModel,
        SystemStateModel $systemStateModel,
        SystemCountryModel $systemCountryModel,
        WavepickHdrModel $wavepickHdrModel
    ) {

        try {
            $orderHdr = $this->orderHdrModel->getOrderHdrRequestBy($orderId, AbstractModel::getCurrentWhsId());

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            //validate whs cus
            $this->chkWhsAndCus($orderHdr['whs_id'], $orderHdr['cus_id']);

            //get partial from customer config
            $customerConfigInfo2 = $this->customerConfigModel->getPartialCustomerConfigs(object_get($orderHdr, 'cus_id',
                ''));
            if ($customerConfigInfo2->toArray()) {
                $partial = true;
            } else {
                $partial = false;
            }
            $orderHdr->partial = $partial;
            $so_id = object_get($orderHdr, 'so_id', 0);
            $shippingInfo = $this->shippingOrderModel->getFirstWhere(['so_id' => $so_id]);
            $isEcoOrder = object_get($shippingInfo, 'type', '') == Status::getByValue('Ecomm',
                'ORDER-TYPE') ? true : false;


            $orderCartonInfos = $this->orderDtlModel->getOrderDtlBy(
                $orderHdr->odr_id,
                AbstractModel::getCurrentWhsId()
            );


            foreach ($orderCartonInfos as $key => $item) {
                $temp[$key] = [
                    'sku'           => array_get($item, 'sku', null),
                    'des'           => array_get($item, 'des', null),
                    'lot'           => array_get($item, 'lot', null),
                    'uom_name'      => array_get($item, 'uom_name', null),
                    'pack'          => array_get($item, 'pack', null),
                    'ctns_ttl'      => array_get($item, 'ctns_ttl', null),
                    'piece_qty_ttl' => array_get($item, 'piece_qty_ttl', null),

                ];

                $orderHdr['items'] = $temp;
            }

            $orderHdrArr = $orderHdr->toArray();

            $userInfo = new \Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            $totalSku = count(array_get($orderHdrArr, 'items', ''));
            $orderNum = array_get($orderHdrArr, 'odr_num', '');
            $odrSts = Status::getByKey("Order-status", array_get($orderHdrArr, 'odr_sts', ''));
            $odrType = Status::getByKey("Order-type", array_get($orderHdrArr, 'odr_type', ''));

            $warehouseInfo = $warehouseModel->getFirstBy('whs_id', $orderHdrArr['whs_id']);
            $warehouse = $warehouseInfo->whs_code . ' - ' . $warehouseInfo->whs_name;

            $customer = '';
            if ($customerInfo = $customerModel->getFirstBy('cus_id', $orderHdrArr['cus_id'])) {
                $customer = $customerInfo->cus_code . ' - ' . $customerInfo->cus_name;
            }

            $state = '';
            if ($stateInfo = $systemStateModel->getFirstBy('sys_state_code', $orderHdrArr['ship_to_state'])) {
                $state = $stateInfo->sys_state_name;
            }

            $country = '';
            if ($countryInfo = $systemCountryModel->getFirstBy('sys_country_code', $orderHdrArr['ship_to_country'])) {
                $country = $countryInfo->sys_country_name;
            }

            $wv_num = '';
            if ($wavePickInfo = $wavepickHdrModel->getFirstBy('wv_id', $orderHdrArr['wv_id'])) {
                $wv_num = $wavePickInfo->wv_num;
            }

            $this->printOrderRequestSlipPDFData($orderHdrArr, $totalSku, $orderNum, $printedBy,
                $odrSts, $warehouse, $customer, $odrType, $state, $country, $wv_num);

        } catch
        (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderHdrArr
     * @param $totalSku
     * @param $orderNum
     * @param $printedBy
     * @param $odrSts
     * @param $warehouse
     * @param $customer
     * @param $odrType
     * @param $state
     * @param $country
     * @param $wv_num
     */
    private function printOrderRequestSlipPDFData(
        $orderHdrArr,
        $totalSku,
        $orderNum,
        $printedBy,
        $odrSts,
        $warehouse,
        $customer,
        $odrType,
        $state,
        $country,
        $wv_num
    ) {
        $pdf = new \Mpdf\Mpdf(['c', 'A4-L', $default_font_size = 0, $default_font = '', $mgl = 15, $mgr = 15, $mgt = 5,
            $mgb = 5, $mgh = 9, $mgf = 9, $orientation = 'P']);
        $html = (string)view('OrderRequestSlipPrintoutTemplate', [
            'orderHdrArr' => $orderHdrArr,
            'totalSku'    => $totalSku,
            'orderNum'    => $orderNum,
            'printedBy'   => $printedBy,
            'odrSts'      => $odrSts,
            'warehouse'   => $warehouse,
            'customer'    => $customer,
            'odrType'     => $odrType,
            'state'       => $state,
            'country'     => $country,
            'wv_num'      => $wv_num
        ]);

        $pdf->WriteHTML($html);
        //$dir = storage_path("PutAway/$whsId");
        //$pdf->Output("$dir/$grHdrNum.pdf", 'F');
        $pdf->Output("$orderNum.pdf", "D");
    }

    /**
     * @param $orderId
     * @param OrderDtlTransformer $orderDtlTransformer
     *
     * @return Response|void
     */
    public function show(
        $orderId,
        OrderDtlTransformer $orderDtlTransformer
    ) {
        try {
            $orderHdr = $this->orderHdrModel->getFirstWhere([
                'odr_id' => $orderId,
                'whs_id' => AbstractModel::getCurrentWhsId()
            ], ['orderSoldTo']);

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            //validate whs cus
            $this->chkWhsAndCus($orderHdr['whs_id'], $orderHdr['cus_id']);

            //get partial from customer config
            $customerConfigInfo2 = $this->customerConfigModel->getPartialCustomerConfigs(object_get($orderHdr, 'cus_id',
                ''));
            if ($customerConfigInfo2->toArray()) {
                $partial = true;
            } else {
                $partial = false;
            }
            $orderHdr->partial = $partial;
            $so_id = object_get($orderHdr, 'so_id', 0);
            $shippingInfo = $this->shippingOrderModel->getFirstWhere(['so_id' => $so_id]);
            /*$isEcoOrder = object_get($shippingInfo, 'type', '') == Status::getByValue('Ecomm',
                'ORDER-TYPE') ? true : false;*/

//            $orderDtl = $this->orderDtlModel
//                ->allBy('odr_id', $orderHdr->odr_id, ['inventorySummary.item', 'systemUom'])
//                ->toArray();

            $orderDtl = $this->orderDtlModel
                ->getModel()
                ->leftJoin('item', 'item.item_id', '=', 'odr_dtl.item_id')
                ->where('odr_dtl.odr_id', '=', $orderHdr->odr_id)
                ->select('odr_dtl.*', 'item.spc_hdl_code')
                ->with(['inventorySummary.item', 'systemUom'])
                ->get()
                ->toArray();

            $cartonModel = new CartonModel();
            $pallet=new PalletModel();
            foreach ($orderDtl as $key => $item) {
                $sumField = 'avail';
                if ($orderHdr->odr_type === 'XDK') {
                    $sumField = 'crs_doc_qty';
                }
                /*$invtSum = $this->inventorySummaryModel->getInvSumbyOdrDlt(
                    ['item_id' => $item['item_id']],
                    $item['lot'],
                    $orderHdr->whs_id,
                    $sumField
                );*/
                $inventory = DB::table("invt_smr")->where("whs_id",$orderHdr->whs_id)
                    ->where("cus_id",$orderHdr->cus_id)->where("item_id", $item['item_id'])
                    ->select("avail")->first();
                $avail = (!empty($inventory['avail']))?$inventory['avail']:0;
                $cartonEcom = $cartonModel->getFirstWhere([
                    'item_id' => $item['item_id'],
                    'is_ecom' => 1
                ]);
                
                $arrLot =$pallet->getLot($item['item_id']) ;
                //get is_ecom from carton
                //$ecom_qty = object_get($cartonEcom, 'piece_remain', 0);
                //$avail = $invtSum;
                //$avail = (!empty($inventory['in_hand_qty']))?$inventory['in_hand_qty']:0;
                $level = intval(array_get($item, 'system_uom.level_id', 0));
                $uom_qty = null;
                switch ($level) {
                    case 1:
                    case 2:
                        $uom_qty = $item['piece_qty'];
                        break;
                    case 3:
                        $uom_qty = $item['qty'];
                        break;
                    default:
                        break;
                }

                $onHandQty = (int)array_get($item, 'inventory_summary.avail', 0) - (int)array_get($item, 'inventory_summary.lock_qty', 0);
                $temp[$key] = [
                    'odr_dtl_id'    => $item['odr_dtl_id'],
                    'itm_id'        => $item['item_id'],
                    'sku'           => $item['sku'],
                    'spc_hdl_code'  => array_get($item, 'spc_hdl_code'),
                    'color'         => $item['color'],
                    'size'          => $item['size'],
                    'lot'           => $item['lot'],
                    'arrLot'        => $arrLot,
                    'uom_qty'       => $uom_qty,
                    'uom_id'        => $item['uom_id'],
                    'uom_code'      => array_get($item, 'system_uom.sys_uom_code', null),
                    'uom_name'      => array_get($item, 'system_uom.sys_uom_name', null),
                    'piece_qty'     => $item['piece_qty'],
                    'pack'          => $item['pack'],
                    'qty'           => $item['qty'],
                    'back_odr'      => $item['back_odr'],
                    'avail'         => $avail > 0 ? $avail : 0,
                    'allocated_qty' => $item['alloc_qty'],
                    'cus_upc'       => $item['cus_upc'],
                    'ship_track_id' => $item['ship_track_id'],
                    'uom_level'     => $level,
                    'picked_qty'    => $item['picked_qty'],
                    'on_hand_qty'   => $onHandQty > 0 ? $onHandQty : 0,
                    'prod_line'     => $item['prod_line'],
                    'cmp'           => $item['cmp'],
                    'hs_num'        => array_get($item, 'hs_num'),
                    'hs_unit'       => array_get($item, 'hs_unit'),
                    'price'         => array_get($item, 'price')
                ];

                $orderHdr['items'] = $temp;
            }

			// Order Sold To
			$orderSoldTo = null;
			if ($orderHdr->orderSoldTo) {
				$orderSoldTo = [
					'name' => $orderHdr->orderSoldTo->name,
					'street_address' => $orderHdr->orderSoldTo->street_address,
					'city' => $orderHdr->orderSoldTo->city,
					'country' => $orderHdr->orderSoldTo->country,
					'zip' => $orderHdr->orderSoldTo->zip,
					'contact_name' => $orderHdr->orderSoldTo->contact_name,
				];
			}
			unset($orderHdr->orderSoldTo);
			$orderHdr->orderSoldTo = $orderSoldTo;

            return $this->response->item($orderHdr, $orderDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        Request $request,
        OrderHdrTransformer $orderHdrTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $inputStatuses = array_get($input, 'odr_sts', []);

            $type = array_get($input, 'type', '');
            if ($type) {
                $userId = Data::getCurrentUserId();
                $this->userMetaModel->modifyUserMeta([
                    'user_id'   => $userId,
                    'qualifier' => config('constants.qualifier.ORDER-STATUS')
                ], $inputStatuses, $type);
            }

            //Export CSV
            if (!empty($input['export']) && $input['export'] == 1) {
                $this->exportGRCSV($input);
                die;
            }

            $orderHdr = $this->orderHdrModel->search($input, [
                'shippingOrder',
                'waveHdr',
                'details'
            ], array_get($input, 'limit'));
            $arrShipIds = array_pluck($orderHdr, 'ship_id');
            $arrShipIds = array_filter($arrShipIds);
            if ($arrShipIds) {
                $shipMents = $this->shippingOrderModel->getShipStsByShipIds($arrShipIds);
                if ($shipMents) {
                    $arrShipSts = [];
                    foreach ($shipMents as $shipMent) {
                        $arrShipSts[$shipMent->ship_id] = [
                            'ship_sts' => $shipMent->ship_sts,
                            'bo_num'   => $shipMent->bo_num
                        ];
                    }

                    foreach ($orderHdr as $order) {
                        if ($order->ship_id
                            && !empty($arrShipSts[$order->ship_id])
                        ) {
                            $order->ship_sts = array_get($arrShipSts[$order->ship_id], 'ship_sts', null);
                            $order->bo_num = array_get($arrShipSts[$order->ship_id], 'bo_num', null);
                        }
                    }
                }
            }

            foreach ($orderHdr as $order) {
                $orderHdrMeta = DB::table('odr_hdr_meta')->where('odr_id', $order->odr_id)->where('qualifier', 'DL0')->first();
                if($orderHdrMeta) {
                    $value = json_decode($orderHdrMeta['value']);
                    if(isset($value->driver_id)) {
                        $driver = DB::table('users')->where('user_id', $value->driver_id)->first();
                        $driverName = $driver['first_name'] . ' ' . $driver['last_name'];
                        $order->driver_name = $driverName;
                    }
                }
            }

            return $this->response->paginator($orderHdr, $orderHdrTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $input
     *
     * @return $this|void
     */
    private function exportGRCSV($input)
    {
        try {
            $orderHdr = $this->orderHdrModel->search($input, [
                'shippingOrder',
                'waveHdr',
                'details'
            ], null, true);

            $arrShipIds = array_pluck($orderHdr, 'ship_id');
            $arrShipIds = array_filter($arrShipIds);
            if ($arrShipIds) {
                $shipMents = $this->shippingOrderModel->getShipStsByShipIds($arrShipIds);
                if ($shipMents) {
                    $arrShipSts = [];
                    foreach ($shipMents as $shipMent) {
                        $arrShipSts[$shipMent->ship_id] = [
                            'ship_sts' => $shipMent->ship_sts,
                            'bo_num'   => $shipMent->bo_num
                        ];
                    }

                    foreach ($orderHdr as $order) {
                        if ($order->ship_id
                            && !empty($arrShipSts[$order->ship_id])
                        ) {
                            $order->ship_sts = array_get($arrShipSts[$order->ship_id], 'ship_sts', null);
                            $order->bo_num = array_get($arrShipSts[$order->ship_id], 'bo_num', null);
                        }
                    }
                }
            }

            $orderinfos = [];
            if ($orderHdr) {
                foreach ($orderHdr as $orderHd) {

                    $createdAt = date("m/d/Y", strtotime($orderHd->created_at));
                    $OrderInfo = new OrderHdrModel();
                    $workOrderHdrInfo = WorkOrderHdr::where('odr_hdr_id', $orderHd->odr_id)->first();
                    $statusName = Status::getByKey("Order-Status", $orderHd->odr_sts);
                    if ($orderHd->back_odr) {
                        $statusName = str_replace('Partial ', '', $statusName);
                        $statusName = 'Partial ' . $statusName;
                    }
                    $orderinfos[] = [
                        'odr_sts_name'  => $statusName,
                        'odr_num'       => object_get($orderHd, 'odr_num', null),
                        'cus_name'      => object_get($orderHd, 'customer.cus_code', null),
                        'odr_type_name' => Status::getByKey("Order-Type", object_get($orderHd, 'odr_type', null)),
                        'back_odr'      => object_get($orderHd, 'back_odr', null) ? 'YES' : "NO",
                        'bol'           => Status::getByKey("SHIP-STATUS", $orderHd->ship_sts)
                            ? Status::getByKey("SHIP-STATUS", $orderHd->ship_sts)
                            : 'No Bol',
                        'cus_odr_num'   => object_get($orderHd, 'cus_odr_num', null),
                        'cus_po'        => object_get($orderHd, 'cus_po', null),
                        'ship_to_name'  => $orderHd->ship_to_name,
                        'ship_by_dt'    => empty($orderHd->ship_by_dt) ? null :
                            date("m/d/Y", $orderHd->ship_by_dt),
                        'cancel_by_dt'  => empty($orderHd->cancel_by_dt) ? null :
                            date("m/d/Y", $orderHd->cancel_by_dt),
                        // 'act_cancel_dt' => empty($orderHd->act_cancel_dt) ? null :
                            // date("m/d/Y", $orderHd->act_cancel_dt),
                        'shipped_dt'    => empty($orderHd->shipped_dt) ? null :
                            date("m/d/Y", $orderHd->shipped_dt),
                        'csr_name'      => trim(object_get($orderHd, 'csrUser.first_name', null) . " " .
                            object_get($orderHd, 'csrUser.last_name', null)),
                        'created_by'    => trim(object_get($orderHd, "createdBy.first_name", null) . " " .
                            object_get($orderHd, "createdBy.last_name", null)),
                        'created_at'    => $createdAt,

                    ];
                }
            }

            $title = [
                'odr_sts_name'  => 'Status',
                'odr_num'       => 'Order Number',
                'cus_name'      => 'Customer',
                'odr_type_name' => 'Order Type',
                'back_odr'      => 'Back Order',
                'bol'           => 'BOL',
                'cus_odr_num'   => 'Customer Order',
                'cus_po'        => 'PO',
                'ship_to_name'  => 'Shipping To',
                'ship_by_dt'    => 'Ship By Date',
                'cancel_by_dt'  => 'Cancel By Date',
                // 'act_cancel_dt' => 'Actual Cancel Date',
                'shipped_dt'    => 'Shipped Date',
                'csr_name'      => 'CSR',
                'created_by'    => 'User',
                'created_at'    => 'Created Date',
            ];

            // $filePath = storage_path() . "/Report_PBPM_{$warehouse->whs_name}.csv";

            $today = time();
            $filePath = "Report_Order_{$today}";
            $this->saveFile($title, $orderinfos, $filePath);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param int $date
     *
     * @return false|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }

    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->openToBrowser($filePath); // stream data directly to the browser

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();
    }

    /**
     * @param Request $request
     * @param OrderHdrValidator $orderHdrValidator
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(
        Request $request,
        OrderHdrValidator $orderHdrValidator,
        OrderHdrTransformer $orderHdrTransformer,
        CustomerMetaModel $customerMetaModel,
        OrderHdrMetaModel $orderHdrMetaModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['whs_id'] = (isset($input['edi']) && isset($input['whs_id'])) ? $input['whs_id'] : AbstractModel::getCurrentWhsId();
        $csrId = $customerMetaModel->getPNP($input['cus_id'], 'CSR', $input['whs_id']) ?: Data::getCurrentUserId();
        // validation
        if (!isset($input['edi'])) {
            $orderHdrValidator->validate($input);
            if($input['odr_type'] == 'RTL'){
                if(empty($input['carrier_code'])){
                    throw new \Exception("Carrier Code is required");
                }
                if(empty($input['ship_to_phone'])){
                    throw new \Exception("Ship to phone is required");
                }
                if(empty($input['dlvy_srvc_code'])){
                    throw new \Exception("Delievry Service Code is required");
                }
            }
        }
        try {
            DB::beginTransaction();

            // Update Or Insert Third party
            $thirdParty = $this->createOrUpdateThirdParty($input);

            //value to save
            $status = Status::getByValue("New", "Order-Status");

            $params = [
                // Order Hdr
                'carrier'          => $input['carrier'],
                'ship_to_name'     => $input['ship_to_name'],
                'ship_to_add_1'    => $input['ship_to_add_1'],
                'ship_to_add_2'    => array_get($input, 'ship_to_add_2', null),
                'ship_to_city'     => $input['ship_to_city'],
                'ship_to_country'  => $input['ship_to_country'],
                'ship_to_state'    => $input['ship_to_state'],
                'ref_cod'          => array_get($input, 'ref_cod', null),
                'ship_to_zip'      => $input['ship_to_zip'],
                'ship_by_dt'       => !empty($input['ship_by_dt']) ? strtotime($input['ship_by_dt']) : 0,
                'cancel_by_dt'     => !empty($input['cancel_by_dt']) ? strtotime($input['cancel_by_dt']) : 0,
                'req_cmpl_dt'      => $this->getCompleteDateByShipDate($input['ship_by_dt']),
                'act_cmpl_dt'      => strtotime(array_get($input, 'act_cmpl_dt', null)),
                'act_cancel_dt'    => strtotime(array_get($input, 'act_cancel_dt', null)),
                'in_notes'         => array_get($input, 'in_notes', null),
                'cus_notes'        => array_get($input, 'cus_notes', null),
                'csr_notes'        => array_get($input, 'csr_notes', null),
                // Order Info
                'cus_id'           => $input['cus_id'],
                'whs_id'           => $input['whs_id'],
                'cus_odr_num'      => $input['cus_odr_num'],
                'cus_po'           => $input['cus_po'],
                'odr_type'         => $input['odr_type'],
                'rush_odr'         => (int)array_get($input, 'rush_odr', null),
                'odr_sts'          => $status,
                'sku_ttl'          => count($input['items']),
                'is_ecom'          => $input['odr_type'] == 'EC' ? 1 : (int)array_get($input, 'is_ecom', 0),
                'shipping_addr_id' => $thirdParty->tp_id
            ];

            if($input['odr_type'] == 'RTL'){
                $params['carrier_code'] = array_get($input, 'carrier_code', null);
                $params['ship_to_phone'] = array_get($input, 'ship_to_phone', null);
                $params['dlvy_srvc'] = array_get($input, 'dlvy_srvc', null);
                $params['dlvy_srvc_code'] = array_get($input, 'dlvy_srvc_code', null);
                $params['cosignee_sig'] = array_get($input, 'cosignee_sig', 0);
                //$params['csr'] = $csrId;
            }

            $odrNum = $this->orderHdrModel->getOrderNum();
            $params['odr_num'] = $odrNum;

            $params['so_id'] = $this->getShippingOdr($input);

            // Check duplicate
            $this->orderHdrModel->refreshModel();
            if ($this->orderHdrModel->checkWhere([
                'whs_id'      => $input['whs_id'],
                'cus_id'      => $input['cus_id'],
                'cus_odr_num' => $input['cus_odr_num'],
                'cus_po'      => $input['cus_po'],
                'odr_type'    => $input['odr_type'],
                'odr_sts'    => array('odr_sts', '<>', 'CC')
            ])
            ) {
                if (isset($input['edi'])) {
                    $return = [
                        'data'    => null,
                        'message' => Message::get("BM006", "Warehouse-Customer-OrderID-PO-Type")
                    ];

                    return json_encode($return);
                } else {
                    throw new \Exception(Message::get("BM006", "Warehouse-Customer-OrderID-PO-Type"));
                }
            }

            // Insert Order Header
            $this->orderHdrModel->refreshModel();
            $orderHdr = $this->orderHdrModel->create($params);

            // Insert Order Detail
            if (!empty($input['items'])) {
                foreach ($input['items'] as $detail) {
                    if ($input['odr_type'] == 'EC'){
                        $detail['itm_id'] = $this->_createNewItemEcom($detail['itm_id'], $input['whs_id']);
                    }

                    $this->orderDtlModel->refreshModel();
                    $detail['whs_id'] = $input['whs_id'];
                    $detail['cus_id'] = $input['cus_id'];
                    $detail['odr_id'] = $orderHdr->odr_id;
                    $detail['item_id'] = $detail['itm_id'];
                    $detail['lot'] = empty($detail['lot']) ? 'NA' : $detail['lot'];
                    $detail['pack'] = empty($detail['pack']) ? null : $detail['pack'];
                    $detail['uom_id'] = empty($detail['uom_id']) ? null : $detail['uom_id'];
                    $detail['uom_code'] = empty($detail['uom_code']) ? null : $detail['uom_code'];
                    $detail['qty'] = empty($detail['qty']) ? null : $detail['qty'];
                    $detail['prod_line'] = empty($input['prod_line']) ? null : $input['prod_line'];;
                    $detail['cmp'] = empty($input['cmp']) ? null : $input['cmp'];
                    // $detail['ship_track_id'] = $input['odr_type'] == 'EC' ? $detail['ship_track_id'] : null;
                    $detail['sts'] = "i";
                    $this->orderDtlModel->create($detail);

                    //---- Insert Order Info for SKU Tracking report
                    //$cusId = $input['cus_id'];
                    //$cusInfo = DB::Table('customer')
                    //    ->where('customer.cus_id', $cusId)
                    //    ->first();
                    //
                    //$itemId = $detail['itm_id'];
                    //$itemInfo = DB::Table('item')
                    //    ->where('item.item_id', $itemId)
                    //    ->first();
                    //$length = array_get($itemInfo, 'length', '');
                    //$width = array_get($itemInfo, 'width', '');
                    //$height = array_get($itemInfo, 'height', '');
                    //
                    //$dataSKUTrackingRpt = [
                    //    'cus_id'   => $cusId,
                    //    'cus_name' => array_get($cusInfo, 'cus_name', ''),
                    //    'cus_code' => array_get($cusInfo, 'cus_code', ''),
                    //
                    //    'odr_id'        => $orderHdr->odr_id,
                    //    'trans_num'     => $odrNum,
                    //    'whs_id'        => $input['whs_id'],
                    //    'po_ctnr'       => $input['cus_po'],
                    //    'ref_cus_order' => $input['ref_cod'],
                    //    'actual_date'   => !empty($input['ship_by_dt']) ? strtotime($input['ship_by_dt']) : 0,
                    //
                    //    'item_id' => $itemId,
                    //    'sku'     => $detail['sku'],
                    //    'size'    => $detail['size'],
                    //    'color'   => $detail['color'],
                    //    'pack'    => empty($detail['pack']) ? null : $detail['pack'],
                    //    'lot'     => $detail['lot'],
                    //
                    //    'qty'  => (empty($detail['qty']) ? 0 : $detail['qty']) * (-1),
                    //    'ctns' => ROUND(((empty($detail['qty']) ? 0 : $detail['qty']) / (empty($detail['pack']) ? 0 :
                    //                $detail['pack'])) * (-1), 0),
                    //    'cube' => ROUND(((empty($detail['qty']) ? 0 : $detail['qty']) / (empty($detail['pack']) ? 0 :
                    //                $detail['pack'])) * (-1) * ($length * $width * $height) / 1728, 2),
                    //
                    //
                    //
                    //];
                    //
                    //$this->SKUTrackingReportModel->create($dataSKUTrackingRpt)->toArray();
                    //---- /Insert Order Info for SKU Tracking report


                }
            }

            // Insert Event Tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $input['whs_id'],
                'cus_id'    => $input['cus_id'],
                'owner'     => $odrNum,
                'evt_code'  => Status::getByKey("event", "Order-New"),
                'trans_num' => $odrNum,
                'info'      => sprintf(Status::getByKey("event-info", "Order-New"), $odrNum)

            ]);

            // Change request for Indo Demo - 7/12/2016
            // Insert data to table odr_hdr_meta
            $this->orderHdrMetaModel->insertOdrHdrMeta([
                'odr_id'     => $orderHdr->odr_id,
                'qualifier'  => 'OFP',
                'value'      => array_get($input, 'pack_odr', 0),
                'created_at' => time()
            ]);
            // get oder flow from order meta
            $oderFlow = array_get($input, 'qualifier', []);
            if (!empty($oderFlow)) {
                $valueArrJson = [];
                foreach ($oderFlow as $getVal) {
                    $value = [
                        'odr_flow_id' => array_get($getVal, 'odr_flow_id', ''),
                        'step'        => array_get($getVal, 'step', ''),
                        'flow_code'   => array_get($getVal, 'flow_code', ''),
                        'odr_sts'     => array_get($getVal, 'odr_sts', ''),
                        'name'        => array_get($getVal, 'name', ''),
                        'description' => array_get($getVal, 'description', null),
                        'dependency'  => array_get($getVal, 'dependency', 0),
                        'type'        => array_get($getVal, 'type', ''),
                        'usage'       => array_get($getVal, 'usage', '')
                    ];

                    $valueJson = json_encode($value);
                    array_push($valueArrJson, $valueJson);
                }
                $valueJsons = json_encode($valueArrJson);
                $this->orderHdrMetaModel->insertOdrHdrMeta([
                    'odr_id'    => $orderHdr->odr_id,
                    'qualifier' => 'OFF',
                    'value'     => $valueJsons
                ]);

            } else {

                $qualifier = "OFF";
                if ($input['odr_type'] == "RTL") {
                    $qualifier = "OSF";
                }

                $cus_meta = $customerMetaModel->getFirstWhere([
                    'cus_id'    => intval($input['cus_id']),
                    'qualifier' => $qualifier
                ]);
                $this->orderHdrMetaModel->insertOdrHdrMeta([
                    'odr_id'    => $orderHdr->odr_id,
                    'qualifier' => $qualifier,
                    'value'     => object_get($cus_meta, 'value', '')
                ]);
            }
            DB::commit();

            if ($orderHdr->odr_type != 'XDK') {
                dispatch(new OrderFlowJob($orderHdr->odr_id, 1, $input['cus_id'], $request, $customerMetaModel,
                    $orderHdrMetaModel));
            }

            // Warehouse Transfer IMS API
            if($orderHdr->odr_type == 'WHS') {
                dispatch(new TransferWarehouseIMSJob($orderHdr->odr_id));
            }


            return $this->response->item($orderHdr, $orderHdrTransformer)->setStatusCode(Response::HTTP_CREATED);
        } catch (\PDOException $e) {
            DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param OrderHdrValidator $orderHdrValidator
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function storeNineFourZero(
        Request $request,
        OrderHdrValidator $orderHdrValidator,
        OrderHdrTransformer $orderHdrTransformer,
        CustomerMetaModel $customerMetaModel,
        OrderHdrMetaModel $orderHdrMetaModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['whs_id'] = (isset($input['edi']) && isset($input['whs_id'])) ? $input['whs_id'] : AbstractModel::getCurrentWhsId();
        $csrId = $customerMetaModel->getPNP($input['cus_id'], 'CSR', $input['whs_id']) ?: Data::getCurrentUserId();
        // validation
        if (!isset($input['edi'])) {
            $orderHdrValidator->validate($input);
            if($input['odr_type'] == 'RTL'){
                if(empty($input['carrier_code'])){
                    throw new \Exception("Carrier Code is required");
                }
                if(empty($input['ship_to_phone'])){
                    throw new \Exception("Ship to phone is required");
                }
                if(empty($input['dlvy_srvc_code'])){
                    throw new \Exception("Delievry Service Code is required");
                }
            }
        }
        try {
            DB::beginTransaction();

            // Update Or Insert Third party
            $thirdParty = $this->createOrUpdateThirdParty($input);

            //value to save
            $status = Status::getByValue("New", "Order-Status");

            $params = [
                // Order Hdr
                'carrier'          => $input['carrier'],
                'ship_to_name'     => $input['ship_to_name'],
                'ship_to_add_1'    => $input['ship_to_add_1'],
                'ship_to_add_2'    => array_get($input, 'ship_to_add_2', null),
                'ship_to_city'     => $input['ship_to_city'],
                'ship_to_country'  => $input['ship_to_country'],
                'ship_to_state'    => $input['ship_to_state'],
                'ref_cod'          => array_get($input, 'ref_cod', null),
                'ship_to_zip'      => $input['ship_to_zip'],
                'ship_by_dt'       => !empty($input['ship_by_dt']) ? strtotime($input['ship_by_dt']) : 0,
                'cancel_by_dt'     => !empty($input['cancel_by_dt']) ? strtotime($input['cancel_by_dt']) : 0,
                'req_cmpl_dt'      => $this->getCompleteDateByShipDate($input['ship_by_dt']),
                'act_cmpl_dt'      => strtotime(array_get($input, 'act_cmpl_dt', null)),
                'act_cancel_dt'    => strtotime(array_get($input, 'act_cancel_dt', null)),
                'in_notes'         => array_get($input, 'in_notes', null),
                'cus_notes'        => array_get($input, 'cus_notes', null),
                'csr_notes'        => array_get($input, 'csr_notes', null),
                // Order Info
                'cus_id'           => $input['cus_id'],
                'whs_id'           => $input['whs_id'],
                'cus_odr_num'      => $input['cus_odr_num'],
                'cus_po'           => $input['cus_po'],
                'odr_type'         => $input['odr_type'],
                'rush_odr'         => (int)array_get($input, 'rush_odr', null),
                'odr_sts'          => $status,
                'sku_ttl'          => count($input['items']),
                'is_ecom'          => $input['odr_type'] == 'EC' ? 1 : (int)array_get($input, 'is_ecom', 0),
                'cus_pick_num'     => (!empty($input['cus_pick_num']))?$input['cus_pick_num']:null,
                'cus_dept_ref'     => (!empty($input['cus_dept_ref']))?$input['cus_dept_ref']:null,
                'odr_req_dt'       => (!empty($input['odr_req_dt']))?strtotime($input['odr_req_dt']):null,
                'cus_notes'        => (!empty($input['cus_notes']))?$input['cus_notes']:null,
                'shipping_addr_id' => $thirdParty->tp_id
            ];

            if($input['odr_type'] == 'RTL'){
                $params['carrier_code'] = array_get($input, 'carrier_code', null);
                $params['ship_to_phone'] = array_get($input, 'ship_to_phone', null);
                $params['dlvy_srvc'] = array_get($input, 'dlvy_srvc', null);
                $params['dlvy_srvc_code'] = array_get($input, 'dlvy_srvc_code', null);
                $params['cosignee_sig'] = array_get($input, 'cosignee_sig', 0);
                $params['csr'] = $csrId;
            }

            $odrNum = $this->orderHdrModel->getOrderNum();
            $params['odr_num'] = $odrNum;

            $params['so_id'] = $this->getShippingOdr($input);

            // Check duplicate
            $this->orderHdrModel->refreshModel();
            if ($this->orderHdrModel->checkWhere([
                'whs_id'      => $input['whs_id'],
                'cus_id'      => $input['cus_id'],
                'cus_odr_num' => $input['cus_odr_num'],
                'cus_po'      => $input['cus_po'],
                'odr_type'    => $input['odr_type'],
                'odr_sts'    => array('odr_sts', '<>', 'CC')
            ])
            ) {
                if (isset($input['edi'])) {
                    $return = [
                        'data'    => null,
                        'message' => Message::get("BM006", "Warehouse-Customer-OrderID-PO-Type")
                    ];

                    return json_encode($return);
                } else {
                    throw new \Exception(Message::get("BM006", "Warehouse-Customer-OrderID-PO-Type"));
                }
            }

            // Insert Order Header
            $this->orderHdrModel->refreshModel();
            $orderHdr = $this->orderHdrModel->create($params);

            // Insert Order Detail
            if (!empty($input['items'])) {
                foreach ($input['items'] as $detail) {
                    if ($input['odr_type'] == 'EC'){
                        $detail['itm_id'] = $this->_createNewItemEcom($detail['itm_id'], $input['whs_id']);
                    }

                    $this->orderDtlModel->refreshModel();
                    $detail['whs_id'] = $input['whs_id'];
                    $detail['cus_id'] = $input['cus_id'];
                    $detail['odr_id'] = $orderHdr->odr_id;
                    $detail['item_id'] = $detail['itm_id'];
                    $detail['pack'] = empty($detail['pack']) ? null : $detail['pack'];
                    $detail['uom_id'] = empty($detail['uom_id']) ? null : $detail['uom_id'];
                    $detail['uom_code'] = empty($detail['uom_code']) ? null : $detail['uom_code'];
                    $detail['qty'] = empty($detail['qty']) ? null : $detail['qty'];
                    // $detail['ship_track_id'] = $input['odr_type'] == 'EC' ? $detail['ship_track_id'] : null;
                    $detail['sts'] = "i";
                    $this->orderDtlModel->create($detail);

                    // 945
                    $productMeta945 = array_get($detail, 'product_meta', '');
                    if (!empty($productMeta945)) {
                        $check = DB::table('item_meta')->where('qualifier',945)->where('itm_id',$detail['item_id'])->first();
                        if(!empty($check)) {
                            DB::table('item_meta')->where('qualifier',945)
                                ->where('itm_id',$detail['item_id'])
                                ->update([
                                    'value' => $productMeta945,
                                    'updated_at' => time(),
                                    'updated_by' => Data::getCurrentUserId(),
                                    'deleted' => 0,
                                    'deleted_at' => 915148800
                                ]);
                        }  else {
                            DB::table('item_meta')->insert([
                                'qualifier' => 945,
                                'itm_id' => $detail['item_id'],
                                'value' => $productMeta945,
                                'created_at' => time(),
                                'created_by' => Data::getCurrentUserId(),
                                'updated_at' => time(),
                                'updated_by' => Data::getCurrentUserId(),
                                'deleted' => 0,
                                'deleted_at' => 915148800
                            ]);
                        }
                    }
                }
            }

            // Insert Event Tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $input['whs_id'],
                'cus_id'    => $input['cus_id'],
                'owner'     => $odrNum,
                'evt_code'  => Status::getByKey("event", "Order-New"),
                'trans_num' => $odrNum,
                'info'      => sprintf(Status::getByKey("event-info", "Order-New"), $odrNum)

            ]);

            // Change request for Indo Demo - 7/12/2016
            // Insert data to table odr_hdr_meta
            $this->orderHdrMetaModel->insertOdrHdrMeta([
                'odr_id'     => $orderHdr->odr_id,
                'qualifier'  => 'OFP',
                'value'      => array_get($input, 'pack_odr', 0),
                'created_at' => time()
            ]);
            // get oder flow from order meta
            $oderFlow = array_get($input, 'qualifier.value', []);
            if (!empty($oderFlow)) {
                $valueArrJson = [];
                foreach ($oderFlow as $getVal) {
                    $value = [
                        'odr_flow_id' => array_get($getVal, 'odr_flow_id', ''),
                        'step'        => array_get($getVal, 'step', ''),
                        'flow_code'   => array_get($getVal, 'flow_code', ''),
                        'odr_sts'     => array_get($getVal, 'odr_sts', ''),
                        'name'        => array_get($getVal, 'name', ''),
                        'description' => array_get($getVal, 'description', null),
                        'dependency'  => array_get($getVal, 'dependency', 0),
                        'type'        => array_get($getVal, 'type', ''),
                        'usage'       => array_get($getVal, 'usage', '')
                    ];
                    // if($orderHdr->odr_type == 'RTL' && in_array(array_get($getVal, 'odr_flow_id', ''), [7, 11, 12])){
                    //     $value['usage'] = 1;
                    // }

                    $valueJson = json_encode($value);
                    array_push($valueArrJson, $valueJson);
                }
                $valueJsons = json_encode($valueArrJson);
                $this->orderHdrMetaModel->insertOdrHdrMeta([
                    'odr_id'    => $orderHdr->odr_id,
                    'qualifier' => 'OFF',
                    'value'     => $valueJsons
                ]);

            } else {
                $cus_meta = $customerMetaModel->getFirstWhere([
                    'cus_id'    => intval($input['cus_id']),
                    'qualifier' => 'OFF'
                ]);
                $this->orderHdrMetaModel->insertOdrHdrMeta([
                    'odr_id'    => $orderHdr->odr_id,
                    'qualifier' => 'OFF',
                    'value'     => object_get($cus_meta, 'value', '')
                ]);
            }

            // 945
            $odrMeta945 = array_get($input, 'order_meta', '');
            if (!empty($odrMeta945)) {
                $this->orderHdrMetaModel->insertOdrHdrMeta([
                    'odr_id'    => $orderHdr->odr_id,
                    'qualifier' => '945',
                    'value'     => $odrMeta945,
                    'created_at' => time(),
                    'updated_at' => time()
                ]);
            }

            DB::commit();

            if ($orderHdr->odr_type != 'XDK') {
                dispatch(new OrderFlowJob($orderHdr->odr_id, 1, $input['cus_id'], $request, $customerMetaModel,
                    $orderHdrMetaModel));
            }


            return $this->response->item($orderHdr, $orderHdrTransformer)->setStatusCode(Response::HTTP_CREATED);
        } catch (\PDOException $e) {
            DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function getCompleteDateByShipDate($shipDate)
    {
        if (!$shipDate) {
            return null;
        }
        $currentDateTimestamp = strtotime(date('Y-m-d'));
        $shipDateTimestamp = strtotime($shipDate);

        if ($shipDateTimestamp < $currentDateTimestamp) {
            return $shipDateTimestamp;
        }

        if ($shipDateTimestamp - $currentDateTimestamp >= 3 * 86400) {
            return $shipDateTimestamp - 3 * 86400;
        } else {
            return $currentDateTimestamp;
        }
    }

    private function createOrUpdateThirdParty(
        $input
    ) {
        $whs_id = array_get($input, 'whs_id', null);
        $name = array_get($input, 'ship_to_name', null);
        $add_1 = array_get($input, 'ship_to_add_1', null);
        $add_2 = array_get($input, 'ship_to_add_2', null);
        $city = array_get($input, 'ship_to_city', null);
        $state = array_get($input, 'ship_to_state', null);
        $zip = array_get($input, 'ship_to_zip', null);
        $country = array_get($input, 'ship_to_country', null);
        $cus_id = array_get($input, 'cus_id');

        $thirdParty = $this->thirdPartyModel->getFirstBy(DB::raw("UPPER(REPLACE(name, ' ', ''))"),
            strtoupper(preg_replace('/\s+/', '', $name)));

        // Update
        if ($thirdParty) {

            $thirdParty->whs_id = $whs_id;
            $thirdParty->cus_id = $cus_id;
            $thirdParty->name = $name;
            $thirdParty->add_1 = $add_1;
            $thirdParty->add_2 = $add_2;
            $thirdParty->city = $city;
            $thirdParty->state = $state;
            $thirdParty->zip = $zip;
            $thirdParty->country = $country;
            $thirdParty->save();

        } else {
            // Create
            $thirdParty = $this->thirdPartyModel->create([
                'type'    => 'CUS',
                'name'    => $name,
                'whs_id'  => $whs_id,
                'cus_id'  => $cus_id,
                'add_1'   => $add_1,
                'add_2'   => $add_2,
                'city'    => $city,
                'state'   => $state,
                'zip'     => $zip,
                'country' => $country
            ]);
        }

        return $thirdParty;

    }

    /**
     * @param $orderId
     * @param Request $request
     * @param OrderHdrValidator $orderHdrValidator
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function update(
        $orderId,
        Request $request,
        OrderHdrValidator $orderHdrValidator,
        OrderHdrTransformer $orderHdrTransformer,
        CustomerMetaModel $customerMetaModel,
        OrderHdrMetaModel $orderHdrMetaModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['whs_id'] = AbstractModel::getCurrentWhsId();
        $input['whs_id_to'] = (isset($input['whs_id_to'])) ? $input['whs_id_to'] : null;

        // validation
        $orderHdrValidator->validate($input);
        if($input['odr_type'] == 'RTL'){
            if(empty($input['carrier_code'])){
                throw new \Exception("Carrier Code is required");
            }
            if(empty($input['ship_to_phone'])){
                throw new \Exception("Ship to phone is required");
            }
            if(empty($input['dlvy_srvc_code'])){
                throw new \Exception("Delievry Service Code is required");
            }
        }
        try {
            //value to save
            $status = Status::getByValue("New", "Order-Status");

            $params = [
                // Order Hdr
                'odr_id'          => $orderId,
                'carrier'         => $input['carrier'],
                'ship_to_name'    => $input['ship_to_name'],
                'ship_to_add_1'   => $input['ship_to_add_1'],
                'ship_to_add_2'   => array_get($input, 'ship_to_add_2', null),
                'ship_to_city'    => $input['ship_to_city'],
                'ship_to_country' => $input['ship_to_country'],
                'ship_to_state'   => $input['ship_to_state'],
                'ship_to_zip'     => $input['ship_to_zip'],
                'ship_by_dt'      => !empty($input['ship_by_dt']) ? strtotime($input['ship_by_dt']) : 0,
                'cancel_by_dt'    => !empty($input['cancel_by_dt']) ? strtotime($input['cancel_by_dt']) : 0,
                'req_cmpl_dt'     => $this->getCompleteDateByShipDate($input['ship_by_dt']),
                'act_cmpl_dt'     => strtotime(array_get($input, 'act_cmpl_dt', null)),
                'act_cancel_dt'   => strtotime(array_get($input, 'act_cancel_dt', null)),
                'in_notes'        => array_get($input, 'in_notes', null),
                'cus_notes'       => array_get($input, 'cus_notes', null),
                'csr_notes'       => array_get($input, 'csr_notes', null),
                // Order Info
                'cus_id'          => $input['cus_id'],
                'whs_id'          => $input['whs_id'],
                'whs_id_to'       => $input['whs_id_to'],
                'cus_odr_num'     => $input['cus_odr_num'],
                'ref_cod'         => array_get($input, 'ref_cod', null),
                'cus_po'          => $input['cus_po'],
                'odr_type'        => $input['odr_type'],
                'rush_odr'        => (int)array_get($input, 'rush_odr', null),
                'odr_sts'         => $status,
                'sku_ttl'         => count($input['items']),
                'hs_num'          => array_get($input, 'hs_num'),
                'hs_unit'         => array_get($input, 'hs_unit'),
                'price'           => array_get($input, 'price')
            ];

            if($input['odr_type'] == 'RTL'){
                $params['carrier_code'] = array_get($input, 'carrier_code', null);
                $params['ship_to_phone'] = array_get($input, 'ship_to_phone', null);
                $params['dlvy_srvc'] = array_get($input, 'dlvy_srvc', null);
                $params['dlvy_srvc_code'] = array_get($input, 'dlvy_srvc_code', null);
                $params['cosignee_sig'] = array_get($input, 'cosignee_sig', 0);
            }

            // Check Exist
            $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId);
            $oldOrderHdr = $orderHdr; // Make a copy to check
            if (empty($orderHdr->odr_id)) {
                throw new \Exception(Message::get("BM017", "Order Hdr"));
            }

            //Allow to update schd_ship_dt and routed_dt if updater is CSR
            $userId = Data::getCurrentUserId();

            $orderCsrIds = DB::table('odr_csr')->where('odr_id', $orderId)->pluck('csr');
            if(count($orderCsrIds)>=1) {
                if (in_array($userId, $orderCsrIds) || $orderHdr->csr == $userId) {
                    $schd_ship_dt = array_get($input, 'schd_ship_dt', null);
                    $routed_dt = array_get($input, 'routed_dt', null);
                    $params['schd_ship_dt'] = $schd_ship_dt ? strtotime($schd_ship_dt) : null;
                    $params['routed_dt'] = $routed_dt ? strtotime($routed_dt) : null;
                }
            }

            // Check duplicate
            $this->orderHdrModel->refreshModel();
            if (($odr = $this->orderHdrModel->getFirstWhere([
                    'whs_id'      => $input['whs_id'],
                    'cus_id'      => $input['cus_id'],
                    'cus_odr_num' => $input['cus_odr_num'],
                    'cus_po'      => $input['cus_po'],
                    'odr_type'    => $input['odr_type'],
                    'odr_sts'    => 'CC'
                ])) && $odr->odr_id != $orderId
            ) {
                throw new \Exception(Message::get("BM006", "Warehouse-Customer-OrderID-PO-Type"));
            }

            // Check Status
            /*if ($orderHdr->odr_sts != Status::getByValue("New", "Order-status")) {
                throw new \Exception(Message::get("VR064", "Order Status", "'New'"));
            }*/

            $params['so_id'] = $this->getShippingOdr($input, $orderHdr->ref_cod, true);

            // Shipping Order info.
            $so = $this->shippingOrderModel->getFirstWhere([
                'cus_odr_num' => $input['cus_odr_num'],
                'whs_id'      => $input['whs_id'],
                'cus_id'      => $input['cus_id']
            ]);
            if ($so) {
                // Update Shipping Order.
                $shipParams['so_id'] = $orderHdr->so_id;
                $this->shippingOrderModel->update($shipParams)->so_id;
            } else {
                $shipParams['po_total'] = 1;
                $this->shippingOrderModel->create($shipParams)->so_id;
            }

            DB::beginTransaction();

            // Update Or Insert Third party
            $thirdParty = $this->createOrUpdateThirdParty($input);
            $params['shipping_addr_id'] = $thirdParty->tp_id;

            if ($orderHdr->odr_sts != Status::getByValue("New", "Order-status")) {
                $paramsUpdate = [
                    'odr_id'           => $params['odr_id'],
                    'carrier'          => $params['carrier'],
                    'ship_to_name'     => $params['ship_to_name'],
                    'ship_to_add_1'    => $params['ship_to_add_1'],
                    'ship_to_city'     => $params['ship_to_city'],
                    'ship_to_country'  => $params['ship_to_country'],
                    'ship_to_state'    => $params['ship_to_state'],
                    'ship_to_zip'      => $params['ship_to_zip'],
                    'in_notes'         => $params['in_notes'],
                    'cus_notes'        => $params['cus_notes'],
                    'csr_notes'        => $params['csr_notes'],
                    'so_id'            => $params['so_id'],
                    'shipping_addr_id' => $params['shipping_addr_id'],
                    'ship_by_dt'       => $params['ship_by_dt'],
                    'cancel_by_dt'     => $params['cancel_by_dt'],
                    'req_cmpl_dt'      => $params['req_cmpl_dt'],
                    'rush_odr'         => $params['rush_odr'],
                    'hs_num'           => array_get($params, 'hs_num'),
                    'hs_unit'          => array_get($params, 'hs_unit'),
                    'price'            => array_get($params, 'price')
                ];
                if($orderHdr->odr_type == 'RTL'){
                    $paramsUpdate['carrier_code'] = array_get($params, 'carrier_code', null);
                    $paramsUpdate['ship_to_phone'] = array_get($params, 'ship_to_phone', null);
                    $paramsUpdate['dlvy_srvc'] = array_get($params, 'dlvy_srvc', null);
                    $paramsUpdate['dlvy_srvc_code'] = array_get($params, 'dlvy_srvc_code', null);
                    $paramsUpdate['cosignee_sig'] = array_get($params, 'cosignee_sig', 0);
                }

                $params = $paramsUpdate;
            }

            // Update Order Header
            $oldSoId = $orderHdr->so_id;
            $oldRef = $orderHdr->ref_cod;
            $orderHdr = $this->orderHdrModel->update($params);
            if (!$orderHdr) {
                throw new \Exception(Message::get("BM010"));
            }
			// Update Sold To
            $this->orderHdrModel->updateSoldTo($orderId, $input['order_sold_to'][0] ?? null);

            // Update Shipping Odr
            if ($oldRef !== $input['ref_cod']) {
                $this->updateOtherShipping($oldSoId);
            }

            // Insert Order Detail
            if (!empty($input['items'])) {
                $newOdrDtlIds = array_column($input['items'], 'odr_dtl_id');

                $deleteIds = $this->orderDtlModel->getModel()
                    ->where("odr_id", $orderId)
                    ->whereNotIn('odr_dtl_id', $newOdrDtlIds)
                    ->pluck('odr_dtl_id')
                    ->toArray();

                if (!empty($deleteIds)) {
                    $this->orderDtlModel->deleteDtls($deleteIds);
                }

                foreach ($input['items'] as $detail) {
                    $this->orderDtlModel->refreshModel();

                    $detail['whs_id'] = $input['whs_id'];
                    $detail['cus_id'] = $input['cus_id'];
                    $detail['odr_id'] = $orderHdr->odr_id;
                    $detail['item_id'] = $detail['itm_id'];
                    $detail['pack'] = empty($detail['pack']) ? null : $detail['pack'];
                    $detail['qty'] = empty($detail['qty']) ? null : $detail['qty'];
                    $detail['uom_id'] = empty($detail['uom_id']) ? null : $detail['uom_id'];
                    $detail['uom_code'] = empty($detail['uom_code']) ? null : $detail['uom_code'];
                    // $detail['ship_track_id'] = $input['odr_type'] == 'EC' ? $detail['ship_track_id'] : null;

                    if (empty($detail['odr_dtl_id'])) {
                        $detail['sts'] = "i";
                        $this->orderDtlModel->create($detail);
                    } else {
                        $detail['sts'] = "u";
                        $this->orderDtlModel->update($detail);


                        //---- Update Order Info for SKU Tracking report
                        $cusId = $input['cus_id'];
                        $cusInfo = DB::Table('customer')
                            ->where('customer.cus_id', $cusId)
                            ->first();

                        $itemId = $detail['itm_id'];
                        $itemInfo = DB::Table('item')
                            ->where('item.item_id', $itemId)
                            ->first();
                        $length = array_get($itemInfo, 'length', '');
                        $width = array_get($itemInfo, 'width', '');
                        $height = array_get($itemInfo, 'height', '');

                        $dataSKUTrackingRpt = [
                            'cus_id'   => $cusId,
                            'cus_name' => array_get($cusInfo, 'cus_name', ''),
                            'cus_code' => array_get($cusInfo, 'cus_code', ''),

                            'odr_id'        => $orderHdr->odr_id,
                            'trans_num'     => $input['odr_num'],
                            'whs_id'        => $input['whs_id'],
                            'po_ctnr'       => $input['cus_po'],
                            'ref_cus_order' => $input['ref_cod'],
                            'actual_date'   => !empty($input['ship_by_dt']) ? strtotime($input['ship_by_dt']) : 0,

                            'item_id' => $itemId,
                            'sku'     => $detail['sku'],
                            'size'    => $detail['size'],
                            'color'   => $detail['color'],
                            'pack'    => empty($detail['pack']) ? null : $detail['pack'],
                            'lot'     => $detail['lot'],

                            'qty'  => (empty($detail['qty']) ? 0 : $detail['qty']) * (-1),
                            'ctns' => ROUND(((empty($detail['qty']) ? 0 : $detail['qty']) / (empty($detail['pack']) ? 0 :
                                        $detail['pack'])) * (-1), 0),
                            'cube' => ROUND(((empty($detail['qty']) ? 0 : $detail['qty']) / (empty($detail['pack']) ? 0 :
                                        $detail['pack'])) * (-1) * ($length * $width * $height) / 1728, 2),

                        ];

                        DB::table('rpt_sku_tracking')->where('item_id', $itemId)->where('odr_id', $orderHdr->odr_id)
                            ->update($dataSKUTrackingRpt);
                        //---- /Update Order Info for SKU Tracking report


                    }
                }
            }

            // Insert Event Tracking
            $eventTrackingInfo = [];
            if (strcmp($input['in_notes'], $oldOrderHdr['in_notes']) != 0) {
                array_push($eventTrackingInfo, 'WHS Comments');
            }
            if (strcmp($input['cus_notes'], $oldOrderHdr['cus_notes']) != 0) {
                array_push($eventTrackingInfo, 'External Comments');
            }
            if (strcmp($input['csr_notes'], $oldOrderHdr['csr_notes']) != 0) {
                array_push($eventTrackingInfo, 'CSR Comments');
            }
            if (!empty($eventTrackingInfo)) {
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $input['whs_id'],
                    'cus_id'    => $input['cus_id'],
                    'owner'     => $orderHdr['odr_num'],
                    'evt_code'  => Status::getByKey("event", "ORDER-COMMENT-EDIT"),
                    'trans_num' => $orderHdr['odr_num'],
                    'info'      => 'Updated to ' . implode(", ", $eventTrackingInfo)
                ]);
            }

            DB::commit();
            dispatch(new OrderFlowJob($orderHdr->odr_id, 1, $input['cus_id'], $request, $customerMetaModel,
                $orderHdrMetaModel));

            return $this->response->item($orderHdr, $orderHdrTransformer)->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param string $odrNum
     *
     * @return mixed
     */
    public function integrateFileToDMS(
        Request $request,
        $odrNum
    ) {
        $fileRequest = $request->getUploadedFiles();
        if (isset($fileRequest['file'])) {
            $fileTmps = $fileRequest['file'];
            $fileTmps = is_array($fileTmps) ? $fileTmps : [$fileTmps];

            foreach($fileTmps as $fileTmp){
                $clientFileName = $fileTmp->getClientFilename();
                $dmsFolderPath = storage_path(self::ORDER_DOCUMENT_FOLDER . "/dms");

                if (!file_exists($dmsFolderPath)) {
                    mkdir($dmsFolderPath, 0777, true);
                }
                $filePath = "{$dmsFolderPath}/{$clientFileName}";
                $fileTmp->moveTo($filePath);
                $fileUrl = env('SYSTEM_BASE_URL') . "/core/orders/public/download-document?type=dms&file_name={$clientFileName}";

                $client = new Client();

                $dmsApiUrl = env('DMS_DOCUMENT_API');

                $res = $client->request('POST', $dmsApiUrl, [
                    'http_errors' => false,
                    'form_params' => [
                        'sys'           => env('SYSTEM_NAME', 'wms360.dev'),
                        'jwt'           => str_replace('Bearer ', '', $request->getHeaders()['authorization'][0]),
                        'file_url'      => $fileUrl,
                        'sts'           => 1,
                        'transaction'   => $odrNum,
                        'document_type' => 'order_doc',
                    ]
                ]);

                if (isset($filePath) && file_exists($filePath)) {
                    unlink($filePath);
                }

                if ($res->getStatusCode() == 200) {
                    $messages[] = "File {$clientFileName} uploaded successfully.";
                } else {
                    $messages[] = "File {$clientFileName} was failed to upload to DMS.";
                }
            }

            return [
                'msg'   => $messages
            ];
        }
    }

    /**
     * @param $orderId
     *
     * @return Response|void
     */
    public function destroy(
        $orderId
    ) {
        try {
            // Check Exist
            if (!$this->orderHdrModel->checkWhere(['ord_hdr_id' => $orderId])) {
                return $this->response->errorBadRequest(Message::get("BM017", "Order Hdr"));
            }

            // Check Exist in Order Detail
            if ($this->orderDtlModel->checkWhere(['ord_hdr_id' => $orderId])) {
                return $this->response->errorBadRequest(Message::get("BM003", "Order"));
            }

            if ($this->orderHdrModel->deleteOrderHdr($orderId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param UserModel $userModel
     * @param CsrValidator $csrValidator
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return Response|void
     */
    public function updateCsr(
        Request $request,
        UserModel $userModel,
        CsrValidator $csrValidator,
        OrderHdrTransformer $orderHdrTransformer,
        CustomerMetaModel $customerMetaModel,
        OrderHdrMetaModel $orderHdrMetaModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $csrValidator->validate($input);

        $params = [
            'csr' => $input['user_id']
        ];

        try {

            // Load User
            $userInfo = $userModel->getFirstWhere(['user_id' => $input['user_id']]);
            if (empty($userInfo)) {
                throw new \Exception(Message::get("BM017", "User"));
            }
            //check user status
            $csrValidator->checkUserStatus(object_get($userInfo, 'status', null));

            // Check Exist orders
            $odrHdrs = $this->orderHdrModel->getOrderById($input['odr_id']);
            if (empty($odrHdrs)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }
            $odrStsCancel = Status::getByValue("Canceled", "Order-Status");
            foreach ($odrHdrs as $odrHdr) {
                if ($odrHdr->odr_sts === $odrStsCancel) {
                    throw new \Exception(Message::get('BM123', 'Canceled order', 'assigned CSR'));
                }
            }

            $orderHdr = $this->orderHdrModel->updateWhereIn($params, $input['odr_id'], 'odr_id');
            $orderInfo = $this->orderHdrModel->getOrderById($input['odr_id']);
            if ($orderInfo)
            {
                foreach ($orderInfo as $odI) {
                    if ($odI->odr_sts == 'NW' && object_get($odI, 'csr')) {
                        dispatch(new OrderFlowJob($odI->odr_id, 2, $odI->cus_id, $request, $customerMetaModel,
                            $orderHdrMetaModel));
                    }
                }
                return $this->response->item($orderInfo, $orderHdrTransformer);
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param IRequest $request
     *
     * @return array|\Symfony\Component\HttpFoundation\Response|void
     */
    public function validateImport(
        IRequest $request
    ) {
        $hashKey = md5($request->header('Authorization'));

        $orderImport = new OrderImport(
            $hashKey,
            $this->orderHdrModel,
            $this->orderDtlModel,
            $this->shippingOrderModel,
            $this->warehouseModel,
            $this->itemModel,
            $this->eventTrackingModel,
            $this->orderImportValidator
        );

        $rs = $orderImport->validate($request);

        if ($rs['status']) {
            return ['data' => $rs['data']];
        } else {
            if (!empty($rs['data']['code'])) {
                return $this->response->noContent()
                    ->setContent(['data' => $rs['data']])->setStatusCode($rs['data']['code']);
            } else {
                return $this->response->errorBadRequest($rs['data']['message']);
            }
        }
    }

    public function processImport(
        $cusId,
        IRequest $request
    ) {
        $hashKey = md5($request->header('Authorization'));
        $orderImport = new OrderImport(
            $hashKey,
            $this->orderHdrModel,
            $this->orderDtlModel,
            $this->shippingOrderModel,
            $this->warehouseModel,
            $this->itemModel,
            $this->eventTrackingModel,
            $this->orderImportValidator
        );
        $orderImport->processImport($cusId);

        return ['data' => ['message' => Message::get('BM124')]];
    }

    /**
     * @param $input
     * @param bool $isUpdate
     *
     * @return mixed
     */
    private function getShippingOdr(
        $input,
        $oldRef = null,
        $isUpdate = false
    ) {
        $this->shippingOrderModel->refreshModel();

        $shipParams = [
            'cus_id'      => $input['cus_id'],
            'whs_id'      => $input['whs_id'],
            'cus_odr_num' => $input['cus_odr_num'],
            'so_sts'      => Status::getByValue("New", "Order-Status"),
            'type'        => $input['odr_type'],
        ];

        // Create Shipping Order.
        $so = $this->shippingOrderModel->getFirstWhere([
            'cus_odr_num' => $shipParams['cus_odr_num'],
            'whs_id'      => $shipParams['whs_id'],
            'cus_id'      => $shipParams['cus_id']
        ]);

        if (empty($so)) {
            // Create Shipping Odr
            $shipParams['po_total'] = 1;
            $soId = $this->shippingOrderModel->create($shipParams)->so_id;
        } else {
            // Update po_total
            $soId = $so->so_id;
            if ($isUpdate) {
                if ($oldRef !== $shipParams['cus_odr_num']) {
                    $shipParams['po_total'] = (int)$so->po_total + 1;
                    $shipParams['so_id'] = $soId;
                    $soId = $this->shippingOrderModel->update($shipParams)->so_id;
                }
            }
        }

        return $soId;
    }

    /**
     * @param $soId
     */
    private function updateOtherShipping(
        $soId
    ) {
        $this->shippingOrderModel->refreshModel();
        $so = $this->shippingOrderModel->getFirstWhere(['so_id' => $soId])->toArray();

        if ($so['so_id']) {
            $this->shippingOrderModel->refreshModel();
            $so['po_total'] = (int)$so['po_total'] - 1;
            $this->shippingOrderModel->update($so);
        }
    }

    public
    function printPdf(
        $orderId
    ) {
        $odrObj = $this->orderHdrModel->byId($orderId, ['details']);

        if ($odrObj->odr_sts === Status::getByValue("Canceled", "Order-Status")) {
            throw new \Exception(Message::get('BM123', 'Canceled order', 'print packing slip'));
        }

        if (strlen($odrObj->odr_num) > 14) {
            $odrNum = substr($odrObj->odr_num, 0, 14);
            $odrChk = $this->orderHdrModel->getFirstBy('odr_num', $odrNum);
            if ($odrChk->odr_type !== 'EC') {
                return $this->errorBadRequest(Message::get('BM125'));
            }
        }

        $tbl = "";
        $odrDate = $odrObj->created_at->format('m/d/Y');
        foreach ($odrObj->details as $odrDtl) {
            $tbl .= <<<EOT
<tr>
    <td>$odrDtl->piece_qty</td>
    <td>$odrDtl->item_id</td>
    <td>$odrDtl->cus_upc</td>
    <td>$odrDtl->sku <br/>$odrDtl->size <br/>$odrDtl->color</td>
    <td>$odrDtl->des</td>
    <td>
        <barcode code='$odrDtl->ship_track_id' type="C128B" text="1" height="2" size="0.4"/>
        <div style="font-family: ocrb; font-size: 10px;">$odrDtl->ship_track_id</div>
    </td>
</tr>
EOT;

        }

        $html = <<<EOL
<html>
    <body style="font-family:sans-serif;font-size:12px;">
    <style>
        td { text-align: center; }
        .upercase { text-transform: uppercase; font-weight: bold; }
    </style>

        ship to: <br />
        <span class="upercase">$odrObj->ship_to_name</span> <br />
        <span class="upercase">$odrObj->ship_to_add_1</span> <br />
        <span class="upercase">$odrObj->ship_to_city $odrObj->ship_to_state $odrObj->ship_to_zip</span>
        <br /><br />

        Order #: $odrObj->cus_odr_num <br />
        PO #: $odrObj->cus_po <br />
        Order Date: $odrDate <br />
        Shipping Service: $odrObj->carrier <br /><br />
        <barcode code='$odrObj->odr_num' type="C128B" text="1" height="2.5" size="0.5" style="margin-left: -5px"/>
        <div style="font-family: ocrb; font-size: 12px;">$odrObj->odr_num</div>
        <br /><br />
        <table cellspacing="0" cellpadding="10" border="1" style="width:100%;">
            <tr>
                <th>Quantity</th>
                <th>Item ID</th>
                <th>UPC</th>
                <th>SKU <br/>Color <br/>Size</th>
                <th>Product Details</th>
                <th>Tracking Number</th>
            </tr>
            $tbl
        </table>
    </body>
</html>

EOL;

        $mpdf = new \mPDF('', '', '', '', 20, 15, 25, 25, 10, 10);
        $mpdf->WriteHTML($html);
        $mpdf->Output('packing-slip.pdf', 'D');
    }

    public function listCartonByOrder(
        $orderId,
        Request $request,
        PackHdrTransformer $packHdrTransformer
    ) {
        $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId);

        if (empty($orderHdr)) {
            throw new \Exception(Message::get("BM017", "Order"));
        }

        // get data from HTTP
        $input = $request->getQueryParams();
        $input['odr_hdr_id'] = $orderId;
        try {
            $packHdr = $this->packHdrModel->search($input, [

            ], array_get($input, 'limit'));

            return $this->response->paginator($packHdr, $packHdrTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderId
     * @param Request $request
     * @param OrderCancelListTransformer $orderCancelListTransformer
     *
     * @return Response|void
     * @throws \Exception
     */
    public function orderCancelList(
        $orderId,
        Request $request,
        OrderCancelListTransformer $orderCancelListTransformer
    ) {
        // get data from HTTP (orderId is order origin)
        $input = $request->getQueryParams();

        //check exist of first orderID
        $orderHdr_first = $this->orderHdrModel->getFirstBy('odr_id', $orderId);

        if (empty($orderHdr_first)) {
            throw new \Exception(Message::get("BM017", "Order"));
        }

        //validate whs cus
        $this->chkWhsAndCus($orderHdr_first['whs_id'], $orderHdr_first['cus_id']);


        $odr_num = object_get($orderHdr_first, 'odr_num', '');
        if (empty($odr_num)) {
            throw new \Exception(Message::get('VR028', 'Order num'));
        }

        //get origin order according to first orderId that is inputted from user
        $orderHdrId = $this->orderHdrModel->getOriginOrder($odr_num);
        $orderHdrInfo = $this->orderHdrModel->getAllOrderIdsAccordingToOriginOrdNum($odr_num);
        $orderHdrIds = array_pluck($orderHdrInfo, 'odr_id');

        //get order info from origin order
        $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderHdrId);
        if (empty($orderHdr)) {
            throw new \Exception(Message::get("BM017", "Origin order"));
        }

        $input['odr_hdr_id'] = $orderHdrId;

        //Count Odr_dtl according to OdrID
        $odrDtl_NO = $this->orderDtlModel->countOrderSku($orderHdrId);

        try {

            if ($odrDtl_NO) {
                $orderHdr->odrDtl_NO = $odrDtl_NO;
            }

            //BackOrder list
            $backOrderInfos = $this->orderHdrModel->getBackOrderList($orderHdr->odr_num);
            $orderIds = array_pluck($backOrderInfos, 'odr_id');
            $backOrderList = [];
            $backOrderIds = [];
            if ($backOrderInfos) {
                foreach ($backOrderInfos as $backOrderInfo) {
                    $backOrderIds = [$backOrderInfo->odr_id];
                    $backOrderList[] = [
                        'odr_id'                => $backOrderInfo->odr_id,
                        'odr_num'               => $backOrderInfo->odr_num,
                        'odr_sts'               => Status::getByKey('ORDER-STATUS', $backOrderInfo->odr_sts),
                        'odr_type'              => Status::getByKey('ORDER-TYPE', $backOrderInfo->odr_type),
                        'backOrderList_sku_ttl' => $this->orderDtlModel->countSku($backOrderIds),
                    ];
                }
            }
            $orderHdr->backOrderList = $backOrderList;

            //WorkOrder list
            $workOrderInfos = $this->workOrderModel->getAllWorkOrderOfOriginAndBackOrder($odr_num);
            $workOrderIds = array_pluck($workOrderInfos, 'wo_hdr_id');

            $workOrderList = [];
            if ($workOrderInfos) {
                foreach ($workOrderInfos as $workOrderInfo) {
                    $workOrderList[] = [
                        'wo_hdr_id'  => $workOrderInfo->wo_hdr_id,
                        'wo_hdr_num' => $workOrderInfo->wo_hdr_num,
                        'wo_sts'     => Status::getByKey('wo_status', $workOrderInfo->wo_sts)

                    ];

                }
            }
            $orderHdr->workOrderList_sku_ttl = $this->workOrderDtlModel->countSku($workOrderIds);
            $orderHdr->workOrderList = $workOrderList;

            //Wave Pick list
            $wavePickIds = $this->orderHdrModel->getDistinctWavePickByOrderIds($odr_num);
            $wvIds = array_pluck($wavePickIds, 'wv_id');
            $wvInfos = $this->wavepickHdrModel->getWavePick($wvIds);
            $WavePickList = [];
            if ($wvInfos) {
                foreach ($wvInfos as $wvInfo) {
                    $WavePickList[] = [
                        'wv_id'  => $wvInfo->wv_id,
                        'wv_num' => $wvInfo->wv_num,
                        'wv_sts' => Status::getByKey('WAVEPICK-STATUS', $wvInfo->wv_sts),

                    ];
                }
            }
            $orderHdr->wavPick_sku_ttl = $this->wavepickDtlModel->countSku($wvIds);
            $orderHdr->wavePick_ctn_ttl = $this->orderCartonModel->checkWhere([
                'odr_hdr_id' => $orderHdrId
            ]);

            $orderHdr->wavePickList = $WavePickList;

            //Packing list
            $packInfos = $this->packHdrModel->whereIn($orderHdrIds);
            $packHdrId = array_pluck($packInfos, 'pack_hdr_id');

            $packList = [];
            if ($packInfos) {
                $carton_ttl = 0;
                foreach ($packInfos as $packInfo) {
                    $packList[] = [
                        'pack_hdr_id'  => $packInfo->pack_hdr_id,
                        'pack_hdr_num' => $packInfo->pack_hdr_num,
                        'pack_sts'     => Status::getByKey('PACK-STATUS', $packInfo->pack_sts),
                    ];
                    $carton_ttl++;
                }
            }
            $orderHdr->carton_ttl = $carton_ttl;
            $orderHdr->packList_sku_ttl = $this->packDtlModel->countSku($packHdrId);
            $orderHdr->packList = $packList;

            //Pallet list that is shipping
            $GetPackDistinctOutPltIdByOrderIds = $this->packHdrModel->getDistinctOutPltIdByOrderIds(
                $orderHdrIds
            );
            $OutPltIds = array_pluck($GetPackDistinctOutPltIdByOrderIds, 'out_plt_id');
            $palletList = [];
            $carton_inPallets_ttl = 0;
            if ($OutPltIds) {
                foreach ($OutPltIds as $key => $value) {
                    $OutPltInfos = $this->outPalletModel->findWhere(
                        ['plt_id' => $value]
                    );

                    foreach ($OutPltInfos as $OutPltInfo) {
                        $palletList[] = [
                            'plt_id'  => object_get($OutPltInfo, 'plt_id', ''),
                            'plt_num' => object_get($OutPltInfo, 'plt_num', ''),
                            'ctn_ttl' => object_get($OutPltInfo, 'ctn_ttl', ''),
                        ];
                        $carton_inPallets_ttl = $carton_inPallets_ttl + object_get($OutPltInfo, 'ctn_ttl', '');
                    }

                }
            }
            $pallet_ttl = $this->packHdrModel->countPalletForAllOriginAndPackOrder($orderHdrIds);

            $orderHdr->pallet_ttl = $pallet_ttl;
            $orderHdr->carton_inPallets_ttl = $carton_inPallets_ttl;
            $orderHdr->palletList = $palletList;

            return $this->response->item($orderHdr, $orderCancelListTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderId
     * @param Request $request
     *
     * @throws \Exception
     */
    public function printWapLPN(
        $orderId,
        Request $request
    ) {
        $input = $request->getQueryParams();

        if (empty($input['new_lpn'])) {
            throw new \Exception("new_lpn is required!");
        }

        if (!is_numeric($input['new_lpn']) && $input['new_lpn'] <= 0) {
            throw new \Exception("new_lpn is invalid!");
        }

        $odrHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId, ['createdBy']);
        if (empty($odrHdr)) {
            throw new \Exception(Message::get("BM017", "Order"));
        }


        //$pdf = new TCPDF("L", PDF_UNIT, "A5", true, 'UTF-8', false);
        //$pdf->setPrintHeader(false);
        //$pdf->setPrintFooter(false);

        // set font
        //$pdf->SetFont('helvetica', '', 12);

        // add a page
        //$txt = "";
        //$pdf->AddPage();
        //for ($i = 1; $i <= $input['new_lpn']; $i++) {
        //    $licensePlate = array_get($odrHdr, "odr_num", null) . "-" . str_pad($i, 4, "0", STR_PAD_LEFT);
        //    $txt .= "\n\n";
        //    $txt .= "{$odrHdr->customer->cus_name}\n";
        //    $txt .= "{$odrHdr->odr_num}\n";
        //    $txt .= "\n\n\n";
        //    $licensePlate = str_replace("ORD", "LPN", $licensePlate);
        //    $txt .= "$licensePlate\n\n\n\n\n";
        //    if ($i % 2 == 0) {
        //        $pdf->write1DBarcode($licensePlate, 'C128', '68%', 95.5, 75, 14, 4, ['padding: 10px'], 'C');
        //        $pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
        //        $txt = "";
        //        $pdf->AddPage();
        //        continue;
        //    }
        //    $pdf->write1DBarcode($licensePlate, 'C128', '68%', 32, 75, 14, 4, ['padding: 10px'], 'C');
        //}
        //
        //if (!empty($txt)) {
        //    $pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
        //}

        DB::beginTransaction();
        $this->orderHdrModel->update(['odr_id' => $orderId, 'new_lpn' => $input['new_lpn']]);
        DB::commit();

        //Close and output PDF document
        //$pdf->Output("License_Plate_{$odrHdr->odr_num}.pdf", 'D');

        //print Wap LPN
        $odrHdrInfo = $odrHdr->toArray();
        $cusName = array_get($odrHdr, 'customer.cus_name', '');

        $LPNNo = $input['new_lpn'];
        $this->createWapLPNPdfFile($odrHdrInfo, $LPNNo, $cusName);
    }

    /**
     * @param $odrHdr
     * @param $LPNNo
     * @param $cusName
     *
     * @throws \MpdfException
     */
    private function createWapLPNPdfFile(
        $odrHdr,
        $LPNNo,
        $cusName
    ) {
        $pdf = new Mpdf();

        $CurrentDate=date('m/d/Y h:i:s a ', time());

        $html = (string)view('WapLPNListPrintoutTemplate', [
            'odrHdr'  => $odrHdr,
            'LPNNo'   => $LPNNo,
            'cusName' => $cusName,
            'CurrentDate' => $CurrentDate
        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        //$pdf->Output("$odrNum.pdf", "D");
        $pdf->Output();
    }

    /**
     * @param $shipId
     * @param Request $request
     *
     * @throws \Exception
     */
    public function printBolLPN(
        $shipId,
        Request $request
    ) {
        $input = $request->getQueryParams();

        if (empty($input['new_lpn'])) {
            throw new \Exception("new_lpn is required!");
        }

        if (!is_numeric($input['new_lpn']) || $input['new_lpn'] <= 0) {
            throw new \Exception("new_lpn is invalid!");
        }

        //check exist BOL
        $shipmentInfo = $this->shipmentModel->getBolLpn($shipId);
        if (empty($shipmentInfo)) {
            throw new \Exception(Message::get("BM017", "BOL"));
        }

        //Get Order Detail
        $OrderDetailInfos = $this->shipmentModel->getOrderDetailInfos($shipId);
        if (empty($OrderDetailInfos)) {
            throw new \Exception(Message::get("BM017", "Order"));
        }
        $OrderInfos = $this->shipmentModel->getOrderInfos($shipId);

        //print Wap LPN
        $shipmentInfoArr = $shipmentInfo->toArray();
        $cusName = array_get($shipmentInfoArr, 'cus_name', '');
        $LPNNo = $input['new_lpn'];
        $this->createBOLLPNPdfFile($shipmentInfoArr, $LPNNo, $cusName, $OrderInfos, $OrderDetailInfos);
    }

    /**
     * @param $shipmentInfoArr
     * @param $LPNNo
     * @param $cusName
     */
    private function createBOLLPNPdfFile(
        $shipmentInfoArr,
        $LPNNo,
        $cusName,
        $OrderInfos,
        $OrderDetailInfos
    ) {
        $pdf = new \Mpdf\Mpdf(['utf-8', 'A4']);
        $html = (string)view('BolLpnListPrintoutTemplate', [
            'shipmentInfoArr'  => $shipmentInfoArr,
            'LPNNo'            => $LPNNo,
            'cusName'          => $cusName,
            'OrderInfos'       => $OrderInfos,
            'OrderDetailInfos' => $OrderDetailInfos
        ]);

        $htmlOdrDTL = (string)view('BolLpnDetailOrderListPrintoutTemplate', [
            'shipmentInfoArr'  => $shipmentInfoArr,
            'LPNNo'            => $LPNNo,
            'cusName'          => $cusName,
            'OrderInfos'       => $OrderInfos,
            'OrderDetailInfos' => $OrderDetailInfos
        ]);

        $pdf->WriteHTML($html);

        //$pdf->AddPage();
        //
        //$pdf->WriteHTML($htmlOdrDTL);

        $pdf->Output();
    }

    public function updateShipDate(
        $orderId,
        Request $request
    ) {
        $input = $request->getParsedBody();

        if (empty($input['ship_dt'])) {
            throw new \Exception("The 'ship_dt' is required!");
        }

        $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId);
        if (empty($orderHdr)) {
            throw new \Exception(Message::get("BM017", "Order"));
        }

        if ($orderHdr->odr_sts != Status::getByValue("Shipped", "Order-Status")) {
            throw new \Exception("Order Status must be Shipped");
        }

        try {
            $this->orderHdrModel->refreshModel();
            DB::beginTransaction();
            $this->orderHdrModel->update([
                'odr_id'     => $orderHdr->odr_id,
                'shipped_dt' => strtotime($input['ship_dt'])
            ]);
            DB::commit();

            return ["status" => Response::HTTP_OK, "message" => "Update Successful!"];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $oderId
     * @param Request $request
     *
     * @return array|null|object
     * @throws \Exception
     */
    public function storeMultiCSR(
        $oderId,
        Request $request

    ) {
        $input = $request->getParsedBody();

        //validate
        //$validator = new OrderMultiCSRValidator();
        //$validator->validate($input);

        try {
            DB::beginTransaction();

            $predis = new Data();
            $userInfo = $predis->getUserInfo();
            $userId = array_get($userInfo, 'user_id', 0);

            DB::table('odr_csr')->where('odr_id', $oderId)
                ->delete();
            $orderCSRdata = [];
            foreach ($input['data'] as $val) {
                $data = [
                    //'odr_csr_id'     => $loc['odr_csr_id'],
                    'odr_id'     => $val['odr_id'],
                    'csr'        => $val['csr'],
                    'created_at' => time(),
                    'updated_at' => time(),
                    'created_by' => $userId,
                    'updated_by' => $userId,
                    'deleted'    => 0,
                    'deleted_at' => 915148800,

                ];
                $orderCSRdata[] = $data;
            }

            /**
             * Insert batch
             */
            $res = DB::table('odr_csr')->insert(
                $orderCSRdata
            );

            DB::commit();

            return $input;
        } catch (\PDOException $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $oderId
     * @param Request $request
     *
     * @return array|void
     */
    public function getListMultiCSR(
        $oderId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $orderCSRs = DB::table('odr_csr')
                ->select([
                    'odr_csr.odr_id',
                    'odr_csr.csr',

                    'u.emp_code',
                    'u.user_id',
                    'u.first_name',
                    'u.last_name',
                    'u.dept',
                    'u.off_loc',
                    'u.email',
                    'u.phone',
                    DB::raw("IF(u.status = 'AC','Active', 'Locked') AS status"),

                ])
                ->Join('users as u', 'u.user_id', '=', 'odr_csr.csr')
                ->where('odr_csr.odr_id', $oderId)
                ->get();

            return ['data' => $orderCSRs];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $oderCSRId
     *
     * @return array|void
     */
    public function showMultiCSR(
        $oderCSRId
    ) {
        try {
            $query = DB::table('odr_csr')
                ->select(['odr_csr_id', 'odr_id', 'csr', 'created_by', 'updated_at'])
                ->where('odr_csr_id', $oderCSRId)
                ->first();
            $odrCSRInfo = $query;

            if (!$odrCSRInfo) {
                throw new \Exception(Message::get("BM017", "CSR"));
            }

            return ['data' => $odrCSRInfo];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Update schd_ship_dt or routed_dt if exist
     *
     * @param Integer $orderId
     * @param Request $request
     *
     * @return mixed
     */
    public function editScheduleShipAndRoutedDate($orderId, Request $request)
    {
        $input = $request->getParsedBody();
        try {
            $updateParams = [];
            if (isset($input['schd_ship_dt']) && $input['schd_ship_dt'] != '') {
                $updateParams['schd_ship_dt'] = strtotime($input['schd_ship_dt']);
            }
            if (isset($input['routed_dt']) && $input['routed_dt'] != '') {
                $updateParams['routed_dt'] = strtotime($input['routed_dt']);
            }

            if (!empty($updateParams)) {
                $updateParams['odr_id'] = $orderId;
                $this->orderHdrModel->update($updateParams);

                return ['data' => ['message' => 'Updated order successfully.']];
            } else {
                return ['data' => ['message' => 'Nothing to update.']];
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Integer $orderId
     * @param Request $request
     *
     * @return mixed
     */
    public function updateOrderComments($orderId, Request $request)
    {
        $input = $request->getParsedBody();
        $oldOrderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId);
        try {
            $updateParams = [];
            if (isset($input['cus_notes'])) {
                $updateParams['cus_notes'] = $input['cus_notes'];
            }
            if (isset($input['in_notes'])) {
                $updateParams['in_notes'] = $input['in_notes'];
            }
            if (isset($input['csr_notes'])) {
                $updateParams['csr_notes'] = $input['csr_notes'];
            }
            if (isset($input['rush_odr'])) {
                $updateParams['rush_odr'] = $input['rush_odr'];
            }

            if (!empty($updateParams)) {
                $updateParams['odr_id'] = $orderId;
                $this->orderHdrModel->update($updateParams);
                // Insert Event Tracking
                $eventTrackingInfo = [];
                if (strcmp($input['in_notes'], $oldOrderHdr['in_notes']) != 0) {
                    array_push($eventTrackingInfo, 'WHS Comments');
                }
                if (strcmp($input['cus_notes'], $oldOrderHdr['cus_notes']) != 0) {
                    array_push($eventTrackingInfo, 'External Comments');
                }
                if (strcmp($input['csr_notes'], $oldOrderHdr['csr_notes']) != 0) {
                    array_push($eventTrackingInfo, 'CSR Comments');
                }
                if (!empty($eventTrackingInfo)) {
                    $this->eventTrackingModel->refreshModel();
                    $this->eventTrackingModel->create([
                        'whs_id'    => $oldOrderHdr['whs_id'],
                        'cus_id'    => $oldOrderHdr['cus_id'],
                        'owner'     => $oldOrderHdr['odr_num'],
                        'evt_code'  => Status::getByKey("event", "ORDER-COMMENT-EDIT"),
                        'trans_num' => $oldOrderHdr['odr_num'],
                        'info'      => 'Updated to ' . implode(", ", $eventTrackingInfo)
                    ]);
                }

                return ['data' => ['message' => 'Updated order successfully.']];
            } else {
                return ['data' => ['message' => 'Nothing to update.']];
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function updateCommentsForManyOrders(
        Request $request
    ) {
        $input = $request->getParsedBody();
        try {
            $updateParams = [];
            if (isset($input['cus_notes'])) {
                $updateParams['cus_notes'] = $input['cus_notes'];
            }
            if (isset($input['in_notes'])) {
                $updateParams['in_notes'] = $input['in_notes'];
            }
            if (isset($input['csr_notes'])) {
                $updateParams['csr_notes'] = $input['csr_notes'];
            }


            if (!empty($updateParams)) {
                $orderIds = $input['orderIds'];

                DB::beginTransaction();

                // Insert Event Tracking
                if($orderIds){
                    foreach($orderIds as $orderId){
                        $oldOrderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId);
                        $eventTrackingInfo = array();

                        if (isset($input['in_notes']) && strcmp($input['in_notes'], $oldOrderHdr['in_notes']) != 0)
                        {
                            array_push($eventTrackingInfo, 'WHS Comments');
                        }
                        if (isset($input['cus_notes']) && strcmp($input['cus_notes'], $oldOrderHdr['cus_notes']) != 0) {
                            array_push($eventTrackingInfo, 'External Comments');
                        }
                        if (isset($input['csr_notes']) && strcmp($input['csr_notes'], $oldOrderHdr['csr_notes']) != 0) {
                            array_push($eventTrackingInfo, 'CSR Comments');
                        }
                        if(!empty($eventTrackingInfo)){
                            $this->eventTrackingModel->refreshModel();
                            $this->eventTrackingModel->create([
                                'whs_id'    => $oldOrderHdr['whs_id'],
                                'cus_id'    => $oldOrderHdr['cus_id'],
                                'owner'     => $oldOrderHdr['odr_num'],
                                'evt_code'  => Status::getByKey("event", "ORDER-COMMENT-EDIT"),
                                'trans_num' => $oldOrderHdr['odr_num'],
                                'info'      => 'Updated to ' . implode(", ", $eventTrackingInfo)
                            ]);
                        }
                    }
                }

                $this->orderHdrModel->updateCommentsForManyOrders($orderIds, $updateParams);

                DB::commit();

                return ['data' => ['message' => 'Updated order successfully.']];
            } else {
                return ['data' => ['message' => 'Nothing to update.']];
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function updateUserMeta(Int $userId, String $type)
    {
        DB::table('user_meta')
            ->where('user_id', $userId)
            ->where('qualifier', 'OSF')
            ->delete();
    }

    public function autocompleteCarrier(Request $request) {
        try{
            $input = $request->getQueryParams();

            $limit = (!empty($input['limit']))?$input['limit']:20;
            $carrier = DB::table("odr_hdr")
                ->where("carrier","like","%". $input['name'] ."%")
                ->select(DB::raw("DISTINCT(carrier) as carrier_name"))
                ->limit($limit)
                ->get();

            return ['data' => $carrier];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _createNewItemEcom($itemId, $whsId)
    {
        $iTemp = DB::table('item')->where('item_id', $itemId)->first();
        $tempInvtSmr = DB::table('invt_smr')->where('item_id', $itemId)->first();
        if(!empty($iTemp) && $iTemp['pack'] != 1){
            unset($iTemp['item_id']);
            $iTemp['pack'] = 1;
            $nTemp = $this->itemModel->create($iTemp);
            $itemId = $nTemp->item_id;
        }
        // insert new invt-smr
        if(!empty($tempInvtSmr) && $tempInvtSmr['lot'] != 'ECO'){
            unset($tempInvtSmr['inv_sum_id']);
            $tempInvtSmr['item_id'] = $itemId;
            $tempInvtSmr['lot'] = 'ECO';
            $tempInvtSmr['ttl'] = 0;
            $tempInvtSmr['picked_qty'] = 0;
            $tempInvtSmr['allocated_qty'] = 0;
            $tempInvtSmr['dmg_qty'] = 0;
            $tempInvtSmr['avail'] = 0;
            $tempInvtSmr['back_qty'] = 0;
            $tempInvtSmr['ecom_qty'] = 0;
            $tempInvtSmr['lock_qty'] = 0;
            $tempInvtSmr['crs_doc_qty'] = 0;
            $tempInvtSmr['created_at'] = time();
            $tempInvtSmr['updated_at'] = time();
            $nInvtSmr = DB::table('invt_smr')->insert($tempInvtSmr);
        } elseif (!empty($iTemp)) {
            $tempInvtSmr['item_id'] = $itemId;
            $tempInvtSmr['cus_id'] = $iTemp['cus_id'];
            $tempInvtSmr['whs_id'] = $whsId;
            $tempInvtSmr['sku'] = $iTemp['sku'];
            $tempInvtSmr['color'] = $iTemp['color'];
            $tempInvtSmr['size'] = $iTemp['size'];
            $tempInvtSmr['upc'] = $iTemp['cus_upc'];
            $tempInvtSmr['lot'] = 'ECO';
            $tempInvtSmr['ttl'] = 0;
            $tempInvtSmr['picked_qty'] = 0;
            $tempInvtSmr['allocated_qty'] = 0;
            $tempInvtSmr['dmg_qty'] = 0;
            $tempInvtSmr['avail'] = 0;
            $tempInvtSmr['back_qty'] = 0;
            $tempInvtSmr['ecom_qty'] = 0;
            $tempInvtSmr['lock_qty'] = 0;
            $tempInvtSmr['crs_doc_qty'] = 0;
            $tempInvtSmr['created_at'] = time();
            $tempInvtSmr['updated_at'] = time();
            $nInvtSmr = DB::table('invt_smr')->insert($tempInvtSmr);
        }

        return $itemId;
    }

    public function getCarrier($cusId, $odrId)
    {
        $order = $this->orderHdrModel->getModel()
                    ->where('cus_id', $cusId)
                    ->where('odr_id', $odrId)
                    ->first();

        if ( !$order ) {
            $msg = 'Order not exists';
            return $this->response->errorBadRequest($msg);
        }

        $data = [
            'carrier'   => $order->carrier
        ];

        return ['data' => $data];
    }

    public function updateTrackNum(
        $orderId,
        Request $request,
        OrderHdrTransformer $orderHdrTransformer
    )
    {
        // get data from HTTPs
        $input = $request->getParsedBody();
        Profiler::log('updateTrackNum: '.json_encode(['orderId'=>$orderId,'input'=>$input]));
        $orderHdr = DB::table('odr_hdr')->where('odr_id', $orderId)->first();
        $listStatusAllow = ['PA', 'RS', 'SS', 'PTD'];
        if ($orderHdr && in_array($orderHdr['odr_sts'], $listStatusAllow)){
            try {
                $params = [
                    'track_num' => array_get($input, 'track_num', null),
                    'odr_id' => $orderId,
                ];
                $orderHdr = $this->orderHdrModel->update($params);
                if (!$orderHdr) {
                    throw new \Exception(Message::get("BM010"));
                }

                DB::table('pack_hdr')->where('odr_hdr_id', $orderId)->update(['tracking_number' => array_get($input, 'track_num', null)]);

                return $this->response->item($orderHdr, $orderHdrTransformer)->setStatusCode(Response::HTTP_OK);
            } catch (\PDOException $e) {
                DB::rollback();
                return $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
                );
            } catch (\Exception $e) {
                DB::rollback();

                return $this->response->errorBadRequest($e->getMessage());
            }
        } else {
            throw new \Exception("Order can not update track num");
        }
    }

    public function getShippingInfo($orderId)
    {
        try {
            $valueArray = [];
            $orderMeta = DB::table('odr_hdr_meta')->where('odr_id', $orderId)->where('qualifier', self::DELIVERY_ORDER_QUALIFIER)->first();
            if (! empty($orderMeta)) {
                $valueArray = \GuzzleHttp\json_decode(array_get($orderMeta, 'value'), true);
            }
            $shippingInfo = [
                'cus_sig' => array_get($valueArray, 'cus_sig', null)
                    ? Storage::disk('s3')->url(base64_decode(array_get($valueArray, 'cus_sig')))
                    : null,
                'img' => array_get($valueArray, 'cus_sig', null)
                    ? Storage::disk('s3')->url(base64_decode(array_get($valueArray, 'img')))
                    : null,
                'note' => array_get($valueArray, 'note', null)
            ];
            return [
                'data' => $shippingInfo
            ];
        } catch (\Exception $e) {
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }
}
