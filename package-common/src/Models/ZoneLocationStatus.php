<?php

namespace Seldat\Wms2\Models;

/**
 *
 * @property mixed loc_sts_code
 * @property mixed loc_sts_desc
 * @property mixed loc_sts_name
 */
class ZoneLocationStatus extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loc_status';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;

    public $incrementing = false;

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'loc_sts_code',
        'loc_sts_name',
        'loc_sts_desc',
    ];

    public function location()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_sts_code', 'loc_sts_code');
    }
}
