<?php

namespace App\Api\V1\Models;



use Seldat\Wms2\Models\CustomerZone;

class CustomerZoneModel extends AbstractModel
{
    protected $model;

    public function __construct(CustomerZone $model = null)
    {
        $this->model = ($model) ?: new CustomerZone();
    }
}
