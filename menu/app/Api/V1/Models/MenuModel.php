<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 11:09 AM
 */

namespace App\Api\V1\Models;

use App\Menu;
use App\Api\V1\Validators\MenuValidator;

class MenuModel extends AbstractModel
{
    /**
     * @var QueryBuilder
     */
    protected $model;

    /**
     * @param Menu $model
     */
    public function __construct(Menu $model = null)
    {
        $this->model = ($model) ?: new Menu();
    }

    public function saveMass($menuGroupId, $input, MenuValidator $menuValidator, MenuFavoriteModel $menuFavoriteModel)
    {
        $newMenuIds = [];
        $result = $this->_saveMass($menuGroupId, $input, $newMenuIds, 0, 0, $menuValidator);

        $allMenuIdsOfGroup = $this->listMenuIdsByMenuGroupId($menuGroupId);

        // delete non-use id
        if ($removingMenuIds = array_diff($allMenuIdsOfGroup, $newMenuIds)) {
            //delete favorite menu
            $menuFavoriteModel->deleteByMenuIds($removingMenuIds);
            // delete menu
            $this->deleteById($removingMenuIds);
        }

        return $result;
    }

    private function _saveMass($menuGroupId, $input, &$menuIds, $parentId, $displayOrder, MenuValidator $menuValidator)
    {
        foreach ($input as $eachMenu) {
            // assign menu group id
            $eachMenu['menu_group_id'] = $menuGroupId;
            $eachMenu['parent_id'] = $parentId;
            $eachMenu['display_order'] = ++$displayOrder;

            // create or update
            $result = (isset($eachMenu['menu_id']) && !empty($eachMenu['menu_id']))
                ? $this->_updateData($eachMenu['menu_id'], $eachMenu, $menuValidator)
                : $this->_createData($eachMenu, $menuValidator);

            // stop, has error
            if (!$result) {
                return false;
            }

            $menuId = $result->getKey();
            $menuIds[] = $menuId;

            // recursive
            if (isset($eachMenu['nodes']) && !empty($eachMenu['nodes'])
                && !$this->_saveMass($menuGroupId, $eachMenu['nodes'], $menuIds, $menuId, 0, $menuValidator)
            ) {

                return false;
            }
        }

        return true;
    }

    protected function _upsertData($input, MenuValidator $menuValidator, $menuId = false)
    {
        try {
            $menuValidator->validate($input);
        } catch (\Exception $e) {
            throw $e;
        }

        $params = [
            'parent_id'       => $input['parent_id'],
            'menu_group_id'   => $input['menu_group_id'],
            'name'            => trim($input['name']),
            'description'     => trim(array_get($input, 'description', '')),
            'url'             => trim($input['url']),
            'display_order'   => $input['display_order'],
            'target'          => trim(array_get($input, 'target', '')),
            'permission_name' => trim(array_get($input, 'permission_name', '')),
            'icon_class'      => trim(array_get($input, 'icon_class', '')),
        ];

        if ($menuId) {
            $params['menu_id'] = $menuId;
        }

        try {
            // refresh model before save
            $this->refreshModel();

            if ($menuId) {
                if ($result = $this->update($params)) {
                    return $result;
                }
            } else {
                if ($result = $this->create($params)) {
                    return $result;
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }

        return false;
    }

    protected function _createData($input, MenuValidator $menuValidator)
    {
        return $this->_upsertData($input, $menuValidator);
    }

    protected function _updateData($menuId, $input, MenuValidator $menuValidator)
    {
        return $this->_upsertData($input, $menuValidator, $menuId);
    }

    /**
     * @param int $menuGroupId
     *
     * @return array
     */
    public function getHierarchyMenuByMenuGroupId($menuGroupId)
    {
        $data = $this->model
            ->where('menu_group_id', $menuGroupId)
            ->orderBy('display_order', 'asc')
            ->get();

        return $this->drawHierarchy($data);
    }

    /**
     * @param int $menuGroupId
     * @param array $permissionNames
     *
     * @return array
     */
    public function getHierarchyMenuByPermissionName($menuGroupId, $permissionNames)
    {
        $data = $this->model
            ->where('menu_group_id', $menuGroupId)
            ->where(function ($query) use ($permissionNames) {
                $query->whereIn('permission_name', $permissionNames)
                    ->orWhere(['parent_id' => 0, 'permission_name' => 'viewMenu']);
            })
            ->orderBy('display_order', 'asc')
            ->get();

        return $this->drawHierarchy($data);
    }

    /**
     * draw hierarchy category without recursive
     *
     * @link http://stackoverflow.com/questions/3116330/recursive-categories-with-a-single-query
     *
     * @param array $data
     *
     * @return array
     */
    public function drawHierarchy($data)
    {
        $tmpData = [];
        foreach ($data as $item) {
            unset($item->menu_group_id);
            unset($item->created_at);
            unset($item->updated_at);

            $tmpData[$item->menu_id] = array_merge($item->toArray(), ['nodes' => []]);
        }

        $tree = [];
        foreach ($tmpData as &$item) {
            if (!$item['parent_id'] || !isset($tmpData[$item['parent_id']])) {
                $tree[] = &$item;
            } else {
                $tmpData[$item['parent_id']]['nodes'][] = &$item;
            }
        }

        //remove empty parent menu
        $return =[];
        foreach ($tree as $i => $item2) {
            if ($item2['permission_name'] == 'viewMenu' && empty($item2['nodes'])) {
                //unset($ite);
            }else{
                $return[]=$item2;
            }

        }
        
        return $return;
    }

    public function listMenuIdsByMenuGroupId($menuGroupId)
    {
        $result = $this->model->where('menu_group_id', $menuGroupId)
            ->select('menu_id')
            ->get();

        $res = [];
        foreach ($result as $item) {
            $res[] = $item['menu_id'];
        }

        return $res;
    }

    /**
     * Delete Menu, then also delete favorite menu
     * and then move (if there) its children to root, and update display_order to 0
     *
     * @param $menuId
     * @param MenuFavoriteModel $menuFavoriteModel
     *
     * @return bool
     */
    public function destroyMenu($menuId, MenuFavoriteModel $menuFavoriteModel)
    {
        $menuFavoriteModel->deleteByMenuIds([$menuId]);
        $this->deleteById($menuId);

        $this->model
            ->where('parent_id', $menuId)
            ->update(['parent_id' => 0, 'display_order' => 0]);

        return true;
    }
}