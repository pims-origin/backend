<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
//        'cache' => [
//            'class' => 'yii\caching\FileCache',
//        ],
        'authManager' => [
            'class' => 'common\rbac\DbManager',
            'itemTable' => 'authority',
            'itemChildTable' => 'authority_child',
            'assignmentTable' => 'user_role',
            'ruleTable' => 'athrty_rule'
        ],
    ],
];
