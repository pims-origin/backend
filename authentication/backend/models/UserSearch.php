<?php

namespace backend\models;

use common\models\User;
use yii\db\Query;
use yii\data\Pagination;
use backend\forms\Form;
use backend\forms\UserSearchForm;
use Yii;

class UserSearch extends BaseUser
{
    private static $_instance;

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new UserSearch();
        }

        return self::$_instance;
    }

    public static function getUsers($user_ids)
    {
        $query = (new Query());
        $users = $query->from('users')
            ->where(['user_id' => $user_ids])
            ->select(UserSearchForm::ALLOW_SELECT_FIELDS)
            ->all();
        if (null === $users) {
            return false;
        }

        return $users;
    }

    protected static function _getUsersQuery(Form $form, &$count)
    {
        $query = (new Query());
        $query->from('users');
        $query->where(["!=", 'status', User::STATUS_DELETED]);
        $params = $form->makeUserCriteria();
        $form->makeOrder();

        foreach ($params as $field => $detail) {

            if ($field == 'item_name') {
                $query->innerJoin(['ur' => 'user_role'], 'ur.user_id = users.user_id');
            }

            $query->andWhere(
                [
                    $detail['opr'],
                    $field,
                    $detail['value']
                ]
            );

        }

        $count = $query->count();

        if (!$count) {
            return false;
        }

        return $query;
    }

    public static function getList(Form $form)
    {
        $query = self::_getUsersQuery($form, $count);
        if (!$query) {
            return [];
        }

        // Pagination
        $pages = new Pagination(
            [
                'totalCount'    => $count,
                'pageSize'      => $form->limit,
                'page'          => $form->page - 1,
                'pageSizeParam' => 'limit'
            ]
        );

        if (!empty($form->sort)) {
            $query->orderBy($form->sort);
        }

        $arrSelect = [];
        if (UserSearchForm::ALLOW_SELECT_FIELDS) {
            foreach (UserSearchForm::ALLOW_SELECT_FIELDS as $field) {
                $arrSelect[] = "users.$field";
            }
        }

        $data = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->select(!empty($arrSelect) ? $arrSelect : 'users.*')
            ->all();

        // Meta
        $meta['totalCount'] = $pages->totalCount;
        $meta['pageCount'] = $pages->pageCount;
        $meta['currentPage'] = $pages->page + 1;
        $meta['perPage'] = $pages->limit;

        $links = self::makePaginationLinks($pages->getLinks(true));

        return [
            'data' => $data,
            'link' => $links,
            'meta' => $meta,
        ];
    }

    public static function getCsrByCustomerWarehouse($cusIds, $whsId)
    {
        $fields = [
            'u.user_id',
            'u.username',
            'u.email',
            'u.emp_code',
            'u.first_name',
            'u.last_name',
            'u.created_at',
            'u.updated_at',
            'u.dept',
            'u.off_loc',
            'u.phone',
            "status" => "if(u.status='AC', 'Active', 'Locked')",
        ];

        //$cusIds = explode(',', $cusIds);
        //if (empty($cusIds)) {
        //    return false;
        //}

        $url = Yii::$app->params['authorization_api'] . 'users/csr';
        $resp = Yii::$app->httpclient->get($url, [], [], true);

        if ($resp->getStatusCode() !== 200) {
            throw new \Exception("Can't get CSR user list");
        }

        $userIds = \GuzzleHttp\json_decode($resp->getBody(), true)['data'];

        $arrCusMeta = (new Query())
            ->from(['cm' => 'cus_meta'])
            ->select(['value'])
           // ->where(['in', 'cm.cus_id', $cusIds])
            ->andWhere("cm.qualifier = 'CSR'")->all();

        $arrUser = [];
        foreach ($arrCusMeta as $cusMeta) {
            foreach ($cusMeta as $value) {
                $user = \GuzzleHttp\json_decode($value, true);
                $arrUser = array_merge($arrUser, $user);
            }
        }

        $arrUserIds = [];
        foreach ($arrUser as $objUser) {
            $value = \GuzzleHttp\json_decode($objUser, true);
            if (!in_array($value['user_id'], $arrUserIds) && $value['whs_id'] == $whsId) {
                $arrUserIds[] = (int)$value['user_id'];
            }
        }

        $query = (new Query())
            ->from(['u' => 'users'])
            ->select($fields)
            ->groupBy('u.user_id')
            ->andWhere(['in', 'u.user_id', $userIds])
            ->andWhere(['in', 'u.user_id', $arrUserIds])
            ->andWhere("u.status != 'IA'")
            ->orderBy("u.first_name");

        return [
            'data' => $query->all(),
        ];
    }

    public static function getCsrByWarehouse($whsId)
    {
        $fields = [
            'u.user_id',
            'u.username',
            'u.email',
            'u.emp_code',
            'u.first_name',
            'u.last_name',
            'u.created_at',
            'u.updated_at',
            'u.dept',
            'u.off_loc',
            'u.phone',
            "status" => "if(u.status='AC', 'Active', 'Locked')",
        ];

        $query = (new Query)
            ->from('user_role ur')
            ->innerJoin(['ac' => 'authority_child'], 'ur.item_name = ac.parent')
            ->innerJoin(['uw' => 'user_whs'], 'uw.user_id = ur.user_id')
            ->innerJoin(['u' => 'users'], 'u.user_id = ur.user_id')
            ->select($fields)
            ->groupBy('u.user_id')
            ->where("ac.child = 'isCSR'")
            ->andWhere("u.status != 'IA'")
            ->andWhere(['uw.whs_id' => $whsId]);

        return [
            'data' => $query->all(),
        ];
    }


    public static function getPickerByWarehouse($whsId)
    {
        $fields = [
            'u.user_id',
            'u.username',
            'u.email',
            'u.emp_code',
            'u.first_name',
            'u.last_name',
            'u.created_at',
            'u.updated_at',
            'u.dept',
            'u.off_loc',
            'u.phone',
            "status" => "if(u.status='AC', 'Active', 'Locked')",
        ];

        $query = (new Query)
            ->from('user_role ur')
            ->innerJoin(['ac' => 'authority_child'], 'ur.item_name = ac.parent')
            ->innerJoin(['uw' => 'user_whs'], 'uw.user_id = ur.user_id')
            ->innerJoin(['u' => 'users'], 'u.user_id = ur.user_id')
            ->select($fields)
            ->groupBy('u.user_id')
            ->where("ac.child = 'updateOrderPicking'")
            ->andWhere("u.status != 'IA'")
            ->andWhere(['uw.whs_id' => $whsId]);

        return [
            'data' => $query->all(),
        ];
    }

    public static function getAllUser(Form $form)
    {
        $query = self::_getUsersQuery($form, $count);
        if (!$query) {
            return [];
        }

        if (!empty($form->sort)) {
            $query->orderBy($form->sort);
        }

        $data = $query->select(UserSearchForm::ALLOW_SELECT_FIELDS)->all();

        return [
            'data' => $data
        ];
    }

    public static function makePaginationLinks(array $links)
    {
        $result = [];
        foreach ($links as $key => $link) {
            $result[$key] = [
                'href' => $link,
            ];
        }

        return $result;
    }

    public static function getUserWhs($userId)
    {
        $query = (new Query());
        $userWhs = $query->from('user_whs')
            ->where(['user_id' => $userId])
            ->select('whs_id')
            ->column();
        if (null === $userWhs) {
            return false;
        }

        return $userWhs;
    }

    public function getFullUserInfo($id)
    {
        return $this->_getUserInfo($id);
    }

    public static function getCustomerCsrByCustomer($cusIds, $whsId)
    {
        $fields = [
            'u.user_id',
            'u.username',
            'u.email',
            'u.emp_code',
            'u.first_name',
            'u.last_name',
            'u.created_at',
            'u.updated_at',
            'u.dept',
            'u.off_loc',
            'u.phone',
            "status" => "if(u.status='AC', 'Active', 'Locked')",
        ];

        $cusIds = explode(',', $cusIds);
        if (empty($cusIds)) {
            return false;
        }

        $userId = yii::$app->user->getId();
        $userMeta = new UserMetaProcess();
        //$wid = $userMeta->get($userId, 'WID');

        $url = Yii::$app->params['authorization_api'] . 'users/csr';
        $resp = Yii::$app->httpclient->get($url, [], [], true);

        if ($resp->getStatusCode() !== 200) {
            throw new \Exception("Can't get CSR user list");
        }

        $userIds = \GuzzleHttp\json_decode($resp->getBody(), true)['data'];

        $query = (new Query())
            ->from(['u' => 'users'])
            ->select($fields)
            ->addSelect(['totalCus' => 'COUNT(ciu.cus_id)'])
            ->innerJoin(['ciu' => 'cus_in_user'], 'ciu.user_id = u.user_id')
            ->groupBy('u.user_id')
            ->where('ciu.whs_id = :whsId', ['whsId' => $whsId])
            ->andWhere(['in', 'ciu.cus_id', $cusIds])
            ->andWhere(['in', 'u.user_id', $userIds])
            ->andWhere("u.status != 'IA'")
            ->orderBy("u.first_name")
            ->having('totalCus = :totalCus', ['totalCus' => count($cusIds)]);

        return [
            'data' => $query->all(),
        ];
    }

    public static function getCustomerPickerByCustomerWarehouse($cusIds, $whsId)
    {
        $fields = [
            'u.user_id',
            'u.username',
            'u.email',
            'u.emp_code',
            'u.first_name',
            'u.last_name',
            'u.created_at',
            'u.updated_at',
            'u.dept',
            'u.off_loc',
            'u.phone',
            "status" => "if(u.status='AC', 'Active', 'Locked')",
        ];
        $cusIds = array_unique(explode(',', $cusIds));
        if (empty($cusIds)) {
            return false;
        }

        $userId = yii::$app->user->getId();
        $userMeta = new UserMetaProcess();
        $wid = $userMeta->get($userId, 'WID');

        $url = Yii::$app->params['authorization_api'] . 'users/picker';
        $resp = Yii::$app->httpclient->get($url, [], [], true);

        if ($resp->getStatusCode() !== 200) {
            throw new \Exception("Can't get Picker user list");
        }

        $userIds = \GuzzleHttp\json_decode($resp->getBody(), true)['data'];

        $query = (new Query())
            ->from(['u' => 'users'])
            ->select($fields)
            ->addSelect(['totalCus' => 'COUNT(ciu.cus_id)'])
            ->innerJoin(['ciu' => 'cus_in_user'], 'ciu.user_id = u.user_id')
            ->groupBy('u.user_id')
            ->where('ciu.whs_id = :whsId', ['whsId' => $whsId])
            ->andWhere(['in', 'ciu.cus_id', $cusIds])
            ->andWhere(['in', 'u.user_id', $userIds])
            ->andWhere("u.status != 'IA'")
            ->orderBy("u.first_name")
            ->having('totalCus = :totalCus', ['totalCus' => count($cusIds)]);

        return [
            'data' => $query->all(),
        ];
    }

    public static function getPutterByCustomerWarehouse($cusIds, $whsId)
    {
        $fields = [
            'u.user_id',
            'u.username',
            'u.email',
            'u.emp_code',
            'u.first_name',
            'u.last_name',
            'u.created_at',
            'u.updated_at',
            'u.dept',
            'u.off_loc',
            'u.phone',
            "status" => "if(u.status='AC', 'Active', 'Locked')",
        ];
        $cusIds = array_unique(explode(',', $cusIds));
        if (empty($cusIds)) {
            return false;
        }

        $userId = yii::$app->user->getId();
        $userMeta = new UserMetaProcess();
        $wid = $userMeta->get($userId, 'WID');

        $url = Yii::$app->params['authorization_api'] . 'users/putter';
        $resp = Yii::$app->httpclient->get($url, [], [], true);

        if ($resp->getStatusCode() !== 200) {
            throw new \Exception("Can't get Putter user list");
        }

        $userIds = \GuzzleHttp\json_decode($resp->getBody(), true)['data'];

        $query = (new Query())
            ->from(['u' => 'users'])
            ->select($fields)
            ->addSelect(['totalCus' => 'COUNT(ciu.cus_id)'])
            ->innerJoin(['ciu' => 'cus_in_user'], 'ciu.user_id = u.user_id')
            ->groupBy('u.user_id')
            ->where('ciu.whs_id = :whsId', ['whsId' => $whsId])
            ->andWhere(['in', 'ciu.cus_id', $cusIds])
            ->andWhere(['in', 'u.user_id', $userIds])
            ->andWhere("u.status != 'IA'")
            ->orderBy("u.first_name")
            ->having('totalCus = :totalCus', ['totalCus' => count($cusIds)]);

        return [
            'data' => $query->all(),
        ];
    }

    public static function getPutBackerByCustomerWarehouse($cusIds, $whsId)
    {
        $fields = [
            'u.user_id',
            'u.username',
            'u.email',
            'u.emp_code',
            'u.first_name',
            'u.last_name',
            'u.created_at',
            'u.updated_at',
            'u.dept',
            'u.off_loc',
            'u.phone',
            "status" => "if(u.status='AC', 'Active', 'Locked')",
        ];
        $cusIds = array_unique(explode(',', $cusIds));
        if (empty($cusIds)) {
            return false;
        }

        $userId = yii::$app->user->getId();
        $userMeta = new UserMetaProcess();
        $wid = $userMeta->get($userId, 'WID');

        $url = Yii::$app->params['authorization_api'] . 'users/put-backer';
        $resp = Yii::$app->httpclient->get($url, [], [], true);

        if ($resp->getStatusCode() !== 200) {
            throw new \Exception("Can't get Putter user list");
        }

        $userIds = \GuzzleHttp\json_decode($resp->getBody(), true)['data'];

        $query = (new Query())
            ->from(['u' => 'users'])
            ->select($fields)
            ->addSelect(['totalCus' => 'COUNT(ciu.cus_id)'])
            ->innerJoin(['ciu' => 'cus_in_user'], 'ciu.user_id = u.user_id')
            ->groupBy('u.user_id')
            ->where('ciu.whs_id = :whsId', ['whsId' => $whsId])
            ->andWhere(['in', 'ciu.cus_id', $cusIds])
            ->andWhere(['in', 'u.user_id', $userIds])
            ->andWhere("u.status != 'IA'")
            ->orderBy("u.first_name")
            ->having('totalCus = :totalCus', ['totalCus' => count($cusIds)]);

        return [
            'data' => $query->all(),
        ];
    }

}
