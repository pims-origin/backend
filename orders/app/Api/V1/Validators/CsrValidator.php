<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 7-Sep-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class CsrValidator
 *
 * @package App\Api\V1\Validators
 */
class CsrValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'user_id' => 'required|integer',
            'odr_id'  => 'required',
        ];
    }

    public function checkUserStatus($userStatus)
    {
        switch ($userStatus) {
            case config('constants.INACTIVE'):
                throw new UnknownVersionException('The user is being inactive.', null, 5);

            default:
                break;
        }
    }

}
