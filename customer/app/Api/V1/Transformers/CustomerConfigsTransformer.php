<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:19
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerConfig;
use App\Api\V1\Models\CustomerConfigModel;
use League\Fractal\TransformerAbstract;

class CustomerConfigsTransformer extends TransformerAbstract
{

    /**
     * @param CustomerConfig $customerConfig
     *
     * @return array
     */
    public function transform(CustomerConfig $customerConfig)
    {
        $whsId = object_get($customerConfig, 'whs_id', '');
        $cusId = object_get($customerConfig, 'cus_id', '');
        $edi = object_get($customerConfig, 'edi', '');
        $customerConfigModel = new CustomerConfigModel();
        $customerConfigData = $customerConfigModel->getCustomerEdi($whsId, $cusId, $edi, $customerConfigModel = []);
        $customerConfigDatas = $customerConfigData->toArray();
        $edi = 0;
        if (!empty($customerConfigDatas)) {
            $ac = array_pluck($customerConfigDatas, 'ac');
            foreach ($ac as $acc) {
                if ($acc) {
                    if ($acc == 'y' || $acc == 'Y') {
                        $edi = 1;
                    }
                }
            }
        }

        return [
            'cus_id'       => $customerConfig->cus_id,
            'whs_id'       => $customerConfig->whs_id,
            'config_name'  => $customerConfig->config_name,
            'config_value' => $customerConfig->config_value,
            'ac'           => $customerConfig->ac,
            'cus_name'     => object_get($customerConfig, 'customer.cus_name', ''),
            'whs_name'     => object_get($customerConfig, 'warehouse.whs_name', ''),
            'edi'          => $edi
        ];
    }
}
