<?php

namespace App\Api\V1\Validators;


class LocationPalletListByWvIDValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'wv_id' => 'required'
        ];
    }
}
