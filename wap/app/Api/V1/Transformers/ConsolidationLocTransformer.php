<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Item;

class ConsolidationLocTransformer extends TransformerAbstract
{
    //return result
    public function transform(Carton $carton)
    {
        return [
            //carton
            'item_id'         => object_get($carton, 'item_id', null),
            'ctn_num'         => object_get($carton, 'ctn_num', null),
            'ctn_id'          => object_get($carton, 'ctn_id', null),
            //item
            'sku'             => array_get($carton, 'item.sku', null),
            'size'            => array_get($carton, 'item.size', null),
            'color'           => array_get($carton, 'item.color', null),
            //asnDtl
            'asn_dtl_lot'     => array_get($carton, 'AsnDtl.asn_dtl_lot', null),
            'asn_dtl_pack'    => array_get($carton, 'AsnDtl.asn_dtl_pack', null),
            'asn_dtl_cus_upc' => array_get($carton, 'AsnDtl.asn_dtl_cus_upc', null),
            'uom_name'        => array_get($carton, 'AsnDtl.systemUom.sys_uom_name', null),
            'uom_id'          => array_get($carton, 'AsnDtl.systemUom.sys_uom_id', null),
            'asn_dtl_weight'  => array_get($carton, 'AsnDtl.asn_dtl_weight', null),
            'asn_dtl_height'  => array_get($carton, 'AsnDtl.asn_dtl_height', null),
            'asn_dtl_width'   => array_get($carton, 'AsnDtl.asn_dtl_width', null),

        ];
    }

}
