<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\StyleLocations;
use League\Fractal\TransformerAbstract;

class StyleLocationsTransformer extends TransformerAbstract
{
    public function transform(StyleLocations $styleLocations)
    {
        $values = $styleLocations->toArray();
        $piece_remain = $styleLocations->piece_remain;

        $cus_id = array_get($values, 'cus_id', NULL);
        $item_id = array_get($values, 'item_id', NULL);
        $loc_id = array_get($values, 'loc_id', NULL);
        $plt_id = array_get($values, 'plt_id', NULL);
        $ctn_pack_size = array_get($values, 'ctn_pack_size', NULL);

        $total_cartons = $styleLocations
            ->where('cus_id', $cus_id)
            ->where('item_id', $item_id)
            ->where('loc_id', $loc_id)
            ->where('plt_id', $plt_id)
            ->where('ctn_pack_size', $ctn_pack_size)
            ->count();

        return [
            'sku' => $styleLocations->sku,
            'plt_num' => object_get($styleLocations, 'pallet.plt_num', ''),
            'loc_name' => $styleLocations->loc_code,
            'ctn_sts' => $styleLocations->ctn_sts,
            'ctnr_num' => $styleLocations->ctnr_num,
            'ctn_pack_size' => $styleLocations->ctn_pack_size,
            'piece_remain' => $piece_remain,
            'total_cartons' => $total_cartons,
            'total_pieces' => $piece_remain * $total_cartons,
            'cus_name' => object_get($styleLocations, 'customer.cus_name', ''),
            'upc' => $styleLocations->upc,
            'size' => $styleLocations->size,
            'color' => $styleLocations->color,
            'po' => $styleLocations->po,
            'lot' => $styleLocations->lot,
            'length' => $styleLocations->length,
            'width' => $styleLocations->width,
            'height' => $styleLocations->height,
            'weight' => $styleLocations->weight,
            'created_at' => $styleLocations->created_at->format('m/d/Y'),
        ];
    }

    /*
    ****************************************************************************
    */

}
