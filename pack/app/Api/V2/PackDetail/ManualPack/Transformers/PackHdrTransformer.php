<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V2\PackDetail\ManualPack\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\Status;

/**
 * Class PackHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class PackHdrTransformer extends TransformerAbstract
{
    /**
     * @param PackHdr $packHdr
     *
     * @return array
     */
    public function transform(PackHdr $packHdr)
    {
        $createdAt = date("m/d/Y", strtotime($packHdr->created_at));

        return [
            'odr_id'        => $packHdr->odr_id,
            'so_id'         => $packHdr->so_id,

            // Warehouse
            'whs_id'        => $packHdr->whs_id,
            'whs_name'      => object_get($packHdr, 'warehouse.whs_name', null),
            'whs_code'      => object_get($packHdr, 'warehouse.whs_code', null),

            // Customer
            'cus_id'        => $packHdr->cus_id,
            'cus_name'      => object_get($packHdr, 'customer.cus_name', null),
            'cus_code'      => object_get($packHdr, 'customer.cus_code', null),

            // pack Status
            'odr_sts'       => $packHdr->odr_sts,
            'odr_sts_name'  => Status::getByKey("Pack-Status", $packHdr->odr_sts),

            // User
            'csr_id'        => $packHdr->csr,
            'csr_name'      => trim(object_get($packHdr, 'csrUser.first_name', null) . " " .
                object_get($packHdr, 'csrUser.last_name', null)),

            // Pack Type
            'odr_type'      => $packHdr->odr_type,
            'odr_type_name' => Status::getByKey("Pack-Type", $packHdr->odr_type),

            'odr_num'      => $packHdr->odr_num,
            'ship_to_name' => $packHdr->ship_to_name,
            'ship_by_dt'   => $this->dateFormat($packHdr->ship_by_dt),
            'cancel_by_dt' => $this->dateFormat($packHdr->cancel_by_dt),
            'back_odr'     => $packHdr->back_odr,
            'alloc_qty'    => $packHdr->alloc_qty,
            'created_at'   => $createdAt,
            'created_by'   => trim(object_get($packHdr, "createdBy.first_name", null) . " " .
                object_get($packHdr, "createdBy.last_name", null))
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }
}
