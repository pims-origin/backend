<?php
/**
 * Created by PhpStorm.
 * User: PhucTran
 * Date: 3/16/2017
 * Time: 10:14 AM
 */

namespace App\Api\V1\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $primaryKey = 'file_id';
    public $timestamps = false;

    protected $hidden = [
        'external',
        'doc_num'
    ];

    public function ofUser(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getUrl(){
        return url($this->file_path);
    }
}