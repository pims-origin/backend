<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class SystemUomModel extends AbstractModel
{
    /**
     * SystemUomModel constructor.
     *
     * @param SystemUom|null $model
     */
    public function __construct(SystemUom $model = null)
    {
        $this->model = ($model) ?: new SystemUom();
    }

    /**
     * Search SystemUom
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['sys_uom_code', 'sys_uom_name'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        // Get
        $models = $query->paginate($limit);

        return $models;
    }
}
