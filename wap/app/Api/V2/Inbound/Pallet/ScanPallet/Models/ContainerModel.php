<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/24/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Inbound\Pallet\ScanPallet\Models;

use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ContainerModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     * @param Container $model
     */
    public function __construct(Container $model = null)
    {
        $this->model = ($model) ?: new Container();
    }

    /**
     * @param int $containerId
     * @return int
     */
    public function deleteContainer($containerId)
    {
        return $this->model
            ->where('ctnr_id', $containerId)
            ->delete();
    }

    /**
     * Search Container
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'ctnr_num') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $query->whereHas('asnDtl.asnHdr', function ($query) use ($attributes) {
            if (isset($attributes['whs_id'])) {
                $query->where('whs_id', SelStr::escapeLike($attributes['whs_id']));
            }
        });

        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);
        return $models;
    }

    public function loadCTNRList($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->join('asn_dtl', 'asn_dtl.ctnr_id', '=', 'container.ctnr_id')
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
            ->whereIn('asn_hdr.asn_sts', ['NW', 'RG'])
            ->whereIn('asn_dtl.asn_dtl_sts', ['NW', 'RG']);

        if (isset($attributes['whs_id'])) {
            $query->where('asn_hdr.whs_id', SelStr::escapeLike($attributes['whs_id']));
        }

        if (isset($attributes['cus_id'])) {
            $query->where('asn_hdr.cus_id', SelStr::escapeLike($attributes['cus_id']));
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('container.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        $query->groupBy('container.ctnr_id');

        $this->sortBuilder($query, $attributes);

        if (!$limit) {
            $limit = 20;
        }

        $models = $query->paginate($limit);

        return $models;
    }

    public function getByNumber($number)
    {
        return $this->getFirstBy('ctnr_num', $number);
    }
}