<?php

namespace Seldat\Wms2\Models;

class CycleCountNotification extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cc_notification';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ccn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'ccn_id',
        'whs_id',
        'loc_id',
        'loc_code',
        'cc_ntf_sts',
        'cc_ntf_date',
        'reason',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted',
        'item_id',
        'sku',
        'lot',
        'pack',
        'size',
        'color',
        'cus_id',
        'uom_code',
        'uom_name',
        'remain_qty',
        'des',
        'cycle_dtl_id',
    ];

}
