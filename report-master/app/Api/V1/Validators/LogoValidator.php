<?php

namespace App\Api\V1\Validators;


class LogoValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'file' => 'required|max:2048',
        ];
    }
}
