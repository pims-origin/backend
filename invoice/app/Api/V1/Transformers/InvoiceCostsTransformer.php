<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class InvoiceCostsTransformer extends TransformerAbstract
{
    public function transform($invoiceCosts)
    {
        return [
            'cus_name' => object_get($invoiceCosts, 'customer.cus_name', ''),
            'chg_type_name' => object_get($invoiceCosts, 'chargeCode.chargeType.chg_type_name', ''),
            'chg_code' => object_get($invoiceCosts, 'chargeCode.chg_code', ''),
            'chg_code_name' => object_get($invoiceCosts, 'chargeCode.chg_code_name', ''),
            'chg_code_des' => object_get($invoiceCosts, 'chargeCode.chg_code_des', ''),
            'sys_uom_name' => object_get($invoiceCosts, 'chargeCode.systemUom.sys_uom_name', ''),
            'chg_cd_cur' => $invoiceCosts->chg_cd_cur,
            'chg_cd_price' => $invoiceCosts->chg_cd_price,
            'cus_id' => $invoiceCosts->cus_id,
            'chg_code_id' => $invoiceCosts->chg_code_id,
        ];
    }

    /*
    ****************************************************************************
    */

}
