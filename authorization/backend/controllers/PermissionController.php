<?php
namespace backend\controllers;

use backend\models\CreateRoleForm;
use backend\models\RoleSearch;
use backend\models\RoleSearchForm;
use common\models\Authority;
use Yii;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use backend\models\RoleUsers;

class PermissionController extends Controller
{

    public function actionList()
    {
        $form = new RoleSearchForm();
        $form->load(Yii::$app->request->get());
        $form->type = Authority::PERMISSION_TYPE;
        if (! $form->validate()) {
            return [
                'error' => $form->errors
            ];
        }
        $result = RoleSearch::getList($form);
        return $result;
    }

    public function actionCreate()
    {
        $model = new CreateRoleForm();
        $model->load(Yii::$app->request->post());

        $model->type = Authority::PERMISSION_TYPE;
        if ($model->createRole()) {
            Yii::$app->response->statusCode = 201;
            return ['data' =>
                [
                    'success' => true,
                ]
            ];
        } else {
            Yii::$app->response->statusCode = 400;
            return ['error' => $model->errors];
        }
    }

    public function actionByUser($id)
    {
        $auth = Yii::$app->authManager;
        $permission = $auth->getPermissionsByUser($id);

        if (! $permission) {
            throw new BadRequestHttpException('User Or Permission not found');
        }

        return ['data' => array_values($permission)];
    }

    public function actionGroups($id)
    {
        $auth = Yii::$app->authManager;
        if (!$auth->checkAccess($id, 'assignPermissionToRole')){
            throw new BadRequestHttpException('User have not permission');
        }

        $permission = $auth->getPermissions();
        $permission = array_keys($permission);

        $rs = $auth->getPermWithGrp($permission);

        return ['data' => ['group_permissions' => $rs]];
    }

    public function actionGetCsrUser()
    {
        $auth = Yii::$app->authManager;
        return ['data' => $auth->getCSRUser()];
    }
    public function actionGetPickerUser()
    {
        $auth = Yii::$app->authManager;
        return ['data' => $auth->getPickerUser()];
    }
    public function actionGetPutterUser()
    {
        $auth = Yii::$app->authManager;
        return ['data' => $auth->getPutterUser()];
    }
    public function actionGetPutBackerUser()
    {
        $auth = Yii::$app->authManager;
        return ['data' => $auth->getPutBackerUser()];
    }

    public function actionUsers($permission)
    {
        $params = Yii::$app->request->get();
        $condition = $permission == 'AccountManager' ? true : false;
        $user_ids =  Yii::$app->authManager->getAccountUser($condition, $params);
        $model = new RoleUsers();
        $requestHeader = Yii::$app->request->headers;

        if ($result = $model->getUsers($user_ids, $requestHeader)) {
            return [
                'data' => $result
            ];
        } else {
            throw new BadRequestHttpException('Users not found');
        }
    }
}