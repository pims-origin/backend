<?php

namespace App\Modules\Asns\Models;

use Illuminate\Support\Facades\DB;
use App\Modules\Asns\Eloquents\Item;
use Illuminate\Support\Facades\Config;
use App\Modules\Asns\Eloquents\AsnHdr;
use App\Modules\Asns\Eloquents\AsnDtl;
use App\Modules\Asns\Eloquents\Carton;
use App\Modules\Asns\Eloquents\Container;
use App\Modules\Asns\Eloquents\AsnHdrMeta;
use App\Modules\Asns\Eloquents\AsnDtlMeta;

class AsnModel extends AbstractModel
{
    protected $item;
    protected $asnHdr;
    protected $asnDtl;
    protected $container;

    const STATUS_NEW = 'NW';
    const STATUS_CANCEL = 'CC';
    const STATUS_RECEIVED = 'RE';
    const STATUS_RECEIVING = 'RG';
    const ASN_TYPE = 'ASN';
    const UOM_PIECE = 25;
    const ASN_QUALIFIER = 'ASN';
    
    public function __construct(Item $item, AsnHdr $asnHdr, AsnDtl $asnDtl, Container $container)
    {
        $this->item      = $item;
        $this->asnHdr    = $asnHdr;
        $this->asnDtl    = $asnDtl;
        $this->container = $container;
    }

    public function getListAsnByCus($whsId, $cusId, $limit, $baseUrl)
    {
        $asns = $this->asnHdr->where('deleted', 0)
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId)
                ->orderBy('created_at', 'DESC');

        if ($this->checkCusOfUser($whsId, $cusId))
        {
            return $asns->paginate($limit)->withPath($baseUrl);
        }
    }

    public function getAsnDetail($whsId, $cusId, $asnId)
    {
        $asn = $this->asnHdr->where('deleted', 0)
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->find($asnId);

        if ($this->checkCusOfUser($whsId, $cusId))
        {
            return $asn;
        }
    }

    public function createAsn($input)
    {
        $whsId = array_get($input, 'whs_id');
        $cusId = array_get($input, 'cus_id');

        if ($this->checkCusOfUser($whsId, $cusId))
        {
            if ( !empty($items = $this->getItemsToCheck($input)) )
            {
                $checkItm = $this->checkExistItem($items);
                if (!empty($checkItm))
                {
                    $msg = 'SKU ' . str_replace(['[', '"', ']'], '' ,json_encode($checkItm)) . ' does not existed!';
                    throw new \Exception(json_encode(['title' => $msg, 'detail' => 'Please chosse another SKU!']));
                }
            }

            $checkCtnrData = [
                'whs_id'    => $whsId,
                'cus_id'    => $cusId,
                'ctnr_num'  => array_get($input, 'ctnr_num'),
                'status' => [
                    'cc' => self::STATUS_CANCEL,
                    'nw' => self::STATUS_NEW,
                    'rg' => self::STATUS_RECEIVING,
                    're' => self::STATUS_RECEIVED
                ]
            ];

            if ($this->checkCtnrProcessing($checkCtnrData))
            {
                $msg = 'Container {' . array_get($input, 'ctnr_num') . '} is processing!';
                throw new \Exception(json_encode(['title' => $msg, 'detail' => 'Please chosse another Container!']));
            }

            $asnHdr = NULL;
            DB::transaction(function() use($input, &$asnHdr) {
                $asnHdr = $this->createAsnHdr($input);
                // $asnHdrMeta = $this->createAsnHdrMeta($asnHdr, $input);
                $asnDtl = $this->createAsnDtl($input, $asnHdr);
                // $asnDtlMeta = $this->createAsnDtlMeta($asnDtl, $input);
            });

            return $asnHdr;
        }
    }

    public function getItemsToCheck($input)
    {
        $itemArr = [];
        $items = array_get($input, 'items');

        foreach ($items as $item) {
            $itemArr[] = [
                'cus_id' => array_get($input, 'cus_id'),
                'sku'    => array_get($item, 'sku'),
            ];
        }

        return $itemArr;
    }

    public function createAsnHdr($input)
    {
        $asnHdrData = $this->getAsnHdrData($input);
        $asnHdr = $this->asnHdr->create($asnHdrData);

        return $asnHdr;
    }

    public function createAsnHdrMeta($asnHdr, $input)
    {
        $data = [
            'qualifier' => self::ASN_QUALIFIER,
            'asn_hdr_id' => $asnHdr->asn_hdr_id,
            'value'      => json_encode(array_get($input, 'meta'))
        ];
        $asnHdrMeta = AsnHdrMeta::create($data);

        return $asnHdrMeta;
    }

    public function createAsnDtl($input, $asnHdr)
    {
        $asnDtlData = $this->getAsnDtlData($input, $asnHdr);
        $asnDtl = $asnHdr->asnDtl()->createMany($asnDtlData);

        return $asnDtl;
    }

    public function createAsnDtlMeta($asnDtl, $input)
    {
        $asnDtlMeta = [];
        $items = array_get($input, 'items');

        foreach ($items as $item) {
            foreach ($asnDtl as $data) {
                if ($item['sku'] == $data->asn_dtl_sku && $item['po'] == $data->asn_dtl_po)
                {
                    $asnDtlId = $data->asn_dtl_id;
                    break;
                }
            }
            $arrAsnDtlMeta = [
                'qualifier' => self::ASN_QUALIFIER,
                'asn_dtl_id' => $asnDtlId,
                'value'      => json_encode(array_get($item, 'meta'))
            ];

            $asnDtlMeta[] = AsnDtlMeta::create($arrAsnDtlMeta);
        }

        return $asnDtlMeta;
    }

    public function updateAsn($input)
    {
        $whsId = array_get($input, 'whs_id');
        $cusId = array_get($input, 'cus_id');

        if ($this->checkCusOfUser($whsId, $cusId))
        {
            if ( !empty($items = $this->getItemsToCheck($input)) )
            {
                $checkItm = $this->checkExistItem($items);
                if (!empty($checkItm))
                {
                    $msg = 'SKU ' . str_replace(['[', '"', ']'], '' ,json_encode($checkItm)) . ' does not existed!';
                    throw new \Exception(json_encode(['title' => $msg, 'detail' => 'Please chosse another SKU!']));
                }
            }

            $checkCtnrData = [
                'whs_id'    => $whsId,
                'cus_id'    => $cusId,
                'ctnr_num'  => array_get($input, 'ctnr_num'),
                'status' => [
                    'cc' => self::STATUS_CANCEL,
                    'nw' => self::STATUS_NEW,
                    'rg' => self::STATUS_RECEIVING,
                    're' => self::STATUS_RECEIVED
                ]
            ];

            if ($this->checkCtnrProcessing($checkCtnrData))
            {
                $msg = 'Container {' . array_get($input, 'ctnr_num') . '} is processing!';
                throw new \Exception(json_encode(['title' => $msg, 'detail' => 'Please chosse another Container!']));
            }

            $asnHdr = NULL;
            DB::transaction(function() use($input, &$asnHdr) {
                $asnHdr = $this->updateAsnHdr($input);
                $asnHdrMeta = $this->updateAsnHdrMeta($input);
                $asnDtl = $this->updateAsnDtl($input, $asnHdr);
                $asnDtlMeta = $this->updateAsnDtlMeta($asnDtl, $input);
            });

            return $asnHdr;
        }
    }

    public function updateAsnHdr($input)
    {
        $asnHdr = NULL;
        $asnHdrData = $this->getAsnHdrData($input);
        $asnHdrId = array_get($input, 'asn_hdr_id');

        $asnHdr = $this->asnHdr->where('asn_hdr_id', $asnHdrId)
            ->where('whs_id', $input['whs_id'])
            ->where('cus_id', $input['cus_id'])
            ->first();
        if (!$asnHdr)
        {
            $msg = 'ASN not existed!';
            throw new \Exception(json_encode(['title' => $msg, 'detail' => 'Please try!']));
        }
        else if (!in_array($asnHdr->asn_sts, [self::STATUS_RECEIVING, self::STATUS_NEW]))
        {
            $msg = 'Only New Or Receiving ASNs can be updated!';
            throw new \Exception(json_encode(['title' => $msg, 'detail' => 'Please try!']));
        }

        $asnHdr->update($asnHdrData);
        $asnHdr = $asnHdr->fill($asnHdrData);

        return $asnHdr;
    }

    public function updateAsnHdrMeta($input)
    {
        $asnHdrMeta = [];
        $asnHdrId = array_get($input, 'asn_hdr_id');
        $value = [
            'value' => json_encode(array_get($input, 'meta'))
        ];

        $asnHdrMeta = AsnHdrMeta::find($asnHdrId);

        if ($asnHdrMeta)
        {
            $asnHdrMeta->update($value);
            $asnHdrMeta = $asnHdrMeta->fill($value);
        }
        else
        {
            $data = [
                'qualifier' => self::ASN_QUALIFIER,
                'asn_hdr_id' => $asnHdrId,
                'value'      => $value['value']
            ];
            $asnHdrMeta = AsnHdrMeta::create($data);
        }

        return $asnHdrMeta;
    }

    public function updateAsnDtl($input, $asnHdr)
    {
        $asnDtlArr = $asnDtlRemoveArr = [];
        $ctnrNum = array_get($input, 'ctnr_num');
        $asnDtlData = $this->getAsnDtlData($input, $asnHdr);

        if ($asnHdr->asnDtl()->where(['ctnr_num' => $ctnrNum, 'asn_dtl_ept_dt' => $input['ctnr_ept_dt']])->first())
        {
            $msg = 'Expected date is unique for each container';
            throw new \Exception(json_encode(['title' => $msg, 'detail' => 'Please try!']));
        }

        // Remove asnDtl if sku not have input params
        $asnDtlRemoveArr = $this->removeAsnDtlWhenUpdate($asnHdr, $ctnrNum, $asnDtlData);

        // Update asnDtl
        foreach ($asnDtlData as $data) {
            $attributes = [
                'ctnr_num' => $ctnrNum,
                'asn_dtl_sku' => array_get($data, 'asn_dtl_sku'),
                'asn_dtl_po' => array_get($data, 'asn_dtl_po'),
            ];
            $oldAsnDtl = $this->getOldAsnDtl($attributes, $asnHdr);

            if ($oldAsnDtl)
            {
                $oldAsnDtl->update($data);
                $asnDtlArr[] = $oldAsnDtl->fill($data);
            } else
            {
                $asnDtlArr[] = $this->asnDtl->create($data);
            }
        }

        return $asnDtlArr;
    }

    public function updateAsnDtlMeta($asnDtl, $input)
    {
        $asnDtlMetaArr = [];
        $items = array_get($input, 'items');

        foreach ($asnDtl as $data) {
            foreach ($items as $item) {
                if ($data->asn_dtl_sku == $item['sku'] && $data->asn_dtl_po == $item['po'])
                {
                    $asnDtlId = $data->asn_dtl_id;
                    $value = [
                        'value' => json_encode($item['meta'])
                    ];
                    break;
                }
            }

            if ($asnDtlMeta = AsnDtlMeta::find($asnDtlId))
            {
                $asnDtlMeta->update($value);
                $asnDtlMetaArr[] = $asnDtlMeta->fill($value);
            }
            else
            {
                $arrAsnDtlMeta = [
                    'qualifier' => self::ASN_QUALIFIER,
                    'asn_dtl_id' => $asnDtlId,
                    'value'      => $value['value']
                ];
                $asnDtlMetaArr[] = AsnDtlMeta::create($arrAsnDtlMeta);
            }
        }

        return $asnDtlMetaArr;
    }

    public function getOldAsnDtl($attributes, $asnHdr)
    {
        $asnDtl = $asnHdr->asnDtl()->where($attributes)->first();

        return $asnDtl;
    }

    public function removeAsnDtlWhenUpdate($asnHdr, $ctnrNum, $asnDtlData)
    {
        $asnDtlByCtnr = $asnHdr->asnDtl()->where('ctnr_num', $ctnrNum)->get()->toArray();
        $oldSku = array_column($asnDtlByCtnr, 'asn_dtl_sku');
        $newSku = array_column($asnDtlData, 'asn_dtl_sku');
        $arrSkuDelete = array_diff($oldSku, $newSku);

        if (count($arrSkuDelete) > 0)
        {
            Carton::whereIn('sku', $arrSkuDelete)->delete();
            $asnHdr->asnDtl()->whereIn('asn_dtl_sku', $arrSkuDelete)->delete();
        }

        return $arrSkuDelete;
    }

    public function getAsnHdrData($input)
    {
        $generateAsnNumAndSeq = $this->generateAsnNumAndSeq();
        $items = array_get($input, 'items');
        // $asnCtn = array_sum(array_column($items, 'ept_qty', 0));
        $asnCtn = array_sum(array_column($items, 'qty', 0));

        $asnHdrData = [
            'cus_id'           => array_get($input, 'cus_id'),
            'whs_id'           => array_get($input, 'whs_id'),
            'asn_hdr_seq'      => array_get($generateAsnNumAndSeq, 'asn_seq', 0),
            'asn_hdr_ept_dt'   => strtotime(array_get($input, 'ctnr_ept_dt', date('Y-m-d'))),
            'asn_hdr_num'      => array_get($generateAsnNumAndSeq, 'asn_num', 'NA'),
            'asn_hdr_ref'      => array_get($input, 'ref_code', 'NA'),
            'sys_mea_code'     => array_get($input, 'measurement_code', 'IN'),
            'asn_type'         => self::ASN_TYPE,
            'asn_sts'          => self::STATUS_NEW,
            'asn_hdr_des'      => array_get($input, 'hdr_des', NULL),
            'asn_hdr_ctn_ttl'  => $asnCtn,
            'asn_hdr_itm_ttl'  => count($items),
            'asn_hdr_ctnr_ttl' => count(array_get($input, 'ctnr_num', 1)),
        ];

        return $asnHdrData;
    }

    public function getAsnDtlData($input, $asnHdr)
    {
        $asnDtlData = $arrCheckDuplicateItem = [];
        $cusId = array_get($input, 'cus_id');
        $items = array_get($input, 'items', NULL);

        $ctnrNum = array_get($input, 'ctnr_num');
        $ctnr = $this->createNewContainer(['ctnr_num' => $ctnrNum, 'ctnr_note' => NULL]);

        foreach ($items as $item)
        {
            $itemInfo = $this->getItemInfo(['cus_id' => $cusId, 'sku' => array_get($item, 'sku')]);
            $arrCheckDuplicateItem[] = [
                'item_id'   => object_get($itemInfo, 'item_id'),
                'sku'       => object_get($itemInfo, 'sku'),
                'size'      => object_get($itemInfo, 'size'),
                'color'     => object_get($itemInfo, 'color'),
                'pack'      => object_get($itemInfo, 'pack')
            ];

            $length = floatval(array_get($item, 'length'));
            $width = floatval(array_get($item, 'width'));
            $height = floatval(array_get($item, 'height'));
            $weight = floatval(array_get($item, 'weight'));
            $volumeAndCube = $this->volumeAndCube($length, $width, $height);

            $ucc128 = $this->generateUpc128(object_get($itemInfo, 'item_id'), $cusId);

            $poDate = array_get($item, 'po_dt') ? strtotime(array_get($item, 'po_dt')) : 0;
            $eptDtlDate = array_get($input, 'ctnr_ept_dt') ? strtotime(array_get($input, 'ctnr_ept_dt')) : 0;
            $eptDate = array_get($item, 'ept_dtl_dt') ? strtotime(array_get($item, 'ept_dtl_dt')) : 0;

            $dtlPack = object_get($itemInfo, 'pack');
            $dtlPack = $dtlPack ? $dtlPack : 1;
            $dtlCsrDoc = array_get($item, 'x_dock');
            $dtlCsrDoc = $dtlCsrDoc ? $dtlCsrDoc : 0;
            // $dtlCtnTtl = array_get($item, 'ept_qty');
            $dtlCtnTtl = array_get($item, 'qty');
            $dtlCtnTtl = $dtlCtnTtl ? $dtlCtnTtl : 0;

            $asnDtlQtyTtl = $dtlCtnTtl * $dtlPack;
            $asnDtlCrsDocQty = $dtlCsrDoc * $dtlPack;

            $asnDtlData[] = [
                'asn_hdr_id'            => $asnHdr->asn_hdr_id,
                'item_id'               => object_get($itemInfo, 'item_id'),
                'ctnr_id'               => object_get($ctnr, 'ctnr_id'),
                'ctnr_num'              => object_get($ctnr, 'ctnr_num'),
                'asn_dtl_po'            => array_get($item, 'po', 'NA'),
                'asn_dtl_po_dt'         => $poDate,
                'asn_dtl_ctn_ttl'       => $dtlCtnTtl,
                'asn_dtl_crs_doc'       => $dtlCsrDoc,
                'asn_dtl_des'           => array_get($item, 'dtl_des', NULL),
                'asn_dtl_sts'           => self::STATUS_NEW,
                'asn_dtl_lot'           => array_get($item, 'lot', 'NA'),
                'asn_dtl_length'        => $length,
                'asn_dtl_width'         => $width,
                'asn_dtl_height'        => $height,
                'asn_dtl_weight'        => $weight,
                'asn_dtl_volume'        => array_get($volumeAndCube, 'volume'),
                'asn_dtl_cube'          => array_get($volumeAndCube, 'cube'),
                'asn_dtl_pack'          => $dtlPack,
                'asn_dtl_cus_upc'       => object_get($itemInfo, 'cus_upc'),
                'asn_dtl_sku'           => array_get($item, 'sku'),
                'asn_dtl_size'          => object_get($itemInfo, 'size'),
                'asn_dtl_color'         => object_get($itemInfo, 'color'),
                'expired_dt'            => $eptDate,
                'uom_id'                => object_get($itemInfo, 'uom_id'),
                'uom_code'              => object_get($itemInfo, 'uom_code'),
                'uom_name'              => object_get($itemInfo, 'uom_name'),
                'asn_dtl_qty_ttl'       => $asnDtlQtyTtl,
                'asn_dtl_crs_doc_qty'   => $asnDtlCrsDocQty,
                'ucc128'                => $ucc128,
                'spc_hdl_code'          => object_get($itemInfo, 'spc_hdl_code'),
                'spc_hdl_name'          => object_get($itemInfo, 'spc_hdl_name'),
                'cat_code'              => object_get($itemInfo, 'cat_code'),
                'cat_name'              => object_get($itemInfo, 'cat_name'),
                'asn_dtl_ept_dt'        => $eptDtlDate
            ];
        }

        // Validate duplicate item
        $this->exportErrorDuplicateItem($arrCheckDuplicateItem);

        return $asnDtlData;
    }

    public function exportErrorDuplicateItem($arrCheckDuplicateItem)
    {
        if ($valiResult = $this->validateDuplicateItem($arrCheckDuplicateItem))
        {
            foreach ($valiResult as $val) {
                $msg = "SKU {$val['sku']} IS DUPLICATED.";
                throw new \Exception($msg);
            }
        }
    }
}