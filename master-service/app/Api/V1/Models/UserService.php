<?php
namespace App\Api\V1\Models;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use GuzzleHttp\Exception\RequestException;

class UserService extends BaseService
{
    private $userId;

    public function __construct(IRequest $request, $userId = null, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_AUTHENTICATION');
        parent::__construct($request, $api);
        $this->userId = $userId;
    }

    public function getUserListByQueryString($queryStr)
    {
        $uri = env('USER_URI');
        $uri = sprintf('%s?%s', $uri, $queryStr);

        try {
            $response = $this->client->get($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function getUser($userId)
    {
        $uri = $this->getUserUri($userId);

        try {
            $response = $this->client->get($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function getUserInfo($userId)
    {
        $uri = "users/info/$userId";

        try {
            $response = $this->client->get($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function getUserData($userId)
    {
        $response = $this->getUser($userId);
        $content = $response->getBody()->getContents();

        return $this->responseToArray($content);
    }

    public function getUserUri($userId)
    {
        $uri = env('USER_URI') . '/' . $userId;

        return $uri;
    }

    public function createUser($params)
    {
        $uri = env('USER_URI');
        $options = [
            'form_params' => $params
        ];
        try {
            $response = $this->client->post($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function updateUser($userId, $params)
    {
        $uri = $this->getUserUri($userId);
        $options = [
            'form_params' => $params
        ];
        try {
            $response = $this->client->put($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function deleteUser($userId)
    {
        $uri = $this->getUserUri($userId);

        try {
            $response = $this->client->delete($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function deleteMultipleUser($body)
    {
        $uri = env('USER_URI') . '/multiple';
        $options = [
            'form_params' => $body
        ];
        try {
            $response = $this->client->delete($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    protected function _savePassword($userId, $body, $uri)
    {
        $options = [
            'form_params' => [
                'new_password'        => $body['password'],
                'repeat_new_password' => $body['password_confirmation']
            ]
        ];

        if (isset($body['token'])) {
            $options['form_params']['token'] = $body['token'];
        }

        if (isset($body['old_password'])) {
            $options['form_params']['old_password'] = $body['old_password'];
        }

        try {
            $response = $this->client->post($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function changePassword($userId, $body)
    {
        $uri = 'users/change-password';

        return $this->_savePassword($userId, $body, $uri);
    }

    public function updatePassword($userId, $body)
    {
        $uri = 'users/update-password';

        return $this->_savePassword($userId, $body, $uri);
    }

    public function resetPassword($userId, $body)
    {
        $uri = 'users/update-password';

        $options = [
            'form_params' => [
                'new_password'        => $body['password'],
                'repeat_new_password' => $body['password_confirmation'],
                'user_id'             => $userId
            ]
        ];

        try {
            $response = $this->client->post($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }
    
    public function getProfile($userId)
    {
        $uri = 'users/' . $userId;

        try {
            $response = $this->client->get($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function updateProfile($userId, $body)
    {
        $uri = 'users/update-profile/' . $userId;

        $options = [
            'form_params' => $body
        ];
        try {
            $response = $this->client->put($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function userExport($type)
    {
        try {
            $response = $this->client->get("users/export/".$type);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }
}
