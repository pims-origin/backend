<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    public $header;
    public $attach;
    public $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($header, $attach = null, $content = null)
    {
        $this->header = $header;
        $this->attach = $attach;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->attach) {
            return $this->view('emails.invoice')
                ->attach($this->attach, [
                    'as' => 'invoice.pdf',
                    'mime' => 'application/pdf',
                ]);;
        }

        return $this->view('emails.invoice');
    }
}
