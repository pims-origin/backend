<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\CustomerInUserModel;
use App\Api\V1\Validators\CustomerMetaValidator;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use Wms2\UserInfo\Data;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;

class CustomerInUserController extends AbstractController
{
    /*
     * @var CustomerModel
     * */
    protected $customerModel;

    /**
     * @var $customerMetaModel
     */
    protected $customerMetaModel;

    /**
     * @var $customerInUserModel
     */
    protected $customerInUserModel;

    /**
     * @var $customerMetaValidator
     */
    protected $customerMetaValidator;

    /**
     * @param OrderFlowMasterModel $orderFlowMasterModel
     * @param CustomerMetaValidator $customerMetaValidator
     */
    public function __construct(
        CustomerMetaModel $customerMetaModel,
        CustomerInUserModel $customerInUserModel,
        CustomerMetaValidator $customerMetaValidator
    ) {
        parent::__construct();
        $this->customerMetaModel = $customerMetaModel;
        $this->customerInUserModel = $customerInUserModel;
        $this->customerMetaValidator = $customerMetaValidator;
    }

    /**
     * @param $customerId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getUser($customerId)
    {
        try {
            $customerInUserModel = $this->customerInUserModel->getUserOfCustomer($customerId);

            if (empty($customerInUserModel)) {
                return $this->response->errorBadRequest(Message::get("BM017", "User Of Customer"));
            }

            $result = ['data' => $customerInUserModel];

            return $result;

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    protected function upsert($request, $cusId)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->customerMetaValidator->validate($input);

        $valueArrJson = [];
        foreach ($input['value'] as $getVal) {
            $value = [
                'user_id'     => array_get($getVal, 'user_id', ''),
                'whs_id'      => array_get($getVal, 'whs_id', ''),
                'set_default' => array_get($getVal, 'set_default', 0)
            ];
            $valueJson = json_encode($value);
            array_push($valueArrJson, $valueJson);
        }

        $valueJsons = json_encode($valueArrJson);
        $params = [
            'cus_id'    => $cusId,
            'qualifier' => array_get($input, 'qualifier', ''),
            'value'     => $valueJsons
        ];

        // check exist of cus_id
        $CusMeta = $this->customerMetaModel->getFirstWhere([
            'cus_id'    => $cusId,
            'qualifier' => array_get($input, 'qualifier')
        ]);

        try {
            if ($CusMeta) {
                //update
                $CustomerMeta = $this->customerMetaModel->updateCustomerMeta($params);
            } else {
                //create
                $CustomerMeta = $this->customerMetaModel->create($params);
            }

            return ["data" => "successful"];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function store(Request $request, $cusId)
    {
        return $this->upsert($request, $cusId);
    }

    /**
     * @param Request $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(Request $request, $cusId)
    {
        return $this->upsert($request, $cusId);
    }


}
