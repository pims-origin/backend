<?php
define('RUNNING_TESTS', true);
ini_set('xdebug.max_nesting_level', 500);

require __DIR__ . '/../../' . 'vendor/autoload.php';
$app = require __DIR__ . '/../../' . 'bootstrap/app.php';
//$app->loadEnvironmentFrom('.env.testing');
$app->instance('request', new \Zend\Diactoros\ServerRequest);

