<?php

namespace App\Modules\GoodsReceipts\Eloquents;

use Illuminate\Database\Eloquent\Model;

class GoodsReceiptStatus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gr_status';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;

    public $incrementing = false;

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'gr_sts_code',
        'gr_sts_name',
        'gr_sts_des',
    ];

    public function goodsReceipt()
    {
        return $this->hasOne(__NAMESPACE__ . '\GoodsReceipt', 'gr_sts', 'whs_sts_code');
    }
}
