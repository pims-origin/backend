<?php

namespace App\Http\Middleware;

use Closure;
use Namshi\JOSE\JWS;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Utils;
use Seldat\Wms2\Utils\JWTUtil;

class VerifySecret
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws JWTException
     * @throws TokenInvalidException
     */
    public function handle($request, Closure $next)
    {
        $token = JWTAuth::getToken();

        if (! $token) {
            throw new JWTException('A token is required', 400);
        }

        try {
            $jws = JWS::load($token);
        } catch (Exception $e) {
            throw new TokenInvalidException('Could not decode token: '.$e->getMessage());
        }

        if (! $jws->verify(config('jwt.secret'), config('jwt.algo'))) {
            throw new TokenInvalidException('Token Signature could not be verified.');
        }

        if (Utils::timestamp(\Seldat\Wms2\Utils\JWTUtil::getPayloadValue('exp'))->isPast()) {
            throw new TokenExpiredException('Token has expired');
        }

        return $next($request);
    }
}
