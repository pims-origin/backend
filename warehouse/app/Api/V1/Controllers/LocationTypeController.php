<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\LocationTypeModel;
use App\Api\V1\Transformers\LocationTypeTransformer;
use App\Api\V1\Validators\DeleteMassLocationTypeValidator;
use App\Api\V1\Validators\LocationTypeValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class LocationTypeController extends AbstractController
{
    /**
     * @var LocationTypeModel
     */
    protected $locationTypeModel;

    /**
     * @var $locationTypeTransformer
     */
    protected $locationTypeTransformer;

    /**
     * @var $locationTypeValidator
     */
    protected $locationTypeValidator;

    /**
     * @var $delMassLocTypeValidator
     */
    protected $delMassLocTypeValidator;

    /**
     * LocationTypeController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->locationTypeModel = new LocationTypeModel();
        $this->locationTypeTransformer = new LocationTypeTransformer();
        $this->locationTypeValidator = new LocationTypeValidator();
        $this->delMassLocTypeValidator = new DeleteMassLocationTypeValidator();
    }

    /**
     * API Read Location Type
     *
     * @param int $locationTypeId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($locationTypeId)
    {
        try {
            $locationType = $this->locationTypeModel->getFirstBy('loc_type_id', $locationTypeId);

            return $this->response->item($locationType, $this->locationTypeTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Search Location Type
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $locationType = $this->locationTypeModel->search($input, [], array_get($input, 'limit'));

            return $this->response->paginator($locationType, $this->locationTypeTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Create Location Type
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->locationTypeValidator->validate($input);

        $params = [
            'loc_type_name' => $input['loc_type_name'],
            'loc_type_code' => $input['loc_type_code'],
            'loc_type_desc' => array_get($input, 'loc_type_desc', ''),
        ];

        try {
            // check duplicate code
            if ($this->locationTypeModel->getFirstBy('loc_type_code', $params['loc_type_code'])) {
                throw new \Exception(Message::get("BM006", "Location Type Code"));
            }

            // check duplicate code
            if ($this->locationTypeModel->getFirstBy('loc_type_name', $params['loc_type_name'])) {
                throw new \Exception(Message::get("BM006", "Location Type Name"));
            }

            if ($locationType = $this->locationTypeModel->create($params)) {
                return $this->response->item($locationType,
                    $this->locationTypeTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * API Update Location Type
     *
     * @param int $locationTypeId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(
        $locationTypeId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->locationTypeValidator->validate($input);

        $params = [
            'loc_type_id'   => $locationTypeId,
            'loc_type_name' => $input['loc_type_name'],
            'loc_type_code' => $input['loc_type_code'],
            'loc_type_desc' => array_get($input, 'loc_type_desc', ''),
        ];

        try {
            // check duplicate code
            if (($locType = $this->locationTypeModel->getFirstBy('loc_type_code', $params['loc_type_code']))
                && $locType->loc_type_id != $locationTypeId
            ) {

                throw new \Exception(Message::get("BM006", "Location Type Code"));
            }

            // check duplicate code
            if (($locType = $this->locationTypeModel->getFirstBy('loc_type_name', $params['loc_type_name']))
                && $locType->loc_type_id != $locationTypeId
            ) {

                throw new \Exception(Message::get("BM006", "Location Type Name"));
            }

            if ($locationType = $this->locationTypeModel->update($params)) {
                return $this->response->item($locationType, $this->locationTypeTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * API Delete Location Type
     *
     * @param int $locationTypeId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function destroy($locationTypeId)
    {
        try {
            // Check Exist Zone in Location
            $locationModel = new LocationModel();
            $locations = $locationModel->loadByMany(['loc_type_id' => $locationTypeId])->first();
            if (!empty($locations)) {
                return $this->response
                    ->errorBadRequest(Message::get("BM003", "Location Type"));
            }

            if ($this->locationTypeModel->deleteLocationType($locationTypeId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->delMassLocTypeValidator->validate($input);

        $input['loc_type_id'] = is_array($input['loc_type_id']) ? $input['loc_type_id'] : [$input['loc_type_id']];

        try {
            // Check Exist Zone in Location
            $locationModel = new LocationModel();
            $locations = $locationModel->loadByMany(['loc_type_id' => $input['loc_type_id']])->first();
            if (!empty($locations)) {
                return $this->response
                    ->errorBadRequest(Message::get("BM003", "Location Type"));
            }
            $this->locationTypeModel->deleteMassLocationType($input['loc_type_id']);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }
}
