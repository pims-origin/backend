<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\SampleTransformer;
use Dingo\Api\Http\Request;

class SampleController extends AbstractController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $this->checkPermission($request, 'CreatePost');

        //do some thing
        return $this->response->array(['message' => 'Pass Authorization']);
    }
}
