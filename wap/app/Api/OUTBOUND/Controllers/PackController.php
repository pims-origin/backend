<?php

namespace App\Api\OUTBOUND\Controllers;

use App\Api\OUTBOUND\Models\CartonModel;
use App\Api\OUTBOUND\Models\OrderDtlModel;
use App\Api\OUTBOUND\Models\OrderHdrModel;
use App\Api\OUTBOUND\Models\OutPalletModel;
use App\Api\OUTBOUND\Models\PackHdrModel;
use App\Api\OUTBOUND\Models\PalletModel;
use App\Api\OUTBOUND\Transformers\PackHdrListOfPalletTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Maatwebsite\Excel\Excel;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use App\Api\V1\Models\Log;
use App\MyHelper;

class PackController extends AbstractController
{
    /**
     * @var CartonModel
     */
    protected $packHdrModel;
    protected $outPltModel;
    protected $orderHdrModel;
    protected $orderDtlModel;

    protected $cartonModel;


    public function __construct
    (
        CartonModel $cartonModel
    )
    {
        $this->packHdrModel = new PackHdrModel();
        $this->outPltModel = new OutPalletModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->orderDtlModel = new OrderDtlModel();

        $this->cartonModel   = $cartonModel;

    }

    /**
     * @param $whsId
     * @param $palletNum
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function getCartonsOfPallet($whsId, $palletNum, Request $request)
    {

        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "outbound/whs/{$whsId}/pallet/{$palletNum}/cartons";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'COP',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get cartons of pallet'
        ]);
        /*
         * end logs
         */

        // check pallet num
        $outPallet = $this->outPltModel->getFirstWhere([
            'whs_id'  => $whsId,
            'plt_num' => $palletNum,
        ]);

        if ($outPallet && $outPallet->out_plt_sts != 'AC') {
            $msg = sprintf("The pallet num %s is %s and not active", $palletNum, $outPallet->out_plt_sts);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    =>  []
            ];
        }

        $pltNumArr = explode('-', $palletNum);
        if (count($pltNumArr) < 4) {
            $msg = sprintf("The pallet num %s is invalid", $palletNum);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    =>  []
            ];
        }

        // Check exists Order hdl num
        $odrNum = "ORD-".$pltNumArr[1]."-".$pltNumArr[2];
        $order = $this->orderHdrModel->getFirstWhere(['odr_num' => $odrNum]);
        if(!$order){
            $msg = sprintf("No order has been found for the pallet num %s", $palletNum);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    =>  []
            ];
        }

        $odrId  = $order->odr_id;
        $cusId  = $order->cus_id;

        $packTtl = $this->packHdrModel->packTotal($whsId, $odrId);
        if($packTtl == 0){
            $msg = sprintf("No cartons have been packed on the order %s yet.", object_get($order, 'odr_num'));

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    =>  []
            ];
        }

       // $result = $this->packHdrModel->getCartonsByOrderId($odrId, $whsId);

        try {
            // start transaction
            DB::beginTransaction();

            if (!$outPallet) {
                $outPallet = $this->outPltModel->createOutPallet($whsId, $cusId, $palletNum);
                $scanByPallet    = $this->packHdrModel->scannedByPalletTotal($whsId, $outPallet->plt_id);
            } else {
                $scanByPallet = $this->packHdrModel->scannedByPalletTotal($whsId, $outPallet->plt_id);
            }
            $scnTtl =$this->packHdrModel->scannedByOrderTotal($whsId, $odrId);
            $skus = $this->orderDtlModel->getListOrderDtlInfo([$odrId], ['item_id', 'sku', 'size', 'color', 'lot', 'pack']);
            $skus = ($skus) ? $skus->toArray() : [];


            foreach ($scanByPallet as $key => $packHdr) {
                $ctnId = array_get($packHdr, 'cnt_id', null);
                if ($ctnId) {
                    $cartonObj = $this->cartonModel->getFirstWhere(['ctn_id' => $ctnId]);
                    $scanByPallet[$key]['ctn_rfid'] = object_get($cartonObj, 'rfid', null);
                } else {
                    $scanByPallet[$key]['ctn_rfid'] = null;
                }

                $isAssignRfid = array_get($packHdr, 'is_assigned_rfid', null);
                if ($isAssignRfid == 1) {
                    $scanByPallet[$key]['ctn_scanned'] = $scanByPallet[$key]['ctn_rfid'];
                } else {
                    $scanByPallet[$key]['ctn_scanned'] = array_get($packHdr, 'pack_hdr_num', null);
                }
            }

            DB::commit();

            return [
                "iat"     => time(),
                "status"  => true,
                "message" => [
                    "msg"         => "Successfully",
                    "status_code" => 1,
                ],
                "oderNum" => $odrNum,
                "totalCartons"    => $packTtl,
                "scn_ttl"     => $scnTtl,
                "data"        => $scanByPallet,
                'skus' => $skus
            ];

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => "Not successfully",
                    "status_code" => -1,
                ],
                "data"    =>  []
            ];
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

}
