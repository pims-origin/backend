<?php

namespace App\Modules\Orders\Validators;

use Validator;

class OrderValidator
{
    public function rules()
    {
        
    }

    public function validate($input)
    {
        $validator = Validator::make($input, $this->rules(), $this->messages());

        return $validator;
    }

    public function messages() {
        
    }

    public function checkOrder($order) {

        if(empty($order)) {
           throw new \Exception(json_encode(['title' =>'Order is not existed.', 'detail' => 'Please try']));
        }
    }

    public function checkCancelOrder($order) {

        if(empty($order)) {
           throw new \Exception(json_encode(['title' =>'Cancel order unsuccesfully.', 'detail' => 'Please try']));
        }
    }
}