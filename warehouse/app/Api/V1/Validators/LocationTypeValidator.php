<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Validators;


class LocationTypeValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_type_name' => 'required',
            'loc_type_code' => 'required',
        ];
    }
}
