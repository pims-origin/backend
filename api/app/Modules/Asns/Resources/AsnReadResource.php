<?php

namespace App\Modules\Asns\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AsnReadResource extends JsonResource
{
    public function toArray($request)
    {

        // $items = ItemResource::collection($this->asnDtl()->where('ctnr_num', $request->ctnr_num)->get());

        $ctnrNums = [];
        foreach ($this->asnDtl as $data) {
            $ctnrNums[] = $data->ctnr_num;
        }

        return [
            'type' => 'asns',
            'id' => (int) $this->asn_hdr_id,
            'attributes' => [
                'asn_id'        => $this->asn_hdr_id,
                'asn_num'       => $this->asn_hdr_num,
                'ref'           => $this->asn_hdr_ref,
                'expected_date' => $this->asn_hdr_ept_dt ? date('Y-m-d', $this->asn_hdr_ept_dt) : NULL,

                /*'ctnr_num'      => $request->ctnr_num,
                'items'         => $items,*/

                'ctnr_num'      => array_unique($ctnrNums),
                'items'         => ItemResource::collection($this->asnDtl),

                'note'          => $this->asn_hdr_des,
                'status'        => $this->asnHdrStatus->asn_sts_name,
                'created_at'    => $this->created_at ? date('Y-m-d', strtotime($this->created_at)) : NULL,
                'updated_at'    => $this->updated_at ? date('Y-m-d', strtotime($this->updated_at)) : NULL,
                'meta'          => $this->asnHdrMeta ? json_decode($this->asnHdrMeta->value) : NULL
            ]
        ];
    }
}