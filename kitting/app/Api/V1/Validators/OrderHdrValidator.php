<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

/**
 * Class OrderHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class OrderHdrValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'whs_id'          => 'required|integer|exists:warehouse,whs_id',
            'cus_id'          => 'required|integer|exists:customer,cus_id',

            'items'              => 'required|array',
            'items.*.odr_dtl_id' => 'integer|exists:odr_dtl,odr_dtl_id',
            'items.*.itm_id'     => 'required|integer|exists:item,item_id',
            'items.*.sku'        => 'required',
            'items.*.uom_id'     => 'integer|exists:system_uom,sys_uom_id',
            'items.*.qty'        => 'integer',
        ];
    }


}
