<?php

namespace App\Api\V2\PackDetail\ManualPack\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\Status;

/**
 * Class PackViewDtlTransformer
 *
 * @package App\Api\V2\PackDetail\ManualPack\Transformers
 */
class PackViewDtlTransformer extends TransformerAbstract
{
    /**
     * @param PackHdr $packHdr
     *
     * @return array
     */
    public function transform(PackHdr $packHdr)
    {
        $pack_type = $packHdr->pack_type;
        $length = (float)object_get($packHdr, 'length', null);
        $width = (float)object_get($packHdr, 'width', null);
        $height = (float)object_get($packHdr, 'height', null);

        $totalWeight  = array_sum(array_column($packHdr->items,'weight'));

        return [
            'pack_type'       => $pack_type,
            'odr_hdr_id'      => $packHdr->odr_hdr_id,
            'pack_ref_id'     => $packHdr->pack_ref_id,
            'pack_ref'        => $packHdr->pack_ref_id,
            'length'          => $length,
            'width'           => $width,
            'height'          => $height,
            'dimension'       => sprintf("%sx%sx%s", $length, $width, $height),
            'sku_ttl'         => $packHdr->sku_ttl,
            'piece_ttl'       => $packHdr->piece_ttl,
            'pack_hdr_num'    => $packHdr->pack_hdr_num,
            'order_type_key'  => $packHdr->order_type_key,
            'order_type'      => Status::getByKey("Order-Type", $packHdr->order_type_key),
            'origin_odr_type' => $packHdr->origin_odr_type,
            'odr_num'         => $packHdr->odr_hdr_num,
            'items'           => $packHdr->items,
            'new_sku'         => object_get($packHdr, 'sku', null),
            'weight'          => object_get($packHdr, 'weight', null),
            'net_weight'      => $totalWeight,
            'uom_id'          => object_get($packHdr, 'uom_id', null),

        ];
    }
}
