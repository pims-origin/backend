<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 3/6/2019
 * Time: 3:22 PM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackHdrModel;
use Carbon\Carbon;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OutPallet;
use Wms2\UserInfo\Data;
use App\Jobs\BOLJob;


class ShippingController extends AbstractController
{
    protected $packHdrModel;
    protected $orderHdrModel;
    protected $locationModel;
    protected $outPallet;

    public function __construct(
        PackHdrModel $packHdrModel,
        OrderHdrModel $orderHdrModel,
        OutPallet $outPallet,
        LocationModel $locationModel
    )
    {
        $this->packHdrModel     = $packHdrModel;
        $this->orderHdrModel    = $orderHdrModel;
        $this->locationModel    = $locationModel;
        $this->outPallet        = $outPallet;
    }

    public function assignShippingLane(Request $request, $whsId) {
        try {
            $input = $request->getParsedBody();
            $input['whs_id'] = $whsId;

            $this->validateShip($input);

            $location   = $input['location'];
            $packHdrs    = $input['packHdrs'];
            $odrHdr     = $input['orderHdr'];

            $outPallet = $this->outPallet->where("loc_id", $location->loc_id)
                ->where("whs_id", $input['whs_id'])->where("cus_id", $odrHdr->cus_id)
                ->whereIn("out_plt_sts", ["AC","NW"])->first();

            DB::beginTransaction();

            if(empty($outPallet)) {
                $prefix = str_replace_first("ORD","LPN",$odrHdr->odr_num);
                $prefix .= "-";
                $odrOutPallet = $this->outPallet->where("plt_num", "LIKE", $prefix."%")
                    ->where("whs_id", $input['whs_id'])->where("cus_id", $odrHdr->cus_id)
                    ->orderBy("plt_num", "DESC")->first();
                $code = (!empty($odrOutPallet->plt_num))?$odrOutPallet->plt_num:null;
                $newPltNum = $this->nextNum($code,$prefix);
                $outPalletParams = [
                    "loc_id"            => $location->loc_id,
                    "cus_id"            => $odrHdr->cus_id,
                    "whs_id"            => $odrHdr->whs_id,
                    "plt_num"           => $newPltNum,
                    "ctn_ttl"           => 1,
                    "is_movement"       => 1,
                    "loc_name"          => $location->loc_code,
                    "out_plt_sts"       => 'AC',
                    "loc_code"          => $location->loc_code,
                ];
                $outPallet = $this->outPallet->create($outPalletParams);
            }
            foreach ($packHdrs as $packHdr) {
                $packHdr->out_plt_id = $outPallet->plt_id;
                $packHdr->save();
            }


            $check = $this->packHdrModel->getModel()->where("odr_hdr_id", $odrHdr->odr_id)
                ->whereNull("out_plt_id")->where("whs_id", $input['whs_id'])->first();
            $userId = Data::getCurrentUserId();
            $current_time = Carbon::now()->timestamp;
            $dataLaborTracking = [
                'user_id'       => $userId,
                'cus_id'        => object_get($odrHdr, 'cus_id'),
                'whs_id'        => $whsId,
                'trans_num'     => object_get($packHdr, 'tracking_number', ''),
                'trans_dtl_id'  => object_get($odrHdr, 'odr_id'),
                'start_time'    => strtotime('-5 minutes'),
                'end_time'      => time(),
                'lt_type'       => "OB",
                'owner'         => object_get($odrHdr, "odr_num", "")
            ];
            DB::table('rpt_labor_tracking')->insert($dataLaborTracking);
            if(empty($check)) {
                $odrHdr->odr_sts = "SS";
                $odrHdr->save();
            }

            DB::commit();

            // Auto Create BOL
            dispatch(new BOLJob($odrHdr->odr_id, $whsId, $request));

            return [
                "data" => [
                    "message" => "Submit Successful!"
                ]
            ];
        } catch(\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function validateShip(&$attribute) {
        if(empty($attribute['shipping_label'])) {
            return $this->response->errorBadRequest("Shipping label is required!");
        }

        if(empty($attribute['loc_code'])) {
            return $this->response->errorBadRequest("Location code is required!");
        }

        $packHdrs = $this->packHdrModel->findWhere([
            "tracking_number"       => $attribute['shipping_label'],
            "whs_id"                => $attribute['whs_id']
        ]);
        if($packHdrs->count() == 0) {
            $msg = sprintf("Shipping label %s is not exit", $attribute['shipping_label']);
            return $this->response->errorBadRequest($msg);
        }
        $packHdr = $packHdrs->first();
        $orderHdr = $this->orderHdrModel->getFirstWhere([
            "odr_id"        => $packHdr->odr_hdr_id,
            "whs_id"        => $attribute['whs_id']
        ]);

        if(empty($orderHdr)) {
            return $this->response->errorBadRequest("Order is not exit!");
        }

        if($orderHdr->odr_sts != "PA" && $orderHdr->odr_sts != "PTD") {
            return $this->response->errorBadRequest("This order status is not Packed or Palletized");
        }

        $location = $this->locationModel->getModel()->join("loc_type", "loc_type.loc_type_id", "=", "location.loc_type_id")
            ->where("location.loc_code", $attribute['loc_code'])->where("loc_whs_id", $attribute['whs_id'])
            ->select("location.*","loc_type.loc_type_code")->first();

        if(empty($location)) {
            $msg = sprintf("Location %s is not exit", $attribute['loc_code']);
            return $this->response->errorBadRequest($msg);
        }

        if($location->loc_type_code != "SHP") {
            $msg = sprintf("Location %s is not shipping type", $attribute['loc_code']);
            return $this->response->errorBadRequest($msg);
        }

        $attribute['orderHdr'] = $orderHdr;
        $attribute['packHdrs']  = $packHdrs;
        $attribute['location'] = $location;
    }

    protected function nextNum($code = null, $prefix) {
        if(empty($code)) {
            return $prefix."0001";
        }
        $num = str_replace_first($prefix, "",$code);
        ++$num;
        $newnum = str_pad($num, 4, STR_PAD_LEFT);
        return $prefix.$newnum;
    }
}
