<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\WarehouseLocationModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Transformers\SystemStateTransformer;
use App\Api\V1\Validators\DeleteMassSystemStateValidator;
use App\Api\V1\Validators\DeleteMassZoneValidator;
use App\Api\V1\Validators\SystemStateValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;

class SystemStateController extends AbstractController
{
    /**
     * @var $systemStateModel
     */
    protected $systemStateModel;
    /**
     * @var $systemStateValidator
     */
    protected $systemStateValidator;
    /**
     * @var $systemStateTransformer
     */
    protected $systemStateTransformer;
    /**
     * @var $delMassSysStateValidator
     */
    protected $delMassSysStateValidator;

    public function __construct()
    {
        parent::__construct();

        $this->systemStateModel = new SystemStateModel();
        $this->systemStateValidator = new SystemStateValidator();
        $this->systemStateTransformer = new SystemStateTransformer();
        $this->delMassSysStateValidator = new DeleteMassSystemStateValidator();
    }

    protected function upsert($request, $id = false)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemStateValidator->validate($input);

        $params = [
            'sys_state_name'       => $input['sys_state_name'],
            'sys_state_code'       => $input['sys_state_code'],
            'sys_state_country_id' => $input['sys_state_country_id'],
            'sys_state_desc'       => array_get($input, 'sys_state_desc', '')
        ];

        if ($id) {
            $params['sys_state_id'] = $id;
        }

        try {
            // check duplicate code
            $stateByCode = $this->systemStateModel
                ->getStateInCountryByCode($params['sys_state_code'], $input['sys_state_country_id']);
            if ( $stateByCode && (!$id || ($id && $stateByCode->sys_state_id != $id)) ) {
                throw new \Exception("State Code is existed!");
            }
            
            // check duplicate name
            $stateByName = $this->systemStateModel
                ->getStateInCountryByName($params['sys_state_name'], $input['sys_state_country_id']);
            if ( $stateByCode && (!$id || ($id && $stateByName->sys_state_id != $id)) ) {
                throw new \Exception("State Name is existed!");
            }

            if ($id) {
                if ($systemState = $this->systemStateModel->update($params)) {
                    return $this->response->item($systemState, $this->systemStateTransformer)
                        ->setStatusCode(IlluminateResponse::HTTP_OK);
                }
            } else {
                if ($SystemState = $this->systemStateModel->create($params)) {
                    return $this->response->item($SystemState,
                        $this->systemStateTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
                }
            }

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    public function update($systemStateId, Request $request)
    {
        return $this->upsert($request, $systemStateId);
    }

    public function show($systemStateId)
    {
        $systemState = $this->systemStateModel->getFirstBy('sys_state_id', $systemStateId);
        if ($systemState) {
            return $this->response->item($systemState, $this->systemStateTransformer);
        } else {
            return $this->response->noContent();
        }
    }

    public function destroy($systemStateId)
    {
        try {
            if ($this->systemStateModel->deleteSystemState($systemStateId)) {
                return $this->response->noContent();
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Cannot Delete this item');
    }

    public function search(Request $request)
    {
        $input = $request->getQueryParams();

        try {
            $limit = array_get($input, 'limit', PAGING_LIMIT);
            $systemState = $this->systemStateModel->search($input, ['systemCountry'], $limit);

            return $this->response->paginator($systemState, $this->systemStateTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['sys_state_id'] = is_array($input['sys_state_id']) ? $input['sys_state_id'] : [$input['sys_state_id']];

        // validation
        $this->delMassSysStateValidator->validate($input);

        try {
            $this->systemStateModel->deleteMassSystemState($input['sys_state_id']);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }

    public function listStatesByCountryId($countryId, Request $request)
    {
        $input = $request->getQueryParams();

        $states = $this->systemStateModel->allBy('sys_state_country_id', $countryId, [], $input);

        return $this->response->collection($states, $this->systemStateTransformer);
    }
}
