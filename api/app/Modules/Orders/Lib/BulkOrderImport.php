<?php

namespace App\Modules\Orders\Lib;

use App\Modules\Orders\Eloquents\EventTracking;
use App\Modules\Orders\Eloquents\Item;
use App\Modules\Orders\Eloquents\OdrHdr;
use App\Modules\Orders\Eloquents\ShippingOdr;
use App\Modules\Orders\Eloquents\ThirdParty;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Modules\Orders\Eloquents\OdrHdrMeta;
use App\Modules\Orders\Eloquents\OdrDtlMeta;

class BulkOrderImport implements OrderImport
{
    protected static $odrTypes = ['Bulk' => 'BU', 'Retail' => 'RTL', 'Ecom' => 'EC'];

    protected static $acceptOdrTypes = ['BU', 'RTL', 'XDK', 'EC', 'TK', 'PC'];

    protected static $uoms = ['PC' => 2, 'CT' => 25];

    const ORDER_DTL_QUALIFIER = 945;
    const ORDER_HDR_QUALIFIER = 'OFP';

    public function import(array $data)
    {
        $formatData = $this->formatData($data);
        $odrHdr = null;

        $checkExist = OdrHdr::where('cus_odr_num', array_get($formatData, 'cus_odr_num'))
                            ->where('cus_po', array_get($formatData, 'cus_po'))
                            ->where('odr_sts', '!=', 'CC')
                            ->first();
        if ( $checkExist ) {
            throw new \Exception('Customer Order Number or PO Number already taken');
        }

        $thirdParty = $this->createOrUpdateThirdParty($formatData);
        $shippingOdr = $this->createOrUpdateShippingOdr($formatData);

        $formatData['shipping_addr_id'] = $thirdParty->tp_id;
        $formatData['so_id'] = $shippingOdr->so_id;

        $paramOdrHeader = $this->getDataHeader($formatData);
        $odrHdr = OdrHdr::create($paramOdrHeader);

        $paramOdrDetails = $this->getDataDetails($formatData, $odrHdr);
        $odrDtls = $odrHdr->odrDtls()->createMany($paramOdrDetails);

        $odrHdrMeta = $this->createOrUpdateOdrHdrMeta($formatData, $odrHdr);
        
        $newOrdHdr = OdrHdr::where("odr_id",$odrHdr->odr_id)->first();
        $dataEventTracking = ['whs_id'    => $newOrdHdr['whs_id'],
            'cus_id'    => $newOrdHdr['cus_id'],
            'owner'     => $newOrdHdr['odr_num'],
            'evt_code'  => "ONW",
            'trans_num' => $newOrdHdr['odr_num'],
            'info'      => sprintf("New order %s created", $newOrdHdr['order_num'])];
        EventTracking::create($dataEventTracking);
        return $odrHdr;
    }

    public function formatData($data)
    {
        $type = array_get($data, 'order_type', 'bulk');
        $type = Str::title($type);

        return [
            'odr_type'      => self::$odrTypes[$type],
            'cus_id'        => array_get($data, 'cus_id'),
            'whs_id'        => array_get($data, 'whs_id'),
            'cus_odr_num'   => array_get($data, 'order_num'),
            'cus_po'        => array_get($data, 'po_num'),
            'carrier'       => array_get($data, 'carrier.carrier'),
            'dlvy_srvc'     => array_get($data, 'carrier.dlvy_srvc'),
            'cus_notes'     => array_get($data, 'additional_information'),
            'spec_instruct' => array_get($data, 'special_instruction'),
            'odr_req_dt'    => array_get($data, 'order_date'),
            'ship_by_dt'    => array_get($data, 'ship_date'),
            'cancel_by_dt'  => array_get($data, 'cancel_date'),
            'ship_to_name'  => array_get($data, 'contacts.0.name'),
            'ship_to_phone' => array_get($data, 'contacts.0.phone'),
            'ship_to_email' => array_get($data, 'contacts.0.email'),
            'ship_to_add_1' => array_get($data, 'addresses.0.address_line_1'),
            'ship_to_add_2' => array_get($data, 'addresses.0.address_line_2'),
            'ship_to_city'  => array_get($data, 'addresses.0.city'),
            'ship_to_state' => array_get($data, 'addresses.0.state'),
            'ship_to_country' => array_get($data, 'addresses.0.country'),
            'ship_to_zip'   => array_get($data, 'addresses.0.zip'),
            'rush_odr'       => (int) array_get($data, 'rush_priority', 0),
            'ref_cod'       => array_get($data, 'order_ref'),
            'cus_pick_num'  => array_get($data, 'pick_ref'),
            'cus_dept_ref'  => array_get($data, 'dept_ref'),
            'meta'          => array_get($data, 'meta'),
            // ------
            'items'         => array_get($data, 'items'),
            'contacts'      => array_get($data, 'contacts'),
            'addresses'     => array_get($data, 'addresses'),
        ];
    }

    private function generateOdrNum()
    {
        $currentYearMonth = date('ym');
        $defaultWoNum = "ORD-${currentYearMonth}-00001";
        $order = OdrHdr::whereIn('odr_type', self::$acceptOdrTypes)
                        ->orderBy('odr_num', 'desc')
                        ->first();

        $lastNum = object_get($order, 'odr_num', '');

        if (empty($lastNum) || strpos($lastNum, "-${currentYearMonth}-") === false) {
            return $defaultWoNum;
        }

        return ++$lastNum;
    }

    public function createOrUpdateThirdParty($data) {
        $name = array_get($data, 'contacts.0.name');
        $name = preg_replace('/\s+/', ' ',$name);

        $thirdPartyParams = [
            'whs_id'    => array_get($data, 'whs_id'),
            'cus_id'    => array_get($data, 'cus_id'),
            'name'      => $name,
            'des'       => array_get($data, 'contacts.0.description'),
            'phone'     => array_get($data, 'contacts.0.phone'),
            'mobile'    => array_get($data, 'contacts.0.mobile'),
            'ct_first_name' => array_get($data, 'contacts.0.first_name'),
            'ct_last_name'  => array_get($data, 'contacts.0.last_name'),
            'extns' => array_get($data, 'contacts.0.extension'),
            'add_1'     => array_get($data, 'addresses.0.address_line_1'),
            'add_2'     => array_get($data, 'addresses.0.address_line_2'),
            'city'      => array_get($data, 'addresses.0.city'),
            'state'     => array_get($data, 'addresses.0.state'),
            'zip'       => array_get($data, 'addresses.0.zip'),
            'country'   => array_get($data, 'addresses.0.country'),
            'email'     => array_get($data, 'contacts.0.email'),
            'ctt_type'  => 'receiver',
            'addr_type' => 'ship_to'
        ];

        $thirdParty = ThirdParty::where(
                        DB::raw("UPPER(REPLACE(name, ' ', ''))"),
                        strtoupper(preg_replace('/\s+/', '', $thirdPartyParams['name']))
                    )
                    ->first();

        // Update
        if ($thirdParty) {
            $thirdParty->update($thirdPartyParams);

            return $thirdParty;
        }

        // Create
        $thirdParty = ThirdParty::create($thirdPartyParams);

        return $thirdParty;
    }

    public function createOrUpdateShippingOdr($data, $oldRef = null, $isUpdate = false) {
        $shipParams = [
            'cus_id'      => $data['cus_id'],
            'whs_id'      => $data['whs_id'],
            'cus_odr_num' => $data['cus_odr_num'],
            'type'        => $data['odr_type'],
            'so_sts'      => 'NW',
        ];

        // Create Shipping Order.
        $shippingOdr = ShippingOdr::where([
            'cus_odr_num' => $shipParams['cus_odr_num'],
            'whs_id'      => $shipParams['whs_id'],
            'cus_id'      => $shipParams['cus_id'],
            'type'        => $shipParams['type']
        ])->first();

        if ( !$shippingOdr ) {
            // Create Shipping Odr
            $shipParams['po_total'] = 1;
            $shippingOdr = ShippingOdr::create($shipParams);
            return $shippingOdr;
        }

        // Update po_total
        if ( $isUpdate && $oldRef !== $shipParams['cus_odr_num'] ) {
            $shipParams['po_total'] = (int)$so->po_total + 1;
            $shipParams['so_id'] = $soId;
            $shippingOdr->update($shipParams);
        }

        return $shippingOdr;
    }

    private function getCompleteDateByShipDate($shipDate)
    {
        if (!$shipDate) {
            return null;
        }
        $currentDateTimestamp = strtotime(date('Y-m-d'));
        $shipDateTimestamp = strtotime($shipDate);

        if ($shipDateTimestamp < $currentDateTimestamp) {
            return $shipDateTimestamp;
        }

        if ($shipDateTimestamp - $currentDateTimestamp >= 3 * 86400) {
            return $shipDateTimestamp - 3 * 86400;
        } else {
            return $currentDateTimestamp;
        }
    }

    public function getDataHeader($data)
    {
        $header = [
            // Type
            'is_ecom'          => 0,
            // Order Hdr
            'odr_type'         => array_get($data, 'odr_type'),
            'odr_num'          => $this->generateOdrNum(),
            'carrier'          => array_get($data, 'carrier'),
            'ship_to_name'     => array_get($data, 'ship_to_name'),
            'ship_to_email'    => array_get($data, 'ship_to_email'),
            'ship_to_add_1'    => array_get($data, 'ship_to_add_1'),
            'ship_to_add_2'    => array_get($data, 'ship_to_add_2'),
            'ship_to_city'     => array_get($data, 'ship_to_city'),
            'ship_to_country'  => array_get($data, 'ship_to_country'),
            'ship_to_state'    => array_get($data, 'ship_to_state'),
            'ship_to_phone'    => array_get($data, 'ship_to_phone'),
            'ref_cod'          => array_get($data, 'ref_cod'),
            'ship_to_zip'      => array_get($data, 'ship_to_zip'),
            'odr_req_dt'       => !empty($data['odr_req_dt']) ? strtotime($data['odr_req_dt']) : 0,
            'ship_by_dt'       => !empty($data['ship_by_dt']) ? strtotime($data['ship_by_dt']) : 0,
            'cancel_by_dt'     => !empty($data['cancel_by_dt']) ? strtotime($data['cancel_by_dt']) : 0,
            'req_cmpl_dt'      => $this->getCompleteDateByShipDate($data['ship_by_dt']),
            'act_cmpl_dt'      => strtotime(array_get($data, 'act_cmpl_dt', null)),
            'act_cancel_dt'    => strtotime(array_get($data, 'act_cancel_dt', null)),
            'in_notes'         => array_get($data, 'in_notes', null),
            'cus_notes'        => array_get($data, 'cus_notes', null),
            'csr_notes'        => array_get($data, 'csr_notes', null),
            // Order Info
            'cus_id'           => array_get($data, 'cus_id'),
            'whs_id'           => array_get($data, 'whs_id'),
            'cus_odr_num'      => array_get($data, 'cus_odr_num'),
            'cus_po'           => array_get($data, 'cus_po'),
            'odr_type'         => array_get($data, 'odr_type'),
            'rush_odr'         => (int)array_get($data, 'rush_odr', 0),
            'odr_sts'          => 'NW',
            'sku_ttl'          => count($data['items']),
            'shipping_addr_id' => array_get($data, 'shipping_addr_id'),
            'so_id'            => array_get($data, 'so_id'),
            'cus_pick_num'     => array_get($data, 'cus_pick_num'),
            'cus_dept_ref'     => array_get($data, 'cus_dept_ref'),

            'dlvy_srvc'        => array_get($data, 'dlvy_srvc'),
            'carrier_code'     => array_get($data, 'carrier'),
            'spec_instruct'    => array_get($data, 'spec_instruct'),
        ];

        return $header;
    }

    public function getDataDetails($data, $odrHdr)
    {
        $details = [];

        foreach ( $data['items'] as $detail ) {
            $item = DB::table('item')
                ->select('item.*', DB::raw('IFNULL(SUM(invt_smr.avail)  - SUM(invt_smr.lock_qty), 0) AS avail'))
                ->where('item.sku', array_get($detail, 'sku'))
                ->where('item.cus_id', $odrHdr->cus_id)
                ->where('item.status', 'AC')
                ->leftJoin('invt_smr', 'item.item_id', '=', 'invt_smr.item_id')
                // ->where('invt_smr.whs_id', $odrHdr->whs_id)
                ->groupBy('item.item_id')
                ->first();

            if ( !$item ) {
                throw new \Exception("Item {$detail['sku']} does not exist");
            }

            $item = (array) $item;
            $pack = array_get($item, 'pack', 1);
            $pack = $pack > 0 ? $pack : 1;
            $size = array_get($item, 'size');
            $color = array_get($item, 'color');
            $description = array_get($detail, 'description');
            $cusUpc = array_get($item, 'cus_upc');
            $uom = array_get($item, 'uom_code', 'PC');
            $lot = array_get($detail, 'lot', 'NA');

            if ( strtoupper($uom) == 'PC' ){
                $pieceQty = $detail['qty'];
                $qty = ceil($detail['qty'] / $pack);
            }

            if ( strtoupper($uom) == 'CT' ){
                $qty = ceil($detail['qty']);
                $pieceQty = $qty * $pack;
            }

            $details[] = [
                'whs_id'        => $odrHdr->whs_id,
                'cus_id'        => $odrHdr->cus_id,
                // -----
                'sku'           => array_get($item, 'sku'),
                'item_id'       => array_get($item, 'item_id'),
                'cus_upc'       => $cusUpc,
                'size'          => $size,
                'color'         => $color,
                'lot'           => empty($lot) ? 'NA' : $lot,
                'des'           => $description,
                'qty'           => $qty,
                'piece_qty'     => $pieceQty,
                'uom_code'      => $uom,
                'uom_id'        => self::$uoms[$uom],
                'pack'          => $pack,
                'sts'           => 'i',
                'ship_track_id' => array_get($detail, 'tracking_number'),
                //---
                'prod_line'     => array_get($detail, 'product_line', null),
                'cmp'           => array_get($detail, 'campaign', null),
            ];
        }

        return $details;
    }

    public function createOrUpdateOdrHdrMeta($data, $odrHdr)
    {
        $odrHdrMeta = [];
        if (array_get($data, 'meta')) {
            $odrHdrMetaData = [
                'qualifier'  => self::ORDER_HDR_QUALIFIER,
                'odr_id'     => $odrHdr->odr_id,
                'value'      => json_encode(array_get($data, 'meta'))
            ];

            if (!$odrHdrMeta = $odrHdr->odrHdrMeta)
            {
                $odrHdrMeta = OdrHdrMeta::create($odrHdrMetaData);
            } else
            {
                $odrHdrMeta->update($odrHdrMetaData);
                $odrHdrMeta = $odrHdrMeta->fill($odrHdrMetaData);
            }
        }

        return $odrHdrMeta;
    }

    public function createOrUpdateOdrDtlMeta($data, $odrDtls)
    {
        $odrDtlMetaArr = [];
        $arrOdrDtl = array_pluck($odrDtls, 'odr_dtl_id', 'sku');

        $items = array_get($data, 'items');

        foreach ($items as $item) {

            if (array_get($item, 'meta')) {
                foreach ($arrOdrDtl as $sku => $id) {
                    if ($item['sku'] == $sku)
                    {
                        $odrDtlId = $id;
                        break;
                    }
                }
                $arrOdrDtlMeta = [
                    'qualifier' => self::ORDER_DTL_QUALIFIER,
                    'odr_dtl_id' => $odrDtlId,
                    'value'      => json_encode(array_get($item, 'meta'))
                ];

                if (!$odrDtlMeta = OdrDtlMeta::withTrashed()->find($odrDtlId))
                {
                    $odrDtlMetaArr[] = OdrDtlMeta::create($arrOdrDtlMeta);
                } else
                {
                    if ($odrDtlMeta->deleted == 0)
                    {
                        $odrDtlMeta->update($arrOdrDtlMeta);
                    } else
                    {
                        $odrDtlMeta->update($arrOdrDtlMeta);
                        $odrDtlMeta->restore();
                    }
                    $odrDtlMetaArr[] = $odrDtlMeta->fill($arrOdrDtlMeta);
                }
            }
        }

        return $odrDtlMetaArr;
    }
}