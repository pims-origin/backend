<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 09-May-16
 * Time: 10:24
 */

namespace App\Api\V1\Transformers;

use App\MenuFavorite;
use League\Fractal\TransformerAbstract;

class MenuFavoriteTransformer extends TransformerAbstract
{
    /**
     * @param MenuFavorite $menuFavorite
     *
     * @return array
     */
    public function transform(MenuFavorite $menuFavorite)
    {
        return [
            'user_id'       => $menuFavorite->user_id,
            'menu_id'       => $menuFavorite->menu_id,
            'display_order' => $menuFavorite->display_order,
        ];
    }
}
