<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ChargeCode;

class ChargeCodesModel extends AbstractModel
{
    public function __construct(ChargeCode $model = null)
    {
        $this->model = ($model) ?: new ChargeCode();
    }

    /*
    ****************************************************************************
    */

}
