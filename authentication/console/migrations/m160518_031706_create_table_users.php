<?php

use yii\db\Migration;

/**
 * Handles the creation for table `users`.
 */
class m160518_031706_create_table_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'user_id'    => $this->primaryKey(),
            'type'       => $this->string(1)->notNull(),
            'first_name' => $this->string(35),
            'last_name'  => $this->string(35),
            'email'      => $this->string(64)->notNull()->unique(),
            'password'   => $this->string(64)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'status'     => $this->string(2)->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
