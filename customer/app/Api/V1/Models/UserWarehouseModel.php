<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\UserWarehouse;

class UserWarehouseModel extends AbstractModel
{
    /**
     * UserWarehouseModel constructor.
     *
     * @param UserWarehouse|null $model
     */
    public function __construct(UserWarehouse $model = null)
    {
        $this->model = ($model) ?: new UserWarehouse();
    }
}
