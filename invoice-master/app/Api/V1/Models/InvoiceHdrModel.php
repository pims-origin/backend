<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 22-Mar-18
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Models\InvoiceHeader;


/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class InvoiceHdrModel extends AbstractModel
{
    /**
     * InvoiceHdrModel constructor.
     *
     * @param InvoiceHeader|null $model
     */
    public function __construct(InvoiceHeader $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
    }

    /**
     * @param $invId
     *
     * @return mixed
     */
    public function printInvoice($invId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_hdr')
            ->select([
                'inv_hdr.*'
            ]);

        $query->where('inv_hdr.inv_id', $invId);
        $row = $query->first();

        return $row;
    }

    /**
     * @param $invId
     *
     * @return mixed
     */
    public function InvoiceDetail($invId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('inv_dtl')
            ->select([
                'inv_dtl.*',
                'iu.inv_uom_des'
            ])
            ->leftJoin('inv_uom as iu', 'iu.inv_uom_code', '=', 'inv_dtl.uom')
            ->where('inv_dtl.inv_id', $invId)
            ->where('inv_dtl.deleted', 0);
        $row = $query->get();

        return $row;
    }


}
