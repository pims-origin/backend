<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\EventTracking;

class EventTrackingAutoTransformer extends TransformerAbstract
{
    /**
     * @param EventTracking $eventTracking
     *
     * @return array
     */
    public function transform(EventTracking $eventTracking)
    {
        return [
            'owner' => object_get($eventTracking, 'owner', null)
        ];

    }

}
