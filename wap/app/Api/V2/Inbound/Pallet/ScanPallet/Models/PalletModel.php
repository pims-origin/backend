<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Inbound\Pallet\ScanPallet\Models;

use DB;
use Dingo\Api\Exception\UnknownVersionException;
use phpDocumentor\Reflection\Types\This;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class PalletModel extends AbstractModel
{
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @param Pallet $model
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        Pallet $model = null,
        CartonModel $cartonModel = null,
        LocationModel $locationModel = null,
        EventTrackingModel $eventTrackingModel = null

    )
    {
        $this->model = ($model) ?: new Pallet();
        $this->cartonModel = ($cartonModel) ?: new CartonModel();
        $this->locationModel = ($locationModel) ?: new LocationModel();
        $this->eventTrackingModel = $eventTrackingModel;
    }

    public function findWhereGoodReceipt(array $attributes = [], array $with = [], $columns = ['*'])
    {
        $query = $this->make($with);

        $query->whereHas('carton.goodReceiptDtl', function ($query) use ($attributes) {
            $query->where('gr_hdr_id', $attributes['gr_hdr_id']);

        });
        $this->sortBuilder($query, $attributes);

        return $query->get($columns);

    }

    /**
     * @param $locationIds
     *
     * @return array
     */
    public function getPltIdsByLocIds($locationIds)
    {
        $locationIds = is_array($locationIds) ? $locationIds : [$locationIds];

        $rows = $this->model
            ->select('plt_id')
            ->whereIn('loc_id', $locationIds)
            ->get();

        $ids = [];
        if (!$rows->isEmpty()) {
            foreach ($rows as $row) {
                $ids[] = $row->plt_id;
            }
        }

        return $ids;
    }



    //not in use
    /*public function getPalletAllByRfid($rfid, $whsID)
    {
        return $this->model->where('rfid', $rfid)->where('whs_id', $whsID)->first();
    }*/

    /**
     * @param $rfid
     *
     * @return mixed
     */
    public function countPalletByRfid($rfid)
    {
        return $this->model->select('plt_id')->where('rfid', $rfid)->count();
    }

    /**
     * @param $params
     * @param $grDetail
     *
     * @return bool
     */
    public function assignPallet($pltRfid, $ctnRfid, $grDetail)
    {
        $palletRfid = $pltRfid;

        $goodsReceipt = $grDetail[0]['goods_receipt'];

        $itemId = $grDetail[0]['asn_detail']['item_id'];

        $asnDtlId = $grDetail[0]['asn_detail']['asn_dtl_id'];

        $ctnTtlOnPlt = count($ctnRfid);

        $itemIds = array_pluck($grDetail, 'asn_detail.item_id', 'asn_dtl_id');

        $palletCode = str_replace('GDR', 'LPN', $goodsReceipt['gr_hdr_num']);

        $countPallet = \DB::table('pallet')->where('rfid', $palletRfid)->count();

        $palletNum = $palletCode . "-" . str_pad($countPallet + 1, 3, "0", STR_PAD_LEFT);

        //check is existed pallet RFID
        $isExistPalletRfid = $this->countPalletByRfid($palletRfid);
        if ($isExistPalletRfid > 0) {
            return false;
        }

        // Add a pallet
        $palletResult = (new PalletModel())->create([
            'cus_id' => $goodsReceipt['cus_id'],
            'whs_id' => $goodsReceipt['whs_id'],
            'plt_num' => $palletNum,
            'rfid' => $palletRfid,
            'ctn_ttl' => $ctnTtlOnPlt
        ]);

        $params = [
            'plt_id' => $palletResult->plt_id,
            'cus_id' => $goodsReceipt['cus_id'],
            'whs_id' => $goodsReceipt['whs_id'],
            'item_id' => $itemId,
            'asn_dtl_id' => $asnDtlId
        ];

        // Add carton to that pallet.
        $this->cartonModel->assignedPalletToCarton($params, $ctnTtlOnPlt);

        return true;
    }

    public function suggestLocation($customerId, $warehouseId, $type = 'RAC')
    {
        // Location not in table Pallet
        // Location in table pallet but ctn_total = 0 or null
        $exceptLocationIds = $this->model
            ->join('location', 'pallet.loc_id', '=', 'location.loc_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->select(['location.loc_id'])
            ->where('location.loc_id', '>', 0)
            ->where('location.deleted', 0)
            ->where('loc_type.deleted', 0)
            //->where('loc_type.loc_type_code', '!=', 'MZ')
            ->where('loc_type.loc_type_code', $type)// PUTWAY, ECOM, RACK , ECOM : ECO,  RAC
            ->get()->toArray();

        $exceptLocationIds = array_pluck($exceptLocationIds, 'loc_id', 'loc_id');

        $location = $this->locationModel->loadSuggestedLocation($customerId, $warehouseId, $exceptLocationIds, $type);

        return $location;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'plt_num') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        //filterData in searching Pallet
        $this->model->filterData($query);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param array $param
     *
     * @return mixed
     */

    public function countPalletWhereInLocation(array $param)
    {
        $query = $this->model->whereIn('loc_id', $param);

        return $query->count();
    }

    /**
     * @param array $param
     */
    public function removeWhereInLocation(array $param)
    {
        return $this->model->whereIn('loc_id', $param)->update([
            'loc_id' => null,
            'loc_name' => null,
            'loc_code' => null
        ]);
    }

    /**
     * @param array $param
     */
    public function removeWhereLocIdPltId($param)
    {
        return $this->model
            ->where('plt_id', $param['plt_id'])
            ->update([
                'loc_id' => null,
                'loc_name' => null,
                'loc_code' => null
            ]);
    }

    /**
     * Generate Asn number and sequence
     *
     * @return array
     */
    public function generateEventOwner($type)
    {

        $currentYearMonth = date('ym');

        $lastEvent = $this->eventTrackingModel->getFirstWhere(
            [['owner', 'like', '%' . $type . '%']],
            [],
            ['id' => 'desc']
        );
        $next = 1;
        if ($lastEvent) {
            list(, $yymm, $number) = explode('-', $lastEvent->owner);
            $next = ($yymm != $currentYearMonth) ? $next : intval($number) + 1;
        }

        $result = sprintf('%s-%s-%s',
            $type,
            $currentYearMonth,
            str_pad($next, 6, '0', STR_PAD_LEFT));

        return $result;
    }

    /**
     * @param $locId
     *
     * @return bool
     */
    public function checkLocationExistOnPallet($locId)
    {
        $query = $this->model->where('loc_id', $locId);
        if ($query->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function loadAllLoc($with = [])
    {
        $query = $this->make($with);
        $query->whereNotNull('loc_id')
            ->groupBy('loc_id');

        $this->model->filterData($query, true);

        $model = $query->get();

        return $model;
    }

    public function loadLocNotEmptyPallet()
    {
        $model = $this->model
            ->select('loc_id')
            ->whereNotNull('loc_id')
            ->get();

        return $model;
    }

    public function updatePallet($pltInfo)
    {
        $pltId = array_get($pltInfo, 'plt_id', null);
        $created_at = array_get($pltInfo, 'created_at', 0);
        $zeroDt = time();

        // Calculate storage_duration
        $date1 = date("Y-m-d",
            is_int($created_at) || is_string($created_at) ? (int)$created_at : $created_at->timestamp);
        $date2 = date("Y-m-d");

        $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
        if ($dateDiff == 0) {
            $storageDuration = 1;
        } else {
            $storageDuration = $dateDiff;
        }

        return $this->model
            ->where('plt_id', $pltId)
            ->update([
                'loc_id' => null,
                'loc_code' => null,
                'loc_name' => null,
                'ctn_ttl' => 0,
                'plt_sts' => 'PD',
                'zero_date' => $zeroDt,
                'storage_duration' => $storageDuration
            ]);
    }

    public function getAllPalletHasNoCarton($pltIds)
    {
        return $this->model
            ->select('pallet.plt_id', 'pallet.created_at')
            ->whereIn('pallet.plt_id', $pltIds)
            ->whereRaw('(pallet.loc_id is not null or pallet.ctn_ttl > 0)')
            ->whereRaw('(Select count(cartons.ctn_id) from cartons where cartons.plt_id = pallet.plt_id) = 0')
            ->get();
    }


    public function getPalletByRfid($whsId, $rfid)
    {
        $model = $this->model
            ->where('whs_id', $whsId)
            ->where('rfid', $rfid)
            // ->where('ctn_ttl', '>', 0)
            // ->where('plt_sts', 'AC')
            ->first();
        return $model;
    }

    public function getPalletByRfidUsing($whsId, $rfid)
    {
        $model = $this->model
            ->select('cus_id', 'plt_id')
            ->where('whs_id', $whsId)
            ->where('rfid', $rfid)
            ->where('ctn_ttl', '>', 0)
            // ->where('plt_sts', 'AC')
            ->first();

        return $model;
    }

    public function checkPalletHasLocation($whsId, $rfid)
    {
        $model = $this->model
            ->where('whs_id', $whsId)
            ->where('rfid', $rfid)
            ->whereNotNull('loc_id')
            // ->where('plt_sts', 'AC')
            ->count();

        return (bool)$model;
    }

    public function checkPalletExist($whsId, $rfid)
    {
        $model = $this->model
            ->where('whs_id', $whsId)
            ->where('rfid', $rfid)
            // ->where('plt_sts', 'AC')
            ->count();

        return (bool)$model;
    }

    public function updatePalletCtnTtl($locIds)
    {
        return $this->model
            ->whereIn('loc_id', $locIds)
            ->update([
                'ctn_ttl' => DB::raw("(SELECT COUNT(c.ctn_id) FROM cartons c WHERE pallet.plt_id = c.plt_id)")
            ]);
    }


    public function updateZeroPallet($locIds)
    {
        //$strSQL = sprintf("IF(DATEDIFF(NOW(), FROM_UNIXTIME(created_at)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        //(created_at)), 1)", date('Y-m-d'));

        return $this->model
            ->whereIn('loc_id', $locIds)
            ->where('ctn_ttl', 0)
            ->update([
                'loc_id' => null,
                'loc_code' => null,
                'loc_name' => null,
                'zero_date' => time(),
                'storage_duration' => CartonModel::getCalculateStorageDurationRaw()
            ]);
    }

    public function countPalletWhereGRHdrId($gHdrId)
    {
        return $this->model->where([
            'gr_hdr_id' => $gHdrId
        ])
            ->count();
    }

    /**
     * @param $grHdrNum
     *
     * @return string
     */
    public function generatePalletNum($grHdrNum)
    {
        $palletCode = str_replace('GDR', 'LPN', $grHdrNum);

        $countPallet = \DB::table('pallet')->select('plt_num')
            ->where('plt_num', 'like', $palletCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();
        $max = 0;
        if ($countPallet) {
            $pltCode = array_get($countPallet, 'plt_num', null);
            $arrMax = explode('-', $pltCode);
            $max = (int)end($arrMax);
        }

        return $palletCode . "-" . str_pad($max + 1, 3, "0", STR_PAD_LEFT);
    }

    /**
     * 4. Check Pallet if not existed, create a new pallet and update carton total
     *
     * @param type $pltRfid
     * @param type $grDtl
     * @param type $grHdr
     * @param type $asnDetail
     */
    public function createPalletIfNotExist($pltRfid, $grDtl, $grHdr, $asnDetail)
    {
        $pallet = $this->getModel()
            ->where([
                'rfid' => $pltRfid,
                // 'plt_sts' => 'AC',
                'gr_dtl_id' => object_get($grDtl, 'gr_dtl_id')
            ])
            ->get()
            ->first();
        if (!$pallet) {
            $pallet = $this->createPallet($pltRfid, $grDtl, $grHdr, $asnDetail);

            return $pallet;
        }

        return $pallet;
    }

    /**
     * Create pallet
     *
     * @param type $pltRfid
     * @param type $grDtl
     * @param type $grHdr
     * @param type $asnDetail
     *
     * @return type
     */
    private function createPallet_bk($pltRfid, $grDtl, $grHdr, $asnDetail)
    {
        $vtlCtnModel = new VirtualCartonModel();
        $grHdrNum = object_get($grHdr, 'gr_hdr_num');
        $whsId = object_get($grHdr, 'whs_id');
        $cusId = object_get($grHdr, 'cus_id');
        $cusId = object_get($grHdr, 'cus_id');
        $pltNum = $this->generatePalletNum($grHdrNum);

        $pallet = $this->getModel()
            ->where([
                'rfid' => $pltRfid,
                // 'plt_sts' => 'AC',
                'gr_dtl_id' => object_get($grDtl, 'gr_dtl_id')
            ])
            ->get()
            ->first();

        $ctnTtl = $vtlCtnModel->getModel()
            ->where([
                'plt_rfid' => $pltRfid,
                'whs_id' => $whsId,
                'asn_dtl_id' => object_get($asnDetail, 'asn_dtl_id')
            ])
            ->get();
        if ($pallet) {
            $pallet->update([
                'ctn_ttl' => $ctnTtl->count()
            ]);

            return $pallet;
        }
        $firstCarton = $ctnTtl->count() ? $ctnTtl[0] : new \stdClass();
        $locId = object_get($firstCarton, 'loc_id', null);
        $locCode = object_get($firstCarton, 'loc_code', null);
        $grHdrId = object_get($grHdr, 'gr_hdr_id', null);
        $grDtlId = object_get($grDtl, 'gr_dtl_id', null);
        $palletData = [
            'whs_id' => $whsId,
            'cus_id' => $cusId,
            'plt_num' => $pltNum,
            'rfid' => $pltRfid,
            'loc_id' => $locId,
            'loc_code' => $locCode,
            'loc_name' => $locCode,
            'gr_hdr_id' => $grHdrId,
            'gr_dtl_id' => $grDtlId,
            'ctn_ttl' => $ctnTtl->count()
        ];

        $this->refreshModel();
        $palletObj = $this->create($palletData);

        return $palletObj;
    }

    public function createPallet($grDtl, $grHrd, $asnDetail, $pltRfid, $arrCtnRfid, $dmgTtl = 0)
    {
        $grHdrNum = object_get($grHrd, 'gr_hdr_num', null);
        $grDtlId = array_get($grDtl, 'gr_dtl_id', 0);
        $cusId = array_get($grHrd, 'cus_id', 0);
        $whsId = array_get($grHrd, 'whs_id', 0);
        $grHdrId = array_get($grHrd, 'gr_hdr_id', 0);
        /*
                //case 1, not exist pallet, create new pallet
                $pallet = $this->checkPalletExistedByRFID($pltRfid, $grHdrId, $grDtlId, $whsId, $cusId);

                //update ctn ttl
                if ($pallet) {
                    return $pallet;
                }*/

        //case 2, not exist pallet, create new pallet
        $pltNum = $this->generatePalletNum($grHdrNum);

        $palletData = [
            'whs_id'    => $whsId,
            'cus_id'    => $cusId,
            'plt_num'   => $pltNum,
            'rfid'      => $pltRfid,
            'gr_hdr_id' => $grHdrId,
            'gr_dtl_id' => $grDtlId,
            'ctn_ttl'   => count($arrCtnRfid),
            'dmg_ttl'   => $dmgTtl,
            'data'      => \json_encode([]),
            'plt_sts'   => 'RG',
            'item_id'   => object_get($asnDetail, 'item_id', null),
            'sku'       => object_get($asnDetail, 'asn_dtl_sku', null),
            'size'      => object_get($asnDetail, 'asn_dtl_size', null),
            'color'     => object_get($asnDetail, 'asn_dtl_color', null),
            'lot'       => object_get($asnDetail, 'asn_dtl_lot', 'NA'),
            'pack'      => object_get($asnDetail, 'asn_dtl_pack', null),
        ];

        $this->refreshModel();
        $palletObj = $this->create($palletData);

        return $palletObj;
    }


    public function checkPalletExistedByRFID($pltRfid, $grHdrId, $grDtlId, $whsId, $cusId)
    {
        $query = $this->model
            ->where('ctn_ttl', '>', 0)
            // ->where('plt_sts', 'AC')
            ->where('rfid', $pltRfid)
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->where('gr_dtl_id', $grDtlId)
            ->where('gr_hdr_id', $grHdrId);

        $result = $query->first();

        return $result;
    }

    public function updatePalletCtnTtlAndDamaged($whsId, $grHdrIds, $pltId, $ccn, $ctnOtherPlt = [], $unscanned=[])
    {
        $grHdrIds = is_array($grHdrIds) ? implode(",", $grHdrIds) : $grHdrIds;
        $ctnTtl = "(SELECT COUNT(DISTINCT cartons.rfid) FROM cartons
                                  WHERE cartons.plt_id = {$pltId} AND cartons.whs_id = {$whsId}
                                    AND cartons.gr_hdr_id IN ($grHdrIds) AND cartons.deleted = 0 AND cartons.deleted_at = 915148800)";
        $dmgTtl = "(SELECT COUNT(DISTINCT cartons.rfid) FROM cartons
                                  WHERE cartons.plt_id = {$pltId} AND cartons.whs_id = {$whsId} AND cartons.is_damaged = 1
                                    AND cartons.gr_hdr_id IN ($grHdrIds) AND cartons.deleted = 0 AND cartons.deleted_at = 915148800)";
        $ctnTtlGrDtl = DB::table('cartons')
            ->where('plt_id', $pltId)
            ->where('deleted', 0)
            ->selectRaw('gr_dtl_id, COUNT(ctn_id) as ctn_ttl')->groupBy('gr_dtl_id')->get();


        $dmgTtlGrDtl = DB::table('cartons')
            ->where('plt_id', $pltId)
            ->where('deleted', 0)
            ->where('is_damaged', 1)
            ->selectRaw('gr_dtl_id, COUNT(ctn_id) as dmg_ttl')->groupBy('gr_dtl_id')->get();
        $data = [
            'dmg_ttls' => array_pluck($dmgTtlGrDtl, 'dmg_ttl', 'gr_dtl_id'),
            'ctn_ttls' =>
                array_pluck($ctnTtlGrDtl, 'ctn_ttl', 'gr_dtl_id'),

        ];
        if (count($ctnOtherPlt) > 0) {
            $data['ctns_other_plt'] = $ctnOtherPlt;
        }
        if(count($unscanned) >0 ) {
            $data['unscanned'] = $unscanned;
        }

        $data = \json_encode($data);

        $userId = Data::getCurrentUserId();
        $result = false;
        if ($ctnTtl) {
            $result = DB::table('pallet')->where('plt_id', $pltId)
                ->update([
                    'ctn_ttl'      => DB::raw($ctnTtl),
                    'init_ctn_ttl' => DB::raw($ctnTtl),
                    'dmg_ttl'      => DB::raw($dmgTtl),
                    'data'         => $data,
                    'ccn'          => $ccn,
                    'updated_by'   => $userId,
                    'updated_at'   => time(),
                ]);

            // update carton total for pallet
            DB::table('pal_sug_loc')
                ->where('plt_id', $pltId)
                ->where('deleted', 0)
                ->update(['ctn_ttl' => DB::raw($ctnTtl)]);
        } else {
            $result = DB::table('pallet')->where('plt_id', $pltId)
                ->update([
                    'updated_by'   => $userId,
                    'updated_at'   => time(),
                    'deleted'      => 1,
                    'deleted_at'   => time(),
                ]);
            // Deleted old location sugg for pallet
            DB::table('pal_sug_loc')
                ->where('plt_id', $pltId)
                ->where('deleted', 0)
                ->update(['deleted' => 1, 'deleted_at' => time()]);
        }


        return $result;
    }

}
