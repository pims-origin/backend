<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\ReportConfigs;

class ReportDetailTotalsTransformer extends TransformerAbstract
{
    /**
     * @param ReportConfigs $reportConfigs
     *
     * @return array
     */
    public function transform(ReportConfigs $reportConfigs)
    {
        /**
         * return Transform
         */
        return [
            'qualifier'         =>  object_get($reportConfigs, 'qualifier', null),
            'total'             =>  object_get($reportConfigs, 'total', null),
        ];
    }
}
