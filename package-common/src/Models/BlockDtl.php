<?php

namespace Seldat\Wms2\Models;

class BlockDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'block_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'block_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'block_hdr_id',
        'whs_id',
        'cus_id',
        'block_dtl_sts',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'loc_id',
        'loc_name',
        'sts'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function blockHdr()
    {
        return $this->belongsTo(BlockHdr::class, 'block_hdr_id', 'block_hdr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'whs_id', 'whs_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'cus_id', 'cus_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function location()
    {
        return $this->belongsTo(Location::class, 'loc_id', 'loc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function itemInfo()
    {
        return $this->belongsTo(Item::class, 'item_id', 'item_id');
    }
}
