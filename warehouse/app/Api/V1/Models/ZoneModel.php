<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Zone;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class ZoneModel extends AbstractModel
{
    const DEFAULT_ZONE_MAX_COUNT = 100;
    /**
     * @var ZoneModel
     */
    protected $model;

    /**
     * ZoneModel constructor.
     *
     * @param Zone|null $model
     */
    public function __construct(Zone $model = null)
    {
        $this->model = ($model) ?: new Zone();
    }

    /**
     * Get Zone
     *
     * @param array $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function getzones($cus_id, $with)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis::getCurrentCusIds();
        $query = $this->make($with);

        $query->select('zone_id', 'zone_name', 'zone_code')
            ->where('dynamic', 0)
            ->where('zone_whs_id', $currentWH);

        $query->whereHas('customerZone', function ($query) use ($cus_id, $cus_ids) {
            if ($cus_id) {
                $query->where('cus_id', $cus_id);
            } else {
                $query->whereIn('cus_id', $cus_ids);
            }
        });

        return $query->get();
    }

    public function getCapacity($cus_ids, $whsId){
        $capacity = [
            'zone_id'   => 0,
            'zone_name' => null,
            'per_full'  => 0,
            'details'   => []
        ];

        $zones = $this->model
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'zone.zone_id')
            ->join('location', 'location.loc_zone_id', '=', 'zone.zone_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin('pallet', 'pallet.loc_id', '=', 'location.loc_id')
            ->whereIn('customer_zone.cus_id', $cus_ids)
            ->where('location.loc_whs_id', $whsId)
            ->where('location.loc_sts_code', 'AC')
            ->whereNull('location.parent_id')
            ->whereNotIn('loc_type.loc_type_code', ['XDK', 'ECO'])
            ->select([
                'zone.zone_id',
                'zone.zone_name',
                'zone.zone_max_count',
                DB::raw('COUNT(location.loc_id) AS total'),
                DB::raw('COUNT(DISTINCT IF(pallet.plt_id IS NOT NULL, location.loc_id, NULL)) AS used')
            ])
            ->groupBy('location.loc_zone_id')
            ->get()->toArray();
        if(count($zones) > 0){
            $critical = [];
            foreach($zones as &$zone){
                if(empty($zone['zone_max_count']) || $zone['zone_max_count'] == 0){
                    $zone['zone_max_count'] = self::DEFAULT_ZONE_MAX_COUNT;
                }
                if(empty($zone['total']) || $zone['total'] == 0){
                    $zone['per_full'] = 100;
                }else{
                    $zone['per_full'] = intval(($zone['used'] / $zone['total']) * 100);
                }

                if($zone['per_full'] >= intval($zone['zone_max_count'])){
                    $critical[] = [
                        'zone_id'   => $zone['zone_id'],
                        'zone_name' => $zone['zone_name'],
                        'per_full'  => $zone['per_full']
                    ];
                }
            }

            if(count($critical) > 0){
                usort($critical, function($a, $b)
                {
                    return $a['per_full'] <= $b['per_full'];
                });
                $capacity['zone_id'] = $critical[0]['zone_id'];
                $capacity['zone_name'] = $critical[0]['zone_name'];
                $capacity['per_full']  = $critical[0]['per_full'];
                $capacity['details']   = $critical;
            }
        }

        return $capacity;
    }

    public function loadZoneByCustomerForWarehouseLayout($cusId)
    {
        $models = DB::table('zone')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'zone.zone_id')
            ->where('customer_zone.cus_id', $cusId)
            ->where('customer_zone.deleted', 0)
            ->where('customer_zone.deleted_at', 915148800)
            ->where('zone.deleted', 0)
            ->where('zone.deleted_at', 915148800)
            ->select('zone.*')
            ->get();

        return $models;
    }
}