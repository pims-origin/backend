<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

/**
 * Class ReturnHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class ReturnHdrValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'odr_hdr_id' => 'required|integer|exists:odr_hdr,odr_id|exists:odr_cartons,odr_hdr_id',
        ];
    }


}
