<?php

/*
 * This file is part of the MultiShip package.
 *
 * (c) 2013 Fraser Reed
 *      <fraser.reed@gmail.com>
 *      github.com/fraserreed
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MultiShip\Label;


/**
 * MultiShip shipment label object
 *
 * @author fraserreed
 */
class ShipmentLabel
{
    /**
     * @var string
     */
    protected $imageFormat;

    /**
     * @var string
     */
    protected $imageDescription;

    /**
     * @var string
     */
    protected $graphicImage;

    /**
     * @var string
     */
    protected $htmlImage;

    /**
     * @var string
     */
    protected $pdfImage;

    /**
     * @param string $imageFormat
     */
    public function setImageFormat($imageFormat)
    {
        $this->imageFormat = $imageFormat;
    }

    /**
     * @return string
     */
    public function getImageFormat()
    {
        return $this->imageFormat;
    }

    /**
     * @param string $imageDescription
     */
    public function setImageDescription($imageDescription)
    {
        $this->imageDescription = $imageDescription;
    }

    /**
     * @return string
     */
    public function getImageDescription()
    {
        return $this->imageDescription;
    }

    /**
     * @param string $graphicImage
     */
    public function setGraphicImage($graphicImage)
    {
        $this->graphicImage = $graphicImage;
    }

    /**
     * @return string
     */
    public function getGraphicImage()
    {
        return $this->graphicImage;
    }

    /**
     * @param string $htmlImage
     */
    public function setHtmlImage($htmlImage)
    {
        $this->htmlImage = $htmlImage;
    }

    /**
     * @return string
     */
    public function getHtmlImage()
    {
        return $this->htmlImage;
    }

    /**
     * @param string $pdfImage
     */
    public function setPdfImage($pdfImage)
    {
        $this->pdfImage = $pdfImage;
    }

    /**
     * @return string
     */
    public function getPdfImage()
    {
        return $this->pdfImage;
    }

    public function saveImage($path, $trackingNumber = null)
    {
        $imgVar = strtolower($this->imageFormat) . 'Image';

        try {
            if ($imgVar === 'gifImage') {
                $binary = base64_decode($this->graphicImage);
                $fp = fopen($path . ".gif", 'wb');
                fwrite($fp, $binary);
                fclose($fp);
                $binary = null;

                $mpdf = new \Mpdf\Mpdf();
                $file = $path . ".gif";
                $size = getimagesize($file);
                $width = $size[0];
                $height = $size[1];
                $mpdf->WriteHTML('');
                $mpdf->Image($file, 60, 50, $width, $height, 'jpg', '', true, true);
                $mpdf->Output($path);

                return;
            }


            $fp = fopen($path, 'wb');
            fwrite($fp, $this->$imgVar);
            fclose($fp);

            //Add custom barcode tracking number
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->SetImportUse();
            $pagecount = $mpdf->SetSourceFile($path);

            // Import the last page of the source PDF file
            $tplId = $mpdf->ImportPage($pagecount);
            $mpdf->UseTemplate($tplId);

            $mpdf->WriteHTML('<div style="position: fixed; rotate: 90; text-align: center">' . $trackingNumber); //
            $mpdf->WriteHTML('<br /><barcode height="1.5" size="1.7" code="' . $trackingNumber . '" type="C128C"/>');
            $mpdf->WriteHTML('</div>');

            $mpdf->Output($path, 'F');
        } catch (\Exception $e) {
            throw $e;
        }

    }
}
