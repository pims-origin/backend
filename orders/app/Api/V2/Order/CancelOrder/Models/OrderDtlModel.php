<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\Order\CancelOrder\Models;

use DB;
use HttpException;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

/**
 * Class OrderDtlModel
 *
 * @package App\Api\V1\Models
 */
class OrderDtlModel extends AbstractModel
{
    /**
     * @param $odrDtlIds
     *
     * @return mixed
     */
    public function deleteDtls($odrDtlIds)
    {
        return $this->model
            ->whereIn('odr_dtl_id', $odrDtlIds)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->where('ord_hdr_id', $attributes['ord_hdr_id']);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrLike = ['color', 'sku', 'size', 'lot'];
        $arrEqual = [
            'ord_hdr_id',
            'whs_id',
            'cus_id',
            'item_id',
            'uom_id',
            'request_qty',
            'qty_to_allocated',
            'allocated_qty',
            'damaged',
            'special_handling',
            'back_ord'
        ];
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }
        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $orderHdrId
     * @param $itemIds
     * @param array $with
     *
     * @return mixed
     */
    public function loadByOrdIds($orderHdrIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('odr_id', $orderHdrIds);
        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }

    /**
     * @param array $columns
     * @param string $where
     * @param array $params
     * @param bool|false $upperCase
     *
     * @return array
     */
    public function fetchColumn($columns = [], $where = "", $params = [], $upperCase = false)
    {
        $tblName = $this->getTable();
        $colNames = implode(', ', $columns);

        $query = "
            SELECT $colNames
            FROM $tblName
        ";
        if (!empty($where)) {
            $query .= " WHERE $where";
        }

        $result = [];

        $db = $this->model->getConnection()->getPdo();

        $statement = $db->prepare($query);
        $statement->execute($params);

        $colKey = $columns;
        end($columns);
        $endKey = key($columns);
        unset($colKey[$endKey]);

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            end($row);
            $end = key($row);
            $val = $row[$columns[$endKey]];
            unset($row[$end]);
            $key = implode("-", $row);

            if ($upperCase) {
                $key = strtoupper($key);
            }

            $result[$key] = $val;
        }

        return $result;
    }

    /**
     * Process revert data for cancel order
     *
     * @param int $odrId
     * @param bool $isNew
     */
    public function revertOrder($odrObj, $isNew)
    {
        $odrId = $odrObj->odr_id;
        $odrType = $odrObj->odr_type;
        $whsId = $odrObj->whs_id;

        $odrStsNotUpdate = [
            Status::getByValue('Picking'        , 'Order-Status'),
            Status::getByValue('Picked'         , 'Order-Status'),
            Status::getByValue('Partial Picked' , 'Order-Status'),
            Status::getByValue('Packing'        , 'Order-Status'),
            Status::getByValue('Partial Packing', 'Order-Status'),
            Status::getByValue('Packed'         , 'Order-Status'),
            Status::getByValue('Partial Packed' , 'Order-Status'),
            Status::getByValue('Staging'        , 'Order-Status'),
            Status::getByValue('Partial Staging', 'Order-Status'),
            Status::getByValue('Shipped'        , 'Order-Status'),
            Status::getByValue('Partial Shipped', 'Order-Status'),
            Status::getByValue('Hold'           , 'Order-Status')
        ];

        if (in_array($odrObj->odr_sts, $odrStsNotUpdate)) {
            return true;
        }

        // WMS2-4404 When order hasn't been picked, return Order's allocated to Inventory summary 's allocated. In others case, will return when wavepick update or cartons consolidate.
        $invSumrModel = new InventorySummaryModel();
        $return = $invSumrModel->updateAllocatedOrderInventory($whsId, $odrId);

        return $return;
    }

    /**
     * @param $orderIds
     * count Origin or back order
     *
     * @return mixed
     */
    public function countSku($orderIds)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->whereIn('odr_id', $orderIds)
            ->count(['item_id']);

        return $skuCount;
    }

    public function countOrderSku($orderId)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->where('odr_id', $orderId)
            ->count(['item_id']);

        return $skuCount;
    }

    public function getInvSumByOdrDtl($where, $lot, $algorithm, $packSize)
    {


        if (!$this->isLotAny($lot)) {
            $where['lot'] = $lot;
        }
        $query = DB::table('cartons')->where($where)->where('piece_remain', '>', 0)->where('ctn_sts', 'AC')->where('ctn_pack_size', $packSize);
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        };


        $items = $query->groupBy('item_id', 'lot')->select('lot')->get();
        $lots= array_pluck($items, 'lot');

        $invItems = InventorySummary::where($where)
            ->where('avail', '>', 0)
            ->get();

        $sortInvSums = [];
        foreach ($lots as $lot){
            foreach ($invItems as $key=>$value) {
                if($lot == $value->lot){
                    $sortInvSums[] = $value;
                    unset($invItems[$key]);
                    break;
                }
            }
        }

        return $sortInvSums;
    }

    public function isLotAny($lot)
    {
        $lot = strtoupper($lot);
        if ($lot === 'ANY') {
            return true;
        }

        return false;
    }

    public function updateOrdDtl($odrHdr, $odrDtls, $isPartial, $algorithm)
    {
        $where = [
            'whs_id' => $odrHdr->whs_id,
            'cus_id' => $odrHdr->cus_id,
        ];

        $backOrdDlts = [];
        $dtlCount = 0;

        foreach ($odrDtls as $odrDtl) {
            $where['item_id'] = $odrDtl->item_id;

            //select inv_sum by item_id
            $invItems = $this->getInvSumByOdrDtl($where, $odrDtl['lot'], $algorithm, $odrDtl->pack);

            $allQty = $reqQty = $odrDtl['piece_qty'];
            $availQty = 0;
            $isLast = count($invItems) === 1;
            $lotIndex = 0;
            foreach ($invItems as $invItem) {
                $dtlCount++;
                $lotIndex++;
                if ($lotIndex == count($invItems)) {
                    $isLast = true;
                }
                if ($invItem->avail < $allQty) {
                    $availQty += $invItem->avail;
                    $this->createNewOdrDtl($invItem, $odrDtl, $invItem->avail, $isLast);
                    $allQty = $this->updateInvSum($invItem, $allQty);

                } else {
                    $this->updateInvSum($invItem, $allQty);
                    $this->createNewOdrDtl($invItem, $odrDtl, $allQty, $isLast);
                    $allQty = 0;
                    break;
                }
            }


            //Check backorder
            if ($allQty > 0) {
                if ($isPartial === false) {
                    /**
                     * Throw error can not allocate this order
                     */
                    $msg = sprintf("Order %s does not support partial allocation! Please edit the requested quantity!",
                        $odrHdr->odr_num);
                    throw new \Exception($msg);
                } else {

                    /**
                     * Do backorder
                     */
                    $odrDtl->back_odr_qty = $allQty;
                    $odrDtl->back_odr = 1;
                    $odrDtl->save();
                    $backOrdDlts[] = $odrDtl;
                    $odrHdr->back_odr = 1;
                }
            }

            if ($this->isLotAny($odrDtl->lot)) {
                $odrDtl->delete();
            }


        }

        // Insert Event Tracking
        $evtTracking = new EventTrackingModel(new EventTracking());
        if ($odrHdr->back_odr) {
            $eventData = [
                'whs_id'    => $odrHdr['whs_id'],
                'cus_id'    => $odrHdr['cus_id'],
                'owner'     => $odrHdr['odr_num'],
                'evt_code'  => Status::getByKey("event", "PARTIAL-ALLOCATED"),
                'trans_num' => $odrHdr['odr_num'],
                'info'      => sprintf(Status::getByKey("event-info", "PAL"), $odrHdr['odr_num'])
            ];
        } else {
            $eventData = [
                'whs_id'    => object_get($odrHdr, 'whs_id', ''),
                'cus_id'    => object_get($odrHdr, 'cus_id', ''),
                'owner'     => object_get($odrHdr, 'odr_num', ''),
                'evt_code'  => config('constants.event.ALLOCATED-ORD'),
                'trans_num' => object_get($odrHdr, 'odr_num', ''),
                'info'      => sprintf(config('constants.event-info.ALLOCATED-ORD'), $odrHdr['odr_num'])
            ];
        }
        $evtTracking->create($eventData);

        $odrHdr->sku_ttl = $dtlCount;
        //Update odr_sts = allocated
        $sts = Status::getByValue('Allocated', 'ORDER-STATUS');
        //if($odrHdr->back_odr || $odrHdr->org_odr_id ){
        //    $sts = Status::getByValue('Partial Allocated', 'ORDER-STATUS');
        //}
        $odrHdr->odr_sts = $sts;
        $odrHdr->save();

        return $backOrdDlts;

    }

    private function createNewOdrDtl($invSum, $odrDtl, $allQty, $isOne = false)
    {
        $newOdrDlt = $odrDtl;
        $lot = $odrDtl->lot;
        if ($this->isLotAny($lot) && $isOne == false) {
            $newOdrDlt = $odrDtl->replicate();
        }

        $newOdrDlt->lot = $invSum->lot;
        $newOdrDlt->alloc_qty = $allQty;
        $newOdrDlt->qty = $newOdrDlt->pack ? ceil($newOdrDlt->alloc_qty / $newOdrDlt->pack) : 0;

        if ($this->isLotAny($lot) && $isOne == false) {
            return $newOdrDlt->push();
        } else {
            //$odrDtl->piece_qty = $piece_qty;
            return $newOdrDlt->save();
        }
    }


    private function updateInvSum($invSum, $allQty)
    {
        if ($invSum->avail < $allQty) {
            $allQty = $allQty - $invSum->avail;
            $invSum->allocated_qty = $invSum->allocated_qty + $invSum->avail;
            $invSum->avail = 0;

        } else {
            $invSum->allocated_qty = $invSum->allocated_qty + $allQty;
            $invSum->avail = $invSum->avail - $allQty;
            $allQty = 0;
        }
        $invSum->save();

        return $allQty;
    }

}
