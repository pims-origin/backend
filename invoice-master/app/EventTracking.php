<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;

class EventTracking extends BaseInvoiceModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evt_tracking';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'evt_code',
        'owner',
        'trans_num',
        'info',
        'whs_id',
        'cus_id'
    ];

    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }

    public function setUpdatedByAttribute($value)
    {
        // to Disable updated_by
    }

    public function setDeletedAtAttribute($value)
    {
        // to Disable deleted_at
    }

    public function setDeletedAttribute($value)
    {
        // to Disable deleted
    }
}
