<?php

namespace App\Api\V2\WavePick\Models;

use Seldat\Wms2\Models\OrderCarton;
use Illuminate\Support\Facades\DB;

class OrderCartonModel extends AbstractModel
{

    protected $model;

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    /**
     * @param $wvId
     */
    public function getDataAfterPicked($wvId)
    {
        return $this->model
            ->select([
                'odr_cartons.*',
                'pallet.plt_num',
                'pallet.init_ctn_ttl',
                'item.description',
                DB::raw("CONCAT(users.first_name, ' ', users.last_name) AS picked_by"),
                DB::raw("SUM(odr_cartons.piece_qty) as s_piece_qty"),
                DB::raw("count(DISTINCT odr_cartons.plt_id) as countPlt")
            ])
            ->where('odr_cartons.wv_hdr_id', $wvId)
            ->where('odr_cartons.deleted', 0)
            ->leftJoin('item', 'item.item_id', '=', 'odr_cartons.item_id')
            ->leftJoin('pallet', 'pallet.plt_id', '=', 'odr_cartons.plt_id')
            ->join('users', 'users.user_id', '=', 'odr_cartons.created_by')
            ->groupBy('odr_cartons.wv_dtl_id', 'odr_cartons.loc_id')
            ->get();
    }
}
