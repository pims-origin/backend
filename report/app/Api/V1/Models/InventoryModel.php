<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Inventory;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;

class InventoryModel extends AbstractModel
{
    public function __construct(Inventory $model = NULL)
    {
        $this->model = ($model) ?: new Inventory();
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with)
            ->where('cartons.whs_id', $attributes['whs_id']);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $attributes['compare_operator']['ctnr_num'] = 'like';
        $attributes['compare_operator']['ctn_num'] = 'like';
        $attributes['compare_operator']['loc_code'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'cus_id',
                'whs_id',
                'ctnr_num',
                'ctn_num',
                'loc_code',
            ],
            'attributes' => $attributes,
        ]);

        return $this->runSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

    /*
    ***************************************************************************
    */

    /**
     * @param $whs_id
     * @param array $attributes
     *
     * @return mixed
     */

    public function customers($whs_id, $attributes = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        // Search whs_id
        $query = DB::table('customer AS c')
            ->select(['c.cus_id', 'c.cus_name'])
            ->join('invt_smr', 'c.cus_id', '=', 'invt_smr.cus_id')
            ->where('invt_smr.whs_id', $whs_id);

        $result = $query->groupBy('c.cus_id')->orderBy('c.cus_name')->get();

        return $result;
    }


    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */

    public function skuInvAutoComplete($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('item')
            ->leftJoin('invt_smr as inv', function ($join) {
                $join->on('inv.item_id', '=', 'item.item_id');
                $join->on('inv.cus_id', '=', 'item.cus_id');
            })
//            ->select(['inv.sku', 'inv.size', 'inv.color', 'inv.lot', 'inv.item_id'])
            ->select(DB::Raw("item.sku, item.size, item.color, item.item_id, IF(inv.lot IS NULL, 'NA', inv.lot) as lot"))
            ->where('item.status', 'AC');

//        $check_inv = DB::table('item')
//            ->join('invt_smr as inv', function ($join) {
//                $join->on('inv.item_id', '=', 'item.item_id');
//                $join->on('inv.cus_id', '=', 'item.cus_id');
//            })
//            ->where('item.sku', 'LIKE', "%{$attributes['sku']}%")
//            ->where('inv.whs_id', $whs_id)
//            ->first();
//
//        if (!empty($check_inv)) {
//            $query->where('inv.whs_id', $whs_id);
//        }

        if (isset($attributes['cus_id'])) {
            $query->where('item.cus_id', $attributes['cus_id']);
        }

        // Search sku
        if (isset($attributes['sku'])) {
            $query->where('item.sku', 'LIKE', "%{$attributes['sku']}%");
        }

        $result = $query->groupBy('item.item_id', 'inv.lot')
            ->orderBy('item.sku')->take($limit)->get();

        return $result;
    }

    public function getDataExportForInventory()
    {
        $query = $this->model
            ->select([
                'cartons.whs_id',
                'warehouse.whs_name',
                'customer.cus_name',
                'cartons.sku',
                'item.description',
                DB::raw('SUM(cartons.piece_remain) AS piece_remain'),
                'cartons.loc_code'
            ])
            ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->join('warehouse', 'warehouse.whs_id', '=', 'cartons.whs_id')
            ->whereIn('cartons.ctn_sts', ['AC', 'LK', 'RG'])
            ->where('cartons.deleted', 0)
        ;
        $query = $query->groupBy([
            'cartons.cus_id',
            'cartons.item_id',
            'cartons.whs_id',
            'cartons.loc_id',
        ])->get()->toArray();
        $result = array();
        foreach ($query as $element) {
            $result[$element['whs_name']][] = $element;
        }
        return $result;
    }

}
