<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 26-May-16
 * Time: 09:34
 */

namespace Seldat\Wms2\Models;

/**
 * Class Refill
 *
 * @package Seldat\Wms2\Models
 */
class Refill extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'refill';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'rf_id';


    /**
     * @var array
     */
    protected $fillable = [
        'loc_id',
        'loc_code',
        'item_id',
        'cus_id',
        'sku',
        'color',
        'size',
        'upc',
        'lot',
        'min',
        'max'
    ];

}
