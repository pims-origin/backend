<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 18-Aug-16
 * Time: 16:38
 */

namespace Seldat\Wms2\Utils;

use Maatwebsite\Excel\Facades\Excel;
use Box\Spout\Writer\WriterFactory;

class SelExport
{
    /**
     * @var
     */
    protected static $_data;

    /**
     * @param $data
     * @param $title
     *
     * @return bool
     */
    public static function setDataList($data, $title)
    {
        if (empty($title) || !is_array($title)) {
            self::$_data = [];

            return false;
        }

        self::$_data[] = array_keys($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $value) {
                $temp[] = implode(" ", array_map(function ($e) use ($item) {
                    return object_get($item, $e, null);
                }, $values = explode("|", $value)));
            }
            self::$_data[] = $temp;
        }

        return true;
    }

    /**
     * Export Data from Array to File
     *
     * @param string $fileName
     * @param array $data
     * @param string $type
     *
     * @return bool
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    public static function exportList($fileName, $type = 'csv', $data = [])
    {
        if (!empty($data) && is_array($data)) {
            self::$_data = $data;
        }
        if (!in_array($type, ['csv', 'xls', 'xlsx', 'ods', 'pdf'])) {
            return false;
        }

        if (in_array($type, ['csv', 'xls', 'xlsx', 'ods'])) {
            $writer = WriterFactory::create($type);
            $writer->openToBrowser("$fileName.$type");
            $writer->addRows(self::$_data);
            $writer->close();
        }

        if ($type == 'pdf') {
            $width = ['landscape' => 230, 'portrait' => 160];
            $orientation = "portrait";
            $total = !empty(self::$_data[0]) ? count(self::$_data[0]) : 0;
            if ($total > 10) {
                $orientation = "landscape";
            }

            $width = $width[$orientation] / $total;

            Excel::create($fileName,
                function ($reader) use ($orientation, $width, $total) {
                    $reader->sheet('Location List', function ($sheet) use ($orientation, $width, $total) {
                        $sheet->setOrientation($orientation);
                        $sheet->setPageMargin(0.25);

                        // Array Column from A --> CZ
                        $columns = self::columns("CZ");
                        $col = 0;
                        while ($col < $total) {
                            $sheet->setWidth($columns[$col], $width);
                            $col++;
                        }

                        $sheet->setFontSize(7);

                        foreach (self::$_data as $line => $row) {
                            $sheet->row($line + 1, $row);
                        }
                    });
                })->download('pdf');
        }

        return true;
    }

    /**
     * @param $endColumn
     * @param string $first
     *
     * @return array
     */
    private static function columns($endColumn, $first = "")
    {
        $columns = [];
        $length = strlen($endColumn);
        $letters = range('A', 'Z');

        // Iterate over 26 letters.
        foreach ($letters as $letter) {
            // Paste the $first_letters before the next.
            $column = $first . $letter;

            // Add the column to the final array.
            $columns[] = $column;

            // If it was the end column that was added, return the columns.
            if ($column == $endColumn) {
                return $columns;
            }
        }

        // Add the column children.
        foreach ($columns as $column) {
            // Don't itterate if the $end_column was already set in a previous itteration.
            // Stop iterating if you've reached the maximum character length.
            if (!in_array($endColumn, $columns) && strlen($column) < $length) {
                $new_columns = self::columns($endColumn, $column);
                // Merge the new columns which were created with the final columns array.
                $columns = array_merge($columns, $new_columns);
            }
        }

        return $columns;
    }
}
