<?php

namespace App\Api\V2\BillOfLadings\MasterPalletList\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Utils\Status;

/**
 * Class MasterPalletTransformer
 *
 * @package App\Api\V2\BillOfLadings\MasterPalletList\Transformers
 */
class MasterPalletTransformer extends TransformerAbstract
{
    /**
     * @param OutPallet $outPallet
     *
     * @return array
     */
    public function transform(OutPallet $outPallet)
    {
        $shipSts     = object_get($outPallet, 'ship_sts');
        $mixedOrder  = object_get($outPallet, 'mixed_order', 0) > 1 ? "Yes" : "No";
        $shipStsName = Status::getByKey("Ship-Status", $shipSts);
        $odrSts      = object_get($outPallet, 'odr_sts');
        if ($odrSts == 'SH') {
            $shipStsName = Status::getByKey("ORDER-STATUS", $odrSts);
        }

        return [
            'plt_id'        => object_get($outPallet, 'plt_id'),
            'plt_num'       => object_get($outPallet, 'plt_num'),
            'mixed_order'   => $mixedOrder,
            'loc_code'      => object_get($outPallet, 'loc_code'),
            'loc_name'      => object_get($outPallet, 'loc_name'),
            'ctns'          => object_get($outPallet, 'ctn_ttl'),
            'ship_sts'      => $shipSts,
            'ship_sts_name' => $shipStsName,
        ];
    }
}
