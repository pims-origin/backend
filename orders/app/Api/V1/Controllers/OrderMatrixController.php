<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 1/29/2019
 * Time: 1:56 PM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\ShippingOrderModel;
use Illuminate\Http\Request as IRequest;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Helper;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;


class OrderMatrixController extends AbstractController
{
    protected $itemModel;

    protected $orderModel;

    protected $orderDtlModel;

    protected $shippingOrderModel;

    public function __construct(
        ItemModel $itemModel,
        OrderHdrModel $orderHdrModel,
        ShippingOrderModel $shippingOrderModel,
        OrderDtlModel $orderDtlModel
    )
    {
        $this->itemModel = $itemModel;
        $this->orderModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->shippingOrderModel = $shippingOrderModel;
    }

    public function OrderMatrixKit(Request $request) {
        try {

            $input = $request->getParsedBody();

            $this->validateMatrixKit($input);

            DB::beginTransaction();

            $customer = DB::table('customer')->where('cus_id', $input['cus_id'])->first();
            if(empty($customer)) {
                $this->response->errorBadRequest("This customer is not exit");
            }

            $skuHeader = $customer['cus_code'] .'-'. date('ym') . '-1';
            $skuLatest = $this->itemModel->getModel()->where("cus_id",$input['cus_id'])
                ->where("sku","LIKE",$skuHeader."%")->orderBy("sku", "DESC")->first();
            $newSku = $this->generateNextCode($skuHeader, $skuLatest);

            $cusHeader =  $customer['cus_code'] .'-'. date('ym').'-';
            $cus_odr_num = $this->orderModel->getModel()->where("cus_odr_num","LIKE", $cusHeader)
                ->orderBy("cus_odr_num","DESC")->first();
            $newCusOdr = $this->generateNextCode($cusHeader, $cus_odr_num);

            $result  = [];
            $tes = [];
            $uom = DB::table("system_uom")->where("sys_uom_code", "MC")->where("deleted", 0)->first();
            if(empty($uom)) {
                $this->response->errorBadRequest("Plz create system uom code Master Carton(MC)");
            }

            foreach ($input['items'] as $key => $value) {

                // Create Item Kitting
                $item = $this->createMasterItem($newSku,$uom,$input['cus_id']);
                $newSku = $this->generateNextCode($skuHeader, $newSku);
                $tes[] = $item->item_id;
                // Create Child Item
                $this->createChildItem($item, $value['inner_items']);

                $inputParams = [
                    "whs_id"            => $input['whs_id'],
                    "cus_id"            => $input['cus_id'],
                    "item"              => $item,
                    "cus_odr_num"       => $newCusOdr,
                    "need_qty"          => $value['need_qty']
                ];
                $newCusOdr = $this->generateNextCode($cusHeader, $cus_odr_num);

                $orderHdr = $this->createOrderKiting($inputParams);

                $arr_save = [
                    "item"          => $item,
                    "order"         => $orderHdr,
                ];

                $result[] = $arr_save;
            }
            DB::commit();

            return $result;

        } catch
        (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function validateMatrixKit(&$attribute) {
        if(empty($attribute['items'])) {
            return $this->response->errorBadRequest("Items is required!");
        }

        if(empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest("Cus id is required!");
        }

        if(empty($attribute['whs_id'])) {
            return $this->response->errorBadRequest("Whs id is required!");
        }

        foreach ($attribute['items'] as $key => $item) {
            if(empty($item['inner_items'])) {
                return $this->response->errorBadRequest("Missing inner_item!");
            }
        }

        if( empty($attribute['type']) || strtoupper($attribute['type']) != 'KIT')

        return 0;
    }

    protected function generateNextCode($prefix, $latestString)
    {
        if(!empty($latestString)) {
            $n = str_replace($prefix, '', $latestString);

            try {
                $newN = str_pad(++$n, 6, 0, STR_PAD_LEFT);
            } catch (Exception $e) {
                $newN = str_pad(1, 6, 0, STR_PAD_LEFT);
            }

        }else{
            $newN = str_pad(1, 6, 0, STR_PAD_LEFT);
        }

        return $prefix.$newN;
    }

    protected function createMasterItem($newSku, $uom, $cus_id) {
        $params = [
            'description' => 'Master Item create at'.date("Y-m-d H:i:s"),
            'size'        => 'NA',
            'color'       => 'NA',
            'sku'         => $newSku,
            'cus_upc'     => '',
            'uom_id'      => $uom['sys_uom_id'],
            'pack'        => 1,
            'length'      => floatval(1),
            'width'       => floatval(1),
            'height'      => floatval(1),
            'weight'      => floatval(1),
            'cus_id'      => intval($cus_id),
            'status'      => 'AC',
            'condition'   => '',
            'item_hier'   => '',
            'charge_by'   => null,
            'spc_hdl_code'=> 'RAC',
        ];

        $params['uom_name'] = $uom['sys_uom_name'];
        $params['uom_code'] = $uom['sys_uom_code'];
        $params['level_id'] = $uom['level_id'];

        // calculate volume & cube
        $params['volume'] = Helper::calculateVolume($params['length'], $params['width'], $params['height']);
        $params['cube'] = Helper::calculateCube($params['length'], $params['width'], $params['height']);
        $this->itemModel->refreshModel();
        return $this->itemModel->create($params);
    }

    protected function createChildItem($itemParent, $childList) {
        $type = (count($childList) > 1)?'MIX' : 'SGL';
        $params = [];
        $pack = 0;
        $check = DB::table("item_child")->where("parent",$itemParent->item_id)->delete();
        foreach ($childList as $child) {

            $childItem = $this->itemModel->getFirstWhere(['item_id' => $child['item_id']]);
            if(empty($childItem)) {
                $msg = sprintf("Sku %s is not exit!", $child['sku']);
                return $this->response->errorBadRequest($msg);
            }

            $pack += $child['pack'];
            $itemParams = [
                "parent"            => $itemParent->item_id,
                "child"             => $child['item_id'],
                "type"              => $type,
                "origin"            => $child['item_id'],
                "pack"              => $child['pack']
            ];
            $params[] = $itemParams;
        }
        $itemParent->pack = $pack;
        $itemParent->save();
        DB::table("item_child")->insert($params);
        return 0;
    }

    protected function createOrderKiting($inputParams) {
        $params = [
            'carrier'          => 'NA',
            'ship_to_name'     => 'NA',
            'ship_to_add_1'    => 'NA',
            'ship_to_add_2'    => 'NA',
            'ship_to_city'     => 'NA',
            'ship_to_country'  => 'NA',
            'ship_to_state'    => 'NA',
            'ship_to_zip'      => 'NA',
            'ship_by_dt'       => 0,
            'cancel_by_dt'     => 0,
            'req_cmpl_dt'      => 0,
            'act_cmpl_dt'      => 0,
            'act_cancel_dt'    => 0,
            'in_notes'         => null,
            'cus_notes'        => null,
            'cus_id'           => $inputParams['cus_id'],
            'whs_id'           => $inputParams['whs_id'],
            'cus_odr_num'      => $inputParams['cus_odr_num'],
            'cus_po'           => $inputParams['cus_odr_num'],
            'ref_cod'          => $inputParams['cus_odr_num'],
            'odr_type'         => 'KIT',
            'rush_odr'         => null,
            'odr_sts'          => Status::getByValue("New", "Order-Status"),
            'sku_ttl'          => 1,
        ];

        $params['odr_num'] = $this->orderModel->getOrderNum();
        $params['so_id'] = $this->getShippingOdr($inputParams);
        $this->orderModel->refreshModel();
        $orderHdr = $this->orderModel->create($params);

        $detail = [
            'whs_id'        => $inputParams['whs_id'],
            'cus_id'        => $inputParams['cus_id'],
            'odr_id'        => $orderHdr->odr_id,
            'item_id'       => $inputParams['item']->item_id,
            'pack'          => $inputParams['item']->pack,
            'uom_id'        => $inputParams['item']->uom_id,
            'uom_code'      => $inputParams['item']->uom_code,
            'qty'           => $inputParams['need_qty'],
            'piece_qty'     => $inputParams['need_qty'],
            'sts'           => "i",
            'is_kitting'    => 1,
            'lot'           => 'NA',
        ];

        $this->orderDtlModel->refreshModel();
        $this->orderDtlModel->create($detail);
        return $orderHdr;
    }

    /**
     * @param $input
     * @param bool $isUpdate
     * @param $oldRef
     *
     * @return mixed
     */
    private function getShippingOdr($params) {
        $this->shippingOrderModel->refreshModel();

        $shipParams = [
            'cus_id'      => $params['cus_id'],
            'whs_id'      => $params['whs_id'],
            'cus_odr_num' => $params['cus_odr_num'],
            'so_sts'      => Status::getByValue("New", "Order-Status"),
            'type'        => 'KIT',
        ];

        // Create Shipping Order.
        $so = $this->shippingOrderModel->getFirstWhere([
            'cus_odr_num' => $shipParams['cus_odr_num'],
            'whs_id'      => $shipParams['whs_id'],
            'cus_id'      => $shipParams['cus_id']
        ]);

        if (empty($so)) {
            // Create Shipping Odr
            $shipParams['po_total'] = 1;
            $soId = $this->shippingOrderModel->create($shipParams)->so_id;
        } else {
            // Update po_total
            $soId = $so->so_id;
        }

        return $soId;
    }
}