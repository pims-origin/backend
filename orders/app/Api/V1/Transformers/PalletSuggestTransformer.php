<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Pallet;
use League\Fractal\TransformerAbstract;

/**
 * Class PalletSuggestTransformer
 *
 * @package App\Api\V1\Transformers
 */
class PalletSuggestTransformer extends TransformerAbstract
{
    /**
     * @param Pallet $pallet
     *
     * @return array
     */
    public function transform(Pallet $pallet)
    {
        return [
            'plt_id'  => $pallet->plt_id,
            'plt_num' => $pallet->plt_num,
            'ctn_ttl' => $pallet->ctn_ttl,
        ];
    }
}
