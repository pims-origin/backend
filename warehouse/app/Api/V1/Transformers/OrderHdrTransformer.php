<?php
/**
 * Created by PhpStorm.
 *
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderHdr;
use App\Api\V1\Models\OrderHdrModel;
use Seldat\Wms2\Models\OrderDtl;
use App\Api\V1\Models\OrderDtlModel;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderHdrTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {//print_r($orderHdr);die();
        $act_cmpl_dt = ($orderHdr->act_cmpl_dt) ? date('m/d/Y', $orderHdr->act_cmpl_dt) : '';
        $req_cmpl_dt = ($orderHdr->req_cmpl_dt) ? date('m/d/Y', $orderHdr->req_cmpl_dt) : '';
        //$created_at = (OrderHdr.created_at) ? date('m/d/Y', $orderHdr->OrderHdr.created_at) : '';

        return [
            'odr_id'      => $orderHdr->odr_id,
            'odr_num'     => $orderHdr->odr_num,
            'cus_id'      => $orderHdr->cus_id,
            'cus_name'    => object_get($orderHdr, 'customer.cus_name', ''),
            'sku_ttl'     => OrderDtl::where('odr_id', $orderHdr->odr_id)->count('item_id'),
            'req_qty'     => OrderDtl::where('odr_id', $orderHdr->odr_id)->sum('alloc_qty'),
            'shipped_qty' => OrderCarton::where('odr_hdr_id', $orderHdr->odr_id)->where('sts', 'SH')->sum('piece_qty'),
            'act_cmpl_dt' => $act_cmpl_dt,
            'req_cmpl_dt' => $req_cmpl_dt,
            'updated_by'  => object_get($orderHdr, 'createdBy.first_name', '') . ' ' .
                object_get($orderHdr, 'createdBy.last_name', ''),
            //'created_at'  => $created_at,

        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }
}
