<?php

namespace App\Api\V1\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;

/**
 * Class AbstractController
 *
 * @package App\Api\V1\Controllers
 *
 */
abstract class AbstractController extends Controller
{
    use Helpers;

    public function __construct()
    {

    }
}