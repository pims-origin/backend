<?php

namespace App\Api\V1\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class BaseController
 * @package App\Api\V1\Controllers
 *
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="api.domain",
 *     basePath="/",
 *     definitions={},
 *     @SWG\Info(
 *         version="WMS 2.0.0",
 *         title="Your API Functional Name",
 *         description="Description for APIs",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="your@email.address"
 *         ),
 *         @SWG\License(
 *             name="WMS2.0",
 *             url="seldatinc.com"
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="data",
 *         type="object",
 *         properties={
 *             @SWG\Property(property="menu_group_id", type="integer"),
 *             @SWG\Property(property="name", type="string"),
 *             @SWG\Property(property="description", type="string"),
 *         },
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */
abstract class AbstractController extends Controller
{
    use Helpers;

    /**
     * @return Client
     */
    protected function getVerifyClient(){
        return new Client([
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . JWTAuth::getToken()->get(),
            ],
            RequestOptions::HTTP_ERRORS => false
        ]);
    }

    /**
     * @return mixed
     */
    protected function getUserCache(){
        $client = $this->getVerifyClient();
        $res = $client->get(env('API_GET_USER_CACHE'));
        return \GuzzleHttp\json_decode($res->getBody()->getContents(), true)['data'];
    }
}
