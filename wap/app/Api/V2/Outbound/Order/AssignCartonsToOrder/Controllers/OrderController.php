<?php

namespace App\Api\V2\Outbound\Order\AssignCartonsToOrder\Controllers;

use App\Api\V2\Outbound\Models\EventTrackingModel;
use App\Api\V2\Outbound\Models\OrderCartonModel;
use App\Api\V2\Outbound\Models\OrderDtlModel;
use App\Api\V2\Outbound\Models\OrderHdrModel;
use App\Api\V2\Outbound\Models\CartonModel;
use App\Api\V2\Outbound\Models\WavePickDtlModel;
use App\Api\V2\Outbound\Models\WavePickHdrModel;
use App\Api\V2\Outbound\Models\WaveDtlLocModel;
use App\libraries\RFIDValidate;

use App\Api\OUTBOUND\Transformers\OrderListShippingTransformer;
use App\Api\OUTBOUND\Transformers\OrderListTransformer;
use App\Api\OUTBOUND\Transformers\OrderDetailTransformer;
use App\Api\OUTBOUND\Transformers\OrderByOdrIDTransformer;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OrderController extends AbstractController
{

    protected $orderCartonModel;
    protected $orderDtlModel;
    protected $orderHdrModel;
    protected $cartonModel;
    protected $eventTrackingModel;
    protected $wavePickDtlModel;
    protected $wavePickHdrModel;
    protected $waveDtlLocModel;

    public function __construct()
    {
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->cartonModel = new CartonModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->wavePickDtlModel = new WavePickDtlModel();
        $this->wavePickHdrModel = new WavePickHdrModel();
        $this->waveDtlLocModel  = new WaveDtlLocModel();
    }

    /**
     * @param $whsId
     * @param $odrId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function putCartonOrder($whsId, $odrId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/order/{$odrId}/cartons";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'PCO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Assign carton to order'
        ]);

        /*
         * end logs
         */

        $input['whs_id'] = $whsId;
        $input['odr_id'] = $odrId;

        $ctns = array_get($input, 'ctns-rfid', null);
        if (empty($ctns)) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'] = [
                'status_code' => -1,
                'msg' => "Carton rfid is required"
            ];

            return $this->response->noContent()
                ->setContent(['data' => $msg])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        }

        if (!is_array($ctns)) {
            $ctns = [$ctns];
        }
        // Check duplicate cartons rfid
       $uniqueRFIDs =  array_unique($ctns);
       if(count($uniqueRFIDs) != count($ctns)){
           $duplicateRFID = array_unique(array_diff_assoc($ctns, $uniqueRFIDs));

           $msg = [];
           $msg['status'] = false;
           $msg['iat'] = time();
           $msg['data'] = [];
           $msg['message'] = [
               'status_code' => -7,
               'msg' => 'Duplicate Carton: ' . implode(', ', $duplicateRFID)
           ];

           return new Response($msg, 200, [], null);
       }

        //validate carton RFID
        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($ctns as $item) {

            $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
            if (! $ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $item;
                if('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'] = [
                'status_code' => -6,
                'msg' => $errorMsg
            ];

            return new Response($msg, 200, [], null);
        }

        $checkOdr = $this->orderHdrModel->getModel()
            ->where('whs_id', $whsId)
            ->where('odr_id', $odrId)
            // ->whereIn('odr_sts', ['NW', 'PK'])
            ->where('odr_sts', 'PK')
            ->first();

        if (empty($checkOdr)) {
            $errorMessage = sprintf("The order %s doesn't exist", $odrId);
            $checkOdr = $this->orderHdrModel->getFirstWhere([
                'whs_id' => $whsId,
                'odr_id' => $odrId,
            ]);

            if ($checkOdr) {
                $odrNum = object_get($checkOdr, 'odr_num');
                $odrSts = object_get($checkOdr, 'odr_sts');
                if ($odrSts == 'NW' || $odrSts == "AL") {
                    $errorMessage = sprintf("The order %s doesn't create wavepick yet", $odrNum);
                }
                if ($odrSts == 'PD') {
                    $errorMessage = sprintf("The order %s is picked", $odrNum);
                }
                if ($odrSts == 'PN') {
                    $errorMessage = sprintf("The order %s is packing", $odrNum);
                }
                if ($odrSts == 'PA') {
                    $errorMessage = sprintf("The order %s is packed", $odrNum);
                }
                if ($odrSts == 'ST') {
                    $errorMessage = sprintf("The order %s is staging", $odrNum);
                }
                if ($odrSts == 'SH') {
                    $errorMessage = sprintf("The order %s is shipped", $odrNum);
                }
                if ($odrSts == 'CC') {
                    $errorMessage = sprintf("The order %s is canceled", $odrNum);
                }
            } else {
                $checkOdr = $this->orderHdrModel->getFirstWhere([
                    'odr_id' => $odrId,
                ]);
                if ($checkOdr) {
                    $odrNum = object_get($checkOdr, 'odr_num');
                    $errorMessage = sprintf("The order %s doesn't belong to warehouse", $odrNum);
                }
            }

            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['message'] = [
                'status_code' => -3,
                'msg' => $errorMessage
            ];
            $msg['data'] = [];
            return $msg;
        }

        // check ctns exists on odr ctn table
        $ctnRFIDNotExisted = [];
        foreach ($ctns as $ctn) {
            $checkCtn = $this->orderCartonModel->getFirstWhere([
                'ctn_rfid'   => $ctn,
                'odr_hdr_id' => null
            ]);

            if (empty($checkCtn)) {
                $ctnRFIDNotExisted[] = $ctn;
            }
        }

        if ($ctnRFIDNotExisted) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();

            $ctnRFIDAssigned = [];
            foreach ($ctnRFIDNotExisted as $ctn) {
                $checkCtn = $this->orderCartonModel->getModel()
                                    ->where('ctn_rfid', $ctn)
                                    ->whereNotNull('odr_hdr_id')
                                    ->where('deleted', 0)
                                    ->first();

                if ($checkCtn) {
                    $ctnRFIDAssigned[$ctn] = object_get($checkCtn, 'odr_num');
                }
            }
            if (count($ctnRFIDAssigned) != 0) {
                $ctns = array_keys($ctnRFIDAssigned);
                $msg['message'] = [
                    'status_code' => -2,
                    'msg' => sprintf("The carton %s has been assigned to order %s", implode(', ', $ctns), implode(', ', $ctnRFIDAssigned)),
                ];
            } else {
                $msg['message'] = [
                    'status_code' => -1,
                    'msg' => "The carton hasn't been picked."
                ];

                $checkCarton = $this->cartonModel->getFirstwhere(['rfid' => array_first($ctnRFIDNotExisted)]);
                if (!$checkCarton) {
                    $msg['message'] = [
                        'status_code' => -1,
                        'msg' => "The carton doesn't exist."
                    ];
                }
            }
            $msg['data'] = [];
            return $msg;
        }

        try {

            $data = [];
            $actOrderCarton = $this->orderCartonModel->countOdrHdr($odrId);
            if(count($ctns) + $actOrderCarton > $this->orderDtlModel->getTotalCarton($odrId))
            {
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['message'] = [
                    'status_code' => -7,
                    'msg' => "Assigned Carton QTY is greater than the required carton QTY for this Order!"
                ];

                $msg['data'] = [];
                return $msg;
            }

            // start transaction
            DB::beginTransaction();

            $wvStsFrom = null;
            // update carton
            foreach ($ctns as $ctn) {
                // checking cross Cartons between Wave picks
                $ctnDt = $this->orderCartonModel->getFirstWhere([
                    'ctn_rfid' => $ctn
                ]);

                $odrDtl = $this->orderDtlModel->getFirstWhere([
                    'odr_id'  => $odrId,
                    'item_id' => $ctnDt->item_id,
                    'lot' => $ctnDt->lot,
                ]);

                if (empty($odrDtl)) {
                    DB::rollback();

                    $msg = [];
                    $msg['status'] = false;
                    $msg['iat'] = time();
                    $msg['data'] = [];
                    $msg['message'] = [
                        'status_code' => -5,
                        'msg' => sprintf("Carton RFID %s doesn't belong to Order %s!",
                            $ctn,
                            object_get($checkOdr, 'odr_num')),
                    ];

                    return $msg;
                }

                // in case cross cartons
                $wvNum = object_get($ctnDt, 'wv_num');

                $orderIds = $this->orderHdrModel->getOdrIdByWvNum($wvNum);

                if (!in_array($odrId, $orderIds)) {
                    $wvInfo = $this->_getWvInfoByOdrId($ctnDt, $checkOdr);
                    // cross cartons
                    $wvStsFrom = $this->_updateWvPickInCross($ctnDt, $wvInfo, $checkOdr);

                    // $this->_updateWvPickDtlLocInCross($ctnDt, $wvInfo);

                    $this->_updateOdrCartonInCrossCartonCase($ctnDt, $wvInfo);
                }

                $odrDtlId = $odrDtl->odr_dtl_id;

                $dataOdr = [
                    'odr_hdr_id' => $odrId,
                    'odr_num'    => $checkOdr->odr_num,
                    'odr_dtl_id' => $odrDtlId,
                    'ctn_sts'    => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                    'updated_at' => time()
                ];

                $this->orderCartonModel->updateWhere($dataOdr, ['ctn_rfid' => $ctn]);

                // WMS2-4435 - BUG - Able to assign cartons to order detail more than expected if order has more than one SKU
                $actPieceQtyOdrDtl = $this->orderCartonModel->sumPieceQtyOdrCtnByOdrDtlID($odrDtlId);
                if ($actPieceQtyOdrDtl > object_get($odrDtl, 'alloc_qty', 0)) {
                    DB::rollback();

                    $msg = [];
                    $msg['status'] = false;
                    $msg['iat'] = time();
                    $msg['data'] = [];
                    $msg['message'] = [
                        'status_code' => -5,
                        'msg' => sprintf("Assigned Carton QTY is greater than the required carton QTY for Order detail!"),
                    ];

                    return $msg;
                }
                //Update Picking for order details
                $odrDtl->itm_sts = 'PK';
                $odrDtl->save();
                $data = [
                    'result' => [
                        "item_id" => object_get($odrDtl, 'item_id'),
                        "sku"     => object_get($odrDtl, 'sku'),
                        "size"    => object_get($odrDtl, 'size'),
                        "color"   => object_get($odrDtl, 'color'),
                        "lot"     => object_get($odrDtl, 'lot'),
                        "sts"     => true,
                    ]
                ];
                break;
            }

            //Update Picking for order header
            $checkOdr->odr_sts = 'PK';
            $checkOdr->save();

            // WMS2-4636 - Update picked_qty of order detail
            $this->orderDtlModel->updateOrderDetailPickedQty($odrId);
            //update odr_sts to PD
            $dtlPicked = $this->orderDtlModel->updateOrderDetailPicked($odrId);
            if ($dtlPicked) {
                $this->eventTrackingModel->eventTracking([
                    'whs_id' => $whsId,
                    'cus_id' => $checkOdr->cus_id,
                    'owner' => $checkOdr->odr_num,
                    'evt_code' => 'WOD',
                    'trans_num' => $checkOdr->odr_num,
                    'info' => sprintf('Wap - Assigning Carton(s) to Order Details %s - %s - %s - %s Completed', $ctnDt->sku, $ctnDt->size, $ctnDt->color, $ctnDt->lot)
                ]);
            }

            $hdrPicked = $this->orderHdrModel->updateOrderPicked($odrId);
            if ($hdrPicked) {
                $this->eventTrackingModel->eventTracking([
                    'whs_id' => $whsId,
                    'cus_id' => $checkOdr->cus_id,
                    'owner' => $checkOdr->odr_num,
                    'evt_code' => 'WOH',
                    'trans_num' => $checkOdr->odr_num,
                    'info' => "Wap - Assigning Carton(s) to Order Completed"
                ]);
            }

            // update wave pick is picking when status is new
            $this->wavePickHdrModel->getModel()
                ->where('wv_id', $checkOdr->wv_id)
                ->where('wv_sts', 'NW')
                ->update(['wv_sts'=> 'PK']);

            // update wave pick completed
            $wvId = $checkOdr->wv_id;
            $wvSts = $this->wavePickHdrModel->updatePicked($wvId);
            // set status of both two wave pick header
            $dataWvSts[] = [
                'wv_id'  => $wvId,
                'picked' => $wvSts,
            ];
            if ($wvStsFrom) {
                $dataWvSts[] = $wvStsFrom;
            }

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['message'] = [
                'status_code' => 1,
                'msg' => sprintf("Successfully!")
            ];
            $data['wv_sts'] = $dataWvSts;
            $msg['data']   = $data;

            return $msg;

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return new Response($this->getResponseData(), 200, [], null);
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _updateWvPickInCross($odrCtnFrom, $wvInfo, $odrObj)
    {
        $wvHdrIdFrom = object_get($odrCtnFrom, 'wv_hdr_id');
        $wvHdrIdTo   = array_get($wvInfo, 'wv_hdr_id');

        $wvDtlIdFrom = object_get($odrCtnFrom, 'wv_dtl_id');
        $wvDtlIdTo   = array_get($wvInfo, 'wv_dtl_id');

        $wvPieceQtyTo = $wvPieceQtyFrom = object_get($odrCtnFrom, 'piece_qty');

        $wvDtlFrom = $this->wavePickDtlModel->getFirstWhere(['wv_dtl_id' => $wvDtlIdFrom]);
        $wvDtlTo   = $this->wavePickDtlModel->getFirstWhere(['wv_dtl_id' => $wvDtlIdTo]);

        if ($wvDtlFrom->act_piece_qty < $wvPieceQtyFrom) {
            $msg = "Wave pick actual piece QTY [from] is not enough!";
            throw new \Exception($msg);
        }

        if (object_get($wvDtlTo, 'pack_size') != object_get($odrCtnFrom, 'pack')) {
            $msg = sprintf(
                'The Carton RFID %s pack size %d is not same pack size %d of Order %s!',
                object_get($odrCtnFrom, 'ctn_rfid'),
                object_get($odrCtnFrom, 'pack'),
                object_get($wvDtlTo, 'pack_size'),
                object_get($odrObj, 'odr_num')
            );
            throw new \Exception($msg);
        }

        $userId = Data::getCurrentUserId();
        // wave pick [from]
        $wvDtlFrom->act_piece_qty -= $wvPieceQtyFrom;
        $wvDtlFrom->wv_dtl_sts = $wvDtlFrom->act_piece_qty >= $wvDtlFrom->piece_qty ? "PD" : "PK";
        $wvDtlFrom->updated_at = time();
        $wvDtlFrom->updated_by = $userId;

        $availChange     = 0;
        $allocatedChange = 0;
        // $remainingQty    = 0;
        // update wave pick [data]
        $wvDataArrFrom = json_decode($wvDtlFrom->data, true);
        if (!$wvDataArrFrom) {
            $wvDataArrFrom['avail']     = 0;
            $wvDataArrFrom['allocated'] = 0;
        }
        // over picking
        if ($wvDataArrFrom['avail'] > 0 || $wvDataArrFrom['allocated'] > 0) {
            // return available qty
            if ($wvDataArrFrom['avail'] > $wvPieceQtyFrom) {
                $availChange = $wvPieceQtyFrom;
            } elseif ($wvDataArrFrom['avail'] + $wvDataArrFrom['allocated']> $wvPieceQtyFrom) {
                $availChange     = $wvDataArrFrom['avail'];
                $allocatedChange = $wvPieceQtyFrom - $wvDataArrFrom['avail'];
            } else { // return over picking
                $allocatedChange = $wvDataArrFrom['allocated'];
                $availChange     = $wvDataArrFrom['avail'];
                // $remainingQty    = $wvPieceQtyFrom - ($allocatedChange + $availChange);
            }
        }

        $wvDataArrFrom['avail']     -= $availChange;
        $wvDataArrFrom['allocated'] -= $allocatedChange;
        $wvDtlFrom->data = json_encode($wvDataArrFrom);
        $wvDtlFrom->update();

        // wave pick [to]
        // over picking
        // update wave pick [data]
        $wvDataArrTo = json_decode($wvDtlTo->data, true);
        if (!$wvDataArrTo) {
            $wvDataArrTo['avail']     = 0;
            $wvDataArrTo['allocated'] = 0;
        }
        if ($wvDtlTo->act_piece_qty + $wvPieceQtyTo > $wvDtlTo->piece_qty) {
            // exist over picking
            if ($wvDataArrTo['avail'] > 0 || $wvDataArrTo['allocated'] > 0) {
                // $remainingQty == 0
                if ($availChange + $allocatedChange == $wvPieceQtyTo) {
                    $wvDataArrTo['avail']     += $availChange;
                    $wvDataArrTo['allocated'] += $allocatedChange;
                } else {
                    $wvDataArrTo['avail']     += $availChange;
                    $wvDataArrTo['allocated'] += $wvPieceQtyTo - $availChange;
                }
            } else { // a half over picking
                $overPicking = $wvDtlTo->act_piece_qty + $wvPieceQtyTo - $wvDtlTo->piece_qty;
                if ($overPicking > $availChange) {
                    $wvDataArrTo['avail']     = $availChange;
                    $wvDataArrTo['allocated'] = $overPicking - $allocatedChange;
                } else {
                    $wvDataArrTo['avail']     = $overPicking;
                    $wvDataArrTo['allocated'] = 0;
                }
            }
        }

        $wvDtlTo->data = json_encode($wvDataArrTo);
        $wvDtlTo->act_piece_qty += $wvPieceQtyTo;
        $wvDtlTo->wv_dtl_sts = $wvDtlTo->act_piece_qty >= $wvDtlTo->piece_qty ? "PD" : "PK";
        $wvDtlTo->updated_at = time();
        $wvDtlTo->updated_by = $userId;

        $wvDtlTo->save();

        //update wave header status
        $wvSts = $this->wavePickHdrModel->updatePicked($wvHdrIdFrom);

        return [
            'wv_id'  => $wvHdrIdFrom,
            'picked' => $wvSts,
        ];
    }

    private function _updateOdrCartonInCrossCartonCase($odrCtnFrom, $wvInfo)
    {
        $wvNumTo   = array_get($wvInfo, 'wv_num');
        $wvHdrIdTo = array_get($wvInfo, 'wv_hdr_id');
        $wvDtlIdTo = array_get($wvInfo, 'wv_dtl_id');

        $odrCtnFrom->wv_num    = $wvNumTo;
        $odrCtnFrom->wv_hdr_id = $wvHdrIdTo;
        $odrCtnFrom->wv_dtl_id = $wvDtlIdTo;
        $odrCtnFrom->updated_at = time();
        $odrCtnFrom->updated_by = Data::getCurrentUserId();
        $odrCtnFrom->save();
    }

    private function _getWvInfoByOdrId($odrCtnObj, $odrHdrObj)
    {
        $wvId   = object_get($odrHdrObj, 'wv_id');
        $wvNum  = object_get($odrHdrObj, 'wv_num');
        $itemId = object_get($odrCtnObj, 'item_id');

        $wvDtlObj = $this->wavePickDtlModel->getFirstWhere([
                                            "item_id" => $itemId,
                                            "wv_id"   => $wvId,
                                            ]);
        $wvDtlId = object_get($wvDtlObj, 'wv_dtl_id');

        if (!$wvDtlId) {
            $msg = sprintf("Carton RFID %s doesn't belong to Order %s!", object_get($odrCtnObj, 'ctn_rfid'), object_get($odrHdrObj, 'odr_num'));
            if (!$wvId) {
                $msg = sprintf("The Order %s need to be created wave pick before this step!", object_get($odrHdrObj, 'odr_num'));
            }
            throw new \Exception($msg);
        }

        return [
            "wv_hdr_id" => $wvId,
            "wv_dtl_id" => $wvDtlId,
            "wv_num"    => $wvNum,
        ];
    }

    private function _updateWvPickDtlLocInCross($odrCtnFrom, $wvInfo)
    {
        $wvDtlIdFrom = array_get($odrCtnFrom, 'wv_dtl_id');
        $ctnId       = array_get($odrCtnFrom, 'ctn_id');
        $pieceQty    = array_get($odrCtnFrom, 'piece_qty');
        $wvDtlIdTo   = array_get($wvInfo, 'wv_dtl_id');

        $wvDtlLocFrom = $this->waveDtlLocModel->getFirstwhere(['wv_dtl_id'=> $wvDtlIdFrom]);
        $wvDtlLocTo   = $this->waveDtlLocModel->getFirstwhere(['wv_dtl_id'=> $wvDtlIdTo]);

        $changeActLocData = null;
        $changeCartonData = null;
        if ($wvDtlLocFrom && $wvDtlLocFrom->act_loc_ids) {
            $actLocs = json_decode($wvDtlLocFrom->act_loc_ids, true);
            foreach ($actLocs as $keyLoc => $actLoc) {
                foreach ($actLoc['cartons'] as $keyCtn => $cartonOld) {
                    if ($cartonOld['ctn_id'] == $ctnId) {
                        $changeActLocData = $actLoc;
                        $changeCartonData = $cartonOld;
                        // remove carton
                        unset($actLocs[$keyLoc]['cartons'][$keyCtn]);
                        // update location
                        $actLocs[$keyLoc]['picked_qty'] -= $pieceQty;
                        break;
                    }
                }
                // update carton
                $actLocs[$keyLoc]['cartons'] = array_values($actLocs[$keyLoc]['cartons']);
                // update location
                if (count($actLocs[$keyLoc]['cartons']) == 0) {
                    // remove location
                    unset($actLocs[$keyLoc]);
                }
            }

            $wvDtlLocFrom->act_loc_ids = json_encode(array_values($actLocs));

            $wvDtlLocFrom->update();
        }

        if (!$changeActLocData || !$changeCartonData) {
            $msg = sprintf("Not found carton RFID %s in any location!", array_get($odrCtnFrom, 'ctn_id'));
            throw new \Exception($msg);
        }

        if ($wvDtlLocTo) {
            if ($wvDtlLocTo->act_loc_ids) {
                $actLocs = json_decode($wvDtlLocTo->act_loc_ids, true);
                $update = false;
                foreach ($actLocs as $keyLoc => $actLoc) {
                    if ($actLoc['loc_id'] == $changeActLocData['loc_id']) {
                        $update = true;
                        // update loc
                        $actLocs[$keyLoc]['cartons'][]   = $changeCartonData;
                        $actLocs[$keyLoc]['picked_qty'] += $pieceQty;
                        break;
                    }
                }

                if (!$update) {
                    $changeActLocData['cartons']    = [$changeCartonData];
                    $changeActLocData['picked_qty'] = $pieceQty;
                    $actLocs[] = $changeActLocData;
                }
                $wvDtlLocTo->act_loc_ids = json_encode($actLocs);
            } else {

                $changeActLocData['cartons']    = [$changeCartonData];
                $changeActLocData['picked_qty'] = $pieceQty;

                $wvDtlLocTo->act_loc_ids = json_encode([$changeActLocData]);
            }

            $wvDtlLocTo->update();
        }
    }
}