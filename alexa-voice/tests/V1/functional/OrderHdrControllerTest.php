<?php
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Utils\Status;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Message;

class OrderHdrControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function getEndPoint()
    {
        return "/v1/orders/";
    }

    public function getPoint()
    {
        return "/v1/";
    }

    public function test_Show_Ok()
    {
        // Create before Show
        $orderId = factory(OrderHdr::class)->create()->odr_id;

        // Test Show
        $data = $this->call('GET', $this->getEndPoint() . $orderId);

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);
    }

    public function test_Show_NotExitsOrderHdr_Empty()
    {
        // Test Show
        $data = $this->call('GET', $this->getEndPoint() . '999999999');

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "Order"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_Store_Ok()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item1 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);
        $item2 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => (int)$shippingOdr->whs_id,
            'cus_id'          => (int)$shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'cus_odr_num'     => 123456789,
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'items'           => [
                [
                    'itm_id'    => $item1->item_id,
                    'sku'       => $item1->sku,
                    'qty'       => 1,
                    'uom_id'    => $item1->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
                [
                    'itm_id'    => $item2->item_id,
                    'sku'       => $item2->sku,
                    'qty'       => 1,
                    'uom_id'    => $item2->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
            ]
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 201
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_CREATED);

        // Case 2: Data is not null
        $this->assertNotNull(json_decode($data->content(), true)['data']);
    }

    public function test_Store_InvalidWarehouseType_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item1 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);
        $item2 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => "abc",
            'cus_id'          => (int)$shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'cus_odr_num'     => 123456789,
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'items'           => [
                [
                    'itm_id'    => $item1->item_id,
                    'sku'       => $item1->sku,
                    'qty'       => 1,
                    'uom_id'    => $item1->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
                [
                    'itm_id'    => $item2->item_id,
                    'sku'       => $item2->sku,
                    'qty'       => 1,
                    'uom_id'    => $item2->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
            ]
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The whs id must be an integer.",
            json_decode($data->content(), true)['errors']['errors']['whs_id'][0]
        );
    }

    public function test_Store_InvalidWarehouse_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item1 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);
        $item2 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => 99999999,
            'cus_id'          => (int)$shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'cus_odr_num'     => 123456789,
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'items'           => [
                [
                    'itm_id'    => $item1->item_id,
                    'sku'       => $item1->sku,
                    'qty'       => 1,
                    'uom_id'    => $item1->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
                [
                    'itm_id'    => $item2->item_id,
                    'sku'       => $item2->sku,
                    'qty'       => 1,
                    'uom_id'    => $item2->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
            ]
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The selected whs id is invalid.",
            json_decode($data->content(), true)['errors']['errors']['whs_id'][0]
        );
    }

    public function test_Store_InvalidCustomerType_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item1 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);
        $item2 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => (int)$shippingOdr->whs_id,
            'cus_id'          => "abc",
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'cus_odr_num'     => 123456789,
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'items'           => [
                [
                    'itm_id'    => $item1->item_id,
                    'sku'       => $item1->sku,
                    'qty'       => 1,
                    'uom_id'    => $item1->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
                [
                    'itm_id'    => $item2->item_id,
                    'sku'       => $item2->sku,
                    'qty'       => 1,
                    'uom_id'    => $item2->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
            ]
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The cus id must be an integer.",
            json_decode($data->content(), true)['errors']['errors']['cus_id'][0]
        );
    }

    public function test_Store_InvalidCustomer_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item1 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);
        $item2 = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => 999999999,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'cus_odr_num'     => 123456789,
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'items'           => [
                [
                    'itm_id'    => $item1->item_id,
                    'sku'       => $item1->sku,
                    'qty'       => 1,
                    'uom_id'    => $item1->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
                [
                    'itm_id'    => $item2->item_id,
                    'sku'       => $item2->sku,
                    'qty'       => 1,
                    'uom_id'    => $item2->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ],
            ]
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The selected cus id is invalid.",
            json_decode($data->content(), true)['errors']['errors']['cus_id'][0]
        );
    }

    public function test_Store_MissParram_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'items'           => []
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The cus odr num field is required.",
            json_decode($data->content(), true)['errors']['errors']['cus_odr_num'][0]
        );
    }

    public function test_Store_InvalidDetail_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => "abc"
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The items must be an array.",
            json_decode($data->content(), true)['errors']['errors']['items'][0]
        );
    }

    public function test_Store_EmptyDetail_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => []
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The items field is required.",
            json_decode($data->content(), true)['errors']['errors']['items'][0]
        );
    }

    public function test_Store_InvalidDetailId_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => [
                [
                    'odr_dtl_id' => 999999999,
                    'itm_id'     => $item->item_id,
                    'sku'        => $item->sku,
                    'qty'        => 1,
                    'uom_id'     => $item->uom_id,
                    'color'      => 'red',
                    'size'       => 10,
                    'pack'       => 20,
                    'piece_qty'  => 200
                ]
            ]
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The selected items.0.odr_dtl_id is invalid.",
            json_decode($data->content(), true)['errors']['errors']['items.0.odr_dtl_id'][0]
        );
    }

    public function test_Store_InvalidItemId_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => [
                [
                    'itm_id'    => 999999999,
                    'sku'       => $item->sku,
                    'qty'       => 1,
                    'uom_id'    => $item->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ]
            ]
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The selected items.0.itm_id is invalid.",
            json_decode($data->content(), true)['errors']['errors']['items.0.itm_id'][0]
        );
    }

    public function test_Store_InvalidUomId_Fail()
    {
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => [
                [
                    'itm_id'    => $item->item_id,
                    'sku'       => $item->sku,
                    'qty'       => 1,
                    'uom_id'    => 999999999,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ]
            ]
        ];

        $data = $this->call('POST', $this->getEndPoint(), $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The selected items.0.uom_id is invalid.",
            json_decode($data->content(), true)['errors']['errors']['items.0.uom_id'][0]
        );
    }

    public function test_Update_Ok()
    {
        $orderHdrId = factory(OrderHdr::class)->create()->odr_id;

        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => [
                [
                    'itm_id'    => $item->item_id,
                    'sku'       => $item->sku,
                    'qty'       => 1,
                    'uom_id'    => $item->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ]
            ]
        ];

        $data = $this->call('PUT', $this->getEndPoint() . $orderHdrId, $param);

        // Case 1: Response code is 200 ok
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);
    }

    public function test_Update_MissParam_Fail()
    {
        // Create Order
        $orderHdrId = factory(OrderHdr::class)->create()->odr_id;
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'items'           => []
        ];

        $data = $this->call('PUT', $this->getEndPoint() . $orderHdrId, $param);

        // Case 1: Response code is 422 Unprocessable Entity
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message "The ctnr num field is required."
        $this->assertEquals(
            'The cus odr num field is required.',
            json_decode($data->content(), true)['errors']['errors']['cus_odr_num'][0]
        );
    }

    public function test_Update_OrderHdrIDNotExit_Fail()
    {
        //test update
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => [
                [
                    'itm_id'    => $item->item_id,
                    'sku'       => $item->sku,
                    'qty'       => 1,
                    'uom_id'    => $item->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ]
            ]
        ];

        $data = $this->call('PUT', $this->getEndPoint() . "999999999", $param);

        // Case 1: Response code is 400 Bad Request
        $this->assertEquals(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST, $data->status());
    }

    public function test_Update_InvalidDetail_Fail()
    {
        //test update
        $orderHdrId = factory(OrderHdr::class)->create()->odr_id;
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => "abc"
        ];

        $data = $this->call('PUT', $this->getEndPoint() . $orderHdrId, $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Response code is 400 Bad Request
        $this->assertEquals(
            "The items must be an array.",
            json_decode($data->content(), true)['errors']['errors']['items'][0]
        );
    }

    public function test_Update_EmptyDetail_Fail()
    {
        //test update
        $orderHdrId = factory(OrderHdr::class)->create()->odr_id;
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => []
        ];

        $data = $this->call('PUT', $this->getEndPoint() . $orderHdrId, $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The items field is required.",
            json_decode($data->content(), true)['errors']['errors']['items'][0]
        );
    }

    public function test_Update_InvalidDetailId_Fail()
    {
        //test update
        $orderHdrId = factory(OrderHdr::class)->create()->odr_id;
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => [
                [
                    'odr_dtl_id' => 999999999,
                    'itm_id'     => $item->item_id,
                    'sku'        => $item->sku,
                    'qty'        => 1,
                    'uom_id'     => $item->uom_id,
                    'color'      => 'red',
                    'size'       => 10,
                    'pack'       => 20,
                    'piece_qty'  => 200
                ]
            ]
        ];

        $data = $this->call('PUT', $this->getEndPoint() . $orderHdrId, $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The selected items.0.odr_dtl_id is invalid.",
            json_decode($data->content(), true)['errors']['errors']['items.0.odr_dtl_id'][0]
        );
    }

    public function test_Update_InvalidItemId_Fail()
    {
        //test update
        $orderHdrId = factory(OrderHdr::class)->create()->odr_id;
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => [
                [
                    'itm_id'    => 999999999,
                    'sku'       => $item->sku,
                    'qty'       => 1,
                    'uom_id'    => $item->uom_id,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ]
            ]
        ];

        $data = $this->call('PUT', $this->getEndPoint() . $orderHdrId, $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The selected items.0.itm_id is invalid.",
            json_decode($data->content(), true)['errors']['errors']['items.0.itm_id'][0]
        );
    }

    public function test_Update_InvalidUomId_Fail()
    {
        //test update
        $orderHdrId = factory(OrderHdr::class)->create()->odr_id;
        $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();
        $item = factory(Seldat\Wms2\Models\Item::class)->create(['cus_id' => $shippingOdr->cus_id]);

        $param = [
            'whs_id'          => $shippingOdr->whs_id,
            'cus_id'          => $shippingOdr->cus_id,
            'odr_sts'         => Status::getByValue("New", "Order-status"),
            'odr_type'        => Status::getByValue("Bulk", "Order-type"),
            'carrier'         => "ABC",
            'ship_to_name'    => "Dai Ho",
            'ship_to_add_1'   => "72 Phan Dang Luu",
            'ship_to_city'    => "Viet Nam",
            'ship_to_country' => "HCM",
            'ship_to_state'   => "HCM",
            'ship_to_zip'     => 1234,
            'ship_by_dt'      => "01/01/2016",
            'cancel_by_dt'    => "12/31/2016",
            'req_cmpl_dt'     => "01/01/2016",
            'act_cmpl_dt'     => "01/01/2016",
            'act_cancel_dt'   => "01/01/2016",
            'in_notes'        => "in notes",
            'cus_notes'       => "Cus notes",
            'ref_cod'         => 123456,
            'cus_po'          => $shippingOdr->ref_num,
            'rush_odr'        => 1,
            'cus_odr_num'     => 123456789,
            'items'           => [
                [
                    'itm_id'    => $item->item_id,
                    'sku'       => $item->sku,
                    'qty'       => 1,
                    'uom_id'    => 999999999,
                    'color'     => 'red',
                    'size'      => 10,
                    'pack'      => 20,
                    'piece_qty' => 200
                ]
            ]
        ];

        $data = $this->call('PUT', $this->getEndPoint() . $orderHdrId, $param);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            "The selected items.0.uom_id is invalid.",
            json_decode($data->content(), true)['errors']['errors']['items.0.uom_id'][0]
        );
    }

    public function test_Search_Ok()
    {
        // Test Search
        $this->call('GET', $this->getEndPoint());

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }

    //public function test_updateCsr_Ok()
    //{
    //    // Create before Show
    //    $userInfo = factory(User::class)->create();
    //    $user_id = $userInfo->user_id;
    //
    //    $orderInfo1 = factory(OrderHdr::class)->create();
    //    $odr_id1 = $orderInfo1->odr_id;
    //
    //    $orderInfo2 = factory(OrderHdr::class)->create();
    //    $odr_id2 = $orderInfo2->odr_id;
    //
    //    $odr_id = [$odr_id1, $odr_id2];
    //
    //    // Test update
    //    $data = $this->call('PUT', $this->getPoint() . "assign-csr", [
    //        'user_id' => $user_id,
    //        'odr_id'  => $odr_id,
    //    ]);
    //
    //    // Case 1: Response code is 200 ok
    //    $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    //
    //    // Case 2: Data is not null.
    //    $this->assertNotNull(json_decode($data->content(), true)['odr_hdrs']);
    //}

    //public function test_updateCsr_NotExist_Order_Fail()
    //{
    //    // Create before Show
    //    $userInfo = factory(User::class)->create();
    //    $user_id = $userInfo->user_id;
    //
    //
    //    $data = $this->call('PUT', $this->getPoint() . "assign-csr", [
    //        'user_id' => $user_id,
    //        'odr_id'  => [9999999999999999],
    //    ]);
    //
    //    // Case 1: Response code is 400
    //    $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);
    //
    //    // Case 2: Show message
    //    $this->assertEquals(
    //        Message::get("BM017", "Order"),
    //        json_decode($data->content(), true)['errors']['message']
    //    );
    //
    //}

    public function test_updateCsr_NotExist_User_Fail()
    {
        // Create before Show
        $orderInfo1 = factory(OrderHdr::class)->create();
        $odr_id1 = $orderInfo1->odr_id;

        $orderInfo2 = factory(OrderHdr::class)->create();
        $odr_id2 = $orderInfo2->odr_id;

        $odr_id = [$odr_id1, $odr_id2];

        $data = $this->call('PUT', $this->getPoint() . "assign-csr", [
            'user_id' => 99999,
            'odr_id'  => $odr_id,
        ]);

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "User"),
            json_decode($data->content(), true)['errors']['message']
        );

    }

    public function test_updateCsr_UserHaveTo_Integer_Fail()
    {
        // Create before Show
        $orderInfo1 = factory(OrderHdr::class)->create();
        $odr_id1 = $orderInfo1->odr_id;

        $orderInfo2 = factory(OrderHdr::class)->create();
        $odr_id2 = $orderInfo2->odr_id;

        $odr_id = [$odr_id1, $odr_id2];

        $data = $this->call('PUT', $this->getPoint() . "assign-csr", [
            'user_id' => '999999999999',
            'odr_id'  => $odr_id,
        ]);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            'The user id must be an integer.',
            json_decode($data->content(), true)['errors']['errors']['user_id'][0]
        );

    }
}
