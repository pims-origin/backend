<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\Status;

/**
 * Class PackHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class PackHdrTransformer extends TransformerAbstract
{
    /**
     * @param PackHdr $packHdr
     *
     * @return array
     */
    public function transform(PackHdr $packHdr)
    {
        $demension = !empty($packHdr['length']) ? $packHdr['length']. 'x' : '';
        $demension .= !empty($packHdr['width']) ? $packHdr['width']. 'x' : '';
        $demension .= !empty($packHdr['height']) ? $packHdr['height'] : '';

        return [
            'pack_hdr_id'   => $packHdr['pack_hdr_id'],
            'odr_hdr_id'    => $packHdr['odr_hdr_id'],
            'pack_sts'      => $packHdr['pack_sts'],
            'pack_sts_name' => Status::getByKey("Pack-Status", $packHdr['pack_sts']),
            'pack_hdr_num'  => $packHdr['pack_hdr_num'],
            'sku_ttl'       => $packHdr['sku_ttl'],
            'piece_ttl'     => $packHdr['piece_ttl'],
            'pack_type'     => !empty($packHdr['pack_type']) ? Status::getByKey('PACK-TYPE',$packHdr['pack_type']) : '',
            'dimension'     => $demension,
            'is_print'      => (int)$packHdr['is_print'],
            'sku'           => $packHdr['sku'],
            'tracking_number'           => $packHdr['tracking_number'],
            'has_rfid'      => $packHdr['rfid'] ? true : false,
            'weight'        => !empty($packHdr['weight']) ? $packHdr['weight'] : 0
        ];
    }
}
