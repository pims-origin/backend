<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:00
 */

namespace App\Api\V1\Validators;

class ChargeTypeDeleteMassValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'chg_type_id'   => 'required',
            'chg_type_id.*' => 'required|exists:charge_type,chg_type_id'
        ];
    }
}
