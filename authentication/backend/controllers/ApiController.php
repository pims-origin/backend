<?php
namespace backend\controllers;

use backend\models\UserCache;
use common\models\User;
use Firebase\JWT\JWT;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;

abstract class ApiController extends ActiveController
{
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
        'metaEnvelope'       => 'meta',
        'linksEnvelope'      => 'links',
    ];

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
            'bearerAuth' => [
                'class'  => HttpBearerAuth::className(),
                'except' => ['login', 'signup', 'update-password', 'setup-password'],
            ],
        ]);
    }

    /**
     * Fire after every action run
     *
     * @param \yii\base\Action $action
     * @param mixed $result
     *
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if (Yii::$app->getResponse()->getStatusCode() === 200 && $action->controller->id === 'user') {
            $actName = $action->controller->action->id;
            $this->clearUserCache($actName, $action);
        }

        return $result;
    }

    /**
     * Clear user cache info
     *
     * @param $actName
     * @param null $id
     */
    protected function clearUserCache($actName, $action)
    {
        $trigger = ['update', 'delete'];
        $userCache = UserCache::getInstance();

        //single trigger
        if (in_array($actName, $trigger)) {
            $getId = $action->controller->actionParams['id'];
            $userCache->deleteUserCache($getId);
        }

        //multiple trigger
        if ($actName === 'delete-multiple') {
            $userIds = Yii::$app->request->post()['user_ids'];
            $userIds = explode(',', $userIds);
            foreach ($userIds as $userId) {
                $userCache->deleteUserCache($userId);
            }
        }
    }

    protected function checkPermission($permission)
    {
        $url = Yii::$app->params['authorization_api'] . 'authorize';
        $requestHeader = Yii::$app->request->headers;
        $token = $requestHeader->get('authorization');
        $option = [
            'headers' => [
                'Authorization' => $token,
            ]
        ];
        $resp = Yii::$app->httpclient->post($url, ['permission' => $permission], $option, true);

        return $resp['data']['permission'] ? $resp['data']['permission'] : false;
    }

    protected function getAuth()
    {
        $secret = User::getSecretKey();
        $requestHeader = Yii::$app->request->headers;
        $token = $requestHeader->get('authorization');
        $token = str_replace('Bearer ', '', $token);

        return (array)JWT::decode($token, $secret, [User::getAlgo()]);
    }
}
