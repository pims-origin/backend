<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GoodsReceiptStatus;


class GoodReceiptStatusTransformer extends TransformerAbstract
{

    public function transform(GoodsReceiptStatus $goodsReceiptStatus)
    {
        return [
            'gr_sts_code' => object_get($goodsReceiptStatus, 'gr_sts_code',null),
            'gr_sts_name' => object_get($goodsReceiptStatus, 'gr_sts_name', null)
        ];
    }
}
