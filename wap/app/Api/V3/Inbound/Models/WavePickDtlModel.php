<?php

namespace App\Api\V3\Inbound\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Status;

class WavePickDtlModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickDtl($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function loadData($wv_id)
    {
        return $this->model->select([
            'item_id as ITEM_ID',
            'sku as SKU',
            'color as Color',
            'size as SIZE',
            'lot as Lot',
            'pack_size as Pack_size',
            'ctn_qty as Cartons',
            'piece_qty as Picking_QTY',
            'primary_loc',
            'bu_loc_1',
            'bu_loc_2',
            'bu_loc_3',
            'bu_loc_4',
            'bu_loc_5',
            DB::raw("(select count(*) from cartons where item_id = ITEM_ID and loc_code = primary_loc) as numCartons")
        ])
            ->where('wv_id', '=', $wv_id)->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateWvDtl($data)
    {
        return $this->model
            ->where('item_id', '=', $data['item_id'])
            ->where('wv_id', '=', $data['wv_id'])
            ->update([
                'act_piece_qty' => $data['act_piece_qty'],
                'act_loc_id'    => $data['act_loc_id'],
                'act_loc'       => $data['act_loc'],
                'wv_dtl_sts'    => Status::getByValue("Complete", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function loadWvDetail($wv_id)
    {
        return $this->model->select([
            '*',
            DB::raw("SUM(piece_qty) as s_piece_qty")
        ])
            ->where('wv_id', '=', $wv_id)
            ->groupBy('item_id', 'sku', 'color', 'size')
            ->get();
    }

    public function getActualWvDtl($wvId)
    {
        return $this->model
            ->where('wv_id', $wvId)
            ->whereRaw('act_piece_qty = piece_qty')
            ->select(DB::raw('COUNT(wv_dtl_id) as actual'))->get();
    }

    public function getStatusWvHdr($wvId)
    {
        return $this->model
            ->where('wv_id', $wvId)
            ->select(DB::raw('SUM(act_piece_qty) as actual, SUM(piece_qty) as piece_qty'))
            ->get();
    }

    public function updateWvDtlPickCtn($data)
    {
        return $this->model->where('wv_dtl_id', $data['wv_dtl_id'])
            ->update([
                'act_piece_qty' => $data['act_piece_qty'],
                'wv_dtl_sts'    => $data['wv_dtl_sts'],
            ]);
    }

    public function checkStaCO($wvId)
    {
        $query =  $this->model
            ->where('wv_id', $wvId)
            ->whereNotIn('wv_dtl_sts', [Status::getByValue('Picked', 'WAVE_DETAIL_STATUS')])
            ->first();

        return $query;
    }

    public function getWavePickInfo($wvId, $itemID) {
        return $this->model
            ->select('wv_dtl_id', 'wv_hdr.wv_num')
            ->join('wv_hdr', 'wv_hdr.wv_id', '=', 'wv_dtl.wv_id')
            ->where('wv_hdr.wv_id', $wvId)
            ->where('item_id', $itemID)
            ->get();
    }
}
