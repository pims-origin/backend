<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\chargeCode;
use League\Fractal\TransformerAbstract;

class ChargeCodeTransformer extends TransformerAbstract
{
    public function transform(ChargeCode $chargeCode)
    {
        return [
            'chg_code_id'   => $chargeCode->chg_code_id,
            'chg_code'      => $chargeCode->chg_code,
            'chg_code_des'  => $chargeCode->chg_code_des,
            'chg_uom_id'    => $chargeCode->chg_uom_id,
            'chg_uom_code'  => object_get($chargeCode, 'systemUom.sys_uom_code', ''),
            'chg_uom_name'  => object_get($chargeCode, 'systemUom.sys_uom_name', ''),
            'sys_uom_des'   => object_get($chargeCode, 'systemUom.sys_uom_des', ''),
            'chg_type_id'   => $chargeCode->chg_type_id,
            'chg_type_code' => object_get($chargeCode, 'chargeType.chg_type_code', ''),
            'chg_type_name' => object_get($chargeCode, 'chargeType.chg_type_name', ''),
            'chg_type_des'  => object_get($chargeCode, 'chargeType.chg_type_des', ''),
            'created_at'    => $chargeCode->created_at,
            'updated_at'    => $chargeCode->updated_at,
            'deleted_at'    => $chargeCode->deleted_at,
        ];
    }
}
