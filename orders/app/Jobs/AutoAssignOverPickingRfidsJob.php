<?php

namespace App\Jobs;

use DB;
use GuzzleHttp\Client;
use App\Api\V1\Models\Log;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderDtl;
use Wms2\UserInfo\Data;

class AutoAssignOverPickingRfidsJob extends Job
{
    protected $whsId;
    protected $wvId;
    protected $rfids;
    protected $request;
    protected $odrId;
    protected $pieceAssign;
    /**
     * AutoPackJob constructor.
     */
    public function __construct($whsId, $wvId, $rfids, $odrId = null, $request, $pieceAssign = null)
    {
        $this->whsId    = $whsId;
        $this->wvId     = $wvId;
        $this->rfids    = $rfids;
        $this->request  = $request;
        $this->odrId    = $odrId;
        $this->pieceAssign = $pieceAssign;
    }

    public function handle()
    {
        $wvId = $this->wvId;
        $rfids = $this->rfids;
        $whsId = $this->whsId;
        $odrId = $this->odrId;
        $request = $this->request;

        $ctnOverPicking = OrderCarton::whereNull('odr_hdr_id')
            //->whereIn('ctn_rfid', $rfids)
            ->where('wv_hdr_id', $wvId)
            ->get();

        $itemAllocated = OrderDtl::where('wv_id', $wvId)
            ->select(DB::raw('item_id, alloc_qty, picked_qty'))
            ->get()->toArray();

        $result = [];
        foreach ($itemAllocated as $values){
            $result[$values['item_id']] = [
                'alloc_qty'     => $values['alloc_qty'],
                'picked_qty'    => $values['picked_qty'],
                'piece_remain'  => $values['alloc_qty'] - $values['picked_qty'],
            ];
        }

        try{
            $client = new Client();
            $version = "v4";
            $url = sprintf('/%s/whs/%d/order/%d/pieces-with-rfid', $version, $whsId, $odrId);
            $authorization = $request->getHeader('Authorization');
            foreach ($ctnOverPicking as $ctn)
            {
                $response = $client->request('PUT', env("API_WAP"). $url,
                    [
                        'headers'     => ['Authorization' => $authorization],
                        'form_params' => [
                            "ctn-rfid" => $ctn->ctn_rfid,
                            "picked-qty" => $result[$ctn->item_id]['piece_remain'],
                            "picker" => Data::getCurrentUserId()
                        ],
                    ]
                );
            }
        }catch (\Exception $exception) {
            $dataError = [
                'date'     => time(),
                'api_name' => "WAP",
                'error'    => $exception->getMessage(),
            ];

            DB::table('sys_bugs')->insert($dataError);
        }
    }
}
