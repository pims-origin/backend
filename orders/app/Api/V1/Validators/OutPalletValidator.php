<?php

namespace App\Api\V1\Validators;

/**
 * Class OutPalletValidator
 *
 * @package App\Api\V1\Validators
 */
class OutPalletValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'volume'    => 'required|numeric|between:0,999999999999999.99',
            'length'    => 'numeric|between:0,99999.99',
            'width'     => 'numeric|between:0,99999.99',
            'height'    => 'numeric|between:0,99999.99',
            'weight'    => 'required|numeric|between:0,99999.99',
        ];
    }
}
