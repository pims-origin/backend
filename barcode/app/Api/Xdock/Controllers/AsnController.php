<?php

namespace App\Api\Xdock\Controllers;

use App\Api\Inbound2\Models\CustomerModel;
use App\Api\Inbound2\Models\GoodsReceiptDetailModel;
use App\Api\Inbound2\Models\ItemModel;
use App\Api\Inbound2\Models\SystemUomModel;
use App\Api\Inbound2\Transformers\AsnListsCanSortTransformer;
use App\Api\Inbound2\Transformers\GoodsReceiptTransformer;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\Xdock\Models\AsnDtlModel;
use App\Api\Xdock\Models\AsnHdrModel;
use App\Api\Xdock\Transformers\AsnDetailTransformer;
use App\Api\Xdock\Transformers\AsnListsTransformer;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\Item;
use Validator;


class AsnController extends AbstractController
{
    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;

    /**
     * @var AsnHdrModel
     */
    protected $asnHdrModel;


    /**
     * AsnController constructor.
     *
     * @param Item $itemModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(Item $itemModel, EventTrackingModel $eventTrackingModel)
    {
        $this->asnDtlModel = new AsnDtlModel();
        $this->asnHdrModel = new AsnHdrModel();
    }

    public function listAsn(Request $request, AsnListsTransformer $asnListsTransformer, $whsId)
    {
        set_time_limit(0);
        $input = $request->getQueryParams();
        //get needed params
        $input['whs_id'] = $whsId;

        try {
            $asns = $this->asnHdrModel->getListASN($input, 2);

            return $this->response->paginator($asns, $asnListsTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function loadASNDtl(
        $whsId,
        $cusId,
        $asnID,
        $ctnID,
        AsnDetailTransformer $asnDetailTransformer,
        Request $request
    ) {

        if (!$asnHrd = $this->asnHdrModel->getFirstBy('asn_hdr_id', $asnID)) {
            $msg = Message::get("BM017", "ASN");
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $asnDtls = $this->asnDtlModel->findWhere(
            [
                'asn_hdr_id'      => $asnID,
                'ctnr_id'         => $ctnID,
                'asn_dtl_crs_doc' => ['asn_dtl_crs_doc', '>', 0],
                'asn_dtl_sts'     => ['asn_dtl_sts', '!=', 'CC']
            ],
            ['container', 'grDtl', 'grDtl.goodsReceipt', 'item', 'systemUom'],
            ['asn_dtl_id' => 'asc']
        );

        $details = [];
        if ($asnDtls) {
            foreach ($asnDtls as $asnDtl) {
                $details[] = [
                    'asn_dtl_id'        => $asnDtl->asn_dtl_id,
                    'asn_hdr_id'        => $asnDtl->asn_hdr_id,
                    'ctnr_id'           => $asnDtl->ctnr_id,
                    'ctnr_num'          => $asnDtl->container->ctnr_num,
                    'dtl_item_id'       => $asnDtl->item_id,
                    'asn_dtl_cus_upc'   => $asnDtl->asn_dtl_cus_upc,
                    'dtl_size'          => $asnDtl->item->size,
                    'dtl_color'         => $asnDtl->item->color,
                    'dtl_uom_id'        => $asnDtl->uom_id,
                    'dtl_uom_code'      => object_get($asnDtl, 'systemUom.sys_uom_code', ''),
                    'dtl_uom_name'      => object_get($asnDtl, 'systemUom.sys_uom_name', ''),
                    'dtl_po'            => $asnDtl->asn_dtl_po,
                    'ucc128'            => $asnDtl->ucc128,
                    'upc'               => $asnDtl->asn_dtl_cus_upc,
                    'dtl_po_date'       => ($asnDtl->asn_dtl_po_dt) ? date('Y/m/d', $asnDtl->asn_dtl_po_dt) : '',
                    'gr_hdr_num'        => object_get($asnDtl, 'grDtl.goodsReceipt.gr_hdr_num', ''),
                    'gr_dtl_plt_ttl'    => object_get($asnDtl, 'grDtl.gr_dtl_plt_ttl', 0),
                    'dtl_gr_dtl_is_dmg' => object_get($asnDtl, 'grDtl.gr_dtl_is_dmg', 0),
                    'dtl_gr_dtl_disc'   => object_get($asnDtl, 'grDtl.gr_dtl_disc', 0),


                    'asn_dtl_lot' => $asnDtl->asn_dtl_lot, //USED
                    'dtl_sku'     => $asnDtl->item->sku, //USED
                    //'dtl_ctn_ttl'            => $asnDtl->asn_dtl_ctn_ttl, //USED
                    //'dtl_gr_dtl_act_ctn_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_act_ctn_ttl', 0), //USED
                    //'dtl_gr_dtl_act_qty_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_act_qty_ttl', 0), //USED
                    //'asn_dtl_qty_ttl'        => object_get($asnDtl, 'asn_dtl_qty_ttl', 0), //USED

                    'dtl_ctn_ttl'            => $asnDtl->asn_dtl_crs_doc, //USED
                    'dtl_gr_dtl_act_ctn_ttl' => object_get($asnDtl, 'grDtl.crs_doc', 0), //USED
                    'asn_dtl_qty_ttl'        => object_get($asnDtl, 'asn_dtl_crs_doc_qty', 0), //USED
                    'dtl_gr_dtl_act_qty_ttl' => object_get($asnDtl, 'grDtl.crs_doc_qty', 0), //USED
                    'crs_doc'                => object_get($asnDtl, 'grDtl.crs_doc', 0), //NEW
                    'crs_doc_qty'            => object_get($asnDtl, 'grDtl.crs_doc_qty', 0), //NEW
                ];
            }
        }

        $asnHrd->details = $details;

        return $this->response->item($asnHrd, $asnDetailTransformer);
    }

    /**
     * @param $whsId
     * @param $asnDtlId
     *
     * @return array
     */
    public function getAsnDetail2($whsId, $asnDtlId)
    {
        $data = AsnDtl::select([
            'asn_dtl.asn_dtl_id',
            'asn_dtl.asn_hdr_id',
            'asn_dtl.ctnr_num',
            'asn_dtl.ctnr_id',
            'asn_dtl.item_id',
            'asn_dtl.asn_dtl_sku',
            'asn_dtl.asn_dtl_size',
            'asn_dtl.asn_dtl_color',
            'asn_dtl.asn_dtl_lot',
            'asn_dtl.asn_dtl_des',
            'asn_dtl.ucc128',
            'asn_dtl.asn_dtl_ctn_ttl',
            'asn_dtl.asn_dtl_cus_upc AS upc',
            'asn_dtl.expired_dt',
            'asn_dtl.uom_code',
            DB::raw('gr_dtl.gr_dtl_act_ctn_ttl AS act_ctn'),
            DB::raw('gr_dtl.gr_dtl_plt_ttl AS plts'),
            DB::raw('gr_dtl.gr_dtl_dmg_ttl AS dmg_ttl'),
            'gr_dtl.gr_dtl_plt_ttl',
            DB::raw("CONCAT(COALESCE(gr_dtl.crs_doc, 0), '/', asn_dtl_crs_doc) AS ctns"),
            DB::raw("CONCAT(COALESCE(gr_dtl.crs_doc_qty, 0), '/', asn_dtl_crs_doc_qty) AS qty"),
        ])
            ->leftJoin('gr_dtl', 'gr_dtl.asn_dtl_id', '=', 'asn_dtl.asn_dtl_id')
            ->where([
                'asn_dtl.asn_dtl_id' => $asnDtlId,
            ])
            ->whereIn('asn_dtl.asn_dtl_sts', ['NW', 'RG'])
            ->first();

        $data = $data ? $data->toArray() : [];

        if (!empty($data['expired_dt'])) {
            $data['expired_dt'] = date('ymd', $data['expired_dt']);
        }

        return (array)$data;
    }
}
