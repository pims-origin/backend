<?php

namespace Seldat\Wms2\Models;

/**
 * Class OrderDtl
 *
 * @property mixed odr_dtl_id
 * @property mixed wv_id
 * @property mixed odr_id
 * @property mixed whs_id
 * @property mixed cus_id
 * @property mixed item_id
 * @property mixed uom_id
 * @property mixed color
 * @property mixed sku
 * @property mixed size
 * @property mixed lot
 * @property mixed pack
 * @property mixed des
 * @property mixed qty
 * @property mixed peice_qty
 * @property mixed itm_sts
 * @property mixed sts
 * @property mixed back_odr
 * @property mixed special_hdl
 * @property mixed back_odr_qty
 *
 * @package Seldat\Wms2\Models
 */
class OrderDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'odr_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'wv_id',
        'odr_id',
        'whs_id',
        'cus_id',
        'item_id',
        'uom_id',
        'color',
        'sku',
        'size',
        'lot',
        'pack',
        'des',
        'qty',
        'piece_qty',
        'itm_sts',
        'sts',
        'back_odr',
        'alloc_qty',
        'special_hdl',
        'back_odr_qty',
        'cus_upc',
        'ship_track_id',
        'packed_qty',
        'picked_qty',
        'allow_dmg',
        "parent_id",
        'gr_dtl_id',
        'x_dock_qty',
        'type',
        'dmg_qty',
        'hs_num',
        'hs_unit',
        'price',
    ];
	
    public function waveHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\WavepickHdr', 'wv_id', 'wv_id');
    }

    public function orderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function item()
    {
        return $this->hasOne(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function inventorySummary()
    {
        return $this->hasOne(__NAMESPACE__ . '\InventorySummary', 'item_id', 'item_id');
    }

    public function systemUom()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemUom', 'sys_uom_id', 'uom_id');
    }

    public function itemStatus()
    {
        return $this->hasOne(__NAMESPACE__ . '\ItemStatus', 'itm_sts', 'item_sts_code');
    }

    public function returnDtl()
    {
        return $this->hasOne(__NAMESPACE__ . '\ReturnDtl', 'odr_dtl_id', 'odr_dtl_id');
    }
}
