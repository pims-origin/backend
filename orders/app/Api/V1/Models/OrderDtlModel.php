<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use DB;
use Seldat\Wms2\Models\Inventories;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

/**
 * Class OrderDtlModel
 *
 * @package App\Api\V1\Models
 */
class OrderDtlModel extends AbstractModel
{
    private $customerConfigModel;

    /**
     * @param $odrDtlIds
     *
     * @return mixed
     */
    public function deleteDtls($odrDtlIds)
    {
        return $this->model
            ->whereIn('odr_dtl_id', $odrDtlIds)
            ->forceDelete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->where('ord_hdr_id', $attributes['ord_hdr_id']);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrLike = ['color', 'sku', 'size', 'lot'];
        $arrEqual = [
            'ord_hdr_id',
            'whs_id',
            'cus_id',
            'item_id',
            'uom_id',
            'request_qty',
            'qty_to_allocated',
            'allocated_qty',
            'damaged',
            'special_handling',
            'back_ord'
        ];
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }
        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $orderHdrId
     * @param $itemIds
     * @param array $with
     *
     * @return mixed
     */
    public function loadByOrdIds($orderHdrIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('odr_id', $orderHdrIds);
        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }

    /**
     * @param array $columns
     * @param string $where
     * @param array $params
     * @param bool|false $upperCase
     *
     * @return array
     */
    public function fetchColumn($columns = [], $where = "", $params = [], $upperCase = false)
    {
        $tblName = $this->getTable();
        $colNames = implode(', ', $columns);

        $query = "
            SELECT $colNames
            FROM $tblName
        ";
        if (!empty($where)) {
            $query .= " WHERE $where";
        }

        $result = [];

        $db = $this->model->getConnection()->getPdo();

        $statement = $db->prepare($query);
        $statement->execute($params);

        $colKey = $columns;
        end($columns);
        $endKey = key($columns);
        unset($colKey[$endKey]);

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            end($row);
            $end = key($row);
            $val = $row[$columns[$endKey]];
            unset($row[$end]);
            $key = implode("-", $row);

            if ($upperCase) {
                $key = strtoupper($key);
            }

            $result[$key] = $val;
        }

        return $result;
    }

    /**
     * Process revert data for cancel order
     *
     * @param int $odrId
     * @param bool $isNew
     */
    public function revertOrder($odrObj, $isNew)
    {
        $odrId = $odrObj->odr_id;
        $odrType = $odrObj->odr_type;
        $whsId = $odrObj->whs_id;

        $odrStsNotUpdate = [
            Status::getByValue('Picked', 'Order-Status'),
            Status::getByValue('Partial Picked', 'Order-Status'),
            Status::getByValue('Packing', 'Order-Status'),
            Status::getByValue('Partial Packing', 'Order-Status'),
            Status::getByValue('Packed', 'Order-Status'),
            Status::getByValue('Partial Packed', 'Order-Status'),
            Status::getByValue('Staging', 'Order-Status'),
            Status::getByValue('Partial Staging', 'Order-Status'),
            Status::getByValue('Shipped', 'Order-Status'),
            Status::getByValue('Partial Shipped', 'Order-Status'),
            Status::getByValue('Hold', 'Order-Status')
        ];

        if (in_array($odrObj->odr_sts, $odrStsNotUpdate)) {
            return true;
        }
        $invSumrModel = new InventorySummaryModel();
        $return = $invSumrModel->updateAllocatedOrderInventory($whsId, [$odrId]);

        //$condition = "";
        //if (!$isNew) {
        //    $condition = "
        //        avail = COALESCE(avail, 0) + COALESCE(dtl.alloc_qty, 0),
        //        allocated_qty = COALESCE(allocated_qty, 0) - COALESCE(dtl.alloc_qty, 0)
        //    ";
        //} elseif ($odrType === "BAC") {
        //    $condition .= "back_qty = COALESCE(back_qty, 0) - COALESCE(dtl.piece_qty, 0)";
        //}
        //
        //if ($condition === "") {
        //    return true;
        //}
        //
        //$query = "
        //    UPDATE invt_smr,
        //    (
        //        SELECT odr_dtl_id, item_id, alloc_qty, piece_qty, back_odr_qty
        //        FROM odr_dtl
        //        WHERE deleted = 0 AND odr_id = $odrId
        //    ) as dtl
        //    SET
        //        $condition
        //    WHERE invt_smr.item_id = dtl.item_id AND invt_smr.whs_id = $whsId
        //";
        //
        //$return = $this->naturalExec($query);

        //update alloc_qty for odr_dtl
        //$this->model
        //    ->where('odr_id', $odrObj->odr_id)
        //    ->update(['alloc_qty' => 0]);


        return $return;

    }

    /**
     * @param $orderIds
     * count Origin or back order
     *
     * @return mixed
     */
    public function countSku($orderIds)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->whereIn('odr_id', $orderIds)
            ->count(['item_id']);

        return $skuCount;
    }

    public function countOrderSku($orderId)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->where('odr_id', $orderId)
            ->count(['item_id']);

        return $skuCount;
    }

    public function getInvSumByOdrDtl($where, $lot, $algorithm, $packSize)
    {


        if (!$this->isLotAny($lot)) {
            $where['lot'] = $lot;
        }
        $query = DB::table('cartons')->where($where)->where('piece_remain', '>', 0)->where('ctn_sts',
            'AC');
        // ->where('ctn_pack_size', $packSize);
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        };


        $items = $query->groupBy('item_id', 'lot')->select('lot')->get();
        $lots = array_pluck($items, 'lot');

        $invItems = InventorySummary::where($where)
            // ->where(DB::raw('avail > lock_qty'), '>', 0)
            ->whereRaw(DB::raw('avail > lock_qty'))
            ->get();

        $sortInvSums = [];
        $sortInvSums['sum_pick_able'] = 0;
        foreach ($lots as $lot) {
            foreach ($invItems as $key => $value) {
                if (strtolower($lot) == strtolower($value->lot)) {
                    $sortInvSums['sum_pick_able'] += ($value->avail - $value->lock_qty);
                    $sortInvSums[] = $value;
                    unset($invItems[$key]);
                    break;
                }
            }
        }

        return $sortInvSums;
    }

    public function isLotAny($lot)
    {
        $lot = strtoupper($lot);
        if ($lot === 'ANY') {
            return true;
        }

        return false;
    }

    public function updateOrdDtl($odrHdr, $odrDtls, $isPartial, $algorithm)
    {
        $where = [
            'whs_id' => $odrHdr->whs_id,
            'cus_id' => $odrHdr->cus_id,
        ];
        //Get config back_order of customer
        $this->customerConfigModel = new CustomerConfigModel();
        $cus_back_order = $this->customerConfigModel->getCusBackOrder($odrHdr->whs_id, $odrHdr->cus_id);

        $backOrdDlts = [];
        $dtlCount = $countDtlNotPick = 0;
        $count_OdrDtl = count($odrDtls);
        foreach ($odrDtls as $idx => $odrDtl) {
            $where['item_id'] = $odrDtl->item_id;
//            $invItems = $this->getInvSumByOdrDtl($where, $odrDtl['lot'], $algorithm, $odrDtl->pack);
            $invItems = InventorySummary::where($where)
                ->where(function ($where) use ($odrDtl) {
                    if (strtoupper($odrDtl->lot) != 'ANY') {
                        $where->where('lot', '=', $odrDtl->lot);
                    }
                })
                ->where('avail', '>', 0)
                ->get();

            $sum_pick_able = $invItems->sum('avail');

            $allQty = $reqQty = $odrDtl['piece_qty'];
            $isLast = count($invItems) === 1;
            $lotIndex = 0;
            $isBK = false;
            $isCreateFull = false;

            if ($sum_pick_able < $allQty) {
                $countDtlNotPick++;
                $isBK = true;
            }

            if ($countDtlNotPick == $count_OdrDtl && !$isPartial) {
                $msg = sprintf("Order %s unable to create partial order with all SKU is not enough to pick!",
                    $odrHdr->odr_num);
                throw new \Exception($msg);
            }
            // check case $invItems = [] (pick able =0)

//            $deleteOdrDtlAny = true;
//
//            if(count($invItems) == 0) {
//                if ($cus_back_order == 'full' && $isBK) {
//                    $deleteOdrDtlAny = false;
//
//                }
//
//            }

            foreach ($invItems as $key => $invItem) {
                $dtlCount++;
                $lotIndex++;
                if ($lotIndex == count($invItems)) {
                    $isLast = true;
                }
                $pickableQty = $invItem->avail;
                if ($pickableQty < $allQty) {

                    if ($cus_back_order == 'full' && $isBK) {
                        //Check all item not enough to pick
                        if (!$isCreateFull) {
                            $this->createNewOdrDtl($invItem, $odrDtl, $pickableQty, $isLast);
                            
                            $allQty = $this->updateInvSum($invItem, $allQty, true);
                            $isCreateFull = true;
                        }

                    } else {
                        $this->createNewOdrDtl($invItem, $odrDtl, $pickableQty, $isLast);

                        $allQty = $this->updateInvSum($invItem, $allQty);

                    }

                } else {
                    $this->updateInvSum($invItem, $allQty);
                    $this->createNewOdrDtl($invItem, $odrDtl, $allQty, $isLast);
                    $allQty = 0;
                    break;
                }

            }

            //Check backorder
            if ($allQty > 0) {
                if ($isPartial === false) {
                    throw new \Exception(sprintf("Order %s does not support partial allocation! Please edit the requested quantity!", $odrHdr->odr_num));
                } else {
                    
                    if ($count_OdrDtl > 1 && $cus_back_order == 'full') {
                        $odrDtl->back_odr_qty = $reqQty - $sum_pick_able;
                    } else {
                        $odrDtl->back_odr_qty = $allQty;
                    }
                    $odrDtl->back_odr = 1;
                    $odrDtl->save();
                    $backOrdDlts[] = $odrDtl;
                    $odrHdr->back_odr = 1;
                }
            }

//            if ($this->isLotAny($odrDtl->lot) && $deleteOdrDtlAny) {
//                $odrDtl->delete();
//            }


        }
        // Insert Event Tracking
        $evtTracking = new EventTrackingModel(new EventTracking());
        if ($odrHdr->back_odr) {
            $eventData = [
                'whs_id'    => $odrHdr['whs_id'],
                'cus_id'    => $odrHdr['cus_id'],
                'owner'     => $odrHdr['odr_num'],
                'evt_code'  => Status::getByKey("event", "PARTIAL-ALLOCATED"),
                'trans_num' => $odrHdr['odr_num'],
                'info'      => sprintf(Status::getByKey("event-info", "PAL"), $odrHdr['odr_num'])
            ];
        } else {
            $eventData = [
                'whs_id'    => object_get($odrHdr, 'whs_id', ''),
                'cus_id'    => object_get($odrHdr, 'cus_id', ''),
                'owner'     => object_get($odrHdr, 'odr_num', ''),
                'evt_code'  => config('constants.event.ALLOCATED-ORD'),
                'trans_num' => object_get($odrHdr, 'odr_num', ''),
                'info'      => sprintf(config('constants.event-info.ALLOCATED-ORD'), $odrHdr['odr_num'])
            ];
        }
        $evtTracking->create($eventData);

        $odrHdr->sku_ttl = $dtlCount;
        //Update odr_sts = allocated
        $sts = Status::getByValue('Allocated', 'ORDER-STATUS');
        //if($odrHdr->back_odr || $odrHdr->org_odr_id ){
        //    $sts = Status::getByValue('Partial Allocated', 'ORDER-STATUS');
        //}
        $odrHdr->odr_sts = $sts;
        $odrHdr->save();

        return $backOrdDlts;

    }

    private function createNewOdrDtl($invSum, $odrDtl, $allQty, $isOne = false)
    {
        $newOdrDlt = $odrDtl;
        $lot = $odrDtl->lot;
        if ($this->isLotAny($lot) && $isOne == false) {
            $newOdrDlt = $odrDtl->replicate();
        }

        $newOdrDlt->lot = $invSum->lot;
        $newOdrDlt->alloc_qty = $newOdrDlt->piece_qty = $allQty;
        $newOdrDlt->qty = $newOdrDlt->pack ? ceil($newOdrDlt->alloc_qty / $newOdrDlt->pack) : 0;
        
        if ($this->isLotAny($lot) && $isOne == false) {
            return $newOdrDlt->push();
        } else {
            //$odrDtl->piece_qty = $piece_qty;
            return $newOdrDlt->save();
        }
    }

    /**
     * @param $invSum
     * @param $odrDtl
     * @param $allQty
     * @param bool $isOne
     *
     * @return mixed
     */
    private function createNewOdrDtlStsFull($invSum, $odrDtl, $allQty, $isOne = false)
    {
        $newOdrDlt = $odrDtl;
        $lot = $odrDtl->lot;
        if ($this->isLotAny($lot) && $isOne == false) {
            $newOdrDlt = $odrDtl->replicate();
        }
        $newOdrDlt->lot = $invSum->lot;
        $newOdrDlt->alloc_qty = 0;
        $newOdrDlt->back_odr_qty = $odrDtl->piece_qty;

        if ($this->isLotAny($lot) && $isOne == false) {
            return $newOdrDlt->push();
        } else {
            return $newOdrDlt->save();
        }
    }

    private function updateInvSum($invSum, $allQty, $isFull = true)
    {
        $this->updateOldInvSum($invSum, $allQty);
//         $pickableQty = $invSum->in_hand_qty;
//         if ($pickableQty < $allQty) {
//             $allQty = $allQty - $pickableQty;

//             if ($isFull) {
//                 $invSum->in_pick_qty = $invSum->in_pick_qty + $pickableQty;
//                 $invSum->in_hand_qty = 0;
//             }
//         } else {
//             $invSum->in_pick_qty = $invSum->in_pick_qty + $allQty;
//             $invSum->in_hand_qty = $invSum->in_hand_qty - $allQty;
//             $allQty = 0;
//         }
//         $invSum->save();

        return $allQty;
    }

    private function updateOldInvSum($invSum, $allQty, $isFull = true)
    {
        $invSmr = InventorySummary::where("item_id", $invSum->item_id)
            ->where("avail", ">", 0)->where("cus_id", $invSum->cus_id)->where("whs_id", $invSum->whs_id)->first();

        if($invSmr) {
            $pickableQty = $invSmr->avail;
            if ($pickableQty < $allQty) {
                $allQty = $allQty - $pickableQty;

                if ($isFull) {
                    $invSmr->allocated_qty = $invSmr->allocated_qty + $pickableQty;
                    $invSmr->avail = 0;
                }
            } else {
                $invSmr->allocated_qty = $invSmr->allocated_qty + $allQty;
                $invSmr->avail = $invSmr->avail - $allQty;
                $allQty = 0;
            }
            $invSmr->save();
        }
        

        return $allQty;
    }

    public function updateXdocOrdDtl($odrHdr, $odrDtls, $isPartial, $algorithm)
    {
        $where = [
            'whs_id' => $odrHdr->whs_id,
            'cus_id' => $odrHdr->cus_id,
        ];

        $backOrdDlts = [];
        $dtlCount = 0;

        foreach ($odrDtls as $odrDtl) {
            $where['item_id'] = $odrDtl->item_id;

            //select inv_sum by item_id
            $invItems = $this->getXdocInvSumByOdrDtl($where, $odrDtl['lot'], $algorithm);

            $allQty = $reqQty = $odrDtl['piece_qty'];
            $lotIndex = 0;
            foreach ($invItems as $invItem) {
                $dtlCount++;
                $lotIndex++;
                if ($lotIndex == count($invItems)) {
                    $isLast = true;
                }
                if ($invItem->crs_doc_qty < $allQty) {
                    $this->createNewOdrDtl($invItem, $odrDtl, $invItem->crs_doc_qty, false);
                    $allQty -= $invItem->crs_doc_qty;
                } else {
                    $this->createNewOdrDtl($invItem, $odrDtl, $allQty, false);
                    $allQty = 0;
                    break;
                }
            }
            //Check backorder
            if ($allQty > 0) {
                throw new HttpException(403, 'Cross Dock cartons are not enough');
            }

            if ($this->isLotAny($odrDtl->lot)) {
                $odrDtl->delete();
            }

        }
    }

    public function getXdocInvSumByOdrDtl($where, $lot, $algorithm)
    {
        if (!$this->isLotAny($lot)) {
            $where['lot'] = $lot;
        }
        $query = DB::table('cartons')
            ->where($where)
            ->where('piece_remain', '>', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'XDK');
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        };


        $items = $query->groupBy('item_id', 'lot')->select('lot')->get();
        $lots = array_pluck($items, 'lot');

        $invItems = InventorySummary::where($where)
            ->where('crs_doc_qty', '>', 0)
            ->get();

        $sortInvSums = [];
        foreach ($lots as $lot) {
            foreach ($invItems as $key => $value) {
                if (strtoupper($lot) == strtoupper($value->lot)) {
                    $sortInvSums[] = $value;
                    unset($invItems[$key]);
                    break;
                }
            }
        }

        return $sortInvSums;
    }

    /**
     * @param $odr_id
     * @param $whs_id
     *
     * @return mixed
     */
    public function getOrderDtlBy($odr_id, $whs_id)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('odr_dtl as od')->select([
            'od.odr_dtl_id',
            'od.sku',
            'od.des',
            'od.lot',
            'su.sys_uom_name as uom_name',
            'od.pack',
            DB::raw("round((od.piece_qty/od.pack), 0) AS ctns_ttl"),
            DB::raw("od.piece_qty AS piece_qty_ttl"),

        ])
            ->leftJoin('system_uom as su', 'su.sys_uom_id', '=', 'od.uom_id')

            ->where('od.odr_id', $odr_id)
            ->where('od.deleted', 0)
            ->where('od.whs_id', $whs_id)
            ->groupBy('od.odr_dtl_id')

            ->get();

        return $query;


    }
}
