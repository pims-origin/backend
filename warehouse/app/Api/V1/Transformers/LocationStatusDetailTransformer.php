<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\LocationStatusDetail;

class LocationStatusDetailTransformer extends TransformerAbstract
{
    /**
     * @param LocationStatusDetail $locationStatusDetail
     *
     * @return array
     */
    public function transform(LocationStatusDetail $locationStatusDetail)
    {
        return [
            'loc_sts_dtl_loc_id'    => $locationStatusDetail->loc_sts_dtl_loc_id,
            'loc_sts_dtl_sts_code'  => $locationStatusDetail->loc_sts_dtl_sts_code,
            'loc_sts_dtl_from_date' => $locationStatusDetail->loc_sts_dtl_from_date,
            'loc_sts_dtl_to_date'   => $locationStatusDetail->loc_sts_dtl_to_date,
            'loc_sts_dtl_reason'    => $locationStatusDetail->loc_sts_dtl_reason,
            'loc_sts_reason_id'     => $locationStatusDetail->loc_sts_reason_id,
            'loc_sts_dtl_remark'    => $locationStatusDetail->loc_sts_dtl_remark,
            'updated_by'            => object_get($locationStatusDetail, 'updatedUser.first_name', '') . ' '
                                       . object_get($locationStatusDetail, 'updatedUser.last_name', ''),
            'created_at'            => $locationStatusDetail->created_at,
            'updated_at'            => $locationStatusDetail->updated_at,
            'deleted_at'            => $locationStatusDetail->deleted_at
        ];
    }
}
