<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\CustomersModel;
use App\Api\V1\Transformers\CustomersTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class CustomersController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new CustomersModel();
        $this->transformer = new CustomersTransformer();
    }

    /*
    ****************************************************************************
    */

}
