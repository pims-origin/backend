<?php

namespace common\models;

use yii\db\ActiveRecord;

class Customer extends ActiveRecord
{
    public static function tableName()
    {
        return 'customer';
    }

    public function getCusExternalUser()
    {
        return $this->hasMany(CusInternalUser::className(), ['cus_id' => 'cus_id'])
            ->viaTable('cus_internal_user', ['user_id' => 'user_id']);
    }

//    public function getUsers()
//    {
//        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
//    }


}