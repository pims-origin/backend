<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\DeliveryNoteModel;
use App\Api\V1\Models\DeliveryServiceModel;
use App\Api\V1\Transformers\DeliveryNoteTransformer;
use App\Api\V1\Transformers\DeliveryServiceTransformer;
use App\Api\V1\Validators\DeliveryNoteValidator;
use App\Api\V1\Validators\DeliveryServiceValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Utils\Message;
use Swagger\Annotations as SWG;

class DeliveryServiceController extends AbstractController
{
    /**
     * @var DeliveryNoteModel
     */
    protected $deliveryServiceModel;

    /**
     * @var $deliveryServiceTransformer
     */
    protected $deliveryServiceTransformer;

    /*
     * @var LocationValidator
    */
    protected $deliveryServiceValidator;

    /**
     * DeliveryServiceController constructor.
     *
     */
    public function __construct(
        DeliveryServiceModel $deliveryServiceModel,
        DeliveryServiceTransformer $deliveryServiceTransformer,
        DeliveryServiceValidator $deliveryServiceValidator
    )
    {
        $this->deliveryServiceModel = $deliveryServiceModel;
        $this->deliveryServiceTransformer = $deliveryServiceTransformer;
        $this->deliveryServiceValidator = $deliveryServiceValidator;
    }

    /**
     * API Get List Delivery Service
     *
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response|void
     */
    public function getList(IRequest $request)
    {
        try {
            $reliveryService = $this->deliveryServiceModel->getList();
            return $this->response->collection($reliveryService, $this->deliveryServiceTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Create Delivery Service
     *
     * @param IRequest $request
     *
     * @return Response|void
     */
    public function store(IRequest $request)
    {
        return $this->upsert($request);
    }

    /**
     * API Update Delivery Service
     *
     * @param integer $dnId
     * @param IRequest $request
     *
     * @return Response|void
     */
    public function update($dnId, IRequest $request)
    {
        return $this->upsert($request, $dnId);
    }


    /**
     * @param $request
     * @param bool|false $dnId
     *
     * @return Response|void
     */
    protected function upsert($request, $dnId = false)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->deliveryServiceValidator->validate($input);

        $params = [
            'ds_key' => $input['ds_key'],
            'ds_value' => $input['ds_value'],
        ];
        
        if ($dnId) {
            $params['ds_id'] = $dnId;
        }

        try {
            // check duplicate key
            $chkDupKey = true;
            $deliveryServiceData = $this->deliveryServiceModel->getFirstWhere([
                'ds_key' => $params['ds_key']
            ]);
            
            if ($deliveryServiceData && $dnId) {
                $chkDupKey = ($deliveryServiceData->dn_id != $dnId);
            }
            if ($deliveryServiceData && $chkDupKey) {
                return $this->response->errorBadRequest(Message::get("BM006", "Delivery Service"));
            }

            if ($dnId) {
                $deliveryService = $this->deliveryServiceModel->getFirstWhere(['ds_id' => $dnId]);
                if (empty($deliveryService)) {
                    throw new \Exception(Message::get("BM017", "Delivery Service"));
                }
                $deliveryService = $this->deliveryServiceModel->update($params);
            } else {
                $deliveryService = $this->deliveryServiceModel->create($params);
            }

            return $this->response->item($deliveryService, $this->deliveryServiceTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
