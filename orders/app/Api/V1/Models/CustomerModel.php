<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;
use \Seldat\Wms2\Models\CustomerAddress;

class CustomerModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     *
     * @param Customer $model
     */
    public function __construct(Customer $model = null, CustomerAddress $customerAddressModel = null)
    {
        $this->model = ($model) ?: new Customer();
        $this->customerAddressModel = ($customerAddressModel) ?: new CustomerAddress();
    }

    /**
     * @param int $customerId
     *
     * @return int
     */
    public function deleteCustomer($customerId)
    {
        return $this->model
            ->where('cus_id', $customerId)
            ->delete();
    }

    public function deleteMassCustomer(array $customerIds)
    {
        return $this->model
            ->whereIn('cus_id', $customerIds)
            ->delete();
    }

    /**
     * Search Customer
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {

            $paramAddress = [
                'cus_add_state_id'    => array_get($attributes, 'cus_state_id', null),
                'cus_add_city_name'   => array_get($attributes, 'cus_city_name', null),
                'cus_add_postal_code' => array_get($attributes, 'cus_postal_code', null),
            ];

            $paramAddress = SelArr::removeNullOrEmptyString($paramAddress);

            if (!empty($paramAddress)) {
                $queryAddress = $this->customerAddressModel;
                foreach ($paramAddress as $addrKey => $addrValue) {
                    if ($addrKey == 'cus_add_state_id') {
                        $queryAddress = $queryAddress->where($addrKey, $addrValue);
                    } else {
                        $queryAddress = $queryAddress->where(
                            $addrKey,
                            'like',
                            "%" . SelStr::escapeLike($addrValue) . "%"
                        );
                    }
                }
                $customerAddress = $queryAddress->get();
                $customerIds = array_pluck($customerAddress, 'cus_add_cus_id', 'cus_add_cus_id');
                $query->whereIn('cus_id', $customerIds);
            }

            $searchLike = ['cus_code', 'cus_name', 'cus_billing_account'];
            $searchEqual = ['cus_country_id'];

            foreach ($attributes as $key => $value) {
                if (in_array($key, $searchLike)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                } elseif (in_array($key, $searchEqual)) {
                    $query->where($key, $value);
                }
            }
        }
        $mode = array_get($attributes, 'mode', 0);
        if ($mode == 0) {
            $this->model->filterData($query, true);
        }
        if ($mode == 2) {
            $this->model->filterData($query, true, false);
        }

        // Get

        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $CusId
     *
     * @return mixed
     */
    public function getCusById($CusId)
    {
        $rows = $this->model
            ->where('cus_id', $CusId)
            ->first();

        return $rows;
    }

}