<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\PackDtl;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class PackHdrModel extends AbstractModel
{
    protected $packRef;

    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
        $this->packRef = [];
    }

    /**
     * @param $packHdrId
     *
     * @return mixed
     */
    public function deletePackHdr($packHdrId)
    {
        return $this->model
            ->where('ord_hdr_id', $packHdrId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrEqual = ['cus_id', 'odr_sts', 'odr_type'];

        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === "csr") {
                    $query->whereHas("csrUser", function ($q) use ($attributes) {
                        $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere(
                            DB::raw("concat(first_name, ' ', last_name)"),
                            'like',
                            "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['csr'])) . "%"
                        );
                    });
                    continue;
                }
                if ($key === "odr_num") {
                    $query->where($key, "like", "%" . SelStr::escapeLike($value) . "%");
                    continue;
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }

    public function getPackNum()
    {
        $pack = $this->model
            ->where('odr_num', 'like', 'ORD-' . (date("ym", time())) . "-%")
            ->packBy('odr_id', 'desc')
            ->first();
        $index = 0;
        $packNum = object_get($pack, "odr_num", null);

        if ($packNum) {
            $temp = explode("-", $packNum);
            $index = (int)end($temp);
        }

        return 'ORD-' . (date("ym", time())) . "-" . str_pad(++$index, 5, "0", STR_PAD_LEFT);
    }

    /**
     * @param $packIds
     *
     * @return mixed
     */
    public function checkWhereIn($packIds)
    {
        return $this->model
            ->whereIn('odr_id', $packIds)
            ->count();
    }

    /**
     * @param $pack_ids
     * @param null $odrHdrId
     * @param array $with
     *
     * @return mixed
     */
    public function getPackById($pack_ids, $odrHdrId = null, $with = [])
    {
        $pack_ids = is_array($pack_ids) ? $pack_ids : [$pack_ids];
        $query = $this->make($with);
        if (!empty($odrHdrId)) {
            $query->where('odr_hdr_id', $odrHdrId);
        }
        $query->whereIn('pack_hdr_id', $pack_ids);

        return $query->get();
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getPackOdrId($odr_ids)
    {
        $odr_ids = is_array($odr_ids) ? $odr_ids : [$odr_ids];

        $rows = $this->model
            ->select('SUM(sku_ttl) as sku_ttl')
            ->whereIn('odr_hdr_id', $odr_ids)
            ->get();

        return $rows;
    }

    public function getAllDistinctOrderIds()
    {
        $rows = $this->model
            ->distinct('odr_hdr_id')
            ->get(['odr_hdr_id']);

        return $rows;
    }

    public function getPackDtlByPackId_and_CheckSum(
        $pack_ids,
        $with = [],
        array $orderBy = [],
        $columns = ['*'],
        $or = false
    ) {
        $pack_ids = is_array($pack_ids) ? $pack_ids : [$pack_ids];
        $query = $this->make($with);
        $query->whereIn('odr_hdr_id', $pack_ids);
        $query->groupBy('pack_dt_checksum')
            ->select(DB::raw('count(pack_hdr_id) as total, pack_hdr_id, pack_dt_checksum'));

        return $query->get();
    }

    public function getFistPackNotAssignByCheckSum($checksum, $odr_id, $with = [])
    {
        $query = $this->make($with);
        $result = $query->where('pack_dt_checksum', $checksum)
            ->where('odr_hdr_id', $odr_id)
            ->whereNull('out_plt_id')
            ->first();

        return $result;
    }


    public function insertBatch($packDatas, $odr_id)
    {
        $numPack = count($packDatas);
        $this->model->insert($packDatas);
        $checkSum = array_pluck($packDatas, 'pack_dt_checksum');
        $result = $this->model->where('odr_hdr_id', $odr_id)
            ->where('created_by', $this->getUserId())
            ->whereIn('pack_dt_checksum', $checkSum)
            ->take($numPack)->skip(0)
            ->orderBy('pack_hdr_id', 'DESC')
            ->get(['pack_hdr_id', 'pack_hdr_num']);

        return $result;
    }

    public function getSumPieceByOrder($odr_ids)
    {
        $rows = $this->model
            ->select(DB::raw('SUM(piece_ttl) as pieces_ttl'))
            ->whereIn('odr_hdr_id', $odr_ids)
            ->first();

        return $rows;
    }

    public function autoPack($odrDtlIds, $odrId)
    {
        //validate
        $eventData = [];
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        $OrderHdrModel = new OrderHdrModel();
        $orderHdr = $OrderHdrModel->getFirstWhere(['odr_id' => $odrId]);
        if ($orderHdr->odr_sts === 'PA') {
            throw new HttpException(400, 'This order was packed');
        }

        DB::table('odr_hdr')
            ->where('odr_hdr.odr_id', $odrId)
            ->update(['odr_hdr.odr_sts' => 'PN']);

        foreach ($odrDtlIds as $odrDtlId) {
            $odrDtlObj = OrderDtl::where('odr_dtl_id', $odrDtlId)->first();
            $eventData[] = [
                'whs_id'     => object_get($orderHdr, 'whs_id'),
                'cus_id'     => object_get($orderHdr, 'cus_id'),
                'owner'      => object_get($orderHdr, 'odr_num'),
                'evt_code'   => Status::getByKey("Event", "PACKING"),
                'trans_num'  => object_get($orderHdr, 'odr_num'),
                'created_at' => time(),
                'created_by' => $userId,
                'info'       => sprintf('%s - Auto pack %s', object_get($orderHdr, 'odr_num'), object_get
                    ($odrDtlObj, 'sku') . "-" . object_get($odrDtlObj, 'lot'))
            ];


            $packDtlObj = PackDtl::where('odr_hdr_id', $odrId)
                ->where([
                    'item_id' => $odrDtlObj->item_id,
                    'lot'     => $odrDtlObj->lot,
                ])
                ->first();
            if ($packDtlObj) {
                $msg = "Item(s) was Packed";
                throw new HttpException(400, $msg);
            }
        }


        /*
         * get order carton with pack = picked qty by item
         * create pack
         * create event
         * save pack
         * save event
         * change order status
         */

        $orderCarton = new OrderCartonModel();

        $eventTrackingModel = new EventTrackingModel(new EventTracking());


        $data = $orderCarton->getCartonByOdrDtl($odrDtlIds);

        $packHdr = [];
        $packDtl = [];


        $seq = 0;

        foreach ($data as $carton) {


            $pack_type = "CT";
            $width = array_get($carton, 'width', 0) ?? 0;
            $height = array_get($carton, 'height', 0) ?? 0;
            $length = array_get($carton, 'length', 0) ?? 0;
            $weight = array_get($carton, 'weight', 0) ?? 0;

            if (!$width || !$height || !$length || !$weight){
                $item = DB::table('item')->where('item_id', $carton['item_id'])->where('deleted', 0)->first();
                $width = array_get($item, 'width', 0) ?? 0;
                $height = array_get($item, 'height', 0) ?? 0;
                $length = array_get($item, 'length', 0) ?? 0;
                $weight = array_get($item, 'weight', 0) ?? 0;
            }

            $pack_ref_id = $this->insertDimension($length, $width, $height, $pack_type);

            $packHdr[] = [
                'pack_hdr_num'     => $carton['ctn_num'],
                'odr_hdr_id'       => $odrId,
                'cnt_id'           => $carton['ctn_id'],
                'whs_id'           => $carton['whs_id'],
                'cus_id'           => $carton['cus_id'],
                'seq'              => ++$seq,
                'carrier_name'     => '',
                'sku_ttl'          => 0,
                'piece_ttl'        => $carton['piece_qty'],
                'pack_sts'         => 'NW',
                'created_at'       => time(),
                'updated_at'       => time(),
                'created_by'       => Data::getCurrentUserId(),
                'updated_by'       => Data::getCurrentUserId(),
                'deleted_at'       => $carton['deleted_at'],
                'sts'              => 'I',
                'ship_to_name'     => '',
                'out_plt_id'       => null,
                'pack_type'        => $pack_type,
                'width'            => $width,
                'height'           => $height,
                'length'           => $length,
                'weight'           => $weight,
                'pack_ref_id'      => $pack_ref_id,
                'deleted'          => 0,
                'is_print'         => 0,
                //'pack_dt_checksum' => $carton['item_id'],
                'pack_dt_checksum' => $carton['item_id'] . "-" . $carton['lot'],
                'item_id'          => $carton['item_id'],
                'sku'              => $carton['sku'],
                'size'             => $carton['size'],
                'color'            => $carton['color'],
                'lot'              => $carton['lot'],
                'cus_upc'          => $carton['upc']

            ];
        }
        $size = 1500;
        //auto pack process
        if (!empty($packHdr)) {
            $isFirst = true;
            foreach (array_chunk($packHdr, $size) as $key => $pack) {
                if ($key > 0) {
                    $isFirst = false;
                }
                DB::table('pack_hdr')->insert($pack);
                $this->updateOrderStatus($odrId, $eventData, $isFirst);
            }
        }

        //insert pack_dtl
        DB::statement("
            INSERT INTO pack_dtl
            (SELECT NULL AS pack_dtl_id,
                p.pack_hdr_id,
                p.odr_hdr_id,
                c.whs_id,
                c.cus_id,
                c.item_id,
                c.size, c.lot, c.sku, c.upc AS cus_upc, c.uom_id, SUM(c.piece_qty) AS piece_qty,
                p.created_at,
                p.created_by,
                p.updated_by,
                p.updated_at,
                p.deleted_at,
                p.deleted,
                p.sts,
                c.color,
                c.weight
                FROM pack_hdr p
                JOIN odr_cartons c ON c.ctn_id = p.cnt_id
                WHERE p.cnt_id IS NOT NULL
                AND p.sku_ttl = 0
                AND p.odr_hdr_id = $odrId
                GROUP BY p.cnt_id
                )
        ");

        DB::table('pack_hdr')->where('odr_hdr_id', $odrId)
            ->update(['sku_ttl' => 1]);

        $eventTrackingModel->createBatch($eventData);
    }

    public function insertDimension($length, $width, $height, $pack_type)
    {
        $key = $pack_type . $length . $width . $height;

        if (!array_key_exists($key, $this->packRef)) {
            $dimension = sprintf("%sx%sx%s", $width, $length, $height);
            $packRefModel = new PackRefModel();
            $result = $packRefModel->getFirstWhere([
                'dimension' => $dimension,
                'pack_type' => $pack_type

            ]);
            if (empty($result)) {
                $packRefModel = new PackRefModel();
                return null;
                $result = $packRefModel->create([
                    'width'      => $width,
                    'height'     => $height,
                    'length'     => $length,
                    'dimension'  => $dimension,
                    'pack_type'  => $pack_type,
                    'created_at' => time(),
                    'updated_at' => time()
                ]);
            }
            $this->packRef[$key] = object_get($result, 'pack_ref_id');
        }

        return $this->packRef[$key];
    }

    /**
     * @param $orderHdrId
     * @param $events
     * @param bool|true $isCreate
     * @param bool|false $isFirst
     */
    public function updateOrderStatus($orderHdrId, &$events, $isCreate = false)
    {
        $orderHdrModel = new OrderHdrModel();
        $wavePickDtlModel = new WavePickDtlModel();
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        // Order
        $orderHdr = $orderHdrModel->getFirstBy("odr_id", $orderHdrId, ['details'])->toArray();

        //  Compare act_piece_qty and
        $wvDtls = $wavePickDtlModel->getActualQtyByWvIds($orderHdr['wv_id']);
        $orderPieceQty = array_sum(array_pluck($wvDtls, 'act_piece_qty'));

        $packHdr = $this->getSumPieceByOrder([$orderHdrId]);
        $packTotalPiece = intval(object_get($packHdr, 'pieces_ttl'));

        $status = Status::getByValue("Packing", "Order-Status");
        //if ($orderHdr['odr_sts'] === Status::getByValue("Partial Picked", "Order-Status")) {
        //    $status = Status::getByValue("Partial Packing", "Order-Status");
        //}
        //$arrStsPar = [
        //    Status::getByValue("Partial Picked", "Order-Status"),
        //    Status::getByValue("Partial Packing", "Order-Status")
        //];
        //if (in_array($orderHdr['odr_sts'], $arrStsPar)) {
        //    $status = Status::getByValue("Partial Packing", "Order-Status");
        //}

        // Create Event Tracking
        if ($isCreate) {
            array_unshift($events, [
                'whs_id'     => $orderHdr['whs_id'],
                'cus_id'     => $orderHdr['cus_id'],
                'owner'      => $orderHdr['odr_num'],
                'evt_code'   => Status::getByKey("Event", "ORDER-PACKING"),
                'trans_num'  => $orderHdr['odr_num'],
                'created_at' => time(),
                'created_by' => $userId,
                'info'       => sprintf(Status::getByKey("Event-Info", "ORDER-PACKING"), $orderHdr['odr_num']),
            ]);
        }

        if ($orderPieceQty === $packTotalPiece) {
            $status = Status::getByValue("Packed", "Order-Status");
            //if ($orderHdr['odr_sts'] === Status::getByValue("Partial Packing", "Order-Status")
            //    || $orderHdr['odr_sts'] === Status::getByValue("Partial Picked", "Order-Status")
            //) {
            //    $status = Status::getByValue("Partial Packed", "Order-Status");
            //}
            //
            //if (in_array($orderHdr['odr_sts'], $arrStsPar)) {
            //    $status = Status::getByValue("Partial Packed", "Order-Status");
            //}

            // Create Evt tracking
            $events[] = [
                'whs_id'     => $orderHdr['whs_id'],
                'cus_id'     => $orderHdr['cus_id'],
                'owner'      => $orderHdr['odr_num'],
                'evt_code'   => Status::getByKey("Event", "ORDER-PACKING-COMPLETE"),
                'trans_num'  => $orderHdr['odr_num'],
                'created_at' => time(),
                'created_by' => $userId,
                'info'       => sprintf(Status::getByKey("Event-Info", "ORDER-PACKING-COMPLETE"), $orderHdr['odr_num']),
            ];
        }

        $orderHdrModel->refreshModel();
        $orderHdrModel->updateWhere([
            'odr_sts' => $status
        ], [
            'odr_id' => $orderHdrId
        ]);

        // Update sts odr_dtl
        $sqlStr = "
            odr_dtl.alloc_qty = (
                SELECT SUM(piece_qty)
                FROM pack_dtl
                WHERE pack_dtl.odr_hdr_id = odr_dtl.odr_id
                    AND pack_dtl.item_id = odr_dtl.item_id
                    AND pack_dtl.lot = odr_dtl.lot
            )

        ";
        DB::table('odr_dtl')
            ->where('odr_dtl.odr_id', $orderHdrId)
            ->where('odr_dtl.deleted', 0)
            ->whereRaw($sqlStr)
            ->update(['odr_dtl.itm_sts' => 'PA']);

        // Update sts odr_hdr
        $sqlStr = "
            0 = (
                SELECT COUNT(1) FROM odr_dtl
                WHERE odr_hdr.odr_id = odr_dtl.odr_id
                    AND odr_dtl.deleted = 0
                    AND odr_dtl.itm_sts != 'PA'
                    AND odr_dtl.lot!='Any'
            )
        ";
        DB::table('odr_hdr')
            ->where('odr_hdr.odr_id', $orderHdrId)
            ->whereRaw($sqlStr)
            ->update(['odr_hdr.odr_sts' => 'PA']);
    }

    public function updatePackedStatus($orderHdrId)
    {
        // Update sts odr_dtl
        $sqlStr = "
            odr_dtl.alloc_qty = (
                SELECT SUM(piece_qty)
                FROM pack_dtl
                WHERE pack_dtl.odr_hdr_id = odr_dtl.odr_id
                    AND pack_dtl.item_id = odr_dtl.item_id
                    AND pack_dtl.lot = odr_dtl.lot
            )

        ";
        DB::table('odr_dtl')
            ->where('odr_dtl.odr_id', $orderHdrId)
            ->where('odr_dtl.deleted', 0)
            ->whereRaw($sqlStr)
            ->update(['odr_dtl.itm_sts' => 'PA']);

        // Update sts odr_hdr
        $sqlStr = "
            0 = (
                SELECT COUNT(1) FROM odr_dtl
                WHERE odr_hdr.odr_id = odr_dtl.odr_id
                    AND odr_dtl.deleted = 0
                    AND odr_dtl.itm_sts != 'PA'
                    AND odr_dtl.lot!='Any'
            )
        ";
        DB::table('odr_hdr')
            ->where('odr_hdr.odr_id', $orderHdrId)
            ->whereRaw($sqlStr)
            ->update(['odr_hdr.odr_sts' => 'PA']);
    }

    /**
     * @param $odr_hdr_id
     * @param $whs_id
     * @return mixed
     */
    public function forceDeletePackHdr($odr_hdr_id, $whs_id)
    {
        $packHdrs = $this->model
            ->where('odr_hdr_id', $odr_hdr_id)
            ->where('whs_id', $whs_id)
            ->get();
        $packHdrIds = array_pluck($packHdrs, 'pack_hdr_id');

        return $this->model
            ->whereIn('pack_hdr_id', $packHdrIds)
            ->forceDelete();
    }
}
