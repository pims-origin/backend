<?php

namespace App\Jobs;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Artisan;

class AwmsForecastsJob extends Job
{
    /**
     * @var string
     */
    protected $name;

    /*
     * @var string
     */
    protected $system;

    /**
     * MappingJob constructor.
     * @param Schedule $schedule
     */
    public function __construct($name, $system = false)
    {
        $this->name = $name;
        $this->system = $system;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->system) {
            Artisan::call($this->name, [
                'system' => $this->system
            ]);
        } else {
            Artisan::call($this->name);
        }
    }
}
