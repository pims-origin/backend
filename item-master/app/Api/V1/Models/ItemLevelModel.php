<?php
/**
 * Created by PhpStorm.
 * User: Long Do
 * Date: 07-14-2017
 * Time: 13:48
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ItemLevel;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ItemLevelModel extends AbstractModel
{

    /**
     * ItemModel constructor.
     *
     * @param Item|null $model
     */
    public function __construct(ItemLevel $model = null)
    {
        $this->model = ($model) ?: new ItemLevel();
    }

    public function all(array $with = [])
    {
        $query = $this->make($with);
        // Get
        $models = $query->get();

        return $models;
    }

}
