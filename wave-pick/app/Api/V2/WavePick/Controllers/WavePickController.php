<?php

namespace App\Api\V2\WavePick\Controllers;

use App\Api\V2\WavePick\Models\CartonModel;
use App\Api\V2\WavePick\Models\CustomerConfigModel;
use App\Api\V2\WavePick\Models\LocationModel;
use App\Api\V2\WavePick\Models\OrderCartonModel;
use App\Api\V2\WavePick\Models\OrderDtlAllocationModel;
use App\Api\V2\WavePick\Models\WavePickDetailSuggestModel;
use App\Api\V2\WavePick\Models\WavePickDtlLocModel;
use App\Api\V2\WavePick\Models\WavePickDtlModel;
use App\Api\V2\WavePick\Models\WavePickHdrModel;
use Exception;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class WavePickController extends AbstractController
{
    /**
     * @var carton Model
     */
    protected $cartonModel;

    /**
     * @var location Model
     */
    protected $locationModel;

    /**
     * @var Customer Config Model
     */
    protected $customerConfigModel;

    /**
     * @var Order Carton Model
     */
    protected $orderCartonModel;

    /**
     * @var Wave pick Hdr Model
     */
    protected $wavePickHdrModel;

    /**
     * @var Wave pick Dtl Model
     */
    protected $wavePickDtlModel;

    /**
     * @var Wave Pick Dtl Loc Model
     */
    protected $wavePickDtlLocModel;

    protected $orderDtlAllocationModel;
    protected $wavePickDetailSuggestModel;


    /**
     * WavePickController constructor.
     */
    public function __construct()
    {
        $this->locationModel = new LocationModel();
        $this->cartonModel = new CartonModel();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->wavePickHdrModel = new WavePickHdrModel();
        $this->wavePickDtlModel = new WavePickDtlModel();
        $this->wavePickDtlLocModel = new WavePickDtlLocModel();
        //$this->orderDtlAllocationModel = new OrderDtlAllocationModel();
        $this->wavePickDetailSuggestModel = new WavePickDetailSuggestModel();
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function printWavepick(Request $request)
    {
        $input = $request->getQueryParams();

        $wave = $this->wavePickHdrModel->getFirstBy('wv_id', $input['wv_id'], ['pickerUser']);

        if (empty($wave)) {
            throw new \Exception(Message::get("BM017", "Wavepick"));
        }

        //Export list wv after picked (sts is complete) to Excel File
        if ($wave->wv_sts == 'CO') {
            return $this->printWvAfterPicked($wave);
        }

        return $this->printWvDtl($wave, $input['wv_id']);
    }

    /**
     * Export list wv_dtl to Excel File
     *
     * @param $wave
     * @param $wvId
     * @return mixed
     */
    protected function printWvDtl($wave, $wvId)
    {
        $titleExcel = [
            'wv'               => 'WV#',
            'picker'           => 'Picker',
            'sku'              => 'SKU',
            'description'      => 'Description',
            'batch'            => 'Batch',
            'pack'             => 'Pack',
            'uom'              => 'UOM',
            'allocated_ctns'   => 'Allocated CTNS',
            'allocated_qty_kg' => 'Allocated QTY/KG',
        ];
        $results = $this->wavePickDtlModel->findWhere([
            'wv_id' => $wave->wv_id
        ], ['item', 'picker']);
        $data = $results->transform(function($item){
            return [
                'wv'               => object_get($item, 'wv_num', null),
                'picker'           => $this->getWvDtlPickerName($item),
                'sku'              => object_get($item, 'sku', null),
                'description'      => object_get($item, 'item.description', null),
                'batch'            => object_get($item, 'lot', null),
                'pack'             => object_get($item, 'pack_size', null),
                'uom'              => object_get($item, 'item.uom_code', null),
                'allocated_ctns'   => object_get($item, 'ctn_qty', 0),
                'allocated_qty_kg' => object_get($item, 'piece_qty', 0)
            ];
        })->prepend($titleExcel)->toArray();

        //get data for Before Picking sheet in export Excel Wave Pick file
        $titleExcelBeforePicking = [
            'wv'               => 'WV#',
            'picker'           => 'Picker',
            'sku'              => 'SKU',
            'description'      => 'Description',
            'batch'            => 'Batch',
            'pack'             => 'Pack',
            'uom'              => 'UOM',
            'loc'              => 'Suggested Location',
            'allocated_ctns'   => 'CTNS',
            'allocated_qty_kg' => 'QTY/KG',
        ];
        $wavePickDetailSuggestInfo = $this->wavePickDtlLocModel
            ->getWavePickDetailLocation($wvId, ['waveDtl.item', 'waveDtl.picker']);

        $dataBeforePicking = $wavePickDetailSuggestInfo->transform(function($item){
            if($item->sug_loc_ids){
                $loc = Location::find($item->sug_loc_ids,['loc_code']);
            }
            return [
                'wv'          => $item->waveDtl->wv_num ?? '',
                'picker'      => $this->getWvDtlPickerName($item->waveDtl),
                'sku'         => $item->waveDtl->sku ?? '',
                'description' => $item->waveDtl->description ?? '',
                'batch'       => $item->waveDtl->lot ?? '',
                'pack'        => $item->waveDtl->pack_size ?? '',
                'uom'         => $item->waveDtl->item->uom_code ?? '',
                'loc'         => $loc->loc_code ?? '',
                'ctns'        => $item->waveDtl->ctn_qty ?? '',
                'qty_kg'      => $item->waveDtl->piece_qty ?? '',
            ];
        })->prepend($titleExcelBeforePicking)->toArray();

        return $this->writeExcelWvDtl($data, $dataBeforePicking);
    }

    /**
     * @param $dataWvDtl
     *
     * @return array
     */
    private function prepareDataForExcel($dataWvDtl)
    {
        $pickQty = array_map(function ($e) {
            return [
                'ITEM_ID'     => $e['ITEM_ID'],
                'SKU'         => $e['SKU'],
                'Color'       => $e['Color'],
                'SIZE'        => $e['SIZE'],
                'Lot'         => $e['Lot'],
                'Pack_size'   => $e['Pack_size'],
                'Cartons'     => $e['Cartons'],
                'Picking_QTY' => $e['Picking_QTY'],
                'primary_loc' => $e['primary_loc'],
                'bu_loc_1'    => $e['bu_loc_1'],
                'bu_loc_2'    => $e['bu_loc_2'],
                'bu_loc_3'    => $e['bu_loc_3'],
                'bu_loc_4'    => $e['bu_loc_4'],
                'bu_loc_5'    => $e['bu_loc_5'],
                'cus_id'      => $e['cus_id'],
                'wv_dtl_id'   => $e['wv_dtl_id'],
                'description' => $e['description'],
                'uom_code'    => $e['uom_code'],
                'wv_num'      => $e['wv_num'],
                'pick_pallet' => $e['pick_pallet'],

            ];
        }, $dataWvDtl);

        return $pickQty;
    }

    /**
     * @param $wvId
     * @param $dataWvDtls
     *
     * @return array
     */
    public function getLoationsNumberCartons($wvId, $dataWvDtls)
    {
        if (empty($dataWvDtls)) {
            return [];
        }
        $itemIds = [];
        $itemLots = [];

        foreach ($dataWvDtls as $dataWvDtl) {
            $itemIds[$dataWvDtl['ITEM_ID']] = $dataWvDtl['ITEM_ID'];
            $itemLots[$dataWvDtl['ITEM_ID']] = $dataWvDtl['Lot'];
        }


        //$wvDtlLocs = $this->wv_dtl_loc->getSugLocByWvId($wvId)->toArray();


        // Load data location for excel
        $wvHdrInfo = $this->wavePickHdrModel->getFirstWhere(['wv_id' => $wvId]);
        $wvSts = object_get($wvHdrInfo, 'wv_sts', '');
        if ($wvSts == 'CO') {
            $SugLocByWvIds = $this->wv_dtl_loc->getSugLocByWvId($wvId);
            $cartons = [];
            foreach ($SugLocByWvIds as $SugLocByWvId) {
                $act_loc_ids = (\GuzzleHttp\json_decode(array_get($SugLocByWvId, 'act_loc_ids', '')));
                foreach ($act_loc_ids as $act_loc_id) {
                    $cartons[] = [
                        "item_id"     => array_get($SugLocByWvId, 'item_id', ''),
                        "lot"         => array_get($SugLocByWvId, 'lot', 'NA'),
                        "loc_id"      => object_get($act_loc_id, 'loc_id', ''),
                        "loc_code"    => object_get($act_loc_id, 'loc_code', ''),
                        "plt_num"     => object_get($act_loc_id, 'plt_num', ''),//LPN
                        "numCarton"   => "getWvDtlLoc",//CTNS = QTY/pack size
                        "pieceRemain" => object_get($act_loc_id, 'avail_qty', ''),//QTY, pieceRemain
                        "picked_qty"  => object_get($act_loc_id, 'picked_qty', ''),//ActualQTY =>
                        //ActulCTNS = ActualQTY/packsize
                        "act_loc"     => object_get($act_loc_id, 'act_loc', ''),//Actual location
                        "act_loc_id"  => object_get($act_loc_id, 'act_loc_id', ''),

                    ];
                }

            }
        } else {
            $wvDtlLocs = $this->wavePickDtlLocModel->getSugLocIdByWvId($wvId)->toArray();

            $locIds = [];
            foreach ($wvDtlLocs as $wvDtlLoc) {
                $ids = array_filter(explode(',', $wvDtlLoc['sug_loc_ids']));
                $locIds = array_merge($locIds, $ids);
            }

            //$moreLocIds = [];
            foreach ($itemIds as $itemId) {
                $data = ['item_id' => $itemId, 'lot' => $itemLots[$itemId], 'is_ecom' => 0];
                $moreLoc = $this->cartonModel->getMoreSugLocByWvDtl($locIds, $data, 5);
                $moreLocIds = [];
                if (!empty($moreLoc)) {
                    $moreLocIds = array_merge(array_pluck($moreLoc, 'loc_id'));
                }
                $locIds = array_merge($locIds, $moreLocIds);
            }

            //if (!empty($moreLocIds)) {
            //    $cartons[] = $this->carton->getCartonForWavePick($moreLocIds, $itemIds);
            //}

            $cartons = $this->cartonModel->getCartonForWavePick($locIds, $itemIds);
        }

        $tempCartons = [];
        if ($cartons) {
            foreach ($cartons as $carton) {
                $tempCartons[$carton['item_id']][$carton['lot']][] = $carton;
            }
        }

        $locCode = array_pluck($cartons, 'loc_code');
        $tempCartons['loc_codes'] = $locCode;

        return $tempCartons;
    }

    /**
     * Export list wv after picked (sts is complete) to Excel File
     *
     * @param object $wavePickHdr
     *
     * @return mixed
     */
    protected function printWvAfterPicked($wave)
    {
        $data = [];
        $titleExcel = [
            'wv'              => 'WV#',
            'ord'             => 'ORD#',
            'plt_num'         => 'LPN',
            'sku'             => 'SKU',
            'description'     => 'Description',
            'batch'           => 'Batch',
            'uom'             => 'UOM',
            'pack'            => 'Pack',
            'picked_location' => 'Picked Location',
            'init_ctn_ttl'    => 'Initial CNTS',
            'ctns'            => 'CTNS',
            'qty_kg'          => 'QTY/KG',
            'picked_at'       => 'Picked At',
            'picked_by'       => 'Picked By'
        ];
        $results = $this->orderCartonModel->getDataAfterPicked($wave->wv_id);
        $data = [];
        foreach ($results as $result) {
            $sumPieceQty = object_get($result, 's_piece_qty', 0);

            if ($result['uom_code'] == 'KG') {
                $sumCnt = $result['countPlt'];
            } else {
                $sumCnt = ceil($sumPieceQty / $result['pack']);
            }

            $data[] = [
                'wv'              => object_get($result, 'wv_num', null),
                'ord'             => object_get($result, 'odr_num', null),
                'plt_num'         => object_get($result, 'plt_num', null),
                'sku'             => object_get($result, 'sku', null),
                'description'     => object_get($result, 'description', null),
                'batch'           => object_get($result, 'lot', null),
                'uom'             => object_get($result, 'uom_name', null),
                'pack'            => object_get($result, 'pack', null),
                'picked_location' => object_get($result, 'loc_code', null),
                'init_ctn_ttl'    => object_get($result, 'init_ctn_ttl', null),
                'ctns'            => $sumCnt,
                'qty_kg'          => $sumPieceQty,
                'picked_at'       => object_get($result, 'updated_at', null)->format('Y-m-d H:i:s'),
                'picked_by'       => object_get($result, 'picked_by', null)
            ];
        }

        $data = array_merge([$titleExcel], $data);

        return $this->writeExcel($data);
    }

    /**
     * @param array $data
     * @param array $dataBeforePicking
     * @param string $ext
     *
     * @return mixed
     */
    private function writeExcelWvDtl($data, $dataBeforePicking, $ext = 'xlsx')
    {
        return Excel::create('WavePick', function ($excel) use ($data, $dataBeforePicking) {
            $excel->sheet('Wave Pick', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
                $sheet->row(1, function ($row) {
                    $row->setFontWeight('bold');
                });
            });
            $excel->sheet('Before Picking', function ($sheet) use ($dataBeforePicking) {
                $sheet->fromArray($dataBeforePicking, null, 'A1', false, false);
                $sheet->row(1, function ($row) {
                    $row->setFontWeight('bold');
                });
            });
        })
            ->download($ext);
    }

    /**
     * @param array $data
     * @param string $ext
     *
     * @return mixed
     */
    private function writeExcel($data, $ext = 'xlsx')
    {
        Excel::create('WavePickAfterPicked', function ($excel) use ($data) {
            //Sheet: After Picked
            $excel->sheet('After Picked', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
                $sheet->row(1, function ($row) {
                    $row->setFontWeight('bold');
                });
            });


        })
            ->download($ext); //using
            //->store($ext, storage_path('excel/exports'));
    }

    /**
     * @param object $wave
     *
     * @return mixed
     */
    protected function getPickerBy($wave)
    {
        $firstName = object_get($wave, 'pickerUser.first_name', '');
        $lastName = object_get($wave, 'pickerUser.last_name', '');

        return $firstName . " " . $lastName;
    }

    private function getWvDtlPickerName($wvDtl) {
        $firstName = object_get($wvDtl, 'picker.first_name', '');
        $lastName = object_get($wvDtl, 'picker.last_name', '');

        return strtoupper($firstName . " " . $lastName);
    }
}
