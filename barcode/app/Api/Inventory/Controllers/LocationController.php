<?php

namespace App\Api\Inventory\Controllers;

use App\Api\Inventory\Models\CycleDtlModel;
use App\Api\Inventory\Models\LocationModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class LocationController extends AbstractController
{

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var Request
     */
    protected $request;

    /**
     * LocationController constructor.
     * @param LocationModel $model
     * @param Request $request
     */
    public function __construct(LocationModel $model, Request $request)
    {
        $this->locationModel = $model;
        $this->request = $request;
    }

    /**
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function filter()
    {
        $input = $this->request->getQueryParams();

        try {
            $condition = [
                'loc_whs_id' => $input['whs_id'],
                'term' => $input['term'],

            ];
            $data = $this->locationModel->filterLocation($condition, 10);
            if ($data) {
                $result = [];
                foreach ($data as $location) {
                    $result[$location->loc_id] = $location->loc_code;
                }
                return response()->json($result);
            }
            return response(Message::get('BM067', 'Data'), 404);
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }
}
