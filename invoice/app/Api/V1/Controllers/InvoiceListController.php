<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\InvoiceListModel;
use App\Api\V1\Transformers\InvoiceListTransformer;
use App\Api\V1\Validators\InvoiceListValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class CountryController
 *
 * @package App\Api\V1\Controllers
 */
class InvoiceListController extends AbstractController
{
    protected $invoiceListModel;
    protected $invoiceListValidator;
    protected $invoiceListTransformer;

    /*
    ****************************************************************************
    */

    public function __construct(IRequest $request)
    {
//        $object = new AuthenticationService($request);
//
//        parent::__construct($request, $object);

        $this->invoiceListModel = new InvoiceListModel();
        $this->invoiceListValidator = new InvoiceListValidator();
        $this->invoiceListTransformer = new InvoiceListTransformer();
    }

    /*
    ****************************************************************************
    */

	public function listInvoice(Request $request)
    {
        $input = $request->getQueryParams();

        //Sample Data
        $getVars = [
           'custID' => array_get($input, 'cus_id', NULL),
           'startDate' => $input['fromDate'],
           'endDate' => $input['toDate'],
           'sums' => TRUE,
           'whs_id' => $input['whs_id']
        ];

        try {
            // get list invoices
            $result = $this->invoiceListModel->updateInvoTables($getVars);

            $data = (object) [
                'invoices' => $result['return'],
                'items' => $result['items'],
                'details' => $result['details'],
                'invItemsIDs' => $result['invItemsIDs']
            ];

            return $this->response->item($data, new InvoiceListTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
    ****************************************************************************
    */

    public function billableCustomer(Request $request)
    {
        $input = $request->getQueryParams();

        $whs_id = $input['whs_id'];

        $billableCusts = $this->invoiceListModel->getBillableCusts($whs_id);

        return $billableCusts;

    }


    /*
    ****************************************************************************
    */

    public function getInvoices(Request $request)
    {
        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', PAGING_LIMIT);

        $this->invoiceListTransformer->fromDate = array_get($input, 'fromDate');
        $this->invoiceListTransformer->toDate = array_get($input, 'toDate');

        $model = $this->invoiceListModel;
        $transformer = $this->invoiceListTransformer;

        try {

            $invoiceList = $model->search($input, [], $limit);

            return $this->response->paginator($invoiceList, $transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /*
    ****************************************************************************
    */

    /**
     * @request array
     * @return object
     */

    public function getCurrentRevenue(Request $request)
    {
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', PAGING_LIMIT);

        try {

            $revenueList =  $this->invoiceListModel->currentRevenueData($input, [], $limit);

            return $revenueList;

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));


    }
}
