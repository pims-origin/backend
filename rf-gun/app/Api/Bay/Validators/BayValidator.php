<?php

namespace App\Api\Bay\Validators;


class BayValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'bay_name'  => 'required|string',
            'bay_sts'   => 'required|string',
            'pos'       => 'required|integer',
            'line'      => 'required|integer'
        ];

    }


}
