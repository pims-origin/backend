<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12/13/2018
 * Time: 3:04 PM
 */

namespace App\Api\V1\Controllers;


use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Models\ReportLaborTrackingModel;
use App\Api\V1\Validators\ItemValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Inventories;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Models\SysBug;


class ItemController extends AbstractController
{
    protected $itemModel;
    protected $palSugLoc;
    protected $cartonModel;
    protected $itemValidator;
    protected $palletModel;
    protected $reportLaborTrackingModel;

    public function __construct (
        ItemModel     $itemModel,
        CartonModel   $cartonModel,
        ItemValidator $itemValidator,
        PalletModel $palletModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        ReportLaborTrackingModel $reportLaborTrackingModel
    ) {
        $this->itemModel = $itemModel;
        $this->palSugLoc = $palletSuggestLocationModel;
        $this->cartonModel = $cartonModel;
        $this->itemValidator = $itemValidator;
        $this->palletModel = $palletModel;
        $this->reportLaborTrackingModel = $reportLaborTrackingModel;
    }

    public function search(Request $request, $whsId) {
        try{
            $input = $request->getQueryParams();
            $input['limit'] = (!empty($input['limit']))?$input['limit']:20;

            if(empty($input['cus_id'])) {
                return $this->response->errorBadRequest("Cus_id is required!");
            }

            $result = $this->itemModel->search($input, $whsId);

            return [
                'data' => [
                    "items" => $result
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        }catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function validateSku(Request $request, $whsId) {
        try{
            $input = $request->getQueryParams();

            if(empty($input['cus_id'])) {
                return $this->response->errorBadRequest("Cus_id is required!");
            }

            if(empty($input['sku'])) {
                return $this->response->errorBadRequest("Sku is required!");
            }

            $result = $this->itemModel->validateSku($input);

            if(empty($result)) {
                return $this->response->errorBadRequest("This Sku does not exits!");
            }

            return [
                'data' => [
                    "items" => $result
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        }catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function validateUpc(Request $request, $whsId) {
        try{
            $input = $request->getQueryParams();

            if(empty($input['cus_id'])) {
                return $this->response->errorBadRequest("Cus_id is required!");
            }

            if(empty($input['upc'])) {
                return $this->response->errorBadRequest("Upc is required!");
            }

            $result = $this->itemModel->validateUpc($input);

            if(empty($result)) {
                return $this->response->errorBadRequest("This Upc does not exits!");
            }

            return [
                'data' => [
                    "items" => $result
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        }catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    public function store(Request $request, $whsId) {
        $input = $request->getParsedBody();
        try {
            $this->validateParams($input);

            $newItem = $this->itemModel->addNewItem($input);

            if(!empty($newItem['is_error']) && $newItem['is_error'] == 1) {
                return $this->response->errorBadRequest($newItem['message']);
            }

            return [
                "data" => [
                    "item"      => $newItem,
                    "message"   => "Item create successful"
                ]
            ];

        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getSkuAsn(Request $request,$whsId) {
        try {
            $input = $request->getQueryParams();

            if(empty($input['asn_hdr_id'])) {
                return $this->response->errorBadRequest("Field asn_hdr_id is required!!");
            }

            $palSugs = $this->cartonModel->getItemByAsn($input, $whsId)->toArray();

            $result = [];
            if(!empty($palSugs)) {
                foreach ($palSugs as $palSug) {
                    $result[] = [
                        'sku'       => $palSug['sku'],
                        'loc_code'  => $palSug['loc_code'],
                        'plt_num'   => $palSug['lpn_carton'],
                        'qty'       => $palSug['qty_ttl']
                    ];
                }
            }

            $this->setTrackingStartTime($input['asn_hdr_id']);

            return [
                "data" => [
                    "items" => $result,
                    "message" => "Get Successful"
                ]
            ];

        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function setTrackingStartTime($asnHdrId)
    {
        $predis = new Data;
        $userId = $predis->getCurrentUserId();
        $whsId = $predis->getCurrentWhsId();

        $asnHdr = DB::table('asn_hdr')
            ->where('deleted', 0)
            ->where('asn_hdr_id', $asnHdrId)
            ->first();

        $setData = [
            'user_id'       =>  $userId,
            'whs_id'        =>  $whsId,
            'cus_id'        =>  $asnHdr['cus_id'],
            'owner'         =>  $asnHdr['asn_hdr_num'],
            'trans_num'     =>  null,
            'trans_dtl_id'  =>  null,
            'end_time'      =>  null,
            'lt_type'       =>  'IB'
        ];

        $getStart = $this->reportLaborTrackingModel->getFirstWhere($setData, [], ['lt_id' => 'DESC']);
        // dd($getStart);

        if(empty($getStart)) {
            $setData['start_time'] = time();

            $this->reportLaborTrackingModel->refreshModel();
            $this->reportLaborTrackingModel->create($setData);
        }else{
            $getStart->update(['start_time' => time()]);
        }
        
    }

    public function createItemKit(Request $request,$whsId) {
        try {
            $input = $request->getParsedBody();

            $input['size'] = isset($input['size']) ? $this->normalizeNA($input['size']) : '';
            $input['color'] = isset($input['color']) ? $this->normalizeNA($input['color']) : '';
            $uomId = intval(array_get($input, 'uom_id'));

            // validation
            $this->itemValidator->validate($input);

            DB::beginTransaction();
            $params = [
                'description'   => array_get($input, 'description', ''),
                'sku'           => strtoupper(array_get($input, 'sku', '')),
                'size'          => array_get($input, 'size', ''),
                'color'         => array_get($input, 'color', ''),
                'cus_upc'       => array_get($input, 'cus_upc', ''),
                'uom_id'        => $uomId,
                'pack'          => empty($input['pack']) ? null : intval($input['pack']),
                'length'        => floatval(array_get($input, 'length',1)),
                'width'         => floatval(array_get($input, 'width',1)),
                'height'        => floatval(array_get($input, 'height',1)),
                'weight'        => floatval(array_get($input, 'weight',1)),
                'cus_id'        => intval($input['cus_id']),
                'status'        => array_get($input, 'status', config('constants.item_status.ACTIVE')),
                'condition'     => array_get($input, 'condition', ''),
                'spc_hdl_code'  => array_get($input, 'spc_hdl_code', 'NA'),
                'item_hier'     => array_get($input, 'item_hier', ''),
                'charge_by'     => array_get($input, 'charge_by', NULL)
            ];

            if(array_get($input,'charge_by') == 'CF'){
                if(empty($input['length']) || empty($input['width']) || empty($input['height'])){
                    return $this->response->errorBadRequest('Width - length - height must not empty.');
                }
            }

            //set NULL value for cus_upc
            if(empty($params['cus_upc'])) {
                $params['cus_upc'] = NULL;
            }

            $item = $this->itemModel->createOrUpdate($params);
            if(!empty($input['inner_item'])) {

            }

            DB::commit();


        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function checkSku(Request $request,$whsId) {
        try {
            $input = $request->getQueryParams();

            if(empty($input['sku']) && empty($input['upc'])) {
                return $this->response->errorBadRequest("Field sku/upc is required!!");
            }

            $checkWhere =[];
            if(!empty($input['sku'])) {
                $checkWhere['sku'] = $input['sku'];
            }

            if(!empty($input['upc'])) {
                $checkWhere['cus_upc'] = $input['upc'];
            }

            if(!$this->itemModel->checkWhere($checkWhere)) {
                return $this->response->errorBadRequest("Item does not exits!!!");
            }

            $result = $this->cartonModel->checkSku($input, $whsId);

            return [
                "data" => [
                    "items"     => $result,
                    "message"   => "Get Succesful"
                ]
            ];

        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @return CartonModel
     */
    public function getItemByPlt(Request $request,$whsId)
    {
        try {
            $input = $request->getParsedBody();
            $input['whs_id'] = $whsId;
            $this->validateItemByPlt($input);

            $result = $this->cartonModel->getCartonByPlt4PW($input, $whsId)->toArray();

            return [
                "data" => [
                    "items"     => $result,
                    "message"   => "Get Succesful"
                ]
            ];

        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function validateParams($attribute) {
        if(empty($attribute['sku']) && empty($attribute['upc']) && empty($attribute['description'])) {
            return $this->response->errorBadRequest("There are aleast input sku, upc, description!");
        }

        if(empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest("Cus_id is required");
        }

        if(!empty($attribute['sku'])) {
            if(!empty($this->itemModel->validateSku($attribute))) {
                $msg = sprintf("This SKU %s have akready exits!", $attribute['sku']);
                return $this->response->errorBadRequest($msg);
            }
        }

        if(!empty($attribute['upc'])) {
            if(!empty($this->itemModel->validateUpc($attribute))) {
                $msg = sprintf("This UPC %s have akready exist!", $attribute['upc']);
                return $this->response->errorBadRequest($msg);
            }
        }
    }

    protected function validateItemByPlt($attribute) {
        if(empty($attribute['plt_num'])) {
            return $this->response->errorBadRequest("Pallet num is required");
        }

        if(!$this->palletModel->validateLPNFormat($attribute['plt_num'])) {
            return $this->response->errorBadRequest("Pallet num is invalid");
        }

    }

    private function normalizeNA($str)
    {
        $str = trim($str);
        if (strtoupper($str) === 'NA' || empty($str)) {
            return 'NA';
        }

        return $str;
    }

    public function getItemByLpn(Request $request,$whsId)
    {
        try {
            $input = $request->getQueryParams();
            if(empty($input['item_id'])) {
                return $this->response->errorBadRequest("Item id is required!");
            }
            if(empty($input['lpn_carton'])) {
                return $this->response->errorBadRequest("Lpn carton is required!");
            }

            $isLpn = $this->cartonModel->getModel()->where("lpn_carton", $input['lpn_carton'])
                                        ->where("whs_id", $whsId)
                                        ->where("cus_id",$input['cus_id'])
                                        ->first();

            if(empty($isLpn)) {
                return $this->response->errorBadRequest("Lpn carton is invalid!");
            }
            $isCarton = $this->cartonModel->getModel()->where("lpn_carton", $input['lpn_carton'])
                                        ->where("whs_id", $whsId)
                                        ->where("item_id", $input['item_id'])
                                        ->where("cus_id",$input['cus_id'])
                                        ->first();
            if(empty($isCarton)) {
                return $this->response->errorBadRequest("Carton is not exsit!");
            }

            $invItems = Inventories::where("item_id", $input['item_id'])
                ->where("whs_id", $whsId)->where("cus_id",$input['cus_id'])->get();

            $sum_pick_able = (!empty($invItems[0]->in_hand_qty))?$invItems[0]->in_hand_qty:0;
            return [
                "data" => [
                    "avail_qty"     => $sum_pick_able,
                    "message"   => "Get Succesful"
                ]
            ];

        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}