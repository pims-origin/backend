<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput', 'setWarehouseTimezone'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () {
            return ['Seldat API V1'];
        });

        //asn
        $api->get('/asns', ['action' => "viewASN", 'uses' => 'AsnController@searchCanSort']);
        $api->post('/asns-count', ['action' => "viewASN", 'uses' => 'AsnController@countSearchCanSort']);
        $api->post('asns', ['action' => "createASN", 'uses' => 'AsnController@store']);
        $api->post('asns/send-mail', ['action' => "createASN", 'uses' => 'AsnController@sendMailToAE']);
        $api->post('asns/change-customer', ['action' => "createASN", 'uses' => 'AsnController@correctData']);
        $api->post('asns/multi', ['action' => "createASN", 'uses' => 'AsnController@multiStore']);
        $api->put('asns/{asnID:[0-9]+}/containers/{ctnID:[0-9]+}',
            ['action' => "editASN", 'uses' => 'AsnController@update']);
        $api->get('asns/{asnID:[0-9]+}/containers/{ctnID:[0-9]+}',
            ['action' => "viewASN", 'uses' => 'AsnController@show']);
        $api->get('asns/{asnID:[0-9]+}/list-containers',
            ['action' => "viewASN", 'uses' => 'AsnController@listContainers']);
        $api->get('/asns/{asnID:[0-9]+}/containers/{ctnID:[0-9]+}/print',
            ['action' => "viewASN", 'uses' => 'AsnController@printASN']);
        $api->delete('/asns/{asnID:[0-9]+}',
            ['action' => "deleteASN", 'uses' => 'AsnController@destroy']);
        $api->put('asns/ref-code',
            ['action' => "editASN", 'uses' => 'AsnController@updateRefCode']);

        //Print LPN for Warehouse
        $api->get('/warehouse/print-lpn',
            ['action' => 'viewASN', 'uses' => 'AsnController@printLPN']);
        $api->get('/warehouse/lpn/show',
            ['action' => 'viewASN', 'uses' => 'AsnController@showLPN']);

        $api->get('asns/{asnId:[0-9]+}/{ctnId:[0-9]+}/print',
            ['action' => "viewASN", 'uses' => 'AsnController@printASNBarcode']);

        $api->get('asns/{asnID:[0-9]+}/cancel',
            ['action' => "cancelASN", 'uses' => 'AsnController@cancelASN']);

        //ASN complete receiving with actual date
        $api->put('/asns/{asnID:[0-9]+}/complete_receiving_asn',
            ['action' => "viewASN", 'uses' => 'AsnController@complete']);

        //ASN complete receiving with actual date
        $api->put('/asns/{asnID:[0-9]+}/revert_receiving_asn',
            ['action' => "viewASN", 'uses' => 'AsnController@revert']);

        //Carton RFID
        $api->get('asns-cartonrfids/{asnID:[0-9]+}/{vtlCtnrId:[0-9]+}',
            ['action' => "viewASN", 'uses' => 'AsnController@listASNDtlAndCartonVTL']);

        //Pallet RFID
        $api->get('asns-palletrfids/{asnID:[0-9]+}/{vtlCtnrId:[0-9]+}',
            ['action' => "viewASN", 'uses' => 'AsnController@listASNDtlAndPalletRFID']);

        //get Pallet by RFID - WMS2-4529
        $api->get('/palletrfid/{whsId:[0-9]+}',
            ['action' => "viewPallet", 'uses' => 'PalletController@getPalletByRFID']);

        //Tranfer pallet - WMS2-4529
        $api->post('/transfer-pallet/{whsId:[0-9]+}',
            ['action' => "viewPallet", 'uses' => 'PalletController@transferPallet']);

        $api->get('asns-errors-log/{whsId:[0-9]+}/{asnID:[0-9]+}',

            ['action' => "viewASN", 'uses' => 'AsnController@asnErrorsLog']);
        // Asn Status
        $api->get('/asn-statuses', ['action' => "viewASN", 'uses' => 'AsnStatusController@index']);

        // ASN type
        $api->get('/asn-types', ['action' => "viewASN", 'uses' => 'AsnController@asnType']);
        // Asn Status
        $api->get('/asns/dashboard', ['action' => "viewASN", 'uses' => 'AsnController@dashboard']);

        // GoodsReceipt
        $api->get('/goods-receipts', ['action' => "viewGoodsReceipt", 'uses' => 'GoodsReceiptController@search']);
        $api->get('/goods-receipts/export',
            ['action' => "viewGoodsReceipt", 'uses' => 'GoodsReceiptController@export']);
        $api->get('/goods-receipts/{goodsReceiptId:[0-9]+}',
            ['action' => "viewGoodsReceipt", 'uses' => 'GoodsReceiptController@show']);
        $api->post('/goods-receipts', ['action' => "createGoodsReceipt", 'uses' => 'GoodsReceiptController@store']);
        $api->post('/goods-receipts/approve', ['action' => "viewGoodsReceipt", 'uses' => 'GoodsReceiptController@approve']);
        $api->put('/goods-receipts/{goodsReceiptId:[0-9]+}/delete',
            ['action' => "softDeleteGoodsReceipt", 'uses' => 'GoodsReceiptController@deleteGoodsReceipt']);

        // Edit/Rescan LPN
        $api->put('/goods-receipts/{goodsReceiptId:[0-9]+}/rescan-lpn', ['action' => "createGoodsReceipt", 'uses' => 'GoodsReceiptController@rescanLPN']);

        // Print LPN
        $api->post('/goods-receipts/{goodsReceiptId:[0-9]+}/print-pallet-lpn', ['action' => "viewGoodsReceipt", 'uses' => 'PalletController@printPalletLPN']);

        /*-----------------TUYEN------------------------*/
        $api->get('/goods-receipts/{goodsReceiptId:[0-9]+}/search', ['action' => 'viewGoodsReceipt', 'uses' => 'GoodsReceiptCartonController@search']);
        //RMA
        $api->get('/asn/rma-orders', [
            'action' => 'viewASN',
            'uses'   => 'RMAController@getOrdersWithStatusShipped'
        ]);

        $api->post('/asn/rma-orders', [
            'action' => 'createASN',
            'uses'   => 'RMAController@createASN'
        ]);

        $api->get('/asn/rma/re-generate-billable-in', [
            'action'    => 'viewASN',
            'uses'      => 'RMAController@reGenerateBillableIn'
        ]);

        $api->get('/asn/rma/re-generate-billable-out', [
            'action'    => 'viewASN',
            'uses'      => 'RMAController@reGenerateBillableOut'
        ]);
        /*----------------END TUYEN---------------------*/

        $api->put('/goods-receipts/{goodsReceiptId:[0-9]+}',
            ['action' => "editGoodsReceipt", 'uses' => 'GoodsReceiptController@update']);
        $api->delete('/goods-receipts/{goodsReceiptId:[0-9]+}',
            ['action' => "deleteGoodsReceipt", 'uses' => 'GoodsReceiptController@destroy']);
        $api->delete('/goods-receipts',
            ['action' => "deleteGoodsReceipt", 'uses' => 'GoodsReceiptController@deleteMass']);
        $api->get('/goods-receipt/getGrType',
            ['action' => 'viewGoodsReceipt', 'uses' => 'GoodsReceiptController@getGrType']);
        //xdock process
        $api->get('/goods-receipts/assign-xdoc/{grHdrId:[0-9]+}',
            ['action' => "viewGoodsReceipt", 'uses' => 'GoodsReceiptController@assignXdocForm']
        );
        $api->put('/goods-receipts/update-xdoc',
            ['action' => "editGoodsReceipt", 'uses' => 'GoodsReceiptController@updateXdoc']);
        $api->get('/goods-receipts/xdoc-location/{cusId:[0-9]+}',
            ['action' => "viewGoodsReceipt", 'uses' => 'GoodsReceiptController@xdocLocation']
        );
        $api->put('/goods-receipts/assign-xdoc/{grHdrId:[0-9]+}',
            ['action' => "editGoodsReceipt", 'uses' => 'GoodsReceiptController@assignXdoc']);

        //damaged carton for Goods Receipt detail
        $api->get('/damage-carton/{goodsReceiptId:[0-9]+}/list', [
            'action' => "editGoodsReceipt",
            'uses'   => 'GoodsReceiptController@DamagedCartonList'
        ]);
        $api->put('/damage-carton/unset', [
            'action' => "editGoodsReceipt",
            'uses'   => 'GoodsReceiptController@UnSetDamagedCarton'
        ]);
        $api->put('/damage-carton/update', [
            'action' => "editGoodsReceipt",
            'uses'   => 'GoodsReceiptController@UpdateDamagedCarton'
        ]);

        // Goods receipt status
        $api->get('/gr-statuses', ['action' => "viewGoodsReceipt", 'uses' => 'GoodReceiptStatusController@index']);

        // container
        $api->get('/containers', ['action' => "viewContainer", 'uses' => 'ContainerController@search']);

        //Location
        $api->get('/locations/{locationId:[0-9]+}',
            ['action' => "consolidate", 'uses' => 'CartonController@viewLocationDetail']);

        // Relocation
//        $api->put('/relocations', ['action' => "relocation", 'uses' => 'PalletController@update']);
        $api->put('/relocations', ['action' => "relocation", 'uses' => 'PalletController@relocate']);
        $api->get('/relocations/{locationId:[0-9]+}',
            ['action' => "relocation", 'uses' => 'CartonController@getItemsList']);
        $api->get('/relocations/lpn/{lpn}',
            ['action' => "relocation", 'uses' => 'CartonController@getLPNItemsList']);
        $api->put('/locations/{locationId:[0-9]+}/changeLocationStatus',
            ['action' => "relocation", 'uses' => 'CartonController@changeLocationStatus']);
        $api->put('/locations/{locationId:[0-9]+}/overWriteNewLocation',
            ['action' => "relocation", 'uses' => 'PalletController@overWriteNewLocation']);
        $api->get('/lpns/autocomplete',
            ['action' => "relocation", 'uses' => 'PalletController@lpnAutoComplete']);

        // Consolidation
        $api->put('/consolidation_loc', ['action' => "consolidate", 'uses' => 'PalletController@consolidationLoc']);
        $api->put('/consolidation_plt', ['action' => "consolidate", 'uses' => 'PalletController@consolidationPlt']);
        $api->get('/consolidation_loc/{locationId:[0-9]+}',
            ['action' => "consolidate", 'uses' => 'CartonController@getCartonListByLocation']);
        $api->get('/consolidation_plt/{lpnCarton}',
            ['action' => "consolidate", 'uses' => 'PalletController@getCartonListByPallet']);

        // Assign Pallet
        $api->post('/goods-receipts/{goodsReceiptId:[0-9]+}/pallets',
            ['action' => "saveAssignPallet", 'uses' => 'PalletController@store']);
        $api->get('/goods-receipts/{goodsReceiptId:[0-9]+}/print',
            ['action' => "viewPutaway", 'uses' => 'PalletController@printPutAway']);
        //$api->get('/goods-receipts/{goodsReceiptId:[0-9]+}/print-lpn',
        //    ['action' => "viewPutaway", 'uses' => 'PalletController@printLpn']);

        //routes
        $api->get('/pallet/reports/{palletId:[0-9]+}/cartons-list',
            ['action' => "consolidate", 'uses' => 'PalletController@getCartonListByPalletForPalletReport']);

        // Carton
        $api->post('/damage-carton', ['action' => "editGoodsReceipt", 'uses' => 'CartonController@saveDamageCarton']);
        $api->post('/damage-carton/print/{goodsReceiptId?}',
            ['action' => "viewGoodsReceipt", 'uses' => 'CartonController@printDamageCarton']);
        $api->get('/damage-carton/{goodsReceiptId:[0-9]+}',
            ['action' => "editGoodsReceipt", 'uses' => 'CartonController@showDamageItem']);
        $api->get('/damage-carton/nextcarton/{cartonId:[0-9]+}/gr-dtl-id/{goodsReceiptDetailId:[0-9]+}',
            ['action' => "editGoodsReceipt", 'uses' => 'CartonController@showNextCarton']);
        $api->get('/carton-list', ['action' => "editGoodsReceipt", 'uses' => 'CartonController@cartonList']);

        // Damage Type
        $api->get('/damage-type', ['action' => "viewDamageType", 'uses' => 'DamageTypeController@search']);
        $api->get('/damage-type/{dmgTypeId:[0-9]+}',
            ['action' => "viewDamageType", 'uses' => 'DamageTypeController@show']);
        $api->post('/damage-type', ['action' => "createDamageType", 'uses' => 'DamageTypeController@store']);
        $api->put('/damage-type/{dmgTypeId:[0-9]+}',
            ['action' => "editDamageType", 'uses' => 'DamageTypeController@update']);
        $api->delete('/damage-type/{dmgTypeId:[0-9]+}',
            ['action' => "deleteDamageType", 'uses' => 'DamageTypeController@destroy']);

        //putaway
        $api->get('/putaway', ['action' => "viewPutaway", 'uses' => 'PutawayController@search']);
        $api->get('/putaway/itemupdate/{goodsReceiptId:[0-9]+}',
            ['action' => "viewPutaway", 'uses' => 'PutawayController@itemsUpdateFromPalSugLoc']);
        $api->put('/putaway/itemupdate/{goodsReceiptId:[0-9]+}',
            ['action' => "updateInventory", 'uses' => 'PutawayController@update']);
        $api->post('/putaway/{whsId:[0-9]+}/{cusId:[0-9]+}/check-locations',
            ['action' => "updateInventory", 'uses' => 'PutawayController@checkLocations']);

        // CSR
        $api->put('/putaway/assign-putter',
            ['action' => "putawayAssignment", 'uses' => 'PutawayController@updatePutter']);

        //pallet
        $api->get('/pallets', ['action' => "viewPallet", 'uses' => 'PalletController@search']);
        $api->put('/updatePallets', ['action' => "relocation", 'uses' => 'PalletController@updatePallet']);
        $api->get('/pallet-list', ['action' => "viewPallet", 'uses' => 'PalletController@palletList']);

        //Event Tracking/lookup
        $api->get('/eventracking',
            ['action' => "inboundEventTracking", 'uses' => 'EventTrackingController@search']);
        $api->get('/event-code-list',
            ['action' => "inboundEventTracking", 'uses' => 'EventLookupController@search']);
        $api->get('/eventracking/indicator-drop-downs',
            ['action' => "inboundEventTracking", 'uses' => 'EventTrackingController@indicatorDropDown']);
        $api->get('/eventracking/auto-owner',
            ['action' => "inboundEventTracking", 'uses' => 'EventTrackingController@autoOwner']);
        // Evt Reports
        $api->get('/event-reports/{userId:[0-9]+}',
            ['action' => "inboundEventTracking", 'uses' => 'EventTrackingController@report']);

        // Evt Reports
        $api->get('/performance-reports/{userId:[0-9]+}',
            ['action' => "inboundEventTracking", 'uses' => 'EventTrackingController@performance']);

        $api->get('/eventracking/users',
            ['action' => "inboundEventTracking", 'uses' => 'EventTrackingController@getListUser']);

        // Dashboard
        $api->get('/goodReceiptDashboard',
            ['action' => "inboundEventTracking", 'uses' => 'DashBoardController@goodReceiptDashboard']);

        $api->get('/cartons-damage/containers',
            ['action' => "inboundEventTracking", 'uses' => 'DamageController@containers']);

        $api->get('/cartons-damage/good-receipts',
            ['action' => "inboundEventTracking", 'uses' => 'DamageController@goodReceipts']);

        $api->get('/cartons-damage/damage-type',
            ['action' => "inboundEventTracking", 'uses' => 'DamageController@damageType']);

        $api->get('/cartons-damage/sku',
            ['action' => "inboundEventTracking", 'uses' => 'DamageController@sku']);

        $api->get('/cartons-damage/search',
            ['action' => "inboundEventTracking", 'uses' => 'DamageController@search']);

        $api->get('/cartons-damage/export-csv',
            ['action' => "inboundEventTracking", 'uses' => 'DamageController@exportCsv']);

        //
        //$api->get('/whs/{whsId:[0-9]+}/good-receipt-complete/{grHdrId:[0-9]+}',
        //    ['action' => "inboundEventTracking", 'uses' => 'GoodsReceiptController@complete']);
        $api->post('/whs/{whsId:[0-9]+}/good-receipt-complete/{grHdrId:[0-9]+}',
            ['action' => "inboundEventTracking", 'uses' => 'GoodsReceiptController@complete']);
        //Pallet tab in detail Good Receipt
        $api->get('/whs/{whsId:[0-9]+}/pallet-cartons/{grHdrId:[0-9]+}',
            ['action' => "viewGoodsReceipt", 'uses' => 'PalletController@showCartonsPallet']);

        $api->put('/whs/{whsId:[0-9]+}/pallet-cartons/{grHdrId:[0-9]+}',
            ['action' => "editGoodsReceipt", 'uses' => 'PalletController@updateCartonsOnPallet']);
        $api->put('/whs/{whsId:[0-9]+}/pallet-cartons/{grHdrId:[0-9]+}/location',
            ['action' => "editGoodsReceipt", 'uses' => 'PalletController@updateCurrentLocationOnPallet']);
        $api->get('/whs/{whsId:[0-9]+}/auto-avail-location',
            ['action' => "editGoodsReceipt", 'uses' => 'PalletController@autoAvailLocbyRackType']);

        $api->get('/loc-lpn', ['action' => "viewPallet", 'uses' => 'CartonController@searchLpnLoc']);
        $api->get('/to-loc-lpn', ['action' => "viewPallet", 'uses' => 'CartonController@searchLpnLocation']);

        //consolidate
//        $api->put('/consolidation', ['action' => "consolidate", 'uses' => 'PalletController@consolidation']);
        $api->put('/consolidation', ['action' => "consolidate", 'uses' => 'PalletController@consolidate']);

        //insert cartons
        $api->get('/goods-receipts/cartons', ['action' => "viewGoodsReceipt", 'uses' => 'GoodsReceiptController@insertCartons']);
    });
});