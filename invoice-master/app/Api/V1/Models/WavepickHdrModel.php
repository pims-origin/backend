<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use App\WavepickDtl;
use App\WavepickHdr;
use App\Wms2\UserInfo\Data;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class WavepickHdrModel extends AbstractModel
{
    /**
     * OrderHdrModel constructor.
     *
     * @param WavepickHdr|null $model
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }
    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);

        $searchEqualFields = [

        ];

        $searchLikeFields = [
            'wv_num'
        ];

        foreach ($attributes as $key => $value) {
            //search equal
            if (in_array($key, $searchEqualFields)) {
                if ($value != '') {
                    $query->where($this->getTable().'.' . $key, $value);
                }
            }
            //search like
            if (in_array($key, $searchLikeFields)) {
                if ($value != '') {
                    $query->where($this->getTable().'.' . $key, 'like', '%' . escape_like($value) . '%');
                }
            }
        }
        //filter Order that wv_num not in ref_num_arr array
        if (!empty($attributes['ref_num_arr'])) {
            $query->whereNotIn($this->getTable().'.wv_num', $attributes['ref_num_arr']);
        }

        //Filter whs_id and cus_id
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where($this->getTable().'.whs_id', $currentWH);
        $cus_ids = $predis->getCurrentCusIds();
        $query->whereHas('details', function ($query) use ($cus_ids, $attributes){
            $query->whereIn((new WavepickDtl())->getTable().'.cus_id', $cus_ids);
            if (isset($attributes['cus_id']) && $attributes['cus_id'] != '') {
                $query->where((new WavepickDtl())->getTable().'.cus_id', $attributes['cus_id']);
            }
        });

        $models = $query->paginate($limit);

        return $models;
    }
}
