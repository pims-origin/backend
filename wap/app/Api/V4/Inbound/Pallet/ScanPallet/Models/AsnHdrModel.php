<?php

namespace App\Api\V4\Inbound\Pallet\ScanPallet\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;


class AsnHdrModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnHdr();
    }

    /**
     * @return mixed
     */
    public function getLatestAsnNumber()
    {
        return $this->model->orderBy('asn_hdr_num', 'desc')->take(1)->get()->first();
    }

    public function search($attributes, array $with, $limit = 20)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        // search whs id
        if (isset($attributes['whs_id'])) {
            $query->where('whs_id', (int)$attributes['whs_id']);
        }

        // search customer
        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }

        // search container num
        $query->whereHas('asnDtl.container', function ($query) use ($attributes) {
            if (isset($attributes['ctnr_id'])) {
                $query->where('ctnr_id', (int)$attributes['ctnr_id']);
            }
        });

        // search asn status
        $query->where(function ($q) {
            $q->where('asn_sts', Status::getByKey("ASN_STATUS", "NEW"));
            $q->orWhere('asn_sts', Status::getByKey("ASN_STATUS", "RECEIVING"));
        });

        $query->groupBy('asn_hdr_id');

        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }

    /**
     * @param $asnHdrId
     * @return mixed
     * @Author:CuongNguyen
     */
    public function getASNHdrById ($asnHdrId) {
        return $this->model->where('asn_hdr_id', $asnHdrId)->first();
    }

    public function getASNReceiving($attributes, array $with, $limit = 20)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        // search whs id
        if (isset($attributes['whs_id'])) {
            $query->where('whs_id', (int)$attributes['whs_id']);
        }

        // search asn status
        $query->where(function ($q) {
            $q->orWhere('asn_sts', Status::getByKey("ASN_STATUS", "RECEIVING"));
        });

        $query->groupBy('asn_hdr_id');

        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }

    public function getFullASN($asn_hdr_id)
    {
        $query = $this->model
            ->select('asn_sts', 'asn_hdr_id', 'asn_hdr_ept_dt', 'asn_hdr_num', 'asn_hdr_ref',
                'cus_name', 'asn_sts_name', 'asn_hdr.created_at', 'customer.cus_id', 'whs_id')
            ->join('customer', 'customer.cus_id', '=', 'asn_hdr.cus_id')
            ->join('asn_status', 'asn_status.asn_sts_code', '=', 'asn_hdr.asn_sts')
            ->where('asn_hdr_id', $asn_hdr_id);

        return $query->first();
    }

    public function checkCompleted($asn_hdr_id) {
        $rs = AsnDtl::where([
            'asn_dtl.asn_hdr_id' => $asn_hdr_id,
        ])
            ->where('asn_dtl.asn_dtl_sts', '!=', 'RE' )
            ->count();
        return $rs > 0 ? false : true;
    }

    public function updateASNHdrStatusRG($asnHdrId)
    {
        $query = $this->model
            ->where('asn_hdr_id', $asnHdrId)
            ->update([
                    'asn_sts' => 'RG'
                ]
            );

        return (bool) $query;
    }

    public function updateASNHdrStatusRE($asnHdrId)
    {
        $ttlAsnDtlRESql = "(SELECT COUNT(asn_dtl.asn_dtl_id) FROM asn_dtl WHERE asn_dtl.asn_hdr_id = asn_hdr.asn_hdr_id AND asn_dtl.`asn_dtl_sts` != 'RE' ) = 0";
        $ttlAsnGrRESql = "(SELECT COUNT(DISTINCT(asn_dtl.ctnr_id)) FROM asn_dtl WHERE asn_dtl.asn_hdr_id = asn_hdr.asn_hdr_id AND asn_dtl.`asn_dtl_sts` = 'RE' )
        =(SELECT COUNT(gr_hdr.gr_hdr_id) FROM gr_hdr WHERE gr_hdr.asn_hdr_id = asn_hdr.asn_hdr_id AND gr_hdr.gr_sts = 'RE')";

        $res = $this->model
            ->where('asn_hdr_id', $asnHdrId)
            ->whereRaw($ttlAsnDtlRESql)
            ->whereRaw($ttlAsnGrRESql)
            ->update([
                'asn_sts'     => 'RE',
                'updated_by'  => $this->getUserId(),
                ]
            );

        return (bool) $res;
    }
}
