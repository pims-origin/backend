<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12/13/2018
 * Time: 3:04 PM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Transformers\CustomerTransformer;
use App\Api\V1\Transformers\GoodReceiptTransformer;
use App\Api\V1\Models\AsnHdrModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Models\SysBug;

class GoodReceiptController extends AbstractController
{
    protected $asnHdrModel;
    protected $customerModel;
    protected $containerModel;
    protected $goodsReceiptModel;
    protected $asnDtlModel;
    protected $itemModel;
    protected $eventTrackingModel;
    protected $palletModel;
    protected $cartonModel;

    public function __construct (
        AsnHdrModel     $asnHdrModel,
        CustomerModel   $customerModel,
        ContainerModel       $container,
        AsnDtlModel $asnDtlModel,
        ItemModel $itemModel,
        GoodsReceiptModel $goodsReceiptModel,
        PalletModel $palletModel,
        CartonModel $cartonModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->asnHdrModel = $asnHdrModel;
        $this->customerModel = $customerModel;
        $this->containerModel = $container;
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->asnDtlModel = $asnDtlModel;
        $this->itemModel = $itemModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->palletModel = $palletModel;
        $this->cartonModel = $cartonModel;

    }

    public function search(Request $request, $whsId, GoodReceiptTransformer $goodReceiptTransformer) {
        try{
            $input = $request->getQueryParams();
            $input['limit'] = (!empty($input['limit']))?$input['limit']:20;
            $result = $this->asnHdrModel->search($input, $whsId);

            return $this->response->collection($result, $goodReceiptTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        }catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function store(Request $request, $whsId) {
        $input = $request->getParsedBody();
        try {
            $this->validateRef($input);

            DB::beginTransaction();

            $user_id = Data::getCurrentUserId();
            $time = time();

            $asnNumAndSeq = $this->asnHdrModel->generateAsnNumAndSeq();
            $asnHdr = $this->asnHdrModel->create([
                'asn_hdr_seq'           => $asnNumAndSeq['asn_seq'],
                'asn_hdr_num'           => $asnNumAndSeq['asn_num'],
                'asn_hdr_ref'           => (!empty($input['ref_code']))? trim($input['ref_code']):"",
                'asn_hdr_ept_dt'        => strtotime($input['date']),
                'asn_hdr_act_dt'        => strtotime($input['date']),
                'cus_id'                => $input['cus_id'],
                'whs_id'                => $whsId,
                'asn_type'              => 'ASN',
                'asn_sts'               => 'NW',
                'sys_mea_code'          => 'IN'
            ]);

//           $ctnr_num = (!empty($input['container']))?$input['container']:"NA";
            $ctnr_num = $input['container'];
            $container = $this->containerModel->getFirstWhere(['ctnr_num' => $ctnr_num]);

            if(empty($container)) {
                $container = $this->containerModel->create(['ctnr_num' => $ctnr_num]);
            }

            // Do Good Receipt

            $grHdr = $this->goodsReceiptModel->getModel()
                ->where('asn_hdr_id', $asnHdr->asn_hdr_id)
                ->whereIn("gr_sts", ['RG','RE'])
                ->first();
            if(empty($grHdr)) {
                $grNumAndSeq = $this->generateGrNumAndSeq($asnHdr->asn_hdr_num, $asnHdr->asn_hdr_id);

                $grHdr = $this->goodsReceiptModel->create([
                    "ctnr_id"           => $container->ctnr_id,
                    "asn_hdr_id"        => $asnHdr->asn_hdr_id,
                    "gr_hdr_seq"        => $grNumAndSeq['gr_seq'],
                    "gr_hdr_ept_dt"     => $asnHdr->asn_hdr_ept_dt,
                    "gr_hdr_act_dt"     => $asnHdr->asn_hdr_ept_dt,
                    "gr_hdr_num"        => $grNumAndSeq['gr_num'],
                    "gr_sts"            => 'RG',
                    "whs_id"            => $asnHdr->whs_id,
                    "cus_id"            => $asnHdr->cus_id,
                    "ctnr_num"          => $container->ctnr_num,
                    "ref_code"          => (!empty($asnHdr->asn_hdr_ref))?$asnHdr->asn_hdr_ref:"",
                    'created_from'      => 'GUN',
                    "putaway"           => 0
                ]);
            }

            // Event Tracking
            $event[] = [
                'whs_id'    => $asnHdr->whs_id,
                'cus_id'    => $asnHdr->cus_id,
                'owner'     => $asnHdr->asn_hdr_num,
                'evt_code'  => 'ANW',
                'trans_num' => $asnHdr->asn_hdr_num,
                'info'      => sprintf("GUN - Create ASN %s", $asnHdr->asn_hdr_num),
                "created_at"            => $time,
                "created_by"            => $user_id,
            ];

            $event[] = [
                'whs_id'    => $grHdr->whs_id,
                'cus_id'    => $grHdr->cus_id,
                'owner'     => $grHdr->gr_hdr_num,
                'evt_code'  => 'GRP',
                'trans_num' => $container->ctnr_num,
                'info'      => sprintf("GUN - Create Good Receipt %s", $asnHdr->asn_hdr_num),
                "created_at"            => $time,
                "created_by"            => $user_id,
            ];

            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->getModel()->insert($event);


            $result =$this->transformAsn($asnHdr,$container);

            DB::commit();

            return [
                "data" => [
                    "asn"       => $result,
                    "message"   => "Submit Successfully"
                ]
            ];

        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function createAsnDetailOneStep(Request $request, $whsId) {
        $input = $request->getParsedBody();
        try {
            $this->validateAsnDetail($input);

            $result = $this->asnDtlModel->createAsnOneStep($input);

            if($result['is_error'] == 1) {
                return $this->response->errorBadRequest($result['msg']);
            }

            return [
                "data" => [
                    "item" => $result['items'],
                    "qty"   => $result["qty"],
                    "message" => $result['msg']
                ]
            ];

        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function createAsnDetailTwoStep(Request $request, $whsId) {
        $input = $request->getParsedBody();
        try {
            $this->validateAsnDetail($input);
            $result = $this->asnDtlModel->createAsnTwoStep($input);

            if($result['is_error'] == 1) {
                return $this->response->errorBadRequest($result['msg']);
            }

            return [
                "data" => [
                    "item" => $result['items'],
                    "qty"   => $result["qty"],
                    "message" => $result['msg']
                ]
            ];

        } catch(Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function getListCustomer(Request $request, $whsId, CustomerTransformer $customerTransformer) {
        try {
            $input = $request->getQueryParams();
            $input['limit'] = (!empty($input['limit']))?$input['limit']:20;
            $result = $this->customerModel->search($input, $whsId);

            return $this->response->collection($result, $customerTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        }catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    protected function validateRef($attribute) {
        if(empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest("The input cus_id is required!");
        }

        if(!empty($attribute['cus_id']) && !empty($attribute['ref_code'])) {
            $check = $this->asnHdrModel->getRefByCus($attribute);
            if(!empty($check)) {
                $msg = sprintf("There are have Asn ref %s by this customer",$attribute['ref_code']);
                return $this->response->errorBadRequest($msg);
            }
        }

        if(empty($attribute['container'])) {
            return $this->response->errorBadRequest("The input container is required!");
        }

        if (strlen($attribute['ref_code']) > 32) {
            return $this->response->errorBadRequest("The input ref is less than 32 characters!");
        }

        if (strlen($attribute['container']) > 14) {
            return $this->response->errorBadRequest("The input container is less than 14 characters!");
        }
    }


    protected  function transformAsn($asnHdr, $container) {
        $result = $asnHdr->toArray();
        $result['asn_hdr_ept_dt'] = ($asnHdr->asn_hdr_ept_dt)?date("m-d-Y",$asnHdr->asn_hdr_ept_dt):"";
        $result['asn_hdr_act_dt'] = ($asnHdr->asn_hdr_ept_dt)?date("m-d-Y",$asnHdr->asn_hdr_ept_dt):"";
        $result['ref_code'] = $asnHdr->asn_hdr_ref;
        $customer = $this->customerModel->getFirstWhere(["cus_id" => $asnHdr->cus_id]);
        $result['cus_name'] = $customer->cus_name;
        $result['cus_code'] = $customer->cus_code;
        $result['ctnr_id']  = $container->ctnr_id;
        $result['ctnr_num'] = $container->ctnr_num;
        $cus_meta = DB::table("cus_meta")->where("cus_id",$asnHdr->cus_id)->where("qualifier","IBC")->first();
        $result['step'] = 0;
        $result['expire_dt'] = false;
        if(!empty($cus_meta)) {
            $value = json_decode($cus_meta['value']);
            $result['step'] = (!empty($value->step))?$value->step:0;
            $result['expire_dt']  = boolval($value->expire_dt);
        }

        return $result;
    }

    protected function validateAsnDetail(&$attribute) {

        if(empty($attribute['asn_hdr_id'])) {
            return $this->response->errorBadRequest("Asn id is required");
        }

        if(empty($attribute['ctnr_num'])) {
            return $this->response->errorBadRequest("Container num is required");
        }

        $asnHdr = $this->asnHdrModel->getFirstWhere(["asn_hdr_id" => $attribute['asn_hdr_id']]);
        if(empty($asnHdr)) {
            $msg = sprintf("Asn with this customer does not exits");
            return $this->response->errorBadRequest($msg);
        }

        if(!in_array($asnHdr->asn_sts,['RG','NW'])) {
            $msg = sprintf("Asn status is not receiving");
            return $this->response->errorBadRequest($msg);
        }

        if(empty($attribute['item_id']) && empty($attribute['sku']) &&  empty($attribute['upc'])) {
            return $this->response->errorBadRequest("Item_id, Sku are required !");
        }

        if(empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest("Cus id is required");
        }

//        if(empty($attribute['spc_hdl_code'])) {
//            return $this->response->errorBadRequest("Special Handle Code is required");
//        }

        if(empty($attribute['item_id'])) {
            return $this->response->errorBadRequest("Item id is required");
        }

        $item_meta = DB::table('item')
                        ->join('item_meta', 'item_meta.itm_id', 'item.item_id')
                        ->where('item.item_id', $attribute['item_id'])
                        ->value('item_meta.value');
        $item_meta = json_decode($item_meta);
        $spc_hdl_code = [];
        if ($item_meta) {
            foreach ($item_meta as $key => $value) {
                if ($value == 1) {
                    $spc_hdl_code[] = $key;
                }
            }
        }

        if(empty($spc_hdl_code)) {
            return $this->response->errorBadRequest("Special Handle Code is required");
        }

        $item = $this->itemModel->getFirstWhere(["item_id" => $attribute['item_id']]);
        $item_id = $attribute['item_id'];
        $asnDtl = $this->asnDtlModel->findWhere(["asn_hdr_id" => $attribute['asn_hdr_id']])->toArray();
        $asnDtlId = array_column($asnDtl, "asn_dtl_id");
        $carton = $this->cartonModel->getModel()
            ->where("lpn_carton" , $attribute['plt_num'])
            ->where(function ($query) use ($item_id, $asnDtlId) {
                $query->where("item_id", "<>", $item_id);
                if(!empty($asnDtlId)){
                    $query->orWhereNotIn("asn_dtl_id", $asnDtlId);
                }
            })
            ->whereIn("ctn_sts", ["AC","RG", 'LK'])->first();
        if(!empty($carton)) {
            $msg = sprintf('The LPN %s has been used', $attribute['plt_num']);
            return $this->response->errorBadRequest($msg);
        }
        if(empty($item)) {
            return $this->response->errorBadRequest("Item is not exit!");
        }

        if(!empty($attribute['item_id'])) {
            $item = $this->itemModel->getFirstWhere(["item_id" => $attribute['item_id']]);
            if(empty($item->cus_upc) && !empty($attribute['upc'])) {
                $item->cus_upc = $attribute['upc'];
                $item->save();
            }

        }
//        else {
//            $checkParam = [
//                "cus_id" => $attribute['cus_id']
//            ];
//            if(!empty($attribute['sku'])) {
//                $checkParam['sku'] = $attribute['sku'];
//            }
//            if(!empty($attribute['upc'])) {
//                $checkParam['cus_upc'] = $attribute['upc'];
//            }
//
//            $newItem = $this->itemModel->getFirstWhere($checkParam);
//            if(empty($newItem)) {
//                $newItem = $this->itemModel->addNewItem($attribute);
//                if(!empty($newItem['is_error']) && $newItem['is_error'] == 1) {
//                    return $this->response->errorBadRequest($newItem['message']);
//                }
//            }
//            $attribute['item_id'] = $newItem->item_id;
//            $attribute['sku'] = $newItem->sku;
//            $attribute['upc'] = $newItem->cus_upc;
//        }

//        $cus_meta = DB::table("cus_meta")
//            ->where("cus_id", $attribute['cus_id'])
//            ->where("qualifier", "IBC")->first();
//        if(!empty($cus_meta)) {
//            $cus_config = \GuzzleHttp\json_decode($cus_meta['value']);
//            $cus_config = \GuzzleHttp\json_decode(json_encode($cus_config), true);
//            if($cus_config['step'] != $attribute['type']) {
//                $msg = sprintf("This Customer does not allow Goods Receipt %s Step",$attribute['type']);
//                return $this->response->errorBadRequest($msg);
//            }
////            if(!empty($cus_config['pallet']) && $cus_config['pallet'] == 1 && empty($attribute['plt_num'])) {
////                return $this->response->errorBadRequest("This Customer required pallet");
////            }
//            if(!empty($cus_config['lot']) && $cus_config['lot'] == 1 && empty($attribute['lot'])) {
//                return $this->response->errorBadRequest("This Customer required lot");
//            }
//            if(!empty($cus_config['expire_dt']) && $cus_config['expire_dt'] == 1 && empty($attribute['expired_dt'])) {
//                return $this->response->errorBadRequest("This Customer required expired date");
//            }
//        }

        if($attribute['type'] == 2) {
            $this->validateTwoStep($attribute,$asnHdr);
        }

        if($attribute['type'] == 1) {
            $this->validateOneStep($attribute, $asnHdr);
        }
    }

    protected function validateTwoStep($attribute, $asnHdr) {

        if(empty($attribute['plt_num'])) {
            return $this->response->errorBadRequest("Field plt_num is required!");
        }

        if($asnHdr->asn_type == "1ST") {
            $msg = sprintf("Asn is 1 step asn");
            return $this->response->errorBadRequest($msg);
        }

        if(!$this->palletModel->validateLPNFormat($attribute['plt_num'])) {
            $msg = sprintf("LPN code %s is invalid",$attribute['plt_num']);
            return $this->response->errorBadRequest($msg);
        }
        $pallet = DB::table("pallet")->where("plt_num", $attribute['plt_num'])
            ->where("whs_id", $asnHdr->whs_id)->where("deleted",0)->first();

        if(!empty($pallet->cus_id) && $pallet->cus_id != $asnHdr->cus_id && $pallet->plt_sts != 'NW') {
            $msg = sprintf("LPN %s is not belong to this customer",$attribute['plt_num']);
            return $this->response->errorBadRequest($msg);
        }
        $item_id = $attribute['item_id'];
        $carton = $this->cartonModel->getModel()
            ->where("lpn_carton", $attribute['plt_num'])
            ->where(function ($query) use ($item_id) {
                $query->where("item_id", "<>", $item_id);
                $query->orwhere(DB::raw('`gr_hdr_id` is not null'));
            })
            ->whereIn("ctn_sts", ["AC","RG", 'LK'])->first();
        if(!empty($carton)) {
            $msg = sprintf('The LPN %s has been used.', $attribute['plt_num']);
            return $this->response->errorBadRequest($msg);
        }

//        if(!empty($pallet['plt_id'])) {
//            if(!empty($carton)) {
//                $item = $this->itemModel->getFirstWhere(['item_id'=>$carton->item_id]);
//                if($item->spc_hdl_code != $attribute['spc_hdl_code']) {
//                    return $this->response->errorBadRequest("This Item has special handle code different with the item in this Pallet.");
//                }
//                return $this->response->errorBadRequest("This LPN has use to another item.");
//            }
//        }

    }

    protected function validateOneStep(&$attribute, $asnHdr) {

        if(!empty($attribute['plt_num'])) {
            if(!$this->palletModel->validateLPNFormat($attribute['plt_num'])) {
                $msg = sprintf("Pallet code %s is invalid",$attribute['plt_num']);
                return $this->response->errorBadRequest($msg);
            }
        }

        if($asnHdr->asn_type == "2ST") {
            $msg = sprintf("Asn is 2 step asn");
            return $this->response->errorBadRequest($msg);
        }

        if(empty($attribute['loc_code']) ) {
            return $this->response->errorBadRequest("Loc_code are required !");
        }

        $locationModel = new Location();
        $location = $locationModel
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where("loc_code", $attribute['loc_code'])->where("loc_whs_id", $asnHdr->whs_id)->first();

        if(empty($location)) {
            $msg = sprintf("Location %s does not exits",$attribute['loc_code']);
            return $this->response->errorBadRequest($msg);
        }

        if($location->loc_type_code != "RAC") {
            $msg = sprintf("Location %s is not storage type",$attribute['loc_code']);
            return $this->response->errorBadRequest($msg);
        }

        if(!empty($location->is_block_stack) && !empty($location->parent_id)){
            $msg = sprintf("Location %s is child location",$attribute['loc_code']);
            return $this->response->errorBadRequest($msg);
        }

        if($location->spc_hdl_code != $attribute['spc_hdl_code']) {
//            $msg = sprintf("This Item can not put to this Location",$attribute['loc_code']);
            return $this->response->errorBadRequest("This Item can not put to this Location");
        }

    }

    protected function generateContainerNum() {
        return $this->containerModel->generateContainerNum();
    }

    private function generateGrNumAndSeq($asnNum, $asnHrdId)
    {
        $numOfGr = $this->goodsReceiptModel->checkWhere(['asn_hdr_id' => $asnHrdId]);

        $result = [
            'gr_num' => '',
            'gr_seq' => '',
        ];

        $result['gr_seq'] = ($numOfGr) ? $numOfGr + 1 : 1;

        $grNum = str_replace(config('constants.asn_prefix'), config('constants.gr_prefix'), $asnNum);

        $result['gr_num'] = sprintf('%s-%s',
            $grNum,
            str_pad($result['gr_seq'], 2, '0', STR_PAD_LEFT));

        return $result;
    }



}