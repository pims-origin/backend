<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class OrderHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class OrderHdrValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'whs_id'          => 'required|integer|exists:warehouse,whs_id',
            'cus_id'          => 'required|integer|exists:customer,cus_id',
            'cus_odr_num'     => 'required',
            'cus_po'          => 'required',
            'carrier'         => 'required',
            'ship_to_name'    => 'required',
            'ship_to_add_1'   => 'required',
            'ship_to_city'    => 'required',
            'ship_to_country' => 'required',
            'ship_to_state'   => 'required',
            'ship_to_zip'     => 'required',
            'odr_type'        => 'required',

            'items'              => 'required|array',
            'items.*.odr_dtl_id' => 'integer|exists:odr_dtl,odr_dtl_id',
            'items.*.itm_id'     => 'required|integer|exists:item,item_id',
            'items.*.sku'        => 'required',
            'items.*.uom_id'     => 'integer|exists:system_uom,sys_uom_id',
            'items.*.qty'        => 'integer',
            'items.*.piece_qty'  => 'integer',

			'odr_sold_to'		 => 'array'
        ];
    }


}
