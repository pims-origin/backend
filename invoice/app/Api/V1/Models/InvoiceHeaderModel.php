<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\InvoiceHeader;


class InvoiceHeaderModel extends AbstractModel
{
    /**
     * InvoiceHeaderModel constructor.
    */

    public function __construct(
            InvoiceHeader $model = NULL
    )
    {
        $this->model = ($model) ?: new InvoiceHeader();
    }

    /**
    * @params array
    * @return array
    */

    public function updateInvoicePayment($params)
    {
       //update invoice header
        return $this->update($params);
    }

    /**
    * @invoiceNumber int
    * @return int
    */

    public function getByInvoiceNumber($invoiceNumber)
    {
        if (! $invoiceNumber) {
            return [];
        }

        $result = $this->model
                        ->where('inv_num', $invoiceNumber)
                        ->value('inv_id');

        return $result;
    }

    /**
    * @invoiceNo int
    * @return array
    */

   public function get($invoiceNo)
    {
        $invHeader = $this->model
                        ->select(
                            'invoice_hdr.cus_id',
                            'inv_dt',
                            'cust_ref',
                            'bill_to_add1 AS bill_to_add',
                            'bill_to_city',
                            'bill_to_state',
                            'bill_to_cnty',
                            'bill_to_zip',
                            'invoice_hdr.net_terms'
                        )
                        ->with([
                            'customer' => function ($query) {
                                $query->select('cus_id', 'cus_name');
                            },
                        ])
                        ->where('inv_num', $invoiceNo)
                        ->get();

        $result = [];
        foreach ($invHeader as $row) {
            $result = [
              'cus_id' => $row->cus_id,
              'custName' => $row->cus_name,
              'inv_dt' => $row->inv_dt,
              'cust_ref' => $row->cust_ref,
              'bill_to_add' => $row->bill_to_add,
              'bill_to_city'  => $row->bill_to_city,
              'bill_to_state' => $row->bill_to_state,
              'bill_to_cnty'  => $row->bill_to_cnty,
              'bill_to_zip'  => $row->bill_to_zip,
              'net_terms'  => $row->net_terms
            ];
        }

        return $result;
    }
    
    /**
    * @return int
    */

    public function getNextInvoiceNumber()
    {
        $result = $this->model
                       ->select(DB::raw('MAX(inv_num) AS maxInv'))
                       ->first();

        $invNum = $result->maxInv + 1;

        return $invNum;
    }

    /**
    * @invoiceNo int
    * @return int
    */

    public function insertInvHeader($params)
    {
        $this->model->create([
            'inv_num' => $params['invoiceNo'],
            'create_by' => $params['userID'],
            'cus_id' => $params['cusID'],
            'whs_id' => $params['whsID']
        ]);
    }


    /**
    * @invoiceNo int
    * @return string
    */

    public function getStatusByNumber($invoiceNo)
    {
        $result = $this->model
                        ->where('inv_num', $invoiceNo)
                        ->value('inv_sts');

        return $result;
    }

    /**
    * @invoiceNo int
    * @return array
    */

    public function getCancellingData($invoiceNo)
    {
        $cnclHeader = $this->model
                        ->select('cus_id', 'whs_id', 'inv_sts', 'inv_cur', 'cust_ref',
                                  DB::raw('-1 * inv_amt AS inv_amt'), DB::raw('-1 * inv_tax AS inv_tax'),
                                  'net_terms', 'bill_to_add1 AS bill_to_add', 'bill_to_city',
                                  'bill_to_state', 'bill_to_cnty', 'bill_to_zip', 'bill_to_contact'
                        )
                        ->where('inv_num', $invoiceNo)
                        ->first()
                        ->toArray();

        return $cnclHeader;
    }

}