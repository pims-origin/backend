<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 26-Feb-16
 * Time: 3:00 PM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\Helper;

class ReportModel
{

    /**
     * @param $orderId
     * @return array
     */
    public function shipped($orderId)
    {

        $orderData = [];
        $return = [];
        $results = DB::table('odr_hdr')
            ->join("shipment","shipment.ship_id","=","odr_hdr.ship_id")
            ->join("wv_hdr","wv_hdr.wv_id","=","odr_hdr.wv_id")
            ->whereIn('odr_id', $orderId)
            ->where("odr_hdr.deleted",0)
            ->where("wv_hdr.deleted",0)
            ->where("shipment.deleted",0)
            ->select('odr_hdr.*', 'wv_hdr.wv_num', 'shipment.bo_label')
            ->first();

        if (!$results) {
            return false;
        }
        $finalStatuses = ['SH', 'CC'];
        if (!in_array($results['odr_sts'], $finalStatuses)) {
            $orderData['wms_status_code'] = $results['odr_sts'];
            $orderData['wms_status_name'] = $results['odr_sts'];
        }
        if (empty($results)) {
            return $orderData;
        }
        $odr_meta = DB::table("odr_hdr_meta")
            ->where("qualifier", 945)
            ->where("odr_id",$orderId)
            ->select("value")->first();
        ;
        $orderData = (array) $results;

        $odr_dtls = DB::table('odr_dtl')
            ->select('odr_dtl.*')
            ->whereIn("odr_id",$orderId)
            ->where("deleted",0)
            ->get();
        $total_weight = 0;
        $total_piece = 0;
        $total_volume = 0;

        foreach ($odr_dtls as $odr_dtl) {
            $orderData['items'][$odr_dtl['odr_dtl_id']]['product_order']= (array) $odr_dtl;
            $item_meta = [];
            $item_meta = DB::table("item_meta")->where("qualifier",945)
                ->where("itm_id",$odr_dtl['item_id'])->select("value")->first();
            $orderData['items'][$odr_dtl['odr_dtl_id']]['product_order']["product_meta"] = ($item_meta['value'])?$item_meta['value']:"";

            $packs = [];
            $packs = DB::table("pack_hdr")
                ->join("pack_dtl","pack_dtl.pack_hdr_id","=","pack_hdr.pack_hdr_id")
                ->join("odr_dtl", function($q) {
                    $q->on("odr_dtl.odr_id","=","pack_dtl.odr_hdr_id");
                    $q->on("odr_dtl.item_id","=","pack_dtl.item_id");
                })
                ->where("pack_dtl.odr_hdr_id",$orderId)
                ->where("pack_hdr.deleted",0)
                ->select([
                    "pack_hdr.length",
                    "pack_hdr.width",
                    "pack_hdr.height",
                    "pack_hdr.weight",
                    "pack_hdr.pack_hdr_num",
                    "pack_hdr.pack_hdr_id",
                    "pack_dtl.piece_qty"
                ])
                ->get();

            $orderData['items'][$odr_dtl['odr_dtl_id']]['product_order']["cartons"] = $packs;
            foreach ($packs as $pack) {
                $total_piece    += ($pack['piece_qty'])?$pack['piece_qty']:0;
                $total_weight   += ($pack['weight'])?$pack['weight']:0;
                $volume = Helper::calculateVolume($pack['length'],$pack['width'],$pack['height']);
                $total_volume += $volume;
            }
        }
        $orderData['isSuccess'] = true;
        $orderData['order_meta'] = ($odr_meta['value'])?$odr_meta['value']:'';
        $orderData['total_piece'] = $total_piece;
        $orderData['total_weight'] = $total_weight;
        $orderData['total_volume'] = $total_volume;

        $return ['cus_id'] = $results['cus_id'];
        $return ['whs_id'] = $results['whs_id'];

        $return ['api_app'] = 'edi';
        $return ['transactional_code'] = 'ORD';
        $return ['success_key'] = $results['odr_num'];
        $return ['data'] = [$results['odr_num'] => $orderData];

        return $return;
    }

    /**
     * @param $shipId
     * @return array
     */
    public function shipment($shipId)
    {
        $return = [];

        $shipments = DB::table('shipment')->where('shipment.ship_id', $shipId)->where('ship_sts', 'FN')->first();
        if (! $shipments) {
            return false;
        }

        $shipments['bol_odr_dtl'] = DB::table('shipment')
            ->select('bol_odr_dtl.*')
            ->join('bol_odr_dtl', 'bol_odr_dtl.ship_id', '=', 'shipment.ship_id')
            ->where('shipment.ship_id', $shipId)->get();

        $shipments['bol_carrier_dtl'] = DB::table('shipment')
            ->select('bol_carrier_dtl.*')
            ->join('bol_carrier_dtl', 'bol_carrier_dtl.ship_id', '=', 'shipment.ship_id')
            ->where('shipment.ship_id', $shipId)->get();

        $shipments['isSuccess'] = true;
        $return ['cus_id'] = $shipments['cus_id'];
        $return ['whs_id'] = $shipments['whs_id'];
        $return ['api_app'] = 'edi';
        $return ['transactional_code'] = 'BOL';
        $return ['success_key'] = $shipments['bo_num'];
        $return ['data'] = [$shipments['bo_num'] => $shipments];

        return $return;

    }

}
