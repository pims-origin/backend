<?php


namespace App\Api\ReplenishmentConfig\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\ReplenishmentConfig;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class ReplenishmentConfigTransformer extends TransformerAbstract
{
    public function transform(ReplenishmentConfig $replenishmentConfig)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $updateAt = object_get($replenishmentConfig, 'updated_at', '');
        $min = object_get($replenishmentConfig, 'min', 0);
        $max = object_get($replenishmentConfig, 'max', 0);
        $locId = object_get($replenishmentConfig, 'loc_id', '');

        $locWithOneSKU = DB::table('cartons')
            ->select([
                DB::raw("COUNT(ctn_id) AS ctnNo")
            ])
            ->where('loc_id', $locId)
            ->first();
        $currentQty = array_get($locWithOneSKU, 'ctnNo', 0);

        $transferQty = 0;
        if ($currentQty < $min) {
            $transferQty = $max - $currentQty;
        }
        return [
            'id'           => object_get($replenishmentConfig, 'id', ''),
            'cus_id'       => object_get($replenishmentConfig, 'cus_id', ''),
            'cus_name'     => object_get($replenishmentConfig, 'cus_name', ''),
            'whs_id'       => object_get($replenishmentConfig, 'whs_id', ''),
            'loc_id'       => object_get($replenishmentConfig, 'loc_id', ''),
            'loc_code'     => object_get($replenishmentConfig, 'loc_code', ''),
            'item_id'      => object_get($replenishmentConfig, 'item_id', ''),
            'sku'          => object_get($replenishmentConfig, 'sku', ''),
            'min'          => object_get($replenishmentConfig, 'min', 0),
            'max'          => object_get($replenishmentConfig, 'max', 0),
            'current_qty'  => $currentQty,
            'transfer_qty' => $transferQty,
            'uom_id'       => object_get($replenishmentConfig, 'uom_id', ''),
            'uom_code'     => object_get($replenishmentConfig, 'uom_code', ''),
            'uom_name'     => object_get($replenishmentConfig, 'uom_name', ''),
            'des'          => object_get($replenishmentConfig, 'des', ''),
            'updated_at'   => (empty($updateAt)) ? '' : date("Y-m-d", strtotime($updateAt)),
            'updated_by'   => object_get($replenishmentConfig, 'updated_by', ''),

        ];
    }

}
