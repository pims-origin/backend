<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateOrderFlow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:order-flow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update order flow for all customer';

    /**
     * Create a new command instance.
     *
     * @param DripEmailer $drip
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $cusIds = DB::table('customer')->where('deleted', 0)->pluck('cus_id');
            foreach ($cusIds as $cusId) {
                $arrQualifiers = ['OFF', 'OSF'];
                foreach ($arrQualifiers as $arrQualifier) {
                    $cusMeta = DB::table('cus_meta')->where(['cus_id' => $cusId, 'qualifier' => $arrQualifier])->first();
                    if ($cusMeta) {
                        $valueJsons = json_decode($cusMeta['value']);
                        $newJsonValue = [];
                        if ($valueJsons) {
                            foreach ($valueJsons as $valueJson) {
                                $odrflow = json_decode($valueJson);
                                if ($odrflow->odr_flow_id == 7) {
                                    $odrflow->usage = 1;
                                }
                                $newJsonValue[] = json_encode($odrflow);
                            }
                        }
                        DB::table('cus_meta')->where('cus_id', $cusId)->update(['value' => json_encode($newJsonValue)]);
                    }
                }
            }

            DB::commit();

            dd('OK');

        } catch (\PDOException $e) {
            DB::rollback();
            dd($e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }
}