<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:58
 */

namespace App\Api\V1\Validators;


class ChargeCodeValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'chg_code'    => 'required',
            'chg_uom_id'  => 'required|integer',
            'chg_type_id' => 'required|integer',
        ];
    }
}