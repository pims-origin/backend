<?php

namespace App\Api\V1\Validators;

class CustomerWarehouseValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id' => 'required|exists:warehouse,whs_id'
        ];
    }
}
