<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

/**
 * Class UpdatePutBackValidator
 *
 * @package App\Api\V1\Validators
 */
class UpdatePutBackValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'return_id'             => 'required|integer|exists:return_hdr,return_id',
            'items'                 => 'required|array',
            'items.*.return_dtl_id' => 'required|exists:put_back,return_dtl_id',
            'items.*.pb_id'         => 'required|exists:put_back,pb_id',
            'items.*.loc_id'        => 'required|exists:location,loc_id',
            'items.*.loc_code'      => 'required|exists:location,loc_code',
        ];
    }
}
