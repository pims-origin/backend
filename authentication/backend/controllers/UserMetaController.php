<?php

namespace backend\controllers;

use backend\models\LastLogin;
use backend\models\UserCache;
use backend\models\UserMetaProcess;
use Yii;
use yii\di\Instance;
use yii\web\NotFoundHttpException;

class UserMetaController extends ApiController
{
    public $modelClass = 'common\models\UserMeta';
    protected $qualifier = 'UTC';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view'], $actions['index'], $actions['create'], $actions['delete'], $actions['update']);

        return $actions;
    }

    public function actionGet($qualifier)
    {
        $user_id = Yii::$app->user->getId();
        $userMeta = new UserMetaProcess();
        $value = $userMeta->get($user_id, $qualifier);

        return ['data' => $value];
    }

    public function actionSet($qualifier)
    {
        $qualifier = strtoupper($qualifier);

        $user_id = Yii::$app->user->getId();
        $value = Yii::$app->request->post();

        if (empty($value['value'])) {
            return [
                'error' => [
                    'status' => 404,
                    'code'   => '404',
                    'title'  => 'value parameter is required',
                ]
            ];
        }
        $value = $value['value'];

        //update cache user
        if ($qualifier === 'WID') {
            $userCache = UserCache::getInstance();
            $getUserCache = $userCache->getUserCache();
            if ($getUserCache['current_whs'] != $value) {
                $arrToken = $this->getAuth();
                LastLogin::deleteAll('user_id = :userId AND expired_at != :exp', [
                    ':userId' => $arrToken['jti'], ':exp' => $arrToken['exp']
                ]);

                $rs = $userCache->setWID($value);
                if (!$rs) {
                    throw new BadRequestHttpException('WID don\'t belongs user');
                }
            }

        }
        $userMeta = new UserMetaProcess();
        $userMeta->set($user_id, $qualifier, $value);

        return ['data' => ['success' => true]];
    }

    private function setWID()
    {

    }
}