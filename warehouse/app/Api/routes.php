<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

$api = app('Dingo\Api\Routing\Router');

$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
    $middleware[] = 'setWarehouseTimezone';
}

$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
        // status
        $api->get('warehouses/statuses', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseStatusController@search'
        ]);
        $api->get('/warehouses', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseController@search'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseController@show'
        ]);
        $api->post('/warehouses', [
            'action' => 'createWarehouse',
            'uses'   => 'WarehouseController@store'
        ]);
        $api->put('/warehouses/{warehouseId:[0-9]+}', [
            'action' => 'editWarehouse',
            'uses'   => 'WarehouseController@update'
        ]);
        $api->delete('/warehouses/{warehouseId:[0-9]+}', [
            'action' => 'deleteWarehouse',
            'uses'   => 'WarehouseController@destroy'
        ]);
        $api->delete('/warehouses', [
            'action' => 'deleteWarehouse',
            'uses'   => 'WarehouseController@deleteMass'
        ]);
        $api->get('/warehouses/export', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseController@export'
        ]);
        // Warehouse Address
        $api->get('/warehouses/{warehouseId:[0-9]+}/addresses', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseAddressController@index'
        ]);
        $api->post('/warehouses/{warehouseId:[0-9]+}/addresses', [
            'action' => 'createWarehouse',
            'uses'   => 'WarehouseAddressController@store'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/addresses/{addressId:[0-9]+}', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseAddressController@show'
        ]);
        $api->put('/warehouses/{warehouseId:[0-9]+}/addresses/{addressId:[0-9]+}', [
            'action' => 'editWarehouse',
            'uses'   => 'WarehouseAddressController@update'
        ]);
        $api->delete('/warehouses/{warehouseId:[0-9]+}/addresses/{addressId:[0-9]+}', [
            'action' => 'deleteWarehouse',
            'uses'   => 'WarehouseAddressController@destroy'
        ]);
        // Warehouse Contact
        $api->get('/warehouses/{warehouseId:[0-9]+}/contacts', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseContactController@index'
        ]);
        $api->post('/warehouses/{warehouseId:[0-9]+}/contacts', [
            'action' => 'createWarehouse',
            'uses'   => 'WarehouseContactController@store'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/contacts/{contactId:[0-9]+}', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseContactController@show'
        ]);
        $api->put('/warehouses/{warehouseId:[0-9]+}/contacts/{contactId:[0-9]+}', [
            'action' => 'editWarehouse',
            'uses'   => 'WarehouseContactController@update'
        ]);
        $api->delete('/warehouses/{warehouseId:[0-9]+}/contacts/{contactId:[0-9]+}',
            ['action' => 'deletedWarehouse', 'uses' => 'WarehouseContactController@destroy']);
        $api->post('/warehouses/{warehouseId:[0-9]+}/contacts/save-mass-and-delete-unused', [
            'action' => 'deleteWarehouse',
            'uses'   => 'WarehouseContactController@saveMassAndDeleteUnused'
        ]);
        //Warehouse Meta
        $api->get('/warehouses/{warehouseId:[0-9]+}/meta', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseMetaController@index'
        ]);
        $api->post('/warehouses/{warehouseId:[0-9]+}/meta', [
            'action' => 'createWarehouse',
            'uses'   => 'WarehouseMetaController@store'
        ]);
        $api->post('/warehouses/{warehouseId:[0-9]+}/meta/save-mass-and-delete-unused', [
            'action' => 'createWarehouse',
            'uses'   => 'WarehouseMetaController@saveMassAndDeleteUnused'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/meta/{qualifier}', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseMetaController@show'
        ]);
        $api->put('/warehouses/{warehouseId:[0-9]+}/meta/{qualifier}', [
            'action' => 'editWarehouse',
            'uses'   => 'WarehouseMetaController@update'
        ]);
        $api->delete('/warehouses/{warehouseId:[0-9]+}/meta/{qualifier}', [
            'action' => 'deleteWarehouse',
            'uses'   => 'WarehouseMetaController@destroy'
        ]);

        //Configuration Warehouse
        $api->post('/warehouses/{warehouseId:[0-9]+}/configuration', [
            'action' => 'createWarehouse',
            'uses'   => 'WarehouseMetaController@storeConfiguration'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/configuration', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseMetaController@showConfiguration'
        ]);

        $api->get('/warehouses/search-timezone', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseMetaController@searchTimezone'
        ]);

        //Warehouse Zone
        $api->get('warehouses/free-zones', [
            'action' => 'viewZone',
            'uses'   => 'WarehouseZoneController@freeZone'
        ]);
        $api->get('/zones', [
            'action' => 'viewZone',
            'uses'   => 'WarehouseZoneController@loadBy'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/wh-zones', [
            'action' => 'viewWarehouse',
            'uses'   => 'WarehouseZoneController@search'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/zones', [
            'action' => 'viewZone',
            'uses'   => 'WarehouseZoneController@search'
        ]);
        $api->post('/warehouses/{warehouseId:[0-9]+}/zones', [
            'action' => 'createZone',
            'uses'   => 'WarehouseZoneController@store'
        ]);

        $api->post('/warehouses/{warehouseId:[0-9]+}/zone-import', [
            'action' => 'createZone',
            'uses'   => 'WarehouseZoneController@zoneImport'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/zones/{zoneId:[0-9]+}', [
            'action' => 'viewZone',
            'uses'   => 'WarehouseZoneController@show'
        ]);
        $api->put('/warehouses/{warehouseId:[0-9]+}/zones/{zoneId:[0-9]+}', [
            'action' => 'editZone',
            'uses'   => 'WarehouseZoneController@update'
        ]);
        $api->delete('/warehouses/{warehouseId:[0-9]+}/zones/{zoneId:[0-9]+}', [
            'action' => 'deleteZone',
            'uses'   => 'WarehouseZoneController@destroy'
        ]);
        $api->delete('/warehouses/{warehouseId:[0-9]+}/zones', [
            'action' => 'deleteZone',
            'uses'   => 'WarehouseZoneController@deleteMass'
        ]);
        // Warehouse Zone Type
        $api->get('/zone-types/{zoneTypeId:[0-9]+}', [
            'action' => 'viewZoneType',
            'uses'   => 'WarehouseZoneTypeController@show'
        ]);
        $api->get('/zone-types', [
            'action' => 'viewZoneType',
            'uses'   => 'WarehouseZoneTypeController@search'
        ]);
        $api->post('/zone-types', [
            'action' => 'createZoneType',
            'uses'   => 'WarehouseZoneTypeController@store'
        ]);
        $api->put('/zone-types/{zoneTypeId:[0-9]+}', [
            'action' => 'viewZoneType',
            'uses'   => 'WarehouseZoneTypeController@update'
        ]);
        $api->delete('/zone-types/{zoneTypeId:[0-9]+}', [
            'action' => 'deleteZoneType',
            'uses'   => 'WarehouseZoneTypeController@destroy'
        ]);
        $api->delete('/zone-types', [
            'action' => 'deleteZoneType',
            'uses'   => 'WarehouseZoneTypeController@deleteMass'
        ]);
        // Location
        $api->get('/warehouses/{warehouseId:[0-9]+}/wh-locations', [
            'action' => 'viewWarehouse',
            'uses'   => 'LocationController@search'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/locations', [
            'action' => 'viewLocation',
            'uses'   => 'LocationController@search'
        ]);
        $api->get('/warehouses/{whsId:[0-9]+}/locations-by-customers/{cusId:[0-9]+}', [
            'action' => 'viewLocation',
            'uses'   => 'LocationController@locationsByCustomer'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/locations/export', [
            'action' => 'viewLocation',
            'uses'   => 'LocationController@export'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/zones/{zoneId:[0-9]+}/locations', [
            'action' => 'viewLocation',
            'uses'   => 'LocationController@listByZone'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/free-locations', [
            'action' => 'viewLocation',
            'uses'   => 'LocationController@searchExceptZone'
        ]);
        $api->post('/warehouses/{warehouseId:[0-9]+}/locations', [
            'action' => 'createLocation',
            'uses'   => 'LocationController@store'
        ]);
        $api->post('warehouses/{warehouseId:[0-9]+}/zones/{zoneId:[0-9]+}/locations/save-mass-and-delete-unused', [
            'action' => 'createLocation',
            'uses'   => 'LocationController@saveMassAndDeleteUnused'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}', [
            'action' => 'viewLocation',
            'uses'   => 'LocationController@show'
        ]);
        $api->put('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}', [
            'action' => 'editLocation',
            'uses'   => 'LocationController@update'
        ]);
        $api->delete('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}', [
            'action' => 'deleteLocation',
            'uses'   => 'LocationController@destroy'
        ]);
        $api->post('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}/change-status', [
            'action' => 'changeLocationStatus',
            'uses'   => 'LocationController@changeLocationStatus'
        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/locations/{locationId:[0-9]+}/change-status', [
            'action' => 'viewLocation',
            'uses'   => 'LocationController@getLocationStatus'
        ]);
        $api->get('/reasons', [
            'action' => 'viewLocation',
            'uses'   => 'ReasonController@search'
        ]);
        $api->delete('/warehouses/{warehouseId:[0-9]+}/locations', [
            'action' => 'deleteLocation',
            'uses'   => 'LocationController@deleteMass'
        ]);
        $api->post('/locations/validate-import', [
            'action' => 'importLocation',
            'uses'   => 'LocationController@validateImportFile'
        ]);
//        $api->get('/locations/import-status', [
//            'action' => 'importLocation',
//            'uses'   => 'LocationController@getStatusImport'
//        ]);
        $api->get('/locations/process-import', [
            'action' => 'importLocation',
            'uses'   => 'LocationController@getStatusImport'
        ]);
        $api->get('/locations/download/{hash}', [
            'action' => 'viewLocation',
            function ($hash) {
                $file = storage_path("Errors/$hash");

                return response()->download($file);
            }
        ]);

        //Print Location Barcode
        $api->get('/location/print-location-barcode',
            ['action' => 'viewLocation', 'uses' => 'LocationController@printLocBarcode']);

        // Zone Location Status
        $api->get('/location-statuses', [
            'action' => 'viewLocation',
            'uses'   => 'ZoneLocationStatusController@search'
        ]);
        // Location Type
        $api->get('/location-types', [
            'action' => 'viewLocationType',
            'uses'   => 'LocationTypeController@search'
        ]);
        $api->post('/location-types', [
            'action' => 'createLocationType',
            'uses'   => 'LocationTypeController@store'
        ]);
        $api->get('/location-types/{locationTypeId:[0-9]+}', [
            'action' => 'viewLocationType',
            'uses'   => 'LocationTypeController@show'
        ]);
        $api->put('/location-types/{locationTypeId:[0-9]+}', [
            'action' => 'editLocationType',
            'uses'   => 'LocationTypeController@update'
        ]);
        $api->delete('/location-types/{locationTypeId:[0-9]+}', [
            'action' => 'deleteLocationType',
            'uses'   => 'LocationTypeController@destroy'
        ]);
        $api->delete('/location-types', [
            'action' => 'deleteLocationType',
            'uses'   => 'LocationTypeController@deleteMass'
        ]);

        // Warehouse Layout
        $api->get('/whs-layout', [
            'action' => 'viewLocation',
            'uses'   => 'WarehouseController@listLayout'
        ]);

        // List Customer for Warehouse Layout
        $api->get('/whs-layout/customer', [
            'action' => 'viewLocation',
            'uses'   => 'WarehouseController@listCustomer'
        ]);

        // List Zone for Warehouse Layout
        $api->get('/whs-layout/zone', [
            'action' => 'viewLocation',
            'uses'   => 'WarehouseController@listZone'
        ]);

        $api->get('/whs-layout/{zone}', [
            'action' => 'viewLocation',
            'uses'   =>
                'WarehouseController@showLayout'
        ]);

        $api->get('/whs-layout/{locId:[0-9]+}/show', [
            'action' => 'viewLocation',
            'uses'   =>
                'WarehouseController@showPopup'
        ]);

        //inventory report
        $api->get('/warehouses/inventory-report', [
            'action' => 'viewInventory',
            'uses'   => 'WarehouseController@inventorReport'
        ]);

        //SKU Report
        $api->get('/warehouses/{warehouseId:[0-9]+}/sku-report', [
            'action' => 'viewInventory',
            'uses'   => 'WarehouseController@skuReport'
        ]);

        //shipping report
//        $api->get('/warehouses/{warehouseId:[0-9]+}/report/shipping', [
//            'action' => 'viewOrder',
//            'uses'   => 'OrderHdrController@shippingReport'
//        ]);
        $api->get('/warehouses/{warehouseId:[0-9]+}/report/shipping', [
            'action' => 'viewOrder',
            'uses'   => 'OrderCtnController@shippingReport'
        ]);

        //order report
        $api->get('/warehouses/{warehouseId:[0-9]+}/report/order', [
            'action' => 'viewOrder',
            'uses'   => 'OrderHdrController@orderReport'
        ]);

        //order report
        $api->get('/warehouses/report/order-status-not-shipping', [
            'action' => 'viewOrder',
            'uses'   => 'OrderHdrController@orderStatusNotShipping'
        ]);

        // dashboard
        $api->get('/dashboard/asns', ['action' => "viewASN", 'uses' => 'DashboardController@asnDashboard']);

        $api->get('/dashboard/orders',
            ['action' => 'viewOrder', 'uses' => 'DashboardController@odrDashboard']);

        $api->get('/dashboard/order-inprocess',
            ['action' => 'viewOrder', 'uses' => 'DashboardController@inprocessDashboard']);

        $api->get('/dashboard/putaway',
            ['action' => 'viewPutaway', 'uses' => 'DashboardController@putawayDashboard']);

        $api->get('/dashboard/asn-admin', [
            'action' => "dashboardReceiving",
            'uses'   =>
                'DashboardController@asnAdminDashboard'
        ]);

        $api->get('/dashboard/order-admin',
            ['action' => 'dashboardOrder', 'uses' => 'DashboardController@odrAdminDashboard']);

        $api->get('/dashboard/order-inprocess-admin',
            ['action' => 'dashboardInprogress', 'uses' => 'DashboardController@inprocessAdminDashboard']);

        $api->get('/dashboard/putaway-admin',
            ['action' => 'dashboardPutaway', 'uses' => 'DashboardController@putawayAdminDashboard']);

        $api->get('/dashboard/wavepick',
            ['action' => 'wavepickDashboard', 'uses' => 'DashboardController@wavepickDashboard']);

        $api->get('/dashboard/goods-receipt',
            ['action' => 'inboundEventTracking', 'uses' => 'DashboardController@goodReceiptDashboard']);

        $api->get('/dashboard/putaway-received',
            ['action' => 'putawayDashboard', 'uses' => 'DashboardController@putawayReceivedDashboard']);

        $api->get('/dashboard/occupancy-pct',
            ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@OccupancyPCTDashboard']);

        $api->get('/dashboard/occupancy-pct-by-cus',
            ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@occupancyPCTDashboardByCus']);

        $api->get('/dashboard/occupancy-pct-status',[
                'action' => 'dashboardWarehouseCapacity',
                'uses' => 'DashboardController@occupancyPCTDashboardStatus'
            ]);

        $api->get('/dashboard/wave-pick',
            ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@wavePickStatisticDashboard']);

        $api->get('/dashboard/cycle-count',
            ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@cycleCountStatisticDashboard']);

        $api->get('/dashboard/zoneCapacity-admin', [
            'action' => 'dashboardWarehouseCapacity',
            'uses' => 'DashboardController@zoneCapaticyAdminDashboard'
        ]);

        $api->get('/dashboard/room-admin',
            ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@roomAdminDashboard']);

        $api->get('/dashboard/alert-bar',
            ['action' => 'dashboardWarehouseCapacity', 'uses' => 'DashboardController@alertBarDashboard'
            ]);

        //refill
        $api->post('refill', [
            'action' => 'createLocation',
            'uses'   => 'RefillController@store'
        ]);
        $api->put('refill/{refillId:[0-9]+}', [
            'action' => 'editLocation',
            'uses'   => 'RefillController@update'
        ]);
        $api->get('refill/{refillId:[0-9]+}', [
            'action' => 'viewLocation',
            'uses'   => 'RefillController@show'
        ]);
        $api->get('refill/location/{locationId:[0-9]+}', [
            'action' => 'viewLocation',
            'uses'   => 'RefillController@showRefillAccordingToLocation'
        ]);
        $api->delete('refill/{refillId:[0-9]+}', [
            'action' => 'deleteLocation',
            'uses'   => 'RefillController@destroy'
        ]);


    });
});
