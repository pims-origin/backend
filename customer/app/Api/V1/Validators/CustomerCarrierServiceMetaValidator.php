<?php

namespace App\Api\V1\Validators;

class CustomerCarrierServiceMetaValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            '*.value' => 'required',
            '*.qualifier' => 'required',
        ];
    }
}
