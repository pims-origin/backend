<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 9/6/2016
 * Time: 10:59 AM
 */

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Seldat\Wms2\Models\OrderHdr::class, 15)->create();
        factory(\Seldat\Wms2\Models\OrderDtl::class, 15)->create();
    }
}