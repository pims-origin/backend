<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\Labor;
use Seldat\Wms2\Utils\Status;


class WorkHoursModel extends AbstractModel
{
    public $vendorList = [];
    
    /**
    * Work Hours Model constructor.
    */

    public function __construct()
    {
        $this->labor =  new Labor();
    }
    
    /**
     * @data array
     * @recNums array
     * @return mixed
    */
    
    public function getReceivingHisLabor($data, $recNums=[])
    {
        $wrkHrs = $this->labor
                    ->join('gr_hdr AS gh', 'gh.gr_hdr_id', '=', 'labor.ref_id')
//                    ->join('evt_tracking AS et', 'et.trans_num', '=', 'gh.gr_hdr_num')
                    ->join('pallet AS plt', 'plt.gr_hdr_id', '=', 'gh.gr_hdr_id')
                    ->select(DB::raw('DISTINCT labor.ref_id'))
                    ->whereHas('invoiceHeader', function ($q) {
                           $q->where('inv_typ', 'o');
                           $q->whereIn('inv_sts', ['p', 'i']);
                    })
                    ->where(function($query) use($data, $recNums) {
//                        $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '>=', $data['startDate']);
//                        $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '<=', $data['endDate']);
                        $query->where('plt.gr_dt', '>=', strtotime($data['startDate']));
                        $query->where('plt.gr_dt', '<=', strtotime($data['endDate'] . ' 23:59:59'));
                        $query->where('category', config('constants.labor_cat.RCV'));
                        $query->where('inv_sts', 1);
                        $query->where('gh.gr_sts', config('constants.gr_status.RECEIVED'));
//                        $query->where('evt_code', config('constants.event.GR-COMPLETE'));

                        if ($recNums) {
                            $query->whereIn('gh.gr_hdr_id', $recNums);
                        }
                        
                        if ($data['custIDs']) {
                                $query->whereIn('labor.cus_id', $data['custIDs']);
                            }
                            
                        if ($data['whsID']) {
                            $query->where('labor.whs_id', $data['whsID']);
                        }
                    })
                   ->get();

        $rcvNums = [];
        if (!$wrkHrs->isEmpty()) {
            foreach ($wrkHrs as $row) {
                $rcvNums[] = $row->ref_id;
            }
        }

        return $rcvNums ? $rcvNums : NULL;
    }
    
    /**
     * @data array
     * @select array
     * @return mixed
    */
    
    
    public function getRecLaborQuery($data, $select)
    {
        $param = [
            'recNums' => $select['containerClause'],
            'cusIDs' => $select['custClause'],
            'whsID' => $select['whsID'],
            'hisRcvNums' => $select['hisRcvNums'],
            'labor' => $select['laborClause']
        ];
     
        $result = $this->labor
                        ->join('gr_hdr AS gh', 'gh.gr_hdr_id', '=', 'labor.ref_id')
//                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'gh.gr_hdr_num')
                        ->join('pallet AS plt', 'plt.gr_hdr_id', '=', 'gh.gr_hdr_id')
                        ->select('labor.cus_id', 'labor.whs_id', 'type', 
                                  'urgency', DB::raw('SUM(amount) AS amount')
                                )  
                        ->where(function($query) use($data, $param) 
                        {
//                            $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '>=', $data['startDate']);
//                            $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '<=', $data['endDate']);
                            $query->where('plt.gr_dt', '>=', strtotime($data['startDate']));
                            $query->where('plt.gr_dt', '<=', strtotime($data['endDate'] . ' 23:59:59'));
                            $query->where('inv_sts', 1);
                            $query->where('category', config('constants.labor_cat.RCV'));
                            $query->where('type', config('constants.labor_type.ACTUAL'));
                            $query->where('urgency', $param['labor']);
                            $query->where('gh.gr_sts', config('constants.gr_status.RECEIVED'));
//                            $query->where('evt_code', config('constants.event.GR-COMPLETE'));

                            if ($param['recNums']) {
                                $query->whereIn('gh.gr_hdr_id', $param['recNums']);
                            }

                            if ($param['cusIDs']) {
                                $query->whereIn('labor.cus_id', $param['cusIDs']);
                            }
                            
                            if ($param['whsID']) {
                                $query->where('labor.whs_id', $param['whsID']);
                            }

                            if ($param['hisRcvNums']) {
                                $query->whereNotIn('gh.gr_hdr_id', $param['hisRcvNums']);
                            }
                         })
                         ->groupBy('labor.cus_id', 'labor.whs_id', 'type')
                         ->get()
                         ->toArray();
        

        $list = [];

        foreach ($result as $value) {
            $cusID = $value['cus_id'];
            $amount = $value['amount'];
            $list[$cusID][] = $amount;
        }

        return $list ? $list : [];
    }

    /**
     * @data array
     * @orderNums array
     * @return mixed
    */
    
    public function getOrderProcHisLabor($data, $orderIDs=[])
    {
        $eventCode = [
            config('constants.event.ORD-SHIPPED'),
            config('constants.event.PR-SHIPPED')
        ];
        
        $wrkHrs = $this->labor
                    ->join('odr_hdr AS oh', 'oh.odr_id', '=', 'labor.ref_id')
//                    ->join('evt_tracking AS et', 'et.trans_num', '=', 'oh.odr_num')
                    ->select(DB::raw('DISTINCT oh.odr_num'))
                    ->whereHas('invoiceHeader', function ($q) {
                           $q->where('inv_typ', 'o');
                           $q->whereIn('inv_sts', ['p', 'i']);
                    })
                    ->where(function($query) use($data, $orderIDs, $eventCode) {
//                        $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '>=', $data['startDate']);
//                        $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '<=', $data['endDate']);
                        $query->where('oh.shipped_dt', '>=', strtotime($data['startDate']));
                        $query->where('oh.shipped_dt', '<=', strtotime($data['endDate'] . ' 23:59:59'));
                        $query->where('category', config('constants.labor_cat.OP'));
                        $query->where('inv_sts', 1);
//                        $query->whereIn('evt_code', $eventCode);
                        $query->where('oh.odr_sts', Status::getByValue('Shipped', 'ORDER-STATUS'));
                        
                        if ($orderIDs) {
                            $query->whereIn('oh.odr_id', $orderIDs);
                        }
                        
                        if ($data['custIDs']) {
                            $query->whereIn('labor.cus_id', $data['custIDs']);
                        }
                            
                        if ($data['whsID']) {
                            $query->where('labor.whs_id', $data['whsID']);
                        }
                    })
                   ->get();


        $odrNums = [];
        if (!$wrkHrs->isEmpty()) {
            foreach ($wrkHrs as $row) {
                $odrNums[] = $row->odr_num;
            }
        }

        return $odrNums ? $odrNums : NULL;
    }
    
    /**
     * @data array
     * @select array
     * @return mixed
    */

    public function getOrdLaborQuery($data, $select)
    {
        $cancelStatus = config('constants.odr_status.CANCELLED');
        
        $eventCode = [
            config('constants.event.ORD-SHIPPED'),
            config('constants.event.PR-SHIPPED')
        ];
        
        $param = [
            'ordIDs' => $select['orderClause'],
            'cusIDs' => $select['custClause'],
            'whsID' => $select['whsID'],
            'hisOrdNums' => $select['hisOrdNums'],
            'labor' => $select['laborClause'],
            'cancel' => $cancelStatus
        ];

        $result = $this->labor
                        ->join('odr_hdr AS oh', 'oh.odr_id', '=', 'labor.ref_id')
//                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'oh.odr_num')
                        ->select('labor.cus_id', 'labor.whs_id', 'type', 
                                 'urgency', DB::raw('SUM(amount) AS amount')
                                )
                        ->where(function($query) use($data, $param, $eventCode) 
                        {
//                            $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '>=', $data['startDate']);
//                            $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '<=', $data['endDate']);
                            $query->where('oh.shipped_dt', '>=', strtotime($data['startDate']));
                            $query->where('oh.shipped_dt', '<=', strtotime($data['endDate'] . ' 23:59:59'));
                            $query->where('inv_sts', 1);
                            $query->where('category', config('constants.labor_cat.OP'));
                            $query->where('type', config('constants.labor_type.ACTUAL'));
//                            $query->whereIn('evt_code', $eventCode);
                            $query->where('oh.odr_sts', Status::getByValue('Shipped', 'ORDER-STATUS'));
                            $query->where('urgency', $param['labor']);
                            $query->where('oh.odr_sts', '!=' , $param['cancel']);

                            if ($param['ordIDs']) {
                                $query->whereIn('oh.odr_id', $param['ordIDs']);
                            }

                            if ($param['cusIDs']) {
                                $query->whereIn('labor.cus_id', $param['cusIDs']);
                            }
                            
                            if ($param['whsID']) {
                                $query->where('labor.whs_id', $param['whsID']);
                            }

                            if ($param['hisOrdNums']) {
                                $query->whereNotIn('oh.odr_num', $param['hisOrdNums']);
                            }
                         })
                         ->groupBy('labor.cus_id', 'labor.whs_id', 'type')
                         ->get();

        $list = [];

        foreach ($result as $value) {
            $cusID = $value->cus_id;
            $amount = $value->amount;
            $list[$cusID][] = $amount;
        }

        return $list ? $list : [];
    }
    
    /**
     * @data array
     * @select array
     * @return mixed
    */
    
    public function getWoLaborQuery($data, $select)
    {
        $cancelStatus = config('constants.odr_status.CANCELLED');
        
        $eventCode = [
            config('constants.event.ORD-SHIPPED'),
            config('constants.event.PR-SHIPPED')
        ];
        
        $param = [
            'ordIDs' => $select['orderClause'],
            'cusIDs' => $select['custClause'],
            'whsID' => $select['whsID'],
            'hisOrdNums' => $select['hisOrdNums'],
            'labor' => $select['laborClause'],
            'cancel' => $cancelStatus
        ];

        $result = $this->labor
                        ->join('wo_hdr AS wh', 'wh.wo_hdr_id', '=', 'labor.ref_id')
                        ->join('odr_hdr AS oh', 'oh.odr_id', '=', 'wh.odr_hdr_id')
//                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'oh.odr_num')
                        ->select('labor.cus_id', 'labor.whs_id', 'labor.type', 
                                  'urgency', DB::raw('SUM(amount) AS amount')
                                )
                        ->where(function($query) use($data, $param, $eventCode) 
                        {
//                            $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '>=', $data['startDate']);
//                            $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '<=', $data['endDate']);
                            $query->where('oh.shipped_dt', '>=', strtotime($data['startDate']));
                            $query->where('oh.shipped_dt', '<=', strtotime($data['endDate'] . ' 23:59:59'));
                            $query->where('inv_sts', 1);
                            $query->where('category', config('constants.labor_cat.WO'));
                            $query->where('labor.type', config('constants.labor_type.ACTUAL'));
//                            $query->whereIn('evt_code', $eventCode);
                            $query->where('oh.odr_sts', Status::getByValue('Shipped', 'ORDER-STATUS'));
                            $query->where('urgency', $param['labor']);
                            $query->where('oh.odr_sts', '!=' , $param['cancel']);

                            if ($param['ordIDs']) {
                                $query->whereIn('wh.odr_hdr_id', $param['ordIDs']);
                            }

                            if ($param['cusIDs']) {
                                $query->whereIn('labor.cus_id', $param['cusIDs']);
                            }
                            
                            if ($param['whsID']) {
                                $query->where('labor.whs_id', $param['whsID']);
                            }

                            if ($param['hisOrdNums']) {
                                $query->whereNotIn('wh.odr_hdr_num', $param['hisOrdNums']);
                            }
                         })
                         ->groupBy('labor.cus_id', 'labor.whs_id', 'labor.type')
                         ->get();

        $list = [];

        foreach ($result as $value) {
            $cusID = $value->cus_id;
            $amount = $value->amount;
            $list[$cusID][] = $amount;
        }

        return $list ? $list : [];
    }

}
