<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemState;

class SystemStateModel extends AbstractModel
{

    /**
     * SystemStateModel constructor.
     */
    public function __construct()
    {
        $this->model = new SystemState();
    }

    public function fetchColumn($columns = [], $where = "", $params = [], $upperCase = false)
    {
        $tblName = $this->getTable();
        $colNames = implode(', ', $columns) . ", sys_country_code";

        $query = "
            SELECT $colNames
            FROM $tblName s, system_country c
            WHERE c.sys_country_id = s.sys_state_country_id
        ";
        if (!empty($where)) {
            $query .= " and $where";
        }

        $result = [];

        $db = $this->model->getConnection()->getPdo();

        $statement = $db->prepare($query);
        $statement->execute($params);

        $colKey = $columns;
        end($columns);
        $endKey = key($columns);
        unset($colKey[$endKey]);

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $rowRight = $row;
            $rowLeft = array_splice($rowRight, 0, (count($rowRight) - 1));

            $val = $row[$columns[$endKey]];
            $key = implode("-", $rowLeft);

            if ($upperCase) {
                $key = strtoupper($key);
            }

            $result[$key] = $val;
        }

        return $result;
    }
}
