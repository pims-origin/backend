<?php

/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 25-May-16
 * Time: 15:10
 */

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Response as IlluminateResponse;

class MenuGroupTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var
     */
    protected $url;

    /**
     * MenuGroupTest constructor.
     */
    public function __construct()
    {
        $this->url = "/v1/groups";
    }

    public function testShow_Ok()
    {
        $menuGroup = factory(App\MenuGroup::class)->create();

        $response = $this->call('GET', $this->url . "/{$menuGroup->menu_group_id}");

        $this->assertEquals(IlluminateResponse::HTTP_OK, $response->status());

        $responsedItem = json_decode($response->content())->data;

        $this->assertEquals($menuGroup->name, $responsedItem->name);
    }

    public function testShow_Fail()
    {
        $response = $this->call('GET', $this->url . "/999999");

        $this->assertEquals(IlluminateResponse::HTTP_BAD_REQUEST, $response->status());
    }

    /**
     * Test Create Menu Group
     *
     * No Input
     * Return OK
     */
    public function testCreate_Ok()
    {
        $name = "test case testCreate_Ok";

        $response = $this->call('POST', $this->url, [
            'name'        => $name,
            'description' => "This menu group from test case",
        ]);

        $this->assertEquals(201, $response->status());
    }

    /**
     * Test Create new Menu Group with exist Name in DB
     *
     * Input: Name, Description
     *
     * Output: Error
     */
    public function testCreate_NameExist_Error()
    {
        $menuGroup = factory(App\MenuGroup::class)->create();

        $response = $this->call('POST', $this->url, [
            'name'        => $menuGroup->name,
            'description' => "New Menu Description",
        ]);

        $this->assertEquals(400, $response->status());
    }

    /**
     * Test Update Menu Group
     *
     * No Input
     * Return OK
     */
    public function testUpdate_Ok()
    {
        $menuGroup = factory(App\MenuGroup::class)->create();

        $response = $this->call('PUT', $this->url . "/{$menuGroup->menu_group_id}", [
            'name'        => "Menu Group edited",
            'description' => "New Menu Description Edited",
        ]);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Test Update Menu Group with exist Name in DB
     *
     * Input: Name, Description
     *
     * Output: Error
     */
    public function testUpdate_NameExist_Error()
    {
        $menuGroup1 = factory(App\MenuGroup::class)->create();
        $menuGroup2 = factory(App\MenuGroup::class)->create();

        $response = $this->call('PUT', $this->url . "/{$menuGroup2->menu_group_id}", [
            'name'        => $menuGroup1->name,
            'description' => "New Menu Description Edited",
        ]);

        $this->assertEquals(400, $response->status());

        $responsedItem = json_decode($response->content())->errors;

        $this->assertContains('Menu Group name is existed', $responsedItem->message);
    }

    /**
     * Test Delete Menu Group
     *
     * AssertEqual code 200
     */
    public function testDelete_Ok()
    {
        $menuGroup = factory(App\MenuGroup::class)->create();

        $response = $this->call('DELETE', "{$this->url}/{$menuGroup->menu_group_id}");

        $this->assertEquals(\Dingo\Api\Http\Response::HTTP_NO_CONTENT, $response->status());
    }

    public function testDelete_Fail()
    {
        $response = $this->call('DELETE', "{$this->url}/999999");

        $this->assertEquals(IlluminateResponse::HTTP_BAD_REQUEST, $response->status());
    }

    /**
     * Test List Menu Group
     *
     * No Input
     * AssertEqual
     */
    public function testList()
    {
        $response = $this->call('GET', $this->url);
        $this->assertEquals(200, $response->status());
    }
}
