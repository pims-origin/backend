<?php

namespace App\Api\V1\Models;

abstract class AbstractModel
{
    /**
     * @var object
     */
    protected $model;

    /**
     * Get empty model.
     *
     * @return object Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Refresh model to be clean
     *
     * @return object Model
     */
    public function refreshModel()
    {
        $this->model = $this->model->newInstance();
    }

    /**
     * Get table name.
     *
     * @return string
     */
    public function getTable()
    {
        return $this->model->getTable();
    }

    /**
     * Make a new instance of the entity to query on.
     *
     * @param array $with
     *
     * @return object
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    /**
     * Find a single entity by key value.
     *
     * @param string $key
     * @param string $value
     * @param array $with
     *
     * @return object|null
     */
    public function getFirstBy($key, $value, array $with = [])
    {
        $query = $this->make($with);
        $this->model->filterDataIn($query, true, false);
        return $query->where($key, '=', $value)->first();
    }

    /**
     * Generate query for methods: findWhere, getFirstWhere, checkWhere
     *
     * @param $where
     * @param array $with
     * @param bool $or
     *
     * @return object QueryBuilder
     */
    protected function getQuery($where, array $with = [], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }
        $this->model->filterDataIn($query, true, false);
        return $query;
    }

    /**
     * Get One collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $columns
     * @param bool $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getFirstWhere($where, array $with = [], $columns = ['*'], $or = false)
    {
        $query = $this->getQuery($where, $with, $or);

        return $query->first($columns);
    }

    /**
     * Find a collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $columns
     * @param bool $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function findWhere($where, array $with = [], $columns = ['*'], $or = false)
    {
        $query = $this->getQuery($where, $with, $or);

        return $query->get($columns);
    }

    /**
     * @param array $where
     * @param bool $or
     *
     * @return mixed
     */
    public function checkWhere($where, $or = false)
    {
        $query = $this->getQuery($where, [], $or);

        return $query->count();
    }

    /**
     * Get a model by id  regardless of status.
     *
     * @param int $id model ID
     * @param array $with
     *
     * @return object|null
     */
    public function byId($id, array $with = [])
    {
        $query = $this->make($with)->where($this->model->getKeyName(), $id);
        $this->model->filterDataIn($query, true, false);
        $model = $query->firstOrFail();

        return $model;
    }

    /**
     * Get next model.
     *
     * @param object $model
     * @param array $with
     *
     * @return object|null
     */
    public function next($model, array $with = [])
    {
        return $this->adjacent(1, $model, $with);
    }

    /**
     * Get prev model.
     *
     * @param object $model
     * @param array $with
     *
     * @return object|null
     */
    public function prev($model, array $with = [])
    {
        return $this->adjacent(-1, $model, $with);
    }

    /**
     * Get prev model.
     *
     * @param int $direction
     * @param object $model
     * @param array $with
     *
     * @return object|null
     */
    public function adjacent($direction, $model, array $with = [])
    {
        $currentModel = $model;
        $models = $this->all($with);

        foreach ($models as $key => $model) {
            if ($currentModel->{$this->model->getKeyName()} == $model->{$this->model->getKeyName()}) {
                $adjacentKey = $key + $direction;

                return isset($models[$adjacentKey]) ? $models[$adjacentKey] : null;
            }
        }
    }

    /**
     * Get paginated models.
     *
     * @param int $page Number of models per page
     * @param int $limit Results per page
     * @param array $with Eager load related models
     *
     * @return object with object $items && int $totalItems
     */
    public function byPage($page = 1, $limit = 10, array $with = [])
    {
        $result = new stdClass();
        $result->page = $page;
        $result->limit = $limit;
        $result->totalItems = 0;
        $result->items = [];
        $query = $this->make($with);

        $totalItems = $query->count();
        $query->skip($limit * ($page - 1))
            ->take($limit);
        $models = $query->get();
        // Put items and totalItems in stdClass
        $result->totalItems = $totalItems;
        $result->items = $models->all();

        return $result;
    }

    /**
     * Get all models.
     *
     * @param array $with Eager load related models
     *
     * @return object Collection
     */
    public function all(array $with = [])
    {
        $query = $this->make($with);
        $this->model->filterDataIn($query, true, false);
        // Get
        return $query->get();
    }

    /**
     * Get all models by key/value.
     *
     * @param string $key
     * @param string $value
     * @param array $with
     *
     * @return object $models
     */
    public function allBy($key, $value, array $with = [])
    {
        $query = $this->make($with);

        $query->where($key, $value);
        $this->model->filterDataIn($query, true, false);
        // Get
        $models = $query->get();

        return $models;
    }

    /**
     * Get latest models.
     *
     * @param int $number number of items to take
     * @param array $with array of related items
     *
     * @return object Collection
     */
    public function latest($number = 10, array $with = [])
    {
        $query = $this->make($with);
        $this->model->filterDataIn($query, true, false);

        return $query->take($number)->get();
    }

    /**
     * Get single model by Slug.
     *
     * @param string $slug slug
     * @param array $with related tables
     *
     * @return object|null
     */
    public function bySlug($slug, array $with = [])
    {
        $query = $this->make($with)
            ->where('slug', '=', $slug);
        $this->model->filterDataIn($query, true, false);
        $model = $query->firstOrFail();

        return $model;
    }

    /**
     * Return all results that have a required relationship.
     *
     * @param string $relation
     * @param array $with
     *
     * @return object Collection
     */
    public function has($relation, array $with = [])
    {
        $entity = $this->make($with);

        return $entity->has($relation)->get();
    }

    /**
     * Create a new model.
     *
     * @param array $data
     *
     * @return object|false
     */
    public function create(array $data)
    {
        // Create the model

        $model = $this->model->fill($data);

        if ($model->save()) {

            return $model;
        }

        return false;
    }

    /**
     * Update an existing model.
     *
     * @param array $data
     *
     * @return object|false
     */
    public function update(array $data)
    {
        $model = $this->model->findOrFail($data[$this->model->getKeyName()]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    /**
     * Sort models.
     *
     * @param array $data updated data
     *
     * @return none
     */
    public function sort(array $data)
    {
        foreach ($data['item'] as $position => $item) {
            $page = $this->model->find($item[$this->model->getKeyName()]);
            $sortData = $this->getSortData($position + 1, $item);
            $page->update($sortData);
        }
    }

    /**
     * Get sort data.
     *
     * @param int $position
     *
     * @return array
     */
    protected function getSortData($position)
    {
        return [
            'position' => $position,
        ];
    }

    /**
     * Delete model.
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete($model)
    {
        return $model->delete();
    }

    /**
     * Delete model By Ids
     *
     * @param array|int $ids
     *
     * @return bool
     */
    public function deleteById($ids)
    {
        $ids = is_array($ids) ? $ids : [$ids];

        return $this->model->destroy($ids);
    }

    /**
     * Sync related items for model.
     *
     * @param Model $model
     * @param array $data
     * @param string $table
     *
     * @return none
     */
    public function syncRelation($model, array $data, $table = null)
    {
        if (!method_exists($model, $table)) {
            return false;
        }
        if (!isset($data[$table])) {
            return false;
        }
        // add related items
        $pivotData = [];
        $position = 0;
        if (is_array($data[$table])) {
            foreach ($data[$table] as $id) {
                $pivotData[$id] = ['position' => $position++];
            }
        }
        // Sync related items
        $model->$table()->sync($pivotData);
    }

    /**
     * @param $query
     * @param array $attributes
     *
     * @return bool
     */
    public function sortBuilder(&$query, $attributes = [])
    {
        $validConditions = ['asc', 'desc'];
        $validColumn = $this->model->getTableColumns();

        if (empty($attributes['sort'])) {
            return false;
        }

        foreach ($attributes['sort'] as $key => $value) {

            if (!$value) {
                $value = 'asc';
            }

            if (!in_array($value, $validConditions)) {
                continue;
            }

            if (!in_array($key, $validColumn)) {
                continue;
            }

            $query->orderBy($key, $value);
        }
    }

    public function updateWhere($dataUpdated, $where)
    {
        $query = $this->make();
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query->where($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query->where($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query->where($field, '=', $search);
                }
            } else {
                $query->where($field, '=', $value);
            }
        }

        return $query->update($dataUpdated);
    }
}
