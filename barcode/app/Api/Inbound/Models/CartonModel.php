<?php
/**
 * Created by PhpStorm.
 * User: Phuc
 * Date: 3/8/2017
 * Time: 9:32 AM
 */

namespace App\Api\Inbound\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;

class CartonModel extends AbstractModel
{
    /**
     * PalletModel constructor.
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    public function setDamagedCartons(int $grID){

        $sql = sprintf("
            REPLACE INTO damage_carton (ctn_id,created_at,created_by,updated_at,updated_by,deleted,deleted_at,
                    dmg_id,dmg_note)
            SELECT
                ctn_id,created_at,created_by,updated_at,updated_by,deleted,deleted_at,1 AS dmg_id,' ' AS dmg_note
            FROM cartons 
            WHERE gr_hdr_id =%d AND is_damaged = 1
        ", $grID);

       return DB::insert($sql);
    }
}