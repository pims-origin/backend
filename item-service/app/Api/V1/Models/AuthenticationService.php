<?php
namespace App\Api\V1\Models;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use GuzzleHttp\Exception\RequestException;

class AuthenticationService extends BaseService
{
    const USER_TOKEN_URI = 'users/token';
    const USER_LOGIN_URI = 'users/login';
    const USER_LOGOUT_URI = 'users/logout';

    /**
     * AuthorizationService constructor.
     * @param $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_AUTHENTICATION');
        parent::__construct($request, $api);
    }

    public function getUserIdByToken($token)
    {
        $options = [
            'headers' => [
                'Authorization' => $token
            ]
        ];
        try {
            $response = $this->client->post(self::USER_TOKEN_URI, $options);
            if ($response->getStatusCode() != 200) {
                return false;
            }
            $responseData = json_decode($response->getBody(), true);
            if (empty($responseData['data']) || empty($responseData['data']['user_id'])) {
                return false;
            }
            return $responseData['data']['user_id'];
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getUsers()
    {
        try {
            $response = $this->client->get(env('USER_URI'));
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function login(IRequest $request)
    {
        $body = [
            'form_params' => $request->getParsedBody()
        ];
        try {
            $response = $this->client->post(self::USER_LOGIN_URI, $body);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function logout(IRequest $request)
    {
        try {
            $response = $this->client->post(self::USER_LOGOUT_URI);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }
}
