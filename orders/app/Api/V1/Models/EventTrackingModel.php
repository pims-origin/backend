<?php

namespace App\Api\V1\Models;


use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Utils\SelStr;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * @param EventTracking $model
     */
    public function __construct(EventTracking $model)
    {
        $this->model = $model;
    }

    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);

        if (isset($attributes['evt_code'])) {
            $query->where('evt_code', 'like', "%" . SelStr::escapeLike($attributes['evt_code']) . "%");
        }

        if (isset($attributes['owner'])) {
            $query->where('owner', 'like', "%" . SelStr::escapeLike($attributes['owner']) . "%");
        }

        if (isset($attributes['trans_num'])) {
            $query->where('trans_num', 'like', "%" . SelStr::escapeLike($attributes['trans_num']) . "%");
        }
        $this->model->filterData($query, true);
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);

    }


}
