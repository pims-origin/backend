<?php

namespace App\Api\Ecom\Controllers;

use App\Api\Ecom\Models\CartonModel;
use App\Api\Ecom\Models\XferDetailModel;
use App\Api\Ecom\Models\XferHdrModel;
use App\Api\Ecom\Models\XferTicketModel;
use App\Api\Ecom\Transformers\XferHdrTransformer;
use App\Api\Ecom\Transformers\XferTransformer;
use App\Api\Ecom\Validators\XferPutawayValidator;
use App\Api\Ecom\Validators\XferValidator;
use App\Api\Ecom\Models\PalletModel;
use App\Api\Ecom\Models\EventTrackingModel;
use App\Utils\JWTUtil;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;
use Illuminate\Http\Response as IlluminateResponse;

class XferController extends AbstractController
{
    const XFER_STATUS = [
        'complete_dtl'    => 1,
        'complete_hdr'    => 2,
        'complete_ticket' => 3
    ];

    const PLT_TOTE_NUM = 'VLPN-0001';

    protected $xferTicketModel;

    protected $palletModel;

    protected $xferDetailModel;

    protected $xferHdrModel;

    protected $cartonModel;

    protected $eventTrackingModel;


    public function __construct(
        XferTicketModel $xferTicketModel,
        PalletModel     $palletModel,
        XferDetailModel $xferDetailModel,
        XferHdrModel    $xferHdrModel,
        CartonModel    $cartonModel,
        EventTrackingModel    $eventTrackingModel
    ) {
        $this->xferTicketModel = $xferTicketModel;
        $this->palletModel     = $palletModel;
        $this->xferDetailModel = $xferDetailModel;
        $this->xferHdrModel    = $xferHdrModel;
        $this->cartonModel    = $cartonModel;
        $this->eventTrackingModel = $eventTrackingModel;
    }

    public function search($whsId, Request $request, XferTransformer $xferTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        try {
            $xFerList = $this->xferTicketModel->getModel()
                ->select([
                    'xfer_ticket.*',
                    DB::raw('COUNT(xfer_hdr.xfer_hdr_id) as ttl_item'),
                    DB::raw('COUNT(IF(xfer_hdr.xfer_sts = "CO", 1, NULL)) as co_item')
                ])
                ->join('xfer_hdr', 'xfer_hdr.xfer_ticket_id', '=', 'xfer_ticket.xfer_ticket_id')
                ->where('xfer_ticket.whs_id', $whsId)
                ->where('xfer_hdr.whs_id', $whsId)
                ->where('xfer_ticket.xfer_ticket_sts', 'PK')
                ->groupBy('xfer_ticket.xfer_ticket_id')
                ->orderBy('xfer_ticket.created_at', 'DESC')
                ->paginate(array_get($input, 'limit', 10));
            
            return $this->response->paginator($xFerList, $xferTransformer);
            
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show ($whsId, $xTicketNum, Request $request, XferHdrTransformer $xferHdrTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        try {

            $xferHdr = $this->xferHdrModel->getModel()
                ->select([
                    'xfer_ticket.xfer_ticket_num',
                    'xfer_hdr.xfer_hdr_id',
                    'xfer_hdr.xfer_hdr_num',
                    'xfer_hdr.item_id',
                    'xfer_hdr.cus_id',
                    'xfer_hdr.sku',
                    'xfer_hdr.size',
                    'xfer_hdr.color',
                    'xfer_hdr.cus_upc',
                    DB::raw("SUM(IF(xfer_dtl.xfer_dtl_sts = 'PI', xfer_dtl.allocated_qty, 0)) as act_qty"),
                    DB::raw('SUM(xfer_dtl.allocated_qty) as remain_qty')
                ])
                ->join('xfer_ticket', 'xfer_ticket.xfer_ticket_id', '=', 'xfer_hdr.xfer_ticket_id')
                ->join('xfer_dtl', 'xfer_dtl.xfer_hdr_id', '=', 'xfer_hdr.xfer_hdr_id')
                ->where('xfer_ticket.whs_id', $whsId)
                ->where('xfer_hdr.whs_id', $whsId)
                ->where('xfer_ticket.xfer_ticket_num', $xTicketNum)
                ->where('xfer_ticket.xfer_ticket_sts', 'PK')
                ->where('xfer_hdr.xfer_sts', 'PK')
                ->groupBy('xfer_hdr.xfer_hdr_id')
                ->orderBy('xfer_hdr.xfer_sts', 'DESC')
                ->paginate(array_get($input, 'limit', 10));

            if (empty($xferHdr)) {
                return $this->response->errorBadRequest('Xfer Ticket is not exists!');
            }

            return $this->response->paginator($xferHdr, $xferHdrTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function detail ($whsId, $xTicketNum, $xHdrNum, Request $request)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        try {
             $xDtls = $this->xferDetailModel->getModel()
                ->join('xfer_hdr', 'xfer_hdr.xfer_hdr_id', '=', 'xfer_dtl.xfer_hdr_id')
                ->join('xfer_ticket', 'xfer_ticket.xfer_ticket_id', '=', 'xfer_dtl.xfer_ticket_id')
                ->where('xfer_ticket.xfer_ticket_num', $xTicketNum)
                ->where('xfer_hdr.xfer_hdr_num', $xHdrNum)
                ->where('xfer_ticket.whs_id', $whsId)
                ->where('xfer_dtl.xfer_dtl_sts', 'PK')
                ->groupBy('xfer_dtl.xfer_dtl_id')
                ->orderBy('xfer_dtl.mez_loc_id')
                ->get();

            if (count($xDtls) == 0) {
                return $this->response->errorBadRequest('Item is transfered!');
            }

            $currentXferDtl = $xDtls[0];

            $sku_size_color = $currentXferDtl['sku'];
            if (!empty($currentXferDtl['size']) && $currentXferDtl['size'] != 'NA') {
                $sku_size_color .= $currentXferDtl['size'];
            }
            if (!empty($currentXferDtl['color']) && $currentXferDtl['color'] != 'NA') {
                $sku_size_color .= $currentXferDtl['color'];
            }

            $act_qty = $this->xferDetailModel->getModel()
                ->where('xfer_dtl.xfer_hdr_id', $currentXferDtl['xfer_hdr_id'])
                ->where('xfer_dtl.xfer_dtl_sts', 'PI')
                ->sum('allocated_qty');

            $remain_qty = 0;
            foreach ($xDtls as $xDtl) {
                //$act_qty += ($xDtl['xfer_dtl_sts'] == 'PI') ? $xDtl['allocated_qty'] : 0;
                $remain_qty += !empty($xDtl['allocated_qty']) ? $xDtl['allocated_qty'] : 0;
            }

            $cartons = DB::table('cartons')
                ->where('item_id', $currentXferDtl['item_id'])
                ->where('loc_id', $currentXferDtl['req_loc_id'])
                ->where('cus_id', $currentXferDtl['cus_id'])
                ->where('deleted', 0)
                ->where('ctn_sts', 'AC')
                ->whereRaw('(is_ecom is null or is_ecom = 0)')
                ->select(
                    DB::raw('SUM(piece_remain) AS piece_remain'),
                    'ctn_pack_size', 'upc'
                )->first();

            $is_last = (count($xDtls) <= 1) ? true : false;

            return [
                'data' => [
                    'xfer_ticket_id'  => $currentXferDtl['xfer_ticket_id'],
                    'xfer_ticket_num' => $currentXferDtl['xfer_ticket_num'],
                    'xfer_hdr_id'     => $currentXferDtl['xfer_hdr_id'],
                    'xfer_hdr_num'    => $currentXferDtl['xfer_hdr_num'],
                    'xfer_dtl_id'     => $currentXferDtl['xfer_dtl_id'],
                    'item_id'         => $currentXferDtl['item_id'],
                    'sku'             => $sku_size_color,
                    'cus_upc'         => $currentXferDtl['cus_upc'],
                    'ucc128'          => Item::generateUpc128($currentXferDtl['cus_id'], $currentXferDtl['item_id']),
                    'to_loc_id'       => $currentXferDtl['mez_loc_id'],
                    'to_loc_code'     => $currentXferDtl['mez_loc'],
                    'qty'             => $act_qty. '/' . ($remain_qty + $act_qty),
                    'from_loc_id'     => $currentXferDtl['req_loc_id'],
                    'from_loc_code'   => $currentXferDtl['req_loc'],
                    'avail_qty'       => array_get($cartons, 'piece_remain', 0),
                    'is_last'         => $is_last,
                    'cus_id'          => $currentXferDtl['cus_id'],
                    'avail_qty'       => array_get($cartons, 'piece_remain', 0),
                    'pack'            => array_get($cartons, 'ctn_pack_size'),
                    'upc'             => array_get($cartons, 'upc'),
                    'pick_more'       => count($xDtls) > 1 ? 1 : 0
                ]
            ];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function update($whsId, Request $request, XferValidator $xferValidator)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $input = $request->getParsedBody();

        $userId = Data::getCurrentUserId();
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        $xferValidator->validate($input);

        try {
            //validate input
            $this->validateUpdateXferTicket($whsId, $input);

            $cartonsForXfer = $this->getCartonsToPick($whsId, $input);

            DB::beginTransaction();

            $pltToteId = $this->transferCartons($cartonsForXfer, $input, $userId);

            //clone to mez loc or update
            //$toLocArr = ['loc_id' => $input['to_loc_id'], 'loc_code' => $input['to_loc_code']];
            //$this->processMezLoc($userId, $cloneCtn, $toLocArr, $input['pick_qty']);

            // update xfer_dtl
            $xferDtlObj = $this->updateXferDtl($pltToteId, $input);

            if ((!empty($input['is_last']) && $input['is_last']) || (!empty($input['complete']) && $input['complete'] == 1)) {
                // complete xfer hdr
                $this->xferHdrModel->updateWhere(['xfer_sts' => 'PI'], ['xfer_hdr_id' => $input['xfer_hdr_id']]);

                // check exist xfer_hdr not complete
                $xferHdrNotComplete = $this->xferHdrModel->getModel()
                    ->where('xfer_ticket_id', $input['xfer_ticket_id'])
                    ->where('xfer_sts', 'PK')
                    ->count();

                if ($xferHdrNotComplete <= 0) {
                    $this->xferTicketModel->updateWhere(['xfer_ticket_sts' => 'PI'], ['xfer_ticket_id' => $input['xfer_ticket_id']]);
                    $result = [
                        'message' => 'Xfer Ticket is completed!',
                        'status'  => self::XFER_STATUS['complete_ticket'],
                        'data'    => [
                            'xfer_ticket_num' => $input['xfer_ticket_num'],
                            'xfer_hdr_num'    => $input['xfer_hdr_num']
                        ]
                    ];
                } else {
                    $result = [
                        'message' => 'XferHdr is picked!',
                        'status'  => self::XFER_STATUS['complete_hdr'],
                        'data'    => [
                            'xfer_ticket_num' => $input['xfer_ticket_num'],
                            'xfer_hdr_num'    => $input['xfer_hdr_num']
                        ]
                    ];
                }
            } else {
                $result = [
                    'message' => 'Xfer Detail is picked!',
                    'status'  => self::XFER_STATUS['complete_dtl'],
                    'data'    => [
                        'xfer_ticket_num' => $input['xfer_ticket_num'],
                        'xfer_hdr_num'    => $input['xfer_hdr_num']
                    ]
                ];

                if ($xferDtlObj->xfer_dtl_sts == 'PK') {
                    $result['message'] = 'Xfer Detail is picking!';
                    $result['status'] = false;
                }
            }

            if ($result['status'] == 2) {
                $this->xferDetailModel->updateWhere(['xfer_dtl_sts' => 'PI'], ['xfer_hdr_id' => $input['xfer_hdr_id']]);
            }

            if ($result['status'] == 3) {
                $this->xferDetailModel->updateWhere(['xfer_dtl_sts' => 'PI'], ['xfer_ticket_id' => $input['xfer_ticket_id']]);
            }

            $this->createEventTracking($whsId, $input, $result['status']);

            DB::commit();
            return $result;

        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function getCartonsToPick($whsId, $input)
    {
        $item_id = $input['item_id'];
        $pick_qty = $input['pick_qty'];
        $cus_id = $input['cus_id'];
        $from_loc = $input['from_loc_id'];

        DB::statement("SET @sum = 0;");

        $result = DB::select("
            SELECT ctn_id FROM (
            SELECT *, IF(@sum < $pick_qty, @sum:=@sum + piece_remain, -1) AS mysum FROM cartons
            WHERE item_id = $item_id
                AND whs_id = $whsId
                AND cus_id = $cus_id
                AND loc_id = $from_loc
                AND loc_type_code = 'RAC'
                AND is_damaged = 0
                AND is_ecom = 0
                AND deleted = 0
                AND ctn_sts = 'AC' 
                AND @sum >= 0
            ORDER BY piece_remain ) t WHERE mysum > 0
        ");

        $checkPieceRemain = DB::select("SELECT @sum AS sum_cartons");

        $sum_cartons = $checkPieceRemain[0]['sum_cartons'];

        if ($sum_cartons < $input['pick_qty']) {
            throw new HttpException(400, "Location ". $input['from_loc_code'] ." is insufficient to transfer. Please select another");
        }

        return array_pluck($result, 'ctn_id');
    }

    private function transferCartons($ctnIds, $input, $userId)
    {
        $pickQty = $input['pick_qty'];
        if (empty($ctnIds)) {
            return true;
        }
        // list cartons
        $cartons = DB::table('cartons')
            ->where('deleted', 0)
            ->whereIn('ctn_id', $ctnIds)
            ->get();

        //list pallet
        $pallets = DB::table('cartons')
            ->select('cartons.*', DB::raw('COUNT(cartons.ctn_id) AS ctn_ttl'), 'cartons.plt_id')
            ->whereIn('ctn_id', $ctnIds)
            ->whereNotNull('plt_id')
            ->where('deleted', 0)
            ->groupBy('plt_id')
            ->get();
        $pltIds = array_pluck($pallets, 'plt_id');

        // Get Pallet Tote Id
        $pltToteId = $this->createPalletToteId($pallets, $input);

        // update piece_remain and piece_ttl when pickQty is odd
        foreach ($cartons as $carton) {
            if ($pickQty > 0) {
                if ($pickQty < $carton['piece_remain']) {
                    DB::table('cartons')
                        ->where('ctn_id', $carton['ctn_id'])
                        ->update([
                            'piece_remain' => $carton['piece_remain'] - $pickQty,
                            'piece_ttl' => $carton['piece_remain'] - $pickQty
                        ]);
                    $ctnIds = array_diff($ctnIds, [$carton['ctn_id']]);

                    // Create Carton Pick Piece
                    $this->createCartonPickPiece($carton, $pltToteId, $pickQty, $userId);
                }
                $pickQty -= $carton['piece_remain'];
            }
        }

        if (!empty($ctnIds)) {
            // remove cartons and update pallet
            $ctnIds = implode(',', $ctnIds);
            $query = "
                UPDATE cartons c, pallet p
                INNER JOIN (
                    SELECT plt_id, count(*) as tt FROM cartons 
                    WHERE ctn_id IN ($ctnIds) 
                        AND deleted = 0
                    GROUP BY plt_id
                ) as j ON p.plt_id = j.plt_id
                SET c.ctn_sts = 'TF', c.loc_id = NULL,  c.loc_code = NULL, c.plt_id = $pltToteId,
                    p.ctn_ttl = p.ctn_ttl - j.tt, c.is_ecom = 1, c.is_damaged = 0
                WHERE c.plt_id = p.plt_id 
                    AND c.ctn_id IN ($ctnIds) 
            ";
            DB::update($query);
        }

        // update pallet
        if (empty($pltIds)) {
            return true;
        }

        $palletInfos = DB::table('pallet')
            ->whereIn('plt_id', $pltIds)
            ->where('deleted', 0)
            ->get();
        foreach ($palletInfos as $palletInfo) {
            $created_at = array_get($palletInfo, 'created_at', 0);
            $zeroDt = array_get($palletInfo, 'zero_date', 0);

            // Calculate storage_duration
            $date1 = date("Y-m-d", is_int($created_at) ?: $created_at->timestamp );
            $date2 = date("Y-m-d", is_int($zeroDt) ?: $created_at );

            $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }

            DB::table('pallet')
                ->where('plt_id', $palletInfo['plt_id'])
                ->update([
                    'storage_duration' => $storageDuration
                ]);
        }

        $pltIds = implode(',', $pltIds);
        $date = strtotime(date("Y-m-d", time()));
        $query = "
            UPDATE pallet p 
            LEFT JOIN cartons c ON c.plt_id = p.plt_id
            SET p.loc_id = NULL, p.loc_code = NULL, p.zero_date = $date
            WHERE p.plt_id IN ($pltIds) AND c.plt_id IS NULL";
        DB::update($query);


        return $pltToteId;
    }

    private function validateUpdateXferTicket ($whsId, $input)
    {
        // check status xfer_ticket and xfer_hdr: exists and is picking
        $xferTicket = $this->xferTicketModel->byId($input['xfer_ticket_id'], ['xferHdr', 'xferDtl']);
        if (empty($xferTicket)) {
            throw new HttpException(400, 'Ticket is not exists.');
        }
        if ($xferTicket->xfer_ticket_sts == 'PI') {
            throw new HttpException(400, 'Ticket status is Picked');
        }
        if ($xferTicket->xfer_ticket_sts !== 'PK') {
            throw new HttpException(400, 'Ticket status isn\'t Picking');
        }

        foreach ($xferTicket->xferDtl as $xferDtl) {
            if ($xferDtl->xfer_dtl_id == $input['xfer_dtl_id']) {
                if ($xferDtl->xfer_dtl_sts == 'CO') {
                    throw new HttpException(400, 'Location '. $input['from_loc_code']. ' has been transfered!');
                }
                break;
            }
        }

        // check location from and to is Active
        $locChk = DB::table('location AS l')
            ->select('l.loc_id')
            ->join('loc_type AS lt', 'l.loc_type_id', '=', 'lt.loc_type_id')
            ->where('l.deleted', 0)
            ->where('l.loc_sts_code', 'AC')
            ->whereIn('l.loc_id', [$input['from_loc_id'], $input['to_loc_id']])
            ->pluck('l.loc_id')->toArray();
        $error = '';
        if (!in_array($input['from_loc_id'], $locChk)) {
            $error .= " Request location " . $input['from_loc_code'] . " not available.";
        }
        if (!in_array($input['to_loc_id'], $locChk)) {
            $error .= "To location " . $input['to_loc_code'] . " not available.";
        }
        if ($error) {
            throw new HttpException(400, $error);
        }

        // check item is Active in ItemMaster
        $itemChk = DB::table('item')
            ->where('item_id', $input['item_id'])
            ->where('status', 'AC')
            ->where('deleted', 0)
            ->first();
        if (empty($itemChk)) {
            throw new HttpException(400, 'Item '. $input['item_id'] . ' is not available.');
        }

        // check cartons is exists location ECOM with sku-size-color-cus_id
        $cartonsChk = DB::table('cartons')
            ->where('cartons.loc_id', '<>', $input['to_loc_id'])
            ->where('cartons.whs_id', $whsId)
            ->where([
                'cartons.sku'    => $itemChk['sku'],
                'cartons.cus_id' => $itemChk['cus_id'],
                'cartons.size'   => $itemChk['size'],
                'cartons.color'  => $itemChk['color'],
                'cartons.lot'    => 'ECO'
            ])
            ->where('cartons.deleted', 0)
            ->where('is_ecom', 1)
            ->get();
        if (count($cartonsChk) > 0) {
            $arrLoc = [];
            foreach ($cartonsChk as $ctnChk) {
                array_push($arrLoc, $ctnChk['loc_code']);
            }
            $strLoc = implode(', ', $arrLoc);
            throw new HttpException(400, "Item is currently processing in location " . $strLoc);
        }

        return $xferTicket;
    }

    private function updateXferDtl ($pltToteId, $input)
    {
        $xferDtlObj = $this->xferDetailModel->byId($input['xfer_dtl_id']);
        $xferDtlObj->act_req_loc_id = $input['from_loc_id'] ?: null;
        $xferDtlObj->act_req_loc = $input['from_loc_code'] ?: null;
        $xferDtlObj->mez_loc_id = $input['to_loc_id'];
        $xferDtlObj->mez_loc = $input['to_loc_code'];
        $xferDtlObj->act_piece_qty += (int)$input['pick_qty'];
        $xferDtlObj->xfer_dtl_sts = 'PI';
        /*$ctnTtlPicked = $this->cartonModel->getModel()
            ->where('plt_id', $pltToteId)
            ->where('item_id', $input['item_id'])
            ->where('ctn_sts', 'TF')
            ->sum('piece_remain');

        if ((int)$ctnTtlPicked == (int)$xferDtlObj->allocated_qty) {
            $xferDtlObj->xfer_dtl_sts = 'PI';
        } else {
            $xferDtlObj->xfer_dtl_sts = 'PK';
        }*/
        $xferDtlObj->save();

        return $xferDtlObj;
    }

    private function createPalletToteId($pallets, $input)
    {
        $palletTote =  $this->palletModel->getFirstBy('plt_num', self::PLT_TOTE_NUM);
        $itemChk = array_first($pallets);

        if (!empty($palletTote)) {
            $pltToteId = $palletTote->plt_id;
            $this->palletModel->getModel()
                ->where('plt_id', $pltToteId)
                ->update([
                    'init_piece_ttl' => (int)$palletTote->init_piece_ttl + (int)$input['pick_qty'],
                ]);

        } else {

            $pltTote = $this->palletModel->create([
                'plt_num' =>  self::PLT_TOTE_NUM,
                'whs_id'  => $itemChk['whs_id'],
                'cus_id'  => $itemChk['cus_id'],
                'init_piece_ttl' => (int)$input['pick_qty'],
                'ctn_ttl' => (int)$itemChk['ctn_ttl']
            ]);
            $pltToteId = $pltTote->plt_id;
        }

        return $pltToteId;
    }

    private function updateXferHdrAndXferTicket ($xferTicket)
    {
        $xferTicket->xfer_ticket_sts = 'CO';
        $xferTicket->save();

        $xferHdr = $xferTicket->xferHdr{0};
        $xferHdr->xfer_sts = 'CO';
        $xferHdr->save();
    }

    private function processMezLoc($userId, $cloneCtn, $mezLoc, $actQty)
    {
        //update mezLoc (to loc) item inventory
        $invtObj = InventorySummary::generateInvtItem($cloneCtn['sku'], $cloneCtn['size'], $cloneCtn['color'], 1, 'ECO',
            $cloneCtn['whs_id'], $cloneCtn['cus_id']);
        $invtObj->ttl += $actQty;
        $invtObj->avail += $actQty;
        $invtObj->save();

        //update from loc item inventory
        InventorySummary::where([
            'item_id' => $cloneCtn['item_id'],
            'lot'     => $cloneCtn['lot']
        ])
            ->update([
                'ttl'   => DB::raw('ttl - ' . $actQty),
                'avail' => DB::raw('avail - ' . $actQty),
            ]);
        //----------------------------------------------

        //clone cartons to mezLoc
        $itemId = $invtObj['item_id'];
        $locId = $mezLoc['loc_id'];
        $locCode = $mezLoc['loc_code'];
        $freDate = date('ym');

        $rs = DB::table('cartons')
            ->where([
                'item_id' => $itemId,
                'lot'     => 'ECO',
                'is_ecom' => 1,
                'deleted' => 0,
                'ctn_sts' => 'AC',
                'loc_id'  => $locId
            ])
            ->where('ctn_num', 'LIKE', "CTN-$freDate-Z-%")
            ->first();

        if (!empty($rs)) {
            $rs['piece_remain'] = $rs['piece_ttl'] = $rs['ctn_pack_size'] = $rs['piece_remain'] + $actQty;
            $rs['updated_by'] = $userId;
            $rs['updated_at'] = time();

            DB::table('cartons')
                ->where('ctn_id', $rs['ctn_id'])
                ->update($rs);
        } else {
            $cntMaxNum = $this->getMaxCtnNum();

            $cloneCtn['piece_remain'] = $cloneCtn['piece_ttl'] = $cloneCtn['ctn_pack_size'] = $actQty;
            $cloneCtn['ctn_num'] = ++$cntMaxNum;
            $cloneCtn['loc_id'] = $locId;
            $cloneCtn['loc_name'] = $cloneCtn['loc_code'] = $locCode;
            $cloneCtn['plt_id'] = null;
            $cloneCtn['is_damaged'] = 0;
            $cloneCtn['item_id'] = $itemId;
            $cloneCtn['lot'] = "ECO";

            $cloneCtn['is_ecom'] = 1;
            $cloneCtn['loc_type_code'] = 'ECO';
            $cloneCtn['updated_by'] = $cloneCtn['created_by'] = $userId;
            $cloneCtn['created_at'] = $cloneCtn['updated_at'] = time();
            $cloneCtn['rfid'] = null;

            unset($cloneCtn['ctn_id']);
            DB::table('cartons')->insert($cloneCtn);
        }

    }

    private function getMaxCtnNum()
    {
        $freDate = date('ym');
        $dfCtnNum = "CTN-$freDate-Z-00000";
        $query = "SELECT MAX(ctn_num) AS max_ctn_num FROM cartons WHERE deleted = 0 AND ctn_num LIKE 'CTN-$freDate-Z-%'";

        $ctnNum = DB::selectOne($query);

        if (!$ctnNum['max_ctn_num']) {
            return $dfCtnNum;
        }

        return $ctnNum['max_ctn_num'];
    }


    private function createCartonPickPiece($carton, $pltToteId, $pickQty, $userId)
    {
        // Create Cartons for pallet tote
        $cntMaxNum = $this->getMaxCtnNum();
        $cloneCtn = $carton;
        $cloneCtn['piece_remain'] = $cloneCtn['piece_ttl'] = $cloneCtn['ctn_pack_size'] = $pickQty;
        $cloneCtn['ctn_num'] = ++$cntMaxNum;
        $cloneCtn['plt_id'] = $pltToteId;
        $cloneCtn['is_damaged'] = 0;
        $cloneCtn['ctn_sts'] = 'TF';

        $cloneCtn['is_ecom'] = 1;
        $cloneCtn['updated_by'] = $cloneCtn['created_by'] = $userId;
        $cloneCtn['created_at'] = $cloneCtn['updated_at'] = time();
        $cloneCtn['rfid'] = null;

        unset($cloneCtn['ctn_id']);
        DB::table('cartons')->insert($cloneCtn);
    }

    private function createEventTracking ($whsId, $input, $result = 1)
    {
        $xferDtl = $this->xferDetailModel->getFirstBy('xfer_dtl_id', $input['xfer_dtl_id']);
        switch ($result) {
            case 2:
                $evt = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $input['cus_id'],
                    'owner'      => $input['xfer_ticket_num'],
                    'evt_code'   => Status::getByKey("event", "WHS-XFER-PICKED"),
                    'trans_num'  => $input['xfer_hdr_num'],
                    'info'       => sprintf(Status::getByKey("event-info", "WHS-XFER-PICKED"), $input['pick_qty'],
                        "$xferDtl->sku, $xferDtl->size, $xferDtl->color", $input['from_loc_code'], self::PLT_TOTE_NUM),
                    'created_at' => time(),
                    'created_by' => JWTUtil::getPayloadValue('jti')
                ];
                DB::table('evt_tracking')->insert($evt);
                break;
            case 3:
                $evt = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $input['cus_id'],
                    'owner'      => $input['xfer_ticket_num'],
                    'evt_code'   => Status::getByKey("event", "WHS-XFER-TICKET-PICKED"),
                    'trans_num'  => $input['xfer_hdr_num'],
                    'info'       => sprintf(Status::getByKey("event-info", "WHS-XFER-TICKET-PICKED"), $input['xfer_ticket_num']),
                    'created_at' => time(),
                    'created_by' => JWTUtil::getPayloadValue('jti')
                ];
                DB::table('evt_tracking')->insert($evt);
                break;
            default:
                $evt = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $input['cus_id'],
                    'owner'      => $input['xfer_ticket_num'],
                    'evt_code'   => Status::getByKey("event", "WHS-XFER-PICKING"),
                    'trans_num'  => $input['xfer_hdr_num'],
                    'info'       => sprintf(Status::getByKey("event-info", "WHS-XFER-PICKING"), $input['pick_qty'],
                        $xferDtl->item_id, $xferDtl->sku, $xferDtl->size, $xferDtl->color, $input['from_loc_code'], $input['to_loc_code']),
                    'created_at' => time(),
                    'created_by' => JWTUtil::getPayloadValue('jti')
                ];
                DB::table('evt_tracking')->insert($evt);
                break;
        }
    }

    public function ecomPutawaySuggest($whsId, $upc, Request $request)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);
        $user_customers = array_get($userInfo, 'user_customers', 0);
        $cusIds = array_pluck($user_customers, 'cus_id');

        try {

            $xferDetail = $this->xferDetailModel->getModel()
                ->select(
                    'xfer_dtl.mez_loc',
                    'xfer_dtl.mez_loc_id',
                    'xfer_hdr.item_id',
                    DB::raw('SUM(xfer_dtl.putaway_qty) AS putaway_qty')
                )
                ->join('xfer_hdr', 'xfer_hdr.xfer_hdr_id', '=', 'xfer_dtl.xfer_hdr_id')
                ->join('xfer_ticket', 'xfer_ticket.xfer_ticket_id', '=', 'xfer_dtl.xfer_ticket_id')
                ->where('xfer_ticket.deleted', 0)
                ->where('xfer_ticket.xfer_ticket_sts', 'PI')
                ->where('xfer_hdr.cus_upc', $upc)
                ->where('xfer_dtl.whs_id', $whsId)
                ->whereIn('xfer_dtl.cus_id', $cusIds)
                ->groupBy('xfer_dtl.mez_loc_id')
                ->first();

            if (empty($xferDetail)) {
                return $this->response->errorBadRequest('Xfer is not exists.');
            }

            $item_id = $xferDetail->item_id;

            // Get Quantity
            $pallet = $this->palletModel->getModel()
                ->select(
                    DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                    'cartons.sku',
                    'cartons.size',
                    'cartons.color'
                )
                ->join('cartons', 'pallet.plt_id', '=', 'cartons.plt_id')
                ->where('plt_num', self::PLT_TOTE_NUM)
                ->where('cartons.deleted', 0)
                ->where('cartons.item_id', $item_id)
                ->groupBy('cartons.item_id')
                ->first();

            if (empty($pallet)) {
                return $this->response->errorBadRequest('Cartons is tranfered.');
            }

            return [
                'data' => [
                    'mez_loc_id' => $xferDetail->mez_loc_id,
                    'mez_loc'    => $xferDetail->mez_loc,
                    'item_id'    => $item_id,
                    'sku'        => $pallet->sku,
                    'size'       => $pallet->size,
                    'color'      => $pallet->color,
                    'avail_qty'  => $pallet->avail_qty + $xferDetail->putaway_qty,
                    'putaway_qty'=> $xferDetail->putaway_qty
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function ecomPutawayUpdate($whsId, Request $request, XferPutawayValidator $xferPutawayValidator)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $input = $request->getParsedBody();

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        $xferPutawayValidator->validate($input);

        try {
            $item_id = $input['item_id'];
            $act_loc_code = $input['act_loc_code'];
            $actQty = $input['act_qty'];

            $palletTote = $this->palletModel->getFirstBy('plt_num', self::PLT_TOTE_NUM);
            if (empty($palletTote)) {
                return $this->response->errorBadRequest('Pallet is not exist.');
            }

            $cartons = $this->cartonModel->getModel()
                ->where('plt_id', $palletTote->plt_id)
                ->where('item_id', $item_id)
                ->get();

            if(empty($cartons) || count($cartons) == 0) {
                return $this->response->errorBadRequest('Pallet does not have cartons belong to this sku');
            }

            $ttlPiece = array_pluck($cartons, 'piece_remain', 'ctn_id');
            if (!empty($ttlPiece) && $actQty > (int)array_sum($ttlPiece)) {
                return $this->response->errorBadRequest('Actual Qty is less more than or equal Available Qty');
            }

            $locToChkId = DB::table('location AS l')
                ->select('l.loc_id')
                ->join('loc_type AS lt', 'l.loc_type_id', '=', 'lt.loc_type_id')
                ->where('l.deleted', 0)
                ->where('l.loc_sts_code', 'AC')
                ->where('lt.loc_type_code', 'ECO')
                ->where('l.loc_code', $act_loc_code)
                ->value('l.loc_id');

            if (empty($locToChkId)) {
                return $this->response->errorBadRequest('Locatoin is not exists.');
            }

            DB::beginTransaction();

            $cartons = $cartons->toArray();
            $cartonFirst = $cartons[0];

            // Update Xfer Complete
            $xferTicket = $this->updateXferComplete($locToChkId, $item_id, $actQty, $input);

            // Update Pallet Tote Piece Ttl
            $palletTote->init_piece_ttl = (int)$palletTote->init_piece_ttl - $actQty;
            $palletTote->save();

            // Update Inventory Ecom
            $invtObj = InventorySummary::generateInvtItem($cartonFirst['sku'], $cartonFirst['size'],
                $cartonFirst['color'], 1, 'ECO', $cartonFirst['whs_id'], $cartonFirst['cus_id']);

            $invtObj->ttl += $actQty;
            $invtObj->avail += $actQty;
            $invtObj->save();

            //Update inventory not Ecom
            InventorySummary::where([
                'item_id' => $invtObj->item_id,
                'lot'     => $cartonFirst['lot']
            ])
                ->update([
                    'ttl'   => DB::raw('ttl - ' . $actQty),
                    'avail' => DB::raw('avail - ' . $actQty),
                ]);

            //clone cartons to mezLoc
            $this->proccessCartonsPutaway($cartons,
                [
                    'item_id' => $invtObj->item_id,
                    'whs_id' => $whsId,
                    'act_loc_id' => $locToChkId,
                    'act_loc_code' => $act_loc_code,
                    'act_qty' => $actQty
                ]);

            // Insert Event Tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $cartonFirst['whs_id'],
                'cus_id'    => $cartonFirst['cus_id'],
                'owner'     => $xferTicket['xfer_ticket_num'],
                'evt_code'   => $xferTicket['status'],
                'trans_num' => $xferTicket['xfer_ticket_num'],
                'info'       => sprintf($xferTicket['status'], $xferTicket['xfer_ticket_num'])
            ]);
            DB::commit();
            return [
                'message' => 'Putaway is successfully!',
                'data'    => [
                    'xfer_ticket_num' => $xferTicket['xfer_ticket_num'],
                ]
            ];
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function proccessCartonsPutaway($cartons, $params)
    {
        $userId = Data::getCurrentUserId();
        $actQty = $params['act_qty'];
        $freDate = date('ym');

        $rs = DB::table('cartons')
            ->where([
                'item_id' => $params['item_id'],
                'lot'     => 'ECO',
                'is_ecom' => 1,
                'deleted' => 0,
                'ctn_sts' => 'AC',
                'loc_id'  => $params['act_loc_id']
            ])
            ->where('deleted', 0)
            ->where('ctn_num', 'LIKE', "CTN-$freDate-Z-%")
            ->first();

        $cartonFirst = '';
        $ctnRmIds = [];
        $arr = [];
        foreach ($cartons as $carton) {
            if ($actQty > 0) {

                $ctnRmIds[$carton['ctn_id']] = $carton['ctn_id'];

                if ($actQty < $carton['piece_remain']) {
                    DB::table('cartons')
                        ->where('ctn_id', $carton['ctn_id'])
                        ->update([
                            'piece_remain' => $carton['piece_remain'] - $actQty,
                            'piece_ttl' => $carton['piece_remain'] - $actQty
                        ]);

                    unset($ctnRmIds[$carton['ctn_id']]);

                    $actQty = 0;

                } else {
                    $actQty -= $carton['piece_remain'];
                }

                if (empty($rs) && empty($cartonFirst)) {
                    $cartonFirst = $carton;
                }
                $arr[] = $carton['ctn_id'];
            }
        }

        if (!empty($rs)) {
            $rs['piece_remain'] = $rs['piece_ttl'] = $rs['ctn_pack_size'] = $rs['piece_remain'] + $params['act_qty'];
            $rs['updated_by'] = $userId;
            $rs['updated_at'] = time();

            DB::table('cartons')
                ->where('ctn_id', $rs['ctn_id'])
                ->update($rs);

        } else {
            $cntMaxNum = $this->getMaxCtnNum();
            $cloneCtn = $cartonFirst;
            $cloneCtn['piece_remain'] = $cloneCtn['piece_ttl'] = $cloneCtn['ctn_pack_size'] = $params['act_qty'];
            $cloneCtn['ctn_num'] = ++$cntMaxNum;
            $cloneCtn['loc_id'] = $params['act_loc_id'];
            $cloneCtn['loc_name'] = $cloneCtn['loc_code'] = $params['act_loc_code'];
            $cloneCtn['item_id'] = $params['item_id'];
            $cloneCtn['plt_id'] = null;
            $cloneCtn['is_damaged'] = 0;
            $cloneCtn['is_ecom'] = 1;
            $cloneCtn['loc_type_code'] = $cloneCtn['lot'] = 'ECO';
            $cloneCtn['ctn_sts'] = 'AC';
            $cloneCtn['updated_by'] = $cloneCtn['created_by'] = $userId;
            $cloneCtn['created_at'] = $cloneCtn['updated_at'] = time();
            $cloneCtn['deleted_at'] = 915148800;
            $cloneCtn['deleted'] = 0;
            $cloneCtn['rfid'] = null;

            unset($cloneCtn['ctn_id']);

            
            $this->cartonModel->create($cloneCtn);
        }

        if (!empty($ctnRmIds)) {
            DB::table('cartons')
                ->whereIn('ctn_id', $ctnRmIds)
                ->where('deleted', 0)
                ->update([
                    'plt_id' => null,
                    'deleted_at' => time(),
                    'deleted' => 1,
                    'ctn_sts' => 'IA',
                    'piece_remain' => 0,
                    'piece_ttl' => 0
                ]);
        }
    }

    private function updateXferComplete($locToChkId, $item_id, $actQty, $input)
    {
        $xferDetails = $this->xferDetailModel->getModel()
            ->where('item_id', $item_id)
            ->where('mez_loc_id', $input['mez_loc_id'])
            ->where('xfer_dtl_sts', 'PI')
            ->whereRaw('putaway_qty < act_piece_qty')
            ->get();

        if (count($xferDetails) == 0) {
            return $this->response->errorBadRequest('Xfer Detail is not exists.');
        }

        foreach ($xferDetails as $xferDetail) {
            if($actQty > 0) {

                $putaway_qty = $xferDetail->act_piece_qty - $xferDetail->putaway_qty;

                $xferDetail->putaway_qty += ($actQty - $putaway_qty) >= 0 ? $putaway_qty : $actQty;

                $xferDetail->act_mez_loc_id = $locToChkId;
                $xferDetail->act_mez_loc = $input['act_loc_code'];

                if ($xferDetail->putaway_qty == $xferDetail->act_piece_qty) {
                    $xferDetail->xfer_dtl_sts = 'CO';
                }

                $actQty -= $putaway_qty;

                $xferDetail->save();
            }
        }

        // Xfer hdr
        DB::table('xfer_hdr')
            ->where('xfer_hdr_id', $xferDetail->xfer_hdr_id)
            ->where('deleted', 0)
            ->where('xfer_sts', '<>', 'CO')
            ->whereRaw("
                (SELECT COUNT(1) 
                    FROM xfer_dtl 
                    WHERE xfer_dtl.xfer_hdr_id = $xferDetail->xfer_hdr_id
                        AND xfer_dtl.xfer_dtl_sts <> 'CO') = 0
               ")
            ->update(['xfer_sts' => 'CO']);

        // Xfer hdr
        DB::table('xfer_ticket')
            ->where('xfer_ticket_id', $xferDetail->xfer_ticket_id)
            ->where('xfer_ticket_sts', '<>', 'CO')
            ->where('deleted', 0)
            ->whereRaw("
                (SELECT COUNT(1) 
                    FROM xfer_hdr
                    WHERE xfer_hdr.xfer_ticket_id = $xferDetail->xfer_ticket_id 
                        AND xfer_hdr.xfer_sts <> 'CO') = 0
               ")
            ->update(['xfer_ticket_sts' => 'CO']);

        $xfer_ticket = $this->xferTicketModel->getFirstBy('xfer_ticket_id', $xferDetail->xfer_ticket_id);
        $status = Status::getByKey("event", "WHS-XFER-PICKING");
        if ($xfer_ticket->xfer_ticket_sts == 'CO') {
            $status = Status::getByKey("event", "WHS-XFER-TICKET-COMPLETE");
        } else {
            $xfer_hdr = $this->xferHdrModel->getFirstBy('xfer_hdr_id', $xferDetail->xfer_hdr_id);
            if ($xfer_hdr->xfer_sts == 'CO') {
                $status = Status::getByKey("event", "WHS-XFER-COMPLETE");
            }
        }
        return [
            'status' => $status,
            'xfer_ticket_num' => $xfer_ticket->xfer_ticket_num
        ];
    }
}