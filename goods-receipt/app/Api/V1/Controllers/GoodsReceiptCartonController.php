<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\GoodsReceiptCartonTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\GoodsReceipt;
use App\Api\V1\Models\GoodsReceiptCartonModel;


class GoodsReceiptCartonController extends AbstractController
{
	public function __construct(GoodsReceiptCartonModel $goodsReceiptCartonModel)
	{
		$this->model = $goodsReceiptCartonModel;
	}

	public function search($goodsReceiptId, Request $request)
	{
		$goodsReceipt = GoodsReceipt::findOrFail($goodsReceiptId);	

		$input = $request->getQueryParams();		
		$limit = array_get($input, 'limit', 20);

		try{
			$result = $this->model->search($goodsReceipt, $input, ['pallet', 'goodReceiptDtl', 'goodReceiptDtl.goodsReceipt', 'goodReceiptDtl.goodsReceipt.user', 'goodReceiptDtl.goodsReceipt.updatedBy'], $limit);	
			return $this->response->paginator($result, new GoodsReceiptCartonTransformer());
		}catch(\Exception $e){
			return $this->response->errorBadRequest($e->getMessage());
		}
	}
}