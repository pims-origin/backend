<?php

/**
 * Created by PhpStorm.
 * User: rober
 * Date: 20/10/2016
 * Time: 14:09
 */
namespace App\Api\V1\Services;

use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\LockModel;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\View;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class CycleCountService
{

    /**
     * @var CycleHdrModel
     */
    protected $cycleHdrModel;

    /**
     * @var CycleDtlModel
     */
    protected $cycleDtlModel;

    /**
     * @var CycleHdr
     */
    protected $cycleHdr;

    /**
     * @var CycleDtl
     */
    protected $cycleDtl;

    /**
     * @var
     */
    protected $errors;

    /**
     * CycleCountService constructor.
     *
     * @param CycleHdrModel $cycleHdrModel
     * @param CycleDtlModel $cycleDtlModel
     */
    public function __construct(CycleHdrModel $cycleHdrModel, CycleDtlModel $cycleDtlModel)
    {
        $this->cycleHdrModel = $cycleHdrModel;
        $this->cycleDtlModel = $cycleDtlModel;
        $this->cycleHdr = new CycleHdr();
        $this->cycleDtl = new CycleDtl();
    }

    /**
     * @param $cycleHdrIds
     *
     * @return array|string
     */
    public function deleteCycleCounts($cycleHdrIds)
    {
        $response['status'] = true;
        $currentUserId = self::getUserId();

        $cycleHdrs = $this->cycleHdrModel->getCycleHdrByIds($cycleHdrIds);

        $isAssigned = $this->cycleHdrModel->getCycleStatusByCycleId($cycleHdrs);

        if ($isAssigned) {
            $response = [
                'status' => false,
                'msg'    => $isAssigned . '. That Cycle Count is processing or isn\'t exist. Delete not complete.',
            ];

            return $response;
        }

        $cycleHdrAssignMe = $this->cycleHdrModel->getCycleAssigToUserId($cycleHdrs, $currentUserId);

        if ($cycleHdrAssignMe) {
            $response = [
                'status' => false,
                'msg'    => 'You can not delete the cycle count: ' . implode(',', $cycleHdrAssignMe),
            ];

            return $response;
        }

        try {
            DB::beginTransaction();

            foreach ($cycleHdrIds as $cycleHdrId) {

                $cycleHdr = $this->cycleHdrModel->getCycleInfo($cycleHdrId, true);
                $cycleDtlIds = $items = [];
                foreach ($cycleHdr->cycleDtl as $cycleDtl) {
                    $cycleDtlIds[] = $cycleDtl->cycle_dtl_id;
                    $items[] = $cycleDtl->item_id;
                }

                $params = [
                    'whsId'     => $cycleHdr->whs_id,
                    'by'        => $cycleHdr->cycle_type,
                    'itemIDs'   => $items,
                    'newStatus' => LockModel::STATUS_ACTIVE
                ];

                if ($cycleHdr->cycle_type == CycleHdrModel::CYCLE_TYPE_LOCATION) {
                    $locIdHdr = explode(',', $cycleHdr->cycle_detail);
                    $locIdsDtl = $cycleHdr->cycleDtl->pluck('sys_loc_id')->toArray();
                    $params['locIds'] = array_merge($locIdHdr, $locIdsDtl);
                    LockModel::updateStatusLocations($params['locIds'], $cycleHdr->whs_id);
                }

                LockModel::updateStatusCartonsByType($params);

                if ($cycleHdr->cycle_type == CycleHdrModel::CYCLE_TYPE_CUSTOMER || $cycleHdr->cycle_type == CycleHdrModel::CYCLE_TYPE_SKU) {
//                    $locIDs = LocationModel::getLocationIdsByCustomer($cycleHdr->whs_id, $cycleHdr->cycle_detail,
//                        LockModel::STATUS_LOCK);
                    $locIdsInCurrentCC = $cycleHdr->cycleDtl->pluck('sys_loc_id')->toArray();
                    $locIdsInAnotherCC = $this->cycleDtlModel->getLocationInAnotherCycleCountWasLocked($cycleHdr->cycle_hdr_id, $locIdsInCurrentCC);
                    $locIdsInAnotherCC = array_pluck($locIdsInAnotherCC, 'sys_loc_id');
                    $locIdsWillUnLock = array_diff($locIdsInCurrentCC, $locIdsInAnotherCC);
                    LockModel::updateStatusLocations($locIdsWillUnLock, $cycleHdr->whs_id);
                }

                $this->cycleDtlModel->deleteCycleDtls($cycleHdrId, $cycleDtlIds);
            }

            $this->cycleHdrModel->deleteCycleHdrs($cycleHdrIds);

            CycleHdrModel::updateStatusLocationPutAwayHistory();

            DB::commit();

            return $response;

        } catch (\Exception $e) {
            DB::rollback();

            $response = [
                'status' => false,
                'msg'    => $e->getMessage(),
            ];

            return $response;
        }
    }

    public function printCycleCountDetail($cycleHdrId)
    {
        $compiler = new BladeCompiler(new Filesystem(), storage_path());
        $BladeEngine = new CompilerEngine($compiler);
        $factory = app('view');
        $cycleHdr = $this->cycleHdrModel->getCycleInfo($cycleHdrId);
        if (!$cycleHdr) {
            return response('Cycle Count ID not found', 404);
        }
        $detail = [];
        foreach ($cycleHdr->cycleDtl as $item) {
            array_push($detail, [
                'customer'     => $item->customer->cus_name,
                'customerCode' => $item->customer->cus_code,
                'sku'          => $item->sku,
                'size'         => $item->size,
                'color'        => $item->color,
                'lot'          => $item->lot,
                'remain'       => $item->remain,
                'pack'         => $item->pack,
                'ttlCtn'       => strtoupper($cycleHdr->count_by) == CycleHdrModel::COUNT_BY_CARTON ?
                    $item->sys_qty : $item->sys_qty / $item->remain,
                'ttlPieces'    => strtoupper($cycleHdr->count_by) == CycleHdrModel::COUNT_BY_CARTON ?
                    $item->sys_qty * $item->remain : $item->sys_qty,
                'actQty'       => $item->act_qty,
                'sysLoc'       => $item->sys_loc_name,
                'actLoc'       => $item->act_loc_name
            ]);
        }

        $userInfo = new \Wms2\UserInfo\Data();
        $userInfo = $userInfo->getUserInfo();
        $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
        $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

        $data = [
            'cycleID'      => '#' . $cycleHdrId,
            'cycleName'    => $cycleHdr->cycle_name,
            'whsName'      => $cycleHdr->warehouse->whs_name,
            'description'  => $cycleHdr->description,
            'assignedTo'   => $cycleHdr->assignee_to_name,
            'assignedBy'   => $cycleHdr->assignee_by_name,
            'cycleType'    => CycleHdrModel::$arrCycleType[$cycleHdr->cycle_type],
            'countBy'      => strtoupper($cycleHdr->count_by),
            'status'       => CycleHdrModel::$arrCycleStatus[$cycleHdr->cycle_sts],
            'hasColorSize' => $cycleHdr->has_color_size == 0 ? 'No' : 'Yes',
            'dueDate'      => ($cycleHdr->due_dt) ? date('Y-m-d', strtotime($cycleHdr->due_dt)) : '',
            'detail'       => $detail,
            'dynamicLabel' => strtoupper($cycleHdr->count_by) == CycleHdrModel::COUNT_BY_CARTON ? 'Ctns' : 'Pieces',
            'printedBy'    => $printedBy
        ];
        $viewObj = new View(
            $factory,
            $BladeEngine,
            'basic_template',
            resource_path('views/basic_template.blade.php'),
            $data
        );

        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($viewObj->render());
        $pdf->setPaper('A4', 'landscape');

        return $pdf->stream();
    }

    /**
     * @return mixed
     */
    public static function getCurrentUserInfo()
    {
        $data = new Data();
        $user = $data->getUserInfo();

        return $user;
    }

    /**
     * @return mixed
     */
    public static function getUserId()
    {
        $usrInfo = new Data();
        $usrId = $usrInfo->getUserInfo()['user_id'];

        return $usrId;
    }

    /**
     * @return mixed
     */
    public static function getCurrentWhs()
    {
        $userInfo = new Data();
        $whsId = $userInfo->getCurrentWhs();

        return $whsId;
    }
}