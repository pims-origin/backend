<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Transformers\SystemStateTransformer;
use App\Api\V1\Validators\SystemStateValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Utils\Message;
use Illuminate\Http\Response as IlluminateResponse;

/**
 * Class StatesController
 *
 * @package App\Api\V1\Controllers
 */
class StatesController extends AbstractController
{
    /**
     * @var SystemStateModel
     */
    protected $systemStateModel;

    /**
     * @var SystemStateValidator
     */
    protected $systemStateValidator;

    /**
     * @var SystemStateTransformer
     */
    protected $systemStateTransformer;

    protected $model;

    /**
     * StatesController constructor.
     *
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->model = new SystemStateModel();
        $this->systemStateModel = new SystemStateModel();
        $this->systemStateValidator = new SystemStateValidator();
        $this->systemStateTransformer = new SystemStateTransformer();
    }

    /**
     *
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IRequest $request)
    {
        $input = $request->getQueryParams();

        try {
            $limit = array_get($input, 'limit', PAGING_LIMIT);
            $systemState = $this->systemStateModel->search($input, $limit);

            return $this->response->paginator($systemState, $this->systemStateTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param IRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemStateValidator->validate($input);

        $params = [
            'sys_state_name'       => $input['sys_state_name'],
            'sys_state_code'       => $input['sys_state_code'],
            'sys_state_country_id' => $input['sys_state_country_id'],
            'sys_state_desc'       => array_get($input, 'sys_state_desc', '')
        ];
        try {
            // check duplicate code
            $stateByCode = $this->systemStateModel
                ->getStateInCountryByCode($params['sys_state_code'], $input['sys_state_country_id']);
            if ($stateByCode) {
                throw new \Exception(Message::get("BM006", "State Code"));
            }
            // check duplicate name
            $stateByName = $this->systemStateModel
                ->getStateInCountryByName($params['sys_state_name'], $input['sys_state_country_id']);
            if ($stateByName) {
                throw new \Exception(Message::get("BM006", "State Name"));
            }

            if ($SystemState = $this->systemStateModel->create($params)) {
                return $this->response->item($SystemState,
                    $this->systemStateTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $systemStateId
     *
     * @return \Dingo\Api\Http\Response
     */
    public function show($systemStateId)
    {
        $systemState = $this->systemStateModel->getFirstBy('sys_state_id', $systemStateId);
        if ($systemState) {
            return $this->response->item($systemState, $this->systemStateTransformer);
        } else {
            return $this->response->noContent();
        }
    }

    /**
     * @param $systemStateId
     * @param IRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function update($systemStateId, IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemStateValidator->validate($input);

        $params = [
            'sys_state_id'         => $systemStateId,
            'sys_state_code'       => $input['sys_state_code'],
            'sys_state_name'       => $input['sys_state_name'],
            'sys_state_country_id' => $input['sys_state_country_id'],
            'sys_state_desc'       => array_get($input, 'sys_state_desc', '')
        ];
        try {
            // Check existed
            if (!$this->systemStateModel->checkWhere(['sys_state_id' => $systemStateId])) {
                throw new \Exception(Message::get("BM017", "System State"));
            }
            // check duplicate code
            $stateByCode = $this->systemStateModel
                ->getStateInCountryByCode($params['sys_state_code'], $input['sys_state_country_id']);
            if ($stateByCode && $stateByCode->sys_state_id != $systemStateId) {
                throw new \Exception(Message::get("BM006", "State Code"));
            }
            // check duplicate name
            $stateByName = $this->systemStateModel
                ->getStateInCountryByName($params['sys_state_name'], $input['sys_state_country_id']);

            if ($stateByName && $stateByName->sys_state_id != $systemStateId
            ) {
                throw new \Exception(Message::get("BM006", "State Name"));
            }

            if ($systemState = $this->systemStateModel->update($params)) {
                return $this->response->item($systemState, $this->systemStateTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}