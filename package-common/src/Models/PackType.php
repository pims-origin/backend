<?php

namespace Seldat\Wms2\Models;

/**
 * Class PackType
 *
 * @package Seldat\Wms2\Models
 */
class PackType extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pack_type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pack_code';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'pack_code',
        'pack_name'
    ];

}
