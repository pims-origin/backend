<?php

return [
    'wo_status'         => [
        'NW' => 'New',
        'CO' => 'Completed',
    ],
    'pack_by'           => [
        'ChargeCode' => 'Charge Code',
        'SKU'        => 'SKU',
    ],
    'odr_status'        => [
        'NEW'        => 'NW',
        'SUBMITTED'  => 'SM',
        'ALLOCATED'  => 'AL',
        'PICKING'    => 'PK',
        'PROCESSING' => 'PC',
        'DISPATCH'   => 'DP',
    ],
    'ACTIVE'            => 'AC',
    'INACTIVE'          => 'IA',
    'LOCKED'            => 'LK',
    'RESERVED'          => 'RS',
    'asn_prefix'        => 'ASN',
    'gr_prefix'         => 'GDR',
    'relocation_prefix' => 'REL',
    'asn_status'        => [
        'NEW'          => 'NW',
        'RECEIVING'    => 'RG',
        'RCVD-PARTIAL' => 'RP',
        'COMPLETED'    => 'CO',
    ],
    'gr_status'         => [
        'RECEIVING'       => 'RG',
        'RECEIVED'        => 'RE',
        'RCVD-DISCREPANT' => 'RD',
    ],
    'item_status'       => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'location_status'   => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'ctn_status'        => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'event'             => [
        'ASN-NEW'          => "ANW",
        'ASN-RECEIVING'    => "ARV",
        'ASN-RCVD-PARTIAL' => "ARP",
        'ASN-COMPLETE'     => "ARC",
        'GR-RECEIVING'     => "GRP",
        'GR-EXCEPTION'     => "GRX",
        'GR-COMPLETE'      => "GRC",
        'GR-CANCEL'        => 'GRU',
        'PL-AWAY'          => "PUT",
        'REL-CTN-LOC'      => "LTL",
        'REL-LP-LOC'       => "LPL",
        'COMPLETE-RELOC'   => "RLC",
        'CON-CTN-LOC'      => "CTL",
        'CON-LP-LOC'       => "CTP",
        'COMPLETE-CON'     => "CNC",
        'ALLOCATED-ORD'    => "ORA",
    ],
    'event-info'        => [
        'ASN-NEW'          => "%s created",
        'ASN-RECEIVING'    => "%s receiving",
        'ASN-RCVD-PARTIAL' => "%s received partial",
        'ASN-COMPLETE'     => "%s received",
        'GR-RECEIVING'     => "Good Receipt started for %s",
        'GR-EXCEPTION'     => "% received partial",
        'GR-COMPLETE'      => "Goods receipt %s created for %s",
        'GR-CANCEL'        => '',
        'PL-AWAY'          => "Move to %s",
        'REL-CTN-LOC'      => "Change carton %s location from %s to %s",
        'REL-LP-LOC'       => "Change License Plate %s location from %s to %s",
        'COMPLETE-RELOC'   => "Complete Relocation %s",
        'CON-CTN-LOC'      => "Change carton %s location from %s to %s",
        'CON-LP-LOC'       => "Change carton %s licence plate from %s to %s",
        'COMPLETE-CON'     => "Complete Consolidation %s",
        'ALLOCATED-ORD'    => "%s Allocated",
    ],
    'cus_config_name'   =>[
        'EDI_INTEGRATION'  =>'edi_integration'
    ],
    'cus_config_value'  =>[
        'EDI888'    => '888',
        'EDI846'    => '846',
        'EDI856'    => '856',
        'EDI858'    => '858',
        'EDI861'    => '861',
        'EDI940'    => '940',
        'EDI943'    => '943',
        'EDI945'    => '945'
    ],
    'cus_config_active' =>[
        'YES'   => 'Y',
        'NO'    => 'N'
    ]
];