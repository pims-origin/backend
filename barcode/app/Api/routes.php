<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/time', function () use ($app) {
    return date('Y-m-d H:i:s');
});

$app->get('/gun-version',[
    'uses' => 'App\Api\V1\Controllers\VersionController@index'
]);

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = [
    'trimInput', 'rfGun', 'verifySecret', 'setWarehouseTimezone'
];

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    require(__DIR__ . '/ReplenishmentConfig/routes.php');
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
        $api->get('/', function () {
            return ['Seldat API RF GUNS V1'];
        });

        //Goodreceipt Pim

        $api->get("/{whsId:[0-9]+}/goodreceipt", ['uses' => 'GoodReceiptController@search']);
        $api->post("/{whsId:[0-9]+}/goodreceipt", ['uses' => 'GoodReceiptController@store']);
        $api->get("/{whsId:[0-9]+}/autocomplete-customer", ['uses' => 'GoodReceiptController@getListCustomer']);

        $api->post("/{whsId:[0-9]+}/gr-one-step", ['uses' => 'GoodReceiptController@createAsnDetailOneStep']);
        $api->post("/{whsId:[0-9]+}/gr-two-step", ['uses' => 'GoodReceiptController@createAsnDetailTwoStep']);

       // Item Controll Pim
        $api->get("/{whsId:[0-9]+}/item", ['uses' => 'ItemController@search']);
        $api->get("/{whsId:[0-9]+}/validateSku", ['uses' => 'ItemController@validateSku']);
        $api->get("/{whsId:[0-9]+}/validateUpc", ['uses' => 'ItemController@validateUpc']);
        $api->post("/{whsId:[0-9]+}/item", ['uses' => 'ItemController@store']);
        $api->get("/{whsId:[0-9]+}/getSkuAsn", ['uses' => 'ItemController@getSkuAsn']);
        $api->get("/partial-putaway/{whsId:[0-9]+}/getSkuCus", ['uses' => 'ItemController@checkSku']);
        $api->get("/partial-putaway/{whsId:[0-9]+}/getItemPlt", ['uses' => 'ItemController@getItemByPlt']);

        // Location Controll Pim
        $api->get("/{whsId:[0-9]+}/location", ['uses' => 'LocationController@getLocationByCode']);
        $api->get("/{whsId:[0-9]+}/suggest-location", ['uses' => 'LocationController@suggestLocation']);
        $api->get("/{whsId:[0-9]+}/suggest-put-away", ['uses' => 'LocationController@suggestLocationPutAway']);

        // Putaway Partial Pim
        $api->post("/partial-putaway/{whsId:[0-9]+}/submit", ['uses' => 'PutawayController@submitPartialPutaway']);

        // Kitting
        $api->post("/kitting/{whsId:[0-9]+}/allocate", ['uses' => 'KittingController@allocateOrder']);
        $api->post("/kitting/{whsId:[0-9]+}/submit", ['uses' => 'OrderController@submitKitDtl']);

//        $api->get("/kitting/{whsId:[0-9]+}/test", ['uses' => 'OrderController@testDB']);

        // Order Kitting Pim
//        $api->get("/{whsId:[0-9]+}/order-kitting", ['uses' => 'GoodReceiptController@search']);
//        $api->post("/{whsId:[0-9]+}/order-kitting", ['uses' => 'GoodReceiptController@search']);

        // Order Packing Pim
        $api->get("/packing/{whsId:[0-9]+}/order-packing", ['uses' => 'PackingController@getList']);
        $api->post("/packing/{whsId:[0-9]+}/order-packing", ['uses' => 'PackingController@storePacking']);
        $api->get("/packing/{whsId:[0-9]+}/order-packing/item", ['uses' => 'PackingController@getListItemPack']);
        $api->get("/packing/{whsId:[0-9]+}/order-packing/listBox", ['uses' => 'PackingController@getListOutsideBox']);

        $api->post("/packing/{whsId:[0-9]+}/order-packing-auto", ['uses' => 'PackingController@autoStorePacking']);

        // Assign Shipping Lane Pim
        $api->post("/shipping/{whsId:[0-9]+}/assign-shipping-lane", ['uses' => 'ShippingController@assignShippingLane']);


        $api->get('/barcode/{idGrDtl}', ['uses' => 'PalletController@getBarcode']);

        // get gr_dtl_id from pal_sug_loc
        $api->get('/{whsId:[0-9]+}/put-away/suggest/{grDtlId:[0-9]+}',
            ['action' => "doPutAway", 'uses' => 'PalletController@getSuggestLocation']);

        //  Update loc_id to pallet table
        $api->put('/{whsId:[0-9]+}/put-away',
            ['action' => "doPutAway", 'uses' => 'PalletController@updatePalletToLocation']);

        // RF Gun
        $api->get('/{whsId:[0-9]+}/put-away', ['action' => "putAway", 'uses' => 'PalletController@search']);
        $api->get('/{whsId:[0-9]+}/put-away/{grHdrId:[0-9]+}', [
            'action' => "doPutAway",
            'uses'   => 'PalletController@show'
        ]);

        $api->get('/pallet-list', ['action' => "viewPallet", 'uses' => 'PalletController@palletList']);

        // Wave
        $api->get('/{whsId:[0-9]+}/wave', ['action' => "wavePick", 'uses' => 'WaveController@searchV2']);
        $api->get('/{whsId:[0-9]+}/wave/{wvHdrId:[0-9]+}',
            ['action' => "wavePick", 'uses' => 'WaveController@show']);
        $api->get('/{whsId:[0-9]+}/wave/sku/{wvDtlId:[0-9]+}', [
            'action' => "wavePick",
            'uses'   =>
                'WaveController@detailV2'
        ]);
        $api->get('/{whsId:[0-9]+}/wave/sku/{wvDtlId:[0-9]+}/suggest', [
            'action' => "wavePick",
            'uses'   =>
                'WaveController@suggest'
        ]);
        $api->get('{whsId:[0-9]+}/more-suggest-location', [
            'action' => 'wavePick',
            //'uses'   => 'WaveController@moreSuggestLocation'
            'uses'   => 'SuggestMoreLocationPickPalletController@moreSuggestLocation'
        ]);
        $api->post('{whsId:[0-9]+}/check-wavepick-location', [
            'action' => 'createWavePick',
            'uses'   => 'WaveController@checkWvLocation'
        ]);

        // Relocation
        $api->post('/{whsId:[0-9]+}/relocation/customer', ['action' => "relocate", 'uses' => 'RelocationController@getCustomerByLocation']);
        $api->put('/{whsId:[0-9]+}/relocation', ['action' => "relocate", 'uses' => 'RelocationController@relocation']);

        // consolidate
        //$api->put('/consolidation', ['action' => "consolidate", 'uses' => 'PalletController@consolidation']);
        $api->put('/consolidation', ['action' => "consolidate", 'uses' => 'PalletController@consolidate']);


        // update wave pick -- Not in used
        $api->put('/{whsId:[0-9]+}/wave/sku/{wv_dtl_id}', [
            'action' => "wavepick",
            'uses'   =>
                'WaveController@updateWavepick'
        ]);

        $api->get('/{whsId:[0-9]+}/putawayDashboard',
            ['action' => "putawayDashboard", 'uses' => 'PalletController@putawayDashboard']);


        $api->get('/{whsId:[0-9]+}/wavepickDashboard',
            ['action' => "wavepickDashboard", 'uses' => 'WaveController@wavepickDashboard']);

        $api->post('/{whsId:[0-9]+}/check/location', [
            'action' => 'wavePick',
            'uses' => 'LocationController@checkLocation'
        ]);

        $api->post('/{whsId:[0-9]+}/check/pallet', [
            'uses' => 'PalletController@checkPallet'
        ]);

        $api->post('/{whsId:[0-9]+}/check/location-pallet', [
            'action' => 'wavePick',
            'uses' => 'PalletController@checkLocationPallet'
        ]);

        $api->put('/{whs_id:[0-9]+}/update-wavepick', [
            'action' => 'wavePick',
            'uses' => 'WaveController@updateWavePickNoEcom'
        ]);

        $api->get('/{whs_id:[0-9]+}/wv-loc-validate/{locCode}', [
            'action' => 'wavePick',
            'uses' => 'WaveController@validateWvLoc'
        ]);
        $api->group(['prefix' => '/orders/{whs_id:[0-9]+}'], function ($api) {
            $api->get('/packing',[
                'action' => "oders.packing",
                'uses'   => 'OrderController@search'
            ]);
            $api->get('/order-detail/{orderId:[0-9]+}',[
                'action' => "oders.packing",
                'uses'   => 'OrderController@orderDetail'
            ]);
        });
        $api->get('/{whsId:[0-9]+}/put-away-info', [
            'action' => 'doPutAway',
            'uses' => 'PalletController@searchPutAwayInfo'
        ]);

        $api->get('/{whsId:[0-9]+}/get-item-by-lpn', [
            'uses' => 'ItemController@getItemByLpn'
        ]);
    });

});

//------------------ INBOUND groups
$api->version('v1', [
    'prefix'    => 'inbound',
    'namespace' => 'App\Api\Inbound\Controllers',
    'middleware' => $middleware
], function ($api) {

    //goods-receipt
    $api->group(['prefix' => '{whsId:[0-9]+}/goods-receipts'], function ($api) {
        //$api->get('/', ['action' => 'goodsReceipt', 'uses' => 'GoodsReceiptController@listGrs']);
        //$api->get('/{grId:[0-9]+}', ['action' => 'goodsReceipt', 'uses' => 'GoodsReceiptController@show']);
        $api->get('{asnHdrId:[0-9]+}/{ctnrId}', ['action' => 'goodsReceipt', 'uses' =>
            'GoodsReceiptController@skuList']);

        $api->post('asn-dtl/{asnDtlId:[0-9]+}', ['action' => 'goodsReceipt', 'uses' =>
            'GoodsReceiptController@scanCartonAndPalletByKg']);

        $api->put('complete', [
            'action' => 'goodsReceipt',
            'uses'   =>
                'GoodsReceiptController@complete'
        ]);

        $api->get('/scan-pallet', [
            'action' => 'goodsReceipt',
            'uses'   => 'GoodsReceiptController@getSuggestionLocations'
        ]);

        $api->get('/view-detail/{grId:[0-9]+}', [
            'action' => 'goodsReceipt',
            'uses'   => 'GoodsReceiptController@getGoodsReceiptDetails'
        ]);

        $api->get('/damage-type', ['action' => "goodsReceipt", 'uses' => 'DamageTypeController@search']);

    });

    $api->group(['prefix' => '{whsId:[0-9]+}/putaway'], function ($api) {
        $api->get('/list', ['action' => 'doPutAway', 'uses' => 'GoodsReceiptController@putAwayList']);
    });

    //asn
    $api->group(['prefix' => 'whs/{whsId:[0-9]+}'], function($api) {

        $api->get('asnsV1', ['action' => "goodsReceipt", 'uses'   => 'AsnController@searchCanSortV1']);

        $api->get('cus/{cusId:[0-9]+}/asns/{asnID:[0-9]+}/containers/{ctnID:[0-9]+}',
            ['action' => "goodsReceipt", 'uses' => 'AsnController@loadASNDtl']);

        $api->get('asn-detail2/{asnDtlId:[0-9]+}', [
            'action' => "goodsReceipt",
            'uses'   => 'AsnController@getAsnDetail2'
        ]);
    });


});


$api->version('v1', [
    'prefix'    => 'inbound2',
    'namespace' => 'App\Api\Inbound2\Controllers',
    'middleware' => $middleware
], function ($api) {

    //goods-receipt
    $api->group(['prefix' => '{whsId:[0-9]+}/goods-receipts'], function ($api) {
        //$api->get('/', ['action' => 'goodsReceipt', 'uses' => 'GoodsReceiptController@listGrs']);
        //$api->get('/{grId:[0-9]+}', ['action' => 'goodsReceipt', 'uses' => 'GoodsReceiptController@show']);
        $api->get('{asnHdrId:[0-9]+}/{ctnrId}', ['action' => 'goodsReceipt', 'uses' =>
            'GoodsReceiptController@skuList']);

        $api->post('asn-dtl/{asnDtlId:[0-9]+}', ['action' => 'goodsReceipt', 'uses' =>
            'GoodsReceiptController@scanCartonAndPalletByKg']);

        $api->put('complete', [
            'action' => 'goodsReceipt',
            'uses'   =>
                'GoodsReceiptController@complete'
        ]);

        $api->get('/scan-pallet', [
            'action' => 'goodsReceipt',
            'uses'   => 'GoodsReceiptController@getSuggestionLocations'
        ]);

        $api->get('/view-detail/{grId:[0-9]+}', [
            'action' => 'goodsReceipt',
            'uses'   => 'GoodsReceiptController@getGoodsReceiptDetails'
        ]);

        $api->get('/damage-type', ['action' => "goodsReceipt", 'uses' => 'DamageTypeController@search']);

    });

    $api->group(['prefix' => '{whsId:[0-9]+}/putaway'], function ($api) {
        $api->get('/list', ['action' => 'doPutAway', 'uses' => 'GoodsReceiptController@putAwayList']);
    });

    //asn
    $api->group(['prefix' => 'whs/{whsId:[0-9]+}'], function($api) {

        $api->get('asnsV1', ['action' => "goodsReceipt", 'uses'   => 'AsnController@searchCanSortV1']);

        $api->get('cus/{cusId:[0-9]+}/asns/{asnID:[0-9]+}/containers/{ctnID:[0-9]+}',
            ['action' => "goodsReceipt", 'uses' => 'AsnController@loadASNDtl']);

        $api->get('asn-detail2/{asnDtlId:[0-9]+}', [
            'action' => "goodsReceipt",
            'uses'   => 'AsnController@getAsnDetail2'
        ]);
    });


});

//------------------ INVENTORY groups
$api->version('v1', [
    'prefix'    => 'inventory',
    'namespace' => 'App\Api\Inventory\Controllers',
    'middleware' => $middleware
], function ($api) {
    $api->group(['prefix' => '{whs_id:[0-9]+}'], function ($api) {
        include('Inventory/routes.php');
    });
});


//------------------ Xdock groups
$api->version('v1', [
    'prefix'    => 'xdock',
    'namespace' => 'App\Api\Xdock\Controllers',
    'middleware' => $middleware
], function ($api) {
    $api->group(['prefix' => 'whs/{whsId:[0-9]+}'], function ($api) {
        include('Xdock/routes.php');
    });
});


//------------------ Ecom groups
$api->version('v1', [
    'prefix'    => 'ecom',
    'namespace' => 'App\Api\Ecom\Controllers',
    'middleware' => $middleware
], function ($api) {
    $api->group(['prefix' => 'whs/{whsId:[0-9]+}'], function ($api) {
        include('Ecom/routes.php');
    });
});

//------------------ Shipping Lane groups
$api->version('v1', [
    'prefix'    => 'shipping',
    'namespace' => 'App\Api\Shipping\Controllers',
    'middleware' => $middleware
], function ($api) {
    $api->group(['prefix' => 'whs/{whsId:[0-9]+}'], function ($api) {
        include('Shipping/routes.php');
    });
});
$api->version('v1', [
    'prefix'    => 'v1/setting',
    'namespace' => 'App\Api\V1\Controllers',
    'middleware' => $middleware
], function ($api) {
    $api->group(['prefix' => '/{whsId:[0-9]+}'], function ($api) {
        $api->get("/",function(){
            return "Setting Config";
        });
        $api->get('/printer/{cusId:[0-9]+}',[
            'action' => "barcodeSetting",
            'uses'   => 'SettingController@getSettingByKey'
        ]);
        $api->post('/printer/{cusId:[0-9]+}',[
            'action' => "barcodeSetting",
            'uses'   => 'SettingController@upsertPrinter'
        ]);
    });
});


