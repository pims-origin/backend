<?php
namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class OrderHdrModel extends AbstractModel
{

    protected $odrDtlModel;
    protected $itemModel;
    protected $inventoryModel;
    protected $cartonModel;
    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(
        OrderHdr $model = null,
        ItemModel $itemModel,
        InventoriesModel $inventoriesModel,
        CartonModel $cartonModel,
        OrderDtlModel $orderDtlModel
    )
    {
        $this->model = ($model) ?: new OrderHdr();
        $this->odrDtlModel = $orderDtlModel;
        $this->itemModel = $itemModel;
        $this->inventoryModel = $inventoriesModel;
        $this->cartonModel = $cartonModel;
    }

    public function search($param, $whsId){
        $query = OrderHdr::select([
                "odr_hdr.odr_id",
                DB::raw('count(odr_dtl.sku) as num_sku'),
                'odr_hdr.cus_odr_num',
                'odr_hdr.cus_po',
                'odr_hdr.odr_sts'
            ])
            ->leftJoin('odr_dtl','odr_dtl.odr_id','odr_hdr.odr_id')
            ->whereIn('odr_hdr.odr_sts',[Status::getByValue("Picked",'ORDER-STATUS'),Status::getByValue("Packing",'ORDER-STATUS')])
            ->where('odr_hdr.odr_type',Status::getByValue("Kitting",'ORDER-TYPE'))
            ->where('odr_hdr.whs_id',$whsId);
        if(isset($param['sku'])){
            $query = $query->where('odr_dtl.sku','like','%'.$param['sku'].'%');
        }
        if(isset($param['cus_odr_num'])){
            $query = $query->where('odr_hdr.cus_odr_num','like','%'.$param['sku'].'%');
        }
        if(isset($param['cus_po'])){
            $query = $query->where('odr_hdr.sku','cus_po','%'.$param['sku'].'%');
        }
        $res = $query->groupBy('odr_hdr.odr_id')
            ->paginate($param['limit']);
        return $res;
    }

    public function searchShippedOrders($param, $whsId){
        $query = OrderHdr::select([
                "odr_hdr.odr_id",
                DB::raw('count(odr_dtl.sku) as num_sku'),
                'odr_hdr.cus_odr_num',
                'odr_hdr.cus_po',
                'odr_hdr.odr_sts'
            ])
            ->leftJoin('odr_dtl','odr_dtl.odr_id','odr_hdr.odr_id')
            ->where('odr_hdr.odr_sts',Status::getByValue("Shipped",'ORDER-STATUS'))
            ->where('odr_hdr.whs_id',$whsId);
        if(isset($param['sku'])){
            $query = $query->where('odr_dtl.sku','like','%'.$param['sku'].'%');
        }
        if(isset($param['cus_odr_num'])){
            $query = $query->where('odr_hdr.cus_odr_num','like','%'.$param['sku'].'%');
        }
        if(isset($param['cus_po'])){
            $query = $query->where('odr_hdr.sku','cus_po','%'.$param['sku'].'%');
        }
        $res = $query->groupBy('odr_hdr.odr_id')
            ->paginate($param['limit']);
        return $res;
    }

    public function updatePKOdrHdr($wvHdrId)
    {
        $result = DB::table('odr_hdr as oh')
            ->where('oh.wv_id', $wvHdrId)
            ->update(['oh.odr_sts' => 'PK']);

        return $result;
    }

    public function updatePDOdrHdr($wvHdrId)
    {
        $result = DB::table('odr_hdr as oh')
            ->where('oh.wv_id', $wvHdrId)
            ->where('oh.odr_sts', 'PK')
            ->where(
                DB::raw("(
                    SELECT COUNT(1) 
                    FROM `odr_dtl` as od 
                    WHERE od.`odr_id` = oh.odr_id 
                        AND od.`itm_sts` = 'PD'
                        AND od.deleted = 0
                )"),
                DB::raw("(
                    SELECT COUNT(1) 
                    FROM `odr_dtl` as od 
                    WHERE od.`odr_id` = oh.odr_id
                        AND od.deleted = 0
                )")
            )
            ->update(['oh.odr_sts' => 'PD']);

        return $result;
    }

    public function updatePAOdrHdr($odrHdrIds)
    {
        $result = DB::table('odr_hdr')
            ->whereIn('odr_id', $odrHdrIds)
            ->where('odr_sts', 'PD')
            ->update(['odr_sts' => 'PA']);

        return $result;
    }

    public function updateOrderPickedByWv($wvId)
    {
        $this->getModel()->where([
            'odr_sts' => 'PK',
            'wv_id'   => $wvId
        ])
            ->whereRaw("
                0 = (
                    SELECT count(*) from odr_dtl 
                    WHERE odr_dtl.odr_id = odr_hdr.odr_id
                    AND itm_sts != 'PD'
                    AND alloc_qty > 0
                    AND odr_dtl.deleted = 0
                )
            ")
            ->update([
                'odr_sts' => DB::raw("IF(no_pack_bol = 1, 'ST', 'PD')"),
                'sts'     => 'u'
            ]);
    }

    public function submitKitDtl($attribute) {
        $odrHdr = $this->getFirstWhere([
                "odr_id"    => $attribute['odr_id'],
                "odr_type"  => "KIT",
        ]);

//        if(empty($odrHdr) || !(in_array($odrHdr->odr_sts, ['PD']))) {
//            return [
//                'is_error' => 1,
//                'message'  => "Order is invalid!!"
//            ];
//        }
        $odrDtl = $this->odrDtlModel->getFirstWhere([
                "odr_dtl_id" => $attribute['odr_dtl_id'],
                "is_kitting" => 1
        ]);

//        if(empty($odrDtl)) {
//            return [
//                'is_error' => 1,
//                'message'  => "This Order Detail is not exit!!"
//            ];
//        }
//
//        if($odrDtl->item_id != $attribute['item_id']) {
//            return [
//                'is_error' => 1,
//                'message'  => "This item is not belong to this Order Detail!!"
//            ];
//        }

        $item = $this->itemModel->getFirstWhere(['item_id' => $attribute['item_id']]);
//        if(empty($item)) {
//            return [
//                'is_error' => 1,
//                'message'  => "This item is not exit!!"
//            ];
//        }
        $user_id = Data::getCurrentUserId();
        $time = time();

        DB::beginTransaction();
        $odrDtl->packed_qty += 1;
        $odrDtl->itm_sts = "KT";
        if($odrDtl->packed_qty == $odrDtl->qty) {
            $odrDtl->itm_sts = "KD";
        }
        $odrDtl->save();

        $arrChildItems = DB::table("item_child")->where("parent", $attribute['item_id'])->get();
        $cartonPickParams = [
            "cus_id"                => $attribute['cus_id'],
            "whs_id"                => $attribute['whs_id'],
            "odr_dtl_id"            => $attribute['odr_dtl_id'],
        ];
        foreach ($arrChildItems as $childItem) {
            $cartonPickParams['item_id'] = $childItem['child'];
            $this->cartonModel->pickCtnByItemKit($cartonPickParams, $childItem['pack']);
        }
        $maxCTNnum = $this->cartonModel->_generateCtnNum();
        $maxCTNnum = $this->cartonModel->getNewCtnNum($maxCTNnum);

        $cartonParams = [
            "cus_id"                => $attribute['cus_id'],
            "whs_id"                => $attribute['whs_id'],
            "ctn_num"               => $maxCTNnum,
            "ctn_sts"               => 'AC',
            "item_id"               => $item->item_id,
            "ctn_pack_size"         => $item->pack,
            "sku"                   => $item->sku,
            "size"                  => $item->size,
            "color"                 => $item->color,
            "lot"                   => (!empty($attribute['lot']))?$attribute['lot']:'NA',
            "ctn_uom_id"            => $item->uom_id,
            "is_damaged"            => 0,
            "piece_remain"          => $item->pack,
            "piece_ttl"             => $item->pack,
            "is_ecom"               => 0,
            "picked_dt"             => 0,
            "storage_duration"      => 0,
            "uom_code"              => $item->uom_code,
            "uom_name"              => $item->uom_name,
            "upc"                   => (!empty($item->cus_upc))?$item->cus_upc:"",
            "length"                => $item->length,
            "width"                 => $item->width,
            "height"                => $item->height,
            "weight"                => $item->weight,
            "cube"                  => $item->cube,
            "volume"                => $item->volume,
            "odr_dtl_id"            => $attribute['odr_dtl_id'],
            "created_at"            => $time,
            "created_by"            => $user_id,
            "updated_at"            => $time,
            "updated_by"            => $user_id,
            "expired_dt"            => (!empty($attribute['expired_dt']))?strtotime($attribute['expired_dt']):'',
            "inner_pack"            => 0
        ];
        $this->cartonModel->create($cartonParams);

        DB::commit();

    }

    public function AllocateOrderKit($attribute) {
        $odrHdr = $attribute['odr_hdr'];
        $odrDtls = $this->odrDtlModel->getModel()
                    ->where("odr_id", $attribute['odr_id'])->get();
        if(empty($odrDtls)) {
            return [
                "is_error"  => 1,
                "message"   => "Order Detail does not exist!!!"
            ];
        }

        DB::beginTransaction();
        foreach ($odrDtls as $odrDtl) {
            $this->odrDtlModel->allocateKit($odrDtl);
        }

        $check = $this->odrDtlModel->getFirstWhere([
            "odr_id"    => $attribute['odr_id'],
            "itm_sts"   => "NW"
        ]);
        if(empty($check)) {
            $odrHdr->odr_sts = "AL";
            $odrHdr->save();
        }

//        $test = $this->getFirstWhere(['odr_id' => 1913]);
//        $test = DB::select(DB::raw("SELECT * FROM inventory WHERE item_id IN (15752,15753,15754)"));
//        dd($test);
//        dd(0);
        DB::commit();
        return 0;
    }

    public function getOrderList($whsId, $attributes)
    {
        $limit = array_get($attributes, 'limit') ? array_get($attributes, 'limit') : 20;
        $query = $this->model->join('odr_dtl', 'odr_dtl.odr_id', 'odr_hdr.odr_id')
            ->where('odr_hdr.whs_id', $whsId)
            ->where('odr_dtl.picked_qty', '>', 0)
            ->select([
                'odr_hdr.whs_id',
                'odr_hdr.cus_id',
                'odr_hdr.odr_id',
                'odr_hdr.odr_num',
                'odr_hdr.odr_sts',
                'odr_hdr.cancel_by_dt',
                'odr_hdr.act_cancel_dt'
            ]);

        if (isset($attributes['cus_id']) && !empty($attributes['cus_id'])) {
            $query->where('odr_hdr.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['status']) && !empty($attributes['status'])) {
            $query->where('odr_hdr.odr_sts', $attributes['status']);
        }

        return $query->groupBy('odr_hdr.odr_id')->orderBy('odr_hdr.updated_at', 'DESC')->paginate($limit);
    }

    public function getDeliveryOrders($attributes, $whsId, $qualifier)
    {
        $limit = array_get($attributes, 'limit', 20);
        $query = $this->model->leftJoin('odr_hdr_meta', function ($join) use ($qualifier) {
            $join->on('odr_hdr_meta.odr_id', '=', 'odr_hdr.odr_id');
            $join->where('odr_hdr_meta.qualifier', $qualifier);
        })
            ->where('odr_hdr.whs_id', $whsId)
            ->whereIn('odr_hdr.odr_sts', ['SS', 'SP']);

        if (! empty($attributes['cus_odr_num'])) {
            $query->where('odr_hdr.cus_odr_num', 'like', "%{$attributes['cus_odr_num']}%");
        }

        $query->select([
            'odr_hdr.odr_id',
            'odr_hdr.cus_odr_num',
            'odr_hdr.odr_num',
            'odr_hdr.odr_sts',
            DB::raw("odr_hdr_meta.value ->> '$.drv_id' as drv_id"),
            DB::raw('IF(odr_hdr_meta.odr_id IS NULL, 0, 1) as assigned')
        ]);

        $orders = $query->orderBy('assigned', 'DESC')
            ->orderBy('odr_hdr.rush_odr', 'DESC')
            ->orderBy('odr_hdr.updated_at')
            ->paginate($limit);

        return $orders;
    }
}
