<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\BOL\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use stdClass;
use Wms2\UserInfo\Data;


/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class CartonModel extends AbstractModel
{
    protected $packRef = [];

    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    protected $currentWH;

    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    public static function getCalculateStorageDurationRaw($startDate = 'gr_dt')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

}