<?php

namespace App\Modules\Orders\Models;

use Illuminate\Support\Facades\DB;
use App\Modules\Orders\Eloquents\InventorySummary;
use App\Helpers\SelStr;

class InventorySummaryModel extends AbstractModel
{
    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        // search sku
        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        // search sku
        if (isset($attributes['color'])) {
            $query->where('color', 'like', "%" . SelStr::escapeLike($attributes['color']) . "%");
        }

        // search sku
        if (isset($attributes['size'])) {
            $query->where('size', 'like', "%" . SelStr::escapeLike($attributes['size']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * @param $item_ids
     *
     * @return mixed
     */
    public function getByItemIds($item_ids)
    {
        $userInfo = $this->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $item_ids = is_array($item_ids) ? $item_ids : [$item_ids];

        $rows = $this->model
            ->whereIn('item_id', $item_ids)
            ->where('whs_id', '=', $currentWH)
            ->get();

        return $rows;
    }

    public function getAvailableQty($itemId)
    {
        $query = $this->make([]);

        $query->where('item_id', $itemId);
        $query->where('avail', '>', 0);
        $this->model->filterData($query);

        return $query->first();
    }

    public function getInvSumbyOdrDlt($itemId, $lot, $whs_id, $get = 'avail')
    {
        $query = $this->make([]);

        $query = $query->where('item_id', $itemId);
        if (strtoupper($lot) != 'ANY') {
            $query->where('lot', $lot);
        }

        $query->where('whs_id', $whs_id);

        //$this->model->filterData($query);

        return $query->sum($get);
    }

    public function updateAllocatedOrderInventory($whsId, $odrId)
    {
        $odrDtls = DB::table('odr_dtl')->where('deleted', 0)
            ->where('odr_id', $odrId)
            ->where('whs_id', $whsId)
            ->where('lot', '!=', 'Any')
            ->get();
        if (count($odrDtls) > 0) {
            foreach ($odrDtls as $odrDtl) {
                $itemId   = array_get($odrDtl, 'item_id');
                $lot      = array_get($odrDtl, 'lot');
                $allocQty = array_get($odrDtl, 'alloc_qty');

                $this->updateAllocInvtWhenOdrPicking($whsId, $itemId, $lot, $allocQty);
            }
        }
    }

    public function updateAllocInvtWhenOdrPicking($whsId, $itemId, $lot, $qty)
    {
        $invtObj = $this->getFirstWhere([
            'whs_id'  => $whsId,
            'item_id' => $itemId,
            'lot'     => $lot,
        ]);
        if (!$invtObj) {
            $msg = sprintf("Not found item id %s, lot %s, warehouse %s in inventory summary", $itemId, $lot, $whsId);
            throw new \Exception($msg);
        }
        $invtAlloc = object_get($invtObj, 'allocated_qty', 0);
        $invtAvail = object_get($invtObj, 'avail', 0);

        $returnAlloc = 0;
        $returnAvail = 0;
        if ($invtAlloc < $qty) {
            $returnAlloc = $invtAlloc;
            $returnAvail = $invtAlloc;
        } else {
            $returnAlloc = $qty;
            $returnAvail = $qty;
        }

        $invtObj->allocated_qty = $invtAlloc - $returnAlloc;
        $invtObj->avail = $invtAvail + $returnAvail;
        $invtObj->save();
    }
}