<?php

namespace App\Api\Ecom\Validators;


use Illuminate\Support\Facades\Input;

class XferValidator extends AbstractValidator
{

    protected function rules()
    {
        $avail = Input::get('avail_qty');

        return [
            'xfer_ticket_id'  => 'required|exists:xfer_ticket,xfer_ticket_id',
            'xfer_ticket_num' => 'required|exists:xfer_ticket,xfer_ticket_num',
            'xfer_hdr_id'     => 'required|exists:xfer_hdr,xfer_hdr_id',
            'xfer_hdr_num'    => 'required|exists:xfer_hdr,xfer_hdr_num',
            'xfer_dtl_id'     => 'required|exists:xfer_dtl,xfer_dtl_id',
            'from_loc_id'     => 'required|integer',
            'from_loc_code'   => 'required',
            'to_loc_id'       => 'required|integer',
            'to_loc_code'     => 'required',
            'avail_qty'       => 'required|integer',
            'pick_qty'        => 'required|integer|max:'.$avail,
            'item_id'         => 'required|integer',
            'cus_id'          => 'required|integer',
            'is_last'         => 'required',
            'complete'        => 'required'
        ];

    }


}
