<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:00
 */

namespace App\Api\V1\Validators;

class SystemUomDeleteMassValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'sys_uom_id'   => 'required',
            'sys_uom_id.*' => 'required|exists:system_uom,sys_uom_id'
        ];
    }
}
