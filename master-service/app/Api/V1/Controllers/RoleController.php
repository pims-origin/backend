<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\RoleService;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class RoleController extends AbstractController
{
    protected $service;

    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new RoleService($request);
    }

    public function getList(IRequest $request)
    {
        $this->checkPermission($request, 'viewRole');
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getRoleListByQueryString($queryStr);
        return $this->convertResponse($result);
    }

    public function getRoleDetail(IRequest $request, $roleName)
    {
        $this->checkPermission($request, 'viewRole');
        $result = $this->service->getRole($roleName);

        return $this->convertResponse($result);
    }

    public function getUserByRole(IRequest $request, $role)
    {
        $this->checkPermission($request, 'viewUser');
        $result = $this->service->getUserByRole($role);
        return $this->convertResponse($result);
    }

    public function getPermissionByRole(IRequest $request, $role)
    {
        $this->checkPermission($request, 'viewRole');
        $result = $this->service->getPermissionByRole($role);
        return $this->convertResponse($result);
    }

    public function assignPermission(IRequest $request, $role)
    {
        $this->checkPermission($request, 'assignPermissionToRole');
        $body = $request->getParsedBody();
        $result = $this->service->assignPermission($role, $body);

        return $this->convertResponse($result);
    }

    public function createRole(IRequest $request)
    {
        $this->checkPermission($request, 'createRole');
        $result = $this->service->createRole($request->getParsedBody());

        return $this->convertResponse($result);
    }

    public function updateRole(IRequest $request, $roleName)
    {
        $this->checkPermission($request, 'editRole');
        $result = $this->service->updateRole($roleName, $request->getParsedBody());

        return $this->convertResponse($result);
    }
    
    public function deleteRoles(IRequest $request)
    {
        $this->checkPermission($request, 'deleteRole');
        $result = $this->service->deleteRoles($request->getParsedBody());
        
        return $this->convertResponse($result);
    }
}
