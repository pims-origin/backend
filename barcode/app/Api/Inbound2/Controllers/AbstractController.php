<?php

namespace App\Api\Inbound2\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;


abstract class AbstractController extends Controller
{
    use Helpers;
}
