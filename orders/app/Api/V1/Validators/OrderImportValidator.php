<?php

namespace App\Api\V1\Validators;

class OrderImportValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'file'   => 'required',
            'cus_id' => 'required|exists:customer,cus_id'
        ];
    }
}
