<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\Zone;

class ZoneModel extends AbstractModel
{
    protected $model;

    public function __construct(Zone $model = null)
    {
        $this->model = ($model) ?: new Zone();
    }

    public function getWhsIdsByZoneIds($zoneIds)
    {
        $zoneIds = is_array($zoneIds) ? $zoneIds : [$zoneIds];

        $rows = $this->model
            ->whereIn('zone_id', $zoneIds)
            ->groupBy('zone_whs_id');
        $this->model->filterData($rows);
        $rows = $rows->get(['zone_whs_id']);
        $ids = [];
        foreach ($rows as $row) {
            $ids[] = $row->zone_whs_id;
        }

        return $ids;
    }
}
