<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');



// Handle application Route
$api->version('v1', [], function ($api) {

    require(__DIR__ . '/V2/routes.php');

    // Bypass middleware when testing
    $middleware = ['trimInput'];
    if (env('APP_ENV') != 'testing') {
        $middleware[] = 'verifySecret';
        $middleware[] = 'authorize';
        $middleware[] = 'setWarehouseTimezone';
    }

    $api->group(['prefix' => 'public', 'namespace' => 'App\Api\V1\Controllers', 'middleware' => []], function ($api) {
        $api->get('/download-document', ['action' => 'editOrder', 'uses' => 'DocumentController@downloadDocument']);
    });

    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers', 'middleware' => $middleware], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V1'];
        });

        // Pack - Undo Packing/Packed
        $api->put('/packs/{orderId:[0-9]+}/undo', ['action' => "orderPacking", "uses" => "PackHdrController@undoPackingPackedOrder"]);

        //pack
        $api->delete('/packs/{packId:[0-9]+}', ['action' => "orderPacking", "uses" => "PackHdrController@delete"]);

        // Order
        $api->get('/packs/{orderId:[0-9]+}', [
            'action' => "orderPacking",
            'uses'   => 'PackHdrController@show'
        ]);
        $api->get('/packs/{orderHdrId:[0-9]+}/print', [
            'action' => "orderPacking",
            'uses'   => 'PackHdrController@printCartonLabel'
        ]);
        $api->get('/packs', [
            'action' => "orderPacking",
            'uses'   => 'PackHdrController@search'
        ]);

        // Pack Detail
        $api->get('/packs/{orderHdrId:[0-9]+}/details', [
            'action' => "orderPacking",
            'uses'   => 'PackDtlController@show'
        ]);
        $api->post('/packs/{orderHdrId:[0-9]+}', [
            'action' => "orderPacking",
            'uses'   => 'PackDtlController@store'
        ]);
        $api->post('/packs/auto/{orderHdrId:[0-9]+}', [
            'action' => "orderPacking",
            'uses'   => 'PackHdrController@autoStore'
        ]);
        $api->put('/packs/{orderHdrId:[0-9]+}/details/{packHdrId:[0-9]+}', [
            'action' => "orderPacking",
            'uses'   => 'PackDtlController@update'
        ]);

        // Pack Ref
        $api->get('/pack-ref/{packRefId:[0-9]+}', [
            'action' => "orderPacking",
            'uses'   => 'PackRefController@show'
        ]);
        $api->get('/pack-ref', [
            'action' => "orderPacking",
            'uses'   => 'PackRefController@search'
        ]);

        //Assign pallet
        $api->put('/packs-assign-pallets/{print:[0-9]+}', [
            'action' => "palletAssignment",
            'uses'   => 'PackHdrController@assignPallet'
        ]);
        $api->get('/show-packs-assign-pallets', [
            'action' => "palletAssignment",
            'uses'   => 'PackHdrController@showAssignPallet'
        ]);
        $api->put('/packs-create-check-sum-all-orders', [
            'action' => "palletAssignment",
            'uses'
                     => 'PackDtlController@createPackDtlChecksumAllOrders'
        ]);
        $api->get('/packs-assign-pallets-printLpns', [
            'action' => "palletAssignment",
            'uses'   => 'PackHdrController@printLpns'
        ]);

    });
});