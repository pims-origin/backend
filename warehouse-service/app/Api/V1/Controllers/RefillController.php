<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\BaseService;
use App\Api\V1\Models\RefillService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;


class RefillController extends AbstractController
{
    /**
     * @var MenuService
     */
    protected $service;

    /**
     * MenuController constructor.
     *
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new RefillService($request);
    }

    /**
     * @param $refillId
     *
     * @return \Illuminate\Http\Response
     */
    public function show($refillId)
    {
        //$queryStr = $request->getUri()->getQuery();
        $result = $this->service->show($refillId);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param $locationId
     *
     * @return Response|void
     */
    public function showRefillAccordingToLocation($locationId)
    {
        //$queryStr = $request->getUri()->getQuery();
        $result = $this->service->showRefillAccordingToLocation($locationId);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param $refillId
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($refillId)
    {
        //$queryStr = $request->getUri()->getQuery();
        $result = $this->service->destroy($refillId);
        $response = $this->convertResponse($result);

        return $response;

    }

    /**
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(IRequest $request)
    {
        $input = $request->getParsedBody();

        $result = $this->service->store($input);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param IRequest $request
     * @param $refillId
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function update(IRequest $request, $refillId)
    {
        //$queryStr = $request->getUri()->getQuery();
        //$result = $this->service->update($queryStr, $refillId);
        //$response = $this->convertResponse($result);
        //
        //return $response;

        // get data from HTTP
        $input = $request->getParsedBody();
        $result = $this->service->update($input, $refillId);

        if ($result instanceof \GuzzleHttp\Psr7\Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }


}
