<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportShipping;

class ReportShippingTransformer extends AbstractTransformer
{

    /**
     * @param ReportShipping $reportShipping
     * @return array
     */
    public function transform(ReportShipping $reportShipping)
    {
        return [
        	"id"				=>	array_get($reportShipping, 'id', null),
            "cus_id"			=>	array_get($reportShipping, 'cus_id', null),
            "cus_name"			=>	array_get($reportShipping, 'cus_name', null),
            "cus_code"			=>	array_get($reportShipping, 'cus_code', null),
            "whs_id"			=>	array_get($reportShipping, 'whs_id', null),
            "odr_id"			=>	array_get($reportShipping, 'odr_id', null),
            "odr_num"			=>	array_get($reportShipping, 'odr_num', null),
            "odr_type"			=>	array_get($reportShipping, 'odr_type', null),
            "item_id"			=>	array_get($reportShipping, 'item_id', null),
            "sku"				=>	array_get($reportShipping, 'sku', null),
            "size"				=>	array_get($reportShipping, 'color', null),
            "color"				=>	array_get($reportShipping, 'color', null),
            "lot"				=>	array_get($reportShipping, 'lot', null),
            "pack"				=>	array_get($reportShipping, 'pack', null),
            "des"				=>	array_get($reportShipping, 'des', null),
            "cus_upc"			=>	array_get($reportShipping, 'cus_upc', null),
            "shipping_ctns"		=>	array_get($reportShipping, 'shipping_ctns', null),
            "cube"				=>	array_get($reportShipping, 'cube', null),
            "picked_qty"		=>	array_get($reportShipping, 'picked_qty', null),
            "shipped_dt"		=>	$this->setTimeZone(array_get($reportShipping, 'whs_id', null), array_get($reportShipping, 'shipped_dt', null)),
            "ship_by"			=>	array_get($reportShipping, 'cus_odr_num', null),
            "cus_odr_num"		=>	array_get($reportShipping, 'cus_odr_num', null),
            "cus_po"			=>	array_get($reportShipping, 'cus_po', null),
            "ship_to_name"		=>	array_get($reportShipping, 'ship_to_name', null),
            "ship_to_add_1"		=>	array_get($reportShipping, 'ship_to_add_1', null),
            "ship_to_city"		=>	array_get($reportShipping, 'ship_to_city', null),
            "ship_to_state"		=>	array_get($reportShipping, 'ship_to_state', null),
            "sys_state_name"	=>	array_get($reportShipping, 'sys_state_name', null),
            "ship_to_zip"		=>	array_get($reportShipping, 'ship_to_zip', null),
            "carrier"			=>	array_get($reportShipping, 'carrier', null),
            "updated_by"		=>	array_get($reportShipping, 'updated_by', null),
            "created_at"		=>	$this->setTimeZone(array_get($reportShipping, 'whs_id', null), array_get($reportShipping->toArray(), 'created_at', null)),
            "updated_at"		=>	$this->setTimeZone(array_get($reportShipping, 'whs_id', null), array_get($reportShipping->toArray(), 'updated_at', null)),
            "created_by"		=>	array_get($reportShipping, 'created_by', null),
            "deleted_at"		=>	$this->setTimeZone(array_get($reportShipping, 'whs_id', null), array_get($reportShipping, 'deleted_at', null)),
            "deleted"			=>	array_get($reportShipping, 'deleted', null),
            'timezone'     		=>  $this->getWhsTimeZone(array_get($reportShipping, 'whs_id', null)) ?: 'UTC±0',
        ];
    }
}
