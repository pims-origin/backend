<?php

namespace App\Api\ReplenishmentConfig\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\ReplenishmentConfig;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class ReplenishmentConfigModel extends AbstractModel
{
    /**
     * ReplenishmentConfigModel constructor.
     */
    public function __construct()
    {
        $this->model = new ReplenishmentConfig();
    }

    /**
     * @param $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function search($attributes, array $with, $limit = null, $export = false)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query = $this->model
            ->select([
                    'replenishment_config.id',
                    'replenishment_config.cus_id',
                    'c.cus_name',
                    'replenishment_config.whs_id',
                    'replenishment_config.loc_id',
                    'replenishment_config.loc_code',
                    'replenishment_config.item_id',
                    'replenishment_config.loc_code',
                    'replenishment_config.sku',
                    'replenishment_config.min',
                    'replenishment_config.max',
                    'replenishment_config.uom_id',
                    'replenishment_config.uom_code',
                    'su.sys_uom_name AS uom_name',
                    'replenishment_config.des',
                    'replenishment_config.updated_at',
                    'replenishment_config.updated_by',
                    //'replenishment_config.current_qty',
                    //DB::raw("IF(replenishment_config.current_qty < replenishment_config.min, replenishment_config.max-replenishment_config.current_qty, 0) AS transfer_qty")
                ]
            )
            ->leftJoin('customer AS c', 'c.cus_id', '=', 'replenishment_config.cus_id')
            ->leftJoin('system_uom AS su', 'su.sys_uom_id', '=', 'replenishment_config.uom_id');

        if (isset($attributes['sku'])) {
            $query->where('replenishment_config.sku', $attributes['sku']);
        }
        $query->where('replenishment_config.whs_id', $currentWH);
        //$query->whereRaw("replenishment_config.min > replenishment_config.current_qty");

        $query->orderBy('replenishment_config.id', 'DESC');
        $this->sortBuilder($query, $attributes);

        //if ($export) {
        $models = $query->get();
        //} else {
        //    $models = $query->paginate($limit);
        //}

        return $models;
    }

    /**
     * @param $repConfId
     *
     * @return mixed
     */
    public function showDetail($repConfId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->model
            ->select([
                    'replenishment_config.id',
                    'replenishment_config.cus_id',
                    'c.cus_name',
                    'replenishment_config.whs_id',
                    'replenishment_config.loc_id',
                    'replenishment_config.loc_code',
                    'replenishment_config.item_id',
                    'replenishment_config.loc_code',
                    'replenishment_config.sku',
                    'replenishment_config.min',
                    'replenishment_config.max',
                    'replenishment_config.uom_id',
                    'replenishment_config.uom_code',
                    'su.sys_uom_name AS uom_name',
                    'replenishment_config.des',
                    'replenishment_config.updated_at',
                    'replenishment_config.updated_by',
                    //'replenishment_config.current_qty',
                    //DB::raw("IF(replenishment_config.current_qty < replenishment_config.min, replenishment_config.max-replenishment_config.current_qty, 0) AS transfer_qty")
                ]
            )
            ->leftJoin('customer AS c', 'c.cus_id', '=', 'replenishment_config.cus_id')
            ->leftJoin('system_uom AS su', 'su.sys_uom_id', '=', 'replenishment_config.uom_id')
            ->where('replenishment_config.id', $repConfId);

        $models = $query->first();

        return $models;
    }

    public function getCartonNumberOfLoc($locCode, $currentWH, $itemId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons')
            ->select([
                    DB::raw("COUNT(*) AS ctnNo")
                ]
            )
            ->where('cartons.loc_code', $locCode)
            ->where('cartons.whs_id', $currentWH)
            ->where('cartons.item_id', $itemId);

        $models = $query->first();

        return $models;
    }

    /**
     * @param $cusId
     * @param $currentWH
     * @param $itemId
     * @param $locIdTo
     *
     * @return mixed
     */
    public function suggestLocation($cusId, $currentWH, $itemId, $locIdTo)
    {
        $customerConfigModel = new CustomerConfigModel();
        $algorithm = $customerConfigModel->getPickingAlgorithm($currentWH, $cusId);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = (new Carton())->getModel()
            ->select([
                    'cartons.loc_id',
                    'cartons.loc_code',
                    'cartons.loc_name',
                    DB::raw("COUNT(cartons.ctn_id) AS ctnNo"),
                ]
            )
            ->where('cartons.cus_id', $cusId)
            ->where('cartons.whs_id', $currentWH)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.uom_code', 'CT')
            ->where('cartons.is_damaged', 0)
            ->where('cartons.item_id', $itemId)
            ->where('cartons.loc_id', '!=', $locIdTo)
            ->whereNotNull('cartons.loc_id');
             switch (strtoupper($algorithm)) {
                 case "LIFO":
                     $query->orderBy('cartons.gr_dt', 'DESC');
                     $query->orderBy('cartons.created_at', 'DESC');
                     break;
                 case "FEFO":
                     $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                     $query->orderBy('cartons.created_at', 'ASC');
                     break;
                 case "FIFO":
                     $query->orderBy('cartons.gr_dt', 'ASC');
                     $query->orderBy('cartons.created_at', 'ASC');
                     break;
             }
            $query->orderBy('cartons.piece_remain', 'ASC');
            $query->groupBy('cartons.loc_id');
            $query->take(5);

        $models = $query->get();

        return $models;
    }


}
