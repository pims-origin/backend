<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class GoodReceiptHdrModel extends AbstractModel
{
    const DAS_LIMIT = 0;
    /**
     * GoodReceiptHdrModel constructor.
     *
     * @param GoodsReceipt|null $model
     */
    public function __construct(GoodsReceipt $model = null)
    {
        $this->model = ($model) ?: new GoodsReceipt();
    }

    /**
     * @return mixed
     */
    public function putawayDashBoard($input)
    {
        $limit = (!empty($input['day']) && is_numeric($input['day'])) ? $input['day'] : self::DAS_LIMIT;

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId =  array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $putAlready = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as putAlready"))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('pal_sug_loc as psl')
                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
            })
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('g.putter', $userId);
            })
            ->where(DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id )'), '=',
                DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id AND put_sts = 
                "'.config('constants.putaway_status.COMPLETED').'")')
            )
            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->first();

        $putaway = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as putaway"))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('pal_sug_loc as psl')
                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
            })
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('g.putter', $userId);
            })
            ->where(DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id )'), '=',
                DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id AND put_sts = 
                "'.config('constants.putaway_status.NEW').'")')
            )
            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->first();

        $totalPutaway = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as total"))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('pal_sug_loc as psl')
                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
            })
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('g.putter', $userId);
            })
            ->where('g.gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->first();

        $putAlready = !empty($putAlready['putAlready']) ? $putAlready['putAlready'] : 0;
        $putaway = !empty($putaway['putaway']) ? $putaway['putaway'] : 0;
        $totalPutaway = !empty($totalPutaway['total']) ? $totalPutaway['total'] : 0;

        return [
            "putaway_total" => ($putaway > 0) ? $putaway : 0,
            "putting" => ($totalPutaway -  $putaway - $putAlready) > 0
                ? ($totalPutaway -  $putaway - $putAlready) : 0,
            "putReady" => ($putAlready > 0) ? $putAlready : 0
        ];
    }

    /**
     * @return mixed
     */
//    public function putawayAdminDashBoard($input)
//    {
//        $limit = (!empty($input['day']) && is_numeric($input['day'])) ? $input['day'] : self::DAS_LIMIT;
//
//        $predis = new Data();
//        $userInfo = $predis->getUserInfo();
//        $currentWH = array_get($userInfo, 'current_whs', 0);
//
//        $putAlready = DB::table('gr_hdr as g')
//            ->select(DB::raw("COUNT(1) as putAlready"))
//            ->whereExists(function ($query) {
//                $query->select(DB::raw(1))
//                    ->from('pal_sug_loc as psl')
//                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
//            })
//            ->where('g.whs_id', $currentWH)
//            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
//            ->whereNotNull('g.putter')
//            ->where(DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id )'), '=',
//                DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id AND put_sts =
//                "'.config('constants.putaway_status.COMPLETED').'")')
//            )
//            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
//            ->first();
//
//        $putaway = DB::table('gr_hdr as g')
//            ->select(DB::raw("COUNT(1) as putaway"))
//            ->whereExists(function ($query) {
//                $query->select(DB::raw(1))
//                    ->from('pal_sug_loc as psl')
//                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
//            })
//            ->where('g.whs_id', $currentWH)
//            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
//            ->whereNotNull('g.putter')
//            ->where(DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id )'), '=',
//                DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id AND put_sts =
//                "'.config('constants.putaway_status.NEW').'")')
//            )
//            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
//            ->first();
//
//        $totalPutaway = DB::table('gr_hdr as g')
//            ->select(DB::raw("COUNT(1) as total"))
//            ->whereExists(function ($query) {
//                $query->select(DB::raw(1))
//                    ->from('pal_sug_loc as psl')
//                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
//            })
//            ->where('g.whs_id', $currentWH)
//            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
//            ->whereNotNull('g.putter')
//            ->where('g.gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
//            ->first();
//
//        $putAlready = !empty($putAlready['putAlready']) ? $putAlready['putAlready'] : 0;
//        $putaway = !empty($putaway['putaway']) ? $putaway['putaway'] : 0;
//        $totalPutaway = !empty($totalPutaway['total']) ? $totalPutaway['total'] : 0;
//
//        return [
//            "putaway_total" => ($putaway > 0) ? $putaway : 0,
//            "putting" => ($totalPutaway -  $putaway - $putAlready) > 0
//                ? ($totalPutaway -  $putaway - $putAlready) : 0,
//            "putReady" => ($putAlready > 0) ? $putAlready : 0
//        ];
//    }
    public function putawayAdminDashBoard($input)
    {
        $limit = (!empty($input['day']) && is_numeric($input['day'])) ? $input['day'] : self::DAS_LIMIT;

        $currentWH = Data::getCurrentWhsId();
        $currentCusIds = Data::getCurrentCusIds();

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $data = DB::table('gr_hdr')
            ->selectRaw("
                COUNT(DISTINCT IF((gr_hdr.putaway = 0 AND pallet.gr_hdr_id IS NULL), gr_hdr.gr_hdr_id, NULL)) AS putaway,
                COUNT(DISTINCT IF((gr_hdr.putaway = 0 AND pallet.gr_hdr_id IS NOT NULL), gr_hdr.gr_hdr_id, NULL)) AS putting,
                COUNT(DISTINCT IF((gr_hdr.putaway = 1), gr_hdr.gr_hdr_id, NULL)) AS putAlready
            ")
            ->leftJoin('pallet', function($join) {
                $join->on('gr_hdr.gr_hdr_id', '=', 'pallet.gr_hdr_id');
                $join->on('pallet.deleted', '=', DB::raw(0));
            })
            ->where('gr_hdr.deleted', 0)
            ->where('gr_hdr.whs_id', $currentWH)
            ->whereIn('gr_hdr.cus_id', $currentCusIds)
            ->where('gr_hdr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $limit . ' DAY)'))
            ->first();

        if (!$data) {
            $data = [
                'putaway' => 0,
                'putting'       => 0,
                'putAlready'    => 0
            ];
        }

        return $data;
    }

    /**
     * @return mixed
     */
    public function grDashBoard($input)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $limit = (!empty($input['day']) && is_numeric($input['day'])) ? $input['day'] : self::DAS_LIMIT;

        $inprocess = DB::table('gr_hdr')
            ->select(DB::raw('COUNT(1) as inprocess'))
            ->where('whs_id', $currentWH)
            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVING"))
            ->where('updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('created_by', $userId)
                    ->orWhere('updated_by', $userId);
            })
            ->first();

        $received = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as received"))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('pal_sug_loc as psl')
                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
            })
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('g.created_by', $userId)
                    ->orWhere('g.updated_by', $userId);
            })
            ->whereNotNull('g.putter')
            ->where(DB::raw('(SELECT COUNT(plt_id) FROM pallet WHERE g.gr_hdr_id = pallet.gr_hdr_id AND pallet.loc_id IS NULL)'),
                '>', 0)
            ->where('g.gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->first();

        return [
            'ttl_gr_in_process'     => !empty($inprocess['inprocess']) ? $inprocess['inprocess'] : 0,
            'ttl_gr_ready_put_away' => !empty($received['received']) ? $received['received'] : 0
        ];
    }

    /**
     * @return mixed
     */
    public function putawayReceivedDashBoard($input)
    {
        $limit = (!empty($input['day']) && is_numeric($input['day'])) ? $input['day'] : self::DAS_LIMIT;

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $putAlready = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as putAlready"))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('pal_sug_loc as psl')
                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
            })
            ->where(function ($query) use ($userId) {
                $query->where('g.created_by', $userId)
                    ->orWhere('g.updated_by', $userId);
            })
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id )'), '=',
                DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id AND put_sts = 
                "'.config('constants.putaway_status.COMPLETED').'")')
            )
            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->first();

        $putaway = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as putaway"))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('pal_sug_loc as psl')
                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
            })
            ->where(function ($query) use ($userId) {
                $query->where('g.created_by', $userId)
                    ->orWhere('g.updated_by', $userId);
            })
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id )'), '=',
                DB::raw('(SELECT COUNT(id) FROM pal_sug_loc WHERE g.gr_hdr_id = pal_sug_loc.gr_hdr_id AND put_sts = 
                "'.config('constants.putaway_status.NEW').'")')
            )
            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->first();

        $totalPutaway = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as total"))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('pal_sug_loc as psl')
                    ->whereRaw('psl.gr_hdr_id = g.gr_hdr_id');
            })
            ->where(function ($query) use ($userId) {
                $query->where('g.created_by', $userId)
                    ->orWhere('g.updated_by', $userId);
            })
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where('g.gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->first();

        $putAlready = !empty($putAlready['putAlready']) ? $putAlready['putAlready'] : 0;
        $putaway = !empty($putaway['putaway']) ? $putaway['putaway'] : 0;
        $totalPutaway = !empty($totalPutaway['total']) ? $totalPutaway['total'] : 0;

        return [
            "putaway_total" => ($putaway > 0) ? $putaway : 0,
            "putting" => ($totalPutaway -  $putaway - $putAlready) > 0
                ? ($totalPutaway -  $putaway - $putAlready) : 0,
            "putReady" => ($putAlready > 0) ? $putAlready : 0
        ];
    }
}