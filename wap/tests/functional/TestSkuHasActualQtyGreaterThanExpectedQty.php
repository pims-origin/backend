<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\AsnDtlModel;
use Dingo\Api\Routing\Helpers;
use App\Api\V1\Transformers\AsnListsCanSortV1Transformer;
use Seldat\Wms2\Models\VirtualCarton;

class TestSkuHasActualQtyGreaterThanExpectedQty extends TestCase
{
    //use DatabaseTransactions;
    use Helpers;

    private $whsId = 1;
    private $cusId = 1;
    public static $_caseCreateVTCarton = [];
    private $_token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0ODkwMjM5NTUsImV4cCI6MTQ4OTExMDM1NSwibmFtZSI6ImN1b25nIG5ndXllbiIsInVzZXJuYW1lIjoiY2hpY3VvbmdodCIsImp0aSI6MTR9.9EZ7SL0Vd6Ju5gzehKNNTHck1VMhjjemfWN_C7JvAjg";

    public function setUp()
    {
        parent::setUp();


    }

    private function getASNInfo()
    {
        $asnModel = new AsnDtlModel();
        $input['whs_id'] = $this->whsId;
        $return = $asnModel->getListASN(
            $input, null
        );
        $obj = $return->getCollection()->first();
        return $obj;
    }


    private function createVirtualCarton($obj, $ranNum)
    {
        $tran = new AsnListsCanSortV1Transformer();
        $data = $tran->transform($obj);

        $asnHdrId = $data['asn_hdr_id'];

        //check them item co hay ko
        $asnDtlId = $data['items'][0]['asn_dtl_id'];
        $itemId = $data['items'][0]['item_id'];
        $ctnrId = $data['ctnr_id'];
        $cusId = $data['cus_id'];
        $vtlCtnOnPallet = [];

        $expectTtlCtl = $data['items'][0]['asn_dtl_ctn_ttl'];

        for ($i = 0; $i < $expectTtlCtl + $ranNum; $i++) {
            $vtlCtnOnPallet[] = strtoupper(uniqid("CTN-"));
        }

        $params = [
            'asn_hdr_id' => $asnHdrId,
            'asn_dtl_id' => $asnDtlId,
            'type' => 1,
            'item_id' => $itemId,
            'ctnr_id' => $ctnrId,
            'ctns_rfid' => $vtlCtnOnPallet,
        ];

        $url = "/inbound/whs/{$this->whsId}/cus/{$cusId}/add-virtual-cartons";

        $response = $this->call("POST", $url, $params, [], []
            , ['HTTP_Authorization' => $this->_token]
        );
        $content = $response->content();

        return [
            'content' => $content,
            'url' => $url,
            'vtlCtnOnPallet' => $vtlCtnOnPallet,
            'expectTtlCtl' => $expectTtlCtl,
        ];
    }

    private function createPallet($vtlCtnOnPallet) {
        //Second call
        $pltRfid = strtoupper(uniqid("FF-"));
        $param = [
            'pallet.ctn-rfid' => $vtlCtnOnPallet,
            'pallet.pallet-rfid' => $pltRfid
        ];

        $url = "/inbound/whs/{$this->whsId}/scan-pallet/";
        $this->refreshApplication();
        $res = $this->call('PUT', $url, $param);
        $content = $res->content();

        return [
            'url' => $url,
            'data' => json_encode($param),
            'content' => $content,
        ];
    }

    private function completeSKU() {

    }

    public function testSkuHasActualQtyGreaterThanExpectedQty_NotCompeteSKU_OK()
    {
    }

    public function testSkuHasActualQtyGreaterThanExpectedQty()
    {
        //get ASN
        $obj = $this->getASNInfo();
        if (!$obj) {
            echo "[SKIP] No ANS to test\n";
            echo "[DONE] testSkuHasActualQtyGreaterThanExpectedQty_OK\n";
            $this->markTestSkipped("[SKIP] No ANS to test\n");
        }

        $ranNum = rand(1, 5);
        $content = $this->createVirtualCarton($obj, $ranNum);

        echo "[RURL] 1.{$content['url']}\n";
        echo "[CONT] {$content['content']}\n";
        $expectTtlCtl = $content['expectTtlCtl'];
        $receivedTtlCtl = $expectTtlCtl + $ranNum;

        $jsonObj = json_decode($content['content']);
        if (!isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            echo "[ERROR] ".$jsonObj->data->message ."\n";
            $this->fail("[FAIL] Step 1 insert carton unsuccessfully!\n");
        }

        $vtlCtnOnPallet = $content['vtlCtnOnPallet'];
        $content = $this->createPallet($vtlCtnOnPallet);
        if (!isset($jsonObj->data->status) || ! $jsonObj->data->status) {
            $this->fail("[FAIL] Step 2 create pallet unsuccessfully!\n");
        }
        echo "[RURL] 2.{$content['url']}\n";
        echo "[DATA] ".$content['data']."\n";
        echo "[CONT] {$content['content']}\n";

        $jsonObj = json_decode($content['content']);

        if (isset($jsonObj->status) && $jsonObj->status) {
            $this->assertTrue($jsonObj->status, '[FAIL] Result is false: ' . $jsonObj->message);
            $this->assertGreaterThan($expectTtlCtl, $receivedTtlCtl, 'ASN expect carton num: '.$expectTtlCtl." An received :". $receivedTtlCtl);
        } else {
            $this->fail("[FAIL] Unknown structure: {$content['content']}\n");
        }

        echo "=============END THIS CASE=============\n";
    }
}
