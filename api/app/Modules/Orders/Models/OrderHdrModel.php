<?php

namespace App\Modules\Orders\Models;

use App\Modules\Orders\Eloquents\OdrHdr;
use App\Helpers\Status;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{
    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OdrHdr $model = null)
    {
        $this->model = ($model) ?: new OdrHdr();
    }

    public function validateCancelOdr($odrObj)
    {
        $odrStsNotAllowCancel = [
            Status::getByValue("Partial Shipped", "Order-status"),
            Status::getByValue("Hold", "Order-status"),
            Status::getByValue("Shipped", "Order-status"),
            Status::getByValue("Canceled", "Order-status")
        ];

        $odrStatus = $odrObj->odr_sts;
        if (in_array($odrStatus, $odrStsNotAllowCancel)) {
            return false;
        }

        return true;
    }
}
