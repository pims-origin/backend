<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\InventorySummary;

class ItemDetailTransformer extends TransformerAbstract
{
    public $version;
    public function __construct($version=1)
    {
        $this->version=$version;
    }

    /*
     * Transform item
     * 
     * @param object $item
     * @return array
     */
    public function transform(Item $item)
    {
        $arrItemHier = [];
        //if ($item['type_flag'] == 'show') {
        //    // For show case
        //    if (!empty($item->itemHier)) {
        //        $level_max = 5;
        //        for ($i = 1; $i <= $level_max; $i++) {
        //            $arrItemHier[$i]['level_id'] = $i;
        //            foreach ($item->itemHier as $hier) {
        //                if ($hier->level_id == $i) {
        //                    $arrItemHier[$i]['details'] = $hier;
        //                }
        //            }
        //        }
        //    }
        //} else {
        //    // For search case
        //    if (!empty($item->itemHier)) {
        //        foreach ($item->itemHier as $itemHier) {
        //            $itemHier->sys_uom_id = $itemHier->uom_id;
        //        }
        //        $arrItemHier = $item->itemHier;
        //    }
        //}

        $data= [
            'item_id'       => $item->item_id,
            'description'   => $item->description,
            'sku'           => $item->sku,
            'size'          => $item->size,
            'color'         => $item->color,
            'uom_id'        => $item->uom_id,
            'uom_code'      => object_get($item, 'uom_code', null),
            'uom_name'      => object_get($item, 'uom_name', null),
            'pack'          => $item->pack,
            'length'        => number_format($item->length, 2),
            'width'         => number_format($item->width, 2),
            'height'        => number_format($item->height, 2),
            'weight'        => number_format($item->weight, 2),
            'volume'        => $item->volume,
            'cus_id'        => $item->cus_id,
            'customer_name' => object_get($item, 'customer.cus_name', null),
            'cus_upc'       => $item->cus_upc,
            'status'        => $item->status,
            'condition'     => $item->condition,
            'created_at'    => $item->created_at,
            'updated_at'    => $item->updated_at,
            'deleted_at'    => $item->deleted_at,
            'item_hier'     => $arrItemHier,
            'charge_by'     => $item->charge_by,
            'spc_hdl_code'  => $item->spc_hdl_code,
            'itm_img'       => $item->itm_img,
            'inventory'     => null,
        ];
        if($this->version==1) {
            $asnDtl = AsnDtl::where('item_id', $item->item_id)->first();
            $inventorySummary = InventorySummary::where('item_id', $item->item_id)->first();
            $data['inventory'] = $asnDtl ? ((count($asnDtl) > 0 || count($inventorySummary) > 0) ? 1 : 0) : 0;
        }
        return $data;
    }
}
