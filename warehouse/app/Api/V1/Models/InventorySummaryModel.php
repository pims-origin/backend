<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 29-August-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class InventorySummaryModel extends AbstractModel
{
    /**
     * @param InventorySummary $model
     */
    public function __construct(InventorySummary $model = null)
    {
        $this->model = ($model) ?: new InventorySummary();
    }

    /**
     * @param array $params
     * @param array $with
     * @param int $limit
     * @param bool|false $export
     *
     * @return mixed
     */
    public function inventoryReports($params = [], $with = [], $limit = 2000, $export = false)
    {
        $params = SelArr::removeNullOrEmptyString($params);
        //$query = $this->make($with);
        $query = (new InventorySummary())->getModel()
        ->join('customer', 'customer.cus_id', '=', 'invt_smr.cus_id')
        ->join('item', 'item.item_id', '=', 'invt_smr.item_id')
        ->select([
        'invt_smr.item_id',
        'invt_smr.sku',
        'invt_smr.item_id',
        'invt_smr.size',
        'invt_smr.color',
        'invt_smr.lot',
        'customer.cus_id',
        'customer.cus_name',
        'customer.cus_code',
        'invt_smr.avail',
        'invt_smr.allocated_qty',
        'invt_smr.picked_qty',
        'invt_smr.lock_qty',
        'invt_smr.dmg_qty',
        'invt_smr.crs_doc_qty',
        'invt_smr.ttl',
        'invt_smr.upc',

        'item.pack',
        'item.description',
        DB::raw('round(invt_smr.ttl/item.pack, 2) AS ctns'),

    ]);

        if (isset($params['cus_id'])) {
            $query->where('invt_smr.cus_id', intval($params['cus_id']));
        }

        if (isset($params['sku'])) {
            $query->where('invt_smr.sku', 'like', "%" . SelStr::escapeLike($params['sku']) . "%");
        }
        if (isset($params['lot'])) {
            $lot = trim($params['lot']);
            if (!empty($lot)) {
                $query->where('invt_smr.lot', $lot);
            }
        }

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('invt_smr.whs_id', $currentWH);
        $query->where('invt_smr.ttl', '!=', 0);
        $query->orderBy('invt_smr.updated_at', 'desc');

        $query->groupBy('invt_smr.item_id', 'invt_smr.lot');
        $this->sortBuilder($query, $params);

        if ($export) {
            return $query->get();
        } else {
            return $query->paginate($limit);
        }
    }
}
