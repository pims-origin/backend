<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Outbound\Order\AssignCartonsWithBarcode\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class WaveHdrModel extends AbstractModel
{

    /**
     * WaveHdrModel constructor.
     *
     * @param WavepickHdr|null $model
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $hdrDtlId
     *
     * @return mixed
     */
    public function getHdrPutAwayByHdrDtlID($hdrDtlId)
    {
        $sql1 = "pal_sug_loc.gr_hdr_num, pal_sug_loc.sku, pal_sug_loc.size, pal_sug_loc.color, count(*) as total,( " .
            " select count(*) as actual from pallet where pallet.plt_id = pal_sug_loc.plt_id " .
            " AND pallet.loc_id IS NOT NULL ) as actual";
        $query = $this->model
            ->select([
                DB::raw($sql1),
                "pal_sug_loc.gr_hdr_num",
                "pal_sug_loc.sku",
                "pal_sug_loc.size",
                "pal_sug_loc.color",
            ])
            ->where('pal_sug_loc.gr_dtl_id', $hdrDtlId)
            ->get();

        return $query;
    }

    public function getDtlPutAwayByHdrDtlID($hdrDtlId)
    {

        $query = $this->model
            ->select('location.loc_code', 'pallet.plt_num')
            ->join('location', 'location.loc_id', '=', 'pal_sug_loc.loc_id')
            ->join('pallet', 'pallet.plt_id', '=', 'pal_sug_loc.plt_id')
            ->whereNull('pallet.loc_id')
            ->where('pal_sug_loc.gr_dtl_id', $hdrDtlId)
            ->get();

        return $query;
    }

    /**
     * @param $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function search($attributes, $with = [])
    {
        $query = $this->make($with)
            ->select(['plt_id', 'gr_hdr_id', 'gr_hdr_num', DB::raw("count(gr_dtl_id) as total")])
            ->where('putter', $attributes['putter'])
            ->where('whs_id', $attributes['whs_id'])
            ->groupBy('gr_hdr_id');

        $model = $query->get();

        return $model;
    }

    public function show($attributes, $with = [])
    {
        $color = "case when color is not null then concat('-', color) else null end";
        $size = "case when size is not null then concat('-', size) else null end";
        $query = $this->make($with)
            ->select([
                'gr_hdr_id',
                'gr_hdr_num',
                'gr_dtl_id',
                'sku',
                'color',
                'size',
                DB::raw("concat(sku, $size, $color) as sku_size_color"),
                DB::raw("count(*) as total"),
                DB::raw("(select count(*) from pallet p where p.plt_id = pal_sug_loc.plt_id and p.loc_id is not null) as actual")
            ]);
        if (!empty($attributes['gr_hdr_id'])) {
            $query->where('gr_hdr_id', $attributes['gr_hdr_id']);
        }
        $query->where('putter', $attributes['putter'])
            ->where('whs_id', $attributes['whs_id'])
            ->groupBy('gr_dtl_id')
            ->orderBy('gr_hdr_id');

        $model = $query->get();

        return $model;
    }

    public function getWaveDashBoard($attributes, $with = [])
    {
        $query = $this->make($with);

        $query->where('whs_id', $attributes['whs_id'])
              ->where('picker', $attributes['picker']);

       $limit = (!empty($day=$attributes['day']) && is_numeric($day)) ? $day : 1;
        $query->where('created_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));
        return $query->get();
    }

    public function updateWvComplete($wv_id)
    {
        return $this->model
            ->where('wv_id', $wv_id)
            ->where('wv_sts', 'PK')
            ->whereRaw("
                0 = (
                    SELECT count(wv_dtl.wv_id) from wv_dtl
                    WHERE wv_dtl.wv_id = wv_hdr.wv_id
                    AND wv_dtl_sts != 'PD'
                )
            ")
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    public function updateWvPicking($wv_id)
    {
        return $this->model
            ->where('wv_id', $wv_id)
            ->update([
                'wv_sts'    => 'PK',
            ]);
    }

    /**
     * @param string $wvId
     *
     * @return mixed
     */
    public function updatePicked($wvId)
    {
        $sqlOdrPicked = sprintf("(SELECT COUNT(1) FROM odr_dtl WHERE odr_dtl.wv_id = %s AND odr_dtl.itm_sts IN ('NW', 'PK') AND deleted =0 AND alloc_qty > 0 and odr_hdr.odr_id = odr_dtl.odr_id) = 0", $wvId);
        $sqlWvPickedStr = "(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty != wv_dtl.piece_qty AND deleted =0) = 0";
        return \DB::table('wv_hdr')
            ->join('odr_hdr', 'odr_hdr.wv_num', '=', 'wv_hdr.wv_num')
            ->where('wv_hdr.wv_id', $wvId)
            ->where('wv_hdr.deleted', 0)
            ->where('odr_hdr.deleted', 0)
            ->whereRaw(DB::Raw($sqlWvPickedStr))
            ->whereRaw(DB::Raw($sqlOdrPicked))
            ->update([
                'wv_hdr.wv_sts'     => Status::getByValue("Completed", "WAVEPICK-STATUS"),
                'wv_hdr.updated_at' => time(),
                'wv_hdr.updated_by' => Data::getCurrentUserId(),
            ]);
    }

}
