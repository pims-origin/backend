<?php

namespace Seldat\Wms2\Models;

/**
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property mixed created_by
 * @property mixed updated_by
 * @property mixed loc_sts_dtl_sts_code
 * @property mixed loc_sts_dtl_from_date
 * @property mixed loc_sts_dtl_to_date
 * @property mixed loc_sts_dtl_reason
 * @property mixed loc_sts_dtl_remark
 * @property mixed loc_sts_dtl_loc_id
 */
class LocationStatusDetail extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loc_status_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'loc_sts_dtl_loc_id';

//    public $incrementing = false;


    /**
     * @var array
     */
    protected $fillable = [
        'loc_sts_dtl_loc_id',
        'loc_sts_dtl_sts_code',
        'loc_sts_dtl_from_date',
        'loc_sts_dtl_to_date',
        'loc_sts_dtl_reason',
        'loc_sts_dtl_remark',
        'loc_sts_reason_id',
        'created_by',
        'updated_by',
    ];

    public function updatedUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'updated_by', 'user_id');
    }
}
