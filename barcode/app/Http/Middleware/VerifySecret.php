<?php

namespace App\Http\Middleware;

use Closure;
use Namshi\JOSE\JWS;
use Seldat\Wms2\Utils\JWTUtil;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Utils;
use Wms2\UserInfo\Data;

class VerifySecret
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws JWTException
     * @throws TokenInvalidException
     */
    public function handle($request, Closure $next)
    {
        $token = JWTAuth::getToken();

        if (! $token) {
            throw new JWTException('A token is required', 400);
        }

        try {
            $jws = JWS::load($token);
        } catch (Exception $e) {
            throw new TokenInvalidException('Could not decode token: '.$e->getMessage());
        }

        if (! $jws->verify(config('jwt.secret'), config('jwt.algo'))) {
            throw new TokenInvalidException('Token Signature could not be verified.');
        }

        if (Utils::timestamp(JWTUtil::getPayloadValue('exp'))->isPast()) {
            throw new TokenExpiredException('Token has expired');
        }

        $data = new Data();
        $perListData = $data->getUserInfo();

        $perList = $perListData['permissions'];

        if(! in_array('accessGun', $perList)) {
            throw new TokenExpiredException("Access denied! You don't have permission.");
        }

        $response = $next($request);

        $content = \GuzzleHttp\json_decode($response->getContent(), true);
        if (!isset($content['errors']['message']) || env('API_DEBUG')) {
            return $response;
        }

        if(str_contains( $content['errors']['message'], 'SQLSTATE')) {
            $content['errors']['message'] = 'Data error, please contact WMS support or email to wms_support@seldatinc.com!';
            $response->setContent(\GuzzleHttp\json_encode($content));
        }

        return $response;
    }
}
