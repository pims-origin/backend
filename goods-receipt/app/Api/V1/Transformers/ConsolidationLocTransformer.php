<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Item;

class ConsolidationLocTransformer extends TransformerAbstract
{
    //return result
    public function transform(Carton $carton)
    {
        $expiredDt = '';
        $expired_dt = array_get($carton, 'expired_dt', null);
        if ($expired_dt != 0 && $expired_dt != null)
        {
            $expiredDt = date("m/d/Y", $expired_dt);
        }
        return [
            //carton
            'item_id'         => object_get($carton, 'item_id', null),
            'ctn_num'         => object_get($carton, 'ctn_num', null),
            'ctn_id'          => object_get($carton, 'ctn_id', null),
            'rfid'            => object_get($carton, 'rfid', null),
            'des'             => object_get($carton, 'des', null),
            //item
            'sku'             => array_get($carton, 'item.sku', null),
            'size'            => array_get($carton, 'item.size', null),
            'color'           => array_get($carton, 'item.color', null),
            //asnDtl change to get carton information
            'asn_dtl_lot'     => array_get($carton, 'lot', null),
            'asn_dtl_pack'    => array_get($carton, 'ctn_pack_size', null),
            'asn_dtl_cus_upc' => array_get($carton, 'upc', null),
            'uom_name'        => array_get($carton, 'uom_name', null),
            'uom_id'          => array_get($carton, 'ctn_uom_id', null),
            'asn_dtl_weight'  => array_get($carton, 'weight', null),
            'asn_dtl_height'  => array_get($carton, 'height', null),
            'asn_dtl_width'   => array_get($carton, 'width', null),
            'lpn'             => array_get($carton, 'lpn_carton', null),
            'qty'             => array_get($carton, 'qty', null),
            'expired_dt'      => $expiredDt,
        ];
    }

}
