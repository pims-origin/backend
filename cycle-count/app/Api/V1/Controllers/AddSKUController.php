<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AddSKUModel;
use App\Api\V1\Validators\AddSKUValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\V1\Transformers\CycleDtlTransformer;
use Swagger\Annotations as SWG;
use App\Api\V1\Models\EventTrackingModel;

class AddSKUController extends AbstractController
{
    protected $model;
    protected $validator;
    protected $request;
    protected $eventTrackingModel;
    public $errors = [];

    /**
     * AddSKUController constructor.
     * @param AddSKUModel $model
     * @param AddSKUValidator $validator
     * @param Request $request
     * @param EventTrackingModel $eventTrackingModel
     */

    public function __construct(
        AddSKUModel $model,
        AddSKUValidator $validator,
        Request $request,
        EventTrackingModel $eventTrackingModel
    )
    {
        $this->model = $model;
        $this->validator = $validator;
        $this->request = $request;
        $this->eventTrackingModel = $eventTrackingModel;
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response|void
     */

    public function store(Request $request, $cycle_id)
    {
        $input = $request->getParsedBody();
        $input['cycle_hdr_id'] = $cycle_id;
        $this->validator->validate($input);
        $itemID = $this->model->getItemID($input);
        if(!empty($itemID)){
            $itemID = $itemID[0]['item_id'];
        }
        $item_meta = DB::table("item_meta")->where("itm_id", $itemID)->first();
        if(!empty($item_meta)){
            $item_meta = $item_meta['value'];
            $item_meta = json_decode($item_meta, true);
        }

        $result = $this->model->getLocationIDByWarehouse($input['act_loc'], $input['whs_id']);
        $input['act_loc_id'] = $result['loc_id'];
        $spc_hdl_code_loc = DB::table("location")->where("loc_id",$input['act_loc_id'])->value("spc_hdl_code");
        if(empty($item_meta)) {
            $item_meta = [];
        }
        if(!array_key_exists($spc_hdl_code_loc, $item_meta) || $item_meta[$spc_hdl_code_loc] == 0) {
            throw new \Exception("Item and Location do not belong to the same Special Handling!");
        }
        $data = $this->model->processAddSKU($input);

        if (! $data) {
            return $this->response->errorBadRequest(implode($this->model->errors));
        }

        //event tracking
        $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingTransNum(
            $cycle_id,
            EventTrackingModel::CYCLE_ADD_SKU
        );
        $this->eventTrackingModel->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => isset($data['cus_id']) ? $data['cus_id'] : null,
            'owner'     => $cycleNumAndSeq['cycle_num'],
            'evt_code'  => EventTrackingModel::CYCLE_ADD_SKU,
            'trans_num' => $cycleNumAndSeq['trans_num'],
            'info'      => EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_ADD_SKU] . ' ' .
                $cycleNumAndSeq['trans_num'] . ' for ' . $cycleNumAndSeq['cycle_num']

        ]);

        return $this->response->item($data, new CycleDtlTransformer());
    }

    public function storeToCycle(Request $request, $cycle_id)
    {
        $input = $request->getParsedBody();
        $input['cycle_hdr_id'] = $cycle_id;
//
//        $this->validator->validate($input);

        $data = $this->model->processCreateCdtBySku($input);

        if (! $data) {
            return $this->response->errorBadRequest(implode($this->model->errors));
        }

        //event tracking
        $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingTransNum(
            $cycle_id,
            EventTrackingModel::CYCLE_ADD_SKU
        );
        $this->eventTrackingModel->create([
            'whs_id'    => $data['whs_id'],
            'cus_id'    => isset($data['cus_id']) ? $data['cus_id'] : null,
            'owner'     => $cycleNumAndSeq['cycle_num'],
            'evt_code'  => EventTrackingModel::CYCLE_ADD_SKU,
            'trans_num' => $cycleNumAndSeq['trans_num'],
            'info'      => EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_LOCATION] . ' ' .
                $input['sys_loc_name'] . ' for ' . $cycleNumAndSeq['cycle_num']

        ]);

        return $this->response->item($data, new CycleDtlTransformer());
    }
}