<?php

namespace App\Api\V1\Validators;


class PutawayCheckLocationValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'whs_id'    => 'required|integer|exists:warehouse,whs_id',
            'cus_id'    => 'required|integer|exists:customer_zone,cus_id',
            'loc_ids'   => 'required|array',
            'loc_ids.*' => 'required|integer'
        ];

    }


}
