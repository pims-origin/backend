<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 01/July/2016
 * Time: 11:05
 */

namespace App\Api\V1\Validators;


class EventLookupValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'evt_code' => 'required',
        ];
    }
}
