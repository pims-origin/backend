<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace Seldat\Wms2\Models;

class Pallet extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pallet';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'plt_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'loc_id',
        'whs_id',
        'plt_num',
        'ctn_ttl',
        'plt_block',
        'plt_tier',
        'rfid',
        'plt_sts',
        'loc_name',
        'loc_code',
        'created_by',
        'updated_by',
        'zero_date',
        'gr_hdr_id',
        'gr_dtl_id',
        'storage_duration',
        'return_id',
        'is_movement',
        'dmg_ttl',
        'data',
        'init_ctn_ttl',
        'gr_dt',
        'init_piece_ttl',
        'pack',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'is_xdock',
        'weight',
        "odr_id",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function grDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptDetail', 'gr_dtl_id', 'gr_dtl_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function palletSuggestLocation()
    {
        return $this->belongsTo(__NAMESPACE__ . '\PalletSuggestLocation', 'plt_id', 'plt_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Location()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Location', 'loc_id', 'loc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carton()
    {
        return $this->hasMany(__NAMESPACE__ . '\Carton', 'plt_id', 'plt_id');
    }
}
