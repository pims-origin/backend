<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\LocationTypeModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\ZoneModel;
use App\Api\V1\Transformers\ListAllLocationTransformer;
use App\Api\V1\Validators\AssignPalletToShippingLocationValidator;
use App\Api\V1\Validators\LocationUpdateRFIDValidator;
use App\libraries\RFIDValidate;
use App\Api\V1\Validators\UpdateLocationStatusValidator;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\LocationType;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use App\Api\V1\Models\Log;
use App\MyHelper;
use Symfony\Component\Console\Helper\Helper;
use Illuminate\Http\Response as IlluminateResponse;

class LocationController extends AbstractController
{

    protected $locationModel;
    protected $zoneModel;
    protected $locationUpdateRFIDValidator;

    public function __construct()
    {
        $this->locationModel = new LocationModel();
        $this->zoneModel = new ZoneModel();
        $this->locationUpdateRFIDValidator = new LocationUpdateRFIDValidator();
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param Request $request
     * @param ListAllLocationTransformer $listAllLocationTransformer
     *
     * @return Response|void
     */
    public function listAllLocation(
        $whsId,
        $cusId,
        Request $request,
        ListAllLocationTransformer $listAllLocationTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $params = [
            'whsId'    => $whsId,
            'cusId'    => $cusId,
            'loc_type' => $input['loc_type']
        ];
        try {

            /*
            * start logs
            */

            $url = "/whs/$whsId/cus/$cusId/location/list-all-location";
            $owner = $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'LAL',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'List all location'
            ]);
            /*
            * end logs
            */

            $loc = $this->zoneModel->searchAllLocation($params,
                [
                    'locationZone',
                    'locationZone.locationType',
                    'locationZone.pallet',
                    'customerZone'
                ],
                array_get($input, 'limit', 20));

            foreach ($loc as $key => $item) {
                foreach ($item['locationZone'] as $key2 => $item2) {
                    $loc[$key]['locationZone'][$key2]['loc_sts_name'] = Status::getByValue($item2['loc_sts_code'],
                        'LOCATION_STATUS');
                }
            }

            return $this->response->paginator($loc, $listAllLocationTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return null|string|void
     * @author: cuongnguyen
     */
    public function getLocationStatus($whsId, Request $request)
    {
        $input = $request->getQueryParams();
        $locCode = array_get($input, 'loc_code', null);
        $locRfid = array_get($input, 'loc_rfid', null);
        if (!$locCode && !$locRfid) {
            $msg = "Please input location's RFID or location code !";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }
        $status = null;
        try {
            /*
            * start logs
            */

            $url = "/whs/$whsId/location/get-status-location";
            $owner = "";
            $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'GLS',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Get location status'
            ]);
            /*
            * end logs
            */

            $location = $this->locationModel->getLocationStatusByLocCode($input, $whsId);
            if (!empty($location)) {
                $status = Status::getByValue(object_get($location, 'loc_sts_code', null), 'LOCATION_STATUS');
            }

            return $status;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     */

    public function getShippingLane($whsId, $loc_rfid = false)
    {
        try {
            $location = $this->locationModel->getLocShipLane($whsId, $loc_rfid);
            if (!empty($location)) {
                $status = Status::getByValue(object_get($location, 'loc_sts_code', null), 'LOCATION_STATUS');
            }
            $data['data'] = $this->formatArr($location, true);

            return new Response($data, 200, [], null);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function updateLocationStatus($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        //validate
        //(new UpdateLocationStatusValidator())->validate($input);
        $requireFieldsValidate = (new UpdateLocationStatusValidator())->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        try {
            DB::beginTransaction();
            $this->locationModel->updateWhere(
                [
                    'loc_sts_code' => Status::getByKey("LOCATION_STATUS", "RESERVED")
                ],
                [
                    'loc_code' => array_get($input, 'loc_code', null)
                ]
            );
            DB::commit();

            return $this->response->noContent()->setContent([
                'message' => "Update location status successfully."
            ])->setStatusCode(Response::HTTP_OK);
        } catch (Exception $e) {
            \DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function assignPalletToShippingLane($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        //validate
        //(new AssignPalletToShippingLocationValidator())->validate($input);
        $requireFieldsValidate = (new AssignPalletToShippingLocationValidator())->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        try {
            DB::beginTransaction();
            //get location with loc_code
            $locObj = $this->locationModel->getLocShipping($input, $whsId);

            if (empty($locObj)) {
                $msg = sprintf("There is no shipping location has loc code: %s", $input['loc_code']);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $palletModel = new PalletModel();
            //update pallet loc info with result
            $palletModel->updateWhere(
                [
                    "loc_id"   => $locObj->loc_id,
                    "loc_code" => $locObj->loc_code,
                    "loc_name" => $locObj->loc_name,
                ],
                [
                    "rfid"   => $input['plt_rfid'],
                    "whs_id" => $whsId
                ]
            );
            DB::commit();

            return $this->response->noContent()->setContent([
                'message' => sprintf("Assigned pallet %s to shipping lane %s successfully.", $input['plt_rfid'],
                    $input['loc_code'])
            ])->setStatusCode(Response::HTTP_OK);
        } catch (Exception $e) {
            \DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

    }


    public function updateLocationRFID($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        //(new LocationUpdateRFIDValidator())->validate($input);
        $requireFieldsValidate = (new LocationUpdateRFIDValidator())->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        $locCode = array_get($input, 'loc_code', null);
        $locRFID = array_get($input, 'loc_rfid', null);

        /*
           * start logs
           */

        $url = "/whs/{$whsId}/location/update-rfid";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Update location RFID by location code'
        ]);
        /*
        * end logs
        */


        //validate location RFID
        $locRFIDValid = new RFIDValidate($locRFID, RFIDValidate::TYPE_LOCATION, $whsId);
        if (!$locRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $locRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            DB::beginTransaction();
            //get location with loc_code
            $locObj = $this->locationModel->getLocByLocCode($locCode, $whsId);

            if (empty($locObj)) {
                $msg = sprintf("The location code: %s doesn't exist", $locCode);
                $locObj = $this->locationModel->getFirstWhere(['loc_code' => $locCode]);
                if ($locObj) {
                    $msg = sprintf("The location code: %s doesn't belong to warehouse", $locCode);
                }
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $locExistRFID = $this->locationModel->getFirstWhere(['rfid' => $locRFID]);
            if ($locExistRFID) {
                $msg = sprintf("The location RFID %s was defined for location has loc code: %s", $locRFID,
                    $locExistRFID->loc_code);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            if ($locObj->rfid) {
                $msg = sprintf("This location %s has already existed RFID %s", $locCode, $locObj->rfid);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //update loction RFID
            $locObj->rfid = $locRFID;
            $locObj->save();

            DB::commit();
            $msg = sprintf("Update location rfid %s to location code %s successfully.", $locRFID, $locCode);
            $data = [
                'data'    => [
                    'loc_code' => $locCode,
                    'loc_rfid' => $locRFID,
                ],
                'message' => $msg,
                'status'  => true,
            ];

            return new Response($data, 200, [], null);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function updateLocationAllowOverrideRFID($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        $requireFieldsValidate = $this->locationUpdateRFIDValidator->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        $locCode = array_get($input, 'loc_code', null);
        $locRFID = array_get($input, 'loc_rfid', null);

        /*
           * start logs
           */

        $url = "/whs/{$whsId}/location/override/update-rfid'";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Update location RFID by location code'
        ]);
        /*
        * end logs
        */

        //validate location RFID
        $locRFIDValid = new RFIDValidate($locRFID, RFIDValidate::TYPE_LOCATION, $whsId);
        if (!$locRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $locRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            DB::beginTransaction();
            //get location with loc_code
            $locObj = $this->locationModel->getLocByLocCode($locCode, $whsId);

            if (empty($locObj)) {
                $msg = sprintf("The location code: %s doesn't exist", $locCode);
                $locObj = $this->locationModel->getFirstWhere(['loc_code' => $locCode]);
                if ($locObj) {
                    $msg = sprintf("The location code: %s doesn't belong to warehouse", $locCode);
                }
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $locExistRFID = $this->locationModel->getFirstWhere(['rfid' => $locRFID]);
            if ($locExistRFID && $locExistRFID->loc_code != $locObj->loc_code) {
                $msg = sprintf("The location RFID %s was defined for location has loc code: %s", $locRFID,
                    $locExistRFID->loc_code);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //update loction RFID
            $locObj->rfid = $locRFID;
            $locObj->save();

            DB::commit();
            $msg = sprintf("Update location rfid %s to location code %s successfully.", $locRFID, $locCode);
            $data = [
                'data'    => [
                    'loc_code' => $locCode,
                    'loc_rfid' => $locRFID,
                ],
                'message' => $msg,
                'status'  => true,
            ];

            return new Response($data, 200, [], null);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function getLocationType($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
           * start logs
           */

        $url = "/whs/{$whsId}/locations/type";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Get location type'
        ]);
        /*
        * end logs
        */

        try {
            $locType = (new LocationTypeModel())->getLocTypeList(10);

            $msg = "Get location type successfully.";
            $data = [
                'data'    => [
                    'loc-type' => $locType
                ],
                'message' => $msg,
                'status'  => true,
            ];

            return new Response($data, 200, [], null);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function getLocationList($whsId, Request $request)
    {
        $input = $request->getQueryParams();

        $locTypeId = array_get($input, 'loc_type_id', null);
        /*
           * start logs
           */

        $url = "/whs/{$whsId}/locations";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Get location list'
        ]);

        /*
        * end logs
        */

        try {

            $locObj = $this->locationModel->getLocationList($locTypeId, 20);
            $msg = "Get location list successfully.";
            $data = [
                'data'    => [
                    'loc-list' => $locObj
                ],
                'message' => $msg,
                'status'  => true,
            ];

            return new Response($data, 200, [], null);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function getLocationListByRFID($whsId, $rfid, Request $request)
    {

        $url = "/whs/{$whsId}/location/{$rfid}";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Get location by RFID'
        ]);

        try {
            $locObj = $this->locationModel->getLocByRfId($whsId, $rfid);
            $msg = "Get location successfully.";
            $data = [
                'data'    => [
                    'location' => $locObj
                ],
                'message' => $msg,
                'status'  => true,
            ];

            return new Response($data, 200, [], null);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function checkLocationListByLocCode($whsId, $locCode, Request $request)
    {

        $url = "/whs/{$whsId}/location/{$locCode}";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Check location by loc code'
        ]);

        try {
            $locObj = $this->locationModel->getLocByLocCode($locCode, $whsId);
            if ($locObj) {
                $msg = sprintf("Location %s is existing.", $locCode);
                $data = [
                    'data'    => [
                        'location' => true,
                    ],
                    'message' => $msg,
                    'status'  => true,
                ];

                return new Response($data, 200, [], null);
            }

            $msg = sprintf("Location %s doesn't exist.", $locCode);
            $data = [
                'data'    => [
                    'location' => false,
                ],
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function setNullLocationRFID($whsId, $locCode, Request $request)
    {

        $url = "/whs/$whsId/location/$locCode/set-null-rfid";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Check location by loc code'
        ]);

        try {
            $locObj = $this->locationModel->getLocByLocCode($locCode, $whsId);

            if (!$locObj) {
                $msg = sprintf("Location %s doesn't exist.", $locCode);
                $data = [
                    'data'    => [
                        'location' => false,
                    ],
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $locObj->rfid = null;
            $locObj->save();

            $msg = sprintf("Set null location code %s successfully.", $locCode);
            $data = [
                'data'    => [
                    'location' => $locCode,
                ],
                'message' => $msg,
                'status'  => true,
            ];

            return new Response($data, 200, [], null);

        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    /**
     * @param $whsId
     * @param $rfid
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function getLocByRfid($whsId, $rfid, Request $request)
    {
        $input = $request->getParsedBody();

        //validate location RFID
        $locRFID = $rfid;
        $locRFIDValid = new RFIDValidate($locRFID, RFIDValidate::TYPE_LOCATION, $whsId);
        if (!$locRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $locRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $location = $this->locationModel->getLocByRfId($whsId, $rfid);

        if ($location) {
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'] = [
                'loc_code' => $location->loc_code,
            ];
            $msg['message'] = [
                'status_code' => 1,
                'msg'         => "Successfully!",
            ];

            return $msg;
        } else {
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $errorMsg = sprintf("The RFID %s doesn't exist!", $rfid);
            $msg['message'] = [
                'status_code' => -1,
                'msg'         => $errorMsg,
            ];

            return $msg;
        }
    }

    /**
     * @param $whsId
     * @param $locId
     * @param Request $request
     *
     * @return Response
     */
    public function showLocationDetail(
        $whsId,
        $locId,
        Request $request
    ) {
        $location = $this->locationModel->getLocById($whsId, $locId);

        if ($location) {
            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'] = $location;
            $msg['message'] = [
                'status_code' => 1,
                'msg'         => sprintf('Successfully!')
            ];

            return $msg;

        } else {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'] = [
                'status_code' => -1,
                'msg'         => sprintf("The location doesn't exist!")
            ];

            return $msg;

        }
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response
     */
    public function getLocationWithPalletNoRFID(
        $whsId,
        Request $request
    ) {
        $locationList = $this->locationModel->getLocationWithPalletNoRFID($whsId);

        if ($locationList->toArray()) {
            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['data'] = $locationList;
            $msg['message'] = [
                'status_code' => 1,
                'msg'         => sprintf('Successfully!')
            ];

            return $msg;

        } else {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'] = [
                'status_code' => -1,
                'msg'         => sprintf("No data!")
            ];

            return $msg;

        }
    }


}
