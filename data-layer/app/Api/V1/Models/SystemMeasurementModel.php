<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 29-Jun-16
 * Time: 10:15
 */

namespace App\Api\V1\Models;

use App\SystemMeasurement;
use App\Utils\SelStr;
use App\Utils\SelArr;

class SystemMeasurementModel extends AbstractModel
{
    public function __construct(SystemMeasurement $model = null)
    {
        $this->model = ($model) ?: new SystemMeasurement();
    }

    public function deleteSystemMeasurement($systemMeasurementId)
    {
        return $this->model
            ->where('sys_mea_code', $systemMeasurementId)
            ->delete();
    }

    public function deleteMassSystemMeasurement(array $systemMeasurementIds)
    {
        return $this->model
            ->whereIn('sys_mea_code', $systemMeasurementIds)
            ->delete();
    }

    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'sys_mea_name' || $key === 'sys_mea_code' || $key === 'sys_mea_desc') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        //Implement sort builder for api
        //usage: ?sort[name]=desc&sort[title]=asc
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }
}