
<!DOCTYPE html>
<html>
<head>
	<title>Invoice Tracking</title>
</head>
<body>
	<h2>Invoice Notification</h2>
	<p>Invoice # : {!! $header->inv_num !!}</p>
	<p>Status : {!! $header->inv_sts_name !!}</p>
	<p>Link : {!! env('URL_INVOICE_INFO_FE', '') . $header->inv_id !!}</p>
	@if ($content)
	<p>Content : {!! $content !!}</p>
	@endif
</body>
</html>