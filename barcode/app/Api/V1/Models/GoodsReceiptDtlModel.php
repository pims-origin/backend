<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class GoodsReceiptDtlModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new GoodsReceiptDetail();
    }
}
