<?php

namespace App\Api\V2\WavePickDetails\WvDtlList\Controllers;

use App\Api\V2\WavePickDetails\WvDtlList\Models\OrderHdrModel;
use App\Api\V2\WavePickDetails\WvDtlList\Models\WavePickDtlLocModel;
use App\Api\V2\WavePickDetails\WvDtlList\Models\WavePickDtlModel;
use App\Api\V2\WavePickDetails\WvDtlList\Models\WavePickHdrModel;
use App\Api\V2\WavePickDetails\WvDtlList\Transformers\WavePickInfoTransformer;
use App\Api\V2\WavePickDetails\WvDtlList\Validators\WaveDtlListValidator;
use Illuminate\Support\Facades\DB;
use Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Maatwebsite\Excel\Facades\Excel;

class WavePickDetailController extends AbstractController
{
    /**
     * @var Wave pick header
     */
    protected $wvHdrModel;

    /**
     * @var Wave pick detail
     */
    protected $wvDtlModel;

    /**
     * @var Order Header
     */
    protected $odrHdrModel;


    /**
     * WavePick Dtl Loc
     */
    protected $wvDtlLocModel;
    /**
     * OrderPickingController constructor.
     */
    public function __construct()
    {
        $this->wvHdrModel  = new WavePickHdrModel();
        $this->wvDtlModel  = new WavePickDtlModel();
        $this->odrHdrModel = new OrderHdrModel();
        $this->wvDtlLocModel = new WavePickDtlLocModel();
    }

    /**
     * Detail wave pick
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function detailWavePick($whsId, Request $request)
    {
        $input = $request->getQueryParams();
        (new WaveDtlListValidator())->validate($input);

        $wvId = array_get($input, 'wv_id');
        try {
            //Load wave pick header
            $dataWvDtl = $this->wvHdrModel->getWvHdrInfo($wvId, $whsId);
            // Load order_num
            $odrHdrModels = $this->odrHdrModel->getOrderHdrNumsByWvId($wvId, $whsId)->toArray();

            if ($dataWvDtl) {
                $dataWvDtl->odr_num = implode(", ", array_unique(array_column($odrHdrModels, 'odr_num')));
                $wvDtlObjs = $this->wvDtlModel->loadWvDetail($wvId);
                foreach ($wvDtlObjs as $wvDtlObj) {
                    $sqlSum = "sum(odr_cartons.piece_qty) as picked_qty";
                    if ($wvDtlObj->wv_dtl_sts == 'NW') {
                        $actLocs = json_decode(object_get($this->wvDtlLocModel->getFirstBy('wv_dtl_id',$wvDtlObj->wv_dtl_id), 'act_loc_ids'));
                    } else {
                        $actLocs = DB::table('odr_cartons')
                            ->leftJoin('cartons', 'cartons.ctn_id', '=', 'odr_cartons.ctn_id')
                            ->select([
                                'odr_cartons.loc_id as act_loc_id',
                                'odr_cartons.loc_code as act_loc',
                                DB::raw($sqlSum),
                                DB::raw("IF(cartons.lpn_carton = '', null, cartons.lpn_carton) as lpn")
                            ])
                            ->where('odr_cartons.deleted', 0)
                            ->where('odr_cartons.wv_dtl_id', $wvDtlObj->wv_dtl_id)
                            ->groupBy('odr_cartons.loc_id', 'lpn')
                            ->get();
                    }
                    $wvDtlObj->act_loc_ids = !empty($actLocs) ? array_values($actLocs) : null;
                }
                $dataWvDtl->wvDetail = $wvDtlObjs;
            }

            return $this->response->item($dataWvDtl, new WavePickInfoTransformer());
        } catch (Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * this function is used to export wavepick detailinto excel file
     *
     * @param [type] $whsId
     * @param Request $request
     */
    public function exportWavePickDetail( $whsId,  Request $request ) {

        $input      = $request->getQueryParams();
        $wvId = array_get($input, 'wv_id');
       
        try {
            //Load wave pick header
            $dataWvDtl = $this->wvHdrModel->getWvHdrInfo($wvId, $whsId);
            // Load order_num
            $odrHdrModels = $this->odrHdrModel->getOrderHdrNumsByWvId($wvId, $whsId)->toArray();

            if ($dataWvDtl) {
                $dataWvDtl->odr_num = implode(", ", array_unique(array_column($odrHdrModels, 'odr_num')));
                $dataWvDtl->cus_po = implode(", ", array_unique(array_column($odrHdrModels, 'cus_po')));
                $dataWvDtl->cus_odr_num = implode(", ", array_unique(array_column($odrHdrModels, 'cus_odr_num')));
                
                $wvDtlObjs = $this->wvDtlModel->loadWvDetail($wvId);
                foreach ($wvDtlObjs as $wvDtlObj) {
                    $sqlSumAct = "sum(piece_qty) as picked_qty";
                    $sqlCountAct = "COUNT(ctn_id) as picked_count";

                    $actLocs = json_decode(object_get($this->wvDtlLocModel->getFirstBy('wv_dtl_id',$wvDtlObj->wv_dtl_id), 'act_loc_ids'));
                    $sugLocs = object_get($this->wvDtlLocModel->getFirstBy('wv_dtl_id',$wvDtlObj->wv_dtl_id), 'sug_loc_ids');
                    $sugLocsArr = explode(",", $sugLocs);

                    $sqlSum = "sum(c.piece_remain) as picked_qty";
                    $sqlCount = "COUNT(c.ctn_id) as picked_count";

                    $sugLocsFromWvDtl = DB::table('cartons as c')
                                ->select([
                                    'c.loc_id',
                                    'c.item_id',
                                    'c.loc_code as sug_loc',
                                    'c.sku',
                                    DB::raw($sqlCount),
                                    DB::raw($sqlSum),
                                ])
                                ->where('c.deleted', 0)
                                ->where('c.item_id', $wvDtlObj->item_id)
                                ->whereIn('c.loc_id', $sugLocsArr)
                                ->groupBy('c.loc_id','c.item_id');
                    
                    $sqlFromOdrCartons = DB::table('odr_cartons')
                                        ->select([
                                            'loc_id',
                                            'item_id',
                                            'loc_code as sug_loc',
                                            'sku',
                                            DB::raw($sqlCountAct),
                                            DB::raw($sqlSumAct),
                                        ])
                                        ->where('deleted', 0)
                                        ->whereIn('loc_id', $sugLocsArr)
                                       ->where('item_id', $wvDtlObj->item_id)
                                       ->where('updated_at','>=', $dataWvDtl->created_at)
                                        ->groupBy('loc_id','item_id');

                    $sql =     DB::table(DB::raw("({$sugLocsFromWvDtl->toSql()}) as A"))
                                    ->select([
                                        'A.*',
                                    ])->union( DB::table(DB::raw("({$sqlFromOdrCartons->toSql()}) as B"))
                                        ->select([
                                            'B.*',
                                        ]));
                                    
                    $sugLocs =  DB::table(DB::raw("({$sql->toSql()}) as D"))
                                ->select([
                                    'D.loc_id',
                                    'D.item_id',
                                    'D.sug_loc',
                                    'D.sku',
                                    DB::raw('SUM(D.picked_count) AS picked_count'),
                                    DB::raw('SUM(D.picked_qty) AS picked_qty')
                                ])
                                ->join('location', 'D.loc_id', '=', 'location.loc_id')
                                ->groupBy('D.loc_id','D.item_id')
                                ->orderBy('location.aisle', 'ASC')
                                ->orderBy('location.level', 'ASC')
                                ->orderBy('location.row', 'ASC')
                                ->orderBy('location.bin', 'ASC')
                                ->setBindings([0, $wvDtlObj->item_id, $sugLocsArr, 0, $sugLocsArr, $wvDtlObj->item_id, date_timestamp_get($dataWvDtl->created_at)])
                                ->get();

                    $wvDtlObj->sug_loc_ids = !empty($sugLocs) ? array_values($sugLocs) : null;

                    $actLocs = DB::table('odr_cartons')
                    ->select([
                        'loc_id as act_loc_id',
                        'loc_code as act_loc',
                        DB::raw($sqlCountAct),
                        DB::raw($sqlSumAct),
                    ])
                    ->where('deleted', 0)
                    ->where('wv_dtl_id', $wvDtlObj->wv_dtl_id)
                    ->groupBy('loc_id','wv_dtl_id')
                    ->get();

                    $wvDtlObj->act_loc_ids = !empty($actLocs) ? array_values($actLocs) : null;
                }
                $dataWvDtl->wvDetail = $wvDtlObjs;
            }

            $ext = 'xls';
            Excel::create('WavePickDetailReport', function ($excel) use (
                &$dataWvDtl
                ) {
                // Set sheet 1
                $excel->sheet('Wave pick', function ($sheet) use (
                    &$dataWvDtl
                ) {
                    //get total of items
                    $totalItems = $dataWvDtl->wvDetail->count();
                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'Arial',
                            'size' => 11,
                            'bold' => false,
                        ),
                    ))
                        ->row(1, array(
                            'WV#',
                            'Cus Order#',
                            'Cus PO',
                            'SKU',
                            'Description',
                            'Lot',
                            'Pack',
                            'UOM',
                            'Allocated CTNS',
                            'Allocated QTY',
                        ))
                        ->cells('A1:J1', function ($cells) {
                            $cells->setFontSize(11)
                                ->setFontWeight('bold');
                        });
                        for($i = 0; $i< $totalItems; $i++){
                            $sheet->row(2+$i, array(
                                $dataWvDtl['wv_num'],
                                $dataWvDtl->cus_odr_num,
                                $dataWvDtl->cus_po,//Cus PO
                                $dataWvDtl->wvDetail[$i]['sku'],
                                '',//Description
                                $dataWvDtl->wvDetail[$i]['lot'],
                                $dataWvDtl->wvDetail[$i]['pack_size'],
                                $dataWvDtl->wvDetail[$i]['uom_name'],//UOM  
                                $dataWvDtl->wvDetail[$i]['ctn_qty'],
                                $dataWvDtl->wvDetail[$i]['piece_qty'],
                            ));
                        }
                });


                // Set sheet 2
                 $excel->sheet('Before picking', function ($sheet) use (
                    &$dataWvDtl,
                    &$sugLocs
                ) {
                    //get total of items
                    $totalItems = $dataWvDtl->wvDetail->count();

                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'Arial',
                            'size' => 11,
                            'bold' => false,
                        ),
                    ))
                        ->row(1, array(
                            'WV#',
                            'SKU',
                            'Description',
                            'Lot',
                            'Pack',
                            'UOM',
                            'Suggested Location',
                            'CTNS',
                            'QTY',
                        ))
                        ->cells('A1:J1', function ($cells) {
                            $cells->setFontSize(11)
                                ->setFontWeight('bold');
                        });
                        $currentRow = 1;
                        for ($i = 0; $i< $totalItems; $i++) {
                            $totalSuggestedLocs = count($dataWvDtl->wvDetail[$i]['sug_loc_ids']);
                            if($totalSuggestedLocs > 0) {
                                for($j = 0; $j < $totalSuggestedLocs; $j ++ )
                                {
                                    $sheet->row($currentRow + 1 + $j, array(
                                        $dataWvDtl['wv_num'],
                                        $dataWvDtl->wvDetail[$i]['sku'],
                                        '',//Description
                                        $dataWvDtl->wvDetail[$i]['lot'],
                                        $dataWvDtl->wvDetail[$i]['pack_size'],
                                        $dataWvDtl->wvDetail[$i]['uom_name'],//UOM  
                                        $dataWvDtl->wvDetail[$i]['sug_loc_ids'][$j]['sug_loc'],//Suggested Location
                                        $dataWvDtl->wvDetail[$i]['sug_loc_ids'][$j]['picked_count'],
                                        $dataWvDtl->wvDetail[$i]['sug_loc_ids'][$j]['picked_qty'],
                                        ));
                                }
                                
                            } else {
                                $sheet->row($currentRow + 1 , array(
                                    $dataWvDtl['wv_num'],
                                    $dataWvDtl->wvDetail[$i]['sku'],
                                    '',//Description
                                    $dataWvDtl->wvDetail[$i]['lot'],
                                    $dataWvDtl->wvDetail[$i]['pack_size'],
                                    $dataWvDtl->wvDetail[$i]['uom_name'],//UOM  
                                    $dataWvDtl->wvDetail[$i]['primary_loc'],//Suggested Location
                                    $dataWvDtl->wvDetail[$i]['ctn_qty'],
                                    $dataWvDtl->wvDetail[$i]['piece_qty'],
                                ));
                                $currentRow +=1;
                            }
                            //increase current row
                            $currentRow += $totalSuggestedLocs;
                        }
                });

                // Set sheet 3
                $excel->sheet('After picked', function ($sheet) use (
                    &$dataWvDtl
                ) {
                    //get total of items
                    $totalItems = $dataWvDtl->wvDetail->count();
                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'Arial',
                            'size' => 11,
                            'bold' => false,
                        ),
                    ))
                        ->row(1, array(
                            'WV#',
                            'ORD#',
                            'Cus Order#',
                            'Cus PO#',
                            'SKU',
                            'Description',
                            'Lot',
                            'UOM',
                            'Pack',
                            'Picked Location',
                            'Picked CTNS',
                            'Picked QTY',
                            'Picked At',
                            'Picked By',
                        ))
                        ->cells('A1:J1', function ($cells) {
                            $cells->setFontSize(11)
                                ->setFontWeight('bold');
                        });

                        $currentRow = 1;
                        for ($i = 0; $i< $totalItems; $i++) {
                            if(!empty($dataWvDtl->wvDetail[$i]['act_loc_ids'])) {
                                //get totalOfActualLoc
                                $totalOfActualLoc = count($dataWvDtl->wvDetail[$i]['act_loc_ids']);
                                for ($j = 0; $j< $totalOfActualLoc; $j++) {
                                    $sheet->row($currentRow + 1 + $j, array(
                                        $dataWvDtl['wv_num'],
                                        $dataWvDtl['odr_num'],
                                        $dataWvDtl->cus_odr_num,//Cus Order#
                                        $dataWvDtl->cus_po,//Cus PO#
                                        $dataWvDtl->wvDetail[$i]['sku'],
                                        '',//Description
                                        $dataWvDtl->wvDetail[$i]['lot'],
                                        $dataWvDtl->wvDetail[$i]['uom_name'],//UOM  
                                        $dataWvDtl->wvDetail[$i]['pack_size'],
                                        $dataWvDtl->wvDetail[$i]['act_loc_ids'][$j]['act_loc'],//Picked Location  
                                        $dataWvDtl->wvDetail[$i]['act_loc_ids'][$j]['picked_count'],//Picked CTNS  
                                        $dataWvDtl->wvDetail[$i]['act_loc_ids'][$j]['picked_qty'],//Picked QTY  
                                        $dataWvDtl->wvDetail[$i]['updated_at'],//Picked At  
                                        $dataWvDtl->wvDetail[$i]['picker_name'],//Picked By  
                                    ));
                                }
                                //increase current row
                                $currentRow += $totalOfActualLoc;
                            }
                        }
                });

            })
            ->download($ext);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));

    }


}
