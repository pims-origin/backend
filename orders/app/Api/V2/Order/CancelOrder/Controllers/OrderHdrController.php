<?php

namespace App\Api\V2\Order\CancelOrder\Controllers;

use App\Api\V2\Order\CancelOrder\Models\AbstractModel;
use App\Api\V2\Order\CancelOrder\Models\CartonModel;
use App\Api\V2\Order\CancelOrder\Models\EventTrackingModel;
use App\Api\V2\Order\CancelOrder\Models\InventorySummaryModel;
use App\Api\V2\Order\CancelOrder\Models\OrderCartonModel;
use App\Api\V2\Order\CancelOrder\Models\OrderDtlModel;
use App\Api\V2\Order\CancelOrder\Models\OrderHdrModel;
use App\Api\V2\Order\CancelOrder\Models\WavepickDtlModel;
use App\Api\V2\Order\CancelOrder\Models\WavepickHdrModel;
use App\Api\V2\Order\CancelOrder\Traits\OrderHdrControllerTrait;
use App\Api\V2\Order\CancelOrder\Validators\OrderCancelValidator;
use App\Jobs\CanceledOrderJob;
use Dingo\Api\Http\Response;
use Illuminate\Http\Request as IRequest;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use TCPDF;
use Wms2\UserInfo\Data;
use mPDF;

/**
 * Class OrderHdrController
 *
 * @package App\Api\V2\Order\CancelOrder\Controllers
 */
class OrderHdrController extends AbstractController
{
    use OrderHdrControllerTrait;

    protected $orderHdrModel;
    protected $orderDtlModel;
    protected $inventorySummaryModel;
    protected $eventTrackingModel;
    protected $wavepickHdrModel;
    protected $wavepickDtlModel;

    /**
     * OrderHdrController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param EventTrackingModel $eventTrackingModel
     * @param WavepickHdrModel $wavepickHdrModel
     * @param WavepickDtlModel $wavepickDtlModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        InventorySummaryModel $inventorySummaryModel,
        EventTrackingModel $eventTrackingModel,
        WavepickHdrModel $wavepickHdrModel,
        WavepickDtlModel $wavepickDtlModel,
        OrderCartonModel $orderCartonModel
    ) {

        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->orderCartonModel = $orderCartonModel;
        $this->wavepickHdrModel = $wavepickHdrModel;
        $this->wavepickDtlModel = $wavepickDtlModel;
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return array|void
     */
    public function cancel($whsId, Request $request)
    {
        $input = $request->getQueryParams();

        (new OrderCancelValidator())->validate($input);

        $odrHdrId = array_get($input, 'odr_id');

        $actCancelDt = array_get($input, 'act_cancel_dt', '') ?
            array_get($input, 'act_cancel_dt', '') : date("m/d/Y", time());

        $odrChk = $this->orderHdrModel->byId($odrHdrId);
        $parentOdrNum = substr($odrChk->odr_num, 0, 14);

        $odrIds = [];
        $subOdrIds = [];
        if ($odrChk->odr_sts == 'NW') {
            $subOdrIds[] = $odrHdrId;
        } else {
            $odrIds[] = $odrHdrId;
        }

        if ($odrChk->odr_type === 'BAC'){
           // $odrIds =  array_filter($odrIds, function($value) use($odrHdrId){
           //      return $value == $odrHdrId;
           // });

        } else {
            // get back order != new
            $odrIdTmp = \DB::table('odr_hdr')
                // ->where('odr_num', 'LIKE', $parentOdrNum . '%')
                ->where('deleted', 0)
                ->where('odr_type', 'BAC')
                ->where('org_odr_id', $odrHdrId)
                ->where('odr_sts', '!=', 'NW')
                ->pluck('odr_id');
            $odrIds = array_merge($odrIds, $odrIdTmp);

            // get back order new
            $subOdrIdsTmp= \DB::table('odr_hdr')
                // ->where('odr_num', 'LIKE', $parentOdrNum . '%')
                ->where('deleted', 0)
                ->where('odr_type', 'BAC')
                ->where('org_odr_id', $odrHdrId)
                ->where('odr_sts', 'NW')
                ->pluck('odr_id');
            $subOdrIds = array_merge($subOdrIds, $subOdrIdsTmp);
        }

        $evtTracking = [];

        $userId = Data::getCurrentUserId();
        try {
            DB::beginTransaction();
            $error = true;
            $cancelOdr = [];
            if ($odrIds) {


                foreach ($odrIds as $idx => $odrId) {
                    // WMS2-5062 - [Outbound - Putback][WEB] Show error message if it exist RFID carton
                    $this->_executionOrderRfidCarton($odrId);

                    $odrObj = $this->orderHdrModel->byId($odrId, ['details']);

                    /* WMS2-4477 START we will allow to cancel order which is picking */
                    // order is picking
                    if (object_get($odrObj, 'odr_sts', '') == 'PK') {

                        $notAssignCarton2Pallet = false;
                        $odrDtls = $odrObj->details()->where('lot', '!=', 'Any')->whereNotNull('wv_id')->get();
                        $ctnPicked = 0;
                        $returnIvtOrdDtl = [];
                        foreach ($odrDtls as $odrDtl) {
                            $odrDtlId = object_get($odrDtl, 'odr_dtl_id');
                            $itemId = object_get($odrDtl, 'item_id');
                            $lot = object_get($odrDtl, 'lot');

                            $wvdtl = DB::table('wv_dtl')
                                ->where('item_id', $itemId)
                                ->where('wv_id', object_get($odrDtl, 'wv_id'))
                                ->where('lot', $lot)
                                ->where('pack_size', object_get($odrDtl, 'pack'))
                                ->where('deleted', 0)
                                ->where('wv_dtl_sts', '!=', 'NW')
                                // ->select(DB::raw('SUM(ctn_qty) as ctn_qty_ttl'))->first();
                                ->select(DB::raw('SUM(act_piece_qty)'))->first();

                            // $ctnPicked += array_get($wvdtl, 'ctn_qty_ttl', 0);
                            // if($ctnPicked > 0) {
                            //     $notAssignCarton2Pallet = true;
                            // }
                            $actQty = array_get($wvdtl, 'act_piece_qty', 0);
                            $returnIvtCtnQty = object_get($odrDtl, 'alloc_qty', 0) - $actQty;
                            if ($returnIvtCtnQty > 0) {
                                $this->inventorySummaryModel->updateAllocInvtWhenOdrPicking(
                                    $whsId, $itemId, $lot, $returnIvtCtnQty
                                );
                            }
                        }
                        // if($notAssignCarton2Pallet) {
                        //     $msg = sprintf('All cartons should be picked and assign to this order first before cancelling');
                        //     return $this->response->errorBadRequest($msg);
                        // }
                    }
                    /* WMS2-4477 END we will allow to cancel order which is picking */

                    if ($this->processCancel($odrObj)) {
                        $error = false;
                        $cancelOdr[] = $odrId;

                        // Remove ShipId
                        $this->removeShipId($odrId);
                    } else {
                        continue;
                    }

                    //event tracking
                    $evtTracking[] = [
                        'whs_id'     => $odrObj->whs_id,
                        'cus_id'     => $odrObj->cus_id,
                        'owner'      => $odrObj->odr_num,
                        'evt_code'   => Status::getByKey("event", "ORDER-CANCELED"),
                        'trans_num'  => $odrObj->odr_num,
                        'info'       => sprintf(Status::getByKey("event-info", "ORDER-CANCELED"), $odrObj->odr_num),
                        'created_at' => time(),
                        'created_by' => $userId
                    ];
                    //update status cancel
                    $this->orderHdrModel->getModel()
                        ->where('odr_id', $odrId)
                        ->update([
                            'odr_sts'       => 'CC',
                            'wv_id'         => null,
                            'act_cancel_dt' => strtotime($actCancelDt),
                            'updated_by'    => $userId,
                            'updated_at'    => time()
                        ]);
                    if (!empty($odrObj->wv_id) && $odrObj->odr_sts != "CC") {
                        $this->changeStatusWavepick($odrObj->wv_id, $odrObj->odr_sts, $cancelOdr);
                    }
                }


                if ($error) {
                    return $this->response->errorBadRequest('Order has been shipped or finalized BOL cannot be cancelled!');
                }
            } else {
                foreach ($subOdrIds as $idx => $NewOdrId) {
                    $NewOdrObj = $this->orderHdrModel->byId($NewOdrId, ['details']);
                    //event tracking
                    $evtTracking[] = [
                        'whs_id'     => $NewOdrObj->whs_id,
                        'cus_id'     => $NewOdrObj->cus_id,
                        'owner'      => $NewOdrObj->odr_num,
                        'evt_code'   => Status::getByKey("event", "ORDER-CANCELED"),
                        'trans_num'  => $NewOdrObj->odr_num,
                        'info'       => sprintf(Status::getByKey("event-info", "ORDER-CANCELED"), $NewOdrObj->odr_num),
                        'created_at' => time(),
                        'created_by' => $userId
                    ];
                }
            }

            // update order cartons
            $this->orderCartonModel->getModel()
                ->whereIn('odr_hdr_id', $cancelOdr)
                ->update([
//                    'odr_hdr_id' => null,
//                    'odr_dtl_id' => null,
//                    'odr_num'    => null,
                    'updated_by' => $userId,
                    'updated_at' => time()
                ]);

            //update status cancel for subOdrIds
            $this->orderHdrModel->getModel()
                ->whereIn('odr_id', $subOdrIds)
                ->update([
                    'odr_sts'       => 'CC',
                    'wv_id'         => null,
                    'act_cancel_dt' => strtotime($actCancelDt),
                    'updated_by'    => $userId,
                    'updated_at'    => time()
                ]);
            //update status cancel for odr_dtl
            $this->orderDtlModel->getModel()
                ->whereIn('odr_id', $subOdrIds)
                ->update([
                    'itm_sts'       => 'CC',
                    'updated_by'    => $userId,
                    'updated_at'    => time()
                ]);

            //Event Tracking
            DB::table('evt_tracking')->insert($evtTracking);

            $this->updateInvetorySmrOdr($odrHdrId);
            $this->updateInvetoryReportOdr($odrHdrId);
            DB::commit();

            // Send data to IMS
            dispatch(new CanceledOrderJob($odrHdrId));

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return ['data' => 'Order canceled successfully!'];
    }

    public function cancelRMA($whsId, Request $request)
    {
        $input = $request->getQueryParams();

        (new OrderCancelValidator())->validate($input);

        //+ Clone source from cancel order
        //+ Not update anything in wavepick
        //+ Inventory: reduce picked_qty and ttl by total piece qty of order
        //+ cartons and odr_cartons: update ctn_sts = "PB" (PUTBACK)
        //+ Update odr_hdr.odr_sts = "RMA"

        $userId = Data::getCurrentUserId();
        $odrHdrId = (int)$input['odr_id'];
        $actCancelDt = array_get($input, 'act_cancel_dt');
        $odrHdr = OrderHdr::find($odrHdrId);
        $evtTracking = [];
        $cancelOdr = [];

        $notValidStatuses = [
            Status::getByValue("New", "ORDER-STATUS"),
            Status::getByValue("Allocated", "ORDER-STATUS"),
            Status::getByValue("Picking", "ORDER-STATUS"),
            Status::getByValue("Shipped", "ORDER-STATUS"),
            Status::getByValue("Canceled", "ORDER-STATUS"),
        ];
        if (in_array($odrHdr->odr_sts, $notValidStatuses)){
            return $this->response->errorBadRequest('Order status is invalid for cancel rma');
        }

        $parentOdrNum = substr($odrHdr->odr_num, 0, 14);
        $odrIds = OrderHdr::where('odr_num', 'LIKE', $parentOdrNum . '%')
                        ->where('odr_sts', '!=', 'NW')
                        ->pluck('odr_id');

        $subOdrIds = OrderHdr::where('odr_num', 'LIKE', $parentOdrNum . '%')
                            ->where('odr_sts', 'NW')
                            ->pluck('odr_id');

        if ($odrHdr->odr_type === 'BAC'){
           $odrIds =  $odrIds->filter(function($value) use($odrHdrId){
                return $value == $odrHdrId;
           });
        }

        try{
            DB::beginTransaction();

            if ($odrIds) {
                foreach ($odrIds as $idx => $odrHdrId) {
                    //Insert Data Into Order Hdr Meta If Order Has Carton With RFID
                    $this->_executionOrderRfidCarton($odrHdrId);

                    //Update Inventory Summary
                    $odrHdr = OrderHdr::with('details')->find($odrHdrId);
                    foreach($odrHdr->details as $odrDtl){
                        $invSum = InventorySummary::where('whs_id', $odrDtl->whs_id)
                                        ->where('cus_id', $odrDtl->cus_id)
                                        ->where('item_id', $odrDtl->item_id)
                                        ->where('lot', $odrDtl->lot)
                                        ->first();
                        $ttl = $invSum->ttl - $odrDtl->picked_qty;
                        $ttl = $ttl >= 0 ? $ttl : 0;

                        $pickedQty = $invSum->picked_qty - $odrDtl->picked_qty;
                        $pickedQty = $pickedQty >= 0 ? $pickedQty : 0;
                        $invSum->update([
                            'ttl' => $ttl,
                            'picked_qty' => $pickedQty
                        ]);
                    }


                    //update order hdr
                    $odrHdr->update([
                        'odr_sts'       => 'RMA',
                        // 'wv_id'         => null,
                        'act_cancel_dt' => empty($actCancelDt) ? time() : strtotime($actCancelDt),
                        'updated_by'    => $userId,
                        'updated_at'    => time()
                    ]);

                    //event tracking
                    $evtTracking[] = [
                        'whs_id'     => $odrHdr->whs_id,
                        'cus_id'     => $odrHdr->cus_id,
                        'owner'      => $odrHdr->odr_num,
                        'evt_code'   => Status::getByKey("event", "ORDER-CANCELED-RMA"),
                        'trans_num'  => $odrHdr->odr_num,
                        'info'       => sprintf(Status::getByKey("event-info", "ORDER-CANCELED-RMA"), $odrHdr->odr_num),
                        'created_at' => time(),
                        'created_by' => $userId
                    ];
                }
            }

            if (!$odrIds){
               foreach ($subOdrIds as $idx => $odrHdrId) {
                    $odrHdr = OrderHdr::find($odrHdrId);

                    //event tracking
                    $evtTracking[] = [
                        'whs_id'     => $odrHdr->whs_id,
                        'cus_id'     => $odrHdr->cus_id,
                        'owner'      => $odrHdr->odr_num,
                        'evt_code'   => Status::getByKey("event", "ORDER-CANCELED-RMA"),
                        'trans_num'  => $odrHdr->odr_num,
                        'info'       => sprintf(Status::getByKey("event-info", "ORDER-CANCELED-RMA"), $odrHdr->odr_num),
                        'created_at' => time(),
                        'created_by' => $userId
                    ];
                }
            }

            // update order cartons
            $this->orderCartonModel->getModel()
                ->whereIn('odr_hdr_id', $cancelOdr)
                ->update([
                    'ctn_sts'    => 'PB',
                    'updated_by' => $userId,
                    'updated_at' => time()
                ]);

            // update cartons
            $cartonIds = OrderCarton::whereIn('odr_hdr_id', $odrIds)
                                ->pluck('ctn_id');
            Carton::whereIn('ctn_id', $cartonIds)
                    ->update([
                        'ctn_sts'   => 'PB',
                        'updated_by' => $userId,
                        'updated_at' => time()
                    ]);


            //update status cancel for subOdrIds
            $this->orderHdrModel->getModel()
                ->whereIn('odr_id', $subOdrIds)
                ->update([
                    'odr_sts'       => 'RMA',
                    'act_cancel_dt' => time(),
                    'updated_by'    => $userId,
                    'updated_at'    => time()
                ]);

            //update status cancel for odr_dtl
            $this->orderDtlModel->getModel()
                ->whereIn('odr_id', $subOdrIds)
                ->update([
                    'itm_sts'       => 'CC',
                    'updated_by'    => $userId,
                    'updated_at'    => time()
                ]);

            DB::table('evt_tracking')->insert($evtTracking);
            DB::commit();
        }catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }

        return ['data' => 'Order canceled rma successfully!'];
    }

    private function _executionOrderRfidCarton($odrId)
    {
        $chkOrderHasRfidCarton = $this->orderCartonModel->getModel()
            ->where('odr_hdr_id', $odrId)
            ->whereNotNull('ctn_rfid')
            ->where('ctn_rfid', '!=', '')
            ->first();

        if ($chkOrderHasRfidCarton) {
            $values = [
                'odr_rfid_ctn' => true,
            ];

            $data = [
                'odr_id'     => $odrId,
                'qualifier'  => 'ORC',
                'value'      => json_encode($values),
                'created_at' => time(),
                'updated_at' => time(),
            ];

            DB::table('odr_hdr_meta')->insert($data);
        }
    }

    public function updateInvetorySmrOdr($odrHdrID) {
        // Update picked_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS picked_qty
                FROM
                    cartons c
                WHERE
                    c.ctn_sts = 'PD'
                AND c.deleted = 0
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            SET i.picked_qty = IFNULL(t.picked_qty, 0)
            WHERE i.picked_qty != IFNULL(t.picked_qty, 0)
            AND d.odr_id = $odrHdrID
        ");

        // Update allocated_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    h.odr_sts,
                    d.whs_id,
                    d.item_id,
                    d.lot,
                    SUM(

                        IF (
                            d.alloc_qty < d.picked_qty,
                            0,
                            d.alloc_qty - d.picked_qty
                        )
                    ) AS allocated_qty
                FROM
                    odr_dtl d
                JOIN odr_hdr h ON h.odr_id = d.odr_id
                WHERE
                    h.odr_sts IN ('AL', 'PK')
                AND d.alloc_qty > 0
                AND d.deleted = 0
                GROUP BY
                    d.whs_id,
                    d.item_id,
                    d.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            SET i.allocated_qty = IFNULL(t.allocated_qty, 0)
            WHERE i.allocated_qty != IFNULL(t.allocated_qty, 0)
            AND d.odr_id = $odrHdrID
        ");

        // Update avail_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS available_qty
                FROM
                    cartons c
                WHERE
                    c.deleted = 0
                AND c.ctn_sts IN ('AC', 'LK')
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            SET i.avail = GREATEST(
                IFNULL(
                    t.available_qty - i.allocated_qty,
                    0
                ),
                0
            ),
             i.allocated_qty =
            IF (
                IFNULL(
                    t.available_qty - i.allocated_qty,
                    0
                ) < 0,
                ABS(
                    IFNULL(
                        t.available_qty - i.allocated_qty,
                        0
                    )
                ),
                i.allocated_qty
            )
            WHERE
                (i.avail != GREATEST(
                        IFNULL(
                            t.available_qty - i.allocated_qty,
                            0
                        ),
                        0
                    )
                OR i.allocated_qty !=
                IF (
                    IFNULL(
                        t.available_qty - i.allocated_qty,
                        0
                    ) < 0,
                    ABS(
                        IFNULL(
                            t.available_qty - i.allocated_qty,
                            0
                        )
                    ),
                    i.allocated_qty
                ))
            AND d.odr_id = $odrHdrID
        ");

        // Update total_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS total_qty
                FROM
                    cartons c
                WHERE
                    c.deleted = 0
                AND c.ctn_sts IN ('AC', 'LK', 'PD')
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            SET i.ttl = IFNULL(t.total_qty, 0)
            WHERE i.ttl != IFNULL(t.total_qty, 0)
            AND d.odr_id = $odrHdrID
        ");
    }

    public function updateInvetoryReportOdr($odrHdrID) {
        DB::select(DB::raw("UPDATE rpt_inventory s
            JOIN (
                SELECT
                    h.whs_id,
                    d.item_id,
                    SUM( d.alloc_qty) AS qty 
                FROM
                    odr_hdr h
                JOIN odr_dtl d ON h.odr_id = d.odr_id 
                WHERE h.odr_id = $odrHdrID
                    AND d.deleted = 0
                    AND d.alloc_qty > 0 
                GROUP BY
                    h.whs_id, d.item_id 
            )   AS t
            ON t.whs_id = s.whs_id AND s.item_id = t.item_id
            SET s.in_pick_qty = s.in_pick_qty - t.qty,
                s.in_hand_qty = s.in_hand_qty + t.qty
            WHERE s.type = 'I'"));
        return 0;
    }
}