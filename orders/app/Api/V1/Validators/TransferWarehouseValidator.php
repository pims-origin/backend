<?php

namespace App\Api\V1\Validators;

class TransferWarehouseValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id'      => 'required|integer',
            'cus_id'      => 'required|integer',
            'odr_type'    => 'required',

            'whs_to'      => 'required|integer',

            'items'              => 'required|array',
            'items.*.odr_dtl_id' => 'integer|exists:odr_dtl,odr_dtl_id',
            'items.*.sku'        => 'required',
            'items.*.qty'        => 'integer',
            'items.*.piece_qty'  => 'numeric',
        ];
    }
}
