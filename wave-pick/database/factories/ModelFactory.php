<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 9/5/2016
 * Time: 2:12 PM
 */

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\Status;

$factory->define(Seldat\Wms2\Models\User::class, function ($faker) {
    return [
        'first_name' => $faker->name,
        'last_name'  => $faker->name,
        'email'      => $faker->email,
        'password'   => $faker->password,
        'status'     => "AC",
    ];
});
$factory->define(Seldat\Wms2\Models\Customer::class, function ($faker) {
    return [
        'cus_name'            => $faker->name,
        'cus_code'            => $faker->name,
        'cus_status'          => DB::table('cus_status')->select('cus_sts_code')->first()['cus_sts_code'],
        'cus_billing_account' => $faker->randomLetter,
    ];
});
$factory->define(Seldat\Wms2\Models\Warehouse::class, function ($faker) {
    return [
        'whs_name'       => $faker->name,
        'whs_code'       => $faker->name,
        'whs_status'     => DB::table('whs_status')->select('whs_sts_code')->first()['whs_sts_code'],
        'whs_country_id' => DB::table('system_country')->select('sys_country_id')->first()['sys_country_id'],
        'whs_state_id'   => DB::table('system_state')->select('sys_state_id')->first()['sys_state_id'],
        'whs_short_name' => $faker->randomLetter,
    ];
});
$factory->define(Seldat\Wms2\Models\SystemUom::class, function ($faker) {
    return [
        'sys_uom_code' => $faker->randomLetter .$faker->randomNumber . $faker->randomLetter,
        'sys_uom_name' => $faker->name,
    ];
});
$factory->define(Seldat\Wms2\Models\Item::class, function ($faker) {
    return [
        'sku'    => $faker->randomLetter,
        'size'   => $faker->randomNumber,
        'color'  => $faker->randomNumber,
        'uom_id' => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()['sys_uom_id'];
        },
        'cus_id' => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()['cus_id'];
        },
        'width'  => 10,
        'pack'   => 10,
        'length' => 10,
        'height' => 20,
        'weight' => 30,
        'status' => DB::table('item_status')->select('item_sts_code')->first()['item_sts_code']
    ];
});
$factory->define(Seldat\Wms2\Models\ShippingOrder::class, function ($faker) {
    return [
        'whs_id'   => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'cus_id'   => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'ref_num'  => $faker->randomLetter,
        'po_total' => $faker->randomNumber,
        'so_sts'   => Status::getByValue("New", "Order-status"),
        'type'     => Status::getByValue("New", "Order-type"),
    ];
});
$factory->define(Seldat\Wms2\Models\OrderHdr::class, function ($faker) {
    $shippingOdr = factory(Seldat\Wms2\Models\ShippingOrder::class)->create();

    return [
        'ship_id'         => null,
        'whs_id'          => $shippingOdr->whs_id,
        'cus_id'          => $shippingOdr->cus_id,
        'so_id'           => $shippingOdr->so_id,
        'csr'             => function () {
            return factory(Seldat\Wms2\Models\User::class)->create()['user_id'];
        },
        'odr_sts'         => $shippingOdr->so_sts,
        'odr_type'        => $shippingOdr->type,
        'ref_cod'         => $faker->postcode,
        'odr_num'         => str_random(10),
        'cus_odr_num'     => str_random(10),
        'cus_po'          => $faker->randomLetter,
        'ship_to_name'    => str_random(10),
        'ship_to_add_1'   => $faker->streetAddress,
        'ship_to_city'    => $faker->city,
        'ship_to_state'   => $faker->state,
        'ship_to_zip'     => $faker->postcode,
        'ship_to_country' => $faker->country,
        'carrier'         => $faker->jobTitle,
        'odr_req_dt'      => $faker->unixTime,
        'req_cmpl_dt'     => $faker->unixTime,
        'act_cmpl_dt'     => $faker->unixTime,
        'ship_by_dt'      => $faker->unixTime,
        'cancel_by_dt'    => $faker->unixTime,
        'act_cancel_dt'   => $faker->unixTime,
        'ship_after_dt'   => $faker->unixTime,
        'shipped_dt'      => $faker->unixTime,
        'cus_pick_num'    => str_random(10),
        'ship_method'     => $faker->randomLetter,
        'cus_dept_ref'    => $faker->randomLetter,
        'rush_odr'        => (int)$faker->boolean,
        'cus_notes'       => str_random(10),
        'in_notes'        => str_random(10),
        'hold_sts'        => (int)$faker->boolean,
        'back_odr'        => (int)$faker->boolean,
        'back_odr_seq'    => $faker->randomNumber,
        'org_odr_id'      => $faker->randomNumber,
        'sku_ttl'         => $faker->randomNumber,
        'wv_num'          => str_random(10),
    ];
});
$factory->define(Seldat\Wms2\Models\OrderDtl::class, function ($faker) {
    $carton = factory(Seldat\Wms2\Models\Carton::class)->create();
    return [
        'odr_id'      => function () {
            return factory(Seldat\Wms2\Models\OrderHdr::class)->create()->odr_id;
        },
        'whs_id'      => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'cus_id'      => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'item_id'     => $carton->item_id,
        'uom_id'      => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        },
        'color'       => uniqid() . $faker->randomLetter . $faker->randomLetter,
        'sku'         => $faker->randomLetter . uniqid() . $faker->randomLetter,
        'size'        => $faker->randomLetter . $faker->randomLetter . uniqid(),
        'lot'         => $faker->randomLetter . uniqid() . uniqid(),
        'qty'         => random_int(1, 100),
        'piece_qty'   => random_int(1, 100),
        'sts'         => $faker->randomLetter,
        'special_hdl' => (int)$faker->boolean,
        'back_odr'    => (int)$faker->boolean,
    ];
});
$factory->define(Seldat\Wms2\Models\OrderShippingInfo::class, function ($faker) {
    return [
        'ship_to_cus_name' => $faker->name,
        'ship_to_addr'     => $faker->address,
        'ship_to_city'     => $faker->city,
        'ship_to_state'    => $faker->state,
        'ship_to_zip'      => $faker->state,
        'ship_to_country'  => $faker->country,
        'ship_by_dt'       => time(),
        'ship_dt'          => time(),
    ];
});
$factory->define(Seldat\Wms2\Models\InventorySummary::class, function ($faker) {
    return [
        'item_id'       => function () {
            return factory(Seldat\Wms2\Models\Item::class)->create()['item_id'];
        },
        'cus_id'        => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()['cus_id'];
        },
        'whs_id'        => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()['whs_id'];
        },
        'color'         => str_random(10),
        'size'          => str_random(10),
        'lot'           => str_random(10),
        'ttl'           => 9999,
        'allocated_qty' => 4,
        'dmg_qty'       => 1,
        'avail'         => 9999,
        'sku'           => str_random(10),
    ];
});

$factory->define(Seldat\Wms2\Models\Zone::class, function ($faker) {
    return [
        'zone_name'        => $faker->name,
        'zone_code'        => uniqid(),
        'zone_whs_id'      => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'zone_type_id'     => function () {
            return factory(Seldat\Wms2\Models\ZoneType::class)->create()->zone_type_id;
        },
        'zone_description' => $faker->text,
        'zone_num_of_loc'  => 5, // need to replace,
        'zone_min_count'   => 1,
        'zone_max_count'   => 999
    ];
});

$factory->define(Seldat\Wms2\Models\ZoneType::class, function ($faker) {
    return [
        'zone_type_name' => $faker->name,
        'zone_type_code' => uniqid(),
        'zone_type_desc' => $faker->text,
    ];
});

$factory->define(Seldat\Wms2\Models\Location::class, function ($faker) {
    return [
        'loc_whs_id'           => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'loc_alternative_name' => $faker->name,
        'loc_code'             => uniqid(),
        'loc_zone_id'          => function () {
            return factory(Seldat\Wms2\Models\Zone::class)->create()->zone_id;
        },
        'loc_type_id'          => function () {
            return factory(Seldat\Wms2\Models\LocationType::class)->create()->loc_type_id;
        },
        'loc_sts_code'         => 'AC',
    ];
});

$factory->define(Seldat\Wms2\Models\LocationType::class, function ($faker) {
    return [
        'loc_type_name' => $faker->name,
        'loc_type_code' => uniqid(),
        'loc_type_desc' => $faker->text,
    ];
});

$factory->define(\Seldat\Wms2\Models\WavepickHdr::class, function ($faker) {
    $code1 = $faker->randomNumber . $faker->randomNumber . $faker->randomNumber . $faker->randomNumber;
    $code2 = $faker->randomNumber . $faker->randomNumber . $faker->randomNumber . $faker->randomNumber . $faker->randomNumber;

    return [
        'whs_id' => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'wv_num' => 'WAV_' . $code1 . '_' . $code2,
        'wv_sts' => 'NW',
        'seq'    => $faker->randomNumber,
    ];
});

$factory->define(\Seldat\Wms2\Models\WavepickDtl::class, function ($faker) {
    $carton = factory(\Seldat\Wms2\Models\Carton::class)->create();
    $odrHdr = factory(Seldat\Wms2\Models\OrderHdr::class)->create();
    $odrDtl = factory(Seldat\Wms2\Models\OrderDtl::class)->create();
    $odrDtl->item_id = $carton->item_id;
    $wvHdr = factory(Seldat\Wms2\Models\WavepickHdr::class)->create();

    $loc1 = factory(Seldat\Wms2\Models\Location::class)->create();
    $loc2 = factory(Seldat\Wms2\Models\Location::class)->create();
    $loc3 = factory(Seldat\Wms2\Models\Location::class)->create();
    $loc4 = factory(Seldat\Wms2\Models\Location::class)->create();
    $loc5 = factory(Seldat\Wms2\Models\Location::class)->create();

    return [
        'cus_id'         => $odrHdr->cus_id,
        'whs_id'         => $wvHdr->whs_id,
        'wv_id'          => $wvHdr->wv_id,
        'item_id'        => $odrDtl->item_id,
        'uom_id'         => $odrDtl->uom_id,
        'wv_num'         => $wvHdr->wv_num,
        'color'          => $odrDtl->color,
        'sku'            => $odrDtl->sku,
        'size'           => $odrDtl->size,
        'pack_size'      => $odrDtl->pack_size,
        'lot'            => $odrDtl->lot,
        'ctn_qty'        => $faker->randomNumber,
        'piece_qty'      => $odrDtl->piece_qty,
        'act_piece_qty'  => $odrDtl->piece_qty * $faker->randomNumber,
        'primary_loc_id' => $carton->loc_id,
        'primary_loc'    => $carton->loc_code,
        'bu_loc_1_id'    => $loc1->loc_id,
        'bu_loc_1'       => $loc1->loc_code,
        'bu_loc_2_id'    => $loc2->loc_id,
        'bu_loc_2'       => $loc2->loc_code,
        'bu_loc_3_id'    => $loc3->loc_id,
        'bu_loc_3'       => $loc3->loc_code,
        'bu_loc_4_id'    => $loc4->loc_id,
        'bu_loc_4'       => $loc4->loc_code,
        'bu_loc_5_id'    => $loc5->loc_id,
        'bu_loc_5'       => $loc5->loc_code,
        'cus_upc'        => $odrDtl->cus_upc,
        'wv_dtl_sts'     => 'NW',
    ];
});

$factory->define(Seldat\Wms2\Models\Pallet::class, function ($faker) {
    return [
        'whs_id'   => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'loc_id'   => function () {
            return factory(Seldat\Wms2\Models\Location::class)->create()->loc_id;
        },
        'loc_name' => $faker->name,
        'loc_code' => uniqid(),
        'plt_num'  => $faker->text,
    ];
});

$factory->define(Seldat\Wms2\Models\Item::class, function ($faker) {
    return [
        'sku'    => $faker->randomLetter,
        'size'   => $faker->randomNumber,
        'color'  => $faker->randomNumber,
        'uom_id' => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        },
        'width'  => $faker->randomNumber,
        'pack'   => $faker->randomNumber,
        'length' => $faker->randomNumber,
        'height' => $faker->randomNumber,
        'weight' => $faker->randomNumber,
        'status' => 'AC'
    ];
});

$factory->define(Seldat\Wms2\Models\AsnHdr::class, function ($faker) {
    return [
        'asn_hdr_seq'    => $faker->randomNumber,
        'asn_hdr_ept_dt' => date('Y-m-d'),
        'asn_hdr_num'    => uniqid(),
        'asn_hdr_ref'    => $faker->randomLetter,
        'cus_id'         => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'whs_id'         => function () {

            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'asn_sts'        => 'NW',
        'sys_mea_code'   => 'CM',
        'asn_hdr_des'    => $faker->randomLetter
    ];
});

$factory->define(Seldat\Wms2\Models\AsnDtl::class, function ($faker) {
    return [
        'asn_hdr_id'      => function () {
            return factory(Seldat\Wms2\Models\AsnHdr::class)->create()->asn_hdr_id;
        },
        'ctnr_id'         => function () {
            return factory(Seldat\Wms2\Models\Container::class)->create()->ctnr_id;
        },
        'item_id'         => function () {
            return factory(Seldat\Wms2\Models\Item::class)->create()->item_id;
        },
        'asn_dtl_po'      => $faker->randomNumber,
        'asn_dtl_po_dt'   => $faker->randomLetter,
        'asn_dtl_ctn_ttl' => 3,
        'asn_dtl_crs_doc' => $faker->randomNumber,
        'asn_dtl_des'     => $faker->randomLetter,
        'uom_id'          => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        },
    ];
});

$factory->define(Seldat\Wms2\Models\GoodsReceipt::class, function ($faker) {
    $asnHeader = factory(Seldat\Wms2\Models\AsnHdr::class)->create();
    $containerId = factory(Seldat\Wms2\Models\Container::class)->create()->ctnr_id;

    return [
        'ctnr_id'       => $containerId,
        'asn_hdr_id'    => $asnHeader->asn_hdr_id,
        'gr_hdr_seq'    => $faker->randomNumber,
        'gr_hdr_ept_dt' => $faker->randomNumber,
        'gr_hdr_num'    => $faker->randomLetter,
        'cus_id'        => $asnHeader->cus_id,
        'whs_id'        => $asnHeader->whs_id,
        'gr_in_note'    => 'Gr_in_note',
        'gr_ex_note'    => 'Gr_ex_note',
        'gr_sts'        => 'RG',
    ];
});

$factory->define(Seldat\Wms2\Models\GoodsReceiptDetail::class, function ($faker) {
    $asnHeader = factory(Seldat\Wms2\Models\AsnHdr::class)->create();
    $containerId = factory(Seldat\Wms2\Models\Container::class)->create()->ctnr_id;
    $asnDtl = factory(Seldat\Wms2\Models\AsnDtl::class)->create([
        'asn_hdr_id' => $asnHeader->asn_hdr_id,
        'ctnr_id'    => $containerId
    ]);

    $goodsReceiptId = factory(Seldat\Wms2\Models\GoodsReceipt::class)->create([
        'ctnr_id'    => $containerId,
        'asn_hdr_id' => $asnHeader->asn_hdr_id,
        'cus_id'     => $asnHeader->cus_id,
        'whs_id'     => $asnHeader->whs_id
    ])->gr_hdr_id;

    return [
        'asn_dtl_id'         => $asnDtl->asn_dtl_id,
        'gr_hdr_id'          => $goodsReceiptId,
        'gr_dtl_ept_ctn_ttl' => $asnDtl->asn_dtl_ctn_ttl,
        'gr_dtl_act_ctn_ttl' => $asnDtl->asn_dtl_ctn_ttl,
        'gr_dtl_cus_note'    => $faker->randomLetter,
        'gr_dtl_disc'        => 1,
        'gr_dtl_is_dmg'      => 1
    ];
});

$factory->define(\Seldat\Wms2\Models\InventorySummary::class, function ($faker) {
    $item = factory(Seldat\Wms2\Models\Item::class)->create();

    return [
        'item_id'       => $item->item_id,
        'ttl'           => 1010,
        'cus_id'        => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'whs_id'        => function () {

            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'color'         => array_get($item, 'color', ''),
        'size'          => array_get($item, 'size', ''),
        'lot'           => array_get($item, 'lot', ''),
        'allocated_qty' => 0,
        'dmg_qty'       => 10,
        'sku'           => array_get($item, 'sku', ''),
        'avail'         => 1000,
    ];
});

$factory->define(Seldat\Wms2\Models\Container::class, function ($faker) {
    return [
        'ctnr_num'  => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+[A-Z]{2,4}'),
        'ctnr_note' => $faker->name,
    ];
});

$factory->define(\Seldat\Wms2\Models\Carton::class, function ($faker) {
    $asnHeader = factory(Seldat\Wms2\Models\AsnHdr::class)->create();
    $containerId = factory(Seldat\Wms2\Models\Container::class)->create()->ctnr_id;

    $itemId = factory(Seldat\Wms2\Models\Item::class)->create()->item_id;

    $asnDtl = factory(Seldat\Wms2\Models\AsnDtl::class)->create([
        'asn_hdr_id' => $asnHeader->asn_hdr_id,
        'ctnr_id'    => $containerId,
        'item_id'    => $itemId
    ]);

    $goodsReceiptId = factory(Seldat\Wms2\Models\GoodsReceipt::class)->create([
        'ctnr_id'    => $containerId,
        'asn_hdr_id' => $asnHeader->asn_hdr_id,
        'cus_id'     => $asnHeader->cus_id,
        'whs_id'     => $asnHeader->whs_id,
        'gr_sts'     => 'RD'
    ])->gr_hdr_id;

    $grDtl = factory(Seldat\Wms2\Models\GoodsReceiptDetail::class)->create([
        'asn_dtl_id'         => $asnDtl->asn_dtl_id,
        'gr_hdr_id'          => $goodsReceiptId,
        'gr_dtl_ept_ctn_ttl' => $asnDtl->asn_dtl_ctn_ttl,
        'gr_dtl_act_ctn_ttl' => ($asnDtl->asn_dtl_ctn_ttl) - 2
    ]);
    $location = factory(Seldat\Wms2\Models\Location::class)->create();

    return [
        'asn_dtl_id'    => $grDtl->asn_dtl_id,
        'item_id'       => $asnDtl->item_id,
        'cus_id'        => $asnHeader->cus_id,
        'ctn_num'       => uniqid(),
        'ctn_sts'       => DB::table('ctn_status')->select('status')->first()['status'],
        'ctn_uom_id'    => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        },
        'ctn_pack_size' => $faker->randomNumber,
        'loc_id'        => $location->loc_id,
        'loc_code'      => $location->loc_code,
        'plt_id'        => factory(Seldat\Wms2\Models\Pallet::class)->create()->plt_id,
    ];
});

