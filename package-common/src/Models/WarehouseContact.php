<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:34
 */

namespace Seldat\Wms2\Models;

/**
 * Class WarehouseContact
 *
 * @property mixed whs_con_id
 * @property mixed whs_con_fname
 * @property mixed whs_con_lname
 * @property mixed whs_con_email
 * @property mixed whs_con_phone
 * @property mixed whs_con_mobile
 * @property mixed whs_con_ext
 * @property mixed whs_con_position
 * @property mixed whs_con_whs_id
 * @property mixed whs_con_department
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 *
 * @package App
 */
class WarehouseContact extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whs_contact';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'whs_con_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_con_fname',
        'whs_con_lname',
        'whs_con_email',
        'whs_con_phone',
        'whs_con_mobile',
        'whs_con_ext',
        'whs_con_position',
        'whs_con_whs_id',
        'whs_con_department',
    ];

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_con_whs_id', 'whs_id');
    }
}
