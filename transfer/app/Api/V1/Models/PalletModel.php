<?php

namespace App\Api\V1\Models;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Wms2\UserInfo\Data;


class PalletModel extends AbstractModel
{
    protected $model;

    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
    }

    public function getPalletByLocId($locId, $cusId)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table('pallet')
            ->where('pallet.loc_id', $locId)
            ->where('pallet.cus_id', $cusId)
            ->where('pallet.whs_id', $currentWH)
            ->get();
    }

    // Change request: Update table pallet - update column storage_duration - 24-11-2016
    public function updatePallet($pltIds)
    {
        // Get Pallet info
        $pltInfos = $this->model
            ->whereIn('plt_id', $pltIds)
            ->get();
        foreach ($pltInfos as $pltInfo) {
            $created_at = array_get($pltInfo, 'created_at', 0);
            $zeroDt = array_get($pltInfo, 'zero_date', 0);

            // Calculate storage_duration
            $date1 = date("Y-m-d", is_int($created_at) ?: $created_at->timestamp );
            $date2 = date("Y-m-d", is_int($zeroDt) ?: $created_at->timestamp );

            $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }

            $this->model
                ->where('plt_id', $pltInfo['plt_id'])
                ->update([
                    'storage_duration' => $storageDuration
                ]);
        }
    }
}
