<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 7/25/2016
 * Time: 5:36 PM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\DamageCarton;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class DamageCartonModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     * @param DamageCarton $model
     */
    public function __construct(DamageCarton $model = null)
    {
        $this->model = ($model) ? : new DamageCarton();
    }

    public function checkExistedCartons($ctn_ids)
    {
        $resultCount = $this->model->whereIn('ctn_id', $ctn_ids)->count();

        return $resultCount > 0 ? true: false;
    }

    /**
     * @param $CartonId
     *
     * @return mixed
     */
    public function deleteDamageCarton($CartonId)
    {
        return $this->model
            ->where('ctn_id', $CartonId)
            ->delete();
    }

    /**
     * @param $CartonId
     *
     * @return mixed
     */
    public function createDamageCarton($cartonIds)
    {
        $cartonIds = is_array($cartonIds) ? $cartonIds : (array)$cartonIds;
        if (empty($cartonIds)) {
            return false;
        }

        // Get damage type first
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $dmgType = DB::table('damage_type')->select('dmg_id')->first();
        $dmgType = !empty($dmgType['dmg_id']) ? $dmgType['dmg_id'] : 1;

        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        $damage = [];
        foreach ($cartonIds as $ctnId) {
            $damage[$ctnId] = [
                'ctn_id' => $ctnId,
                'dmg_id' => $dmgType,
                'created_at' => time(),
                'updated_at' => time(),
                'deleted'    => 0,
                'deleted_at' => 915148800,
                'created_by' => $userId,
                'updated_by' => $userId
            ];
        }

        if (empty($damage)) {
            return false;
        }
        // Deleted damage cartons exists by ctnIds
        $this->model->whereIn('ctn_id', array_keys($damage))->delete();

        // Insert New
        $result = $this->model->insert($damage);

        return $result;
    }


}