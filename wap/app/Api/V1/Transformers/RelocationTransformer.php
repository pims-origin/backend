<?php
//pt:

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Pallet;

class RelocationTransformer extends TransformerAbstract
{
    //return result
    public function transform(Pallet $pallet)
    {
        return [
            'plt_id'   => object_get($pallet, 'plt_id'),
            'loc_id'   => object_get($pallet, 'loc_id'),
            'loc_name' => object_get($pallet, 'loc_name'),
            'plt_num'  => object_get($pallet, 'plt_num'),
            'loc_code' => object_get($pallet, 'loc_code'),
        ];
    }

}
