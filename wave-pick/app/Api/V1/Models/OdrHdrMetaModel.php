<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\OrderHdrMeta;

class OdrHdrMetaModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    protected $odrHdrMeta;

    public function __construct(OrderHdrMeta $model = null)
    {
        $this->model = ($model) ?: new OrderHdrMeta();
    }

    /**
     * @param $odrId
     * @param $qualifier
     *
     * @return mixed
     */
    public function getOdrHdrMetaInfo($odrId, $qualifier)
    {
        $odrIds = is_array($odrId) ? $odrId : [$odrId];

        return $this->model->whereIn('odr_id', $odrIds)
            ->where('qualifier', $qualifier)
            ->get();
    }


    public function getOrderFlow($odr_id)
    {
        $flow = $this->getFirstWhere([
            'odr_id'    => $odr_id,
            'qualifier' => 'OFF'
        ]);
        $value = object_get($flow, 'value', null);
        $val_json_decodes = [];
        if ($value) {
            $valueJsons = \GuzzleHttp\json_decode($value);
            if ($valueJsons) {
                foreach ($valueJsons as $valueJson) {
                    $val_json_decode = [
                        'odr_flow_id' => object_get(\GuzzleHttp\json_decode($valueJson), 'odr_flow_id', 0),
                        'step'        => object_get(\GuzzleHttp\json_decode($valueJson), 'step', 0),
                        'flow_code'   => object_get(\GuzzleHttp\json_decode($valueJson), 'flow_code', ''),
                        'odr_sts'     => object_get(\GuzzleHttp\json_decode($valueJson), 'odr_sts', ''),
                        'name'        => object_get(\GuzzleHttp\json_decode($valueJson), 'name', ''),
                        'description' => object_get(\GuzzleHttp\json_decode($valueJson), 'description', ''),
                        'dependency'  => object_get(\GuzzleHttp\json_decode($valueJson), 'dependency', 0),
                        'type'        => object_get(\GuzzleHttp\json_decode($valueJson), 'type', ''),
                        'usage'       => object_get(\GuzzleHttp\json_decode($valueJson), 'usage', 0),
                    ];
                    array_push($val_json_decodes, $val_json_decode);
                }
            }
        }

        return $val_json_decodes;
    }

}
