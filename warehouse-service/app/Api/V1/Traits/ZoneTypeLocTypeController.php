<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/10/2016
 * Time: 11:16 AM
 */

namespace App\Api\V1\Traits;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use GuzzleHttp\Psr7\Response;

trait ZoneTypeLocTypeController
{
    /**
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getList($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function show($id)
    {
        $result = $this->service->getDetail($id);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function store(IRequest $request)
    {
        $input = $request->getParsedBody();
        $result = $this->service->create($input);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param IRequest $request
     * @param $zoneTypeId
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function update(IRequest $request, $zoneTypeId)
    {
        $input = $request->getParsedBody();
        $result = $this->service->edit($input, $zoneTypeId);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    public function destroy($zoneTypeId)
    {
        $result = $this->service->destroy($zoneTypeId);
        $response = $this->convertResponse($result);

        return $response;
    }
}