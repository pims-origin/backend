<?php

namespace App\Api\V1\Traits;

use Psr\Http\Message\ServerRequestInterface as Request;
use DB;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use App\Api\V1\Transformers\AsnTransformer;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;
use App\Jobs\ASNReceivingEmailJob;

trait AsnControllerTrait
{
    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function dashboard(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboard = $this->asnHdrModel->dashboard($input);

            return ['data' => $dashboard];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $asnID
     * @param Request $request
     * @return mixed
     */
    public function complete($asnID, Request $request)
    {
        $input = $request->getParsedBody();
        $asnHrd = $this->getAsnByCurrentUser($asnID);
        if (empty($asnHrd)) {
            return $this->response->errorBadRequest(Message::get("BM017", "ASN"));
        }

        try {            

            // $isAllApproved = $this->sendApproveListToAE($asnID, $input);

            $asn_hdr_act_dt = array_get($input, 'asn_hdr_act_dt', null);
            if (empty($asn_hdr_act_dt) || $asn_hdr_act_dt == null || $asn_hdr_act_dt == "") {
                $asn_hdr_act_dt = time();
            } else {
                $asn_hdr_act_dt = strtotime($asn_hdr_act_dt);
            }

            // check exist asn by id
            // Update ASN
            DB::table('asn_hdr')->where('asn_hdr_id', $asnID)
                ->update([
                    'asn_sts' => 'CO',
                    'asn_hdr_act_dt' => $asn_hdr_act_dt
                ]);

            // Event tracking for ASN complete
            $asn_hdr_num = $asnHrd->asn_hdr_num;
            $cus_id = $asnHrd->cus_id;
            $whs_id = $asnHrd->whs_id;
            $this->eventTrackingModel->create([
                'whs_id'    => $whs_id,
                'cus_id'    => $cus_id,
                'owner'     => $asn_hdr_num,
                'evt_code'  => Status::getByKey("Event", "ASN-EDITED"),
                'trans_num' => $asn_hdr_num,
                'info'      => 'ASN Completed Receiving'
            ]);

            // if (!$isAllApproved) {
            //     return [
            //         'message'  =>  'Have items is not approved yet, the items list has been sent to A/E email.'
            //     ];
            // }
            
            return $this->response->item($asnHrd, new AsnTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function sendApproveListToAE($asnID, $input)
    {
        $items = DB::table('asn_dtl')
            ->leftJoin('item', 'item.item_id', '=', 'asn_dtl.item_id')
            ->leftJoin('gr_dtl', 'gr_dtl.asn_dtl_id', '=', 'asn_dtl.asn_dtl_id')
            ->where('asn_dtl.asn_hdr_id', $asnID)
            ->where('asn_dtl.ctnr_id', $input['ctnr_id'])
            ->where('item.status', 'RG')
            ->select(
                'item.item_id',
                'item.cus_upc',
                'item.sku',
                'item.size',
                'item.color',
                'item.uom_name',
                'item.pack',
                'item.description',
                'item.length',
                'item.width',
                'item.height',
                'item.weight',
                'asn_dtl.asn_dtl_lot',
                'asn_dtl.ctnr_num',
                'asn_dtl.asn_dtl_ctn_ttl as exp_ctns',
                'asn_dtl.asn_dtl_crs_doc as xdock',
                'asn_dtl.asn_dtl_po',
                'asn_dtl.asn_dtl_po_dt',
                'asn_dtl.expired_dt',
                'gr_dtl.gr_dtl_act_qty_ttl as act_ctn',
                'gr_dtl.gr_dtl_disc_qty as discrep',
                'gr_dtl.gr_dtl_dmg_ttl as dmg_ttl'
            )
            ->groupBy('item.item_id')
            ->get();

        $isAllApproved = true;

        // If have Items not Approve, send email
        if(count($items) > 0) {
            if (empty($input['ctnr_id'])) {
                return $this->response->errorBadRequest("The ctnr_id is required!");
            }

            $userInfo = new Data();
            $userInfo = $userInfo->getUserInfo();

            $grSts = DB::table('gr_hdr')->select('gr_sts')
                ->where('asn_hdr_id', $asnID)
                ->where('ctnr_id', $input['ctnr_id'])
                ->first();

            $totalSku = count($items);
            $completeBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];
            $grStatus = Status::getByValue(array_get($grSts, 'gr_sts', ''), 'GR_STATUS');

            $data = [
                'asnHrd'    =>  $asnHrd,
                'items'     =>  $items,
                'totalSku'  =>  $totalSku,
                'completeBy'=>  $completeBy,
                'currentDate'=> date('Y/m/d H:i:s'),
                'grStatus'  =>  $grStatus,
            ];

            // has item not approve
            $isAllApproved = false;

            // Send Wait-Approve item list to Customer Email
            dispatch(new ASNReceivingEmailJob($data));
        }

        return $isAllApproved;
    }

    public function revert($asnID, Request $request)
    {
        $input = $request->getParsedBody();
        // check exit asn by id
        $asnHrd = $this->getAsnByCurrentUser($asnID);
        if (empty($asnHrd)) {
            return $this->response->errorBadRequest(Message::get("BM017", "ASN"));
        }

        try {
            // update ASN
            DB::table('asn_hdr')->where('asn_hdr_id', $asnID)
                ->update([
                    'asn_sts' => 'RG',
                    'asn_hdr_act_dt' => 0
                ]);

            // event tracking for ASN complete
            $asn_hdr_num = $asnHrd->asn_hdr_num;
            $cus_id = $asnHrd->cus_id;
            $whs_id = $asnHrd->whs_id;
            $this->eventTrackingModel->create([
                'whs_id'    => $whs_id,
                'cus_id'    => $cus_id,
                'owner'     => $asn_hdr_num,
                'evt_code'  => Status::getByKey("Event", "ASN-EDITED"),
                'trans_num' => $asn_hdr_num,
                'info'      => 'ASN Revert Completed Receiving'
            ]);

            return $this->response->item($asnHrd, new AsnTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}