<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 1-Sep-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\User;


/**
 * Class UserModel
 *
 * @package App\Api\V1\Models
 */
class UserModel extends AbstractModel
{
    /**
     * @param User $model
     */
    public function __construct(User $model = null)
    {
        $this->model = ($model) ?: new User();
    }

    /**
     * @param $userId
     *
     * @return mixed
     */
    public function getUserNameById($userId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('users');
        $query->addSelect(DB::raw("CONCAT(first_name,' ',last_name) as username"))
            ->where('user_id', $userId);
        $row = $query->first();

        return $row;
    }

}
