<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use App\Api\V1\Models\Sac;
use App\InvoiceDetail;
use Seldat\Wms2\Models\User;

class BillableDtl extends BaseInvoiceModel
{
    protected $table      = 'billable_dtl';
    protected $primaryKey = 'ba_dtl_id';
    protected $fillable   = [
        'ba_id',
        'sac_id',
        'sac',
        'sac_name',
        'ref_num',
        'qty',
        'ba_type',
        'unit_price',
        'discount',
        'amount',
        'ba_type',
        'cus_id',
        'whs_id',
        'item',
        'description',
        'ba_user_id'
    ];

    public function header()
    {
        return $this->belongsTo(__NAMESPACE__ . '\BillableHdr', 'ba_id');
    }

    public function sacModel()
    {
        return $this->belongsTo(Sac::class, 'sac_id');
    }

    public function invoiceDetail()
    {
        return $this->hasOne(InvoiceDetail::class, 'ba_dtl_id');
    }

    public function orderHdr()
    {
        return $this->belongsTo('Seldat\Wms2\Models\OrderHdr', 'ref_num', 'odr_num');
    }

    public function getSacUniqueAttribute()
    {
        if ($this->sacModel && $this->isSacUnique()) {
            if ($this->sacModel->group) {
                return $this->ba_id.'_'.$this->sacModel->type.':'.$this->ba_dtl_id.':'.$this->sacModel->group;
            } else {
                return $this->ba_id.'_'.$this->sacModel->type.':'.$this->ba_dtl_id;
            }
        }

        return null;
    }

    public function getSacDescriptionAttribute()
    {
        return $this->sacModel->description ?? '';
    }

    public function getSacUnitUomAttribute()
    {
        return $this->sacModel->unit_uom ?? '';
    }

    public function getSacUnitUomNameAttribute()
    {
        return $this->sacModel->unit_uom_name ?? '';
    }

    public function getSacNameAttribute()
    {
        return $this->sacModel->sac_name ?? '';
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getCreatedNameAttribute()
    {
        if ($this->createdBy) {
            return $this->createdBy->first_name . ' ' . $this->createdBy->last_name;
        }

        return null;
    }

    public function setDescriptionAttribute($value)
    {
        if ($value) {
            $prefix = $this->getDescriptionPrefix();

            return $this->attributes['description'] = $prefix[$this->ba_type] ? $prefix[$this->ba_type] . ':' . $value : $value;
        }
    }

    public function getDescriptionPrefix()
    {
        return [
            'INB' => 'CTNR',
            'OUB' => 'PO',
            'PB' => 'PO',
            'STR' => '',
            'TRA' => '',
            'WO' => ''
        ];
    }

    public function isSacUnique()
    {
        $sacUnique = [
            'CTN' => 'QTY',
            'PC'  => 'QTY',
            'CUB' => 'VOLM'
        ];

        if (data_get($sacUnique, $this->sacModel->unit_uom, '') == $this->sacModel->period_uom) {
            return true;
        }

        return false;
    }

    public function getInvNumAttribute()
    {
        return $this->invoiceDetail->inv_num ?? '';
    }

    public function getInvDtFormatAttribute()
    {
        return $this->invoiceDetail->inv_dt_format ?? '';
    }
}
