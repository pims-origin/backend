<?php

namespace App\Api\V1\Validators;

class SampleValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'title'       => 'required',
            'description' => 'required',
        ];
    }
}
