<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 26-Feb-16
 * Time: 3:00 PM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

class ReceivingModel
{

    /**
     * @param $clientID
     * @param $refIDs
     * @return bool
     */
    public function receiving($asnHdrId)
    {
        $validate = DB::table('asn_hdr AS ah')->where('ah.asn_hdr_id', $asnHdrId)
            ->where('ah.asn_sts', config('constants.asn_status.RECEIVED'))->first();
        if (! $validate) {
            return false;
        }
        $reports = $receivingData = DB::table('asn_hdr AS ah')
            ->join('asn_dtl AS ad', 'ad.asn_hdr_id', '=', 'ah.asn_hdr_id')
            ->leftjoin('container AS co', 'co.ctnr_id', '=', 'ad.ctnr_id')
            ->leftjoin('gr_dtl AS gd', 'gd.asn_dtl_id', '=', 'ad.asn_dtl_id')
            ->leftjoin('item AS it', 'it.item_id', '=', 'ad.item_id')
            ->select(
                'ah.asn_hdr_ref AS reference',
                'ah.cus_id',
                'ah.whs_id',
                'ad.asn_dtl_po AS purchase_order',
                'ad.ctnr_num AS container_name',
                'it.item_id',
                'it.sku',
                'ad.asn_dtl_des AS description',
                'it.color',
                'it.size',
                'ad.asn_dtl_pack AS pack_size',
                DB::raw('SUM(gd.gr_dtl_ept_ctn_ttl) AS pcs_expected'),
                DB::raw('SUM(gd.gr_dtl_act_ctn_ttl) AS pcs_received'),
                DB::raw('SUM(gd.gr_dtl_disc) AS pcs_discrepancy'),
                DB::raw('SUM(gd.gr_dtl_ept_ctn_ttl) AS ctns_expected'),
                DB::raw('SUM(gd.gr_dtl_act_ctn_ttl) AS ctns_received'),
                DB::raw('SUM(gd.gr_dtl_disc) AS ctns_discrepancy')
            )
            ->where('ah.asn_hdr_id', $asnHdrId)
            ->where('ah.asn_sts', config('constants.asn_status.RECEIVED'))
            ->orderBy('ad.asn_dtl_po')
            ->orderBy('it.sku')
            ->orderBy('gd.gr_dtl_ept_ctn_ttl', 'DESC')
            ->groupBy('it.sku')
            ->get();
        $this->addMetaInformation($reports);
        $data = $this->processReceivingData($reports);
        $receivingData = array_shift($receivingData);
        $results = [];
        $results ['cus_id'] = $receivingData['cus_id'];
        $results ['whs_id'] = $receivingData['whs_id'];
        $results ['api_app'] = 'edi';
        $results ['transactional_code'] = 'REC';
        $results ['success_key'] = $receivingData['reference'];;
        $results ['data'] = $data;
        return $results;

    }

    public function getItemMeta($primKeys)
    {
        $return = [];

        $results = DB::table('itm_qualifier AS iq')
            ->select(
                'iq.itm_qualifier_des',
                'im.itm_id',
                'im.value'
            )
            ->leftjoin('item_meta AS im', 'im.qualifier', '=', 'iq.qualifier')
            ->whereIn('im.itm_id', $primKeys)
            ->get();

        foreach ($results as $data) {
            $return[$data['itm_id']]['itm_qualifier_des'] = $data['value'];
        }

        return $return;
    }

    private function addMetaInformation(&$reports)
    {
        $temIDs  = [];

        foreach ($reports as $item) {
            $temIDs[] = $item['item_id'];
        }

        $itemMeta = $this->getItemMeta($temIDs);

        foreach ($reports as $item) {
            if (isset($itemMeta['item_id'])) {
                foreach ($itemMeta['item_id'] as $key => $value) {
                    $item[$key] = $value;
                }
            }
        }
        return $reports;
    }

    /**
     * @param $reports
     * @return array
     */
    private function processReceivingData($reports)
    {
        $data = [];
        foreach ($reports as $item) {
            $ref = $item['reference'];
            $data[$ref][] = $item;
        }
        $return = $this->groupByFields($data);
        return $return;
    }


    private function groupByFields($reports)
    {
        $data = [];
        foreach ($reports as $refID => $ref) {
            $itemData = [];
            foreach ($ref as $item) {

                $containerName = $item['container_name'];
                $purchaseOrder = $item['purchase_order'];
                $itemID = $item['item_id'];
                $packSize = $item['pack_size'];
                $description = $item['description'];
                $primeKey = $containerName.$purchaseOrder.$itemID.$packSize.$description;
                $primeKey = str_replace(' ', '', $primeKey);
                $primeKey = strtolower($primeKey);
                if (! isset($itemData[$primeKey])) {
                    $itemData[$primeKey] = $item;
                }
                else {
                    $itemData[$primeKey]['ctns_expected'] += $item['ctns_expected'];
                    $itemData[$primeKey]['ctns_received'] += $item['ctns_received'];
                    $itemData[$primeKey]['pcs_received'] += $item['pcs_received'];
                    $itemData[$primeKey]['pcs_expected'] += $item['pcs_expected'];
                }
                $itemData[$primeKey]['pcs_descrepancy'] = $itemData[$primeKey]['pcs_received'] -
                    $itemData[$primeKey]['pcs_expected'];
                $itemData[$primeKey]['ctns_descrepancy'] = $itemData[$primeKey]['ctns_received'] -
                    $itemData[$primeKey]['ctns_expected'];
            }
            $data[$refID] = $itemData;
            $data[$refID]['isSuccess'] = true;
        }
        return $data;
    }


}
