<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\Customer;
use League\Fractal\TransformerAbstract;

class CustomerDropDownTransformer extends TransformerAbstract
{
    public function transform(Customer $customer)
    {
        return [
            'cus_id'              => $customer->cus_id,
            'cus_name'            => $customer->cus_name,
        ];
    }
}
