<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\InventoryModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\LocationStatusDetailModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Models\SystemUomModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Transformers\GoodsReceiptListTransformer;
use App\Api\V1\Transformers\PutAwayUpdateTransformer;
use App\Api\V1\Validators\PutawayCheckLocationValidator;
use App\Api\V1\Validators\PutawayValidator;
use App\Api\V1\Validators\PutterValidator;
use App\Jobs\AutoCreateBillable;
use App\Jobs\CorrectDataJob;
use Dingo\Api\Exception\UnknownVersionException;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Jobs\AutoUpdateDailyInventoryAndPalletReport;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CustomerZone;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class PutawayController extends AbstractController
{
    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var PutAwayValidator
     */
    protected $validator;

    /**
     * @var $eventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var $goodsReceiptDtlModel
     */
    protected $goodsReceiptDtlModel;

    /**
     * @var $systemUomModel
     */
    protected $systemUomModel;

    /**
     * @var $palletSusggestLocModel
     */
    protected $palletSusggestLocModel;

    /**
     * @var $userModel
     */
    protected $userModel;
    /**
     * [$asnHdrModel description]
     *
     * @var [type]
     */
    protected $asnHdrModel;
    /**
     * [$asnDtlModel description]
     *
     * @var [type]
     */
    protected $asnDtlModel;
    /**
     * [$inventorySummaryModel description]
     *
     * @var [type]
     */
    protected $inventorySummaryModel;

    /**
     * @var InventoryModel
     */
    protected  $inventoryModel;
    /**
     * PutawayController constructor.
     *
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param PutawayValidator $putawayValidator
     * @param EventTrackingModel $eventTrackingModel
     * @param GoodsReceiptDetailModel $goodsReceiptDtlModel
     * @param SystemUomModel $systemUomModel
     * @param PalletSuggestLocationModel $palletSusggestLocModel
     */
    public function __construct(
        GoodsReceiptModel $goodsReceiptModel,
        PalletModel $palletModel,
        CartonModel $cartonModel,
        LocationModel $locationModel,
        PutawayValidator $putawayValidator,
        EventTrackingModel $eventTrackingModel,
        GoodsReceiptDetailModel $goodsReceiptDtlModel,
        SystemUomModel $systemUomModel,
        PalletSuggestLocationModel $palletSusggestLocModel,
        UserModel $userModel,
        AsnDtlModel $asnDtlModel,
        AsnHdrModel $asnHdrModel,
        InventorySummaryModel $inventorySummaryModel
    ) {
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->palletModel = $palletModel;
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->validator = $putawayValidator;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->goodsReceiptDtlModel = $goodsReceiptDtlModel;
        $this->systemUomModel = $systemUomModel;
        $this->palletSusggestLocModel = $palletSusggestLocModel;
        $this->userModel = $userModel;
        $this->asnDtlModel = $asnDtlModel;
        $this->asnHdrModel = $asnHdrModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->inventoryModel = new InventoryModel();
    }

    /**
     * @param Request $request
     * @param GoodsReceiptListTransformer $goodsReceiptListTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        Request $request,
        GoodsReceiptListTransformer $goodsReceiptListTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['gr_sts'] = config('constants.gr_status.RECEIVING');
        $input['created_from'] = 'WMS';

        try {
            $goodsReceiptModel = $this->goodsReceiptModel->search($input, [
                'goodsReceiptDetail',
                'asnHdr',
                'asnHdr.asnDtl',
                'palletSuggestLocation',
                'container'
            ], array_get($input, 'limit'));

            return $this->response->paginator($goodsReceiptModel, $goodsReceiptListTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $goodsReceiptId
     *
     * @return array|void
     *
     * @deprecated don't use this function
     *
     */
    public function itemsUpdate($goodsReceiptId)
    {
        try {

            // check user that is logging is putter or not...
            $predis = new Data();
            $userInfo = $predis->getUserInfo();
            $userId = array_get($userInfo, 'user_id', 0);

            $arrWhere = ['gr_hdr_id' => $goodsReceiptId];

            $permission = $this->goodsReceiptModel->checkPermissionAdminManager($userId);
            if (!$permission) {
                $arrWhere['putter'] = $userId;
            }

            $goodRCs = $this->goodsReceiptModel->findWhere($arrWhere)->toArray();

            if (empty($goodRCs)) {
                throw new \Exception(Message::get("BM017", 'Good receipt'));
            }

            // Get gr_dtl_id by gr_hdr_id
            $goodReDtls = $this->goodsReceiptDtlModel->findWhere(
                ['gr_hdr_id' => $goodsReceiptId]
            );

            $gr_dtl_ids = array_pluck($goodReDtls, 'gr_dtl_id', 'gr_dtl_id');

            // Get carton by gr_dtl_id
            $cartons = $this->cartonModel->getCartonPutAwayIntUpdate($gr_dtl_ids);

            if (empty($cartons)) {
                throw new \Exception(Message::get("BM017", 'Cartons'));
            }

            $ctn_uom_ids = array_pluck($cartons, 'ctn_uom_id', 'ctn_uom_id');
            $systemUoms = [];
            if (!empty($ctn_uom_ids)) {
                $systemUoms = array_pluck($this->systemUomModel->getSysUomByIds(
                    $ctn_uom_ids
                ), 'sys_uom_name', 'sys_uom_id');
            }

            $arrItems = [];
            foreach ($cartons as $carton) {
                $pltId = $carton['plt_id'];
                $key = $carton['item_id'] . '-' . $pltId;

                $this->palletSusggestLocModel->refreshModel();
                $palletSugLoc = $this->palletSusggestLocModel->getFirstWhere([
                    'item_id'   => $carton['item_id'],
                    'gr_hdr_id' => $goodsReceiptId,
                    'gr_dtl_id' => $carton['gr_dtl_id'],
                    'plt_id'    => $carton['plt_id']
                ], ['pallet', 'location']);

                if (empty($arrItems[$key])) {
                    $arrItems[$key] = [
                        'item_id'           => $carton['item_id'],
                        'plt_id'            => $pltId,
                        'plt_num'           => object_get($palletSugLoc, 'pallet.plt_num', null),
                        'sku'               => array_get($carton, 'sku', null),
                        'color'             => array_get($carton, 'color', null),
                        'size'              => array_get($carton, 'size', null),
                        'lot'               => array_get($carton, 'asn_dtl_lot', null),
                        'uom'               => !empty($systemUoms[$carton['ctn_uom_id']])
                            ? $systemUoms[$carton['ctn_uom_id']] : '',
                        'length'            => array_get($carton, 'asn_dtl_length', null),
                        'width'             => array_get($carton, 'asn_dtl_width', null),
                        'height'            => array_get($carton, 'asn_dtl_height', null),
                        'weight'            => array_get($carton, 'asn_dtl_weight', null),
                        'asn_dtl_cus_upc'   => array_get($carton, 'asn_dtl_cus_upc', null),
                        'asn_dtl_pack'      => array_get($carton, 'asn_dtl_pack', null),
                        'assigned_loc_id'   => array_get($palletSugLoc, 'loc_id', null),
                        'assigned_loc_code' => array_get($palletSugLoc, 'location.loc_code', null),
                        'assigned_loc_name' => array_get($palletSugLoc, 'location.loc_alternative_name', null),
                        'ctn_ttl'           => array_get($palletSugLoc, 'ctn_ttl', null),
                        "actual_loc_code"   => array_get($carton, "loc_code", null),
                        "actual_loc_name"   => array_get($carton, "loc_name", null),
                        "actual_loc_id"     => array_get($carton, "loc_id", null),
                        "po"                => array_get($carton, "asn_dtl_po", null)
                    ];
                }
            }

            return ['data' => array_values($arrItems)];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $goodsReceiptId
     *
     * @return array|void
     */
    public function itemsUpdateFromPalSugLoc($goodsReceiptId)
    {
        try {
            // check user that is logging is putter or not...
            $predis = Data::getInstance();
            $userInfo = $predis->getUserInfo();
            $userId = array_get($userInfo, 'user_id', 0);

            $arrWhere = ['gr_hdr_id' => $goodsReceiptId];

            $gr = $this->goodsReceiptModel->getGRByCurrentUser($goodsReceiptId);
            if (!$gr) {
                return $this->response->errorBadRequest('Goods receipt not exited');
            }

            // $permission = $this->goodsReceiptModel->checkPermissionAdminManager($userId);
            // if (!$permission) {
            //     $arrWhere['putter'] = $userId;
            // }

            $goodRCs = $this->goodsReceiptModel->findWhere($arrWhere)->toArray();

            if (empty($goodRCs)) {
                throw new \Exception(Message::get("BM017", 'Good receipt'));
            }

            //$palletSugLocs = $this->palletSusggestLocModel->findWhere(
            //    ['gr_hdr_id' => $goodsReceiptId],
            //    ['pallet', 'location']
            //)->toArray();

            $palletSugLocs = $this->palletSusggestLocModel->getModel()
                ->select([
                    'pal_sug_loc.item_id',
                    'pal_sug_loc.sku',
                    'pal_sug_loc.color',
                    'pal_sug_loc.size',
                    'pal_sug_loc.lot',
                    'pal_sug_loc.loc_id',
                    'pal_sug_loc.ctn_ttl',
                    'pal_sug_loc.act_loc_id',
                    'pal_sug_loc.act_loc_code',
                    'location.loc_code',
                    'location.loc_alternative_name',
                    'pallet.plt_id',
                    'pallet.plt_num',

                ])
                ->join('pallet', 'pallet.plt_id', '=', 'pal_sug_loc.plt_id')
                ->leftJoin('location', 'location.loc_id', '=', 'pal_sug_loc.loc_id')
                ->where('pallet.plt_sts', '!=', 'CC')
                ->where('pal_sug_loc.gr_hdr_id', $goodsReceiptId)
                ->get()->toArray();

            $arrItems = [];
            if ($palletSugLocs) {
                foreach ($palletSugLocs as $palletSugLoc) {
                    $arrItems[] = [
                        'item_id'           => array_get($palletSugLoc, 'item_id', null),
                        'plt_id'            => array_get($palletSugLoc, 'plt_id', null),
                        'plt_num'           => array_get($palletSugLoc, 'plt_num', null),
                        'sku'               => array_get($palletSugLoc, 'sku', null),
                        'color'             => array_get($palletSugLoc, 'color', null),
                        'size'              => array_get($palletSugLoc, 'size', null),
                        'lot'               => array_get($palletSugLoc, 'lot', null),
                        'assigned_loc_id'   => array_get($palletSugLoc, 'loc_id', null),
                        'assigned_loc_code' => array_get($palletSugLoc, 'loc_code', null),
                        'assigned_loc_name' => array_get($palletSugLoc, 'loc_alternative_name', null),
                        'ctn_ttl'           => array_get($palletSugLoc, 'ctn_ttl', null),
                        "actual_loc_code"   => array_get($palletSugLoc, "act_loc_code", null),
                        "actual_loc_name"   => array_get($palletSugLoc, "act_loc_code", null),
                        "actual_loc_id"     => array_get($palletSugLoc, "act_loc_id", null),
                    ];
                }
            }

            //get xdock location
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $xdockPallets = Carton::select([
                'cartons.item_id',
                'cartons.plt_id',
                'pallet.plt_num',
                'cartons.sku',
                'cartons.color',
                'cartons.size',
                'cartons.lot',
                DB::raw("'' AS assigned_loc_id"),
                DB::raw("'' AS assigned_loc_code"),
                DB::raw("'' AS assigned_loc_name"),
                DB::raw('COUNT(cartons.ctn_id) AS ctn_ttl'),
                DB::raw("cartons.loc_code AS actual_loc_code"),
                DB::raw("cartons.loc_code AS actual_loc_name"),
                DB::raw("cartons.loc_id AS actual_loc_id")
            ])
                ->join('pallet', 'cartons.plt_id', '=', 'pallet.plt_id')
                ->where([
                    'cartons.gr_hdr_id' => $goodsReceiptId,
                    'cartons.ctn_sts'   => 'AC',
                    'pallet.is_xdock'   => 1
                ])
                ->whereNotNull('cartons.loc_id')
                ->groupBy('cartons.plt_id')
                ->get()
                ->toArray();

            return ['data' => array_merge($arrItems, $xdockPallets)];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    /**
     * @param $goodsReceiptId
     * @param Request $request
     * @param PutAwayUpdateTransformer $putAwayUpdateTransformer
     * @param LocationStatusDetailModel $locationStatusDetailModel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function update
    (
        $goodsReceiptId,
        Request $request,
        PutAwayUpdateTransformer $putAwayUpdateTransformer,
        LocationStatusDetailModel $locationStatusDetailModel
    ) {
        set_time_limit(0);
        $arrWhere = ['gr_hdr_id' => $goodsReceiptId];

        $gr = $this->goodsReceiptModel->getGRByCurrentUser($goodsReceiptId);
        if (!$gr) {
            return $this->response->errorBadRequest('Goods receipt not exited');
        }

        $goodRCs = $this->goodsReceiptModel->findWhere($arrWhere)->toArray();

        if (empty($goodRCs)) {
            throw new \Exception(Message::get("BM017", 'Good receipt'));
        }

        $input = $request->getParsedBody();

        $params = $request->getQueryParams();
        $activeLoc = array_get($params, 'activeLoc', '');

        //validate data input
        $this->validator->validate($input);

        // get change loc_id
        $arr_loc = $input;
        foreach ($arr_loc as $key => $itm) {
            if ($itm['loc_id'] == $itm['old_loc_id']) {
                unset($arr_loc[$key]);
            }
        }
        $arr_loc_id = array_column($arr_loc, 'loc_id');
        /**
         * cus_id
         * loc_ids
         * 1. (query loc_id IN (loc_ids) location  join zone and customer)
         */

        $locations = $this->locationModel->getLocationByCus($goodRCs[0]['whs_id'], $arr_loc_id,
            $goodRCs[0]['cus_id'])->toArray();
        if (!empty($locations)) {
            $cusLocIds = array_column($locations, 'loc_id', '');
            $errLocIds = [];
            foreach ($arr_loc_id as $locId) {
                if (!in_array($locId, $cusLocIds)) {
                    $errLocIds[] = $locId;
                }
            }
            if (!empty($errLocIds)) {
                $errLocs = $this->locationModel->getLocationByIds($errLocIds)->toArray();
                $errLocs = array_column($errLocs, 'loc_code', '');
                $msg = sprintf('The location(s) %s not belonged to this customer', implode(', ', $errLocs));
                throw new \InvalidArgumentException($msg);
            }
        } else {
            $msg = 'There are at least one location that does not belong to this customer';
            throw new \InvalidArgumentException($msg);
        }

        // check Locations are not active
        $loc_not_ac = $this->locationModel->countLocationNotHasStatus($arr_loc_id,
            config('constants.location_status.ACTIVE'));
        if ($loc_not_ac > 0) {
            if ($activeLoc == 1) {
                $this->locationModel->updateLocStatusWhereIn($arr_loc_id, config('constants.location_status.ACTIVE'));
                $locationStatusDetailModel->updateLocStatusWhereIn($arr_loc_id,
                    config('constants.location_status.ACTIVE'));

            } else {
                throw new UnknownVersionException(Message::get("BM022"), null, 5);
            }
        }

        // check location has pallet
        $count_pallets = $this->palletModel->countPalletWhereInLocation($arr_loc_id);
        if ($count_pallets > 0) {
            throw new UnknownVersionException('Some locations already have pallets', null, 4);
        }

        try {

            $goodsReceipt = $this->goodsReceiptModel->getFirstBy('gr_hdr_id', $goodsReceiptId);

            if (is_null($goodsReceipt)) {
                throw new \Exception(Message::get("BM017", "Goods Receipt"));
            }

            DB::beginTransaction();

            foreach ($arr_loc as $row) {


                $location = $this->locationModel->getFirstWhere(['loc_id' => $row['loc_id']], ['locationType']);
                if ($location) {
                    //check duplicate loc_id
                    $checkDupLoc = $this->palletModel->getFirstBy('loc_id', object_get($location, 'loc_id', null));
                    if ($checkDupLoc) {
                        throw new UnknownVersionException('At least 1 or more locations are already in use.', null, 4);
                    }
                    //update carton và pallet with new location
                    $data = [
                        'plt_id'   => $row['plt_id'],
                        'loc_id'   => object_get($location, 'loc_id', null),
                        'loc_code' => object_get($location, 'loc_code', null),
                        'loc_name' => object_get($location, 'loc_alternative_name', null),
                    ];
                    $this->palletModel->refreshModel();

                    $this->palletModel->update($data);

                    // update pallet suggest loc
                    $this->palletSusggestLocModel->refreshModel();
                    $this->palletSusggestLocModel->updateWhere([
                        'act_loc_id'   => object_get($location, 'loc_id', null),
                        'act_loc_code' => object_get($location, 'loc_code', null),
                    ], [
                        'plt_id' => $row['plt_id']
                    ]);
                    // update carton

                    $this->cartonModel->refreshModel();

                    $dataUpdate = [
                        'loc_id'        => object_get($location, 'loc_id', null),
                        'loc_code'      => object_get($location, 'loc_code', null),
                        'loc_type_code' => object_get($location, 'locationType.loc_type_code', null),
                        'loc_name'      => object_get($location, 'loc_alternative_name', null),
                    ];
                    $this->cartonModel->updateWhere($dataUpdate, [
                        ["plt_id", "=", $row['plt_id']]
                    ]);

                    // event tracking
                    $carton = $this->cartonModel->getFirstWhere(['plt_id' => $row['plt_id']]);

                    $this->eventTrackingModel->refreshModel();
                    $this->eventTrackingModel->create([
                        'whs_id'    => object_get($goodsReceipt, 'whs_id', ''),
                        'cus_id'    => object_get($goodsReceipt, 'cus_id', ''),
                        'owner'     => object_get($goodsReceipt, 'gr_hdr_num', ''),
                        'evt_code'  => config('constants.event.PL-AWAY'),
                        'trans_num' => object_get($carton, 'pallet.plt_num', ''),
                        'info'      => sprintf(config('constants.event-info.PL-AWAY'),
                            object_get($location, 'loc_code', ''))

                    ]);

                } else {
                    throw new \Exception(Message::get("BM017", "location"));
                }

                //update  putaway status in table pallet susggest location
                $dataUpdated = [
                    'put_sts' => config('constants.putaway_status.COMPLETED')
                ];
                $this->palletSusggestLocModel->updateWhere($dataUpdated, ['gr_hdr_id' => $goodsReceiptId]);

            }

            /**
             * Event tracking for Complete PUT AWAY
             */
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => object_get($goodsReceipt, 'whs_id', ''),
                'cus_id'    => object_get($goodsReceipt, 'cus_id', ''),
                'owner'     => object_get($goodsReceipt, 'gr_hdr_num', ''),
                'evt_code'  => config('constants.event.PUTAWAY-COMPLETED'),
                'trans_num' => object_get($goodsReceipt, 'gr_hdr_num', ''),
                'info'      => sprintf('Put %d pallet(s) on Rack completed.', count($arr_loc))

            ]);
            $this->goodsReceiptModel->updateWhere(['putaway' => 1], ['gr_hdr_id' => $goodsReceiptId]);
            //Complete Good Reciept when we Putaway success on web (WMS)
            if ($goodsReceipt->whs_id && ($goodsReceipt->created_from === 'WMS')) {
                $this->wmsComplete($goodsReceipt->whs_id, $goodsReceipt, $request);
            }

            DB::commit();
            if($goodsReceipt){
                $userId = Data::getCurrentUserId();
                dispatch( new CorrectDataJob($request, $goodsReceipt->gr_hdr_id, $userId) );

                dispatch(new AutoCreateBillable($goodsReceipt->whs_id, $goodsReceipt->cus_id, $goodsReceipt->gr_hdr_id, $request));
                $invDate = strtotime(date('Y-m-d'));
                dispatch(new AutoUpdateDailyInventoryAndPalletReport($goodsReceipt->whs_id, $goodsReceipt->cus_id, $invDate));
            }
            return $this->response->item($goodsReceipt, $putAwayUpdateTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        }
    }

    /**
     * @param Request $request
     * @param UserModel $userModel
     * @param CsrValidator $csrValidator
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return Response|void
     */
    public function updatePutter(
        Request $request,
        PutterValidator $putterValidator,
        GoodsReceiptListTransformer $goodsReceiptListTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $putterValidator->validate($input);

        $params = [
            'putter' => $input['user_id']
        ];

        try {

            // Load User
            $userInfo = $this->userModel->getFirstWhere(['user_id' => $input['user_id']]);
            if (empty($userInfo)) {
                throw new \Exception(Message::get("BM017", "User"));
            }
            //check user status
            $putterValidator->checkUserStatus(object_get($userInfo, 'status', null));

            //$gr = $this->goodsReceiptModel->getGRByCurrentUser($input['gr_hdr_id']);
            //if(! $gr){
            //    return $this->response->errorBadRequest('Goods receipt not exited');
            //}


            // Check Exist Goods Receipt
            $grHdrs = $this->goodsReceiptModel->getGrHdrById($input['gr_hdr_id']);
            if (empty($grHdrs)) {
                throw new \Exception(Message::get("BM017", "Goods Receipt"));
            }

            if ($grHdr = $this->goodsReceiptModel->updateWhereIn($params, $input['gr_hdr_id'], 'gr_hdr_id')) {

                //Update pallet suggest location
                $this->palletSusggestLocModel->updateWhereIn($params, $input['gr_hdr_id'], 'gr_hdr_id');

                if ($grHdrInfo = $this->goodsReceiptModel->getGrHdrById($input['gr_hdr_id'])
                ) {
                    return $this->response->item($grHdrInfo, $goodsReceiptListTransformer);
                }

            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param Request $request
     * @param PutawayCheckLocationValidator $putCheckValidator
     *
     * @return array
     */
    public function checkLocations($whsId, $cusId, Request $request, PutawayCheckLocationValidator $putCheckValidator)
    {

        $input = $request->getParsedBody();
        $input['whs_id'] = $whsId;
        $input['cus_id'] = $cusId;

        $putCheckValidator->validate($input);

        $zoneId = CustomerZone::where(
            [
                'cus_id'     => $input['cus_id'],
                'deleted_at' => 915148800,
                'deleted'    => 0
            ])->first();
        if (empty($zoneId->zone_id)) {
            return $this->response->errorBadRequest(Message::get("BM017", "Customer"));
        }

        $input['loc_zone_id'] = $zoneId->zone_id;

        $totalUnAvailable = $this->locationModel->getUnAvailableLocation($input)->toArray();
        $totalUnAvailable = array_pluck($totalUnAvailable, "loc_id", "loc_id");

        // Load all location where In LocIds input.
        $allLocation = $this->locationModel->getAll(['loc_id' => ['in' => $input['loc_ids']]], ['pallet'])->toArray();

        $unAvailable = null;
        $lockLoc = null;
        $inactiveLoc = null;
        $existLoc = null;
        $details = [];

        foreach ($allLocation as $location) {


            if (!empty($totalUnAvailable[$location['loc_id']])) {
                // Check unavailable
                $unAvailable .= $location['loc_code'] . ", ";
                $details[$location['loc_id']] = "Location is unavailable";
            } elseif (!empty($location['pallet'])) {
                // Add number of existed location in pallet
                $existLoc .= $location['loc_code'] . ", ";
                $details[$location['loc_id']] = "Location has pallet";
            } elseif ($location['loc_sts_code'] == "LK") {
                // Add number lock location
                $lockLoc .= $location['loc_code'] . ", ";
                $details[$location['loc_id']] = "Location is locked";
            } elseif ($location['loc_sts_code'] == "IA") {
                // Add number of inactive location
                $inactiveLoc .= $location['loc_code'] . ", ";
                $details[$location['loc_id']] = "Location is inactive";
            } else {
                // Location is valid
                $details[$location['loc_id']] = null;
            }
        }

        $error = false;
        $errorData = [];
        if (!empty($unAvailable)) {
            $errorData[] = "Unavailable locations: " . trim($unAvailable, ", ");
            $error = true;
        }

        if (!empty($lockLoc)) {
            $errorData[] = "Locked locations: " . trim($lockLoc, ", ");
            $error = true;
        }

        if (!empty($inactiveLoc)) {
            $errorData[] = "Inactive locations: " . trim($inactiveLoc, ", ");
            $error = true;
        }

        if (!empty($existLoc)) {
            $errorData[] = "Locations that already have pallet: " . trim($existLoc, ", ");
            $error = true;
        }

        $data = [
            'valid'   => $error ? false : true,
            'errors'  => $errorData,
            'details' => $details
        ];

        return $data;
    }

    /**
     * [wmsComplete description]
     *
     * @param  [type] $whsId   [description]
     * @param  [type] $grHdr   [description]
     * @param  [type] $request [description]
     *
     * @return [type]          [description]
     */
    private function wmsComplete($whsId, $grHdr, $request)
    {
        // get data from HTTP
        $inputQuery = $request->getQueryParams();
        $inputPost = $request->getParsedBody();
        $agree = array_get($inputQuery, 'agree', false);
        $inNote = array_get($inputPost, 'in_note', null);
        $exNote = array_get($inputPost, 'ex_note', null);
        $actGrDt = array_get($inputPost, 'gr_hdr_act_dt', date("Y-m-d H:i"));

        $grHdrId = $grHdr->gr_hdr_id;

        $grDtls = $this->goodsReceiptDtlModel->getGoodsReceiptDetailByGrHdrId($grHdrId);

        if (!$grDtls) {
            throw new \Exception('There is no Goods Receipt Details');
        }

        $noPutaway = DB::table('pallet')->where([
            'gr_hdr_id' => $grHdrId,
            'deleted'   => 0,
            'loc_id'    => null,
        ])
            ->whereNotNull('rfid')
            ->count();

        if ($noPutaway) {
            return $this->response->errorBadRequest("Goods Receipt Pallets don't putaway all yet");
        }

        //create ref
        $ref = md5('GR' . time());
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $asnDtls = DB::table('asn_dtl')
            ->where('ctnr_id', $grHdr->ctnr_id)
            ->where('asn_hdr_id', $grHdr->asn_hdr_id)
            ->where('asn_dtl_sts', '<>', 'CC')
            ->where('deleted', 0)
            ->get();

        // If ans dtl not in gr_dtl return these asn_dtl
        $grDtlAsnDtlIds = array_pluck($grDtls->toArray(), 'asn_dtl_id');
        $asnDtls = array_filter($asnDtls, function ($asnDtl) use ($grDtlAsnDtlIds) {
            return !in_array($asnDtl['asn_dtl_id'], $grDtlAsnDtlIds);
        });

        /* fix missing SKU */
//        if (count($asnDtls)) {
//            if ($agree) {
//                ///create all gr dtls from $asnDtls with actual ctn ttl = 0
//                $this->createGoodsReceiptDetails($grHdr->gr_hdr_id, $asnDtls);
//            } else {
//                //Error 2
//                $arr = [];
//                foreach ($asnDtls as $item) {
//                    $arr[] = $item;
//                }
//
//                return [
//                    'data'    => $arr,
//                    'error'   => 2,
//                    'message' => 'Missing SKUs in GR'
//                ];
//            }
//        }

        // WMS2-5190 - Save gr_dtl_act_qty_ttl,gr_dtl_ept_qty_ttl  when Complete Goods Receipt
        $this->goodsReceiptDtlModel->computeGrDtlQty($grHdrId);

        $res = $this->goodsReceiptModel->updateGRRecevied($grHdrId, $inNote, $exNote,null,date('Y-m-d H:i:s'));
        if ($res) {
            $gr_hdr_num = $grHdr->gr_hdr_num;
            // Add Event Tracking Goods Receipt
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $whsId,
                'cus_id'    => $grHdr->cus_id,
                'owner'     => $gr_hdr_num,
                'evt_code'  => Status::getByKey("Event", "GR-COMPLETE"),
                'trans_num' => $gr_hdr_num,
                'info'      => 'GR Received - Complete GR'
            ]);
        }
        $this->asnDtlModel->updateAsnDtlsReceivedByCtnrID($grHdr->ctnr_id, $grHdr->asn_hdr_id);

        $grNotRe = DB::table('asn_dtl')
            ->where('asn_hdr_id', $grHdr->asn_hdr_id)
            ->whereNotIn('asn_dtl_sts', ['RE', 'CC'])
            ->where('deleted', 0)
            ->first();

        if(!$grNotRe) {

            $resAsn = $this->asnHdrModel->updateAsnHdrToReceived($grHdr->asn_hdr_id);
            if ($resAsn) {
                $ansHdr = $this->asnHdrModel->getAsnHdrById($grHdr->asn_hdr_id);
                $asnNum = $ansHdr->asn_hdr_num;
                // Add Event Tracking Asn received
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $whsId,
                    'cus_id'    => $grHdr->cus_id,
                    'owner'     => $asnNum,
                    'evt_code'  => Status::getByKey("Event", "ASN-COMPLETE"),
                    'trans_num' => $asnNum,
                    'info'      => sprintf(Status::getByKey("Event-info", "ASN-COMPLETE"), $asnNum)
                ]);
            }
        }

        //Update status for Good Receipt Detail.
        $this->goodsReceiptDtlModel->updateGoodsReceiptDetailReceived($grHdrId);

        //Insert Inventory
        $this->insertInvtSmr($grHdr, $grDtls);

        //update inventory
        $this->updateInventory($grHdrId);

        //change status cartons, pallet, location to AC
        $this->updateCartonStatus($grHdrId, $actGrDt, $ref);

        //Update status pallet
        $this->updatePalletStatus($grHdrId, $actGrDt, $ref);

        $this->inventoryModel->updateInvent($grHdrId, $whsId);

    }

    /**
     * [updatePalletStatus description]
     *
     * @param  [type] $grHdrId [description]
     * @param  [type] $actGrDt [description]
     * @param  [type] $ref     [description]
     *
     * @return [type]          [description]
     */
    private function updatePalletStatus($grHdrId, $actGrDt, $ref)
    {
        return DB::table('pallet AS p')
            ->join('location AS l', 'l.loc_id', '=', 'p.loc_id')
            ->where('p.gr_hdr_id', $grHdrId)
            ->where('p.plt_sts', 'RG')
            ->where('l.loc_sts_code', 'RG')
            ->update([
                'l.loc_sts_code' => 'AC',
                'p.plt_sts'      => 'AC',
                'p.gr_dt'        => strtotime($actGrDt),
                'ref'            => $ref
            ]);
    }

    /**
     * [updateCartonStatus description]
     *
     * @param  [type] $grHdrId [description]
     * @param  [type] $actGrDt [description]
     * @param  [type] $ref     [description]
     *
     * @return [type]          [description]
     */
    private function updateCartonStatus($grHdrId, $actGrDt, $ref)
    {
        return DB::table('cartons')->where([
            'deleted'   => 0,
            'ctn_sts'   => 'RG',
            'gr_hdr_id' => $grHdrId
        ])->update([
            'ctn_sts' => 'AC',
            'gr_dt'   => strtotime($actGrDt),
            'ref'     => $ref
        ]);
    }

    /**
     * [updateInventory description]
     *
     * @param  [type] $grHdrId [description]
     *
     * @return [type]          [description]
     */
    private function updateInventory($grHdrId)
    {
        return DB::table('invt_smr AS iv')
            ->join(DB::raw("(
                SELECT item_id,
                    whs_id,
                    SUM(piece_remain) AS avail,
                    SUM(IF(loc_type_code = 'XDK', piece_remain, 0)) AS xdock_qty,
                    SUM(IF(is_damaged, piece_remain, 0)) AS dmg_qty
                FROM cartons
                WHERE gr_hdr_id = $grHdrId
                    AND deleted = 0
                    AND ctn_sts = 'RG'
                GROUP BY item_id, whs_id
            ) tt"), function ($join) {
                $join->on('iv.item_id', '=', 'tt.item_id')
                    // ->on('iv.lot', '=', 'tt.lot');
                    ->on('iv.whs_id', '=', 'tt.whs_id');
            })
            ->where([
                'iv.whs_id' => Data::getCurrentWhsId()
            ])
            ->update([
                // "iv.ttl"         => DB::raw("iv.ttl + tt.avail + tt.dmg_qty"),
                "iv.ttl"         => DB::raw("iv.ttl + tt.avail"),
                "iv.avail"       => DB::raw("iv.avail + tt.avail - tt.xdock_qty"),
                "iv.dmg_qty"     => DB::raw("iv.dmg_qty + tt.dmg_qty"),
                "iv.crs_doc_qty" => DB::raw("iv.crs_doc_qty + tt.xdock_qty"),
            ]);
    }

    /**
     * [insertInvtSmr description]
     *
     * @param  [type] $grHdr  [description]
     * @param  [type] $grDtls [description]
     *
     * @return [type]         [description]
     */
    private function insertInvtSmr($grHdr, $grDtls)
    {
        if (!empty($grDtls)) {

            $whsId = $grHdr->whs_id;
            $cusId = $grHdr->cus_id;
            $insertInvtSmr = [];

            $created_at = time();

            foreach ($grDtls as $grDtl) {
                if ($grDtl->gr_dtl_sts == 'CC') {
                    continue;
                }
                $ivt = $this->inventorySummaryModel
                    ->getFirstWhere([
                        'item_id' => $grDtl['item_id'],
                        'lot'     => $grDtl['lot'],
                        'whs_id'  => $whsId
                    ]);

                if (!$ivt) {

                    $insertInvtSmr[$grDtl['item_id'].'-'.$grDtl['lot'].'-'.$whsId] = [
                        'item_id'       => $grDtl['item_id'],
                        'cus_id'        => $cusId,
                        'whs_id'        => $whsId,
                        'color'         => array_get($grDtl, "color", null),
                        'sku'           => array_get($grDtl, "sku", null),
                        'size'          => array_get($grDtl, "size", null),
                        'lot'           => $grDtl['lot'],
                        'ttl'           => 0,
                        'allocated_qty' => 0,
                        'dmg_qty'       => 0,
                        'avail'         => 0,
                        'back_qty'      => 0,
                        'upc'           => array_get($grDtl, "upc", 'NA'),
                        'crs_doc_qty'   => 0,
                        'created_at'    => $created_at,
                        'updated_at'    => $created_at
                    ];
                }
            }

            if (!empty($insertInvtSmr)) {
                $this->inventorySummaryModel->refreshModel();
                $this->inventorySummaryModel->getModel()->insert(array_values($insertInvtSmr));
            }
        }
    }
}
