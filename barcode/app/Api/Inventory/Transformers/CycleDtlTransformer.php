<?php

namespace App\Api\Inventory\Transformers;

use App\Api\Inventory\Models\CycleDtlModel;
use App\Api\Inventory\Models\CycleHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\CycleDtl;

class CycleDtlTransformer extends TransformerAbstract
{
    /*
     * Transform CycleHdr
     * 
     * @param object $CycleHdr
     * @return array
     */
    public function transform(CycleDtl $cycleDtl)
    {
        $listCartons = object_get($cycleDtl, 'systemLocation.cartons', '');
        $listUCC128 = [];
        if(!empty($listCartons)) {
            foreach ($listCartons as $carton) {
                $listUCC128[] = $carton['ucc128'];
            }
        }
        return [
            'cycle_dtl_id'       => $cycleDtl->cycle_dtl_id,
            'cycle_hdr_id'       => $cycleDtl->cycle_hdr_id,
            'whs_id'             => $cycleDtl->whs_id,
            'cus_id'             => $cycleDtl->cus_id,
            'item_id'            => $cycleDtl->item_id,
            'sku'                => $cycleDtl->sku,
            'size'               => $cycleDtl->size,
            'color'              => $cycleDtl->color,
            'lot'                => $cycleDtl->lot,
            'pack'               => $cycleDtl->pack,
            'remain'             => $cycleDtl->remain,
            'sys_qty'            => $cycleDtl->sys_qty,
            'act_qty'            => $cycleDtl->act_qty,
            'sys_loc_id'         => $cycleDtl->sys_loc_id,
            'sys_loc_name'       => $cycleDtl->sys_loc_name,
            'act_loc_id'         => $cycleDtl->act_loc_id,
            'act_loc_name'       => $cycleDtl->act_loc_name,
            'cycle_dtl_sts'      => $cycleDtl->cycle_dtl_sts,
            'is_new_sku'         => $cycleDtl->is_new_sku,
            'created_by'         => $cycleDtl->created_by,
            'updated_by'         => $cycleDtl->updated_by,
            'created_at'         => $cycleDtl->created_at,
            'updated_at'         => $cycleDtl->updated_at,
            'sts'                => $cycleDtl->sts,
            'deleted'            => $cycleDtl->deleted,
            'deleted_at'         => $cycleDtl->deleted_at,
            'whs_name'           => object_get($cycleDtl, 'warehouse.whs_name', ''),
            'cus_name'           => object_get($cycleDtl, 'customer.cus_name', ''),
            'ttlCtn'             => strtoupper(object_get($cycleDtl, 'cycleHdr.count_by',
                '')) == CycleHdrModel::COUNT_BY_CARTON ?
                $cycleDtl->sys_qty : ceil($cycleDtl->sys_qty / $cycleDtl->remain),
            'ttlPieces'          => strtoupper(object_get($cycleDtl, 'cycleHdr.count_by',
                '')) == CycleHdrModel::COUNT_BY_CARTON ?
                $cycleDtl->sys_qty * $cycleDtl->remain : $cycleDtl->sys_qty,
            'cycle_dtl_sts_name' => CycleDtlModel::$arrCycleDtlStatus[$cycleDtl->cycle_dtl_sts],
            'ucc128'             => $listUCC128,
            'plt_rfid' => $cycleDtl->plt_rfid ?? $cycleDtl->lpn_carton
        ];
    }
}
