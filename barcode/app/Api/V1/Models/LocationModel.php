<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\Zone;
use Wms2\UserInfo\Data;

class LocationModel extends AbstractModel
{
    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    protected $itemModel;
    protected $cartonModel;
    protected $palletModel;
    protected $inventoryModel;
    protected $palletSuggestLocationModel;
    protected $grDtlModel;
    protected $grHdrModel;

    public function __construct(
        Location $model = null,
        ItemModel $itemModel,
        PalletModel $palletModel,
        InventoriesModel $inventoriesModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        GoodsReceiptModel $goodsReceiptModel,
        GoodsReceiptDtlModel $goodsReceiptDtlModel,
        CartonModel $cartonModel
    )
    {
        $this->model = ($model) ?: new Location();
        $this->itemModel = $itemModel;
        $this->palletModel = $palletModel;
        $this->cartonModel = $cartonModel;
        $this->inventoryModel = $inventoriesModel;
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->grHdrModel = $goodsReceiptModel;
        $this->grDtlModel = $goodsReceiptDtlModel;
    }

    public function checkLocationByLocCode($locCode)
    {
        return $this->model->where('loc_code', $locCode)->count();
    }

    public function getLocationByLocCode($isFull, $locCode, $whsId, $pltType, $blockStack = false)
    {
        $query = $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('loc_type.loc_type_code', 'RAC')
            ->leftJoin('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->select([
                'location.loc_id',
                'location.loc_code',
                'location.loc_alternative_name',
                'location.loc_sts_code',
                'location.spc_hdl_code',
                'pallet.rfid',
                'customer_zone.cus_id',
                'location.loc_zone_id',
            ])
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->where('location.loc_whs_id', $whsId);
            //->where('location.plt_type', $pltType);

        if ($blockStack) {
            $query->where('location.loc_code', 'LIKE', $locCode . "%");
            $query->whereNull('pallet.loc_id');
            $query->where('location.loc_sts_code', 'AC');
            $query->where('is_block_stack', 1);
            //$query->orderBy('location.loc_code', 'ASC');
        } else {
            $query->where('location.loc_code', $locCode);
        }

        //Full pallet suggestion
        if ($isFull) {
            $query->orderBy(DB::Raw("location.loc_code REGEXP '([A])[1-2]$', location.loc_code"), 'ASC');
        } else {
            $query->orderBy(DB::Raw("location.loc_code REGEXP '([B-Z])[1-2]$', location.loc_code"), 'ASC');
        }

        return $query->first();
    }

    public function getSuggest($locIds)
    {
        $query = $this->make(['cartons']);
        $query->whereHas('cartons', function ($q) {
            $q->where('ctn_sts', 'AC')
                ->whereNull('rfid');
        });

        return $query->select(['loc_code', 'loc_alternative_name', 'loc_id'])
            ->whereIn('loc_id', $locIds)->get();
    }

    /**
     * @param $locCode
     * @param $whsId
     *
     * @return mixed
     */
    public function getByLocCode($locCode, $whsId)
    {
        if (!is_array($locCode)) {
            $locCode = [$locCode];
        }

        return $this->model
            ->select(['loc_id', 'loc_code'])
            ->whereIn('loc_code', $locCode)
            ->where('loc_whs_id', $whsId)
            ->get();
    }

    public function getDynZone($whsId, $cusId)
    {
        /*
         * Check config Dynamic zone customer
         */
//        $chkDynZone = DB::table('cus_config')->where([
//            "whs_id"      => $whsId,
//            "cus_id"      => $cusId,
//            "config_name" => "DNZ",
//            "ac"          => "Y",
//        ])->value('config_value');
//
//        if (empty($chkDynZone)) {
//            return false;
//        }

        $customerObj = Customer::where('cus_id', $cusId)->first();

        $zoneCode = $customerObj->cus_code . "-D";
        $zoneName = $customerObj->cus_code . " - Dynamic Zone";
        $zoneObj = Zone::where('zone_code', $zoneCode)->where("zone_whs_id", $whsId)->first();

        if ($zoneObj) {
            $zoneObj->zone_num_of_loc += 1;
            $zoneObj->save();

            return $zoneObj->zone_id;
        }

        $colorCode = DB::table('customer_color')->where('cus_id', $cusId)
            ->value('cl_code');

        $arrData = [
            "zone_name"       => $zoneName,
            "zone_code"       => $zoneCode,
            "zone_whs_id"     => $whsId,
            "zone_type_id"    => 5,
            "dynamic"         => 1,
            "zone_min_count"  => 1,
            "zone_max_count"  => 100,
            "zone_color"      => $colorCode ?? '#ffffff',
            "zone_num_of_loc" => 1,
            "created_at"      => time(),
            "updated_at"      => time(),
            "created_by"      => Data::getCurrentUserId(),
            "updated_by"      => Data::getCurrentUserId(),
            "deleted_at"      => 915148800,
            "deleted"         => 0
        ];

        $dZone = DB::table('zone')
            ->where('zone_name', $zoneName)
            ->where('zone_whs_id', $whsId)
            ->where('zone_type_id', 5)
            ->where('dynamic', 1)
            ->where('deleted_at', 915148800)
            ->where('deleted', 0)
            ->first();

        if(empty($dZone)) {
            $zoneId = DB::table('zone')->insertGetId($arrData);
        }else{
            $zoneId = $dZone['zone_id'];
        }

        $cusZone = DB::table('customer_zone')
            ->where('cus_id', $cusId)
            ->where('zone_id', $zoneId)
            ->where('deleted', 0)
            ->where('deleted_at', 915148800)
            ->first();

        if(empty($cusZone)) {
            //add customer_zone
            DB::table('customer_zone')->insert([
                'zone_id'    => $zoneId,
                'cus_id'     => $cusId,
                'created_at' => time(),
                'updated_at' => time(),
                "created_by" => Data::getCurrentUserId(),
                "updated_by" => Data::getCurrentUserId(),
                "deleted_at" => 915148800,
                'deleted'    => 0
            ]);
        }

        return $zoneId;
    }


    public function getActQtyOdrDtlAllocation($wvDtlId)
    {
        $result = DB::table('odr_dtl_allocation')
            ->select('qty', 'loc_code', 'plt_rfid')
            ->where('wv_dtl_id', $wvDtlId)
            ->first();

        return $result;

    }

    public function pickPalletUpdateActiveLocationByWvDtlId($whsId,$wvDtl, $locId)
    {
        // Update Pallet Active
        Pallet::where('loc_id', $locId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->update([
                'plt_sts'    => 'AC',
                'updated_by' => Data::getCurrentUserId()
            ]);

        // Update Carton To Active
        Carton::where('loc_id', $locId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->update([
                'ctn_sts'    => 'AC',
                'updated_by' => Data::getCurrentUserId()
            ]);

        // Update Location AC
        $result = $this->model
            ->where('loc_id', $locId)
            ->where('loc_whs_id', $whsId)
            ->where('deleted', 0)
            ->update([
                'loc_sts_code' => 'AC',
                'updated_by'   => Data::getCurrentUserId()
            ]);

        return $result;

    }

    public function getLocByLocCode($attribute,$whsId) {
        $query = $this->getModel()
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->where('loc_type.loc_type_code', 'RAC')
            ->where('location.loc_whs_id', $whsId)
            ->where('location.loc_code', $attribute['loc_code'])
            ->whereIn('location.loc_sts_code', ['AC','RG'])
            ->select([
                'location.loc_id',
                'location.loc_code',
                'location.loc_alternative_name',
                'location.loc_sts_code',
                'customer_zone.cus_id',
                'location.loc_zone_id',
                'pallet.plt_id',
                'pallet.plt_num'
            ])
            ->first();
        return $query;
    }

    public function getLocationPutAwayHistory($itemId)
    {
        if (empty($itemId)) {
            return null;
        }

        $location = $this->model->join('putaway_hist', 'putaway_hist.loc_id', '=', 'location.loc_id')
            ->where('putaway_hist.item_id', intval($itemId))
            ->whereNull('location.parent_id')
            ->whereIn('location.loc_sts_code', ['AC', 'RG'])
            //->groupBy('putaway_hist.loc_id')
            //->havingRaw('SUM(putaway_hist.ttl_lpn) < location.max_lpn and COUNT(DISTINCT putaway_hist.item_id) < 20')
            ->orderBy('location.aisle', 'ASC')
            ->orderBy('location.row', 'ASC')
            ->orderBy('location.level', 'ASC')
            ->orderBy('location.bin', 'ASC')
            ->selectRaw('location.loc_id, location.loc_code, location.max_lpn')
            ->first();

        return $location;
    }

    public function getSuggestionLocation($input)
    {
        $model = $this->model->where('location.loc_whs_id', $input['whs_id'])
            ->where('location.loc_id', '<>', 3161342)
            ->whereNull('location.parent_id')
            ->whereIn('location.loc_sts_code', ['AC', 'RG'])
            ->whereNotExists(function($query) {
                $query->select(DB::raw(1))
                    ->from('putaway_hist')
                    ->where('location.loc_id', '=', 'putaway_hist.loc_id');
            })
            ->orderBy('location.aisle', 'ASC')
            ->orderBy('location.row', 'ASC')
            ->orderBy('location.level', 'ASC')
            ->orderBy('location.bin', 'ASC');

        if (!empty($input['spc_hdl_code'])) {
            $model->whereIn('location.spc_hdl_code', $input['spc_hdl_code']);
        }

        $location = $model->select(['loc_id', 'loc_code'])->first();

        return $location;
    }

    public function getSuggestLocation($attribute, $whsId) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        if(!empty($attribute['item_id'])) {
            $item = DB::table('item')
                        ->join('item_meta', 'item_meta.itm_id', 'item.item_id')
                        ->where('item_meta.itm_id', $attribute['item_id'])
                        ->select(['item.item_id', 'item_meta.value as spc_hdl_code'])
                        ->first();

//            $item_loc = DB::table('cartons')
//                            ->select('cartons.loc_code')
//                            ->join('location', 'location.loc_id', 'cartons.loc_id')
//                            ->where('cartons.item_id', $attribute['item_id'])
//                            ->where('cartons.whs_id', $attribute['whs_id'])
//                            ->where('cartons.cus_id', $attribute['cus_id'])
//                            ->whereNull('location.parent_id')
//                            ->whereIn('location.loc_sts_code', ['AC', 'RG'])
//                            ->whereNotNull('cartons.loc_code')
//                            ->whereNotExists(function ($query) use ($attribute) {
//                                $query->select(DB::raw(1))
//                                    ->from('cartons')
//                                    ->where('location.spc_hdl_code', '<>', 'RAC')
//                                    ->whereRaw('cartons.whs_id = ' . $attribute['whs_id'])
//                                    ->whereRaw('cartons.loc_code = location.loc_code')
//                                    ->whereIn('cartons.ctn_sts', ['AC', 'RG'])
//                                    ->groupBy('cartons.loc_code')
//                                    ->havingRaw('COUNT(DISTINCT cartons.lpn_carton) >= location.max_lpn');
//                            })
//                            ->groupBy('cartons.loc_code', 'cartons.cus_id')
//                            ->orderBy('location.aisle', 'ASC')
//                            ->orderBy('location.level', 'ASC')
//                            ->orderBy('location.row', 'ASC')
//                            ->orderBy('location.bin', 'ASC')
//                            ->orderBy('location.loc_id', 'ASC')
//                            ->take(1)
//                            ->get();

            $item_loc = DB::table('cartons')
                ->select(DB::Raw('cartons.loc_code, COUNT(DISTINCT cartons.lpn_carton), IF(location.max_lpn IS NULL, 0, location.max_lpn) as max_lpn'))
                ->join('location', 'location.loc_id', 'cartons.loc_id')
                ->where('cartons.item_id', $attribute['item_id'])
                ->where('cartons.whs_id', $attribute['whs_id'])
                ->where('cartons.cus_id', $attribute['cus_id'])
                ->whereIn('location.loc_sts_code', ['AC', 'RG'])
                ->whereNotNull('cartons.loc_code')
                ->groupBy('cartons.loc_code', 'cartons.cus_id')
                ->havingRaw('COUNT(DISTINCT cartons.lpn_carton) < max_lpn')
                ->orderBy('location.aisle', 'ASC')
                ->orderBy('location.level', 'ASC')
                ->orderBy('location.row', 'ASC')
                ->orderBy('location.bin', 'ASC')
                ->orderBy('location.loc_id', 'ASC')
                ->first();

            if ($item_loc && count($item_loc) > 0) {
                return $item_loc;
            }

            $attribute['spc_hdl_code'] = json_decode($item['spc_hdl_code']);

            return $this->getLocByCustomerItem($attribute, $whsId, $limit = 1, true);
        }

        return $this->getLocByCustomerItem($attribute, $whsId, $limit = 1);
    }

    public function getLocationDynamicZone($input, $whsId)
    {
        $query = DB::table('customer')
            ->select('location.loc_code')
            ->join('customer_zone', 'customer_zone.cus_id', '=', 'customer.cus_id')
            ->join('location', 'location.loc_zone_id', '=', 'customer_zone.zone_id')
            ->where('customer.cus_id', '=', $input['cus_id'])
            ->where('location.loc_whs_id', '=', $input['whs_id'])
            ->where('location.is_block_stack', '=', 0)
            ->whereNull('location.loc_cus_id');

        if (!empty($input['item_id'])) {
            $item = DB::table('item')
                ->join('item_meta', 'item_meta.itm_id', 'item.item_id')
                ->where('item_meta.itm_id', $input['item_id'])
                ->select(['item.item_id', 'item_meta.value as spc_hdl_code'])
                ->first();

            $meta_value = json_decode($item['spc_hdl_code']);

            $spc_hdl_code = [];
            if (!empty($meta_value)) {
                foreach ($meta_value as $code => $value) {
                    if ($value == 1) {
                        $spc_hdl_code[] = $code;
                    }
                }
                $query->whereIn('location.spc_hdl_code', $spc_hdl_code);
            }
        }

        $locations = $query->first();

        return $locations;
    }

    public function getLocationDynamicZoneNotInCustomer($input, $whsId)
    {
        $query = DB::table('location')
            ->select('location.loc_code')
            ->where('location.loc_whs_id', '=', $input['whs_id'])
            ->where('location.is_block_stack', '=', 0)
            ->whereNotExists(function ($query) use ($input) {
                $query->select(DB::raw(1))
                    ->from('cartons')
                    ->whereRaw('cartons.whs_id = ' . $input['whs_id'])
                    ->whereRaw('cartons.loc_id = location.loc_id');
            });

        if (!empty($input['item_id'])) {
            $item = DB::table('item')
                ->join('item_meta', 'item_meta.itm_id', 'item.item_id')
                ->where('item_meta.itm_id', $input['item_id'])
                ->select(['item.item_id', 'item_meta.value as spc_hdl_code'])
                ->first();

            $meta_value = json_decode($item['spc_hdl_code']);

            $spc_hdl_code = [];
            if (!empty($meta_value)) {
                foreach ($meta_value as $code => $value) {
                    if ($value == 1) {
                        $spc_hdl_code[] = $code;
                    }
                }
                $query->whereIn('location.spc_hdl_code', $spc_hdl_code);
            }
        }

        $query->orderBy('location.aisle', 'ASC')
            ->orderBy('location.level', 'ASC')
            ->orderBy('location.row', 'ASC')
            ->orderBy('location.bin', 'ASC')
            ->orderBy('location.loc_id', 'ASC')
            ->take(1)
            ->get();

        $locations = $query->first();

        return $locations;
    }

    protected function getLocByCustomerItem($attribute, $whsId, $limit = 1, $hasItem = false) {
        $spc_hdl_code = [];
        if ($attribute['spc_hdl_code']) {
            foreach ($attribute['spc_hdl_code'] as $code => $value) {
                if ($value == 1) {
                    $spc_hdl_code[] = $code;
                }
            }
        }

//        $query = $this->getModel()
//                    ->select(['parent.loc_code', 'parent.spc_hdl_code', 'location.loc_cus_id'])
//                    ->join('customer_zone', function ($join) {
//                        $join->on('customer_zone.cus_id', '=', 'location.loc_cus_id');
//                        $join->on('customer_zone.zone_id', '=', 'location.loc_zone_id');
//                    })
//                    ->join('location as parent', 'parent.loc_id', 'location.parent_id')
//                    ->where('location.loc_whs_id', $attribute['whs_id'])
//                    ->where('location.loc_cus_id', $attribute['cus_id'])
//                    ->whereIn('parent.loc_sts_code', ['AC', 'RG'])
//                    ->whereNotExists(function ($query) use ($attribute) {
//                        $query->select(DB::raw(1))
//                            ->from('pallet')
//                            ->where('parent.spc_hdl_code', '<>', 'RAC')
//                            ->where('pallet.whs_id', '=', $attribute['whs_id'])
//                            ->whereRaw('pallet.loc_id = parent.loc_id and pallet.cus_id <> ' . $attribute['cus_id'])
//                            ->groupBy('pallet.loc_id')
//                            ->havingRaw('COUNT(pallet.cus_id) >= ?', [20]);
//                    })
//                    ->whereNotExists(function ($query) use ($attribute) {
//                        $query->select(DB::raw(1))
//                            ->from('cartons')
//                            ->where('parent.spc_hdl_code', '<>', 'RAC')
//                            ->whereRaw('cartons.whs_id = ' . $attribute['whs_id'])
//                            ->whereRaw('cartons.loc_id = parent.loc_id')
//                            ->whereIn('cartons.ctn_sts', ['AC', 'RG'])
//                            ->groupBy('cartons.loc_id')
//                            ->havingRaw('COUNT(DISTINCT cartons.lpn_carton) >= parent.max_lpn');
//                    });

        $locations = DB::table('cartons')
            ->select(DB::Raw('cartons.loc_code, COUNT(DISTINCT cartons.lpn_carton), COUNT( DISTINCT cartons.cus_id ) as cids, IF(location.max_lpn IS NULL, 0, location.max_lpn) as max_lpn, location.spc_hdl_code'))
            ->join('location', 'location.loc_id', 'cartons.loc_id')
            ->where('cartons.whs_id', $attribute['whs_id'])
            ->where('cartons.cus_id', $attribute['cus_id'])
            ->whereIn('location.spc_hdl_code', $spc_hdl_code)
            ->whereIn('location.loc_sts_code', ['AC', 'RG'])
            ->whereNotNull('cartons.loc_code')
            ->groupBy('cartons.loc_code', 'cartons.cus_id')
            ->havingRaw("COUNT(DISTINCT cartons.lpn_carton) < IF(location.spc_hdl_code = 'RAC', 100000, max_lpn) AND cids < 20")
            ->orderBy('location.aisle', 'ASC')
            ->orderBy('location.level', 'ASC')
            ->orderBy('location.row', 'ASC')
            ->orderBy('location.bin', 'ASC')
            ->orderBy('location.loc_id', 'ASC')
            ->first();

        return $locations;
    }

    protected function getLocationNearByCustomerASC($location, $limit = 2, $type = 1) {
        $query = $this->getModel()
            ->join("customer_zone","customer_zone.zone_id","=","location.loc_zone_id")
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin("pallet","pallet.loc_id","=","location.loc_id")
            ->where('loc_type.loc_type_code', 'RAC')
            ->where("location.aisle",">=",$location->aisle)
            ->where("location.row",">=",$location->row)
            ->where("location.loc_whs_id", $location->loc_whs_id)
            ->where("location.spc_hdl_code", $location->spc_hdl_code)
            ->whereNull("pallet.loc_id")
            ->where("customer_zone.cus_id",$location->cus_id)
            ->whereIn("location.loc_sts_code", ["RG",'AC']);

        return  $query->orderBy("location.aisle")
            ->orderBy("location.row")
            ->orderBy("location.level")
            ->orderBy("location.bin")
            ->select("location.loc_code")
            ->limit($limit)
            ->select("location.loc_code")
            ->get();
    }

    protected function getLocationNearByCustomerDESC($location, $limit = 2, $type = 1) {
        $query = $this->getModel()
            ->join("customer_zone","customer_zone.zone_id","=","location.loc_zone_id")
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin("pallet","pallet.loc_id","=","location.loc_id")
            ->where('loc_type.loc_type_code', 'RAC')
            ->where("location.aisle",">=",$location->aisle)
            ->where("location.row","<=",$location->row)
            ->where("location.loc_whs_id", $location->loc_whs_id)
            ->where("location.spc_hdl_code", $location->spc_hdl_code)
            ->whereNull("pallet.loc_id")
            ->where("customer_zone.cus_id",$location->cus_id)
            ->whereIn("location.loc_sts_code", ["RG",'AC']);

        return  $query->orderBy("location.aisle")
            ->orderBy("location.row","DESC")
            ->orderBy("location.level")
            ->orderBy("location.bin")
            ->select("location.loc_code")
            ->limit($limit)
            ->select("location.loc_code")
            ->get();
    }

    protected function getLocByCustomer($attribute,$whs_id, $limit = 2) {
        $query = $this->getModel()
            ->join("customer_zone","customer_zone.zone_id","=","location.loc_zone_id")
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin("pallet","pallet.loc_id","=","location.loc_id")
            ->where('loc_type.loc_type_code', 'RAC')
            ->where("location.loc_whs_id", $whs_id)
            ->whereNull("pallet.loc_id")
            ->where("customer_zone.cus_id",$attribute['cus_id'])
            ->whereIn("location.loc_sts_code", ["RG",'AC']);

        if(!empty($attribute['spc_hdl_code'])) {
            $query->where("location.spc_hdl_code", $attribute['spc_hdl_code']);
        }

        return $query->orderBy("location.aisle")
            ->orderBy("location.row")
            ->orderBy("location.level")
            ->orderBy("location.bin")
            ->select("location.*")
            ->limit($limit)
            ->get();
    }

    protected function getLocNotInCustomer($whs_id,$attribute, $limit = 2) {
        $query = $this->getModel()
            ->leftJoin("customer_zone","customer_zone.zone_id","=","location.loc_zone_id")
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftJoin("pallet","pallet.loc_id","=","location.loc_id")
            ->where("location.loc_whs_id", $whs_id)
            ->where('loc_type.loc_type_code', 'RAC')
            ->whereNull("pallet.loc_id")
            ->whereNull("location.loc_zone_id")
            ->whereIn("location.loc_sts_code", ["RG",'AC']);

        if(!empty($attribute['spc_hdl_code'])) {
            $query->where("location.spc_hdl_code", $attribute['spc_hdl_code']);
        }

        return $query->select("location.loc_code")
            ->limit($limit)
            ->get();
    }

    public function submitPartialPutaway($attribute) {
        DB::beginTransaction();

        $location = $attribute['location'];
        //Check Dynamic Zone
        if (is_null($location->loc_zone_id)) {

            $zoneId = $this->getDynZone($attribute['whsId'], $attribute['cus_id']);
            if (!$zoneId) {
                return ["is_error" => 1,"msg" => "This customer don't support dynamic zone."];
            }

            $location->loc_zone_id = $zoneId;
            $location->save();

            if($location->is_block_stack == 1) {
                // Update zone to all children location
                $this->getModel()->where("parent_id", $location->loc_id)->update(["loc_zone_id" => $zoneId]);
                $location_child = $this->getFirstChildEmpty($location);
                if(!empty($location_child)) {
                    $location = $location_child;
                }
            }


            $zone = DB::table("zone")->where("zone_id", $zoneId)->first();
            $zone_num_of_loc = $zone['zone_num_of_loc'] + 1;
            DB::table("zone")->where("zone_id", $location->loc_zone_id)->update(['zone_num_of_loc'=> $zone_num_of_loc]);
        }

        $putaway_pallet = $this->putawayPallet($attribute);

        $cartonPutawayParams = [
            "location"      => $location,
            "cus_id"        => $attribute['cus_id'],
            "whs_id"        => $attribute['whsId'],
            "pallet_to"     => $putaway_pallet['pallet'],
            "type"          => $putaway_pallet['type']
        ];

        if($cartonPutawayParams['type'] == 2) {
            $cartonPutawayParams['tote'] = $attribute['pallet'];
        }
        // Updata Inventory
        $getCtnByPlt = $this->cartonModel->getCartonByPlt($attribute, $attribute['whsId']);
        $arrItems = array_pluck($getCtnByPlt,'qty_ttl', 'item_id');
        // foreach ($arrItems as $itemId => $itemQty) {
        //     $this->inventoryModel->updateInventory($attribute['whsId'],$attribute['cus_id'],$itemId,$itemQty);
        // }

        //Update Pallet
        $grHdr = $this->updateQtyPallet($cartonPutawayParams);

        // Putaway Carton
        $this->cartonModel->updateCartonPartialPutaway($cartonPutawayParams);

        if(!empty($grHdr)) {
            if ($grHdr->gr_sts == 'RE') {
                GoodsReceipt::where([
                    'gr_hdr_id' => $grHdr->gr_hdr_id
                ])
                    ->whereRaw("
                        (
                            SELECT COUNT(pal_sug_loc.act_loc_id) 
                            FROM pal_sug_loc 
                            JOIN gr_dtl ON pal_sug_loc.`gr_dtl_id` = gr_dtl.`gr_dtl_id`
                            WHERE pal_sug_loc.act_loc_id IS NOT NULL
                                AND pal_sug_loc.gr_hdr_id = gr_hdr.gr_hdr_id
                                AND gr_dtl.`gr_dtl_sts` = 'RG'
                        ) = 
                        (
                            SELECT SUM(gr_dtl.gr_dtl_plt_ttl) FROM gr_dtl
                            WHERE gr_dtl.gr_hdr_id = gr_hdr.gr_hdr_id
                            AND gr_dtl.`gr_dtl_sts` = 'RG'
                        )
                    ")
                    ->update(['gr_hdr.putaway' => 1]);
            } else {
                GoodsReceipt::where([
                    'gr_hdr_id' => $grHdr->gr_hdr_id,
                    'created_from' => 'GUN'
                ])
                    ->whereRaw("
                        (
                            SELECT COUNT(pal_sug_loc.act_loc_id) 
                            FROM pal_sug_loc 
                            JOIN gr_dtl ON pal_sug_loc.`gr_dtl_id` = gr_dtl.`gr_dtl_id`
                            WHERE pal_sug_loc.act_loc_id IS NOT NULL
                                AND pal_sug_loc.gr_hdr_id = gr_hdr.gr_hdr_id
                                AND gr_dtl.`gr_dtl_sts` = 'RG'
                        ) = 
                        (
                            SELECT SUM(gr_dtl.gr_dtl_plt_ttl) FROM gr_dtl
                            WHERE gr_dtl.gr_hdr_id = gr_hdr.gr_hdr_id
                            AND gr_dtl.`gr_dtl_sts` = 'RG'
                        )
                    ")
                    ->update(['gr_hdr.putaway' => 1]);
            }
            GoodsReceipt::where(['gr_hdr_id' => $grHdr->gr_hdr_id])->update(['gr_hdr.putaway_dt'=>time()]);
        }
        $location->loc_sts_code = "RG";
        $location->save();

        $userId = Data::getCurrentUserId();

        $asn = DB::table('asn_hdr')->where('asn_hdr_id', $grHdr->asn_hdr_id)->first();

        DB::table('rpt_labor_tracking')->insert([
            'user_id'       => $userId,
            'cus_id'        => object_get($grHdr, 'cus_id', null),
            'whs_id'        => $attribute['whsId'],
            'trans_num'     => object_get($grHdr, 'gr_hdr_num', null),
            'trans_dtl_id'  => object_get($grHdr, 'gr_hdr_id'),
            'start_time'    => strtotime('-5 minutes'),
            'end_time'      => time(),
            'lt_type'       => "IB",
            'owner'         => array_get($asn, "asn_hdr_num", null)
        ]);

        DB::commit();
    }

    public function updatePutAwayHistory($input)
    {
        if (!empty($input['item_id'])) {
            $data = DB::table('putaway_hist')->where('item_id', $input['item_id'])->where('loc_id', $input['loc_id'])->first();
            if (empty($data)) {
                DB::table('putaway_hist')->insert($input);
            } else {
                $dataUpdate = [
                    'ttl_lpn' => intval($data['ttl_lpn']) > 0 ? ($data['ttl_lpn'] + 1) : 1
                ];
                DB::table('putaway_hist')->where('item_id', $input['item_id'])->where('loc_id', $input['loc_id'])->update($dataUpdate);
            }
        }
        DB::statement('
            UPDATE putaway_hist
            JOIN location ON location.loc_id = putaway_hist.loc_id 
            SET putaway_hist.loc_sts_code = location.loc_sts_code 
            WHERE location.deleted = 0;
        ');
    }

    /*
     * @Putaway for Pallet
     * @Param: LocationModel $location, $plt_num
     *
     * return type, pallet
     */
    protected function putawayPallet($attribute) {
        if(in_array($attribute['location']->spc_hdl_code,['SHE','BIN', 'RAC'])) {
            $pallet_loc = $this->palletModel->getFirstWhere([
                "loc_id" => $attribute['location']->loc_id,
                'cus_id' => $attribute['cus_id']
            ]);
            if(empty($pallet_loc)) {
                $plt_num = $this->palletModel->generatePltNum();
                $pallet_loc = $this->palletModel->create([
                    "plt_num"   => $plt_num,
                    "loc_id"    => $attribute['location']->loc_id,
                    "loc_code"  => $attribute['location']->loc_code,
                    "whs_id"    => $attribute['whsId'],
                    "cus_id"    => $attribute['cus_id'],
                    "ctn_ttl"   => 0,
                    "loc_name"  => $attribute['location']->loc_code]);

            }
            $plt_num = array_get($pallet_loc, 'plt_num');
            $plt_id = array_get($pallet_loc, 'plt_id');
            $listCartons = DB::table("cartons")->select([
                "cartons.sku", "cartons.color", "cartons.size", "cartons.ctn_pack_size", "pallet.ctn_ttl", "cartons.piece_remain", DB::raw('count(*) as count')
            ])
                ->join("pallet", "pallet.plt_id", "cartons.plt_id")
                ->where("cartons.plt_id", $attribute['pallet']->plt_id)
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('rpt_pallet')
                        ->whereRaw('cartons.plt_id = rpt_pallet.plt_id and cartons.sku = rpt_pallet.sku');
                })
                ->groupBy('cartons.sku', 'cartons.plt_id')->get();
            if (count($listCartons)<1) {
                $listCartons = DB::table("cartons")->select([
                    "cartons.sku", "cartons.color", "cartons.size", "cartons.ctn_pack_size", "pallet.ctn_ttl", "cartons.piece_remain", DB::raw('count(*) as count')
                ])
                    ->join("pallet", "pallet.plt_id", "cartons.plt_id")
                    ->where("cartons.plt_id", $attribute['pallet']->plt_id)
                    ->groupBy('cartons.sku', 'cartons.plt_id')->get();
            }

            $userId = Data::getCurrentUserId();

            foreach ($listCartons as $carton) {
                $cusInfo = DB::table('customer')->where('cus_id', $attribute['cus_id'])->first();
                $cusName = array_get($cusInfo,'cus_name','');
                $cusCode = array_get($cusInfo,'cus_code','');

                DB::table("rpt_pallet")->insert([
                    "plt_id"            => $plt_id,
                    "plt_num"           => $plt_num,
                    "loc_id"            => $attribute['location']->loc_id,
                    "loc_code"          => $attribute['location']->loc_code,
                    "whs_id"            => $attribute['whsId'],
                    "cus_id"            => $attribute['cus_id'],
                    "cus_code"          => $cusCode,
                    "cus_name"          => $cusName,
                    "ctn_ttl"           => 0,
                    "loc_name"          => $attribute['location']->loc_code,
                    "sku"               => array_get($carton, 'sku', ''),
                    "color"             => array_get($carton, 'color', ''),
                    "size"              => array_get($carton, 'size', ''),
                    "pack"              => array_get($carton, 'ctn_pack_size', ''),
                    "current_ctns"      => array_get($carton, 'ctn_ttl', ''),
                    "current_pieces"    => array_get($carton, 'piece_remain', '') * array_get($carton, 'count', ''),
                    "level"             => $attribute['location']->level,
                    "row"               => $attribute['location']->row,
                    "aisle"             => $attribute['location']->aisle,
                    'created_at'        => time(),
                    'created_by'        => $userId,
                    'updated_at'        => time(),
                    'updated_by'        => $userId,
                    'deleted'           => 0,
                    'deleted_at'        => 915148800
                ]);
            }


            return [
                "pallet" => $pallet_loc,
                "type"  => 2
            ];
        }

        return [
            "pallet" => $attribute['pallet'],
            "type"  => 1
        ];

    }

    public static function updateStatusLocationPutAwayHistory()
    {
        DB::statement('
            UPDATE putaway_hist
            JOIN location ON location.loc_id = putaway_hist.loc_id 
            SET putaway_hist.loc_sts_code = location.loc_sts_code 
            WHERE location.deleted = 0;
        ');
    }

    protected function updateQtyPallet($cartonPutawayParams) {
        $ctn_ttl = $cartonPutawayParams['pallet_to']->ctn_ttl;
        $pallet_to_data = $cartonPutawayParams['pallet_to']->toArray();
        $cusInfo = DB::table('customer')->where('cus_id',$cartonPutawayParams['pallet_to']->cus_id)->first();
        $cus_name = array_get($cusInfo,'cus_name','');
        $cus_code = array_get($cusInfo,'cus_code','');
        $rpt_pallet['cus_name'] = $cus_name;
        $rpt_pallet['cus_code'] = $cus_code;
        $rpt_pallet['rfid'] = array_get($pallet_to_data,'plt_num','');
        $rpt_pallet['loc_sts_code'] = "RG";
        DB::table("rpt_pallet")->where('plt_id', $cartonPutawayParams['pallet_to']->plt_id)->update($rpt_pallet);
        if(!empty($cartonPutawayParams['tote'])) {
            $ctn_ttl += $cartonPutawayParams['tote']->ctn_ttl;
            $grHdr = $this->grHdrModel->getFirstWhere(['gr_hdr_id' => $cartonPutawayParams['tote']->gr_hdr_id]);

            // Create Pallet Suggest location for GR
            $countCtns = DB::table("cartons")
                ->where("plt_id",$cartonPutawayParams['tote']->plt_id)->where("ctn_sts", "RG")
                ->groupBy("gr_dtl_id")
                ->select(DB::raw("COUNT(ctn_id) AS total_ctn, SUM(piece_remain) AS total_qty, gr_dtl_id"))->get();
            foreach ($countCtns as $countCtn) {
                $grDtl = $this->grDtlModel->getFirstWhere(['gr_dtl_id' => $countCtn['gr_dtl_id']]);
                $qtyCtn = [
                    "qty"       => $countCtn['total_qty'],
                    "ctn_ttl"   => $countCtn['total_ctn']
                ];
                $this->palletSuggestLocationModel->upsertWithGrDTl($cartonPutawayParams['pallet_to'], $grDtl, $grHdr,$qtyCtn);
            }

            $cartonPutawayParams['pallet_to']->ctn_ttl = $ctn_ttl;
            $cartonPutawayParams['pallet_to']->loc_id    = $cartonPutawayParams["location"]->loc_id;
            $cartonPutawayParams['pallet_to']->loc_code  = $cartonPutawayParams["location"]->loc_code;
            $cartonPutawayParams['pallet_to']->loc_name  = $cartonPutawayParams["location"]->loc_code;
            $cartonPutawayParams['pallet_to']->gr_dtl_id  = $cartonPutawayParams['tote']->gr_dtl_id;
            $cartonPutawayParams['pallet_to']->gr_hdr_id  = $cartonPutawayParams['tote']->gr_hdr_id;
            $cartonPutawayParams['pallet_to']->plt_sts = "RG";
            $cartonPutawayParams['pallet_to']->save();

            DB::table("rpt_pallet")->where('plt_id', $cartonPutawayParams['pallet_to']->plt_id)->update([
                "ctn_ttl"   => $ctn_ttl,
                "loc_id"    => $cartonPutawayParams["location"]->loc_id,
                "loc_code"  => $cartonPutawayParams["location"]->loc_code,
                "loc_name"    => $cartonPutawayParams["location"]->loc_code,
                "gr_dtl_id"    => $cartonPutawayParams['tote']->gr_dtl_id,
                "gr_hdr_id"   => $cartonPutawayParams['tote']->gr_hdr_id,
                "gr_hdr_num"    =>array_get($grHdr,'gr_hdr_num','')
                ]);

            $cartonPutawayParams['tote']->ctn_ttl = 0;
            $cartonPutawayParams['tote']->gr_dtl_id = null;
            $cartonPutawayParams['tote']->gr_hdr_id = null;
            $cartonPutawayParams['tote']->plt_sts = "AC";
            $cartonPutawayParams['tote']->save();

            DB::table("rpt_pallet")->where('plt_id', $cartonPutawayParams['tote']->plt_id)->update([
                "ctn_ttl"   => 0,
                "gr_dtl_id"    => null,
                "gr_hdr_id"   => null,
                "plt_sts"       => "AC"
            ]);

            return $grHdr;
        }

        //$grDtl = $this->grDtlModel->getFirstWhere(['gr_dtl_id' => $cartonPutawayParams['pallet_to']->gr_dtl_id]);
        $grHdr = $this->grHdrModel->getFirstWhere(['gr_hdr_id' => $cartonPutawayParams['pallet_to']->gr_hdr_id]);

        // Create Pallet Suggest location for GR
        $countCtns = DB::table("cartons")
            ->where("plt_id",$cartonPutawayParams['pallet_to']->plt_id)->where("ctn_sts", "RG")
            ->groupBy("gr_dtl_id")
            ->select(DB::raw("COUNT(ctn_id) AS total_ctn, SUM(piece_remain) AS total_qty, gr_dtl_id"))->get();

        $cartonPutawayParams['pallet_to']->loc_id    = $cartonPutawayParams["location"]->loc_id;
        $cartonPutawayParams['pallet_to']->loc_code  = $cartonPutawayParams["location"]->loc_code;
        $cartonPutawayParams['pallet_to']->loc_name  = $cartonPutawayParams["location"]->loc_code;
        $cartonPutawayParams['pallet_to']->plt_sts = "RG";
        $cartonPutawayParams['pallet_to']->save();

        DB::table("rpt_pallet")->where('plt_id', $cartonPutawayParams['pallet_to']->plt_id)->update([
            "loc_id"   => $cartonPutawayParams["location"]->loc_id,
            "loc_code"    => $cartonPutawayParams["location"]->loc_code,
            "loc_name"   => $cartonPutawayParams["location"]->loc_code
        ]);

        foreach ($countCtns as $countCtn) {
            $grDtl = $this->grDtlModel->getFirstWhere(['gr_dtl_id' => $countCtn['gr_dtl_id']]);
            $qtyCtn = [
                "qty"       => $countCtn['total_qty'],
                "ctn_ttl"   => $countCtn['total_ctn']
            ];
            $this->palletSuggestLocationModel->upsertWithGrDTl($cartonPutawayParams['pallet_to'], $grDtl, $grHdr,$qtyCtn);
        }

        return $grHdr;
    }

    public function getFirstChildEmpty($location) {
        return $this->getModel()
                    ->where("parent_id", $location->loc_id)
                    ->whereRaw(
                '0 = (SELECT COUNT(pallet.loc_id) FROM pallet WHERE pallet.loc_id = location.loc_id AND pallet.loc_id IS NOT NULL)'
                    )
                    ->where("is_block_stack",1)
                    ->orderBy("plt_lv","DESC")
                    ->first()
                    ;
    }
}
