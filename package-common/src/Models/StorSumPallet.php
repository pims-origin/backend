<?php

namespace Seldat\Wms2\Models;

class StorSumPallet extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stor_sum_plt';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'carton_id';

    /**
     * @var array
     */
    protected $fillable = [
        'plate_num',
        'whs_id',
        'cus_id',
        'start_event_id',
        'rcv_dt',
        'last_event_id',
        'last_active'
    ];

    
    public function pallet()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Pallet', 'plate_num', 'plate_num');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
    
    public function eventTracking()
    {
        return $this->belongsTo(__NAMESPACE__ . '\EventTracking', 'last_ctn_event_id', 'id');
    }
}


