<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:34
 */

namespace Seldat\Wms2\Models;

/**
 * @property mixed loc_id
 * @property mixed cus_id
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 *
 */
class Location0808 extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'location_0808';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'loc_id';


    /**
     * @var array
     */
    protected $fillable = [
        'loc_code',
        'loc_alternative_name',
        'loc_whs_id',
        'loc_zone_id',
        'loc_type_id',
        'loc_sts_code',
        'loc_available_capacity',
        'loc_width',
        'loc_length',
        'loc_height',
        'loc_max_weight',
        'loc_weight_capacity',
        'loc_min_count',
        'loc_max_count',
        'loc_desc',
        'rfid',
        'level',
        'row',
        'aisle',
        'bin',
        'area',
        'reserved_at',
        'is_block_stack'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customerZone()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerZone', 'zone_id', 'loc_zone_id');
    }

    public function customerWarehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerWarehouse', 'whs_id', 'loc_whs_id');
    }

    public function zone()
    {
        return $this->belongsTo(__NAMESPACE__ . '\WarehouseZone', 'loc_zone_id', 'zone_id');
    }

    public function zoneLocation()
    {
        return $this->hasOne(__NAMESPACE__ . '\Zone', 'zone_id', 'loc_zone_id');
    }

    public function locationType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\LocationType', 'loc_type_id', 'loc_type_id');
    }

    public function locationStatus()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ZoneLocationStatus', 'loc_sts_code', 'loc_sts_code');
    }

    public function locationStatusDetail()
    {
        return $this->belongsTo(__NAMESPACE__ . '\LocationStatusDetail', 'loc_id', 'loc_sts_dtl_loc_id');
    }

    public function pallet()
    {
        return $this->hasOne(__NAMESPACE__ . '\Pallet', 'loc_id', 'loc_id');
    }

    public function cycleDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\CycleDtl', 'loc_id', 'sys_loc_id');
    }

    public function cartons()
    {
        return $this->hasMany(__NAMESPACE__ . '\Carton', 'loc_id', 'loc_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'loc_whs_id');
    }
}
