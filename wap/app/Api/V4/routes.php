<?php

$api->group(['prefix' => '/v4/whs/{whsId:[0-9]+}/ib/pallet/', 'namespace' => 'App\Api\V4\Inbound\Pallet'], function ($api) {
    $api->put('scan-pallet',
        ['uses' => 'ScanPallet\Controllers\PalletController@assignCartonsToPallet']);
});

// wave pick
$api->group(['prefix' => '/v4/whs/{whsId:[0-9]+}/wave', 'namespace' => 'App\Api\V4\Outbound\Wavepick'], function ($api) {
    // update wave pick
    $api->put('/{wvId:[0-9]+}/update',
        [
            'action' => "updateWavepick",
            'uses'   => 'UpdateWithRfid\Controllers\WavepickController@updateWavePick'
        ]
    );
});

// Order
$api->group(['prefix' => '/v4/whs/{whsId:[0-9]+}/order', 'namespace' => 'App\Api\V4\Outbound\Order'], function ($api) {
    // Assign cartons to Order with RFID
    $api->put('/{odrId:[0-9]+}/cartons',
        [
            'action' => "assignCartonsToOrder",
            'uses'   => 'AssignCartonsToOrder21\Controllers\OrderController@putCartonOrder'
        ]
    );
    // Assign pieces to Order with RFID
    $api->put('/{odrId:[0-9]+}/pieces-with-rfid',
        [
            'action' => "assignCartonsToOrder",
            'uses'   => 'AssignPiecesToOrderWithRfid\Controllers\OrderController@putPiecesToOrder'
        ]
    );
});

// Out Pallet
$api->group(['prefix' => '/v4/whs/{whsId:[0-9]+}/outpallet', 'namespace' => 'App\Api\V4\Outbound\OutPallet'], function ($api) {
    $api->put('/assign-cartons',
        [
            'action' => "assignBarcodeToOutpallet",
            'uses'   => 'AssignCartons\Controllers\OutPalletController@assignCartonToPallet'
        ]
    );
});


