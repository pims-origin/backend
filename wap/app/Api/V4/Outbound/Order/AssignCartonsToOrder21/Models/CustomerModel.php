<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 8/19/16
 * Time: 11:43 AM
 */

namespace App\Api\V4\Outbound\Order\AssignCartonsToOrder21\Models;

use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;


class CustomerModel extends AbstractModel
{
    /**
     * @param Customer $model
     */
    public function __construct(Customer $model = null)
    {
        $this->model = ($model) ?: new Customer();
    }

    public function getCusListByWhsId($whsId, $attributes)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query = \DB::table('customer as c')
                    ->join('customer_warehouse as cw', 'c.cus_id', '=', 'cw.cus_id')
                    ->select([
                        'c.cus_id',
                        'c.cus_code',
                        'c.cus_name',
                        ])
                    ->where('cw.whs_id', $whsId)
                    ->where('cw.deleted', 0)
                    ->where('c.deleted', 0)
                    ->orderBy('c.cus_name');

        if (isset($attributes['cus_name'])) {
            $query->where('c.cus_name', 'like',  SelStr::escapeLike($attributes['cus_name']) . "%");
        }

        return $query->get();
    }

}