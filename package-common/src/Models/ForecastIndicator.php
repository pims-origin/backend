<?php

namespace Seldat\Wms2\Models;


/**
 * Class ForecastIndicator
 * @package Seldat\Wms2\Models
 * @property mixed fc_ind_id
 * @property mixed fc_ind_code
 * @property mixed fc_ind_name
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed created_by
 * @property mixed updated_by
 */
class ForecastIndicator extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fc_indicator';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'fc_ind_id';

    /**
     * @var array
     */
    protected $fillable = [
        'fc_ind_id',
        'fc_ind_code',
        'fc_ind_name',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function indicator()
    {
        return $this->hasMany(__NAMESPACE__ . '\Forecast', 'fc_ind_id', 'fc_ind_id');
    }

}
