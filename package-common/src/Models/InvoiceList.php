<?php

namespace Seldat\Wms2\Models;

class InvoiceList extends BaseModel
{
    protected $table = 'invoice_summary';

    protected $primaryKey = 'inv_sum_id';

    protected $fillable = [
        'cus_id',
        'whs_id',
        'inv_id',
        'inv_num',
        'chg_code_id',
        'chg_uom_id',
        'chg_type_id',
        'dt',
        'qty',
        'sum_sts',
    ];

    protected $invoiceCost = NULL;

    /*
    ****************************************************************************
    */

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    /*
    ****************************************************************************
    */

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    /*
    ****************************************************************************
    */

}
