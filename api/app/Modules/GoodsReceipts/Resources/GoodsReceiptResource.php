<?php

namespace App\Modules\GoodsReceipts\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GoodsReceiptResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'goods-receipts',
            'id' => (int) $this->gr_hdr_id,
            'attributes' => [
                'gr_id'         => $this->gr_hdr_id,
                'gr_num'        => $this->gr_hdr_num,
                'ref'           => $this->ref_code,
                'expected_date' => $this->gr_hdr_ept_dt ? date('Y-m-d', $this->gr_hdr_ept_dt) : NULL,
                'actual_date'   => ($this->gr_hdr_ept_dt && $this->gr_hdr_ept_dt != 0) ? date('Y-m-d', $this->gr_hdr_ept_dt) : NULL,
                'ctnr_num'      => $this->container->ctnr_num,
                'note'          => $this->gr_hdr_des,
                'status'        => $this->goodsReceiptStatus->gr_sts_name,
                'created_at'    => $this->created_at ? date('Y-m-d', strtotime($this->created_at)) : NULL,
                'updated_at'    => $this->updated_at ? date('Y-m-d', strtotime($this->updated_at)) : NULL
            ]
        ];
    }
}