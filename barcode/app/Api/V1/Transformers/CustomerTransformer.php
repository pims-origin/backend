<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Customer;

class CustomerTransformer extends TransformerAbstract
{
    public function transform(Customer $customer)
    {
        return [
            'cus_id'                => object_get($customer, 'cus_id'),
            'cus_name'              => object_get($customer, 'cus_name'),
            'cus_code'              => object_get($customer, 'cus_code'),
        ];
    }
}
