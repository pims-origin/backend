<?php

namespace App\Api\V6\Outbound\Wavepick\UpdateWithRfid\Models;

use App\Utils\JWTUtil;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class OrderCartonModel extends AbstractModel
{

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    public function autoPack($wvHdrId)
    {
        $result = DB::table('odr_hdr as o')
            ->join('odr_hdr_meta as ohm', 'ohm.odr_id', '=', 'o.odr_id')
            ->join('odr_cartons as oc', 'o.odr_id', '=', 'oc.odr_hdr_id')
            ->join('cartons as c', 'c.ctn_id', '=', 'oc.ctn_id')
            ->where('ohm.value', 1)
            ->where('o.odr_sts', 'PD')
            ->where('o.wv_id', $wvHdrId)
            ->groupBy('c.ctn_id')
            ->get();

        return $result;
    }

    public function sumPieceQtyOdrCtnByOdrDtlID($odrDtlId)
    {
        return $this->model
            ->where('odr_dtl_id', $odrDtlId)
            ->sum('piece_qty');
    }

    public function getOrderCT($odrDtlId)
    {

        $query = $this->model
            ->select([
                'odr_num',
                'wv_num',
                'odr_dtl_id',
                'odr_cartons.ctn_num',
                'odr_cartons.ctn_id',
                'rfid as ctn_rfid'
            ])
            ->join('cartons', 'odr_cartons.ctn_id', '=', 'cartons.ctn_id')
            ->where('odr_dtl_id', $odrDtlId)
            ->groupBy('odr_cartons.ctn_id');

        return $query->get();
    }

    /**
     * @param $wvDtlID
     *
     * @return mixed
     */
    public function countOrderCTByWvDtlId($wvDtlID)
    {
        $query = $this->model
            ->where('wv_dtl_id', $wvDtlID)
            ->count('ctn_num');

        return $query;
    }

    public function insertOrderCartons($ctnLists, $wvDtl, $pickerId = null)
    {
        $pltIds = array_pluck($ctnLists, 'plt_id');
        $pallets = \DB::table('pallet')
            ->whereIn('plt_id', array_unique($pltIds))
            ->where('deleted', 0)
            ->get();
        $pltIdRfidMap = array_pluck($pallets, 'rfid', 'plt_id');
        if (count($pltIdRfidMap) == 0) {
            $pltIdRfidMap = null;
        }
        $dataOrderCartons = [];
        $userId = $pickerId ?? JWTUtil::getPayloadValue('jti');
        $palletArr = [];
        $locArr = [];
        foreach ($ctnLists as $carton) {
            $isStorage = $carton['ctn_pack_size'] > $carton['piece_remain'] ? 1 : 0;
            $pltId     = array_get($carton, 'plt_id');
            $pltRfid   = array_get($pltIdRfidMap, $pltId);
            if (!$pltId) {
                $pltRfid = null;
            }

            $locName = array_get($carton, 'loc_name');
            $locId = array_get($carton, 'loc_id');
            if (!$locId && $locName) {
                if (!count($locArr) || ($locName != array_get($locArr, 'loc_alternative_name'))) {
                    $locArr = DB::table('location')->where('loc_alternative_name', $locName)->first();
                }
                $carton['loc_id'] = array_get($locArr, 'loc_id');
                $carton['loc_code'] = array_get($locArr, 'loc_code');
            }
            $data = [
                'ctn_num'    => $carton['ctn_num'],
                'ctn_id'     => $carton['ctn_id'],
                'wv_hdr_id'  => array_get($wvDtl, 'wv_id', null),
                'wv_dtl_id'  => array_get($wvDtl, 'wv_dtl_id', null),
                'wv_num'     => array_get($wvDtl, 'wv_num', null),
                'piece_qty'  => array_get($carton, 'piece_remain', null),
                'ship_dt'    => 0,
                'ctn_rfid'   => $carton['rfid'],
                'ctn_sts'    => 'PD',
                'sts'        => 'i',
                'item_id'    => $carton['item_id'],
                'sku'        => $carton['sku'],
                'size'       => $carton['size'],
                'color'      => $carton['size'],
                'upc'        => $carton['upc'],
                'lot'        => $carton['lot'],
                'pack'       => $carton['ctn_pack_size'],
                'uom_id'     => $carton['ctn_uom_id'],
                'uom_code'   => $carton['uom_code'],
                'uom_name'   => $carton['uom_name'],
                'sts'        => 'i',
                'is_storage' => $isStorage,
                'created_at' => time(),
                'updated_at' => time(),
                'created_by' => ($userId) ? $userId : 0,
                'updated_by' => ($userId) ? $userId : 0,
                'deleted_at' => $carton['deleted_at'],
                'deleted'    => 0,
                'whs_id'     => array_get($carton, 'whs_id'),
                'cus_id'     => array_get($carton, 'cus_id'),
                'loc_id'     => array_get($carton, 'loc_id'),
                'loc_code'   => array_get($carton, 'loc_code'),
                'length'     => array_get($carton, 'length'),
                'width'      => array_get($carton, 'width'),
                'height'     => array_get($carton, 'height'),
                'weight'     => array_get($carton, 'weight'),
                'volume'     => array_get($carton, 'volume'),
                'plt_id'     => $pltId,
                'plt_rfid'   => $pltRfid,

            ];
            $dataOrderCartons[] = $data;
        }

        return DB::table('odr_cartons')->insert($dataOrderCartons);


    }

    /**
     * @param $odrId
     *
     * @return mixed
     */
    public function countOdrHdr($odrId)
    {
        return $this->model->where('odr_hdr_id', '=', $odrId)->count();
    }

    /**
     * @param array $ctnRfids
     *
     * @return mixed
     */
    public function deleteOdrCartonInReturn($ctnRfids)
    {
        return $this->model->whereIn('ctn_rfid', $ctnRfids)
                        ->update([
                            'updated_by' => Data::getCurrentUserId(),
                            'deleted'    => 1,
                            'ctn_sts'    => 'PB',
                            'deleted_at' => time(),
                            'updated_at' => time(),
                        ]);
    }

    /**
     * @param array $ctnRfids
     *
     * @return mixed
     */
    public function getCartonAssignedToOrder($ctnRfids)
    {
        return $this->model->whereIn('ctn_rfid', $ctnRfids)
                        ->whereNotNull('odr_hdr_id')
                        ->get();
    }
}
