<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\PackRef;

/**
 * Class PackRefTransformer
 *
 * @package App\Api\V1\Transformers
 */
class PackRefTransformer extends TransformerAbstract
{
    /**
     * @param PackRef $packRef
     *
     * @return array
     */
    public function transform(PackRef $packRef)
    {
        return [
            'pack_ref_id' => $packRef->pack_ref_id,
            'length'      => $packRef->length,
            'width'       => $packRef->width,
            'height'      => $packRef->height,
            'dimension'   => $packRef->dimension,
            'pack_type'   => $packRef->pack_type,
        ];
    }
}
