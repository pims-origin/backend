<?php

namespace App\Modules\Asns\Validators;

use Validator;

class AsnValidator
{
    public function rules()
    {
        
    }

    public function validate($input)
    {
        $validator = Validator::make($input, $this->rules(), $this->messages());

        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                }
            }
        }

        return $validator;
    }

    public function messages()
    {
        
    }

    public function checkAsn($asn)
    {
        if(empty($asn))
        {
            throw new \Exception(json_encode(['title' =>'Asn is not existed.', 'detail' => 'Please try']));
        }
    }
}