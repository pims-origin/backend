<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AbstractModel;
use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\KittingPutawayModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\WavePickDtlLocModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Transformers\OrderDtlTransformer;
use App\Api\V1\Transformers\OrderHdrTransformer;
use App\Api\V1\Validators\OrderHdrValidator;
use App\EventTracking;
use App\Wms2\UserInfo\Data;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use mPDF;

/**
 * Class OrderHdrController
 *
 * @package App\Api\V1\Controllers
 */
class OrderHdrController extends AbstractController
{
    protected $STR_PAD_LEFT = 0;
    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var ShippingOrderModel
     */
    protected $shippingOrderModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var WavePickHdrModel
     */
    protected $wavePickModel;

    /**
     * @var WavePickDtlModel
     */
    protected $wavePickDtlModel;

    /**
     * @var WavePickDtlLocModel
     */
    protected $wavePickDtlLocModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    protected $cartonModel;
    /**
     * OrderHdrController constructor.
     *
     * @param Request $request
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param EventTrackingModel $eventTrackingModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param ShippingOrderModel $shippingOrderModel
     * @param ItemModel $itemModel
     * @param WavePickHdrModel $wavePickModel
     * @param WavePickDtlModel $wavePickDtlModel
     * @param WavePickDtlLocModel $wavePickDtlLocModel
     * @param LocationModel $locationModel
     * @param CustomerConfigModel $customerConfigModel
     * @param CartonModel $cartonModel
     *
     */
    public function __construct(
        Request $request,
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        EventTrackingModel $eventTrackingModel,
        InventorySummaryModel $inventorySummaryModel,
        ShippingOrderModel $shippingOrderModel,
        ItemModel $itemModel,
        WavePickHdrModel $wavePickModel,
        WavePickDtlModel $wavePickDtlModel,
        WavePickDtlLocModel $wavePickDtlLocModel,
        LocationModel $locationModel,
        CustomerConfigModel $customerConfigModel,
        CartonModel $cartonModel
    ) {
        parent::__construct($request, new AuthenticationService($request));
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->shippingOrderModel = $shippingOrderModel;
        $this->itemModel = $itemModel;
        $this->wavePickModel = $wavePickModel;
        $this->wavePickDtlModel = $wavePickDtlModel;
        $this->wavePickDtlLocModel = $wavePickDtlLocModel;
        $this->locationModel = $locationModel;
        $this->customerConfigModel = $customerConfigModel;
        $this->cartonModel = $cartonModel;
    }

    /**
     * @param Request $request
     * @param OrderHdrValidator $orderHdrValidator
     * @param OrderHdrTransformer $orderHdrTransformer
     * @return mixed
     */
    public function store(
        Request $request,
        OrderHdrValidator $orderHdrValidator,
        OrderHdrTransformer $orderHdrTransformer
    ) {
        $input = $request->getParsedBody();
        $input['whs_id'] = isset($input['whs_id']) ? $input['whs_id'] : AbstractModel::getCurrentWhsId();

        $orderHdrValidator->validate($input);
        try {
            DB::beginTransaction();

            $params = [
                'carrier'          => 'NA',
                'ship_to_name'     => 'NA',
                'ship_to_add_1'    => 'NA',
                'ship_to_add_2'    => 'NA',
                'ship_to_city'     => 'NA',
                'ship_to_country'  => 'NA',
                'ship_to_state'    => 'NA',
                'ship_to_zip'      => 'NA',
                'ship_by_dt'       => !empty($input['ship_by_dt']) ? strtotime($input['ship_by_dt']) : 0,
                'cancel_by_dt'     => !empty($input['cancel_by_dt']) ? strtotime($input['cancel_by_dt']) : 0,
                'req_cmpl_dt'      => !empty($input['ship_by_dt']) ? strtotime($input['req_cmpl_dt']) : 0,
                'act_cmpl_dt'      => !empty($input['ship_by_dt']) ? strtotime($input['act_cmpl_dt']) : 0,
                'act_cancel_dt'    => !empty($input['ship_by_dt']) ? strtotime($input['act_cancel_dt']) : 0,
                'in_notes'         => array_get($input, 'in_notes', null),
                'cus_notes'        => array_get($input, 'cus_notes', null),
                'cus_id'           => $input['cus_id'],
                'whs_id'           => $input['whs_id'],
                'cus_odr_num'      => array_get($input, 'ref_cod', null),
                'cus_po'           => array_get($input, 'ref_cod', null),
                'ref_cod'          => array_get($input, 'ref_cod', null),
                'odr_type'         => 'KIT',
                'rush_odr'         => (int)array_get($input, 'rush_odr', null),
                'odr_sts'          => Status::getByValue("New", "Order-Status"),
                'sku_ttl'          => count($input['items']),
            ];

            $odrNum = $this->orderHdrModel->getOrderNum();
            $params['odr_num'] = $odrNum;
            $params['so_id'] = $this->getShippingOdr($params);
            if (!empty($input['ref_cod'])) {
                // Check duplicate
                $this->orderHdrModel->refreshModel();
                $arrCheckWhere = [
                    'whs_id'      => $input['whs_id'],
                    'cus_id'      => $input['cus_id'],
                    'odr_type'    => 'KIT'
                ];
                $arrCheckWhere['ref_cod'] = $input['ref_cod'];
                $checkExist = $this->orderHdrModel->checkWhere($arrCheckWhere);
                if ($checkExist) {
                    throw new \Exception(Message::get("BM006", "Ref"));
                }
            }

            // Insert Order Header
            $this->orderHdrModel->refreshModel();
            $orderHdr = $this->orderHdrModel->create($params);

            // Insert Order Detail
            if (!empty($input['items'])) {
                foreach ($input['items'] as $detail) {
                    $this->orderDtlModel->refreshModel();
                    $detail['whs_id'] = $input['whs_id'];
                    $detail['cus_id'] = $input['cus_id'];
                    $detail['odr_id'] = $orderHdr->odr_id;
                    $detail['item_id'] = $detail['itm_id'];
                    $detail['pack'] = empty($detail['pack']) ? null : $detail['pack'];
                    $detail['uom_id'] = empty($detail['uom_id']) ? null : $detail['uom_id'];
                    $detail['uom_code'] = empty($detail['uom_code']) ? null : $detail['uom_code'];
                    $detail['qty'] = empty($detail['qty']) ? null : $detail['qty'];
                    $detail['piece_qty'] = empty($detail['qty']) ? null : $detail['qty'] * $detail['pack'];
                    $detail['sts'] = "i";
                    $detail['is_kitting'] = 1;
                    $detail['lot'] = 'NA';
                    $this->orderDtlModel->create($detail);
                }
            }

            // Insert Event Tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $input['whs_id'],
                'cus_id'    => $input['cus_id'],
                'owner'     => $odrNum,
                'evt_code'  => Status::getByKey("event", "Order-New"),
                'trans_num' => $odrNum,
                'info'      => sprintf(Status::getByKey("event-info", "Order-New"), $odrNum)

            ]);

            DB::commit();

            return $this->response->item($orderHdr, $orderHdrTransformer)->setStatusCode(Response::HTTP_CREATED);
        } catch (\PDOException $e) {
            DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $input
     * @param bool $isUpdate
     * @param $oldRef
     *
     * @return mixed
     */
    private function getShippingOdr(
        $input,
        $oldRef = null,
        $isUpdate = false
    ) {
        $this->shippingOrderModel->refreshModel();

        $shipParams = [
            'cus_id'      => $input['cus_id'],
            'whs_id'      => $input['whs_id'],
            'cus_odr_num' => $input['cus_odr_num'],
            'so_sts'      => Status::getByValue("New", "Order-Status"),
            'type'        => 'KIT',
        ];

        // Create Shipping Order.
        $so = $this->shippingOrderModel->getFirstWhere([
            'cus_odr_num' => $shipParams['cus_odr_num'],
            'whs_id'      => $shipParams['whs_id'],
            'cus_id'      => $shipParams['cus_id']
        ]);

        if (empty($so)) {
            // Create Shipping Odr
            $shipParams['po_total'] = 1;
            $soId = $this->shippingOrderModel->create($shipParams)->so_id;
        } else {
            // Update po_total
            $soId = $so->so_id;
            if ($isUpdate) {
                if ($oldRef !== $shipParams['cus_odr_num']) {
                    $shipParams['po_total'] = (int)$so->po_total + 1;
                    $shipParams['so_id'] = $soId;
                    $soId = $this->shippingOrderModel->update($shipParams)->so_id;
                }
            }
        }

        return $soId;
    }

    /**
     * @param $orderId
     * @param Request $request
     * @param OrderHdrValidator $orderHdrValidator
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function update(
        $orderId,
        Request $request,
        OrderHdrValidator $orderHdrValidator,
        OrderHdrTransformer $orderHdrTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['whs_id'] = AbstractModel::getCurrentWhsId();

        // validation
        $orderHdrValidator->validate($input);
        try {

            $params = [
                // Order Hdr
                'odr_id'          => $orderId,
                'carrier'          => 'NA',
                'ship_to_name'     => 'NA',
                'ship_to_add_1'    => 'NA',
                'ship_to_add_2'    => 'NA',
                'ship_to_city'     => 'NA',
                'ship_to_country'  => 'NA',
                'ship_to_state'    => 'NA',
                'ship_to_zip'      => 'NA',
                'ship_by_dt'      => !empty($input['ship_by_dt']) ? strtotime($input['ship_by_dt']) : 0,
                'cancel_by_dt'    => !empty($input['cancel_by_dt']) ? strtotime($input['cancel_by_dt']) : 0,
                'req_cmpl_dt'     => !empty($input['req_cmpl_dt']) ? strtotime($input['req_cmpl_dt']) : 0,
                'act_cmpl_dt'     => !empty($input['act_cmpl_dt']) ? strtotime($input['act_cmpl_dt']) : 0,
                'act_cancel_dt'   => !empty($input['act_cancel_dt']) ? strtotime($input['act_cancel_dt']) : 0,
                'in_notes'        => array_get($input, 'in_notes', null),
                'cus_notes'       => array_get($input, 'cus_notes', null),
                // Order Info
                'cus_id'          => $input['cus_id'],
                'whs_id'          => $input['whs_id'],
                'cus_odr_num'     => array_get($input, 'ref_cod', null),
                'cus_po'          => array_get($input, 'ref_cod', null),
                'ref_cod'         => array_get($input, 'ref_cod', null),
                'rush_odr'        => (int)array_get($input, 'rush_odr', null),
                'odr_sts'         => Status::getByValue("New", "Order-Status"),
                'sku_ttl'         => count($input['items'])
            ];


            // Check Exist
            $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId);
            if (empty($orderHdr->odr_id)) {
                throw new \Exception(Message::get("BM017", "Order Hdr"));
            }

            // Check duplicate
            if (!empty($input['ref_cod'])) {
                $this->orderHdrModel->refreshModel();
                $arrCheckWhere = [
                    'whs_id'      => $input['whs_id'],
                    'cus_id'      => $input['cus_id'],
                    'odr_type'    => 'KIT',
                ];
                $arrCheckWhere['ref_cod'] = $input['ref_cod'];
                $odr = $this->orderHdrModel->getFirstWhere($arrCheckWhere);
                if ($odr && $odr->odr_id != $orderId) {
                    throw new \Exception(Message::get("BM006", "Ref"));
                }
            }

            // Check Status
            if ($orderHdr->odr_sts != Status::getByValue("New", "Order-status")) {
                throw new \Exception(Message::get("VR064", "Order Status", "'New'"));
            }

            DB::beginTransaction();
            $params['so_id'] = $this->getShippingOdr($params);
            // Update Order Header
            $orderHdr = $this->orderHdrModel->update($params);
            if (!$orderHdr) {
                throw new \Exception(Message::get("BM010"));
            }

            // Insert Order Detail
            if (!empty($input['items'])) {
                $newOdrDtlIds = array_column($input['items'], 'odr_dtl_id');

                $deleteIds = $this->orderDtlModel->getModel()
                    ->where("odr_id", $orderId)
                    ->whereNotIn('odr_dtl_id', $newOdrDtlIds)
                    ->pluck('odr_dtl_id')
                    ->toArray();

                if (!empty($deleteIds)) {
                    $this->orderDtlModel->deleteDtls($deleteIds);
                }

                foreach ($input['items'] as $detail) {
                    $this->orderDtlModel->refreshModel();

                    $detail['whs_id'] = $input['whs_id'];
                    $detail['cus_id'] = $input['cus_id'];
                    $detail['odr_id'] = $orderHdr->odr_id;
                    $detail['item_id'] = $detail['itm_id'];
                    $detail['pack'] = empty($detail['pack']) ? null : $detail['pack'];
                    $detail['qty'] = empty($detail['qty']) ? null : $detail['qty'];
                    $detail['piece_qty'] = empty($detail['qty']) ? null : $detail['qty'];
                    $detail['uom_id'] = empty($detail['uom_id']) ? null : $detail['uom_id'];
                    $detail['uom_code'] = empty($detail['uom_code']) ? null : $detail['uom_code'];
                    $detail['lot'] = 'NA';
                    $detail['is_kitting'] = 1;
                    if (empty($detail['odr_dtl_id'])) {
                        $detail['sts'] = "i";
                        $this->orderDtlModel->create($detail);
                    } else {
                        $detail['sts'] = "u";
                        $this->orderDtlModel->update($detail);
                    }
                }
            }
            DB::commit();

            return $this->response->item($orderHdr, $orderHdrTransformer)->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        Request $request,
        OrderHdrTransformer $orderHdrTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $orderHdr = $this->orderHdrModel->search($input, [], array_get($input, 'limit'));

            return $this->response->paginator($orderHdr, $orderHdrTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderId
     * @param OrderDtlTransformer $orderDtlTransformer
     *
     * @return Response|void
     */
    public function show(
        $orderId,
        OrderDtlTransformer $orderDtlTransformer
    ) {
        try {
            $orderHdr = $this->orderHdrModel->getFirstWhere([
                'odr_id' => $orderId,
                'whs_id' => AbstractModel::getCurrentWhsId()
            ]);

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            //validate whs cus
            $this->chkWhsAndCus($orderHdr['whs_id'], $orderHdr['cus_id']);

            $orderDtl = $this->orderDtlModel
                ->allBy('odr_id', $orderHdr->odr_id, ['SystemUom'])
                ->toArray();

            $items = [];
            $pickingItems = [];
            foreach ($orderDtl as $key => $item) {

                $arrData = [
                    'odr_dtl_id'        => $item['odr_dtl_id'],
                    'itm_id'            => $item['item_id'],
                    'itm_sts'           => $item['itm_sts'],
                    'itm_sts_name'      => Status::getByKey('ORDER-STATUS', $item['itm_sts']),
                    'sku'               => $item['sku'],
                    'color'             => $item['color'],
                    'size'              => $item['size'],
                    'uom_id'            => $item['uom_id'],
                    'uom_code'          => array_get($item, 'system_uom.sys_uom_code', null),
                    'uom_name'          => array_get($item, 'system_uom.sys_uom_name', null),
                    'piece_qty'         => $item['qty'] * $item['pack'],
                    'pack'              => $item['pack'],
                    'qty'               => $item['qty'],
                    'allocated_qty'     => $item['alloc_qty'],
                    'kitting_parent'    => $item['kitting_parent'],
                ];

                if ($item['is_kitting'] == 1) {
                    $items[] = $arrData;
                } else {
                    $pickingItems[] = $arrData;
                }

                //Generate picking items if odr_sts = NW
                if ($orderHdr->odr_sts == 'NW') {
                    $this->generatePickingOdrDtl($item, $pickingItems);
                }
            }
            $orderHdr['items'] = $items;
            $orderHdr['picking_items'] = array_values($pickingItems);

            return $this->response->item($orderHdr, $orderDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function generatePickingOdrDtl($kittingOdrDtl, &$pickingItems) {
        $childItems = $this->itemModel->getChildren($kittingOdrDtl['item_id'], ['SystemUom']);
        foreach ($childItems as $childItem) {
            if (isset($pickingItems[$childItem['item_id']])) {
                $pickingItems[$childItem['item_id']]['qty'] += $kittingOdrDtl['qty'] * $childItem['pack'] * $childItem['inner_pack'];
                $pickingItems[$childItem['item_id']]['piece_qty'] += $kittingOdrDtl['qty'] * $childItem['pack'] * $childItem['inner_pack'];
            } else {
                $pickingItems[$childItem['item_id']] = [
                    'itm_id'            => $childItem['item_id'],
                    'sku'               => $childItem['sku'],
                    'color'             => $childItem['color'],
                    'size'              => $childItem['size'],
                    'uom_id'            => $childItem['uom_id'],
                    'uom_code'          => isset($childItem->systemUom) ? $childItem->systemUom->sys_uom_code : null,
                    'uom_name'          => isset($childItem->systemUom) ? $childItem->systemUom->sys_uom_name : null,
                    'piece_qty'         => $kittingOdrDtl['qty'] * $childItem['pack'] * $childItem['inner_pack'],
                    'pack'              => $childItem['inner_pack'],
                    'qty'               => $kittingOdrDtl['qty'] * $childItem['pack'] * $childItem['inner_pack'],
                    'allocated_qty'     => 0,
                    'kitting_parent'    => $kittingOdrDtl['odr_dtl_id'],
                    'description'       => $childItem['description']
                ];
            }

        }
    }

    public function allocate($orderHdrId, Request $request) {
        try{
            // Load order
            $orderInfo = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderHdrId]);
            // Check Exist orders: The {0} is not existed!
            if (empty($orderInfo)) {
                throw new \Exception('The Kitting is not existed!');
            }
            //check order status: Only New Orders can be allocated!
            if (object_get($orderInfo, 'odr_sts', null) !== 'NW') {
                throw new \Exception("Only New Orders can be allocated!");
            }
            $pickingItems = [];
            foreach ($orderInfo->details as $kittingOdrDtl) {
                $this->generatePickingOdrDtl($kittingOdrDtl, $pickingItems);
            }
            $error = false;
            $errorData = [];
            foreach ($pickingItems as $pickingItem) {
                $inventorySmr = $this->inventorySummaryModel->getAvailableQty($pickingItem['itm_id']);
                if ($pickingItem['qty'] > $inventorySmr['total_avail']) {
                    $error = true;
                    $errorData[] = [
                        'Sku'           => $pickingItem['sku'],
                        'Description'   => $pickingItem['description'],
                        'Color'         => $pickingItem['color'],
                        'Pack'          => $pickingItem['pack'],
                        'Uom'           => $pickingItem['uom_name'],
                        'Picking'       => $pickingItem['qty'],
                        'Avail'         => $inventorySmr['total_avail'],
                        'Discrepancy'   => $inventorySmr['total_avail'] - $pickingItem['qty'],
                    ];
                }
            }

            if ($error) {
                return $this->exportErrorExcelFile($errorData);
            }

            DB::beginTransaction();
            //create picking items and allocate them
            foreach ($pickingItems as $pickingItem) {
                $data = [
                    'whs_id'            => $orderInfo->whs_id,
                    'cus_id'            => $orderInfo->cus_id,
                    'odr_id'            => $orderHdrId,
                    'item_id'           => $pickingItem['itm_id'],
                    'pack'              => $pickingItem['pack'],
                    'uom_id'            => $pickingItem['uom_id'],
                    'uom_code'          => $pickingItem['uom_code'],
                    'qty'               => $pickingItem['qty'],
                    'piece_qty'         => $pickingItem['piece_qty'],
                    'sku'               => $pickingItem['sku'],
                    'size'              => $pickingItem['size'],
                    'color'             => $pickingItem['color'],
                    'sts'               => 'i',
                    'is_kitting'        => 0,
                    'kitting_parent'    => $pickingItem['kitting_parent'],
                    'lot'               => 'NA',
                    'itm_sts'           => 'AL',
                    'alloc_qty'         => $pickingItem['qty'],
                ];
                $this->orderDtlModel->refreshModel();
                $odrDtl = $this->orderDtlModel->create($data);
                $this->orderDtlModel->updateOrdDtl($orderInfo, $odrDtl);
            }
            // Update all order detail status to Allocate
            $this->orderDtlModel->updateWhere(['itm_sts' => 'AL'], ['odr_id' => $orderHdrId]);
            // Insert Event Tracking
            $evtTracking = new EventTrackingModel(new EventTracking());
            $eventData = [
                'whs_id'    => object_get($orderInfo, 'whs_id', ''),
                'cus_id'    => object_get($orderInfo, 'cus_id', ''),
                'owner'     => object_get($orderInfo, 'odr_num', ''),
                'evt_code'  => config('constants.event.ALLOCATED-ORD'),
                'trans_num' => object_get($orderInfo, 'odr_num', ''),
                'info'      => sprintf(config('constants.event-info.ALLOCATED-ORD'), $orderInfo['odr_num'])
            ];
            $evtTracking->create($eventData);
            //Update odr_sts = allocated
            $sts = Status::getByValue('Allocated', 'ORDER-STATUS');
            $orderInfo->odr_sts = $sts;
            $orderInfo->save();

            //Create wave pick
            $this->createWavePick([$orderHdrId]);
            DB::commit();
            return ['data' => 'Update successfully'];
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
    private function exportErrorExcelFile($data) {
        http_response_code(201);
        Excel::create('Order', function($excel) use ($data) {
            $excel->sheet('Error', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }

    public function createWavePick($odrLsts)
    {
        // Parse to integer
        $odrLstAfters = [];
        foreach ($odrLsts as $odrLst) {
            array_push($odrLstAfters, (int)$odrLst);
        }

        // Load odr hdr data
        $odrHdrDt = $this->orderHdrModel->getListOrderHdrInfo($odrLstAfters, "");

        // Check valid order list
        if ($this->checkInvalidOdr($odrLstAfters, $odrHdrDt) == false) {
            throw new \Exception("Wave pick can be created for allocated orders only!");
        }

        // Detect to make sure that all order only be normal or online
        $detectOrdType = $this->checkOnlineOrNormalOdr($odrHdrDt);
        if ($detectOrdType['checkNormalOdr'] == count($odrLstAfters)) {
            $isOnline = false;
        } elseif ($detectOrdType['checkOnlineOdr'] == count($odrLstAfters)) {
            $isOnline = true;
        } else {
            throw new \Exception('Please select orders belonging to 1 order type only!');
        }


        try {
            DB::beginTransaction();
            // load data
            $odrDtlList = $this->orderDtlModel->getListOrderDtlInfo($odrLstAfters);

            if (empty($odrDtlList)) {
                throw new \Exception("The Orders is not existed!");
            }

            // group the same
            $data = $this->groupSameItem($odrDtlList);

            // save wave pick header
            $wvHdr = $this->saveWvHdr($data[0][0], $isOnline);

            // make data to save to db
            foreach ($data as $ds) {
                $cus_id = $ds[0]->cus_id;

                $pickingAlgorithm = $this->customerConfigModel->getPickingAlgorithm(array_get($wvHdr, 'whs_id', 0),
                    $cus_id);

                // Detect pack size, carton qty , picking qty & alloc_qty
                $sumQty = $this->detectQty($ds);

                // Detect primary &  backup location
                $loc = $this->findNormalOrderLocation($ds, $pickingAlgorithm, $sumQty['ctn_qty']);

                // Save wave pick detail
                $wvDtl = $this->saveWvDtl($ds, $wvHdr, $sumQty, $loc);

                // Save wave pick suggest loc detail\
                $this->saveWvDtlLoc($wvHdr, $wvDtl, $loc['loc']);

            }

            // update order detail
            $this->updateOdrDtl($odrLstAfters, $wvHdr);

            // update order header
            $this->updateOdrHdr($odrHdrDt, $wvHdr);

            // Change request --------------------------------------------------------------
            // If carton_qty is null, find num carton with sum (piece_remain) <= picking qty
            // $updateCtnQty = $this->updateCartonQty($temp);

            $odrHdrDtAfter = $this->orderHdrModel->getListOrderHdrInfo($odrLstAfters, "updated");

            $this->logEventTracking($odrHdrDtAfter, $wvHdr, $data[0][0]);

            DB::commit();
            return (new OrderPickingController())->processPrintWavepick(['wv_id' => $wvHdr['wv_id']]);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param $odrLst
     *
     * @return bool
     */
    private function checkInvalidOdr($odrLst, $odrHdrDt)
    {
        $check = true;
        if (count($odrHdrDt) != count($odrLst)) {
            $check = false;
        }

        return $check;
    }

    /**
     * @param $odrHdrDt
     *
     * @return array
     */
    public function checkOnlineOrNormalOdr($odrHdrDt)
    {
        $checkOnline = 0;
        $checkNormal = 0;
        foreach ($odrHdrDt as $arr) {
            if (strtolower($arr['type']) == "ec") {
                $checkOnline++;
            } else {
                $checkNormal++;
            }
        }

        return [
            'checkOnlineOdr' => $checkOnline,
            'checkNormalOdr' => $checkNormal
        ];
    }

    /**
     * @param $odrDtlList
     *
     * @return array
     */
    private function groupSameItem($odrDtlList)
    {
        // group the same
        $data = [];
        $map = [];
        $groupNum = -1;
        for ($i = 0; $i < count($odrDtlList); $i++) {
            $flag = false;
            for ($j = 0; $j < $i; $j++) {
                // Start to compare
                $comSkuColr = ($odrDtlList[$i]['sku'] == $odrDtlList[$j]['sku'] && $odrDtlList[$i]['color'] ==
                    $odrDtlList[$j]['color'] && $odrDtlList[$i]['lot'] ==
                    $odrDtlList[$j]['lot']);

                $comSizePack = ($odrDtlList[$i]['size'] == $odrDtlList[$j]['size']
                    && $odrDtlList[$i]['pack'] == $odrDtlList[$j]['pack']);

                if ($odrDtlList[$i]['item_id'] == $odrDtlList[$j]['item_id']
                    && $comSkuColr
                    && $comSizePack
                ) {
                    $data[$map[$j]][] = $odrDtlList[$i];
                    $map[$i] = $map[$j];
                    $flag = true;
                    break;
                }
            }
            if (!$flag) {
                $groupNum++;
                $data[$groupNum][] = $odrDtlList[$i];
                $map[$i] = $groupNum;
            }
        }

        return $data;
    }

    /**
     * @param $data
     * @param $isOnline
     *
     * @return array
     */
    private function saveWvHdr($data, $isOnline)
    {
        $wvNum = $this->generateWavNumAndSeq();

        // Make data of wave pick header
        $wavePickHdr = [
            'whs_id'  => $data->whs_id,
            'wv_num'  => $wvNum['wave_num'],
            'wv_sts'  => Status::getByValue("New", "WAVEPICK-STATUS"),
            'seq'     => $wvNum['seq'],
            'is_ecom' => intval($isOnline)
        ];

        // save to db: wave pick header
        $wvHdr = $this->wavePickModel->saveWavePickHdr($wavePickHdr);

        return [
            'wv_id'  => $wvHdr->wv_id,
            'wv_num' => $wvNum['wave_num'],
            'whs_id' => $data->whs_id
        ];
    }

    /**
     * @return array
     */
    private function generateWavNumAndSeq()
    {
        $wavNumber = $this->wavePickModel->getLatestWavePickNumber();

        $result = [
            'wave_num' => '',
            'seq'      => '',
        ];

        $currentYearMonth = date('ym');
        if ($wavNumber) {
            list(, $yymm, $result['seq']) = explode('-', $wavNumber->wv_num);
            // whether reset asmNum
            $wavNumber = ($yymm != $currentYearMonth) ? '' : $wavNumber;
        }

        $result['seq'] = (!$wavNumber) ? 1 : ((int)$result['seq']) + 1;

        $result['wave_num'] = sprintf('%s-%s-%s',
            'WAV',
            $currentYearMonth,
            str_pad($result['seq'], 5, '0', $this->STR_PAD_LEFT));

        return $result;
    }

    /**
     * @param $odrDtls
     *
     * @return array
     */
    private function detectQty($odrDtls)
    {
        $piece_qty = 0;
        $packSize = 0;
        foreach ($odrDtls as $odrDtl) {
            $piece_qty += (int)$odrDtl->alloc_qty;
            $packSize = $odrDtl->pack;
        }

        $ctn_qty = ceil($piece_qty / $packSize);

        return ([
            'ctn_qty'   => $ctn_qty,
            'piece_qty' => $piece_qty
        ]);
    }
    /**
     * @param $data
     * @param $algorithm
     * @param $ctn_qty
     *
     * @return array
     * @throws  \Exception
     */
    private function findNormalOrderLocation($data, $algorithm, $ctn_qty = null)
    {
        $itemId = object_get($data[0], 'item_id', 0);
        $lot = object_get($data[0], 'lot', null);

        // Only get data with location type is rack
        $loc = $this->locationModel->getLocationList($itemId, $algorithm, $ctn_qty, $lot);

        $result = [];
        $locs = [];
        if ($loc) {
            foreach ($loc as $item) {
                $locs[$item['loc_id']] = $item;

            }
            $result['loc'] = $locs;
        }

        if (count($locs) > 0) {
            $primary = array_shift($locs);

            $result['primary_loc_id'] = array_get($primary, 'loc_id', null);
            $result['primary_loc'] = array_get($primary, 'loc_code', null);

            $result['bu_loc_1_id'] = array_get($locs, '1.loc_id', null);
            $result['bu_loc_1'] = array_get($locs, '1.loc_code', null);
            $result['bu_loc_2_id'] = array_get($locs, '2.loc_id', null);
            $result['bu_loc_2'] = array_get($locs, '2.loc_code', null);
            $result['bu_loc_3_id'] = array_get($locs, '3.loc_id', null);
            $result['bu_loc_3'] = array_get($locs, '3.loc_code', null);
            $result['bu_loc_4_id'] = array_get($locs, '4.loc_id', null);
            $result['bu_loc_4'] = array_get($locs, '4.loc_code', null);
            $result['bu_loc_5_id'] = array_get($locs, '5.loc_id', null);
            $result['bu_loc_5'] = array_get($locs, '5.loc_code', null);
        } else {
            throw new \Exception('Number of locations is insufficient to pick. There is no carton in any location to pick');
        }

        return $result;
    }

    /**
     * @param $ds
     * @param $wvHdr
     * @param $sumQty
     * @param $loc
     *
     * @return mixed
     */
    private function saveWvDtl($ds, $wvHdr, $sumQty, $loc)
    {

        // Make data of wave pick detail
        $wavePickDtl = [
            'cus_id'         => $ds[0]->cus_id,
            'whs_id'         => $ds[0]->whs_id,
            'wv_id'          => $wvHdr['wv_id'],
            'item_id'        => $ds[0]->item_id,
            'uom_id'         => $ds[0]->uom_id,
            'wv_num'         => $wvHdr['wv_num'],
            'color'          => $ds[0]->color,
            'sku'            => $ds[0]->sku,
            'size'           => $ds[0]->size,
            'lot'            => $ds[0]->lot,
            'cus_upc'        => $ds[0]->cus_upc,
            'wv_dtl_sts'     => Status::getByValue("New", "WAVEPICK-STATUS"),
            'pack_size'      => $ds[0]->pack,
            'ctn_qty'        => $sumQty['ctn_qty'],
            'piece_qty'      => $sumQty['piece_qty'],
            'primary_loc_id' => (!is_null($loc) ? $loc['primary_loc_id'] : null),
            'primary_loc'    => (!is_null($loc) ? $loc['primary_loc'] : null),
            'bu_loc_1_id'    => (!is_null($loc) ? $loc['bu_loc_1_id'] : null),
            'bu_loc_1'       => (!is_null($loc) ? $loc['bu_loc_1'] : null),
            'bu_loc_2_id'    => (!is_null($loc) ? $loc['bu_loc_2_id'] : null),
            'bu_loc_2'       => (!is_null($loc) ? $loc['bu_loc_2'] : null),
            'bu_loc_3_id'    => (!is_null($loc) ? $loc['bu_loc_3_id'] : null),
            'bu_loc_3'       => (!is_null($loc) ? $loc['bu_loc_3'] : null),
            'bu_loc_4_id'    => (!is_null($loc) ? $loc['bu_loc_4_id'] : null),
            'bu_loc_4'       => (!is_null($loc) ? $loc['bu_loc_4'] : null),
            'bu_loc_5_id'    => (!is_null($loc) ? $loc['bu_loc_5_id'] : null),
            'bu_loc_5'       => (!is_null($loc) ? $loc['bu_loc_5'] : null),
        ];

        return $this->wavePickDtlModel->saveWavePickDtl($wavePickDtl);
    }

    /**
     * @param $wvHdr
     * @param $wvDtl
     * @param $loc
     */
    private function saveWvDtlLoc($wvHdr, $wvDtl, $loc)
    {

        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        $data = [
            'wv_id'       => $wvHdr['wv_id'],
            'wv_dtl_id'   => $wvDtl['wv_dtl_id'],
            'sug_loc_ids' => (!is_null($loc) ? implode(',', array_keys($loc)) : null),
            'created_at'  => time(),
            'updated_at'  => time(),
            'created_by'  => $userId,
            'updated_by'  => $userId,
        ];

        // Make data of wave pick detail
        $this->wavePickDtlLocModel->saveWavePickDtlLoc($data);
    }

    /**
     * @param $data
     * @param $wvHdr
     */
    private function updateOdrDtl($data, $wvHdr)
    {
        // Make data to update order detail
        foreach ($data as $d) {
            $odrDtl = [
                'odr_id' => $d,
                'wv_id'  => $wvHdr['wv_id']
            ];

            $this->orderDtlModel->updateOrtDtl($odrDtl);
        }
    }

    /**
     * @param $odrHdrDt
     * @param $wvHdr
     */
    private function updateOdrHdr($odrHdrDt, $wvHdr)
    {
        // Make data to update order detail
        foreach ($odrHdrDt as $odrHdr) {
            $odrHdr = [
                'odr_id'  => $odrHdr['odr_id'],
                'wv_num'  => $wvHdr['wv_num'],
                'wv_id'   => $wvHdr['wv_id'],
                'odr_sts' => $odrHdr['odr_sts']
            ];

            $this->orderHdrModel->updateOrdHdr($odrHdr);
        }
    }

    /**
     * @param $odrHdrDt
     * @param $wvHdr
     * @param $data
     */
    private function logEventTracking($odrHdrDt, $wvHdr, $data)
    {
        // event tracking - ORDER
        foreach ($odrHdrDt as $odrHdr) {
            // Partial allocated --> Partial Picking
            if ($odrHdr['odr_sts'] == Status::getByValue("Partial Picking", "ORDER-STATUS")) {
                $evt_code = Status::getByKey("EVENT", "PARTIAL-PICKING");
                $info = sprintf(Status::getByKey("EVENT-INFO", "PPK"), $odrHdr['odr_num']);
            } // Full allocated --> Full Picking
            elseif ($odrHdr['odr_sts'] == Status::getByValue("Picking", "ORDER-STATUS")) {
                $evt_code = Status::getByKey("EVENT", "ORDER-PICKING");
                $info = sprintf(Status::getByKey("EVENT-INFO", "ORDER-PICKING"), $odrHdr['odr_num']);
            } else {
                // event tracking - WAVE PICK
                $evt_code = Status::getByKey("EVENT", "WAVE-PICK-NEW");

                $info = sprintf(Status::getByKey("EVENT-INFO", "WAVE-PICK-NEW"), $wvHdr['wv_num'], $odrHdr['odr_num']);

                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $wvHdr['whs_id'],
                    'cus_id'    => $data->cus_id,
                    'owner'     => $odrHdr['odr_num'],
                    'evt_code'  => $evt_code,
                    'trans_num' => $wvHdr['wv_num'],
                    'info'      => $info
                ]);

                return;
            }

            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $wvHdr['whs_id'],
                'cus_id'    => $data->cus_id,
                'owner'     => $odrHdr['odr_num'],
                'evt_code'  => $evt_code,
                'trans_num' => $odrHdr['odr_num'],
                'info'      => $info

            ]);
        }
    }

    public function getKittingStatus() {
        $kittingStatuses = config('constants.kitting_status');
        $result = [];
        foreach ($kittingStatuses as $key => $value) {
            $result[] = [
                'sts_code' => $key,
                'sts_name' => $value,
            ];
        }
        return [
            'data' => $result
        ];
    }

    public function getKittingPutAway($orderId) {
        $kittingPutawayModel = new KittingPutawayModel();
        $kittingPutaways = $kittingPutawayModel->getByKittingId($orderId);

        return [
            'data' => $kittingPutaways->toArray(),
        ];
    }
}