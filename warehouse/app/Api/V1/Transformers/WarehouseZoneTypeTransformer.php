<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseZoneType;

class WarehouseZoneTypeTransformer extends TransformerAbstract
{
    /**
     * @param WarehouseZoneType $warehouseZoneType
     *
     * @return array
     */
    public function transform(WarehouseZoneType $warehouseZoneType)
    {
        return [
            'zone_type_id'   => $warehouseZoneType->zone_type_id,
            'zone_type_name' => $warehouseZoneType->zone_type_name,
            'zone_type_code' => $warehouseZoneType->zone_type_code,
            'zone_type_desc' => $warehouseZoneType->zone_type_desc,
        ];
    }
}
