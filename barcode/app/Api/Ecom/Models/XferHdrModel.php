<?php

namespace App\Api\Ecom\Models;

use Seldat\Wms2\Models\XferHdr;

class XferHdrModel extends AbstractModel
{
    public function __construct(XferHdr $model = null)
    {
        $this->model = ($model) ?: new XferHdr();
    }
}
