<?php

namespace App\Api\V1\Models;


use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Utils\SelStr;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * @param EventTracking $model
     */
    public function __construct(EventTracking $model = null)
    {
        $this->model = ($model) ?: new EventTracking();
    }

    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);

        if (isset($attributes['evt_code'])) {
            $query->where('evt_code', 'like', "%" . SelStr::escapeLike($attributes['evt_code']) . "%");
        }

        if (isset($attributes['owner'])) {
            $query->where('owner', 'like', "%" . SelStr::escapeLike($attributes['owner']) . "%");
        }

        if (isset($attributes['trans_num'])) {
            $query->where('trans_num', 'like', "%" . SelStr::escapeLike($attributes['trans_num']) . "%");
        }

        //Filter according to type = ib/ob/wo
        $query->whereHas('event', function ($query) use ($attributes) {
            //inbound process
            $trans_ib = ['ASN', 'GRD', 'ASN', 'ASN', 'ASN'];
            //outbound process
            $trans_ob = ['ORD'];
            //warehouse operating
            $trans_wo = ['RLC', 'CNC'];

            if (isset($attributes['type'])) {
                $type = $attributes['type'];
                switch ($type) {
                    case config('constants.INBOUND'):
                        $query->whereIn('trans', $trans_ib);
                        break;
                    case config('constants.OUTBOUND'):
                        $query->whereIn('trans', $trans_ob);
                        break;
                    case config('constants.WAREHOUSE-OPERATING'):
                        $query->whereIn('trans', $trans_wo);
                        break;
                    default:
                        $query->whereIn('trans', $trans_ib);
                        break;
                }
            }
        });

        $this->model->filterData($query, true);
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);

    }


}
