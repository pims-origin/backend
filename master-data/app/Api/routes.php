<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput', 'setWarehouseTimezone'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V1'];
        });

        // master data
        $api->get('/master-data/search', [
            'action' => "viewMasterDataList",
            'uses' => 'MasterDataController@search']);
        $api->get('/master-data/{mdType:[a-zA-Z]+}/{mdCode:[a-zA-Z]+}', [
            'action' => "viewMasterDataList",
            'uses' => 'MasterDataController@getInfo']);
        $api->post('/master-data/create', [
            'action' => "createMasterDataList",
            'uses' => 'MasterDataController@store']);
        $api->put('/master-data/update/{mdType:[a-zA-Z]+}/{mdCode:[a-zA-Z]+}', [
            'action' => "editMasterDataList",
            'uses' => 'MasterDataController@update']);
        $api->get('/master-data/status', [
            'action' => "viewMasterDataList",
            'uses' => 'MasterDataController@mdStatusDropDown']);
        $api->get('/master-data/mdtypes', [
            'action' => "viewMasterDataList",
            'uses' => 'MasterDataController@mdTypesDropDown']);

        // master data type
        $api->get('/master-data-type/search', [
            'action' => "viewMasterDataType",
            'uses' => 'MasterDataTypeController@search']);
        $api->get('/master-data-type/{mdType:[a-zA-Z]+}', [
            'action' => "viewMasterDataType",
            'uses' => 'MasterDataTypeController@getInfo']);
        $api->post('/master-data-type/create', [
            'action' => "createMasterDataType",
            'uses' => 'MasterDataTypeController@store']);
        $api->put('/master-data-type/update/{mdType:[a-zA-Z]+}', [
            'action' => "editMasterDataType",
            'uses' => 'MasterDataTypeController@update']);
        $api->get('/master-data-type/status-dropdown', [
            'action' => "viewMasterDataType",
            'uses' => 'MasterDataTypeController@mdtStatusDropDown']);

        //CRUD example
        $api->get('/crud_get', 'CRUDController@getInfo');
        $api->post('/crud_create', 'CRUDController@storeInfo');
        $api->put('/crud_update/{sysBugId:[0-9]+}', 'CRUDController@updateInfo');
        $api->delete('/crud_delete/{sysBugId:[0-9]+}', 'CRUDController@deleteInfo');

    });
});
