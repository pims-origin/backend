<?php

namespace Seldat\Wms2\Models;

class Storages extends BaseSoftModel
{
    protected $table = 'cartons';

    protected $primaryKey = 'ctn_id';

    protected $fillable = [
        'sku',
        'plt_num',
        'loc_name',
        'ctn_sts',
        'ctnr_num',
        'piece_remain',
        'total_cartons',
        'total_pieces',
        'cus_name',
        'item_code',
        'size',
        'color',
        'prefix',
        'suffix',
        'length',
        'width',
        'height',
        'weight',
        'created_at',
    ];
    
    public function Warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id','whs_id');
    }

    public function AsnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function pallet()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Pallet', 'plt_id', 'plt_id');
    }
    public function cartonorder() {
        return $this ->belongsTo(__NAMESPACE__ . '\OrderCarton', 'ctn_id', 'ctn_id');
    }
     public function systemUom()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemUom', 'sys_uom_id', 'ctn_uom_id');
    }
}
    