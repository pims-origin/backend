<?php

// Bypass middleware when testing
$middleware = ['trimInput', 'setWarehouseTimezone'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
}

$api->version('v2', ['middleware' => []], function ($api) {
    // Assign Carton to Pallet
    $api->group(['prefix' => '/v2', 'namespace' => 'App\Api\ReplenishmentConfig\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V2'];
        });

        //Replenishment configuration
        $api->get('replenishment-config', [
            'action' => "viewReplenishmentConfig",
            'uses'   => 'ReplenishmentConfigController@getList'
        ]);
        $api->get('replenishment-config/{repConfId:[0-9]+}', [
            'action' => "viewReplenishmentConfig",
            'uses'   => 'ReplenishmentConfigController@show'
        ]);
        $api->post('replenishment-config', [
            'action' => "createReplenishmentConfig",
            'uses'   => 'ReplenishmentConfigController@store'
        ]);
        $api->get('replenishment-config/location', [
            'action' => "viewReplenishmentConfig",
            'uses'   => 'ReplenishmentConfigController@getCartonNumberOfLoc'
        ]);
        $api->get('/list-suggest-location', [
            'action' => 'viewReplenishmentConfig',
            'uses'   => 'ReplenishmentConfigController@suggestLocation'
        ]);



    });


});
