<?php

namespace App\Api\V1\Models;

use App\Sac;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelStr;

class SacModel extends AbstractModel
{
    /**
     * Constructor.
     */

    public function __construct()
    {
        $this->model = new Sac();
    }

    public function search($input, $with = [], $limit = 10) {
        $query = $this->make($with);

        if (!empty($input['whs_id'])) {
            $query->where('whs_id', $input['whs_id']);
        }

        if (!empty($input['types'])) {
            $query->whereIn('type', $input['types']);
        }

        if (isset($input['sac']) && $input['sac'] != '') {
            $query->where('sac', 'like', "%" . SelStr::escapeLike($input['sac']) . "%");
        }

        if (isset($input['sac_name']) && $input['sac_name'] != '') {
            $query->where('sac_name', 'like', "%" . SelStr::escapeLike($input['sac_name']) . "%");
        }

        if (isset($input['by']) && $input['by'] != '') {
            $query->where('range_uom', $input['by']);
        }

        if (isset($input['uom']) && $input['uom'] != '') {
            $query->where('unit_uom', $input['uom']);
        }

        if (array_has($input, 'sort')) {
            if (array_has($input['sort'], 'type_name')) {
                $input['sort']['type'] = $input['sort']['type_name'];
                unset($input['sort']['type_name']);
            }

            if (array_has($input['sort'], 'unit_uom_name')) {
                $input['sort']['unit_uom'] = $input['sort']['unit_uom_name'];
                unset($input['sort']['unit_uom_name']);
            }

            if (array_has($input['sort'], 'range_uom_name')) {
                $input['sort']['range_uom'] = $input['sort']['range_uom_name'];
                unset($input['sort']['range_uom_name']);
            }

            if (array_has($input['sort'], 'period_uom_name')) {
                $input['sort']['period_uom'] = $input['sort']['period_uom_name'];
                unset($input['sort']['period_uom_name']);
            }
        }

        $this->sortBuilder($query, $input);
        return $query->paginate($limit);
    }

    public function updateMulti(array $data)
    {
        foreach ($data as $item) {
            $this->model->where('sac_id', $item['sac_id'])->update([
                'price' => $item['price'],
                'description' => $item['description']
            ]);
        }
    }

    public function getListUnitUOM(string $invUOMType = null){
        $query = DB::table('inv_uom');
        if ($invUOMType){
            $query->where('inv_uom_type', $invUOMType);
        }
        return $query->get();
    }

    public function generateSacByType(string $type){
        $sac = DB::table('sac')
                ->where('type', $type)
                ->orderBy('sac', 'DESC')
                ->first();
        if(empty($sac)) {
            $sacChar    = substr($type, 0, 1);
            return $sacChar."000";
        }
        $sacChar    = substr($sac->sac, 0, 1);
        $sacNum     = (int)substr($sac->sac, 1) + 1;
        return $sacChar . sprintf('%03d', $sacNum);
    }
}

