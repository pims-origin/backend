<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Location;

/**
 * Class LocationModel
 *
 * @package App\Api\V1\Models
 */
class LocationModel extends AbstractModel
{
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    /**
     * @param $inZoneIds
     * @param array $exceptLocIds
     * @param $whsId
     *
     * @return mixed
     */
    public function getSuggestData($inZoneIds, $exceptLocIds = [], $whsId)
    {
        $query = $this->model
            ->select(['loc_id', 'loc_code'])
            ->where('loc_sts_code', 'AC')
            ->where('loc_whs_id', $whsId)
            ->whereIn('loc_zone_id', $inZoneIds);
        if (!empty($exceptLocIds)) {
            $query->whereNotIn('loc_id', $exceptLocIds);
        }

        return $query->get();
    }

    public function searchAutoOutPallet($whsId, $locCode, $limit)
    {
        $query = $this->model
            ->select([
                'location.loc_id',
                'location.loc_code',
                'location.rfid'
            ])
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('loc_type.loc_type_code', 'SHP')
            ->where('location.loc_whs_id', $whsId)
            ->where('location.deleted', 0)
            ->where('location.loc_sts_code', 'AC')
            ->where(function($query) use ($locCode){
                $query->where('location.loc_code', 'LIKE', "%{$locCode}%")
                    ->orWhere('location.loc_alternative_name', 'LIKE', "%{$locCode}%")
                    ->orWhere('location.rfid', 'LIKE', "%{$locCode}%");
            });

        $result = $query->take($limit)->get();
        return $result;
    }


    public function getLocationByLocCode($whsId, $locCode)
    {
        $query = $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('loc_type.loc_type_code', 'SHP')
            ->where('location.loc_whs_id', $whsId)
            ->where('location.deleted', 0)
            ->where('location.loc_code', $locCode);
        return $query->first();
    }


    public function getLocationByLocCodeWithOutType($whsId, $locCode)
    {
        $query = $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('location.loc_whs_id', $whsId)
            ->where('location.deleted', 0)
            ->where('location.loc_code', $locCode);

        return $query->first();
    }
}
