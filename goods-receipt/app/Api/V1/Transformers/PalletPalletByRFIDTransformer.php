<?php


namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Pallet;

class PalletPalletByRFIDTransformer extends TransformerAbstract
{
    //return result
    public function transform(Pallet $pallet)
    {
        return [
            'plt_id'       => object_get($pallet, 'plt_id'),
            'loc_id'       => object_get($pallet, 'loc_id'),
            'loc_name'     => object_get($pallet, 'loc_name'),
            'plt_num'      => object_get($pallet, 'plt_num'),
            'plt_sts'      => object_get($pallet, 'plt_sts', 'NA'),
            'ctn_ttl'      => object_get($pallet, 'ctn_ttl', 0),
            'rfid'         => object_get($pallet, 'rfid', null),
            'data'         => object_get($pallet, 'data', null),
        ];
    }

}
