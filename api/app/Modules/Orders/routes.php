<?php

$middleware = ['app', 'user', 'cus', 'whs', 'timezone'];

$router->group([
        'prefix' => '/rest/wms360/seldat/v1',
        'middleware' => $middleware,
        'namespace' => 'App\Modules\Orders\Controllers'
    ], function () use ($router) {

    $router->get('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/orders', [
        'uses'      => 'OrderController@index'
    ]);

    $router->get('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/orders/{odrHdrId:[0-9]+}', [
        'uses'      => 'OrderController@read'
    ]);

    $router->post('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/orders', [
        'uses'      => 'OrderImportController@store'
    ]);

    $router->patch('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/orders/{odrNum}/cancel', [
        'uses'      => 'OrderController@cancel'
    ]);

    $router->get('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/orders/shipped', [
        'uses'      => 'OrderController@shipped'
    ]);

    $router->put('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/orders/{odrHdrId:[0-9]+}', [
        'uses'      => 'OrderController@update'
    ]);

});