<?php

namespace Seldat\Wms2\Models;

/**
 * Class Shipment
 *
 * @property mixed ord_shipping_id
 * @property mixed whs_id
 * @property mixed cus_id
 * @property mixed bo_num
 * @property mixed ship_dt
 * @property mixed ship_sts
 * @property mixed ship_from_name
 * @property mixed ship_from_addr_1
 * @property mixed ship_from_addr_2
 * @property mixed ship_from_city
 * @property mixed ship_from_state
 * @property mixed ship_from_zip
 * @property mixed ship_from_country
 * @property mixed ship_from_sid
 * @property mixed shi_from_fob
 * @property mixed ship_to_name
 * @property mixed ship_to_addr_1
 * @property mixed ship_to_addr_2
 * @property mixed ship_to_city
 * @property mixed ship_to_state
 * @property mixed ship_to_zip
 * @property mixed ship_to_country
 * @property mixed ship_to_tel
 * @property mixed ship_to_fob
 * @property mixed bill_to_name
 * @property mixed bill_to_addr_1
 * @property mixed bill_to_addr_2
 * @property mixed bill_to_city
 * @property mixed bill_to_state
 * @property mixed bill_to_zip
 * @property mixed bill_to_country
 * @property mixed special_inst
 * @property mixed carrier
 * @property mixed trailer_num
 * @property mixed seal_num
 * @property mixed scac
 * @property mixed pro_num
 * @property mixed freight_charge_terms
 * @property mixed freght_charge_cost
 * @property mixed po_qty_ttl
 * @property mixed weight_ttl
 * @property mixed ctn_qty_ttl
 * @property mixed piece_qty_ttl
 * @property mixed vol_qty_ttl
 * @property mixed plt_qty_ttl
 * @property mixed fee_terms
 * @property mixed fee_term_cost
 * @property mixed cus_accept
 * @property mixed trailer_loaded_by
 * @property mixed freight_counted_by
 * @property mixed shipper_sign_dt
 * @property mixed carrier_sign_dt
 * @property mixed sts
 *
 * @package Seldat\Wms2\Models
 */
class Shipment extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shipment';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ship_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'bo_num',
        'bo_label',
        'ship_dt',
        'ship_sts',
        'ship_from_name',
        'ship_from_addr_1',
        'ship_from_addr_2',
        'ship_from_city',
        'ship_from_state',
        'ship_from_zip',
        'ship_from_country',
        'ship_from_sid',
        'shi_from_fob',
        'ship_to_name',
        'ship_to_addr_1',
        'ship_to_addr_2',
        'ship_to_city',
        'ship_to_state',
        'ship_to_zip',
        'ship_to_country',
        'ship_to_tel',
        'ship_to_fob',
        'bill_to_name',
        'bill_to_addr_1',
        'bill_to_addr_2',
        'bill_to_city',
        'bill_to_state',
        'bill_to_zip',
        'bill_to_country',
        'special_inst',
        'carrier',
        'trailer_num',
        'seal_num',
        'scac',
        'pro_num',
        'freight_charge_terms',
        'freight_charge_cost',
        'freight_counted_by',
        'po_qty_ttl',
        'weight_ttl',
        'ctn_qty_ttl',
        'piece_qty_ttl',
        'vol_qty_ttl',
        'cube_qty_ttl',
        'plt_qty_ttl',
        'fee_terms',
        'cus_accept',
        'trailer_loaded_by',
        'freight_counted_by',
        'shipper_sign_dt',
        'carrier_sign_dt',
        'sts',
        'deli_service',
        'party_acc',
        'is_attach',
        'cmd_id',
        'ship_method',
        'shipping_addr_id'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function odr_hdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\OrderHdr', 'ship_id', 'ship_id');
    }

    public function commodity()
    {
        return $this->hasOne(__NAMESPACE__ . '\Commodity', 'cmd_id', 'cmd_id');
    }

    public function bolOdrDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\BOLOrderDetail', 'ship_id', 'ship_id');
    }

    public function bolCarrierDtl()
    {
        return $this->hasOne(__NAMESPACE__ . '\BOLCarrierDetail', 'ship_id', 'ship_id');
    }

    public function fromState()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemState', 'sys_state_code', 'ship_from_state');
    }

    public function toState()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemState', 'sys_state_code', 'ship_to_state');
    }

    public function billState()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemState', 'sys_state_code', 'bill_to_state');
    }
}
