<?php

namespace App\Api\ReplenishmentConfig\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\ReplenishmentConfig\Models\ReplenishmentConfigModel;
use Seldat\Wms2\Models\Carton;

class SuggestLocationForRepConfTransformer extends TransformerAbstract
{
    public function transform(Carton $repConf)
    {
        return [
            'loc_id'   => array_get($repConf, 'loc_id', null),
            'loc_code' => array_get($repConf, 'loc_code', null),
            'loc_name' => array_get($repConf, 'loc_name', null),
            'ctnNo'    => array_get($repConf, 'ctnNo', null),
        ];
    }
}
