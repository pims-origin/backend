<?php
namespace App\Api\TEST\Controllers;


use App\Api\OUTBOUND\Models\CartonModel;
use App\Api\OUTBOUND\Models\LocationModel;
use App\Api\OUTBOUND\Models\OrderHdrModel;
use App\Api\OUTBOUND\Models\OutPalletModel;
use App\Api\OUTBOUND\Models\PalletModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\DamageCartonModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\LocationTypeModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\AsnDtlModel;
use App\MessageCode;
use App\MyHelper;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Laravel\Lumen\Routing\Controller;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\VirtualCarton;

class TestController extends Controller
{

    public function __construct()
    {
        if (env('APP_ENV') != 'testing') {
            return "Access denied!";
        }
    }

    public function index() {
        return "Unit test API.";
    }

    public function testLog() {
        $msg = MessageCode::get('WAP001');
        $data = [
            'data'    => null,
            'message' => $msg,
            'status'  => false,
        ];

        return new Response($data, 200, [], null);

        MyHelper::log("test", [1,2,8], ['fileName'=>'cuong']);
        MyHelper::logException("test", [1,2,8]);
        return "Wrote file done.";
    }
    public function updateOutPallet ($whsId, $palletRFID) {
        $pallet = new PalletModel();
        $pltNum = $pallet->getPalletIDWhereRFID($palletRFID);
        if (! $pltNum) {
            return "Error";
        }
        $oPallet = new OutPalletModel();
        return $oPallet->updateOutPalletMovement($whsId, $pltNum);

    }

    public function testUpdateWPFailCarton() {
        $cartons = [
            "0" => '',
            "1" => '',
            "2" => '',
        ];
        $ctnLists = (new CartonModel())->getActiveCartonByCtnRFID($cartons);

        if (count($ctnLists) != count($cartons)) {
            $trueRFIDs = array_pluck($ctnLists, 'rfid', null);
            $wrongRFIDS = array_diff($cartons, $trueRFIDs);
            $msg = "There is one of carton's rfid is not existed.";
            $data = [
                'data'    => $wrongRFIDS,
                'message' => $msg,
                'status'  => false,
            ];
            return new Response($data, 200, [], null);
        }
    }

    public function updatePalletPutBack($whsId, Request $request) {
        $input = $request->getParsedBody();
        //validate

        $palletRFID = $input['pallet_rfid'];
        $locRFID = $input['loc_rfid'];

        //get pallet by RFID and check exist
        $pltObj = (new PalletModel())->getPalletPutBackWhereRFID($palletRFID, $whsId);
        return $pltObj;
        //get loc by RFID and check exist
        $locObj = (new LocationModel())->getLocIdByRFID($locRFID);
        return $locObj;
        $arrLoc = $locObj->toArray();

        //check pallet put back to old RAC, only update is_movement = 0
        if ($pltObj->loc_id == $arrLoc['loc_id']) {
            $pltObj->is_movement = 0;
            $pltObj->save();
        }
        else { //pallet put back to new RAC

            //check loc is contained pallet or not
            return  (new PalletModel())->getLocationIsContainPallet($locObj->loc_id);

            //update old pallet location is not in use
           return (new PalletModel())->updatePalletPutBackNewLoc($pltObj->plt_id, $arrLoc);


        }
    }

    public function testEvtPutAway() {
        $whsId =1;
        $grObj = (new GoodsReceiptModel())->getFirstWhere(['gr_hdr_id'=>4]);
        if ($grObj) {
            //count pallet of
            $countPallet = (new PalletModel())->countPalletWhereGRHdrId($grObj->gr_hdr_id);
            $data = [
                'whs_id'    => $whsId,
                'cus_id'    => object_get($grObj, 'cus_id', ''),
                'owner'     => object_get($grObj, 'gr_hdr_num', ''),
                'evt_code'  => 'GPU',
                'trans_num' => object_get($grObj, 'gr_hdr_num', ''),
                'info'      => sprintf('Put %d pallet(s) on Rack completed.', $countPallet)
            ];
            //call Event tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($data);
        }
    }

    public function testEvtPutAwayToLocation() {
        $whsId =1;
        $palletObj = (new PalletModel())->getPalletByGrHdrId(5);
        dd($palletObj);
        $grObj = (new GoodsReceiptModel())->getFirstWhere(['gr_hdr_id'=>4]);
        if ($grObj) {
            //count pallet of
            $countPallet = (new PalletModel())->countPalletWhereGRHdrId($grObj->gr_hdr_id);
            $data = [
                'whs_id'    => $whsId,
                'cus_id'    => object_get($grObj, 'cus_id', ''),
                'owner'     => object_get($grObj, 'gr_hdr_num', ''),
                'evt_code'  => 'GPU',
                'trans_num' => object_get($grObj, 'gr_hdr_num', ''),
                'info'      => sprintf('Put %d pallet(s) on Rack completed.', $countPallet)
            ];
            //call Event tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($data);
        }
    }

    public function putawayEventTracking()
    {
        $palletRfid = 'FFFFFFFF0000000000000002';
        $locCode = '1213';

        $vtlObj = (new VirtualCartonModel())->getASNByVtlCtn($palletRfid);

        $data = [
            'whs_id'    => object_get($vtlObj, 'whs_id', null),
            'cus_id'    => object_get($vtlObj, 'whs_id', null),
            'owner'     => object_get($vtlObj, 'asn_hdr_num', ''),
            'evt_code'  => 'PUT',
            'trans_num' => object_get($vtlObj, 'ctnr_num', ''),
            'info'      => sprintf('Move %s to %s', $palletRfid, $locCode)
        ];

        return $data;
    }

    public function testCompleteSKU ($whsId, $asnDtlId) {
        /*
         * 1. Scan carton
         * 2. scan pallet
         * 3. call this function to complete
         */
        $asnDtl = AsnDtl::where('asn_dtl.asn_dtl_id', $asnDtlId)->first()->toArray();

        dd($asnDtl);

    }

    public function testRule()
    {
        $pallet = [
            'FF3456789123456789123456',
            'FF34567891234567891234512',
            'FF3456789123456789123451',
            'FF3456789123456789123453',
        ];
        if (! MyHelper::checkRule($pallet) ) {
            $msg = sprintf(MessageCode::get('WAP003'), 'carton');
            $data = [
                'data'    => null,
                'message' => $msg,
                'code' => 'WAP003',
                'status'  => false,
            ];
            return new Response($data, 200, [], null);
        }
    }

    public function getPalletNotInUse()
    {
        $pltRfid= "FF1234567878984";
        $checkExistedRealPallet =(new PalletModel())
            ->getModel()
            ->where('ctn_ttl', 0)
            ->where('rfid', $pltRfid)
            ->whereNotNull('zero_date')
            ->where('storage_duration', '!=', 0)
            ->first();
return $checkExistedRealPallet;

    }

    public function getWaveList()
    {
         return (new WavePickDtlModel())->getModel()->findWhere([
                'wv_id'  => 4,
                'whs_id' => 1,
            ])
            ->whereIn('wv_dtl_sts', ['PK', 'NW'])
            ->get();


    }


    public function generateData($num) {
        $arr = [];
        for($i=0;$i<$num;$i++){
            $rand = uniqid().uniqid();
            $randStr = strtoupper(substr($rand, 0, 24));
            $arr[] = "\"{$i}\":\"{$randStr}\"";
        }
        $str = "[\n".implode(",\n", $arr)."\n]";

        return $str;
    }


    public function setDamageCtn() {
        $ctnIDs = [8, 172];
        foreach ($ctnIDs as $ctnID) {
            $data = [
                'ctn_id' => $ctnID,
                'dmg_id' => 1,
                'dmg_note' => 'WAP set carton damage default.',
            ];
            (new DamageCartonModel())->getModel()->create($data);
        }
        return "OK";
    }

    public function updateLocationSts() {
        $ctnrID = 20;
        $asn_hdr_id = 16;
        $location = new \App\Api\V1\Models\LocationModel();
        return  $location->updateLocationSts($asn_hdr_id, $ctnrID);
    }
    
    public function testCreateGRHdr($asnHdrId, $ctnrId){
        $result = $this->goodsReceiptModel->createGRHdrNew($asnHdrId, $ctnrId);
        dd($result);
    }
    
    public function testCreateGRDtl($grHdrId, $asnDtlId){
        $grHdr = $this->goodsReceiptModel->getModel()->find($grHdrId);
        $asnDtl = $this->asnDtlModel->getModel()->find($asnDtlId);
        $result = $this->goodsReceiptDetailModel->createGrDtlNew($grHdr, $asnDtl);
        dd($result);
    }
    
    public function testCreatePalletIfNotExist($grDtlId, $grHdrId, $pltRfid) {
        
//        $this->vtlCtnModel->updatePalletRFIDNull($pltRfid, $ansHdrId, $ctnrId);
//        $this->vtlCtnModel->updateVtlCtnPltRfidByVtlCtnRfid(array_values($arrCtnRfid), $pltRfid);
        
        $grDtl = $this->goodsReceiptDetailModel->getModel()->find($grDtlId);
        $grHdr = $this->goodsReceiptModel->getModel()->find($grHdrId);
        $asnDetailId = object_get($grDtl, 'asn_dtl_id');
        $asnDetail = $this->asnDtlModel->getModel()->find($asnDetailId);
        $result = $this->palletModel->createPalletNew($pltRfid, $grDtl, $grHdr, $asnDetail);
        dd($result);
    }
    
    public function testDeleteExistNotInPltRfid(Request $request) {
        $input = $request->getParsedBody();
        $arrCtnRfid = array_get($input, 'pallet.ctn-rfid', null);
        $pltRfid = array_get($input, 'pallet.pallet-rfid', null);
        $grDtlId = array_get($input, 'gr_dtl_id', null);
        //$result = $this->virtualCartonModel->deleteExistNotInPallet($pltRfid, $arrCtnRfid);
        $result = $this->cartonModel->deleteExistNotInPallet($pltRfid, $grDtlId, $arrCtnRfid);
        dd($result);
    }
    
    public function testCreateCtnIfNotDefined(Request $request) {
        $input = $request->getParsedBody();
        $whsId = array_get($input, 'whs_id');
        $pltRfid = array_get($input, 'plt_rfid');
        $wrongRFIDS =  array_get($input, 'wrong_rfid');
        $asnDtlId = array_get($input, 'asn_dtl_id');
        $grDtlId = array_get($input, 'gr_dtl_id');
        $grDtlArr = (new GoodsReceiptDetailModel())->getModel()
                ->find($grDtlId)
                ->toArray();
        $asnDetailArr = (new AsnDtlModel())->getModel()
                ->find($asnDtlId)
                ->toArray();
        $grHdrId = array_get($grDtlArr, 'gr_hdr_id');
        $grHdr = (new GoodsReceiptModel())->getModel()
                ->find($grHdrId);
        $grHdrNum = object_get($grHdr, 'gr_hdr_num');
        $grModel = new GoodsReceiptModel();
        $grModel->createCtnIfNotDefined($whsId, $pltRfid, $wrongRFIDS, $grDtlArr, $asnDetailArr, $grHdrNum);
    }
}
