<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    //TODO
    $middleware[] = 'verifySecret';
    $middleware[] = 'setWarehouseTimezone';
}

// Handle application Route
$api->version('v1', function ($api) use ($middleware) {

    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
        // Send Email
        $api->get('/mails/send-invoice-email', 'MailController@sendInvoiceEmail');
        $api->post('/mails/reset-password', 'MailController@resetPassword');
        $api->post('/mails/setup-password', 'MailController@setupPassword');
        $api->post('/mails/welcome-user', 'MailController@welcomeUser');
        $api->post('/mails/user-reset-password', 'MailController@userResetPassword');
        
        $api->post('mails/report-issue', 'MailController@reportIssue');
        // Send Message Queue
        $api->post('/queue/send', 'QueueController@send');
    });

    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers', 'middleware' => $middleware],
        function ($api) {
            $api->get('settings', 'SettingController@settings');
            $api->get('settings/{key}', ['as' => 'setting.instance', 'uses' => 'SettingController@instance']);
            $api->post('settings', 'SettingController@create');
            $api->post('settings/{key}', 'SettingController@createByKey');
            $api->put('settings/{key}', 'SettingController@update');
            $api->delete('settings/{key}', 'SettingController@delete');
        });


    $api->group(['prefix' => 'v2', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
        $api->get('/', function () {
            return 'v2';
        });
    });

    $api->group(['prefix' => 'migrate-data', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
        $api->get('/', function () {
            return 'Migrate data HG';
        });

        //insert basic data, pallet, item, loction
        $api->get('/migrate/insert', ['namespace' => 'App\Api\V1\Controllers', 'uses' =>
            'MigrateController@store']);

        //insert carton
        $api->get('/migrate', ['namespace' => 'App\Api\V1\Controllers', 'uses' =>
            'MigrateController@update']);

        //update invt_sum, update pallet
        $api->get('/migrate/update-ivt-smr', ['namespace' => 'App\Api\V1\Controllers', 'uses' =>
            'MigrateController@updateIvtSmr']);




        // Inser basic data, container, item
        $api->get('/migrate/shipment/insert', ['namespace' => 'App\Api\V1\Controllers', 'uses' =>
            'MigrateShipmentController@store']);
        // Insert container
        $api->get('/migrate/shipment', ['namespace' => 'App\Api\V1\Controllers', 'uses' =>
            'MigrateShipmentController@update']);
    });

});
