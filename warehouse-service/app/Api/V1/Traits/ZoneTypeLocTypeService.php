<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/10/2016
 * Time: 10:53 AM
 */

namespace App\Api\V1\Traits;

trait ZoneTypeLocTypeService
{
    /**
     * @param $params
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create($params)
    {
        $uri = $this->getUri();

        //  Call api create warehouse
        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    public function edit($params, $zoneTypeId)
    {
        $uri = $this->getUri() . $zoneTypeId;

        $options = [
            'form_params' => $params
        ];

        return $this->client->put($uri, $options);
    }
}