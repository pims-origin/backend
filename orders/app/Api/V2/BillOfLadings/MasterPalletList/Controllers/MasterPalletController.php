<?php

namespace App\Api\V2\BillOfLadings\MasterPalletList\Controllers;

use App\Api\V2\BillOfLadings\MasterPalletList\Models\OutPalletModel;
use App\Api\V2\BillOfLadings\MasterPalletList\Transformers\MasterPalletTransformer;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Wms2\UserInfo\Data;
use mPDF;

/**
 * Class BOLController
 *
 * @package App\Api\V2\BillOfLadings\MasterPalletList\Controllers
 */
class MasterPalletController extends AbstractController
{
    protected $outPalletModel;

    /**
     * MasterPalletController constructor.
     *

     * @param OutPalletModel $outPalletModel
     */
    public function __construct(
        OutPalletModel $outPalletModel
    ) {
        $this->outPalletModel = $outPalletModel;
    }

    /**
     * @param $shipId
     * @param $orderDtlId
     * @param $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getList($whsId, $shipId, Request $request)
    {
        try {
            $masterPallets = $this->outPalletModel->getMasterPallet($whsId, $shipId);

            return $this->response->paginator($masterPallets, (new MasterPalletTransformer()));

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}
