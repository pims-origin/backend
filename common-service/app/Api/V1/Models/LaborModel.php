<?php

namespace App\Api\V1\Models;
use Seldat\Wms2\Models\Labor;

class LaborModel extends AbstractModel
{
    public function __construct(Labor $model = null)
    {
        $this->model = ($model) ?: new Labor();
    }

    public function  firstOrNew($where=[]) {
        return $this->model->firstOrNew($where);
    }

}
