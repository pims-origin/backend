<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\ReturnHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ReturnOrderModel extends AbstractModel
{

    protected $model;

    /**
     * @param ReturnHdr $model
     */
    public function __construct(ReturnHdr $model = null)
    {
        $this->model = ($model) ?: new ReturnHdr();
    }


    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        if (!empty($attributes['putter']) && $attributes['putter'] == 1) {
            $userId = JWTUtil::getPayloadValue('jti') ?? 0;
            $roles = DB::table('user_role')
                ->select(DB::raw('upper(item_name) as item_name'))
                ->where('user_id', $userId)
                ->get();

            $roles = array_pluck($roles, 'item_name', 'item_name');

            if (empty($roles['ADMIN']) && empty($roles['MANAGER'])) {
                $query->where('putter', $userId);
            }
        }

        $attributes = SelArr::removeNullOrEmptyString($attributes);


        if (isset($attributes['return_num'])) {
            $query->where('return_num', 'like', "%" . SelStr::escapeLike($attributes['return_num']) . "%");
        }

        if (isset($attributes['odr_num'])) {
            $query->where('odr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['odr_num']) . "%")
                ->groupBy('odr_hdr_num');
        }

        $query->whereHas('odrHdr.customer', function ($query) use ($attributes) {
            if (isset($attributes['cus_id'])) {
                $query->where('cus_id', 'like', "%" . SelStr::escapeLike($attributes['cus_id']) . "%");
            }
        });

        if (isset($attributes['return_sts'])) {
            $query->where('return_sts', 'like', "%" . SelStr::escapeLike($attributes['return_sts']) . "%");
        }

        if (!empty($attributes['no_assign'])) {
            $query->where('return_sts', '!=', "AS");
        }

        //Status::getByKey("RETURN-ORDER", 'New')
        if (isset($attributes['type']) && $attributes['type'] == 'putaway') {
            $query->whereNotIn('return_sts', ['NW']);
        }

        $query->whereHas('odrHdr', function ($q1) use ($attributes) {
            $q1->where('whs_id', $attributes['whs_id']);
        });

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'odr_num') {
                    $attributes['sort']['odr_hdr_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

            }
        }

        $this->sortBuilder($query, $attributes);

        $query->whereHas('odrHdr', function ($q1) use ($attributes) {
            $q1->where('whs_id', $attributes['whs_id']);
        });

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @return mixed
     */
    public function getAllOrderIdInReturnHdr()
    {
        return $this->model
            ->get();
    }

}