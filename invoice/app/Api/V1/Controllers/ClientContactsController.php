<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Response as IlluminateResponse;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\ClientContactsModel;
use App\Api\V1\Transformers\ClientContactsTransformer;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

class ClientContactsController extends AbstractController
{
    protected $clientContactsModel;
    protected $clientContactsTransformer;

    /*
    ****************************************************************************
    */

    public function __construct(IRequest $request)
    {
//        $object = new AuthenticationService($request);
//
//        parent::__construct($request, $object);

        $this->clientContactsModel = new ClientContactsModel();
        $this->clientContactsTransformer = new ClientContactsTransformer();
    }

    /*
    ****************************************************************************
    */

    public function get(Request $request)
    {
        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', PAGING_LIMIT);

        $model = $this->clientContactsModel;
        $transformer = $this->clientContactsTransformer;

        try {

            $clientContacts = $model->search($input, [], $limit);

            return $this->response->paginator($clientContacts, $transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /*
    ****************************************************************************
    */

    public function setDafaultContact($contactID, $clientID, Request $request)
    {
        $model = $this->clientContactsModel;
        $transformer = $this->clientContactsTransformer;

        try {
            $results = $model->setDafaultContact($contactID, $clientID);

            return $this->response->item($results, $transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
    ****************************************************************************
    */

    public function removeContacts(Request $request)
    {
        $input = $request->getParsedBody();

        $model = $this->clientContactsModel;
        $transformer = $this->clientContactsTransformer;

        try {

            $results = $model->removeContacts($input['contacts']);

            return $this->response->item($results, $transformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
    ****************************************************************************
    */

    public function addContact($clientID, Request $request)
    {
        $contactInfo = $request->getParsedBody();

        $model = $this->clientContactsModel;
        $transformer = $this->clientContactsTransformer;

        try {

            $results = $model->addContact($clientID, $contactInfo);

            return $this->response->item($results, $transformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /*
    ****************************************************************************
    */

}
