<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LocationModel;
use App\Api\V1\Validators\RefillValidator;
use App\Api\V1\Transformers\RefillTransformer;
use App\Api\V1\Models\RefillModel;
use App\Api\V1\Models\CartonModel;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Illuminate\Http\Request as IRequest;
use Seldat\Wms2\Utils\SelExport;
use Illuminate\Support\Facades\DB;


class RefillController extends AbstractController
{
    /*
     * @var LocationModel
    */
    protected $refillModel;

    /*
     * @var LocationTransformer
    */
    protected $refillTransformer;
    /*
     * @var LocationValidator
    */
    protected $refillValidator;

    /*
     * @var $locationModel
     */
    protected $locationModel;

    /**
     * @var $refill
     */
    protected $refill;

    /*
     * @var $cartonModel
     */
    protected $cartonModel;

    /**
     * @param RefillModel $refillModel
     * @param RefillValidator $refillValidator
     * @param LocationModel $locationModel
     * @param RefillTransformer $refillTransformer
     */
    public function __construct(
        RefillModel $refillModel,
        RefillValidator $refillValidator,
        LocationModel $locationModel,
        CartonModel $cartonModel,
        RefillTransformer $refillTransformer
    ) {
        parent::__construct();
        $this->refillValidator = $refillValidator;
        $this->refillModel = $refillModel;
        $this->locationModel = $locationModel;
        $this->cartonModel = $cartonModel;
        $this->refillTransformer = $refillTransformer;
    }

    /**
     * @param $refillId
     *
     * @return Response|void
     */
    public function show($refillId)
    {
        try {
            $refill = $this->refillModel->loadBy(['rf_id' => $refillId])->first();

            return $this->response->item($refill, $this->refillTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $locationId
     *
     * @return Response|void
     */
    public function showRefillAccordingToLocation($locationId)
    {
        try {
            $refills = $this->refillModel->findWhere(['loc_id' => $locationId]);

            return $this->response->collection($refills, $this->refillTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $refillId
     */
    public function destroy($refillId)
    {
        if (!$this->refillModel->checkWhere(
            [
                'rf_id' => $refillId
            ])
        ) {
            return $this->response->errorBadRequest("Refill is not existed!");
        }

        try {
            if ($this->refillModel->deleteRefill($refillId)) {
                return $this->response->errorBadRequest("Success!");
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param $request
     *
     * @return Response|void
     */
    protected function upsert($request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        //$this->refillValidator->validate($input);

        try {
            if ($input) {
                //check duplicate in input
                $allRfIdsWillGetToDeletes = array_pluck($input['data'], 'item_id');

                if (count(array_unique($allRfIdsWillGetToDeletes)) < count($allRfIdsWillGetToDeletes)) {
                    return $this->response->errorBadRequest(Message::get("BM006", "Refill"));
                    // Array has duplicates
                }

                // check existed item
                $items = $this->checkExistedItem($input['data']);

                if (!empty($items)) {
                    $return = [
                        "errors" => [
                            'message'     => 'These Items are existed in another location',
                            'status_code' => 400,
                            'data'        => $items
                        ]
                    ];
                    return $this->response->noContent()->setContent($return)->setStatusCode(Response::HTTP_BAD_REQUEST);
                }

                DB::beginTransaction();
                //--delete refillIds is not used according location
                if ($input['data']) {
                    $locId = array_pluck($input['data'], 'loc_id', 'loc_id');
                    $allRfInfos = $this->refillModel->loadBy(['loc_id' => $locId]);
                    $allRfIdsWillGetToDeletes = array_pluck($allRfInfos, 'rf_id', 'rf_id');//(3=>3, 1=>1)

                    $allRfIdsWillGetToUpdates = array_pluck($input['data'], 'rf_id');//(0=>1, 1=>null)//input
                    foreach ($input['data'] as $inputN) {
                        if (isset($inputN['rf_id']) && isset($allRfIdsWillGetToDeletes[$inputN['rf_id']]) &&
                            ($inputN['rf_id'] == $allRfIdsWillGetToDeletes[$inputN['rf_id']])
                        ) {
                            unset($allRfIdsWillGetToDeletes[$inputN['rf_id']]);
                            continue;
                        }
                    }
                    $this->refillModel->deleteRefills($allRfIdsWillGetToDeletes);
                }
                //--/delete refillIds is used according location

                foreach ($input['data'] as $inputS) {
                    $refillId = array_get($inputS, 'rf_id', 0);
                    $params = [
                        'loc_code' => array_get($inputS, 'loc_code', ''),
                        'loc_id'   => array_get($inputS, 'loc_id', ''),
                        'item_id'  => array_get($inputS, 'item_id', ''),
                        'cus_id'   => array_get($inputS, 'cus_id', ''),
                        'sku'      => array_get($inputS, 'sku', ''),
                        'color'    => array_get($inputS, 'color', null),
                        'size'     => array_get($inputS, 'size', 0),
                        'upc'      => array_get($inputS, 'upc', ''),
                        'lot'      => array_get($inputS, 'lot', ''),
                        'min'      => is_numeric($inputS['min']) ? $inputS['min'] : null,
                        'max'      => is_numeric($inputS['max']) ? $inputS['max'] : null,

                    ];

                    //Check: Refill is not existed!
                    if ($refillId) {
                        $params['rf_id'] = $refillId;
                        $refill = $this->refillModel->getFirstWhere(['rf_id' => $refillId]);
                        if (empty($refill)) {
                            throw new \Exception(Message::get("BM017", "Refill"));
                        }
                    }
                    // check duplicate with database
                    $chkDupCode = true;
                    $rf = $this->refillModel->getFirstWhere([
                        'loc_id'  => $params['loc_id'],
                        'item_id' => $params['item_id'],
                    ]);

                    if ($rf && ($refillId)) {
                        $chkDupCode = ($rf->rf_id != $refillId);
                    }
                    if ($rf && $chkDupCode) {
                        return $this->response->errorBadRequest(Message::get("BM006", "Refill"));
                    }

                    if ($refillId) {
                        //update
                        $this->refillModel->refreshModel();
                        $this->refillModel->update($params);
                    } else {
                        //create
                        $this->refillModel->refreshModel();
                        $this->refillModel->create($params);
                    }
                }
            }
            DB::commit();

            return ["data" => "successful"];

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     *
     * @return Response|void
     */
    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    /**
     * @param Request $request
     *
     * @return Response|void
     */
    public function update(Request $request)
    {
        return $this->upsert($request);
    }

    /**
     * @param array $arrItem
     *
     * @return Response|void
     */
    public function checkExistedItem(array $arrItem)
    {
        $result = null;
        $arrExistedItem = [];

        foreach ($arrItem as $item) {
            $checkInCarton = $this->cartonModel->checkIsEcomByItemIdAndLocId($item['item_id'], $item['loc_id']);
            $checkInRefill =
                $this->refillModel->checkByItemIdAndLocId($item['item_id'], $item['loc_id']);
            if ($checkInCarton || $checkInRefill) {
                $arrExistedItem[] = $item;
            }
        }

        if (!empty($arrExistedItem)) {
            $result = $arrExistedItem;
        }

        return $result;
    }


}
