<?php

namespace Seldat\Wms2\Models;


class InvoiceVolumeRates extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inv_vol_rates';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'chg_code_id',
        'chg_uom_id',
        'min_vol',
        'max_vol',
        'chg_type_id'
    ];

    
    public function chargeCode()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ChargeCode', 'chg_code_id', 'chg_code_id');
    }

    public function systemUom()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemUom', 'chg_uom_id', 'sys_uom_id');
    }
    
    public function chargeType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ChargeType', 'chg_type_id', 'chg_type_id');
    }

}