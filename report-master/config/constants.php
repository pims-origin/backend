<?php

return [
       'report'         => [
        'array_key'        => [
            'name'      => 'Field Name',
            'dataType'      => 'Data Type',
            'defaultDisplayName'            => 'Default Display name',
            'displayName'            => 'Display name',
            'searchType' => 'Search Type',
            'condition' => 'Search Condition',
            'width' => 'Width',
            'fixed' => 'Fixed',
            'show' => 'On/Off',
            "index" => 'Index',
        ],
        'ext_file'         => ['csv', 'xls', 'xlsx'],
        'limit_rows'       => 1000,
    ],
    'lang' => [
        'array_key'        => [
            'category'    => 'Category(*)',
            'message'     => 'Message(*)',
            'translation' => 'Translation',
        ],
        'array_key_sap'    => [
            'Category'    => 'category',
            'Message'     => 'message',
            'Translation' => 'translation',
        ],
        'prefix'           => 'LUS',
        'ext_file'         => ['csv', 'xls', 'xlsx'],
        'limit_rows'       => 1000,
        'col_heading_file' => [
            'standard' => 3,
            'sap'      => 27
        ],
    ],
    'logo'         => [
        'ext_file'         => ['png', 'jpg', 'jpeg', 'bmp'],
    ],


];
