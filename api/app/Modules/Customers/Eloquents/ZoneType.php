<?php

namespace App\Modules\Customers\Eloquents;

class ZoneType extends Model
{


    protected $table = 'zone_type';

    /**
     * @var array
     */
    protected $fillable = [
        'zone_type_name',
        'zone_type_code',
        'zone_type_desc'
    ];

    public function zone()
    {
        return $this->hasOne(__NAMESPACE__ . '\Zone', 'zone_type_id', 'zone_type_id');
    }
}
