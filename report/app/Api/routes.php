<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'setWarehouseTimezone';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V1'];
        });

        $api->get('/report-list', 'ReportController@getListReport');
        $api->get('/inventory', 'InventoryController@search');
        $api->get('/available-inventory', 'AvailableInventoryController@index');
        $api->get('/style-locations', 'StyleLocationsController@index');
        $api->get('/warehouse-locations', 'WarehouseLocationsController@index');
        $api->get('/inventory-history', 'InventoryHistoryController@index');
        $api->get('/customers', 'CustomersController@index');
        $api->get('/orders', 'OrderController@index');
        $api->get('/online-orders', 'OnlineOrdersController@index');
        $api->get('/wavePicks', 'WavePicksController@index');
        $api->get('/open-orders', 'OpenOrdersController@index');

        $api->get('/receiving-report', 'ReceivingReportController@index');

        $api->post('/open-orders/update/{orderID:[0-9]+}/{userID:[0-9]+}',
            'OpenOrdersController@updateStatus');

        $api->get('/shipping-report/auto-sku/{whs_id}',
            'InventoryController@autoSku');

        $api->get('/shipping-report/auto-order-num/{whs_id}',
            'InventoryController@autoOrderNum');

        $api->get('/shipping-report/customers/{whs_id}',
            'InventoryController@customers');

        //inventory report
        $api->get('/inventory/customers/{whs_id}', 'InventoryController@customersInvDropDown');
        $api->get('/inventory/auto-sku/{whs_id}', 'InventoryController@skuInvAutoComplete');

        //inventory export
        $api->get('/inventory/export', 'InventoryController@exportInventory');
        //daily pallet report
        $api->get('/pallet/customers/{whs_id}/{cus_id}', 'DailyPalletController@dailyPalletReport');

        //daily inventory report
        $api->get('/inventory/customers/{whs_id}/{cus_id}', 'DailyInventoryController@dailyInventoryReport');
        $api->get('/inventory/correct-daily-inventory', 'DailyInventoryController@correctDailyInventory');

        $api->get('/inventory/daily_report/{whs_id}/{cus_id}', 'DailyInventoryController@changeDailyInventoryReport');

        $api->get('/inventory/update-report/{whs_id}/{cus_id}', 'DailyInventoryController@updateReport');

        //goods receipt report
        $api->get('/goods-receipt/reports/{whs_id}', 'GoodsReceiptController@search');
        $api->get('/goods-receipt/reports/{whs_id}/export', 'GoodsReceiptController@export');
        $api->get('/goods-receipt/customers/{whs_id}', 'GoodsReceiptController@customersGrDropDown');
        $api->get('/goods-receipt/auto-sku/{whs_id}', 'GoodsReceiptController@skuGrAutoComplete');
        $api->get('/goods-receipt/auto-gr-num/{whs_id}', 'GoodsReceiptController@autoGrNum');
        $api->get('/goods-receipt/auto-ref-code/{whs_id}', 'GoodsReceiptController@autoRefCode');
        $api->get('/goods-receipt/auto-ctnr-num/{whs_id}', 'GoodsReceiptController@autoCtnrNum');

        //SKU Tracking report
        $api->get('/sku-tracking/reports/{whs_id}', 'GoodsReceiptController@skuTrackingSearch');
        $api->get('/sku-tracking/auto-sku-tracking/{whs_id}', 'GoodsReceiptController@skuTrackingAutoComplete');

        //ASN
        $api->get('/asn/receiving-slip/{whs_id}/asn/{asn_id:[0-9]+}', 'GoodsReceiptController@exportReceivingSlip');

        //pallet report
        $api->get('/pallet/reports/{whs_id}', 'PalletController@search');
        $api->get('/pallet/customers/{whs_id}', 'PalletController@customersPalletDropDown');
        $api->get('/pallet/auto-sku/{whs_id}', 'PalletController@skuPalletAutoComplete');

        //lpn report
        $api->get('/lpn/reports/{whs_id}', 'LPNCartonController@search');

        //carton report
        $api->get('/carton/reports/{whs_id}', 'CartonsController@search');
        $api->get('/carton/customers/{whs_id}', 'CartonsController@customersCartonsDropDown');
        $api->get('/carton/status', 'CartonsController@cartonsStatusDropDown');
        $api->get('/carton/auto-sku/{whs_id}', 'CartonsController@skuCartonsAutoComplete');
        $api->get('/carton/auto-ctnr-num/{whs_id}', 'CartonsController@autoCtnrNum');
        $api->get('/carton/auto-ctn-num/{whs_id}', 'CartonsController@autoCtnNum');
        $api->get('/carton/auto-loc-code/{whs_id}', 'CartonsController@autoLocCode');
        $api->get('/carton/auto-rfid/{whs_id}', 'CartonsController@autoRFID');
        $api->get('/carton/auto-pallet-rfid/{whs_id}', 'CartonsController@autoPalletRFID');
        $api->get('/carton/get-loc-type', 'CartonsController@getLocType');

        $api->get('{whs_id:[0-9]+}/location-history', 'LocationHistoryController@search');

        //Tuyen
        $api->get('/auto-complete/loc-row', 'AutoCompleteController@locationRow');
        $api->get('/auto-complete/loc-level', 'AutoCompleteController@locationLevel');
        $api->get('/auto-complete/loc-aisle', 'AutoCompleteController@locationAisle');
        $api->get('/master-inventory-report/customer/{cus_id:[0-9]+}', 'MasterInventoryReportController@getMasterInventoryReport');

        $api->group(['prefix' => 'forecast'], function ($api) {
            $api->post('/import', 'ForecastController@import');
            $api->get('/show', 'ForecastController@show');
            $api->get('/get-year-month-have-data/{cusId:[0-9]+}', 'ForecastController@getYearMonthHaveData');
        });

        $api->get('/storage-aging', 'StorageController@storageAging');
        
        $api->get('/storages', 'StorageController@search');
//        $api->get('/storages/reports/{whsId:[0-9]+}', 'StorageController@report');
        $api->get('/storages/reports/{whsId:[0-9]+}', 'StorageController@storageReportDaily');

        $api->get('/labor-tracking/reports/{whsId:[0-9]+}', 'LaborReportController@search');
        $api->get('/labor-tracking/reports/{whs_id:[0-9]+}/popupDetail/{owner}/{trans_num}/{user_id}', 'LaborReportController@popupDetail');
    });
});
