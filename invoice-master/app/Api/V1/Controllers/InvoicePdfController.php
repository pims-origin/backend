<?php

namespace App\Api\V1\Controllers;

use Illuminate\Support\Facades\App;

class InvoicePdfController extends AbstractController
{

    public function __construct() {

    }

    public function exportPdf() {
        header("Content-type:application/pdf");

        header("Content-Disposition:attachment;filename='downloaded.pdf'");
        readfile(base_path('public/document.pdf'));
//        $data = [];
//        $pdf = \PDF::loadView('pdf.invoice', $data);
//        return $pdf->download();
    }
}
