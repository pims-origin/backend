<?php

namespace App\Api\V1\Models;

use App\CustomerMeta;
use App\Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class CustomerMetaModel extends AbstractModel
{
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new CustomerMeta();
    }
    /**
     * Get Currency By current User's warehouse
     * @param int $cusId
     * @return string
     * */
    public function getCurrencyByCurrentWhs($cusId) {
        //Get Customer currency
        $redis = new Data();
        $currentWhsId = $redis->getCurrentWhs();
        $cusMeta = new CustomerMetaModel();
        $currency = 'USD';
        $currencyMetaData = $cusMeta->getFirstWhere([
            'cus_id'    => $cusId,
            'qualifier' => 'CUR',
        ]);
        if (!empty($currencyMetaData)) {
            $currencyData = json_decode($currencyMetaData->value, true);
            if (isset($currencyData[$currentWhsId])){
                $currency = $currencyData[$currentWhsId]['currency'];
            }
        }
        return $currency;
    }

    /**
     * Check currency already has Invoice or WorkOrder
     * @param int $cusId
     * @return bool
     * */
    public function checkCurrencyEditableOrNot($cusId) {
        $redis = new Data();
        $currentWhsId = $redis->getCurrentWhs();
        //check Invoice
        $checkInvoice = DB::table('invoice_hdr')
            ->where('whs_id', $currentWhsId)
            ->where('cus_id', $cusId)
            ->first();
        //check WorkOrder
        $checkWorkOrder = DB::table('vas_hdr')
            ->where('whs_id', $currentWhsId)
            ->where('cus_id', $cusId)
            ->first();
        if ($checkWorkOrder || $checkInvoice) {
            return false;
        }
        return true;
    }

    public function updateCurrencyByCusId($cusId, $value) {
        $this->model
            ->where('cus_id', $cusId)
            ->where('qualifier', 'CUR')
            ->update([
                'value' => json_encode($value),
            ]);
    }

    /**
     * @param $cusId
     * @param $value
     */
    public function updateTaxByCusId($cusId, $value) {
        $this->model
            ->where('cus_id', $cusId)
            ->where('qualifier', 'TAX')
            ->update([
                'value' => json_encode($value),
            ]);
    }

    public function getTaxByWarehouse($cusId) {
        //Get Customer currency
        $redis = new Data();
        $currentWhsId = $redis->getCurrentWhs();

        $cusMeta = new CustomerMetaModel();
        $taxMetaData = $cusMeta->getFirstWhere([
            'cus_id'    => $cusId,
            'qualifier' => 'TAX',
        ]);

        $tax = 0;
        if (!empty($taxMetaData)) {
            $taxMetaData = json_decode($taxMetaData->value, true);
            if (isset($taxMetaData[$currentWhsId])){
                $tax = $taxMetaData[$currentWhsId]['sale_tax'];
            }
        }
        return $tax;
    }
}

