<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

/**
 * Class OrderDtlValidator
 *
 * @package App\Api\V1\Validators
 */
class OrderDtlValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'ord_dtl_id'       => 'required|integer|exists:order_hdr,ord_hdr_id',
            'whs_id'           => 'required|integer|exists:warehouse,whs_id',
            'cus_id'           => 'required|integer|exists:customer,cus_id',
            'itm_id'           => 'required|exists:item,item_id',
            'uom_id'           => 'required|integer|exists:system_uom,sys_uom_id',
            'request_qty'      => 'integer',
            'qty_to_allocated' => 'integer',
            'allocated_qty'    => 'integer',
            'damaged'          => 'integer',
            'special_handling' => 'integer',
            'back_ord'         => 'integer',
        ];
    }
}
