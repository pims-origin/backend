<?php

namespace App\Api\V1\Models;

use \DateTime;
use App\Invoice;

use Illuminate\Support\Facades\DB;
use App\Api\V1\Models\CommonInvoiceModel;
use App\Api\V1\Models\InvoicingSummaryModel;
use App\Api\V1\Models\VolumeRatesModel;

use Seldat\Wms2\Models\InvoiceCost;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\InvoicingSummary;
use Seldat\Wms2\Models\OrderCarton;


class StorageChargeCodeModel extends AbstractModel
{
    /**
    * @var
    */
    protected $invSum;
    protected $cusCharge;
    protected $vol;
    protected $common;
    protected $volRates;
    protected $carton;
    protected $plt;
    protected $inv;
    protected $ordCarton;
    protected $ivcSumModel;

    static $monthDays;
    
    public $commonUoms;
    public $currentUoms;
    public $monthlyUoms;
    

    /**
    * CommonInvoice Model Constructor
    * VolumeRates Model Constructor
    */

    public function __construct() {
        $this->plt = new Pallet();
        $this->carton = new Carton();
        $this->cusCharge =  new InvoiceCost();
        $this->inv = new InvoicingSummary();
        $this->ordCarton = new OrderCarton();

        $this->common = new CommonInvoiceModel();
        $this->vol = new VolumeRatesModel();
        $this->ivcSumModel = new InvoicingSummaryModel();
    }

    /**
    * Storage charge codes
    *
    * @param array params
    * @param array recNums
    * @param string returnRecs
    * @return array
    */

    public function calculate($params, $model)
    {
        $totals = $billedDates = [];
        
        $type = Invoice::ST;
        
        $volRangeUOMs = $this->ivcSumModel->getVolRangeUOMs();
        $commonUOMs = $this->getStorCommonUOMs();
        
        $this->commonUoms = array_merge($commonUOMs, $volRangeUOMs);

        $this->currentUoms = $this->getCurrentUOMs();

        $this->monthlyUoms = $this->getMonthlyUOMs();
      
        $custID = getDefault($params['custID']);
        $whsID = getDefault($params['whs_id'], NULL);

        $passedEnd = $params['endDate'];

        $storage = $params['details'] ?
            getDefault($params['items']['Storage']) : [];

        if ($params['details'] && array_filter($params['items']) && ! $storage) {
            return [$custID => 0];
        }
                
        self::$monthDays = date('t', strtotime($passedEnd));
        
        $uomCusts = $model->customerUOMs($type);

        $custUOMs = $model->custUOMs($type);

        $allUOMs = array_keys($uomCusts);
        $allCusts = array_keys($custUOMs);

        //get free days charge codes
        $custChgResults = array_get($params, 'uomRes.info.' . $type, []);
        $chgCodeIDs = array_column($custChgResults, 'chg_cd_id');

        // Don't run if there are no customers with charge codes
        if (! $allCusts) {
            return [];
        }

        $billed = $this->common->getBillableDts([
            'cat' => 's',
            'period' => 'daily',
            'dates' => $params,
            'custs' => $allCusts,
            'details' => TRUE,
            'getBilled' => TRUE,
        ]);


        foreach ($billed as $dt)
        {
            $billedDates = $dt;
        }
        
        
        //volume range
        $volumeModel = $this->ivcSumModel->volumeRanges(Invoice::ST);
     
        $data = [
            'startDate' => $params['startDate'],
            'endDate' => $params['endDate'],
            'custID' => $custID,
            'whsID' => $whsID,
            'billedDates' => $billedDates,
            'custUOMs' => $custUOMs,
            'chgCodeIDs' => $chgCodeIDs,
            'volumeModel' => $volumeModel
        ];

        //get free days data
        $freeResults = $this->getFreedays($custID, $whsID, $chgCodeIDs);

        if ($freeResults) {
            
            foreach ($freeResults as $row) {
                $cusID = $row['cus_id'];
                $uom = $row['uom'];
                
                $custStor = $this->getSummaryData($data, $row);

                $finalResults = $this->calculateStor($custStor, $row);

                $totals[$cusID][$uom] = $this->getChargeQty($finalResults, $row, $data);
            }
  
            return $totals;
        }
        
             
        //1 - commonUoms
        $selectCommon = array_intersect($this->commonUoms, $allUOMs);
        $commonResult = $this->getQuery($data, $selectCommon);
             

        //2 - currentUoms
        $selectCurrent = array_intersect($this->currentUoms, $allUOMs);
        $currentResult = $this->getQuery($data, $selectCurrent);


        //3- monthlyUoms
        $selectMonthly = array_intersect($this->monthlyUoms, $allUOMs);
        $monthlyResult = $this->getQuery($data, $selectMonthly);
        
        
        //final
        $finalResult = array_merge($commonResult, $currentResult, $monthlyResult);

        foreach ($finalResult as $row) {
            $custID = $row['cus_id'];
            $uom = $row['uom'];

            $custHasUOM = getDefault($data['custUOMs'][$custID][$uom]);

            if (! $custHasUOM) {
                continue;
            }

            $totals[$custID][$uom] = $row['qty'];
        }

        return $totals;
    }

    /**
    * @custID array
    * @whsID array
    * @return integer
    */

    public function getFreedays($custID, $whsID, $chgCodeIDs)
    {
        $days = $this->cusCharge
                     ->join('free_days AS f', 'f.chg_code_id', '=', 'invoice_cost.chg_code_id')
                     ->join('charge_code AS ch', 'ch.chg_code_id', '=', 'f.chg_Code_id')
                     ->join('system_uom AS sy', 'sy.sys_uom_id', '=', 'ch.chg_uom_id')
                     ->select('f.cus_id',
                            'f.whs_id',
                            'days',
                            'f.chg_code_id',
                            'ch.chg_code_name',
                            'sys_uom_name AS uom' 
                            )
                     ->where('f.cus_id', $custID)
                     ->where('f.whs_id', $whsID)
                     ->whereIn('f.chg_code_id', $chgCodeIDs)
                     ->where('f.sts', 1)
                     ->groupBY('f.chg_code_id')
                     ->get()
                     ->toArray();

        return $days;
    }

       
    /**
    * @data array
    * @uoms array
    * @return array
    */

    
    public function getQuery($data, $uoms)
    {
        $op = array_diff($uoms, $this->currentUoms) ? '<=' : '=';

        $qty = array_diff($uoms, $this->monthlyUoms) ? 'SUM(qty)' : 'SUM(qty) / ' . self::$monthDays;

        $query = $this->inv
                            ->join('charge_code AS ch', 'ch.chg_code_id', '=', 'invoice_summary.chg_code_id')
                            ->join('system_uom AS s', 's.sys_uom_id', '=', 'invoice_summary.chg_uom_id')
                            ->join('charge_type AS ct', 'ct.chg_type_id', '=', 'invoice_summary.chg_type_id')
                            ->select('cus_id',
                                    'whs_id',
                                    'ch.chg_code_id',
                                    'chg_code_name',
                                    'sys_uom_name AS uom',
                                     DB::raw($qty . ' AS qty')
                                    )
                             ->whereIn('sys_uom_name', $uoms)
                             ->where('chg_type_name', Invoice::ST)
                             ->where('dt', '>=', $data['startDate'])
                             ->where('dt', $op, $data['endDate'])
                             ->where('cus_id', $data['custID'])
                             ->where('whs_id', $data['whsID'])
                             ->where('inv_sts', 1)
                             ->where('sum_sts', 'billable')
                             ->whereNull('inv_num')
                             ->where(function($q) use($data) {
                                    $data['billedDates'] ? $q->whereNotIn('dt', $data['billedDates']) : NULL;
                             })
                             ->groupBy('cus_id', 'whs_id', 'chg_code_id');

        $result =  $query->get()->toArray();

        if (! $result) {
            return [];
        }

        return $result;
    }
    
    /**
    * @param array
    * @freeResults array
    * @return array
    */
    
    public function getSummaryData($param, $freeRes) 
    {
        $storData = [];
         
        $chgCodeID = $freeRes['chg_code_id'];
   
        $startDate = $param['startDate'];
        $priorStartDtObj = new \DateTime($startDate . ' -' . $freeRes['days']. ' day');
        $priorStartDate = $priorStartDtObj->format('Y-m-d');
        
        $param['priorStartDate'] = $priorStartDate;
        
        //query from invoice_summary table - free days before the Billing StartDate
        $sumResults = $this->inv
                            ->join('charge_code AS ch', 'ch.chg_code_id', '=', 'invoice_summary.chg_code_id')
                            ->join('charge_type AS ct', 'ct.chg_type_id', '=', 'invoice_summary.chg_type_id')
                            ->select('cus_id',
                                    'whs_id',
                                    'ch.chg_code_id',
                                    'chg_code_name',
                                    'dt',
                                    DB::raw('qty')
                            )
                            ->where('chg_type_name', Invoice::ST)
                            ->where('ch.chg_code_id', $chgCodeID)
                            ->where('dt', '>=', $priorStartDate)
                            ->where('dt', '<=', $param['endDate'])
                            ->where('cus_id', $param['custID'])
                            ->where('whs_id', $param['whsID'])
                            ->where('inv_sts', 1)
                            ->where('sum_sts', 'billable')
                            ->orderBy('dt')
                            ->lists('qty', 'dt')
                            ->toArray();

        $dateRanges = array_keys($sumResults);


        foreach ($dateRanges as $date) {
            $val = getDefault($sumResults[$date]) ? $sumResults[$date] : 0;
            $storData[$date]['summary'] = $val;
        }

        $param['storData'] = $storData;
        $param['dateRanges'] = $dateRanges;
        
        $results = $this->shippingData($param, $freeRes);

        return $results;
    }
    
    
    /**
    * @stor array
    * @return array
    */
    
    public function shippingData($param, $freeRes) 
    {
        $volModel = $param['volumeModel'];
        $dateRanges = $param['dateRanges'];
        $storData = $param['storData'];
             
        $uom = $freeRes['uom'];
        
        $param['uom'] = $uom;
        
        //get the volume range for customer
        $custVolRate = $volModel->custVolumeRange(Invoice::ST, $freeRes);
     
        switch ($uom) {
            case Invoice::$carton:
            case Invoice::$cartonCurrent:
                $select = DB::raw('COUNT(cartons.ctn_id) AS qty');
                $shipResults = $this->getShippedCartonsData($param, $select);
                break;
            case Invoice::$volume:
            case Invoice::$volumeCurrent:
            case Invoice::$monthVolume:
            case Invoice::$volumeImperial:
            case Invoice::$volumeCurrentImperial:
            case Invoice::$monthVolumeImperial:
                $select =  DB::raw('SUM(cube) AS qty');
                $shipResults = $this->getShippedCartonsData($param, $select);
                break;
            case Invoice::$volumeMetric:
            case Invoice::$volumeCurrentMetric:
            case Invoice::$monthVolumeMetric:
                $select =  DB::raw('ROUND(SUM(cube) * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8) AS qty');
                $shipResults = $this->getShippedCartonsData($param, $select);
                break;
            case Invoice::$piece:
                $select = DB::raw('SUM(piece_qty) AS qty');
                $shipResults = $this->getShippedPiecesData($param, $select);
                break;
            case Invoice::$pallet:
            case Invoice::$palletCurrent:
            case Invoice::$monthPallet:
                $select = DB::raw('COUNT(plt_id) AS qty');
                $shipResults = $this->getZeroOutPalletsData($param, $select);
                break;
            case Invoice::$smallCarton:
            case Invoice::$mediumCarton:
            case Invoice::$largeCarton:
            case Invoice::$xlCarton:
            case Invoice::$xxlCarton:
            case Invoice::$smallCartonImperial:
            case Invoice::$mediumCartonImperial:
            case Invoice::$largeCartonImperial:
            case Invoice::$xlCartonImperial:
            case Invoice::$xxlCartonImperial:
                $param['volField'] = 'cube';
  
                $select = DB::raw('COUNT(cartons.ctn_id) AS qty');
                
                if (! getDefault($custVolRate[$uom]['min_vol'])) {
                    return [];
                }
                
                $shipResults = $this->getShippedRangeData($param, $select, $custVolRate);
                break;
            case Invoice::$smallCartonMetric:
            case Invoice::$mediumCartonMetric:
            case Invoice::$largeCartonMetric:
            case Invoice::$xlCartonMetric:
            case Invoice::$xxlCartonMetric:
                $param['volField'] = DB::raw('ROUND(cube * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8)');
                    
                $select = DB::raw('COUNT(cartons.ctn_id) AS qty');
                
                if (! getDefault($custVolRate[$uom]['min_vol'])) {
                    return [];
                }
                
                $shipResults = $this->getShippedRangeData($param, $select, $custVolRate);

                break;
            default:
                break;
        }
        
        
        foreach ($dateRanges as $date) {
            $val = getDefault($shipResults[$date]) ? $shipResults[$date] : 0;
            $storData[$date]['shipped'] = $val == 0 ? $val : '-' . $val;
        }
        
        return $storData;
    }
    
    /**
    * @stor array
    * @return array
    */
    
    public function calculateStor($stor, $free) 
    {
        $daysFree = $free['days'];

        $shipments = array_column($stor, 'shipped'); 

        $start = 0; 
        $prior = array_slice($shipments, $start, $daysFree); 

        $charges = []; 

        $keys = array_keys($stor);
        $invKeys = array_slice($keys, $daysFree); 

        $value = array_shift($stor); 

        while ($prior && count($stor) > $daysFree - 1) { 
            $prior = array_slice($shipments, $start, $daysFree); 

            $start++; 
            $shippmentSum = array_sum($prior); 
            $result = $value['summary'] + $shippmentSum; 

            $charges[] = $result > 0 ? $result : 0; 
            $value = array_shift($stor); 
        } 

        $storageData = $charges ? array_combine($invKeys, $charges) : [];

        return $storageData;
    }
    
    /**
    * @final array
    * @free array
    * @return array
    */
    
    public function getChargeQty($final, $free, $data) 
    {
        $totals = [];
       
        $cusID = $data['custID'];
        $uom = $free['uom'];
        
        $custHasUOM = getDefault($data['custUOMs'][$cusID][$uom]);

        if (! $custHasUOM) {
            return $totals;
        }

        $billedDates = array_flip($data['billedDates']);   
     
        $results = array_diff_key($final, $billedDates);

 
        //1 - commonUoms
        $selectCommon = array_intersect($this->commonUoms, [$uom]);
   
        if ($selectCommon) {
           $totals = array_sum($results);
        }
              

        //2 - currentUoms
        $selectCurrent = array_intersect($this->currentUoms, [$uom]);
      
        if ($selectCurrent) {
           $keys = $data['endDate'];
           $totals = $results[$keys];
        }
      

        //3- monthlyUoms
        $selectMonthly = array_intersect($this->monthlyUoms, [$uom]);
      
        if ($selectMonthly) {
           $qty = array_sum($results);
           $totals = $qty / self::$monthDays;
        }
        
        return $totals;
    }
    
    /**
    * @final array
    * @free array
    * @return array
    */
    
    public function getShippedCartonsData($param, $select) 
    {
        //query the shipped date and count of cartons/sum of cuFt value 
        $query = $this->carton  
                          ->select('cus_id', 'whs_id', 
                                    DB::raw('FROM_UNIXTIME(shipped_dt, "%Y-%m-%d") AS ship'),
                                    $select
                          )
                          ->where('cartons.ctn_sts', '=', config('constants.ctn_status.SHIPPED'))
                          ->whereNotNull('gr_dt')
                          ->where('cus_id', $param['custID'])
                          ->where('whs_id', $param['whsID'])
                          ->where(DB::raw('FROM_UNIXTIME(shipped_dt, "%Y-%m-%d")'), '>=', $param['priorStartDate'])
                          ->where(DB::raw('FROM_UNIXTIME(shipped_dt, "%Y-%m-%d")'), '<=', $param['endDate']);
        
        $shipResults = $query
                          ->groupBy('cus_id', 'whs_id', 'ship')
                          ->lists('qty', 'ship')
                          ->toArray();

        return $shipResults;
    }
    
    /**
    * @final array
    * @free array
    * @return array
    */
    
    public function getShippedPiecesData($param, $select) 
    {
        //query the shipped date with pieces shipped from odr_cartons table 
        $query = $this->ordCarton 
                          ->join('cartons AS c', 'c.ctn_id', '=', 'odr_cartons.ctn_id')  
                          ->select('cus_id', 'whs_id', 
                                    DB::raw('FROM_UNIXTIME(ship_dt, "%Y-%m-%d") AS ship'),
                                    $select
                          )
                          ->where('odr_cartons.ctn_sts', '=', config('constants.odr_status.SHIPPED'))
                          ->where('cus_id', $param['custID'])
                          ->where('whs_id', $param['whsID'])
                          ->where(DB::raw('FROM_UNIXTIME(ship_dt, "%Y-%m-%d")'), '>=', $param['priorStartDate'])
                          ->where(DB::raw('FROM_UNIXTIME(ship_dt, "%Y-%m-%d")'), '<=', $param['endDate']);
        
 
        $shipResults = $query
                          ->groupBy('cus_id', 'whs_id', 'ship')
                          ->lists('qty', 'ship')
                          ->toArray();

        return $shipResults;
    }
    
    
    /**
    * @param array
    * @select string
    * @return array
    */
    
    public function getZeroOutPalletsData($param, $select) 
    {
        //query the zero date and count of pallet value 
        $zeroResults = $this->plt
                            ->select('plt_id', 'cus_id', 'whs_id', 
                                      DB::raw('FROM_UNIXTIME(zero_date, "%Y-%m-%d") AS zero_date'),
                                      $select
                            )
                            ->where('cus_id', $param['custID'])
                            ->where('whs_id', $param['whsID'])
                            ->where(DB::raw('FROM_UNIXTIME(zero_date, "%Y-%m-%d")'), '>=', $param['priorStartDate'])
                            ->where(DB::raw('FROM_UNIXTIME(zero_date, "%Y-%m-%d")'), '<=', $param['endDate'])
                            ->whereNotNull('zero_date')
                            ->groupBy('cus_id', 'whs_id', 'zero_date')
                            ->lists('qty', 'zero_date')
                            ->toArray();

        return $zeroResults;
    }
    
    /**
    * @param array
    * @select string
    * @freeRes array
    * @return array
    */
    
    public function getShippedRangeData($param, $select, $custVolRate)
    {
        $volField = $param['volField'];
        
        $uom = $param['uom'];
        
        $query = $this->carton 
                      ->select('cus_id', 'whs_id', 
                                    DB::raw('FROM_UNIXTIME(shipped_dt, "%Y-%m-%d") AS ship'),
                                    $select
                          )
                          ->where('cartons.ctn_sts', '=', config('constants.ctn_status.SHIPPED'))
                          ->whereNotNull('gr_dt')
                          ->where('cus_id', $param['custID'])
                          ->where('whs_id', $param['whsID'])
                          ->where(DB::raw('FROM_UNIXTIME(shipped_dt, "%Y-%m-%d")'), '>=', $param['priorStartDate'])
                          ->where(DB::raw('FROM_UNIXTIME(shipped_dt, "%Y-%m-%d")'), '<=', $param['endDate']);
        
        $ordCtnQuery =  $query->where(function($q) use($custVolRate, $volField, $uom) {
                                $min = getDefault($custVolRate[$uom]['min_vol']);
                                $max = getDefault($custVolRate[$uom]['max_vol']);
                                $min ? $q->where($volField, '>', $custVolRate[$uom]['min_vol']) : NULL;
                                floatVal($max) ? $q->where($volField, '<=', $max) : NULL;
                        });

        $shipResults = $ordCtnQuery
                          ->groupBy('cus_id', 'whs_id', 'ship')
                          ->lists('qty', 'ship')
                          ->toArray();

        return $shipResults;
    }
    
     
    /**
    * @return array
    */
    
    public function getStorCommonUOMs() 
    {
        return [
            Invoice::$carton,
            Invoice::$piece,
            Invoice::$pallet,
            Invoice::$volume,
            Invoice::$volumeImperial,
            Invoice::$volumeMetric,
        ];
    }
    
    /**
    * @return array
    */
    
    public function getCurrentUOMs() 
    {
        return [
            Invoice::$cartonCurrent,
            Invoice::$volumeCurrent,
            Invoice::$volumeCurrentImperial,
            Invoice::$volumeCurrentMetric,
            Invoice::$palletCurrent
        ];
    }
    
    /**
    * @return array
    */
    
    public function getMonthlyUOMs() 
    {
        return  [
            Invoice::$monthVolume,
            Invoice::$monthVolumeImperial,
            Invoice::$monthVolumeMetric,
            Invoice::$monthPallet
        ];
    }
}