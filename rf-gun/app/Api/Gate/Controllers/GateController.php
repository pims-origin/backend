<?php

namespace App\Api\Gate\Controllers;

use App\Api\Gate\Models\GateDtlModel;
use App\Api\Gate\Models\GateModel;
use App\Api\Gate\Transformers\GateTransformer;
use App\Api\Gate\Validators\GateValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;


/**
 * Class GateController
 *
 * @package App\Api\Gate\Controllers
 */
class GateController extends AbstractController
{

    protected $userId;
    /**
     * @var GateModel
     */
    protected $gateModel;

    /**
     * @var GateDtlModel
     */
    protected $gateDtlModel;

    /**
     * GateController constructor.
     *
     * @param GateModel $gateModel
     * @param GateDtlModel $gateDtlModel
     */
    public function __construct(
        GateModel $gateModel,
        GateDtlModel $gateDtlModel
    ) {
        $this->gateModel = $gateModel;
        $this->gateDtlModel = $gateDtlModel;
        $this->userId = JWTUtil::getPayloadValue('jti') ? JWTUtil::getPayloadValue('jti') : 0;
    }

    /**
     * @return array|void
     */
    public function index()
    {
        $attributes = ['sort' => ['gate_id' => 'asc',]];

        try {
            $gates = $this->gateModel->search($attributes);

            $items = [];
            if ($gates) {
                foreach ($gates as $gate) {
                    $items[] = [
                        'gate_id'       => object_get($gate, 'gate_id', null),
                        'gate_name'     => object_get($gate, 'gate_name', null),
                        'gate_sts'      => object_get($gate, 'gate_sts', null),
                        'gate_sts_name' => Status::getByKey('GATE_STATUS', object_get($gate, 'gate_sts')),
                        'pos'           => object_get($gate, 'pos', null),
                        'line'          => object_get($gate, 'line', null),
                    ];
                }
            }

            return ['data' => $items];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param GateTransformer $gateTransformer
     *
     * @return array|void
     */
    public function search(
        Request $request,
        GateTransformer $gateTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {

            $input['sort']['line'] = 'ASC';
            $input['sort']['pos'] = 'ASC';
            $gates = $this->gateModel->search($input, [], array_get($input, 'limit', 99999999));

            $arrGate = $gates->toArray();
            $gateIds = array_pluck($arrGate['data'], 'gate_id', 'gate_id');

            //  Get Gate Detail By GateIds
            $gateDtls = $this->gateDtlModel->getGateDtlOpenByGateIds($gateIds);

            $arrGateDtl = [];
            if ($gateDtls) {
                foreach ($gateDtls as $gateDtl) {
                    $arrGateDtl[$gateDtl->gate_id] = [
                        'gate_dtl_id'       => object_get($gateDtl, 'gate_dtl_id', null),
                        'ctnr_id'           => object_get($gateDtl, 'ctnr_id', null),
                        'ctnr_num'          => object_get($gateDtl, 'ctnr_num', null),
                        'cus_id'            => object_get($gateDtl, 'cus_id', null),
                        'cus_name'          => object_get($gateDtl, 'cus_name', null),
                        'gate_dtl_sts'      => object_get($gateDtl, 'gate_dtl_sts', null),
                        'gate_dtl_sts_name' => object_get($gateDtl, 'gate_dtl_sts', null)
                            ? Status::getByKey('Gate_Dtl_Status', object_get($gateDtl, 'gate_dtl_sts', null))
                            : '',
                        'open'              => object_get($gateDtl, 'open', null)
                            ? date('Y-m-d H:i', strtotime(object_get($gateDtl, 'open', null))) : '',
                        'close'             => object_get($gateDtl, 'close', null)
                            ? date('Y-m-d H:i', strtotime(object_get($gateDtl, 'close', null))) : '',
                        'duration'          => object_get($gateDtl, 'duration', null)
                    ];
                }
            }


            $items = [];
            if ($gates) {
                foreach ($gates as $gate) {

                    $container = [];
                    if (object_get($gate, 'gate_sts', null) == "U") {
                        $container = $arrGateDtl[$gate->gate_id];
                    }

                    $items[$gate->line][$gate->pos] = [
                        'gate_id'       => object_get($gate, 'gate_id', null),
                        'gate_name'     => object_get($gate, 'gate_name', null),
                        'gate_sts'      => object_get($gate, 'gate_sts', null),
                        'gate_sts_name' => Status::getByKey('GATE_STATUS', object_get($gate, 'gate_sts')),
                        'pos'           => object_get($gate, 'pos', null),
                        'line'          => object_get($gate, 'line', null),
                        'container'     => $container
                    ];
                }

                foreach ($items as $key => $item) {
                    $items[$key] = array_values($item);
                }
            }

            return ['data' => $items];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $gateId
     * @param GateTransformer $gateTransformer
     *
     * @return mixed
     */
    public function show($gateId, GateTransformer $gateTransformer)
    {
        try {
            $gates = $this->gateModel->allBy("gate_id", $gateId);

            return $this->response->collection($gates, $gateTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param GateValidator $gateValidator
     *
     * @return mixed
     */
    public function store(
        Request $request,
        GateValidator $gateValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $gateValidator->validate($input);

        try {
            DB::beginTransaction();

            $gate = $this->gateModel->create($input);

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'gate_id' => $gate->gate_id
                ]
            ])->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $gateId
     * @param Request $request
     * @param GateValidator $gateValidator
     *
     * @return mixed
     */
    public function update(
        $gateId,
        Request $request,
        GateValidator $gateValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['gate_id'] = $gateId;
        $gateValidator->validate($input);

        $paramsUpdate = [
            'gate_name' => $input['gate_name'],
            'gate_sts'  => $input['gate_sts'],
            'pos'       => (int)$input['pos'],
            'line'      => (int)$input['line']
        ];

        try {
            DB::beginTransaction();

            // Update Gate Model
            $this->gateModel->refreshModel();
            $this->gateModel->updateWhere($paramsUpdate, ['gate_id' => $gateId]);

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'gate_id' => $gateId
                ]
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

}

