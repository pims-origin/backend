<?php

namespace App\Api\V2\Outbound\PalletShippingLane\Update\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Response;

abstract class AbstractController extends Controller
{
    use Helpers;

    private $response = [
                         'code'    => 200,
                         'status'  => false,
                         'message' => null,
                         'data'    => null,
                        ];


    public function formatArr($arr=[], $check=false)
    {
        if (empty($arr))
            return $arr;

        if (!is_array($arr) && is_object($arr))
            $arr = $arr->toArray();

        $data = [];
        foreach ($arr as $k => $v) {
            if ($check) {
                foreach ($v as $t => $h) {
                    if (is_numeric($t)) {
                        unset($v[$t]);
                    }
                }

                $data[] = $v;
            } else if (is_numeric($k)) unset($arr[$k]);
        }

        if ($check)
            return $data;

        return $arr;

    }//end formatArr()


    final protected function returnError($msg)
    {
        $data = [
                 'data'    => null,
                 'message' => $msg,
                 'status'  => false,
                ];

        return new Response($data, 200, [], null);

    }//end returnError()


    final protected function setStatus($status)
    {
        $this->response['status'] = $status;

    }//end setStatus()


    final protected function setData($data=[])
    {
        $this->response['data'] = $data;

    }//end setData()


    final protected function setMessage($message='')
    {
        $this->response['message'] = $message;

    }//end setMessage()


    final protected function setCode($code='')
    {
        $this->response['code'] = $code;

    }//end setCode()


    final protected function getStatus()
    {
        return $this->response['status'];

    }//end getStatus()


    final protected function getData()
    {
        return $this->response['data'];

    }//end getData()


    final protected function getMessage()
    {
        return $this->response['message'];

    }//end getMessage()


    final protected function getCode()
    {
        return $this->response['code'];

    }//end getCode()


    final protected function getResponseData()
    {
        return $this->response;

    }//end getResponseData()


}//end class
