<?php
/**
 * Created by PhpStorm.
 * User: admn
 * Date: 3/15/2017
 * Time: 3:32 PM
 */

namespace App\Jobs;


use App\Api\V1\Models\BaseInvoiceModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\WavePickHdrModel;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\HeaderBag;
use Tymon\JWTAuth\Facades\JWTAuth;

class WavePickJob extends Job
{
    protected $wvId;
    protected $request;

    public function __construct($wvId, $request)
    {
        $this->wvId = $wvId;
        $this->request = $request;
    }

    public function handle(){
        $this->loadTokenInQueue($this->request);
        $data = DB::table('wv_queue')
            ->where('queue_sts', 'NW')
            ->where('wv_id', $this->wvId)
            ->where('type', 'UP')
            ->get();

        try {
            DB::beginTransaction();
            foreach($data as $rw) {
                if ($rw['data']) {
                    $orderCartons = unserialize($rw['data']);
                    $insertBatchs = array_chunk($orderCartons, 2000);
                    foreach ($insertBatchs as $batch) {
                        DB::table('odr_cartons')->insert($batch);
                    }
                    DB::table('wv_queue')->where('wv_id', $rw['wv_id'])
                        ->delete();

                    (new OrderHdrModel())->updateOrderPickedByWv($rw['wv_id']);
                    (new WavePickHdrModel())->updateWvComplete($rw['wv_id']);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            DB::table('sys_bugs')->where('id', 8 )->update([
                'error' => $e->getMessage(),
                'api_name' => 'KITTING'
            ]);
            //  return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function loadTokenInQueue($request) {
        $currentRequest = app('Illuminate\Http\Request');
        $currentRequest->headers = new HeaderBag($request->getHeaders());

        $authorization = $request->getHeader('Authorization');
        $token = str_replace('Bearer ', '', $authorization[0]);
        JWTAuth::setToken($token);
        BaseInvoiceModel::boot();
    }
}