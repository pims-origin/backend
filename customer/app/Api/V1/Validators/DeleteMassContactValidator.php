<?php

namespace App\Api\V1\Validators;

class DeleteMassContactValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_ctt_id'   => 'required',
            'cus_ctt_id.*' => 'required|exists:cus_contact,cus_ctt_id'
        ];
    }
}