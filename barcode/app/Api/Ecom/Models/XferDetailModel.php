<?php

namespace App\Api\Ecom\Models;

use Seldat\Wms2\Models\XferDtl;

class XferDetailModel extends AbstractModel
{
    public function __construct(XferDtl $model = null)
    {
        $this->model = ($model) ?: new XferDtl();
    }
}
