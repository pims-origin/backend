<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\TransactionModel;
use App\Api\V1\Transformers\TransactionTransformer;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use App\Api\V1\Validators\CustomerSacValidator as Validator;
use Illuminate\Support\Facades\DB;

class TransactionController extends AbstractController
{
    protected $transactionModel;

    protected $transformer;

    /**
     * Init.
     *
     */
    public function __construct()
    {
        $this->transactionModel = new TransactionModel();
        $this->transformer = new TransactionTransformer();
    }

    /**
     * @SWG\Get(
     *     path="/transaction/search",
     *     tags={"Invoice Transaction"},
     *     summary="Search Invoice Transaction",
     *     description="Search Invoice Transaction",
     *     operationId="getInvoiceTransaction",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="inv_id",
     *         in="query",
     *         description="ID of Invoice",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Invoice",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */    
    /**
     * Display a listing of the resource.
     * @param IRequest $request
     * @return mixed
     */
    public function search(IRequest $request)
    {
        $input = $request->getQueryParams();

        try {
            // get list transacion
            $trans = $this->transactionModel->search($input, [], array_get($input, 'limit'));

            return $this->response->paginator($trans, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/transaction/",
     *     tags={"Invoice Transaction"},
     *     summary="Add Invoice Transaction",
     *     description="Add Invoice Transaction",
     *     operationId="addInvoiceTransaction",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }} 
     * )
     */
    /**
     * Store a newly created data in customer sac.
     *
     * @param  IRequest $request
     * @return Response
     */
    public function add(IRequest $request)
    {
        $input = $request->getParsedBody();
        try {
            // Store transaction(s)
            $add = $this->transactionModel->add($input);
            if(!$add) {
                return $this->response->errorBadRequest('Over paid, please re-input the amount!');
            }

            return $this->response->created(null, '{}');
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function method()
    {
        return [ 
            'list' => ['Cash', 'Bank Transfer', 'POS', 'Cheque']
        ];
    }
}
