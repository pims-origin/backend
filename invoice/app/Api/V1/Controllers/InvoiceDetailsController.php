<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\InvoiceDetailsModel;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class CountryController
 *
 * @package App\Api\V1\Controllers
 */
class InvoiceDetailsController extends AbstractController
{

    protected $invoiceDetailsModel;

    /*
    ****************************************************************************
    */

    public function __construct(IRequest $request)
    {
//        $object = new AuthenticationService($request);
//
//        parent::__construct($request, $object);

        $this->invoiceDetailsModel = new InvoiceDetailsModel();
    }

    /*
    ****************************************************************************
    */

    public function getDetails(Request $request)
    {
        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', PAGING_LIMIT);

        $model = $this->invoiceDetailsModel;

        try {

            return $model->search($input, [], $limit);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }
}



