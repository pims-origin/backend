<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickHdr;
use Wms2\UserInfo\Data;

class WaveHdrModel extends AbstractModel
{

    const DAS_LIMIT = 2;
    /**
     * WaveHdrModel constructor.
     *
     * @param WavepickHdr|null $model
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }


    public function getWaveDashBoard($input, $with = [])
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();

        $userId =  array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $query = $this->make($with);

        $query->where('whs_id', $currentWH)
              ->where('picker', $userId);

       $limit = (!empty($input['day']) && is_numeric($input['day'])) ? $input['day'] : self::DAS_LIMIT;
        $query->where('created_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));
        return $query->get();

    }

    /**
     * @param $input
     * @param array $with
     *
     * @return mixed
     */
    public function wavePickStatisticDashboard($input, $with = [], $limit = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis::getCurrentCusIds();

        $query = $this->make($with);
        $query->select([
            'wv_hdr.wv_id',
            'wv_hdr.wv_num',
            'wv_hdr.wv_sts',
            DB::raw("(sum(wv_dtl.piece_qty)) AS total"),
            DB::raw("(sum(wv_dtl.act_piece_qty)) AS picked"),
            DB::raw("((sum(wv_dtl.act_piece_qty)*100)/sum(wv_dtl.piece_qty)) AS percent_picked"),
            'wv_hdr.created_at',
        ]);
        $query->join('wv_dtl', 'wv_dtl.wv_id', '=', 'wv_hdr.wv_id');
        $query->leftJoin('users AS u2', 'u2.user_id', '=', 'wv_hdr.picker')
            ->addSelect(
                DB::RAW("CONCAT(u2.first_name,' ',u2.last_name) as picker")
            );
        $query->where('wv_hdr.whs_id', $currentWH);
        $query->whereIn('wv_dtl.cus_id', $cus_ids);
        $query->whereIn('wv_hdr.wv_sts', ['NW', 'PK']);
        $query->where('wv_hdr.picker', '>', 0);
        $query->orderBy('wv_hdr.created_at', 'desc');
        $query->groupBy('wv_hdr.wv_id');

        //$limit = (!empty($input['day']) && is_numeric($input['day'])) ? $input['day'] : self::DAS_LIMIT;
        //$query->where('created_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));

        $models = $query->paginate($limit);

        return $models;

    }
}
