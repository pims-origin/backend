<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V4\Outbound\Order\AssignCartonsToOrder21\Models;

use Seldat\Wms2\Models\WaveDtlLoc;

class WaveDtlLocModel extends AbstractModel
{

    /**
     * WaveDtlLocModel constructor.
     *
     * @param WaveDtlLoc|null $model
     */
    public function __construct(WaveDtlLoc $model = null)
    {
        $this->model = ($model) ?: new WaveDtlLoc();
    }


}
