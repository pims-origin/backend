<?php

namespace App\Api\V2\Inbound\Carton\UnSetDamageCarton\Controllers;

use App\Api\V2\Inbound\Carton\UnSetDamageCarton\Models\VirtualCartonModel;
use App\Api\V2\Inbound\Carton\UnSetDamageCarton\Models\CartonModel;
use App\Api\V2\Inbound\Carton\UnSetDamageCarton\Models\GoodsReceiptDetailModel;
use App\Api\V2\Inbound\Carton\UnSetDamageCarton\Models\PalletModel;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Models\CustomerWarehouse;
use App\Api\V1\Models\Log;

class VirtualCartonController extends AbstractController
{

    private $vtlCtn;
    private $_errorMsgs;
    private $_wrongCtnRfids;
    private $_CtnRfidsScannedPallet;
    private $carton;
    private $grDtlModel;
    private $pltModel;

    /**
     * VirtualCartonController constructor.
     *
     * @param VirtualCartonModel $vtlCtn
     */
    public function __construct(VirtualCartonModel $vtlCtn, CartonModel $carton, GoodsReceiptDetailModel $grDtl, PalletModel $plt)
    {
        $this->vtlCtn = $vtlCtn;
        $this->_errorMsgs = [];
        $this->_wrongCtnRfids = [];
        $this->_CtnRfidsScannedPallet = [];
        $this->carton = $carton;
        $this->grDtlModel = $grDtl;
        $this->pltModel = $plt;
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function unSetDamageCarton($whsId, $cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $owner = $transaction = "";
        $urlEndpoint = "/v2/whs/{$whsId}/cus/{$cusId}/carton/unset-damage-carton";
        $apiName = 'Set damage Carton';
        Log:: info($request, $whsId, [
            'evt_code'     => 'UPD',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $urlEndpoint,
            'message'      => $apiName
        ]);

        $params = [
            'whsId'    => $whsId,
            'cusId'    => $cusId,
            'ctn_rfids' => array_get($input, 'ctn_rfids', null),
        ];

        $ctnRfids = $params['ctn_rfids'];

        foreach ($ctnRfids as $ctnRfid) {
            //validate carton RFID
            $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
            if (!$ctnRFIDValid->validate()) {
                $this->_setErrorMsg($ctnRFIDValid->error);
                $this->_wrongCtnRfids[] = $ctnRfid;
                continue;
            }

            //WMS2-3964
            $errors = $this->checkCusBelongWhs($whsId, $cusId);
            if ($errors) {
                $this->_setErrorMsg($errors);
                $this->_wrongCtnRfids[] = $ctnRfid;
                continue;
            }

            $isBelong = $this->vtlCtn->getFirstWhere([
                'cus_id'   => $cusId,
                'whs_id'   => $whsId,
                'ctn_rfid' => $ctnRfid
            ]);

            if (!$isBelong) {
                $msg = "Virtual carton doesn't exist";
                $isExist = $this->vtlCtn->getFirstWhere(['ctn_rfid' => $ctnRfid]);
                if ($isExist) {
                    $msg = "Virtual carton doesn't belong to warehouse and customer";
                }
                $this->_setErrorMsg($msg);
                $this->_wrongCtnRfids[] = $ctnRfid;
                continue;
            }

            //check this vtl ctn can delete
            $vtlCtnSts = $this->vtlCtn->getVtlCtnStatus($ctnRfid);

            if (empty($vtlCtnSts) || count($vtlCtnSts) == 0 || is_null($vtlCtnSts)) {
                $msg = sprintf("Can not find carton has RFID: %s .", $ctnRfid);
                $this->_setErrorMsg($msg);
                $this->_wrongCtnRfids[] = $ctnRfid;
                continue;
            }

            $vtlCtnSts = object_get($vtlCtnSts, 'vtl_ctn_sts', null);
            if ($vtlCtnSts == 'GC') {
//                $msg = "Can't set damage cartons which have been assigned to pallet!";
//                $this->_setErrorMsg($msg);
//                $this->_wrongCtnRfids[] = $ctnRfid;
//                continue;
                $this->_CtnRfidsScannedPallet[] = $ctnRfid;
                continue;
            }
        }


        try {
            if (count($this->getErrorMsg())){
                $numberWrongCtn = count(array_unique($this->_wrongCtnRfids));
                $formatMsg = sprintf("are %s cartons", $numberWrongCtn);
                if ($numberWrongCtn == 1) {
                    $formatMsg = sprintf("is %s carton", $numberWrongCtn);
                }
                $msg = sprintf("Failed!. There %s cannot be unset damaged. ".PHP_EOL."[{%s}].",
                    $formatMsg,
                    implode("}; ".PHP_EOL."{", $this->getErrorMsg()));
                return $this->_responseErrorMessage($msg);
            }
            DB::Begintransaction();


            $this->vtlCtn->getModel()
                ->whereIn('ctn_rfid', $ctnRfids)
                ->update([
                    'is_damaged' => 0,
                    'dmg_id'     => 0,
                ]);

            $grHdrIds = [];
            $grDtlIds = [];
            $pltIds = [];
            foreach ($this->_CtnRfidsScannedPallet as $ctnRfidsScannedPallet) {
                $carton = $this->carton->getFirstWhere(['rfid' => $ctnRfidsScannedPallet]);
                if($carton){
                    $grHdrIds[] = $carton->gr_hdr_id;
                    $grDtlIds[] = $carton->gr_dtl_id;
                    $pltIds[] = $carton->plt_id;
                    // Get current user id
                    $userId = Data::getCurrentUserId();
                    // Update damage_carton
                    DB::table('damage_carton')->where([
                        'ctn_id'     => $carton->ctn_id,
                        'dmg_note'   => '',
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ])->forceDelete();

                    // Update cartons
                    $this->carton->updateWhere(
                        [
                            'is_damaged' => '0'
                        ],
                        [
                            'rfid'   => $ctnRfidsScannedPallet,
                            'ctn_id' => $carton->ctn_id
                        ]
                    );
                }
            }
            $this->grDtlModel->updateGrDtlWhenScanPallet($grDtlIds);
            /**
             * Update pallet: carton total, damage
             */
            foreach ($pltIds as $pltId){
                $this->pltModel->updatePalletCtnTtlAndDamaged($whsId, $grHdrIds, $pltId, 'null', [], []);
            }

            DB::Commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => sprintf("Unset damage carton has RFID %s successfully!", implode(', ', $ctnRfids))
            ]];
            $msg['data'] = [$ctnRfids];

            return $msg;

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $urlEndpoint,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return $this->_responseErrorMessage($this->getResponseData());
        }
    }

    /**
     * Check customer is belong to warehouse
     *
     * @param integer $whsId
     * @param integer $cusId
     *
     * @throws \Exception
     */
    private function checkCusBelongWhs($whsId, $cusId)
    {
        $cusWhsModel = (new CustomerWarehouse())->where([
            'whs_id' => $whsId,
            'cus_id' => $cusId
        ])->first();
        if (!$cusWhsModel) {
            //throw new \Exception('Customer is not belong to warehouse');
            $msg = sprintf('Customer is not belong to warehouse.');
            return $msg;
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _setErrorMsg($msg)
    {
        $this->_errorMsgs[] = $msg;
    }

    public function getErrorMsg()
    {
        return $this->_errorMsgs;
    }
}
