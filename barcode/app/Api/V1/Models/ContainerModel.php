<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/24/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class ContainerModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     * @param Container $model
     */
    public function __construct(Container $model = null)
    {
        $this->model = ($model) ?: new Container();
    }

    /**
     * @param int $containerId
     * @return int
     */
    public function deleteContainer($containerId)
    {
        return $this->model
            ->where('ctnr_id', $containerId)
            ->delete();
    }


    public function generateContainerNum($prefix = 'CONT')
    {
        $currentYearMonth = date('ym');
        $container = Container::where('ctnr_num', 'LIKE', '%-' . $currentYearMonth . '-%')
            ->orderBy('ctnr_id', 'DESC')
            ->first();

        if (!$container){
            return $prefix . '-' . $currentYearMonth . '-' . '00001';
        }

        $aNum = explode("-", $container['ctnr_num']);
        $aTemp = array_keys($aNum);
        $end = end($aTemp);
        $index = (int)$aNum[$end];
        $aNum[0] = "CONT";
        $aNum[$end] = str_pad(++$index, 5, "0", STR_PAD_LEFT);
       return implode("-", $aNum);
    }

}