<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Transformers\OrderListShippingTransformer;
use App\Api\V1\Transformers\OrderListTransformer;
use App\Api\V1\Transformers\OrderDetailTransformer;
use App\Api\OUTBOUND\Transformers\OrderByOdrIDTransformer;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;

class OrderController extends AbstractController
{

    protected $orderCartonModel;
    protected $orderDtlModel;
    protected $orderHdrModel;
    protected $cartonModel;

    public function __construct()
    {
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->cartonModel = new CartonModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     * @return Response|void
     */
    public function getListOrder($whsId, Request $request, OrderListTransformer $orderListTransformer) {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $loc = $this->orderHdrModel->getListOrder($whsId, array_get($input, 'limit', 20));
            return $this->response->paginator($loc, $orderListTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getOrderDetail($whsId, $ordDtl) {
        try {
            $dt = $this->orderDtlModel->getFirstBy('odr_dtl_id', $ordDtl);
            if(empty($dt)){
                $msg = sprintf("There is no orderDtl %s .", $ordDtl);
                $data = [
                    'data' => null,
                    'message' => $msg,
                    'status' => false,
                ];
                return new Response($data, 200, [], null);
            }

            return $this->response->item($dt, new OrderDetailTransformer());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getListOrderPacked($whsId) {
        try {
            $dt = $this->orderHdrModel->getListOrderPacked($whsId);

            return $this->response->collection($dt, new OrderListShippingTransformer());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getCartonRfid($whsId, $rfid) {
        try {
            $data = [];
            $dt = $this->orderCartonModel->getFirstWhere([
                'ctn_rfid' => $rfid,
                'ctn_sts' => Status::getByValue('PICKING', 'ODR_CTN_STATUS')
            ]);
            if(empty($dt)){
                $msg = sprintf("There is no carton %s.", $rfid);
                $data = [
                    'data' => null,
                    'message' => $msg,
                    'status' => false,
                ];
                return new Response($data, 200, [], null);
            }
            $dt->piece_picking = $dt->piece_qty;
            $dt->piece_qty = $dt->piece_qty . '/0';
            $item = $this->cartonModel->getFirstBy('ctn_id', $dt->ctn_id);

            if(!empty($item)){
                $dt->item_id = $item->item_id;
            }
            $data['data'] = $this->formatArr($dt);

            return new Response($data, 200, [], null);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     *  @param $odrId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function putCartonOrder($whsId, $odrId, Request $request)
    {
        $input = $request->getParsedBody();
        $ctns = array_get($input, 'ctns-rfid', null);

        $checkOdr = $this->orderHdrModel->getFirstWhere([
                'whs_id' => $whsId,
                'odr_id' => $odrId
        ]);

        if (empty($checkOdr)) {
            $msg = sprintf("The order %s doesn't exist", $odrId);
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        if(empty($ctns)){
            $msg = sprintf("No carton assigned to order %s", object_get($checkOdr, 'odr_num'));
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        // check ctns exists on odr ctn table
        $str = '';
        foreach($ctns as $ctn){
            $checkCtn = $this->orderCartonModel->getFirstWhere([
                    'ctn_rfid' => $ctn,
                    'odr_hdr_id' => NULL
            ]);

            if (empty($checkCtn))
                $str = $str . ', ' . $ctn;
        }

        $str = trim(substr($str, 1));
        if (!empty($str)) {
            $msg = sprintf("The cartons %s are assigned to order.", $str);
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        try {
            // start transaction
            DB::beginTransaction();

            // update carton
            foreach($ctns as $ctn) {

                $ctnDt = $this->cartonModel->getFirstWhere([
                    'rfid' => $ctn
                ]);
                if (empty($ctnDt)) {
                    $msg = sprintf("The carton %s doesn't exist", $ctn);
                    $data = [
                        'data' => null,
                        'message' => $msg,
                        'status' => false,
                    ];
                    return new Response($data, 200, [], null);
                }

                $odrDtl = $this->orderDtlModel->getFirstWhere([
                    'odr_id' => $odrId,
                    'item_id' => $ctnDt->item_id
                ]);
                if (empty($odrDtl)) {
                    $msg = sprintf("The order carton %s doesn't exist", $ctn);
                    $data = [
                        'data' => null,
                        'message' => $msg,
                        'status' => false,
                    ];
                    return new Response($data, 200, [], null);
                }

                $dataOdr = [
                    'odr_id' => $odrId,
                    'odr_num' => $checkOdr->odr_num,
                    'odr_dtl_id' => $odrDtl->odr_dtl_id,
                    'ctn_sts' => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                    'updated_at' => time()
                ];

                $this->orderCartonModel->updateWhere($dataOdr, ['ctn_rfid' => $ctn]);

            }

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['message'] = sprintf("Order carton updated successfully!");

            return $this->response->noContent()
                ->setContent(['data' => $msg])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    // get order by id

    public function getOrderByID($whsId, $ordID) {
        try {
            $odr = $this->orderHdrModel->getFirstBy('odr_id', $ordID);
            if(empty($odr)){
                $msg = sprintf("There is no order ID %s .", $ordID);
                $data = [
                    'data' => null,
                    'message' => $msg,
                    'status' => false,
                ];
                return new Response($data, 200, [], null);
            }

            return $this->response->item($odr, new OrderByOdrIDTransformer());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}
