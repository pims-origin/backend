<?php

namespace App\Api\V1\Models;

use App\Utils\Helpers;
use Illuminate\Support\Facades\DB;
use App\Invoice;

use App\Api\V1\Models\InvoiceHeaderModel;
use App\Api\V1\Models\InvoiceDetailsModel;
use App\Api\V1\Models\ClientMasterModel;
use App\Api\V1\Models\CommonInvoiceModel;
use App\Api\V1\Models\InvoiceListModel;
use App\Api\V1\Models\OrderModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Models\GoodsReceiptModel;

use Seldat\Wms2\Models\InvoicingSummary;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\InvoiceHeader;
use Seldat\Wms2\Models\InvoiceDetail;

use Seldat\Wms2\Models\InvoiceReceivingHistory;
use Seldat\Wms2\Models\InvoiceOrdProcHistory;
use Seldat\Wms2\Models\InvoiceHistoryMonth;
use Seldat\Wms2\Models\Labor;
use Seldat\Wms2\Models\InvEmailed;

use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\JWTUtil;


class CreateInvoicesModel extends AbstractModel
{
    /**
    * @var
    */
    protected $invHeader;
    protected $clientModel;
    protected $invDetail;
    protected $commonModel;
    protected $orderModel;
    protected $invoiceSum;
    protected $invoiceListModel;
    protected $orderHdr;
    protected $header;
    protected $detail;
    protected $invHisRcv;
    protected $invHisOrdProc;
    protected $invHisMonth;
    protected $labor;
    protected $InvEmailed;
    protected $container;
    protected $grHdr;

    public $pdfPortraitPageWidth = 195;

    public $pdfPortraitPageLength = 60;

    public $pdfLeftMargin = 10;

    public $pdfLogoWidth = 100;

    public $pdfSeldatAddressWidth = 60;

    public $pdfRowHeight = 4.5;

    public $pdfFontSize = 9;

    public $pdfPageNo = 1;

    public $pdfFooterWidth = 35;

    public $pdfInvoiceFooterHeight = 4;

    public $pdfInvoiceNumberWidth = 10;

    public $currencyCode = 'USD';

    public $pdfSeldatAddress = [
        'Seldat Distribution Inc.',
        '1900 River Road',
        'Burlington, NJ 08016',
    ];

    public $recItems = [];

    public $ordItems = [];

    public $rcvUOM = [];

    public $containerName = NULL;

    public $orderNumber = [];

    public $cancelNums = [];

    public $storRange = NULL;

    public $pdfContactAddress = [
        'Renee Williams (732) 348-0000',
        'renee.williams@seldatinc.com',
    ];

    public $companyName = 'Seldat Distribution Inc.';

    public $pdContactNumber = [
        'Seldat Distribution, Inc',
        '107 Corporate Blvd',
        'South PlainField, NJ',
        '07080',
        'Phone: [732-348-0000]',
        'Fax:   renee.williams@seldatinc.com',
    ];

    public $pdfInvoiceWidth = 120;

    public $pdfExtraLength = 17;




    /**
    *  Constructor
    */

    public function __construct()
    {
        $this->invHeader = new InvoiceHeaderModel();
        $this->invDetail = new InvoiceDetailsModel();
        $this->clientModel = new ClientMasterModel();
        $this->commonModel = new CommonInvoiceModel();
        $this->invoiceListModel = new InvoiceListModel();
        $this->orderModel = new OrderModel();
        $this->container = new ContainerModel();
        $this->grHdr = new GoodsReceiptModel();

        $this->invoiceSum =  new InvoicingSummary();
        $this->orderHdr = new OrderHdr();
        $this->header = new InvoiceHeader();
        $this->detail = new InvoiceDetail();

        $this->invHisRcv = new InvoiceReceivingHistory();
        $this->invHisOrdProc = new InvoiceOrdProcHistory();
        $this->invHisMonth = new InvoiceHistoryMonth();

        $this->labor =  new Labor();
        $this->InvEmailed = new InvEmailed();
    }


    /**
    * @post array
    * @return array
    */

    function updateInvoiceProcessing($post)
    {
        $chargeCodes = $uoms = $recMonths = $storDays =
                $containerDates = $orderDates = [];

        $invoiceNo = $post['invoiceNo'];
        $vendorID = $post['cus_id'];
        $warehouseID = $post['whs_id'];
        $details = $post['details']['data'][$vendorID];
        $startDate = $post['dateRange']['startDate'];
        $endDate = $post['dateRange']['endDate'];

        //get open items
        $openItems = [];
        if (getDefault($post['openItems'])) {
            foreach ($post['openItems'] as $val) {
                $cat = $val['cat'];
                $id = $val['id'];

                $openItems[$cat][$id] = 'on';
            }
        }

        //selectItems or openItems
        $items = $post['items'] ? $post['items'] : $openItems;

        $invoiceExists = $this->invDetail->checkIfExists($invoiceNo);

        if ($invoiceExists) {

            $this->printInvoice([
                'inv_num' => $invoiceNo,
                'cus_id' => $vendorID,
                'startDate' => $startDate,
                'endDate' => $endDate
            ]);

            return FALSE;
        }

        $billTo = $post['billTo'];

        $status = $this->invHeader->getStatusByNumber($invoiceNo);

        // "paid" or "canceled" Invoice can not be invoiced
        if (in_array($status, ['p', 'c'])) {
            return FALSE;
        }

        $amount = 0;

        //get userID
        $userID = JWTUtil::getPayloadValue('jti') ?: 1;


        foreach ($details as $values) {

            $chargeCode = $values['chg_cd_name'];

            $chargeCodes[$chargeCode] = TRUE;

            $uom = $values['sys_uom_name'];

            $uoms[$uom] = TRUE;
        }

        $uomKeys = array_keys($uoms);

        $chargeKeys = array_keys($chargeCodes);

        $chargeIDs = $this->commonModel->getChargIDsByCodes($chargeKeys);


        $orders = getDefault($post['invItemsIDs']['Order Processing']) ?
                    getDefault($post['invItemsIDs']['Order Processing']) :
                         getDefault($post['invItemsIDs']['orderPrc']);

        $billableOrders = [];

        if ($orders) {
            $results = $this->orderHdr
                            ->select('odr_id', 'odr_num')
                            ->whereIn('odr_id', $orders)
                            ->lists('odr_num', 'odr_id')
                            ->toArray();

            $billableOrders = ! $results ? [] :
                $this->orderModel->billableOrders([
                    'orderNums' => $results
                ]);
        }

        //get order shipped dates
        $ordNums = array_values($billableOrders);
        $orderResults = $this->orderModel->getOrderDetails($ordNums);

        $orderRes = $orderResults
                            ->get()
                            ->toArray();
        
        $actCmplDt = array_filter(array_column($orderRes, 'act_cmpl_dt'));
        $actCnclDt = array_filter(array_column($orderRes, 'act_cancel_dt'));
        $orderDates = array_unique(array_merge($actCmplDt, $actCnclDt));

        $recNums = getDefault($post['invItemsIDs']['Receiving']);

        $containerInfo = $recNums ? $this->getContainerInfo($recNums) : [];

        //get container received dates
        $containerDates = array_column($containerInfo, 'created_at');
        $uniqueCtnrDates = array_unique($containerDates);

        //get the receiving labor type = "e"
        $recLabor = $recNums ?
                $this->getRcvLaborHrs($recNums, $post['dateRange']) : [];

        //get the order labor type = "e"
        $ordLabor = $orders ?
                $this->getOrdLaborHrs($orders, $post['dateRange']) : [];


        //get the wprk order labor type = "e"
        $woLabor = $orders ?
                $this->getWoLaborHrs($orders, $post['dateRange']) : [];

       //get billabeDates for receiving charge code "Month" UOM
       if (getDefault($items['Receiving']) &&
                                    in_array(Invoice::$month, $uomKeys)) {
            $recMonths = $this->commonModel->getBillableDts([
                'cat' => 'r',
                'period' => 'monthly',
                'dates' => $post['dateRange'],
                'custs' => [$vendorID],
                'whs_id' => $post['whs_id'],
                'details' => TRUE,
             ]);
        }

        //get billabeDates for storage charge code
        if (getDefault($items['Storage'])) {
            $storDays = $this->commonModel->getBillableDts([
                'cat' => 's',
                'period' => 'daily',
                'dates' => $post['dateRange'],
                'custs' => [$vendorID],
                'whs_id' => $post['whs_id'],
                'details' => TRUE,
            ]);
        }

        //get invoice ID
        $invoiceID = $this->invHeader->getByInvoiceNumber($invoiceNo);

        $summaryParams = [
            'cus_id' => $vendorID,
            'whs_id' => $warehouseID,
            'chargeIDs' => $chargeIDs,
            'invoiceID' => $invoiceID,
            'invoiceNo' => $invoiceNo,
            'startDate' =>  $post['dateRange']['startDate'],
            'endDate' => $post['dateRange']['endDate'],
            'userID' => $userID,
            'details' => $details,
            'storage' => $storDays,
            'order' => $orderDates,
            'receiving' => $uniqueCtnrDates
        ];


        DB::beginTransaction();

        //invoice details
        foreach ($details as $values) {

            $chargeCode = $values['chg_cd_name'];

            $price = $values['rate'] ? $values['rate'] : 1;

            $quantity = $values['quantity'];

            if (is_string($quantity)) {
                $quantity = str_replace(",", "", $quantity);
            }

            $detailsParams = [
                'whs_id' => $warehouseID,
                'cus_id' => $vendorID,
                'inv_num' =>  $invoiceNo,
                'inv_amt' => $amount,
                'chg_code_id' => $values['chg_cd_id'],
                'chg_cd_desc' => getDefault($values['chg_cd_desc']),
                'chg_cd_qty' => $quantity,
                'chg_uom_id' => $values['chg_uom_id'],
                'chg_cd_price' => $price,
                'chg_cd_amt' => $values['ccTotal'],
                'create_by' => $userID,
                'update_by' => $userID
            ];

            $this->detail->create($detailsParams);

            $amount += $values['ccTotal'];
        }

        //invoice header
        $this->header
             ->where('inv_num', $invoiceNo)
             ->update([
                    'whs_id' => $warehouseID,
                    'cus_id' => $vendorID,
                    'inv_dt' =>  $post['header']['invDt'],
                    'inv_amt' => $amount,
                    'inv_sts' => 'i',
                    'cust_ref' => $post['header']['custRef'],
                    'net_terms' => $post['header']['terms'],
                    'bill_to_add1' => trim(
                            trim($billTo['bill_to_add_line_1']) . ' ' .
                            trim($billTo['bill_to_add_line_2'])
                    ),
                    'bill_to_city' => $billTo['bill_to_city'],
                    'bill_to_state' => $billTo['bill_to_state'],
                    'bill_to_cnty' =>  $billTo['bill_to_country'],
                    'bill_to_zip' => $billTo['bill_to_zip'],
                    'sts' => 'u',
                    'update_by' => $userID
            ]);


        //inv_his_rcv
        $this->updateHistory([
            'invID' => $invoiceID,
            'invNum' => $invoiceNo,
            'target' => $post['invItemsIDs'],
            'cust_id' => $vendorID,
            'whs_id' => $warehouseID,
            'containerValues' => $containerInfo,
            'table' => $this->invHisRcv
        ]);


        //inv_his_ord_proc
        $this->updateOrdProcHistory([
            'values' => $billableOrders,
            'cust_id' => $vendorID,
            'whs_id' => $warehouseID,
            'invID' => $invoiceID,
            'invNum' => $invoiceNo,
            'table' => $this->invHisOrdProc
        ]);


        //inv_his_month
        $this->updateMonthHistory([
            'cust_id' => $vendorID,
            'whs_id' => $warehouseID,
            'invID' => $invoiceID,
            'invNum' => $invoiceNo,
            'cats' => [
                'r' => $recMonths,
                's' =>  $storDays,
            ],
            'table' => $this->invHisMonth
        ]);


        //insert into invoice_summary" table
        $this->storeInvoiceSummary($summaryParams);


        //insert into inv_emailed table
        $fileName = 'Invoice_' . $invoiceNo . '.pdf';

        $emailParams = [
            'cus_id' => $vendorID,
            'inv_num' => $invoiceNo,
            'inv_dt' => $post['header']['invDt'],
            'file_url' => env('BUCKET_INVOICE') . $fileName,
        ];

        $this->invoiceEmailed($emailParams);


        //update receiving labor
        $this->updateRcvLaborHistory([
            'invID' => $invoiceID,
            'invNum' => $invoiceNo,
            'recLabor' => $recLabor,
            'table' => $this->labor
        ]);


        //update order labor
        $this->updateOrdLaborHistory([
            'invID' => $invoiceID,
            'invNum' => $invoiceNo,
            'ordLabor' => $ordLabor,
            'table' => $this->labor
        ]);


        //update work order labor
        $this->updateWoLaborHistory([
            'invID' => $invoiceID,
            'invNum' => $invoiceNo,
            'woLabor' => $woLabor,
            'table' => $this->labor
        ]);

        DB::commit();

        //print invoice
        $this->printInvoice([
            'inv_num' => $invoiceNo,
            'cus_id' => $vendorID,
            'startDate' => $startDate,
            'endDate' => $endDate
        ]);

        return TRUE;
    }

    /*
    ****************************************************************************
    */

    public function storeInvoiceSummary($data)
    {
        $results = [];

        if (! $data['storage'] && ! $data['receiving'] && ! $data['order']) {
            return;
        }

        $chgCodeIDs = array_column($data['chargeIDs'], 'chg_code_id');

        $cusID = $data['cus_id'];


        //Receiving charge code
        if ($data['receiving']) {

            $rcvResults =  $this->invoiceSum
                                ->join('charge_type AS ch', 'ch.chg_type_id', '=', 'invoice_summary.chg_type_id')
                                ->select('inv_sum_id', 'cus_id', 'whs_id', 'chg_code_id',
                                          'chg_uom_id', 'ch.chg_type_id', 'dt', 'qty')
                                ->whereIn('chg_code_id', $chgCodeIDs)
                                ->where('cus_id', $data['cus_id'])
                                ->where('whs_id', $data['whs_id'])
                                ->where('sum_sts', 'billable')
                                ->where('inv_sts', 1)
                                ->whereNull('inv_num')
                                ->where('dt', '>=', $data['startDate'])
                                ->where('dt', '<=', $data['endDate'])
                                ->where('chg_type_name', Invoice::RC)
                                ->whereIn('dt', $data['receiving'])
                                ->get()
                                ->toArray();

            $results = array_merge($results, $rcvResults);
        }


        $storDates = getDefault($data['storage'][$cusID]) ?
                        array_keys($data['storage'][$cusID]) : NULL;

        //Storage charge code
        if ($storDates) {

            $storResults =  $this->invoiceSum
                                ->join('charge_type AS ch', 'ch.chg_type_id', '=', 'invoice_summary.chg_type_id')
                                ->select('inv_sum_id', 'cus_id', 'whs_id', 'chg_code_id',
                                          'chg_uom_id', 'ch.chg_type_id', 'dt', 'qty')
                                ->whereIn('chg_code_id', $chgCodeIDs)
                                ->where('cus_id', $data['cus_id'])
                                ->where('whs_id', $data['whs_id'])
                                ->where('sum_sts', 'billable')
                                ->where('inv_sts', 1)
                                ->whereNull('inv_num')
                                ->where('dt', '>=', $data['startDate'])
                                ->where('dt', '<=', $data['endDate'])
                                ->where('chg_type_name', Invoice::ST)
                                ->whereIn('dt', $storDates)
                                ->get()
                                ->toArray();

            $results = array_merge($results, $storResults);
        }


        //Order Processing charge code
        if ($data['order']) {

            $ordResults = $this->invoiceSum
                                ->join('charge_type AS ch', 'ch.chg_type_id', '=', 'invoice_summary.chg_type_id')
                                ->select('inv_sum_id', 'cus_id', 'whs_id', 'chg_code_id',
                                          'chg_uom_id', 'ch.chg_type_id', 'dt', 'qty')
                                ->whereIn('chg_code_id', $chgCodeIDs)
                                ->where('cus_id', $data['cus_id'])
                                ->where('whs_id', $data['whs_id'])
                                ->where('sum_sts', 'billable')
                                ->where('inv_sts', 1)
                                ->whereNull('inv_num')
                                ->where('dt', '>=', $data['startDate'])
                                ->where('dt', '<=', $data['endDate'])
                                ->where('chg_type_name', Invoice::OP)
                                ->whereIn('dt', $data['order'])
                                ->get()
                                ->toArray();

            $results = array_merge($results, $ordResults);
        }


        if (! $results) {
            return;
        }

        foreach ($results as $row) {
            $qty = '-' . $row['qty'];

            $insertParams = [
                'whs_id' => $data['whs_id'],
                'cus_id' => $data['cus_id'],
                'inv_id' =>  $data['invoiceID'],
                'inv_num' =>  $data['invoiceNo'],
                'chg_code_id' => $row['chg_code_id'],
                'chg_uom_id' => $row['chg_uom_id'],
                'chg_type_id' => $row['chg_type_id'],
                'dt' => $row['dt'],
                'qty' => $qty,
                'sum_sts' => 'invoiced'
            ];

            //update the billable charge codes that are
            //invoiced with invoice number
            $this->invoiceSum
                 ->where('inv_sum_id', $row['inv_sum_id'])
                 ->whereNull('inv_num')
                 ->update([
                        'inv_id' => $data['invoiceID'],
                        'inv_num' => $data['invoiceNo'],
                        'updated_by' => $data['userID'],
                ]);

            //insert invoiced entries
            $this->invoiceSum->create($insertParams);
        }
    }

     /**
    * @data array
    * @return array
    */

    public function invoiceEmailed($insert)
    {
        //insert invoiced entries
        $this->InvEmailed->create($insert);
    }


    /**
    * @params array
    * @return array
    */

    function updateHistory($params)
    {
        $cust_id = $params['cust_id'];
        $whs_id = $params['whs_id'];
        $invoiceID = $params['invID'];
        $invoiceNo = $params['invNum'];
        $containerValues = $params['containerValues'];

        $model = $params['table'];

        $values = getDefault($params['target']['Receiving']);

        if (! $values) {
            return FALSE;
        }

        $this->rmInvHist($model, $params);

        foreach ($containerValues as $value) {

            $check = [
                 'gr_hdr_id' => $value->gr_hdr_id,
            ];

            $insert = [
                'inv_id' =>  $invoiceID,
                'inv_num' => $invoiceNo,
                'cus_id' => $cust_id,
                'whs_id' =>  $whs_id,
                'gr_hdr_id' => $value->gr_hdr_id,
                'recv_dt' => $value->created_at,
                'inv_sts' => 1
            ];

            $model->updateOrCreate($check, $insert);
        }
    }

    /**
    * @table array
    * @params array
    * @return array
    */

    function rmInvHist($model, $params)
    {
        $model
            ->where(function($query) use($params) {
                $query->where('inv_id', $params['invID']);

                if (getDefault($params['cat'])) {
                     $query->where('category', $params['cat']);
                }
            })
            ->update([
                'inv_sts' => 0
            ]);
    }

    /**
    * @params array
    * @return array
    */

    function updateOrdProcHistory($params)
    {
        if (! $params['values']) {
            return FALSE;
        }

        $model =  $params['table'];

        $this->rmInvHist($model, $params);

        foreach ($params['values'] as $ordID => $ord_num) {

            $check = [
                'ord_id' => $ordID,
            ];

            $insert = [
                'inv_id' =>  $params['invID'],
                'inv_num' => $params['invNum'],
                'cus_id' => $params['cust_id'],
                'whs_id' =>  $params['whs_id'],
                'ord_id' => $ordID,
                'ord_num' => $ord_num,
                'inv_sts' => 1
            ];

           $model->updateOrCreate($check, $insert);
        }
    }

    /**
    * @params array
    * @return array
    */

    function updateMonthHistory($params)
    {
        $invID = $params['invID'];
        $invNum = $params['invNum'];
        $cust_id = $params['cust_id'];
        $whs_id = $params['whs_id'];
        $model =  $params['table'];

        $this->rmInvHist($model, ['invID' => $invID]);

        foreach ($params['cats'] as $cat => $dates) {
            if (! getDefault($dates[$cust_id])) {
                continue;
            }

            foreach (array_keys($dates[$cust_id]) as $date) {

                $check = [
                    'cus_id' => $cust_id,
                    'whs_id' => $whs_id,
                    'inv_date' => $date,
                    'type' => $cat,
                ];

                $insert = [
                    'inv_id' => $invID,
                    'inv_num' => $invNum,
                    'type' => $cat,
                    'cus_id' => $cust_id,
                    'whs_id' => $whs_id,
                    'inv_date' => $date,
                    'inv_sts' => 1
                ];

                $model->updateOrCreate($check, $insert);
            }

        }
    }


    /**
    * @values array
    * @return array
    */

    function getContainerInfo($values)
    {
        $results = DB::table('gr_hdr AS gh')
                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'gh.gr_hdr_num')
                        ->join('asn_hdr AS ah', 'ah.asn_hdr_id', '=', 'gh.asn_hdr_id')
                        ->join('customer AS cs', 'gh.cus_id', '=', 'cs.cus_id')
                        ->select('gh.gr_hdr_id', 'gh.ctnr_num', 'ah.asn_hdr_ref', 'gh.gr_hdr_num',
                                   DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d") AS created_at')
                                )
                        ->where('evt_code', config('constants.event.GR-COMPLETE'))
                        ->whereIn('gh.gr_hdr_id', $values)
                        ->groupBy('gh.gr_hdr_id')
                        ->get();

        return $results;
    }

    /**
    * @recNums array
    * @date array
    * @return array
    */

    function getRcvLaborHrs($recNums, $date)
    {
        //get expected using gr_hdr_id
        $results = $this->labor
                        ->join('gr_hdr AS gh', 'gh.gr_hdr_id', '=', 'labor.ref_id')
                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'gh.gr_hdr_num')
                        ->select('labor.id')
                        ->where(function($query) use($date, $recNums) {
                                $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '>=', $date['startDate']);
                                $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '<=', $date['endDate']);

                                $query->where('category', config('constants.labor_cat.RCV'));
                                $query->where('inv_sts', 1);
                                $query->where('gh.gr_sts', config('constants.gr_status.RECEIVED'));
                                $query->where('evt_code', config('constants.event.GR-COMPLETE'));

                                if ($recNums) {
                                    $query->whereIn('gh.gr_hdr_id', $recNums);
                                }
                            })
                        ->lists('id')
                        ->toArray();

        return $results;
    }

    /**
    * @params array
    * @return array
    */

    function updateRcvLaborHistory($params)
    {
        $invNum = $params['invNum'];
        $invoiceID = $params['invID'];
        $recLabor = $params['recLabor'];
        $model = $params['table'];

        if (! $recLabor) {
            return FALSE;
        }

        $this->rmInvHist($model, [
            'invID' => $invoiceID,
            'cat' => 'rcv',
        ]);

        $update = [
            'inv_id' =>  $invoiceID,
            'inv_num' => $invNum,
            'inv_sts' => 1
        ];

        $model->whereIn('id', $recLabor)
              ->update($update);
    }

    /**
    * @values array
    * @dateRange array
    * @return array
    */

    function getOrdLaborHrs($orderIDs, $date)
    {
        $eventCode = [
            config('constants.event.ORD-SHIPPED'),
            config('constants.event.PR-SHIPPED')
        ];

        $results = $this->labor
                        ->join('odr_hdr AS oh', 'oh.odr_id', '=', 'labor.ref_id')
                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'oh.odr_num')
                        ->select('labor.id')
                        ->where(function($query) use($date, $orderIDs, $eventCode) {
                               $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '>=', $date['startDate']);
                               $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '<=', $date['endDate']);
                               $query->where('category', config('constants.labor_cat.OP'));
                               $query->where('inv_sts', 1);
                               $query->whereIn('evt_code', $eventCode);

                               if ($orderIDs) {
                                   $query->whereIn('oh.odr_id', $orderIDs);
                               }
                           })
                          ->lists('id')
                          ->toArray();

        return $results;
    }

    /**
    * @params array
    * @return array
    */

    function  updateOrdLaborHistory($params)
    {
        $invNum = $params['invNum'];
        $invoiceID = $params['invID'];
        $ordLabor = $params['ordLabor'];
        $model = $params['table'];

        if (! $ordLabor) {
            return FALSE;
        }

        //$this->rmInvHist($model, ['invID' => $invoiceID]);

        $this->rmInvHist($model, [
            'invID' => $invoiceID,
            'cat' => 'op',
        ]);

        $update = [
            'inv_id' =>  $invoiceID,
            'inv_num' => $invNum,
            'inv_sts' => 1
        ];

        $model->whereIn('id', $ordLabor)
              ->update($update);
    }

    /**
    * @values array
    * @dateRange array
    * @return array
    */

    function getWoLaborHrs($orderIDs, $date)
    {
        $eventCode = [
            config('constants.event.ORD-SHIPPED'),
            config('constants.event.PR-SHIPPED')
        ];

        $results = $this->labor
                        ->join('wo_hdr AS wh', 'wh.wo_hdr_id', '=', 'labor.ref_id')
                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'wh.odr_hdr_num')
                        ->select('labor.id')
                        ->where(function($query) use($date, $orderIDs, $eventCode) {
                               $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '>=', $date['startDate']);
                               $query->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), '<=', $date['endDate']);
                               $query->where('category', config('constants.labor_cat.WO'));
                               $query->where('inv_sts', 1);
                               $query->whereIn('evt_code', $eventCode);

                               if ($orderIDs) {
                                   $query->whereIn('wh.odr_hdr_id', $orderIDs);
                               }
                           })
                          ->lists('id')
                          ->toArray();

        return $results;
    }

    /**
    * @params array
    * @return array
    */

    function  updateWoLaborHistory($params)
    {
        $invNum = $params['invNum'];
        $invoiceID = $params['invID'];
        $woLabor = $params['woLabor'];
        $model = $params['table'];

        if (! $woLabor) {
            return FALSE;
        }

        //$this->rmInvHist($model, ['invID' => $invoiceID]);

        $this->rmInvHist($model, [
            'invID' => $invoiceID,
            'cat' => 'wo',
        ]);

        $update = [
            'inv_id' =>  $invoiceID,
            'inv_num' => $invNum,
            'inv_sts' => 1
        ];

        $model->whereIn('id', $woLabor)
              ->update($update);
    }

    /**
    * @input array
    */

    public function printInvoice($input)
    {
        $containerName = $referenceCode = $storRange = $rcvItems = $ordItems =
                 $rcvResult = $ordResult = $container = [];

        $inv_num = $input['inv_num'];
        $cusID = $input['cus_id'];
        $startDate = $input['startDate'];
        $endDate = $input['endDate'];

        $storRange = $startDate . ' TO ' . $endDate;

        $pageLength = $this->pdfPortraitPageLength;
        $fontSize = $this->pdfFontSize;
        $invoiceFooterHeight = $this->pdfInvoiceFooterHeight;

        $pdf = new \TCPDF('P', 'mm', 'Letter', TRUE, 'UTF-8', FALSE);

        $pdf->setPrintHeader(FALSE);
        $pdf->setPrintFooter(FALSE);
        $pdf->SetAutoPageBreak(TRUE, 0);
        $pdf->SetLeftMargin(10);
        $pdf->setCellPaddings(1, 0, 1, 0);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFont('helvetica', '', $fontSize);
        $pdf->SetFillColor(240, 240, 240);


        $detailsStru = [
            '' => [
                'width' => 10,
                'align'  => 'C',
            ],
            'DESC' => [
                'width' => 70,
                'field' => 'chg_cd_desc',
                'desc' => TRUE,
                'align'  => 'L',
            ],
            'QTY' => [
                'width' => 20,
                'field' => 'chg_cd_qty',
                'align'  => 'L',
            ],
            'UOM' => [
                'width' => 40,
                'field' => 'sys_uom_name',
                'align'  => 'L',
            ],
            'PRICE' => [
                'width' => 25,
                'field' => 'chg_cd_price',
                'currency' => TRUE,
                'align'  => 'L',
            ],
            'AMT' => [
                'width' => 30,
                'field' => 'chg_cd_amt',
                'currency' => TRUE,
                'align'  => 'L',
            ],
        ];


        $headerParams = $this->getPrintInvoiceHeaderParams([
            'pdf' => $pdf,
            'inv_num' => $inv_num,
            'cus_id' => $cusID,
            'detailsStru' => $detailsStru,
        ]);

        $rowCount = $this->displayInvoiceHeader($headerParams);

        $invoiceDetails = $this->getPrintInvoiceDetailsParams([
            'pdf' => $pdf,
            'inv_num' => $inv_num,
            'cus_id' => $cusID,
            'detailsStru' => $detailsStru,
        ]);

        //get containers from inv_his_rcv table
        $rcvItems = $this->container->getInvoicedContainers($inv_num);

        if (count($rcvItems) == 1) {
            //$container = $this->container->getContainerName($rcvItems);
            $containerRes = $this->grHdr->getContainerData($rcvItems);

            $containerName = $containerRes['containerName'];
            $referenceCode  = $containerRes['referenceCode'];
        }

        $receiving = $this->grHdr->getContainerReceivedDate($rcvItems);
        $rcvResult = $receiving->get()
                                ->toArray();

        //get order numbers from inv_his_ord_prc
        $ordItems = $this->orderModel->getInvoicedOrders($inv_num);

        $order = $this->orderModel->getOrderDetails($ordItems);
        $ordResult = $order->get()
                           ->toArray();


        $netOrder = $discount = $freight = $salesTax = 0;
        $currencyCode = NULL;

        $detailsRowCount = count($invoiceDetails);

        $count = 0;


        foreach ($invoiceDetails as $values) {

            $rowCount++;
            $count++;

            $isLastRow = $count == $detailsRowCount;

            if ($rowCount > $pageLength
             || $isLastRow && $rowCount > $pageLength) {

                $this->displayPageFooter($pdf, $rowCount);

                $rowCount = $this->displayInvoiceHeader($headerParams);

                $rowCount++;
            }

            $values['chg_cd_cur'] = $this->currencyCode;

            $detailParams = [
                'pdf' => $pdf,
                'detailsStru' => $detailsStru,
                'uom' => $values['sys_uom_name'],
                'type' => $values['chg_cd_type'],
                'currency' => $values['chg_cd_cur'],
                'rcvItems' => $rcvItems,
                'ordItems' => $ordItems,
                'rcvResult' => $rcvResult,
                'ordResult' => $ordResult,
                'containerName' => $containerName,
                'referenceCode' => $referenceCode,
                'storRange' => $storRange,
                'count' => $count,
                'rowCount' => $rowCount,
                'values' => $values
            ];


            $rowCount = $this->displayDetailRow($detailParams);

            $currencyCode = $values['chg_cd_cur'];

            $netOrder += $values['chg_cd_amt'];

            $pdf->Ln();
        }

        $pdf->Ln();

        $rowCount = $this->displayInvoiceFooter($pdf, $currencyCode, $rowCount, [
            'Net Order' => $netOrder,
            'Discount' => $discount,
            'Freight' => $freight,
            'Sales Tax' => $salesTax
        ]);


        if ($rowCount + $this->pdfExtraLength > $pageLength) {

            $this->displayPageFooter($pdf, $rowCount);

            $pdf->AddPage('P');

            $rowCount = 0;
        }

        $rowCount++;

        $rowCount = $this->displayContact($pdf, $rowCount);

        $pdf->Ln();

        $this->displayDetach($pdf, $headerParams, $rowCount);

        $pdf->Ln();
        $pdf->Ln();

        $containerParams = [
            'pdf' => $pdf,
            'data' => $rcvResult,
            'headerStru' => $this->containerHeader(),
            'title' => 'LIST OF CONTAINERS'
        ];

        $orderParams = [
            'pdf' => $pdf,
            'data' => $ordResult,
            'headerStru' => $this->orderHeader(),
            'title' => 'LIST OF ORDERS'
        ];

        //containers attachment list
        if (count($rcvItems) > 1) {
            $this->displayAttachementList($containerParams);
        }

        $pdf->Ln();
        $pdf->Ln();

        //Orders attachement List
        if (count($ordItems) > 1) {
            $this->displayAttachementList($orderParams);
        }

        $fileName = 'Invoice_' . $inv_num . '.pdf';

        $pdf->setTitle($fileName);

        $pdf->Output($fileName, 'D'); // for user download

        //$path = public_path('download/invoice');

        $path = storage_path('logs/'. $fileName);

        $pdf->Output($path, 'F'); // for server repository

        // for server repository
        Helpers::uploadFileS3(env('BUCKET_INVOICE') . $fileName, $path);

        //delete file on server
        unlink($path);
    }


    /**
    * @input array
    */

    public function printStatementInvoice($input)
    {
        $cusID = $input['cusId'];
        $startDate = $input['startDate'];
        $endDate = $input['endDate'];

        //printstatement
        $pageLength = $this->pdfPortraitPageLength;
        $rowHeight = $this->pdfRowHeight;
        $fontSize = $this->pdfFontSize;

        $pdf = new \TCPDF('P', 'mm', 'Letter', TRUE, 'UTF-8', FALSE);

        $pdf->setPrintHeader(FALSE);
        $pdf->setPrintFooter(FALSE);
        $pdf->SetAutoPageBreak(TRUE, 0);
        $pdf->SetLeftMargin($this->pdfLeftMargin);
        $pdf->setCellPaddings(1, 0, 1, 0);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFont('helvetica', '', $fontSize);
        $pdf->SetFillColor(240, 240, 240);


        $tableStru = [
            '' => [
                'width' => 10,
            ],
            'INV NBR' => [
                'width' => 30,
                'field' => 'invNbr',
            ],
            'INV DT' => [
                'width' => 40,
                'field' => 'invDT',
            ],
            'TTL' => [
                'width' => 40,
                'field' => 'total',
            ],
            'INV STS' => [
                'width' => 35,
                'field' => 'sts',
            ],
            'PMNT DT' => [
                'width' => 40,
                'field' => 'pmntDT',
            ],
        ];

        $headerParams = [
            'pdf' => $pdf,
            'cus_id' => $cusID,
            'tableStru' => $tableStru,
        ];

        $headerParams['billTo'] = $this->clientModel->getBillTo($cusID);
        $headerParams['fromDate'] = $startDate;
        $headerParams['toDate'] = $endDate;

        $results = $this->getInvoiceStatementData($input);

        //replace the sts code
        $rowCount = $this->displayStatementHeader($headerParams);

        $count = $total = 0;

        foreach ($results as $values) {

            $rowCount++;
            $count++;

            if ($rowCount > $pageLength) {

                $rowCount = $this->displayStatementHeader($headerParams);

                $rowCount++;
            }

            foreach ($headerParams['tableStru'] as $stru) {

                $field = getDefault($stru['field']);

                $text = $field ? $values[$field] : $count;

                $this->myMultiCell($pdf, $stru['width'], $rowHeight, $text, 0,
                        'L ', 1, $rowCount % 2 == 0);
            }

            $total += str_replace($this->currencyCode, NULL, $values['total']);

            $pdf->Ln();
        }

        $pdf->SetFont('helvetica', 'B', $fontSize);

        foreach ($headerParams['tableStru'] as $stru) {

            $field = getDefault($stru['field']);

            $text = $field == 'total' ?
                    $this->currencyCode . ' ' . number_format($total, 2) : NULL;

            $this->myMultiCell($pdf, $stru['width'], $rowHeight, $text, 0, 'L ', 1);
        }

        $pdf->SetFont('helvetica', '', $fontSize);

        $rowCount++;

        $this->displayPageFooter($pdf, $rowCount);

        $customerName = 'test';

        $fileName = 'Invoice Statement_' . $customerName . '.pdf';

        $pdf->setTitle($fileName);

        $pdf->Output($fileName, 'D'); // for user download

        //$path = public_path('download/invoice');

        //$pdf->Output($path . '/' . $fileName, 'F'); // for server repository

        $path = storage_path('logs/'. $fileName);

        $pdf->Output($path, 'F'); // for server repository

        // for server repository
        Helpers::uploadFileS3(env('BUCKET_INVOICE') . $fileName, $path);

        //delete file on server
        unlink($path);
    }

    /*
    ****************************************************************************
    */

    public function getInvoiceStatementData($data)
    {
        $whsID = $data['whsId'];
        $cusID = $data['cusId'];
        $startDate = $data['startDate'];
        $endDate = $data['endDate'];

        $invoiceStatus = [
            INVOICE_STATUS_INVOICED['code'],
            INVOICE_STATUS_PAID['code'],
            INVOICE_STATUS_CANCELLED['code']
        ];

        $results = $this->header
            ->join('customer AS cs', 'cs.cus_id', '=', 'invoice_hdr.cus_id')
            ->select(
                'inv_num AS invNbr',
                'inv_dt AS invDT',
                'inv_amt AS total',
                'inv_sts AS sts',
                'inv_paid_dt AS pmntDT'
            )
            ->where('inv_typ', INVOICE_TYPE_ORIGINAL['code'])
            ->whereIn('inv_sts', $invoiceStatus)
            ->where('invoice_hdr.cus_id', $cusID)
            ->where('whs_id', $whsID)
            ->where('inv_dt', '>=', $startDate)
            ->where('inv_dt', '<=', $endDate)
            ->get()
            ->toArray();

        foreach ($results as &$row) {

            $sts = $row['sts'];

            $row['sts'] = INVOICE_STATUSES[$sts];
        }

        return $results;
    }

    /*
    ****************************************************************************
    */

    function displayInvoiceHeader($data)
    {
        $pdf = $data['pdf'];
        $invoiceNo = $data['invoiceNo'];
        $detailsStru = $data['detailsStru'];
        $invoiceHeader = $data['invoiceHeader'];
        $shipToValues = $data['shipToValues'];

        $pageWidth = $this->pdfPortraitPageWidth;
        $rowHeight = $this->pdfRowHeight;
        $fontSize = $this->pdfFontSize;

        $pdf->AddPage('P');

        $formattedInvoiceNo =
                str_pad($invoiceNo, $this->pdfInvoiceNumberWidth, '0', STR_PAD_LEFT);

        $titleParam = [
            'pdf' => $pdf,
            'invoiceNo' => $formattedInvoiceNo,
            'invoiceHeader' => $invoiceHeader
        ];

        $rowCount = $this->displayInvoiceTitle($titleParam);

        $estimateMargin = 5;

        $columnWidth = floor(($pageWidth - $estimateMargin) / 2);

        $margin = $pageWidth - $columnWidth * 2;

        $captionWidth = 30;

        $valueWidth = $columnWidth - $captionWidth;

        $addresses = [
            'Customer Name' => [
                'vendorName',
                'vendorName',
            ],
            'Address' => [
                'bill_to_add',
                'ship_to_add',
            ],
            'City' => [
                'bill_to_city',
                'ship_to_city',
            ],
            'State' => [
                'bill_to_state',
                'ship_to_state',
            ],
            'Country' => [
                'bill_to_cnty',
                'ship_to_cnty',
            ],
            'Zip' => [
                'bill_to_zip',
                'ship_to_zip',
            ],
            'Tel' => [
                'ctc_ph',
            ],
            'Attn' => [
                'ctc_nm',
            ],
        ];

        $pdf->SetFont('helvetica', 'B', $fontSize);

        $this->myMultiCell($pdf, $captionWidth, $rowHeight, NULL, 0);
        $this->myMultiCell($pdf, $valueWidth, $rowHeight, 'Bill To', 0, 'L');
        $this->myMultiCell($pdf, $margin, $rowHeight, NULL, 0);
        $this->myMultiCell($pdf, $captionWidth, $rowHeight, NULL, 0);
        $this->myMultiCell($pdf, $valueWidth, $rowHeight, 'Ship To', 0, 'L');

        $pdf->Ln();

        $pdf->SetFont('helvetica', '', $fontSize);

        foreach ($addresses as $caption => $field) {

            $billField = getDefault($field[0]);
            $shipField = getDefault($field[1]);

            if (isset($invoiceHeader[$billField]) || isset($shipToValues[$shipField])) {

                $this->displayCustomerAddress([
                    'pdf' => $pdf,
                    'array' => $invoiceHeader,
                    'field' => $billField,
                    'caption' => $caption,
                    'captionWidth' => $captionWidth,
                    'valueWidth' => $valueWidth,
                ]);

                $this->myMultiCell($pdf, $margin, $rowHeight, NULL, 0, 'R');

                $this->displayCustomerAddress([
                    'pdf' => $pdf,
                    'array' => $shipToValues,
                    'field' => getDefault($shipField),
                    'caption' => $caption,
                    'captionWidth' => $captionWidth,
                    'valueWidth' => $valueWidth,
                ]);

                $rowCount++;

                $pdf->Ln();
            }
        }

        $rowCount++;

        $pdf->Ln();

        $headerStru = [
            'CUST' => [
                'width' => 45,
                'value' => $invoiceHeader['vendorName'],
            ],
            'CUST REF' => [
                'width' => 30,
                'value' => $invoiceHeader['cust_ref'],
            ],
            'INV NBR' => [
                'width' => 45,
                'value' => $formattedInvoiceNo,
            ],
            'INV DT' => [
                'width' => 30,
                'value' => $invoiceHeader['inv_dt'],
            ],
            'TERMS' => [
                'width' => 45,
                'value' => $invoiceHeader['net_terms'],
            ],
        ];


        $pdf->SetFont('helvetica', 'B', $fontSize);

        foreach ($headerStru as $caption => $values) {
            $this->myMultiCell($pdf, $values['width'], $rowHeight, $caption, 0, 'C', 1);
        }

        $pdf->SetFont('helvetica', '', $fontSize);

        $rowCount++;

        $pdf->Ln();

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->Line($x, $y, $x + $pageWidth, $y);

        $headerValues = [
            'CUST' => [
                'width' => 45,
                'value' => $invoiceHeader['vendorName'],
            ],
            'CUST REF' => [
                'width' => 30,
                'value' => $invoiceHeader['cust_ref'],
            ],
            'INV NBR' => [
                'width' => 45,
                'value' => $formattedInvoiceNo,
            ],
            'INV DT' => [
                'width' => 30,
                'value' => $invoiceHeader['inv_dt'],
            ],
            'TERMS' => [
                'width' => 45,
                'value' => $invoiceHeader['net_terms'],
            ],
        ];


        foreach ($headerValues as $caption => $values) {
            $this->myMultiCell($pdf, $values['width'], $rowHeight,
                    $values['value'], 0, 'C', 1, TRUE);
        }

        $rowCount += 2;

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('helvetica', 'B', $fontSize);

        foreach ($detailsStru as $key => $stru) {
            $this->myMultiCell($pdf, $stru['width'], $rowHeight, $key, 0, 'L', 1);
        }

        $pdf->SetFont('helvetica', '', $fontSize);

        $rowCount++;

        $pdf->Ln();

        $y = $pdf->GetY();

        $pdf->Line($x, $y, $x + $pageWidth, $y);

        return $rowCount;
    }

    /*
    ****************************************************************************
    */

    function displayInvoiceTitle($param)
    {
        $pdf = $param['pdf'];
        $invoiceNo = $param['invoiceNo'];

        $custRef = $param['invoiceHeader']['cust_ref'];
        $invDate = $param['invoiceHeader']['inv_dt'];


        $pageWidth = $this->pdfPortraitPageWidth;
        $rowHeight = $this->pdfRowHeight;
        $leftMargin = $this->pdfLeftMargin;
        $fontSize = $this->pdfFontSize;
        $invoiceWidth = $this->pdfInvoiceWidth;

        $invoiceMargin = $pageWidth - $this->pdfLogoWidth;
        $invoiceHeight = $rowHeight * 2;

        $valueWidth = 30;
        $columnMargin = $pageWidth - $invoiceWidth;


        $rowCount = 1;

        //logo
        $logo = env('HTTPS_API_DOMAIN') . '/assets/images/logo-full.png';

        $y = $pdf->GetY();

        $invoiceTitle = [
            'Date' => $invDate,
            'Invoice#' => $invoiceNo,
            'Customer ID' => $custRef

        ];

        if ( getimagesize($logo) ) {
            $pdf->Image($logo, '5', '10', '', 23, '', '', '', FALSE, 300, 'L',
                false, false, 0, false, false, false);

            $pdf->SetFont('helvetica', '', $fontSize + 6);

            $text = 'INVOICE ';

            $pdf->SetXY($invoiceMargin, $y);

            $this->myMultiCell($pdf, $invoiceMargin, $invoiceHeight, $text, 0, 'R', 1);

            $pdf->Ln();

            $y = $pdf->GetY();

            foreach ($invoiceTitle as $caption => $value) {

                $pdf->SetFont('helvetica', '', $fontSize);

                $text = $caption . CHR(32);

                $pdf->SetXY($invoiceMargin, $y);

                $this->myMultiCell($pdf, $columnMargin, $rowHeight, $text, 0, 'R');
                $this->myMultiCell($pdf, $valueWidth, $rowHeight, $value, 1, 'L');

                $y += $rowHeight;

                $rowCount++;
            }

            $pdf->Ln(10);
        }

        $addressWidth = $this->pdfSeldatAddressWidth;

        $titleHeight = $rowHeight * count($this->pdfSeldatAddress);
        $titleWidth = $pageWidth - $addressWidth;

        $y = $pdf->GetY();

        $pdf->SetFont('helvetica', '', $fontSize);

        foreach ($this->pdfSeldatAddress as $address) {

            $pdf->SetXY($leftMargin, $y);

            $this->myMultiCell($pdf, $addressWidth, $rowHeight, $address, 0, 'L');

            $y += $rowHeight;

            $rowCount++;
        }

        $pdf->Ln();

        $pdf->SetFont('helvetica', '', $fontSize + 6);

        $text = $invoiceNo ? 'INVOICE ' . $invoiceNo : NULL;

        $this->myMultiCell($pdf, $titleWidth, $titleHeight, $text, 0, 'L', 1);

        $rowCount++;

        $pdf->Ln();

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->Line($x, $y, $x + $pageWidth, $y, [
            'width' => 0.5,
        ]);

        // adding 5 to compensate cells height greater than normal cell height
        $rowCount += 5;

        return $rowCount;
    }

    /*
    ****************************************************************************
    */

    function displayInvoiceFooter($pdf, $currencyCode, $rowCount, $data)
    {
        $rowHeight = $this->pdfRowHeight;
        $fontSize = $this->pdfFontSize;
        $pageWidth = $this->pdfPortraitPageWidth;
        $footerWidth = $this->pdfFooterWidth;

        $footerMargin = $pageWidth - $footerWidth;

        $amount = 0;

        foreach ($data as $caption => $value) {

            $rowCount++;

            $this->myMultiCell($pdf, $footerMargin, $rowHeight, $caption, 0, 'R', 1);
            $this->myMultiCell($pdf, $footerWidth, $rowHeight,
                    $currencyCode . ' ' . number_format($value, 2), 0, 'L', 1);

            $amount += $value;

            $pdf->Ln();
        }

        $pdf->Ln();

        $pdf->SetFont('helvetica', 'B', $fontSize);

        $this->myMultiCell($pdf, $footerMargin, $rowHeight, 'Total Invoice', 0, 'R', 1);
        $this->myMultiCell($pdf, $footerWidth, $rowHeight,
                $currencyCode . ' ' . number_format($amount, 2), 0, 'L', 1);

        $pdf->Ln();

        $pdf->SetFont('helvetica', '', $fontSize);

        $rowCount += 4;

        return $rowCount;
    }

    /*
    ****************************************************************************
    */

    function displayPageFooter($pdf, $rowCount)
    {
        $pageWidth = $this->pdfPortraitPageWidth;
        $rowHeight = $this->pdfRowHeight;

        for ($count = 0; $count < max(1, $this->pdfPortraitPageLength - $rowCount); $count++) {
            $this->myMultiCell($pdf, $pageWidth, $rowHeight, NULL, 0, 'C');
            $pdf->Ln();
        }

        $text = 'Page ' . $this->pdfPageNo++;

        $this->myMultiCell($pdf, $pageWidth, $rowHeight, $text, 0, 'C', 1);
    }

    /*
    ****************************************************************************
    */

    function displayStatementHeader($data)
    {
        $pdf = $data['pdf'];
        $tableStru = $data['tableStru'];
        $billTo = $data['billTo'];

        $pageWidth = $this->pdfPortraitPageWidth;
        $rowHeight = $this->pdfRowHeight;
        $fontSize = $this->pdfFontSize;

        $pdf->AddPage('P');

        $rowCount = $rowCount = $this->displayStatementTitle($pdf);

        $pdf->SetFont('helvetica', '', $fontSize + 8);

        $vendorText = 'INVOICE STATEMENT for ' . $billTo['cus_name'];

        $this->myMultiCell($pdf, $pageWidth, $rowHeight + 3, $vendorText, 0);

        $rowCount++;

        $pdf->Ln();

        $fromDate = strtotime($data['fromDate']);
        $toDate = strtotime($data['toDate']);

        $dateText = date('F j, Y', $fromDate) . ' ~ ' . date('F j, Y', $toDate);

        $this->myMultiCell($pdf, $pageWidth, $rowHeight + 3, $dateText, 0);

        $pdf->SetFont('helvetica', '', $fontSize);

        $rowCount += 3;

        $pdf->Ln();

        $margin = 5;

        $columnWidth = floor(($pageWidth - $margin) / 2) - $margin;

        $captionWidth = 30;

        $valueWidth = $columnWidth - $captionWidth;

        $addresses = [
            'Customer Name' => 'cus_name',
            'Address' => 'bill_to_add_line_1',
            'City' => 'bill_to_city',
            'State' => 'bill_to_state',
            'Country' => 'bill_to_cnty',
            'Zip' => 'bill_to_zip'
        ];

        $pdf->SetFont('helvetica', 'B', $fontSize);

        $this->myMultiCell($pdf, $captionWidth, $rowHeight, NULL, 0);
        $this->myMultiCell($pdf, $valueWidth, $rowHeight, 'Bill To', 0, 'L');

        $pdf->Ln();

        $pdf->SetFont('helvetica', '', $fontSize);

        foreach ($addresses as $caption => $field) {
            if (isset($billTo[$field])) {

                $this->displayCustomerAddress([
                    'pdf' => $pdf,
                    'array' => $billTo,
                    'field' => $field,
                    'caption' => $caption,
                    'captionWidth' => $captionWidth,
                    'valueWidth' => $valueWidth,
                ]);

                $rowCount++;

                $pdf->Ln();
            }
        }

        $rowCount++;

        $pdf->Ln();

        $pdf->SetFont('helvetica', 'B', $fontSize);

        foreach ($tableStru as $key => $stru) {
            $this->myMultiCell($pdf, $stru['width'], $rowHeight, $key, 0, 'L', 1);
        }

        $pdf->SetFont('helvetica', '', $fontSize);

        $rowCount++;

        $pdf->Ln();

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->Line($x, $y, $x + $pageWidth, $y);

        return $rowCount;
    }

    /*
    ****************************************************************************
    */

    function displayAttachementList($containerParams)
    {
        $pdf = $containerParams['pdf'];
        $data = $containerParams['data'];
        $headerStru = $containerParams['headerStru'];
        $title = $containerParams['title'];

        $pageWidth = $this->pdfPortraitPageWidth;
        $rowHeight = $this->pdfRowHeight;
        $fontSize = $this->pdfFontSize;

        $pageLength = $this->pdfPortraitPageLength;

        $pdf->AddPage('P');

        $rowCount = $count = 0;

        $pdf->SetFont('helvetica', 'B', $fontSize);
        $pdf->Ln();
        $pdf->Ln();
        $this->myMultiCell($pdf, $pageWidth, $rowHeight, $title, 0, 'C', 1);

        $pdf->Ln();
        $pdf->Ln();

        $rowCount+=5;

        foreach ($headerStru as $caption => $values) {
            $this->myMultiCell($pdf, $values['width'], $rowHeight, $caption, 0, 'C', 1);
        }

        $pdf->SetFont('helvetica', '', $fontSize);

        $pdf->Ln();

        $rowCount+=2;

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->Line($x, $y, $x + $pageWidth, $y);

        $detailsRowCount = count($data);

        $headerParams = [
            'pdf' => $pdf,
            'fontSize' => $fontSize,
            'rowHeight' => $rowHeight,
            'pageWidth' => $pageWidth,
            'headerStru' => $headerStru
        ];

        foreach ($data as $values) {
            $rowCount++;
            $count++;

            $isLastRow = $count == $detailsRowCount;

            if ($rowCount > $pageLength || $isLastRow
                    && $rowCount > $pageLength ) {

                $this->displayPageFooter($pdf, $rowCount);

                $pdf->AddPage('P');

                $pdf->Ln();

                $rowCount = $this->displayListHeader($headerParams);

                $rowCount++;
            }


            foreach ($headerStru as $stru) {
                $field = getDefault($stru['field']);

                $text = $values[$field];

                $this->myMultiCell($pdf, $stru['dWidth'], $rowHeight, $text, 0,
                                  $stru['align'], 1, $rowCount % 2);
            }

            $pdf->Ln();
        }

        $rowCount += 3;

        $this->displayPageFooter($pdf, $rowCount);

        return $rowCount;
    }

    /*
    ****************************************************************************
    */

    function displayListHeader($params)
    {
        $rowCount = 0;

        $pdf = $params['pdf'];
        $fontSize = $params['fontSize'];
        $rowHeight = $params['rowHeight'];
        $pageWidth = $params['pageWidth'];
        $headerStru = $params['headerStru'];

        $pdf->SetFont('helvetica', 'B', $fontSize);

        $pdf->Ln();

        $rowCount++;

        foreach ($headerStru as $caption => $row) {
            $this->myMultiCell($pdf, $row['width'], $rowHeight, $caption, 0, 'C', 1);
        }

        $pdf->Ln();
        $pdf->SetFont('helvetica', '', $fontSize);

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->Line($x, $y, $x + $pageWidth, $y);

        $pdf->Ln();

        $rowCount += 4;

        return $rowCount;
    }

    /*
    ****************************************************************************
    */

    function containerHeader()
    {
        $containerStru = [
            'CLIENT' => [
                'dWidth' => 40,
                'width' => 35,
                'field' => 'cus_name',
                'align'  => 'L',
            ],
            'CONTAINER NBR' => [
                'dWidth' => 45,
                'width' => 40,
                'field' => 'ctnr_num',
                'align'  => 'L',
            ],
            'REF CODE' => [
                'dWidth' => 40,
                'width' => 40,
                'field' => 'asn_hdr_ref',
                'align'  => 'L',
            ],
            'MEASUREMENT SYSTEM' => [
                'dWidth' => 40,
                'width' => 45,
                'field' => 'sys_mea_code',
                'align'  => 'L',
            ],
            'RECEIVED DATE' => [
                'dWidth' => 30,
                'width' => 35,
                'field' => 'created',
                'align'  => 'L',
            ]
        ];

        return $containerStru;
    }

    /*
    ****************************************************************************
    */


    function orderHeader()
    {
        $orderStru = [
            'CLIENT' => [
                'dWidth' => 37,
                'width' => 35,
                'field' => 'cus_name',
                'align'  => 'L',
            ],
            'CLIENT NBR' => [
                'dWidth' => 30,
                'width' => 30,
                'field' => 'cus_po',
                'align'  => 'L',
            ],
            'CUST NBR' => [
                'dWidth' => 30,
                'width' => 30,
                'field' => 'cus_odr_num',
                'align'  => 'L',
            ],
            'SCAN NBR' => [
                'dWidth' => 30,
                'width' => 30,
                'field' => 'odr_num',
                'align'  => 'L',
            ],
            'COMPLETE DATE' => [
                'dWidth' => 30,
                'width' => 30,
                'field' => 'act_cmpl_dt',
                'align'  => 'L',
            ],
            'SHIP BY DATE' => [
                'dWidth' => 25,
                'width' => 22,
                'field' => 'ship_by_dt',
                'align'  => 'L',
            ],
            'STATUS' => [
                'dWidth' => 15,
                'width' => 22,
                'field' => 'odr_sts',
                'align'  => 'L',
            ]
        ];

        return $orderStru;
    }

    /*
    ****************************************************************************
    */

     function displayDetailRow($param)
    {
        $rowHeight = $this->pdfRowHeight;

        $this->rcvUOM = [
            Invoice::$container,
            Invoice::$piece,
            Invoice::$pallet,
            Invoice::$carton,
            Invoice::$month,
            Invoice::$smallCarton,
            Invoice::$mediumCarton,
            Invoice::$largeCarton,
            Invoice::$volume,
            Invoice::$smallCartonImperial,
            Invoice::$mediumCartonImperial,
            Invoice::$largeCartonImperial,
            Invoice::$smallCartonMetric,
            Invoice::$mediumCartonMetric,
            Invoice::$largeCartonMetric,
            Invoice::$volumeImperial,
            Invoice::$volumeMetric
        ];

        $ordItems = $param['ordItems'];
        $rcvItems = $param['rcvItems'];
        $rcvResult = $param['rcvResult'];
        $ordResult = $param['ordResult'];
        $values = $param['values'];
        $storRange = $param['storRange'];

        $currency = $param['currency'];
        $type = $param['type'];

        $pdf = $param['pdf'];
        $detailsStru = $param['detailsStru'];

        $rowCount = $param['rowCount'];

        $addRow = [];

        $columnCount = 0;

        $detail = NULL;

        $highlight = $param['count'] % 2;

        $labor = [
            'LABOR',
            'RUSH-LABOR',
            'OVERTIME-LABOR'
        ];

        if(in_array($values['chg_cd_name'], $labor)) {
            $values['chg_cd_qty'] = '-';
        }

        foreach ($detailsStru as $stru) {

            $columnCount++;

            switch ($columnCount) {
                case 1:
                case 2:
                    $addRow[$columnCount] = $stru['width'];
                break;

                default:
                    $addRow[3] = getDefault($addRow[3], 0);
                    $addRow[3] += $stru['width'];
                break;
            }

            $field = getDefault($stru['field']);

            if ($field) {

                $prefix = getDefault($stru['currency']) ? $currency . ' ' : NULL;

                if ( in_array($param['uom'], $this->rcvUOM)
                        && count($rcvItems) === 1
                        && $type === Invoice::RC
                        && getDefault($stru['desc'])) {

                    $setDate = implode(',' , array_column($rcvResult, 'created'));

                    $detail =  'Container# ' . $param['containerName'] . ' / ' . $param['referenceCode'] .
                               ' Date: ' . $setDate;

                } else if ( (in_array($param['uom'], $this->rcvUOM) || $param['uom'] === Invoice::$order)
                        && count($ordItems) === 1
                        && $type === Invoice::OP
                        && getDefault($stru['desc'])) {

                    $clientOrder = implode(',' , array_column($ordResult, 'cus_po'));
                    $shipByDate = array_get($ordResult[0], 'act_cmpl_dt');
                    $detail = 'Order# ' .  $clientOrder . ' Date:' . $shipByDate;
                } else if ($type === Invoice::ST
                        && getDefault($stru['desc'])) {
                    $detail = $storRange;
                }

                $text = $prefix . $values[$field];

            } else {
                $text = $param['count'];
            }

            $this->myMultiCell($pdf, $stru['width'], $rowHeight, $text, 0,
                    $stru['align'], 1, $highlight);
        }

        if ($detail) {

            $pdf->Ln();

            $this->myMultiCell($pdf, $addRow[1], $rowHeight, '', 0, 'C', 1,
                    $highlight);

            $this->myMultiCell($pdf, $addRow[2], $rowHeight, $detail, 0, 'L', 1,
                    $highlight);

            $this->myMultiCell($pdf, $addRow[3], $rowHeight, '', 0, 'C', 1,
                    $highlight);

            $rowCount++;
        }

        return $rowCount;
    }

    /*
    ****************************************************************************
    */

    function displayContact($pdf, $rowCount)
    {
        $pageLength = $this->pdfPortraitPageLength;
        $rowHeight = $this->pdfRowHeight;
        $fontSize = $this->pdfFontSize;
        $leftMargin = $this->pdfLeftMargin;
        $pageWidth = $this->pdfPortraitPageWidth;

        $columnWidth = floor(($pageWidth - $leftMargin) / 2);

        $margin = $pageWidth - $columnWidth * 2;

        $pdf->Ln();

        $pdf->SetFont('helvetica', 'B', $fontSize);
        $pdf->SetTextColor(0, 0, 128);

        $contact = 'If you have any questions about this invoice,please contact';
        $make = 'Make all checks payable to';

        $this->myMultiCell($pdf, $columnWidth, $rowHeight, $contact, 0, 'L', 1);
        $this->myMultiCell($pdf, $margin, $rowHeight, NULL, 0, 'L', 1);
        $this->myMultiCell($pdf, $columnWidth, $rowHeight, $make, 0, 'R', 1);

        $pdf->Ln();

        $rowCount++;

        $count = 1;

        foreach ($this->pdfContactAddress as $value) {

           $pdf->SetFont('helvetica', '', $fontSize);

            $this->myMultiCell($pdf, $columnWidth, $rowHeight, $value, 0, 'C');
            $this->myMultiCell($pdf, $margin, $rowHeight, NULL, 0, 'L', 1);

            if ($count === 1) {
                $this->myMultiCell($pdf, $columnWidth, $rowHeight, $this->companyName, 0, 'R', 1);
            } else {
                $this->myMultiCell($pdf, $columnWidth, $rowHeight, NULL, 0, 'R', 1);
            }

            $count++;

            $pdf->Ln();

            $rowCount++;
        }

        $pdf->SetFont('helvetica', 'B', $fontSize);

        $thank = 'Thank You For Your Business!';

        $this->myMultiCell($pdf, $pageWidth, $rowHeight, $thank, 0, 'C', 1);

        $pdf->Ln();

        $rowCount++;

        $pdf->SetTextColor(0, 0, 0);

        return $rowCount;
    }

    /*
    ****************************************************************************
    */

    function displayDetach($pdf, $params, $rowCount)
    {

        $rowHeight = $this->pdfRowHeight;
        $fontSize = $this->pdfFontSize;
        $leftMargin = $this->pdfLeftMargin;
        $pageWidth = $this->pdfPortraitPageWidth;

        $invoiceNo = $params['invoiceNo'];

        $formattedInvoiceNo =
                str_pad($invoiceNo, $this->pdfInvoiceNumberWidth, '0', STR_PAD_LEFT);

        $custRef = $params['invoiceHeader']['cust_ref'];

        $columnWidth = floor(($pageWidth - $leftMargin) / 2);

        $margin = $pageWidth - $columnWidth * 2;

        $valueWidth = 30;

        $detachTitle = [
            'DATE' => NULL,
            'Invoice#' => $formattedInvoiceNo,
            'Customer ID' => $custRef,
        ];

        $pdf->Ln();

        $pdf->SetFont('helvetica', 'B', $fontSize);

        $pdf->SetLineStyle([
                    'width' => 0.5,
                    'cap' => 'square',
                    'join' => 'miter',
                    'dash' => 3,
                    'color' => [0,0,0]
        ]);

        $pdf->SetTextColor(0, 0, 0);

        $detach = 'Please detach the portion below and return it with your payment.';

        $this->myMultiCell($pdf, $columnWidth, $rowHeight, $detach, 0, 'L', 1);

        $pdf->Ln();
        $pdf->Ln();

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->SetXY($leftMargin, $y);

        $pdf->Line($x, $y, $x + $pageWidth, $y);

        $pdf->Ln();

        $pdf->SetFont('helvetica', 'B', $fontSize);

        $this->myMultiCell($pdf, $pageWidth, $rowHeight + 5, 'REMITTANCE', 0, 'C', 1);

        $pdf->Ln();

        $pdf->SetFont('helvetica', '', $fontSize);

        $y = $pdf->GetY();

        $rowCount += 3;


        foreach ($this->pdContactNumber as $row) {
           $this->myMultiCell($pdf, $columnWidth, $rowHeight, $row, 0, 'L', 1);
           $pdf->Ln();
           $rowCount++;
        }

        $pdf->SetFont('helvetica', '', $fontSize);

        $pdf->SetLineStyle([
                    'width' => 0.1,
                    'cap' => 'square',
                    'join' => 'miter',
                    'dash' => 0,
                    'color' => [0,0,0]
        ]);


        foreach ($detachTitle as $caption => $value) {

            $pdf->SetXY($columnWidth - $margin, $y);

            $text = $caption . CHR(32);
            $this->myMultiCell($pdf, $columnWidth, $rowHeight, $text, 0, 'R');
            $this->myMultiCell($pdf, $valueWidth, $rowHeight, $value, 1, 'L');

            $y += $rowHeight;
        }

        $y += $rowHeight;
        $pdf->SetXY($columnWidth - $margin, $y);
        $this->myMultiCell($pdf, $columnWidth, $rowHeight, NULL, 0, 'R');
        $this->myMultiCell($pdf, $valueWidth, $rowHeight, NULL, 0, 'L');

        $y += $rowHeight;
        $pdf->SetXY($columnWidth - $margin, $y);
        $this->myMultiCell($pdf, $columnWidth, $rowHeight, 'AMOUNT ENCLOSED', 0, 'R');
        $this->myMultiCell($pdf, $valueWidth, $rowHeight, NULL, 1, 'L');

        $this->displayPageFooter($pdf, $rowCount + $this->pdfInvoiceFooterHeight);

    }


    /*
    ****************************************************************************
    */


    function myMultiCell($pdf, $width, $height, $text=NULL, $border = 1,
        $align = 'C', $stretch = 0, $fill=FALSE)
    {
        if ($stretch) {
            // use cell function to avoid word wrap
            $pdf->Cell($width, $height, $text, $border, 0, $align, $fill, '',
                $stretch, FALSE, 'T', 'M');
        } else {
            $pdf->MultiCell($width, $height, $text, $border, $align, $fill, 0,
                '', '', TRUE, 0, FALSE, FALSE, $height, 'T', FALSE);
        }
    }

    /*
    ****************************************************************************
    */

    function displayCustomerAddress($data)
    {
        $pdf = $data['pdf'];
        $array = $data['array'];
        $field = $data['field'];
        $caption = $data['caption'];
        $captionWidth = $data['captionWidth'];
        $valueWidth = $data['valueWidth'];

        $rowHeight = $this->pdfRowHeight;

        if (isset($array[$field])) {
            $this->myMultiCell($pdf, $captionWidth, $rowHeight, $caption, 0, 'L');
            $this->myMultiCell($pdf, $valueWidth, $rowHeight, $array[$field], 0, 'L');
        } else {
            $this->myMultiCell($pdf, $captionWidth + $valueWidth, $rowHeight, NULL, 0);
        }
    }

    /*
    ****************************************************************************
    */

    function getPrintInvoiceHeaderParams($data)
    {
        $pdf = $data['pdf'];
        $inv_num = $data['inv_num'];
        $detailsStru = $data['detailsStru'];
        $cus_id = $data['cus_id'];

        $cus_info = $this->clientModel->getClientInfo($cus_id);

        $inv_header = $this->invHeader->get($inv_num);

        $invoiceHeader = [
            'cust_id' => $cus_id,
            'vendorName' => $cus_info['cus_name'],
            'inv_dt' => $inv_header['inv_dt'],
            'cust_ref' => $cus_info['cus_code'],
            'bill_to_add' => trim(
                    trim($cus_info['bill_to_add_line_1']) .
                    ' ' .
                    trim($cus_info['bill_to_add_line_1'])
            ),
            'bill_to_state' => $cus_info['bill_to_state'],
            'bill_to_city' => $cus_info['bill_to_city'],
            'bill_to_cnty' => $cus_info['bill_to_country'],
            'bill_to_zip' => $cus_info['bill_to_zip'],
            'net_terms' => $inv_header['net_terms'],
        ];

        $shipToValues = [
            'ship_to_add' => trim(
                    trim($cus_info['ship_to_add_line_1']) .
                    ' ' .
                    trim($cus_info['ship_to_add_line_1'])
            ),
            'ship_to_state' => $cus_info['ship_to_state'],
            'ship_to_city' => $cus_info['ship_to_city'],
            'ship_to_cnty' => $cus_info['ship_to_country'],
            'ship_to_zip' => $cus_info['ship_to_zip'],
        ];

        return [
            'pdf' => $pdf,
            'invoiceNo' => $inv_num,
            'detailsStru' => $detailsStru,
            'invoiceHeader' => $invoiceHeader,
            'shipToValues' => $shipToValues,
        ];
    }

    /*
    ****************************************************************************
    */

    function getPrintInvoiceDetailsParams($data)
    {
        $pdf = $data['pdf'];
        $inv_num = $data['inv_num'];
        $detailsStru = $data['detailsStru'];
        $cus_id = $data['cus_id'];


        return $this->invDetail->get($inv_num);
    }

    /*
    ****************************************************************************
    */

    function displayStatementTitle($pdf, $invoiceNo=NULL)
    {
        $pageWidth = $this->pdfPortraitPageWidth;
        $rowHeight = $this->pdfRowHeight;
        $leftMargin = $this->pdfLeftMargin;
        $fontSize = $this->pdfFontSize;
        $logoWidth = $this->pdfLogoWidth;

        $logoMargin = $pageWidth - $this->pdfLogoWidth;
        $logoHeight = $rowHeight * 3;

        $rowCount = 4;

        //logo
        $logo = env('HTTPS_API_DOMAIN') . '/assets/images/logo-full.png';

        if ( getimagesize($logo) ) {
            $pdf->Image($logo, '5', '10', '', 23, '', '', '', FALSE, 300, 'R',
                false, false, 0, false, false, false);
            $pdf->Ln(25);
        }

        $pdf->Ln();

        $addressWidth = $this->pdfSeldatAddressWidth;

        $titleHeight = $rowHeight * count($this->pdfSeldatAddress);
        $titleWidth = $pageWidth - $addressWidth - 5;

        $y = $pdf->GetY();

        $pdf->SetFont('helvetica', '', $fontSize);

        foreach ($this->pdfSeldatAddress as $address) {

            $pdf->SetXY($leftMargin + $titleWidth, $y);

            $this->myMultiCell($pdf, $addressWidth, $rowHeight, $address, 0, 'R');

            $y += $rowHeight;

            $rowCount++;
        }

        $pdf->Ln();

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->Line($x, $y, $x + $pageWidth, $y, [
            'width' => 0.5,
        ]);

        return $rowCount;
    }

    /*
    ****************************************************************************
    */
    function checkDoubleBilling($data)
    {
        $inv_sts = $this->invHeader->getStatusByNumber($data['inv_num']);

        if ($inv_sts != 'o') {
            return [];
        }

        $open = $this->invoiceListModel->getBillableData([
            'whs_id' => $data['whs_id'],
            'custID' => $data['cus_id'],
            'startDate' => $data['startDate'],
            'endDate' => $data['endDate'],
        ]);

        $doubleBilling = [];

        $openItems = $open['items'];

        if (! $data['storeItems']) {
            // all billable data was selected by checking radio button
            foreach ($data['invoInfo']['items'] as $values) {

                $cat = $values['cat'];

                $data['storeItems'][$cat][] = $values['id'];
            }
        }

        foreach ($data['storeItems'] as $type => $values) {
            if ($type == 'Storage') {
                // skip storage billing
                continue;
            }

            foreach ($values as $value) {

                $checkResult = $this->checkBillingItems($openItems, $type, $value);

                if (! $checkResult) {
                    $doubleBilling[$type][] = $this->getDoubleBilled(
                            $data['invoInfo']['items'], $type, $value
                    );
                }
            }
        }

        return $doubleBilling;
    }

    /*
    ****************************************************************************
    */

    public function checkBillingItems($openItems, $type, $value)
    {
        foreach ($openItems as $openItems) {
            // Receiving and Order Processing
            if ($openItems['cat'] == $type && $openItems['id'] == $value) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /*
    ****************************************************************************
    */

    public function getDoubleBilled($data, $type, $value)
    {
        foreach ($data as $item) {
            if ($item['cat'] == $type && $item['id'] == $value) {
                return $item['ext']['name'];
            }
        }

        return NULL;
    }

    /*
    ****************************************************************************
    */

}
