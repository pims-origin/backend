<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use App\OrderHdr;
use Seldat\Wms2\Models\OrderHdrMeta;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderDtlTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderDtlTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $odrHdrMeta = OrderHdrMeta::where('odr_id', $orderHdr->odr_id)
            ->where('qualifier', 'OFP')
            ->first();
        $partial = $orderHdr->back_odr == 1 ? "Partial " : "";

        $objItems = object_get($orderHdr, 'items', null);
        if (!empty($objItems)) {
            foreach ($objItems as &$item) {
                if (!in_array($item['uom_code'], ['KG', 'PL'])) {
                    $item['piece_qty'] = (int)$item['piece_qty'];
                    $item['qty'] = (int)$item['qty'];
                    $item['allocated_qty'] = (int)$item['allocated_qty'];
                }
            }
        }

        $objPickingItems = object_get($orderHdr, 'picking_items', null);
        if (!empty($objPickingItems)) {
            foreach ($objPickingItems as &$pickingItem) {
                if (!in_array($pickingItem['uom_code'], ['KG', 'PL'])) {
                    $pickingItem['piece_qty'] = (int)$pickingItem['piece_qty'];
                    $pickingItem['qty'] = (int)$pickingItem['qty'];
                    $pickingItem['allocated_qty'] = (int)$pickingItem['allocated_qty'];
                }
            }
        }
        return [
            // Customer
            'cus_id'               => $orderHdr->cus_id,
            'cus_name'             => object_get($orderHdr, 'customer.cus_name', null),
            'cus_odr_num'          => $orderHdr->cus_odr_num,
            'ref_cod'              => $orderHdr->ref_cod,
            'cus_po'               => $orderHdr->cus_po,
            'odr_type_key'         => $orderHdr->odr_type,
            'odr_type'             => Status::getByKey("Order-type", $orderHdr->odr_type),
            'rush_odr'             => $orderHdr->rush_odr,
            'pack_odr'             => array_get($odrHdrMeta, 'value', null),
            'sku_ttl'              => $orderHdr->sku_ttl,
            'odr_sts_key'          => $orderHdr->odr_sts,
            'odr_sts'              => $partial . Status::getByKey("Order-status", $orderHdr->odr_sts),
            'odr_num'              => $orderHdr->odr_num,
            'items'                => $objItems,
            'picking_items'        => $objPickingItems,
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date ? date("m/d/Y", $date) : null;
    }
}
