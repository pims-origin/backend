<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class WavePickHdrModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickHdr($data){
        return $this->model->create($data);
    }

    /**
     * @return mixed
     */
    public function getLatestWavePickNumber()
    {
        return $this->model->orderBy('wv_num', 'desc')->take(1)->first();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getWvHdrInfo($wv_id){
        $whs_id = self::getCurrentWhsId();
        return $this->model
            ->where('wv_hdr.wv_id','=',$wv_id)
            ->where('wv_hdr.whs_id','=',$whs_id)
            ->select(
                'wv_hdr.wv_id',
                'wv_hdr.wv_num',
                'wv_hdr.wv_sts',
                'wv_hdr.whs_id',
                'wv_hdr.picker'
            )
            ->first();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->getSearchQuery($attributes, $with);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    public function getSearchQuery($attributes = [], $with = [])
    {
        $query = $this->make($with);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId   = array_get($userInfo, 'current_whs', 0);
        $cusLsts = array_get($userInfo, 'user_customers', 0);
        $cusIds = array_column($cusLsts, 'cus_id');
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        //foreach($cusLsts as $cusLst){
        //    if(!in_array($cusLst['cus_id'], $cusIds)){
        //        array_push($cusIds, $cusLst['cus_id']);
        //    }
        //}

        $query->where('wv_hdr.whs_id', $whsId);
        //$query->where('wv_hdr.wv_sts', '<>', 'PK');
        $query->leftJoin('odr_hdr', 'odr_hdr.wv_id', '=', 'wv_hdr.wv_id');
        $query->select(['wv_hdr.wv_id']);
        $query->select([
            'wv_hdr.*',
            'odr_hdr.odr_id',
            'odr_hdr.cus_po',
            'odr_hdr.odr_num',
            'odr_hdr.cus_odr_num',
        ]);

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'wv_num') {
                    $query->where('wv_hdr.' . $key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if ($key === 'wv_sts'){
                    $query->where(function($query) use($value){
                        foreach ($value as $index => $subValue){
                            if ($index === 0){
                                $query->where('wv_sts', 'LIKE', '%' . SelStr::escapeLike($subValue) . '%');
                            }else{
                                $query->orWhere('wv_sts', 'LIKE', '%' . SelStr::escapeLike($subValue) . '%');
                            }
                        }
                    });
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    if (in_array($key, ['odr_id', 'cus_po', 'odr_num', 'cus_odr_num'])) {
                        $query->orderBy('odr_hdr.'.$key, $order);
                    } else {
                        $query->orderBy('wv_hdr.'.$key, $order);
                    }
                }
            }
        }
        // concat all cus_id of order of wave pick
        // select wave has concat column in customer current user
        $query->whereHas("details", function ($query) use ($cusIds) {
            $query->whereIn("cus_id", $cusIds);
        });
        if (!empty($attributes['picker_id'])) {
            $count = DB::table('user_role')->where('user_id', $userId)->whereIn('item_name', ['Admin', 'manager'])
                ->count();
            if (!$count) {
                $query->where('picker', $userId);
            }
        }
        if (!empty($attributes['picker'])) {

            $query->whereHas("pickerUser", function ($q) use ($attributes) {
                $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['picker']) . "%");
                $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['picker']) . "%");
                $q->orWhere(
                    DB::raw("concat(first_name, ' ', last_name)"),
                    'like',
                    "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['picker'])) . "%"
                );
            });
        }


        if ( isset($attributes['odr_num']) ){
            $query->where('odr_hdr.odr_num', 'LIKE', '%' . $attributes['odr_num'] . '%');
        }

        if ( isset($attributes['cus_po']) ){
            $query->where('odr_hdr.cus_po', 'LIKE', '%' . $attributes['cus_po'] . '%');
        }

        if ( isset($attributes['cus_odr_num']) ){
            $query->where('odr_hdr.cus_odr_num', 'LIKE', '%' . $attributes['cus_odr_num'] . '%');
        }

        //check no-ecom
        $query->where('wv_hdr.is_ecom', 0);

        $this->model->filterData($query);

        return $query;
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     * @deprecated  DONt USE it
     */
    public function updateStatusWvHdr($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    public function updateWvComplete($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function updateWvPicking($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Picking", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function getWvHdrId($wvHdrIds)
    {
        $wvHdrIds = is_array($wvHdrIds) ? $wvHdrIds : [$wvHdrIds];

        $rows = $this->model
            ->whereIn('wv_id', $wvHdrIds)
            ->get();

        return $rows;
    }

    public function isWvCompleted($wv_id){
        $res = $this->model
                    ->where('wv_id', '=', $wv_id)
                    ->where('wv_sts', '=', 'CO')
                    ->first();
        return (!empty($res)) ? true : false;
    }

}
