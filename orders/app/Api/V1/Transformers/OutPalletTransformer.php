<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OutPallet;

/**
 * Class OutPalletTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OutPalletTransformer extends TransformerAbstract
{
    /**
     * @param OutPalletModel $outPallet
     *
     * @return array
     */
    public function transform(OutPallet $outPallet)
    {
        return [
            // Warehouse
            'plt_id'        => object_get($outPallet, 'plt_id', null),
            'odr_id'        => object_get($outPallet, 'odr_hdr_id', null),
            'loc_id'        => object_get($outPallet, 'loc_id', null),
            'loc_code'      => object_get($outPallet, 'loc_code', null),
            'loc_name'      => object_get($outPallet, 'loc_name', null),
            'cus_id'        => object_get($outPallet, 'cus_id', null),
            'whs_id'        => object_get($outPallet, 'whs_id', null),
            'plt_num'       => object_get($outPallet, 'plt_num', null),
            'plt_block'     => object_get($outPallet, 'plt_block', null),
            'plt_tier'      => object_get($outPallet, 'plt_tier', null),
            'ctn_ttl'       => object_get($outPallet, 'ctn_ttl', null),
            'is_movement'   => object_get($outPallet, 'is_movement', null),
            'out_plt_sts'   => object_get($outPallet, 'out_plt_sts', null),
            'created_at'    => $this->dateFormat($outPallet->created_at)
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }
}
