<?php

namespace App\Api\V1\Validators;


class AssignPalletToShippingLocationValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_code' => 'required',
            'plt_rfid' => 'required',
        ];
    }
}
