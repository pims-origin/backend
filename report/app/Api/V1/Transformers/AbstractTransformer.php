<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

abstract class AbstractTransformer extends TransformerAbstract
{
    protected function getDate($date)
    {
        return strlen($date) >= 10 ? date('m/d/Y', $date) : '';
    }
}
