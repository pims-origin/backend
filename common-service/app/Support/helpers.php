<?php

/**
 * Generate random token based on app key
 *
 * @return string
 */
function randomToken()
{
    return sha1(time() . env('APP_KEY'));
}

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     *
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}
