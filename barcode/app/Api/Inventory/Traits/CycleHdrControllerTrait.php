<?php

namespace App\Api\Inventory\Traits;

use App\Api\Inventory\Models\CartonModel;
use App\Api\Inventory\Models\CycleDisModel;
use App\Api\Inventory\Models\CycleDtlModel;
use App\Api\Inventory\Models\CycleHdrModel;
use App\Api\Inventory\Models\CycleProcess;
use App\Api\Inventory\Models\EventTrackingModel;
use App\Api\Inventory\Models\LockModel;
use App\Api\Inventory\Models\PalletModel;
use App\Api\Inventory\Services\CycleCountService;
use App\Api\Inventory\Validators\RecountValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\Item;
use Wms2\UserInfo\Data;
use Illuminate\Http\Response as IlluminateResponse;

trait CycleHdrControllerTrait
{
    /**
     * @param Request $request
     * @param RecountValidator $validator
     *
     * @return mixed
     */
    public function recount(Request $request, RecountValidator $validator)
    {
        $input = $request->getParsedBody();
        $validator->validate($input);

        $model = new CycleProcess();
        $result = $model->recount($input);

        if ($result) {
            return $this->response->accepted(null, ['message' => 'Recount successfully', 'data' => 'success']);
        } else {
            return $this->response->error(implode($model->errors), IlluminateResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @param RecountValidator $validator
     *
     * @return array
     */
    public function approve(Request $request, RecountValidator $validator)
    {
        $input = $request->getParsedBody();
        $validator->validate($input);
        if (empty($input['cycle_dtl_ids'])) {
            return $this->response->errorBadRequest('cycle_dtl_ids can\'t empty');
        }
        $cycleHdrId = $input['cycle_hdr_id'];
        $cycleDtlIds = $arrCycleDtlId = $input['cycle_dtl_ids'];

        if (!array_filter($cycleDtlIds, 'is_int')) {
            return $this->response->errorBadRequest('cycle_dtl_id must be a integer');
        }

        $userId = CycleCountService::getUserId();
        //get cycleHdrObj

        $cycleHdrObj = $this->model->byId($cycleHdrId);

        if ($cycleHdrObj->assignee_id == $userId) {
            return $this->response->errorBadRequest('Permission denied!');
        }

        $cycleProc = new CycleProcess();
        $cycleDtlArr = $cycleProc->getCycleDtlDataProcess($cycleHdrId, $arrCycleDtlId);

        if (!$cycleDtlArr) {
            return $this->response->errorBadRequest('Data not found.');
        }

        $lackCartons = $oddCartons = [];

        $countBy = $cycleHdrObj->count_by;
        $isEach = strtoupper($countBy) === 'EACH' ? true : false; //current only use 2 types
        try {
            DB::beginTransaction();
            $updatedInv = []; //contain list cycle_dtl_id

            foreach ($cycleDtlArr as $rw) {
                $ttlDiscrepancy = $disc = $rw['act_qty'] - $rw['sys_qty'];

                if ($disc < 0) {
                    $oddCartons[] = $rw;
                }
                if ($disc > 0) {
                    $lackCartons[] = $rw;
                }

                //process count_by each
                if (!$isEach) {
                    $ttlDiscrepancy = $disc * $rw['remain'];
                }

                if ($ttlDiscrepancy != 0 && !in_array($rw['cycle_dtl_id'], $updatedInv)) {
                    $cycleProc->updateInventory($rw['whs_id'], $rw['item_id'], $rw['lot'], $ttlDiscrepancy);
                    $updatedInv[] = $rw['cycle_dtl_id'];
                }
            }
            unset($updatedInv); //free memory

            $palletModel = new PalletModel();
            if ($oddCartons) {
                $locIds = $cycleProc->delCtnDiscrepancy($oddCartons, $userId, $isEach);
                /*
                 * 1. Update pallet, ctn_ttl count from cartons where locids
                 * 2. update zero when ctl = 0
                 */

                $palletModel->updatePalletCtnTtl($locIds);

                $palletModel->updateZeroPallet($locIds);
            }

            if ($lackCartons) {
                //clone cartons, discrepancy
                $cycleProc->cloneCtnDiscrepancy($lackCartons, $userId, $isEach);
            }

            //process cycle_dtl
            $cycleProc->updateActLocForCycleDtl($cycleHdrId, $arrCycleDtlId);

            //update sts cycle_hdr, cycle_dtl
            CycleDtl::where('cycle_hdr_id', $cycleHdrId)
                ->whereIn('cycle_dtl_id', $arrCycleDtlId)
                ->update([
                    'cycle_dtl_sts' => CycleDtlModel::STATUS_CYCLEDTL_ACCEPTED
                ]);

            $unLockAll = $this->model->updateCycelHdrAfterProcessed($cycleHdrId);

            //unclock carton / location
            $whsId = $cycleHdrObj->whs_id;
            $itemIds = [];
            if ($unLockAll) {
                $itemIds = $cycleProc->getUnclockItemIds($cycleHdrId);
            }

            $sysLocIds = array_unique(array_column($cycleDtlArr, 'sys_loc_id'));
            $sysLocIdUnLock = $this->model->getSysLocIdCanUnLock($cycleHdrId, $sysLocIds);

            if ($itemIds) {
                $locIds = CycleDtlModel::getLocIdByItem($cycleDtlArr, $itemIds);
                LockModel::unLockCartonByItems($whsId, $itemIds, $locIds);
            }

            if ($sysLocIdUnLock) {
                LockModel::unLockLocations($sysLocIdUnLock);
            }

            //unclock All
            if ($unLockAll) {
                (new LockModel())->unClockAllLoc($cycleHdrObj);
            }

            //event tracking
            $data = CycleHdrModel::getCycleHdr($cycleHdrId);
            $cycleNumAndSeq = $this->eventTrackingModel->generateEventTrackingTransNum(
                $cycleHdrId,
                EventTrackingModel::CYCLE_COMPLETED
            );
            $this->eventTrackingModel->create([
                'whs_id'    => $data['whs_id'],
                'cus_id'    => ucfirst($data['cycle_type']) == 'CS' ? $data['cycle_detail'] : null,
                'owner'     => $cycleNumAndSeq['cycle_num'],
                'evt_code'  => EventTrackingModel::CYCLE_COMPLETED,
                'trans_num' => $cycleNumAndSeq['trans_num'],
                'info'      => $cycleNumAndSeq['trans_num'] . ' ' .
                    EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_COMPLETED]
            ]);

            DB::commit();

            return ["data" => "Accept successful"];
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}