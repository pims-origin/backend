<?php

namespace App\Modules\Asns\Eloquents;

class AsnDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'asn_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'asn_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'asn_hdr_id',
        'item_id',
        'ctnr_id',
        'ctnr_num',
        'asn_dtl_po',
        'asn_dtl_po_dt',
        'asn_dtl_ctn_ttl',
        'asn_dtl_crs_doc',
        'asn_dtl_des',
        'asn_dtl_sts',
        'asn_dtl_lot',
        'asn_dtl_length',
        'asn_dtl_width',
        'asn_dtl_height',
        'asn_dtl_weight',
        'asn_dtl_volume',
        'asn_dtl_cube',
        'asn_dtl_pack',
        'asn_dtl_cus_upc',
        'asn_dtl_sku',
        'asn_dtl_size',
        'asn_dtl_color',
        'expired_dt',
        'uom_id',
        'uom_code',
        'uom_name',
        'asn_dtl_qty_ttl',
        'asn_dtl_crs_doc_qty',
        'ucc128',
        'spc_hdl_code',
        'spc_hdl_name',
        'cat_code',
        'cat_name',
        'asn_dtl_ept_dt'
    ];

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    //a GR has a Ctn, a ASN has n Ctn
    public function container()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Container', 'ctnr_id', 'ctnr_id');
    }

    public function asnHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnHdr', 'asn_hdr_id', 'asn_hdr_id');
    }

    public function grDtl()
    {
        return $this->hasOne(__NAMESPACE__ . '\GoodsReceiptDetail', 'asn_dtl_id', 'asn_dtl_id');
    }

    public function systemUom()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemUom', 'uom_id', 'sys_uom_id');
    }

    public function virtualCarton()
    {
        return $this->belongsTo(__NAMESPACE__ . '\VirtualCarton', 'asn_dtl_id', 'asn_dtl_id');
    }

    public function virtualCartonSummary()
    {
        return $this->belongsTo(__NAMESPACE__ . '\VirtualCartonSummary', 'asn_dtl_id', 'asn_dtl_id');
    }
    
    /**
     *  itemCat
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itemCat()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ItemCat', 'cat_code', 'cat_code');
    }

    public function asnDtlMeta()
    {
        return $this->hasOne(__NAMESPACE__ . '\AsnDtlMeta', 'asn_dtl_id', 'asn_dtl_id');
    }
}
