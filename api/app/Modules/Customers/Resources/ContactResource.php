<?php

namespace App\Modules\Customers\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\Customers\Resources\CustomerDetailResource;

class ContactResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
            'type' => 'contacts',
            'id' => (int) $this->cus_ctt_id,
            'attributes' => [
                'first_name'    => $this->cus_ctt_fname,
                'last_name'     => $this->cus_ctt_lname,
                'position'      => $this->cus_ctt_position,
                'email'         => $this->cus_ctt_email,
                'phone'         => $this->cus_ctt_phone,
                'extension'     => $this->cus_ctt_ext,
                'mobile'        => $this->cus_ctt_mobile
            ]
        ];
    }
}