<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 7/25/2016
 * Time: 6:13 PM
 */

namespace App\Api\V1\Validators;


class DamageCartonValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'ctn_id'            => 'integer',
            'dmg_id'            => 'integer',
        ];
    }
}