<?php

namespace App\Api\OUTBOUND\Validators;


use App\Api\V1\Validators\AbstractValidator;

class PutBackPalletValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_rfid' => 'required',
            'pallet_rfid'   => 'required',
        ];
    }
}
