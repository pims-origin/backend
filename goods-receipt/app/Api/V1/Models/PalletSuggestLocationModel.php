<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PalletSuggestLocation;

class PalletSuggestLocationModel extends AbstractModel
{
    /**
     * PalletSuggestLocationModel constructor.
     *
     * @param PalletSuggestLocation|null $model
     */
    public function __construct(PalletSuggestLocation $model = null)
    {
        $this->model = ($model) ?: new PalletSuggestLocation();
    }

    /**
     * Create if not exist
     *
     * @param array $data
     *
     * @return bool
     */
    public function replace(array $data)
    {
        $model = $this->model->firstOrNew(['plt_id' => $data['plt_id']]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    public function createSuggestionLocation($goodsReceipt, $grDtlData, $locations)
    {
        /**
         * 1. check gr_hdr status = RE
         * 2. Get empty locations
         */

        $userInfo = new \Wms2\UserInfo\Data();
        $userInfo = $userInfo->getUserInfo();
        $userId   = $userInfo['user_id'];

        $palSugdata = [];

        foreach ($grDtlData->toArray() as $val) {
            $loc = array_shift($locations);

            $data = [
                'loc_id'     => $loc['loc_id'],
                'plt_id'     => $val['plt_id'],
                'data'       => $loc['loc_code'],
                'item_id'    => array_get($val, 'gr_dtl.item_id', null),
                'ctn_ttl'    => array_get($val, 'ctn_ttl', null),
                'sku'        => array_get($val, 'gr_dtl.sku', null),
                'size'       => array_get($val, 'gr_dtl.size', null),
                'color'      => array_get($val, 'gr_dtl.color', null),
                'gr_hdr_id'  => array_get($val, 'gr_dtl.gr_hdr_id', null),
                'gr_dtl_id'  => array_get($val, 'gr_dtl.gr_dtl_id', null),
                'gr_hdr_num' => $goodsReceipt->gr_hdr_num,
                'whs_id'     => $goodsReceipt->whs_id,
                'put_sts'    => config('constants.putaway_status.NEW'),
                'lot'        => array_get($val, 'gr_dtl.lot', 'NA'),
                'deleted_at' => $goodsReceipt->deleted_at,
                'deleted' => $goodsReceipt->deleted,
                'created_by' => $userId,
                'updated_by' => $userId,
                'created_at' => time(),
                'updated_at' => time(),
            ];
            $palSugdata[] = $data;
        }

        /**
         * Insert batch
         */
        $res = DB::table('pal_sug_loc')->insert(
            $palSugdata
        );

        return $palSugdata;

    }

    /**
     * @param array $pltIds
     *
     * @return mixed
     */
    public function getLocsByPltIds($pltIds = [])
    {
        $pltIds = is_array($pltIds) ? $pltIds : [$pltIds];
        if (empty($pltIds)) {
            return [];
        }

        $query = DB::table('pal_sug_loc')
            ->select([
                'pal_sug_loc.plt_id',
                'location.loc_id',
                'location.loc_code',
                'location.loc_alternative_name',
                'pal_sug_loc.item_id',
                'pal_sug_loc.ctn_ttl',
            ])
            ->leftJoin('location', 'location.loc_id', '=', 'pal_sug_loc.loc_id')
            ->where('pal_sug_loc.deleted', 0);
        $locs = $query->whereIn('pal_sug_loc.plt_id', $pltIds)
            ->get();

        return $locs;
    }

    /**
     * @param $goodsReceiptId
     *
     * @return mixed
     */
    public function GetPalSugLoc($goodsReceiptId)
    {
        $query = $this->model
            ->select([
                'pal_sug_loc.plt_id',
                'pallet.plt_num as plt_name',
                'pal_sug_loc.loc_id',
                'pal_sug_loc.data as suggest_location',
                'pal_sug_loc.item_id',
                'pal_sug_loc.ctn_ttl',
                'pal_sug_loc.sku',
                'pal_sug_loc.color',
                'pal_sug_loc.size',
                'pal_sug_loc.putter',
                'pal_sug_loc.sku',
                'pal_sug_loc.act_loc_id',
                'pal_sug_loc.act_loc_code as actual',
                'pal_sug_loc.lot',
                'pal_sug_loc.updated_at',
                'pallet.created_at',
                'pallet.rfid',
            ])
            ->Join('pallet', 'pallet.plt_id', '=', 'pal_sug_loc.plt_id')
            ->Join('gr_dtl', 'gr_dtl.gr_dtl_id', '=', 'pal_sug_loc.gr_dtl_id')
            ->Join('asn_dtl', 'gr_dtl.asn_dtl_id', '=', 'asn_dtl.asn_dtl_id')
            ->leftJoin('users', 'users.user_id', '=', 'pal_sug_loc.putter');
        $query->addSelect(DB::RAW("CONCAT(users.first_name,' ',users.last_name) as putter_name"));

        $query = $query->where('pal_sug_loc.gr_hdr_id', $goodsReceiptId);
        $query = $query->whereNotIn('pallet.plt_sts', ['IA', 'CC']);
        $query = $query->where('pallet.deleted', 0)

            ->where('asn_dtl.asn_dtl_sts','!=', 'CC')
            ->get();

        return $query;
    }

    public function getPutAwayPrintList($goodsReceiptId){
        $query = $this->model
            ->select([
                DB::raw('count(cartons.ctn_id) as ctn_ttl'),
                'pallet.plt_num as plt_name',
                'pal_sug_loc.loc_id',
                'pal_sug_loc.data as suggest_location',
                'gr_dtl.item_id',
                // 'pal_sug_loc.ctn_ttl',
                'gr_dtl.sku',
                'gr_dtl.color',
                'gr_dtl.size',
                'pal_sug_loc.putter',
                'pal_sug_loc.act_loc_id',
                'pal_sug_loc.act_loc_code as actual',
                'pal_sug_loc.lot',
                'pal_sug_loc.updated_at',
                'pallet.created_at',
                'pallet.rfid',
                'pallet.plt_id',
                'cartons.lpn_carton'
            ])
            ->join('gr_dtl', 'gr_dtl.gr_hdr_id', '=', 'pal_sug_loc.gr_hdr_id')
            ->Join('pallet', 'pallet.plt_id', '=', 'pal_sug_loc.plt_id')
            ->Join('cartons', function($join){
                $join->on('cartons.plt_id', '=', 'pallet.plt_id');
                $join->on('cartons.item_id', '=', 'gr_dtl.item_id');
                $join->on('cartons.gr_dtl_id', '=', 'gr_dtl.gr_dtl_id');
                $join->whereNotIn('cartons.ctn_sts', ['IA', 'AJ']);
                $join->where('cartons.deleted', '=', 0);
            })
            ->leftJoin('users', 'users.user_id', '=', 'pal_sug_loc.putter');

        $query->addSelect(DB::RAW("CONCAT(users.first_name,' ',users.last_name) as putter_name"));
        $query = $query->where('pal_sug_loc.gr_hdr_id', $goodsReceiptId)
                    ->whereIn('pallet.plt_sts', ['AC', 'RG'])
                    ->where('pallet.deleted', 0)
                    ->whereNotNull('cartons.lpn_carton')
                    ->groupBy('pallet.plt_id')
                    ->groupBy('gr_dtl.item_id')
                    ->groupBy('cartons.lpn_carton')
                    ->orderBy('plt_name')
                    ->get();

        return $query;
    }

    public function getPutAwayPrintListV1($goodsReceiptId){
        $query = $this->model
            ->select([
                'pallet.plt_num as plt_name',
                'pal_sug_loc.loc_id',
                'pal_sug_loc.data as suggest_location',
                'gr_dtl.item_id',
                'pal_sug_loc.ctn_ttl',
                'gr_dtl.sku',
                'gr_dtl.color',
                'gr_dtl.size',
                'pal_sug_loc.putter',
                'pal_sug_loc.act_loc_id',
                'pal_sug_loc.act_loc_code as actual',
                'pal_sug_loc.lot',
                'pal_sug_loc.updated_at',
                'pallet.created_at',
                'pallet.rfid',
                'pallet.plt_id',
                'cartons.lpn_carton'
            ])
            ->join('gr_dtl', function($join){
                $join->on('gr_dtl.gr_hdr_id', '=', 'pal_sug_loc.gr_hdr_id');
                $join->on('gr_dtl.item_id', '=', 'pal_sug_loc.item_id');
            })
            ->join('pallet', 'pallet.plt_id', '=', 'pal_sug_loc.plt_id')
            ->join('cartons', 'cartons.plt_id', '=', 'pallet.plt_id')
            ->leftJoin('users', 'users.user_id', '=', 'pal_sug_loc.putter');

        $query->addSelect(DB::RAW("CONCAT(users.first_name,' ',users.last_name) as putter_name"));
        $query = $query->where('pal_sug_loc.gr_hdr_id', $goodsReceiptId)
                    ->whereIn('pallet.plt_sts', ['AC', 'RG'])
                    ->where('pallet.deleted', 0)
                    ->whereNotNull('cartons.lpn_carton')
                    ->groupBy('pallet.plt_id')
                    ->groupBy('gr_dtl.item_id')
                    ->groupBy('cartons.lpn_carton')
                    ->orderBy('plt_name')
                    ->get();

        return $query;
    }

}
