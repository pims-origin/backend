<?php

namespace Seldat\Wms2\Models;

class VirtualCarton extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vtl_ctn';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'vtl_ctn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'ctn_rfid',
        'plt_rfid',
        'loc_rfid',
        'loc_id',
        'loc_code',
        'asn_hdr_id',
        'asn_dtl_id',
        'item_id',
        'ctnr_id',
        'vtl_ctn_sum_id',
        'cus_id',
        'whs_id',
        'is_damaged',
        'vtl_ctn_sts',
        'scanned',
        'created_by',
        'updated_by'
    ];

    public function asnHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnHdr', 'asn_hdr_id', 'asn_hdr_id');
    }

    public function asnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function ctnr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Container', 'ctnr_id', 'ctnr_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function virtualCartonSummary()
    {
        return $this->belongsTo(__NAMESPACE__ . '\VirtualCartonSummary', 'vtl_ctn_sum_id', 'vtl_ctn_sum_id');
    }

    public function locationRfid()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'rfid', 'loc_rfid');
    }

    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }

    public function updatedUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'updated_by', 'user_id');
    }

}
