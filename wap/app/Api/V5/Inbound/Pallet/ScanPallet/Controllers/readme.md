Ticket WAP-455
+ Support multiple SKUs.
+ Unscanned cartons will be assigned to primary SKU.
+ If equal cartons, just choose one SKU.