<?php

namespace Seldat\Wms2\Models;

/**
 * Class ShippingReport
 *
 * @package Seldat\Wms2\Models
 */
class ReportShipping extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rpt_shipping';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'cus_id',
        'cus_name',
        'cus_code',
        'odr_id',
        'odr_num',
        'whs_id',
        'odr_type',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'pack',
        'des',
        'cus_upc',
        'shipping_ctns',
        'cube',
        'picked_qty',
        'shipped_dt',
        'ship_by',
        'cus_odr_num',
        'cus_po',
        'ship_to_name',
        'ship_to_add_1',
        'ship_to_city',
        'ship_to_state',
        'sys_state_name',
        'ship_to_zip',
        'carrier',
		'updated_by',
		'created_at',
		'updated_at',
		'created_by',
		'created_at',
		'deleted',
    ];




}
