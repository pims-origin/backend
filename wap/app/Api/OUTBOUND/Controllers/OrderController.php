<?php

namespace App\Api\OUTBOUND\Controllers;

use App\Api\OUTBOUND\Models\EventTrackingModel;
use App\Api\OUTBOUND\Models\OrderCartonModel;
use App\Api\OUTBOUND\Models\OrderDtlModel;
use App\Api\OUTBOUND\Models\OrderHdrModel;
use App\Api\OUTBOUND\Models\CartonModel;
use App\Api\OUTBOUND\Models\WavePickHdrModel;
use App\Api\OUTBOUND\Models\WavePickDtlModel;
use App\Api\OUTBOUND\Transformers\OrderListShippingTransformer;
use App\Api\OUTBOUND\Transformers\OrderListTransformer;
use App\Api\OUTBOUND\Transformers\OrderDetailTransformer;
use App\Api\OUTBOUND\Transformers\OrderByOdrIDTransformer;
use App\Api\V1\Models\Log;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\OUTBOUND\Transformers\SkuTransformer;

class OrderController extends AbstractController
{

    protected $orderCartonModel;
    protected $orderDtlModel;
    protected $orderHdrModel;
    protected $cartonModel;
    protected $eventTrackingModel;
    protected $wvHdrModel;

    public function __construct()
    {
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->cartonModel = new CartonModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->wvHdrModel  = new WavePickHdrModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function getListOrder($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/$whsId/order";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GLO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get List Order'
        ]);
        /*
         * end logs
         */
        try {

            $sku = array_get($input, 'sku', '');

            $orders = $this->orderHdrModel->getPickingOrders($whsId, $sku, array_get($input, 'limit', 20));

            $orderListTransformer = new OrderListTransformer(['sku'=>$sku]);

            return $this->response->paginator($orders, $orderListTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getOrderDetail($whsId, $ordDtl)
    {
        try {
            $dt = $this->orderDtlModel->getFirstBy('odr_dtl_id', $ordDtl);
            if (empty($dt)) {
                $msg = sprintf("There is no orderDtl %s .", $ordDtl);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            return $this->response->item($dt, new OrderDetailTransformer());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getListOrderPacked($whsId)
    {
        try {
            $dt = $this->orderHdrModel->getListOrderPacked($whsId);

            return $this->response->collection($dt, new OrderListShippingTransformer());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getCartonRfid($whsId, $rfid)
    {
        try {
            $data = [];
            $dt = $this->orderCartonModel->getFirstWhere([
                'ctn_rfid' => $rfid,
                'ctn_sts'  => Status::getByValue('PICKING', 'ODR_CTN_STATUS')
            ]);
            if (empty($dt)) {
                $msg = sprintf("There is no carton %s.", $rfid);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
            $dt->piece_picking = $dt->piece_qty;
            $dt->piece_qty = $dt->piece_qty . '/0';
            $item = $this->cartonModel->getFirstBy('ctn_id', $dt->ctn_id);

            if (!empty($item)) {
                $dt->item_id = $item->item_id;
            }
            $data['data'] = $this->formatArr($dt);

            return new Response($data, 200, [], null);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $odrId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function putCartonOrder($whsId, $odrId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/$whsId/order/$odrId/cartons";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'PCO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Assign carton to order'
        ]);

        /*
         * end logs
         */

        $input['whs_id'] = $whsId;
        $input['odr_id'] = $odrId;

        $ctns = array_get($input, 'ctns-rfid', null);
        if (empty($ctns)) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
           // $msg['code'] = 'CEM';
            $msg['message'] = [
                'status_code' => -4,
                'msg' => "Carton rfid is required"
            ];

            return $this->response->noContent()
                ->setContent(['data' => $msg])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        }

        if (!is_array($ctns)) {
            $ctns = [$ctns];
        }
        // Check duplicate cartons rfid
       $uniqueRFIDs =  array_unique($ctns);
       if(count($uniqueRFIDs) != count($ctns)){
           $duplicateRFID = array_unique(array_diff_assoc($ctns, $uniqueRFIDs));

           $msg = [];
           $msg['status'] = false;
           $msg['iat'] = time();
           $msg['data'] = [];
           // $msg['code'] = 'CEM';
           $msg['message'] = [
               'status_code' => -7,
               'msg' => 'Duplicate Carton: ' . implode(', ', $duplicateRFID)
           ];

           return new Response($msg, 200, [], null);
       }

        //validate carton RFID
        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($ctns as $item) {

            $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
            if (! $ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $item;
                if('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'] = [
                'status_code' => -6,
                'msg' => $errorMsg
            ];

            return new Response($msg, 200, [], null);
        }

        $checkOdr = $this->orderHdrModel->getFirstWhere([
            'whs_id' => $whsId,
            'odr_id' => $odrId
        ]);
        if (empty($checkOdr)) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();

            // $msg['code'] = 'CEM';
            $msg['message'] = [
                'status_code' => -3,
                'msg' => sprintf("The order %s doesn't exist", $odrId)
            ];
            $msg['data'] = [];
            return $msg;
              //  ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        }

        // check ctns not exists on odr ctn table
        $ctnRFIDNotExisted = [];
        foreach ($ctns as $ctn) {
            $checkCtn = $this->orderCartonModel->getFirstWhere([
                'ctn_rfid'   => $ctn
            ]);

            if (empty($checkCtn)) {
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['message'] = [
                    'status_code' => -1,
                    'msg' => "The cartons doesn't exist."
                ];
                $msg['data'] = [];
                return $msg;
            }
            else {
                if (!empty($checkCtn->odr_hdr_id)) {
                    $msg['status'] = false;
                    $msg['iat'] = time();
                    $msg['message'] = [
                        'status_code' => -2,
                        'msg' => "The cartons has been assigned already."
                    ];

                    $msg['data'] = [];
                    return $msg;

                }
            }
            break;
        }

        // check ctns do not belong to order
        $ctnRFIDNotBelongOdr = [];
        foreach ($ctns as $ctn) {
            $checkWv = $this->orderCartonModel->getFirstWhere([
                'ctn_rfid' => $ctn
            ]);
            $wvNum = object_get($checkWv, 'wv_num');

            $orderIds = $this->orderHdrModel->getOdrIdByWvNum($wvNum);

            if (!in_array($odrId, $orderIds)) {
                $ctnRFIDNotBelongOdr[] = $ctn;
            }
        }

        if ($ctnRFIDNotBelongOdr) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            // $msg['code'] = 'CEM';
            $msg['message'] = [
                'status_code' => -1,
                'msg' => sprintf("The cartons doesn't belong to order: %s.", object_get($checkOdr, 'odr_num'))
            ];

            $msg['data'] = $ctnRFIDNotBelongOdr;

            return $msg;
        }

        try {
            // start transaction
            DB::beginTransaction();

            $data = [];
            $actOrderCarton = $this->orderCartonModel->countOdrHdr($odrId);
            if(count($ctns) + $actOrderCarton > $this->orderDtlModel->getTotalCarton($odrId)) {
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['message'] = [
                    'status_code' => -7,
                    'msg' => "Assigned Carton QTY is greater than the required carton QTY for this Order!"
                ];

                $msg['data'] = [];
                return $msg;
            }

            // update carton
            foreach ($ctns as $ctn) {

                $ctnDt = $this->orderCartonModel->getFirstWhere([
                    'ctn_rfid' => $ctn
                ]);

                $odrDtl = $this->orderDtlModel->getFirstWhere([
                    'odr_id'  => $odrId,
                    'item_id' => $ctnDt->item_id,
                    'lot' => $ctnDt->lot,
                ]);

                if (empty($odrDtl)) {
                    $msg = [];
                    $msg['status'] = false;
                    $msg['iat'] = time();
                    $msg['data'] = [];
                    $msg['message'] = [
                        'status_code' => -5,
                        'msg' => sprintf("Wrong SKU %s", $ctn)
                    ];

                    return $msg;
                }

                $dataOdr = [
                    'odr_hdr_id' => $odrId,
                    'odr_num'    => $checkOdr->odr_num,
                    'odr_dtl_id' => $odrDtl->odr_dtl_id,
                    'ctn_sts'    => Status::getByValue('PICKED', 'ODR_CTN_STATUS'),
                    'updated_at' => time()
                ];

                $this->orderCartonModel->updateWhere($dataOdr, ['ctn_rfid' => $ctn]);

                //Update Picking for order details
                $odrDtl->itm_sts = 'PK';
                $odrDtl->save();
                $data = [
                    'result' => [
                        "item_id" => object_get($odrDtl, 'item_id'),
                        "sku" => object_get($odrDtl, 'sku'),
                        "size" => object_get($odrDtl, 'size'),
                        "color" => object_get($odrDtl, 'color'),
                        "lot" => object_get($odrDtl, 'lot'),
                        "sts" => true,
                    ]
                ];
                break;
            }

            //Update Picking for order header
            $checkOdr->odr_sts = 'PK';
            $checkOdr->save();

            //update odr_sts to PD
            $dtlPicked = $this->orderDtlModel->updateOrderDetailPicked($odrId);
            if ($dtlPicked) {
                $this->eventTrackingModel->eventTracking([
                    'whs_id' => $whsId,
                    'cus_id' => $checkOdr->cus_id,
                    'owner' => $checkOdr->odr_num,
                    'evt_code' => 'WOD',
                    'trans_num' => $checkOdr->odr_num,
                    'info' => sprintf('Wap - Assigning Carton(s) to Order Details %s - %s - %s - %s Completed', $ctnDt->sku, $ctnDt->size, $ctnDt->color, $ctnDt->lot)
                ]);
            }

            $hdrPicked = $this->orderHdrModel->updateOrderPicked($odrId);
            if ($hdrPicked) {
                $this->eventTrackingModel->eventTracking([
                    'whs_id' => $whsId,
                    'cus_id' => $checkOdr->cus_id,
                    'owner' => $checkOdr->odr_num,
                    'evt_code' => 'WOH',
                    'trans_num' => $checkOdr->odr_num,
                    'info' => "Wap - Assigning Carton(s) to Order Completed"
                ]);
            }

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['message'] = [
                'status_code' => 1,
                'msg' => sprintf("Successfully!")
            ];
            $msg['data'] = $data;
            return $msg;

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return new Response($this->getResponseData(), 200, [], null);
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function updateOrderStatus($odrID)
    {
        $count = $this->orderDtlModel->countOdrDtlStsNotPD($odrID);
        if (!$count) {
            //update order hdr to PICKED
            return $this->orderHdrModel->updateWhere(
                [
                    'odr_sts' => Status::getByKey('ORDER-STATUS', 'Picked')
                ],
                [
                    'odr_id' => $odrID,
                ]
            );
        }
    }

    // get order by id

    public function getOrderByID(Request $request, $whsId, $ordID)
    {
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/$whsId/order/$ordID";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'GLO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get List Order'
        ]);
        /*
         * end logs
         */

        try {
            $odr = $this->orderHdrModel->getFirstBy('odr_id', $ordID);
            if (empty($odr)) {
                $msg = sprintf("There is no order ID %s .", $ordID);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            return $this->response->item($odr, new OrderByOdrIDTransformer());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function listAssingedCartons($whsId, $odrId, Request $request)
    {
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/$whsId/order/$odrId/cartons";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'LAC',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get List Assigned Cartons'
        ]);
        /*
         * end logs
         */

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $odr = $this->orderDtlModel->getModel()
            ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->join('item', 'item.item_id', '=', 'odr_dtl.item_id')
            ->where('odr_dtl.odr_id', $odrId)
            ->select([
                DB::raw('SUM( CEIL(odr_dtl.alloc_qty/odr_dtl.pack) ) AS total_ctns'),
                DB::raw('SUM( odr_dtl.alloc_qty ) AS piece_ttl'),
                'odr_hdr.odr_num',
                'odr_hdr.odr_id'
            ])->first()->toArray();

        // Check exist order
        $order = $this->orderHdrModel->getFirstWhere(['odr_id' => $odrId]);
        if(!$order){
            $msg = sprintf("This order(order_id=%d) doesn't exist.", $odrId);
            $data = [
                'data'    => $odrId,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        // Check exist order belong with whs_id
        $orders = $this->orderHdrModel->getModel()
                                ->where('whs_id', $whsId)
                                ->get()->toArray();
        $orderIds = array_pluck($orders, 'odr_id', null);
        if(!in_array($odrId, $orderIds)){
            $msg = sprintf("This order(order_num=%s) doesn't belong to this warehouse(whs_id=%d)", object_get($order, 'odr_num'), $whsId);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $odr['cartons'] = $this->orderCartonModel->getModel()
            ->select('ctn_num', 'ctn_rfid', 'ctn_id', 'odr_dtl_id')
            ->where('odr_hdr_id', $odrId)->get()->toArray();

        $odr['assigned_ctns'] = count($odr['cartons']);

        return $odr;
    }

    public function getOrderDtl($whsId, $odrId, Request $request)
    {
        try {
            $input = $request->getParsedBody();
            /*
             * start logs
             */

            $url = "/outbound/{$whsId}/get-detail-order/{$odrId}";
            $owner = $transaction = "";
            Log:: info($request, $whsId, [
                'evt_code' => 'LAC',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => 'Get order detail'
            ]);
            /*
             * end logs
             */

            $order = $this->orderHdrModel->getFirstBy('odr_id', $odrId);
            if(!$order){
                $msg = sprintf("The order (order_id=%d) doesn't exist.", $odrId);

                return [
                    "status" => false,
                    "iat" => time(),
                    "message" => [
                        'status_code' => -1,
                        "msg" => $msg
                    ],
                    "data" => []
                ];
            }

            // Check exist order belong with whs_id
            $orders = $this->orderHdrModel->getModel()
                                    ->where('whs_id', $whsId)
                                    ->get()->toArray();
            $orderIds = array_pluck($orders, 'odr_id', null);
            if(!in_array($odrId, $orderIds)){
                $msg = sprintf("The order (order_num=%s) doesn't belong to warehouse", object_get($order, 'odr_num'));

                return [
                    "status" => false,
                    "iat" => time(),
                    "message" => [
                        'status_code' => -1,
                        "msg" => $msg
                    ],
                    "data" => []
                ];
            }

            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $currentCtns = sprintf("COALESCE((select sum(piece_qty) from odr_cartons where odr_hdr_id = %s AND deleted = 0 AND odr_cartons.odr_dtl_id = ordtl.odr_dtl_id), 0) as current_qtys", $odrId);
            $skus = DB::table('odr_dtl as ordtl')
                ->leftJoin('odr_cartons as odrc', 'ordtl.odr_dtl_id',  '=', 'odrc.odr_dtl_id')
                ->where('ordtl.odr_id', $odrId)
                ->where('ordtl.whs_id', $whsId)
                ->where('ordtl.deleted', 0)
                ->select([
                    DB::raw('ordtl.alloc_qty AS piece_ttl'),
                    DB::raw($currentCtns),
                    DB::raw('count( odrc.odr_dtl_id ) AS scanned_ctns'),
                    DB::raw('GROUP_CONCAT(odrc.ctn_rfid) AS ctn_rfid_list'),
                    'ordtl.sku',
                    'ordtl.size',
                    'ordtl.color',
                    'ordtl.lot',
                    'ordtl.odr_dtl_id',
                    'ordtl.pack',
                ])->groupBy('ordtl.odr_dtl_id')->get();

            $ctnLimitTtl     = 0;
            $ctnCurrentTtl   = 0;
            $pieceLimitTtl   = 0;
            $pieceCurrentTtl = 0;
            foreach($skus as $key=>$curSku) {
                $pack = array_get($curSku, 'pack', 0);
                $pieceLimit      = array_get($curSku, 'piece_ttl', 0);
                $pieceCurrent    = array_get($curSku, 'current_qtys', 0);
                $ctnLimit        = floor($pieceLimit / $pack);
                $ctnCurrent      = floor($pieceCurrent / $pack);
                $ctnLimitTtl     += $ctnLimit;
                $ctnCurrentTtl   += $ctnCurrent;
                $pieceLimitTtl   += $pieceLimit;
                $pieceCurrentTtl += $pieceCurrent;

                $curSku['ctn_limit']     = $ctnLimit;
                $curSku['ctn_current']   = $ctnCurrent;
                $curSku['piece_limit']   = $pieceLimit;
                $curSku['piece_current'] = (int)$pieceCurrent;

                $arr = ($curSku['ctn_rfid_list'] === null) ? [] : explode(',', $curSku['ctn_rfid_list']);
                $curSku['ctn_rfid'] = $arr;
                $skus[$key] = $curSku;
            }

            $result = [
                "status" => true,
                "iat" => time(),
                "message" => [
                    'status_code' => 1,
                    "msg" => "Successfully!"
                ],
                "data" => [
                    "skus"              =>$skus,
                    "odr_num"           => object_get($order, 'odr_num'),
                    "ctn_limit_ttl"     => $ctnLimitTtl,
                    "ctn_current_ttl"   => $ctnCurrentTtl,
                    "piece_limit_ttl"   => $pieceLimitTtl,
                    "piece_current_ttl" => $pieceCurrentTtl,
                ]
            ];

            return $result;

        }catch (\Exception $exception) {
            $error = [
                "status" => false,
                "iat" => time(),
                "message" => [
                    'status_code' => -2,
                    "msg" => "Failed!"
                ],
                "data" => []
            ];
            return $error;
        }

    }

    /**
     * Get sku suggestion for order list page
     * Note: this api don't need paging, because this only suggestion, not for list data
     *
     * @param Integer $whsId
     * @param Request $request
     * @param SkuTransformer $skuTransformer
     *
     * @return Response
     */
    public function skuListSuggestion($whsId, Request $request, SkuTransformer $skuTransformer) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        $url = "/whs/$whsId/order-skus";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'SKS',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Load SKU list for suggestion'
        ]);

        try {
            $sku = array_get($input, 'sku', '');
            if(trim($sku) === '') {
                $skus = new \Illuminate\Support\Collection();
                return $this->response->collection($skus, $skuTransformer);
            }
            //not require limit and prevent submit string
            $limit = intval(array_get($input, 'limit', ''));
            if(!$limit) {
                $limit = null;
            }
            $skus = $this->orderDtlModel->getSkuForSuggestion($whsId, $sku, $limit);
            Log::respond($request, $whsId, [
                'evt_code' => 'SKS',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'response_data' => $this->response->collection($skus, $skuTransformer)
            ]);

            return $this->response->collection($skus, $skuTransformer);

        } catch (\PDOException $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'SKS',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    /**
     * Remove cartons assigns
     *
     * @param Integer $whsId
     * @param Integer $odrDtlId
     *
     * @return Response
     */
    public function removeScannedCartons($whsId, $odrDtlId, Request $request) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        $url = "/whs/$whsId/remove-cartons-from-order";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'RCO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Remove cartons from order'
        ]);

        //Check order detail exist
        $orderDtlCol = $this->orderDtlModel->getModel()
                ->where('odr_dtl_id', $odrDtlId)
                ->where('whs_id', $whsId)
                ->first();
        if (!$orderDtlCol) {
            $data = [
                'status' => false,
                'iat' => time(),
                'message' => [
                    [
                        'status_code' => -1,
                        'msg' => "Order detail don't exist or not belong to warehouse {$whsId}!"
                    ]
                ],
                'data' => []
            ];

            return new Response($data, 404, [], null);
        }

        $sku = object_get($orderDtlCol, 'sku', '');
        //Check order detail is have cartons
        $query = $this->orderCartonModel->getModel()
                ->where('odr_dtl_id', $odrDtlId)
        ;
        $collection = $query->select([
                    'odr_ctn_id'
                ])->get();

        if (!$collection->count()) {
            $data = [
                'status' => false,
                'iat' => time(),
                'message' => [
                    [
                        'status_code' => -1,
                        'msg' => "No carton is found for SKU '{$sku}'!"
                    ]
                ],
                'data' => []
            ];

            return new Response($data, 404, [], null);
        }

        try {
            DB::beginTransaction();

            $ctnIdList = $collection->toArray();
            $update = [
                'odr_hdr_id' => null,
                'odr_dtl_id' => null,
                'odr_num' => null
            ];

            $result = $query->update($update);

            if (!$result) {
                $data = [
                    'status' => false,
                    'iat' => time(),
                    'message' => [
                        [
                            'status_code' => -1,
                            'msg' => 'Remove cartons from order FAILED!'
                        ]
                    ],
                    'data' => []
                ];

                return new Response($data, 404, [], null);
            }

            $odrDtl = $this->orderDtlModel->getModel()->find($odrDtlId);
            $odrHdrId = object_get($odrDtl, 'odr_id');
            $this->orderDtlModel->updateOrderDetailPicking($odrHdrId);
            $this->orderHdrModel->updateOrderPicking($odrHdrId);
            $wvHdrId = object_get($odrDtl, 'wv_id');
            $itemId = object_get($odrDtl, 'item_id');
            $wvHdrModel = new WavePickDtlModel();
            $updResult = $wvHdrModel->updatePickingAfterDeleteCartons($wvHdrId, $itemId);
            $updateWvSts = false;

            if ($updResult) {
                $wvHdrModel = new WavePickHdrModel();
                $wvHdrModel->updatePickingAfterDeleteCartons($wvHdrId);
                $updateWvSts = true;
            }


            if (!$updateWvSts) {

                $data = [
                    'status' => false,
                    'iat' => time(),
                    'message' => [
                        [
                            'status_code' => -1,
                            'msg' => 'Update Wave Pick Status Failed!'
                        ]
                    ],
                    'data' => []
                ];
                DB::rollback();

                return new Response($data, 200, [], null);
            }


            //update wave header status
            //$wvSts = $this->wvHdrModel->updatePickedV2($wvHdrId);


            DB::commit();

            $ctnFilter = [];
            foreach($ctnIdList as $ctn) {
                $ctnRfid = $ctn["odr_ctn_id"];
                $ctnObj = ["odr_ctn_id" => $ctnRfid];
                $ctnFilter[] = $ctnObj;
            }
            $ctnFilterData['ctns'] = $ctnFilter;
            $ctnFilterData['picked'] = [
                'wv_id'  => $wvHdrId,
                'wv_sts' => 0];
            $returnData[] = [$ctnFilterData];

            $data = [
                'status' => true,
                'iat' => time(),
                'message' => [
                    [
                        'status_code' => 1,
                        'msg' => 'Remove cartons from order successfully!'
                    ]
                ],
                'data' => $returnData,
            ];
            // WMS-4309 - Changing response wave pick is picked


            return new Response($data, 200, [], null);
        } catch (\PDOException $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'RCO',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

}
