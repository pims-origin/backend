<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class DailyInventoryReportUpdateSchedule extends AbstractCommand
{
    protected $signature   = 'daily-inventory-rpt-update';
    protected $description = 'Daily Inventory Report Updated.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {
            $this->logSuccess('start-daily-inventory-rpt-update');

            $this->process();

            $this->logSuccess('end-daily-inventory-rpt-update');

        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }
    }

    private function process()
    {
        // Get Customer Warehouse
        $customerWhss = DB::table('customer_warehouse AS cw')
            ->select('cw.cus_id', 'cw.whs_id')
            ->where("cw.deleted", 0)
            ->orderBy('whs_id')
            ->get();

        // Get timezone by warehouse
        $timezone = DB::table('whs_meta')
            ->where('whs_qualifier', 'wtz')
            ->get();

        return DB::transaction(function () use ($customerWhss, $timezone){

            if ($customerWhss) {
                foreach ($customerWhss as $cusWhs) {
                    $cusId = $cusWhs['cus_id'];
                    $whsId = $cusWhs['whs_id'];

                    // Check whsId have Exist in system
                    if ($this->checkWareHouseExistInSystem($whsId, $timezone)) {continue;}
                    
                    // Check format timezone
                    if ($this->checkFormatTimezone($whsId, $timezone)) {continue;}

                    // Set timezone
                    $dataSetDateTime = $this->setDateTime($whsId, $timezone);
                    if (!$dataSetDateTime) {continue;}
                    $now        = $dataSetDateTime['now'];
                    $invDate    = $dataSetDateTime['inv_dt'];
                    $tmrDate    = $dataSetDateTime['tmr_dt'];

                    // Delete Daily Inventory Report Previous
                    $this->deleteDailyInventoryReportPrevious($whsId, $cusId, $invDate);

                    // Insert New Daily Inventory Report
                    $this->insertDailyInventoryReport($whsId, $cusId, $now, $invDate);

                    // Update Daily Inventory Report
                    $this->updateDailyInventoryReport($whsId, $cusId, $now, $invDate, $tmrDate);
                }
            }
        });
    }

    private function checkWareHouseExistInSystem($whsId, $timezone)
    {
        if (!in_array($whsId, array_column($timezone, 'whs_id')))
        {
            return true;
        }
    }

    private function checkFormatTimezone($whsId, $timezone)
    {
        $timezoneList = timezone_identifiers_list();

        foreach ($timezone as $item) {
            if ($whsId == $item['whs_id'] && !in_array($item['whs_meta_value'], $timezoneList))
            {
                return true;
            }
        }
    }

    private function setDateTime($whsId, $timezone)
    {
        foreach ($timezone as $item) {
            if ($whsId == $item['whs_id'])
            {
                $date = new \DateTime(null, new \DateTimeZone($item['whs_meta_value']));

                $now = $date->getTimestamp();

                $curMonth = date('m', $now);
                $curYear = date('Y', $now);

                $invDate = strtotime($curYear . "-" . $curMonth . "- 1 month");
                $tmrDate = strtotime($curYear . "-" . $curMonth . "+ 1 month");
                break;
            }
        }

        return (!empty($now) && !empty($invDate)  && !empty($tmrDate)) ? [
                'now'       => $now,
                'inv_dt'    => $invDate,
                'tmr_dt'    => $tmrDate
            ] : false;
    }

    private function deleteDailyInventoryReportPrevious($whsId, $cusId, $invDate)
    {
        DB::table('daily_inv_rpt')
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->where('inv_dt', $invDate)
            ->delete();
    }

    private function insertDailyInventoryReport($whsId, $cusId, $now, $invDate)
    {
        $dailyInventoryData = [
            'whs_id'        => $whsId,
            'cus_id'        => $cusId,
            'inv_dt'        => $invDate,
            'init_ctns'     => 0,
            'init_qty'      => 0,
            'init_plt'      => 0,
            'in_ctns'       => 0,
            'in_qty'        => 0,
            'in_plt'        => 0,
            'out_ctns'      => 0,
            'out_qty'       => 0,
            'out_plt'       => 0,
            'other_ctns'    => 0,
            'other_qty'     => 0,
            'other_plt'     => 0,
            'created_at'    => $now,
            'updated_at'    => $now
        ];

        $items = DB::table('item')
            ->select(
                'item_id',
                'description',
                'sku',
                'size',
                'color',
                'pack',
                'uom_code AS uom'
            )->where('cus_id', $cusId)
            ->groupBy('item_id')
            ->get();

        foreach ($items as $item) {
            if (!empty($item))
            {
                $validateDailyInven = DB::table('daily_inv_rpt')
                    ->where(function($query) use ($dailyInventoryData, $item) {
                        $query->where('whs_id', $dailyInventoryData['whs_id']);
                        $query->where('inv_dt', $dailyInventoryData['inv_dt']);
                        $query->where('item_id', $item['item_id']);
                    })->first();

                if (empty($validateDailyInven))
                {
                    return DB::table('daily_inv_rpt')->insert(array_merge($dailyInventoryData, $item));
                }
            }
        }
    }

    private function updateDailyInventoryReport($whsId, $cusId, $now, $invDate, $tmrDate)
    {
        /*
        Update In Activity
        */
        DB::select(DB::raw("UPDATE daily_inv_rpt i
                    JOIN (
                        SELECT
                            h.whs_id,
                            d.item_id,
                            SUM( d.gr_dtl_act_ctn_ttl ) AS in_ctns,
                            SUM( d.gr_dtl_plt_ttl ) AS in_plt
                        FROM gr_dtl d
                        JOIN gr_hdr h ON h.gr_hdr_id = d.gr_hdr_id
                        WHERE h.gr_sts = 'RE'
                            AND d.gr_dtl_sts = 'RE'
                            AND d.deleted = 0
                            AND h.gr_hdr_act_dt >= $invDate AND h.gr_hdr_act_dt < $tmrDate
                        GROUP BY h.whs_id, d.item_id
                    ) t ON t.whs_id = i.whs_id
                        AND t.item_id = i.item_id
                        AND i.inv_dt = $invDate
                    SET i.in_qty = t.in_ctns * i.pack,
                        i.in_ctns = t.in_ctns,
                        i.in_plt = t.in_plt
                    WHERE i.whs_id = $whsId 
                    AND i.inv_dt = $invDate 
                    AND i.cus_id = $cusId"));

        /*
        Update Out Activity
        */
        DB::select(DB::raw("UPDATE daily_inv_rpt i
                    JOIN (
                        SELECT
                            h.whs_id,
                            d.item_id,
                            d.pack,
                            SUM( d.picked_qty ) AS out_qty 
                        FROM odr_hdr h
                        JOIN odr_dtl d ON h.odr_id = d.odr_id 
                        WHERE h.odr_sts = 'SH' 
                        AND d.deleted = 0
                        AND h.shipped_dt >= $invDate AND h.shipped_dt < $tmrDate 
                        AND d.picked_qty > 0 
                        GROUP BY
                            h.whs_id, d.item_id 
                    ) t ON t.whs_id = i.whs_id 
                        AND t.item_id = i.item_id 
                        AND i.inv_dt = $invDate 
                    SET i.out_qty = t.out_qty,
                        i.out_ctns = ROUND(t.out_qty / t.pack,0)
                    WHERE i.whs_id = $whsId 
                    AND i.inv_dt = $invDate
                    AND i.cus_id = $cusId"));

        DB::select(DB::raw("UPDATE daily_inv_rpt i
                    JOIN (
                        SELECT
                            h.whs_id,
                            d.item_id,
                            COUNT( DISTINCT ( IF( p.ctn_ttl = 0, p.plt_id , null ))) AS out_plt
                        FROM odr_hdr h
                        JOIN odr_dtl d ON h.odr_id = d.odr_id
                        JOIN odr_cartons o ON d.odr_dtl_id = o.odr_dtl_id
                        JOIN cartons c ON o.ctn_id = c.ctn_id
                        JOIN pallet p ON p.plt_id = c.plt_id 
                        WHERE h.odr_sts = 'SH' 
                        AND c.ctn_sts = 'SH'
                        AND d.deleted = 0
                        AND h.shipped_dt >= $invDate AND h.shipped_dt < $tmrDate
                        AND d.picked_qty > 0 
                        GROUP BY h.whs_id, d.item_id 
                    ) t ON t.whs_id = i.whs_id 
                        AND t.item_id = i.item_id 
                        AND i.inv_dt = $invDate 
                    SET i.out_plt = t.out_plt
                    WHERE i.whs_id = $whsId 
                    AND i.inv_dt = $invDate
                    AND i.cus_id = $cusId"));

        /*
        Update Other 
        */
        DB::select(DB::raw("UPDATE cycle_dtl d
                SET d.remain = d.pack
                WHERE d.remain = 0 OR  d.remain > d.pack"));

        DB::select(DB::raw("UPDATE daily_inv_rpt  i
                    JOIN (
                        SELECT d.whs_id, d.cus_id, d.item_id, 
                            SUM(IF(h.count_by = 'EACH', 1, d.remain) * ( CAST(d.act_qty AS SIGNED) - CAST(d.sys_qty AS SIGNED))) AS other_qty,
                            SUM(IF(d.sys_qty = 0, 1, IF(d.act_qty = 0, -1, 0))) AS other_plt
                        FROM cycle_hdr h 
                        JOIN cycle_dtl d ON h.cycle_hdr_id = d.cycle_hdr_id
                        WHERE h.cycle_sts = 'CP' 
                        AND d.sys_qty != d.act_qty
                        AND h.updated_at >= $invDate AND h.updated_at < $tmrDate
                        GROUP BY d.whs_id, d.cus_id, d.item_id
                    ) t ON t.whs_id = i.whs_id 
                        AND t.item_id = i.item_id 
                        AND i.inv_dt = $invDate 
                    SET i.other_qty = t.other_qty,
                        i.other_ctns =  ROUND(t.other_qty / i.pack, 0),
                        i.other_plt = t.other_plt
                    WHERE i.whs_id = $whsId 
                    AND i.inv_dt = $invDate 
                    AND i.cus_id = $cusId"));

        /*
        Update Current Activity
        */
        DB::select(DB::raw("UPDATE daily_inv_rpt i
                    LEFT JOIN (
                        SELECT
                            c.whs_id,
                            c.item_id,
                            c.init_ctns,
                            c.init_qty,
                            c.init_plt 
                        FROM daily_inv_rpt c 
                        WHERE c.inv_dt = $tmrDate
                    ) t ON t.whs_id = i.whs_id 
                        AND t.item_id = i.item_id 
                        AND i.inv_dt = $invDate 
                    SET i.cur_qty = IFNULL(t.init_qty, 0),
                        i.cur_ctns = ROUND(IFNULL(t.init_qty, 0) / i.pack,0),
                        i.cur_plt = IFNULL(t.init_plt, 0)
                    WHERE i.whs_id = $whsId 
                    AND i.cus_id = $cusId
                    AND i.inv_dt = $invDate"));

        /*
        Init + In - Out + Other = Current
        Init = Current - In + Out - Other
        */
        DB::select(DB::raw("UPDATE daily_inv_rpt i
                    SET i.init_qty = IFNULL(i.cur_qty - i.in_qty + i.out_qty - i.other_qty, 0),
                        i.init_ctns = IFNULL(i.cur_ctns - i.in_ctns + i.out_ctns - i.other_ctns, 0),
                        i.init_plt = IFNULL(i.cur_plt - i.in_plt + i.out_plt - i.other_plt, 0)
                    WHERE i.whs_id = $whsId 
                    AND i.cus_id = $cusId
                    AND i.inv_dt = $invDate"));
    }
}
