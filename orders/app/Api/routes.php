<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Bypass middleware when testing
$middleware = ['trimInput', 'setWarehouseTimezone'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
}

$app->get('/migrate', [
    'namespace' => 'App\Api\V1\Controllers',
    'uses'      =>
        'App\Api\V1\Controllers\MigrateController@update'
]);

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');



// Handle application Route
$api->version('v1', [], function ($api) {

    $api->group(['prefix' => 'public', 'namespace' => 'App\Api\V1\Controllers', 'middleware' => []], function ($api) {
        $api->get('/download-document', ['action' => 'editOrder', 'uses' => 'DocumentController@downloadDocument']);
    });
    require(__DIR__ . '/V2/routes.php');

    // Bypass middleware when testing
    $middleware = ['trimInput', 'setWarehouseTimezone'];
    if (env('APP_ENV') != 'testing') {
        $middleware[] = 'verifySecret';
        $middleware[] = 'authorize';
    }
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers', 'middleware' => $middleware], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V1'];
        });

        // Order
        $api->post('/orders', ['action' => 'createOrder', 'uses' => 'OrderHdrController@store']);
        $api->post('/orders/940', ['action' => 'createOrder', 'uses' => 'OrderHdrController@storeNineFourZero']);
        $api->get('/orders/{orderId:[0-9]+}', ['action' => 'viewOrder', 'uses' => 'OrderHdrController@show']);
        $api->get('/orders/shipping/{orderId:[0-9]+}', ['action' => 'viewOrder', 'uses' => 'OrderHdrController@getShippingInfo']);
        $api->put('/orders/{orderId:[0-9]+}', ['action' => 'editOrder', 'uses' => 'OrderHdrController@update']);
        $api->get('/orders', ['action' => 'viewOrder', 'uses' => 'OrderHdrController@search']);
        $api->get('/orders/print-pdf/{orderId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OrderHdrController@printPdf']);
        $api->put('/orders/track-num/{orderId:[0-9]+}', ['action' => 'editOrder', 'uses' => 'OrderHdrController@updateTrackNum']);

        $api->get('/orders/{orderId:[0-9]+}/order-picking-slip/print', [
            'action' => 'viewOrder',
            'uses'   => 'OrderHdrController@printOrderPickingSlip'
        ]);

        $api->get('/orders/{orderId:[0-9]+}/order-packing-slip/print', [
            'action' => 'viewOrder',
            'uses'   => 'OrderHdrController@printOrderPackingSlip'
        ]);

        $api->get('/orders/{orderId:[0-9]+}/order-request-slip/print', [
            'action' => 'viewOrder',
            'uses'   => 'OrderHdrController@printOrderRequestSlip'
        ]);

        // Order Matrix Kitting
        $api->post('/orders/odr-matrix-kit', ['action' => 'createOrder', 'uses' => 'OrderMatrixController@OrderMatrixKit']);


        $api->get('/orders/cartons-not-assign',
            ['action' => 'viewOrder', 'uses' => 'OrderCartonController@listCartonNotAssignByOrder']);

        //Cancel
        $api->get('/orders/cancel', ['action' => 'cancelOrder', 'uses' => 'OrderHdrController@cancel']);
        $api->post('/orders/cancel-order-details',
            ['action' => 'cancelOrder', 'uses' => 'OrderHdrController@cancelOrderDetails']);

        $api->get('/orders/{orderId:[0-9]+}/cartons',
            ['action' => 'viewOrder', 'uses' => 'OrderHdrController@listCartonByOrder']);

        // Update Tracking number in Packed Cartons tab
        $api->put('/orders/{orderId:[0-9]+}/tracking-num',
            ['action' => 'editOrder', 'uses' => 'OrderCartonController@updateTrackingNumPackedCtns']);

        // Update Order Ship Date
        $api->put('/orders/{orderId:[0-9]+}/update-shipdt', [
            'action' => 'editOrderShipDate',
            'uses'   =>
                'OrderHdrController@updateShipDate'
        ]);

        // Import Order
        $api->post('/orders/validate-import',
            ['action' => 'importOnlineOrder', 'uses' => 'OrderHdrController@validateImport']);
        $api->get('/orders/process-import/{cusId}',
            ['action' => 'importOnlineOrder', 'uses' => 'OrderHdrController@processImport']);
        $api->get('/orders/get-file-error', [
            'action' => 'viewOrder',
            function () {
                $hashKey = md5(getallheaders()['Authorization']);
                $file_type = !empty($_REQUEST['file_type']) ? $_REQUEST['file_type'] : "xlsx";
                $file = storage_path("Errors/$hashKey.$file_type");

                return response()->download($file);
            }
        ]);
        $api->get('/orders/dashboard-orders',
            ['action' => 'viewOrder', 'uses' => 'OrderHdrController@dashboardOdr']);
        $api->get('/orders/dashboard-inprocess',
            ['action' => 'viewOrder', 'uses' => 'OrderHdrController@dashboardInprocess']);
        $api->get('/orders/revert/{odrId:[0-9]+}', ['action' => 'editOrder', 'uses' => 'OrderHdrController@revert']);

        // Order Type
        $api->get('/order-types', ['action' => 'viewOrder', 'uses' => 'OrderTypeController@show']);

        // Order Status
        $api->get('/order-statuses', ['action' => 'viewOrder', 'uses' => 'OrderStatusController@show']);

        // Order Shipping
        $api->post('/order-shippings', ['action' => 'orderShipping', 'uses' => 'OrderShippingController@store']);
        $api->get('/order-shippings/{orderShippingId:[0-9]+}',
            ['action' => 'orderShipping', 'uses' => 'OrderShippingController@show']);
        $api->put('/order-shippings/{orderShippingId:[0-9]+}',
            ['action' => 'orderShipping', 'uses' => 'OrderShippingController@update']);
        $api->delete('/order-shippings/{orderShippingId:[0-9]+}',
            ['action' => 'orderShipping', 'uses' => 'OrderShippingController@destroy']);
        $api->get('/order-shippings', ['action' => 'viewOrder', 'uses' => 'OrderShippingController@search']);
        $api->get('/order-shippings/shipped', ['action' => 'orderShipping', 'uses' => 'OrderShippingController@ship']);

        // CSR
        $api->put('/assign-csr', ['action' => 'assignCSR', 'uses' => 'OrderHdrController@updateCsr']);
        $api->post('/assign-multi-csr/{oderId:[0-9]+}', ['action' => 'assignCSR', 'uses' => 'OrderHdrController@storeMultiCSR']);
        $api->get('/assign-multi-csr/{oderId:[0-9]+}', ['action' => 'assignCSR', 'uses' => 'OrderHdrController@getListMultiCSR']);
        //$api->get('/assign-multi-csr/detail/{oderCSRId:[0-9]+}', ['action' => 'assignCSR', 'uses' =>
        // 'OrderHdrController@showMultiCSR']);

        //Allocate
        $api->put('/allocates/{orderHdrId:[0-9]+}',
            ['action' => 'allocateOrder', 'uses' => 'InventorySummaryController@updateAllocate']);

        // Inventor Summary
        $api->get('/get-lot/{itemId:[0-9]+}', ['action' => 'viewOrder', 'uses' => 'InventorySummaryController@getLot']);
        $api->get('/invt-smr', ['action' => 'viewOrder', 'uses' => 'InventorySummaryController@searchItem']);

        // Xdoc
        $api->put('/xdoc-allocates/{orderHdrId:[0-9]+}',
            ['action' => 'allocateOrder', 'uses' => 'InventorySummaryController@xdocAllocate']);
        $api->get('/invt-smr/xdoc', ['action' => 'viewOrder', 'uses' => 'InventorySummaryController@searchXdocItem']);

        // Load SKU list by CusID and OdrID for work order

        //work orders
        $api->post('/work-orders', ['action' => 'createWorkOrder', 'uses' => 'WorkOrderController@store']);
        $api->put('/work-orders', ['action' => 'editWorkOrder', 'uses' => 'WorkOrderController@update']);
        $api->get('/work-orders/getChargeCode/{customerId:[0-9]+}',
            ['action' => 'viewWorkOrder', 'uses' => 'WorkOrderController@getChargeCode']);
        $api->get('/work-orders/getOrderInfoSku/{cusId:[0-9]+}/{odrNum?}',
            ['action' => 'viewWorkOrder', 'uses' => 'WorkOrderController@getOrderInfoSku']);
        $api->get('/work-orders/workOrderList',
            ['action' => 'viewWorkOrder', 'uses' => 'WorkOrderController@workOrderList']);
        $api->get('/work-orders/{workOrderId:[0-9]+}',
            ['action' => 'viewWorkOrder', 'uses' => 'WorkOrderController@show']);
        $api->get('/work-orders/{workOrderId:[0-9]+}/print',
            ['action' => "viewWorkOrder", 'uses' => 'WorkOrderController@printWorkOrder']);
        $api->put('/work-orders/changestatus/{workOrderId:[0-9]+}', [
            'action' => 'editWorkOrder',
            'uses'   => 'WorkOrderController@ChangeStatus'
        ]);

        //work orders new sku
        $api->get('/work-orders/new-sku/auto-sku/{cusId:[0-9]+}/{odrNum}',
            ['action' => 'viewWorkOrder', 'uses' => 'WorkOrderController@getLikeSkuByOrderNum']);
        $api->get('/work-orders/new-sku/{workOrderId:[0-9]+}/{cusId:[0-9]+}/{odrNum}', ['action' => 'viewWorkOrder', 'uses' => 'WorkOrderController@listNewSku']);
        $api->post('/work-orders/new-sku', ['action' => 'createWorkOrder', 'uses' => 'WorkOrderController@storeNewSku']);
        $api->put('/work-orders/new-sku/{workOrderSkuId:[0-9]+}', ['action' => 'editWorkOrder', 'uses' => 'WorkOrderController@updateNewSku']);

        //get carrier info
        $api->get('/carrier/commodityList', ['action' => 'viewBOL', 'uses' => 'BOLController@getCommodityList']);
        $api->get('/carrier/show-status-drop-down',
            ['action' => 'viewBOL', 'uses' => 'BillOfLandingController@showStatusOrDropDown']);

        // Bol CRUD
        $api->post('/bol', ['action' => 'createBOL', 'uses' => 'BOLController@store']);
        $api->put('/bol/{shipId:[0-9]+}', ['action' => 'editBOL', 'uses' => 'BOLController@update']);

        // Print PDF
        $api->get('/bol/{shipId:[0-9]+}/print', ['action' => 'viewBOL', 'uses' => 'BOLController@printBOL']);

        // Bol Get Address
        $api->get('/bol/address', ['action' => 'viewBOL', 'uses' => 'BOLController@getAddressV2']);
        //get bol list
        $api->get('/bol-list', [
            'action' => 'viewBOL',
            'uses'   =>
                'BOLController@getBOLList'
        ]);
        //BOL order summary
        $api->get('/bol-orders/{cusId:[0-9]+}/{odrIds:[0-9,]+}',
            ['action' => 'viewBOL', 'uses' => 'OrderHdrController@bolOrders']);
        $api->get('/bol-orders/{cusId:[0-9]+}/{odrId:[0-9,]+}/carrier',
            ['action' => 'viewBOL', 'uses' => 'OrderHdrController@getCarrier']);
        // Get OdrHdr same customer for Add BOL
        $api->get('/bol-orders-same-customer/{cusId:[0-9]+}/{odrIds:[0-9,]+}',
            ['action' => 'viewBOL', 'uses' => 'OrderHdrController@bolOrdersSameCustomer']);
        //BOL view
        $api->get('/bol/{shipId:[0-9]+}', ['action' => 'viewBOL', 'uses' => 'OrderHdrController@bolView']);
        //BOL order for update
        $api->get('/orders-by-bol/{cusId:[0-9]+}/{bolId:[0-9]+}',
            ['action' => 'viewBOL', 'uses' => 'OrderHdrController@getOrderByBOL']);

        //Delivery Service
        $api->get('/delivery-service',
            ['action' => 'viewDeliveryService', 'uses' => 'DeliveryServiceController@getList']);
        $api->post('/delivery-service',
            ['action' => 'createDeliveryService', 'uses' => 'DeliveryServiceController@store']);
        $api->put('/delivery-service/{dnId:[0-9]+}',
            ['action' => 'editDeliveryService', 'uses' => 'DeliveryServiceController@update']);

        //Cancel order list
        $api->get('/orders/order-cancel-lists/{orderId:[0-9]+}', [
            'action' => 'cancelOrder',
            'uses'   => 'OrderHdrController@orderCancelList'
        ]);
        $api->get('/orders/canceled-orders-search', [
            'action' => 'cancelOrder',
            'uses'   => 'OrderCartonController@CanceledOrdersSearch'
        ]);

        //order flow
        $api->get('/orders/order-flows/{customerId:[0-9]+}', [
            'action' => "viewOrder",
            'uses'   => 'OrderHdrMetaController@getOrderFlow'
        ]);
        $api->put('/orders/order-flows/{orderId:[0-9]+}', [
            'action' => "editOrder",
            'uses'   => 'OrderHdrMetaController@update'
        ]);
        $api->post('/orders/order-flows/{orderId:[0-9]+}', [
            'action' => "createOrder",
            'uses'   => 'OrderHdrMetaController@store'
        ]);

        // Return Order
        $api->post('/return', ['action' => 'createPutbackReceipt', 'uses' => 'ReturnHdrController@store']);
        $api->get('/putback/pal-assign/suggest-lpn', [
            'action' => 'assignPallet',
            'uses'   => 'ReturnHdrController@suggestLpn'
        ]);

        $api->post('/putback/pal-assign/{returnId:[0-9]+}', [
            'action' => 'assignPalletPutback',
            'uses'   => 'ReturnHdrController@assignPallet'
        ]);

        $api->get('/putback/{returnId:[0-9]+}/print', [
            'action' => 'assignPalletPutback',
            'uses'   => 'ReturnHdrController@printPutBack'
        ]);

        $api->put('/putback/{returnId:[0-9]+}', [
            'action' => 'updatePutback',
            'uses'   => 'ReturnHdrController@updatePutBack'
        ]);
        $api->get('/putback/{returnId:[0-9]+}', [
            'action' => 'updatePutback',
            'uses'   => 'ReturnOrderController@getUpdatePutBack'
        ]);

        $api->get('/putback/{returnId:[0-9]+}/get-items', [
            'action' => 'assignPalletPutback',
            'uses'   => 'ReturnHdrController@getItem'
        ]);

        $api->get('/orders/return-orders', [
            'action' => 'cancelOrder',
            'uses'   => 'ReturnOrderController@ReturnOrders'
        ]);

        $api->get('/orders/return-order-details/{returnId:[0-9]+}', [
            'action' => 'cancelOrder',
            'uses'   => 'ReturnOrderController@ReturnOrdersDetail'
        ]);

        $api->put('/putback/assign-putter', [
            'action' => "updatePutback",
            'uses'   => 'ReturnHdrController@updatePutter'
        ]);

        $api->get('/orders/print-wap-lpn/{orderId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OrderHdrController@printWapLPN']);

        //Print LPNs BOL
        $api->get('/orders/print-bol-lpn/{shipId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OrderHdrController@printBolLPN']);

        //Print Pallets Shipping Label of one Order
        $api->get('/orders/print-pallet-shipping/{orderId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OrderShippingController@printPalletShipping']);
        //Print Cartons Shipping Label of one Order
        $api->get('/orders/print-carton-shipping/{orderId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OrderShippingController@printCartonShipping']);

        $api->get('/itemPackSizes/{itemId:[0-9]+}',
            ['action' => 'viewItem', 'uses' => 'InventorySummaryController@listPackSize']);

        // Put Back Report
        $api->get('/put-back/reports', ['action' => 'updatePutback', 'uses' => 'ReturnDtlController@search']);

        // OutPallet
        $api->get('{whsId:[0-9]+}/out-pallet/{odrHdrId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OutPalletController@search']);
        $api->get('{whsId:[0-9]+}/out-pallet/auto-location',
            ['action' => 'viewOrder', 'uses' => 'OutPalletController@autoLocation']);
        $api->put('{whsId:[0-9]+}/out-pallet/edit',
            ['action' => 'viewOrder', 'uses' => 'OutPalletController@editOutPallet']);

        $api->get('{whsId:[0-9]+}/out-pallet/get-by-order/{odrHdrId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OutPalletController@getOutPalletByOrder']);
        $api->put('{whsId:[0-9]+}/out-pallet/{pltId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OutPalletController@updateOutPalletByOrder']);
        $api->delete('{whsId:[0-9]+}/out-pallet/{pltId:[0-9]+}',
            ['action' => 'viewOrder', 'uses' => 'OutPalletController@deleteOutPallet']);

        $api->get('/orders/complete-order-picking/{orderId:[0-9]+}',
            ['action' => 'updateOrderPicking', 'uses' => 'OrderHdrController@completeOrderPicking']);

        $api->put('/orders/edit-schedule-ship-and-routed-date/{orderId:[0-9]+}', ['action' => 'editOrder', 'uses' => 'OrderHdrController@editScheduleShipAndRoutedDate']);

        //Updating comments for a order
        $api->put('/orders/update-comments/{orderId:[0-9]+}', ['action' => 'editOrder', 'uses' => 'OrderHdrController@updateOrderComments']);

        //Updating comments for many orders at the same time
        $api->put('/orders/update-comments-many-orders', ['action' => 'editOrder', 'uses' => 'OrderHdrController@updateCommentsForManyOrders']);

        $api->post('/integrate-document-to-dms/{odrNum}', ['action' => 'editOrder', 'uses' => 'OrderHdrController@integrateFileToDMS']);

        //get autocomplete carrier name
        $api->get('/carrier/autocomplete', ['action' => 'editOrder', 'uses' => 'OrderHdrController@autocompleteCarrier']);

        // Assign Driver
        $api->post('/drivers/assign', ['action' => 'viewOrder', 'uses' => 'DriverController@store']);

        // transfer warehouse
        $api->group(['prefix' => 'transfer-warehouse'], function ($api) {

            $api->post('orders', [
                'action' => 'createOrder',
                'uses'   => 'TransferWarehouseController@store'
            ]);

            $api->get('{whsId:[0-9]+}', [
                'action' => 'createOrder',
                'uses'   => 'TransferWarehouseController@warehouseAddress'
            ]);

            $api->put('orders/{orderId:[0-9]+}', [
                'action' => 'createOrder',
                'uses'   => 'TransferWarehouseController@update'
            ]);

            $api->put('orders/{orderId:[0-9]+}/allocate', [
                'action' => 'createOrder',
                'uses'   => 'TransferWarehouseController@allocate'
            ]);

            $api->put('orders/{orderId:[0-9]+}/cancel', [
                'action' => 'createOrder',
                'uses'   => 'TransferWarehouseController@cancel'
            ]);

        });

        $api->get('sync-asn/{odrId:[0-9]+}', [
            'action' => 'viewOrder',
            'uses'   => 'SyncController@syncASN'
        ]);

        //Print Invoice on Order list
        $api->get('/invoice/{orderId:[0-9]+}/print', ['action' => 'viewInvoice', 'uses' => 'BOLController@printInvoice']);
        
    });
});
