<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Utils\Status;

class AsnListVirtualCartonTransformer extends TransformerAbstract
{

    public function transform(VirtualCarton $virtualCarton)
    {
        return [
            "vtl_ctn_id"       => $virtualCarton->vtl_ctn_id,
            "ctn_rfid"         => $virtualCarton->ctn_rfid,
            "is_damaged"        => $virtualCarton->is_damaged,
            "vtl_ctn_sts"      => $virtualCarton->vtl_ctn_sts,
            "vtl_ctn_sts_name" => Status::getByKey("VIRTUAL-CARTON-STATUS", $virtualCarton->vtl_ctn_sts)
        ];
    }
}
