<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Models;

class LocationTypeModel extends AbstractModel
{
    /**
     * @param int $locationTypeId
     *
     * @return int
     */
    public function deleteLocationType($locationTypeId)
    {
        return $this->model
            ->where('loc_type_id', $locationTypeId)
            ->delete();
    }

    public function deleteMassLocationType(array $locationTypeIds)
    {
        return $this->model
            ->whereIn('loc_type_id', $locationTypeIds)
            ->delete();
    }

    /**
     * Search LocationType
     *
     * @param array $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $keys = [
            'like' => [
                'loc_type_name', 'loc_type_desc', 'loc_type_code'
            ],
            'equal' => ['loc_type_id']
        ];

        return $this->_search($attributes, $with, $limit, $keys);
    }
}