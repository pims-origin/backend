<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Listeners;

use Illuminate\Support\Facades\Log;

/**
 * Description of DBListener
 *
 * @author daoth
 */
class DBListener
{

    public function boot()
    {
        //
    }

    public function register()
    {
        \DB::listen(function($sql) {
            $query = $sql->sql;
            $bindings = $sql->bindings;
            $mySql = str_replace(array('%', '?'), array('%%', '%s'), $query);
            $myQuery = vsprintf($mySql, $bindings);
            //Log::info($myQuery);
        });
    }

}
