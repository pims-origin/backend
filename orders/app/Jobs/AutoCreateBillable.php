<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class AutoCreateBillable extends Job
{

    protected $whsId;
    protected $cusId;
    protected $returnId;
    protected $request;
    /**
     * AutoPackJob constructor.
     */
    public function __construct($whsId, $cusId, $returnId, $request)
    {
        $this->whsId    = $whsId;
        $this->cusId    = $cusId;
        $this->returnId = $returnId;
        $this->request  = $request;
    }

    public function handle()
    {
        try {
            $client = new Client();
            $client->request('POST', env('API_INVOICE_MASTER') . '/v1/billableitem/put-back',
                [
                    'headers'     => ['Authorization' => $this->request->getHeader('Authorization')],
                    'form_params' => [
                        'whs_id'    => $this->whsId,
                        'cus_id'    => $this->cusId,
                        'return_id' => $this->returnId,
                    ],
                ]
            );
            Log::info('The Job is processing.');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

    }
}
