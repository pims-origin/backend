<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V2\PackDetail\ManualPack\Validators;

use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderHdrModel;
use Dingo\Api\Exception\UnknownVersionException;
use Seldat\Wms2\Utils\Message;

/**
 * Class PackHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class PackHdrAutoValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            '*.odr_dtl_id' => 'required|integer|exists:odr_dtl,odr_dtl_id',
            '*.itm_id'      => 'required|integer|exists:item,item_id',
        ];
    }


    public function checkOdrHdrExit($odrHdrId) {
        $odrHdrModel = new OrderHdrModel();
        if(!$odrHdrModel->checkWhere(['odr_id' => $odrHdrId])) {
            throw new UnknownVersionException(Message::get("BM017", "Order " . $odrHdrId));
        }
    }

    public function checkCanAutoPack(array $odrDtlId) {
        $orderCarton = new OrderCartonModel();
        if(!$orderCarton->getCartonByOdrDtl($odrDtlId)) {
            throw new UnknownVersionException("Can't auto pack. Some cartons was not picked full!");
        }
    }


}
