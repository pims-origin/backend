<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\CartonSatusesService;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class CartonSatusesController extends AbstractController
{
    protected $service;

    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->service = new CartonSatusesService();
    }

    public function getCartonSatuses()
    {
        try {
            return $this->service->getCartonSatuses();
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
