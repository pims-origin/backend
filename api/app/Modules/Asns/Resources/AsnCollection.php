<?php

namespace App\Modules\Asns\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AsnCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => AsnResource::collection($this->collection)
        ];
    }
}