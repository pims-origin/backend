<?php

namespace Seldat\Wms2\Models;

class BOLCarrierDetail extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bol_carrier_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ca_dtl_id';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'ship_id',
        'hdl_type',
        'hdl_qty',
        'pkg_type',
        'pkg_qty',
        'weight',
        'hm',
        'cmd_id',
        'cmd_des',
        'nmfc_num',
        'cls_num',
    ];

    public function shipment()
    {
        return $this->hasOne(__NAMESPACE__ . '\Shipment', 'ship_id', 'ship_id');
    }
}
