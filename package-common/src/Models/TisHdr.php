<?php

namespace Seldat\Wms2\Models;


class TisHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tis_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'tis_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'tis_hdr_num',
        'whs_id',
        'tis_sts',
        'sts',
    ];

    public function details()
    {
        return $this->hasMany(__NAMESPACE__ . '\TisDtl', 'tis_hdr_id', 'tis_hdr_id');
    }
}
