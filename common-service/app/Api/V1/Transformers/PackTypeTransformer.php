<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\PackType;
use League\Fractal\TransformerAbstract;

class PackTypeTransformer extends TransformerAbstract
{
    public function transform(PackType $packType)
    {
        return [
            'pack_code' => $packType->pack_code,
            'pack_name' => $packType->pack_name,
        ];
    }
}
