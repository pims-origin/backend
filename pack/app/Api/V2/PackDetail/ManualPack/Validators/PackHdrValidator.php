<?php

namespace App\Api\V2\PackDetail\ManualPack\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class PackHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class PackHdrValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'odr_hdr_id'          => 'required|exists:odr_hdr,odr_id',
            'number_of_pack'      => 'integer',
            'pack_ref_id'         => 'required|exists:pack_ref,pack_ref_id',
            'items'               => 'required|array',
            'items.*.pack_dtl_id' => 'integer|exists:pack_dtl,pack_dtl_id',
            'items.*.itm_id'      => 'required|integer|exists:item,item_id',
            'items.*.piece_qty'   => 'integer',
        ];
    }


}
