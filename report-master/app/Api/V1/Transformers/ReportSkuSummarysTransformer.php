<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\ReportSkuSummarys;
use App\Api\V1\Models\ReportConfigsModel;

class ReportSkuSummarysTransformer extends TransformerAbstract
{

    /**
     * @param ReportSkuSummarys $reportSKUSummary
     *
     * @return array
     */
    public function transform(ReportSkuSummarys $reportSKUSummary)
    {
        return [
            'id'          => array_get($reportSKUSummary, 'id'),
            'whs_code'    => array_get($reportSKUSummary, 'whs_code', null),
            'cus_code'    => array_get($reportSKUSummary, 'cus_code', null),
            'upc'         => array_get($reportSKUSummary, 'upc', null),
            'des'         => array_get($reportSKUSummary, 'des', null),
            'sku'         => array_get($reportSKUSummary, 'sku', null),
            'size'        => array_get($reportSKUSummary, 'size', null),
            'color'       => array_get($reportSKUSummary, 'color', null),
            'pack'        => array_get($reportSKUSummary, 'pack', null),
            'uom'         => array_get($reportSKUSummary, 'uom', null),
            'in_hand_qty' => array_get($reportSKUSummary, 'in_hand_qty', null),
            'in_pick_qty' => array_get($reportSKUSummary, 'in_pick_qty', null),
            'total_qty'   => array_get($reportSKUSummary, 'total_qty', null),
            'created_at'  => date("m/d/Y", strtotime(array_get($reportSKUSummary, 'created_at'))),
            'updated_at'  => date("m/d/Y", strtotime(array_get($reportSKUSummary, 'updated_at'))),
        ];
    }
}
