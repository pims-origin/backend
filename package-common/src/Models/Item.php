<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace Seldat\Wms2\Models;

use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Item extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'item';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_id';

    /**
     * @var array
     */
    protected $fillable = [
        'item_code',
        'description',
        'lot',
        'upc',
        'sku',
        'size',
        'color',
        'uom_id',
        'uom_name',
        'uom_code',
        'pack',
        'length',
        'width',
        'height',
        'weight',
        'volume',
        'cube',
        'cus_id',
        'cus_upc',
        'status',
        'condition',
        'created_by',
        'updated_by',
        'children_checksum',
        'is_new_sku',
		'spc_hdl_code',
		'level_id',
        'charge_by',
		'net_weight',
        'inner_pack'
    ];

    public function itemStatus()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ItemStatus', 'status', 'item_sts_code');
    }

    public function systemUom()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemUom', 'uom_id', 'sys_uom_id');
    }

    public function carton()
    {
        return $this->hasMany(__NAMESPACE__ . '\Carton', 'item_id', 'item_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function inventorySummary()
    {
        return $this->hasMany(__NAMESPACE__ . '\InventorySummary', 'item_id', 'item_id');
    }

    public function cycleDtl()
    {
        return $this->hasMany(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function itemHier()
    {
        return $this->hasMany(__NAMESPACE__ . '\ItemHier', 'item_id', 'item_id');
    }

    //------------------------------Utils functions-----------------------------------
	public static function generateUpc128($cusId, $itemId)
    {
        return str_pad($cusId, 5, '0', STR_PAD_LEFT) . str_pad($itemId, 8, '0', STR_PAD_LEFT);
    }

	public static function generateItem($sku, $size, $color, $pack, $cusId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $item = self::where([
            'sku'    => $sku,
            'size'   => $size,
            'color'  => $color,
            'cus_id' => $cusId,
            'pack'   => $pack
        ])->first();

        if (empty($item)) {
            $tmpItem = self::where([
                'sku'    => $sku,
                'size'   => $size,
                'color'  => $color,
                'cus_id' => $cusId,
            ])->first();

            if (empty($item)) {
                throw new HttpException(403, "The SKU $sku is invalid");
            }

            $item = $tmpItem->replicate();
            $item->pack = $pack;

            $item->push();
        }

        return $item;
    }

    public static function generateItemCode($cusId, $number = 1)
    {
        return null;

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $cusCode = Customer::where('cus_id', $cusId)
            ->value('cus_code');

        if (empty($cusCode)) {
            throw new HttpException(403, 'This customer has customer_code invalid');
        }

        $key = 'I' . $cusCode;

        $seq = DB::table('seq')
            ->where([
                'tbl_name' => 'item',
                'key'      => $key
            ])
            ->value('seq');

        if (empty($seq)) {
            $seq = $key . '-000001';
            //update
            DB::table('seq')
                ->insert([
                    'tbl_name' => 'item',
                    'key'      => $key,
                    'seq'      => $seq
                ]);
        } else {
            $seq++;
            //update
            DB::table('seq')
                ->where([
                    'tbl_name' => 'item',
                    'key'      => $key
                ])
                ->update(['seq' => $seq]);
        }

        return $seq;
    }
}
