<?php

namespace Seldat\Wms2\Models;

/**
 * Class ShippingOrder
 *
 * @property mixed whs_id
 * @property mixed cus_id
 * @property mixed ref_num
 * @property mixed po_total
 * @property mixed so_sts
 * @property mixed type
 *
 * @package Seldat\Wms2\Models
 */
class ShippingOrder extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shipping_odr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'so_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'cus_odr_num',
        'po_total',
        'so_sts',
        'type',
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }
}
