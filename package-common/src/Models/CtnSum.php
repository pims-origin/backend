<?php

namespace Seldat\Wms2\Models;

class CtnSum extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ctn_sum';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'carton_id';

    /**
     * @var array
     */
    protected $fillable = [
        'gr_dtl_id',
        'whs_id',
        'cus_id',
        'rcv_dt',
        'last_ctn_event_id',
        'last_active',
        'vol',
        'uom'
    ];

    
    public function goodReceiptDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptDetail', 'gr_dtl_id', 'gr_dtl_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
    
    public function eventTracking()
    {
        return $this->belongsTo(__NAMESPACE__ . '\EventTracking', 'last_ctn_event_id', 'id');
    }
}
