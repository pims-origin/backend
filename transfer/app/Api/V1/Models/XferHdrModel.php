<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\XferHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class XferHdrModel extends AbstractModel
{

    /**
     * XferHdrModel constructor.
     *
     * @param XferHdr|null $model
     */
    public function __construct(XferHdr $model = null)
    {
        $this->model = ($model) ?: new XferHdr();
    }

    /**
     * @return mixed
     */
    public function getLatestWvTransferNumber()
    {
        return $this->model->orderBy('xfer_hdr_num', 'desc')->select('xfer_hdr_num')->first();
    }

    /**
     * @param $xferHdrId
     *
     * @return mixed
     */
    public function getXferHdrById($xferHdrId)
    {
        return $this->model->where('xfer_hdr_id', $xferHdrId)->first();
    }

    /**
     * @return string
     */
    public function generateWvTfNum()
    {
        $currentYearMonth = date('ym');
        $defaultWvTfNum = "XFE-${currentYearMonth}-00001";

        $lastWvTf = $this->getLatestWvTransferNumber();
        $oldLastWvTfNum = $lastWvTf['xfer_hdr_num'];

        if (empty($oldLastWvTfNum) || strpos($oldLastWvTfNum, "-${currentYearMonth}-") === false) {
            return $defaultWvTfNum;
        }

        $lastWvTfNum = "XFE" . substr($lastWvTf['xfer_hdr_num'], 3, strlen($lastWvTf['xfer_hdr_num']));

        return ++$lastWvTfNum;
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveXferHdr($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $data
     * @param $id
     *
     * @return mixed
     */
    public function editXferHdr($data, $id)
    {
        return $this->model->where('xfer_hdr_id', $id)->update($data);
    }

    /**
     * @param $item_id
     *
     * @return mixed
     */
    public function checkXferAlreadyHasItem($item_id, $cusId)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return $this->model
            ->where([
                'item_id' => $item_id,
                'whs_id'  => $currentWH,
                'cus_id'  => $cusId
            ])
            ->where('xfer_sts', '<>', Status::getByValue("Completed", "XFER-STATUS"))
            ->first();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateXferHdr($data)
    {
        return $this->updateWhere([
            'xfer_ticket_id' => $data['xfer_ticket_id'],
            'xfer_sts'       => Status::getByValue("Picking", "XFER-STATUS"),
            'sts'            => 'u'
        ], [
            'xfer_hdr_id' => $data['xfer_hdr_id']
        ]);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function loadWvTfInfo($id)
    {
        return DB::table('xfer_hdr')
            ->leftJoin('customer', 'customer.cus_id', '=', 'xfer_hdr.cus_id')
            ->where("xfer_hdr.xfer_hdr_id", $id)
            ->select(
                'customer.cus_name',
                'xfer_hdr.*'
            )
            ->first();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchAllTicket($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['sku', 'cus_id', 'cus_upc'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $query->whereNotNull('xfer_ticket_id');
        $query->where('xfer_sts', '<>', Status::getByKey("XFER-STATUS", "Pending"));
        $query->where('whs_id', $currentWH);

        $wv_tk_sts = array_get($attributes, 'xfer_ticket_sts', '');
        $wv_tk_num = array_get($attributes, 'xfer_ticket_num', '');

        $query->whereHas("xferTicket", function ($query) use ($wv_tk_sts, $wv_tk_num) {
            $query->where('xfer_ticket_sts', 'like', "%" . SelStr::escapeLike($wv_tk_sts) . "%");
            $query->where('xfer_ticket_num', 'like', "%" . SelStr::escapeLike($wv_tk_num) . "%");
        });

        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchAllXferHdr($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $currentUser = array_get($userInfo, 'user_id', 0);

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['sku', 'item_id', 'xfer_hdr_num', 'cus_upc', 'xfer_sts', 'cus_id'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        $query->where('whs_id', $currentWH);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getALLXferHdrByTicketId($id)
    {
        return $this->model->where("xfer_ticket_id", $id)->get();
    }
}
