<?php

namespace App\Api\V5\Inbound\Pallet\ScanPallet\Validators;


class LocationPalletListValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'wv_dtl_id' => 'required'
        ];
    }
}
