<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\OrderFlowMaster;
use League\Fractal\TransformerAbstract;

class OrderFlowMasterTransformer extends TransformerAbstract
{
    public function transform(OrderFlowMaster $orderFlowMaster)
    {
        return [
            'odr_flow_id' => $orderFlowMaster->odr_flow_id,
            'step'        => $orderFlowMaster->step,
            'flow_code'   => $orderFlowMaster->flow_code,
            'odr_sts'     => $orderFlowMaster->odr_sts,
            'name'        => $orderFlowMaster->name,
            'description' => $orderFlowMaster->description,
            'dependency'  => $orderFlowMaster->dependency,
            'type'        => $orderFlowMaster->type
        ];
    }
}