<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;

class GoodsReceiptTransformer extends TransformerAbstract
{
    public function transform(GoodsReceiptDetail $goodsReceiptDtl)
    {
        $packSize = object_get($goodsReceiptDtl, 'pack', 0);

        return [
            //Gr hdr
            'cus_id'         => object_get($goodsReceiptDtl, 'goodsReceipt.cus_id', null),
            'cus_name'       => object_get($goodsReceiptDtl, 'goodsReceipt.customer.cus_name', null),
            'cus_code'       => object_get($goodsReceiptDtl, 'goodsReceipt.customer.cus_code', null),
            'gr_hdr_num'     => object_get($goodsReceiptDtl, 'goodsReceipt.gr_hdr_num', null),
            'ref_code'       => object_get($goodsReceiptDtl, 'goodsReceipt.ref_code', null),
            //Gr Detail
            'gr_hdr_id'      => object_get($goodsReceiptDtl, 'gr_hdr_id', null),
            'ctnr_id'        => object_get($goodsReceiptDtl, 'ctnr_id', null),
            'ctnr_num'       => object_get($goodsReceiptDtl, 'ctnr_num', null),
            'updated_at'     => $this->dateFormat(object_get($goodsReceiptDtl, 'gr_hdr_act_dt', '')),
            'gr_hdr_act_dt'     => $this->dateFormat(object_get($goodsReceiptDtl, 'gr_hdr_act_dt', '')),
            'created_by'     => trim(object_get($goodsReceiptDtl, "goodsReceipt.user.first_name", null) . " " .
                object_get($goodsReceiptDtl, "goodsReceipt.user.last_name", null)),
            'updated_by'     => trim(object_get($goodsReceiptDtl, "goodsReceipt.updatedBy.first_name", null) . " " .
                object_get($goodsReceiptDtl, "goodsReceipt.updatedBy.last_name", null)),
            'created_at'     => date("m/d/Y", strtotime(object_get($goodsReceiptDtl, 'created_at', null))),
            'item_id'        => object_get($goodsReceiptDtl, 'item_id', null),
            'sku'            => object_get($goodsReceiptDtl, 'sku', null),
            'size'           => object_get($goodsReceiptDtl, 'size', null),
            'color'          => object_get($goodsReceiptDtl, 'color', null),
            'lot'            => object_get($goodsReceiptDtl, 'lot', null),
            'upc'            => object_get($goodsReceiptDtl, 'upc', null),
            'gr_qty'         => (object_get($goodsReceiptDtl, 'gr_dtl_act_ctn_ttl', null) * object_get($goodsReceiptDtl,
                    'pack', null)),
            'crs_doc_qty'    => object_get($goodsReceiptDtl, 'crs_doc', 0) * $packSize,
            'pack_size'      => $packSize,
            'gr_dtl_dmg_qty' => object_get($goodsReceiptDtl, 'gr_dtl_dmg_ttl', 0) * $packSize,
            'gr_dtl_dmg_ttl' => object_get($goodsReceiptDtl, 'gr_dtl_dmg_ttl', 0),

            'gr_dtl_ept_ctn_ttl'  => object_get($goodsReceiptDtl, 'gr_dtl_ept_ctn_ttl', 0),
            'gr_dtl_act_ctn_ttl'  => object_get($goodsReceiptDtl, 'gr_dtl_act_ctn_ttl', 0),
            'gr_dtl_disc_ctn_ttl' => (object_get($goodsReceiptDtl, 'gr_dtl_act_ctn_ttl', 0)
                - object_get($goodsReceiptDtl, 'gr_dtl_ept_ctn_ttl', 0)),
            'asn_hdr_ept_dt'      => object_get($goodsReceiptDtl, 'goodsReceipt.asnHdr.asn_hdr_ept_dt', 0) ?
                date("m/d/Y", object_get($goodsReceiptDtl, 'goodsReceipt.asnHdr.asn_hdr_ept_dt', 0)) : '',
            'length'              => round(object_get($goodsReceiptDtl, 'length', 0), 2),
            'width'               => round(object_get($goodsReceiptDtl, 'width', 0), 2),
            'height'              => round(object_get($goodsReceiptDtl, 'height', 0), 2),
            'weight'              => round(object_get($goodsReceiptDtl, 'weight', 0), 1),
            'volume'              => round(object_get($goodsReceiptDtl, 'volume', 0), 2),

        ];

    }

    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date('m/d/Y H:i', $date) : null;
    }

}
