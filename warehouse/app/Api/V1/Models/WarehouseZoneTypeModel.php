<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\WarehouseZoneType;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class WarehouseZoneTypeModel extends AbstractModel
{

    protected $model;
    protected $limit = 10;

    public function __construct(WarehouseZoneType $model = null)
    {
        $this->model = ($model) ?: new WarehouseZoneType();
    }

    public function deleteMassZoneType(array $zoneTypeIds)
    {
        return $this->model
            ->whereIn('zone_type_id', $zoneTypeIds)
            ->delete();
    }

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $keys = [
            "like"  => [
                "zone_type_name",
                "zone_type_code",
                "zone_type_desc"
            ],
            "equal" => [
                "zone_type_id"
            ]
        ];

        return $this->_search($attributes, $with, $limit, $keys);
    }
}
