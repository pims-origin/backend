<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\LocationModel;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

class AddSKUModel extends AbstractModel
{

    const STATUS_CYCLEDTL_NEW = 'NW';
    const STATUS_CYCLEDTL_OPEN = 'OP';
    const STATUS_CYCLEDTL_NOT_APPLICABLE = 'NA';
    const STATUS_CYCLEDTL_RECOUNT = 'RC';
    const STATUS_CYCLEDTL_ACCEPTED = 'AC';
    const STATUS_INSERT = 'I';
    const STATUS_UPDATE = 'U';
    const STATUS_DELETE = 'D';

    const CARTON_STATUS_ACTIVE = 'AC';
    const CARTON_STATUS_LOCK = 'CL';

    const LOCATION_STATUS_ACTIVE = 'AC';
    const LOCATION_STATUS_LOCK = 'LK';

    static $cycleCountInfo = [];
    /**
     * @var array
     */

    protected $arrNumberFields = [
        'cycle_hdr_id',
        'cycle_dtl_id'
    ];

    /**
     * @var array
     */

    protected $arrStringFields = [
        'sku',
        'size',
        'color'
    ];

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * AddSKUModel constructor.
     *
     * @param CycleDtl $model
     */

    public function __construct(
        CycleDtl $model,
        LocationModel $locationModel,
        PalletModel $palletModel
    )
    {
        $this->model = $model ? $model : new CycleDtl();
        $this->locationModel = $locationModel;
        $this->palletModel = $palletModel;
    }

    /**
     * @param $post
     *
     * @return bool|static
     */
    public function processAddSKU($post)
    {
        $cycleDtl = new CycleDtl();
        $validate = $this->validateDataInput($post);

        if (!$validate) {
            return false;
        }
        try {
            DB::beginTransaction();

            $plRfid = array_get($post, 'plt_rfid', null);

            $model = $cycleDtl->create([
                'cycle_hdr_id'  => $post['cycle_hdr_id'],
                'whs_id'        => $post['whs_id'],
                'cus_id'        => $post['cus_id'],
                'item_id'       => $post['item_id'],
                'sku'           => strtoupper($post['sku']),
                'size'          => $post['size'],
                'color'         => $post['color'],
                'lot'           => array_get($post, 'lot', 'NA'),
                'pack'          => $post['pack'],
                'remain'        => $post['remain'],
                'is_new_sku'    => 1,
                'sys_qty'       => 0,
                'act_qty'       => $post['act_qty'],
                'sys_loc_id'    => $post['act_loc_id'],
                'sys_loc_name'  => $post['act_loc'],
                'act_loc_id'    => $post['act_loc_id'],
                'act_loc_name'  => $post['act_loc'],
                'cycle_dtl_sts' => self::STATUS_CYCLEDTL_NEW,
                'sts'           => self::STATUS_INSERT,
                'plt_rfid'      => $plRfid
            ]);
            //Update rfid for pallet
            $pallet = $this->palletModel->getFirstBy('loc_id', $post['act_loc_id']);
            if (!empty($pallet) && empty($pallet->rfid) && !empty($plRfid)) {
                $pallet->rfid = $plRfid;
                $pallet->save();
            }
            //Update status location is locked
            $this->locationModel->update([
                'loc_id'       => $post['act_loc_id'],
                'loc_whs_id'   => $post['whs_id'],
                'loc_sts_code' => self::LOCATION_STATUS_LOCK
            ]);

//            Carton::where([
//                'loc_id'  => $post['act_loc_id'],
//                'ctn_sts' => 'AC'
//            ])
//                ->update(['ctn_sts' => 'LK']);

            //---- Case add more SKU for Cycle Count Info for SKU Tracking report
            $cusId = array_get($post, 'cus_id', '');
            $cusInfo = DB::Table('customer')
                ->where('customer.cus_id', $cusId)
                ->first();

            $itemId = $post['item_id'];
            $itemInfo = DB::Table('item')
                ->where('item.item_id', $itemId)
                ->first();
            $length = array_get($itemInfo, 'length', '');
            $width = array_get($itemInfo, 'width', '');
            $height = array_get($itemInfo, 'height', '');

            $CycHdrInfo = DB::Table('cycle_hdr')
                ->where('cycle_hdr.cycle_hdr_id', $post['cycle_hdr_id'])
                ->first();

            if ($CycHdrInfo['count_by'] = 'EACH') {
                $count_by = 1;
            } else {
                $count_by = array_get($post, 'remain', 0);
            }
            $qty = round(
                $count_by *
                (
                    array_get($post, 'act_qty', 0) - array_get($post, 'sys_qty', 0)
                )
            );

            $ctns = round(
                $count_by *
                (
                    array_get($post, 'act_qty', 0) - array_get($post, 'sys_qty', 0)
                ) / array_get($post, 'pack', 0)
            );

            $cube = round(
                (
                    (
                        $count_by *
                        (
                            array_get($post, 'act_qty', 0) - array_get($post, 'sys_qty', 0)
                        ) / array_get($post, 'pack', 0)
                    ) * ($length * $width * $height)
                ) / 1728, 2
            );

            $dataSKUTrackingRpt = [
                'cus_id'   => $cusId,
                'cus_name' => array_get($cusInfo, 'cus_name', ''),
                'cus_code' => array_get($cusInfo, 'cus_code', ''),

                'cycle_hdr_id'  => $post['cycle_hdr_id'],
                'trans_num'     => $CycHdrInfo['cycle_num'],
                'whs_id'        => $CycHdrInfo['whs_id'],
                'po_ctnr'       => $CycHdrInfo['cycle_num'],
                'ref_cus_order' => $CycHdrInfo['cycle_name'],
                'actual_date'   => $CycHdrInfo['updated_at'],

                'item_id' => $post['item_id'],
                'sku'     => $post['sku'],
                'size'    => $post['size'],
                'color'   => $post['color'],
                'pack'    => empty($post['pack']) ? null : $post['pack'],
                'lot'     => $post['lot'],

                'qty'  => $qty,
                'ctns' => $ctns,
                'cube' => $cube
            ];
            $SKUTrackingReportModel = new SKUTrackingReportModel();
            $SKUTrackingReportModel->create($dataSKUTrackingRpt);

            /*$query = DB::table("invt_smr")
                ->where('sku', $post['sku']);
                if($post['color']!='NA' && $post['color'] != "") {
                    $query->where('color', $post['color']);
                }
                if($post['size']!='NA' && $post['size'] != "") {
                    $query->where('size', $post['size']);
                }
            $query->where('cus_id', $cusId)
                ->where('whs_id', $post['whs_id'])
                ->update(['avail' => $qty]);

            $queryInventory = DB::table("inventory")
                ->where('sku', $post['sku']);
            if($post['color']!='NA' && $post['color'] != "") {
                $queryInventory->where('color', $post['color']);
            }
            if($post['size']!='NA' && $post['size'] != "") {
                $queryInventory->where('size', $post['size']);
            }
            $queryInventory->where('cus_id', $cusId)
                ->where('whs_id', $post['whs_id'])
                ->update(['in_hand_qty' => $qty]);

            $queryRPT = DB::table("rpt_inventory")
                ->where('sku', $post['sku']);
            if($post['color']!='NA' && $post['color'] != "") {
                $queryRPT->where('color', $post['color']);
            }
            if($post['size']!='NA' && $post['size'] != "") {
                $queryRPT->where('size', $post['size']);
            }
            $queryRPT->where('cus_id', $cusId)
                ->where('whs_id', $post['whs_id'])
                ->update(['in_hand_qty' => $qty]);*/
            //---- /Case add more SKU for Cycle Count Info for SKU Tracking report

            DB::commit();

            return $model;

        } catch (\Exception $e) {
            \DB::rollBack();

            throw $e;
        }
    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function validateDataInput(&$post)
    {
        self::$cycleCountInfo = $this->getCycleCountInfo($post['cycle_hdr_id']);

        $result = $this->getLocationIDByWarehouse($post['act_loc'], $post['whs_id']);

        if (!$result) {
            $this->errors['act_loc_name'] = Message::get('BM066', 'Location ' . $post['act_loc'], 'warehouse');

            return false;
        }

        $post['act_loc_id'] = $result['loc_id'];
        $post['act_loc_code'] = $result['loc_code'];
        if (!empty($post['plt_rfid'])) {
            // $this->validatePltRfid($post['plt_rfid'], $post['act_loc_id']);
            if(!$this->validateLPNFormat($post['plt_rfid'])) {
                $this->errors['lpn'] = sprintf("LPN number %s is invalid", $post['plt_rfid']);
                return false;
            }

            $lpnInfo = DB::table('cycle_dtl')->where('plt_rfid', trim($post['plt_rfid']))->where('sku', '<>', $post['sku'])->first();
            if (!empty($lpnInfo)) {
                $this->errors['lpn'] = sprintf("LPN number %s already exists", $post['plt_rfid']);
                return false;
            }

            $cartonInfo = DB::table('cartons')->where('lpn_carton', trim($post['plt_rfid']))->where('sku', '<>', $post['sku'])->first();
            if (!empty($cartonInfo)) {
                $this->errors['lpn'] = sprintf("LPN number %s already exists", $post['plt_rfid']);
                return false;
            }
        }

        //get item_id
        $itemID = $this->getItemID($post);

        if (!$itemID) {
            $this->errors['sku'] = Message::get('BM067', 'Item');

            return false;
        }

        if ($post['act_qty'] <= 0) {
            $this->errors['sku'] = "Actual quantity have to greater than 0";

            return false;
        }

        $post['item_id'] = $itemID[0]['item_id'];

        // Check Item has exists on the another cycle count
        $itemModel = new ItemModel();
        $itemExistCycleOrBlock = $itemModel->checkItemExistOnAnotherCycleOrBlock(
            $post['whs_id'], $post['cycle_hdr_id'], $post['item_id']);

        if (!$itemExistCycleOrBlock['status']) {
            $this->errors['item'] = 'SKU: ' . $post['sku'] . ' existed on the other' . $itemExistCycleOrBlock['type'];

            return false;
        }

        // Check count items is already exist in count items.
        $skuResults = $this->checkForExistingSKU($post);

        if ($skuResults) {
            $this->errors['act_loc_name'] = Message::get('BM068',
                $post['sku'], $post['pack'], $post['remain'], $post['act_loc']);

            return false;
        }
        if (!empty($post['plt_rfid'])) {
            $newLPNCarton = CycleDtl::where('plt_rfid', trim($post['plt_rfid']))->first();
            if (!empty($newLPNCarton)) {
                if ($newLPNCarton->act_loc_id !== $post['act_loc_id']) {
                    $this->errors['act_loc_name'] = sprintf('LPN %s has been added to location %s', $post['plt_rfid'], $newLPNCarton->act_loc_name);

                    return false;
                }
            }
        }

        // check sku have carton (all status).
        $result = $this->checkNewSKUHaveCarton($post);

        if ($result) {
            $this->errors['sku'] = 'SKU ' . $post['sku'] . ' existed in location ' . $post['act_loc'];

            return false;
        }

        if (isset($post['plt_rfid']) && !empty($post['plt_rfid'])) {
            $lpnCarton = Carton::where('lpn_carton', $post['plt_rfid'])->where('whs_id', Data::getCurrentWhsId())->first();
            if ($lpnCarton && $lpnCarton->loc_id !== $post['act_loc_id']) {
                $this->errors['lpn'] = 'LPN ' . $lpnCarton->lpn_carton . ' already exists in location ' . ($lpnCarton->loc_code ?? $lpnCarton->loc_name . ' (Shipped)');

                return false;
            }
        }

        if ($post['cycle_type'] == CycleHdrModel::CYCLE_TYPE_LOCATION) {
            // Check location in range? temporary disable
            //if (!in_array($post['act_loc_id'], explode(',', self::$cycleCountInfo['cycle_detail']))) {
            //    $this->errors['act_loc'] = Message::get('BM069', $post['act_loc']);
            //
            //    return false;
            //}

            // Check sku was cycled in other report
            $results = $this->checkSKUAlreadyExistOtherReport($post);

            if ($results['hasLock'] && !$results['isValid']) {
                $this->errors['sku'] = Message::get('BM070', $post['sku']);

                return false;
            }
        }

        return true;
    }

    public function validatePltRfid($rfid, $locId)
    {
        $pltExistObj = Pallet::where('rfid', $rfid)
            ->where('loc_id', '!=',$locId)
            ->first();
        if ($pltExistObj) {
            throw new HttpException(403, "Pallet RFID {$rfid} existed.");
        }

        $pltObj = Pallet::where('plt_num', 'LIKE', $rfid . "-%")
            ->where('ctn_ttl', '>', 0)
            // ->orderBy('plt_num')
            ->first();

        if ($pltObj && $pltObj->loc_id != $locId) {
            throw new HttpException(403, "Pallet Id '$rfid' was belonged to location '{$pltObj->loc_code}'");
        }

        $chkRfid = CycleHdr::join('cycle_dtl', 'cycle_hdr.cycle_hdr_id', '=', 'cycle_dtl.cycle_hdr_id')
            ->where('cycle_hdr.cycle_sts', '!=', 'CP')
            ->where('cycle_dtl.plt_rfid', '=', $rfid)
            ->first()
            ;

        if ($chkRfid) {
            throw new HttpException(403, "Pallet Id '$rfid' was belonged to cycle count '{$chkRfid->cycle_num}'");
        }
    }

    public function validateLPNFormat($plt_num)
    {
        if ((bool)preg_match("/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }

        if ((bool)preg_match("/^T-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }

        if ((bool)preg_match("/^V-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }

        return false;
    }

    /**
     * @param $loc
     * @param $wshID
     *
     * @return bool
     */

    public function getLocationIDByWarehouse($loc, $wshID)
    {
        return Location::select('loc_id', 'loc_sts_code')
            ->where('loc_code', $loc)
            ->where('loc_whs_id', $wshID)
            ->first();
    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function checkForExistingSKU($post)
    {
        $query = CycleDtl::where('cycle_hdr_id', $post['cycle_hdr_id'])
            ->where('item_id', $post['item_id'])
            ->where('cus_id', $post['cus_id'])
            ->where('act_loc_id', $post['act_loc_id'])
            ->where('pack', $post['pack'])
            ->where('lot', $post['lot']);
            //->where('remain', $post['remain']);
        if (isset($post['plt_rfid']) && !empty($post['plt_rfid'])) {
            $query->where('plt_rfid', '=', $post['plt_rfid']);
        }
        return $query->count();
    }

    /**
     * @param $post
     *
     * @return mixed
     */

    public function checkNewSKUHaveCarton($post)
    {
        $query = Carton::where('item_id', $post['item_id'])
            ->where('cus_id', $post['cus_id'])
            ->where('loc_id', $post['act_loc_id'])
            ->where('deleted', '=', 0);
        if (isset($post['plt_rfid']) && !empty($post['plt_rfid'])) {
            $query->where('lpn_carton', '=', $post['plt_rfid']);
        }
        return $query->exists();
    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function checkSKUAlreadyExistOtherReport($post)
    {
        $isValid = [];

        $results = $this->getCycleCountInfoHaveLockedCarton($post);

        foreach ($results as $row) {
            if (self::$cycleCountInfo['created_at'] < $row['created_at']) {
                $isValid[] = in_array($row['cycle_type'], [
                        CycleHdrModel::CYCLE_TYPE_SKU,
                        CycleHdrModel::CYCLE_TYPE_CUSTOMER
                    ]) || $row['cycle_type'] = CycleHdrModel::CYCLE_TYPE_LOCATION;
            }
        }

        return [
            'hasLock' => $results ? true : false,
            'isValid' => in_array(true, $isValid)
        ];
    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function getItemID($post)
    {
        $query = Item::select('item_id')
            ->where('cus_id', $post['cus_id'])
            ->where('sku', $post['sku']);
        if($post['color']!='NA' && $post['color'] != "") {
            $query->where('color', $post['color']);
        };
        if($post['size']!='NA' && $post['size'] != "") {
            $query->where('size', $post['size']);
        };
        $rs = $query->get()
        ->toArray();

        return $rs;
    }

    /**
     * @param $post
     *
     * @return mixed
     */

    public function getCycleCountInfoHaveLockedCarton($post)
    {
        return Carton::select(
            'cycle_hdr.cycle_hdr_id',
            'cycle_hdr.cycle_type',
            'cycle_hdr.created_at'
        )
            ->join('cycle_dtl', 'cartons.item_id', '=', 'cycle_dtl.item_id')
            ->join('cycle_hdr', 'cycle_dtl.cycle_hdr_id', '=', 'cycle_hdr.cycle_hdr_id')
            ->where('cartons.item_id', '=', $post['item_id'])
            ->where('cycle_dtl.cus_id', '=', $post['cus_id'])
            ->where('cycle_hdr.cycle_hdr_id', '!=', $post['cycle_hdr_id'])
            ->where('cartons.ctn_sts', '=', self::CARTON_STATUS_LOCK)
            ->groupBy('cycle_hdr.cycle_hdr_id')
            ->get()
            ->toArray();
    }

    /**
     * @param $data
     *
     * @return bool
     */

    public function getInventoryCarton($data)
    {
        $cycleHdrModel = new CycleHdrModel();

        $cycleDetail = $data['cycle_type'] == CycleHdrModel::CYCLE_TYPE_CUSTOMER ? [$data['cus_id']]
            : $data['cycle_type'] == CycleHdrModel::CYCLE_TYPE_SKU ? [$data['sku']]
                : explode(',', self::$cycleCountInfo['cycle_detail']);

        $cartonData = $cycleHdrModel->getCycleData(
            $data['whs_id'],
            $data['cycle_type'],
            $cycleDetail
        );

        if (self::$cycleCountInfo['has_color_size']) {
            $hasColorSize = $cycleHdrModel->checkHasColorSize($cartonData);
            if ($hasColorSize) {
                $this->errors['sku'] = Message::get('BM071', implode(',', array_keys($hasColorSize)));

                return false;
            }
        }

        return $cartonData;
    }

    /**
     * @param $cycleID
     *
     * @return mixed
     */

    public function getCycleCountInfo($cycleID)
    {
        return CycleHdr::where('cycle_hdr_id', $cycleID)->first()->toArray();
    }

    public function getInfobyItemId($id) {
        return Item::select(DB::raw('size, color,pack'))
            ->where('item_id',$id)
            ->first();
    }


    public function processCreateCdtBySku($post) {
        $cycleDtl = new CycleDtl();
//        $validate = $this->validateDataInput($post);
//
//        if (!$validate) {
//            return false;
//        }
        $item = $this->getInfobyItemId($post['item_id']);
        if(empty($item)) {
            return $this->response->errorBadRequest("Item is not exit!!!");
        }
        try {
            DB::beginTransaction();

            $plRfid = array_get($post, 'plt_rfid', null);

            $model = $cycleDtl->create([
                'cycle_hdr_id'  => $post['cycle_hdr_id'],
                'whs_id'        => $post['whs_id'],
                'cus_id'        => $post['cus_id'],
                'item_id'       => $post['item_id'],
                'sku'           => strtoupper($post['sku']),
                'size'          => (!empty($item->size))? $item->size : "NA",
                'color'         => (!empty($item->color))? $item->color : "NA",
                'lot'           => array_get($post, 'lot', 'NA'),
                'pack'          => $item->pack,
                'remain'        => $post['remain'],
                'is_new_sku'    => 0,
                'sys_qty'       => $post['sys_qty'],
                'act_qty'       => 0,
                'sys_loc_id'    => $post['sys_loc_id'],
                'sys_loc_name'  => $post['sys_loc_name'],
                'act_loc_id'    => $post['sys_loc_id'],
                'act_loc_name'  => $post['sys_loc_name'],
                'cycle_dtl_sts' => self::STATUS_CYCLEDTL_NEW,
                'sts'           => self::STATUS_INSERT,
                'plt_rfid'      => $plRfid
            ]);
            //Update rfid for pallet
            $pallet = $this->palletModel->getFirstBy('loc_id', $post['act_loc_id']);
            if (!empty($pallet) && empty($pallet->rfid) && !empty($plRfid)) {
                $pallet->rfid = $plRfid;
                $pallet->save();
            }
            //Update status location is locked
            $this->locationModel->update([
                'loc_id'       => $post['act_loc_id'],
                'loc_whs_id'   => $post['whs_id'],
                'loc_sts_code' => self::LOCATION_STATUS_LOCK
            ]);

            Carton::where([
                'loc_id'  => $post['act_loc_id'],
                'ctn_sts' => 'AC'
            ])
                ->update(['ctn_sts' => 'LK']);

            DB::commit();

            return $model;

        } catch (\Exception $e) {
            \DB::rollBack();

            throw $e;
        }
    }
}