<?php
namespace Seldat\Wms2\Models;

/**
 * Class OrderHdr
 *
 * @property mixed odr_id
 * @property mixed qualifier
 * @property mixed value
 * @property mixed created_at
 *
 * @package Seldat\Wms2\Models
 */
class OrderHdrMeta extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'odr_hdr_meta';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_id';
    /**
     * @var array
     */
    protected $fillable = [
        'odr_id',
        'qualifier',
        'value'
    ];

    public $dateFields = [
        'odr_id',
        'qualifier',
        'value',
    ];
}
