<?php

namespace App\Api\V2\Inbound\Location\Putback\Controllers;

use App\Api\V2\Inbound\Location\Putback\Models\CartonModel;
use App\Api\V2\Inbound\Location\Putback\Models\LocationModel;
use App\Api\V2\Inbound\Location\Putback\Models\PalletModel;
use App\Api\V2\Inbound\Location\Putback\Models\Log;
use App\Api\V2\Inbound\Location\Putback\Models\CustomerModel;
use App\Api\V2\Inbound\Location\Putback\Models\OrderCartonModel;
use App\Api\V2\Inbound\Location\Putback\Models\OrderHdrModel;
use App\Api\V2\Inbound\Location\Putback\Models\WavePickDtlModel;
use App\Api\V2\Inbound\Location\Putback\Models\WavePickHdrModel;
use App\Api\V2\Inbound\Location\Putback\Models\InventorySummaryModel;
use App\libraries\RFIDValidate;

use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\MyHelper;

class PutBackController extends AbstractController
{
    const DEFAULT_LOT_PUTBACK   = "NA";
    const DEFAULT_CCN_MIXED_SKU = "Mixed SKU";

    protected $palletModel;
    protected $cartonModel;
    protected $locationModel;
    protected $damagedCartonModel;
    protected $goodsReceiptModel;
    protected $goodsReceiptDetailModel;
    protected $inventorySummaryModel;
    protected $odrDtlModel;
    protected $odrHdrModel;
    protected $orderCartonModel;
    protected $wavePickHdrModel;
    protected $wavePickDtlModel;
    protected $customerModel;

    protected $_pltId;

    /**
     * PutbackController constructor.
     *
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     */
    public function __construct(
        PalletModel $palletModel,
        LocationModel $locationModel,
        CartonModel $cartonModel,
        CustomerModel $customerModel,
        OrderCartonModel $orderCartonModel,
        OrderHdrModel $odrHdrModel,
        WavePickDtlModel $wavePickDtlModel,
        WavePickHdrModel $wavePickHdrModel,
        InventorySummaryModel $inventorySummaryModel
    ) {
        $this->palletModel           = $palletModel;
        $this->locationModel         = $locationModel;
        $this->cartonModel           = $cartonModel;
        $this->customerModel         = $customerModel;
        $this->orderCartonModel      = $orderCartonModel;
        $this->odrHdrModel           = $odrHdrModel;
        $this->wavePickDtlModel      = $wavePickDtlModel;
        $this->wavePickHdrModel      = $wavePickHdrModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function putBack($whsId, Request $request)
    {
        $url = "/V2/whs/{$whsId}/location/rack/putback";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'PTR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Put back to RACK'
        ]);

        $input = $request->getParsedBody();
        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        $pltRfid     = array_get($input, 'pallet_rfid');
        $locRfid     = array_get($input, 'location');
//        $packSizeMax = array_get($input, 'pack_size'); // ignore pack_size max
        $itemId      = array_get($input, 'item_id');
        $cusId       = array_get($input, 'cus_id');
        $cartons     = array_get($input, 'cartons');
        $ctnPutback  = count($cartons);

        //Check cartons putback
        foreach($cartons as $carton){
            $rfid = array_get($carton, 'ctn_rfid', null);
            $piece_remain = array_get($carton, 'piece_remain', null);

            if ( !$rfid || !$piece_remain ){
                $msg = 'Carton rfid and piece remain is required';
                return $this->_responseErrorMessage($msg);
            }
        }

        // Check customer
        $cusArray = DB::table('customer_warehouse')
            ->where('cus_id', $cusId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->first();
        if (!count($cusArray)) {
            $cusObj = $this->customerModel->getFirstWhere(['cus_id' => $cusId]);
            if (!$cusObj) {
                $msg = sprintf("The customer Id %d doesn't exist.", $cusId);
            } else {
                $msg = sprintf("The customer %s doesn't belong to current warehouse.", $cusObj->cus_name);
            }
            return $this->_responseErrorMessage($msg);
        }
        // Get Pallet by Rfid
        $pallet = null;
        if ($pltRfid) {
            $pallet = $this->palletModel->getFirstWhere(['rfid' => $pltRfid]);

            if ($pallet) {
//                $msg = sprintf("The pallet Rfid %s doesn't exist.", $pltRfid);
//                return $this->_responseErrorMessage($msg);
                if ($pallet->plt_sts != 'AC') {
                    $msg = sprintf("The pallet Rfid %s is not Active.", $pltRfid);
                    return $this->_responseErrorMessage($msg);
                }

                if ($pallet->whs_id != $whsId) {
                    $msg = sprintf("The pallet Rfid %s doesn't belong to current warehouse.", $pltRfid);
                    return $this->_responseErrorMessage($msg);
                }

                if ($pallet && $pallet->cus_id != $cusId) {
                    $msg = sprintf("The Pallet Rfid %s doesn't belong to current customer.",
                        $pltRfid, $cusId);
                    return $this->_responseErrorMessage($msg);
                }

                if ($pallet && $pallet->loc_code != null) {
                    $msg = sprintf("The Pallet Rfid %s was already on location %s",
                        $pltRfid, $pallet->loc_code);
                    return $this->_responseErrorMessage($msg);
                }

            }
        }

        $locObj = $this->locationModel->getFirstWhere([
            'loc_whs_id' => $whsId,
            'rfid'       => $locRfid,
        ]);

        if (!$locObj) {
            $msg = sprintf("The Location Rfid %s doesn't exist.", $locRfid);
            $locObj = $this->locationModel->getFirstWhere([
                'rfid'   => $locRfid,
            ]);
            if ($locObj) {
                $msg = sprintf("The Location doesn't belong to current warehouse.");
            }
            return $this->_responseErrorMessage($msg);
        } else {
            if ($locObj->loc_sts_code != "AC") {
                $msg = sprintf("The location %s is not Active.", $locObj->loc_code);
                return $this->_responseErrorMessage($msg);
            }
//            if ($locObj->loc_type_code != "RAC") {
//                $msg = sprintf("Unable Put back cartons to Location is not RAC type.");
//                return $this->_responseErrorMessage($msg);
//            }
        }

        $limit = 1;
        // WAP-584 - Create API get suggest location list by Pallet tag
        if ($pltRfid) {
            $checkLocationHasPallet = $this->palletModel->getFirstWhere(['loc_id' => $locObj->loc_id]);
            if ($checkLocationHasPallet) {
                if (!$checkLocationHasPallet->rfid){
                    $checkLocationHasPallet->rfid = $checkLocationHasPallet->plt_num;
                }
                $msg = sprintf("The location %s existed a Pallet %s.", $locObj->loc_code, $checkLocationHasPallet->rfid);
                return $this->_responseErrorMessage($msg);
            }
            $data = [
                'location'   => $locObj,
            ];
            //o	Suggest empty locations if WAP send cartons within a pallet
            $locObjs = $this->locationModel->getMoreEmptyLocationByCusId($whsId, $cusId, $data,'RAC', $limit);

            if (!count($locObjs)) {
                $msg = sprintf("The location is invalid.");
                // Get pallet by location
                $pallet = $this->palletModel->getFirstWhere([
                    'loc_id' => $locObj->loc_id,
                    'whs_id' => $whsId,
                ]);

                if ($pallet) {
                    $msg = sprintf("The location is not empty.");
                }
                return $this->_responseErrorMessage($msg);
            }
        } else {
            // Get pallet by location
            $pallet = $this->palletModel->getFirstWhere([
                'loc_id' => $locObj->loc_id,
                'whs_id' => $whsId,
            ]);

            if (!$pallet) {
                $msg = sprintf("The location doesn't certain any pallet.");
                return $this->_responseErrorMessage($msg);
//            } else {
//                $limitCtn = object_get($pallet, 'init_ctn_ttl', 0) - object_get($pallet, 'ctn_ttl', 0);
//                if ($limitCtn < $ctnPutback) {
//                    $msg = sprintf("There are %d cartons. But the location cannot contain more %d cartons.", $ctnPutback, $limitCtn);
//                    return $this->_responseErrorMessage($msg);
//                }
            }

            $data = [
                'ctn_ttl'   => $ctnPutback,
//                'pack_size' => $packSizeMax,
//                'lot'       => self::DEFAULT_LOT_PUTBACK,
                'item_id'   => null,
                'location'  => $locObj
            ];

            //o	Suggest locations at the level 1 and less cartons
            $locObjs = $this->locationModel->getMoreLocationCanPutCarton($whsId, $cusId, $data, 'RAC', $limit);
            if (!count($locObjs)) {
                $msg = sprintf("The location is invalid.");

                // check a least one carton is Rfid
                $cartonError = DB::table('cartons')
                    ->where('loc_id', $locObj->loc_id)
                    ->whereNotNull('rfid')
                    ->where('ctn_sts', '!=', 'AJ')
                    ->first();
                if (!count($cartonError)) {
                    $msg = sprintf("The location have to exist at least one carton is Rfid.");
                }
                return $this->_responseErrorMessage($msg);
            }
        }

        try {
            DB::beginTransaction();

            if ($pltRfid) {
                $chkPlt = $this->palletModel->getModel()->where([
                    'loc_id' => $locObj->loc_id,
                    'whs_id' => $whsId
                ])
                    ->first();

                if ($chkPlt && ($chkPlt->rfid <> $input['pallet_rfid'] || $chkPlt->rfid == null)) {
                    $msg = sprintf("There is a pallet %s in this location %s", $chkPlt->plt_num, $locObj->loc_code);
                    return $this->_responseErrorMessage($msg);
                }

            } else {
                $pltRfid = object_get($pallet, 'rfid');
                if (!$pltRfid) {
                    $msg = sprintf("The pallet on location %s has to be Rfid.", $locObj->loc_code);
                    return $this->_responseErrorMessage($msg);
                }
            }

            // WMS2-5698 - [WAP-API] Putback - Support AJ cartons and swap with fake RFID with prefix "CCTC"
            $rfids = array_pluck($cartons, 'ctn_rfid');
            if (count($rfids)){
                $ctnObjs = $this->cartonModel->getModel()
                    ->whereIn('rfid', $rfids)
                    ->where('ctn_sts', 'AJ')
                    ->get();
                $msg = null;
                foreach ($ctnObjs as $ctnObj) {
                    $msg = $this->_processCartonsCCTC($ctnObj, $cusId);
                }
                if ($msg) {
                    return $this->_responseErrorMessage($msg);
                }

            }
             //update inv_smr
            foreach($cartons as $carton){
                $this->updateInvtSummaryWhenPutBack($carton, $whsId);
            }

            $errors = $this->consolidatePallet($whsId, $pltRfid, $locObj, $input);
            if ($errors) {
                DB::rollback();

                return $errors;
            }



            //unlock this location
            $this->locationModel->updateWhere(
                [
                    'loc_sts_code' => Status::getByKey('LOCATION_STATUS', 'ACTIVE'),
                ],
                [
                    'loc_id' => $locObj->loc_id
                ]
            );

            $returnData = $this->cartonModel->getCartonsInfoByPalletId($whsId, $this->_pltId);

            DB::commit();

            $msg = sprintf("Putback cartons to location %s successfully!", $locObj->loc_code);

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => $msg
                    ]
                ],
                'data' => [
                    [
                        'skus'     => $returnData,
                        'location' => $locObj->loc_code
                    ]
                ]
            ];
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _validateParams($attributes, $whsId)
    {
        $pltRfid  = array_get($attributes, 'pallet_rfid');
        $location = array_get($attributes, 'location');
//        $packSize = array_get($attributes, 'pack_size');
        $itemId   = array_get($attributes, 'item_id');
        $cusId    = array_get($attributes, 'cus_id');
        $cartons  = array_get($attributes, 'cartons');

        $names = [
            'pallet_rfid' => 'Pallet Rfid',
            'location'    => 'Location',
            'pack_size'   => 'Pack size',
            'item_id'     => 'Item Id',
            'cus_id'      => 'Customer Id',
            'cartons'     => 'Cartons array',
        ];

        // Check Required
        $requireds = [
//            'pallet_rfid' => $pltRfid,
//            'pack_size' => $packSize,
            'location'  => $location,
            'item_id'   => $itemId,
            'cus_id'    => $cusId,
            'cartons'   => $cartons,
        ];

        // Check int type and greater than 0
        $intGreaterThan0 = [
            'cus_id'    => $cusId,
            'item_id'   => $itemId,
//            'pack_size' => $packSize,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet rfid is string
        if ($pltRfid && !is_string($pltRfid)) {
            $msg = $names['pallet_rfid'] . " must be string type.";
            return $this->_responseErrorMessage($msg);
        }

        // location rfid is string
        if (!is_string($location)) {
            $msg = $names['location'] . " must be string type.";
            return $this->_responseErrorMessage($msg);
        }

        // cartons is array
        if (!is_array($cartons) || !count($cartons)) {
            $msg = $names['cartons'] . " must be array type and not be empty.";
            return $this->_responseErrorMessage($msg);
        }

        foreach ($intGreaterThan0 as $key => $field) {
            if (!is_int($field) || $field < 1) {
                $errorDetail = "{$names[$key]} must be integer type and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        //check invalid code carton
        $ctnRfidInValid = [];
        $errorMsg = '';
        $checkDuplicated = [];
        foreach ($cartons as $carton)
        {
            $ctnRfid     = array_get($carton, 'ctn_rfid');
            $pieceRemain = array_get($carton, 'piece_remain');
            $checkDuplicated[] = $ctnRfid;

            $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
            if (!$ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $ctnRfid;
                if ('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }

//            if (!is_int($pieceRemain) || $pieceRemain < 1 || $pieceRemain > $packSize) {
//                $errorDetail = sprintf("The piece remain of carton Rfid %s must be integer type,
//                greater than 0 and not greater than pack size %s.", $ctnRfid, $packSize);
//                return $this->_responseErrorMessage($errorDetail);
//            }
        }

        if (count($cartons) != count($checkDuplicated)) {
            $errorDetail = sprintf("It is duplicated carton.");
            return $this->_responseErrorMessage($errorDetail);
        }

        if (count($ctnRfidInValid)) {
            return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
        }

        //validate pallet RFID
        if ($pltRfid) {
            $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
            if (!$pltRFIDValid->validate()) {
                return $this->_responseErrorMessage($pltRFIDValid->error);
            }
        }

        //validate location RFID
        $locRFIDValid = new RFIDValidate($location, RFIDValidate::TYPE_LOCATION, $whsId);
        if (!$locRFIDValid->validate()) {
            return $this->_responseErrorMessage($locRFIDValid->error);
        }

    }


    /**
     * @param $whsId
     * @param $palletRFID
     * @param Request $request
     *
     * @return array|Response
     */
    public function consolidatePallet(
        $whsId,
        $pltRfid,
        $locObj,
        $input
    ) {
        try {


    //        $packSizeMax = array_get($input, 'pack_size'); // ignore pack_size max
            $itemId      = array_get($input, 'item_id');
            $cusId       = array_get($input, 'cus_id');
            $cartons     = array_get($input, 'cartons');

            $randRfid = $arrCtnRfid = array_pluck($cartons, 'ctn_rfid');

            //check exist of pallet (new location)
            $newPltId = '';
            $newLocationId = '';
            $newCreatePltId = '';
            $checkPalletExist = $this->palletModel->checkExistedPalletAccordingToRFID($pltRfid, $whsId);
            if ($checkPalletExist) {
                $newLocationId = object_get($checkPalletExist, 'loc_id', '');
                $newPltId = object_get($checkPalletExist, 'plt_id', '');

                $cartonsInNewPlt = $this->cartonModel->findWhere(['plt_id' => $newPltId]);
                if ($cartonsInNewPlt) {
                    foreach ($cartonsInNewPlt as $carton) {
                        if (strtoupper(object_get($carton, 'ctn_sts', '')) == 'LK') {
                            $msg = sprintf("Pallet: %s contains the cartons is locked.", $pltRfid);
                            return $this->_responseErrorMessage($msg);
                        }
                    }
                }
            }

            //check exist of cartons
            $noExistCartons = [];
            $existCartons = [];
            $exceptCartons = '';
            $exceptCartonsStatus = '';
            $oldLocIdArr = [];
            $oldPltIdArr = [];
            $assignedCartons = '';
            $respectiveOdrNum = '';
            $canceledCartons = '';
            $notTheSameCus = '';
            $mixSku = '';
            $mixCusId = '';
            foreach ($arrCtnRfid as $ctnRfid) {
                $checkCartonsExist = $this->cartonModel->checkExistedCartonAccordingToRFID($ctnRfid, $whsId);

                $ctnSts = strtoupper(object_get($checkCartonsExist, 'ctn_sts', ''));

                if ($checkCartonsExist) {
                    $existCartons[] = $ctnRfid;
                }

                if (!$checkCartonsExist) {
                    $noExistCartons[] = $ctnRfid;
                } elseif ($ctnSts != 'AC' && $ctnSts != 'PD' && $ctnSts != 'PB') {
                    $exceptCartons .= $ctnRfid . ' ';
                    $exceptCartonsStatus .= Status::getByValue($checkCartonsExist->ctn_sts, "CTN_STATUS") . ' ';
                }

                if ($ctnSts == 'PD' || $ctnSts == 'PB') {
                    //Check Carton is assigned to order or not
                    $orderCarton = $this->orderCartonModel->getFirstBy('ctn_rfid', $ctnRfid);
                    if ($orderCarton) {
                        $odrHdrId = object_get($orderCarton, 'odr_hdr_id', '');
                        if ($odrHdrId) {
                            $getorderInfo = $this->odrHdrModel->getFirstBy('odr_id', $odrHdrId);
                            if ($getorderInfo) {
                                $odrSts = object_get($getorderInfo, 'odr_sts', '');
                                $odrnum = object_get($getorderInfo, 'odr_num', '');
                                if ($odrSts != 'CC') {
                                    if ($ctnSts == 'PD') {
                                        $assignedCartons .= $ctnRfid . ' ';
                                        $respectiveOdrNum .= $odrnum . ' ';
                                    } elseif ($ctnSts == 'PB') {
                                        $canceledCartons .= $ctnRfid . ' ';
                                    }
                                }
                            }
                        }
                    }
                }

                //get old locked location_id
                if ($checkCartonsExist) {
                    $oldLocId = object_get($checkCartonsExist, 'loc_id', '');
                    if ($oldLocId) {
                        array_push($oldLocIdArr, $oldLocId);
                    }

                    //old pallets
                    $oldPltId = object_get($checkCartonsExist, 'plt_id', '');
                    if ($oldPltId) {
                        array_push($oldPltIdArr, $oldPltId);
                    }

                    // check mixed SKU
                    if ($itemId != object_get($checkCartonsExist, 'item_id')) {
                        $mixSku .= $ctnRfid . " ";
                    }

                    // check mixed Customer
                    if ($cusId != object_get($checkCartonsExist, 'cus_id')) {
                        $mixCusId .= $ctnRfid . " ";
                    }
                    //Current Pallet and New Pallet do not belong to the same customer
                    if ($checkCartonsExist && !$checkPalletExist && !$newCreatePltId) {
                        $cus_id = object_get($checkCartonsExist, 'cus_id', '');
                        $newCreatePltId = $this->palletModel->createNewPallet($pltRfid, $whsId, $cus_id)->plt_id;
                        $newPltId = $newCreatePltId;
                    }

                    if ($newPltId) {
                        $palletToInfo = $this->palletModel->checkPltSameCustomer(
                            object_get($checkCartonsExist, 'whs_id', ''),
                            object_get($checkCartonsExist, 'cus_id', ''),
                            $newPltId
                        );
                        if (empty($palletToInfo)) {
                            $notTheSameCus .= $ctnRfid . ' ';
                        }
                    }
                }
            }

            //check status of old location from cartons
            $lockedLocArr = '';
            if ($oldLocIdArr) {
                $oldLocationInfos = $this->locationModel->getLocationByIds($oldLocIdArr);
                if ($oldLocationInfos) {
                    foreach ($oldLocationInfos->toArray() as $oldLocationInfo) {
                        if (strtoupper($oldLocationInfo['loc_sts_code']) == "LK") {
                            $lockedLocArr .= $oldLocationInfo['loc_code'] . ' ';
                        }
                    }
                }
            }

            if ($lockedLocArr) {
                \DB::rollBack();
                $msg = sprintf("Locations: %s contains the cartons  is locked.",
                    $lockedLocArr);
                return $this->_responseErrorMessage($msg);
            }

            //Current Pallet and New Pallet do not belong to the same customer
            if ($notTheSameCus) {
                \DB::rollBack();
                $msg = sprintf("Unable to put back cartons with mixed customers on a pallet.");
                return $this->_responseErrorMessage($msg);
            }

            if ($mixCusId) {
                \DB::rollBack();
                $msg = sprintf("Unable to put back cartons with mixed customers on a pallet. Current customer is %s. %s", $cusId, $mixCusId);
                return $this->_responseErrorMessage($msg);
            }

            if ($exceptCartons) {
                \DB::rollBack();
                $msg = sprintf("These cartons: %s with respective status are %s, their status should be Active or Picked or Put Back.",
                    $exceptCartons,
                    trim($exceptCartonsStatus));
                return $this->_responseErrorMessage($msg);
            }

            //Show message: Carton is assigned to order or not
            if ($assignedCartons) {
                \DB::rollBack();
                $msg = sprintf("These cartons: %s assigned to order %s and not yet cancel.", trim
                ($assignedCartons), trim($respectiveOdrNum));
                return $this->_responseErrorMessage($msg);
            }

            //Show message: Carton is assigned to order or not
            if ($canceledCartons) {
                \DB::rollBack();
                $msg = sprintf("These cartons: %s have not yet canceled from order", trim
                ($canceledCartons));
                return $this->_responseErrorMessage($msg);
            }

            if (count($noExistCartons)) {
    //            \DB::rollBack();
    //            $msg = sprintf("These cartons: %s are not existed.", $noExistCartons);
    //            return $this->_responseErrorMessage($msg);
                if (!$checkPalletExist && !$newCreatePltId) {
                    $newCreatePltId = $this->palletModel->createNewPallet($pltRfid, $whsId, $cusId)->plt_id;
                    $newPltId = $newCreatePltId;
                }
                $newRfids = [];
                foreach ($cartons as $carton)
                {
                    if (in_array($carton['ctn_rfid'], $noExistCartons)) {
                        $newRfids[$carton['ctn_rfid']] = $carton['piece_remain'];
                    }
                }
                $sysUom = DB::table('system_uom')->where('sys_uom_code', 'CT')->first();
                if (!count($sysUom)){
                    $sysUom = DB::table('system_uom')->orderBy('sys_uom_id')->first();
                }
                $this->_executionNewCartonPutback(
                    $whsId,
                    $cusId,
                    $newRfids,
                    $itemId,
    //                $packSizeMax,
                    $sysUom);
            }

            //check status of new location
            if ($newLocationId) {
                $newLocationInfo = $this->locationModel->getFirstBy('loc_id', $newLocationId);
                $newLocationStatus = object_get($newLocationInfo, 'loc_sts_code', '');

                if (strtoupper($newLocationStatus) == "LK") {
                    \DB::rollBack();
                    $msg = sprintf("Location contains the %s pallet is locked.",
                        $pltRfid);
                    return $this->_responseErrorMessage($msg);
                }
            }

            //Start update data
            //update wave pick
            $wavePickPickeds = $this->updateWaveDtl($whsId, $arrCtnRfid);

            $this->_pltId = $newPltId;
            //update plt_id and ctn_sts=AC  to cartons
            $this->cartonModel->updatePalletID($newPltId, $whsId, $arrCtnRfid);
            // Update Pallet
            $pallet = $this->palletModel->getFirstWhere(['plt_id' => $newPltId]);
            $pallet->loc_id   = $locObj->loc_id;
            $pallet->loc_code = $locObj->loc_code;
            $pallet->loc_name = $locObj->loc_alternative_name;
            $pallet->save();

            // Update Carton
            $dataCtn = [
                'loc_id'        => $locObj->loc_id,
                'loc_code'      => $locObj->loc_code,
                'loc_name'      => $locObj->loc_alternative_name,
                'loc_type_code' => Status::getByValue('RACK', 'LOC_TYPE_CODE'),
                'plt_id'        => $pallet->plt_id
            ];
            $this->cartonModel->updateCartonWithPltID($dataCtn);

            if ($mixSku) {
    //            \DB::rollBack();
    //            $msg = sprintf("Unable put back cartons with mixed SKU for these cartons, current Item is %s. %s", $itemId, $mixSku);
    //            return $this->_responseErrorMessage($msg);
                // Insert CCN "Mixed SKU"
                $pltObj = $this->palletModel->getFirstWhere(['plt_id' => $newPltId]);
                if ($pltObj) {
                    $this->_insertCcn($whsId, $locObj, $pltObj, self::DEFAULT_CCN_MIXED_SKU);
                }
            }

            //count cartons of the new pallet
            $ctnInfo = $this->cartonModel->countCartonsByPalletId($newPltId);
            //remove location for all cartons in new pallet
    //        $this->cartonModel->removeAllLocForCtnInPallet($newPltId, $whsId);

            $ctnTtl = 0;
            if ($ctnInfo) {
                $ctnTtl = $ctnInfo->numberOfCarton;
            }
            //update ctn_ttl, remove locations for the new pallet
            $this->palletModel->updateCtnTtlForPallet($newPltId, $ctnTtl);
            //update ctn_ttl and remove loc if ctn_ttl = 0 for old pallets
            foreach ($oldPltIdArr as $oldPltId) {
                //count cartons of the old pallet
                $oldCtnInfo = $this->cartonModel->countCartonsByPalletId($oldPltId);
                $oldCtnTtl = 0;
                if ($oldCtnInfo) {
                    $oldCtnTtl = $oldCtnInfo->numberOfCarton;
                }
                //update ctn_ttl, remove locations for the old pallet
                $this->palletModel->updateWhere(
                    [
                        'ctn_ttl' => $oldCtnTtl
                    ],
                    [
                        'plt_id' => $oldPltId
                    ]
                );
                if ($oldCtnTtl == 0) {
                    $this->palletModel->updateWhere(
                        [
                            'loc_id'     => null,
                            'loc_code'   => null,
                            'loc_name'   => null,
                            'rfid'       => null,
                            'deleted'    => 1,
                            'deleted_at' => time(),
                        ],
                        [
                            'plt_id' => $oldPltId
                        ]
                    );
                }
            }

            //WMS2-5156 - [WAP API - OUTBOUND] Carton number was dupplicate with auto pack after order packing putback cartons via WAP API
            $this->_updateCtnPBNum($existCartons);

            return false; //successfully!

        } catch (\Exception $e) {
            if ($e->getCode() === '23000') {
                preg_match("/1062 Duplicate entry '([^\-]+)/i", $e->getMessage(), $dupCartons, PREG_OFFSET_CAPTURE);
                if (count($dupCartons) > 1) {
                    unset($dupCartons[0]);
                    $errorRfids = [];
                    foreach ($dupCartons as $dupCarton) {
                        $errorRfids[] = array_first($dupCarton);
                    }

                    $msg = sprintf("Duplicated Carton Rfid %s. Unfortunately, It exists in another warehouse.", implode(', ', $errorRfids));

                    return $this->_responseErrorMessage($msg);
                }
            }

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $arrCtnRfid
     *
     * @return array
     */
    public function updateWaveDtl($whsId, $arrCtnRfid)
    {
        $wvIds = [];
        foreach ($arrCtnRfid as $ctnRfid) {
            $checkCartonsExist = $this->cartonModel->checkExistedCartonAccordingToRFID($ctnRfid, $whsId);
            if (strtoupper(object_get($checkCartonsExist, 'ctn_sts', '')) == 'PD'
            ) {
                //Check Carton is assigned to order or not
                $orderCarton = $this->orderCartonModel->getFirstBy('ctn_rfid', $ctnRfid);
                if ($orderCarton) {
                    $odrHdrId = object_get($orderCarton, 'odr_hdr_id', '');
                    $wvDtlId = object_get($orderCarton, 'wv_dtl_id', '');
                    $wvId = object_get($orderCarton, 'wv_hdr_id', '');
                    $wvIds[] = $wvId;
                    $wvHdrSts = "";

                    // update wavepick is picking with v1 update wavepick
                    if ($wvId) {
                        $wvObj = $this->wavePickHdrModel->getFirstWhere(['wv_id' => $wvId]);
                        $wvHdrSts = $wvObj->wv_sts;
                        if ($wvObj->wv_sts == "CO") {
                            $wvObj->wv_sts = 'PK';
                            $wvObj->save();
                        }
                    }

                    // update carton in wavepick detail location
                    // $this->_updateWvDtlLocInReturn([$orderCarton->toArray()], $wvDtlId);

                    $returnQTY = object_get($orderCarton, 'piece_qty', '');
                    $pieceQtyOrdCtn = object_get($orderCarton, 'piece_qty', '');
                    if ($odrHdrId) {
                        $getorderInfo = $this->odrHdrModel->getFirstBy('odr_id', $odrHdrId);
                        if ($getorderInfo) {
                            $odrSts = object_get($getorderInfo, 'odr_sts', '');
                            if ($odrSts == 'CC') {
                                //update odr_carton status to PB
                                $this->orderCartonModel->updateOrderCarton($ctnRfid);
                                //update wave pick
                                $wavePickDtlinfo = $this->wavePickDtlModel->getFirstBy('wv_dtl_id', $wvDtlId);
                                $actPieceQty = object_get($wavePickDtlinfo, 'act_piece_qty', '');
                                $actPieceQtyNew = $actPieceQty - $pieceQtyOrdCtn;
                                $pieceQty = object_get($wavePickDtlinfo, 'piece_qty', 0);

                                $this->_updateWvDtl($wvDtlId, $actPieceQtyNew, $pieceQty, $wvHdrSts);

                                //Update inventory summary when canceled order
                                $checkUpdateInvtsum = true;
                                $this->_updateInvtSmrCancelOrder($orderCarton, $returnQTY, $whsId);
                            } else {
                                // update invetory summary
                                $wvDtl = $this->wavePickDtlModel->getProperlyWvDtlInReturn($wvId, $wvDtlId, $whsId);
                                if (count($wvDtl) > 0) {
                                    $this->_updateInvtSmrReturnPicking($wvDtl, $returnQTY);
                                }
                            }
                        }
                    } else {
                        // update invetory summary
                        $wvDtl = $this->wavePickDtlModel->getProperlyWvDtlInReturn($wvId, $wvDtlId, $whsId);
                        if (count($wvDtl) > 0) {
                            $this->_updateInvtSmrReturnPicking($wvDtl, $returnQTY);
                        }
                        //update odr_carton status to PB
                        $this->orderCartonModel->updateOrderCarton($ctnRfid);
                        //update wave pick
                        $wavePickDtlinfo = $this->wavePickDtlModel->getFirstBy('wv_dtl_id', $wvDtlId);
                        $actPieceQty = object_get($wavePickDtlinfo, 'act_piece_qty', '');
                        $actPieceQtyNew = $actPieceQty - $pieceQtyOrdCtn;
                        $pieceQty = object_get($wavePickDtlinfo, 'piece_qty', 0);

                        $this->_updateWvDtl($wvDtlId, $actPieceQtyNew, $pieceQty, $wvHdrSts);
                    }
                }
            }
        }

        $wvIds = array_unique($wvIds);
        $wavePickPickeds = [];
        if (count($wvIds) > 0) {
            foreach ($wvIds as $wvId) {
                if ($wvId) {
                    //update wave header status
                    $wvSts = $this->wavePickHdrModel->updatePicked($wvId);

                    // set wavepick status is cancel
                    // 1. Wavepick that  is not overpick will change to Completed when all orders are picked
                    // 2. Wavepick that  is not overpick will change to Canceled when all orders are canceled
                    // Else wavepick is PICKING
                    // if ($wvSts == 0) {
                    $wvSts2 = $this->wavePickHdrModel->updateCanceled($wvId);
                    // }

                    $wavePickPickeds[] = [
                        'wv_id'  => $wvId,
                        'picked' => $wvSts,
                    ];
                }
            }
        }

        return $wavePickPickeds;
    }

    /**
     * @param $cartons
     * @param $wvDtlId
     */
    private function _updateWvDtlLocInReturn($cartons, $wvDtlId)
    {
        $wvDtlLoc = (new WaveDtlLocModel())->getModel()->where('wv_dtl_id', $wvDtlId)->first();

        if ($wvDtlLoc && $wvDtlLoc->act_loc_ids) {
            $actLocs = json_decode($wvDtlLoc->act_loc_ids, true);
            $ctnIdsCheck = array_pluck($cartons, 'ctn_id');
            foreach ($actLocs as $keyLoc => $actLoc) {
                foreach ($actLoc['cartons'] as $keyCtn => $cartonOld) {
                    if (in_array($cartonOld['ctn_id'], $ctnIdsCheck)) {
                        // remove carton
                        unset($actLocs[$keyLoc]['cartons'][$keyCtn]);
                        // update location
                        foreach ($cartons as $carton) {
                            if ($cartonOld['ctn_id'] == $carton['ctn_id']) {
                                // return QTY
                                $actLocs[$keyLoc]['picked_qty'] -= $carton['piece_qty'];
                                break;
                            }
                        }

                    }
                }
                // update carton
                $actLocs[$keyLoc]['cartons'] = array_values($actLocs[$keyLoc]['cartons']);
                if (count($actLocs[$keyLoc]['cartons']) == 0) {
                    // remove location
                    unset($actLocs[$keyLoc]);
                }
            }
            $wvDtlLoc->act_loc_ids = json_encode(array_values($actLocs));

            $wvDtlLoc->update();
        }
    }

    /**
     * @param $wvData
     * @param $returnQTY
     *
     * @throws \Exception
     */
    private function _updateInvtSmrReturnPicking($wvData, $returnQTY)
    {
        $piece_qty = object_get($wvData, 'piece_qty');
        $act_piece_qty = object_get($wvData, 'act_piece_qty');
        $item_id = object_get($wvData, 'item_id');
        $whs_id = object_get($wvData, 'whs_id');
        $lot = object_get($wvData, 'lot');

        $returnOverPicking = 0;
        // piece qty < actual picked

        if ($piece_qty <= $act_piece_qty - $returnQTY) {
            // over picking
            $returnOverPicking = $returnQTY;
        } elseif ($act_piece_qty > $piece_qty) {
            // over picking a part
            $returnOverPicking = $act_piece_qty - $piece_qty;
        }

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $item_id,
                'whs_id'  => $whs_id,
                'lot'     => $lot,
            ]
        );

        if (!$ivt) {
            $msg = sprintf("Not found Inventory with {Item Id, Warehouse Id, LOT}:{%s, %s, %s}", $item_id, $whs_id, $lot);
            throw new \Exception($msg);
        }

        $invtAvail = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;
        $invtPicked = (int)$ivt->picked_qty;

        $wvDataArr = json_decode($wvData->data, true);
        $wvDataArr = json_decode($wvData->data, true);
        if (!$wvDataArr) {
            $wvDataArr['avail'] = 0;
            $wvDataArr['allocated'] = 0;
        }

        $returnAllocatedQTY = 0;
        $returnAvailQTY = 0;

        // return picking < overpicking
        if ($returnOverPicking < $wvDataArr['allocated'] + $wvDataArr['avail']) {
            // return to available is higher priority
            if ($returnOverPicking < $wvDataArr['avail']) {
                $returnAvailQTY = $returnOverPicking;
            } else {
                $returnAllocatedQTY = $returnOverPicking - $wvDataArr['avail'];
                $returnAvailQTY = $wvDataArr['avail'];
            }
        } else {
            // return to inventory allocated Qty a part
            $returnAllocatedQTY = $wvDataArr['allocated'];
            $returnAvailQTY = $returnOverPicking - $wvDataArr['allocated'];
        }

        $wvDataArr['allocated'] = $wvDataArr['allocated'] - $returnAllocatedQTY;
        $wvDataArr['avail'] = $wvDataArr['avail'] - $returnAvailQTY;
        $wvDataArr['avail'] = $wvDataArr['avail'] < 0 ? 0 : $wvDataArr['avail'];

        $wvData->data = json_encode($wvDataArr);
        $wvData->update();

        $reducePicked = $returnQTY;
        $increaseAvail = $returnAvailQTY;
        if ($invtPicked < $returnQTY) {
            $reducePicked = $invtPicked;
            $increaseAvail += $returnQTY - $invtPicked;
            // $msg = "Not enough picked QTY to return picking";
            // throw new \Exception($msg);
        }

        $allocatedSql = sprintf('`allocated_qty` + %d', $returnQTY - $returnAvailQTY);
        $pickedSql = sprintf('`picked_qty` - %d', $reducePicked);
        $availSql = sprintf('`avail` + %d', $increaseAvail);

        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $item_id,
            'whs_id'  => $whs_id,
            'lot'     => $lot,
        ]);

        // correct Actual Allocated Qty
        $this->inventorySummaryModel->correctActualAllocatedQty(
            $item_id,
            $item_id,
            $lot
        );
    }

    private function _updateInvtSmrCancelOrder($orderCarton, $returnQTY, $whs_id)
    {
        $item_id = object_get($orderCarton, 'item_id');
        //$whs_id = object_get($orderCarton, 'whs_id');
        $lot = object_get($orderCarton, 'lot');

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $item_id,
                'whs_id'  => $whs_id,
                'lot'     => $lot,
            ]
        );

        $invtAvail = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;
        $invtPicked = (int)$ivt->picked_qty;


        $reducePicked = $returnQTY;
        $increaseAvail = $returnQTY;
        if ($invtPicked < $returnQTY) {
            $reducePicked = $invtPicked;
            $increaseAvail += $returnQTY - $invtPicked;
            // $msg = "Not enough picked QTY to return picking";
            // throw new \Exception($msg);
        }

        //$allocatedSql = sprintf('`allocated_qty` + %d', $returnQTY - $returnAvailQTY);
        $pickedSql = sprintf('`picked_qty` - %d', $reducePicked);
        $availSql = sprintf('`avail` + %d', $increaseAvail);

        $this->inventorySummaryModel->updateWhere([
            //'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $item_id,
            'whs_id'  => $whs_id,
            'lot'     => $lot,
        ]);
    }

    private function _updateWvDtl($wvDtlId, $actPieceQtyNew, $pieceQty, $wvHdrSts = "")
    {
        $wvDtlSts = Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS");
        if ($actPieceQtyNew >= $pieceQty) {
            $wvDtlSts = Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS");

            if ($actPieceQtyNew == 0 && in_array($wvHdrSts, ["RT", "CC"])) {
                $wvDtlSts = Status::getByValue("Canceled", "WAVEPICK-DETAIL-STATUS");
            }
        }

        $this->wavePickDtlModel->updateWvDtl($actPieceQtyNew, $wvDtlId, $wvDtlSts);
    }

    private function _executionNewCartonPutback(
        $whsId,
        $cusId,
        $newRfids,
        $itemId,
//        $packSize,
        $sysUom
    ){
        $userId = Data::getCurrentUserId();

        $itemArray = DB::table('item')->where('item_id', $itemId)->first();

        $lastCtnNum = $this->cartonModel->getModel()
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->orderBy('ctn_id', 'DESC')
            ->pluck('ctn_num')
            ->first();
        if (!$lastCtnNum) {
            $lastCtnNum = '00007D000003AC8F27061301';
        }

        $maxCtnNum = $this->cartonModel->getMaxCtnNum($lastCtnNum);

        $dataCtns = [];
        $pieceRemainTtl = 0;
        foreach ($newRfids as $rfid => $pieceRemain)
        {
            $dataCtns[] = [
                'whs_id'        => $whsId,
                'cus_id'        => $cusId,
                'ctn_num'       => ++$maxCtnNum,
                'rfid'          => $rfid,
                'ctn_sts'       => 'AC',
                'ctn_uom_id'    => array_get($sysUom, 'sys_uom_id'),
                'uom_code'      => array_get($sysUom, 'sys_uom_code'),
                'uom_name'      => array_get($sysUom, 'sys_uom_name'),
                'is_damaged'    => 0,
                'piece_remain'  => $pieceRemain,
                'piece_ttl'     => array_get($itemArray, 'pack'),
                'created_at'    => time(),
                'updated_at'    => time(),
                'created_by'    => $userId,
                'updated_by'    => $userId,
                'deleted'       => 0,
                'deleted_at'    => '915148800',
                'is_ecom'       => 0,
                'picked_dt'     => 0,
                'item_id'       => $itemId,
                'sku'           => array_get($itemArray, 'sku'),
                'size'          => array_get($itemArray, 'size'),
                'color'         => array_get($itemArray, 'color'),
                'lot'           => self::DEFAULT_LOT_PUTBACK,
                'ctn_pack_size' => array_get($itemArray, 'pack'),
                'inner_pack'    => 0,
                'upc'           => array_get($itemArray, 'cus_upc'),
                'length'        => array_get($itemArray, 'length'),
                'width'         => array_get($itemArray, 'width'),
                'height'        => array_get($itemArray, 'height'),
                'weight'        => array_get($itemArray, 'weight'),
                'cube'          => array_get($itemArray, 'cube'),
                'volume'        => array_get($itemArray, 'volume'),
                'des'           => array_get($itemArray, 'description'),
            ];

            $pieceRemainTtl += $pieceRemain;
        }

        DB::table('cartons')->insert($dataCtns);

        // reduce pieces in order cartons
        $sqlOrderRaw = sprintf("
            (odr_hdr_id is NULL or odr_hdr_id in
            (select odr_id from odr_hdr AS oh where oh.odr_id = odr_cartons.odr_hdr_id and oh.odr_sts = '%s' and oh.deleted = 0))
                ", "CC");
        $odrCtns = $this->orderCartonModel->getModel()
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->where('item_id', $itemId)
            ->whereRaw($sqlOrderRaw)
            ->whereNull('ctn_rfid')
            ->get();

        $lotPieces = [];
        foreach ($odrCtns as $odrCtn)
        {
            if ($pieceRemainTtl == 0)
            {
                break;
            }

            if ($pieceRemainTtl >= $odrCtn->piece_qty) {
                $lotPieces[$odrCtn->lot][] = $odrCtn->piece_qty;

                $pieceRemainTtl -= $odrCtn->piece_qty;
                $odrCtn->deleted = 1;
                $odrCtn->deleted_at = time();
                $odrCtn->save();
            } else {
                $lotPieces[$odrCtn->lot][] = $pieceRemainTtl;

                $odrCtn->piece_qty -= $pieceRemainTtl;
                $odrCtn->save();
                $pieceRemainTtl = 0;
            }
        }

        if ($pieceRemainTtl > 0) {
        //     $msg = "Putback failed. Not enough picked QTY of Cartons Barcode.";
        //     throw new \Exception($msg);
            // Update inventory summary
            $this->_insertInvtSmr($whsId, $itemArray, self::DEFAULT_LOT_PUTBACK);

            // WAP-824 - [Putback] Able putback new RFID that did not be picked by GW
            $this->_computeIvtSmrNewCarton2($whsId, $cusId, $itemId, self::DEFAULT_LOT_PUTBACK, $pieceRemainTtl);
        }

        foreach ($lotPieces as $lot => $pieces)
        {
            $this->_computeIvtSmrNewCarton($whsId, $cusId, $itemId, $lot, array_sum($pieces));
        }
    }

    private function _computeIvtSmrNewCarton($whsId, $cusId, $itemId, $lot, $pieceQty)
    {
        $ivtSmr = $this->inventorySummaryModel->getFirstWhere([
            'whs_id'    => $whsId,
            'cus_id'    => $cusId,
            'item_id'   => $itemId,
            'lot'       => $lot,
        ]);

        if ($ivtSmr) {
            $ivtSmr->picked_qty -= $pieceQty;
            $ivtSmr->avail += $pieceQty;
            if ($ivtSmr->picked_qty < 0) {
                $ivtSmr->ttl += (-1) * $ivtSmr->picked_qty;
                $ivtSmr->picked_qty = 0;
                // $msg = "Putback failed. Not enough picked QTY of Cartons Barcode in Inventory.";
                // throw new \Exception($msg);
            }
            $ivtSmr->save();
        }
    }

    private function _computeIvtSmrNewCarton2($whsId, $cusId, $itemId, $lot, $pieceQty)
    {
        $ivtSmr = $this->inventorySummaryModel->getFirstWhere([
            'whs_id'    => $whsId,
            'cus_id'    => $cusId,
            'item_id'   => $itemId,
            'lot'       => $lot,
        ]);

        if ($ivtSmr) {
            $ivtSmr->avail += $pieceQty;
            $ivtSmr->ttl += $pieceQty;
            $ivtSmr->save();
        }
    }

    private function  _insertCcn($whsId, $locObj, $pltObj, $msg)
    {
        $userId = Data::getCurrentUserId();
        $listCartons = $this->cartonModel->getModel()
            ->where('plt_id', $pltObj->plt_id)
            ->groupBy('item_id')
            ->groupBy('lot')
            ->get();

        $listReason = (new Reason())->getModel()->lists('r_name', 'r_id');
        // $listCartonUnique = $listCartons->unique('item_id');

        foreach ($listCartons as $carton) {
            $dataCCN[] = [
                'whs_id'      => $whsId,
                'cus_id'      => $pltObj->cus_id,
                'loc_id'      => $locObj->loc_id,
                'loc_code'    => $locObj->loc_code,
                'cc_ntf_sts'  => 'NW',
                'cc_ntf_date' => time(),
                'created_at'  => time(),
                'created_by'  => $userId,
                'updated_at'  => time(),
                'updated_by'  => $userId,
                'deleted_at'  => 915148800,
                'deleted'     => 0,
                'reason'      => isset($listReason[$locObj->reason_id]) ? $listReason[$locObj->reason_id] : 'Inactive setting',
                'item_id'     => $carton['item_id'],
                'sku'         => $carton['sku'],
                'lot'         => $carton['lot'],
                'pack'        => $carton['ctn_pack_size'],
                'size'        => $carton['size'],
                'color'       => $carton['color'],
                'uom_code'    => $carton['uom_code'],
                'uom_name'    => $carton['uom_name'],
                'remain_qty'  => $carton['piece_remain'],
                'des'         => $msg,
            ];
        }

        DB::table('cc_notification')->insert($dataCCN);
    }

    private function _updateCtnPBNum($ctnRfids)
    {
        $sql = sprintf("UPDATE cartons SET ctn_num = concat(ctn_num, '-01') where rfid in ('%s') and deleted = 0 and exists (select * from pack_hdr where pack_hdr.pack_hdr_num = cartons.ctn_num)", implode("', '", $ctnRfids));
        return DB::statement($sql);
    }


    private function _insertInvtSmr($whsId, $itemArr, $lot)
    {
        $itemId = array_get($itemArr, 'item_id');

        $ivt = $this->inventorySummaryModel
            ->getFirstWhere([
                'item_id' => $itemId,
                'lot'     => $lot,
                'whs_id'  => $whsId
            ]);

        if (!$ivt) {
            $insertInvtSmr[] = [
                'item_id'       => $itemId,
                'cus_id'        => array_get($itemArr, 'cus_id'),
                'whs_id'        => $whsId,
                'color'         => array_get($itemArr, 'color'),
                'sku'           => array_get($itemArr, 'sku'),
                'size'          => array_get($itemArr, 'size'),
                'lot'           => $lot,
                'ttl'           => 0,
                'allocated_qty' => 0,
                'dmg_qty'       => 0,
                'avail'         => 0,
                'back_qty'      => 0,
                'upc'           => array_get($itemArr, 'cus_upc'),
                'crs_doc_qty'   => 0,
                'created_at'    => time(),
                'updated_at'    => time(),
            ];

            DB::table('invt_smr')->insert($insertInvtSmr);
        }

    }

    private function _processCartonsCCTC($ctnObj, $cusId)
    {
        $itemId = object_get($ctnObj, 'item_id');
        $lot = object_get($ctnObj, 'lot');
        $rfidReal = object_get($ctnObj, 'rfid');
        $odrCarton = $this->orderCartonModel->getModel()
            ->where('item_id', $itemId)
            ->where('lot', $lot)
            ->where('ctn_rfid', 'like', 'CCTC%')
            ->whereNull('odr_hdr_id')
            ->where('cus_id', $cusId)
            ->first();

        if ($odrCarton) {
            $rfidFake = object_get($odrCarton, 'ctn_rfid');
            $ctnIdFake = object_get($odrCarton, 'ctn_id');
            $cartonFake = $this->cartonModel->getFirstWhere(['rfid' => $rfidFake]);
            $cartonFake->rfid = null;
            $cartonFake->save();
            $ctnObj->rfid = $rfidFake;
            $ctnObj->deleted_at = time();
            $ctnObj->deleted = 1;
            $ctnObj->save();
            $cartonFake->rfid = $rfidReal;
            $cartonFake->save();
            $odrCarton->ctn_id = $ctnObj->ctn_id;
            $odrCarton->ctn_rfid = $rfidReal;
            $odrCarton->save();

            // $packHdr = $this->packHdrModel->updateWhere(
            //         ['cnt_id'     => $ctnObj->ctn_id],
            //         [
            //                     'cnt_id'     => $ctnIdFake,
            //                     'cus_id' => $cusId,
            //                 ]
            //             );

        } else {
            $odrCarton = $this->orderCartonModel->getModel()
                ->where('ctn_rfid', $rfidReal)
                ->whereNotNull('odr_hdr_id')
                ->first();
            if ($odrCarton) {
                return $msg = sprinft("Unable to putback carton %s. Please use cancel order at first.", $rfidReal);
            } else {
                $ctnObj->ctn_sts = "AC";
                $ctnObj->save();
            }
        }
    }

    private function updateInvtSummaryWhenPutBack($carton, $whsId){
        $carton = (object)$carton;

        $odrCarton = $this->orderCartonModel->getModel()
                    ->where('ctn_rfid', $carton->ctn_rfid)
                    ->first();

        if (!$odrCarton){
            return;
        }

        if ( !$odrCarton->wv_dtl_id && $odrCarton->ctn_sts === 'PD' ) {
            $invtSummary = $this->inventorySummaryModel->getModel()
                        ->where('item_id', $odrCarton->item_id)
                        ->where('whs_id', $whsId)
                        ->where('lot', $odrCarton->lot)
                        ->first();

            $piece_remain = $invtSummary->picked_qty - $carton->piece_remain;
            $invtSummary->update([
                'picked_qty'    => $piece_remain > 0 ? $piece_remain : 0,
                'avail'         => $invtSummary->avail + $carton->piece_remain
            ]);
        }
    }
}
