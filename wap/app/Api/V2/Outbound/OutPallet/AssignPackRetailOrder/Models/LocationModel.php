<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\Status;

class LocationModel extends AbstractModel
{
    /**
     * LocationModel constructor.
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }
}
