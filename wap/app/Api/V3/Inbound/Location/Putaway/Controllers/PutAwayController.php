<?php

namespace App\Api\V3\Inbound\Location\Putaway\Controllers;

use App\Api\V3\Inbound\Location\Putaway\Models\AsnDtlModel;
use App\Api\V3\Inbound\Location\Putaway\Models\CartonModel;
use App\Api\V3\Inbound\Location\Putaway\Models\EventTrackingModel;
use App\Api\V3\Inbound\Location\Putaway\Models\GoodsReceiptDetailModel;
use App\Api\V3\Inbound\Location\Putaway\Models\GoodsReceiptModel;
use App\Api\V3\Inbound\Location\Putaway\Models\LocationModel;
use App\Api\V3\Inbound\Location\Putaway\Models\PalletModel;
use App\Api\V3\Inbound\Location\Putaway\Models\PalletSuggestLocationModel;
use App\Api\V3\Inbound\Location\Putaway\Models\VirtualCartonModel;
use App\Api\V3\Inbound\Location\Putaway\Models\Log;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\MyHelper;

class PutAwayController extends AbstractController
{
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var VirtualCartonModel
     */
    protected $virtualCartonModel;

    protected $palletSuggestLocationModel;

    protected $eventTrackingModel;

    /**
     * PutAwayController constructor.
     *
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param VirtualCartonModel $virtualCartonModel
     */
    public function __construct(
        PalletModel $palletModel,
        LocationModel $locationModel,
        VirtualCartonModel $virtualCartonModel
    ) {
        $this->palletModel = $palletModel;
        $this->cartonModel = new CartonModel();
        $this->locationModel = $locationModel;
        $this->virtualCartonModel = $virtualCartonModel;
        $this->eventTrackingModel = new EventTrackingModel();
        $this->palletSuggestLocationModel = new PalletSuggestLocationModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function putAway($whsId, Request $request)
    {
        $url = "/v3/whs/$whsId/location/rack/put-pallet";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'PAW',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Put pallet to RACK'
        ]);

        $input = $request->getParsedBody();
        $type = array_get($input, 'type');
        if (!in_array($type, [1, 2])) {
            $msg = "Type is required. Just including 1(RFID) / 2(Barcode)";
            return [
                'status' => false,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => -1,
                        'msg' => $msg
                    ]
                ],
                'data' => []
            ];
        }
        if ('' == trim(array_get($input, 'pallet_rfid')) || array_get($input, 'pallet_rfid') == null) {
            return [
                'status' => false,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => -1,
                        'msg' => 'pallet_rfid is required!'
                    ]
                ],
                'data' => []
            ];
        }

        if ('' == trim(array_get($input,'location')) || array_get($input,'location') == null) {
            return [
                'status' => false,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => -1,
                        'msg' => 'location field is required!'
                    ]
                ],
                'data' => []
            ];
        }

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($input['pallet_rfid'], RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            return [
                'status' => false,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => -1,
                        'msg' => $pltRFIDValid->error
                    ]
                ],
                'data' => []
            ];
        }

        //validate location RFID
        if ($type == 1) {
            $locRFIDValid = new RFIDValidate($input['location'], RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                return [
                    'status' => false,
                    'iat'    => time(),
                    'messages' => [
                        [
                            'status_code' => -1,
                            'msg' => $locRFIDValid->error
                        ]
                    ],
                    'data' => []
                ];
            }
        }

        // Get Pallet by Rfid
        $pallet = $this->palletModel->getModel()->where([
            'rfid'    => $input['pallet_rfid'],
            'whs_id'  => $whsId,
//            'plt_sts' => 'AC',
        ])->where('ctn_ttl', '>', 0)->first();

        if (!$pallet) {
            return [
                'status' => false,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => -1,
                        'msg' => sprintf("The pallet RFID %s doesn't exist, Please try to pass through the inbound gateway again!", $input['pallet_rfid'])
                    ]
                ],
                'data' => []
            ];
        }

        $cusId = object_get($pallet, 'cus_id', '');

        //Check existed for The loc RFID
        $locationInfo = '';
        if ($type == 1) {
            $locationInfo = $this->locationModel->getRackLocationByLocRFID($whsId, $input['location']);
        } else {
            $locationInfo = $this->locationModel->getFirstWhere([
                                'loc_whs_id' => $whsId,
                                'loc_code'   => $input['location']]);
        }
        if (empty($locationInfo)) {
            return [
                'status' => false,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => -1,
                        'msg' => sprintf("The location %s doesn't exist!", $input['location'])
                    ]
                ],
                'data' => []
            ];
        }

        if ($locationInfo && (!$locationInfo->rfid || $locationInfo->rfid == '')) {
            $msg = sprintf("Location %s does not be Rfid",
                $locationInfo->loc_code);
            return $this->_responseErrorMessage($msg);
        }

        $location = $this->locationModel->getRackLocationByCustomerById($whsId, $locationInfo->loc_id, $cusId);
        $user = (new Data())->getUserInfo();

        $returnData = [];
        try {
            DB::beginTransaction();

            if (empty($location)) {
                return [
                    'status' => false,
                    'iat'    => time(),
                    'messages' => [
                        [
                            'status_code' => -1,
                            'msg' => sprintf("The location %s does not belong to this customer.", $input['location'])
                        ]
                    ],
                    'data' => []
                ];
            } elseif ($location->loc_sts_code != 'AC') {
                $msg = sprintf("Location %s is not active. Current Status %s",
                    $location->loc_code,
                    $location->loc_sts_code);
                return [
                    'status' => false,
                    'iat'    => time(),
                    'messages' => [
                        [
                            'status_code' => -1,
                            'msg' => $msg
                        ]
                    ],
                    'data' => []
                ];
            }

            $chkPlt = $this->palletModel->getModel()->where([
                'loc_id' => $location->loc_id,
                'whs_id' => $whsId
            ])
                // ->where('rfid', '<>', $input['pallet_rfid'])
//                ->where('ctn_ttl', '>', 0)
//                ->where('plt_sts', 'AC')
                ->first();

            if ($chkPlt && ($chkPlt->rfid <> $input['pallet_rfid'] || $chkPlt->rfid == null)) {
                $msg = sprintf("There is a pallet %s in this location %s", $chkPlt->plt_num, $location->loc_code);
                return [
                    'status' => false,
                    'iat'    => time(),
                    'messages' => [
                        [
                            'status_code' => -1,
                            'msg' => $msg
                        ]
                    ],
                    'data' => []
                ];
            }

            // Update Pallet
            $this->palletModel->refreshModel();
            $this->palletModel->updateWhere([
                    'loc_id'   => $location->loc_id,
                    'loc_code' => $location->loc_code,
                    'loc_name' => $location->loc_alternative_name
                ],
                [
                    'rfid'             => $input['pallet_rfid'],
                    // 'plt_sts'          => 'AC',
                    'storage_duration' => 0
                ]);

            // Update Carton
            $data = [
                'loc_id'        => $location->loc_id,
                'loc_code'      => $location->loc_code,
                'loc_name'      => $location->loc_alternative_name,
                'loc_type_code' => Status::getByValue('RACK', 'LOC_TYPE_CODE'),
                'plt_id'        => $pallet->plt_id
            ];

            $this->cartonModel->updateCartonWithPltID($data);

            // turn on putaway when enough pallets have put on rack
            $this->_turnOnPutAway($pallet->gr_hdr_id);

            //unlock this location
            $this->locationModel->updateWhere(
                [
                    'loc_sts_code' => Status::getByKey('LOCATION_STATUS', 'ACTIVE'),
                ],
                [
                    'loc_id' => $location->loc_id
                ]
            );

            //insert latest pallet to RAC and put to table pal_sug_loc
            $grObj = (new GoodsReceiptDetailModel())->getModel()
                ->where('gr_dtl_id', $pallet->gr_dtl_id)
                ->join('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'gr_dtl.gr_hdr_id')
                ->select('gr_hdr_num', 'item_id', 'sku', 'size', 'color', 'lot')
                ->first();

            if ($grObj) {
                $this->initPalletSugLoc($pallet, $location, $grObj);
            }

            //insert cc_notification
            $listVirtualCarton = $this->virtualCartonModel->findWhere([
                'plt_rfid' => $input['pallet_rfid']
            ]);

            // $listCartons = $this->cartonModel->findWhere([
            //     'plt_id' => $pallet->plt_id
            // ]);
            $listCartons = $this->cartonModel->getModel()
                        ->where('plt_id', $pallet->plt_id)
                        ->whereIn('ctn_sts', ['AC', 'LK', 'RG'])
                        ->groupBy('item_id')
                        ->groupBy('lot')
                        ->get();

            $listCtnRfidVtlCtn = array_pluck($listVirtualCarton->toArray(), 'ctn_rfid');
            $listCtnRfidCarton = array_pluck($listCartons->toArray(), 'rfid');

            if (array_intersect($listCtnRfidCarton, $listCtnRfidVtlCtn) && count($listCartons) > 0) {
                $listReason = (new Reason())->getModel()->lists('r_name', 'r_id');
                // $listCartonUnique = $listCartons->unique('item_id');

                foreach ($listCartons as $carton) {
                    $dataCCN[] = [
                        'whs_id'      => $whsId,
                        'cus_id'      => $pallet->cus_id,
                        'loc_id'      => $location->loc_id,
                        'loc_code'    => $location->loc_code,
                        'cc_ntf_sts'  => 'NW',
                        'cc_ntf_date' => time(),
                        'created_at'  => time(),
                        'created_by'  => $user['user_id'],
                        'updated_at'  => time(),
                        'updated_by'  => $user['user_id'],
                        'deleted_at'  => 915148800,
                        'deleted'     => 0,
                        'reason'      => isset($listReason[$location->reason_id]) ? $listReason[$location->reason_id] : 'Inactive setting',
                        'item_id'     => $carton['item_id'],
                        'sku'         => $carton['sku'],
                        'lot'         => $carton['lot'],
                        'pack'        => $carton['ctn_pack_size'],
                        'size'        => $carton['size'],
                        'color'       => $carton['color'],
                        'uom_code'    => $carton['uom_code'],
                        'uom_name'    => $carton['uom_name'],
                        'remain_qty'  => $carton['piece_remain'],
                        'des'         => $pallet->ccn,
                    ];
                }

                DB::table('cc_notification')->insert($dataCCN);

                // Update ccn => null
                $this->palletModel->updateWhere([
                    'ccn'   => null
                ],
                [
                    'rfid'             => $input['pallet_rfid'],
                    // 'plt_sts'          => 'AC',
                    'storage_duration' => 0
                ]);
            }

            $returnData = $this->cartonModel->getCartonsInfoByPalletRFID($whsId, $pallet->plt_id);

            //write even tracking
            $vtlObj = $this->virtualCartonModel->getASNByVtlCtn($input['pallet_rfid']);
            $dataEvt = [
                'whs_id'    => object_get($vtlObj, 'whs_id', null),
                'cus_id'    => object_get($vtlObj, 'cus_id', null),
                'owner'     => object_get($vtlObj, 'gr_hdr_num', ''),
                'evt_code'  => 'PUT',
                'trans_num' => object_get($vtlObj, 'ctnr_num', ''),
                'info'      => sprintf('Move %s to %s', $input['pallet_rfid'], $location->loc_code)
            ];

            //call Event tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($dataEvt);

            DB::commit();
            $msg = sprintf("Pallet %s has been put on location %s", $input['pallet_rfid'], $location->loc_code);

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => $msg
                    ]
                ],
                'data' => [
                    [
                        'skus' => $returnData,
                        'location' => $location->loc_code
                    ]
                ]
            ];
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * checking enough pallets have put on rack then turn on putaway within gr_hdr table
     *
     * @param $grHdrId
     *
     * @throws \Exception
     */
    private function _turnOnPutAway($grHdrId)
    {
        try {
            $grHdrObj = (new GoodsReceiptModel)->getModel()->where('gr_hdr_id', $grHdrId)->first();
            if ($grHdrObj) {
                $grStatus = object_get($grHdrObj, 'gr_sts');
                if ($grStatus == Status::getByKey('GR_STATUS', 'RECEIVED')) {
                    // // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                    // if ($grHdrObj && $grHdrObj->putaway != 2) {
                    //     $grHdrObj->putaway = 2;
                    //     $grHdrObj->save();
                    // }
                    return;
                }
                $totalPallet = (new GoodsReceiptDetailModel())->palletTotalOfGrHdr($grHdrId);
                $countPallet = $this->palletModel->countPalletOnRackByGRHdrId($grHdrId);

                // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                $grHdrObj->putaway = 2;
                if ($totalPallet == $countPallet) {
                    $grHdrObj->putaway = 1;
                }
                $grHdrObj->save();
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Input last pallet to RAC and input data into table pal_sug_log
     *
     * @param $pallet
     * @param $location
     * @param $grObj
     *
     * @throws \Exception
     */
    private function initPalletSugLoc($pallet, $location, $grObj)
    {
        try {
            $userId = JWTUtil::getPayloadValue('jti');

            if ($palSugLoc = $this->_checkPalletRFIDExistedOnSuggestLocation($pallet->plt_id)) {
                $palSugLoc->act_loc_id   = $location->loc_id;
                $palSugLoc->act_loc_code = $location->loc_code;
                $palSugLoc->save();
                return;
            }

            $suPltLocData = [
                'plt_id'       => $pallet->plt_id,
                'loc_id'       => $location->loc_id,
                'data'         => $location->loc_code,
                'ctn_ttl'      => $pallet->ctn_ttl,
                'item_id'      => object_get($grObj, 'item_id', null),
                'sku'          => object_get($grObj, 'sku', 0),
                'size'         => object_get($grObj, 'size', 'NA'),
                'color'        => object_get($grObj, 'color', 'NA'),
                'lot'          => object_get($grObj, 'lot', 'NA'),
                'putter'       => $userId,
                'gr_hdr_id'    => $pallet->gr_hdr_id,
                'gr_dtl_id'    => $pallet->gr_dtl_id,
                'gr_hdr_num'   => object_get($grObj, 'gr_hdr_num', ''),
                'whs_id'       => $pallet->whs_id,
                'put_sts'      => "CO",
                'act_loc_id'   => $location->loc_id,
                'act_loc_code' => $location->loc_code,
            ];

            $this->palletSuggestLocationModel->refreshModel();
            $this->palletSuggestLocationModel->create($suPltLocData);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function _checkPalletRFIDExistedOnSuggestLocation($pltId)
    {
        $result = $this->palletSuggestLocationModel->getFirstWhere(
            [
                'plt_id' => $pltId,
            ]
        );

        return $result;
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}
