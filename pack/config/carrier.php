<?php

return [
    'aliases' => [
        'fedex' => "App\Services\Fedex",
        'usps' => "App\Services\USPS",
        'ups' => "App\Services\UPS",
    ]
];
