<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class DailyPalletRpt extends AbstractCommand
{
    protected $signature   = 'daily-pallet-rpt {today?}';
    protected $description = 'Location Histogram successfully.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            $this->logSuccess('start-daily-pallet-rpt');

            $today     = $this->argument('today') ? strtotime($this->argument('today')) : strtotime(date('Y-m-d', time()));
            $yesterday = $today - 86400;
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $customerWhss = DB::table('customer_warehouse AS cw')
                ->select('cw.cus_id', 'cw.whs_id')
                ->join('customer as c', 'c.cus_id', '=', 'cw.cus_id')
                ->where("c.deleted", 0)
                ->where("cw.deleted", 0)
                ->get();

            if ($customerWhss) {
                foreach ($customerWhss as $cusWhs) {
                    $cusId = $cusWhs['cus_id'];
                    $whsId = $cusWhs['whs_id'];
                    DB::statement("CALL update_daily_plt_rpt({$whsId}, {$cusId}, {$yesterday})");
                }
            }

            $this->logSuccess('daily-pallet-rpt');

        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }

    }
}
