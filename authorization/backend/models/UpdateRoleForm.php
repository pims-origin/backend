<?php
namespace backend\models;

use common\models\Authority;
use Yii;
use yii\web\BadRequestHttpException;

class UpdateRoleForm extends CreateRoleForm
{
    public $update_name;

    public function safeAttributes()
    {
        $attributes = parent::safeAttributes();

        return array_merge($attributes, [
            'permissions',
            'update_name'
        ]);
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description', 'code'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['code'], 'string', 'max' => 20],
        ];
    }

    public function updateRole(Authority $model)
    {
        if (!$this->validate()) {
            return null;
        }

        try {
            $transaction = Yii::$app->db->beginTransaction();

            $model->description = $this->description;
            $model->code = $this->code;
            $model->updated_at = time();
            if ($this->name !== $this->update_name) {
                //$model->name = $this->update_name;
                throw new BadRequestHttpException('Role name can\'t edited');
            }

            $result = $model->save();

            $auth = Yii::$app->authManager;
            $parent = $auth->getRole($model->name);
            $permissions = $this->permissions;

            if (!$parent) {
                $transaction->rollBack();
                throw new BadRequestHttpException('Role not found');
            }

            //Remove all permission from role
            $auth->removeChildren($parent);

            foreach ($permissions as $permission) {
                $child = $auth->getPermission($permission);
                $auth->addChild($parent, $child);
            }

            $transaction->commit();
        } catch (Yii\base\Exception $ex) {
            $transaction->rollBack();
            if ($ex->getCode() === 23000) {
                $errMsg = $ex->errorInfo[2];
                if (strpos($errMsg, "'code'") === false) {
                    throw new BadRequestHttpException("The Role Name is existed", 404);
                }

                throw new BadRequestHttpException("The Role Code is existed", 404);
            }

            throw new BadRequestHttpException('Have Error In Create Role.', 404);
        }

        return $result;
    }
}
