<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\CustomerContact;

use Seldat\Wms2\Utils\SelArr;

class ClientContactsModel extends AbstractModel
{
    protected $model;

    public function __construct(
        CustomerContact $model = NULL
    )
    {
        $this->model = ($model) ?: new CustomerContact();
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $query = $this->make($with)
                ->select(
                    'cus_ctt_id',
                    'cus_ctt_dft',
                    'cus_ctt_fname',
                    'cus_ctt_lname',
                    'cus_ctt_email',
                    'cus_ctt_phone',
                    'cus_ctt_mobile',
                    'cus_ctt_ext',
                    'cus_ctt_position',
                    'cus_ctt_cus_id',
                    'cus_ctt_department'
                )
                ->where('cus_ctt_cus_id', $searchAttr['cus_id']);

        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /*
    ****************************************************************************
    */

    function checkEmptyInput($data, $fields)
    {
        $filtered = array_filter($data);

        $filteredKeys = array_keys($filtered);

        $emptyFields = array_diff($fields, $filteredKeys);

        if ($emptyFields) {
            return 'Empty input: ' . implode(', ', $emptyFields);
        }
    }

    /*
    ****************************************************************************
    */

    function setDafaultContact($contactID, $clientID)
    {
        DB::transaction(function() use ($contactID, $clientID) {

            $this->model
                    ->where('cus_ctt_cus_id', $clientID)
                    ->update([
                        'cus_ctt_dft' => 0
                    ]);

            $this->model
                    ->where('cus_ctt_id', $contactID)
                    ->where('cus_ctt_cus_id', $clientID)
                    ->update([
                        'cus_ctt_dft' => 1
                    ]);
        });

        return $this->model;
    }

    /*
    ****************************************************************************
    */

    function addContact($clientID, $contactInfo)
    {
        $contactInfo['cus_ctt_cus_id'] = $clientID;

        $this->model->create($contactInfo);

        return $this->model;
    }

    /*
    ****************************************************************************
    */

    function removeContacts($contacts)
    {
        $this->model
                ->whereIn('cus_ctt_id', $contacts)
                ->delete();

        return $this->model;
    }

    /*
    ****************************************************************************
    */

}

