<?php

use Illuminate\Database\Seeder;
use App\Api\V1\Entities\WarehouseStatus;
use App\Api\V1\Entities\Link;
use App\Api\V1\Entities\Project;
use App\Api\V1\Entities\Note;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UserTableSeeder');
    }
}
