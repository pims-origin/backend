<?php

namespace App\Modules\Customers\Eloquents;

use Illuminate\Database\Eloquent\Model;

class CustomerZone extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_zone';

    protected $fillable = [
        'cus_id',
        'zone_id'
    ];

    public function zone()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Zone', 'zone_id', 'zone_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function location()
    {
        return $this->hasMany(__NAMESPACE__ . '\Location', 'loc_zone_id', 'zone_id');
    }
}
