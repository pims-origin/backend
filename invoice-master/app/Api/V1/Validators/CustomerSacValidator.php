<?php

namespace App\Api\V1\Validators;

class CustomerSacValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_id'        => 'required|integer|exists:customer,cus_id',
            'whs_id'        => 'required|integer|exists:warehouse,whs_id',
            'sac_id'        => 'required||exists:sac.sac_id',
            'price'         => 'required|numeric|max:15',
            'discount'      => 'numeric|max:15',
        ];;
    }
}
