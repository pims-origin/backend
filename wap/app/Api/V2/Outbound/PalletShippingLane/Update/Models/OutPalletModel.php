<?php
namespace App\Api\V2\Outbound\PalletShippingLane\Update\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class OutPalletModel extends AbstractModel
{


    /**
     * OutPalletModel constructor.
     *
     * @param OutPallet|null $model
     */
    public function __construct(OutPallet $model=null)
    {
        $this->model = ($model) ?: new OutPallet();

    }//end __construct()


    public function getOutPalletByWhsIdAndPltNum($attr)
    {
        return $this->model
            ->where('whs_id', $attr['whs_id'])
            ->where('plt_num', $attr['plt_num'])
            ->whereNotNull('loc_id')
            ->whereNotNull('loc_name')
            ->whereNotNull('loc_code')
            ->get();

    }//end getOutPalletByWhsIdAndPltNum()


    public function updateOutPalletByWhsIdAndPltNum($attr, $data)
    {
        DB::beginTransaction();
        try {
            $statusUpdate = $this->model
                ->where('whs_id', $attr['whs_id'])
                ->where('plt_num', $attr['plt_num'])
                ->whereNotNull('loc_id')
                ->whereNotNull('loc_name')
                ->whereNotNull('loc_code')
                ->update($data);
            DB::commit();
            return $statusUpdate;
        } catch (Exception $q) {
            DB::rollback();
            return false;
        }

    }//end updateOutPalletByWhsIdAndPltNum()


}//end class
