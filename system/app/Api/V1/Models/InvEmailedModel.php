<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\InvEmailed;


class InvEmailedModel extends AbstractModel
{
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new InvEmailed();
    }

    /**
     * @recNum array
     * return mixed
    */

    public function getInvEmailedNew()
    {
        $result = DB::table('inv_emailed')
            //->leftJoin('email_sent','email_sent.owner', '=', 'inv_emailed.inv_email_id')
            ->join('customer','customer.cus_id', '=', 'inv_emailed.cus_id')
            ->whereRaw('inv_emailed.inv_sts = "n" AND (select count(esent_sts) from email_sent where esent_sts = 1 and  owner = inv_emailed.inv_email_id ) = 0 ')
            ->first()
        ;

        return $result;
    }
}
