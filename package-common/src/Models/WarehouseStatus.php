<?php

namespace Seldat\Wms2\Models;

/**
 *
 * @property mixed loc_sts_code
 * @property mixed loc_sts_desc
 */
class WarehouseStatus extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whs_status';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;

    public $incrementing = false;

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'whs_sts_code',
        'whs_sts_name',
        'whs_sts_desc',
    ];

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_status', 'whs_sts_code');
    }
}
