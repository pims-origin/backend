<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CreateInvoicesModel;
use App\Api\V1\Models\ClientMasterModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\OrderModel;
use App\Api\V1\Models\InvoiceListModel;
use App\Api\V1\Models\CommonInvoiceModel;

use App\Api\V1\Transformers\ContainerDetailTransformer;
use App\Api\V1\Transformers\OrderDetailTransformer;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use App\Api\V1\Models\AuthenticationService;

use Psr\Http\Message\ServerRequestInterface as Request;
use mPDF;

class CreateInvoicesController extends AbstractController
{
    /**
    * @var
    */
    protected $invModel;
    protected $cusModel;
    protected $grHdrModel;
    protected $containerTransformer;
    protected $orderTransformer;
    protected $invoiceListModel;

    /**
    *  Constructor
    */

    public function __construct(IRequest $request)
    {
//        $object = new AuthenticationService($request);
//
//        parent::__construct($request, $object);

        $this->invModel = new CreateInvoicesModel();
        $this->cusModel = new ClientMasterModel();
        $this->grHdrModel = new GoodsReceiptModel();
        $this->orderModel = new OrderModel();
        $this->invoiceListModel = new InvoiceListModel();

        $this->containerTransformer = new ContainerDetailTransformer();
        $this->orderTransformer = new OrderDetailTransformer();
    }

    /**
     * Create Invoice
     *
     * @param IRequest $request
     *
     * @return array
     */

    public function storeInvoice(IRequest $request)
    {
        $data = $request->getParsedBody();

        $doubleBilling = $this->invModel->checkDoubleBilling($data);

        if ($doubleBilling) {
            return $doubleBilling;
        }

        $params = [
            'invoiceNo' => $data['inv_num'],
            'cus_id' => $data['cus_id'],
            'whs_id' => $data['whs_id'],
            'billTo' => $data['billTo'],
            'shipTo' => $data['shipTo'],
            'header' => [
                'invDt' => $data['inv_dt'],
                'custRef' => $data['billTo']['cus_code'],
                'terms' => $data['billTo']['net_terms'],
            ],
            'details' => [
               'data' => $data['invoInfo']['details'],
               'titles' => ['ITEM', 'DESC', 'INFO', 'QTY', 'UOM', 'PRICE', 'AMT'],
            ],
            'openItems' => getDefault($data['invoInfo']['items']),
            'items' => $data['items'],
            'storeItems' => $data['storeItems'],
            'invItemsIDs' => $data['invoInfo']['invItemsIDs'],
            'dateRange' => [
                'startDate' => $data['startDate'],
                'endDate' => $data['endDate'],
            ],
        ];

        try {

            $this->invModel->updateInvoiceProcessing($params);

            return [];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
    ****************************************************************************
    */

    public function receivePayment($inv_num, Request $request)
    {
        $paymentInfo = $request->getParsedBody();

        $model = $this->clientContactsModel;
        $transformer = $this->clientContactsTransformer;

        try {

            $results = $model->receivePayment($inv_num, $paymentInfo);

            return $this->response->item($results, $transformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
    ****************************************************************************
    */

    public function printInvoice(Request $request)
    {
        $input = $request->getQueryParams();

        $this->invModel->printInvoice($input);
    }

    /*
    ****************************************************************************
    */

    public function getInvoiceStatementData(Request $request)
    {
        $input = $request->getQueryParams();

        return $this->invModel->getInvoiceStatementData($input);
    }

    /*
    ****************************************************************************
    */

    public function printStatementInvoice(Request $request)
    {
        $input = $request->getQueryParams();

        $this->invModel->printStatementInvoice($input);
    }

    /*
    ****************************************************************************
    */

    public function getReceivingDetails(Request $request)
    {
        $input = $request->getQueryParams();

        try {

            $results = $this->invoiceListModel->getReceivingInvoices([
                'gr_hdr_id' => json_decode($input['gr_hdr_id'], TRUE),
                'print' => getDefault($input['print']),
                'displayDetails' => TRUE,
            ]);

            $details = [];

            foreach ($results as $result) {

                $details[] = [
                    'cus_name' => $result['cust'],
                    'ctnr_num' => $result['ext']['name'],
                    'asn_hdr_ref' => $result['ext']['refCode'],
                    'asn_num' => $result['asnNum'],
                    'gr_num' => $result['grNum'],
                    'carton' => $result['ctnVal'],
                    'pallet' => $result['pltVal'],
                    'kg' => $result['volVal'],
                    'created_at' => $result['dt'],
                ];
            }

            if (getDefault($input['print'])) {
                //using mPDF
                $this->printView('RcvPrintViewDetail', $details);

                return $this->response->noContent();
            }

            return $details;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /*
    ****************************************************************************
    */

    public function printView($template, $details)
    {
        $html = (string)view($template, ['details' => $details]);

        $pdf = new mPDF('utf-8', 'A4-L');

        // output the HTML content
        $pdf->WriteHTML($html);

        //Close and output PDF document

        if (!file_exists(storage_path("DETAIL"))) {
            mkdir(storage_path("DETAIL"), 0777, true);
        }

        $dir = storage_path("DETAIL");
        $fileName = $dir . '/' . $template . '.pdf';

        $pdf->Output($fileName, 'F');
        $pdf->Output($fileName, 'D');

    }

    /*
    ****************************************************************************
    */

    public function getOrderProcessingDetails(Request $request)
    {
        $input = $request->getQueryParams();

        try {

            $results = $this->invoiceListModel->getProcessingInvoices([
                'odr_id' => json_decode($input['odr_id'], TRUE),
                'print' => getDefault($input['print']),
                'displayDetails' => TRUE,
            ]);

            $details = [];

            foreach ($results as $result) {
                $details[] = [
                    'client' => $result['cust'],
                    'cus_name' => $result['ext']['custName'],
                    'odr_num' => $result['ext']['name'],
                    'cust_odr_num' => $result['ext']['custNum'],
                    'client_odr_num' => $result['ext']['clientOrdNum'],
                    'cq' => $result['ext']['cq'],
                    'pq' => $result['ext']['pq'],
                    'plq' => $result['ext']['plq'],
                    'dt' => $result['dt'],
                    'odr_typ' => $result['order'],
                ];
            }

            if (getDefault($input['print'])) {
                //using mPDF
                $this->printView('OrdPrintViewDetail', $details);

               return $this->response->noContent();
            }

            return $details;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /*
    ****************************************************************************
    */
}