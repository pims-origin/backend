<?php

namespace App\Modules\Orders\Lib;

interface OrderImport
{
    public function import(array $data);
}