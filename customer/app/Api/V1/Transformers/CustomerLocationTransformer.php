<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:19
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\Location;
use League\Fractal\TransformerAbstract;

class CustomerLocationTransformer extends TransformerAbstract
{
    /**
     * @param Location $location
     *
     * @return array
     */
    public function transform(Location $location)
    {
        return [
            'loc_id'                 => $location->loc_id,
            'loc_code'               => $location->loc_code,
            'loc_alternative_name'   => $location->loc_alternative_name,
            'loc_whs_id'             => $location->loc_whs_id,
            'whs_name'               => object_get($location, "warehouse.whs_name", null),
            'whs_code'               => object_get($location, "warehouse.whs_code", null),
            'loc_zone_id'            => $location->loc_zone_id,
            'loc_zone_name'          => object_get($location, 'zone.zone_name', null),
            'loc_type_id'            => $location->loc_type_id,
            'loc_type_name'          => object_get($location, 'locationType.loc_type_name', null),
            'loc_sts_code'           => $location->loc_sts_code,
            'loc_sts_code_name'      => object_get($location, 'locationStatus.loc_sts_name', null),
            'loc_sts_code_desc'      => object_get($location, 'locationStatus.loc_sts_desc', null),
            'loc_available_capacity' => $location->loc_available_capacity,
            'loc_width'              => $location->loc_width,
            'loc_length'             => $location->loc_length,
            'loc_height'             => $location->loc_height,
            'loc_max_weight'         => $location->loc_max_weight,
            'loc_weight_capacity'    => $location->loc_weight_capacity,
            'loc_min_count'          => $location->loc_min_count,
            'loc_max_count'          => $location->loc_max_count,
            'loc_desc'               => $location->loc_desc,
            'created_at'             => $location->created_at,
            'updated_at'             => $location->updated_at,
            'deleted_at'             => $location->deleted_at,
            'loc_sts_dtl_reason'     => object_get($location, 'locationStatusDetail.loc_sts_dtl_reason', null),
        ];
    }
}
