<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\Order\CancelOrder\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;


/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class CartonModel extends AbstractModel
{
    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    protected $currentWH;

    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $this->currentWH = $currentWH;
    }

    /**
     * @param $itemId
     * @param $cus_id
     * @param $locTypeId
     *
     * @return mixed
     */
    public function sumPieceRemainByLoc($itemIds, $cus_id)
    {
        $query = DB::table('cartons')
            ->select(DB::raw('SUM(cartons.piece_remain) as piece_remain, cartons.item_id as item_id'))
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->where('cartons.cus_id', $cus_id)
            ->whereIn('cartons.item_id', $itemIds)
            ->where('location.loc_sts_code', '<>', Status::getByKey('LOCATION_STATUS', 'ACTIVE'))
            ->where('location.deleted', 0)
            ->where('cartons.deleted', 0);


        $query->where('cartons.whs_id', $this->currentWH);

        $model = $query->groupBy('cartons.item_id')->get();

        return $model;
    }

    public function getAvailItem($itemId, $ctn_sts = 'AC', $loc_type_code = 'RAC')
    {
        $query = DB::table('cartons')->select(DB::raw("sum(`piece_remain`) as avail"))
            ->where('item_id', $itemId)
            ->where('ctn_sts', $ctn_sts)
            ->where('is_damaged', 0)
            ->where('whs_id', $this->currentWH)->first();
        $avail = array_get($query, 'avail', -1);

        return $avail;
    }

    /**
     * @param $odrId
     * @param array $with
     *
     * @return mixed
     */
    public function getItemForAssign($odrId, $with = [])
    {
        $query = $this->make($with)
            ->select([
                DB::raw("sum(odr_cartons.piece_qty) as piece_ttl"),
                'cartons.item_id',
                'odr_cartons.odr_dtl_id',
                'odr_cartons.odr_hdr_id',
                'cartons.ctn_pack_size',
                'odr_cartons.color',
                'odr_cartons.sku',
                'odr_cartons.size',
                'odr_cartons.lot',
                'odr_cartons.pack',
                'odr_cartons.upc',
                'odr_cartons.uom_id',
                'odr_cartons.uom_code',
            ])
            ->join('odr_cartons', 'odr_cartons.ctn_id', '=', 'cartons.ctn_id')
            ->where('odr_cartons.odr_hdr_id', $odrId)
            ->groupBy('odr_dtl_id');

        return $query->get();
    }


    public function updateCartonPB($ctn_id)
    {
        DB::table('cartons')->where('ctn_sts', 'PD')->whereIn('ctn_id', $ctn_id)->update(['ctn_sts' => 'AC']);

    }

    /**
     * Get pack size by array item_id
     *
     * @param Array $itemIdArr Array item_id
     *
     * @return Array
     */
    public function getCartonsByItemId($itemIdArr = [])
    {
        $query = DB::table('cartons')->select(['item_id', 'ctn_pack_size'])
            ->whereIn('ctn_sts', ['AC', 'LK'])
            ->whereIn('cartons.item_id', $itemIdArr)
            ->groupBy();
        $data = $query->get();

        return $data;
    }

    public function getAvailablePackSize($itemId, $lot = 'ANY')
    {
        $query = $this->make([]);

        $query->where('item_id', $itemId)->groupBy('ctn_pack_size');

        if ($lot != 'ANY') {
            $query->where('lot', $lot);
        }

        return $query->paginate(20);

    }

    public static function getCalculateStorageDurationRaw($startDate = 'gr_dt')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }
}