<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class GoodsReceiptModel extends AbstractModel
{
    const DAS_LIMIT = 2;

    /**
     * GoodsReceiptModel constructor.
     *
     * @param GoodsReceipt|null $model
     */
    public function __construct(GoodsReceipt $model = null)
    {
        $this->model = ($model) ? : new GoodsReceipt();
    }

    /**
     * @return mixed
     */
    public function grDashBoard($currentWH, $userId, $day)
    {
        $limit = (! empty($day) && is_numeric($day)) ? $day : self::DAS_LIMIT;


        $putAlready = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as putAlready"))
            ->join('pal_sug_loc as psl', 'psl.gr_hdr_id', '=', 'g.gr_hdr_id')
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('g.created_by', $userId)
                    ->orWhere('g.updated_by', $userId);
            })
            ->whereNotNull('g.putter')
            ->where(DB::raw('(SELECT COUNT(plt_id) FROM pallet WHERE g.gr_hdr_id = pallet.gr_hdr_id AND pallet.loc_id IS NULL)'), 0)
            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->groupBy('g.gr_hdr_id')
            ->first();


        $putaway = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as putaway"))
            ->join('pal_sug_loc as psl', 'psl.gr_hdr_id', '=', 'g.gr_hdr_id')
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('g.created_by', $userId)
                    ->orWhere('g.updated_by', $userId);
            })
            ->whereNotNull('g.putter')
            ->where(DB::raw('(SELECT COUNT(plt_id) FROM pallet WHERE g.gr_hdr_id = pallet.gr_hdr_id AND pallet.loc_id IS NOT NULL)'), 0)
            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->groupBy('g.gr_hdr_id')
            ->first();


        $totalPutaway = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as total"))
            ->join('pal_sug_loc as psl', 'psl.gr_hdr_id', '=', 'g.gr_hdr_id')
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('g.created_by', $userId)
                    ->orWhere('g.updated_by', $userId);
            })
            ->whereNotNull('g.putter')
            ->where('g.gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->groupBy('g.gr_hdr_id')
            ->first();

        $putAlready = ! empty($putAlready['putAlready']) ? $putAlready['putAlready'] : 0;
        $putaway = ! empty($putaway['putaway']) ? $putaway['putaway'] : 0;
        $totalPutaway = ! empty($totalPutaway['total']) ? $totalPutaway['total'] : 0;

        return [
            "putaway_total" => ($putaway > 0) ? $putaway : 0,
            "putting"       => ($totalPutaway - ($putaway - $putAlready)) > 0
                ? ($totalPutaway - ($putaway - $putAlready)) : 0,
            "putReady"      => ($putAlready > 0) ? $putAlready : 0
        ];
    }

    public function getReceivingList()
    {
        $query = AsnHdr::join('asn_dtl', 'asn_dtl.asn_hdr_id', '=', 'asn_hdr.asn_hdr_id')
            ->whereRaw("asn_dtl_sts IN ('NW','RG')")
            ->groupBy([
                'asn_dtl.item_id', 'asn_dtl.asn_dtl_lot'
            ])
            ->select([
                'asn_dtl.ctnr_id',
                'asn_dtl.ctnr_num',
                'asn_hdr.asn_hdr_id',
                'asn_hdr.asn_hdr_num',
                \DB::raw('COUNT(*) AS sku')
            ])

        ;
        $this->model->filterData($query, true);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $query->get();
    }
}