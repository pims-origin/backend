<?php

namespace App\Api\V2\WavePick\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Location;
use Wms2\UserInfo\Data;

class LocationModel extends AbstractModel
{

    protected $model;

    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }
}
