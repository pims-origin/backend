<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use DB;
use Carbon\Carbon;

class BillableLaborJob extends IMSJob
{
    protected $sac = ['I025', 'O016'];

    public function handle()
    {
        try {
            $token = $this->login();

            $endpoint = env('IMS_BILLABLE_LABOR');

            // Check IMS Configuration
            if(empty($endpoint)) {
                $this->writeLogment('IMS Daily Billable Labor', 'IMS Billable Labor Endpoint is not define');
                return false;
            }

            $data = DB::table('billable_hdr')
                ->join('billable_dtl', 'billable_dtl.ba_id', '=', 'billable_hdr.ba_id')
                ->leftJoin('warehouse', 'warehouse.whs_id', '=', 'billable_hdr.whs_id')
                ->leftJoin('customer', 'customer.cus_id', '=', 'billable_hdr.cus_id')
                ->leftJoin('users', 'users.user_id', '=', 'billable_hdr.created_by')
                ->where('billable_hdr.deleted', 0)
                ->whereIn('billable_dtl.sac', $this->sac)
                ->whereBetween('billable_hdr.created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
                ->select([
                    'users.username',
                    'customer.cus_name',
                    'warehouse.whs_name',
                    'billable_dtl.sac_name',
                    'billable_dtl.ref_num',
                    DB::raw('SUM(billable_dtl.qty) as total_qty'),
                    'billable_dtl.ba_type'
                ])
                ->groupBy('billable_dtl.ref_num')
                ->groupBy('billable_dtl.created_by')
                ->get();

            if(count($data) == 0) {
                $this->writeLogment('IMS Daily Billable Labor', 'No data available today');
                return false;
            }

            $maked = $this->transformMake($data);

            $data = ['data' => $data];

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-Type: application/json"
            ];

            $result = $this->call($endpoint, 'POST', json_encode($data), $header);

            $this->writeLogment('IMS Daily Billable Labor - WMS', 'Body: ' . json_encode($data));
            $this->writeLogment('IMS Daily Billable Labor - IMS', 'Response: ' . $result);

        } catch (\Exception $e) {
            $this->writeLogment('IMS Daily Billable Labor', $e->getMessage());
        }
    }

    public function transformMake($data) {
        $result = [];
        foreach($data as $key => $val) {
            $result[$key] = [
                "user_name"     => array_get($val, 'user_name', null),
                "cus_name"      => array_get($val, 'cus_name', null),
                "whs_name"      => array_get($val, 'whs_name', null),
                "sac_name"      => array_get($val, 'sac_name', null),
                "ref_num"       => array_get($val, 'ref_num', null),
                "total_hour"    => array_get($val, 'total_qty', 0),
                "ba_type"       => array_get($val, 'ba_type', null)
            ];
        }

        return $result;
    }
}
