<?php


namespace App\Api\Gate\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Gate;
use Seldat\Wms2\Utils\Status;

class GateTransformer extends TransformerAbstract
{
    //return result
    public function transform(Gate $gate)
    {
        return [
            'gate_name'     => object_get($gate, 'gate_name', null),
            'gate_sts'      => object_get($gate, 'gate_sts', null),
            'gate_sts_name' => Status::getByKey('GATE_STATUS', object_get($gate, 'gate_sts')),
            'pos'           => object_get($gate, 'pos', null),
            'line'          => object_get($gate, 'line', null),
        ];
    }

}
