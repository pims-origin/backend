<?php

namespace App\Api\V2\Inbound\Location\ValidatePallet\Controllers;

use App\Api\V2\Inbound\Location\ValidatePallet\Models\LocationModel;
use App\Api\V2\Inbound\Location\ValidatePallet\Models\PalletModel;
use App\libraries\RFIDValidate;

use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class LocationController extends AbstractController
{

    protected $locationModel;
    protected $palletModel;

    public function __construct()
    {
        $this->locationModel = new LocationModel();
        $this->palletModel   = new PalletModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function validatePallet($whsId, $palletScanned, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/location/validate-pallet/{$palletScanned}";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'CEL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Check empty Location'
        ]);
        /*
         * end logs
         */

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($palletScanned, RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            return $this->_responseErrorMessage($pltRFIDValid->error);
        }

        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);

            // Get Pallet by Rfid
            $pallet = $this->palletModel->getModel()->where([
                'rfid'    => $palletScanned,
                'whs_id'  => $whsId,
                // 'plt_sts' => 'AC',
            ])
            ->where('plt_sts', '!=', 'AJ')
            ->first();

            if (!$pallet) {
                $msg = sprintf("The pallet RFID %s doesn't exist, Please try to pass through the inbound gateway again!", $palletScanned);
                $pallet = $this->palletModel->getFirstWhere(['rfid' => $palletScanned]);
                if ($pallet) {
                    $msg = sprintf("The pallet RFID %s doesn't belong to current warehouse", $palletScanned);
                }
                return $this->_responseErrorMessage($msg);
            }

            if ($pallet->ctn_ttl == 0) {
                $msg = sprintf("The carton on pallet is none.");

                return $this->_responseErrorMessage($msg);
            }
            $locPutaway = [];
            $locId = $pallet->loc_id;
            if ( $locId != null) {
//                $msg = sprintf("The Pallet RFID %s is on Location %s.", $palletScanned, $pallet->loc_code);
                $location = $this->locationModel->getFirstWhere(['loc_id' => $locId]);
                $locPutaway['loc_id']               = $location->loc_id;
                $locPutaway['loc_code']             = $location->loc_code;
                $locPutaway['loc_alternative_name'] = $location->loc_alternative_name;
                $locPutaway['loc_rfid']             = $location->rfid;
//                if ($location->rfid != null) {
//                    $locPutaway = $location->rfid;
//                }
            }

            $cusId = $pallet->cus_id;

            $locationEmpty = $this->locationModel->getEmptyLocationByCusId($whsId, $cusId);

            if (empty($locationEmpty)) {
                $msg = sprintf("Current location is not defined in WMS system. Please contact Administrator");
                return $this->_responseErrorMessage($msg);
            }

//            $code = $location->loc_alternative_name;
//
//            $locCode = explode("-", $code);
//
//            $count = count($locCode);
//            if ($count < 4 || $count > 5) {
//                $msg = "Loc Code is invalid!";
//                return $this->_responseErrorMessage($msg);
//            }

            $suggested = $this->locationModel->getMoreEmptyLocationByCusId($whsId, $cusId, $locationEmpty);
            $locationSuggest = [
                "cus_id"               => $cusId,
                "pallet"               => $palletScanned,
                "ctn_ttl"              => $pallet->init_ctn_ttl,
                "piece_ttl"            => $pallet->init_piece_ttl,
//              "sku_ttl"              => $currentLoc['sku_ttl'],
                "sku"                  => $pallet->sku,
                "size"                 => $pallet->size,
                "color"                => $pallet->color,
                "lot"                  => $pallet->lot,
                "loc_sts_code"         => $locationEmpty->loc_sts_code,
                "loc_alternative_name" => $locationEmpty->loc_alternative_name,
                "loc_id"               => $locationEmpty->loc_id,
                "loc_rfid"             => $locationEmpty->rfid,
//              "scan"                 => $currentLoc['scan'],
                "suggested"            => $suggested,
            ];

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data']   = [[
                "location_putaway" => $locPutaway,
                "location_suggest" => $locationSuggest

            ]];

            return $msg;

        } catch (\Exception $e) {

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}