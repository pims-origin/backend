<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use function GuzzleHttp\Psr7\str;

class IntegrateFilesWithDMS extends Job
{
    protected $request;
    protected $filePath;
    protected $odrHdr;

    protected $docType = 'ORD';

    public function __construct($request, $filePath, $odrHdr)
    {
        $this->request = $request;
        $this->filePath = $filePath;
        $this->odrHdr = $odrHdr;
    }

    public function handle()
    {
        Log::info('Post order files to DMS on start');
        try{
            $this->integrateFileWithDMS();

            Log::info('Completed post files');
        }catch(\Exception $e){
            Log::error($e->getMessage());
        }
    }

    private function integrateFileWithDMS()
    {
        $file = new \SplFileInfo($this->filePath);
        $fileName = $file->getFilename();

        $dir = storage_path('order-document/dms');
        if (!is_dir($dir)){
            mkdir($dir);
        }

        copy($this->filePath, $dir . '/' . $fileName);

        $fileUrl = env('SYSTEM_BASE_URL') . "/core/pack/public/download-document?type=dms&file_name={$fileName}";

        $client = new Client();

        $args = [
            'http_errors' => false,
            'form_params' => [
                'sys'           => env('SYSTEM_NAME', 'wms360.dev'),
                'jwt'           => str_replace('Bearer ', '', $this->request->getHeaders()['authorization'][0]),
                'file_url'      => $fileUrl,
                'sts'           => 1,
                'transaction'   => $this->odrHdr->odr_num,
                'document_type' => $this->docType,
            ]
        ];

        $res = $client->request('POST', env('DMS_DOCUMENT_API'), $args);

        Log::info(json_encode($args));
        // Log::info(str($res));
    }

    private function getInput(){
        $input = collect($this->request->getQueryParams());
        $input = $input->merge($this->request->getParsedBody());
        return $input;
    }
}