<?php

namespace App\Api\V1\Models;



use Seldat\Wms2\Models\CustomerZone;
use Seldat\Wms2\Models\WarehouseZone;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class WarehouseZoneModel extends AbstractModel
{

    protected $model;
    protected $customerZone;
    protected $limit = 10;

    public function __construct(WarehouseZone $model = null, CustomerZone $customerZone = null)
    {
        $this->model = ($model) ?: new WarehouseZone();
        $this->customerZoneModel = ($customerZone) ?: new CustomerZone();
    }

    public function deleteWarehouseZone($warehouseId, $zoneId)
    {
        return $this->model
            ->where('zone_id', $zoneId)
            ->where('zone_whs_id', $warehouseId)
            ->delete();
    }

    public function deleteMassZone($warehouseId, array $zoneId)
    {
        return $this->model
            ->whereIn('zone_id', $zoneId)
            ->where('zone_whs_id', $warehouseId)
            ->forceDelete();
    }

    public function getZone($warehouseId, $zoneId, $with = [])
    {
        $query = $this->make($with);
        $this->model->filterData($query);

        return $query->where('zone_id', $zoneId)
            ->where('zone_whs_id', $warehouseId)
            ->first();
    }

    public function search($warehouseId, $attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with);
        $query->where('zone_whs_id', $warehouseId);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'zone_name' || $key === 'zone_code' || $key === 'zone_description') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                } else {
                    if ($key === 'zone_id' || $key === 'zone_type_id') {
                        $query->where($key, $value);
                    }
                }
            }
        }
        $this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $zoneTypeIds
     *
     * @return mixed
     */
    public function getAllZoneTypeIds($zoneTypeIds)
    {
        $query = $this->model->whereIn('zone_type_id', $zoneTypeIds);
        $this->model->filterData($query);

        return $query->get();
    }

    public function freeZone($warehouseId = 0, $with = [], $limit = null)
    {
        $query = $this->make($with);

//        $customerZone = $this->customerZoneModel->all();
        $customerZone = $this->customerZoneModel
                            ->join('zone', 'zone.zone_id', '=', 'customer_zone.zone_id')
                            ->join('zone_type', 'zone_type.zone_type_id', '=', 'zone.zone_type_id')
                            ->where('zone_type.zone_type_code', '<>', 'VZ')
                            ->select('customer_zone.*')
                            ->get();
        $customerZoneIds = array_pluck($customerZone, 'zone_id', 'zone_id');

        if (!empty($customerZoneIds)) {
            $query->whereNotIn('zone_id', $customerZoneIds);
        }

        if (!empty($warehouseId)) {
            $query->where('zone_whs_id', $warehouseId);
        }

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * Create if not exist
     *
     * @param array $data
     *
     * @return bool
     */
    public function updateNumLoc(array $data)
    {
        $model = $this->model->firstOrNew(['zone_id' => $data['zone_id']]);
        $data['zone_num_of_loc'] += $model->zone_num_of_loc;
        $data['zone_num_of_active_loc'] += $model->zone_num_of_active_loc;
        $data['zone_num_of_inactive_loc'] += $model->zone_num_of_inactive_loc;

        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }
}
