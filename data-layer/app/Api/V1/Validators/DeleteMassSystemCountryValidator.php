<?php

namespace App\Api\V1\Validators;

class DeleteMassSystemCountryValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'sys_country_id'   => 'required',
            'sys_country_id.*' => 'required|exists:system_country,sys_country_id'
        ];
    }
}