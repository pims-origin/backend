<?php
namespace App\Models;

use App\Api\V1\Models\BaseInvoiceModel;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

/**
 * Class WorkOrderHdr
 *
 * @package Seldat\Wms2\Models
 */
class WorkOrderHdr extends BaseInvoiceModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wo_hdr';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wo_hdr_id';
    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'odr_hdr_id',
        'seq',
        'ship_to_name',
        'odr_hdr_num',
        'wo_hdr_num',
        'wo_in_notes',
        'wo_ex_notes',
        'sts',
        'wo_sts',
        'type'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function orderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_hdr_id');
    }

    public function WorkOrderDtl()
    {
        return $this->hasOne(__NAMESPACE__ . '\WorkOrderDtl', 'wo_hdr_id', 'wo_hdr_id');
    }
}
