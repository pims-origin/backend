<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\InventoryHistoryModel;
use App\Api\V1\Transformers\InventoryHistoryTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class InventoryHistoryController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new InventoryHistoryModel();
        $this->transformer = new InventoryHistoryTransformer();
    }

    /*
    ****************************************************************************
    */
    
}
