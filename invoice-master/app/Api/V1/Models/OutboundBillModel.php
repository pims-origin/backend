<?php
/**
 * Created by PhpStorm.
 * User: duong.tran
 * Date: 1/30/2018
 * Time: 8:48 AM
 */
namespace App\Api\V1\Models;

use App\BillableHdr;
use Illuminate\Support\Facades\DB;

class OutboundBillModel extends AbstractModel
{
    protected $billableHdr;
    protected $billableDtl;
    protected $odrHdr;
    const TO_CUBIC_FOOT = 1728;

    public function __construct($odrHdrId)
    {
        $this->billableHdr = new BillableHdrModel();
        $this->billableDtl = new BillableDtlModel();
        $this->odrHdr = $this->getOrderHdr($odrHdrId);
    }

    /**
     * @param int $odrHdrId
     * */
    public function getOrderHdr($odrHdrId) {
        return DB::table('odr_hdr')->where('odr_id', $odrHdrId)->first();
    }

    /*
     * Store Billable for Order
     * */
    public function storeBillable() {
        if ($this->odrHdr->odr_sts == 'SH') {
            $newBillableHdr = new BillableHdr();
            $newBillableHdr->cus_id = $this->odrHdr->cus_id;
            $newBillableHdr->whs_id = $this->odrHdr->whs_id;
            $newBillableHdr->ref_num = $this->odrHdr->odr_num;
            $newBillableHdr->ba_from = $this->odrHdr->shipped_dt;
            $newBillableHdr->ba_to = $this->odrHdr->shipped_dt;
            $newBillableHdr->ba_ttl = 0;
            $newBillableHdr->amount_ttl = 0;
            $newBillableHdr->ba_type = 'OUB';
            $newBillableHdr->ba_period = 'BLOCK';

            if ($newBillableHdr->save()) {

                $amountTtl = 0;
                $baTtl = 0;
                //get cus_sac for Order
                $cusSACs = $this->getCusSac();
                $cartonQtys = $this->getOdrCartonQty()->each(function ($item, $key) {
                    $item->cube = $item->cube / self::TO_CUBIC_FOOT;
                    $item = data_fill($item, 'ttl_cub', $item->qty * $item->cube);
                });
                foreach ($cusSACs as $cusSAC) {

                    if ($cusSAC->sac == 'O016') {
                        foreach($this->countIBLabor() as $labor) {

                            $pieceQty = round($labor['qty'], 4);
                            if ($pieceQty > 0) {
                                $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, $pieceQty, $labor['user_id']);
                                $this->billableDtl->refreshModel();
                                $this->billableDtl->create($billableDtlData);
                                $amountTtl += $billableDtlData['amount'];
                                $baTtl++;
                            }
                        }

                        continue;
                    }

                    $uom = $cusSAC->unit_uom;
                    $rangeUom = $cusSAC->range_uom ?? '';
                    switch ($uom) {
                        case 'PC':
                            $pieceQty = $this->getOdrPieceQty();
                            if ($pieceQty > 0) {
                                $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, $pieceQty);
                                $this->billableDtl->refreshModel();
                                $this->billableDtl->create($billableDtlData);
                                $amountTtl += $billableDtlData['amount'];
                                $baTtl++;
                            }

                            break;
                        case 'UNI':
                            $pieceQty = $this->getOdrPieceQty();
                            if ($pieceQty > 0) {
                                $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, $pieceQty);
                                $this->billableDtl->refreshModel();
                                $this->billableDtl->create($billableDtlData);
                                $amountTtl += $billableDtlData['amount'];
                                $baTtl++;
                            }

                            break;
                        case 'CTN':
                            if ($rangeUom == 'SKU') {
                                $cartonQty = $this->getCartonQtyBySku($cartonQtys, $cusSAC->range_from, $cusSAC->range_to);
                            } else {
                                $cartonQty = $this->getOdrCartonQty()->sum('qty');
                            }

                            if ($cartonQty > 0) {
                                $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, $cartonQty);
                                $this->billableDtl->refreshModel();
                                $this->billableDtl->create($billableDtlData);
                                $amountTtl += $billableDtlData['amount'];
                                $baTtl++;
                            }

                            break;
                        case 'PL':
                            $palletQty = $this->getOdrPalletQty();
                            if ($palletQty > 0) {
                                $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, $palletQty);
                                $this->billableDtl->refreshModel();
                                $this->billableDtl->create($billableDtlData);
                                $amountTtl += $billableDtlData['amount'];
                                $baTtl++;
                            }
                            
                            break;
                        case 'ORD':
                            $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, 1);
                            $this->billableDtl->refreshModel();
                            $this->billableDtl->create($billableDtlData);
                            $amountTtl += $billableDtlData['amount'];
                            $baTtl++;
                            break;
                        case 'BOL':
                            $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, 1);
                            $this->billableDtl->refreshModel();
                            $this->billableDtl->create($billableDtlData);
                            $amountTtl += $billableDtlData['amount'];
                            $baTtl++;
                            break;
                        case 'CUB':
                            $qty = $cartonQtys->sum('ttl_cub');
                            if ($qty > 0) {
                                $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, $qty);
                                $this->billableDtl->refreshModel();
                                $this->billableDtl->create($billableDtlData);
                                $amountTtl += $billableDtlData['amount'];
                                $baTtl++;
                            }

                            break;
                    }
                }
                //Update Billable Header
                $newBillableHdr->amount_ttl = $amountTtl;
                $newBillableHdr->ba_ttl = $baTtl;
                $newBillableHdr->save();
            }
        }
    }

    public function countIBLabor()
    {
        $labor = DB::table('rpt_labor_tracking')
            ->select([
                'rpt_labor_tracking.user_id',
                'rpt_labor_tracking.owner',
                'rpt_labor_tracking.trans_num',
                DB::raw('SUM(end_time - start_time) as total_time')
            ])
            ->leftJoin('users', 'users.user_id', '=', 'rpt_labor_tracking.user_id')
            ->where('whs_id', $this->odrHdr->whs_id)
            ->where('owner', $this->odrHdr->odr_num)
            ->where('lt_type', 'OB')
            ->whereNotNull('end_time')
            ->groupBy('user_id')
            ->get();

        $result = [];
        if(!empty($labor)) {
            foreach($labor as $lb) {
                $result[] = [
                    'qty' => $lb->total_time/3600,
                    'user_id' => $lb->user_id
                ];
            }
        }

        return $result;
    }

    private function makeBillableDtlData($newBillableHdr, $cusSAC, $qty, $userId = null) {
        return [
            'ba_id'         => $newBillableHdr->ba_id,
            'cus_id'        => $newBillableHdr->cus_id,
            'whs_id'        => $newBillableHdr->whs_id,
            'sac_id'        => $cusSAC->sac_id,
            'sac'           => $cusSAC->sac,
            'sac_name'      => $cusSAC->sac_name,
            'ref_num'       => $this->odrHdr->odr_num,
            'qty'           => $qty,
            'ba_type'       => 'OUB',
            'unit_price'    => $cusSAC->price,
            'discount'      => $cusSAC->discount,
            'amount'        => $qty * $cusSAC->price * ( 1 - $cusSAC->discount / 100 ),
            'item'          => $this->odrHdr->cus_odr_num,
            'description'   => $this->odrHdr->cus_po,
            'ba_user_id'    => $userId
        ];
    }
    /*
     * Get cus_sac for this Order
     * */
    public function getCusSac() {
        return DB::table('cus_sac')
            ->where('whs_id', $this->odrHdr->whs_id)
            ->where('cus_id', $this->odrHdr->cus_id)
            ->where('cus_sac.type', 'OUB')
            ->where('cus_sac.deleted', 0)
            ->where('cus_sac.deleted_at', 915148800)
            ->get();
    }

    /*
     * Get carton quantity of Order
     * */
    public function getOdrCartonQty() {
        return DB::table('pack_hdr')
            ->select(DB::raw("count(pack_hdr.pack_hdr_id) AS qty, IF(pack_hdr.cnt_id IS NOT NULL, cartons.cube, (pack_ref.length * pack_ref.width * pack_ref.height)) as cube"))
            ->where('pack_hdr.odr_hdr_id', $this->odrHdr->odr_id)
            ->where('pack_hdr.deleted', 0)
            ->where('pack_hdr.deleted_at', 915148800)
            ->leftJoin('pack_ref', 'pack_ref.pack_ref_id', '=', 'pack_hdr.pack_ref_id')
            ->leftJoin('cartons', 'cartons.ctn_id', '=', 'pack_hdr.cnt_id')
            ->groupBy('pack_hdr.pack_ref_id')
            ->get();
    }
    /*
     * Get carton quantity of Order
     * */
    public function getOdrPalletQty() {
        return DB::table('out_pallet')
            ->selectRaw('COUNT( DISTINCT out_pallet.plt_id ) as plt_qty')
            ->join('pack_hdr', 'out_pallet.plt_id', '=', 'pack_hdr.out_plt_id')
            ->join('odr_hdr' , 'pack_hdr.odr_hdr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_hdr.odr_id', $this->odrHdr->odr_id)
            ->where('pack_hdr.deleted', 0)
            ->value('plt_qty');
    }

    /*
     * Get piece quantity of Order
     * */
    public function getOdrPieceQty() {
        return DB::table('odr_dtl')
            ->selectRaw("SUM(picked_qty)")
            ->where('odr_dtl.odr_id', $this->odrHdr->odr_id)
            ->where('odr_dtl.deleted', 0)
            ->value('picked_qty');
    }

    public function getCartonQtyBySku($cartons, $from, $to)
    {
        $from = $from ?? 0;
        $to = $to ?? 0;
        $total = 0;
        foreach ($cartons as $carton) {
            if ($from == $to) {
                $total += $carton->qty;
            }

            if ($carton->cube >= $from && $carton->cube <= $to) {
                $total += $carton->qty;
            }

            if ($carton->cube >= $from && $to == 0) {
                $total += $carton->qty;
            }
        }

        return $total;
    }
}