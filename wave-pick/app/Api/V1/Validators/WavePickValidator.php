<?php

namespace App\Api\V1\Validators;

class WavePickValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'act_piece_qty' => 'greater_than_zero',
        ];
    }
}
