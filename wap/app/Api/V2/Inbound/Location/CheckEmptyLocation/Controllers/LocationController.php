<?php

namespace App\Api\V2\Inbound\Location\CheckEmptyLocation\Controllers;

use App\Api\V2\Inbound\Location\CheckEmptyLocation\Models\LocationModel;
use App\libraries\RFIDValidate;

use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class LocationController extends AbstractController
{

    protected $locationModel;

    public function __construct()
    {
        $this->locationModel = new LocationModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|void
     */
    public function checkEmpty($whsId, $locScanned, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/location/check-empty/{$locScanned}";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'CEL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Check empty Location'
        ]);
        /*
         * end logs
         */

        $type = array_get($input, 'type');
        if (!in_array($type, [1, 2])) {
            $msg = "The type of Scanned Location is wrong. Just including 1 (RFID) / 2 (Barcode)";
            return $this->_responseErrorMessage($msg);
        }

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $location = "";
        if ($type == 1) {
            //validate location RFID
            $locRFIDValid = new RFIDValidate($locScanned, RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                return $this->_responseErrorMessage($locRFIDValid->error);
            }

            //Check existed for The loc RFID
            $location = $this->locationModel->getRackLocationByLocRFID($whsId, $locScanned);
            if (empty($location)) {
                $msg = sprintf("The location RFID %s doesn't exist!", $locScanned);
                $location = $this->locationModel->getFirstWhere(['rfid' => $locScanned]);
                if ($location) {
                    $msg = sprintf("The location RFID %s doesn't belong to current warehouse!", $locScanned);
                }

                return $this->_responseErrorMessage($msg);
            }
        } else {
            //Check existed for The loc RFID
            $location = $this->locationModel->getFirstWhere([
                                    'loc_whs_id'   => $whsId,
                                    'loc_code' => $locScanned,
                                ]);
            if (empty($location)) {
                $msg = sprintf("The location code %s doesn't exist!", $locScanned);
                $location = $this->locationModel->getFirstWhere(['loc_code' => $locScanned]);
                if ($location) {
                    $msg = sprintf("The location code %s doesn't belong to current warehouse!", $locScanned);
                }

                return $this->_responseErrorMessage($msg);
            }
        }

        if ($location->loc_sts_code != 'AC') {
            $msg = sprintf("Location %s is not active. Current Status is %s", $locScanned,
                Status::getByValue($location->loc_sts_code, 'LOCATION_STATUS'));

            return $this->_responseErrorMessage($msg);
        }

        try {

            $whereRaw = "(loc_id in (select loc_id from cartons where deleted = 0) or loc_id in (select loc_id from pallet where deleted = 0))";
            $query = DB::table('location')
                        ->whereRaw($whereRaw)
                        ->where('location.loc_whs_id', $whsId)
                        ->where('location.deleted', 0);
            $checkEmpty = false;
            if ($type == 1) {
                $checkEmpty = $query->where('location.rfid', $locScanned)
                                ->whereNotNull('location.rfid')
                                ->first();
            } else {
                $checkEmpty = $query->where('location.loc_code', $locScanned)
                                ->first();
            }

            if (count($checkEmpty) > 0) {
                $msg = sprintf("Location %s is not empty!", array_get($checkEmpty, 'loc_code'));
                return $this->_responseErrorMessage($msg);
            }

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data']   = [];

            return $msg;

        } catch (\Exception $e) {

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}