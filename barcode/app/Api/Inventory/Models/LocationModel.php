<?php

namespace App\Api\Inventory\Models;

use phpDocumentor\Reflection\Types\Null_;
use Seldat\Wms2\Models\Location;
use Wms2\UserInfo\Data;

class LocationModel extends AbstractModel
{
    const TYPE_RACK = 'RAC';

    /**
     * LocationModel constructor.
     * @param Location $model
     */
    public function __construct(Location $model)
    {
        $this->model = $model ? $model : new Location();
    }

    /**
     * @param $locCodes
     * @return array
     */
    public static function getLocationByCodes($locCodes)
    {
        $result = [];

        if (! $locCodes) {
            return $result ;
        }

        $locCodes = (array) $locCodes;

        $result = Location::select(
                    'loc_id',
                    'loc_whs_id',
                    'loc_code',
                    'loc_sts_code'
                )
            ->where('loc_sts_code', LockModel::STATUS_ACTIVE)
            ->whereIn('loc_code', $locCodes)
            ->get();

        return $result;
    }

    /**
     * @param $locFrom
     * @param $locTo
     * @return array
     */
    public static function getRangeLocationByFromTo($whdId, $locFrom, $locTo)
    {
        $result = [];
        
        $status = [
            LockModel::STATUS_ACTIVE,
            LockModel::STATUS_LOCK
        ];

        if (! ($locFrom && $locTo && $whdId)) {
            return $result ;
        }

        $locTypeRacId = LockModel::getLocTypeId();

        $result = Location::select(
            'location.loc_id',
            'location.loc_whs_id',
            'location.loc_code'
        )
            ->where('location.loc_type_id', $locTypeRacId)
            ->whereIn('location.loc_sts_code', $status)
            ->where('location.loc_whs_id', $whdId)
            ->whereBetween('location.loc_code', [$locFrom, $locTo])
            ->orderBy('location.loc_code', 'asc')
            ->get();

        return $result;
    }

    /**
     * @param $locations
     * @param $whsId
     * @return array
     */
    public static function getLocationNotInWarehouse($locations, $whsId)
    {
        $result = [];
        if (! ($locations && $whsId)) {
            return $result;
        }

        foreach ($locations as $location) {
            if ($location['loc_whs_id'] != $whsId) {
                $result[] = $location['loc_code'];
            }
        }

        return $result;
    }

    /**
     * @param $condition
     * @return array
     */
    public static function filterLocation($condition, $limit = null)
    {
        if (empty($condition) || !is_array($condition)) {
            return [];
        }

        $locTypeRacId = LockModel::getLocTypeId();

        $query = Location::select(
            'location.loc_id',
            'location.loc_code'
        )
        ->where('location.loc_type_id', $locTypeRacId)
        ->where('location.loc_sts_code', LockModel::STATUS_ACTIVE);

        foreach ($condition as $key => $value) {
            switch ($key) {
                case 'loc_whs_id':
                    $query = $query->where('location.loc_whs_id', '=', $value);
                    break;
                case 'term':
                    $query = $query->where('location.loc_code', 'like', $value . '%');
                    break;
                case 'loc_code':
                    $query = $query->where('location.loc_code', '=', $value);
                    break;
                case 'loc_sts_code':
                    $query = $query->where('location.loc_sts_code', '=', $value);
                    break;
                default:
                    # code...
                    break;
            }
        }

        if ($limit) {
            $query = $query->limit($limit);
        }

        return $query->get();
    }

    /**
     * @param $locCode
     *
     * @return bool
     */
    public static function getLocationByCode($locCode)
    {
        $whsId = Data::getCurrentWhsId();
        if (! $locCode) {
            return false;
        }

        $locTypeRacId = LockModel::getLocTypeId();

        return Location::select(
                'location.loc_id',
                'location.loc_code',
                'location.loc_whs_id',
                'location.loc_sts_code',
                'customer_zone.cus_id'
            )
            ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'zone.zone_id')
            ->where('loc_code', $locCode)
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('customer_zone.deleted', 0)
            ->where('location.loc_whs_id', $whsId)
            ->first();
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param null $status
     * @return array
     */
    public static function getLocationIdsByCustomer($whsId, $cusId, $status = NULL)
    {
        $result = [];

        if (! ($whsId && $cusId)) {
            return $result;
        }

        $status = $status ? $status : LockModel::STATUS_ACTIVE;

        $locTypeRacId = LockModel::getLocTypeId();

        $result = Location::select('location.loc_id')
            ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'zone.zone_id')
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('customer_zone.cus_id', $cusId)
            ->where('customer_zone.deleted', 0)
            ->where('location.loc_sts_code', $status)
            ->distinct()
            ->get()
            ->toArray();
        
        return $result ? array_column($result, 'loc_id') : [] ;
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param null $status
     * @return array
     */
    public static function getLocationFreeOfCustomer($whsId, $cusId, $status = NULL)
    {
        $result = [];

        if (! ($whsId && $cusId)) {
            return $result;
        }

        $status = $status ? $status : LockModel::STATUS_ACTIVE;

        $data = Location::select('location.loc_id')
            ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'zone.zone_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->leftjoin('cartons', 'cartons.loc_id', '=', 'location.loc_id')
            ->where('loc_type.loc_type_code', self::TYPE_RACK)
            ->where('zone.zone_whs_id', $whsId)
            ->where('customer_zone.cus_id', $cusId)
            ->where('customer_zone.deleted', 0)
            ->where('location.loc_sts_code', $status)
            ->where('cartons.ctn_id', NULL)
            ->distinct()
            ->get()
            ->toArray();

        if ($data) {
            $result = array_column($data, 'loc_id');
        }

        return $result;
    }

    /**
     * @param $whsId
     * @param $cusIds
     *
     * @return array
     */
    public static function getLocLockByCus($whsId, $cusIds)
    {
        $result = [];

        if (! ($whsId && $cusIds)) {
            return $result;
        }

        $locTypeRacId = LockModel::getLocTypeId();

        $result = Location::select(
                'location.loc_code'
            )
            ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'zone.zone_id')
            ->where('location.loc_sts_code', LockModel::STATUS_LOCK)
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('zone.zone_whs_id', $whsId)
            ->whereIn('customer_zone.cus_id', $cusIds)
            ->where('customer_zone.deleted', 0)
            ->distinct()
            ->get()
            ->toArray();
        ;

        return $result;
    }

    /**
     * @param $whsId
     * @param $locIds
     *
     * @return array
     */
    public static function getLocLockByLoc($whsId, $locIds)
    {
        $result = [];

        if (! ($whsId && $locIds)) {
            return $result;
        }

        $locTypeRacId = LockModel::getLocTypeId();

        $result = Location::select(
            'location.loc_code'
        )
            ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'zone.zone_id')
            ->where('location.loc_sts_code', LockModel::STATUS_LOCK)
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('zone.zone_whs_id', $whsId)
            ->whereIn('location.loc_id', $locIds)
            ->where('customer_zone.deleted', 0)
            ->distinct()
            ->get()
            ->toArray();

        return $result;
    }

    /**
     * @param $whsId
     * @param $skus
     *
     * @return array
     */
    public static function getLocLockBySku($whsId, $skus)
    {
        $result = [];

        if (! ($whsId && $skus)) {
            return $result;
        }

        $locTypeRacId = LockModel::getLocTypeId();

        $result = Location::select(
            'location.loc_code'
        )
            ->join('cartons', 'cartons.loc_id', '=', 'location.loc_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->where('location.loc_sts_code', LockModel::STATUS_LOCK)
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('cartons.whs_id', $whsId)
            ->whereIn('item.sku', $skus)
            ->distinct()
            ->get()
            ->toArray();

        return $result;
    }

    /**
     * @param $whsId
     * @param $itemId
     */
    public static function getLocLockByItem($whsId, $itemId)
    {
        $result = [];

        if (! ($whsId && $itemId)) {
            return $result;
        }

        $locTypeRacId = LockModel::getLocTypeId();

        $result = Location::select(
            'location.loc_code'
        )
            ->join('cartons', 'cartons.loc_id', '=', 'location.loc_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->where('location.loc_sts_code', LockModel::STATUS_LOCK)
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('cartons.whs_id', $whsId)
            ->whereIn('item.item_id', $itemId)
            ->distinct()
            ->get()
            ->toArray();

        return $result;
    }

    /**
     * @param $whsId
     * @param $cycleType
     * @param $cycleDetail
     */
    public static function getLocationLockByCycleType($whsId, $cycleType, $cycleDetail)
    {
        $result = [];

        switch ($cycleType)
        {
            case CycleHdrModel::CYCLE_TYPE_CUSTOMER:
                $result= self::getLocLockByCus($whsId, $cycleDetail);
                break;

            case CycleHdrModel::CYCLE_TYPE_LOCATION:
                $result= self::getLocLockByLoc($whsId, $cycleDetail);
                break;

            case CycleHdrModel::CYCLE_TYPE_SKU:
                $result= self::getLocLockBySku($whsId, $cycleDetail);
                break;
        }

        return $result ? array_column($result, 'loc_code') : [];
    }

    /**
     * @param $whsId
     * @param $blockType
     * @param $detail
     *
     * @return array
     */
    public static function getLocationLockByBlockType($whsId, $blockType, $detail)
    {
        $result = [];

        switch ($blockType)
        {
            case BlockHdrModel::BLOCK_TYPE_CUSTOMER:
                $result= self::getLocLockByCus($whsId, $detail);
                break;

            case BlockHdrModel::BLOCK_TYPE_LOCATION:
                $result= self::getLocLockByLoc($whsId, $detail);
                break;

            case BlockHdrModel::BLOCK_TYPE_SKU:
                $result= self::getLocLockBySku($whsId, $detail);
                break;
            case BlockHdrModel::BLOCK_TYPE_ITEM_ID:
                $result= self::getLocLockByItem($whsId, $detail);
                break;
        }

        return $result ? array_column($result, 'loc_code') : [];
    }
}