<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\CustomerAddress;

use App\Api\V1\Models\ClientContactsModel;


class ClientMasterModel extends AbstractModel
{
    protected $contact;
    protected $address;
    protected $ctcModel;

    /**
    *  Constructor
    */

    public function __construct()
    {
        $this->model = new Customer();
        $this->customerAddresses = new CustomerAddress();
        $this->ctcModel = new ClientContactsModel();
    }

    /*
    ****************************************************************************
    */

    public function getClientInfo($custID)
    {
        $billToValues = $this->getBillTo($custID);
        $shipToValues = $this->getShipTo($custID);

        return $billToValues + $shipToValues;
    }

    /*
    ****************************************************************************
    */

    public function getBillTo($custID)
    {
        $query = $this->model
                    ->select(
                        'cus_id',
                        'cus_code',
                        'cus_name',
                        'cus_type',
                        'net_terms'
                    )
                    ->with([
                        'customerAddress' => function ($q) {
                            $q->select(
                                'cus_add_cus_id',
                                'cus_add_city_name',
                                'cus_add_postal_code',
                                'cus_add_state_id',
                                'cus_add_country_id',
                                'cus_add_line_1',
                                'cus_add_line_2'
                            );
                            $q->where('cus_add_type', '=', 'bill');
                        },
                        'customerAddress.systemState' => function ($q) {
                            $q->select(
                                'sys_state_id',
                                'sys_state_name as state'
                            );
                        },
                        'customerAddress.systemCountry' => function ($q) {
                            $q->select(
                                'sys_country_id',
                                'sys_country_name as country'
                            );
                        },
                        'customerContact' => function ($q) {
                            $q->select(
                                'cus_ctt_cus_id',
                                'cus_ctt_phone'
                            );
                            $q->selectRaw('CONCAT(cus_ctt_fname, " ", cus_ctt_lname) AS ctc_name');
                            $q->where('cus_ctt_dft', 1);
                        },
                    ])
                    ->where('cus_id', $custID)
                    ->get();

        $billResults = [];

        foreach($query->toArray() as $row) {

            $billResults = [
                'cus_id' => $row['cus_id'],
                'cus_code' => $row['cus_code'],
                'cus_type' => $row['cus_type'],
                'cus_name' => $row['cus_name'],
                'net_terms' => $row['net_terms'],
                'bill_to_add_line_1' => $row['customer_address']['cus_add_line_1'],
                'bill_to_add_line_2' => $row['customer_address']['cus_add_line_2'],
                'bill_to_city' => $row['customer_address']['cus_add_city_name'],
                'bill_to_state_id' => $row['customer_address']['cus_add_state_id'],
                'bill_to_state' => $row['customer_address']['system_state']['state'],
                'bill_to_country_id' => $row['customer_address']['cus_add_country_id'],
                'bill_to_country' => $row['customer_address']['system_country']['country'],
                'bill_to_zip' => $row['customer_address']['cus_add_postal_code'],
                'cust_ctc_id' => getDefault($row['customer_contact'][0]['cus_ctt_cus_id'], null),
                'ctc_ph' => getDefault($row['customer_contact'][0]['cus_ctt_phone'], null),
                'ctc_nm' => getDefault($row['customer_contact'][0]['ctc_name'], null),
            ];
        }

        return $billResults;
    }

    /*
    ****************************************************************************
    */

    function getShipTo($custID)
    {
        $query = $this->model
                    ->select(
                        'cus_id',
                        'cus_name'
                    )
                    ->with([
                        'customerAddress' => function ($q) {
                           $q->select(
                                'cus_add_cus_id',
                                'cus_add_city_name',
                                'cus_add_postal_code',
                                'cus_add_state_id',
                                'cus_add_country_id',
                                'cus_add_line_1',
                                'cus_add_line_2'
                            );
                           $q->where('cus_add_type', '=', 'ship');
                        },
                        'customerAddress.systemState' => function ($q) {
                            $q->select(
                                'sys_state_id',
                                'sys_state_name as state'
                            );
                        },
                        'customerAddress.systemCountry' => function ($q) {
                            $q->select(
                                'sys_country_id',
                                'sys_country_name as country'
                            );
                        },
                    ])
                    ->where('cus_id', $custID)
                    ->get();

        $shipResults = [];

        foreach($query->toArray() as $row) {
            $shipResults = [
                'cus_id' => $row['cus_id'],
                'cus_name' => $row['cus_name'],
                'ship_to_add_line_1' => $row['customer_address']['cus_add_line_1'],
                'ship_to_add_line_2' => $row['customer_address']['cus_add_line_2'],
                'ship_to_city' => $row['customer_address']['cus_add_city_name'],
                'ship_to_state_id' => $row['customer_address']['cus_add_state_id'],
                'ship_to_state' => $row['customer_address']['system_state']['state'],
                'ship_to_country_id' => $row['customer_address']['cus_add_country_id'],
                'ship_to_country' => $row['customer_address']['system_country']['country'],
                'ship_to_zip' => $row['customer_address']['cus_add_postal_code'],
            ];
        }

        return $shipResults;
    }

    /*
    ****************************************************************************
    */

    function updateCustomerInfo($cus_id, $data)
    {
        DB::transaction(function() use ($cus_id, $data) {

            $update = array_get($data, 'update', ['bill', 'ship']);

            $updateValues['cus_name'] = $data['cus_name'];

            foreach (['cus_code', 'cus_type', 'net_terms'] as $value) {
                if (array_key_exists($value, $data)) {
                    $updateValues[$value] = $data[$value];
                }
            }

            if (isset($data['cus_name'])) {
                $this->model
                    ->where('cus_id', $cus_id)
                    ->update($updateValues);
            }

            foreach ($update as $type) {
                $this->customerAddresses
                        ->updateOrCreate([
                            'cus_add_cus_id' => $cus_id,
                            'cus_add_type' => $type,
                        ],
                        [
                            'cus_add_line_1' => $data[$type . '_to_add_line_1'],
                            'cus_add_line_2' => $data[$type . '_to_add_line_2'],
                            'cus_add_city_name' => $data[$type . '_to_city'],
                            'cus_add_state_id' => $data[$type . '_to_state_id'],
                            'cus_add_country_id' => $data[$type . '_to_country_id'],
                            'cus_add_postal_code' => $data[$type . '_to_zip'],
                        ]);
            }
        });

        return $this->model;
    }

    /*
    ****************************************************************************
    */

}

