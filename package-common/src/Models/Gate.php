<?php

namespace Seldat\Wms2\Models;


class Gate extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gate';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gate_id';

    /**
     * @var array
     */
    protected $fillable = [
        'gate_name',
        'gate_sts',
        'pos',
        'line'
    ];
    
    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }

}
