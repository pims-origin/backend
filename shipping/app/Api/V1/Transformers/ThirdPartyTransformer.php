<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class ThirdPartyTransformer extends TransformerAbstract
{
    public function transform($thirdParty)
    {
        return [
            'tp_id'         => $thirdParty->tp_id,
            'cus_id'        => $thirdParty->cus_id,
            'type'          => $thirdParty->type,
            'name'          => $thirdParty->name,
            'des'           => $thirdParty->des,
            'phone'         => $thirdParty->phone,
            'mobile'        => $thirdParty->mobile,
            'ct_first_name' => $thirdParty->ct_first_name,
            'ct_last_name'  => $thirdParty->ct_last_name,
            'add_1'         => $thirdParty->add_1,
            'add_2'         => $thirdParty->add_2,
            'country'       => $thirdParty->country,
            'state'         => $thirdParty->state,
            'city'          => $thirdParty->city,
            'zip'           => $thirdParty->zip
        ];
    }
}
