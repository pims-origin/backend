<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusLockedForCarton extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('ctn_status')->insert(
            array(
                'status' => 'CL',
                'ctn_sts_name' => 'Cycle_Locked',
                'ctn_sts_des' => 'Cycle Count locked',
                'created_at' => time()
            )
        );

        DB::table('loc_status')->insert(
            array(
                'loc_sts_code' => 'CL',
                'loc_sts_name' => 'Cycle_Locked',
                'loc_sts_desc' => 'Cycle Count locked',
                'created_at' => time()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
