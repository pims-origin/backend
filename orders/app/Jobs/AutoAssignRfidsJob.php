<?php

namespace App\Jobs;

use DB;
use GuzzleHttp\Client;
use App\Api\V1\Models\Log;

class AutoAssignRfidsJob extends Job
{
    protected $whsId;
    protected $wvId;
    protected $rfids;
    protected $request;
    protected $odrId;
    protected $pieceAssign;
    /**
     * AutoPackJob constructor.
     */
    public function __construct($whsId, $wvId, $rfids, $request, $odrId = null, $pieceAssign = null)
    {
        $this->whsId    = $whsId;
        $this->wvId     = $wvId;
        $this->rfids    = $rfids;
        $this->request  = $request;
        $this->odrId    = $odrId;
        $this->pieceAssign = $pieceAssign;
    }

    public function handle()
    {
        $odrId = $this->odrId;
        if (!$odrId) {
            $odrArr = DB::table('odr_hdr')
                ->where('wv_id', $this->wvId)
                ->where('whs_id', $this->whsId)
                ->where('deleted', 0)
                ->first();
            $odrId = array_get($odrArr, 'odr_id');
        }

        $cartons = DB::table('odr_cartons')
            ->whereIn('ctn_rfid', $this->rfids)
            ->where('whs_id', $this->whsId)
            ->where('ctn_sts', 'PD')
            ->get();
        try{

            if ($odrId && count($cartons)) {
                $ctnMaps = array_map(function ($carton) {
                    return [
                        'ctn-rfid'   => array_get($carton, 'ctn_rfid'),
                        'picked-qty' => $this->pieceAssign ?? (int)array_get($carton, 'piece_qty'),
                    ];
                }, $cartons);

                $client = new Client();
                $version = "v4";
                $url = sprintf('/%s/whs/%d/order/%d/pieces-with-rfid', $version, $this->whsId, $odrId);
                $authorization = $this->request->getHeader('Authorization');

                foreach ($ctnMaps as $ctnMap)
                {
                    $response = $client->request('PUT', env("API_WAP"). $url,
                        [
                            'headers'     => ['Authorization' => $authorization],
                            'form_params' => $ctnMap,
                        ]
                    );
                }

                $owner = $transaction = "";
                Log:: info($this->request, $this->whsId, [
                    'evt_code'     => 'AAJ',
                    'owner'        => $owner,
                    'transaction'  => $transaction,
                    'url_endpoint' => $url,
                    'message'      => 'Auto Assign Cartons to Order in RFID case. '. json_encode($ctnMaps)
                ]);

            }

        }catch (\Exception $exception) {
            $dataError = [
                'date'     => time(),
                'api_name' => "WAP",
                'error'    => $exception->getMessage(),
            ];

            DB::table('sys_bugs')->insert($dataError);
        }

    }
}
