<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Commodity;
use League\Fractal\TransformerAbstract;

class CommodityTransformer extends TransformerAbstract
{
    public function transform(Commodity $commodity)
    {
        return [
            'cmd_id'         => $commodity->cmd_id,
            'cmd_name'       => $commodity->cmd_name,
            'cmd_des'        => $commodity->cmd_des,
            'commodity_nmfc' => $commodity->commodity_nmfc,
            'cmd_cls'        => $commodity->cmd_cls,
        ];
    }
}
