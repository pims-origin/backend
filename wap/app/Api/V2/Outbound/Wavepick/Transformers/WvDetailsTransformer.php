<?php

namespace App\Api\V2\Outbound\Wavepick\Transformers;

use App\Api\V1\Models\WavePickHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Status;

class WvDetailsTransformer extends TransformerAbstract
{
    public function transform(WavepickDtl $wvDtl)
    {
        $packSize = object_get($wvDtl, 'pack_size', 0);
        $piecesQty = object_get($wvDtl, 'piece_qty', 0);
        $actPiecesQty = object_get($wvDtl, 'act_piece_qty', 0);
        $allocCtnReq = floor($piecesQty/$packSize);
        $allocCtnAct = floor( $actPiecesQty/$packSize);
        $wv = new WavePickHdrModel();
        $wv_dtl_sts = $wv->formatStatus('wvpick', object_get($wvDtl, 'wv_dtl_sts', null));
        $arrWvDtl = [
            'wv_dtl_id' => object_get($wvDtl, 'wv_dtl_id', null),
            'wv_id' => object_get($wvDtl, 'wv_id', null),
            'item_id' => object_get($wvDtl, 'item_id', null),
            'wv_num' => object_get($wvDtl, 'wv_num', null),
            'color' => object_get($wvDtl, 'color', null),
            'sku' => object_get($wvDtl, 'sku', null),
            'size' => object_get($wvDtl, 'size', null),
            'pack_size' => $packSize,
            'ctn_qty' => object_get($wvDtl, 'ctn_qty', 0),
            //'piece_qty' => object_get($wvDtl, 'piece_qty', 0),
            'piece_qty' => $piecesQty,
            'act_piece_qty' => $actPiecesQty,
            'allocate_pieces' => $actPiecesQty . '/' . $piecesQty,
            'allocate_cartons' => ($allocCtnAct . '/' . $allocCtnReq),
            'wv_dtl_sts' => $wv_dtl_sts,
            'wv_dtl_sts_name' => Status::getByKey('WAVE_DETAIL_STATUS', $wv_dtl_sts),
            //'act_loc' => object_get($wvDtl, 'act_loc', null),
            //'act_loc_id' => object_get($wvDtl, 'act_loc_id', null),
            'created_at' => object_get($wvDtl, 'created_at', null),
        ];
        return $arrWvDtl;
    }
}
