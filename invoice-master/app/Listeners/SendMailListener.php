<?php

namespace App\Listeners;

use App\Events\UpdateInvoiceEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\Invoice as InvoiceMail;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\EmailConfigurationModel;
use Seldat\Wms2\Models\CustomerContact;
use Seldat\Wms2\Models\Setting;
use App\Helpers\SettingMailHelper;
use App\EventTracking;

class SendMailListener
{
    protected $customerConfig;
    protected $customerContact;
    protected $emailConfiguration;
    protected $settingMailHelper;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->customerConfig = new CustomerConfigModel;
        $this->customerContact = new CustomerContact;
        $this->emailConfiguration = new EmailConfigurationModel;
        $this->settingMailHelper = new SettingMailHelper;
    }

    /**
     * Handle the event.
     *
     * @param  UpdateInvoiceEvent  $event
     * @return void
     */
    public function handle(UpdateInvoiceEvent $event)
    {
        $header = $event->invoiceHeader;
        $to = $this->getSendToList($header->cus_id);
        if (!$to) {
            $this->tracking($header, 'no receiver email');
            return false;
        }
        
        $smtpRecord = $this->emailConfiguration->getSmtpInfo($header->whs_id);
        if (!$smtpRecord) {
            $this->tracking($header, 'no smtp config');
            return false;
        }
        
        $smtpConfig = json_decode($smtpRecord->whs_meta_value);
        if (!data_get($smtpConfig, 'use_system_email', ''))
            $this->settingMailHelper->setCustomConfig($smtpConfig);

        try {
            $message = new InvoiceMail($header);
            // Send mail
            Mail::to($to)
                ->send($message);
        } catch (\Exception $e) {
            $this->tracking($header, 'mail config error');
        }
    }

    public function getSendToList($cusId)
    {
        $to = [];
        $customerConfig = $this->customerConfig->getFirstWhere([
            'cus_id' => $cusId,
            'qualifier' => 'CIE'
        ]);
        $config = $customerConfig ? json_decode($customerConfig->value) : false;
        if (!$config) 
            return false;
        
        if (data_get($config, 'account_manager_email', '')) 
            $to = array_merge($to, explode(',', $config->account_manager_email));

        if (data_get($config, 'account_email', '')) 
            $to = array_merge($to, explode(',', $config->account_email));
      
        if (data_get($config, 'send_to_customer', '')) 
            $to = array_merge($to, explode(',', $config->customer_email));

        return !empty($to) ? array_unique($to) : false;
    }

    public function tracking($header, $description)
    {
        $tracking = new EventTracking();
        $tracking->create([
            'whs_id'    => $header->whs_id,
            'cus_id'    => $header->cus_id,
            'owner'     => $header->inv_num,
            'evt_code'  => 'INF',
            'trans_num' => $header->inv_num,
            'info'      => sprintf(config('constants.evt_tracking_info.ER'), $header->inv_num, $description)
        ]);
    }
}
