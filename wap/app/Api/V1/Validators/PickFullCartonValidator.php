<?php

namespace App\Api\V1\Validators;


class PickFullCartonValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'ctn_num'     => 'required',
        ];
    }
}