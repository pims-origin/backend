<?php

namespace App\Modules\GoodsReceipts\Models;

use Illuminate\Support\Facades\Config;

class AbstractModel
{
    public function __construct()
    {
        //
    }

    public function checkCusOfUser($whsId, $cusId)
    {
        $cusOfUser = Config::get('data.cusOfUser');

        foreach ($cusOfUser as $data) {
            if ($data->whs_id == $whsId && $data->cus_id == $cusId)
            {
                return true;
            }
        }

        return false;
    }
}