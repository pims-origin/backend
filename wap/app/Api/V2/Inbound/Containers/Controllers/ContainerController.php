<?php

namespace App\Api\V2\Inbound\Containers\Controllers;

use App\Api\V2\Inbound\Models\ContainerModel;
use App\Api\V2\Inbound\Models\AsnDtlModel;
use App\Api\V2\Inbound\Containers\Transformers\ContainerTransformer;
use App\Api\V2\Inbound\Containers\Transformers\SkuTransformer;
use App\MyHelper;
use Psr\Http\Message\ServerRequestInterface as Request;
use Swagger\Annotations as SWG;
use App\Api\V1\Models\Log;
use Dingo\Api\Http\Response;

class ContainerController extends AbstractController
{
    /**
     * @var ContainerModel
     */
    protected $containerModel;

    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;


    /**
     * ContainerController constructor.
     *
     * @param ContainerModel $containerModel
     */
    public function __construct(
        ContainerModel $containerModel,
        AsnDtlModel $asnDtlModel
        )
    {
        $this->containerModel = $containerModel;
        $this->asnDtlModel    = $asnDtlModel;
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param ContainerTransformer $containerTransformer
     * @return \Dingo\Api\Http\Response|void
     */
    public function search($whsId, Request $request, ContainerTransformer $containerTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        /*
         * start logs
         */

        $url = "/whs/$whsId/containers";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'LCL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Load container list'
        ]);
        /*
         * end logs
         */

        try {
            $container = $this->containerModel->loadCTNRList($input, [], array_get($input, 'limit'));

            Log::respond($request, $whsId, [
                'evt_code' => 'LCL',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'response_data' => $this->response->collection($container, $containerTransformer)
            ]);

            return $this->response->collection($container, $containerTransformer);

        } catch (\PDOException $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param SkuTransformer $skuTransformer
     * @return \Dingo\Api\Http\Response|void
     */
    public function searchSku($whsId, Request $request, SkuTransformer $skuTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        /*
         * start logs
         */

        $url = "/whs/$whsId/skus";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'LCL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Load SKU list'
        ]);
        /*
         * end logs
         */

        try {
            $skus = $this->asnDtlModel->loadSkuList($input, [], array_get($input, 'limit'));
            // dd($skus);
            Log::respond($request, $whsId, [
                'evt_code' => 'LCL',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'response_data' => $this->response->collection($skus, $skuTransformer)
            ]);

            return $this->response->collection($skus, $skuTransformer);

        } catch (\PDOException $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return new Response($this->getResponseData(), 200, [], null);
        }
    }
}
