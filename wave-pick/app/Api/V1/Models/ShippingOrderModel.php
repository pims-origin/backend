<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 11/16/2016
 * Time: 5:27 PM
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ShippingOrder;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class ShippingOrderModel
 *
 * @package App\Api\V1\Models
 */
class ShippingOrderModel extends AbstractModel
{
    /**
     * @var ShippingOrder
     */
    protected $model;

    /**
     * ShippingOrderModel constructor.
     *
     * @param ShippingOrder|null $model
     */
    public function __construct(ShippingOrder $model = null)
    {
        $this->model = ($model) ?: new ShippingOrder();
    }

    /**
     * @param $orderShippingOrderId
     *
     * @return mixed
     */
    public function deleteShippingOrder($orderShippingOrderId)
    {
        return $this->model
            ->where('ord_shipping_id', $orderShippingOrderId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrLike = [
            'ship_to_cus_name',
            'ship_to_addr',
            'ship_to_city',
            'ship_to_state',
            'ship_to_zip',
            'ship_to_country'
        ];

        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $poTotal
     *
     * @return array
     */
    public function updateImportShipping($poTotal)
    {
        $result = [];
        foreach ($poTotal as $count => $soIds) {
            $this->refreshModel();
            $result[] = $this->model
                ->whereIn('so_id', $soIds)
                ->update(['po_total' => $count]);
        }

        return $result;
    }
}
