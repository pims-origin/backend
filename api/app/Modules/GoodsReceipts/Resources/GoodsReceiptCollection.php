<?php

namespace App\Modules\GoodsReceipts\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GoodsReceiptCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => GoodsReceiptResource::collection($this->collection)
        ];
    }
}