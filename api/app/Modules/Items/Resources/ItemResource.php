<?php

namespace App\Modules\Items\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'type' => 'items',
            'id' => (int) $this->item_id,
            'attributes' => [
                'item_id'           => $this->item_id,
                'cus_id'            => $this->cus_id,
                'sku'               => $this->sku,
                'size'              => $this->size,
                'color'             => $this->color,
                'description'       => $this->description,
                'uom'               => $this->uom_code,
                'pack'              => $this->pack,
                'upc'               => $this->cus_upc,
                'length'            => $this->length,
                'width'             => $this->width,
                'height'            => $this->height,
                'dim_uom'           => $this->dim_uom,
                'net_weight'        => $this->net_weight,
                'gross_weight'      => $this->weight,
                'weight_uom'        => $this->weight_uom,
                'category'          => $this->cat_name,
                'special_handling'  => $this->spc_hdl_name
            ],
            'meta' => $this->itemMeta ? json_decode($this->itemMeta->value) : NULL
        ];
    }
}