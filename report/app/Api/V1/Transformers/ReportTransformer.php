<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use App\ReportGroup;

class ReportTransformer extends TransformerAbstract
{

    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(ReportGroup $rpGrp)
    {
        return [
            'grp_id'   => $rpGrp->grp_id,
            'desc'     => $rpGrp->desc,
            'grp_sts'  => $rpGrp->grp_sts,
            'sts'      => $rpGrp->sts,
            'disp_odr' => $rpGrp->disp_odr,
            'reports'   => $rpGrp->reports,
        ];
    }
}
