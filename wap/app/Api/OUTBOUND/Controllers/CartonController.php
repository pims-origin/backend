<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 7/25/2016
 * Time: 11:46 AM
 */

namespace App\Api\OUTBOUND\Controllers;

use App\Api\OUTBOUND\Models\CartonModel;
use App\Api\V1\Models\Log;
use App\Api\OUTBOUND\Models\PalletModel;
use App\Api\OUTBOUND\Models\CustomerModel;
use App\Api\OUTBOUND\Models\LocationModel;
use App\Api\OUTBOUND\Models\OrderCartonModel;
use App\Api\OUTBOUND\Models\OrderHdrModel;
use App\Api\OUTBOUND\Models\WavePickDtlModel;
use App\Api\V2\Inbound\Models\WavePickHdrModel;
use App\Api\V2\Outbound\Models\WaveDtlLocModel;
use App\Api\V2\Outbound\Models\InventorySummaryModel;
use App\libraries\RFIDValidate;
use App\Api\OUTBOUND\Validators\ScanCartonValidator;
use App\Jobs\AutoPackJob;
use Psr\Http\Message\ServerRequestInterface as Request;
use Maatwebsite\Excel\Excel;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Wms2\UserInfo\Data;

class CartonController extends AbstractController
{
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var DamageCartonModel
     */
    protected $damagedCartonModel;

    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    protected $odrDtlModel;
    protected $odrHdrModel;
    /**
     * @var orderCarton
     */
    protected $orderCartonModel;
    protected $wavePickHdrModel;
    protected $wavePickDtlModel;
    protected $palletModel;
    protected $customerModel;

    protected $scanCartonsValidator;

    /**
     * CartonController constructor.
     *
     * @param CartonModel $cartonModel
     * @param PalletModel $palletModel
     * @param CustomerModel $customerModel
     * @param OrderCartonModel $orderCartonModel
     * @param LocationModel $locationModel
     * @param OrderHdrModel $odrHdrModel
     * @param WavePickDtlModel $wavePickDtlModel
     * @param WavePickHdrModel $wavePickHdrModel
     */
    public function __construct
    (
        CartonModel $cartonModel,
        PalletModel $palletModel,
        CustomerModel $customerModel,
        OrderCartonModel $orderCartonModel,
        LocationModel $locationModel,
        OrderHdrModel $odrHdrModel,
        WavePickDtlModel $wavePickDtlModel,
        WavePickHdrModel $wavePickHdrModel,
        InventorySummaryModel $inventorySummaryModel
    ) {
        $this->cartonModel = $cartonModel;
        $this->scanCartonsValidator = new ScanCartonValidator();
        $this->palletModel = $palletModel;
        $this->customerModel = $customerModel;
        $this->orderCartonModel = $orderCartonModel;
        $this->locationModel = $locationModel;
        $this->odrHdrModel = $odrHdrModel;
        $this->wavePickDtlModel = $wavePickDtlModel;
        $this->wavePickHdrModel = $wavePickHdrModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
    }

    /**
     * @param $whsId
     * @param $palletRFID
     * @param Request $request
     *
     * @return array|Response
     */
    public function consolidatePallet(
        $whsId,
        $palletRFID,
        Request $request
    ) {
        /**
         * consolidate pallet
         */
        // get data from HTTP
        $input = $request->getParsedBody();

        $requireFieldsValidate = $this->scanCartonsValidator->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'][] = [];
            $msg['messages'][] = [
                'status_code' => -1,
                'msg'         => $requireFieldsValidate
            ];

            return $msg;
        }

        //get an ctn rfid to get grdetail from table ctn
        $randRfid = array_get($input, 'ctn-rfid', null);
        $arrCtnRfid = array_get($input, 'ctn-rfid', null);
        $pltRfid = $palletRFID;

        $owner = $transaction = "";

        try {
            $url = "/whs/{$whsId}/pallet/{$palletRFID}/consolidate/";
            Log:: info($request, $whsId, [
                'evt_code'     => 'ACC',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Consolidate Pallet'
            ]);

            if (!$randRfid) {
                throw new \Exception(Message::get('BM142'));
            }

            //validate pallet RFID
            $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
            if (!$pltRFIDValid->validate()) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'][] = $pltRfid;
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => $pltRFIDValid->error
                ];

                return $msg;

            }

            DB::beginTransaction();
            //check invalid code carton
            $ctnRfidInValid = [];
            $errorMsg = '';
            foreach ($arrCtnRfid as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }

            //if has errors, return all error data and message
            if ($ctnRfidInValid) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'][] = $ctnRfidInValid;
                $msg['messages'][] = [
                    'status_code' => -6,
                    'msg'         => $errorMsg
                ];

                return $msg;
            }
            $data = [];
            //check exist of pallet (new location)
            $newPltId = '';
            $newLocationId = '';
            $newCreatePltId = '';
            $checkPalletExist = $this->palletModel->checkExistedPalletAccordingToRFID($pltRfid, $whsId);
            if ($checkPalletExist) {
                $newLocationId = object_get($checkPalletExist, 'loc_id', '');
                $newPltId = object_get($checkPalletExist, 'plt_id', '');

                $cartonsInNewPlt = $this->cartonModel->findWhere(['plt_id' => $newPltId]);
                if ($cartonsInNewPlt) {
                    foreach ($cartonsInNewPlt as $carton) {
                        if (strtoupper(object_get($carton, 'ctn_sts', '')) == 'LK') {
                            $msg = [];
                            $msg['status'] = false;
                            $msg['iat'] = time();
                            $msg['data'] = [];
                            $msg['messages'][] = [
                                'status_code' => -1,
                                'msg'         => sprintf("Pallet: %s contains the cartons  is locked.", $pltRfid)
                            ];

                            return $msg;
                        }
                    }
                }
            }

            //check exist of cartons
            $noExistCartons = '';
            $exceptCartons = '';
            $exceptCartonsStatus = '';
            $oldLocIdArr = [];
            $oldPltIdArr = [];
            $assignedCartons = '';
            $respectiveOdrNum = '';
            $canceledCartons = '';
            $notTheSameCus = '';
            foreach ($arrCtnRfid as $ctnRfid) {
                $checkCartonsExist = $this->cartonModel->checkExistedCartonAccordingToRFID($ctnRfid, $whsId);

                $ctnSts = strtoupper(object_get($checkCartonsExist, 'ctn_sts', ''));

                if (!$checkCartonsExist) {
                    $noExistCartons .= $ctnRfid . ' ';
                } elseif ($ctnSts != 'AC' && $ctnSts != 'PD' && $ctnSts != 'PB') {
                    $exceptCartons .= $ctnRfid . ' ';
                    $exceptCartonsStatus .= Status::getByValue($checkCartonsExist->ctn_sts, "CTN_STATUS") . ' ';
                }

                if ($ctnSts == 'PD' || $ctnSts == 'PB') {
                    //Check Carton is assigned to order or not
                    $orderCarton = $this->orderCartonModel->getFirstBy('ctn_rfid', $ctnRfid);
                    if ($orderCarton) {
                        $odrHdrId = object_get($orderCarton, 'odr_hdr_id', '');
                        if ($odrHdrId) {
                            $getorderInfo = $this->odrHdrModel->getFirstBy('odr_id', $odrHdrId);
                            if ($getorderInfo) {
                                $odrSts = object_get($getorderInfo, 'odr_sts', '');
                                $odrnum = object_get($getorderInfo, 'odr_num', '');
                                if ($odrSts != 'CC') {
                                    if ($ctnSts == 'PD') {
                                        $assignedCartons .= $ctnRfid . ' ';
                                        $respectiveOdrNum .= $odrnum . ' ';
                                    } elseif ($ctnSts == 'PB') {
                                        $canceledCartons .= $ctnRfid . ' ';
                                    }
                                }
                            }
                        }
                    }
                }

                //get old locked location_id
                if ($checkCartonsExist) {
                    $oldLocId = object_get($checkCartonsExist, 'loc_id', '');
                    if ($oldLocId) {
                        array_push($oldLocIdArr, $oldLocId);
                    }

                    //old pallets
                    $oldPltId = object_get($checkCartonsExist, 'plt_id', '');
                    if ($oldPltId) {
                        array_push($oldPltIdArr, $oldPltId);
                    }
                }

                //Current Pallet and New Pallet do not belong to the same customer
                if ($checkCartonsExist && !$checkPalletExist && !$newCreatePltId) {
                    $cus_id = object_get($checkCartonsExist, 'cus_id', '');
                    $newCreatePltId = $this->palletModel->createNewPallet($pltRfid, $whsId, $cus_id)->plt_id;
                    $newPltId = $newCreatePltId;
                }

                if ($newPltId) {
                    $palletToInfo = $this->palletModel->checkPltSameCustomer(
                        object_get($checkCartonsExist, 'whs_id', ''),
                        object_get($checkCartonsExist, 'cus_id', ''),
                        $newPltId
                    );
                    if (empty($palletToInfo)) {
                        $notTheSameCus .= $ctnRfid . ' ';
                    }
                }
            }


            //check status of old location from cartons
            $lockedLocArr = '';
            if ($oldLocIdArr) {
                $oldLocationInfos = $this->locationModel->getLocationByIds($oldLocIdArr);
                if ($oldLocationInfos) {
                    foreach ($oldLocationInfos->toArray() as $oldLocationInfo) {
                        if (strtoupper($oldLocationInfo['loc_sts_code']) == "LK") {
                            $lockedLocArr .= $oldLocationInfo['loc_code'] . ' ';
                        }
                    }
                }
            }

            if ($lockedLocArr) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = [];
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => sprintf("Locations: %s contains the cartons  is locked.",
                        $lockedLocArr)
                ];

                return $msg;
            }

            if ($noExistCartons) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = [];
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => sprintf("These cartons: %s are not existed.", $noExistCartons)
                ];

                return $msg;
            }
            //Current Pallet and New Pallet do not belong to the same customer
            if ($notTheSameCus) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = [];
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => sprintf("Unable to consolidate cartons with mixed customers on a pallet")
                ];

                return $msg;
            }
            if ($exceptCartons) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = [];
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => sprintf("These cartons: %s with respective status are %s, their status should be Active or Picked or Put Back.",
                        $exceptCartons,
                        trim($exceptCartonsStatus))
                ];

                return $msg;
            }

            //Show message: Carton is assigned to order or not
            if ($assignedCartons) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = [];
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => sprintf("These cartons: %s assigned to order %s and not yet cancel.", trim
                    ($assignedCartons), trim($respectiveOdrNum))
                ];

                return $msg;
            }

            //Show message: Carton is assigned to order or not
            if ($canceledCartons) {
                $msg = [];
                $msg['status'] = false;
                $msg['iat'] = time();
                $msg['data'] = [];
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => sprintf("These cartons: %s have not yet canceled from order", trim
                    ($canceledCartons))
                ];

                return $msg;
            }

            //check status of new location
            if ($newLocationId) {
                $newLocationInfo = $this->locationModel->getFirstBy('loc_id', $newLocationId);
                $newLocationStatus = object_get($newLocationInfo, 'loc_sts_code', '');

                if (strtoupper($newLocationStatus) == "LK") {
                    $msg = [];
                    $msg['status'] = false;
                    $msg['iat'] = time();
                    $msg['data'] = [];
                    $msg['messages'][] = [
                        'status_code' => -1,
                        'msg'         => sprintf("Location contains the %s pallet is locked.",
                            $pltRfid)
                    ];

                    return $msg;
                }
            }

            //Start update data
            //update wave pick
            $wavePickPickeds = $this->updateWaveDtl($whsId, $arrCtnRfid, $request);

            //update plt_id, and ctn_sts=AC  to cartons
            $this->cartonModel->updatePalletID($newPltId, $whsId, $arrCtnRfid);

            //count cartons of the new pallet
            $ctnInfo = $this->cartonModel->countCartonsByPalletId($newPltId);
            //remove location for all cartons in new pallet
            $this->cartonModel->removeAllLocForCtnInPallet($newPltId, $whsId);

            $ctnTtl = 0;
            if ($ctnInfo) {
                $ctnTtl = $ctnInfo->numberOfCarton;
            }
            //update ctn_ttl, remove locations for the new pallet
            $this->palletModel->updateCtnTtlForPallet($newPltId, $ctnTtl);

            //update ctn_ttl and remove loc if ctn_ttl = 0 for old pallets
            foreach ($oldPltIdArr as $oldPltId) {
                //count cartons of the old pallet
                $oldCtnInfo = $this->cartonModel->countCartonsByPalletId($oldPltId);
                $oldCtnTtl = 0;
                if ($oldCtnInfo) {
                    $oldCtnTtl = $oldCtnInfo->numberOfCarton;
                }
                //update ctn_ttl, remove locations for the old pallet
                $this->palletModel->updateWhere(
                    [
                        'ctn_ttl' => $oldCtnTtl
                    ],
                    [
                        'plt_id' => $oldPltId
                    ]
                );
                if ($oldCtnTtl == 0) {
                    $this->palletModel->updateWhere(
                        [
                            'loc_id'     => null,
                            'loc_code'   => null,
                            'loc_name'   => null,
                            'rfid'       => null,
                            'deleted'    => 1,
                            'deleted_at' => time(),
                        ],
                        [
                            'plt_id' => $oldPltId
                        ]
                    );
                }
            }

            DB::commit();

            //Start show messages
            //return result: get all cartons according to the pallet
            $allCtnsByPlt = $this->cartonModel->getAllCtnByPltId($whsId, $newPltId);
            //$allCtnsByCtnRfids = $this->cartonModel->getAllCtnByCtnRfids($whsId, $arrCtnRfid);

            $cartons = [];
            $skus = [];
            if ($allCtnsByPlt) {
                foreach ($allCtnsByPlt as $ctnKey => $allCtnByRfid) {
                    $ctnRfid = object_get($allCtnByRfid, 'rfid', null);
                    $sku = object_get($allCtnByRfid, 'sku', null);
                    $cartons[] = [
                        'rfid'    => $ctnRfid,
                        'ctn_num' => object_get($allCtnByRfid, 'ctn_num', null),
                        'ctn_sts' => object_get($allCtnByRfid, 'ctn_sts', null),
                        'sku'     => $sku
                    ];
                    //List Sku in List Cartons
                    $skus[] = [
                        'sku' => $sku
                    ];

                    //add more "is_new" field for consolidated cartons
                    if (in_array($ctnRfid, $arrCtnRfid)) {
                        $cartons[$ctnKey]['is_new'] = 1;
                    }
                }
            }

            //return result: get pallet info
            $palletByIdInfo = $this->palletModel->getPalletById($newPltId);
            $pallet = [];
            if ($palletByIdInfo) {
                $pallet = [
                    'rfid'     => object_get($palletByIdInfo, 'rfid', null),
                    'plt_num'  => object_get($palletByIdInfo, 'plt_num', null),
                    'loc_name' => object_get($palletByIdInfo, 'loc_name', null),
                ];

            }

            //return result: get customer info
            $cusByIdInfo = $this->customerModel->getCusById($palletByIdInfo->cus_id);
            $customer = [];
            if ($cusByIdInfo) {
                $customer = [
                    'cus_id'   => object_get($cusByIdInfo, 'cus_id', null),
                    'cus_name' => object_get($cusByIdInfo, 'cus_name', null)
                ];

            }

            $sortedSkus = array_column($skus, 'sku');
            sort($sortedSkus);

            $data['customer'] = $customer;
            $data['skus'] = array_unique($sortedSkus);
            $data['pallet'] = $pallet;
            $data['cartons'] = $cartons;
            $data['wv_sts'] = $wavePickPickeds;

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();
            $msg['messages'][] = [
                'status_code' => 1,
                'msg'         => sprintf("Pallet %s consolidated successfully!", $pltRfid)
            ];
            $msg['data'] = [$data];

            return $msg;

        } catch (\Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => "/whs/{$whsId}/pallet/{$palletRFID}/consolidate/",
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    /**
     * @param $whsId
     * @param $arrCtnRfid
     *
     * @return array
     */
    public function updateWaveDtl($whsId, $arrCtnRfid, $request)
    {
        $wvIds = [];
        foreach ($arrCtnRfid as $ctnRfid) {
            $checkCartonsExist = $this->cartonModel->checkExistedCartonAccordingToRFID($ctnRfid, $whsId);
            if (strtoupper(object_get($checkCartonsExist, 'ctn_sts', '')) == 'PD'
            ) {
                //Check Carton is assigned to order or not
                $orderCarton = $this->orderCartonModel->getFirstBy('ctn_rfid', $ctnRfid);
                if ($orderCarton) {
                    $odrHdrId = object_get($orderCarton, 'odr_hdr_id', '');
                    $wvDtlId = object_get($orderCarton, 'wv_dtl_id', '');
                    $wvId = object_get($orderCarton, 'wv_hdr_id', '');
                    $wvIds[] = $wvId;

                    // update wavepick is picking with v1 update wavepick
                    if ($wvId) {
                        $wvObj = $this->wavePickHdrModel->getFirstWhere(['wv_id' => $wvId]);
                        if ($wvObj->wv_sts == "CO") {
                            $wvObj->wv_sts = 'PK';
                            $wvObj->save();
                        }
                    }

                    // update carton in wavepick detail location
                    // $this->_updateWvDtlLocInReturn([$orderCarton->toArray()], $wvDtlId);

                    $returnQTY = object_get($orderCarton, 'piece_qty', '');
                    $pieceQtyOrdCtn = object_get($orderCarton, 'piece_qty', '');
                    if ($odrHdrId) {
                        $getorderInfo = $this->odrHdrModel->getFirstBy('odr_id', $odrHdrId);
                        if ($getorderInfo) {
                            $odrSts = object_get($getorderInfo, 'odr_sts', '');
                            if ($odrSts == 'CC') {
                                //update odr_carton status to PB
                                $this->orderCartonModel->updateOrderCarton($ctnRfid);
                                //update wave pick
                                $wavePickDtlinfo = $this->wavePickDtlModel->getFirstBy('wv_dtl_id', $wvDtlId);
                                $actPieceQty = object_get($wavePickDtlinfo, 'act_piece_qty', '');
                                $actPieceQtyNew = $actPieceQty - $pieceQtyOrdCtn;
                                $pieceQty = object_get($wavePickDtlinfo, 'piece_qty', 0);

                                $this->_updateWvDtl($wvDtlId, $actPieceQtyNew, $pieceQty);

                                //Update inventory summary when canceled order
                                $checkUpdateInvtsum = true;
                                $this->_updateInvtSmrCancelOrder($orderCarton, $returnQTY, $whsId);
                            } else {
                                // update invetory summary
                                $wvDtl = $this->wavePickDtlModel->getProperlyWvDtl($wvId, $wvDtlId, $whsId);
                                if ($wvDtl) {
                                    $this->_updateInvtSmrReturnPicking($wvDtl, $returnQTY);
                                }
                            }
                        }
                    } else {
                        // update invetory summary
                        $wvDtl = $this->wavePickDtlModel->getProperlyWvDtl($wvId, $wvDtlId, $whsId);
                        if ($wvDtl) {
                            $this->_updateInvtSmrReturnPicking($wvDtl, $returnQTY);
                        }
                        //update odr_carton status to PB
                        $this->orderCartonModel->updateOrderCarton($ctnRfid);
                        //update wave pick
                        $wavePickDtlinfo = $this->wavePickDtlModel->getFirstBy('wv_dtl_id', $wvDtlId);
                        $actPieceQty = object_get($wavePickDtlinfo, 'act_piece_qty', '');
                        $actPieceQtyNew = $actPieceQty - $pieceQtyOrdCtn;
                        $pieceQty = object_get($wavePickDtlinfo, 'piece_qty', 0);

                        $this->_updateWvDtl($wvDtlId, $actPieceQtyNew, $pieceQty);
                    }
                }
            }
        }

        $wvIds = array_unique($wvIds);
        $wavePickPickeds = [];
        if (count($wvIds) > 0) {
            foreach ($wvIds as $wvId) {
                if ($wvId) {
                    //update wave header status
                    $wvSts = $this->wavePickHdrModel->updatePicked($wvId);
                    //Call Auto Pack Job if pick successful
                    if ($wvSts == 1) {
                        dispatch(new AutoPackJob($wvId, $request));
                    }
                    // set wavepick status is cancel
                    // 1. Wavepick that  is not overpick will change to Completed when all orders are picked
                    // 2. Wavepick that  is not overpick will change to Canceled when all orders are canceled
                    // Else wavepick is PICKING
                    if ($wvSts == 0) {
                        $wvSts = $this->wavePickHdrModel->updateCanceled($wvId);
                    }

                    $wavePickPickeds[] = [
                        'wv_id'  => $wvId,
                        'picked' => $wvSts,
                    ];
                }
            }
        }

        return $wavePickPickeds;
    }

    /**
     * @param $cartons
     * @param $wvDtlId
     */
    private function _updateWvDtlLocInReturn($cartons, $wvDtlId)
    {
        $wvDtlLoc = (new WaveDtlLocModel())->getModel()->where('wv_dtl_id', $wvDtlId)->first();

        if ($wvDtlLoc && $wvDtlLoc->act_loc_ids) {
            $actLocs = json_decode($wvDtlLoc->act_loc_ids, true);
            $ctnIdsCheck = array_pluck($cartons, 'ctn_id');
            foreach ($actLocs as $keyLoc => $actLoc) {
                foreach ($actLoc['cartons'] as $keyCtn => $cartonOld) {
                    if (in_array($cartonOld['ctn_id'], $ctnIdsCheck)) {
                        // remove carton
                        unset($actLocs[$keyLoc]['cartons'][$keyCtn]);
                        // update location
                        foreach ($cartons as $carton) {
                            if ($cartonOld['ctn_id'] == $carton['ctn_id']) {
                                // return QTY
                                $actLocs[$keyLoc]['picked_qty'] -= $carton['piece_qty'];
                                break;
                            }
                        }

                    }
                }
                // update carton
                $actLocs[$keyLoc]['cartons'] = array_values($actLocs[$keyLoc]['cartons']);
                if (count($actLocs[$keyLoc]['cartons']) == 0) {
                    // remove location
                    unset($actLocs[$keyLoc]);
                }
            }
            $wvDtlLoc->act_loc_ids = json_encode(array_values($actLocs));

            $wvDtlLoc->update();
        }
    }

    /**
     * @param $wvData
     * @param $returnQTY
     *
     * @throws \Exception
     */
    private function _updateInventorySummary($wvData, $pickedQTY)
    {
        $piece_qty = object_get($wvData, 'piece_qty');
        $act_piece_qty = object_get($wvData, 'act_piece_qty');
        $item_id = object_get($wvData, 'item_id');
        $whs_id = object_get($wvData, 'whs_id');
        $lot = object_get($wvData, 'lot');

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $item_id,
                'whs_id'  => $whs_id,
                'lot'     => $lot,
            ]
        );

        $invtAvail     = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;

        $overAvailQTY = 0;
        if ($invtAllocated < $pickedQTY) {
            if ($invtAllocated + $invtAvail < $pickedQTY) {
                $msg = "Not enough allocated QTY to pick";
                throw new \Exception($msg);
            }

            $wvDataArr = json_decode($wvData->data, true);
            if (!$wvDataArr) {
                $wvDataArr['avail']    = 0;
                $wvDataArr['allocated'] = 0;
            }

            $overAvailQTY = $pickedQTY - $invtAllocated;
            $wvDataArr['avail'] += $overAvailQTY;

            $wvData->data = json_encode($wvDataArr);
            $wvData->update();
        }

        $allocatedSql = sprintf('`allocated_qty` - %d', $pickedQTY - $overAvailQTY);
        $pickedSql    = sprintf('`picked_qty` + %d', $pickedQTY);
        $availSql     = sprintf('`avail` - %d', $overAvailQTY);

        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $item_id,
            'whs_id'  => $whs_id,
            'lot'     => $lot,
        ]);
    }

    /**
     * @param $wvData
     * @param $returnQTY
     *
     * @throws \Exception
     */
    private function _updateInvtSmrReturnPicking($wvData, $returnQTY)
    {
        $piece_qty = object_get($wvData, 'piece_qty');
        $act_piece_qty = object_get($wvData, 'act_piece_qty');
        $item_id = object_get($wvData, 'item_id');
        $whs_id = object_get($wvData, 'whs_id');
        $lot = object_get($wvData, 'lot');

        $returnOverPicking = 0;
        // piece qty < actual picked

        if ($piece_qty <= $act_piece_qty - $returnQTY) {
            // over picking
            $returnOverPicking = $returnQTY;
        } elseif ($act_piece_qty > $piece_qty) {
            // over picking a part
            $returnOverPicking = $act_piece_qty - $piece_qty;
        }

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $item_id,
                'whs_id'  => $whs_id,
                'lot'     => $lot,
            ]
        );

        $invtAvail = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;
        $invtPicked = (int)$ivt->picked_qty;

        $wvDataArr = json_decode($wvData->data, true);
        $wvDataArr = json_decode($wvData->data, true);
        if (!$wvDataArr) {
            $wvDataArr['avail'] = 0;
            $wvDataArr['allocated'] = 0;
        }

        $returnAllocatedQTY = 0;
        $returnAvailQTY = 0;

        // return picking < overpicking
        if ($returnOverPicking < $wvDataArr['allocated'] + $wvDataArr['avail']) {
            // return to available is higher priority
            if ($returnOverPicking < $wvDataArr['avail']) {
                $returnAvailQTY = $returnOverPicking;
            } else {
                $returnAllocatedQTY = $returnOverPicking - $wvDataArr['avail'];
                $returnAvailQTY = $wvDataArr['avail'];
            }
        } else {
            // return to inventory allocated Qty a part
            $returnAllocatedQTY = $wvDataArr['allocated'];
            $returnAvailQTY = $returnOverPicking - $wvDataArr['allocated'];
        }

        $wvDataArr['allocated'] = $wvDataArr['allocated'] - $returnAllocatedQTY;
        $wvDataArr['avail'] = $wvDataArr['avail'] - $returnAvailQTY;
        $wvDataArr['avail'] = $wvDataArr['avail'] < 0 ? 0 : $wvDataArr['avail'];

        $wvData->data = json_encode($wvDataArr);
        $wvData->update();

        if ($invtPicked < $returnQTY) {
            $msg = "Not enough picked QTY to return picking";
            throw new \Exception($msg);
        }

        $allocatedSql = sprintf('`allocated_qty` + %d', $returnQTY - $returnAvailQTY);
        $pickedSql = sprintf('`picked_qty` - %d', $returnQTY);
        $availSql = sprintf('`avail` + %d', $returnAvailQTY);

        $this->inventorySummaryModel->updateWhere([
            'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $item_id,
            'whs_id'  => $whs_id,
            'lot'     => $lot,
        ]);
    }

    private function _updateInvtSmrCancelOrder($orderCarton, $returnQTY, $whs_id)
    {
        $item_id = object_get($orderCarton, 'item_id');
        //$whs_id = object_get($orderCarton, 'whs_id');
        $lot = object_get($orderCarton, 'lot');

        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $item_id,
                'whs_id'  => $whs_id,
                'lot'     => $lot,
            ]
        );

        $invtAvail = (int)$ivt->avail;
        $invtAllocated = (int)$ivt->allocated_qty;
        $invtPicked = (int)$ivt->picked_qty;


        if ($invtPicked < $returnQTY) {
            $msg = "Not enough picked QTY to return picking";
            throw new \Exception($msg);
        }

        //$allocatedSql = sprintf('`allocated_qty` + %d', $returnQTY - $returnAvailQTY);
        $pickedSql = sprintf('`picked_qty` - %d', $returnQTY);
        $availSql = sprintf('`avail` + %d', $returnQTY);

        $this->inventorySummaryModel->updateWhere([
            //'allocated_qty' => DB::raw($allocatedSql),
            'picked_qty'    => DB::raw($pickedSql),
            'avail'         => DB::raw($availSql),
        ], [
            'item_id' => $item_id,
            'whs_id'  => $whs_id,
            'lot'     => $lot,
        ]);
    }

    private function _updateWvDtl($wvDtlId, $actPieceQtyNew, $pieceQty)
    {
        $wvDtlSts = Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS");
        if ($actPieceQtyNew >= $pieceQty) {
            $wvDtlSts = Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS");
        }
        $this->wavePickDtlModel->updateWvDtl($actPieceQtyNew, $wvDtlId, $wvDtlSts);
    }
}
