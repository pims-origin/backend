<?php

namespace App\Api\V3\Inbound\Models;



use Seldat\Wms2\Models\WarehouseMeta;

class WarehouseMetaModel extends AbstractModel
{
    protected $model;

    public function __construct(WarehouseMeta $model = null)
    {
        $this->model = ($model) ?: new WarehouseMeta();
    }

    public function countWarehouseMeta($warehouseId, $qualifier)
    {
        $query = $this->model->where('whs_id', $warehouseId)
            ->where('whs_qualifier', $qualifier)
            ->where('whs_meta_value', 1);

        return $query->count();
    }

}
