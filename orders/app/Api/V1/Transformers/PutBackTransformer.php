<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReturnHdr;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\OrderDtl;
use App\Api\V1\Models\OrderHdrModel;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class PutBackTransformer extends TransformerAbstract
{
    /**
     * @param ReturnHdr $returnHdr
     *
     * @return array
     */
    public function transform(ReturnHdr $returnHdr)
    {
        return [
            'return_id'       => object_get($returnHdr, 'return_id', ''),
            'return_num'      => object_get($returnHdr, 'return_num', ''),
            'odr_id'          => object_get($returnHdr, 'odr_hdr_id', ''),
            'odr_num'         => object_get($returnHdr, 'odr_hdr_num', ''),
            'cus_id'          => object_get($returnHdr, 'odrHdr.customer.cus_id', ''),
            'cus_name'        => object_get($returnHdr, 'odrHdr.customer.cus_name', ''),
            'return_sts'      => object_get($returnHdr, 'return_sts', ''),
            'return_sts_name' => Status::getByKey("RETURN-ORDER", object_get($returnHdr, 'return_sts', '')),
            'num_of_sku'      => OrderDtl::where('odr_id',
                $returnHdr->odr_hdr_id)->distinct('item_id')->count('item_id'),
            'return_qty'      => object_get($returnHdr, 'return_qty', ''),
            //get item for return order detail
            'details'           => $returnHdr->details
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }
}
