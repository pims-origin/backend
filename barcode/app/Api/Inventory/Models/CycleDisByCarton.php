<?php
/**
 * Created by PhpStorm.
 * User: duy
 * Date: 20/10/2016
 * Time: 14:33
 */

namespace App\Api\Inventory\Models;

class CycleDisByCarton extends CycleDisModel
{
    /**
     * @param $cyleDtl
     */
    public function getDiscrepancyCarton($cyleDtl)
    {
        $result = [];

        if (!$cyleDtl) {
            return $result;
        }

        $acQty = $cyleDtl['act_qty'] !== null ? $cyleDtl['act_qty'] : $cyleDtl['sys_qty'];

        $discrepancy = $acQty - $cyleDtl['sys_qty'];

        if ($discrepancy > 0) {
            $result = $this->getCartonToClone($cyleDtl, $discrepancy);
        } else {
            $result = $this->getCartonToRemove($cyleDtl, $discrepancy);
        }

        return $result;
    }
}