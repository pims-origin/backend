<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 02-Nov-16
 * Time: 10:23
 */

namespace App\Api\V1\Traits;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerColorModel;
use App\Api\V1\Models\InventoryModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\ZoneModel;
use App\Api\V1\Models\WarehouseModel;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class WarehouseControllerTrait
 *
 * @package App\Api\V1\Traits
 */
trait WarehouseControllerTrait
{
    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var $customerModel
     */
    protected $customerModel;

    /**
     * @var $zoneModel
     */
    protected $zoneModel;

    /**
     * @return mixed
     */
    public function listLayout(Request $request)
    {
        $input = $request->getQueryParams();

        try {
            $with = [
                //'pallet',
                //'zone.customerZone.customer',
                'locationType'
            ];
            if(!empty($input['sku'])){
                array_push($with, 'cartons');
            }
            $cusId = null;
            if (isset($input['code']) && !empty($input['code'])) {
                $cusId = DB::table('customer')->where('deleted', 0)->where('cus_code', $input['code'])->value('cus_id');
            }

            $options=$input;
            $options['cus_id']=$cusId;
            $aisleList = $this->locationModel->loadAisleListPagination($this->whs_id, $options, 'getAisle');

            $options=$input;
            $options['cus_id']=$cusId;
            $options['aisle']=$aisleList;
            $locations = $this->locationModel->loadAisleListPagination($this->whs_id, $options, 'getLoc');

//            $customer_in_layouts = $this->locationModel->getCustomerInWarehouseLayout($this->whs_id);
//            $customer_in_layouts = $this->locationModel->getCustomerInWarehouseLayout2($this->whs_id, $aisleList, $cusId);

            $customer_in_layouts = $this->locationModel->getCustomerInWarehouseLayout3($locations,$this->whs_id);

            $customer_colors = $this->locationModel->getCustomerColor();

            /*foreach ($customer_in_layouts as &$customer_in_layout) {
                foreach ($customer_colors as $customer_color) {
                    if ($customer_in_layout['cus_id'] === $customer_color['cus_id']) {
                        $customer_in_layout = array_merge($customer_in_layout, $customer_color);
                    }
                }
            }*/

            $options=$input;
            $options['cus_id']=$cusId;
            $total = $this->locationModel->loadAisleListPagination($this->whs_id, $input,'count');
            $limit = (int)(!empty($input['limit'])) ? (int)$input['limit'] : 5;
            $page = (int)(!empty($input['page'])) ? (int)$input['page'] : 1;
            $total_pages = (int)(ceil($total / $limit));
            if($total > 0){
                if($page == $total_pages){
                    $count = $total - ($page - 1)*$limit;
                }
                else{ $count = $limit; }
            }
            else{ $count = 0; }
            $pagePrev = ($page < 2) ? '' : ($page - 1);
            $pageNext = ($page == $total_pages) ? $total_pages : ($page + 1);
            $loc_code = (!empty($input['loc_code'])) ? (int)$input['loc_code'] : '';
            $sku = (!empty($input['sku'])) ? (int)$input['sku'] : '';
            $lot = (!empty($input['lot'])) ? (int)$input['lot'] : '';
            $code = (!empty($input['code'])) ? (int)$input['code'] : '';
            $type = (!empty($input['type'])) ? (int)$input['type'] : '';
            $spc_hdl_code = (!empty($input['spc_hdl_code'])) ? (int)$input['spc_hdl_code'] : '';
            $meta = [
                'pagination' => [
                    'count' => $count,
                    'total' => $total,
                    'per_page' => $limit,
                    'current_page' => $page,
                    'total_pages' => $total_pages,
                    'links' => [
                        'prev' => url('/'). '/v1/whs_layout?spc_hdl_code='. $spc_hdl_code .'&loc_code=' . $loc_code . '&sku=' . $sku . '&lot=' . $lot . '&code=' . $code . '&type=' . $type . '&page=' . $pagePrev . '&limit=' . $limit,
                        'next' => url('/'). '/v1/whs_layout?spc_hdl_code='. $spc_hdl_code .'&loc_code=' . $loc_code . '&sku=' . $sku . '&lot=' . $lot . '&code=' . $code . '&type=' . $type . '&page=' . $pageNext . '&limit=' . $limit,
                    ],
                ],
            ];

            $layouts = [];
            //$cusLocs = array_pluck($locations, 'zone.customer_zone.customer', 'loc_id');
            //$zoneLocs = array_pluck($locations, 'zone', 'loc_id');
            $zoneLocs=[];
            // $customerColor = (new CustomerColorModel())->getAllCustomer()->toArray();
            // $colors = array_pluck($customerColor, null, 'cus_id');

            // $useCus = [];
            // $useZone = [];
            // $specialZone = [];
            // $ec = false;
            // $checkCusZone = ['cus' => [], 'zone' => []];
            $loc_parent = [];
            // if (!empty($input['code'])) {
            //     $code = $input['code'];
            //     $cus_id = DB::table('customer')->where('cus_code', '=', $code)->value('cus_id');
            //     $loc_cus = DB::table('cartons')
            //     ->select(['location.loc_id', 'location.loc_code'])
            //     ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            //     ->where('cartons.cus_id', '=', $cus_id)
            //     ->where('cartons.whs_id', '=', $this->whs_id)
            //     ->distinct()
            //     ->get();
            //     $loc_parent = array_map(fun$cusLocsction ($loc_code) {
            //         return $loc_code['loc_code'];
            //     }, $loc_cus);
            // }

            foreach ($locations as $location) {
                $keyRack = $location['aisle'];

                $locType = array_get($location, 'location_type.loc_type_code', null);
                if ($locType == "SHP") {
                    $keyRack = 'SHIPPING';
                }

                if ($locType == "XDK") {
                    $keyRack = 'X-Dock';
                }

                $key2 = preg_replace("/[A-Z]/", "", $location['level']);

                if (empty($layouts[$keyRack][$key2])) {
                    $layouts[$keyRack][$key2] = [];
                }

                $locId = $location['loc_id'];
                $loc_color='#fff';
                $loc_cus_name=null;
                $has_lpn = true;
                if(isset( $customer_in_layouts[$locId]))
                {
                    $cusId = $customer_in_layouts[$locId]['cus_id'];
                    $cus_in_loc=$customer_in_layouts[$locId]['cnt'];
                    $loc_color = $customer_colors[$cusId]['cl_code'] ?? '#fff';
                    $loc_cus_name = $customer_colors[$cusId]['cus_name'] ?? null;
                    if ($cus_in_loc > 1)
                        $loc_color = '#000';
                }

                $customer = [
                    'id'   => null,
                    'code' => null,
                    'name' => $loc_cus_name,
                    'color'    => $loc_color
                ];

                //     if (isset($input['id']) && $input['id'] != $cusId) {
                //         $customer['color'] = '#FFFFFF';
                //     }

                //     if (empty($checkCusZone['cus'][$cusId])) {
                //         $myCus = [
                //             'cus_id'   => array_get($cusLocs, "$locId.cus_id", null),
                //             'cus_code' => array_get($cusLocs, "$locId.cus_code", null),
                //             'cus_name' => array_get($cusLocs, "$locId.cus_name", null),
                //             'color'    => array_get($colors, "$cusId.cl_code", null)
                //         ];
                //         if (isset($input['id']) && $input['id'] != $cusId) {
                //             $myCus['color'] = '#FFFFFF';
                //         }
                //         $myCus['cl_name'] = array_get($colors, "$cusId.cl_name", null);
                //         $myCus['cl_code'] = array_get($colors, "$cusId.cl_code", null);
                //         $useCus[] = $myCus;
                //         $checkCusZone['cus'][$cusId] = 1;
                //     }
                // }

                $zone = null;

                $haveCtn = "NA";
                if ($locType == "ECO") {
                    // $ec = true;
                    $haveCtn = 0;
                    $customer = [
                        'id'   => null,
                        'code' => "ECO",
                        'name' => "Ecommerce",
                        'color'    => "#000000"
                    ];
                    if ((new CartonModel())->checkWhere(['whs_id' => $this->whs_id, 'loc_id' => $locId])) {
                        $haveCtn = 1;
                    }
                }

                // filter loc_code and sku
                /*$is_active = 0;
                if(!empty($input['sku'])){
                    $exist_sku = 0;
                    foreach($location['cartons'] as $carton){
                        if($carton['sku'] == $input['sku'] && (empty($input['lot']) || $carton['lot'] == $input['lot'])){
                            $exist_sku = 1;
                            break;
                        }
                    }
                    if(!empty($input['loc_code'])){
                        if((!empty($input['loc_code']) && $input['loc_code'] == $location['loc_alternative_name'])
                            && $exist_sku == 1){
                            $is_active = 1;
                        }
                    }else{
                        if($exist_sku == 1){
                            $is_active = 1;
                        }
                    }
                }else{
                    if(!empty($input['loc_code'])){
                        if(!empty($input['loc_code']) && $input['loc_code'] == $location['loc_alternative_name']){
                            $is_active = 1;
                        }
                    }
                }*/
                // end filter loc_code and sku

                $data = !empty($input['type']) && $input['type'] == "zone" ? $zone : $customer;
                // if(!empty($input['code'])) {
                //     if($input['code'] == $data['code']){
                //         $layouts[$keyRack]['filter'] = 1;
                //     }
                //     if (in_array($location['loc_alternative_name'], $loc_parent)) {
                //         $layouts[$keyRack]['filter'] = 1;
                //     }
                // }

                /*if($is_active == 1) {
                    $layouts[$keyRack]['search'] = 1;
                }*/

                $rNum = str_pad(preg_replace("/[A-Z]/", "", $location['row']), 4, '0', STR_PAD_LEFT);
                $bNum = str_pad(preg_replace("/[A-Z]/", "", $location['bin']), 4, '0', STR_PAD_LEFT);

                // $layouts[$keyRack][$loc[$endKey - 1]][] = [
                $layouts[$keyRack][$key2][(int)($rNum.$bNum)] = [
                    'data'                 => $data,
                    'pallet'               => $has_lpn ? 1 : 0,//($location['pallet'] ? 1 : 0),
                    'loc_id'               => $location['loc_id'],
                    'loc_alternative_name' => $location['loc_alternative_name'],
                    'loc_sts_code'         => $location['loc_sts_code'],
                    'loc_type'             => $locType,
                    'haveCtn'              => $haveCtn,
                    //'active'               => $is_active
                ];

                asort($layouts[$keyRack][$key2]);
                krsort($layouts[$keyRack]);
            }

            ksort($layouts, SORT_NATURAL | SORT_FLAG_CASE);
            $temp = $layouts;
            reset($temp);
            $firsKey = key($temp);
            if (!empty($layouts[$firsKey])) {
                ksort($layouts[$firsKey]);
            }

            // filter by cus_id or zone_id
            // if(!empty($input['code'])) {
            //     foreach($layouts as $rack => $layout) {
            //         if(empty($layout['filter'])) {
            //             unset($layouts[$rack]);
            //         } else {
            //             unset($layouts[$rack]['filter']);
            //         }
            //     }
            // }

            // search by sku and loc_code
            /*if (!empty($input['sku']) || !empty($input['loc_code'])) {
                foreach($layouts as $rack => $layout) {
                    if(empty($layout['search'])) {
                        unset($layouts[$rack]);
                    } else {
                        unset($layouts[$rack]['search']);
                    }
                }
            }*/

            // make beauty data
            $results = [];
            foreach($layouts as $rack=>$layout) {
                $results[] = [
                    "aisle_label" =>  $rack,
                    "aisle"       => array_values($layout)
                ];
            }


//            if ($ec) {
//                $useCus[] = [
//                    'cus_id'   => null,
//                    'cus_code' => "ECO",
//                    'cus_name' => "Ecommerce",
//                    'color'    => '#000000',
//                    'cl_name'  => "Black",
//                    'cl_code'  => "#000000",
//                ];
//            }

//            $useZone = array_filter($useZone);
//            $useZone = array_values($useZone);
//            $specialZone = array_values($specialZone);
             //\Seldat\Wms2\Utils\Profiler::getQueryLog(false);


            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'layout'   => $results,
                    // 'customer' => $useCus,
                    // 'zone'     => $useZone,
                    // 'spcZone'  => $specialZone,
                    'meta'     => $meta
                ]
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            var_dump($e->getMessage().'|'.$e->getTraceAsString());
            die();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function listCustomer(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $customers = $this->customerModel->loadCustomerForWarehouseLayout($this->whs_id, $this->user_id);
            $customerColor = (new CustomerColorModel())->getAllCustomer()->toArray();
            $colors = array_pluck($customerColor, null, 'cus_id');

            $useCus = [];
            foreach($customers as $cus){
                $cusId = $cus['cus_id'];
                $customer = [
                    'id'   => array_get($cus, "cus_id", null),
                    'code' => array_get($cus, "cus_code", null),
                    'name' => array_get($cus, "cus_name", null),
                    'color'    => array_get($colors, "$cusId.cl_code", null),
                    'cl_name'    => array_get($colors, "$cusId.cl_name", null),
                    'cl_code'    => array_get($colors, "$cusId.cl_code", null)
                ];
                if (isset($input['id']) && $input['id'] != $cusId) {
                    $customer['color'] = '#FFFFFF';
                }
                $useCus[] = $customer;
            }

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'customer' => $useCus
                ]
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function listZone(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $customers = $this->customerModel->loadCustomerForWarehouseLayout($this->whs_id, $this->user_id);
            $useZone = [];
            $specialZone = [];
//            foreach($customers as $cus){
//                $cusId = $cus['cus_id'];
//                // Get zone by customer
//                $arrayZone = [];
//                $arraySpecialZone = [];
//                $zones = $this->zoneModel->loadZoneByCustomerForWarehouseLayout($cusId);
//                foreach($zones as $zone){
//                    $temp = null;
//                    $temp = [
//                        'zone_id'    => array_get($zone, "zone_id", null),
//                        'zone_code'  => array_get($zone, "zone_code", null),
//                        'zone_name'  => array_get($zone, "zone_name", null),
//                        'zone_color' => array_get($zone, "zone_color", null)
//                    ];
//                    $arrayZone[] = $temp;
//                    if (!empty(self::SPECIAL_ZONE[array_get($zone, "zone_code", null)])) {
//                        $arraySpecialZone[] = $temp;
//                    }
//                }
//                // presentation data
//                $zoneByCustomer = [
//                    'id'   => array_get($cus, "cus_id", null),
//                    'code' => array_get($cus, "cus_code", null),
//                    'name' => array_get($cus, "cus_name", null),
//                    'zones'=> $arrayZone
//                ];
//                if (isset($input['id']) && $input['id'] != $cusId) {
//                    $customer['color'] = '#FFFFFF';
//                }
//                $useZone[] = $zoneByCustomer;
//                if(!empty($arraySpecialZone)){
//                    $specialZone[] = [
//                        'id'   => array_get($cus, "cus_id", null),
//                        'code' => array_get($cus, "cus_code", null),
//                        'name' => array_get($cus, "cus_name", null),
//                        'zones'=> $arraySpecialZone
//                    ];
//                }
//            }

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'zone'     => $useZone,
                    'spcZone'  => $specialZone
                ]
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $zone
     *
     * @return mixed
     */
    public function showLayout($zone)
    {
        try {
            $carton = (new CartonModel())->loadViewLayout($this->whs_id)->toArray();
            $carton = array_pluck($carton, null, 'loc_id');

            $locations = $this->locationModel->readWhsLayout($this->whs_id, $zone,
                ['pallet', 'zone.customerZone.customer', 'locationType'])
                ->toArray();

            $layouts = null;

            $cusLocs = array_pluck($locations, 'zone.customer_zone.customer', 'loc_id');

            $customerColor = (new CustomerColorModel())->getAllCustomer()->toArray();

            $colors = array_pluck($customerColor, null, 'cus_id');

            $useCus = [];
            $ec = false;
            foreach ($locations as $location) {
                $loc = explode("-", $location['loc_alternative_name']);
                $count = count($loc);
                if ($count < 4 || $count > 5) {
                    continue;
                }

                $keyRack = $loc[0];
                if ($count == 5) {
                    $keyRack = $loc[0] . "-" . $loc[1];
                }

                end($loc);
                $endKey = key($loc);

                if (empty($layouts[$keyRack][$loc[$endKey - 1]])) {
                    $layouts[$keyRack][$loc[$endKey - 1]] = [];
                }

                $customer = null;
                $locId = $location['loc_id'];

                if (!empty($cusLocs[$locId])) {
                    $cusId = $cusLocs[$locId]['cus_id'];
                    $customer = [
                        'cus_id'   => array_get($cusLocs, "$locId.cus_id", null),
                        'cus_name' => array_get($cusLocs, "$locId.cus_name", null),
                        'color'    => array_get($colors, "$cusId.cl_code", null)
                    ];
                    $useCus[$cusId] = $cusId;
                }

                $locType = array_get($location, 'location_type.loc_type_code', null);
                if ($locType == "ECO") {
                    $ec = true;
                    $customer = [
                        'cus_id'   => null,
                        'cus_name' => null,
                        'color'    => "#000000"
                    ];
                }

                $layouts[$keyRack][$loc[$endKey - 1]][] = [
                    'customer'             => $customer,
                    'pallet'               => ($location['pallet'] ? 1 : 0),
                    'loc_sts_code'         => $location['loc_sts_code'],
                    'ctn_ttl'              => (int)array_get($carton, $locId . '.ctn_ttl', 0),
                    'piece_ttl'            => (int)array_get($carton, $locId . '.piece_remain', 0),
                    'sku_ttl'              => (int)array_get($carton, $locId . '.sku_ttl', 0),
                    'loc_alternative_name' => $location['loc_alternative_name'],
                    'loc_id'               => $locId
                ];

                krsort($layouts[$keyRack]);
            }

            $cl = null;
            if (!empty($useCus)) {
                $cl = array_map(function ($cl) use ($useCus) {
                    $cusId = array_get($cl, 'customer.cus_id', null);
                    if (empty($useCus[$cusId]) || empty($cusId)) {
                        return null;
                    } else {
                        return [
                            'cus_id'   => $cusId,
                            'cus_code' => array_get($cl, 'customer.cus_code', null),
                            'cus_name' => array_get($cl, 'customer.cus_name', null),
                            'cl_name'  => $cl['cl_name'],
                            'cl_code'  => $cl['cl_code'],
                        ];
                    }
                }, $colors);
                $cl = array_filter($cl);
            }

            if ($ec) {
                $cl["EC"] = [
                    'cus_id'   => null,
                    'cus_code' => "ECO",
                    'cus_name' => "Ecommerce",
                    'cl_name'  => "Black",
                    'cl_code'  => "#000000",
                ];
            }

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'layout'   => $layouts,
                    'customer' => $cl
                ]
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $locId
     *
     * @return mixed
     */
    public function showPopup($locId)
    {
        try {
            $cartons = (new CartonModel())->getLocationItems($this->whs_id, $locId, [ 'item'])->toArray();
            $locationModel = new LocationModel();
            $location = $locationModel->getModel()->where("loc_id",$locId)->select("loc_code", "loc_alternative_name","max_lpn")->first()->toArray();
            $data = [
                'loc_alternative_name' => array_get($location, 'loc_alternative_name', null),
                'max_lpn'       => (!empty($location['max_lpn']))?$location['max_lpn']:"",
            ];
            if (!empty($cartons)) {
                $customer_ids=[];
                $warehouse_ids=[];
                foreach ($cartons as $carton){
                    $customer_ids[$carton['cus_id']]=$carton['cus_id'];
                    $warehouse_ids[$carton['whs_id']]=$carton['whs_id'];
                }
                $statusData=(new InventoryModel())->getItemInProcess([
                    'whs_id'=>$warehouse_ids,
                    'customer_ids'=>$customer_ids,
                ]);
                $data['items'] = array_map(function ($e) use($statusData) {
                    $key=$e['item_id'].'.'.$e['cus_id'].'.'.$e['whs_id'];
                    return [
                        'item_id'       => $e['item_id'],
                        'sku'           => array_get($e, 'sku', null),
                        'size'          => array_get($e, 'size', null),
                        'color'         => array_get($e, 'color', null),
                        'lot'           => array_get($e, 'lot', 'NA'),
                        'upc'           => array_get($e, 'upc', null),
                        'po'            => array_get($e, 'po', null),
                        'uom_id'        => array_get($e, 'ctn_uom_id', null),
                        'uom_code'      => array_get($e, 'uom_code', null),
                        'uom_name'      => array_get($e, 'uom_name', null),
                        'length'        => array_get($e, 'length', null),
                        'width'         => array_get($e, 'width', null),
                        'height'        => array_get($e, 'height', null),
                        'weight'        => array_get($e, 'weight', null),
                        'cube'          => array_get($e, 'cube', null),
                        'volume'        => array_get($e, 'volume', null),
                        'ctn_pack_size' => array_get($e, 'ctn_pack_size', null),
                        'ctn_ttl'       => $e['ctn_ttl'],
//                            'piece_ttl'     => $e['piece_remain'],
                        'piece_ttl'     => $e['ctn_ttl'],
                        "lpn_carton"    => $e['lpn_carton'],
                        'plt_num'       => array_get($e, 'plt_num', null),
                        'gr_sts'=>$statusData->contains($key)?1:0,
                    ];
                }, $cartons);
            }

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => $data
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}