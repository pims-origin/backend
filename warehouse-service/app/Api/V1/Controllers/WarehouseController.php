<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\BaseService;
use App\Api\V1\Models\WarehouseService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;


class WarehouseController extends AbstractController
{
    /**
     * @var MenuService
     */
    protected $service;

    /**
     * MenuController constructor.
     *
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new WarehouseService($request);
    }

    /**
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getList($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->service->getDetail($id);

        if (!$result) {
            return ['errors' => ['message' => 'can not find warehouse']];
        }

        return $result;
    }

    /**
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($warehouseId, IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $result = $this->service->update($warehouseId, $input);

        if ($result instanceof \GuzzleHttp\Psr7\Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(IRequest $request)
    {
        $input = $request->getParsedBody();
        $result = $this->service->create($input);
        if ($result instanceof \GuzzleHttp\Psr7\Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     *
     * @param integer $warehouseId
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($warehouseId)
    {
        $result = $this->service->destroy($warehouseId);
        if ($result instanceof \GuzzleHttp\Psr7\Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param IRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->search($queryStr);

        $response = $this->convertResponse($result);

        return $response;
    }

    public function getLocations(IRequest $request, $warehouseId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getLocations($warehouseId, $queryStr);

        $response = $this->convertResponse($result);

        return $response;
    }

    public function getLocationsByCustomers(IRequest $request, $whsId, $cusId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getLocationsByCustomers($whsId, $cusId, $queryStr);

        $response = $this->convertResponse($result);

        return $response;
    }

    public function getFreeLocations(IRequest $request, $warehouseId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getFreeLocations($warehouseId, $queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function getZone(IRequest $request, $warehouseId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getZone($warehouseId, $queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function getStatuses()
    {
        $result = $this->service->getStatuses();
        $response = $this->convertResponse($result);

        return $response;
    }

    public function listLayout(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getLayout($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function showLayout($zone, IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->showLayout($zone, $queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function showPopup($locId)
    {
        $result = $this->service->showPopup($locId);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function invReport(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getInvReport($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function shippingReport(IRequest $request, $whsId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->shippingReport($whsId, $queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function orderReport(IRequest $request, $whsId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->orderReport($whsId, $queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function orderStatusNotShipping(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->orderStatusNotShipping($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }
}
