<?php

namespace App\Api\V1\Traits;

use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;

trait WavePickControllerTrait
{
    /**
     * @param $wvDtlId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function suggestLocation($wvDtlId)
    {
        try {
            $wv_dtl = $this->wv_dtl->getFirstWhere(['wv_dtl_id' => $wvDtlId]);

            $item_id = object_get($wv_dtl, 'item_id', 0);


            $wvDtlLoc = $this->wv_dtl_loc->getSugLocIdByWvDtlId($wvDtlId);
            if (empty($wvDtlLoc)) {
                return $this->response->errorBadRequest(Message::get("BM017", "Wave Detail Location"));
            }
            if (empty($wvDtlLoc->sug_loc_ids)) {
                return $this->response->errorBadRequest(Message::get("BM131", "Wave Detail Location", "Suggest 
                Location Id"));
            }

            //For view wv_dtl when complete wv
            $wvDtlActLocs = $wvDtlLoc->act_loc_ids;
            if (!empty($wvDtlActLocs)) {
                $wvDtlActLocs = \GuzzleHttp\json_decode($wvDtlActLocs, true);

                array_walk($wvDtlActLocs, function (&$v) {
                    if (isset($v['cartons'])) {
                        unset($v['cartons']);
                    }
                });

                return ['data' => $wvDtlActLocs];
            }


            $locIds = explode(',', $wvDtlLoc->sug_loc_ids);

            // check wavepick from wap
            $checkFromWap = DB::table('odr_cartons')
                ->select([
                    'loc_id',
                    'loc_code',
                    DB::raw('SUM(piece_qty) as picked_qty')
                ])
                ->where('wv_dtl_id', $wvDtlId)
                // ->whereNotNull('ctn_rfid')
                ->where('deleted', 0)
                ->groupBy('loc_id')
                ->get();
            
            $byWap = false;
            if (!empty($checkFromWap)) {
                $byWap = true;
                $locIds = array_pluck($checkFromWap, 'loc_id');

                $suggestLocation = $this->carton->getSugLocByLocIds($locIds, $item_id, $wv_dtl->lot, $byWap);
                $data = [];
                foreach ($checkFromWap as $locWap) {
                    $avail_qty = 0;
                    if (!empty($suggestLocation)) {
                        foreach ($suggestLocation as $loc) {
                            if ($loc['loc_id'] == $locWap['loc_id']) {
                                $avail_qty = $loc['avail_qty'];
                                break;
                            }
                        }
                    }
                    $data[] = [
                        'act_loc'    => $locWap['loc_code'],
                        'act_loc_id' => $locWap['loc_id'],
                        'picked_qty' => $locWap['picked_qty'],
                        'avail_qty'  => $avail_qty,
                        'loc_id'     => $locWap['loc_id'],
                        'loc_code'   => $locWap['loc_code']
                    ];
                }

                return ['data' => $data];
            }

            $suggestLocation = $this->carton->getSugLocByLocIds($locIds, $item_id, $wv_dtl->lot, $byWap);

            if (!empty($suggestLocation)) {
                $temp = 0;
                $data = [];
                foreach ($suggestLocation as $loc) {
                    $data[] = $loc;
                    $temp += $loc['avail_qty'];
                    if ($temp >= $wv_dtl->piece_qty) {
                        return ['data' => $data];
                    }
                }
            }

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function moreSuggestLocation(Request $request)
    {
        $input = $request->getQueryParams();

        try {

            if (!isset($input['loc_ids']) || empty($input['loc_ids'])) {
                $input['loc_ids'] = [];
            }
            if (!isset($input['item_id']) || empty($input['item_id'])) {
                throw new \Exception("The loc_ids or item_id input is required!");
            }
            if (!isset($input['lot']) || empty($input['lot'])) {
                throw new \Exception("The lot input is required!");
            }
            if (!isset($input['is_ecom'])) {
                throw new \Exception("The is_ecom input is required!");
            }

            if (empty($input['loc_ids'])) {
                $input['loc_ids'] = 0;
            }

            $locIds = explode(',', $input['loc_ids']);
            $locIds = array_filter($locIds, 'is_numeric');

            $suggestLocation = $this->carton->getMoreSugLocByWvDtl($locIds, $input);

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function suggestActutalLocations(Request $request)
    {
        $input = $request->getQueryParams();

        try {

            if (!isset($input['loc_ids']) || empty($input['loc_ids'])) {
                $input['loc_ids'] = [];
            }
            if (!isset($input['item_id']) || empty($input['item_id'])) {
                throw new \Exception("The loc_ids or item_id input is required!");
            }
            if (!isset($input['lot']) || empty($input['lot'])) {
                throw new \Exception("The lot input is required!");
            }
            if (!isset($input['is_ecom'])) {
                throw new \Exception("The is_ecom input is required!");
            }

            if (empty($input['loc_ids'])) {
                $input['loc_ids'] = 0;
            }

            if (!isset($input['loc_code']) || empty($input['loc_code'])) {
                throw new \Exception("The location code is required!");
            }

            $locIds = explode(',', $input['loc_ids']);
            $locIds = array_filter($locIds, 'is_numeric');

            $suggestLocation = $this->carton->suggestActutalLocations($locIds, $input);

            return ['data' => $suggestLocation];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function checkActutalLocations(Request $request)
    {
        $input = $request->getParsedBody();

        if (!isset($input['lists']) || empty($input['lists'])) {
            throw new \Exception("Lists loc_id is required!");
        }
        if (!isset($input['item_id']) || empty($input['item_id'])) {
            throw new \Exception("The loc_ids or item_id input is required!");
        }
        if (!isset($input['lot']) || empty($input['lot'])) {
            throw new \Exception("The lot input is required!");
        }
        if (!isset($input['is_ecom'])) {
            throw new \Exception("The is_ecom input is required!");
        }

        $lists = array_pluck($input['lists'], 'picked_qty', 'loc_id');

        $data = $this->loc->checkActualLocation($input['item_id'], $input['lot'], $input['is_ecom'], $lists);

        foreach ($data as $key => $rw) {
            $data[$key]['error'] = '';
            if ($rw['avail_qty'] === null) {
                $data[$key]['error'] = 'Location is unavailable';
                continue;
            }

            if ($rw['avail_qty'] < $lists[$rw['loc_id']]) {
                $data[$key]['error'] = 'picked qty > available qty';
                continue;
            }

            if ($rw['loc_sts_code'] != 'AC') {
                $data[$key]['error'] = 'Status of location not active';
                continue;
            }
        }

        return $data;
    }

}