<?php
namespace Seldat\Wms2\Models;

/**
 * Class SourceMessage
 *
 * @package Seldat\Wms2\Models
 */
class Message extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'message';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'language',
        'translation'
    ];

    public function SourceMessage()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SourceMessage', 'id', 'id');
    }

}
