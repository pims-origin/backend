<?php

return [
    'ctn_status' => [
        'ACTIVE'   => 'AC',
        'INACTIVE' => 'IA',
        'RACKED'   => 'RK',
        'RECEIVED' => 'RC',
        'SHIPPED'   => 'SH',
        'ADJUSTED' => 'AJ',
        'LOCKED' => 'LK',
        'PUTBACK' => 'PB',
        'PICKED' => 'PD',
        'TRANSFERRED' => 'TF'
    ],
    'odr_status' => [
        'NEW'        => 'NW',
        'ALLOCATED'  => 'AL',
        'STAGING' =>    'ST',
        'PICKING'    => 'PK',
        'PICKED'    => 'PD',
        'PACKED'    => 'PA',
        'SHIPPED'   => 'SH',
        'HOLD'      => 'HO',
        'CANCELLED' => 'CC'
    ],
    'charge_codes_measures' => [],
    'gr_status'           => [
        'RECEIVING'       => 'RG',
        'RECEIVED'        => 'RE',
        'RCVD-DISCREPANT' => 'RD',
    ],
    'event'  => [
        'GR-COMPLETE'  => 'GRC',
        'ORD-SHIPPED' => 'OSH',
        'ORD-CANCELLED' => 'OCN',
        'PR-SHIPPED' => 'PRC'
    ],
    'odr_type' => [
        'ECOM' => 'EC',
    ],
    'labor_type' => [
        'EXPECTED' => 'expected'
    ],
    'labor_cat' => [
        'OP' => 'op',
        'RCV' => 'rcv',
        'WO'  => 'wo'
    ],
    'temperature' => [
        'FROZEN'    => 'CS',
        'CHILLER'  => 'CH'
    ],
    'labor_urgency' => [
        'WD_LABOR'  => 'ot-wd',
        'WE_LABOR'  => 'ot-we',
        'HD5_LABOR'  => 'ot-ho',
        'HDN_LABOR'  => 'ot-ho',
    ],
    'timezone_utc_zero' => "Etc/UTC",
    // convert from inches to feet in cube
    'CUBE_CONVERT_2_FEET' => 1728,
];
