<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Item;


class ItemSummaryTransformer extends TransformerAbstract
{
    //return result
    public function transform(Item $itemSummary)
    {

        return [
            'item_id'       => object_get($itemSummary, 'item_id', null),
            'item_code'     => object_get($itemSummary, 'item_code', null),
            'color'         => object_get($itemSummary, 'color', null),
            'size'          => object_get($itemSummary, 'size', null),
            'pack'          => object_get($itemSummary, 'pack', null),
            'des'           => object_get($itemSummary, 'description', null),
            'lot'           => object_get($itemSummary, 'lot', null),
            'itm_sts'       => object_get($itemSummary, 'status', null),
            'sts'           => object_get($itemSummary, 'sts', null),
            'ttl'           => object_get($itemSummary, 'ttl', 0),
            'cus_upc'       => object_get($itemSummary, 'cus_upc', null),
            'width'         => object_get($itemSummary, 'width', null),
            'weight'        => object_get($itemSummary, 'weight', null),
            'height'        => object_get($itemSummary, 'height', null),
            'length'        => object_get($itemSummary, 'length', null),
            'uom_id'        => !empty($itemSummary->uom_id) ? $itemSummary->uom_id : null,
            'uom_name'      => object_get($itemSummary, 'systemUom.sys_uom_name', null),
            'allocated_qty' => object_get($itemSummary, 'allocated_qty', null),
            'dmg_qty'       => object_get($itemSummary, 'dmg_qty', null),
            'sku'           => object_get($itemSummary, 'sku', null),
            'avail'         => (int)object_get($itemSummary, 'avail', null),
            'level_id'      => object_get($itemSummary, 'level_id', null),
            'spc_hdl_code'  => object_get($itemSummary, 'spc_hdl_code', null)
        ];
    }


}
