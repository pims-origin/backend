<?php

namespace App\Api\Ecom\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\XferHdr;

class XferHdrTransformer extends TransformerAbstract
{
    public function transform(XferHdr $xferHdr)
    {
        $sku_size_color = object_get($xferHdr, 'sku');

        if (!empty(object_get($xferHdr, 'size')) && object_get($xferHdr, 'size') != 'NA') {
            $sku_size_color .= '-'.object_get($xferHdr, 'size');
        }

        if (!empty(object_get($xferHdr, 'color')) && object_get($xferHdr, 'color') != 'NA') {
            $sku_size_color .= '-'.object_get($xferHdr, 'color');
        }

        if (!empty(object_get($xferHdr, 'pack'))) {
            $sku_size_color .= '-'.object_get($xferHdr, 'pack');
        }

        return [
            'xfer_ticket_num' => $xferHdr['xfer_ticket_num'],
            'xfer_hdr_id'     => $xferHdr['xfer_hdr_id'],
            'xfer_hdr_num'    => $xferHdr['xfer_hdr_num'],
            'sku'             => $sku_size_color,
            'cus_upc'         => object_get($xferHdr, 'cus_upc'),
            'ucc128'          => Item::generateUpc128(object_get($xferHdr, 'cus_id'), object_get($xferHdr, 'item_id')),
            'qty'             => array_get($xferHdr, 'act_qty', 0). '/' .array_get($xferHdr, 'remain_qty', 0)
        ];
    }
}
