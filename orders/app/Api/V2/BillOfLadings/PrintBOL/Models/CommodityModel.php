<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\BillOfLadings\PrintBOL\Models;

use Seldat\Wms2\Models\Commodity;

/**
 * Class ShipmentModel
 *
 * @package App\Api\V1\Models
 */
class CommodityModel extends AbstractModel
{
    /**
     * @var Commodity
     */
    protected $model;

    /**
     * CommodityModel constructor.
     *
     * @param Shipment|null $model
     */
    public function __construct(Commodity $model = null)
    {
        $this->model = ($model) ?: new Commodity();
    }


}
