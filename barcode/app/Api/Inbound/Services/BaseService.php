<?php
namespace App\Api\Inbound\Services;

use GuzzleHttp\Client;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class BaseService
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * BaseService constructor.
     * @param $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $this->client = new Client([
            'base_uri' => $baseUrl,
            'headers' => [
                'Authorization' => $request->getHeader('Authorization')
            ],
        ]);
    }

    /**
     * @param $json
     * @param string $type
     * @return array
     */
    public function responseToArray($json, $type = 'data')
    {
        return \json_decode($json, true)[$type];
    }




}