<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\CommodityModel;
use App\Api\V1\Models\PackRefModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Transformers\SystemCountryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use App\Api\V1\Transformers\CommodityTransformer;
use App\Api\V1\Transformers\PackRefTransformer;
use App\Api\V1\Validators\CommodityValidator;
use App\Api\V1\Validators\SystemCountryValidator;
use App\Api\V1\Validators\PackRefValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class CountryController
 *
 * @package App\Api\V1\Controllers
 */
class PackRefController extends AbstractController
{

    /**
     * @var SystemStateModel
     */
    protected $systemStateModel;

    /**
     * @var SystemCountryModel
     */
    protected $systemCountryModel;

    /**
     * @var SystemCountryValidator
     */
    protected $systemCountryValidator;

    /**
     * @var SystemCountryTransformer
     */
    protected $systemCountryTransformer;

    /**
     * @var SystemStateTransformer
     */
    protected $systemStateTransformer;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var CommodityModel
     */
    protected $commodityModel;

    /**
     * @var PackRefModel
     */
    protected $packRefModel;

    /**
     * @var CommodityTransformer
     */
    protected $commodityTransformer;

    /**
     * @var PackRefTransformer
     */
    protected $packRefTransformer;

    /**
     * CountryController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->model = new SystemCountryModel();
        $this->systemCountryModel = new SystemCountryModel();
        $this->systemStateModel = new SystemStateModel();
        $this->systemCountryValidator = new SystemCountryValidator();
        $this->systemCountryTransformer = new SystemCountryTransformer();
        $this->systemStateTransformer = new SystemStateTransformer();
        $this->commodityModel = new CommodityModel();
        $this->commodityValidator = new CommodityValidator();
        $this->commodityTransformer = new CommodityTransformer();
        $this->packRefModel = new PackRefModel();
        $this->packRefValidator = new PackRefValidator();
        $this->packRefTransformer = new PackRefTransformer();
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    /**
     * @param Request $request
     * @param $packRefId
     *
     * @return array|void
     */
    public function update(Request $request, $packRefId)
    {
        return $this->upsert($request, $packRefId);
    }

    /**
     * @param $request
     * @param bool $packRefId
     *
     * @return array|void
     * @throws \Exception
     */
    protected function upsert($request, $packRefId = false)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->packRefValidator->validate($input);

        //$valueArrJson = [];
        $length = (float)array_get($input, 'length', null);
        $width = (float)array_get($input, 'width', null);
        $height = (float)array_get($input, 'height', null);

        $inputParam = [
            'length'    => $length,
            'width'     => $width,
            'height'    => $height,
            'dimension' => "{$length}x{$width}x{$height}",
            'pack_type' => array_get($input, 'pack_type', ''),
        ];

        try {
            if ($packRefId) {
                //check duplicated
                $checkCartonDimension = $this->packRefModel->getFirstWhere(
                    [
                        'length'    => $length,
                        'width'     => $width,
                        'height'    => $height,
                        'pack_type' => array_get($input, 'pack_type', ''),
                    ]
                );
                if (!empty($checkCartonDimension) && $checkCartonDimension->pack_ref_id != $packRefId) {
                    throw new \Exception(Message::get("BM006", "carton dimension"));
                }

                //update
                $packRefInfo = $this->packRefModel->getFirstWhere(['pack_ref_id' => $packRefId]);
                if (empty($packRefInfo)) {
                    throw new \Exception(Message::get("BM017", "carton dimension"));
                }

                $inputParam['pack_ref_id'] = $packRefId;
                $this->packRefModel->update($inputParam);
            } else {
                //check duplicated
                $checkCartonDimension = $this->packRefModel->getFirstWhere(
                    [
                        'length'    => $length,
                        'width'     => $width,
                        'height'    => $height,
                        'pack_type' => array_get($input, 'pack_type', ''),
                    ]
                );
                if (!empty($checkCartonDimension)) {
                    throw new \Exception(Message::get("BM006", "carton dimension"));
                }

                //create
                $this->packRefModel->create($inputParam);
            }

            return ["data" => "successful"];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param PackRefTransformer $packRefTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function packedCartonDimensionList(
        Request $request,
        PackRefTransformer $packRefTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            // get list
            $packRefInfos = $this->packRefModel->search($input,
                ['packType'],
                array_get($input, 'limit'));

            return $this->response->paginator($packRefInfos, $packRefTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $packRefId
     * @param PackRefTransformer $packRefTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(
        $packRefId,
        PackRefTransformer $packRefTransformer
    ) {

        try {
            $packRefInfo = $this->packRefModel->getFirstBy('pack_ref_id', $packRefId, ['packType']);
            if (empty($packRefInfo)) {
                throw new \Exception(Message::get("BM017", "PackRef"));
            }

            return $this->response->item($packRefInfo, $packRefTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                $e->getMessage() . " " . $e->getLine()
            //SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
