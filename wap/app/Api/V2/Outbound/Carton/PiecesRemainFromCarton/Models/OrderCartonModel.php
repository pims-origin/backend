<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09/10/2018
 * Time: 2:19 PM
 */

namespace App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Models;

use App\MessageCode;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class OrderCartonModel extends AbstractModel
{
    /**
     * WavepickDtlModel constructor.
     * @param OrderCarton |null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $this->model = ($model) ?: new OrderCarton();
    }
}