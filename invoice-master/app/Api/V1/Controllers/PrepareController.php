<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\ClientMasterTransformer;
use App\Api\V1\Transformers\DisplayErrorsTransformer;
use App\Mail\Invoice;
use Illuminate\Http\Request;
// use Illuminate\Http\Response as IlluminateResponse;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class PrepareController extends AbstractController
{
    protected $actions = ['billable', 'invoice'];
    protected $model;
    protected $transformer;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->clientTransformer = new ClientMasterTransformer();
        $this->errorTransformer = new DisplayErrorsTransformer();
    }

    public function getList($type = NULL)
    {
        # return depend on request type
        $type = in_array($type, $this->actions) ? $type : 'billable';
        $result = $this->{$type}();

        try {
            $result = $this->{$type}();

            return $this->response->item($result, $this->clientTransformer)
                    ->setStatusCode(app()->make('statusCode')::HTTP_OK);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function billable(Type $var = null)
    {
        $this->model = 'billable';
        # get billable list
        $bill = new \stdClass;
        $bill->data = 'bill';
        return $bill;
    }

    public function invoice(Type $var = null)
    {
        $this->model = 'invoice';
        # get invoice list
        $invoice = new \stdClass;
        $invoice->data = 'invoice';
        return $invoice;
    }
}
