<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:32
 */

namespace App\Api\V1\Models;


use Illuminate\Database\Query\Builder;
use Seldat\Wms2\Models\WarehouseAddress;

class WarehouseAddressModel extends AbstractModel
{
    /**
     * @var Builder
     */
    protected $model;

    /**
     * EloquentAddress constructor.
     *
     * @param WarehouseAddress $model
     */
    public function __construct(WarehouseAddress $model = null)
    {
        $this->model = ($model) ?: new WarehouseAddress();
    }

    /**
     * @param int $warehouseId
     * @param int $addressId
     *
     * @return int
     */
    public function deleteWarehouseAddress($warehouseId, $addressId)
    {
        return $this->model
            ->where('whs_add_id', $addressId)
            ->where('whs_add_whs_id', $warehouseId)
            ->delete();
    }
}
