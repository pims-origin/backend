<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12/27/2018
 * Time: 2:35 PM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\PalletModel;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Profiler;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;


class PutawayController extends AbstractController
{
    protected $palletModel;
    protected $cartonModel;
    protected $locationModel;
    protected $itemModel;

    public function __construct(
        LocationModel $locationModel,
        CartonModel $cartonModel,
        ItemModel $itemModel,
        PalletModel $palletModel
    )
    {
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->palletModel = $palletModel;
        $this->itemModel = $itemModel;
    }

    public function submitPartialPutaway(Request $request, $whsId) {
        try{
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $input = $request->getParsedBody();
            $input['whsId'] = $whsId;


            $this->validatePartial($input);
            $result = $this->locationModel->submitPartialPutaway($input);

            if($result['is_error'] == 1) {
                return $this->response->errorBadRequest($result['msg']);
            }
            Profiler::end();
            return [
                "data" => [
                    "message" => "Submit Successfully"
                ]
            ];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function validatePartial(&$attribute) {
        if(empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest("Field cus_id is required");
        }

        if(empty($attribute['plt_num'])) {
            return $this->response->errorBadRequest("Field plt_num is required");
        }

        if(!empty($attribute['plt_num'])) {
            if(!$this->palletModel->validateLPNFormat($attribute['plt_num'])) {
                $msg = sprintf("LPN number %s is invalid",$attribute['plt_num']);
                return $this->response->errorBadRequest($msg);
            }
        }

        $pallet = $this->palletModel->getFirstWhere(['plt_num' => $attribute['plt_num'],'whs_id' =>$attribute['whsId']]);

        if(empty($pallet)) {
            $msg = sprintf("Pallet %s is invalid",$attribute['plt_num']);
            return $this->response->errorBadRequest($msg);
        }

        if(empty($attribute['loc_code'])) {
            return $this->response->errorBadRequest("Field loc_code is required");
        }

        $location = $this->locationModel->getFirstWhere(["loc_code" => $attribute['loc_code'], 'loc_whs_id' =>$attribute['whsId']]);

        if(empty($location)){
            $msg = sprintf("Location %s is invalid",$attribute['loc_code']);
            return $this->response->errorBadRequest($msg);
        }

        if ($location->loc_sts_code == 'LK') {
            $msg = sprintf("Location %s is locked.", $attribute['loc_code']);

            return $this->response->errorBadRequest($msg);
        }

        if(!empty($location->is_block_stack) && !empty($location->parent_id)){
            $msg = sprintf("Location %s is child location",$attribute['loc_code']);
            return $this->response->errorBadRequest($msg);
        }

        $carton = $this->cartonModel->getModel()->where("plt_id", $pallet->plt_id)
            ->whereIn("ctn_sts",["AC",'RG'])->select("item_id")->first();
        if(!empty($carton)) {
            $item = $this->itemModel->getFirstWhere(['item_id' => $carton->item_id]);
            $item_meta = DB::table('item_meta')->where('itm_id', $item->item_id)->first();
            $item_meta_value = json_decode($item_meta['value']);
            $spc_hdl_code = [];
            if($item_meta_value) {
                foreach ($item_meta_value as $key => $value) {
                    if ($value == 1) {
                        $spc_hdl_code[] = $key;
                    }
                }

                if (!in_array($location->spc_hdl_code, $spc_hdl_code)) {
                    return $this->response->errorBadRequest("The Item in this Pallet has special handle code different with the item in this Location.");
                }
            }
        }

//        if(in_array($location->spc_hdl_code,['SHE','BIN', 'RAC'])) {
//            $count_car_loc = $this->cartonModel->getModel()->where("loc_id", $location->loc_id)
//                ->whereIn("ctn_sts",["AC",'RG'])->groupBy('lpn_carton')->get()->count();
//            /*$count_car_plt = $this->cartonModel->getModel()->where("plt_id", $pallet->plt_id)
//                ->whereIn("ctn_sts",["AC",'RG'])->count();
//            $total = $count_car_loc + $count_car_plt;*/
//            $total = $count_car_loc;
//            if($total >= $location->max_lpn && !is_null($location->max_lpn) && $location->max_lpn != 1 ) {
//               return $this->response->errorBadRequest("Location is not enought space for this pallet.");
//            }
//        }

        $count_cus = DB::table('pallet')
                ->where('loc_code', $attribute['loc_code'])
                ->where('cus_id', '<>', $attribute['cus_id'])
                ->groupBy('cus_id', 'loc_code')
                ->get();

        if ($count_cus->count() >= 20) {
            return $this->response->errorBadRequest("Location only contains 20 customers maximum.");
        }

        $carton = DB::table('cartons')
            ->where('whs_id', $attribute['whsId'])
            ->where('lpn_carton', $attribute['plt_num'])
            ->where('deleted', 0)
            ->whereIn('ctn_sts', ['AC', 'RG', 'LK'])
            ->whereNotNull('loc_code')
            ->first();

        if (! empty($carton)) {
            if ($carton['loc_code'] !== $attribute['loc_code']) {
                return $this->response()->errorBadRequest(sprintf('LPN %s has been put away at %s', $carton['lpn_carton'], $carton['loc_code']));
            }
        }

        $attribute['location'] = $location;
        $attribute['pallet'] = $pallet;

    }
}
