<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\SystemState;
use League\Fractal\TransformerAbstract;

class SystemStateTransformer extends TransformerAbstract
{
    public function transform(SystemState $systemState)
    {
        return [
            'sys_state_id'         => $systemState->sys_state_id,
            'sys_state_code'       => $systemState->sys_state_code,
            'sys_state_name'       => $systemState->sys_state_name,
            'sys_state_country_id' => $systemState->sys_state_country_id,
            'sys_state_desc'       => $systemState->sys_state_desc,
            'sys_country_name'     => $systemState->sys_country_name
            //object_get($systemState, "systemCountry.sys_country_name", null)
        ];
    }
}
