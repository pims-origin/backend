<?php

namespace App\Api\V2\Inbound\Carton\ValidateItemTag\Controllers;

use App\Api\V2\Inbound\Carton\ValidateItemTag\Models\Log;
use App\Api\V2\Inbound\Carton\ValidateItemTag\Models\CartonModel;
use App\Api\V2\Inbound\Carton\ValidateItemTag\Models\CustomerModel;
use App\libraries\RFIDValidate;

use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Models\Reason;
use Wms2\UserInfo\Data;


/**
 * Class CartonController
 *
 * @package App\Api\V2\Inbound\Carton\ValidateItemTag\Controllers
 */
class CartonController extends AbstractController
{
    protected $cartonModel;
    protected $customerModel;

    private   $_request;
    private   $_whsId;
    private   $_url;
    private   $_transaction;


    /**
     * CartonController constructor
     *
     * @param CartonModel $cartonModel
     */
    public function __construct(
        CartonModel $cartonModel
    ) {
        $this->cartonModel = $cartonModel;
        $this->customerModel = new CustomerModel();
    }

    /**
     * @param  Integer $whsId
     * @param  Request $request
     * @return mixed
     */
    public function validateItemTag($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        /**
         * Logs
         */
        $this->_transaction = $owner = $transaction = "";
        $url = "/v2/whs/{$whsId}/carton/validate-item-tag";
        Log:: info($request, $whsId, [
            'evt_code'     => 'CGR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Validate carton tag after create Goods Receipt'
        ]);

        $this->_request = $request;
        $this->_url     = $url;
        $this->_whsId   = $whsId;

        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        $cusId    = (int)array_get($input, 'cus_id');
        $ctnRfids = array_get($input, 'ctn_rfids');

        try {

            foreach ($ctnRfids as $ctnRfid) {
                $ctnObj = $this->cartonModel->getModel()
                    ->where('rfid', $ctnRfid)
                    ->whereNotIn('ctn_sts', ['CC', 'AJ'])
                    ->first();
                if (!$ctnObj) {
                    $msg = sprintf("Carton RFID %s doesn't exist!", $ctnRfid);
                    return $this->_responseErrorMessage($msg);
                }

                if ($ctnObj && $ctnObj->loc_id) {
                    $msg = sprintf("Carton RFID %s was on location %s!", $ctnRfid, $ctnObj->loc_code);
                    return $this->_responseErrorMessage($msg);
                }

                if ($ctnObj && !in_array($ctnObj->ctn_sts, ['RG', 'AC'])) {
                    $msg = sprintf("Carton RFID %s was %s!", $ctnRfid, Status::getByValue($ctnObj->ctn_sts, 'CTN_STATUS'));
                    return $this->_responseErrorMessage($msg);
                }

                if ($cusId && $ctnObj && $ctnObj->cus_id != $cusId) {
                    $msg = sprintf("Carton RFID %s do not belong to customer %s!", $ctnRfid, $cusId);
                    return $this->_responseErrorMessage($msg);
                }

                if (!$cusId) {
                    $cusId = object_get($ctnObj, 'cus_id');
                }
            }

            $customerObj = '';
            if ($cusId) {
                $customerObj = $this->customerModel->getFirstWhere(['cus_id' => $cusId]);
                if (!$customerObj) {
                    $msg = "Customer doesn't exist!";
                    return $this->_responseErrorMessage($msg);
                }

                $chkCusWarehouse = DB::table('customer_warehouse')
                    ->where('cus_id', $cusId)
                    ->where('whs_id', $whsId)
                    ->where('deleted', 0)
                    ->first();
                if (!count($chkCusWarehouse)) {
                    $msg = "Customer doesn't belong to current warehouse!";
                    return $this->_responseErrorMessage($msg);
                }
            }
            $msg = sprintf("Successfully!");

            return [
                "iat"     => time(),
                "status"  => true,
                "messages" => [[
                    "msg"         => $msg,
                    "status_code" => 1,
                ]],
                "data"    => [['cus_id' => $cusId, 'cus_name' => object_get($customerObj, 'cus_name')]]
            ];

        } catch (\Exception $e) {
            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    private function eventTracking($params)
    {

        // event tracking asn
        $this->eventTrackingModel->refreshModel();

        return $this->eventTrackingModel->create([
            'whs_id'    => $params['whs_id'],
            'cus_id'    => $params['cus_id'],
            'owner'     => $params['owner'],
            'evt_code'  => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info'      => $params['info'],
        ]);
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }


    private function _validateParams($attributes, $whsId)
    {
        $cusId    = (int)array_get($attributes, 'cus_id');
        $ctnRfids = array_get($attributes, 'ctn_rfids');

        $names = [
            'cus_id'    => 'Customer Id',
            'ctn_rfids' => 'Carton Array',
        ];

        // Check Required
        $requireds = [
            'ctn_rfids'   => $ctnRfids,
        ];

        // Check int type and greater than 0
        $intGreaterThan0 = [
            'cus_id' => $cusId,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet rfid is string
        // if ($pltRfid && !is_string($pltRfid)) {
        //     $msg = $names['pallet_rfid'] . " must be string type.";
        //     return $this->_responseErrorMessage($msg);
        // }

        // cartons is array
        if (!is_array($ctnRfids) || !count($ctnRfids)) {
            $msg = $names['ctn_rfids'] . " must be array type and not be empty.";
            return $this->_responseErrorMessage($msg);
        }

        foreach ($intGreaterThan0 as $key => $field) {
            if ($field) {
                if (!is_int($field) || $field < 1) {
                    $errorDetail = "{$names[$key]} must be integer type and greater than 0.";
                    return $this->_responseErrorMessage($errorDetail);
                }
            }
        }

        //check invalid code carton
        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($ctnRfids as $ctnRfid)
        {
            $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
            if (!$ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $ctnRfid;
                if ('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }

//            if (!is_int($pieceRemain) || $pieceRemain < 1 || $pieceRemain > $packSize) {
//                $errorDetail = sprintf("The piece remain of carton Rfid %s must be integer type,
//                greater than 0 and not greater than pack size %s.", $ctnRfid, $packSize);
//                return $this->_responseErrorMessage($errorDetail);
//            }
        }

        if (count($ctnRfids) != count(array_unique($ctnRfids))) {
            $errorDetail = sprintf("It is duplicated carton.");
            return $this->_responseErrorMessage($errorDetail);
        }

        if (count($ctnRfidInValid)) {
            return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
        }
    }
}
