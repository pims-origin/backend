<?php
/**
 * Created by PhpStorm.
 * User: vinhpham
 * Date: 7/25/18
 * Time: 1:54 PM
 */


namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use App\Wms2\UserInfo\Data;


class SetWarehouseTimezone
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $redis = new Data();

        $whsId = $redis->getCurrentWhs();

        $warehouseTimezone = DB::table('whs_meta')
            ->where('whs_qualifier', 'wtz')
            ->where('whs_id', $whsId)
            ->first();

        if ($warehouseTimezone) {
            $timezone = object_get($warehouseTimezone, 'whs_meta_value') ;
            config(['app.timezone' => $timezone]);
            ini_set("date.timezone", $timezone);
            date_default_timezone_set( $timezone);
        }

        return $next($request);
    }
}
