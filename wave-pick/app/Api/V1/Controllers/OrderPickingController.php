<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OdrHdrMetaModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\UserMetaModel;
use App\Api\V1\Models\WavePickDtlLocModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Traits\OrderFlowControllerTrait;
use App\Api\V1\Transformers\WavePickHdrTransformer;
use App\Api\V1\Transformers\WavePickInfoTransformer;
use App\Api\V1\Validators\WavePickDtlLocValidator;
use App\Api\V1\Validators\WavePickValidator;
use App\Jobs\WavePickJob;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Dingo\Api\Exception\UnknownVersionException;
use Exception;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class OrderPickingController extends AbstractController
{
    use OrderFlowControllerTrait;

    const PREFIX_CARTON_RFID_RELOCATE = 'CCTC';

    /**
     * @var Wave pick header
     */
    protected $wv_hdr;

    /**
     * @var Wave pick detail
     */
    protected $wv_dtl;

    /**
     * @var Order Header
     */
    protected $odrHdr;

    /**
     * @var Order Detail
     */
    protected $odrDtl;

    /**
     * @var Location
     */
    protected $loc;

    /**
     * @var InventorySummary
     */
    protected $intv_smr;

    /**
     * @var CartonModel
     */
    protected $ctn;

    /**
     * @var pallet
     */
    protected $plt;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    protected $shippingOrderModel;

    protected $STR_PAD_LEFT = 0;

    protected $inventorySummaryModel;

    protected $orderCartonModel;

    protected $customerConfigModel;

    protected $orderHdrMetaModel;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    protected $packDtlModel;

    protected $wavePickDtlLocModel;

    protected $wavePickDtlLocValidator;

    protected $userMetaModel;


    /**
     * OrderPickingController constructor.
     */
    public function __construct()
    {
        $this->wv_hdr = new WavePickHdrModel();
        $this->wv_dtl = new WavePickDtlModel();
        $this->odrHdr = new OrderHdrModel();
        $this->odrDtl = new OrderDtlModel();
        $this->loc = new LocationModel();
        $this->intv_smr = new InventorySummaryModel();
        $this->ctn = new CartonModel();
        $this->plt = new PalletModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->shippingOrderModel = new ShippingOrderModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->orderHdrMetaModel = new OdrHdrMetaModel();
        $this->packHdrModel = new PackHdrModel();
        $this->packDtlModel = new PackDtlModel();
        $this->wavePickDtlLocModel = new WavePickDtlLocModel();
        $this->wavePickDtlLocValidator = new WavePickDtlLocValidator();
        $this->userMetaModel = new UserMetaModel();
    }

    /**
     * List order picking
     *
     * @param WavePickHdrTransformer $wvHdrTransformer
     *
     * @return \Dingo\Api\Http\Response
     */
    public function odrPickingList(Request $request, WavePickHdrTransformer $wvHdrTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', 20);
        $wvStatuses = array_get($input, 'wv_sts', []);
        $wvType = array_get($input, 'type', '');
        $userId = Data::getCurrentUserId();
        $export = array_get($input, 'export', null);

        if(!$wvType){
            return $this->response->errorBadRequest('Wavepick type is required');
        }

        if ($export == 1){
            $this->exportCsv($input);
            die;
        }

        try {
            $this->userMetaModel->modifyUserMeta(['user_id' => $userId, 'qualifier' => config('constants.qualifier.WAVEPICK-STATUS')], $wvStatuses, $wvType);

            $wvHdrLs = $this->wv_hdr->search($input, ['details', 'pickerUser', 'orderHdr', 'userCreated'], $limit);

            foreach ($wvHdrLs as $wvHdr) {
                $arrCus = [];
                foreach ($wvHdr->details as $details) {
                    $arrCus[] = $details->cus_id;
                }
                $wvHdr->cus_id = $arrCus;
            }

            return $this->response->paginator($wvHdrLs, $wvHdrTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Detail wave pick
     *
     * @param Request $request
     * @param WavePickInfoTransformer $wvInfoTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function detailWavePick(Request $request, WavePickInfoTransformer $wvInfoTransformer)
    {
        $input = $request->getQueryParams();
        $wv_id = $input['wv_id'];

        try {
            //Load wave pick header

            $dataWvDtl = $this->wv_hdr->getWvHdrInfo($wv_id);

            // Load order_num
            $odrHdrs = $this->odrHdr->getOrderHdrNumsByWvId([$wv_id])->toArray();

            $dataWvDtl->odr_num = implode(", ", array_column($odrHdrs, 'odr_num'));

            $dataWvDtl->cust_name = $dataWvDtl->cus_name;
            $dataWvDtl->wvDetail = $this->wv_dtl->loadWvDetail($wv_id);

            return $this->response->item($dataWvDtl, new $wvInfoTransformer);
        } catch (Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function startInsertOrderCartonQueue($whsID, $wvId, Request $request)
    {
        $job = new WavePickJob($wvId, $request);
        $this->dispatch($job);

        return ['data' => 'Successful'];
    }

    public function updateWavePickNoEcom($wv_id, Request $request)
    {
        // get data from HTTP
        $items = $request->getParsedBody();

        $orders = $this->odrHdr->getOrderHdrByWavePikcIds($wv_id);

        if (!empty($orders)) {
            $cancelOdrSts = Status::getByValue("Canceled", "ORDER-STATUS");
            $count = 0;
            foreach ($orders->toArray() as $order) {
                if ($order['odr_sts'] === $cancelOdrSts) {
                    $count++;
                }
            }
            if (count($orders->toArray()) === $count) {
                throw new Exception(Message::get('BM123', 'Canceled order', 'order picking'));
            }
        }

        // Load wvhdr info
        $wvHdrInfo = $this->wv_hdr->getWvHdrInfo($wv_id);
        // Check complete wv_hdr
        $this->checkCompleteWvHdr($wvHdrInfo);

        try {
            \DB::beginTransaction();

            $ttlActLocIds = [];
            $odrDtlList = [];
            $packs = [];
            $eventDatas = [];
            $algorithms = [];

            // Update wavepick detail, inventory summary, carton & pallet
            foreach ((array)$items as $item) {
                /**
                 * 1. Get Cartons
                 * 1.1 - get by picking algorithm: FIFO , LIFO, FEFO
                 * - FIFO order by created_at ASC,
                 * - LIFO order by created_at DESC,
                 * - FEFO order by expired_dt ASC,
                 * 2. Update Wave dtl by sum up piece_remain in carton
                 * 2.1 update wwdtl loc
                 * 3. Update Inventory - where item_id, lot, cus_id, whs_id
                 * 4. update cartons
                 * 4. Update order dtl sts. all_qty
                 * 4.1 update order hdr
                 * 5. Insert order carton
                 * 6. Update pallet
                 * 7. update wv_hdr
                 * 8. auto pack
                 */

                $item['item_id'] = $item['itm_id'];
                $actLocs = (array)$item['act_loc'];
                $actLocIds = array_column($actLocs, 'act_loc_id');


                $ttlActLocIds = array_merge($ttlActLocIds, $actLocIds);
                $pickedQTYs = array_column($actLocs, 'picked_qty');
                $pickedTtl = array_sum($pickedQTYs);
                if (!isset($algorithms[$item['cus_id']])) {
                    $algorithms[$item['cus_id']] = $this->customerConfigModel->getPickingAlgorithm($wvHdrInfo->whs_id,
                        $item['cus_id']);;
                }

                if ($item['piece_qty'] < $item['act_piece_qty']) {
                    $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s picked Qty QTY %d is greater than Allocated Qty %d',
                        $item['sku'], $item['size'], $item['color'], $item['lot'], $item['piece_qty'],
                        $item['act_piece_qty']);
                    throw new Exception($msg);
                }

                if ($item['piece_qty'] != $pickedTtl) {
                    //WMS2-5393 - Not allow to create backorder
                    $msg = sprintf('Unable to Pick QTY less than Allocated QTY');
                    throw new Exception($msg);
                }

                $algorithm = $algorithms[$item['cus_id']];
                $checkQty = 0;
                foreach ($actLocs as $actLoc) {
                    $actLocId   = $actLoc['act_loc_id'];
                    $actLocCode = $actLoc['act_loc'];
                    $pickedQty  = $actLoc['picked_qty'];

                    $qty = $this->ctn->getAvailableQuantity($item['item_id'], $item['lot'], [$actLocId], $wvHdrInfo->whs_id);

                    // WMS2-5393 - [Outound - Wavepick] Update Wavepick on Web in case cartons are RFID
                    $rfidFlag = false;
                    if ($qty < $pickedQty) {
                        $rfidFlag = true;
                        $qty += $this->ctn->getAvailableQuantityRfid($item['item_id'], $item['lot'], [$actLocId], $wvHdrInfo->whs_id);
                        if ($qty < $pickedQty) {
                            $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s available QTY %d is less than Picked Qty %d on Location %s.',
                                $item['sku'], $item['size'], $item['color'], $item['lot'], $qty, $pickedQty, $actLocCode);
                            throw new Exception($msg);
                        }
                        $this->_createFakeRfidsAndAJCartonsByActLocIds($item['item_id'], $item['lot'], [$actLocId]);
                    }
                    $checkQty += $qty;

                    $item['picked_ttl'] = $pickedTtl;
                    $cartons = $this->ctn->getAllCartonsByWvDtl($item, $algorithm, [$actLocId], [$actLoc], $rfidFlag);

                    $cartons = $this->ctn->sortCartonsByLoc($cartons);

                    $data = $this->groupCartonsByLot($cartons);

                    $odrCartons = $this->ctn->updateCartonByLocs($data['cartons']);
                    $this->updateInventoryByLot($data['lots'], $item);
                    $this->wavePickDtlLocModel->updateWaveDtlLoc([$actLoc], $item['wv_dtl_id']);

                    $data = $this->odrDtl->updateOdrDtlPickedQty($item, $odrCartons);
                    unset($odrCartons); //free memory

                    $odrDtlList[] = $data['odrDtls'];
                    $eventDatas = array_merge($eventDatas, $data['eventData']);
                    $packs[] = $data['packs'];

                    $discrepancyTtl = $item['piece_qty'] - $item['act_piece_qty'];
                    if ($discrepancyTtl > 0) {
                        $this->inventorySummaryModel->updateInventoryByLotBackOrder($item, $discrepancyTtl);
                    }

                    // Update order picked
                    $this->odrHdr->refreshModel();
                    $odrSts = 'PK';
                    foreach (array_chunk($data['odrIds'], 200) as $chunkIds) {
                        $this->odrHdr->updateWhereIn(['odr_sts' => $odrSts, 'sts' => 'u'], $chunkIds, 'odr_id');
                    }

                }
                // update wave pick detail
                $this->wv_dtl->updateWaveDtl($item['wv_dtl_id'], $pickedTtl);
                if ($checkQty < $pickedTtl) {
                    $msg = sprintf('SKU:%s Size:%s, Color:%s, Lot:%s available QTY %d is less than Picked Qty %d on Location %s.',
                        $item['sku'], $item['size'], $item['color'], $item['lot'], $qty, $pickedTtl, $actLocCode);
                    throw new Exception($msg);
                }
            }

            $this->plt->updatePalletCtnTtl($ttlActLocIds);
            $this->plt->updateZeroPallet($ttlActLocIds);
            $orders = $this->odrDtl->sortOrderDtlByOrderID($odrDtlList);

            // ------------- FOR BACK ORDER ---------------------
            $this->odrHdr->createBackOrders($orders);
            $this->wv_hdr->updateWvPicking($wv_id);

            $this->logEventTracking($eventDatas, $wvHdrInfo);

            \DB::commit();

            //add Queue
            $this->dispatch(new WavePickJob($wv_id, $request));

            return $this->response
                ->noContent()
                ->setContent("{}")
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (Exception $e) {
            \DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $wvHdrInfo
     */
    private function checkCompleteWvHdr($wvHdrInfo)
    {
        if ($wvHdrInfo->wv_sts == Status::getByValue("Completed", "WAVEPICK-STATUS")) {
            throw new Exception(Message::get("BM037"));
        }

        if ($wvHdrInfo->wv_sts == Status::getByValue("Canceled", "WAVEPICK-STATUS")) {
            throw new Exception('This wave pick has been cancelled!');
        }

        if ($wvHdrInfo->wv_sts != Status::getByValue("New", "WAVEPICK-STATUS")) {
            throw new Exception('Order Pick can be executed for New Wave Pick only!');
        }
    }

    /**
     * @param $eventDatas
     * @param $wvHdrInfo
     */
    private function logEventTracking($eventDatas, $wvHdrInfo)
    {


        // event tracking - WAVE PICK
        $evt_code = Status::getByKey("EVENT", "WAVE-PICK-COMPLETED");

        $info = sprintf(Status::getByKey("EVENT-INFO", "WAVE-PICK-UPDATE"), $wvHdrInfo->wv_num);

        $eventData = [
            'whs_id'     => $wvHdrInfo->whs_id,
            'cus_id'     => $wvHdrInfo->cus_id,
            'owner'      => $wvHdrInfo->wv_num,
            'evt_code'   => $evt_code,
            'trans_num'  => $wvHdrInfo->wv_num,
            'info'       => $info,
            'created_at' => time(),
            'created_by' => JWTUtil::getPayloadValue('jti') ?: 1

        ];

        array_push($eventDatas, $eventData);
        (new EventTracking())->insert($eventDatas);
    }

    public function checkAvail(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        try {
            $loc_id = array_get($input, 'loc_id', 0);
            $item_id = array_get($input, 'item_id', 0);
            $item_qty = array_get($input, 'qty', 0);
            $totalItem = $this->loc->countItemInLocation($loc_id, $item_id);
            $totalItem = is_null($totalItem) ? 0 : $totalItem;
            $result = [
                'avail' => $totalItem >= $item_qty ? true : false
            ];

            return $this->response->array($result);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function autoConsolidate(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        try {
            $loc_id = array_get($input, 'loc_id', 0);
            $item_id = array_get($input, 'item_id', 0);
            $item_qty = array_get($input, 'qty', 0);
            $totalItem = $this->loc->countItemInLocation($loc_id, $item_id);
            $totalItem = is_null($totalItem) ? 0 : $totalItem;
            $numberConsolidate = $item_qty - $totalItem;

            $firstCarton = $this->ctn->getFirstWhere([
                ['loc_id', "=", $loc_id],
                ['item_id', "=", $item_id]
            ]);
            $packSize = object_get($firstCarton, 'ctn_pack_size', 100);

            $numcarton = ceil($numberConsolidate / $packSize);

            $cartons = $this->ctn->getCartonNearByConsolidate($loc_id, $item_id, $numberConsolidate, $numcarton);

            if (!count($cartons)) {
                throw new \Exception("Items are not enough to consolidate to this location!");
            }


            $plt_id = object_get($firstCarton, 'plt_id', 0);

            DB::beginTransaction();

            // chuyen carton sang pallet
            foreach ($cartons as $carton) {
                $pallet = $this->plt->getFirstWhere([
                    'plt_id' => object_get($carton, 'plt_id', 0),
                ]);

                $ctn_ttl = object_get($pallet, 'ctn_ttl', 0);

                $this->ctn->refreshModel();
                $this->ctn->updateWhere([
                    'plt_id'   => $plt_id,
                    'loc_id'   => $loc_id,
                    'loc_name' => object_get($firstCarton, 'loc_name', ''),
                    'loc_code' => object_get($firstCarton, 'loc_code', '')
                ], [
                    'ctn_id' => $carton->ctn_id
                ]);

                $ctn_ttl_up = $ctn_ttl - 1;
                $this->plt->refreshModel();
                $this->plt->updateWhere([
                    'ctn_ttl'  => $ctn_ttl_up,
                    'loc_id'   => $loc_id,
                    'loc_name' => object_get($firstCarton, 'loc_name', ''),
                    'loc_code' => object_get($firstCarton, 'loc_code', '')
                ], [
                    'plt_id' => $plt_id,
                ]);
                if ($ctn_ttl_up == 0) {
                    $this->plt->updatePalletZeroDateAndDurationDays($plt_id);

                    $this->plt->updatePalletZeroDateAndDurationDaysAuto();
                }
            }
            DB::commit();

            $this->response()->noContent()->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function groupCartonsByLot($locs)
    {
        $lots = [];
        $cartons = [];
        foreach ($locs as $loc) {
            foreach ($loc as $carton) {
                if (!isset($lots[$carton['lot']])) {
                    $lots[$carton['lot']] = 0;
                }
                $lots[$carton['lot']] += $carton['picked_qty'];
                $cartons[] = $carton;
            }
        }

        return [
            'cartons' => $cartons,
            'lots'    => $lots
        ];
    }

    private function updateInventoryByLot($lots, $item)
    {
        foreach ($lots as $lot => $pickedQty) {
            $this->inventorySummaryModel->updateInventoryByLot([
                'whs_id'  => $item['whs_id'],
                'item_id' => $item['itm_id'],
                'lot'     => $lot
            ], $pickedQty);
        }
    }

    private function _createFakeRfidsAndAJCartonsByActLocIds($itemId, $lot, $actLocIds)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $rfidRaw = sprintf("rfid not like '%s%s'", self::PREFIX_CARTON_RFID_RELOCATE, '%');
        foreach ($actLocIds as $actLocId) {
            $cartons = $this->ctn->getModel()
                // ->where('item_id', $itemId)
                // ->where('lot', $lot)
                ->where('loc_id', $actLocId)
                ->where('ctn_sts', 'AC')
                ->whereNotNull('rfid')
                ->whereRaw(DB::raw($rfidRaw))
                ->get()->toArray();

            $dataInsert = [];
            foreach ($cartons as $carton) {
                $pallet_info = DB::table('pallet')->where('plt_id', $carton['plt_id'])->first();
                $carton['ctn_num'] = $carton['ctn_num'] . '-F1';
                $carton['rfid'] = self::PREFIX_CARTON_RFID_RELOCATE. $actLocId . str_pad($carton['ctn_id'], 24 - (strlen($carton['ctn_id']) + strlen($actLocId)), "0", STR_PAD_LEFT);
                unset($carton['ctn_id']);

                $cusInfo = DB::table('customer')->where('cus_id', $carton['cus_id'])->first();
                $cusCode = array_get($cusInfo,'cus_code','');
                $carton['cus_code'] = $cusCode;
                $carton['cus_name'] = array_get($cusInfo,'cus_name','');
                $carton['plt_num'] = array_get($pallet_info, 'plt_num', '');
                $carton['plt_rfid'] = array_get($pallet_info, 'plt_rfid', '');
                $dataInsert[] = $carton;
            }

            foreach (array_chunk($dataInsert, 200) as $data) {
                DB::table('cartons')->insert($data);
            }
            foreach (array_chunk($dataInsert, 200) as $data) {
                DB::table('rpt_carton')->insert($data);
            }

            // update real cartons rfid to AJ
            $this->ctn->getModel()
                ->where('loc_id', $actLocId)
                ->where('ctn_sts', 'AC')
                ->whereNotNull('rfid')
                ->whereRaw(DB::raw($rfidRaw))
                ->update([
                    'ctn_sts'    => 'AJ',
                    'updated_at' => time(),
                    'updated_by' => Data::getCurrentUserId(),
                ]);
            DB::table('rpt_carton')
                ->where('loc_id', $actLocId)
                ->where('ctn_sts', 'AC')
                ->whereNotNull('rfid')
                ->whereRaw(DB::raw($rfidRaw))
                ->update([
                    'ctn_sts'    => 'AJ',
                    'updated_at' => time(),
                    'updated_by' => Data::getCurrentUserId(),
                ]);
        }
    }

    private function exportCsv($input)
    {
        try {
            $query = $this->wv_hdr->getSearchQuery(
                $input,
                ['details', 'pickerUser', 'orderHdr', 'userCreated']
            );

            $wvList = $query->get()->transform(function($item){
                if($item->wv_sts == 'CC'){
                    $numSku = $item->details()->count();
                }else{
                    $numSku = OrderDtl::where('wv_id', $item->wv_id)
                                    ->where('odr_id', $item->odr_id)
                                    ->count();
                }

                return [
                    'wv_sts'        => Status::getByKey("WAVEPICK-STATUS", $item->wv_sts),
                    'wv_num'        => array_get($item, 'wv_num', ''),
                    'odr_num'       => array_get($item, 'odr_num', ''),
                    'cus_odr_num'   => array_get($item, 'cus_odr_num', ''),
                    'cus_po'        => array_get($item, 'cus_po', ''),
                    'number_skus'   => $numSku,
                    'number_orders' => $item->orderHdr->count(),
                    'picker_name'   => trim(object_get($item, 'pickerUser.first_name', null) . " " .
                                    object_get($item, 'pickerUser.last_name', null)),
                    'user_name'     => trim(object_get($item, 'userCreated.first_name', null) . " " .
                                    object_get($item, 'userCreated.last_name', null)),
                    'created_date'  => $item->created_at->format('m/d/Y')
                ];
            });

            $this->saveFile($wvList);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function saveFile($wvList)
    {
        if ( !$wvList->count() ) {
            return $this->response->errorBadRequest('Opps! Something went wrong!');
        }

        $filePath = "Wavepick_Update_List_" . time();

        $title = [
            'wv_sts'        => 'Status',
            'wv_num'        => 'Wave Pick Number',
            'odr_num'       => 'Order Number',
            'cus_odr_num'   => 'Customer Order Number',
            'cus_po'        => 'PO',
            'number_skus'   => '# of SKUs',
            'number_orders' => '# of Orders',
            'picker_name'   => 'Picker',
            'user_name'     => 'User',
            'created_date'  => 'Created Date',
        ];

        $wvList = $wvList->prepend(array_values($title));

        WriterFactory::create(Type::CSV)
                    ->openToBrowser($filePath) // stream data directly to the browser
                    ->addRows($wvList->toArray())
                    ->close();
    }
}
