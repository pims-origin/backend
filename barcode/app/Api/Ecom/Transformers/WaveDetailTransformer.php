<?php

namespace App\Api\Ecom\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\WavepickDtl;

class WaveDetailTransformer extends TransformerAbstract
{
    //return result
    public function transform(WavepickDtl $wave)
    {
        $total = object_get($wave, 'piece_qty', 0);
        $actual = object_get($wave, 'act_piece_qty', 0);

        $sku_size_color = object_get($wave, 'sku');

        $size = object_get($wave, 'size', null);
        $color = object_get($wave, 'color', null);
        $size = !empty($size) ? strtoupper($size) : '';
        if ($size != 'NA') {
            $sku_size_color .= '-' . $size;
        }
        $color = !empty($color) ? strtoupper($color) : '';
        if ($color != 'NA') {
            $sku_size_color .= '-' . $color;
        }

        return [
            'wv_id'     => object_get($wave, 'wv_id'),
            'wv_dtl_id' => object_get($wave, 'wv_dtl_id'),
            'wv_num'    => object_get($wave, 'wv_num'),
            'lot'       => object_get($wave, 'lot'),
            'sku'       => $sku_size_color,
            'size'      => $size,
            'color'     => $color,
            'qty'       => $actual . "/" . $total,
            'status'    => $total == $actual ? "DONE" : ($actual == 0 ? "NOT" : "IN"),
            'cus_upc'   => object_get($wave, 'cus_upc'),
            'ucc128'    => Item::generateUpc128(object_get($wave, 'cus_id'), object_get($wave, 'item_id'))
        ];
    }
}
