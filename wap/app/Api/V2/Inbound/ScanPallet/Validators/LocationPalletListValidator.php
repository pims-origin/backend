<?php

namespace App\Api\V2\Inbound\ScanPallet\Validators;


class LocationPalletListValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'wv_dtl_id' => 'required'
        ];
    }
}
