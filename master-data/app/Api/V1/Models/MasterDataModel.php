<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\MasterData;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class MasterDataModel extends AbstractModel
{
    /**
     * MasterDataModel constructor.
     *
     * @param MasterData|null $model
     */
    public function __construct(MasterData $model = null)
    {
        $this->model = ($model) ?: new MasterData();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['md_code'])) {
            $query->where('md_code', 'like', "%" . SelStr::escapeLike($attributes['md_code']) . "%");
        }

        // Search Master Data Type
        $query->whereHas('masterDataType', function ($query) use ($attributes) {
            if (isset($attributes['md_type'])) {
                $query->where('md_type', 'like', "%" . SelStr::escapeLike($attributes['md_type']) . "%");
            }
        });

        if (isset($attributes['md_name'])) {
            $query->where('md_name', 'like', "%" . SelStr::escapeLike($attributes['md_name']) . "%");
        }

        if (isset($attributes['md_des'])) {
            $query->where('md_des', 'like', "%" . SelStr::escapeLike($attributes['md_des']) . "%");
        }

        if (isset($attributes['md_sts'])) {
            $query->where('md_sts', $attributes['md_sts']);
        }

        $query->orderBy('updated_at', 'DESC');

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'updated_at') {
                    $attributes['sort']['updated_at'] = $val;
                    unset($attributes['sort'][$key]);
                }
            }
        }

        // Get
        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * Create an existing model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function createMasterData(array $data)
    {
        return DB::table('master_data')->insert($data);
    }

    /**
     * Update an existing model.
     *
     * @param $mdType
     * @param $mdCode
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function updateMasterData($mdType, $mdCode, array $data)
    {
        return DB::table('master_data')
                    ->where('md_type', $mdType)
                    ->where('md_code', $mdCode)
                    ->update($data);
    }

    /**
     * @param $mdType
     * @param $mdCode
     *
     * @return mixed
     */
    public function byTypeCode($mdType, $mdCode)
    {
        return $this->model
                    ->where('md_type', $mdType)
                    ->where('md_code', $mdCode)
                    ->first();
    }

    /**
     * @param $mdType
     * @param $mdName
     *
     * @return mixed
     */
    public function byTypeName($mdType, $mdName, $mdCode = null)
    {
        $query = $this->model
                    ->where('md_type', $mdType)
                    ->where('md_name', $mdName);

        if ($mdCode) {
            $query->where('md_code', '!=', $mdCode);
        }
        return $query->first();
    }
}
