<?php

namespace App\Jobs;

use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use DB;
use GuzzleHttp\Client;

class AutoCreateWavepickJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $orderId;
    protected $cusId;
    protected $odrHdrMetaModel;
    protected $customerMetaModel;
    protected $request;
    public function __construct(
        $orderId,
        $cusId,
        $request
    ) {
        $this->orderId = $orderId;
        $this->cusId = $cusId;
        $this->odrHdrMetaModel = new OrderHdrMetaModel();
        $this->customerMetaModel = new CustomerMetaModel();
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $cus_meta = $this->odrHdrMetaModel->getOrderFlow($this->orderId);
            $createWavepickFlow = $this->customerMetaModel->getFlow($cus_meta, 'NWP');
            if (array_get($createWavepickFlow, 'usage', -1) == 1) {
                $client = new Client();
                $client->request('POST', env('API_WAVEPICK') . 'create-wave-pick/',
                    [
                        'headers'     => ['Authorization' => $this->request->getHeader('Authorization')],
                        'form_params' => ['odr_ids' => [$this->orderId]]

                    ]
                );
            }
        } catch (\Exception $exception) {
            DB::table('sys_bugs')->where('id', 3)->update(['error' => $exception->getMessage()]);
        }
    }
}
