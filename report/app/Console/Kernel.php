<?php

namespace App\Console;

use App\Jobs\AwmsForecastsJob;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\DeleteLog::class,
        \App\Console\Commands\LocationHistogram::class,
        \App\Console\Commands\LocationHistogramCorrect::class,
        \App\Console\Commands\LocHisDailyPltRptCorrect::class,
        \App\Console\Commands\DailyPalletRptCorrect::class,
        \App\Console\Commands\LocationHistogramFirst::class,
        \App\Console\Commands\DailyInventoryRpt::class,
        \App\Console\Commands\DailyPalletRpt::class,
        \App\Console\Commands\DailyForecastsRpt::class,
        \App\Console\Commands\DailyPalletRpt::class,
        \App\Console\Commands\DailyInventoryRptInsert::class,
        \App\Console\Commands\DailyPalletRptInsert::class,
        \App\Console\Commands\DailyPalletRptInsertCorrect::class,
        \App\Console\Commands\DailyReportCorrection::class,
        \App\Console\Commands\CorrectYesterdayReportData::class,

        \App\Console\Commands\AwmsForecastsFirst::class,
        \App\Console\Commands\AwmsForecastsMonthly::class,
        \App\Console\Commands\WmsForecastsFirst::class,

        \App\Console\Commands\DailyInventoryReportUpdateSchedule::class,
        \App\Console\Commands\InventoryExport::class,
        \App\Console\Commands\StorageReportCommand::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {


        //$schedule->command('location-histogram')->dailyAt('00:01')->withoutOverlapping();

        // $schedule->command('daily-inventory-rpt')->dailyAt('00:05');
        // $schedule->command('daily-pallet-rpt')->dailyAt('00:30');
        // $schedule->command('daily-inventory-rpt-insert')->dailyAt('00:40');
        // $schedule->command('daily-pallet-rpt-insert')->dailyAt('00:50');

        //$schedule->command('awms-forecast-monthly')->monthly(1, '21:00')->withoutOverlapping();
        //$schedule->command('awms-forecast-monthly')->weeklyOn(6, '0:30')->withoutOverlapping();
        //$schedule->command('awms-forecast-monthly')->monthly(date('t'), '22:00')->withoutOverlapping();

        //$schedule->command('daily-forecast-rpt')->monthly(1, '23:00')->withoutOverlapping();
        //$schedule->command('daily-forecast-rpt')->weeklyOn(6, '1:30')->withoutOverlapping();
        //$schedule->command('daily-forecast-rpt')->monthly(date('t'), '23:30')->withoutOverlapping();

        //$schedule->command('delete-log')->daily()->withoutOverlapping();

        //$schedule->command('daily-inventory-rpt-update')->monthly(8, '00::01')->withoutOverlapping();
        $schedule->command('inventory-export')->dailyAt('05:00')->withoutOverlapping();
        $schedule->command('storage-report')->dailyAt('05:00')->withoutOverlapping();
    }
}
