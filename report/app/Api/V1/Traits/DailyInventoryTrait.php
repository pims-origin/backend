<?php

namespace App\Api\V1\Traits;

use Dompdf\Exception;
use  Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


trait DailyInventoryTrait
{
    public function changeDailyInventoryReport(
        $whs_id,
        $cus_id,
        Request $request
    ) {
        //date_default_timezone_set(config('constants.timezone_utc_zero'));
        $input      = $request->getQueryParams();
        $limit      = array_get($input, 'limit', PAGING_LIMIT);
        $startDay   = array_get($input, 'select_day', date('Y-m-d'));
        $title_date = date("Y-m",strtotime($input['select_day']));
        $time = strtotime($startDay );

        try {
            $cusInfo = $this->customersModel->getFirstBy('cus_id', $cus_id);
            if (empty($cusInfo)) {
                throw new \Exception(Message::get("BM017", "Customer"));
            }
            $cusName = object_get($cusInfo, 'cus_name', '');
            //Daily Inventory On Rack and Block Stack
            $now = date("m");
            $dayNow = date("d");
            $month = date("m", strtotime($input['select_day']));
            $previousMonth = date("m", strtotime("- 1 month"));
            if($now === $month) {
                $this->procedureCurrentMonth($input['select_day'],$whs_id,$cus_id);
            }
            if($month === $previousMonth && $dayNow >= 1 && $dayNow <= 7) {
                $this->procedurePreviousMonth($input['select_day'],$whs_id,$cus_id);
            }
            $dailyInventoryReports = $this->dailyInventoryModel
                ->getDailyInventoryReport($whs_id, $cus_id, $time, $input, [], $limit, [1]);

            //Daily Inventory On Rack for RM Kayu
            $dailyInventoryOnRack = $this->transformDailyInventoryReportViaIsRack($dailyInventoryReports, 1);

            //Subtotal RM Kayu for Daily Inventory On Rack
            $subtotalForDailyInventoryOnRack = $this->transformDailyInventoryReport($dailyInventoryOnRack);
            $dailyInventoryOnRackInfos       = $this->convertNumberFormat($dailyInventoryOnRack);

            // //Daily Inventory Block Stack
            // $dailyInventoryBlockStack = $this->transformDailyInventoryReportViaIsRack($dailyInventoryReports, 0);

            // //Subtotal Block Stack for Daily Inventory
            // $subtotalBlockStackForDailyInventory = $this->transformDailyInventoryReport($dailyInventoryBlockStack);
            // $dailyInventoryBlockStackInfos       = $this->convertNumberFormat($dailyInventoryBlockStack);

            //Total Daily Inventory
            $totalForDailyInventory = $this->transformDailyInventoryReport($dailyInventoryOnRack);

            $userInfo  = new \Wms2\UserInfo\Data();
            $userInfo  = $userInfo->getUserInfo();
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            $ext = 'xls';
            Excel::create('MonthlyInventoryReport', function ($excel) use (
                &$dailyInventoryOnRackInfos,
                &$subtotalForDailyInventoryOnRack,
                // &$dailyInventoryBlockStackInfos,
                // &$subtotalBlockStackForDailyInventory,
                &$totalForDailyInventory,
                &$cusName,
                &$printedBy,
                $startDay,
                $title_date
            ) {
                // Set sheet 1
                $excel->sheet('Monthly Inventory Report', function ($sheet) use (
                    &$dailyInventoryOnRackInfos,
                    &$subtotalForDailyInventoryOnRack,
                    // &$dailyInventoryBlockStackInfos,
                    // &$subtotalBlockStackForDailyInventory,
                    &$totalForDailyInventory,
                    &$cusName,
                    &$printedBy,
                    $startDay,
                    $title_date
                ) {
                    $totalOnRack     = $dailyInventoryOnRackInfos->count();
                    $totalBlockStack = -1;
                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'Arial',
                            'size' => 11,
                            'bold' => false,
                        ),
                    ))
                        ->row(1, array(
                            'MONTHLY INVENTORY REPORT',
                        ))->mergeCells('A1:AE1')
                        ->cells('A1:AE1', function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontWeight('600');
                        })
                        ->row(2, array(
                            'Customer: ' . $cusName,
                        ))
                        ->cells('A2:AE2', function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontWeight('bold');
                        })
                        ->mergeCells('A2:AE2')
                        ->row(3, [
                            $title_date,
                            '', '', '', '', '', '', '', '', '', '',
                            date('h:i:s a') . ' WB',
                        ])
                        ->cells('A3:AE3', function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontWeight('bold italic');
                        })
                        ->mergeCells('A3:K3')
                        ->mergeCells('L3:AE3')
                        ->cells('L3:Z3', function ($cells) {
                            $cells->setAlignment('right');
                        })
                        ->row(5, [
                            'Item ID',
                            'Item/SKU',
                            'Description',

                            'Size',
                            'Color ',
                            'Length',
                            'Width',
                            'Height',
                            'Weight',

                            'UOM',
                            'Pack',
                        ])
//                        ->cells('A5:V5', function ($cells) {
//                            $cells->setBorder('1px solid #c0c0c0', '1px solid #ddd', '1px solid #c0c0c0', 'solid');
//                        })
                        ->setMergeColumn(array(
                            'columns' => array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'),
                            'rows'    => array(
                                array(5, 7),
                            ),
                        ))
                        ->setWidth('A', 10)
                        ->setWidth('B', 40)
                        ->setWidth('C', 40)
                        ->setWidth('D', 5)
                        ->setWidth('E', 5)
                        ->setWidth('F', 5)
                        ->setWidth('G', 5)
                        ->setWidth('H', 5)
                        ->setWidth('I', 5)
                        ->setWidth('J', 5)
                        ->setWidth('K', 5)
                        //->mergeCells('E9:P9')
                        //->mergeCells('E10:H10')
                        //->mergeCells('I10:L10')
                        //->mergeCells('M10:P10')
                        ->mergeCells('L5:AE5')
                        ->mergeCells('L6:O6')
                        ->mergeCells('P6:S6')
                        ->mergeCells('T6:W6')
                        ->mergeCells('X6:AA6')
                        ->mergeCells('AB6:AE6')
                        ->row(6, [
                            '', '', '', '','',
                            '', '', '', '','', '',
                             'Initial Stock',
                             '', '',
                            '',
                            'In Activity',
                            '', '',
                            '',
                            'Out Activity',
                            '', '',
                            '',
                             'Other Activity',
                             '', '',
                            '',
                            'Current Stock',
                        ])->row(7, [
                            '', '', '', '','',
                            '', '', '', '','', '',
                             'Carton',
                             'QTY',
                             'Pallet',
                             'Cube',
                            'Carton',
                            'QTY',
                            'Pallet',
                            'Cube',
                            'Carton',
                            'QTY',
                            'Pallet',
                            'Cube',
                             'Carton',
                             'QTY',
                             'Pallet',
                             'Cube',
                            'Carton',
                            'QTY',
                            'Pallet',
                            'Cube',
                        ])->cells('A5:AE5', function ($cells) {
                            $cells->setBackground('#e3f1f2')
                                ->setValignment('center')
                                ->setAlignment('center');
                        })->cells('A6:AE6', function ($cells) {
                            $cells->setBackground('#e3f1f2')
                                ->setValignment('center')
                                ->setAlignment('center');
                        })->cells('A7:AE7', function ($cells) {
                            $cells->setBackground('#e3f1f2')
                                ->setValignment('center')
                                ->setAlignment('center');
                        })
                        ->rows($dailyInventoryOnRackInfos->toArray())
//                        ->row($totalOnRack + 8, [
//                            '','','','',
//                            'Rack Subtotal',
//                            '', '','','','',
//                             $subtotalForDailyInventoryOnRack['init_ctns_qty'] ?? '0,00',
//                             $subtotalForDailyInventoryOnRack['init_qty'] ?? '0,00',
////                             $subtotalForDailyInventoryOnRack['init_plt_qty'] ?? '0',
//                             $subtotalForDailyInventoryOnRack['init_cube'] ?? '0,00',
//
//                            $subtotalForDailyInventoryOnRack['in_ctns_qty'] ?? '0,00',
//                            $subtotalForDailyInventoryOnRack['in_qty'] ?? '0,00',
////                            $subtotalForDailyInventoryOnRack['in_plt_qty'] ?? '0',
//                            $subtotalForDailyInventoryOnRack['in_cube'] ?? '0,00',
//
//                            $subtotalForDailyInventoryOnRack['out_ctns_qty'] ?? '0,00',
//                            $subtotalForDailyInventoryOnRack['out_qty'] ?? '0,00',
////                            $subtotalForDailyInventoryOnRack['out_plt_qty'] ?? '0',
//                            $subtotalForDailyInventoryOnRack['out_cube'] ?? '0,00',
//
//                             $subtotalForDailyInventoryOnRack['other_ctns_qty'] ?? '0,00',
//                             $subtotalForDailyInventoryOnRack['other_qty'] ?? '0,00',
////                             $subtotalForDailyInventoryOnRack['other_plt_qty'] ?? '0',
//                             $subtotalForDailyInventoryOnRack['other_cube'] ?? '0,00',
//
//                            $subtotalForDailyInventoryOnRack['cur_ctns_qty'] ?? '0,00',
//                            $subtotalForDailyInventoryOnRack['cur_qty'] ?? '0,00',
////                            $subtotalForDailyInventoryOnRack['cur_plt_qty'] ?? '0',
//                            $subtotalForDailyInventoryOnRack['cur_cube'] ?? '0,00',
//                        ])
//                        ->cells('A' . ($totalOnRack + 8) . ':Z' . ($totalOnRack + 8), function ($cells) {
//                            $cells->setBackground('#e7e7e7');
//                        })
//                        ->mergeCells('F' . ($totalOnRack + 8) . ':K' . ($totalOnRack + 8))
                        // ->rows($dailyInventoryBlockStackInfos->toArray())
                        // ->row($totalOnRack + 12 + $totalBlockStack + 1, [
                        //     '',
                        //     'Block Stack Subtotal',
                        //     '', '',
                        //     $subtotalBlockStackForDailyInventory['init_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['init_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['init_plt_qty'] ?? '0',

                        //     $subtotalBlockStackForDailyInventory['in_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['in_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['in_plt_qty'] ?? '0',

                        //     $subtotalBlockStackForDailyInventory['out_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['out_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['out_plt_qty'] ?? '0',

                        //     $subtotalBlockStackForDailyInventory['other_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['other_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['other_plt_qty'] ?? '0',

                        //     $subtotalBlockStackForDailyInventory['cur_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['cur_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyInventory['cur_plt_qty'] ?? '0',
                        // ])
                        ->cells('A' . ($totalOnRack + 8) . ':AE' . ($totalOnRack + 8), function ($cells) {
                            $cells->setBackground('#e7e7e7');
                        })
                        ->mergeCells('F' . ($totalOnRack + 8) . ':K' . ($totalOnRack + 8))
                        ->row($totalOnRack + 8,[
                            '','','','','',
                            'Total',
                            '', '','','','',
                            $subtotalForDailyInventoryOnRack['init_ctns_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['init_qty'] ?? '0,00',
                             $totalForDailyInventory['init_plt_qty'] ?? '0',
                            $subtotalForDailyInventoryOnRack['init_cube'] ?? '0,00',

                            $subtotalForDailyInventoryOnRack['in_ctns_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['in_qty'] ?? '0,00',
                            $totalForDailyInventory['in_plt_qty'] ?? '0',
                            $subtotalForDailyInventoryOnRack['in_cube'] ?? '0,00',

                            $subtotalForDailyInventoryOnRack['out_ctns_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['out_qty'] ?? '0,00',
                            $totalForDailyInventory['out_plt_qty'] ?? '0',
                            $subtotalForDailyInventoryOnRack['out_cube'] ?? '0,00',

                            $subtotalForDailyInventoryOnRack['other_ctns_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['other_qty'] ?? '0,00',
                             $totalForDailyInventory['other_plt_qty'] ?? '0',
                            $subtotalForDailyInventoryOnRack['other_cube'] ?? '0,00',

                            $subtotalForDailyInventoryOnRack['cur_ctns_qty'] ?? '0,00',
                            $subtotalForDailyInventoryOnRack['cur_qty'] ?? '0,00',
                            $totalForDailyInventory['cur_plt_qty'] ?? '0',
                            $subtotalForDailyInventoryOnRack['cur_cube'] ?? '0,00',

                        ])
                        ->mergeCells('F' . ($totalOnRack + 8 ) . ':K' . ($totalOnRack + 8 ))
                        ->cells('A' . ($totalOnRack + 8) . ':AE' . ($totalOnRack + 8 ), function ($cells) {
                            $cells->setBackground('#fff000');
                        })
                        ->mergeCells('F' . ($totalOnRack + 8 + $totalBlockStack + 2) . ':K' . ($totalOnRack + 8 +
                                $totalBlockStack + 2))
//                        ->setBorder('A9:V' . ($totalOnRack + 8 + $totalBlockStack + 2), 'thin')
                        ->setBorder('A5:AE' . ($totalOnRack + 8 + $totalBlockStack + 1), 'thin')
                        ->cells('A' . ($totalOnRack + 8 + $totalBlockStack + 2) . ':AE' . ($totalOnRack + 8 + $totalBlockStack + 2), function ($cells) {
                            $cells->setFontWeight('100');
                        })
                        ->cells('B5'.':B' . ($totalOnRack + 8 + $totalBlockStack + 2), function ($cells) {
                            $cells->setAlignment('right');;
                        })
                        ->rows(
                            [
                                [], [], [],
                                [date("m/d/Y h:i:s a") . ' By ' . $printedBy],
                            ]
                        );
                });

            })
                ->download($ext);
            //->store($ext, storage_path('excel/exports'));

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    public function updateReport($whs_id,$cus_id, Request $request) {
        try {
            $input      = $request->getQueryParams();
            $month =  $input["month"];
            $result = $this->procedureCurrentMonth($month,$whs_id,$cus_id);
            return("Update Successful");

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function procedureCurrentMonth($month,$whsId,$cusId)
    {
        try {

            DB::beginTransaction();

            $invDate = strtotime($month);
            $preDate = strtotime($month . "- 1 month");
            $tmrDate = strtotime($month . "+ 1 month");
            $now = time();
            DB::select(DB::raw("
                DELETE FROM daily_inv_rpt 
                WHERE whs_id = $whsId 
                AND cus_id = $cusId  AND inv_dt = $invDate"));
            /*
            Update INIT  Activity
            */

            DB::select(DB::raw("INSERT INTO daily_inv_rpt (
                        
                        `whs_id`,
                        
                        `cus_id`,
                        
                        `item_id`,
                        
                        `description`,
                        
                        `sku`,
                        
                        `size`,
                        
                        `color`,
                        
                        `pack`,
                        
                        `uom`,
                        
                        `inv_dt`,
                        
                        `init_ctns`,
                        
                        `init_qty`,
                        
                        `init_plt`,
                        
                        `in_ctns`,
                        
                        `in_qty`,
                        
                        `in_plt`,
                        
                        `out_ctns`,
                        
                        `out_qty`,
                        
                        `out_plt`,
                        
                        `other_ctns`,
                        
                        `other_qty`,
                        
                        `other_plt`,
                        
                        `created_at`,
                        
                        `updated_at`
                        
                        ) SELECT
                        
                        $whsId  AS whs_id,
                        
                        i.cus_id,
                        
                        i.item_id,
                        
                        i.description,
                        
                        i.sku,
                        
                        i.size,
                        
                        i.color,
                        
                        i.pack,
                        
                        i.uom_code AS uom,
                        
                        $invDate AS inv_dt,
                        
                        0 AS init_ctns,
                        
                        0 AS init_qty,
                        
                        0 AS init_plt,
                        
                        0 AS in_ctns,
                        
                        0 AS in_qty,
                        
                        0 AS in_plt,
                        
                        0 AS out_ctns,
                        
                        0 AS out_qty,
                        
                        0 AS out_plt,
                        
                        0 AS other_ctns,
                        
                        0 AS other_qty,
                        
                        0 AS other_plt,
                        
                        $now AS created_at,
                        
                        $now AS updated_at
                        
                        FROM
                        
                        item i
                        
                        WHERE
                        
                        i.cus_id = $cusId"));

            /*
            Update IN  Activity
            */

            DB::select(DB::raw("UPDATE gr_dtl d
                                
                                JOIN
                                
                                (SELECT h.whs_id, h.cus_id, d.gr_dtl_id, COUNT(DISTINCT(p.plt_id)) AS plt, d.gr_dtl_plt_ttl
                                
                                FROM gr_dtl d
                                
                                JOIN gr_hdr h ON h.gr_hdr_id = d.gr_hdr_id
                                
                                JOIN cartons_rev c ON d.gr_dtl_id = c.gr_dtl_id
                                
                                JOIN pallet p ON p.plt_id = c.plt_id AND p.gr_hdr_id = d.gr_hdr_id
                                
                                WHERE h.whs_id = $whsId
                                
                                AND h.cus_id = $cusId
                                
                                GROUP BY d.gr_dtl_id
                                
                                HAVING plt != gr_dtl_plt_ttl) AS t
                                
                                ON t.gr_dtl_id = d.gr_dtl_id
                                
                                SET d.gr_dtl_plt_ttl = t.plt"));


            DB::select(DB::raw("UPDATE daily_inv_rpt i
                                
                                JOIN (
                                
                                SELECT
                                
                                  h.whs_id,
                                
                                  d.item_id,
                                
                                  SUM( d.gr_dtl_act_ctn_ttl ) AS in_ctns,
                                
                                                SUM( d.gr_dtl_plt_ttl ) AS in_plt
                                
                                FROM
                                
                                  gr_dtl d
                                
                                  JOIN gr_hdr h ON h.gr_hdr_id = d.gr_hdr_id
                                
                                 WHERE
                                
                                                h.whs_id = $whsId
                                
                                                AND h.cus_id = $cusId
                                
                                  AND h.gr_sts = 'RE'       
                                
                                                AND d.gr_dtl_sts = 'RE'
                                
                                                AND d.deleted = 0
                                
                                  AND h.gr_hdr_act_dt >= $invDate AND h.gr_hdr_act_dt < $tmrDate
                                
                                GROUP BY
                                
                                  h.whs_id, d.item_id
                                
                                 ) t ON t.whs_id = i.whs_id
                                
                                 AND t.item_id = i.item_id
                                
                                 AND i.inv_dt = $invDate
                                
                                 SET i.in_qty = t.in_ctns * i.pack,
                                
                                i.in_ctns = t.in_ctns,
                                
                                i.in_plt = t.in_plt
                                
                                WHERE
                                
                                i.whs_id = $whsId
                                
                                 AND i.inv_dt = $invDate
                                
                                 AND i.cus_id = $cusId"));

            /*
            Update Out Activity
            */

            DB::select(DB::raw("UPDATE daily_inv_rpt i
                                
                                JOIN (
                                
                                SELECT
                                
                                 h.whs_id,
                                
                                  d.item_id,
                                
                                  d.pack,
                                
                                  SUM( d.picked_qty ) AS out_qty
                                
                                 FROM
                                
                                  odr_hdr h
                                
                                  JOIN odr_dtl d ON h.odr_id = d.odr_id
                                
                                 WHERE
                                
                                                h.whs_id = $whsId
                                
                                                AND h.cus_id = $cusId
                                
                                  AND   h.odr_sts = 'SH'
                                
                                                AND d.deleted = 0
                                
                                  AND h.shipped_dt >= $invDate AND h.shipped_dt < $tmrDate
                                
                                  AND d.picked_qty > 0
                                
                                 GROUP BY
                                
                                  h.whs_id, d.item_id
                                
                                 ) t ON t.whs_id = i.whs_id
                                
                                 AND t.item_id = i.item_id
                                
                                 AND i.inv_dt = $invDate
                                
                                 SET i.out_qty = t.out_qty,
                                
                                                i.out_ctns = ROUND(t.out_qty / t.pack,0)
                                
                                WHERE
                                
                                i.whs_id = $whsId
                                
                                 AND i.inv_dt = $invDate
                                
                                AND i.cus_id = $cusId"));

            DB::select(DB::raw("UPDATE daily_inv_rpt i
                                
                                JOIN (
                                
                                SELECT
                                
                                  h.whs_id,
                                
                                  d.item_id,
                                
                                  COUNT( DISTINCT ( IF( p.ctn_ttl = 0, p.plt_id , null ))) AS out_plt
                                
                                FROM
                                
                                  odr_hdr h
                                
                                  JOIN odr_dtl d ON h.odr_id = d.odr_id
                                
                                  JOIN odr_cartons o ON d.odr_dtl_id = o.odr_dtl_id
                                
                                  JOIN cartons c ON o.ctn_id = c.ctn_id
                                
                                   JOIN pallet p ON p.plt_id = c.plt_id AND p.plt_sts = 'PD' AND p.updated_at >= $invDate AND p.updated_at < $tmrDate


                                
                                 WHERE
                                
                                                h.whs_id = $whsId
                                
                                                AND h.cus_id = $cusId
                                
                                  AND h.odr_sts = 'SH'
                                
                                                AND c.ctn_sts = 'SH'
                                
                                                AND d.deleted = 0
                                
                                  AND h.shipped_dt >= $invDate AND h.shipped_dt < $tmrDate
                                
                                  AND d.picked_qty > 0
                                
                                  
                                
                                 GROUP BY
                                
                                  h.whs_id, d.item_id
                                
                                 ) t ON t.whs_id = i.whs_id
                                
                                 AND t.item_id = i.item_id
                                
                                 AND i.inv_dt = $invDate
                                
                                 SET i.out_plt = t.out_plt
                                
                                WHERE
                                
                                i.whs_id = $whsId
                                
                                 AND i.inv_dt = $invDate
                                
                                 AND i.cus_id = $cusId"));

            /*

            Update Other Activity

            **/

            DB::select(DB::raw("UPDATE cycle_dtl d
                
                SET d.remain = d.pack
                
                WHERE d.remain = 0 OR  d.remain > d.pack"));



            DB::select(DB::raw("UPDATE daily_inv_rpt  i
                
                JOIN (
                
                SELECT d.whs_id, d.cus_id, d.item_id,
                
                 SUM(IF(h.count_by = 'EACH', 1, d.remain) * ( CAST(d.act_qty AS SIGNED) - CAST(d.sys_qty AS SIGNED))) AS other_qty,
                
                SUM(IF(d.sys_qty = 0, 1, IF(d.act_qty = 0, -1, 0))) AS other_plt
                
                FROM cycle_hdr h
                
                JOIN cycle_dtl d ON h.cycle_hdr_id = d.cycle_hdr_id
                
                WHERE h.cycle_sts = 'CP'
                
                AND d.sys_qty != d.act_qty
                
                AND d.whs_id = $whsId
                
                AND d.cus_id = $cusId
                
                AND h.updated_at >= $invDate AND h.updated_at < $tmrDate
                
                GROUP BY d.whs_id, d.cus_id, d.item_id
                
                ) t ON t.whs_id = i.whs_id
                
                 AND t.item_id = i.item_id
                
                 AND i.inv_dt = $invDate
                
                 SET i.other_qty = t.other_qty,
                
                i.other_ctns =  ROUND(t.other_qty / i.pack, 0),
                
                i.other_plt = t.other_plt
                
                WHERE
                
                i.whs_id = $whsId
                
                 AND i.inv_dt = $invDate
                
                 AND i.cus_id = $cusId"));

            /*
            Update Current Activity
             */

            DB::select(DB::raw("UPDATE daily_inv_rpt i
                                        
                                        LEFT JOIN (
                                        
                                        SELECT
                                        
                                          c.whs_id,
                                        
                                          c.item_id,        
                                        
                                          SUM( c.piece_remain ) AS cur_qty,
                                        
                                        COUNT( DISTINCT ( IFNULL(c.plt_id, 1 ) )) AS cur_plt
                                        
                                         FROM
                                        
                                          cartons c
                                        
                                         WHERE
                                        
                                                        c.whs_id = $whsId
                                        
                                                        AND c.cus_id = $cusId
                                        
                                          AND c.ctn_sts IN ( 'AC', 'LK', 'PD' )
                                        
                                                        AND c.deleted = 0
                                        
                                        GROUP BY c.whs_id, c.item_id
                                        
                                         ) t ON t.whs_id = i.whs_id
                                        
                                         AND t.item_id = i.item_id
                                        
                                         AND i.inv_dt = $invDate
                                        
                                         SET i.cur_qty = IFNULL(t.cur_qty, 0),
                                        
                                        i.cur_ctns = ROUND(IFNULL(t.cur_qty, 0) / i.pack,0),
                                        
                                        i.cur_plt = IFNULL(t.cur_plt,0)
                                        
                                        WHERE
                                        
                                        i.whs_id = $whsId
                                        
                                         AND i.cus_id = $cusId
                                        
                                        AND i.inv_dt = $invDate"));

            DB::select(DB::raw("UPDATE daily_inv_rpt i
                                
                                JOIN (
                                
                                SELECT
                                
                                  c.whs_id,
                                
                                  c.item_id,
                                
                                  c.cur_ctns,
                                
                                  c.cur_qty,
                                
                                  c.cur_plt
                                
                                 FROM
                                
                                  daily_inv_rpt c
                                
                                 WHERE
                                
                                  c.inv_dt = $preDate
                                
                                                AND c.whs_id = $whsId
                                
                                  AND c.cus_id = $cusId
                                
                                ) AS t ON t.whs_id = i.whs_id
                                
                                 AND t.item_id = i.item_id
                                
                                 AND i.inv_dt = $invDate
                                
                                 SET i.init_qty = IFNULL(t.cur_qty, 0),
                                
                                i.init_ctns = IFNULL(t.cur_ctns,0),
                                
                                i.init_plt = IFNULL(t.cur_plt, 0)
                                
                                WHERE
                                
                                i.whs_id = $whsId
                                
                                 AND i.cus_id = $cusId
                                
                                AND i.inv_dt = $invDate"));

            DB::select(DB::raw("DELETE
                        FROM
                          daily_inv_rpt
                        WHERE whs_id = $whsId
                          AND cus_id = $cusId
                          AND inv_dt = $invDate
                          AND init_ctns = 0
                          AND init_qty = 0
                          AND init_plt = 0
                          AND in_ctns = 0
                          AND in_qty = 0
                          AND in_plt = 0
                          AND out_ctns = 0
                          AND out_qty = 0
                          AND out_plt = 0
                          AND other_qty = 0
                          AND other_ctns = 0
                          AND other_plt = 0
                          AND cur_ctns = 0
                          AND cur_qty = 0
                          AND cur_plt = 0"));
            

            DB::commit();
            return 1;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function procedurePreviousMonth($month,$whsId,$cusId)
    {
        try {

            DB::beginTransaction();

            $invDate = strtotime($month . "- 1 month");
            $tmrDate = strtotime($month);
            $invTime = time();

            DB::select(DB::raw("
                                DELETE 
                                FROM
                                 daily_inv_rpt
                                WHERE
                                 whs_id = $whsId 
                                 AND cus_id = $cusId 
                                 AND inv_dt = $invDate
            "));

            /*
            Update INIT  Activity
            */

            DB::select(DB::raw("INSERT INTO daily_inv_rpt (
                                 `whs_id`,
                                 `cus_id`,
                                 `item_id`,
                                 `description`,
                                 `sku`,
                                 `size`,
                                 `color`,
                                 `pack`,
                                 `uom`,
                                 `inv_dt`,
                                 `init_ctns`,
                                 `init_qty`,
                                 `init_plt`,
                                 `in_ctns`,
                                 `in_qty`,
                                 `in_plt`,
                                 `out_ctns`,
                                 `out_qty`,
                                 `out_plt`,
                                 `other_ctns`,
                                 `other_qty`,
                                 `other_plt`,
                                 `created_at`,
                                 `updated_at` 
                                ) SELECT
                                $whsId  AS whs_id,
                                i.cus_id,
                                i.item_id,
                                i.description,
                                i.sku,
                                i.size,
                                i.color,
                                i.pack,
                                i.uom_code AS uom,
                                $invDate AS inv_dt,
                                0 AS init_ctns,
                                0 AS init_qty,
                                0 AS init_plt,
                                0 AS in_ctns,
                                0 AS in_qty,
                                0 AS in_plt,
                                0 AS out_ctns,
                                0 AS out_qty,
                                0 AS out_plt,
                                0 AS other_ctns,
                                0 AS other_qty,
                                0 AS other_plt,
                                $invTime AS created_at,
                                $invTime AS updated_at 
                                FROM
                                 item i
                                WHERE
                                i.cus_id = $cusId
            "));

            /*
            Update IN  Activity
            */
            DB::select(DB::raw("
                                UPDATE daily_inv_rpt i
                                JOIN (
                                 SELECT
                                  h.whs_id,
                                  d.item_id,
                                  SUM( d.gr_dtl_act_ctn_ttl ) AS in_ctns,
                                    SUM( d.gr_dtl_plt_ttl ) AS in_plt
                                 FROM
                                  gr_dtl d
                                  JOIN gr_hdr h ON h.gr_hdr_id = d.gr_hdr_id 
                                 WHERE
                                    h.whs_id = $whsId 
                                    AND h.cus_id = $cusId
                                  AND h.gr_sts = 'RE' 	
                                    AND d.gr_dtl_sts = 'RE'
                                    AND d.deleted = 0
                                  AND h.gr_hdr_act_dt >= $invDate AND h.gr_hdr_act_dt < $tmrDate
                                 GROUP BY
                                  h.whs_id, d.item_id 
                                 ) t ON t.whs_id = i.whs_id 
                                 AND t.item_id = i.item_id 
                                 AND i.inv_dt = $invDate 
                                 SET i.in_qty = t.in_ctns * i.pack,
                                 i.in_ctns = t.in_ctns,
                                 i.in_plt = t.in_plt
                                WHERE
                                 i.whs_id = $whsId 
                                 AND i.inv_dt = $invDate 
                                 AND i.cus_id = $cusId
            "));

            /*
            Update Out Activity
            */

            DB::select(DB::raw("
                                UPDATE daily_inv_rpt i
                                JOIN (
                                 SELECT
                                  h.whs_id,
                                  d.item_id,
                                  d.pack,
                                  SUM( d.picked_qty ) AS out_qty 
                                 FROM
                                  odr_hdr h
                                  JOIN odr_dtl d ON h.odr_id = d.odr_id 
                                 WHERE
                                    h.whs_id = $whsId
                                    AND h.cus_id = $cusId
                                  AND   h.odr_sts = 'SH' 
                                    AND d.deleted = 0
                                  AND h.shipped_dt >= $invDate AND h.shipped_dt < $tmrDate 
                                  AND d.picked_qty > 0 
                                 GROUP BY
                                  h.whs_id, d.item_id 
                                 ) t ON t.whs_id = i.whs_id 
                                 AND t.item_id = i.item_id 
                                 AND i.inv_dt = $invDate 
                                 SET i.out_qty = t.out_qty,
                                    i.out_ctns = ROUND(t.out_qty / t.pack,0)
                                WHERE
                                 i.whs_id = $whsId 
                                 AND i.inv_dt = $invDate
                                 AND i.cus_id = $cusId
            "));

            DB::select(DB::raw("
                                UPDATE daily_inv_rpt i
                                JOIN (
                                 SELECT
                                  h.whs_id,
                                  d.item_id,
                                  COUNT( DISTINCT ( IF( p.ctn_ttl = 0, p.plt_id , null ))) AS out_plt
                                 FROM
                                  odr_hdr h
                                  JOIN odr_dtl d ON h.odr_id = d.odr_id
                                  JOIN odr_cartons o ON d.odr_dtl_id = o.odr_dtl_id
                                  JOIN cartons c ON o.ctn_id = c.ctn_id
                                  JOIN pallet p ON p.plt_id = c.plt_id 
                                 WHERE
                                    h.whs_id = $whsId
                                    AND h.cus_id = $cusId
                                  AND h.odr_sts = 'SH' 
                                    AND c.ctn_sts = 'SH'
                                    AND d.deleted = 0
                                  AND h.shipped_dt >= $invDate AND h.shipped_dt < $tmrDate
                                  AND d.picked_qty > 0 
                                  
                                 GROUP BY
                                  h.whs_id, d.item_id 
                                 ) t ON t.whs_id = i.whs_id 
                                 AND t.item_id = i.item_id 
                                 AND i.inv_dt = $invDate 
                                 SET i.out_plt = t.out_plt
                                WHERE
                                 i.whs_id = $whsId 
                                 AND i.inv_dt = $invDate 
                                 AND i.cus_id = $cusId            
            "));

            /*

            Update Other Activity

            **/

            DB::select(DB::raw("UPDATE cycle_dtl d
                
                SET d.remain = d.pack
                
                WHERE d.remain = 0 OR  d.remain > d.pack
            "));



            DB::select(DB::raw("
                                UPDATE daily_inv_rpt  i
                                JOIN (
                                 SELECT d.whs_id, d.cus_id, d.item_id, 
                                 SUM(IF(h.count_by = 'EACH', 1, d.remain) * ( CAST(d.act_qty AS SIGNED) - CAST(d.sys_qty AS SIGNED))) AS other_qty,
                                 SUM(IF(d.sys_qty = 0, 1, IF(d.act_qty = 0, -1, 0))) AS other_plt
                                 FROM cycle_hdr h 
                                JOIN cycle_dtl d ON h.cycle_hdr_id = d.cycle_hdr_id
                                WHERE h.cycle_sts = 'CP' 
                                AND d.sys_qty != d.act_qty
                                AND d.whs_id = $whsId 
                                AND d.cus_id = $cusId
                                AND h.updated_at >= $invDate AND h.updated_at < $tmrDate
                                GROUP BY d.whs_id, d.cus_id, d.item_id
                                 ) t ON t.whs_id = i.whs_id 
                                 AND t.item_id = i.item_id 
                                 AND i.inv_dt = $invDate 
                                 SET i.other_qty = t.other_qty,
                                 i.other_ctns =  ROUND(t.other_qty / i.pack, 0),
                                 i.other_plt = t.other_plt
                                WHERE
                                 i.whs_id = $whsId 
                                 AND i.inv_dt = $invDate 
                                 AND i.cus_id = $cusId
            "));

            /*

            Update Current Activity

            */
            DB::select(DB::raw("
                            UPDATE daily_inv_rpt i
                            LEFT JOIN (
                             SELECT
                              c.whs_id,
                              c.item_id,
                              c.init_ctns,
                              c.init_qty,
                              c.init_plt 
                             FROM
                              daily_inv_rpt c 
                             WHERE
                              c.inv_dt = $tmrDate
                                AND c.whs_id = $whsId 
                              AND c.cus_id = $cusId
                             ) t ON t.whs_id = i.whs_id 
                             AND t.item_id = i.item_id 
                             AND i.inv_dt = $invDate 
                             SET i.cur_qty = IFNULL(t.init_qty, 0),
                             i.cur_ctns = ROUND(IFNULL(t.init_qty, 0) / i.pack,0),
                             i.cur_plt = IFNULL(t.init_plt, 0)
                            WHERE
                             i.whs_id = $whsId 
                             AND i.cus_id = $cusId
                             AND i.inv_dt = $invDate
            "));

            /*
            Init + In - Out + Other = Current
            Init = Current - In + Out - Other
            */
            DB::select(DB::raw("
                                UPDATE daily_inv_rpt i
                                 SET i.init_qty = i.cur_qty - i.in_qty + i.out_qty - i.other_qty,
                                 i.init_ctns = i.cur_ctns - i.in_ctns + i.out_ctns - i.other_ctns,
                                 i.init_plt = i.cur_plt - i.in_plt + i.out_plt - i.other_plt
                                WHERE
                                 i.whs_id = $whsId 
                                 AND i.cus_id = $cusId
                                 AND i.inv_dt = $invDate
            "));

            DB::commit();
            return 1;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}