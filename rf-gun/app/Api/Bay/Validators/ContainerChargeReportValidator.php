<?php

namespace App\Api\Bay\Validators;


class ContainerChargeReportValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'bay_id'  => 'integer|exists:bay,bay_id',
            'cus_id'  => 'integer|exists:customer,cus_id',
            'ctnr_id' => 'integer|exists:container,ctnr_id',
        ];

    }


}
