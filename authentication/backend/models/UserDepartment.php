<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/17/2016
 * Time: 9:31 AM
 */
namespace backend\models;

use yii\db\Query;
use Yii;

class UserDepartment
{
    public static function getUserDept()
    {
        $query = (new Query());
        $dept = $query->from('user_department')
            ->select('*')
            ->all();
        if (null === $dept) {
            return false;
        }

        return $dept;
    }

    public static function getUserDeptById($id)
    {
        $query = (new Query());
        $dept = $query->from('user_department')
            ->select('*')
            ->where('usr_dpm_id = ' . $id)
            ->one();
        if (null === $dept) {
            return false;
        }

        return $dept;
    }
}