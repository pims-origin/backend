<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 25-Jul-2016
 * Time: 2:50
 */

namespace App\Api\V1\Validators;


class CartonValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'plt_id'        => 'integer',
            'asn_dtl_id'    => 'integer',
            'gr_dtl_id'     => 'integer',
            'item_id'       => 'integer',
            'whs_id'        => 'integer',
            'cus_id'        => 'integer',
            'loc_id'        => 'integer',
            'ctn_total'     => 'integer',
            'ctn_num'       => 'required',
            'ctn_sts'       => 'required',
            'ctn_uom_id'    => 'required',
            'ctn_pack_size' => 'integer',
            'loc_code'      => 'required|alpha_dash',
            'loc_name'      => 'required|alpha_dash',
        ];
    }
}