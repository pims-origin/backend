<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use App\SystemUom;
use App\Utils\SelStr;
use App\Utils\SelArr;

class SystemUomModel extends AbstractModel
{
    /**
     * SystemUomModel constructor.
     *
     * @param SystemUom|null $model
     */
    public function __construct(SystemUom $model = null)
    {
        $this->model = ($model) ?: new SystemUom();
    }

    /**
     * @param int $char_uom_id
     *
     * @return int
     */
    public function deleteSystemUom($char_uom_id)
    {
        return $this->model
            ->where('sys_uom_id', $char_uom_id)
            ->delete();
    }

    public function deleteMassSystemUom(array $char_uom_ids)
    {
        return $this->model
            ->whereIn('sys_uom_id', $char_uom_ids)
            ->delete();
    }

    /**
     * Search SystemUom
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['sys_uom_code', 'sys_uom_name', 'sys_uom_type'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param array $params
     * @param array $with
     *
     * @return mixed
     */
    public function loadBy($params = [], $with = [])
    {
        $query = $this->make($with);

        if (!empty($params) && is_array($params)) {
            foreach ($params as $key => $value) {
                $query->where($key, $value);
            }
        }
        // Get
        $models = $query->get();

        return $models;
    }
}
