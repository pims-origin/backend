<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:34
 */

namespace Seldat\Wms2\Models;

/**
 * @property mixed whs_add_line_1
 * @property mixed whs_add_line_2
 * @property mixed whs_add_country_code
 * @property mixed whs_add_city_name
 * @property mixed whs_add_state_code
 * @property mixed whs_add_postal_code
 * @property mixed whs_add_whs_id
 * @property mixed whs_add_type
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 *
 */
class SystemState extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_state';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_state_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sys_state_code',
        'sys_state_name',
        'sys_state_country_id',
        'sys_state_desc'
    ];


    public function warehouseAddress()
    {
        return $this->hasOne(__NAMESPACE__ . '\WarehouseAddress', 'sys_state_id', 'whs_add_state_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'sys_state_id', 'whs_state_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_state_id', 'sys_state_id');
    }

    public function customerAddress()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerAddress', 'sys_state_id', 'cus_add_state_id');
    }

    public function systemCountry()
    {
        return $this->belongsTo(__NAMESPACE__ .'\SystemCountry', 'sys_state_country_id', 'sys_country_id');
    }
}
