<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:19
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseAddress;

class WarehouseAddressTransformer extends TransformerAbstract
{
    /**
     * @param WarehouseAddress $warehouseAddress
     *
     * @return array
     */
    public function transform(WarehouseAddress $warehouseAddress)
    {
        return [
            'whs_add_id'          => $warehouseAddress->whs_add_id,
            'whs_add_line_1'      => $warehouseAddress->whs_add_line_1,
            'whs_add_line_2'      => $warehouseAddress->whs_add_line_2,
            'whs_add_city_name'   => $warehouseAddress->whs_add_city_name,
            'whs_add_state_id'    => $warehouseAddress->whs_add_state_id,
            'whs_add_state_name'  => !empty($warehouseAddress->systemState->sys_state_name)
                                    ? $warehouseAddress->systemState->sys_state_name
                                    : null,
            'whs_add_postal_code' => $warehouseAddress->whs_add_postal_code,
            'whs_add_whs_id'      => $warehouseAddress->whs_add_whs_id,
            'whs_add_type'        => $warehouseAddress->whs_add_type,
            'created_at'          => $warehouseAddress->created_at,
            'updated_at'          => $warehouseAddress->updated_at,
            'deleted_at'          => $warehouseAddress->deleted_at,
        ];
    }
}
