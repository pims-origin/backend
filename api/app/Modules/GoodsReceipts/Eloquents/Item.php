<?php

namespace App\Modules\Asns\Eloquents;

class Item extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'item';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_id';

    /**
     * @var array
     */
    public $timestamps = false;

    protected $fillable = [
        'item_code',
        'description',
        'lot',
        'upc',
        'sku',
        'size',
        'color',
        'uom_id',
        'uom_name',
        'uom_code',
        'pack',
        'length',
        'width',
        'height',
        'weight',
        'net_weight',
        'volume',
        'cube',
        'cus_id',
        'cus_upc',
        'status',
        'condition',
        'created_by',
        'updated_by',
        'spc_hdl_code',
        'spc_hdl_name',
        'cat_code',
        'cat_name',
        'children_checksum',
        'dim_uom',
        'weight_uom'
    ];

    public function getDateFormat()
    {
        return 'U';
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id');
    }
}
