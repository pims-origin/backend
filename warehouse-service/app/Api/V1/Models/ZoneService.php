<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class ZoneService extends BaseService
{
    /**
     * @param $params
     * @param $warehouseId
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create($params, $warehouseId)
    {
        $uri = $this->getUri($warehouseId);

        $params['whs_zone_whs_id'] = $warehouseId;

        //  Call api create warehouse
        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    public function edit($params, $warehouseId, $zoneId)
    {
        $uri = $this->getUri($warehouseId) . '/' . $zoneId;

        $params['whs_zone_whs_id'] = $warehouseId;
        $params['whs_zone_id'] = $zoneId;

        $options = [
            'form_params' => $params
        ];

        return $this->client->put($uri, $options);
    }

    public function search($warehouseId, $queryStr, $pre = '')
    {
        $uri = $this->getUri($warehouseId, $pre);
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }

    public function getDetail($warehouseId, $id)
    {
        $uri = $this->getUri($warehouseId) . '/' . $id;
        $response = $this->client->get($uri);

        return $response;
    }

    public function getLocations($warehouseId, $zoneId, $queryStr)
    {
        $uri = $this->getUri($warehouseId) . '/' . $zoneId . '/locations';
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }

    public function addLocations($params, $warehouseId, $zoneId)
    {
        $uri = $this->getUri($warehouseId) . '/' . $zoneId . '/locations/save-mass-and-delete-unused';

        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    protected function getUri($warehouseId, $pre = '')
    {
        return env('API_WAREHOUSE') . 'warehouses/' . $warehouseId . '/' . $pre . 'zones';
    }

    public function freeZones($queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'warehouses' . '/free-zones';
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }

}
