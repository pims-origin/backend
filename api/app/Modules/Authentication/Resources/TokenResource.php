<?php

namespace App\Modules\Authentication\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TokenResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
            'type' => 'users',
            'id' => (int) $this->user_id,
            'attributes' => [
                'token'   => $this->token
            ]
        ];
    }
}