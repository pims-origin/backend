<?php

namespace App\Api\V1\Models;

use Psr\Http\Message\ServerRequestInterface as IRequest;

class ItemLevelService extends BaseService
{

    /**
     * ItemCatService constructor.
     * @param IRequest $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_ITEM_MASTER');
        parent::__construct($request, $api);
    }

    /**
     * @param $queryStr
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getList($queryStr)
    {
        $uri = $this->getUri() . "item-level";
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }

    /**
     * @return string
     */
    protected function getUri()
    {
        return env('API_ITEM_MASTER');
    }
}
