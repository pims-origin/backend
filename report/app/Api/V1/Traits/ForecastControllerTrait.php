<?php

namespace App\Api\V1\Traits;


trait ForecastControllerTrait
{
    private function calculateActual(&$data, $storageDate) {
        $firstData = $data->first();

        if (isset($data['CTNR'])) {
            $containerForecast =& $data['CTNR'];
        }

        if (isset($data['PL'])) {
            $palletForecast =& $data['PL'];
        }

        if (isset($data['CTN'])) {
            $cartonForecast =& $data['CTN'];
        }

        if (isset($data['CUB'])) {
            $cubeForecast =& $data['CUB'];
        }

        if (isset($data['UN'])) {
            $unitForecast =& $data['UN'];
        }

        if (isset($data['SHP'])) {
            $shipmentForecast =& $data['SHP'];
        }

        if (isset($data['ORD'])) {
            $orderForecast =& $data['ORD'];
        }

        if (isset($data['LOC'])) {
            $locationForecast =& $data['LOC'];
        }

        $dateArr = $this->getDateRangeTimestamp($firstData['fc_month'], $firstData['fc_year']);

        $inboundData = $this->goodReceiptModel->getForecastInboundActualData($dateArr, $firstData);
        $dailyInvRptData = $this->dailyInventoryModel->getForecastActualData($dateArr, $firstData);
        $outboundData = $this->odrHdrModel->getForecastOutboundData($dateArr, $firstData);
        $storageData = $this->dailyInventoryModel->getStorageData($firstData, $storageDate);
        $storageActPallet = $this->dailyInventoryModel->getForecastActualStorageLocation($firstData, $storageDate);
        $storageActLocation = $this->dailyInventoryModel->getLocationByDate($firstData, $storageDate);

        //Containers
        if (isset($containerForecast)) {
            if ($containerForecast['has_inbound']) {
                $containerForecast['fc_ib_act'] = $inboundData['in_container'];
                $containerForecast['fc_ib_var'] = $containerForecast['fc_ib_act'] - $containerForecast['fc_ib'];
            }
        }

        //Pallets
        if (isset($palletForecast)) {
            if ($palletForecast['has_inbound']) {
                $palletForecast['fc_ib_act'] = $inboundData['in_pallet'];
                $palletForecast['fc_ib_var'] = $palletForecast['fc_ib_act'] - $palletForecast['fc_ib'];
            }
            if ($palletForecast['has_outbound']) {
                $palletForecast['fc_ob_act'] = $outboundData['out_pallet'];
                $palletForecast['fc_ob_var'] = $palletForecast['fc_ob_act'] - $palletForecast['fc_ob'];
            }
            if ($palletForecast['has_storage']) {
                $palletForecast['fc_st_act'] = $storageActLocation;
                $palletForecast['fc_st_var'] = $palletForecast['fc_st_act'] - $palletForecast['fc_st'];
            }
        }

        //Cartons
        if (isset($cartonForecast)) {
            if ($cartonForecast['has_inbound']) {
                $cartonForecast['fc_ib_act'] = $inboundData['in_carton'];
                $cartonForecast['fc_ib_var'] = $cartonForecast['fc_ib_act'] - $cartonForecast['fc_ib'];
            }
            if ($cartonForecast['has_outbound']) {
                $cartonForecast['fc_ob_act'] = $outboundData['out_carton'];
                $cartonForecast['fc_ob_var'] = $cartonForecast['fc_ob_act'] - $cartonForecast['fc_ob'];
            }
            if ($cartonForecast['has_storage']) {
                $cartonForecast['fc_st_act'] = $storageData['cur_ctn_qty'];
                $cartonForecast['fc_st_var'] = $cartonForecast['fc_st_act'] - $cartonForecast['fc_st'];
            }
        }

        //Cube
        if (isset($cubeForecast)) {
            if ($cubeForecast['has_inbound']) {
                $cubeForecast['fc_ib_act'] = $inboundData['in_cube'];
                $cubeForecast['fc_ib_var'] = $cubeForecast['fc_ib_act'] - $cubeForecast['fc_ib'];
            }
            if ($cubeForecast['has_outbound']) {
                $cubeForecast['fc_ob_act'] = $outboundData['out_cube'];
                $cubeForecast['fc_ob_var'] = $cubeForecast['fc_ob_act'] - $cubeForecast['fc_ob'];
            }
            if ($cubeForecast['has_storage']) {
                $cubeForecast['fc_st_act'] = $storageData['cur_cube_qty'];
                $cubeForecast['fc_st_var'] = $cubeForecast['fc_st_act'] - $cubeForecast['fc_st'];
            }
        }

        //Unit
        if (isset($unitForecast)) {
            if ($unitForecast['has_inbound']) {
                $unitForecast['fc_ib_act'] = $inboundData['in_unit'];
                $unitForecast['fc_ib_var'] = $unitForecast['fc_ib_act'] - $unitForecast['fc_ib'];
            }
            if ($unitForecast['has_outbound']) {
                $unitForecast['fc_ob_act'] = $outboundData['out_unit'];
                $unitForecast['fc_ob_var'] = $unitForecast['fc_ob_act'] - $unitForecast['fc_ob'];
            }
            if ($unitForecast['has_storage']) {
                $unitForecast['fc_st_act'] = $storageData['cur_piece_qty'];
                $unitForecast['fc_st_var'] = $unitForecast['fc_st_act'] - $unitForecast['fc_st'];
            }
        }

        //Shipments
        if (isset($shipmentForecast)) {
            if ($shipmentForecast['has_outbound']) {
                $shipmentForecast['fc_ob_act'] = $outboundData['out_shipment'];
                $shipmentForecast['fc_ob_var'] = $shipmentForecast['fc_ob_act'] - $shipmentForecast['fc_ib'];
            }
        }

        //Orders
        if (isset($orderForecast)) {
            if ($orderForecast['has_outbound']) {
                $orderForecast['fc_ob_act'] = $outboundData['out_order'];
                $orderForecast['fc_ob_var'] = $orderForecast['fc_ob_act'] - $orderForecast['fc_ob'];
            }
        }

        //Location
        if (isset($locationForecast)) {
            if ($locationForecast['has_storage']) {
                $locationForecast['fc_st_act'] = $storageActLocation;
                $locationForecast['fc_st_var'] = $locationForecast['fc_st_act'] - $locationForecast['fc_st'];
            }
        }
    }

    private function calculateActualV1(&$data, $storageDate) {
        $firstData = $data->first();

        if (isset($data['CTNR'])) {
            $containerForecast =& $data['CTNR'];
        }

        if (isset($data['PL'])) {
            $palletForecast =& $data['PL'];
        }

        if (isset($data['CTN'])) {
            $cartonForecast =& $data['CTN'];
        }

        if (isset($data['CUB'])) {
            $cubeForecast =& $data['CUB'];
        }

        if (isset($data['UN'])) {
            $unitForecast =& $data['UN'];
        }

        if (isset($data['SHP'])) {
            $shipmentForecast =& $data['SHP'];
        }

        if (isset($data['ORD'])) {
            $orderForecast =& $data['ORD'];
        }

        if (isset($data['LOC'])) {
            $locationForecast =& $data['LOC'];
        }

        $dateArr = $this->getDateRangeTimestamp($firstData['fc_month'], $firstData['fc_year']);

        $dailyInvRptData = $this->dailyInventoryModel->getForecastActualData($dateArr, $firstData);
        $orderData = $this->odrHdrModel->getForecastActualData($dateArr, $firstData);
        $storageData = $this->dailyInventoryModel->getStorageData($firstData, $storageDate);
        $storageActLocation = $this->dailyInventoryModel->getForecastActualStorageLocation($firstData, $storageDate);
        $locationStorageAct = $this->dailyInventoryModel->getLocationByDate($firstData, $storageDate);

        //Containers
        if (isset($containerForecast)) {
            if ($containerForecast['has_inbound']) {
                $containerForecast['fc_ib_act'] = $this->goodReceiptModel->getContainerQtyByDateRange($containerForecast, $dateArr);
                $containerForecast['fc_ib_var'] = $containerForecast['fc_ib_act'] - $containerForecast['fc_ib'];
            }
        }

        //Pallets
        if (isset($palletForecast)) {
            if ($palletForecast['has_inbound']) {
                $palletForecast['fc_ib_act'] = $dailyInvRptData['in_plt_qty'];
                $palletForecast['fc_ib_var'] = $palletForecast['fc_ib_act'] - $palletForecast['fc_ib'];
            }
            if ($palletForecast['has_outbound']) {
                $palletForecast['fc_ob_act'] = $dailyInvRptData['out_plt_qty'];
                $palletForecast['fc_ob_var'] = $palletForecast['fc_ob_act'] - $palletForecast['fc_ob'];
            }
            if ($palletForecast['has_storage']) {
                $palletForecast['fc_st_act'] = $storageActLocation;
                $palletForecast['fc_st_var'] = $palletForecast['fc_st_act'] - $palletForecast['fc_st'];
            }
        }

        //Cartons
        if (isset($cartonForecast)) {
            if ($cartonForecast['has_inbound']) {
                $cartonForecast['fc_ib_act'] = $dailyInvRptData['in_ctn_qty'];
                $cartonForecast['fc_ib_var'] = $cartonForecast['fc_ib_act'] - $cartonForecast['fc_ib'];
            }
            if ($cartonForecast['has_outbound']) {
                $cartonForecast['fc_ob_act'] = $dailyInvRptData['out_ctn_qty'];
                $cartonForecast['fc_ob_var'] = $cartonForecast['fc_ob_act'] - $cartonForecast['fc_ob'];
            }
            if ($cartonForecast['has_storage']) {
                $cartonForecast['fc_st_act'] = $storageData['cur_ctn_qty'];
                $cartonForecast['fc_st_var'] = $cartonForecast['fc_st_act'] - $cartonForecast['fc_st'];
            }
        }

        //Cube
        if (isset($cubeForecast)) {
            if ($cubeForecast['has_inbound']) {
                $cubeForecast['fc_ib_act'] = $dailyInvRptData['in_cube_qty'];
                $cubeForecast['fc_ib_var'] = $cubeForecast['fc_ib_act'] - $cubeForecast['fc_ib'];
            }
            if ($cubeForecast['has_outbound']) {
                $cubeForecast['fc_ob_act'] = $dailyInvRptData['out_cube_qty'];
                $cubeForecast['fc_ob_var'] = $cubeForecast['fc_ob_act'] - $cubeForecast['fc_ob'];
            }
            if ($cubeForecast['has_storage']) {
                $cubeForecast['fc_st_act'] = $storageData['cur_cube_qty'];
                $cubeForecast['fc_st_var'] = $cubeForecast['fc_st_act'] - $cubeForecast['fc_st'];
            }
        }

        //Unit
        if (isset($unitForecast)) {
            if ($unitForecast['has_inbound']) {
                $unitForecast['fc_ib_act'] = $dailyInvRptData['in_piece_qty'];
                $unitForecast['fc_ib_var'] = $unitForecast['fc_ib_act'] - $unitForecast['fc_ib'];
            }
            if ($unitForecast['has_outbound']) {
                $unitForecast['fc_ob_act'] = $dailyInvRptData['out_piece_qty'];
                $unitForecast['fc_ob_var'] = $unitForecast['fc_ob_act'] - $unitForecast['fc_ob'];
            }
            if ($unitForecast['has_storage']) {
                $unitForecast['fc_st_act'] = $storageData['cur_piece_qty'];
                $unitForecast['fc_st_var'] = $unitForecast['fc_st_act'] - $unitForecast['fc_st'];
            }
        }

        //Shipments
        if (isset($shipmentForecast)) {
            if ($shipmentForecast['has_outbound']) {
                $shipmentForecast['fc_ob_act'] = $orderData['shipments'];
                $shipmentForecast['fc_ob_var'] = $shipmentForecast['fc_ob_act'] - $shipmentForecast['fc_ib'];
            }
        }

        //Orders
        if (isset($orderForecast)) {
            if ($orderForecast['has_outbound']) {
                $orderForecast['fc_ob_act'] = $orderData['orders'];
                $orderForecast['fc_ob_var'] = $orderForecast['fc_ob_act'] - $orderForecast['fc_ob'];
            }
        }

        //Location
        if (isset($locationForecast)) {
            if ($locationForecast['has_storage']) {
                $locationForecast['fc_st_act'] = $locationStorageAct;
                $locationForecast['fc_st_var'] = $locationForecast['fc_st_act'] - $locationForecast['fc_st'];
            }
        }
    }

    private function getPreviousMonthsStorageData(&$data, $storageDate){
        $forecastData = $data->first();

        if (isset($data['CTNR'])) {
            $containerForecast =& $data['CTNR'];
        }

        if (isset($data['PL'])) {
            $palletForecast =& $data['PL'];
        }

        if (isset($data['CTN'])) {
            $cartonForecast =& $data['CTN'];
        }

        if (isset($data['CUB'])) {
            $cubeForecast =& $data['CUB'];
        }

        if (isset($data['UN'])) {
            $unitForecast =& $data['UN'];
        }

        if (isset($data['SHP'])) {
            $shipmentForecast =& $data['SHP'];
        }

        if (isset($data['ORD'])) {
            $orderForecast =& $data['ORD'];
        }

        if (isset($data['LOC'])) {
            $locationForecast =& $data['LOC'];
        }

        for ($i = 3; $i >0; $i--) {
            $previousMonthData = $this->forecastModel->getPreviousMonthsStorageData($forecastData, $storageDate, $i);

            if (!$previousMonthData->isEmpty()) {
                if (isset($containerForecast)) {
                    $containerForecast['fc_st_' . $i . '_period_ago'] = $previousMonthData[$containerForecast['fc_ind_id']]['fc_st_act'] ?? 0;
                }

                if (isset($palletForecast)) {
                    $palletForecast['fc_st_' . $i . '_period_ago'] = $previousMonthData[$palletForecast['fc_ind_id']]['fc_st_act'] ?? 0;
                }

                if (isset($cartonForecast)) {
                    $cartonForecast['fc_st_' . $i . '_period_ago'] = $previousMonthData[$cartonForecast['fc_ind_id']]['fc_st_act'] ?? 0;
                }

                if (isset($cubeForecast)) {
                    $cubeForecast['fc_st_' . $i . '_period_ago'] = $previousMonthData[$cubeForecast['fc_ind_id']]['fc_st_act'] ?? 0;
                }

                if (isset($unitForecast)) {
                    $unitForecast['fc_st_' . $i . '_period_ago'] = $previousMonthData[$unitForecast['fc_ind_id']]['fc_st_act'] ?? 0;
                }

                if (isset($shipmentForecast)) {
                    $shipmentForecast['fc_st_' . $i . '_period_ago'] = $previousMonthData[$shipmentForecast['fc_ind_id']]['fc_st_act'] ?? 0;
                }

                if (isset($orderForecast)) {
                    $orderForecast['fc_st_' . $i . '_period_ago'] = $previousMonthData[$orderForecast['fc_ind_id']]['fc_st_act'] ?? 0;
                }

                if (isset($locationForecast)) {
                    $locationForecast['fc_st_' . $i . '_period_ago'] = $previousMonthData[$locationForecast['fc_ind_id']]['fc_st_act'] ?? 0;
                }
            }
        }
    }

    private function getDateRangeTimestamp($month, $year) {
        $lastDateOfMonth = date("t", strtotime("{$year}-{$month}"));
        $data = [
            'fromDate'  => strtotime("{$year}-{$month}-01"),
            'toDate'    => strtotime("{$year}-{$month}-{$lastDateOfMonth} 23:59:59"),
        ];

        return $data;
    }
}