<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\MasterDataType;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class MasterDataTypeModel extends AbstractModel
{
    /**
     * MasterDataModel constructor.
     *
     * @param MasterData|null $model
     */
    public function __construct(MasterDataType $model = null)
    {
        $this->model = ($model) ?: new MasterDataType();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['md_type'])) {
            $query->where('md_type', 'like', "%" . SelStr::escapeLike($attributes['md_type']) . "%");
        }

        if (isset($attributes['mdt_name'])) {
            $query->where('mdt_name', 'like', "%" . SelStr::escapeLike($attributes['mdt_name']) . "%");
        }

        if (isset($attributes['mdt_des'])) {
            $query->where('mdt_des', 'like', "%" . SelStr::escapeLike($attributes['mdt_des']) . "%");
        }

        if (isset($attributes['md_sts'])) {
            $query->where('md_sts', $attributes['md_sts']);
        }

        $query->orderBy('updated_at', 'DESC');

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'updated_at') {
                    $attributes['sort']['updated_at'] = $val;
                    unset($attributes['sort'][$key]);
                }
            }
        }

        // Get
        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     *
     * @return mixed
     */
    public function masterDataTypes()
    {
        return $this->model
                    ->select('md_type', 'mdt_name')
                    ->groupBy('md_type')
                    ->pluck('mdt_name', 'md_type')->toArray();
    }

    /**
     * Create an existing model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function createMasterDataType(array $data)
    {
        return DB::table('master_data_type')->insert($data);
    }

    /**
     * Update an existing model.
     *
     * @param $mdType
     * @param $mdCode
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function updateMasterDataType($mdType, array $data)
    {
        return DB::table('master_data_type')
                    ->where('md_type', $mdType)
                    ->update($data);
    }

    /**
     * @param $mdType
     *
     * @return mixed
     */
    public function byType($mdType)
    {
        return $this->model
                    ->where('md_type', $mdType)
                    ->first();
    }
}
