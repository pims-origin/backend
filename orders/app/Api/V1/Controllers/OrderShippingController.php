<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\ReportModel;
use App\Api\V1\Models\ReportService;
use App\Api\V1\Models\ShipmentModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\UserMetaModel;
use App\Api\V1\Models\ShippingReportModel;
use App\Api\V1\Models\SKUTrackingReportModel;
use App\Api\V1\Traits\OrderFlowControllerTrait;
use App\Api\V1\Transformers\OrderShipTransformer;
use App\Api\V1\Transformers\OrderShippingTransformer;
use App\Api\V1\Validators\OrderShippingValidator;
use App\Jobs\SyncAsnJob;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;
use mPDF;
use GuzzleHttp\Client;


/**
 * Class OrderShippingController
 *
 * @package App\Api\V1\Controllers
 */
class OrderShippingController extends AbstractController
{
    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var ShippingOrderModel
     */
    protected $orderShippingModel;

    /**
     * @var InventorySummary
     */
    protected $invtSmrModel;

    /**
     * @var ShipMent
     */
    protected $shpMntModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var ReportModel
     */
    protected $reportModel;

    /**
     * @var ReportService
     */
    protected $reportService;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    /**
     * @var Carton Model
     */
    protected $cartonModel;

    protected $userMetaModel;

    protected $shippingReportModel;

    protected $SKUTrackingReportModel;

    use OrderFlowControllerTrait;

    /**
     * OrderShippingController constructor.
     *
     * @param PackHdrModel $packHdrModel
     * @param PackDtlModel $packDtlModel
     * @param ShippingOrderModel $orderShippingModel
     * @param OrderHdrModel $orderHdrModel
     * @param InventorySummaryModel $invtSmrModel
     * @param OrderDtlModel $orderDtlModel
     * @param ShipmentModel $shpMntModel
     * @param EventTrackingModel $eventTrackingModel
     * @param ReportModel $reportModel
     * @param ReportService $reportService
     * @param CustomerConfigModel $customerConfigModel
     */
    public function __construct(
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel,
        ShippingOrderModel $orderShippingModel,
        OrderHdrModel $orderHdrModel,
        InventorySummaryModel $invtSmrModel,
        OrderDtlModel $orderDtlModel,
        ShipmentModel $shpMntModel,
        EventTrackingModel $eventTrackingModel,
        ReportModel $reportModel,
        ReportService $reportService,
        CustomerConfigModel $customerConfigModel,
        ShippingReportModel $shippingReportModel,
        SKUTrackingReportModel $SKUTrackingReportModel
    ) {
        $this->packHdrModel = $packHdrModel;
        $this->packDtlModel = $packDtlModel;
        $this->orderShippingModel = $orderShippingModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->invtSmrModel = $invtSmrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->shpMntModel = $shpMntModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->reportModel = $reportModel;
        $this->reportService = $reportService;
        $this->customerConfigModel = $customerConfigModel;
        $this->orderCartonModel = new OrderCartonModel();
        $this->cartonModel = new CartonModel();
        $this->userMetaModel = new UserMetaModel();
        $this->shippingReportModel = $shippingReportModel;
        $this->SKUTrackingReportModel = $SKUTrackingReportModel;
    }


    /**
     * @param $orderShippingId
     * @param OrderShippingTransformer $orderShippingTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($orderShippingId, OrderShippingTransformer $orderShippingTransformer)
    {
        try {


            $orderShipping = $this->orderShippingModel->getFirstBy('ord_shipping_id', $orderShippingId);

            return $this->response->item($orderShipping, $orderShippingTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param OrderShipTransformer $orderShipTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request, OrderShipTransformer $orderShipTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $inputStatuses = array_get($input, 'odr_sts', []);
        $limit = array_get($input, 'limit', 20);
        $type = array_get($input, 'type', 'shipping');

        try {
            //Export CSV
            if (!empty($input['export']) && $input['export'] == 1) {
                $this->exportGRCSV($input);
                die;
            }

            $userId = Data::getCurrentUserId();
            $this->userMetaModel->modifyUserMeta(['user_id' => $userId, 'qualifier' => config('constants.qualifier.ORDER-STATUS')], $inputStatuses, $type);

            $orders = $this->orderHdrModel->search($input, [], $limit);

            $arrShipIds = array_pluck($orders, 'ship_id');
            $arrShipIds = array_filter($arrShipIds);
            if ($arrShipIds) {
                $shipMents = $this->orderShippingModel->getShipStsByShipIds($arrShipIds);
                if ($shipMents) {
                    $arrShipSts = [];
                    foreach ($shipMents as $shipMent) {
                        $arrShipSts[$shipMent->ship_id] = [
                            'ship_sts' => $shipMent->ship_sts,
                            'bo_num'   => $shipMent->bo_num
                        ];
                    }

                    foreach ($orders as $order) {
                        if ($order->ship_id
                            && !empty($arrShipSts[$order->ship_id])
                        ) {
                            $order->ship_sts = array_get($arrShipSts[$order->ship_id], 'ship_sts', null);
                            $order->bo_num = array_get($arrShipSts[$order->ship_id], 'bo_num', null);
                        }
                    }
                }
            }

            return $this->response->paginator($orders, $orderShipTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $input
     *
     * @return $this|void
     */
    private function exportGRCSV($input)
    {
        try {
            $orders = $this->orderHdrModel->search($input, [

            ], array_get($input, 'limit'), true);

            $arrShipIds = array_pluck($orders, 'ship_id');
            $arrShipIds = array_filter($arrShipIds);
            if ($arrShipIds) {
                $shipMents = $this->orderShippingModel->getShipStsByShipIds($arrShipIds);
                if ($shipMents) {
                    $arrShipSts = [];
                    foreach ($shipMents as $shipMent) {
                        $arrShipSts[$shipMent->ship_id] = [
                            'ship_sts' => $shipMent->ship_sts,
                            'bo_num'   => $shipMent->bo_num
                        ];
                    }

                    foreach ($orders as $order) {
                        if ($order->ship_id
                            && !empty($arrShipSts[$order->ship_id])
                        ) {
                            $order->ship_sts = array_get($arrShipSts[$order->ship_id], 'ship_sts', null);
                            $order->bo_num = array_get($arrShipSts[$order->ship_id], 'bo_num', null);
                        }
                    }
                }
            }

            $orderinfos = [];
            if ($orders) {
                foreach ($orders as $orderHd) {
                    $partial = $orderHd->back_odr == 1 ? "Partial " : "";
                    $createdAt = date("m/d/Y", strtotime($orderHd->created_at));

                    $orderinfos[] = [
                        //'odr_sts_name'  => $statusName,
                        'odr_sts_name'  => $partial . Status::getByKey("Order-Status", $orderHd->odr_sts),
                        'odr_num'       => object_get($orderHd, 'odr_num', null),
                        'cus_name'      => object_get($orderHd, 'customer.cus_code', null),
                        'odr_type_name' => Status::getByKey("Order-Type", $orderHd->odr_type),
                        'back_odr'      => object_get($orderHd, 'back_odr', null) ? 'YES' : "NO",
                        'bol'           => Status::getByKey("SHIP-STATUS", $orderHd->ship_sts)
                            ? Status::getByKey("SHIP-STATUS", $orderHd->ship_sts)
                            : 'No Bol',
                        'cus_odr_num'   => object_get($orderHd, 'cus_odr_num', null),
                        'cus_po'        => object_get($orderHd, 'cus_po', null),
                        'ship_to_name'  => $orderHd->ship_to_name,
                        'ship_by_dt'    => $this->dateFormat($orderHd->ship_by_dt),
                        'cancel_by_dt'  => $this->dateFormat($orderHd->cancel_by_dt),
                        // 'act_cancel_dt' => empty($orderHd->act_cancel_dt) ? null :
                            // date("m/d/Y", $orderHd->act_cancel_dt),
                        'shipped_dt'    => empty($orderHd->shipped_dt) ? null : $this->dateFormat($orderHd->shipped_dt),
                        'csr_name'      => trim(object_get($orderHd, 'csrUser.first_name', null) . " " .
                            object_get($orderHd, 'csrUser.last_name', null)),

                        'created_by' => trim(object_get($orderHd, "createdBy.first_name", null) . " " .
                            object_get($orderHd, "createdBy.last_name", null)),
                        'created_at' => $createdAt

                    ];
                }
            }

            $title = [
                'odr_sts_name'  => 'Status',
                'odr_num'       => 'Order Number',
                'cus_name'      => 'Customer',
                'odr_type_name' => 'Order Type',
                'back_odr'      => 'Back Order',
                'bol'           => 'BOL',
                'cus_odr_num'   => 'Customer Order',
                'cus_po'        => 'PO',
                'ship_to_name'  => 'Shipping To',
                'ship_by_dt'    => 'Ship By Date',
                'cancel_by_dt'  => 'Cancel By Date',
                // 'act_cancel_dt' => 'Actual Cancel Date',
                'shipped_dt'    => 'Shipped Date',
                'csr_name'      => 'CSR',
                'created_by'    => 'User',
                'created_at'    => 'Created Date',
            ];

            // $filePath = storage_path() . "/Report_PBPM_{$warehouse->whs_name}.csv";

            $today = time();
            $filePath = "Report_Order_Shipping_{$today}";
            $this->saveFile($title, $orderinfos, $filePath);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param int $date
     *
     * @return false|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }

    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->openToBrowser($filePath); // stream data directly to the browser

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();
    }

    /**
     * @param Request $request
     * @param OrderShippingValidator $orderShippingValidator
     * @param OrderShippingTransformer $orderShippingTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(
        Request $request,
        OrderShippingValidator $orderShippingValidator,
        OrderShippingTransformer $orderShippingTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $orderShippingValidator->validate($input);

        //value to save
        $params = [
            'ship_to_cus_name' => array_get($input, 'ship_to_cus_name', null),
            'ship_to_addr'     => array_get($input, 'ship_to_addr', null),
            'ship_to_city'     => array_get($input, 'ship_to_city', null),
            'ship_to_state'    => array_get($input, 'ship_to_state', null),
            'ship_to_zip'      => array_get($input, 'ship_to_zip', null),
            'ship_to_country'  => array_get($input, 'ship_to_country', null),
            'ship_by_dt'       => array_get($input, 'ship_by_dt', null),
            'ship_dt'          => array_get($input, 'ship_dt', null),
        ];

        try {
            if (($orderShipping = $this->orderShippingModel->create($params))) {
                return $this->response
                    ->item($orderShipping, $orderShippingTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $orderShippingId
     * @param Request $request * @param OrderShippingValidator $orderShippingValidator
     * @param OrderShippingTransformer $orderShippingTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(
        $orderShippingId,
        Request $request,
        OrderShippingValidator $orderShippingValidator,
        OrderShippingTransformer $orderShippingTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $orderShippingValidator->validate($input);

        $params = [
            'ord_shipping_id'  => $orderShippingId,
            'ship_to_cus_name' => array_get($input, 'ship_to_cus_name', null),
            'ship_to_addr'     => array_get($input, 'ship_to_addr', null),
            'ship_to_city'     => array_get($input, 'ship_to_city', null),
            'ship_to_state'    => array_get($input, 'ship_to_state', null),
            'ship_to_zip'      => array_get($input, 'ship_to_zip', null),
            'ship_to_country'  => array_get($input, 'ship_to_country', null),
            'ship_by_dt'       => array_get($input, 'ship_by_dt', null),
            'ship_dt'          => array_get($input, 'ship_dt', null),
        ];

        try {
            // Check Exist
            if (!$this->orderShippingModel->checkWhere(['ord_shipping_id' => $orderShippingId])) {
                throw new \Exception(Message::get("BM017", "Order Shipping"));
            }

            if ($orderShipping = $this->orderShippingModel->update($params)) {
                return $this->response->item($orderShipping, $orderShippingTransformer);
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $orderShippingId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function destroy($orderShippingId)
    {
        try {
            // Check Exist
            if (!$this->orderShippingModel->checkWhere(['ord_shipping_id' => $orderShippingId])) {
                return $this->response->errorBadRequest(Message::get("BM017", "Order Shipping"));
            }

            // Check Exist in Order
            if ($this->orderHdrModel->checkWhere(['ord_shipping_id' => $orderShippingId])) {
                return $this->response->errorBadRequest(Message::get("BM003", "Order Shipping"));
            }

            if ($this->orderShippingModel->deleteOrderShipping($orderShippingId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     *
     * @return array|void
     * @throws \Exception
     */
    public function ship(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $shippedDate = array_get($input, 'shipped_dt', date('m/d/Y'));

        $orderHdr = $this->orderHdrModel->getFirstWhere(['odr_id' => $input['order_id']]);

        if (empty($orderHdr)) {
            return $this->response->errorBadRequest(Message::get("BM017", "Order"));
        }

//        if ( !$this->checkValidDateShipped($orderHdr, $shippedDate) ){
//            throw new \Exception('The shipped date must be in last month or sooner. Please contact to admin.');
//        }

        //$orderHdr = $orderHdr->toArray();

        //// Update Shipped Date
        //$this->orderHdrModel->update(['odr_id' => $orderHdr->odr_id, 'shipped_dt' => strtotime($input['shipped_dt'])]);

        $response = $this->processShip($input['order_id'], strtotime($input['shipped_dt'] . ' UTC'), $request);
        $client = new Client();
        $header = [
            'Authorization' => $request->getHeader('Authorization')
        ];


        // sync asn to transfer warehouse
        if ($orderHdr->odr_type == 'WHS') {
            if (is_int($orderHdr->whs_id_to) === false) {
                throw new \Exception("queue-whs field is required.");
            }
            dispatch(new SyncAsnJob($orderHdr, $orderHdr->whs_id_to, $request));
        }

        return $response;
    }

    /**
     * @param $orderId
     * @param Request $request
     *
     * @return bool
     * @throws \Exception
     */
    public function printPalletShipping(
        $orderId,
        Request $request
    ) {
        if (empty($orderId)) {
            return false;
        }
        $packInfo = $this->packDtlModel->checkWhere(
            [
                'odr_hdr_id' => $orderId

            ]);
        if (empty($packInfo)) {
            throw new \Exception('This order do not have pack!');
        }

        $orderHdrs = $this->orderHdrModel->findOrderShippingAccordingToPallet(
            $orderId
        );

        $ctn_Total = 0;
        $plt_Total = count($orderHdrs);

        if (empty($orderHdrs)) {
            throw new \Exception(Message::get("BM017", "Orders"));
        }

        // Check order status just staging or partial staging
        $arrOdrStaus = [
            Status::getByValue("Palletized", "Order-Status"),
            Status::getByValue("Staging", "Order-Status"),
            Status::getByValue("Partial Staging", "Order-Status"),
            Status::getByValue("Shipped", "Order-Status"),
            Status::getByValue("Partial Shipped", "Order-Status"),
            Status::getByValue("Ready to Ship", "Order-Status"),
            Status::getByValue("Scheduled to Ship", "Order-Status"),
        ];

        foreach ($orderHdrs as $odrHdr) {
            $ctn_Total = $ctn_Total + array_get($odrHdr, 'pallet_num', 0);
            if (!in_array($odrHdr['odr_sts'], $arrOdrStaus)) {
                throw new \Exception('Only from Palletized to shipped orders can be print Shipping Label!');
            }

        }

        $this->createPalletShippingPdfFile($orderHdrs, $plt_Total, $ctn_Total);

    }

    /**
     * @param $orderHdrs
     * @param $plt_Total
     * @param $ctn_Total
     */
    private function createPalletShippingPdfFile($orderHdrs, $plt_Total, $ctn_Total)
    {
        $completedAt = date("m/d/Y H:i:s");
        //$pdf = new Mpdf(
        //    '',    // mode - default ''
        //    [101.6, 177.79999999999998],    // format - A4, for example, default '' (W=4 x H=7 inch)(4 inch = 101.6 mm)
        //    0,     // font size - default 0
        //    '',    // default font family
        //    5,    // margin_left
        //    5,    // margin right
        //    6,     // margin top
        //    6,    // margin bottom
        //    1,     // margin header
        //    1,     // margin footer
        //    'p' // L - landscape, P - portrait
        //);
        $pdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',    // mode - default ''
            'format' => [101.6, 177.79999999999998],    // format - A4, for example, default '' (W=4 x H=7 inch)(4 inch = 101.6 mm)
            'margin_left' => 5,    // margin_left
            'margin_right' => 5,    // margin right
            'margin_top' => 6,     // margin top
            'margin_bottom' => 6,    // margin bottom
            'margin_header' => 1,     // margin header
            'margin_footer' => 1,     // margin footer
            'orientation' => 'P'// L - landscape, P - portrait
        ]);
        $html = (string)view('PalletShippingPrintoutTemplate', [
            'orderHdrs' => $orderHdrs,
            'plt_Total' => $plt_Total,
            'ctn_Total' => $ctn_Total,
            'completed_at' => $completedAt,
        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        //$pdf->Output("$odrNum.pdf", "D");
        $pdf->Output();
    }

    /**
     * @param $orderId
     * @param Request $request
     *
     * @return bool
     * @throws \Exception
     */
    public function printCartonShipping(
        $orderId,
        Request $request
    ) {
        if (empty($orderId)) {
            return false;
        }
        $packInfo = $this->packDtlModel->checkWhere(
            [
                'odr_hdr_id' => $orderId

            ]);
        if (empty($packInfo)) {
            throw new \Exception(Message::get('BM131', 'This order', 'pack'));
        }

        $orderHdrs = $this->orderHdrModel->findOrderShippingAccordingToCarton(
            $orderId
        );

        $ctn_Total = count($orderHdrs);

        if (empty($orderHdrs)) {
            throw new \Exception(Message::get("BM017", "Orders"));
        }

        // Check order status just staging or partial staging
        $arrOdrStaus = [
            Status::getByValue("Palletized", "Order-Status"),
            Status::getByValue("Staging", "Order-Status"),
            Status::getByValue("Partial Staging", "Order-Status"),
            Status::getByValue("Shipped", "Order-Status"),
            Status::getByValue("Partial Shipped", "Order-Status"),
            Status::getByValue("Ready to Ship", "Order-Status"),
            Status::getByValue("Scheduled to Ship", "Order-Status"),
        ];

        foreach ($orderHdrs as $odrHdr) {
            if (!in_array($odrHdr['odr_sts'], $arrOdrStaus)) {
                throw new \Exception('Only from Palletized to shipped orders can be print Shipping Label!');
            }

        }

        $this->createCartonShippingPdfFile($orderHdrs, $ctn_Total);

    }

    /**
     * @param $orderHdrs
     * @param $ctn_Total
     */
    private function createCartonShippingPdfFile($orderHdrs, $ctn_Total)
    {
        //$pdf = new Mpdf(
        //    '',    // mode - default ''
        //    [101.6, 177.79999999999998],    // format - A4, for example, default '' (W=4 x H=7 inch)(4 inch = 101.6 mm)
        //    0,     // font size - default 0
        //    '',    // default font family
        //    5,    // margin_left
        //    5,    // margin right
        //    6,     // margin top
        //    6,    // margin bottom
        //    1,     // margin header
        //    1,     // margin footer
        //    'p' // L - landscape, P - portrait
        //);
        $pdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',    // mode - default ''
            'format' => [101.6, 177.79999999999998],    // format - A4, for example, default '' (W=4 x H=7 inch)(4 inch = 101.6 mm)
            'margin_left' => 5,    // margin_left
            'margin_right' => 5,    // margin right
            'margin_top' => 6,     // margin top
            'margin_bottom' => 6,    // margin bottom
            'margin_header' => 1,     // margin header
            'margin_footer' => 1,     // margin footer
            'orientation' => 'P'// L - landscape, P - portrait
        ]);

        $html = (string)view('CartonShippingPrintoutTemplate', [
            'orderHdrs' => $orderHdrs,
            'ctn_Total' => $ctn_Total,
        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        //$pdf->Output("$odrNum.pdf", "D");
        $pdf->Output();
    }

    public function checkValidDateShipped($orderHdr, $shippedDate)
    {
        $shippedDate = new Carbon($shippedDate);
        // $orderDate = $orderHdr->created_at;
        $currentDate = new Carbon();
        if ($shippedDate->month === $currentDate->month){
            return true;
        }

        if (($currentDate->month - $shippedDate->month) == 1 && $currentDate->day < 8) {
            return true;
        }


        // if ($shippedDate->diffInDays($currentDate) <= 7){
        //     return true;
        // }

        return false;
    }

}
