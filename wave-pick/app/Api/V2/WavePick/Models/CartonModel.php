<?php

namespace App\Api\V2\WavePick\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class CartonModel extends AbstractModel
{

    protected $model;

    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    public function getSuggestionLocByCarton($whsId, $itemId, $lot, $algorithm, $limit = 10)
    {
        $query = DB::table('cartons')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->select([
                'cartons.loc_id',
                'cartons.loc_code',
                DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS ctns')
            ])
            ->where('location.loc_sts_code', 'AC')
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId);

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                $query->orderBy('cartons.gr_dt', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        }

//        $query->orderBy(DB::Raw("cartons.loc_code"), 'ASC');

        return $query->limit($limit)->skip(0)->get();
    }

    /**
     * @param $locIds
     * @param $data
     * @param int $take
     *
     * @return mixed
     */
    public function getMoreSugLocByWvDtl($locIds, $data, $take = 1)
    {
        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();
        // Loc ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];
        $itemId = (int)$data['item_id'];
        $is_ecom = (int)$data['is_ecom'];


        $cus_id = (new Item())->select('cus_id')->where('item_id', $itemId)->value('cus_id');
        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cus_id);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons')
            ->select([
                'loc_id',
                'loc_code',
                DB::raw('sum(piece_remain) as avail_qty')
            ])
            ->where('is_damaged', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('item_id', (int)$itemId)
            ->where('is_ecom', $is_ecom)
            ->where('lot', $data['lot'])
            ->where('deleted', 0)
            ->whereNotIn('loc_id', $locIds)
            ->groupBy('loc_id');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderByRaw('expired_dt > 0 DESC, created_at ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }
        $query->orderBy('cartons.piece_remain', 'DESC');
        if ($take == 1) {
            return $query->first();
        } else {
            return $query->limit($take)->skip(0)->get();

        }
    }

    /**
     * @param $locIds
     * @param $itemIds
     *
     * @return mixed
     */
    public function getCartonForWavePick($locIds, $itemIds)
    {
        // Wv ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];
        // Item ids
        $itemIds = is_array($itemIds) ? $itemIds : [$itemIds];

        $query = DB::table('cartons as C')
            ->join('location as L', 'L.loc_id', '=', 'C.loc_id')
            ->join('pallet as P', 'P.plt_id', '=', 'C.plt_id')
            ->select([
                'C.item_id',
                'C.lot',
                'L.loc_code',
                DB::raw('COUNT(C.ctn_id) as numCarton'),
                DB::raw('SUM(C.piece_remain) as pieceRemain'),
                'P.plt_num'
            ])
            ->whereIn('C.loc_id', $locIds)
            ->whereIn('C.item_id', $itemIds)
            ->where([
                'C.deleted' => 0,
                'P.deleted' => 0,
                'L.deleted' => 0,
            ])
            ->groupBy('L.loc_id', 'C.item_id', 'C.lot')
            ->orderBy('C.created_at');

        return $query->get();
    }


}
