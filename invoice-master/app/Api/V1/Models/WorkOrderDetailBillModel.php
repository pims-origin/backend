<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\BillableHdrModel;
use App\Api\V1\Models\BillableDtlModel;
use App\VasHdr;
use App\VasDtl;
use App\Api\V1\Models\CustomerSacModel;
use Illuminate\Support\Facades\DB;

class WorkOrderDetailBillModel extends AbstractModel
{
    protected $cusId;
    protected $whsId;
    protected $type;
    protected $quantity;
    protected $amount;
    protected $billableHdr;
    protected $billableDtl;
    protected $vasHdr;
    protected $vasDtl;
    protected $cusSacId;

    public function __construct(VasHdr $vasHdr, VasDtl $vasDtl)
    {
        $this->cusId = $vasDtl->cus_id;
        $this->whsId = $vasDtl->whs_id;
        $this->refNum = $vasDtl->ref_num;
        $this->type = 'WO'; // Work Order
        $this->vasDtl = $vasDtl;
        $this->vasHdr = $vasHdr;
        $this->cusSacId = $vasDtl->cus_sac_id;
        $this->billableHdr = new BillableHdrModel();
        $this->billableDtl = new BillableDtlModel();
    }

    public function prepareDetailData()
    {
        $cusSac = DB::table('cus_sac')->where('cus_sac_id', $this->cusSacId)->where('deleted', 0)->first();
        $detail[1] = [
            'ba_id' => '',
            'cus_id' => $this->cusId,
            'whs_id' => $this->whsId,
            'ba_type' => $this->type,
            'sac_id' => object_get($cusSac, 'sac_id', 0),
            'sac' => object_get($cusSac, 'sac', ''),
            'sac_name' => object_get($cusSac, 'sac_name', ''),
            'ref_num' => $this->refNum,
            'item' => $this->vasDtl->vas_item,
            'description' => $this->vasDtl->vas_description,
            'qty' => $this->vasDtl->qty,
            'unit_price' => $this->vasDtl->unit_price,
            'discount' => $this->vasDtl->discount,
            'amount' => $this->vasDtl->amount
        ];
        $this->quantity += $detail[1]['qty'];
        $this->amount += $detail[1]['amount'];

        return $detail;
    }

    public function prepareHeaderData()
    {
        # billable header
        return [
            'cus_id' => $this->cusId,
            'whs_id' => $this->whsId,
            'ref_num' => $this->vasHdr->vas_num, // vas_num
            'ba_from' => time(),
            'ba_to' => time(),
            'ba_ttl' => $this->quantity,
            'amount_ttl' => $this->amount,
            'ba_type' => $this->type,
            'ba_period' => 'DAY',
        ];
    }

    public function saveBillableItem()
    {
        $detail = $this->prepareDetailData();
        if (!empty($detail)) {
            $headerData = $this->prepareHeaderData();
            return DB::transaction(function () use ($detail, $headerData) {
                $header = $this->billableHdr->create($headerData);
                $header->detail()->createMany($detail);
                return $header;
            });
        }
    }
}
