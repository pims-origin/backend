<?php

namespace App\Api\V2\Inbound\Carton\SetDamageCarton\Controllers;

use App\Api\V2\Inbound\Carton\SetDamageCarton\Models\VirtualCartonModel;
use App\Api\V2\Inbound\Carton\SetDamageCarton\Models\CartonModel;
use App\Api\V2\Inbound\Carton\SetDamageCarton\Models\GoodsReceiptDetailModel;
use App\Api\V2\Inbound\Carton\SetDamageCarton\Models\PalletModel;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Models\CustomerWarehouse;
use App\Api\V1\Models\Log;

class VirtualCartonController extends AbstractController
{

    private $vtlCtn;
    private $_errorMsgs;
    private $_wrongCtnRfids;
    private $_CtnRfidsScannedPallet;
    private $carton;
    private $grDtlModel;
    private $pltModel;

    /**
     * VirtualCartonController constructor.
     *
     * @param VirtualCartonModel $vtlCtn
     */
    public function __construct(VirtualCartonModel $vtlCtn, CartonModel $carton, GoodsReceiptDetailModel $grDtl, PalletModel $plt)
    {
        $this->vtlCtn = $vtlCtn;
        $this->_errorMsgs = [];
        $this->_wrongCtnRfids = [];
        $this->_CtnRfidsScannedPallet = [];
        $this->carton = $carton;
        $this->grDtlModel = $grDtl;
        $this->pltModel = $plt;
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function setDamageCarton($whsId, $cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $owner = $transaction = "";
        $urlEndpoint = "/v2/whs/{$whsId}/cus/{$cusId}/carton/set-damage-carton";
        $apiName = 'Set damage Carton';
        Log:: info($request, $whsId, [
            'evt_code'     => 'UPD',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $urlEndpoint,
            'message'      => $apiName
        ]);

        $params = [
            'whsId'    => $whsId,
            'cusId'    => $cusId,
            'ctn_rfid' => array_get($input, 'ctn_rfid', null),
            'dmg_id'   => array_get($input, 'dmg_id', null),
        ];

        $ctnRfid = $params['ctn_rfid'];
        $dmgId   = $params['dmg_id'];

        //validate carton RFID
        $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
        if (!$ctnRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $ctnRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //WMS2-3964
        $errors = $this->checkCusBelongWhs($whsId, $cusId);
        if ($errors) {
            return $errors;
        }

        $isBelong = $this->vtlCtn->getFirstWhere([
            'cus_id'   => $cusId,
            'whs_id'   => $whsId,
            'ctn_rfid' => $ctnRfid
        ]);

        if (!$isBelong) {
            $msg = "Virtual carton doesn't exist";
            $isExist = $this->vtlCtn->getFirstWhere(['ctn_rfid' => $ctnRfid]);
            if ($isExist) {
                $msg = "Virtual carton doesn't belong to warehouse and customer";
            }
            return $this->_responseErrorMessage($msg);
        }

        //check this vtl ctn can delete
        $vtlCtnSts = $this->vtlCtn->getVtlCtnStatus($ctnRfid);

        if (empty($vtlCtnSts) || count($vtlCtnSts) == 0 || is_null($vtlCtnSts)) {
            $msg = sprintf("Can not find carton has RFID: %s .", $ctnRfid);
            return $this->_responseErrorMessage($msg);
        }

        $vtlCtnSts = object_get($vtlCtnSts, 'vtl_ctn_sts', null);
        if ($vtlCtnSts == 'GC') {
//            $msg = "Can't set damage cartons which have been assigned to pallet!";
//            return $this->_responseErrorMessage($msg);
            $ctnRfidsScannedPallet = $ctnRfid;
        }

        $checkDamge = DB::table('damage_type')->where('dmg_id', $dmgId)->first();
        if (!count($checkDamge)) {
            $msg = sprintf("Damage Id %s does not exist!", $dmgId);
            return $this->_responseErrorMessage($msg);
        }

        try {
            DB::Begintransaction();

            $this->vtlCtn->updateWhere(
                [
                    'is_damaged' => '1',
                    'dmg_id'     => $dmgId,
                ],
                [
                    'ctn_rfid' => $ctnRfid
                ]
            );

            if(!empty($ctnRfidsScannedPallet)){
                $carton = $this->carton->getFirstWhere(['rfid' => $ctnRfidsScannedPallet]);
                if($carton){
                    // Update cartons
                    $this->carton->updateWhere(
                        [
                            'is_damaged' => '1'
                        ],
                        [
                            'rfid'   => $ctnRfidsScannedPallet,
                            'ctn_id' => $carton['ctn_id']
                        ]
                    );
                    // Get current user id
                    $userId = Data::getCurrentUserId();
                    // Update damage_carton
                    DB::table('damage_carton')->insert(
                        [
                            'ctn_id'     => $carton['ctn_id'],
                            'dmg_id'     => $dmgId,
                            'dmg_note'   => '',
                            'created_at' => strtotime('now'),
                            'created_by' => $userId,
                            'updated_at' => strtotime('now'),
                            'updated_by' => $userId,
                            'deleted'    => 0,
                            'deleted_at' => 915148800
                        ]
                    );
                }
            }

            DB::Commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => sprintf("Set damage carton has RFID %s successfully!", $ctnRfid)
            ]];
            $msg['data'] = [];

            return $msg;

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $urlEndpoint,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return $this->_responseErrorMessage($this->getResponseData());
        }
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function setDamageToMultiCartons($whsId, $cusId = null, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $owner = $transaction = "";
        if($cusId) {
            $urlEndpoint = "/v2/whs/{$whsId}/cus/{$cusId}/carton/set-damage-multi-cartons";
        }
        else{
            $urlEndpoint = "/v2/whs/{$whsId}/carton/set-damage-multi-cartons";
        }
        $apiName = 'Set damage multiple Cartons';
        Log:: info($request, $whsId, [
            'evt_code'     => 'UPD',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $urlEndpoint,
            'message'      => $apiName
        ]);

        $dataCtns = array_get($input, 'data', null);

        $chkDmgIds = [];
        $chkCusIds = [];
        foreach ($dataCtns as $dataCtn) {
            $ctnRfid = array_get($dataCtn, 'ctn_rfid');
            $dmgId   = array_get($dataCtn, 'dmg_id');
            //validate carton RFID
            $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
            if (!$ctnRFIDValid->validate()) {
                $this->_setErrorMsg($ctnRFIDValid->error);
                $this->_wrongCtnRfids[] = $ctnRfid;
                continue;
            }

            //WMS2-3964
            if(!empty($cusId)){
                $errors = $this->checkCusBelongWhs($whsId, $cusId);
                if ($errors) {
                    $this->_setErrorMsg($errors);
                    $this->_wrongCtnRfids[] = $ctnRfid;
                    continue;
                }
            }

            $isBelong = $this->vtlCtn->getFirstWhere([
                //'cus_id'   => $cusId,
                'whs_id'   => $whsId,
                'ctn_rfid' => $ctnRfid
            ]);

            if (!$isBelong) {
                $msg = sprintf("Virtual carton %s doesn't exist.", $ctnRfid);
                $isExist = $this->vtlCtn->getFirstWhere(['ctn_rfid' => $ctnRfid]);
                if ($isExist) {
                    $msg = sprintf("Virtual carton %s doesn't belong to warehouse and customer", $ctnRfid);
                }
                $this->_setErrorMsg($msg);
                $this->_wrongCtnRfids[] = $ctnRfid;
                continue;
            }

            $chkCusIds[] = object_get($isBelong, 'cus_id', '');

            //check this vtl ctn can delete
            $vtlCtnSts = $this->vtlCtn->getVtlCtnStatus($ctnRfid);

            if (empty($vtlCtnSts) || count($vtlCtnSts) == 0 || is_null($vtlCtnSts)) {
                $msg = sprintf("Can not find carton has RFID: %s .", $ctnRfid);
                $this->_setErrorMsg($msg);
                $this->_wrongCtnRfids[] = $ctnRfid;
                continue;
            }

            $vtlCtnSts = object_get($vtlCtnSts, 'vtl_ctn_sts', null);
            if ($vtlCtnSts == 'GC') {
                $this->_CtnRfidsScannedPallet[] = $ctnRfid;
                continue;
//                $msg = sprintf("Can't set damage carton %s which has been assigned to pallet!", $ctnRfid);
//                $this->_setErrorMsg($msg);
//                $this->_wrongCtnRfids[] = $ctnRfid;$_CtnRfidsScannedPallet
//                continue;
            }

            if (!in_array($dmgId, $chkDmgIds)) {
                $checkDamge = DB::table('damage_type')->where('dmg_id', $dmgId)->first();
                if (!count($checkDamge)) {
                    $msg = sprintf("Damage Id %s does not exist!", $dmgId);
                    return $this->_responseErrorMessage($msg);
                }
                $chkDmgIds[] = $dmgId;
            }
        }

        // Check carton belong 1 customer
        if(count(array_unique($chkCusIds)) != 1){
            $msg = sprintf("Carton need belong to 1 Customer");
            return $this->_responseErrorMessage($msg);
        }

        try {
            if (count($this->getErrorMsg())){
                $numberWrongCtn = count(array_unique($this->_wrongCtnRfids));
                $formatMsg = sprintf("are %s cartons", $numberWrongCtn);
                if ($numberWrongCtn == 1) {
                    $formatMsg = sprintf("is %s carton", $numberWrongCtn);
                }
                $msg = sprintf("Failed!. There %s cannot be set damaged. ".PHP_EOL."[{%s}].",
                    $formatMsg,
                    implode("}; ".PHP_EOL."{", $this->getErrorMsg()));
                return $this->_responseErrorMessage($msg);
            }
            DB::Begintransaction();

            foreach ($dataCtns as $dataCtn) {
                $this->vtlCtn->updateWhere(
                    [
                        'is_damaged' => '1',
                        'dmg_id'     => array_get($dataCtn, 'dmg_id'),
                    ],
                    [
                        'ctn_rfid' => array_get($dataCtn, 'ctn_rfid')
                    ]
                );
            }
            $grHdrIds = [];
            $grDtlIds = [];
            $pltIds = [];
            foreach ($this->_CtnRfidsScannedPallet as $ctnRfidsScannedPallet) {
                $carton = $this->carton->getFirstWhere(['rfid' => $ctnRfidsScannedPallet]);
                if($carton){
                    $grHdrIds[] = $carton->gr_hdr_id;
                    $grDtlIds[] = $carton->gr_dtl_id;
                    $pltIds[] = $carton->plt_id;
                    // Update cartons
                    $this->carton->updateWhere(
                        [
                            'is_damaged' => '1'
                        ],
                        [
                            'rfid'   => $ctnRfidsScannedPallet,
                            'ctn_id' => $carton->ctn_id
                        ]
                    );
                    // Get current user id
                    $userId = Data::getCurrentUserId();
                    // get dmg_id input
                    foreach ($dataCtns as $dataCtn) {
                        if(array_get($dataCtn, 'ctn_rfid') == $ctnRfidsScannedPallet){
                            $dmg_id = array_get($dataCtn, 'dmg_id');
                        }
                    }
                    // Update damage_carton
                    DB::table('damage_carton')->insert(
                        [
                            'ctn_id'     => $carton->ctn_id,
                            'dmg_id'     => $dmg_id,
                            'dmg_note'   => '',
                            'created_at' => strtotime('now'),
                            'created_by' => $userId,
                            'updated_at' => strtotime('now'),
                            'updated_by' => $userId,
                            'deleted'    => 0,
                            'deleted_at' => 915148800
                        ]
                    );
                }
            }
            $this->grDtlModel->updateGrDtlWhenScanPallet($grDtlIds);
            /**
             * Update pallet: carton total, damage
             */
            foreach ($pltIds as $pltId){
                $this->pltModel->updatePalletCtnTtlAndDamaged($whsId, $grHdrIds, $pltId, 'null', [], []);
            }
            DB::Commit();

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => sprintf("Set damaged %s carton(s) successfully!", count($dataCtns))
            ]];
            $msg['data'] = [];

            return $msg;

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $urlEndpoint,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return $this->_responseErrorMessage($this->getResponseData());
        }
    }

    public function setDamageToMultiCartons2($whsId, Request $request){
        return $this->setDamageToMultiCartons($whsId, null, $request);
    }

    private function _setErrorMsg($msg)
    {
        $this->_errorMsgs[] = $msg;
    }

    public function getErrorMsg()
    {
        return $this->_errorMsgs;
    }

    /**
     * Check customer is belong to warehouse
     *
     * @param integer $whsId
     * @param integer $cusId
     *
     * @throws \Exception
     */
    private function checkCusBelongWhs($whsId, $cusId)
    {
        $cusWhsModel = (new CustomerWarehouse())->where([
            'whs_id' => $whsId,
            'cus_id' => $cusId
        ])->first();
        if (!$cusWhsModel) {
            //throw new \Exception('Customer is not belong to warehouse');
            $msg = sprintf('Customer is not belong to warehouse.');
            return $this->_responseErrorMessage($msg);
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}
