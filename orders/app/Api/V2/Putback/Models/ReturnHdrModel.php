<?php

namespace App\Api\V2\Putback\Models;

use App\Api\V2\Putback\Models\ItemModel;
use Seldat\Wms2\Models\ReturnHdr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelArr;

/**
 * Class ReturnHdrModel
 *
 * @package App\Api\V1\Models
 */
class ReturnHdrModel extends AbstractModel
{
    /**
     * ReturnHdrModel constructor.
     *
     * @param ReturnHdr|null $model
     */
    public function __construct(ReturnHdr $model = null)
    {
        $this->model = ($model) ?: new ReturnHdr();
    }

    /**
     * @param $returnHdrId
     *
     * @return mixed
     */
    public function deleteReturnHdr($returnHdrId)
    {
        return $this->model
            ->where('return_id', $returnHdrId)
            ->delete();
    }

    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function checkWhereIn($orderIds)
    {
        return $this->model
            ->whereIn('odr_id', $orderIds)
            ->count();
    }

    /**
     * @param array $width
     *
     * @return mixed
     */
    public function getFirstMax($width = [])
    {
        return $this->make($width)
            ->orderBy('return_id', 'desc')
            ->first();
    }

    /**
     * @param array $odrHdrId
     *
     * @return mixed
     */
    public function getItemForAssign($odrHdrId)
    {
        $queryOdrDtl = DB::table('odr_dtl as od')
            ->select([
                'od.odr_id as odr_hdr_id',
                'od.odr_dtl_id as odr_dtl_id',
                'od.item_id',
                'od.sku',
                'od.size',
                'od.color',
                'od.pack',
                'od.lot',
                DB::raw('(od.picked_qty - od.packed_qty) AS picked_qty'),
                DB::raw('0 AS new_sku'),
                'od.des',
                'od.uom_id',
                DB::raw('CEIL(picked_qty/od.pack) AS picked_ctns')
            ])
            ->where('od.odr_id', $odrHdrId)
            ->whereRaw('(od.picked_qty - od.packed_qty) > 0')
            ->where('od.deleted', 0)
            ->groupBy('od.odr_dtl_id')
            ->get();

        // get parent item when order packed or packing
        $newItems = DB::table('pack_hdr')
            ->join('item', 'item.item_id', '=', 'pack_hdr.item_id')
            ->select([
                'odr_hdr_id',
                DB::raw('NULL AS odr_dtl_id'),
                'item.item_id',
                'item.sku',
                'item.size',
                'item.color',
                'item.pack',
                DB::raw("'NA' AS lot"),
                DB::raw('SUM(item.pack) AS picked_qty'),
                DB::raw('1 AS new_sku'),
                'item.description as des',
                'item.uom_id',
                DB::raw('COUNT(pack_hdr.pack_hdr_id) AS picked_ctns'),
            ])
            ->where('odr_hdr_id', $odrHdrId)
            ->whereNotNull('pack_hdr.item_id')
            ->groupBy('pack_hdr.item_id')
            ->get();

        $queryPackHdr = array_merge($queryOdrDtl, $newItems);

        $sysUoms = DB::table('system_uom')
            ->select([
                'sys_uom_id',
                'sys_uom_code',
                'sys_uom_name'
            ])
            ->where('deleted', 0)
            ->get();
        $sysUoms = array_column($sysUoms, null, 'sys_uom_id');

        foreach ($queryPackHdr as $pKey => $packHdr) {
            if (!$packHdr['item_id']) {
                unset($queryPackHdr[$pKey]);
            }

            $queryPackHdr[$pKey]['uom_code'] = $sysUoms[$packHdr['uom_id']]['sys_uom_code'];
            $queryPackHdr[$pKey]['uom_name'] = $sysUoms[$packHdr['uom_id']]['sys_uom_name'];
        }

        return $queryPackHdr;
    }
}
