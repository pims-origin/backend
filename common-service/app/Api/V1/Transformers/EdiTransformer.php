<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Edi;
use Seldat\Wms2\Models\CustomerConfig;
use League\Fractal\TransformerAbstract;

class EdiTransformer extends TransformerAbstract
{
    public function transform(CustomerConfig $customerConfig)
    {
        return [
            'cus_config_id' => $customerConfig->id,
            'cus_id'        => $customerConfig->cus_id,
            'cus_name'      => $customerConfig->edi_name,
            'edi'           => $customerConfig->edi_sts
        ];
    }
}
