<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\WavepickDtl;

use Seldat\Wms2\Utils\SelArr;

class WavePicksDtlModel extends AbstractModel
{

    public function __construct(WavepickHdr $model = NULL)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with)
            ->where('wv_dtl.whs_id', $attributes['whs_id']);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $attributes['compare_operator']['wv_num'] = 'like';
        $attributes['compare_operator']['sku'] = 'like';
        $attributes['compare_operator']['lot'] = 'like';
        $attributes['compare_operator']['primary_loc'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'cus_id',
                'wv_num',
                'sku',
                'lot',
                'primary_loc',
            ],
            'attributes' => $attributes,
        ]);

        return $this->runSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

    /*
    ***************************************************************************
    */

}
