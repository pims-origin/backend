<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class DisplayErrorsTransformer extends TransformerAbstract
{
    
    /**
     * @param $data
     * @return array
     */
    public function transform($data)
    {
        return [
            'errors' => $data->errors
        ];
    }
}
