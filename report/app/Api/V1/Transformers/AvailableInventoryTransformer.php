<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\AvailableInventory;
use League\Fractal\TransformerAbstract;

class AvailableInventoryTransformer extends TransformerAbstract
{

    /*
    ***************************************************************************
    */

    public function transform(AvailableInventory $availableInventory)
    {
        $cartonsItem = object_get($availableInventory, 'item.carton', null);

        return [
            'cus_name' => object_get($availableInventory, 'customer.cus_name', ''),
            'ctnr_num' => $availableInventory->ctnr_num,
            'sys_mea_name' => object_get($availableInventory, 'AsnDtl.AsnHdr.SystemMeasurement.sys_mea_name', ''),
            'created_at' => $availableInventory->created_at->format('m/d/Y h:i'),
            'upc' => $availableInventory->upc,
            'sku' => $availableInventory->sku,
            'length' => $availableInventory->length,
            'width' => $availableInventory->width,
            'height' => $availableInventory->height,
            'weight' => $availableInventory->weight,
            'po' => $availableInventory->po,
            'lot' => $availableInventory->lot,
            'size' => $availableInventory->size,
            'color' => $availableInventory->color,
            'actual_cartons' => $cartonsItem->count('piece_remain'),
            'piece_remain' => $cartonsItem->sum('piece_remain'),
        ];
    }

    /*
    ***************************************************************************
    */

}
