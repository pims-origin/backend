<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\SystemCountry;
use League\Fractal\TransformerAbstract;

class SystemCountryTransformer extends TransformerAbstract
{
    public function transform(SystemCountry $systemCountry)
    {
        return [
            'sys_country_id'   => $systemCountry->sys_country_id,
            'sys_country_code' => $systemCountry->sys_country_code,
            'sys_country_name' => $systemCountry->sys_country_name,
        ];
    }
}
