<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\LocationStatusDetailModel;
use App\Api\V1\Models\LocationTypeModel;
use App\Api\V1\Models\ReasonModel;
use App\Api\V1\Models\WarehouseZoneModel;
use App\Api\V1\Models\ZoneLocationStatusModel;
use App\Api\V1\Models\ZoneModel;
use App\Api\V1\Transformers\LocationByCustomerTransformer;
use App\Api\V1\Transformers\LocationStatusDetailTransformer;
use App\Api\V1\Transformers\LocationTransformer;
use App\Api\V1\Validators\DeleteMassLocationValidator;
use App\Api\V1\Validators\LocationImportValidator;
use App\Api\V1\Validators\LocationStatusDetailValidator;
use App\Api\V1\Validators\LocationValidator;
use App\Library\LocationImport;
use Dingo\Api\Http\Response;
use Illuminate\Http\Request as IRequest;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\LocationStatusDetail;
use Seldat\Wms2\Models\LocationType;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\Zone;
use Seldat\Wms2\Models\ZoneLocationStatus;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\SystemBug;
use Symfony\Component\HttpKernel\Exception\HttpException;
use mPDF;
use Wms2\UserInfo\Data;

class LocationController extends AbstractController
{
    /*
     * @var LocationModel
    */
    protected $locationModel;

    /*
     * @var LocationTransformer
    */
    protected $locationTransformer;
    /*
     * @var LocationValidator
    */
    protected $locationValidator;
    /*
     * @var DeleteMassLocationValidator
    */
    protected $delMassLocValidator;
    /*
     * @var LocationStatusDetailValidator
    */
    protected $locationStatusDetailValidator;
    /*
     * @var LocationStatusDetailTransformer
    */
    protected $locationStatusDetailTransformer;
    /*
     * @var LocationImportValidator
    */
    protected $locationImportValidator;
    /*
     * @var LocationTypeModel
    */
    protected $locationTypeModel;

    /*
     * @var WarehouseZoneModel
    */
    protected $warehouseZoneModel;
    /*
     * @var ZoneLocationStatusModel
    */
    protected $locationStatusModel;

    protected $cartonModel;

    protected $zoneModel;

    protected $spcHdl = [
        'RAC'   =>  'Rack',
        'SHE'   =>  'Shelf',
        'BIN'   =>  'Bin',
        'FRE'   =>  'Freezer'
    ];

    /**
     * LocationController constructor.
     */
    public function __construct(CartonModel $cartonModel)
    {
        parent::__construct();
        $this->locationModel = new LocationModel();
        $this->locationTransformer = new LocationTransformer();
        $this->locationValidator = new LocationValidator();
        $this->delMassLocValidator = new DeleteMassLocationValidator();
        $this->locationStatusDetailValidator = new LocationStatusDetailValidator();
        $this->locationStatusDetailTransformer = new LocationStatusDetailTransformer();
        $this->locationImportValidator = new LocationImportValidator();
        $this->locationTypeModel = new LocationTypeModel();
        $this->warehouseZoneModel = new WarehouseZoneModel();
        $this->locationStatusModel = new ZoneLocationStatusModel();
        $this->cartonModel = $cartonModel;
        $this->zoneModel = new ZoneModel();
    }

    /**
     * API List By Zone
     *
     * @param int $warehouseId
     * @param int $zoneId
     * @param Request $request
     *
     * @return Response|void
     */
    public function listByZone($warehouseId, $zoneId, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $location = $this->locationModel->loadByZone($warehouseId, $zoneId, $input);

            return $this->response->paginator($location, $this->locationTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Search All Location (Zone is NULL)
     *
     * @param int $warehouseId
     * @param Request $request
     *
     * @return Response|void
     */
    public function searchExceptZone($warehouseId, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $location = $this->locationModel->allExceptZone($warehouseId, $input, [], array_get($input, "limit", 20));

            return $location;
        } catch (\PDOException $e) {
            var_dump($e->getMessage());exit;
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Read Location
     *
     * @param int $warehouseId
     * @param int $locationId
     *
     * @return Response|void
     */
    public function show($warehouseId, $locationId)
    {
        try {
            $location = $this->locationModel->loadLocByIdAndWhsId($locationId, $warehouseId);

            return $this->response->item($location, $this->locationTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Search Location
     *
     * @param int $warehouseId
     * @param Request $request
     *
     * @return Response|void
     */
    public function search($warehouseId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['warehouseId'] = $warehouseId;
        $without = array_get($input, 'without', false);
        $limit = array_get($input, 'limit', 20);
        $except = array_get($input, 'ex');
//        $hasPallet = array_get($input, 'has_pallet', null);
        $hasPallet = null;

        try {
            $location = $this->locationModel->search($input, ['cartons', 'warehouse', 'pallet'], $limit, $without, $except,
                $hasPallet);
            if (!empty($location)) {
                foreach ($location as $key => $loc) {
                    $location[$key]['loc_available_capacity'] = $loc->pallet ? "No" : "Yes";
                    $location[$key]['availableQuantity'] = 0;

                    if (count($loc->cartons) > 0) {
                        $ctns = $loc->cartons->toArray();

                        $items = array_pluck($ctns, 'item_id', 'item_id');
                        $location[$key]['ctn_ttl'] = count($ctns);
                        $location[$key]['has_pallet'] = count($ctns) > 0 ? 1 : 0;
                        $location[$key]['sku_ttl'] = count($items);
                        $location[$key]['piece_ttl'] = array_sum(array_column($ctns, 'piece_ttl'));

                        $location[$key]['availableQuantity'] = $loc->cartons->reduce(
                            function($carry, $item){
                                if ($item->ctn_sts == 'AC' && $item->loc_type_code == 'RAC'){
                                    return $carry + $item['piece_remain'];
                                }
                            }, 0
                        );
                    }
                }
            }

            return $this->response->paginator($location, $this->locationTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * API Search Location
     *
     * @param int $warehouseId
     * @param Request $request
     *
     * @return Response|void
     */
    public function locationsByCustomer($whsId, $cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['cus_id'] = $cusId;
        $limit = array_get($input, 'limit', 20);

        try {
            $location = $this->locationModel->getLocationsByCustomer($input, [], $limit);

            return $this->response->paginator($location, new LocationByCustomerTransformer());

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $request
     * @param int $whsId
     * @param bool|false $locId
     *
     * @return Response|void
     */
    protected function upsert($request, $whsId, $locId = false)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['loc_whs_id'] = $whsId;
        // validation
        $this->locationValidator->validate($input);

        // Check rfid
        $rfid = array_get($input, 'rfid', NULL);
        if (!empty($rfid)) {
            if (!$this->locationModel->isRfid($rfid, $whsId)) {
                return $this->response->errorBadRequest("The Rfid is invalid!");
            }
        }
        $max_LPN = null;
        if(strtoupper(array_get($input, 'spc_hdl_code', 'NA')) == "RAC") {
            $max_LPN = null;
        } else if(strtoupper(array_get($input, 'spc_hdl_code', 'NA')) == "SHE") {
            $max_LPN = 25;
        } else if(strtoupper(array_get($input, 'spc_hdl_code', 'NA')) == "BIN") {
            $max_LPN = 50;
        } else if(strtoupper(array_get($input, 'spc_hdl_code', 'NA')) == "FRE") {
            $max_LPN = 50;
        }

        $params = [
            'loc_code'               => $input['loc_code'],
            'loc_alternative_name'   => array_get($input, 'loc_alternative_name', ''),
            'loc_whs_id'             => $whsId,
            'loc_type_id'            => $input['loc_type_id'],
            'loc_sts_code'           => strtoupper($input['loc_sts_code']),
            'loc_available_capacity' => array_get($input, 'loc_available_capacity', null),
            'loc_width'              => array_get($input, 'loc_width', 0),
            'loc_length'             => array_get($input, 'loc_length', 0),
            'loc_height'             => array_get($input, 'loc_height', 0),
            'loc_max_weight'         => array_get($input, 'loc_max_weight', 0),
            'loc_weight_capacity'    => is_numeric($input['loc_weight_capacity']) ? $input['loc_weight_capacity'] : null,
            // 'loc_min_count'          => is_numeric($input['loc_min_count']) ? $input['loc_min_count'] : null,
            // 'loc_max_count'          => is_numeric($input['loc_max_count']) ? $input['loc_max_count'] : null,
            'loc_max_count'          => null,
            'loc_desc'               => array_get($input, 'loc_desc', ''),
            'rfid'                   => !empty($rfid) ? $rfid : NULL,
            'aisle'                  => array_get($input, 'aisle', ''),
            'level'                  => array_get($input, 'level', 0),
            'row'                    => array_get($input, 'row', ''),
            'bin'                    => array_get($input, 'bin', 0),
            'area'                   => !empty(array_get($input, 'area', 0)) ? array_get($input, 'area', 0) : 0,
            'spc_hdl_code'           => array_get($input, 'spc_hdl_code', 'NA'),
            'spc_hdl_name'           => $this->spcHdl[$input['spc_hdl_code']],
            'max_lpn'                => $max_LPN
        ];
        if ($locId) {
            $params['loc_id'] = $locId;
        }

        try {
            DB::beginTransaction();

            // Check unique aisle, row, level, bin
            $chkDupAisleLevelRowBin = true;
            $lCheck = $this->locationModel->getFirstWhere([
                'aisle'         => $params['aisle'],
                'level'         => $params['level'],
                'row'           => $params['row'],
                'bin'           => $params['bin'],
                'loc_whs_id'    => $whsId
            ]);
            if ($lCheck && $locId) {
                $chkDupAisleLevelRowBin = ($lCheck->loc_id != $locId);
            }
            if ($lCheck && $chkDupAisleLevelRowBin) {
                return $this->response->errorBadRequest(Message::get("BM006", "Location Aisle, Row, Level, Bin"));
            }

            // check duplicate code
            $chkDupCode = true;
            $loc = $this->locationModel->getFirstWhere([
                'loc_code'   => $params['loc_code'],
                'loc_whs_id' => $whsId
            ]);
            if ($loc && $locId) {
                $chkDupCode = ($loc->loc_id != $locId);
            }
            if ($loc && $chkDupCode) {
                return $this->response->errorBadRequest(Message::get("BM006", "Location Code"));
            }

            // check duplicate name
            $chkDupName = true;
            $loc = $this->locationModel->getFirstWhere([
                'loc_alternative_name' => $params['loc_alternative_name'],
                'loc_whs_id'           => $whsId
            ]);
            if ($loc && $locId) {
                $chkDupName = ($loc->loc_id != $locId);
            }
            if ($loc && $chkDupName) {
                return $this->response->errorBadRequest(Message::get("BM006", "Location Name"));
            }

            // Check Duplicate RFID
            if (!empty($params['rfid']) &&
                ($locRfid = $this->locationModel->getFirstWhere(['rfid' => $params['rfid']])) &&
                $locRfid->loc_id != $locId
            ) {
                return $this->response->errorBadRequest(Message::get("BM006", "Rfid"));
            }

            $vZone = DB::table('zone')
                ->select('zone.zone_id')
                ->join('zone_type', 'zone_type.zone_type_id', '=', 'zone.zone_type_id')
                ->where('zone_type.zone_type_code', 'VZ')
                ->first();

            $params['loc_zone_id'] = $vZone['zone_id'];

            // create/update location with loc_type = x-dock
            $type = $this->locationTypeModel->byId($input['loc_type_id']);
            if (!empty($type)) {
                if ($type->loc_type_code == 'XDK') {
                    /*
                 * add zone X-Dock to all of location in current warehouse when these location have loc_type = X-Dock.
                 */
                    $params['loc_zone_id'] = $this->setZoneXDockToLocInCurrentWhs($whsId, $input['loc_type_id']);
                }

                if ($type->loc_type_code == 'SHP') {
                    /*
                 * add zone Shipping to all of location in current warehouse when these location have loc_type = Shipping.
                 */
                    $params['loc_zone_id'] = $this->setZoneShippingToLocInCurrentWhs($whsId, $input['loc_type_id']);
                }
            }

            if ($locId) {
                $location = $this->locationModel->getFirstWhere(['loc_whs_id' => $whsId, 'loc_id' => $locId]);
                if (empty($location)) {
                    throw new \Exception(Message::get("BM0017", "Location"));
                }
                if ($this->checkInventoryForLocation($location, $params['loc_sts_code'])) {
                    throw new \Exception(Message::get("BM030"));
                }
                $location = $this->locationModel->update($params);

                // Update child location
                unset($params['loc_id']);
                unset($params['loc_code']);
                unset($params['loc_alternative_name']);
                $childLoc = $this->locationModel->updateWhere($params, ['parent_id' => $locId]);
            } else {
                $location = $this->locationModel->create($params);

                // Create child location
                $params['parent_id'] = $location['loc_id'];
                $params['parent_code'] = $location['loc_code'];
                $allCus = DB::table('customer')->select('cus_id')->where('deleted', 0)->get();

                foreach($allCus as $key => $cus) {
                    $params['loc_cus_id'] = $cus['cus_id'];
                    $params['loc_code'] = $input['loc_code'] . '-' . ($key+1);
                    $params['loc_alternative_name'] = $input['loc_code'] . '-' . ($key+1);

                    $this->locationModel->refreshModel();
                    $childLoc = $this->locationModel->create($params);
                }
            }

            if (!empty($type) && ($type->loc_type_code == 'SHP' || $type->loc_type_code == 'XDK')) {
                // update num_of_loc and num_of_inactive_loc
                $countLocations = $this->locationModel->getModel()
                    ->select([
                        DB::raw("SUM(IF(loc_sts_code = 'AC', 1, 0)) as num_of_active"),
                        DB::raw("SUM(IF(loc_sts_code = 'IA', 1, 0)) as num_of_inactive"),
                        DB::raw("COUNT(1) as num_of_locations")
                    ])
                    ->where('loc_whs_id', $whsId)
                    ->where('loc_zone_id', $params['loc_zone_id'])
                    ->where('parent_id', null)
                    ->groupBy('loc_zone_id')
                    ->first();
                $this->zoneModel->updateWhere([
                    'zone_num_of_loc'          => $countLocations->num_of_locations,
                    'zone_num_of_active_loc'   => $countLocations->num_of_active,
                    'zone_num_of_inactive_loc' => $countLocations->num_of_inactive
                ],[
                    'zone_id' => $params['loc_zone_id']
                ]);
            }

            DB::commit();

            return $this->response->item($location, $this->locationTransformer);

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function setZoneXDockToLocInCurrentWhs($whsId, $locTypeXdoc)
    {
        $xdkZone = DB::table('zone_type')->where('zone_type_code', 'XDK')->value('zone_type_id');

        $zoneXdoc = $this->zoneModel->getFirstWhere([
            'zone_code'   => 'XDK-D',
            'zone_whs_id' => $whsId
        ]);
        // if not exists zone X-Dock with zone_code = XDK-D in current warehouse -> create
        if (empty($zoneXdoc)) {
            $zoneXdoc = $this->zoneModel->create([
                'zone_name' => 'X-Dock',
                'zone_code' => 'XDK-D',
                'zone_whs_id' => $whsId,
                'zone_type_id' => $xdkZone,
                'zone_min_count' => 1,
                'zone_max_count' => 100,
                'zone_color' => '#e9cf0a'
            ]);
        }

        $zone_id = $zoneXdoc->zone_id;

        // update loc_zone_id for locations have loc_type = X-Dock and included in current warehouse
        $this->locationModel->getModel()
            ->where('loc_type_id', $locTypeXdoc)
            ->where('loc_whs_id', $whsId)
            ->where(function($query) use ($zone_id) {
                $query->where('loc_zone_id', "!=", $zone_id)
                    ->orWhereNull('loc_zone_id');
            })
            ->update([
                'loc_zone_id' => $zone_id
            ]);

        return $zone_id;
    }

    private function setZoneShippingToLocInCurrentWhs($whsId, $locTypeShipping)
    {
        $zoneShipping = $this->zoneModel->getFirstWhere([
            'zone_code'   => 'SHP',
            'zone_whs_id' => $whsId
        ]);
        // if not exists zone X-Dock with zone_code = XDK-D in current warehouse -> create
        if (empty($zoneShipping)) {
            $zone_type_id = 1;
            $zoneType = DB::table('zone_type')->where('zone_type_code', 'SHP')->first();
            if (!empty($zoneType)) {
                $zone_type_id = $zoneType['zone_type_id'];
            }
            $zoneShipping = $this->zoneModel->create([
                'zone_name' => 'Shipping Lane',
                'zone_code' => 'SHP',
                'zone_whs_id' => $whsId,
                'zone_type_id' => $zone_type_id,
                'zone_min_count' => 1,
                'zone_max_count' => 100,
                'zone_color' => '#e9cf0a'
            ]);
        }

        $zone_id = $zoneShipping->zone_id;

        // update loc_zone_id for locations have loc_type = X-Dock and included in current warehouse
        $this->locationModel->getModel()
            ->where('loc_type_id', $locTypeShipping)
            ->where('loc_whs_id', $whsId)
            ->where(function($query) use ($zone_id) {
                $query->where('loc_zone_id', "!=", $zone_id)
                    ->orWhereNull('loc_zone_id');
            })
            ->update([
                'loc_zone_id' => $zone_id
            ]);

        return $zone_id;
    }

    /**
     * API Create Location
     *
     * @param int $warehouseId
     * @param Request $request
     *
     * @return Response|void
     */
    public function store($warehouseId, Request $request)
    {
        return $this->upsert($request, $warehouseId);
    }

    /**
     * API Update Location
     *
     * @param int $warehouseId
     * @param int $locationId
     * @param Request $request
     *
     * @return Response|void
     */
    public function update($warehouseId, $locationId, Request $request)
    {
        return $this->upsert($request, $warehouseId, $locationId);
    }

    /**
     * API Delete Location
     *
     * @param int $warehouseId
     * @param int $locationId
     *
     * @return Response|void
     */
    public function destroy($warehouseId, $locationId)
    {
        //Locations cannot be removed while having inventories! (Location has already item)
        if ($this->cartonModel->checkWhere(
            [
                'loc_id' => $locationId
            ])
        ) {
            return $this->response->errorBadRequest(Message::get("BM031"));
        }

        try {
            if ($this->locationModel->deleteLocation($warehouseId, $locationId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM017", "Location"));
    }

    /**
     * @param int $warehouseId
     * @param Request $request
     *
     * @return Response|void
     */
    public function deleteMass($warehouseId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->delMassLocValidator->validate($input);

        $input['loc_id'] = is_array($input['loc_id']) ? $input['loc_id'] : [$input['loc_id']];

        //Locations cannot be removed while having inventories! (Location has already item)
        if ($this->cartonModel->checkWhereIn($input['loc_id'])) {
            return $this->response->errorBadRequest(Message::get("BM031"));
        }

        try {
            $this->locationModel->deleteMassLocation($warehouseId, $input['loc_id']);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }

    /**
     * @param $warehouseId
     * @param $zoneId
     * @param Request $request
     *
     * @return Response|void
     * @throws \Exception
     */
    public function saveMassAndDeleteUnused(
        $warehouseId,
        $zoneId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $action = $input['action'] ?? null; //"add" or "delete"

        if (!$action) {
            throw new HttpException(400, "Invalid action name");
        }

        $listLocations = $this->locationModel->findWhereIn(
            $warehouseId, $input['loc_id']
        );

        if (!empty($listLocations)) {
            foreach ($listLocations as $item) {
                if ($item->loc_zone_id && $item->loc_zone_id != $zoneId) {
                    throw new \Exception("Location belong to other zone!");
                }
            }

            $listLocations = null;
        }


        DB::beginTransaction();
        $locIds = $input['loc_id'];
        if ($action === "add") {
            if (!empty($locIds)) {
                $this->locationModel->updateLocToZone($locIds, $zoneId, $warehouseId);
                $newLocCount = count($locIds);
                $this->zoneModel->updateWhere([
                    'zone_num_of_loc' => DB::raw("IFNULL(zone_num_of_loc, 0) + {$newLocCount}"),
                    'zone_num_of_active_loc' => DB::raw("IFNULL(zone_num_of_active_loc, 0) + {$newLocCount}")
                ], ['zone_id' => $zoneId]);

                $is_virtual_zone = DB::table('zone')
                    ->where('zone_id', '=', $zoneId)
                    ->where('zone_code', '=', 'VTZ')
                    ->first();

                if ($is_virtual_zone) {
                    $customers = DB::table('customer')->where('deleted', '=', 0)->get();
                    $data_insert = [];
                    foreach ($locIds as $locId) {
                        $loc_info = DB::table('location')
                            ->where('loc_id', '=', $locId)
                            ->where('deleted', '=', 0)
                            ->where('loc_whs_id', '=', $warehouseId)
                            ->whereNull('parent_id')
                            ->first();

                        if ($loc_info) {
                            $has_child = DB::table('location')->where('parent_id', '=', $locId)->first();
                            if ($has_child) {
                                DB::table('location')->where('parent_id', '=', $locId)->update(['loc_zone_id' => $zoneId]);
                            } else {
                                $i = 1;
                                foreach ($customers as $key => $customer) {
                                    $data_insert[] = [
                                        'loc_code' => $loc_info['loc_code'] . '-' . $i,
                                        'loc_alternative_name' => $loc_info['loc_code'] . '-' . $i,
                                        'loc_whs_id' => $loc_info['loc_whs_id'],
                                        'loc_cus_id' => $customer['cus_id'],
                                        'loc_zone_id' => $loc_info['loc_zone_id'],
                                        'loc_type_id' => $loc_info['loc_type_id'],
                                        'rfid' => $loc_info['rfid'],
                                        'loc_sts_code' => $loc_info['loc_sts_code'],
                                        'reason_id' => $loc_info['reason_id'],
                                        'sts_reason' => $loc_info['sts_reason'],
                                        'loc_available_capacity' => $loc_info['loc_available_capacity'],
                                        'loc_width' => $loc_info['loc_width'],
                                        'loc_length' => $loc_info['loc_length'],
                                        'loc_height' => $loc_info['loc_height'],
                                        'loc_max_weight' => $loc_info['loc_max_weight'],
                                        'loc_weight_capacity' => $loc_info['loc_weight_capacity'],
                                        'loc_min_count' => $loc_info['loc_min_count'],
                                        'loc_max_count' => $loc_info['loc_max_count'],
                                        'loc_desc' => $loc_info['loc_desc'],
                                        'created_at' => time(),
                                        'updated_at' => time(),
                                        'created_by' => Data::getCurrentUserId(),
                                        'updated_by' => Data::getCurrentUserId(),
                                        'deleted_at' => $loc_info['deleted_at'],
                                        'deleted' => $loc_info['deleted'],
                                        'max_lpn' => $loc_info['max_lpn'],
                                        'parent_id' => $locId,
                                        'plt_lv' => $loc_info['plt_lv'],
                                        'spc_hdl_code' => $loc_info['spc_hdl_code'],
                                        'spc_hdl_name' => $loc_info['spc_hdl_name'],
                                        'is_block_stack' => $loc_info['is_block_stack'],
                                        'plt_type' => $loc_info['plt_type'],
                                        'room' => $loc_info['room'],
                                        'aisle' => $loc_info['aisle'],
                                        'row' => $loc_info['row'],
                                        'level' => $loc_info['level'],
                                        'bin' => $loc_info['bin'],
                                        'area' => $loc_info['area'],
                                        'reserved_at' => $loc_info['reserved_at'],
                                        'fixed_pick' => $loc_info['fixed_pick'],
                                        'parent_code' => $loc_info['loc_code']
                                    ];
                                    $i++;
                                }
                            }
                        }
                    }

                    DB::table('location')->insert($data_insert);

                }
            }

            $msg = "Location(s) added to zone successfully.";
        } elseif ($action === "delete") {
            try {
                //Zones cannot be removed while having inventories! (Location has already item)
                if ($this->cartonModel->checkWhereIn($locIds)) {
                    return $this->response->errorBadRequest(Message::get("BM031"));
                }

                $this->locationModel->refreshModel();
                $this->locationModel->removeZoneId($zoneId, $locIds);
                $removedLocCount = count($locIds);
                $this->zoneModel->updateWhere([
                    'zone_num_of_loc' => DB::raw("IFNULL(zone_num_of_loc, 0) - {$removedLocCount}"),
                    'zone_num_of_active_loc' => DB::raw("IFNULL(zone_num_of_active_loc, 0) - {$removedLocCount}")
                ], ['zone_id' => $zoneId]);
            } catch (\PDOException $e) {
                return $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest($e->getMessage());
            }
            $msg = "Location(s) deleted successfully.";
        }
        DB::commit();

        return ['data' => $msg];
    }

    /**
     * @param int $warehouseId
     * @param int $locationId
     * @param Request $request
     * @param LocationStatusDetailModel $locationStatusDetailModel
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function changeLocationStatus(
        $warehouseId,
        $locationId,
        Request $request,
        LocationStatusDetailModel $locationStatusDetailModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->locationStatusDetailValidator->validate($input);

        $params = [
            'loc_sts_dtl_loc_id'    => $locationId,
            'loc_sts_dtl_sts_code'  => strtoupper($input['loc_sts_dtl_sts_code']),
            'loc_sts_dtl_from_date' => $input['loc_sts_dtl_from_date'],
            'loc_sts_dtl_to_date'   => array_get($input, 'loc_sts_dtl_to_date', null),
            'loc_sts_dtl_reason'    => array_get($input, 'loc_sts_dtl_reason', null),
            'loc_sts_dtl_remark'    => array_get($input, 'loc_sts_dtl_remark', null),
            'loc_sts_reason_id'     => array_get($input, 'loc_sts_reason_id', null)
                ? array_get($input, 'loc_sts_reason_id', null) : null
        ];

        try {
            $location = $this->locationModel->getFirstBy('loc_id', $locationId, ['pallet']);

            if ($this->checkInventoryForLocation($location, $params['loc_sts_dtl_sts_code'])) {
                throw new \Exception(Message::get("BM030"));
            }

            $result = $locationStatusDetailModel->replace($params);
            if ($result) {
                // Update Location
                $location = $this->locationModel->update([
                    'loc_id'       => $locationId,
                    'loc_whs_id'   => $warehouseId,
                    'loc_sts_code' => $params['loc_sts_dtl_sts_code'],
                ]);

                if (!empty($location['loc_zone_id'])) {
                    $this->changeStatusZone($warehouseId, $location['loc_zone_id']);
                }

                return $this->response->item($result, $this->locationStatusDetailTransformer);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param int $warehouseId
     * @param int $locationId
     *
     * @return Response|void
     */
    public function getLocationStatus(
        $warehouseId,
        $locationId
    ) {
        $locationStatusDetailModel = new LocationStatusDetailModel();
        try {
            $location = $this->locationModel->loadBy(['loc_id' => $locationId, 'loc_whs_id' => $warehouseId])->first();
            if (empty($location)) {
                return $this->response->errorBadRequest(Message::get("BM017", "Location"));
            }

            $locationStatusDetail = $locationStatusDetailModel->getFirstBy('loc_sts_dtl_loc_id', $locationId,
                ['updatedUser']);

            if ($locationStatusDetail) {
                $locationStatusDetail->loc_sts_dtl_reason = '';
                if ($locationStatusDetail->loc_sts_reason_id) {
                    $reasonModel = new ReasonModel();

                    $reason = $reasonModel->getFirstWhere(
                        ['r_id' => $locationStatusDetail->loc_sts_reason_id]
                    );
                    $locationStatusDetail->loc_sts_dtl_reason = array_get($reason, 'r_name', null);
                }

                return $this->response->item($locationStatusDetail, $this->locationStatusDetailTransformer);

            }

            return $this->response->noContent();

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param int $warehouseId
     * @param int $zoneId
     *
     * @return bool
     */
    private function changeStatusZone($warehouseId, $zoneId)
    {
        set_time_limit(0);
        $locations = $this->locationModel->loadBy(['loc_zone_id' => $zoneId, 'loc_whs_id' => $warehouseId])->toArray();
        $zoneParams = [
            'zone_id'                  => $zoneId,
            'zone_whs_id'              => $warehouseId,
            'zone_num_of_loc'          => count($locations),
            'zone_num_of_active_loc'   => 0,
            'zone_num_of_inactive_loc' => 0,
        ];
        foreach ($locations as $record) {
            if ($record['loc_sts_code'] === config("constants.loc_status.ACTIVE")) {
                $zoneParams['zone_num_of_active_loc'] += 1;
            }
            if ($record['loc_sts_code'] === config("constants.loc_status.INACTIVE")) {
                $zoneParams['zone_num_of_inactive_loc'] += 1;
            }
        }
        $zoneModel = new WarehouseZoneModel();
        $zoneModel->update($zoneParams);

        return true;
    }

    /**
     * API Validate File import
     *
     * @param IRequest $request
     *
     * @return array|\Symfony\Component\HttpFoundation\Response|void
     */
    public function validateImportFile(
        IRequest $request
    ) {
        $hashKey = md5($request->header('Authorization'));
        $locationImport = new LocationImport(
            $hashKey,
            $this->locationImportValidator,
            $this->locationModel,
            $this->locationTypeModel,
            $this->warehouseZoneModel,
            $this->locationStatusModel
        );
        $rs = $locationImport->validate($request);

        if ($rs['status']) {
            return $this->response->noContent()
                ->setContent(['data' => $rs['data']])->setStatusCode($rs['data']['code']);
        } else {
            if (!empty($rs['data']['code'])) {
                if ($rs['data']['code'] == 200) {
                    $file = storage_path("Errors") . "/{$hashKey}.xlsx";
                    header('Content-Disposition: attachment; filename=' . $file);
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Length: ' . filesize($file));
                    header('Content-Transfer-Encoding: binary');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    readfile($file);
                }

                return $this->response->noContent()
                    ->setContent(['data' => $rs['data']])->setStatusCode($rs['data']['code']);
            } else {
                return $this->response->errorBadRequest($rs['data']['message']);
            }
        }
    }


    /**
     * @param IRequest $request
     *
     * @param IRequest|Request $request
     *
     * @return array|Response
     */
    public function getStatusImport(IRequest $request)
    {
        $hashKey = md5($request->header('Authorization'));

        $totalDone = DB::table('progress_bar')
            ->select(['total', 'value'])
            ->where('transaction', $hashKey)
            ->first();

        return ['status' => true, 'data' => $totalDone];

    }

    /**
     * @param Location $location
     * @param $newStatusCode
     *
     * @return bool
     */
    private function checkInventoryForLocation(Location $location, $newStatusCode)
    {
        // ERROR: change to INACTIVE of location that have inventory
        if ($location->loc_sts_dtl_sts_code !== $newStatusCode
            && $newStatusCode == config("constants.loc_status.INACTIVE")
            && $location->pallet()->get()->count()
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param $warehouseId
     * @param Request $request
     *
     * @return Response|void
     */
    public function export($warehouseId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['warehouseId'] = $warehouseId;
        $type = strtolower(array_get($input, 'type', 'csv'));

        try {
            if (!in_array($type, ['csv', 'pdf'])) {
                throw new \Exception(Message::get("VR005", "(.csv|.pdf)"));
            }

            $locations = $this->locationModel->search($input, [], array_get($input, 'limit', 999999999));

            // Instead of Load Location with = ['locationStatus', 'locationStatusDetail', 'zone', 'locationType']
            $locationStatus = ZoneLocationStatus::select('loc_sts_code', 'loc_sts_name')->get();
            $locationStatus = array_pluck($locationStatus, "loc_sts_name", 'loc_sts_code');

            $locationStatusDetail = LocationStatusDetail::select('loc_sts_dtl_loc_id', 'loc_sts_dtl_reason')->get();
            $locationStatusDetail = array_pluck($locationStatusDetail, "loc_sts_dtl_reason", 'loc_sts_dtl_loc_id');

            $zone = Zone::select('zone_id', 'zone_name')->get();
            $zone = array_pluck($zone, 'zone_name', 'zone_id');

            $locationType = LocationType::select('loc_type_id', 'loc_type_name')->get();
            $locationType = array_pluck($locationType, 'loc_type_name', 'loc_type_id');

            $data[] = [
                'Location Code',
                'Alternative Name',
                'Type',
                'Width',
                'Length',
                'Height',
                'Available Capacity%',
                'Weight Capacity',
                'Min Count',
                'Max Count',
                'Zone',
                'Status',
                'Reason',
            ];

            foreach ($locations as $location) {
                $data[] = [
                    $location->loc_code,
                    $location->loc_alternative_name,
                    $locationType[$location->loc_type_id],
                    $location->loc_width,
                    $location->loc_length,
                    $location->loc_height,
                    $location->loc_available_capacity,
                    $location->loc_weight_capacity,
                    $location->loc_min_count,
                    $location->loc_max_count,
                    !empty($location->loc_zone_id) ? $zone[$location->loc_zone_id] : null,
                    $locationStatus[$location->loc_sts_code],
                    array_get($locationStatusDetail, "{$location->loc_id}", null),
                ];
            }

            SelExport::exportList("Export_Location_List_" . date("Y-m-d", time()), $type, $data);

            return $this->response->noContent();
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     */
    public function printLocBarcode(
        Request $request
    ) {
        $input = $request->getQueryParams();
        $locs = array_get($input, 'locs', '');
        $locsArr=explode(",",$locs);

        //print
        $this->createLPNPdfFile($locsArr);
    }

    /**
     * @param $locsArr
     */
    private function createLPNPdfFile(
        $locsArr
    ) {
        //$pdf = new Mpdf();
        $pdf = new Mpdf(
            '',    // mode - default ''
            [152.4, 101.6],    // format - A4, for example, default '',(WxH)( 6 inch = 152.4 mm
            // X 4 inch = 101.6 mm X)
            0,     // font size - default 0
            '',    // default font family
            5,    // margin_left
            5,    // margin right
            46,     // margin top
            10,    // margin bottom
            1,     // margin header
            1,     // margin footer
            'p' // L - landscape, P - portrait
        );

        $html = (string)view('LocationBarcodePrintoutTemplate', [
            'locsArr' => $locsArr

        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        $pdf->Output("LocationBarcode.pdf", "D");
        //$pdf->Output();
    }

}
