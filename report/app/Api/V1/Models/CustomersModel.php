<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\SelArr;
use Wms2\UserInfo\Data;

class CustomersModel extends AbstractModel
{
    public function __construct(Customer $model = null)
    {
        $this->model = ($model) ?: new Customer();
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {

        $query = $this->make($with);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches'   => [
                'cus_id',
                'cus_code',
                'cus_status',
                'cus_country_id',
                'cus_state_id',
            ],
            'attributes' => $attributes,
        ]);

        $this->sortBuilder($query, $searchAttr);

        $models = $query->paginate($limit);

        return $models;
    }

    /*
    ****************************************************************************
    */

    /**
     * @param $whs_id
     * @param array $attributes
     *
     * @return mixed
     */
    public function customers($whs_id, $attributes = [], $with = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        // Search whs_id
        //$query = DB::table('customer AS c')
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);
//        $cus_ids = $predis->getCustomersByWhs($currentWH);

        $current_whs_id = array_get($userInfo, 'current_whs', 0);
        if (!$current_whs_id) {
            return [];
        }

//        $query = $this->model
//            ->select(['customer.cus_id', 'customer.cus_name'])
//            ->join('invt_smr', 'customer.cus_id', '=', 'invt_smr.cus_id')
//            ->where('invt_smr.whs_id', $whs_id);
//        $query->whereIn('customer.cus_id', $cus_ids);
//
//        $result = $query->groupBy('customer.cus_id')->orderBy('customer.cus_name')->get();

        $query = $this->model
            ->select(['customer.cus_id', 'customer.cus_name'])
            ->join('cus_in_user', 'cus_in_user.cus_id', '=', 'customer.cus_id')
            ->where('cus_in_user.whs_id', $current_whs_id);

        $result = $query->groupBy('customer.cus_id')->orderBy('customer.cus_name')->get();

        return $result;
    }

}
