<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Models;


use Seldat\Wms2\Models\LocationStatusDetail;

class LocationStatusDetailModel extends AbstractModel
{

    /**
     * @var LocationStatusDetail
     */
    protected $model;

    /**
     * LocationStatusDetailModel constructor.
     *
     * @param LocationStatusDetail|null $model
     */
    public function __construct(LocationStatusDetail $model = null)
    {
        $this->model = ($model) ?: new LocationStatusDetail();
    }

    /**
     * @param int $locationId
     *
     * @return int
     */
    public function deleteLocationStatusDetail($locationId)
    {
        return $this->model
            ->where('loc_sts_dtl_loc_id', $locationId)
            ->delete();
    }

    /**
     * Search
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $warehouseZoneModel = new WarehouseZoneModel();
        $query = $this->make($with);
        $attributes = array_filter($attributes);

        if (!empty($attributes['loc_type_id'])) {
            $query->whereIn('loc_type_id', $attributes['loc_type_id']);
        }

        $attributes['loc_zone_id'] = [];

        if (!empty($attributes['zone_type_id'])) {
            $warehouseZone = $warehouseZoneModel
                ->whereIn('zone_type_id', $attributes['zone_type_id'])
                ->get()
                ->toArray();
            $zoneIds = [];

            foreach ($warehouseZone as $zones) {
                if (in_array($zones['zone_id'], $attributes['loc_zone_id'])) {
                    $zoneIds[] = $zones['zone_id'];
                }
            }

            $attributes['loc_zone_id'] = $zoneIds;
        }

        $query->whereIn('loc_zone_id', $attributes['loc_zone_id']);
        $this->model->filterData($query);

        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * Replace an existing model.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function replace(array $data)
    {
        $model = $this->model->newInstance()->findOrNew($data[$this->model->getKeyName()]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    /**
     * @param array $params
     * @param array $with
     *
     * @return mixed
     */
    public function loadBy($params = [], $with = [], $filter = '', $filterCus = false)
    {
        $query = $this->make($with);

        if (!empty($params) && is_array($params)) {
            foreach ($params as $key => $value) {
                $query->where($key, $value);
            }
        }
        $this->model->filterData($query);
        // Get
        $models = $query->get();

        return $models;
    }
}
