<?php

namespace Seldat\Wms2\Models;

use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Inventories extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventory';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'item_id',
        'cus_id',
        'whs_id',
        'cus_code',
        'whs_code',
        'type',
        'upc',
        'des',
        'sku',
        'color',
        'size',
        'pack',
        'uom',
        'in_hand_qty',
        'in_pick_qty',
        'total_qty',
        'created_at',
        'updated_at'
    ];

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

}
