
<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Utils\JWTUtil;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderDtl;
use App\Api\OUTBOUND\Models\LocationModel;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Models\WaveDtlLoc;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Models\Pallet;
use Tymon\JWTAuth\Facades\JWTAuth;
use Namshi\JOSE\JWS;
use Seldat\Wms2\Models\User;

class TestOutbound extends TestCase
{

    use DatabaseTransactions;

    private $DB_ROLLBACK = true;
    protected $whsId;
    protected $cusId;
    protected $wvId;
    private $user;

    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const TOKEN = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0ODg3ODc2ODIsImV4cCI6MTQ4ODg3NDA4MiwibmFtZSI6IkRhbyBUcmFuIiwidXNlcm5hbWUiOiJjc3JkdHIiLCJqdGkiOjc3fQ.dyjXCa9hS8ip1EsxMM_vLQxCh7A9ODg4sz1ryPdm_H8';

    private $_token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0OTAxNzAyNTgsImV4cCI6MTQ5MDI1NjY1OCwibmFtZSI6IkEgQiIsInVzZXJuYW1lIjoiY2l0YWIiLCJqdGkiOjIwfQ.QJbnHtpxJ7FUJKa1nHyVQLmCQqBFdd_qgwRaioleWZY";
    protected $initParams = [
        "whs_id" => 1,
        "cus_id" => 5,
    ];

    public function setUp()
    {
        parent::setUp();

        $this->user = JWTUtil::getPayloadValue('jti');
        $this->whsId = $this->initParams['whs_id'];
        $this->cusId = $this->initParams['cus_id'];
        $this->wvId = self::getWavePickIdDefault();

        $token = str_replace('Bearer ', '', $this->_token);
        JWTAuth::setToken($token);
    }

    public function getWavePickIdDefault()
    {
        $wvId = (new \App\Api\V1\Models\WavePickHdrModel())->getModel()->where([
                    'whs_id' => $this->whsId
                ])->first()->value('wv_id');
        return $wvId;
    }

    public function getWavePickEndPoint()
    {
        return "/outbound/{$this->whsId}/waves/";
    }

    public function getWavePickDetailEndPoint()
    {
        return "/outbound/{$this->whsId}/wave/{$this->wvId}";
    }

    /*
      public function testGetWavePickList()
      {
      $param = [
      $input['whs_id'] = $this->whsId,
      $input['picker'] = $this->user,
      ];

      $response = $this->call(self::GET, $this->getWavePickEndPoint(), $param);
      //print_r(json_decode($response->content()));

      echo "============get wave pick list============\n";
      $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

      $responseData = json_decode($response->getContent(), true);
      $this->assertNotEmpty($responseData);
      }

      public function testGetWavePickDetail()
      {
      $response = $this->call(self::GET, $this->getWavePickDetailEndPoint());
      //print_r(json_decode($response->content()));

      echo "============get wave pick detail ============\n";
      $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

      $responseData = json_decode($response->getContent(), true);
      $this->assertNotEmpty($responseData);
      } */

    public function testUpdateWavePick()
    {
        echo "[STAR] " . __METHOD__ . "\n";

        $data = WavepickDtl::join('cartons', 'cartons.item_id', '=', 'wv_dtl.item_id')
                ->where([
                    'cartons.ctn_sts' => 'AC',
                    'wv_dtl.wv_dtl_sts' => 'NW',
                ])
                ->where('cartons.piece_remain', '<=', DB::raw('`wv_dtl`.`piece_qty` - `wv_dtl`.`act_piece_qty`'))
                ->select(['wv_dtl.*'])
                ->first();

        if (!$data) {
            $msg = "[SKIP] There is no wave pick match to pick.\n";
            echo $msg;
            $this->markTestSkipped($msg);
        }
        $wvDtlObj = WavepickDtl::find($data->wv_dtl_id);
        $itemId = object_get($wvDtlObj, 'item_id', null);
        $wvId = object_get($wvDtlObj, 'wv_id', null);
        $whsId = object_get($wvDtlObj, 'whs_id', null);

        //get carton to pick
        echo "[DATA] item id {$itemId}, wave pick id {$wvDtlObj->wv_dtl_id}\n";
        $ctnObj = (new \App\Api\OUTBOUND\Models\CartonModel())->getModel()->where(
                        [
                            'item_id' => $itemId,
                            'ctn_sts' => "AC",
                        ]
                )
                ->where('piece_remain', '<=', $wvDtlObj->piece_qty - $wvDtlObj->act_piece_qty)
                //->whereNotNull('rfid')
                ->first();

        if (!$ctnObj) {
            $msg = "[SKIP] There is no carton match to pick.\n";
            echo $msg;
            $this->markTestSkipped($msg);
        }

        $rfid = strtoupper(uniqid('CTN-'));
        $ctnObj->update([
            'rfid' => $rfid
        ]);

        $param = [
            "code" => [
                "ctns" => [
                    $ctnObj->rfid,
                ]
            ]
        ];

        $endPoint = "/outbound/{$whsId}/wave/{$wvId}/update";
        $response = $this->call(self::PUT, $endPoint, $param);
        echo "============update wave pick ============\n";
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);
        print_r($responseData);
        $this->assertNotEmpty($responseData);
        echo "[COMP] " . __METHOD__ . "\n";
    }

    /*
     * Test assign carton to order
     * URL:     /outbound/{whsId:[0-9]+}/order/{odrId:[0-9]+}/cartons
     * Method:  PUT
     * Action:  OrderController@putCartonOrder
     */

    public function testPutOrderCartons()
    {
        echo "\n[STAR] testPutOrderCartons\n";
        /*
         * Check data in db
         */
        $odrCtn = OrderCarton::join('odr_dtl', 'odr_dtl.odr_dtl_id', '=', 'odr_cartons.odr_ctn_id')
                ->whereNotNull('odr_cartons.odr_hdr_id')
                ->where('odr_dtl.lot', '>', 0)
                ->select(['odr_cartons.*'])
                ->first();

        if (!$odrCtn) {
            $this->markTestIncomplete("[INCO] No Order Carton to test\n");
        }

        $rfid = strtoupper(uniqid('UNITEST-'));
        echo "[DATA] {$rfid}\n";
        $odrCtn->update([
            'ctnr_rfid' => $rfid
        ]);

        /*
         * Setup param
         */
        $params = [
            'ctns-rfid' => [$rfid]
        ];
        $odrHdrId = 21;
        $url = "/outbound/{$this->whsId}/order/{$odrHdrId}/cartons";
        echo "[CURL] {$url}\n";
        $res = $this->call('PUT', $url, $params, [], [], ['HTTP_Authorization' => $this->_token]);
        $content = $res->content();
        $jsonObj = json_decode($content);

        if (isset($jsonObj->data) && isset($jsonObj->data->status)) {
            $this->assertTrue($jsonObj->data->status, "[FAIL] Update order carton fail:{$jsonObj->data->message}\n");
        } else if (isset($jsonObj->status)) {
            $this->assertTrue($jsonObj->status, "[FAIL] API fail!\n");
        }

        echo "[DONE] testPutOrderCartons\n";
    }

    /*
     * Test Get Cartons of a pallet
     * URL:     /outbound/{whsId:[0-9]+}/order/{odrId:[0-9]+}/cartons
     * Method:  GET
     * Action:  OrderController@putCartonOrder
     */

    public function testGetOrderCartons()
    {
        echo "\n[STAR] testGetOrderCartons\n";
        /*
         * Check data in db
         */
        $mdOrderHdr = OrderHdr::where([
                    'whs_id' => $this->whsId
                ])
                ->first();
        if (!$mdOrderHdr) {

            $this->markTestIncomplete("[INCO] No data to test\n");
        }

        $url = "/outbound/{$this->whsId}/order/{$mdOrderHdr->odr_id}/cartons";
        echo "[RURL] $url\n";
        $res = $this->call('GET', $url);
        $content = $res->content();
        $jsonObj = json_decode($content);
        if (isset($jsonObj->odr_id)) {
            $mess = "[FAIL] Data return incorrect, "
                    . "expect:{$mdOrderHdr->odr_id}, "
                    . "actual: {$jsonObj->odr_id}\n";
            $this->assertEquals($mdOrderHdr->odr_id, $jsonObj->odr_id, $mess);
        } else {
            $this->markTestIncomplete("[INCO] Test fail\n");
        }
        echo "[DONE] testGetOrderCartons\n";
    }

    /*
     * Test Active locaion
     * URL:     /outbound/whs/{whsId:[0-9]+}/wave/{wv_dtl_id:[0-9]+}/active-location
     * Method:  POST
     * Action:  OrderController@putCartonOrder
     */

    public function testActiveLocaion()
    {
        echo "\n[STAR] testActiveLocaion\n";
        /*
         * Check data in db
         */

        $wavepick = WavepickDtl::limit(1)->first();
        $url = "/outbound/whs/{$this->whsId}/wave/{$wavepick->wv_dtl_id}/active-location";
        echo "[RURL] $url\n";
        $res = $this->call('POST', $url);
        $content = $res->content();
        $jsonObj = json_decode($content);

        //dd($jsonObj);
        $this->assertGreaterThan(0, count($jsonObj->data), "[FAIL] No data\n");

        echo "[DONE] testActiveLocaion\n";
    }

    /*
     * Test More locaion
     * URL:     /outbound/whs/{whsId:[0-9]+}/wave/{wv_dtl_id:[0-9]+}/more-location
     * Method:  POST
     * Action:  LocationController@moreLocation
     */

    public function testPostMoreLocaion()
    {
        echo "\n[STAR] testPostMoreLocaion\n";
        /*
         * Find data to test
         */

        $data = DB::table('cartons AS ct')
                ->select([
                    'ct.sku',
                    'ct.size',
                    'ct.color',
                    'ct.lot',
                    'ct.loc_id',
                    'ct.loc_code',
                    'wv_dtl.wv_dtl_id',
                    'location.rfid as loc_rfid',
                    DB::raw('COUNT(1) AS cartons'),
                    DB::raw('SUM(ct.piece_remain) AS avail_qty')
                ])
                ->join('wv_dtl', function ($join) {
                    $join->on('wv_dtl.item_id', '=', 'ct.item_id')
                    ->on('wv_dtl.lot', '=', 'ct.lot');
                })
                ->leftJoin('location', 'ct.loc_id', '=', 'location.loc_id')
                ->where('ct.whs_id', $this->whsId)
                //->where('wv_dtl.wv_dtl_id', $wvDtlId)
                ->where('ct.is_damaged', 0)
                ->where('ct.ctn_sts', 'AC')
                ->where('ct.loc_type_code', 'RAC')
                ->where('ct.is_ecom', 0)
                ->groupBy('ct.loc_id')
                ->orderBy('ct.loc_id')
                ->get();

        $dataArr = [];
        foreach ($data as $row) {
            $dataArr[$row['wv_dtl_id']][] = $row['loc_id'];
        }
        $wpDtlId = 0;
        $row = [];
        foreach ($dataArr as $wpDtlId => $row) {
            if (count($row) > 1) {
                break;
            }
        }

        if ($wpDtlId == 0) {
            echo "[INCO] No data to test\n";
            $this->markTestIncomplete("[INCO] No data to test\n");
        }

        /*
         * Call api
         */
        $url = "/outbound/whs/{$this->whsId}/wave/{$wpDtlId}/more-location";
        echo "[RURL] $url\n";
        $param = [
            'loc_ids' => $row[0]
        ];
        $res = $this->call('POST', $url, $param);
        $content = $res->content();
        $jsonObj = json_decode($content);
        /*
         * Verify
         */
        //dd($jsonObj);
        $this->assertGreaterThan(0, count($jsonObj->data), "[FAILE] No data return\n");
        echo "[DONE] testPostMoreLocaion\n";
    }

    /*
     * Test get pallet location
     * URL:     /outbound/whs/{whs_id}/wave/sku/{wv_dtl_id}
     * Method:  GET
     * Action:  WavePickController@getSuggestPalletLocation
     */

    public function testGetWavePickSKU()
    {
        echo "\n[STAR] testGetWavePickSKU\n";
        /*
         * Find data to test
         */
        $waveLoc = WaveDtlLoc::first();

        /*
         * Call api
         */
        $url = "/outbound/whs/{$this->whsId}/wave/sku/{$waveLoc->wv_dtl_id}";
        echo "[RURL] $url\n";
        $res = $this->call('GET', $url, [], [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);

        /*
         * Verify
         */
        //dd($jsonObj);
        if (!isset($jsonObj->data)) {
            $this->markTestIncomplete("[FAIL] No data return\n");
        }
        $this->assertGreaterThan(0, count($jsonObj->data), "[FAILE] No data return\n");
        echo "[DONE] testGetWavePickSKU\n";
    }

    /*
     * Test Get next sku
     * URL:     /outbound/whs/{whsId:[0-9]+}/wave/{wv_id:[0-9]+}/next-sku/{current_wv_dtl_id:[0-9]+}
     * Method:  GET
     * Action:  WavePickController@getNextSKU
     */

    public function testGetNextSKU()
    {
        echo "\n[STAR] testGetNextSKU\n";
        /*
         * Find data to test
         */
        $waveLoc = WavepickDtl::select(DB::raw("wv_dtl_id, wv_id, count(wv_dtl_id) as num"))
                ->whereIn('wv_dtl_sts', ['NW', 'PK'])
                ->groupBy('wv_id')
                ->orderBy('num', 'desc')
                ->first();
        if ($waveLoc->num < 2) {
            $this->markTestSkipped("[SKIP] No data to test\n");
        }

        /*
         * Call api
         */
        $url = "/outbound/whs/{$this->whsId}/wave/{$waveLoc->wv_dtl_id}/next-sku/{$waveLoc->wv_dtl_id}";
        echo "[RURL] $url\n";
        $res = $this->call('GET', $url, [], [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);
        /*
         * Verify
         */
        //dd($jsonObj);
        if (!isset($jsonObj->data)) {
            $this->markTestIncomplete("[FAIL] No data return\n");
        }
        $this->assertGreaterThan(0, count($jsonObj->data), "[FAILE] No data return\n");
        echo "[DONE] testGetNextSKU\n";
    }

    /*
     * Test list orders
     * URL:     /outbound/whs/{whs_id}/order
     * Method:  GET
     * Action:  OrderController@getListOrder
     */

    public function testGetListOrder()
    {
        echo "\n[STAR] testGetListOrder\n";
        /*
         * Find data to test
         */
        $waveLoc = OrderHdr::select(DB::raw("count(odr_hdr.odr_id) num, whs_id"))
                ->join('customer', 'customer.cus_id', '=', 'odr_hdr.cus_id')
                ->where('odr_sts', Status::getByValue("Picking", "ORDER-STATUS"))
                ->groupBy('odr_hdr.whs_id')
                ->orderBy('num', 'desc')
                ->first()

        ;

        if (!$waveLoc) {
            $this->markTestSkipped("[SKIP] No data to test\n");
        }
        if (isset($waveLoc->num) && $waveLoc->num == 0) {
            $this->markTestSkipped("[SKIP] No data to test\n");
        }
        $whsId = $waveLoc->whs_id;

        /*
         * Call api
         */
        $url = "/outbound/{$whsId}/order";
        echo "[RURL] $url\n";
        $res = $this->call('GET', $url, [], [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);
        /*
         * Verify
         */
        //dd($jsonObj);
        if (!isset($jsonObj->data)) {
            $this->markTestIncomplete("[FAIL] No data return\n");
        }
        $this->assertGreaterThan(0, count($jsonObj->data), "[FAILE] No data return\n");
        echo "[DONE] testGetListOrder\n";
    }

    /*
     * Test assign carton to pallet
     * URL:     /outbound/{whsId:[0-9]+}/pallet/assign-cartons
     * Method:  PUT
     * Action:  PalletController@assignCartonToPallet
     */

    public function testPutAssignCartonToPallet()
    {
        echo "\n[STAR] testPutAssignCartonToPallet\n";
        /*
         * Find data to test
         */

        $pallet = OutPallet::where([
                    'whs_id' => $this->whsId,
                    'out_plt_sts' => 'AC'
                ])
                ->whereNotNull('plt_num')
                ->first();
        if (!$pallet) {
            $this->markTestSkipped("[SKIP] No pallet active\n");
        }

        $pack = PackHdr::where('whs_id', $this->whsId)
                ->whereNotNull('pack_hdr_num')
                ->whereNull('out_plt_id')
                ->first();

        if (!$pack) {
            $this->markTestSkipped("[SKIP] No PACK data to test\n");
        }
        /*
         * Call api
         */
        $url = "/outbound/{$this->whsId}/pallet/assign-cartons";
        echo "[RURL] $url\n";
        $param = [
            'pallet' => $pallet->plt_num,
            'packs' => [$pack->pack_hdr_num]
        ];
        $res = $this->call('PUT', $url, $param, [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);
        echo "[DATA] {$content}\n";
        /*
         * Verify
         */
        //dd($jsonObj);
        if (!isset($jsonObj->status)) {
            $this->fail("[FAIL] Data not return status\n");
        }

        $this->assertEquals(1, $jsonObj->status, "[FAILE] No data return\n");
        echo "[DONE] testPutAssignCartonToPallet\n";
    }

    /*
     * Test Get Cartons of a pallet
     * URL:     /outbound/whs/{whsId:[0-9]+}/pallet/{palletNum}/cartons
     * Method:  GET
     * Action:  PackController@getCartonsOfPallet
     */

    public function testGetCartonOfPallet()
    {
        echo "\n[STAR] testGetCartonOfPallet\n";
        /*
         * Find data to test
         */
        $outPallet = OutPallet::select(DB::raw("count(pack_hdr.pack_hdr_id) num, plt_num, out_pallet.whs_id"))
                ->join('pack_hdr', 'pack_hdr.out_plt_id', '=', 'out_pallet.plt_id')
                //->where('out_pallet.whs_id', $this->whsId)
                ->whereNotNull('plt_num')
                ->groupBy('out_pallet.whs_id')
                ->orderBy('num', 'desc')
                ->first();
        if (!$outPallet) {
            $this->markTestIncomplete("[INCO] No data to test\n");
        }
        /*
         * Call api
         */
        $url = "/outbound/whs/{$outPallet->whs_id}/pallet/{$outPallet->plt_num}/cartons";
        echo "[RURL] $url\n";

        $res = $this->call('GET', $url, [], [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);

        /*
         * Verify
         */
        //dd($jsonObj);
        $this->assertGreaterThan(0, count($jsonObj->data), "[FAIL] No data return\n");
        echo "[DONE] testGetCartonOfPallet\n";
    }

    /*
     * Test update pallet is_movement
     * URL:     /outbound/whs/{whsId:[0-9]+}/pallet/{palletRFID}/update-pallet-movement
     * Method:  PUT
     * Action:  PalletController@updatePalletMove
     */

    public function testPutUpdatePalletIsMovement()
    {
        echo "\n[STAR] testPutUpdatePalletIsMovement\n";
        /*
         * Find data to test
         */
        $pallet = Pallet::select(DB::raw("count(plt_id) num, whs_id, rfid, is_movement"))
                ->where('is_movement', 0)
                ->where('ctn_ttl', '>', 0)
                ->whereNotNull('rfid')
                ->whereNotNull('loc_id')
                ->groupBy('whs_id')
                ->orderBy("num", "desc")
                ->first();
        if (!$pallet) {
            $this->markTestIncomplete("[INCO] No data to test\n");
        }
        /*
         * Call api
         */
        $url = "/outbound/whs/{$pallet->whs_id}/pallet/{$pallet->rfid}/update-pallet-movement";
        echo "[RURL] $url\n";

        $res = $this->call('PUT', $url, [], [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);

        /*
         * Verify
         */
        //dd($jsonObj);
        $newData = Pallet::where(['rfid' => $pallet->rfid])->first();
        $this->assertEquals(1, $newData->is_movement, "[FAIL] is_movement is not set!\n");
        $this->assertTrue($jsonObj->status, "[FAIL] Update failed\n");
        echo "[DONE] testPutUpdatePalletIsMovement\n";
    }

    /*
     * Test update pallet put back
     * URL:     /outbound/whs/{whsId:[0-9]+}/update-pallet-put-back
     * Method:  PUT
     * Action:  PalletController@updatePalletPutBack
     */

    public function notTestPutPalletPullBack()
    {
        echo "\n[STAR] testPutPalletPullBack\n";
        /*
         * Find data to test
         */
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $pallet = Pallet::select(DB::raw("count(pallet.plt_id) num, pallet.whs_id, pallet.rfid p_rfid, location.rfid l_rfid, is_movement, pallet.loc_id, pallet.plt_id"))
                ->join('location', 'location.loc_id', '=', 'pallet.loc_id')
//                ->where('is_movement', 1)
                ->whereNotNull('pallet.rfid')
                ->whereNotNull('location.rfid')
                ->where('ctn_ttl', '>', 0)
                ->where('loc_sts_code', 'AC')
                ->groupBy('pallet.whs_id')
                ->orderBy('num', 'desc')
                ->first();
        /*
         * Simulate data to test
         */
        $pallet->update(["is_movement" => 1]);
        $pallet2 = Pallet::where(['rfid' => $pallet->p_rfid])->first();

        /*
         * Call api
         */
        $url = "/outbound/whs/{$pallet2->whs_id}/update-pallet-put-back";
        echo "[RURL] $url\n";
        $param = [
            'pallet_rfid' => $pallet2->rfid,
            'loc_rfid' => $pallet->l_rfid
        ];
        $res = $this->call('PUT', $url, $param, [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);

        /*
         * Verify
         */
        if (isset($jsonObj->message)) {
            echo "[MESS] Result message: {$jsonObj->message}\n";
        }
        $this->assertTrue($jsonObj->status, "[FAIL] Update failed\n");
        echo "[DONE] testPutPalletPullBack\n";
    }

    /*
     * Test Get a wave pick detail
     * URL:     /outbound/{whsId:[0-9]+}/wave/{wv_id}
     * Method:  GET
     * Action:  WavePickController@getWavePickList
     */

    public function notTestGetWavePickDetail()
    {
        echo "\n[STAR] " . __METHOD__ . "\n";
        /*
         * Find data to test
         */
        $wavePick = Seldat\Wms2\Models\WavepickHdr::first();

        /*
         * Call api
         */
        $url = "/outbound/{$wavePick->whs_id}/wave/{$wavePick->wv_id}";
        echo "[RURL] $url\n";
        $res = $this->call('GET', $url, [], [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);

        /*
         * Verify
         */
        if (isset($jsonObj->data)) {
            $this->assertGreaterThan(0, count($jsonObj->data));
        } else {
            $this->fail("[FAIL] Unknown data structure\n");
        }
        echo "[DONE] " . __METHOD__ . "\n";
    }

    /*
     * Test Put drop pallet shipping lane
     * URL:     /outbound/{whsId:[0-9]+}/shipping-lane/put-pallet
     * Method:  PUT
     * Action:  PalletController@putPalletShippingLane
     */

    public function notTestPutDropPallet()
    {
        echo "\n[STAR] " . __METHOD__ . "\n";
        /*
         * Find data to test
         */
        $pallet = Pallet::select(["pallet.whs_id", "pallet.plt_num", "pallet.plt_id", "location.rfid as loc_rfid"])
                ->join('location', 'location.loc_id', '=', 'pallet.loc_id')
                ->whereNotNull('plt_num')
                ->whereNotNull('location.rfid')
                ->first();
        if (!$pallet) {
            $this->markTestIncomplete("[INCO] No data to test. Please insert pallet before test!\n");
        }
        /*
         * Call api
         */
        $url = "/outbound/{$pallet->whs_id}/shipping-lane/put-pallet";
        echo "[RURL] $url\n";
        $param = [
            'plt_num' => $pallet->plt_num,
            'loc_rfid' => $pallet->loc_rfid
        ];
        $res = $this->call('PUT', $url, $param, [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);

        /*
         * Verify
         */
        echo "[CONT] {$content}\n";
        if (isset($jsonObj->status)) {
            $newPallet = Pallet::find($pallet->plt_id);
            if ($jsonObj->status) {
                //verify data is remove
                $this->assertNull($newPallet, "[FAIL] Data still exist in database!\n");
            } else {
                $this->assertObjectHasAttribute('plt_id', $newPallet, "[FAIL] Delete fail but data lost!\n");
            }
        } else {
            $this->fail("[FAIL] Unknown data structure\n");
        }
        echo "[DONE] " . __METHOD__ . "\n";
    }

    /*
     * Test Get get order by odr_id
     * URL:     /outbound/{whsId:[0-9]+}/order/{odr_id}
     * Method:  GET
     * Action:  OrderController@getOrderByID
     */

    public function testGetOrderByOdrId()
    {
        echo "\n[STAR] " . __METHOD__ . "\n";
        /*
         * Find data to test
         */
        $order = OrderDtl::first();
        if (!$order) {
            $this->markTestIncomplete("[FAIL] No data to test\n");
        }

        /*
         * Call api
         */
        $url = "/outbound/{$order->whs_id}/order/{$order->odr_id}";
        echo "[RURL] $url\n";

        $res = $this->call('GET', $url, [], [], [], ['HTTP_Authorization' => self::TOKEN]);
        $content = $res->content();
        $jsonObj = json_decode($content);

        /*
         * Verify
         */
        echo "[CONT] {$content}\n";
        $this->assertObjectHasAttribute('data', $jsonObj, "[FAIL] Not found data\n");
        echo "[DONE] " . __METHOD__ . "\n";
    }

    public function generateData($num)
    {
        $arr = [];
        for ($i = 0; $i < $num; $i++) {
            $rand = uniqid() . uniqid();
            $randStr = strtoupper(substr($rand, 0, 25));
            $arr[] = "\"{$i}\":\"{$randStr}\"";
        }
        $str = "[\n" . implode(",\n", $arr) . "\n]";

        return $str;
    }

}
