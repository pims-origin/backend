<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Psr\Http\Message\ServerRequestInterface;

use App\Jobs\BillableLaborJob;
use App\Jobs\BillableStorageJob;

class InvoiceBillableJob extends Job
{

    protected $odrId;
    protected $request;
    /**
     * AutoPackJob constructor.
     * @param int $odrId
     * @param ServerRequestInterface $request
     */
    public function __construct($odrId, $request)
    {
        $this->odrId = $odrId;
        $this->request = $request;
    }

    public function handle()
    {
        $client = new Client();

        $apiInvoice = env('API_INVOICE_MASTER');
        
        try {
            if (!$apiInvoice) {
                $this->writeLogment('Order Shipping: Auto create Billable', 'Can\'t found API INVOICE MASTER in ENV.');
                return false;
            }

            $client->request('POST', $apiInvoice . '/v1/billableitem/outbound',
                [
                    'headers'     => ['Authorization' => $this->request->getHeader('Authorization')],
                    'form_params' => [
                        'odr_id' => $this->odrId
                    ]
                ]
            );

            $this->writeLogment('Order Shipping: Auto create Billable', 'Auto create Billable successfully.');

            // Send IMS Billable Labor
            dispatch(new BillableLaborJob($this->odrId));
            // Send IMS Billable Storage
            dispatch(new BillableStorageJob($this->odrId));

        } catch (\Exception $e) {
            $this->writeLogment('Order Ship: Auto create Billable', $e->getMessage());
        }        
    }
}
