<?php

$middleware = ['app', 'user', 'cus', 'whs'];

$router->group([
        'prefix'     => '/rest/wms360/seldat/v1',
        'middleware' => $middleware,
        'namespace'  => 'App\Modules\Items\Controllers'
    ], function () use ($router) {

    // Get list item of customer
    $router->get('/customers/{cusId:[0-9]+}/items', [
        'uses' => 'ItemController@index'
    ]);

    // Show detail of customer
    $router->get('/customers/{cusId:[0-9]+}/items/{itemId:[0-9]+}', [
        'uses' => 'ItemController@read'
    ]);

    // Create item
    $router->post('/customers/{cusId:[0-9]+}/items', [
        'uses' => 'ItemController@create'
    ]);

    // Update item of customer
    $router->put('/customers/{cusId:[0-9]+}/items/{itemId:[0-9]+}', [
        'uses'      => 'ItemController@update'
    ]);
});