<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class AwmsMapping
{
    public function __construct()
    {
    }

    public function handle()
    {
    }

    /**
     * awms_whs_id => whs_whs_id
     * @return array
     */
    public function whsMap()
    {
        return [
            1 => 1, // Burlington
            2 => '', // Toranto
            3 => '', // Los Angeles
            4 => '', // Fontana
            5 => '', // Transportation Way
            6 => '', // Compton
            7 => 2 // Chino
        ];
    }

    public function customerMapping($whs_id, $isMap = 'AWMS')
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        // BUR warehouse

        $configAwms = [
            //'10001' => 'PL',
            '10002' => 'SB',
            '10003' => 'WA',
            //'10004' => 'IA',
            //'10006' => 'RE',
            '10007' => 'CK',
            //'10008' => 'OR',
            '10009' => 'EB',
            //'10010' => 'GP',
            //'10011' => 'GA',
            //'10012' => 'CG',
            //'10013' => 'KK',
            '10014' => 'CT',
            '10015' => 'JO',
            //'10016' => 'SK',
            //'10017' => 'WH',
            //'11190' => 'GL',
            //'11191' => 'EL',
            //'11192' => 'GS',
            //'11193' => 'JF',
            '11194' => 'AS',
            //'11198' => 'GM',
            //'11199' => 'TS',
            //'11213' => 'LL',
            //'11217' => 'ST',
            //'11220' => 'BH',
            //'11222' => 'SL',
            //'11224' => 'TZ',
            //'11226' => 'EF',
            '11231' => 'HG',
            //'11232' => 'RD',
            //'11233' => 'YL',
            //'11237' => 'BU',
            //'11242' => 'TO',
            '11245' => 'AM',
            '11246' => 'UU',
            //'11247' => 'SV',
            //'11250' => 'DNU',
            //'11251' => 'BA',
            //'11253' => 'RG',
            '11254' => 'WE',
            //'11257' => 'UL',
            '11259' => 'APE',
            //'11260' => 'ARS',
            '11261' => 'SBS',
            '11269' => 'VCN',
            //'11276' => 'MDS',
            '11277' => 'VU',
            '11285' => 'IDN',
            //'11291' => 'RAG',
            //'11292' => 'EAG',
            //'11293' => 'RAU',
            //'11294' => 'EAU',
            '11297' => 'SSC',
            '11299' => 'RCL',
            //'11300' => 'AVR',
            //'11301' => 'WPL',
            //'11302' => 'CJO',
            //'11307' => 'SBL',
            //'11308' => 'JEU',
//            /'11311' => 'FCU',
//            '11314' => 'STL',
//            '11315' => 'GMT',
//            '11316' => 'GMS',
//            '11317' => 'PPP',
            '11319' => 'SFH',
            '11320' => 'AMS',
            '11321' => 'MJH',
            '11322' => 'DGL',
            '11325' => 'TSP',
            '11329' => 'LPF',
            '11332' => 'CAF',
            '11333' => 'TBL',

            /*'10005' => 'CNY',
            '11264' => 'DWR',
            '11249' => 'TLI',
            '11290' => '2MD',
            '11324' => 'SLS'*/
        ];

        $cusSyncWms = [
            '10005' => 'CNY',
            '11264' => 'DWR',
            '11249' => 'TLI',
            '11290' => '2MD',
            '11324' => 'SLS'
        ];

        $config = [];
        switch ($isMap) {
            case 'BOTH':
                $config = $configAwms;
                foreach ($cusSyncWms as $cus_id => $cus_code) {
                    $config[$cus_id] = $cus_code;
                }
                break;
            case 'AWMS':
                $config = $configAwms;
                break;
            case 'WMS':
                $config = $cusSyncWms;
                break;
        }

        $customers = DB::table('customer as c')
            ->select('c.cus_code', 'c.cus_id')
            ->join('customer_warehouse AS cw', 'cw.cus_id', '=', 'c.cus_id')
            ->where('cw.whs_id', $whs_id)
            ->whereIn('c.cus_code', $config)
            //->whereRaw(" EXISTS (SELECT * FROM customer_warehouse AS cw WHERE cw.cus_id = c.cus_id AND cw.whs_id = $whs_id)")
            ->get();

        $cusArr = [];
        if ($customers) {
            $arrConfigFlip = array_filter(array_flip($config));
            foreach ($customers as $customer) {
                if (!empty($arrConfigFlip[$customer['cus_code']])) {
                    $cusArr[$arrConfigFlip[$customer['cus_code']]] = $customer['cus_id'];
                }
            }
        }

        return $cusArr;
    }
}
