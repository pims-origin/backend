<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\Status;

class WavePickHdrTransformer extends TransformerAbstract
{

    /**
     * @param WavepickHdr $wvHdr
     *
     * @return array
     */
    public function transform(WavepickHdr $wvHdr)
    {
        // Change request: show number of order & sku in wave pick list
        // $orderHdrs = OrderHdr::where('wv_id', $wvHdr->wv_id)->get();
        // $numOdr = $orderHdrs->count();
        $numOdr = 0;

        if($wvHdr->wv_sts == 'CC'){
            $numSku = $wvHdr->details()->count();
        }else{
            $numSku = OrderDtl::where('wv_id', $wvHdr->wv_id)
                            ->where('odr_id', $wvHdr->odr_id)
                            ->count();
        }

        $order_num = object_get($wvHdr, 'odr_num', null);
        if($order_num){
            $numOdr = 1;
        }

        $cusOrderNum = object_get($wvHdr, 'cus_odr_num', null);
        $cusPo = object_get($wvHdr, 'cus_po', null);

        $cusIds = object_get($wvHdr, 'cus_id', null);
        $cusIds = array_unique($cusIds);

        return [
            'wv_id'      => $wvHdr->wv_id,
            'wv_num'     => $wvHdr->wv_num,
            'wv_sts'     => $wvHdr->wv_sts == 'PK' ? Status::getByKey("WAVEPICK-STATUS", 'NW') : Status::getByKey("WAVEPICK-STATUS", $wvHdr->wv_sts),
            //'wv_sts'     => strtoupper(WAVEPICK_STATUS[$wvHdr->wv_sts]),
            'created_at' => $wvHdr->created_at ? $wvHdr->created_at->format("m/d/Y") : '',
            'user'       => trim(object_get($wvHdr, "userCreated.first_name", null)
                . " " .
                object_get($wvHdr, "userCreated.last_name", null)),
            // User
            'picker_id'          => $wvHdr->picker,
            'picker_name'        => trim(object_get($wvHdr, 'pickerUser.first_name', null) . " " .
                object_get($wvHdr, 'pickerUser.last_name', null)),
            'num_odr'    => $numOdr,
            'num_sku'    => $numSku,
            'cus_id'     => $cusIds,
            'cus_po'        => $cusPo,
            'cus_odr_num'   => $cusOrderNum,
            'odr_num'       => $order_num
        ];
    }
}
