<?php

namespace App\Api\V1\Models;

use App\BaseModel;
use Seldat\Wms2\Utils\JWTUtil;

class BaseInvoiceModel extends BaseModel
{

    public static function boot()
    {
        parent::boot();
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;

        // create a event to happen on updating
        static::updating(function ($table) use ($userId) {
            $columns = $table->getTableColumns();
            if (in_array('updated_by', $columns)) {
                $table->updated_by = $userId;
            }
        });

        // create a event to happen on saving
        static::saving(function ($table) use ($userId) {
            $columns = $table->getTableColumns();
            if (in_array('created_by', $columns)) {
                $table->created_by = $table->created_by ?: $userId;
            }
            if (in_array('updated_by', $columns)) {
                $table->updated_by = $userId;
            }
        });

        static::creating(function ($table) use ($userId) {
            $columns = $table->getTableColumns();
            if (in_array('created_by', $columns)) {
                $table->created_by = $table->created_by ?: $userId;
            }
            if (in_array('updated_by', $columns)) {
                $table->updated_by = $userId;
            }
        });

        // Scope whs_id
//        if ($whsId) {
//            static::addGlobalScope('warehouse', function (Builder $builder) use ($whsId) {
//                if (in_array('whs_id', $builder->getModel()->getFillAble())) {
//                    $builder->where($builder->getModel()->getTable() . '.whs_id', $whsId);
//                }
//            });
//        }
    }

    public function getUnitUomNameAttribute()
    {
        return $this->unitUomName($this->unit_uom);
    }

    public function getTypeNameAttribute()
    {
        return $this->typeName($this->type);
    }

    public function getRangeUomNameAttribute()
    {
        return $this->periodUomName($this->range_uom);
    }

    public function getPeriodUomNameAttribute()
    {
        return $this->periodUomName($this->period_uom);
    }

    protected function unitUomName($key)
    {
        $uomName = config('constants.unit_uom_name');

        if (array_key_exists($key, $uomName)) {
            return $uomName[$key];
        }

        return null;
    }

    protected function typeName($key)
    {
        $typeName = config('constants.charge_type_name');

        if (array_key_exists($key, $typeName)) {
            return $typeName[$key];
        }

        return null;
    }

    protected function periodUomName($key)
    {
        $periodName = config('constants.PERIOD_UOM_NAME');

        if (array_key_exists($key, $periodName)) {
            return $periodName[$key];
        }

        return null;
    }

}
