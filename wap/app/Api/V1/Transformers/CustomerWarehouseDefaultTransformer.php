<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerWarehouse;
use League\Fractal\TransformerAbstract;

class CustomerWarehouseDefaultTransformer extends TransformerAbstract
{
    public function transform(CustomerWarehouse $customerWarehouse)
    {
        $firstName = object_get($customerWarehouse, 'customer.customerDefaultContact.cus_ctt_fname', null);
        $lastName = object_get($customerWarehouse, 'customer.customerDefaultContact.cus_ctt_lname', null);
        $customerName = $firstName . ' ' . $lastName;

        return [
            'cus_id'            => $customerWarehouse->cus_id,
            'whs_id'            => $customerWarehouse->whs_id,
            'whs_name'          => object_get($customerWarehouse, 'warehouse.whs_name', null),
            'whs_code'          => $customerWarehouse->warehouse->whs_code,
            'cus_code'          => $customerWarehouse->customer->cus_code,
            'cus_name'          => $customerWarehouse->customer->cus_name,
            'cus_ctt_full_name' => $customerName,
        ];
    }
}