<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\chargeType;
use League\Fractal\TransformerAbstract;

class ChargeTypeTransformer extends TransformerAbstract
{
    public function transform(ChargeType $chargeType)
    {
        return [
            'chg_type_id'   => $chargeType->chg_type_id,
            'chg_type_code' => $chargeType->chg_type_code,
            'chg_type_name' => $chargeType->chg_type_name,
            'chg_type_des'  => $chargeType->chg_type_des,
            'created_at'    => $chargeType->created_at,
            'updated_at'    => $chargeType->updated_at,
            'deleted_at'    => $chargeType->deleted_at,
        ];
    }
}
