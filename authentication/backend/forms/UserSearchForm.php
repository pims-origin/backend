<?php
namespace backend\forms;

class UserSearchForm extends Form
{
    const ALLOW_SELECT_FIELDS = [
        'user_id',
        'username',
        'email',
        'emp_code',
        'first_name',
        'last_name',
        'created_at',
        'updated_at',
        'status'
    ];

    const SORT_STYLE = [
        'desc' => SORT_DESC,
        'asc'  => SORT_ASC,
    ];

    public $type;
    public $username;
    public $first_name;
    public $last_name;
    public $email;
    public $status;
    public $limit;
    public $page;
    public $sort;
    public $created_at;
    public $role_name;


    public function rules()
    {
        return [
            [
                ['username'],
                'string',
                'max' => 50
            ],
            [
                ['first_name', 'last_name'],
                'string',
                'max' => 35
            ],
            [
                ['created_at'],
                'string'
            ],
            [
                ['email'],
                'string',
                'max' => 35
            ],
            [
                ['page', 'limit'],
                'integer'
            ],
            [
                'page',
                'default',
                'value' => 1,
            ],
            [
                'page',
                'compare',
                'compareValue' => 0,
                'operator'     => '>'
            ],
            [
                'limit',
                'default',
                'value' => 10,
            ],
            [
                'limit',
                'compare',
                'compareValue' => 5,
                'operator'     => '>='
            ],
            [
                'status',
                'string',
                'max' => 2
            ],
            [
                'type',
                'string',
                'max' => 2
            ],
            [
                'sort',
                'validateSorting'
            ],
            [
                'role_name',
                'string'
            ]
        ];
    }

    public function validateSorting($attribute, $params)
    {
        if (!is_array($this->sort)) {
            $this->addError($attribute, 'Sort param is invalid.');
        }
        foreach ($this->sort as $field => $sort) {
            if (!in_array($field, self::ALLOW_SELECT_FIELDS, true)) {
                $this->addError($attribute, 'Sort param is invalid.');

                return $this;
            }
        }
    }

    public function makeUserCriteria()
    {
        if (!$this->validate()) {
            return false;
        }
        $data = [];
        if (!empty($this->email)) {
            $data['email'] = [
                'opr'   => 'like',
                'value' => $this->email
            ];
        }
        if (!empty($this->type)) {
            $data['type'] = [
                'opr'   => '=',
                'value' => $this->type
            ];
        }
        if (!empty($this->created_at)) {
            $data['from_unixtime(created_at, \'%m/%d/%Y\')'] = [
                'opr'   => '=',
                'value' => $this->created_at
            ];
        }
        if (!empty($this->status)) {
            $data['status'] = [
                'opr'   => '=',
                'value' => $this->status
            ];
        }
        if (!empty($this->first_name)) {
            $data['first_name'] = [
                'opr'   => 'like',
                'value' => $this->first_name
            ];
        }
        if (!empty($this->last_name)) {
            $data['last_name'] = [
                'opr'   => 'like',
                'value' => $this->last_name
            ];
        }
        if (!empty($this->username)) {
            $data['username'] = [
                'opr'   => 'like',
                'value' => $this->username
            ];
        }
        if (!empty($this->role_name)) {
            $data['item_name'] = [
                'opr'   => 'like',
                'value' => $this->role_name
            ];
        }

        return $data;
    }

    public function makeOrder()
    {
        if (!$this->sort) {
            return false;
        }
        foreach ($this->sort as $fields => $sort) {
            if ($sort == 'desc') {
                $this->sort[$fields] = SORT_DESC;
            } else {
                $this->sort[$fields] = SORT_ASC;
            }
        }

        return $this;
    }
}
