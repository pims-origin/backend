<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\OrderFlowMasterModel;
use App\Api\V1\Transformers\CustomerMetaTransformer;
use App\Api\V1\Transformers\OrderFlowMasterTransformer;
use App\Api\V1\Validators\CustomerCarrierServiceMetaValidator;
use App\Api\V1\Validators\CustomerMetaValidator;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use Wms2\UserInfo\Data;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;

class CustomerMetaController extends AbstractController
{
    /*
     * @var CustomerModel
     * */
    protected $customerModel;

    /**
     * @var $customerMetaModel
     */
    protected $customerMetaModel;

    /*
     * @var $customerTransformer
     * */
    protected $customerTransformer;

    /**
     * @var $orderFlowMaster
     */
    protected $orderFlowMaster;

    /**
     * @var $orderFlowMasterTransformer
     */
    protected $orderFlowMasterTransformer;

    /**
     * @var $customerMetaValidator
     */
    protected $customerMetaValidator;

    /**
     * @var $customerCarrierServiceMetaValidator
     */
    protected $customerCarrierServiceMetaValidator;

    protected $customerMetaTransformer;

    /**
     * @param CustomerMetaTransformer $customerMetaTransformer
     * @param OrderFlowMasterModel $orderFlowMasterModel
     * @param OrderFlowMasterTransformer $orderFlowMasterTransformer
     * @param CustomerMetaValidator $customerMetaValidator
     * @param CustomerModel $customerModel
     */
    public function __construct(
        CustomerMetaTransformer $customerMetaTransformer,
        OrderFlowMasterModel $orderFlowMasterModel,
        OrderFlowMasterTransformer $orderFlowMasterTransformer,
        CustomerMetaValidator $customerMetaValidator,
        CustomerModel $customerModel,
        CustomerCarrierServiceMetaValidator $customerCarrierServiceMetaValidator

    ) {
        parent::__construct();
        $this->customerMetaTransformer = $customerMetaTransformer;
        $this->customerMetaModel = new CustomerMetaModel();
        $this->orderFlowMasterModel = $orderFlowMasterModel;
        $this->orderFlowMasterTransformer = $orderFlowMasterTransformer;
        $this->customerMetaValidator = $customerMetaValidator;
        $this->customerModel = $customerModel;
        $this->customerCarrierServiceMetaValidator = $customerCarrierServiceMetaValidator;
    }
    
    /**
     * @param $customerId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getOrderFlow($customerId)
    {
        try {
            $arrQualifiers = ['OFF', 'OSF', 'OEF'];

            $arrQualifierMissing = [];
            $i = 0;
            foreach ($arrQualifiers as $arrQualifier) {
                $cusMeta =  $this->customerMetaModel->getFirstWhere(['cus_id' => $customerId, 'qualifier' => $arrQualifier]);

                if ($cusMeta) {
                    $valueJsons = json_decode($cusMeta->value);
                    if ($valueJsons) {
                        $val_json_decode = [];

                        foreach ($valueJsons as $valueJson) {
                            $odrflow = json_decode($valueJson);
                            $val_json_decode[] = [
                                'odr_flow_id' =>  object_get($odrflow, 'odr_flow_id', 0),
                                'step'        => object_get($odrflow, 'step', 0),
                                'flow_code'   => object_get($odrflow, 'flow_code', ''),
                                'odr_sts'     => object_get($odrflow, 'odr_sts', ''),
                                'name'        => object_get($odrflow, 'name', ''),
                                'description' => object_get($odrflow, 'description', ''),
                                'dependency'  => object_get($odrflow, 'dependency', 0),
                                'type'        => object_get($odrflow, 'type', ''),
                                'usage'       => object_get($odrflow, 'usage', 0),
                            ];
                        }
                    }

                    $val_json_decodes[$i]['qualifier'] = $arrQualifier;
                    $val_json_decodes[$i]['value'] = $val_json_decode;
                    $i++;
                }
                else {
                    $masterFlow = $this->orderFlowMasterModel->all()->toArray();
                    $val_json_decode = [];
                    foreach ($masterFlow as $flow) {
                        $val_json_decode[] = [
                            'odr_flow_id' => $flow['odr_flow_id'],
                            'step'        => array_get($flow, 'step', 0),
                            'flow_code'   => array_get($flow, 'flow_code', ''),
                            'odr_sts'     => array_get($flow, 'odr_sts', ''),
                            'name'        => array_get($flow, 'name', ''),
                            'description' => array_get($flow, 'description', ''),
                            'dependency'  => array_get($flow, 'dependency', 0),
                            'type'        => array_get($flow, 'type', ''),
                            'usage'       => array_get($flow, 'usage', 0),
                        ];
                    }

                    $val_json_decodes[$i]['qualifier'] = $arrQualifier;
                    $val_json_decodes[$i]['value'] = $val_json_decode;
                    $i++;
                }
            }

            return $val_json_decodes;

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $cusId
     *
     * @return array|void
     * @throws \Exception
     */
    protected function upsert($request, $cusId)
    {
        // Get data from HTTP
        $input = $request->getParsedBody();

        // Validation
        // $this->customerMetaValidator->validate($input);

        // Check exist of customer
        $CusInfo = $this->customerModel->getFirstWhere([
            'cus_id' => $cusId
        ]);

        if (empty($CusInfo)) {
            throw new \Exception(Message::get("BM017", "Customer"));
        }

        // Check qualifier input
        foreach ($input as $value) {
            if (!in_array($value['qualifier'], ['OFF', 'OSF', 'OEF'])) {
                throw new \Exception("Qualifier must be in [OFF, OSF, OEF]");
            }
        }

        try {
            $arrInsert = [];
            foreach ($input as $key => $value) {
                $valueArrJson = [];
                foreach ($value['value'] as  $getVal) {

                    $orderFlow = [
                        'odr_flow_id' => array_get($getVal, 'odr_flow_id', ''),
                        'step'        => array_get($getVal, 'step', ''),
                        'flow_code'   => array_get($getVal, 'flow_code', ''),
                        'odr_sts'     => array_get($getVal, 'odr_sts', ''),
                        'name'        => array_get($getVal, 'name', ''),
                        'description' => array_get($getVal, 'description', null),
                        'dependency'  => array_get($getVal, 'dependency', 0),
                        'type'        => array_get($getVal, 'type', ''),
                        'usage'       => array_get($getVal, 'usage', '')
                    ];
                    $valueJson = json_encode($orderFlow);
                    array_push($valueArrJson, $valueJson);
                }

                $input[$key]['value'] = json_encode($valueArrJson);


                $params = [
                    'cus_id'    => $cusId,
                    'qualifier' => array_get($input[$key], 'qualifier', ''),
                    'value'     => array_get($input[$key], 'value', ''),
                    
                ];
                $cusMeta = $this->customerMetaModel->getFirstWhere([
                    'cus_id'    => $cusId,
                    'qualifier' => $value['qualifier']
                ]);
                if ($cusMeta) {
                    // Update
                    $this->customerMetaModel->updateWhere(
                        $params,
                        ['cus_id' => $cusId, 'qualifier' => $value['qualifier']]
                    );
                } else {
                    $params['created_at'] = time();
                    $params['created_by'] = Data::getCurrentUserId();
                    $params['updated_at'] = time();
                    $params['updated_by'] = Data::getCurrentUserId();
                    $params['deleted'] = 0;
                    $params['deleted_at'] = 915148800;
                    $arrInsert[] = $params;
                }
            }

            $this->customerMetaModel->getModel()->insert($arrInsert);

            return ["data" => "Successfully"];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function store(Request $request, $cusId)
    {
        return $this->upsert($request, $cusId);
    }

    /**
     * @param Request $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(Request $request, $cusId)
    {
        return $this->upsert($request, $cusId);
    }

    /**
     * @param $customerId
     *
     * @return \Dingo\Api\Http\Response|void
     * @throws \Exception
     */
    public function getInboundConfig($customerId)
    {
        // check exist of customer
        $CusInfo = $this->customerModel->getFirstWhere([
            'cus_id' => $customerId
        ]);
        if (empty($CusInfo)) {
            throw new \Exception(Message::get("BM017", "Customer"));
        }

        try {
            //customerMeta: $customerId and qualifier==IBC
            $val_json_decode = [];
            if ($cusMeta = $this->customerMetaModel->loadBy(['cus_id' => $customerId, 'qualifier' => 'IBC'])->first()) {
                $valueJson = \GuzzleHttp\json_decode($cusMeta->value, true);
                if ($valueJson) {
                    $val_json_decode = [
                        'step'      => array_get($valueJson, 'step', 0),
                        'pallet'    => array_get($valueJson, 'pallet', 0),
                        'lot'       => array_get($valueJson, 'lot', 0),
                        'expire_dt' => array_get($valueJson, 'expire_dt', 0),

                    ];
                }
            }

            return ['data'=> $val_json_decode];


        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $cusId
     *
     * @return array|void
     * @throws \Exception
     */
    protected function upsertInboundConfig($request, $cusId)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->customerMetaValidator->validate($input);

        // check exist of customer
        $CusInfo = $this->customerModel->getFirstWhere([
            'cus_id' => $cusId
        ]);
        if (empty($CusInfo)) {
            throw new \Exception(Message::get("BM017", "Customer"));
        }

        // check qualifier
        if ($input['qualifier'] != 'IBC') {
            throw new \Exception("Qualifier must be IBC");
        }

        //$valueArrJson = [];
        $valueJson = [];
        foreach ($input['value'] as $getVal) {
            $value = [
                'step'      => array_get($getVal, 'step', ''),
                'pallet'    => array_get($getVal, 'pallet', ''),
                'lot'       => array_get($getVal, 'lot', ''),
                'expire_dt' => array_get($getVal, 'expire_dt', ''),

            ];
            $valueJson = json_encode($value);
        }

        $params = [
            'cus_id'    => $cusId,
            'qualifier' => array_get($input, 'qualifier', ''),
            'value'     => $valueJson
        ];
        $paramsUpdate = [
            'value'     => $valueJson
        ];

        try {
            // check exist of cus_id and qualifier == 'IBC
            $CusMeta = $this->customerMetaModel->getFirstWhere([
                'cus_id'    => $cusId,
                'qualifier' => $input['qualifier']
            ]);

            if ($CusMeta) {
                //update
                $this->customerMetaModel->updateWhere(
                    $paramsUpdate,
                    ['cus_id' => $cusId, 'qualifier' => $input['qualifier']]
                );
            } else {
                //create
                $this->customerMetaModel->create($params);
            }

            return ["data" => "successful"];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function storeInboundConfig(Request $request, $cusId)
    {
        return $this->upsertInboundConfig($request, $cusId);
    }

    /**
     * @param Request $request
     * @param $cusId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function updateInboundConfig(Request $request, $cusId)
    {
        return $this->upsertInboundConfig($request, $cusId);
    }

    /**
     * @param $customerId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getUserMeta($customerId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        if (!isset($input['qualifier']) || empty($input['qualifier'])) {
            return $this->response->errorBadRequest(Message::get("BM088", "qualifier"));
        }

        if (!in_array($input['qualifier'], ['CSR', 'PKR'])) {
            return $this->response->errorBadRequest(Message::get("BM081", "qualifier"));
        }

        try {
            if ($cusMeta = $this->customerMetaModel->getFirstWhere(
                [
                    'cus_id'    => $customerId,
                    'qualifier' => $input['qualifier']
                ])
            ) {
                $valueJsons = \GuzzleHttp\json_decode($cusMeta->value);
                $val_json_decodes = [];
                if ($valueJsons) {
                    foreach ($valueJsons as $valueJson) {
                        $val_json_decode = [
                            'user_id'     => object_get(\GuzzleHttp\json_decode($valueJson), 'user_id', 0),
                            'whs_id'      => object_get(\GuzzleHttp\json_decode($valueJson), 'whs_id', 0),
                            'set_default' => object_get(\GuzzleHttp\json_decode($valueJson), 'set_default', 0)
                        ];
                        array_push($val_json_decodes, $val_json_decode);
                    }
                }
                $cusMeta->val_json_decode = $val_json_decodes;

                return $this->response->item($cusMeta, $this->customerMetaTransformer);
            }

            return ["data" => []];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function createCarrierService($cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->customerCarrierServiceMetaValidator->validate($input);

        // check exist of customer
        $CusInfo = $this->customerModel->getFirstWhere([
            'cus_id' => $cusId
        ]);
        if (empty($CusInfo)) {
            throw new \Exception(Message::get("BM017", "Customer"));
        }

        $valueArrJson = [];
        try {

            foreach ($input as $getVal) {

                $valueJsons = json_encode($getVal['value']);
                $params = [
                    'cus_id' => $cusId,
                    'qualifier' => array_get($getVal, 'qualifier', ''),
                    'value' => $valueJsons
                ];
                $CusMeta = $this->customerMetaModel->getFirstWhere([
                    'cus_id' => $cusId,
                    'qualifier' => $getVal['qualifier']
                ]);

                $this->customerMetaModel->refreshModel();
                if ($CusMeta) {
                    //update
                    $this->customerMetaModel->updateWhere(
                        $params,
                        ['cus_id' => $cusId, 'qualifier' => $getVal['qualifier']]
                    );
                } else {
                    //create
                    $this->customerMetaModel->create($params);
                }
            }
            return ["data" => "successful"];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public  function  getCarrierService ($cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        if (!isset($input['qualifier']) || empty($input['qualifier'])) {
            return $this->response->errorBadRequest(Message::get("BM088", "qualifier"));
        }

        if (count(array_diff($input['qualifier'], ['UPS', 'FDX'])) != 0) {
            return $this->response->errorBadRequest(Message::get("BM081", "qualifier"));
        }

        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            if ($cusMeta = $this->customerMetaModel->getModel()
                ->where('cus_id', $cusId)
                ->whereIn('qualifier', $input['qualifier'])->get()
            ) {
//                $cusMeta->val_json_decode = $cusMeta->toArray();
//                dd($cusMeta->val_json_decode);
                return $this->response->collection($cusMeta, $this->customerMetaTransformer);
            }
            return ["data" => []];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}
