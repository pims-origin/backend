<?php
namespace App\Api\V2\Outbound\Order\AssignCartonsWithBarcode\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class OrderCartonModel extends AbstractModel
{

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    public function getPick($wvHdrId, $wvDtlId)
    {
        $query = $this->model
            ->select(['odr_hdr_id', 'odr_dtl_id', 'wv_hdr_id', 'wv_dtl_id', DB::raw("sum(piece_qty) as piece_ttl")])
            ->where('wv_hdr_id', $wvHdrId)
            ->where('wv_dtl_id', $wvDtlId)
            ->groupBy(['odr_hdr_id','odr_dtl_id'])
            ->get();

        return $query;
    }

    public function autoPack($wvHdrId)
    {
        $result = DB::table('odr_hdr as o')
            ->join('odr_hdr_meta as ohm', 'ohm.odr_id', '=', 'o.odr_id')
            ->join('odr_cartons as oc', 'o.odr_id', '=', 'oc.odr_hdr_id')
            ->join('cartons as c', 'c.ctn_id', '=', 'oc.ctn_id')
            ->where('ohm.value', 1)
            ->where('o.odr_sts', 'PD')
            ->where('o.wv_id', $wvHdrId)
            ->groupBy('c.ctn_id')
            ->get()
        ;

        return $result;
    }

    public function sumPieceQty($odrDtlId)
    {
        $result = DB::table('odr_cartons as oc')
                    // ->join('wv_dtl as wd', 'oc.wv_dtl_id', '=', 'wd.wv_dtl_id')
                    ->join('odr_dtl as od', function ($join) {
                        $join->on('od.item_id', '=', 'oc.item_id');
                        $join->on('od.pack', '=', 'oc.pack');
                        $join->on('od.lot', '=', 'oc.lot');
                        // $join->on('od.wv_id', '=', 'oc.wv_hdr_id');
                    })
                    ->select([
                        'oc.whs_id',
                        'oc.cus_id',
                        'od.pack',
                        'od.odr_dtl_id',
                        DB::raw("SUM(oc.piece_qty) as piece_ttl"),
                        'od.item_id',
                        'od.sku',
                        'od.size',
                        'od.color',
                        'od.lot',
                    ])
                    ->where([
                        'oc.deleted' => 0,
                        // 'wd.deleted' => 0,
                        'od.deleted' => 0,
                        'oc.ctn_sts' => 'PD',
                    ])
                    ->whereNull('oc.ctn_rfid')
                    ->whereNull('oc.odr_dtl_id')
                    ->where('od.odr_dtl_id', $odrDtlId)
                    ->groupBy('od.odr_dtl_id')
                    ->first();

        return $result;
    }

    public function getCartonByWvDtl($wvDtlId)
    {
        $query = $this->model->where('wv_dtl_id', $wvDtlId)
                    ->whereNull('odr_dtl_id')
                    ->where('ctn_sts', 'PD')
                    ->get();

        return $query;
    }

    public function getCartonNotByWvDtl($item, $whsId, $cusId)
    {
        $query = $this->model->where('wv_dtl_id', '!=', array_get($item, 'wv_dtl_id'))
                    ->whereNull('odr_dtl_id')
                    ->where('whs_id', $whsId)
                    ->where('cus_id', $cusId)
                    ->where('ctn_sts', 'PD')
                    ->where('item_id', array_get($item, 'item_id'))
                    ->where('pack', array_get($item, 'pack'))
                    ->where('lot', array_get($item, 'lot'))
                    ->get();

        return $query;
    }
}
