<?php
namespace App\Api\V2\BillOfLadings\CreateBOL\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class BaseService
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $inputs = [];


    /**
     * BaseService constructor.
     * @param $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $this->client = new Client([
            'base_uri' => $baseUrl,
            'headers' => [
                'Authorization' => $request->getHeader('Authorization')
            ],
            'http_errors' => false
        ]);
    }

    /**
     * @param $json
     * @param string $type
     * @return array
     */
    public function responseToArray($json, $type = 'data')
    {
        return \json_decode($json, true)[$type];
    }

    /**
     * @param $inputs
     */
    public function init($inputs)
    {
        $this->inputs = $inputs;
    }

}
