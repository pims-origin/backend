<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = [];
if (env('APP_ENV') != 'testing') {
    $middleware = ['trimInput'];
    $middleware[] = 'verifySecret';
    $middleware[] = 'checkWAPSupport';
    $middleware[] = 'setWarehouseTimezone';
    // $middleware[] = 'authorize';
}
$middleware[] = 'rfGun';
// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {

    require(__DIR__ . '/V2/routes.php');
    require(__DIR__ . '/V3/routes.php');
    require(__DIR__ . '/V4/routes.php');
    require(__DIR__ . '/V5/routes.php');
    require(__DIR__ . '/V6/routes.php');

    /*INBOUND PROCESS*/
    $api->group(['prefix' => 'inbound', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['SELDAT WAP API INBOUND'];
        });

        // get customer list by warehouse
        $api->get('/whs/{whsId:[0-9]+}/cus-list', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerWarehouseController@loadBy'
        ]);

        // container list
        $api->get('/whs/{whsId:[0-9]+}/containers',
            [
                'action' => "viewContainer",
                'uses'   => 'ContainerController@search'
            ]
        );

        // container list
        $api->get('/whs/{whsId:[0-9]+}/skus',
            [
                'action' => "viewSku",
                'uses'   => 'AsnController@searchSku'
            ]
        );

        // get loc_code by location rfid
        $api->get('/whs/{whsId:[0-9]+}/get-location/{rfid}',
            [
                'action' => "viewLocation",
                'uses'   => 'LocationController@getLocByRfid'
            ]
        );

        // show location detail
        $api->get('/whs/{whsId:[0-9]+}/location/{locId}',
            [
                'action' => "viewLocation",
                'uses'   => 'LocationController@showLocationDetail'
            ]
        );

        // get location list with  pallet no rfid
        $api->get('/whs/{whsId:[0-9]+}/locationWithPalletNoRFID',
            [
                'action' => "viewLocation",
                'uses'   => 'LocationController@getLocationWithPalletNoRFID'
            ]
        );

        //get ASN list
        /*$api->get('/whs/{whsId:[0-9]+}/asns', [
            'action' => "viewASN",
            'uses'   => 'AsnController@searchCanSort'
        ]);*/

        //get ASN list V1
        $api->get('/whs/{whsId:[0-9]+}/asnsV1', [
            'action' => "viewASN",
            'uses'   => 'AsnController@searchCanSortV1'
        ]);

        // get ASN History
        $api->get('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asn-history/{asnDtlId:[0-9]+}', [
            'action' => "viewASN",
            'uses'   => 'AsnController@getAsnHistory'
        ]);

        //view ASN detail with container_id and asn_id
        $api->get('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asns/{asnID:[0-9]+}/containers/{ctnID:[0-9]+}',
            ['action' => "viewASN", 'uses' => 'AsnController@loadASNDtl']);

        //create virtual carton
        $api->post('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/add-virtual-carton',
            ['action' => "viewASN", 'uses' => 'VirtualCartonController@insertVirtualCarton']);

        //create virtual array cartons
        $api->post('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/add-virtual-cartons',
            ['action' => "viewASN", 'uses' => 'VirtualCartonController@scanCartons']);

        //verify carton rfids that scanned
        $api->post('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/verify-cartons',
            ['action' => "viewASN", 'uses' => 'VirtualCartonController@verifyCartonRfids']);

        // Scan Pallet -update pallet rfid to vtl ctn
        $api->put('/whs/{whsId:[0-9]+}/scan-pallet_bk/',
            ['action' => "saveAssignPallet", 'uses' => 'PalletController@scanPallet']);

        //$api->put('/whs/{whsId:[0-9]+}/scan-pallet/',
          //  ['action' => "saveAssignPallet", 'uses' => 'PalletController@assignCartonsToPallet']);

        $api->put('/whs/{whsId:[0-9]+}/multiple-scan-pallet/',
            ['action' => "saveAssignPallet", 'uses' => 'PalletController@assignCartonsToPalletMultiple']);
//        $api->put('/whs/{whsId:[0-9]+}/scan-pallet/',
//            ['action' => "saveAssignPallet", 'uses' => 'PalletController@scanPallet']);

        //complete ans detail
        $api->put('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asn-detail/{asnDtlId:[0-9]+}/complete-sku',
            ['action' => "viewASN", 'uses' => 'VirtualCartonController@completeASNDetail']);

        //complete ans detail
        $api->put('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asn-detail/{asnDtlId:[0-9]+}/complete-asn-dtl',
            ['action' => "viewASN", 'uses' => 'VirtualCartonController@completeASNDtl']);

        // Update location for pallet, carton
        $api->put('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/location/put-away/put-pallet',
            ['action' => "updateInventory", 'uses' => 'PutAwayController@paPutAway']);

        // Suggest an empty put away location for a pallet
        $api->get('/whs/{whsId:[0-9]+}/location/put-away/get-empty-location',
            ['action' => "viewPutaway", 'uses' => 'PalletController@suggestEmptyPutawayLocation']);

        //will be replaced by get-suggest-location
        /*$api->get('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/location/rack/get-empty-location',
            ['action' => "viewPutaway", 'uses' => 'PalletController@suggestEmptyRackLocation']);*/

        // get empty location by pallet rfid
        $api->get('/whs/{whsId:[0-9]+}/pallet/{rfid:[a-zA-Z0-9]+}/location/rack/get-empty-locations',
            'PalletController@getEmptyLocations');

        $api->get('/whs/{whsId:[0-9]+}/pallet/{rfid}/drop-location','PalletController@simulatePalletOnRack');

        // Update location for pallet, carton
        $api->put('/whs/{whsId:[0-9]+}/location/rack/put-pallet',
            ['action' => "updateInventory", 'uses' => 'PutAwayController@putAway']);

        // pick pallet on put area
        $api->put('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/location/put-away/pick-pallet',
            'PalletController@pickPalletOnPutAwayArea');

        //===============================================================================================
        // Delete virtual carton
        $api->put('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/carton/delete-virtual-carton',
            'VirtualCartonController@deleteVirtualCarton');

        // List virtual carton
        $api->get('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asns/{asnId:[0-9]+}/containers/{ctnrId:[0-9]+}/carton/list-virtual-carton',
            'VirtualCartonController@listVirtualCarton');

        // Show asn detail
        $api->get('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asn/status/{asndtllID}',
            'VirtualCartonController@showByAsnDtl');

        // Set damage carton
        $api->put('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/carton/set-damage-carton',
            'VirtualCartonController@setDamageCarton');

        // List all carton
        $api->get('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/location/list-all-location',
            'LocationController@listAllLocation');

        // Create good receipt => not in use
        $api->post('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asns/{asnId:[0-9]+}/containers/{ctnrId:[0-9]+}/goods-receipts/create-good-receipt',
            'GoodsReceiptController@createGR');

        // Get location status
        $api->get('/whs/{whsId:[0-9]+}/location/get-status-location', 'LocationController@getLocationStatus');

        // Get all gate which are receiving cartons
        $api->get('/whs/{whsId:[0-9]+}/get-receiving-gates', 'VirtualCartonSumController@getReceivingGates');

        // Get receiving ASN list
        $api->get('/whs/{whsId:[0-9]+}/get-receiving-asn', 'AsnController@getReceivingASN');

        // Get vtl_ctn by rfid
        $api->get('/{whsId:[0-9]+}/get-virtual-ctn/{ctnRfid}', 'VirtualCartonController@getVirtualCtnRfid');

        //get list user where warehouse
        $api->get('/{whsId:[0-9]+}/users', 'UserController@getUsersByWarehouseID');

        // Update location RFID
        $api->put('/whs/{whsId:[0-9]+}/location/update-rfid', 'LocationController@updateLocationRFID');

        // Update location RFID
        $api->put('/whs/{whsId:[0-9]+}/location/override/update-rfid', 'LocationController@updateLocationAllowOverrideRFID');

        // get location type
        $api->get('/whs/{whsId:[0-9]+}/locations/type', 'LocationController@getLocationType');

        // get location list
        $api->get('/whs/{whsId:[0-9]+}/locations', 'LocationController@getLocationList');

        // get location list
        $api->get('/whs/{whsId:[0-9]+}/location/rfid/{rfid}', 'LocationController@getLocationListByRFID');

        // get location list
        $api->get('/whs/{whsId:[0-9]+}/location/loc-code/{locCode}', 'LocationController@checkLocationListByLocCode');

        // get empty location by pallet rfid
        $api->get('/whs/{whsId:[0-9]+}/pallet/{rfid}/location/rack/check-pallet-on-rack',
            'PalletController@checkPalletIsOnRack');

        // Delete a lot of virtual carton
        $api->put('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/carton/delete-virtual-cartons',
            'VirtualCartonController@deleteVirtualCartons');

        // Delete a lot of virtual carton by asn dtl id
        $api->put('/whs/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/asn-dtl/{asnDtlId:[0-9]+}/carton/delete-virtual-cartons',
            'VirtualCartonController@deleteVirtualCartonByASNDtl');

        // update a loc set null RFID
        $api->put('/whs/{whsId:[0-9]+}/location/{locCode}/set-null-rfid',
            'LocationController@setNullLocationRFID');

        $api->get('/test-create-grhdr/{asnHdrId:[0-9]+}/{ctnrId:[0-9]+}',
            'TestController@testCreateGRHdr');

        $api->get('/test-create-grdtl/{grHdrId:[0-9]+}/{asnHdrDtlId:[0-9]+}',
            'TestController@testCreateGRDtl');

        $api->get('/test-create-pallet/{grDtlId:[0-9]+}/{grHdrId:[0-9]+}/{pltRfid}',
            'TestController@testCreatePalletIfNotExist');

        $api->post('/test-deleteExistNotInPallet',
            'TestController@testDeleteExistNotInPltRfid');

        $api->post('/test-createCtnIfNotDefined',
            'TestController@testCreateCtnIfNotDefined');

        $api->put('/test-SetRealDamageCarton',
            'TestController@testSetRealDamageCarton');

        $api->put('/test-UpdateVtlCtnGRCreated',
            'TestController@testUpdateVtlCtnGRCreated');

        // get empty location by pallet rfid
        $api->get('/whs/{whsId:[0-9]+}/pallet/{rfid}/check-pallet-exist',
            'PalletController@checkPalletExist');

        // get empty location by pallet rfid
        $api->get('/whs/{whsId:[0-9]+}/cartons/{rfid}/status',
            'CartonController@getStatusCarton');

        // Scan Carton -update carton rfid, and create new pallet... Consolidate_plt
        //$api->put('/whs/{whsId:[0-9]+}/consolidate_plt/',
        //    ['action' => "saveAssignPallet", 'uses' => 'CartonController@consolidatePallet']);

    });

    $api->group(['prefix' => 'outbound', 'namespace' => 'App\Api\OUTBOUND\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['SELDAT WAP API OUTBOUND'];
        });

        // Get wave pick list
        $api->get('/{whsId:[0-9]+}/waves','WavePickController@getWavePickList');

        // Get a wave pick detail
        $api->get('/{whsId:[0-9]+}/wave/{wv_id}','WavePickController@getWavePickList');

        // update wave pick
        $api->put('/{whsId:[0-9]+}/wave/{wv_id}/update', 'PalletController@updateWavePick');

        // assign carton to order
        $api->put('/{whsId:[0-9]+}/order/{odrId:[0-9]+}/cartons', 'OrderController@putCartonOrder');

        // scanned pallet
        $api->put('/whs/{whsId:[0-9]+}/scan-pallet/{pltNum}', 'PalletController@scannedPallet');

        // assign carton to pallet
        $api->put('/{whsId:[0-9]+}/pallet/assign-cartons', 'PalletController@assignCartonToPallet');

        // drop pallet shipping lane
        $api->put('/{whsId:[0-9]+}/shipping-lane/put-pallet', 'PalletController@putPalletShippingLane');

        // Active locaion
        $api->post('/whs/{whsId:[0-9]+}/wave/{wv_dtl_id:[0-9]+}/active-location', 'LocationController@activeLocation');

        $api->post('/whs/{whsId:[0-9]+}/wave/get-location-sku-info', 'LocationController@getLocationWhenWrong');

        // More locaion
        $api->post('/whs/{whsId:[0-9]+}/wave/{wv_dtl_id:[0-9]+}/more-location', 'LocationController@moreLocation');

        // Get Cartons of a pallet
        $api->get('/whs/{whsId:[0-9]+}/pallet/{palletNum}/cartons','PackController@getCartonsOfPallet');

        // Get next sku
        $api->get('/whs/{whsId:[0-9]+}/wave/{wv_id:[0-9]+}/next-sku/{current_wv_dtl_id:[0-9]+}','WavePickController@getNextSKU');

        //copy from outbound backup, list orders
        $api->get('/{whsId:[0-9]+}/order', 'OrderController@getListOrder');

        // get pallet location
        $api->get('/whs/{whsId:[0-9]+}/wave/sku/{wvDtlID: [0-9]+}', 'WavePickController@getSuggestPalletLocation');

        // get order by odr_id
        $api->get('/{whsId:[0-9]+}/order/{odr_id}', 'OrderController@getOrderByID');

        // get order cartons by odr_id
        $api->get('/{whsId:[0-9]+}/order/{odrId:[0-9]+}/cartons', 'OrderController@listAssingedCartons');

        // get order cartons by odr_id
        $api->get('/{whsId:[0-9]+}/get-detail-order/{odrId:[0-9]+}', 'OrderController@getOrderDtl');

        // UNIT-TEST test-order-detail-picked
        $api->get('/test-order-detail-picked/{odrId:[0-9]+}', 'TestController@testUpdatePickOdrDtl');

        // UNIT-TEST test-order-detail-picked
        $api->get('/test-order-picked/{odrId:[0-9]+}', 'TestController@testUpdatePickOdrHdr');

        // UNIT-TEST test-order-detail-picked
        $api->get('/test-order-staging/{odrId:[0-9]+}', 'TestController@testUpdateOdrHdrToStaging');

        // UNIT-TEST test-virtual-carton
        $api->get('/test-virtual-carton-loc/{whsId:[0-9]+}', 'TestController@getVirtualCtn');

        // UNIT-TEST test-virtual-carton
        $api->get('/get-total-pallet/{asn_dtl_id:[0-9]+}', 'TestController@countPalletInVtlCtn');

        // UNIT-TEST test-virtual-carton
        $api->put('/test-update-inventory/{whsId:[0-9]+}/cus/{cusId:[0-9]+}/', 'TestController@updateInventory');

        // UNIT-TEST test-virtual-carton
        $api->get('/test-update-wavepick-picked/{wvHdrId:[0-9]+}/', 'TestController@updateWPHdr');

        $api->get('/test-insert-order-carton/{wvHdrId:[0-9]+}/', 'TestController@testInsertOrderCarton');

        $api->get('/test-lpn/{odrId:[0-9]+}/', 'TestController@testLPN');

        // update pallet is_movement
        $api->put('/whs/{whsId:[0-9]+}/pallet/{palletRFID}/update-pallet-movement', 'PalletController@updatePalletMove');

        // update pallet is_movement
        $api->put('/whs/{whsId:[0-9]+}/update-pallet-put-back', 'PalletController@updatePalletPutBack');

        $api->get('/whs/{whsId:[0-9]+}/order-skus','OrderController@skuListSuggestion');

        //Remove cartons assigned to order
        $api->put('/whs/{whsId:[0-9]+}/order-detail/{odrDtlId:[0-9]+}/remove-carton-from-order','OrderController@removeScannedCartons');

        // Scan Carton -update carton rfid, and create new pallet... Consolidate_plt
        $api->put('/whs/{whsId:[0-9]+}/pallet/{palletRFID}/consolidate/',
            ['action' => "saveAssignPallet", 'uses' => 'CartonController@consolidatePallet']);


    });

    $api->group(['prefix' => 'outbound-test', 'namespace' => 'App\Api\TEST\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['SELDAT WAP API OUTBOUND UNIT TEST'];
        });

        $api->get('/test-log', 'TestController@testLog');
        // UNIT-TEST update pallet is_movement
        $api->put('/test-update-is-movement/{whsId:[0-9]+}/pallet/{palletRFID}', 'TestController@updateOutPallet');
        $api->get('/test-update-evt-put-away', 'TestController@testEvtPutAway');
        $api->get('/test-update-evt-put-away-all-pallet', 'TestController@testEvtPutAwayToLocation');

        $api->put('/whs/{whsId:[0-9]+}/update-pallet-put-back',
            'TestController@updatePalletPutBack');

        $api->get('/whs/{whsId:[0-9]+}/testPutawayEvent',
            'TestController@putawayEventTracking');

        $api->get('/test-rule', 'TestController@testRule');
        $api->get('/test-wave-list', 'TestController@getWaveList');
    });

    $api->group(['prefix' => 'inbound-test', 'namespace' => 'App\Api\TEST\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['SELDAT WAP API INBOUND UNIT TEST'];
        });

        $api->put('/scan-carton', 'TestController@scanCarton');
        $api->put('/scan-pallet', 'TestController@scanPallet');
        $api->put('/complete-asn-dtl', 'InBoundTestController@completeAsnDtl');
        $api->put('/log-test', 'InBoundTestController@logTest');
        $api->put('/test-format-RFID', 'InBoundTestController@testCtnRFID');
        $api->get('/test-generate/{num:[0-9]+}', 'TestController@generateData');
        $api->get('/assign-ctn-plt', 'TestController@assignCtnToPallet');
        $api->get('/test-set-damage-ctn', 'TestController@setDamageCtn');
        $api->get('/generate-data/{num}', 'InBoundTestController@generateData');
        $api->get('/updateLocationSts', 'TestController@updateLocationSts');

    });
});
