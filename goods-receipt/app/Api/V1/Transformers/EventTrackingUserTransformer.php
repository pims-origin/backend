<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\EventTracking;

class EventTrackingUserTransformer extends TransformerAbstract
{
    /**
     * @param EventTracking $eventTracking
     *
     * @return array
     */
    public function transform(EventTracking $eventTracking)
    {
        return [
            'user_id' => object_get($eventTracking, 'user_id', null),
            'username' => object_get($eventTracking, 'username', null),
            'first_name' => object_get($eventTracking, 'first_name', null),
            'last_name' => object_get($eventTracking, 'last_name', null),
            'email' => object_get($eventTracking, 'email', null),
            'emp_code' => object_get($eventTracking, 'emp_code', null),
            'status' => object_get($eventTracking, 'status', null),
            'created_at' => object_get($eventTracking, 'created_at', null),
            'created_at_str' => $eventTracking->created_at->format('Y-m-d'),
            'updated_at' => object_get($eventTracking, 'updated_at', null),
            'updated_at_str' => $eventTracking->updated_at->format('Y-m-d')
        ];

    }

}
