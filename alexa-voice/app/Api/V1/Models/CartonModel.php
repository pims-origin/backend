<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Item;
use Wms2\UserInfo\Data;

class CartonModel extends AbstractModel
{

    protected $model;

    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    /**
     * @param $locIds
     * @param $itemIds
     *
     * @return mixed
     */
    public function getCartonForItem($whsId, $cusId, $itemId, $lot, $algorithm, $limit = 5)
    {
        DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons as C')
            ->join('location as L', 'L.loc_id', '=', 'C.loc_id')
            ->join('pallet as P', 'P.plt_id', '=', 'C.plt_id')
            ->select([
                'L.loc_id as location_id',
                'L.loc_code as location_code',
                DB::raw('COUNT(C.ctn_id) as number_of_carton'),
                DB::raw('CAST(SUM(C.piece_remain) AS SIGNED) as piece_remain'),
                'P.plt_num as pallet_number'
            ])
            ->where('C.whs_id', $whsId)
            ->where('C.cus_id', $cusId)
            ->where('C.ctn_sts', 'AC')
            ->where('C.loc_type_code', 'RAC')
            ->where('C.item_id', (int)$itemId)
            ->where('C.lot', $lot)
            ->where('C.piece_remain', '>', 0)
            ->where([
                'C.is_damaged' => 0,
                'C.deleted' => 0
            ])
            ->whereNull('C.rfid')
            ->groupBy('L.loc_id');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('C.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('C.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('C.created_at', 'ASC');
                break;
        }

        return $query->take($limit)->get();
    }

    /**
     * @param $locIds
     * @param $itemIds
     *
     * @return mixed
     */
    public function getCartonForWavePick($locIds, $itemIds)
    {
        // Wv ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];
        // Item ids
        $itemIds = is_array($itemIds) ? $itemIds : [$itemIds];

        $query = DB::table('cartons as C')
            ->join('location as L', 'L.loc_id', '=', 'C.loc_id')
            ->join('pallet as P', 'P.plt_id', '=', 'C.plt_id')
            ->select([
                'C.item_id',
                'C.lot',
                'L.loc_code',
                DB::raw('COUNT(C.ctn_id) as numCarton'),
                DB::raw('SUM(C.piece_remain) as pieceRemain'),
                'P.plt_num'
            ])
            ->where('C.ctn_sts', 'AC')
            ->where('C.loc_type_code', 'RAC')
            ->whereIn('C.loc_id', $locIds)
            ->whereIn('C.item_id', $itemIds)
            ->where([
                'C.is_damaged' => 0,
                'C.deleted' => 0
            ])
            ->whereNull('C.rfid')
            ->groupBy('L.loc_id')
            ->orderBy('C.created_at');

        return $query->get();
    }

    /**
     * @param array $locIds
     * @param int $itemId
     *
     * @return mixed
     */
    public function getMoreSugLocByWvDtl($locIds, $data, $take = 1)
    {
        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();
        // Loc ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];
        $itemId = (int)$data['item_id'];
        $is_ecom = (int)$data['is_ecom'];


        $cus_id = (new Item())->select('cus_id')->where('item_id', $itemId)->value('cus_id');
        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cus_id);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons')
            ->select([
                'loc_id as location_id',
                'loc_code as location_code',
                DB::raw('COUNT(ctn_id) as number_of_cartons'),
                DB::raw('SUM(piece_remain) as number_of_piece')
            ])
            ->where('is_damaged', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
            ->where('item_id', (int)$itemId)
            ->where('whs_id', (int)$whsId)
            ->where('is_ecom', $is_ecom)
            ->where('lot', $data['lot'])
            ->whereNotIn('loc_id', $locIds)
            ->whereNull('rfid')
            ->groupBy('loc_id');
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        if ($take == 1) {
            return $query->first();
        } else {
            return $query->limit($take)->skip(0)->get();

        }
    }
}
