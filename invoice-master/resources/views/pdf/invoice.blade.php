<html>

<head>
    <style>
        body {
            font-family: sans-serif;
            font-size: 10pt;
        }

        p {
            margin: 0pt;
        }

        table.items {
            border: 0.1mm solid #e8e8e8;
        }

        td {
            vertical-align: top;
        }

        table thead td {
            background-color: #e2e0e0;
            text-align: center;
            /* border-top: 0.1mm solid #e8e8e8; */
            /* border-bottom: 0.1mm solid #e8e8e8; */
            font-variant: small-caps;
        }

        .items td.blanktotal {
            border-top: 0.1mm solid #e8e8e8;
            border-bottom: 0.1mm solid #e8e8e8;
        }

        .items td.totals {
            text-align: right;
            border: 0.1mm solid #e8e8e8;
        }

        .items td.cost {
            text-align: "." center;
        }
    </style>
</head>

<body>
    <!--mpdf
	<htmlpageheader name="myheader">
		<table width="100%">
			<tr>
				<td width="45%" style="color:#0000BB;">
					<img src="/logo-full.png" alt="" width="214" height="88">
				</td>
				<td width="55%" style="text-align: right; text-weight: bold; font-size: 15px">INVOICE
				</td>
			</tr>
		</table>
	</htmlpageheader>
	<htmlpagefooter name="myfooter">
	<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
	Page {PAGENO} of {nb}
	</div>
	</htmlpagefooter>
	<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
	<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
    <table width="100%" style="font-family: serif; margin-bottom: 10px;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <td width="45%" style="border: 0.1mm solid #888888; background-color: #827f7f; color: #e2dede">Ship From</td>
                <td width="30%" style="background-color: #fff;">&nbsp;</td>
                <td width="25%" style="border: 0.1mm solid #888888; background-color: #827f7f; color: #e2dede">Invoide Info</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="45%" style="border: 0.1mm solid #888888; ">
                    Name: Seldat Distribution Inc.
                    <br />Address: 1900 River Road
                    <br />City / State / Zip: Burlington / NJ / 08016
                    <br />Telephone: 0123456789
                    <br />Email: contact@abc.com
                </td>
                <td width="35%">&nbsp;</td>
                <td width="30%" style="border: 0.1mm solid #888888;">
                    Invoice #: {{ $header->inv_num }}
                    <br />Invoice Date: {{ $header->inv_dt_format }}
                    <br />Customer Code: HGP

                </td>
            </tr>
        </tbody>
    </table>
    <table width="100%" style="font-family: serif; margin-bottom: 10px;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <td width="45%" style="border: 0.1mm solid #888888; background-color: #827f7f; color: #e2dede">Bill From</td>
                <td width="10%" style="background-color: #fff;">&nbsp;</td>
                <td width="45%" style="border: 0.1mm solid #888888; background-color: #827f7f; color: #e2dede">Bill To</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="45%" style="border: 0.1mm solid #888888; text-indent: 5px">
                    Name: {{ $header->bill_from_name }}
                    <br />Address: {{ $header->bill_from_add1 ?? $header->bill_from_add1}}
                    <br />City / State / Zip: {{ $header->bill_from_city_name }}/{{ $header->bill_from_state }}/{{ $header->bill_from_zip
                    }}
                    <br />Telephone: 123456789
                    <br />Email: contact@abc.com
                </td>
                <td width="10%">&nbsp;</td>
                <td width="45%" style="border: 0.1mm solid #888888; text-indent: 5px">
                    Name: {{ $header->bill_to_name }}
                    <br />Address: {{ $header->bill_to_add1 ?? $header->bill_to_add1}}
                    <br />City / State / Zip: {{ $header->bill_to_city_name }}/{{ $header->bill_to_state }}/{{ $header->bill_to_zip
                    }}
                    <br />Telephone: 123456789
                    <br />Email: contact@abc.com
                </td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="items" width="100%" style="font-size: 9pt;">
        <thead>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="50%">Description</td>
                <td width="10%">UOM</td>
                <td width="15%">QTY</td>
                <td width="15%">Unit Price</td>
                <td width="15%">Currency</td>
                <td width="15%">Discount</td>
                <td width="15%">Amount</td>
            </tr>
        </thead>
        <tbody>
            <!-- ITEMS HERE -->
            @if (count($details) > 0) @foreach ($details as $item)
            <tr>
                <td align="center">{{ $loop->iteration }}</td>
                <td align="center">{{ $item->description }}</td>
                <td align="center">{{ $item->uom_name }}</td>
                <td align="center">{{ $item->qty }}</td>
                <td align="center">{{ $item->unit_price }}</td>
                <td align="center">{{ $item->unit_price }}</td>
                <td align="center">{{ $item->discount }}</td>
                <td align="center">{{ $item->amount }}</td>
            </tr>
            @endforeach @endif
            <!-- END ITEMS HERE -->
            <tr>
                <td class="blanktotal" colspan="6" rowspan="6"></td>
                <td class="totals">Net Order:</td>
                <td class="totals cost" align="center">{{ $header->subtotal }}</td>
            </tr>
            <tr>
                <td class="totals">Tax:</td>
                <td class="totals cost" align="center">{{ $header->tax }}</td>
            </tr>
            <tr>
                <td class="totals">Discount:</td>
                <td class="totals cost" align="center">{{ $header->discount }}</td>
            </tr>
            <tr>
                <td class="totals">Freight:</td>
                <td class="totals cost" align="center">0%</td>
            </tr>
            <tr>
                <td class="totals">Sales Tax:</td>
                <td class="totals cost" align="center">0%</td>
            </tr>
            <tr>
                <td class="totals">
                    <b>TOTAL INVOICE:</b>
                </td>
                <td class="totals cost" align="center" color="red">
                    <b>{{ $header->total }}</b>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="page" style="margin-top: 50px;">
        {{--Invoice Description--}}
        <div class="row top-buffer">
            <table width="100%">
                <tr style="text-align: center; font-weight: bold; color: #1d53aa">
                    <td style="text-align: center; font-weight: bold; color: #1d53aa">
                        If you have any questions about this invoice, please contact<br \>
                        David Noah (123) 456-0000<br \>
                        <a href="mailto:contact@abc.com">contact@abc.com</a>
                    </td>
                    <td style="text-align: center; font-weight: bold; color: #1d53aa">
						Make all checks payable to <br \>
						Seldat Distribution Inc.
                    </td>
                </tr>
            </table>
        </div>
        <div class="row top-buffer" style="margin-top: 30px;">
            <div class="col-md-12 text-center invoice-description" style="font-weight: bold; text-align: center;">
                <p>Thank You For Your Business!</p>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-md-12" style="font-weight: bold; text-align: center;">
                <p>Please detach the portion below and return it with your payment.</p>
            </div>
        </div>
        <hr style="border-top: dashed 5px;" />
		<p style="text-align: center;">REMITTANCE</p>
        <div class="row top-buffer padding-15">
            <table width="100%">
                <tr>
                    <td width="40%" style="vertical-align: top;">
                        <div style="width: 600px;">
                            <p>Seldat Distribution Inc.</p>
                            <p>1900 River Road</p>
                            <p>Burlington / NJ / 08016</p>
                            <p>Phone: (123) 456-0000</p>
                            <p>Fax: (123) 456-0001</p>
                        </div>
					</td>
					<td width="20%">&nbsp;</td>
                    <td width="40%">
						<table class="items">
							<tbody>
								<tr>
									<td rowspan="6"></td>
									<td class="grey-header">Date</td>
								</tr>
								<tr>
									<td>{{ $header->inv_dt_format }}</td>
								</tr>
								<tr>
									<td class="grey-header">Invoice #</td>
								</tr>
								<tr>
									<td>{{ $header->inv_num }}</td>
								</tr>
								<tr>
									<td class="grey-header">Customer Code</td>
								</tr>
								<tr>
									<td>HGP</td>
								</tr>
								<tr>
									<td style="white-space: nowrap; vertical-align: middle">AMOUNT ENCLOSED</td>
									<td></td>
								</tr>
							</tbody>
						</table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>