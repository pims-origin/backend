<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 8/19/16
 * Time: 11:43 AM
 */

namespace App\Api\OUTBOUND\Models;

use Seldat\Wms2\Models\Customer;


class CustomerModel extends AbstractModel
{
    /**
     * @param Customer $model
     */
    public function __construct(Customer $model = null)
    {
        $this->model = ($model) ?: new Customer();
    }

    /**
     * @param $cusId
     *
     * @return mixed
     */
    public function getCusById($cusId)
    {
        $query = $this->getModel()->where([
            'cus_id' => $cusId
        ])
            ->select('cus_id', 'cus_name', 'cus_status', 'cus_code');

        return $query->first();
    }

}