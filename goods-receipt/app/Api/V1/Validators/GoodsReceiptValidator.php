<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Validators;


class GoodsReceiptValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'ctnr_id'                      => 'required|integer|exists:container,ctnr_id',
            'asn_hdr_id'                   => 'required|integer|exists:asn_hdr,asn_hdr_id',
            'details'                      => 'required|array',
            'details.*.asn_dtl_id'         => 'required|integer|greater_equal_than_zero',
            'details.*.gr_dtl_act_ctn_ttl' => 'integer|greater_equal_than_zero'
        ];
    }
}
