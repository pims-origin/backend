<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CycleHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Utils\Status;

class CycleCountNotificationTransformer extends TransformerAbstract
{
    /*
     * Transform CycleCountNotification
     * 
     * @param object $ccNotice
     * 
     * @return array
     */

    public function transform(CycleCountNotification $ccNotice)
    {
        return [
            'ccn_id'          => $ccNotice->ccn_id,
            'whs_id'          => $ccNotice->whs_id,
            'loc_id'          => $ccNotice->loc_id,
            'loc_code'        => $ccNotice->loc_code,
            'item_id'         => $ccNotice->item_id,
            'sku'             => $ccNotice->sku,
            'size'            => $ccNotice->size,
            'color'           => $ccNotice->color,
            'lot'             => $ccNotice->lot,
            'pack'            => $ccNotice->pack,
            'remain_qty'      => $ccNotice->remain_qty,
            'reason'          => $ccNotice->reason,
            'created_at'      => $ccNotice->created_at->format("m/d/Y"),
            'cc_ntf_sts'      => $ccNotice->cc_ntf_sts,
            'cc_ntf_sts_name' => Status::getByKey("CCN_STATUS", $ccNotice->cc_ntf_sts),
            //'cc_ntf_date'     => $ccNotice->cc_ntf_date,
            'cus_id'          => $ccNotice->cus_id,
            'uom_code'        => $ccNotice->uom_code,
            'uom_name'        => $ccNotice->uom_name,
            'des'             => $ccNotice->des,
            'cycle_dtl_id'    => $ccNotice->cycle_dtl_id,


        ];
    }

}
