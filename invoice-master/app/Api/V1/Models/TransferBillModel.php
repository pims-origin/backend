<?php

namespace App\Api\V1\Models;

use App\BillableHdr;
use App\Api\V1\Models\BillableHdrModel;
use App\Api\V1\Models\BillableDtlModel;
use Illuminate\Support\Facades\DB;

class TransferBillModel extends AbstractModel
{
    protected $billableHdr;
    protected $billableDtl;
    protected $xferHdr;

    public function __construct($xferHdrId)
    {
        $this->billableHdr = new BillableHdrModel;
        $this->billableDtl = new BillableDtlModel;
        $this->xferHdr = $this->getXferHdr($xferHdrId);
    }

    /**
     * @param int $xferHdrId
     * */
    public function getXferHdr($xferHdrId)
    {
        return DB::table('xfer_hdr')->where('xfer_hdr_id', $xferHdrId)->first();
    }

    /*
     * Store Billable for Order
     * */
    public function storeBillable()
    {
        if ($this->xferHdr->xfer_sts == 'CO') {
            $newBillableHdr = new BillableHdr();
            $newBillableHdr->cus_id = $this->xferHdr->cus_id;
            $newBillableHdr->whs_id = $this->xferHdr->whs_id;
            $newBillableHdr->ref_num = $this->xferHdr->xfer_hdr_num;
            $newBillableHdr->ba_from = $this->xferHdr->updated_at;
            $newBillableHdr->ba_to = $this->xferHdr->updated_at;
            $newBillableHdr->ba_ttl = 0;
            $newBillableHdr->amount_ttl = 0;
            $newBillableHdr->ba_type = 'TRA';
            $newBillableHdr->ba_period = 'BLOCK';

            if ($newBillableHdr->save()) {
                $amountTtl = 0;
                $baTtl = 0;
                //get cus_sac for Order
                $cusSACs = $this->getCusSac();
                foreach ($cusSACs as $cusSAC) {
                    $uom = $cusSAC->unit_uom;
                    $rangeUom = $cusSAC->range_uom ?? '';
                    switch ($uom) {
                        case 'PC':
                            $pieceQty = $this->getXferPieceQty();
                            if ($pieceQty > 0) {
                                $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, $pieceQty);
                                $this->billableDtl->refreshModel();
                                $this->billableDtl->create($billableDtlData);
                                $amountTtl += $billableDtlData['amount'];
                                $baTtl++;
                            }

                            break;
                        case 'UNI':
                            $pieceQty = $this->getXferPieceQty();
                            if ($pieceQty > 0) {
                                $billableDtlData = $this->makeBillableDtlData($newBillableHdr, $cusSAC, $pieceQty);
                                $this->billableDtl->refreshModel();
                                $this->billableDtl->create($billableDtlData);
                                $amountTtl += $billableDtlData['amount'];
                                $baTtl++;
                            }

                            break;
                    }
                }
                //Update Billable Header
                $newBillableHdr->amount_ttl = $amountTtl;
                $newBillableHdr->ba_ttl = $baTtl;
                $newBillableHdr->save();
            }
        }
    }

    private function makeBillableDtlData($newBillableHdr, $cusSAC, $qty)
    {
        return [
            'ba_id'         => $newBillableHdr->ba_id,
            'cus_id'        => $newBillableHdr->cus_id,
            'whs_id'        => $newBillableHdr->whs_id,
            'sac_id'        => $cusSAC->sac_id,
            'sac'           => $cusSAC->sac,
            'sac_name'      => $cusSAC->sac_name,
            'ref_num'       => $this->xferHdr->xfer_hdr_num,
            'qty'           => $qty,
            'ba_type'       => 'TRA',
            'unit_price'    => $cusSAC->price,
            'discount'      => $cusSAC->discount,
            'amount'        => $qty * $cusSAC->price * (1 - $cusSAC->discount / 100),
            'description'   => 'xfer',
        ];
    }
    /*
     * Get cus_sac for this Xfer
     * */
    public function getCusSac()
    {
        return DB::table('cus_sac')
            ->where('whs_id', $this->xferHdr->whs_id)
            ->where('cus_id', $this->xferHdr->cus_id)
            ->where('cus_sac.type', 'TRA')
            ->where('cus_sac.deleted', 0)
            ->where('cus_sac.deleted_at', 915148800)
            ->get();
    }

    /*
     * Get piece quantity of Xfer
     * */
    public function getXferPieceQty()
    {
        return DB::table('xfer_dtl')
            ->selectRaw("SUM(act_piece_qty)")
            ->where('xfer_dtl.xfer_hdr_id', $this->xferHdr->xfer_hdr_id)
            ->where('xfer_dtl.deleted', 0)
            ->where('xfer_dtl.xfer_dtl_sts', 'CO')
            ->value('act_piece_qty');
    }
}
