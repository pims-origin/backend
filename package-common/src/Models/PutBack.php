<?php
/**
 * Created by PhpStorm.
 *
 * Date: 26-May-16
 * Time: 09:27
 */

namespace Seldat\Wms2\Models;

class PutBack extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'put_back';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pb_id';

    /**
     * @var array
     */
    protected $fillable = [
        'return_id',
        'return_num',
        'return_dtl_id',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'pallet_id',
        'suggest_loc_id',
        'suggest_loc_code',
        'actual_loc_code',
        'actual_loc_id',
        'is_new',
        'pack_size',
        'updated_by',
        'created_by',
        'pb_sts',
        'sts',
        'ctn_ttl',
        'pack_size'
    ];

    public function pallet()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Pallet', 'pallet_id', 'plt_id');
    }

    public function returnOrderDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ReturnDtl', 'return_dtl_id', 'return_dtl_id');
    }


}
