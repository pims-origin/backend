<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CustomerMetaModel;
use App\InvoiceHeader;
use League\Fractal\TransformerAbstract;

class ReportTransformer extends TransformerAbstract
{

    /**
     * ps:Show SAC
     *
     * @param InvoiceHeader $invoiceHeader
     *
     * @return array
     */
    public function transform(InvoiceHeader $invoiceHeader)
    {
        return [
            'inv_num'      => $invoiceHeader->inv_num,
            'cus_id'       => $invoiceHeader->cus_id,
            'cus_name'     => $invoiceHeader->cus_name,
            'bill_to_add1' => $invoiceHeader->bill_to_add1,
            'bill_to_add2' => $invoiceHeader->bill_to_add2,
            'inv_sts_name' => $invoiceHeader->inv_sts_name,
            'issue_dt'     => $invoiceHeader->issue_dt_format,
            'due_dt'       => $invoiceHeader->due_dt_format,
            'period'       => $invoiceHeader->period,
            'total'        => $invoiceHeader->total,
            'created_by'   => $invoiceHeader->created_name,
            'created_at'   => $invoiceHeader->created_at->format('Y-m-d'),
            'paid_dt'      => $invoiceHeader->paid_dt_format,
            'currency'     => $invoiceHeader->currency ?? 'USD',
        ];
    }
}
