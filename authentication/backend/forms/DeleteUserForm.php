<?php
namespace backend\forms;

class DeleteUserForm extends Form
{
    public $user_ids;

    public function rules()
    {
        return [
            [['user_ids'], 'required'],
        ];
    }
}
