<?php

namespace App\Api\V1\Validators;

class SaveMassContactValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_ctt_fname.*'    => 'required',
            'cus_ctt_lname.*'    => 'required',
            'cus_ctt_email.*'    => 'required|email',
            'cus_ctt_phone.*'    => 'required',
            'cus_ctt_mobile.*'   => 'required',
            'cus_ctt_ext.*'      => 'required',
            'cus_ctt_position'   => '',
            'cus_ctt_department' => '',
        ];
    }
}
