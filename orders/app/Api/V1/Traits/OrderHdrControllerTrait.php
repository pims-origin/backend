<?php

namespace App\Api\V1\Traits;

use App\Jobs\AutoPackJob;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

trait OrderHdrControllerTrait
{
    protected $_allow = [
        'NW'  => 'New',
        'AL'  => 'Allocated',
        'PK'  => 'Picking',
        'PN'  => 'Packing',
        'PA'  => 'Packed',
        'ST'  => 'Staging',
        'PAL' => 'Partial Allocated',
        'PPK' => 'Partial Picking',
        'PIP' => 'Partial Picked',
        'PPA' => 'Partial Packing',
        'PSH' => 'Partial Staging',
    ];

    protected $_notAllow = [
        'SH'  => 'Shipped',
        'PRC' => 'Partial Shipped',
    ];

    public function cancel(Request $request)
    {
        $input = $request->getQueryParams();

        if (empty($input['odr_id'])) {
            return $this->response->errorBadRequest('Input odr_id can not empty');
        }

        $odrId = $input['odr_id'];
        if (intval($odrId) != $odrId) {
            throw new \Exception("Input odr_id is invalid!");
        }

        $odrChk = $this->orderHdrModel->byId($odrId);
        $parentOdrNum = substr($odrChk->odr_num, 0, 14);

        $odrIds = \DB::table('odr_hdr')
            ->where('odr_num', 'LIKE', $parentOdrNum . '%')
            ->where('deleted', 0)
            ->pluck('odr_id');

        $evtTracking = [];
        $now = strtotime(date("Y-m-d", time()));

        try {
            DB::beginTransaction();
            $error = true;
            $cancelOdr = [];
            foreach ($odrIds as $idx => $odrId) {
                $odrObj = $this->orderHdrModel->byId($odrId, ['details']);
                // kiem tra neu order dang la picking
                if(object_get($odrObj, 'odr_sts', '') == 'PK') {
                    $notAssignCarton2Pallet = false;
                    $odrDtls = $odrObj->details()->whereNotNull('wv_id')->get(['item_id', 'wv_id', 'lot', 'pack']);
                    $ctnPicked = 0;
                    foreach ($odrDtls as $odrDtl) {
                        $wvdtl = DB::table('wv_dtl')
                            ->where('item_id', object_get($odrDtl, 'item_id'))
                            ->where('wv_id', object_get($odrDtl, 'wv_id'))
                            ->where('lot', object_get($odrDtl, 'lot'))
                            ->where('pack_size', object_get($odrDtl, 'pack'))
                            ->where('deleted', 0)
                            ->where('wv_dtl_sts', '!=', 'NW')
                            ->select(DB::raw('SUM(ctn_qty) as ctn_qty_ttl'))->first();


                        $ctnPicked += array_get($wvdtl, 'ctn_qty_ttl', 0);
                        if($ctnPicked>0) {
                            $notAssignCarton2Pallet = true;
                        }

                    }
                    if($notAssignCarton2Pallet) {
                        $msg = sprintf('All cartons should be picked and assign to this order first before cancelling');
                        return $this->response->errorBadRequest($msg);
                    }
                }

                if ($this->processCancel($odrObj)) {
                    $error = false;
                    $cancelOdr[] = $odrId;

                    // Remove ShipId
                    $this->removeShipId($odrId);
                } else {
                    continue;
                }

                //event tracking
                $evtTracking[] = [
                    'whs_id'     => $odrObj->whs_id,
                    'cus_id'     => $odrObj->cus_id,
                    'owner'      => $odrObj->odr_num,
                    'evt_code'   => Status::getByKey("event", "ORDER-CANCELED"),
                    'trans_num'  => $odrObj->odr_num,
                    'info'       => sprintf(Status::getByKey("event-info", "ORDER-CANCELED"), $odrObj->odr_num),
                    'created_at' => time(),
                    'created_by' => JWTUtil::getPayloadValue('jti')
                ];

                if (!empty($odrObj->wv_id) && $odrObj->odr_sts != "CC") {
                    $this->changeStatusWavepick($odrObj->wv_id, $odrObj->odr_sts, $cancelOdr);
                }

                $this->wavepickDtlModel->updateWhere(
                    [
                        'deleted'    => 1,
                        'deleted_at' => time()
                    ],
                    [
                        'wv_id'     => $odrObj->wv_id,
                        'piece_qty' => 0
                    ]);

            }

            if ($error) {
                return $this->response->errorBadRequest('Order has been shipped or finalized BOL cannot be cancelled!');
            }

            //update status cancel
            $predis = new Data();
            $userInfo = $predis->getUserInfo();
            $userId = array_get($userInfo, 'user_id', 0);

            $this->orderHdrModel->getModel()
                ->whereIn('odr_id', $cancelOdr)
                ->update([
                    'odr_sts'       => 'CC',
                    'act_cancel_dt' => $now,
                    'updated_by'    => $userId
                ]);

            //Event Tracking
            DB::table('evt_tracking')->insert($evtTracking);

            $this->correctOrder($odrId);
            $this->updateInvetoryOdr($odrId);
            $this->updateInvetoryReportOdr($odrId);

            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return ['data' => 'Order canceled successfully!'];
    }

    private function processCancel($odrObj)
    {
        $odrStatus = $odrObj->odr_sts;
        //$now = strtotime(date("Y-m-d", time()));
        //
        //if ($now > $odrObj->cancel_by_dt) {
        //    throw new \Exception("Can't cancel order '" . $odrObj->odr_num . "' after date: " . date("m/d/Y",
        //            $odrObj->cancel_by_dt));
        //}

        if (!$this->orderHdrModel->validateCancelOdr($odrObj)) {
            return false;
        }

        $isNew = true;
        //Not care BAC
        if ($odrStatus !== "NW") {
            $isNew = false;
        }

        //process revert data
        $this->orderDtlModel->revertOrder($odrObj, $isNew);

        return true;
    }

    public function bolOrders($cusId, $odrIds)
    {
        if (empty($cusId) || empty($odrIds) || !strpos($cusId . $odrIds, ' ') === false) {
            return [];
        }

        $usrInfo = new Data();
        $curWhs = $usrInfo->getCurrentWhs();

        $odrIds = str_replace(',,', ',', trim($odrIds, ','));
        $odrIds = explode(',', $odrIds);
        $rs = DB::table('odr_hdr as oh')
            ->leftJoin('bol_odr_dtl as bod', "bod.odr_id", "=", "oh.odr_id")
            ->join('pack_hdr as ph', "oh.odr_id", "=", "ph.odr_hdr_id")
            ->join('pack_dtl as pd', "ph.pack_hdr_id", "=", "pd.pack_hdr_id")
            ->select('oh.schd_ship_dt', 'oh.odr_id', 'oh.odr_num AS odr_hdr_num', 'oh.cus_odr_num AS cus_odr', 'oh.cus_po',
                'oh.cus_pick_num AS cus_ticket', 'oh.cus_dept_ref AS cus_dept', 'bod.add_shipper_info',
                DB::raw('COUNT(DISTINCT ph.out_plt_id) as plts'),
                DB::raw('COUNT( DISTINCT pd.pack_hdr_id) as pkgs'), DB::raw("SUM(pd.piece_qty) AS units"),
                DB::raw("(SELECT
                            ROUND(
                                sum(
                                    CASE
                                    WHEN c.ctn_pack_size = oc.piece_qty THEN c.weight
                                    ELSE c.weight / c.ctn_pack_size * oc.piece_qty
                                    END
                                ), 2
                            )
                        FROM
                            cartons as c
                        JOIN odr_cartons as oc ON c.ctn_id = oc.ctn_id
                        WHERE
                            oc.odr_hdr_id = oh.odr_id ) AS weight")
            )
            ->where('oh.deleted', 0)
            ->where('ph.deleted', 0)
            ->where('pd.deleted', 0)
            ->where('oh.cus_id', $cusId)
            ->where('oh.whs_id', $curWhs)
            ->whereIn('oh.odr_id', $odrIds)
            ->groupBy('oh.odr_id')
            ->get();

        for($i = 0; $i < count($rs); $i++){
            if(!empty($rs[$i]['schd_ship_dt'])){
                $rs[$i]['schd_ship_dt'] = date('m/d/Y', $rs[$i]['schd_ship_dt']);
            }
            else{
                $rs[$i]['schd_ship_dt'] = '';
            }
        }
        return ['data' => $rs];
    }

    public function bolOrdersSameCustomer($cusId, $odrIds)
    {
        if (empty($cusId)) {
            return [];
        }

        $usrInfo = new Data();
        $curWhs = $usrInfo->getCurrentWhs();

        $odrIds = str_replace(',,', ',', trim($odrIds, ','));
        $odrIds = explode(',', $odrIds);
        $rs = DB::table('odr_hdr as oh')
            ->leftJoin('bol_odr_dtl as bod', "bod.odr_id", "=", "oh.odr_id")
            ->join('pack_hdr as ph', "oh.odr_id", "=", "ph.odr_hdr_id")
            ->join('pack_dtl as pd', "ph.pack_hdr_id", "=", "pd.pack_hdr_id")
            ->select('oh.schd_ship_dt', 'oh.odr_id', 'oh.odr_num AS odr_hdr_num', 'oh.cus_odr_num AS cus_odr', 'oh.cus_po',
                'oh.cus_pick_num AS cus_ticket', 'oh.cus_dept_ref AS cus_dept', 'bod.add_shipper_info',
                DB::raw('COUNT(DISTINCT ph.out_plt_id) as plts'),
                DB::raw('COUNT( DISTINCT pd.pack_hdr_id) as pkgs'), DB::raw("SUM(pd.piece_qty) AS units"),
                DB::raw("(SELECT
                            ROUND(
                                sum(
                                    CASE
                                    WHEN c.ctn_pack_size = oc.piece_qty THEN c.weight
                                    ELSE c.weight / c.ctn_pack_size * oc.piece_qty
                                    END
                                ), 2
                            )
                        FROM
                            cartons as c
                        JOIN odr_cartons as oc ON c.ctn_id = oc.ctn_id
                        WHERE
                            oc.odr_hdr_id = oh.odr_id ) AS weight")
            )
            ->where('oh.deleted', 0)
            ->where('ph.deleted', 0)
            ->where('pd.deleted', 0)
            ->where('oh.cus_id', $cusId)
            ->where('oh.whs_id', $curWhs)
            ->whereNotIn('oh.odr_id', $odrIds)
            ->where('oh.odr_sts', 'RS')
            ->groupBy('oh.odr_id')
            ->get();

        $odrHaveBol = DB::table('bol_odr_dtl')
            ->whereIn('odr_id', array_pluck($rs, 'odr_id'))
            ->where('deleted', 0)
            ->get();
        $odrHaveBol = array_pluck($odrHaveBol, 'odr_id');

        $indexToDel = [];
        for($i = 0; $i < count($rs); $i++){
            if(!empty($rs[$i]['schd_ship_dt'])){
                $rs[$i]['schd_ship_dt'] = date('m/d/Y', $rs[$i]['schd_ship_dt']);
            }
            else{
                $rs[$i]['schd_ship_dt'] = '';
            }
            if(in_array($rs[$i]['odr_id'], $odrHaveBol)){
                $indexToDel[] = $i;
            }
        }
        foreach ($indexToDel as $i){
            unset($rs[$i]);
        }

        return ['data' => array_values($rs)];
    }

    public function bolView($shipId)
    {
        $usrInfo = new Data();
        $curWhs = $usrInfo->getCurrentWhs();

        $ship_to_addr = 'trim(replace(replace(concat(ship_to_name, ", ", ship_to_addr_1, ", ", ifnull(ship_to_addr_2, ""),
            ", ", ship_to_city, ", ", ship_to_state, " ", ship_to_zip, " ", ship_to_country), ", ,", ","), "  ", " "))';

        $query = "
            (SELECT s.cus_id, c.cus_name, c.cus_code, s.bo_label, s.bo_num,
            $ship_to_addr AS ship_to_addr, s.ship_sts, s.whs_id,
                s.ship_to_name, s.ship_to_addr_1, s.ship_to_addr_2, s.ship_to_city, s.ship_to_state, s.ship_to_country, s.ship_to_zip,
                s.bill_to_name, s.bill_to_addr_1, s.bill_to_city, s.bill_to_state, s.bill_to_zip, s.bill_to_country as bill_to_country_code, sc.sys_country_name as bill_to_country_name, s.special_inst,

                s.carrier, s.deli_service, s.party_acc, bcd.cmd_des, bcd.cmd_id, s.trailer_loaded_by,
                s.freight_counted_by, s.freight_charge_terms, s.freight_charge_cost, s.fee_terms, s.cus_accept,
                s.is_attach, s.ship_method, s.trailer_num, s.seal_num, s.scac, s.pro_num, s.ship_from_name
            FROM shipment s
            LEFT JOIN bol_carrier_dtl bcd ON s.ship_id = bcd.ship_id
            LEFT JOIN customer c ON c.cus_id = s.cus_id
            LEFT JOIN system_country sc ON s.bill_to_country = sc.sys_country_code
            WHERE s.deleted = 0 AND s.ship_id = $shipId AND s.whs_id = $curWhs
        )";
        $rs = $this->orderHdrModel->naturalExec($query);

        if (empty($rs)) {
            return [];
        }

        $query = "
            SELECT bod.odr_id, bod.odr_hdr_num, bod.ship_id, bod.cus_odr, bod.cus_po, bod
            .cus_dept, bod
            .cus_ticket,
            bod.plt_qty AS plts, bod.ctn_qty AS pkgs, bod.piece_qty AS units, ROUND(bod.weight,2) as weight, bod.add_shipper_info,
            oh.schd_ship_dt
            FROM bol_odr_dtl bod
            INNER JOIN odr_hdr oh ON oh.odr_id = bod.odr_id
            WHERE bod.ship_id = $shipId and bod.deleted=0
        ";
        $items = $this->orderHdrModel->naturalExec($query);
        for($i = 0; $i < count($items); $i++){
            if(!empty($items[$i]['schd_ship_dt'])){
                $items[$i]['schd_ship_dt'] = date('m/d/Y', $items[$i]['schd_ship_dt']);
            }
            else{
                $items[$i]['schd_ship_dt'] = '';
            }
        }
        $rs[0]['items'] = $items;
        if (strcmp($rs[0]['ship_from_name'], 'Seldat') == 0) {
            $rs[0]['bol_type'] = 0;
        }
        else{
            $rs[0]['bol_type'] = 1;
        }
        return ['data' => $rs[0]];
    }

    /**
     * @param $waveId
     * @param $odrDtl
     */
    private function changeStatusWavepick($waveId, $odrSts, $cancelOdrId)
    {
        $ignoreSts = [
            Status::getByValue('Picked', 'Order-Status'),
            Status::getByValue('Partial Picked', 'Order-Status'),
            Status::getByValue('Packing', 'Order-Status'),
            Status::getByValue('Partial Packing', 'Order-Status'),
            Status::getByValue('Packed', 'Order-Status'),
            Status::getByValue('Partial Packed', 'Order-Status'),
            Status::getByValue('Staging', 'Order-Status'),
            Status::getByValue('Partial Staging', 'Order-Status'),
        ];

        $waveHdr = $this->wavepickHdrModel->getFirstWhere(['wv_id' => $waveId], ['details']);

        if (!empty($waveHdr)) {
            //$waveDtl = $waveHdr->toArray()['details'];
            $isCancel = false;
            if (!in_array($odrSts, $ignoreSts)) {
                //check for cancel wavepick
                $chkWv = \DB::table('odr_hdr')
                    ->where('wv_id', $waveId)
                    ->where('deleted', 0)
                    ->count();

                if ($chkWv > 1) {

                    \DB::table('wv_dtl')
                        ->join('odr_dtl', function ($join) {
                            $join->on('wv_dtl.wv_id', '=', 'odr_dtl.wv_id')
                                ->on('wv_dtl.item_id', '=', 'odr_dtl.item_id')
                                ->on('wv_dtl.lot', '=', 'odr_dtl.lot');
                        })
                        ->where('wv_dtl.wv_id', $waveId)
                        ->whereIn('odr_dtl.odr_id', $cancelOdrId)
                        ->update([
                            'wv_dtl.piece_qty'  => DB::raw('wv_dtl.piece_qty  - odr_dtl.alloc_qty'),
                            'wv_dtl.updated_at' => time(),
                            'wv_dtl.updated_by' => Data::getCurrentUserId()
                        ]);
                } else {
                    $isCancel = true;
                }
            }

            //check for cancel wavepick
            $chkWv = \DB::table('odr_hdr')
                ->where('wv_id', $waveId)
                ->where('deleted', 0)
                ->where('odr_sts', '!=', 'CC')
                ->whereNotIn('odr_id', $cancelOdrId)
                ->count();
            if ($chkWv === 0 || $isCancel) {
                $this->wavepickHdrModel->refreshModel();
                $this->wavepickHdrModel->updateWhere(['wv_sts' => 'CC'], ['wv_id' => $waveId]);
            }

        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function dashboardOdr(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboardOdrs = $this->orderHdrModel->dashboardOdr($input);

            return ['data' => $dashboardOdrs[0]];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function dashboardInprocess(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboardInprocess = $this->orderHdrModel->dashboardInprocess($input);

            return ['data' => $dashboardInprocess[0]];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function removeShipId($odrId)
    {
        // Remove ship_id
        $this->orderHdrModel->updateWhere(['ship_id' => null], ['odr_id' => $odrId]);

        // Remove ship_id in bol_odr_dtl
        //  $this->bolOrderDetailModel->updateWhere(['deleted' => 1, 'deleted_at' => time()], ['odr_id' => $odrId]);
        DB::table('bol_odr_dtl')->where('odr_id', $odrId)->update(['deleted' => 1, 'deleted_at' => time()]);
    }

    /*
     * for update BOL
     */
    public function getOrderByBOL($cusId, $bolId)
    {
        $odrIds = $this->orderHdrModel->getModel()
            ->join('shipment', function ($join) use ($bolId) {
                $join->on('odr_hdr.cus_id', '=', 'shipment.cus_id');
//                $join->on('odr_hdr.ship_to_name', '=', 'shipment.ship_to_name');
//                $join->on('odr_hdr.ship_to_add_1', '=', 'shipment.ship_to_addr_1');
//                $join->on('odr_hdr.ship_to_city', '=', 'shipment.ship_to_city');
//                $join->on('odr_hdr.ship_to_state', '=', 'shipment.ship_to_state');
//                $join->on('odr_hdr.ship_to_zip', '=', 'shipment.ship_to_zip');
//                $join->on('odr_hdr.ship_to_country', '=', 'shipment.ship_to_country');
            })
            ->where([
                'odr_hdr.odr_sts'  => 'ST',
                'odr_hdr.ship_id'  => null,
                'shipment.ship_id' => $bolId,
                'shipment.cus_id'  => $cusId
            ])
            ->pluck('odr_hdr.odr_id')
            ->toArray();

        if (empty($odrIds)) {
            return ['data' => []];
        }
        $odrIds = implode(',', $odrIds);

        return $this->bolOrders($cusId, $odrIds);
    }

    /*
     * Complete order picking
     */
    public function completeOrderPicking($orderId, Request $request)
    {
        //valid exist order
        $orderHdr = $this->orderHdrModel->getFirstBy('odr_id', $orderId);

        if (empty($orderHdr)) {
            return $this->response->errorBadRequest(Message::get("BM017", "Order"));
        }

        if (object_get($orderHdr, 'odr_sts') == 'PK' && object_get($orderHdr, 'odr_sts') != 'BAC') {
            try {
                //valid access whs & cus in order
                $this->chkWhsAndCus($orderHdr['whs_id'], $orderHdr['cus_id']);

                DB::beginTransaction();

                $listOrderDtl = $this->orderDtlModel->findWhere([
                    'odr_id' => $orderId,
                    'whs_id' => $orderHdr['whs_id'],
                    'cus_id' => $orderHdr['cus_id']
                ]);

                //BackOrder
                $backOrderHdr = $this->orderHdrModel->getFirstWhere([
                    'org_odr_id' => $orderId,
                    'whs_id'     => $orderHdr['whs_id'],
                    'cus_id'     => $orderHdr['cus_id']
                ]);
                if (empty($backOrderHdr)) {
                    //Create new Back Order Header
                    $dataBackOrderHdr = [
                        'ship_id'         => $orderHdr['ship_id'],
                        'cus_id'          => $orderHdr['cus_id'],
                        'whs_id'          => $orderHdr['whs_id'],
                        'wv_id'           => $orderHdr['wv_id'],
                        'so_id'           => $orderHdr['so_id'],
                        'odr_num'         => $orderHdr['odr_num'] . '-01',
                        'cus_odr_num'     => $orderHdr['cus_odr_num'],
                        'ref_code'        => $orderHdr['ref_code'],
                        'cus_po'          => $orderHdr['cus_po'],
                        'odr_type'        => 'BAC',
                        'odr_sts'         => Status::getByValue("New", "Order-Status"),
                        'ship_to_name'    => $orderHdr['ship_to_name'],
                        'ship_to_add_1'   => $orderHdr['ship_to_add_1'],
                        'ship_to_add_2'   => $orderHdr['ship_to_add_2'],
                        'ship_to_city'    => $orderHdr['ship_to_city'],
                        'ship_to_state'   => $orderHdr['ship_to_state'],
                        'ship_to_zip'     => $orderHdr['ship_to_zip'],
                        'ship_to_country' => $orderHdr['ship_to_country'],
                        'carrier'         => $orderHdr['carrier'],
                        'odr_req_dt'      => $orderHdr['odr_req_dt'],
                        'req_cmpl_dt'     => $orderHdr['req_cmpl_dt'],
                        'act_cmpl_dt'     => $orderHdr['act_cmpl_dt'],
                        'ship_by_dt'      => $orderHdr['ship_by_dt'],
                        'cancel_by_dt'    => $orderHdr['cancel_by_dt'],
                        'act_cancel_dt'   => $orderHdr['act_cancel_dt'],
                        'ship_after_dt'   => $orderHdr['ship_after_dt'],
                        'shipped_dt'      => $orderHdr['shipped_dt'],
                        'cus_pick_num'    => $orderHdr['cus_pick_num'],
                        'ship_method'     => $orderHdr['ship_method'],
                        'cus_dept_ref'    => $orderHdr['cus_dept_ref'],
                        'csr'             => $orderHdr['csr'],
                        'rush_odr'        => $orderHdr['rush_odr'],
                        'cus_notes'       => $orderHdr['cus_notes'],
                        'in_notes'        => $orderHdr['in_notes'],
                        'hold_sts'        => $orderHdr['hold_sts'],
                        'back_odr_seq'    => 1,
                        'back_odr'        => 0,
                        'org_odr_id'      => $orderHdr['odr_id'],
                        'sku_ttl'         => count($listOrderDtl),
                        'wv_num'          => $orderHdr['wv_num']
                    ];
                    $backOrderHdr = $this->orderHdrModel->create($dataBackOrderHdr);
                } else if ($backOrderHdr->odr_sts != 'NW') {
                    //Create another BackOrder
                    $dataBackOrderHdr = [
                        'ship_id'         => $orderHdr['ship_id'],
                        'cus_id'          => $orderHdr['cus_id'],
                        'whs_id'          => $orderHdr['whs_id'],
                        'wv_id'           => $orderHdr['wv_id'],
                        'so_id'           => $orderHdr['so_id'],
                        'odr_num'         => $orderHdr['odr_num'] . '-02',
                        'cus_odr_num'     => $orderHdr['cus_odr_num'],
                        'ref_code'        => $orderHdr['ref_code'],
                        'cus_po'          => $orderHdr['cus_po'],
                        'odr_type'        => 'BAC',
                        'odr_sts'         => Status::getByValue("New", "Order-Status"),
                        'ship_to_name'    => $orderHdr['ship_to_name'],
                        'ship_to_add_1'   => $orderHdr['ship_to_add_1'],
                        'ship_to_add_2'   => $orderHdr['ship_to_add_2'],
                        'ship_to_city'    => $orderHdr['ship_to_city'],
                        'ship_to_state'   => $orderHdr['ship_to_state'],
                        'ship_to_zip'     => $orderHdr['ship_to_zip'],
                        'ship_to_country' => $orderHdr['ship_to_country'],
                        'carrier'         => $orderHdr['carrier'],
                        'odr_req_dt'      => $orderHdr['odr_req_dt'],
                        'req_cmpl_dt'     => $orderHdr['req_cmpl_dt'],
                        'act_cmpl_dt'     => $orderHdr['act_cmpl_dt'],
                        'ship_by_dt'      => $orderHdr['ship_by_dt'],
                        'cancel_by_dt'    => $orderHdr['cancel_by_dt'],
                        'act_cancel_dt'   => $orderHdr['act_cancel_dt'],
                        'ship_after_dt'   => $orderHdr['ship_after_dt'],
                        'shipped_dt'      => $orderHdr['shipped_dt'],
                        'cus_pick_num'    => $orderHdr['cus_pick_num'],
                        'ship_method'     => $orderHdr['ship_method'],
                        'cus_dept_ref'    => $orderHdr['cus_dept_ref'],
                        'csr'             => $orderHdr['csr'],
                        'rush_odr'        => $orderHdr['rush_odr'],
                        'cus_notes'       => $orderHdr['cus_notes'],
                        'in_notes'        => $orderHdr['in_notes'],
                        'hold_sts'        => $orderHdr['hold_sts'],
                        'back_odr_seq'    => 2,
                        'back_odr'        => 0,
                        'org_odr_id'      => $orderHdr['odr_id'],
                        'sku_ttl'         => count($listOrderDtl),
                        'wv_num'          => $orderHdr['wv_num']
                    ];
                    $backOrderHdr = $this->orderHdrModel->create($dataBackOrderHdr);
                }

                if (!empty($listOrderDtl)) {
                    $hasPickedOdrDtl = false;
                    $notPickedOdrDtls = [];
                    foreach($listOrderDtl as $orderDtl) {
                        $odrDtlPickedQty = $this->orderCartonModel->getOdrDtlPickedQty($orderDtl);
                        if (!empty($odrDtlPickedQty)) {
                            $hasPickedOdrDtl = true;
                            //get wv_dtl of odrDtl
                            $wavePickDtl = $this->wavepickDtlModel->getFirstWhere([
                                'wv_id' => $orderDtl['wv_id'],
                                'item_id' => $orderDtl['item_id'],
                                'whs_id'  => $orderDtl['whs_id'],
                                'cus_id'  => $orderDtl['cus_id']
                            ]);
                            if ($wavePickDtl->wv_dtl_sts != 'PD') {
                                $backOdrQty = $orderDtl['alloc_qty'] - $odrDtlPickedQty['picked_qty'];
                                $backOdrCtnQty = $orderDtl['pack'] > 0 ? ceil($backOdrQty / $orderDtl['pack']) : 0;
                                $wavePickDtl->piece_qty = $wavePickDtl->piece_qty - $backOdrQty;
                                if ($wavePickDtl->piece_qty <= $wavePickDtl->act_piece_qty) {
                                    $wavePickDtl->wv_dtl_sts = 'PD';
                                }
                                $wavePickDtl->save();
                                /*Handle BackOrder*/
                                //Check Back Order Dtl existed or not
                                $backodrDtl = $this->orderDtlModel->getFirstWhere([
                                    'odr_id' => $backOrderHdr['odr_id'],
                                    'item_id' => $orderDtl['item_id'],
                                ]);
                                if (empty($backodrDtl)) {
                                    //Create Back Odr Dtl
                                    $dataBackOrderDtl = [
                                        'odr_id'        => $backOrderHdr['odr_id'],
                                        'whs_id'        => $backOrderHdr['whs_id'],
                                        'cus_id'        => $backOrderHdr['cus_id'],
                                        'item_id'       => $orderDtl['item_id'],
                                        'uom_id'        => $orderDtl['uom_id'],
                                        'color'         => $orderDtl['color'],
                                        'sku'           => $orderDtl['sku'],
                                        'size'          => $orderDtl['size'],
                                        'lot'           => 'Any',
                                        'pack'          => $orderDtl['pack'],
                                        'des'           => $orderDtl['des'],
                                        'qty'           => $backOdrCtnQty,
                                        'piece_qty'     => $backOdrQty,
                                        'cus_upc'       => $orderDtl['cus_upc'],
                                        'itm_sts'       => $orderDtl['itm_sts'],
                                        'back_odr'      => 0,
                                        'alloc_qty'     => 0,
                                        'special_hdl'   => $orderDtl['special_hdl'],
                                        'back_odr_qty'  => 0,
                                        'ship_track_id' => $orderDtl['ship_track_id'],
                                        'sts'           => $orderDtl['sts']
                                    ];
                                    $this->orderDtlModel->refreshModel();
                                    $this->orderDtlModel->create($dataBackOrderDtl);
                                } else {
                                    //Update Back Order Dtl if existed
                                    $backodrDtl->qty = $backodrDtl['qty'] + $backOdrCtnQty;
                                    $backodrDtl->piece_qty = $backodrDtl['piece_qty'] + $backOdrQty;
                                    $backodrDtl->save();
                                }
                                //Return allocated to avail in inventory summary after create back order
                                $this->inventorySummaryModel->returnAllocatedToAvail($orderDtl['item_id'], $orderDtl['lot'], $backOrderHdr['whs_id'], $backOdrQty);
                                //Update order dtl
                                $this->orderDtlModel->updateWhere(
                                    [
                                        'alloc_qty' => $orderDtl['alloc_qty'] - $backOdrQty,
                                        'back_odr_qty' => $orderDtl['back_odr_qty'] + $backOdrQty,
                                        'itm_sts' => 'PD',
                                    ],
                                    [
                                        'odr_dtl_id' => $orderDtl['odr_dtl_id']
                                    ]
                                );
                            }
                        } else {
                            $notPickedOdrDtls[] = $orderDtl;
                        }
                    }
                    // Need at least 1 picked odr_dtl to complete order picking
                    if ($hasPickedOdrDtl) {
                        /*
                         * Case: odr_dtl has not been picked yet
                         * Solution:
                         *      -Remove item from original order
                         *      -Create new back odr_dtl for the item
                         *      -Update inventory smr return allocated to avail
                         *      -Update wv_dtl piece_qty to 0 and status to 'PD'
                         * */
                        if (!empty($notPickedOdrDtls)) {
                            foreach($notPickedOdrDtls as $notPickedOdrDtl) {
                                $odrDtlAllocatedQty = $notPickedOdrDtl['alloc_qty'];
                                //Remove item from original order
                                $notPickedOdrDtl->alloc_qty = 0;
                                $notPickedOdrDtl->save();
                                //Check Back Order Dtl existed or not
                                $notPickedBackodrDtl = $this->orderDtlModel->getFirstWhere([
                                    'odr_id' => $backOrderHdr['odr_id'],
                                    'item_id' => $notPickedOdrDtl['item_id'],
                                ]);
                                if (empty($notPickedBackodrDtl)) {
                                    //Create new back odr_dtl for the item
                                    $dataBackOrderDtl = [
                                        'odr_id'        => $backOrderHdr['odr_id'],
                                        'whs_id'        => $backOrderHdr['whs_id'],
                                        'cus_id'        => $backOrderHdr['cus_id'],
                                        'item_id'       => $notPickedOdrDtl['item_id'],
                                        'uom_id'        => $notPickedOdrDtl['uom_id'],
                                        'color'         => $notPickedOdrDtl['color'],
                                        'sku'           => $notPickedOdrDtl['sku'],
                                        'size'          => $notPickedOdrDtl['size'],
                                        'lot'           => 'Any',
                                        'pack'          => $notPickedOdrDtl['pack'],
                                        'des'           => $notPickedOdrDtl['des'],
                                        'qty'           => $notPickedOdrDtl['qty'],
                                        'piece_qty'     => $notPickedOdrDtl['piece_qty'],
                                        'cus_upc'       => $notPickedOdrDtl['cus_upc'],
                                        'itm_sts'       => 'NW',
                                        'back_odr'      => 0,
                                        'alloc_qty'     => 0,
                                        'special_hdl'   => $notPickedOdrDtl['special_hdl'],
                                        'back_odr_qty'  => 0,
                                        'ship_track_id' => $notPickedOdrDtl['ship_track_id'],
                                        'sts'           => $notPickedOdrDtl['sts']
                                    ];
                                    $this->orderDtlModel->refreshModel();
                                    $this->orderDtlModel->create($dataBackOrderDtl);
                                } else {
                                    //Update Back Order Dtl if existed
                                    $notPickedBackodrDtl->qty = $notPickedBackodrDtl['qty'] + $notPickedOdrDtl['qty'];
                                    $notPickedBackodrDtl->piece_qty = $notPickedBackodrDtl['piece_qty'] + $notPickedOdrDtl['piece_qty'];
                                    $notPickedBackodrDtl->save();
                                }
                                //Update inventory smr return allocated to avail
                                $this->inventorySummaryModel->returnAllocatedToAvail($notPickedOdrDtl['item_id'], $notPickedOdrDtl['lot'], $backOrderHdr['whs_id'], $odrDtlAllocatedQty);
                                //Update wv_dtl piece_qty to 0 and status to 'PD'
                                $wavePickDtl = $this->wavepickDtlModel->getFirstWhere([
                                    'wv_id'     => $notPickedOdrDtl['wv_id'],
                                    'item_id'   => $notPickedOdrDtl['item_id'],
                                    'whs_id'    => $notPickedOdrDtl['whs_id'],
                                    'cus_id'    => $notPickedOdrDtl['cus_id']
                                ]);
                                if ($wavePickDtl) {
                                    $wavePickDtl->piece_qty = 0;
                                    $wavePickDtl->wv_dtl_sts = 'PD';
                                    $wavePickDtl->save();
                                }

                            }
                        }
                        //Update Odr Hdr
                        $orderHdr->odr_sts = 'PD';
                        $orderHdr->back_odr = 1;
                        $orderHdr->save();
                    } else {
                        return $this->response->errorBadRequest("The order has not been picked yet.");
                    }
                }

                //Update status in wv_hdr
                $validComplete = true;
                $listWavepickDtl = $this->wavepickDtlModel->findWhere([
                    'wv_id'   => $orderHdr['wv_id'],
                    'whs_id'  => $orderHdr['whs_id'],
                    'cus_id'  => $orderHdr['cus_id']
                ]);
                foreach ($listWavepickDtl as $wvDtl) {
                    if ($wvDtl->piece_qty != $wvDtl->act_piece_qty) {
                        $validComplete = false;
                    }
                }
                if ($validComplete) {
                    $this->wavepickHdrModel->updateWhere(
                        [
                            'wv_sts' => 'CO'
                        ],
                        [
                            'wv_id' => $orderHdr['wv_id'],
                            'whs_id' => $orderHdr['whs_id']
                        ]
                    );
                }

                $this->correctOrder($orderId);

                DB::commit();
                if (!empty($orderHdr->wv_id)) {
                    dispatch(new AutoPackJob($orderHdr->wv_id, $request));
                }
                return ['data' => 'Complete Order Picking Successfully!'];
            } catch (\PDOException $e) {
                return $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
                );
            } catch (\Exception $e) {
                return $this->response->errorBadRequest(
                    SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
                );
            }
        }
    }

    public function revert($odrId)
    {
        $whsId = Data::getCurrentWhsId();

        //check order pick pallet
        $pickPallet = $this->orderHdrMetaModel->getValue($odrId, 'PPL');
        if ($pickPallet) {
            return $this->response->errorBadRequest("Order pick pallet can't revert.");
        }

        //check order valid to revert
        $checkOdr = OrderHdr::select([
            'odr_hdr.wv_id',
            'odr_hdr.odr_sts',
            DB::raw('COALESCE( SUM( odr_dtl.picked_qty), 0 ) as picked_qty')
        ])
            ->join('odr_dtl', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_hdr.odr_id', $odrId)
            ->where('odr_hdr.whs_id', $whsId)
            ->groupBy('odr_hdr.odr_id')
            ->first()->toArray();

        if (empty($checkOdr['odr_sts'])) {
            return $this->response->errorBadRequest('Order is not existed');
        }

        if (!in_array($checkOdr['odr_sts'], ['PK', 'AL'])) {
            return $this->response->errorBadRequest('Order status invalid to revert (picking, allocate)');
        }

        if ($checkOdr['picked_qty'] > 0) {
            return $this->response->errorBadRequest('Order has a wavepick. Please use cancel feature');
        }

        //check wv multiple orders
        $chkWvIsMultiple = OrderHdr::where('wv_id', (int)$checkOdr['wv_id'])
            ->count('odr_id');

        $orderHdr = OrderHdr::where('odr_id', $odrId)
            ->where('whs_id', $whsId)
            ->first();

        if (empty($orderHdr)) {
            return $this->response->errorBadRequest('Order is not existed');
        }

        //process revert odr
        try {
            DB::beginTransaction();

            //update ord_hdr
            $orderHdr->wv_id = null;
            $orderHdr->odr_sts = 'NW';
            $orderHdr->save();

            // 1 order => cancel wv
            if ($chkWvIsMultiple == 1) {

                $checkWv = WavepickHdr::where('wv_hdr.wv_id', $checkOdr['wv_id'])
                    ->where('wv_hdr.whs_id', $whsId);
                $checkWv1 = $checkWv;
                $actPieceTtl = $checkWv1->join('wv_dtl', 'wv_dtl.wv_id', '=','wv_hdr.wv_id')
                    ->sum('wv_dtl.act_piece_qty');
                if ($actPieceTtl) {
                    return $this->response->errorBadRequest('Order has a wavepick is picking. Please use cancel feature');
                }

                // $checkWv->update(['wv_hdr.wv_sts' => 'CC']);
                 WavepickHdr::where('wv_hdr.wv_id', $checkOdr['wv_id'])
                    ->where('wv_hdr.whs_id', $whsId)
                    ->update(['wv_hdr.wv_sts' => 'CC']);
            }
            // > 1 order => reduce alloc qty
            if ($chkWvIsMultiple > 1) {
                DB::table('odr_dtl')->join('wv_dtl', function ($join) {
                    $join->on('odr_dtl.wv_id', '=', 'wv_dtl.wv_id');
                    $join->on('odr_dtl.item_id', '=', 'wv_dtl.item_id');
                    $join->on('odr_dtl.lot', '=', 'wv_dtl.lot');

                })
                    ->where([
                        'odr_dtl.odr_id'  => $odrId,
                        'wv_dtl.deleted'  => 0,
                        'odr_dtl.deleted' => 0
                    ])
                    ->update([
                        'wv_dtl.piece_qty'  => DB::raw('IF(wv_dtl.piece_qty < odr_dtl.alloc_qty, 0, wv_dtl.piece_qty - odr_dtl.alloc_qty)'),
                        'wv_dtl.ctn_qty'    => DB::raw('wv_dtl.ctn_qty - (odr_dtl.alloc_qty / odr_dtl.pack)'),
                        'wv_dtl.updated_by' => Data::getCurrentUserId(),
                        'wv_dtl.updated_at' => time(),
                    ]);
            }
            // delete wv_dtl which piece_qty = 0
            if ($chkWvIsMultiple != 0) {
                WavepickDtl::where([
                    'piece_qty' => 0,
                    'wv_id'     => $checkOdr['wv_id']
                ])
                    ->update(['deleted' => 1, 'deleted_at' => time()]);
            }

            if ($checkOdr['wv_id']) {
                $this->wavepickHdrModel->updatePicked($checkOdr['wv_id']);
                $this->wavepickDtlModel->updatedDetailsPicked($checkOdr['wv_id']);
            }

            $this->updateInvetoryOdr($odrId);

            //update inventory: increase avail and decrease alloc
            DB::table('invt_smr')
                ->join('odr_dtl', function ($join) use ($odrId) {
                    $join->on('odr_dtl.item_id', '=', 'invt_smr.item_id');
                    $join->on('odr_dtl.lot'    , '=', 'invt_smr.lot');
                    $join->on('odr_dtl.whs_id' , '=', 'invt_smr.whs_id');
                })
                ->where([
                    'odr_dtl.odr_id'  => $odrId,
                    'odr_dtl.deleted' => 0,
                    'invt_smr.whs_id' => $whsId,
                ])
                ->update([
                    'invt_smr.allocated_qty' => DB::raw('IF(invt_smr.allocated_qty < odr_dtl.alloc_qty, 0, invt_smr.allocated_qty - odr_dtl.alloc_qty)'),
                    'invt_smr.avail'         => DB::raw('invt_smr.avail + odr_dtl.alloc_qty'),
                    'odr_dtl.alloc_qty'      => 0,
                    'odr_dtl.wv_id'          => null,
                    'invt_smr.updated_at'    => time(),
                    'odr_dtl.updated_at'     => time(),
                    'odr_dtl.updated_by'     => Data::getCurrentUserId(),
                ]);

            //Event Tracking
            DB::table('evt_tracking')->insert([
                'whs_id'     => $orderHdr->whs_id,
                'cus_id'     => $orderHdr->cus_id,
                'owner'      => $orderHdr->odr_num,
                'evt_code'   => 'RVT',
                'trans_num'  => $orderHdr->odr_num,
                'info'       => sprintf("%s Reverted status to New", $orderHdr->odr_num),
                'created_at' => time(),
                'created_by' => JWTUtil::getPayloadValue('jti')
            ]);

//            $this->correctOrder($odrId);
            $this->updateInvetoryReportOdr($odrId);

            DB::commit();

            return ['data' => 'Revert successful'];
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }

    public function cancelOrderDetails(Request $request)
    {
        //validate input
        $input = $request->getParsedBody();
        $odrDtls = $input['items'];
        if (empty($odrDtls)) {
            return $this->response->errorBadRequest("Order detail is empty");
        }

        //validate order details
        $odrDtlCancels = [];
        $odrHdrId = null;
        foreach ($odrDtls as $odrDtl) {
            if (empty($odrDtl['odr_dtl_id']) || !isset($odrDtl['cancel_qty'])) {
                return $this->response->errorBadRequest("Order detail is invalid");
            }
            $odrDtlId = $odrDtl['odr_dtl_id'];
            $cancelQty = $odrDtl['cancel_qty'];

            if (empty($cancelQty)) {
                continue;
            }

            $odrDtlObj = OrderDtl::where('odr_dtl_id', $odrDtlId)
                ->first();

            //check order pick pallet
            $pickPallet = $this->orderHdrMetaModel->getValue($odrDtlObj->odr_id, 'PPL');
            if ($pickPallet) {
                return $this->response->errorBadRequest("Order pick pallet can't cancel by sku");
            }

            //check order detail is not existed
            if (empty($odrDtlObj)) {
                return $this->response->errorBadRequest("Order detail id: $odrDtlId is not existed");
            }

            //check order detail is cancelled
            if ($odrDtlObj->itm_sts === 'CC') {
                return $this->response->errorBadRequest("Order detail id: $odrDtlId is cancelled");
            }

            //check $cancelQty > odr dtl alloc
            if ($odrDtlObj->alloc_qty < $cancelQty) {
                $msg = "Order detail id: $odrDtlId has cancell qty greater than allocate qty";

                return $this->response->errorBadRequest($msg);
            }

            //filter order detail which cancel qty > 0
            if ($cancelQty > 0) {
                $odrDtlCancels[] = ['cancel_qty' => $cancelQty, 'odr_dtl_obj' => $odrDtlObj];
            }

            if (empty($odrHdrId)) {
                $odrHdrId = $odrDtlObj->odr_id;
            } elseif ($odrHdrId != $odrDtlObj->odr_id) {
                return $this->response->errorBadRequest("Order details don't belong to same order header");
            }
        }

        //get order hdr obj
        $odrHdrObj = OrderHdr::where('odr_id', $odrHdrId)->first();

        //check order type
        if (empty($odrHdrObj)) {
            return $this->response->errorBadRequest('Order header is not existed');
        } elseif ($odrHdrObj->odr_sts === 'SH' || $odrHdrObj->odr_sts === 'PSH') {
            //check final bol
            $chkShipFinal = \DB::table('shipment')
                ->where('deleted', 0)
                ->where('ship_id', $odrHdrObj->ship_id)
                ->where('ship_sts', 'FN')
                ->count();

            if ($chkShipFinal) {
                return $this->response->errorBadRequest('Order has final BOL');
            }
        } elseif ($odrHdrObj->odr_type === 'BAC') {
            $msg = sprintf("Backorder %s cannot accept to create sub Backorder", $odrHdrObj->odr_num);

            return $this->response->errorBadRequest($msg);
        } elseif ($odrHdrObj->odr_sts === 'CC') {
            return $this->response->errorBadRequest('Order was cancelled');
        }


        try {
            DB::beginTransaction();

            //create bac order to cancel
            $bacCancelOdrObj = $this->orderHdrModel->createCancelBacOrder($odrHdrObj, $odrDtlCancels);

            //update back_odr
            $odrHdrObj->back_odr = 1;
            $odrHdrObj->save();

            //cancel bac order
            $this->cancelOrder([$bacCancelOdrObj->odr_id]);

            DB::statement("
                UPDATE wv_hdr
                JOIN odr_hdr ON odr_hdr.wv_id = wv_hdr.wv_id
                SET wv_hdr.wv_sts = 'CO'
                WHERE odr_hdr.odr_id = {$odrHdrObj->odr_id}
                AND NOT EXISTS (
                    SELECT wv_dtl.wv_id
                    FROM wv_dtl
                    WHERE wv_dtl.wv_id  = wv_hdr.wv_id
                    AND wv_dtl.piece_qty != wv_dtl.act_piece_qty
                    LIMIT 1
                )
            ");

            if (!empty($odrHdrObj->wv_id)) {
                //update wv_dtl sts
                DB::statement("
                    UPDATE wv_dtl
                    SET wv_dtl_sts = 'PD'
                    WHERE wv_id = {$odrHdrObj->wv_id} AND piece_qty = act_piece_qty
                ");
            }

            DB::commit();
            if (!empty($odrHdrObj->wv_id)) {
                dispatch(new AutoPackJob($odrHdrObj->wv_id, $request));
            }


        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }



        return ['data' => 'Cancel order detail succesful'];
    }

    protected function cancelOrder($odrIds)
    {
        $evtTracking = [];
        $now = strtotime(date("Y-m-d", time()));
        $error = true;
        $cancelOdr = [];
        foreach ($odrIds as $idx => $odrId) {
            $odrObj = $this->orderHdrModel->byId($odrId, []);

            if ($this->processCancel($odrObj)) {
                $error = false;
                $cancelOdr[] = $odrId;

                // Remove ShipId
                $this->removeShipId($odrId);
            } else {
                continue;
            }

            //event tracking
            $evtTracking[] = [
                'whs_id'     => $odrObj->whs_id,
                'cus_id'     => $odrObj->cus_id,
                'owner'      => $odrObj->odr_num,
                'evt_code'   => Status::getByKey("event", "ORDER-CANCELED"),
                'trans_num'  => $odrObj->odr_num,
                'info'       => sprintf(Status::getByKey("event-info", "ORDER-CANCELED"), $odrObj->odr_num),
                'created_at' => time(),
                'created_by' => JWTUtil::getPayloadValue('jti')
            ];

            if (!empty($odrObj->wv_id) && $odrObj->odr_sts != "CC") {
                $this->changeStatusWavepick($odrObj->wv_id, $odrObj->odr_sts, $cancelOdr);
            }

            $this->wavepickDtlModel->updateWhere(
                [
                    'deleted'    => 1,
                    'deleted_at' => time()
                ],
                [
                    'wv_id'     => $odrObj->wv_id,
                    'piece_qty' => 0
                ]);
            if (!empty($odrObj->wv_id)) {
                $sqlStrWV = "
                 0 = (
                        SELECT COUNT(1) FROM wv_dtl
                        WHERE wv_hdr.wv_id = wv_dtl.wv_id
                            AND wv_dtl.deleted = 0
                            AND wv_dtl.piece_qty != wv_dtl.act_piece_qty
                            AND wv_dtl.wv_dtl_sts != 'CC'
                    )
                ";

                DB::table('wv_hdr')
                    ->where('wv_id', $odrObj->wv_id)
                    ->where('wv_sts', '!=', 'CC')
                    ->whereRaw($sqlStrWV)
                    ->update(['wv_sts' => 'CO']);
            }

            if (!empty($odrObj->wv_id)) {
                $sqlStrWV = "
                 0 = (
                        SELECT COUNT(1) FROM wv_dtl
                        WHERE wv_hdr.wv_id = wv_dtl.wv_id
                            AND wv_dtl.deleted = 0
                            AND wv_dtl.piece_qty != wv_dtl.act_piece_qty
                            AND wv_dtl.wv_dtl_sts != 'CC'
                    )
                ";

                DB::table('wv_hdr')
                    ->where('wv_id', $odrObj->wv_id)
                    ->where('wv_sts', '!=', 'CC')
                    ->whereRaw($sqlStrWV)
                    ->update(['wv_sts' => 'CO']);
            }

        }

        if ($error) {
            return $this->response->errorBadRequest('Order has been shipped or finalized BOL cannot be cancelled!');
        }

        //update status cancel
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);

        //cancel odr hdr
        $this->orderHdrModel->getModel()
            ->whereIn('odr_id', $cancelOdr)
            ->update([
                'odr_sts'       => 'CC',
                'act_cancel_dt' => $now,
                'updated_by'    => $userId
            ]);
        //cancel odr dtl
        $this->orderDtlModel->getModel()
            ->whereIn('odr_id', $cancelOdr)
            ->update([
                'itm_sts'    => 'CC',
                'updated_by' => $userId
            ]);

        //Event Tracking
        DB::table('evt_tracking')->insert($evtTracking);
    }

    public function correctOrder($odrHdrID)
    {
        // Update picked_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS picked_qty
                FROM
                    cartons c
                WHERE
                    c.ctn_sts = 'PD'
                AND c.deleted = 0
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            AND t.lot = i.lot
            SET i.picked_qty = IFNULL(t.picked_qty, 0)
            WHERE i.picked_qty != IFNULL(t.picked_qty, 0)
            AND d.odr_id = $odrHdrID
        ");

        // Update allocated_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    h.odr_sts,
                    d.whs_id,
                    d.item_id,
                    d.lot,
                    SUM(

                        IF (
                            d.alloc_qty < d.picked_qty,
                            0,
                            d.alloc_qty - d.picked_qty
                        )
                    ) AS allocated_qty
                FROM
                    odr_dtl d
                JOIN odr_hdr h ON h.odr_id = d.odr_id
                WHERE
                    h.odr_sts IN ('AL', 'PK')
                AND d.alloc_qty > 0
                AND d.deleted = 0
                GROUP BY
                    d.whs_id,
                    d.item_id,
                    d.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            AND t.lot = i.lot
            SET i.allocated_qty = IFNULL(t.allocated_qty, 0)
            WHERE i.allocated_qty != IFNULL(t.allocated_qty, 0)
            AND d.odr_id = $odrHdrID
        ");

        // Update avail_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS available_qty
                FROM
                    cartons c
                WHERE
                    c.deleted = 0
                AND c.ctn_sts IN ('AC', 'LK')
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            AND t.lot = i.lot
            SET i.avail = GREATEST(
                IFNULL(
                    t.available_qty - i.allocated_qty,
                    0
                ),
                0
            ),
             i.allocated_qty =
            IF (
                IFNULL(
                    t.available_qty - i.allocated_qty,
                    0
                ) < 0,
                ABS(
                    IFNULL(
                        t.available_qty - i.allocated_qty,
                        0
                    )
                ),
                i.allocated_qty
            )
            WHERE
                (i.avail != GREATEST(
                        IFNULL(
                            t.available_qty - i.allocated_qty,
                            0
                        ),
                        0
                    )
                OR i.allocated_qty !=
                IF (
                    IFNULL(
                        t.available_qty - i.allocated_qty,
                        0
                    ) < 0,
                    ABS(
                        IFNULL(
                            t.available_qty - i.allocated_qty,
                            0
                        )
                    ),
                    i.allocated_qty
                ))
            AND d.odr_id = $odrHdrID
        ");

        // Update total_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS total_qty
                FROM
                    cartons c
                WHERE
                    c.deleted = 0
                AND c.ctn_sts IN ('AC', 'LK', 'PD')
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            AND t.lot = i.lot
            SET i.ttl = IFNULL(t.total_qty, 0)
            WHERE i.ttl != IFNULL(t.total_qty, 0)
            AND d.odr_id = $odrHdrID
        ");
    }

    public function updateInvetoryOdr($odrHdrID) {
        DB::select(DB::raw("UPDATE inventory s
            JOIN (
                SELECT
                    h.whs_id,
                    d.item_id,
                    d.lot,
                    SUM( d.alloc_qty) AS qty
                FROM
                    odr_hdr h
                JOIN odr_dtl d ON h.odr_id = d.odr_id 
                WHERE h.odr_id = $odrHdrID
                    AND d.deleted = 0
                    AND d.alloc_qty > 0 
                GROUP BY
                    h.whs_id, d.item_id, d.lot
            )   AS t
            ON t.whs_id = s.whs_id AND s.item_id = t.item_id AND s.lot = t.lot
            SET s.in_pick_qty = s.in_pick_qty - t.qty,
                s.in_hand_qty = s.in_hand_qty + t.qty
            WHERE s.type = 'I'"));
        return 0;
    }

    public function updateInvetoryReportOdr($odrHdrID) {
        DB::select(DB::raw("UPDATE rpt_inventory s
            JOIN (
                SELECT
                    h.whs_id,
                    d.item_id,
                    d.lot,
                    SUM( d.alloc_qty) AS qty
                FROM
                    odr_hdr h
                JOIN odr_dtl d ON h.odr_id = d.odr_id 
                WHERE h.odr_id = $odrHdrID
                    AND d.deleted = 0
                    AND d.alloc_qty > 0 
                GROUP BY
                    h.whs_id, d.item_id, d.lot
            )   AS t
            ON t.whs_id = s.whs_id AND s.item_id = t.item_id
            SET s.in_pick_qty = s.in_pick_qty - t.qty,
                s.in_hand_qty = s.in_hand_qty + t.qty
            WHERE s.type = 'I'"));
        return 0;
    }
}