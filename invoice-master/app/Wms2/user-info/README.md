Get user infomation for cache or db

Install : Add follow code to composer.json
    "repositories":[
        {
            "type": "vcs",
            "url": "git@gitlab-ssh.seldatdirect.com:viet83/user-info.git",
            "tags-path": "Tags"
        }
    ],
    "require": {
        .....,
        "wms2/user-info": "1.0.*",
        ....
    }

Configuration: 
    - .env file have to had JWT_SECRET (for decode jwt)
    - .env file have to had: 
        JWT_SECRET (for decode jwt)
        API_AUTHENTICATION (for authenticate when user haven't redis cache data)
    
    - config/database.php: add follow code
        return [
            'redis' => [
                'cluster' => false,
                'default' => [
                    'host' => 'redis.dev.seldatdirect.com',
                    'port' => 6379,
                    'database' => 0,
                    'password' => 'c3sellthat'
                ],
                'cache-key' => 'awms2_dev:cache.user.'
            ]
        ];
    
Use:
    $predis = new \Wms2\UserInfo\Data();
    
    $userInfo = $predis->getUserInfo();