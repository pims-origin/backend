<?php

namespace App\Api\V2\Inbound\Pallet\ScanPallet\Controllers;

use App\Api\V2\Inbound\Pallet\ScanPallet\Models\AsnDtlModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\AsnHdrModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\CustomerColorModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\DamageCartonModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\GoodsReceiptModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\EventTrackingModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\ContainerModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\ItemModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\LocationModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\Log;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\PalletModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\PalletSuggestLocationModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\VirtualCartonModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\VirtualCartonSumModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\InventorySummaryModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\CartonModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Models\GoodsReceiptDetailModel;
use App\Api\V2\Inbound\Pallet\ScanPallet\Transformers\SuggestLocationTransformer;
use App\Api\V2\Inbound\Pallet\ScanPallet\Validators\LocationPalletListValidator;
use App\Api\V2\Inbound\Pallet\ScanPallet\Validators\ScanPalletValidator;
use App\libraries\RFIDValidate;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Models\Reason;
use Wms2\UserInfo\Data;


/**
 * Class PalletController
 *
 * @package App\Api\V5\Inbound\Pallet\ScanPallet\Controllers
 */
class PalletController extends AbstractController
{
    protected $palletModel;
    protected $virtualCartonModel;
    protected $scanPallet;
    protected $locationModel;
    protected $locationStatusDetailModel;
    protected $cartonModel;
    protected $asnDtlModel;
    protected $eventTrackingModel;
    protected $palletSuggestLocationModel;
    protected $goodsReceiptModel;
    protected $goodsReceiptDetailModel;
    protected $vtlCtnModel;
    protected $vtlCtnSumModel;
    protected $asnHdrModel;
    protected $itemModel;
    protected $InventorySummaryModel;
    protected $containerModel;
    protected $dmgCtnModel;

    private   $_request;
    private   $_whsId;
    private   $_url;
    private   $_transaction;


    /**
     * PalletController constructor.
     *
     * @param PalletModel $palletModel
     * @param PalletSuggestLocationModel $palletSuggestLocationModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        PalletModel $palletModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        CartonModel $cartonModel,
        LocationModel $locationModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->scanPallet = new ScanPalletValidator();
        $this->palletModel = $palletModel;
        $this->asnDtlModel = new AsnDtlModel();
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->goodsReceiptModel = new GoodsReceiptModel();
        $this->goodsReceiptDetailModel = new GoodsReceiptDetailModel();
        $this->vtlCtnModel = new VirtualCartonModel();
        $this->vtlCtnSumModel = new VirtualCartonSumModel();
        $this->asnHdrModel = new AsnHdrModel();
        $this->virtualCartonModel = new VirtualCartonModel();
        $this->itemModel = new ItemModel();
        $this->InventorySummaryModel = new InventorySummaryModel();
        $this->containerModel = new ContainerModel();
        $this->dmgCtnModel = new DamageCartonModel();
    }

    const REDUCED_BY_OVERREAD = "Reduced by Overread";

    /**
     * @param  Integer $whsId
     * @param  Request $request
     * @return mixed
     */
    public function assignCartonsToPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        /**
         * Logs
         */
        $this->_transaction = $owner = $transaction = "";
        $url = "/v2/whs/{$whsId}/ib/pallet/scan-cartons-to-pallet/";
        Log:: info($request, $whsId, [
            'evt_code'     => 'ACC',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Scan Pallet'
        ]);

        $this->_request = $request;
        $this->_url     = $url;
        $this->_whsId   = $whsId;

        $requireFieldsValidate = $this->scanPallet->validateRequireFields($input);
        if ($requireFieldsValidate) {
            return $this->_responseMessage($requireFieldsValidate);
        }

        //get an ctn rfid to get grdetail from table ctn
        $randRfid = array_get($input, 'ctns', null);
        $arrCtnRfid = array_get($input, 'ctns', null);
        $pltRfid = array_get($input, 'pallet', null);
        $isConfirm = array_get($input, 'is_confirm', '');
        $tabletTag = array_get($input, 'tablet_tag', '');

        try {
            if (!$randRfid) {
                throw new \Exception(Message::get('BM142'));
            }

            //validate pallet RFID
            $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
            if (!$pltRFIDValid->validate()) {
                return $this->_responseMessage($pltRFIDValid->error);
            }

            //check invalid code carton
            $ctnRfidInValid = [];
            $errorMsg = '';
            foreach ($arrCtnRfid as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }

            //if has errors, return all error data and message
            if ($ctnRfidInValid) {
                return $this->_responseMessage($errorMsg, $ctnRfidInValid);
            }


            $uniqueRFIDs = array_unique($randRfid);
            if (count($uniqueRFIDs) != count($randRfid)) {
                $duplicateRFID = array_unique(array_diff_assoc($randRfid, $uniqueRFIDs));

                $this->setMessage('Duplicate Carton: ' . implode(', ', $duplicateRFID));
                return $this->_responseMessage($this->getMessage(), $duplicateRFID);
            }

            //get the  vtl ctn to get info by vtn ctn rfid
            $vtlCtns = $this->vtlCtnModel->getCartonInRfId($randRfid, $whsId);

            if (!$vtlCtns) {
                $this->setData($arrCtnRfid);
                $msg = sprintf('There is no carton on pallet %s is scanned through conveyor.', $pltRfid);
                Log::error($request, $whsId, [
                    'evt_code'      => 'ERN',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);
                return $this->_responseMessage($msg);
            }

            $palletExist = $this->palletModel->getPalletByRfid($whsId, $pltRfid);

            //not allow rescan when pallet on RAC
            if ($palletExist && $palletExist->loc_id) {
                $this->setData($palletExist->loc_id);
                $msg = sprintf('Cannot rescan pallet %s was on RAC %s', $pltRfid, $palletExist->loc_name);
                Log::error($request, $whsId, [
                    'evt_code'      => 'ERL',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);
                return $this->_responseMessage($msg);
            }

            //not allow rescan if Goods Receipt is completed
            if ($palletExist && $palletExist->gr_hdr_id) {
                $chkGrCompleted = $this->goodsReceiptModel->getFirstWhere(['gr_hdr_id' => $palletExist->gr_hdr_id]);
                if (object_get($chkGrCompleted, 'gr_sts') == 'RE') {
                    $this->setData($palletExist->gr_hdr_id);
                    $msg = sprintf('Cannot rescan pallet %s belonged to Goods Receipt %s already completed.', $pltRfid, object_get($chkGrCompleted, 'gr_hdr_num'));
                    Log::error($request, $whsId, [
                        'evt_code'      => 'ERL',
                        'owner'         => $owner,
                        'transaction'   => $transaction,
                        'url_endpoint'  => $url,
                        'message'       => $msg,
                        'response_data' => $this->getResponseData()
                    ]);
                    return $this->_responseMessage($msg);
                }
            }

            //[Complete Receiving] Not edit pallet qty, just support to putaway
            if ($palletExist){
                $palletExist->load('grDtl.asnDetail.asnHdr');
                $asnHdr = object_get($palletExist, 'grDtl.asnDetail.asnHdr');
                if ($asnHdr && $asnHdr->asn_sts === 'CO'){
                    $msg = 'ASN already completed receiving';
                    return $this->_responseMessage($msg);
                }
            }

            $ansHdrVtns = [];
            $sortAsnDtlVtlCtns = [];
            $palletCartons = [];
            $vtlCtnTemp = $vtlCtns;
            $vtnOtherPlt = [];
            $sortCtnrVtlCtns = [];
            $ccNotify = false;
            $vtlCtnOverread = [];
            foreach ($vtlCtns as $key => $vtl) {
                if ($vtl['plt_rfid']) {
                    // rescanned carton
                    $palletCartons[$vtl['plt_rfid']][] = $vtl['ctn_rfid'];
                    $sortAsnDtlVtlCtns[$vtl['asn_dtl_id']][] = $vtl;
                    $sortAsnHdrVtlCtns[$vtl['asn_hdr_id']][] = $vtl;
                    $sortCtnrVtlCtns[$vtl['ctnr_id']][] = $vtl;
                    $ansHdrVtns[$vtl['asn_hdr_id']][$vtl['ctnr_id']][$vtl['asn_dtl_id']][] = $vtl;
                    if ($vtl['plt_rfid'] != $pltRfid) {
                        // WMS2-4454 - Overread carton
                        $ccNotify = true;
                        $vtlCtnOverread[] = $vtl;
                    }
                } else {
                    // new carton
                    $sortAsnDtlVtlCtns[$vtl['asn_dtl_id']][] = $vtl;
                    $sortAsnHdrVtlCtns[$vtl['asn_hdr_id']][] = $vtl;
                    $sortCtnrVtlCtns[$vtl['ctnr_id']][] = $vtl;
                    $ansHdrVtns[$vtl['asn_hdr_id']][$vtl['ctnr_id']][$vtl['asn_dtl_id']][] = $vtl;
                }

            }
            if (!$vtlCtnTemp) {
                $this->setData($arrCtnRfid);
                $msg = sprintf('There is no carton on pallet %s is scanned through conveyor.', $pltRfid);
                Log::error($request, $whsId, [
                    'evt_code'      => 'ERN',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);
                return $this->_responseMessage($msg);
            }

            $asnHdrId = array_get(array_values($vtlCtnTemp)[0], 'asn_hdr_id');

            $vtlCtns = $vtlCtnTemp;
            $randRfid = array_diff($randRfid, $vtnOtherPlt);
            $trueRFIDs = array_pluck($vtlCtns, 'ctn_rfid', null);
            // Cartons unscanned (not exists vtl_ctn table)
            $wrongRFIDS = array_diff($randRfid, $trueRFIDs);

            // check cartons that are removed from consolidate
            $chkConsolidate = $this->cartonModel->getModel()
                                ->whereIn('rfid',$trueRFIDs )
                                ->whereNull('plt_id')
                                ->get();
            if (count($chkConsolidate) > 0) {
                $ctnConsolidates = array_pluck($chkConsolidate, 'rfid');
                $msg = sprintf("Unable to scan pallet with consolidate cartons. These cartons %s have to use Putback feature.",
                    implode(', ', $ctnConsolidates));
                Log::error($request, $whsId, [
                    'evt_code'      => 'ERN',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);
                return $this->_responseMessage($msg, $ctnConsolidates);
            }

            // only accept 2 sku
            $grSKus = array_count_values(array_pluck($vtlCtns, 'asn_dtl_id'));

            $numberSkus = count($grSKus);
            $keySKU = array_keys($grSKus);
            $primaryAsnDtl = null;
            $isMixedSku = false;
            $isUnscanned = false;
            // check is Mixed SKU
            $scnCtn = count($vtlCtns);
            if ($numberSkus > 1) {
                // check only one gr
                $grArr = array_unique(array_pluck($vtlCtns, 'ctnr_id'));

                if (($numberGr = count($grArr)) > 1) {
                    $dataErrors = [];
                    foreach ($vtlCtns as $vtlCtn)
                    {
                        $dataErrors[$vtlCtn['ctnr_id']][] = $vtlCtn['ctn_rfid'];
                    }
                    $msgDetails = '';
                    $countCtnr = $numberGr;
                    foreach ($dataErrors as $ctnrId => $dataError)
                    {
                        $ctnrArr = DB::table('container')->where('ctnr_id', $ctnrId)->first();
                        $msgDetails .= sprintf("Container %s: %s", array_get($ctnrArr, 'ctnr_num'), implode(', ', array_values($dataError)));
                        $countCtnr--;
                        if ($countCtnr > 0) {
                            $msgDetails .= "; ";
                        }
                    }

                    $msg = sprintf('Mixed SKU in %d containers. %s.', $numberGr, $msgDetails);
                    Log::error($request, $whsId, [
                        'evt_code'      => 'ERN',
                        'owner'         => $owner,
                        'transaction'   => $transaction,
                        'url_endpoint'  => $url,
                        'message'       => $msg,
                        'response_data' => $this->getResponseData()
                    ]);
                    return $this->_responseMessage($msg);
                }

                $countSKU = array_values($grSKus);
                    $idx = max($countSKU);
                    foreach ($countSKU as $keySku => $valueSku) {
                        if ($idx == $valueSku) {
                            $idx = $keySku;
                            break;
                        }
                    }
                    $primaryAsnDtl = $keySKU[$idx];

                $isMixedSku = true;
            }

            // check Unscanned cartons
            if (($uscTTl = count($wrongRFIDS)) > 0) {
                $isUnscanned = true;
            }

            $asnHdr = $this->asnHdrModel->getFirstWhere(['asn_hdr_id' => $asnHdrId]);
            $cusId = object_get($asnHdr, 'cus_id');
            $owner = object_get($asnHdr, 'asn_hdr_num', '');
            $asnHdrNums[] = $owner;
            $asnHdrIds[] = $asnHdrId;
            $asnHdrs[$asnHdrId] = $asnHdr;

            if (count($palletCartons) == 0 && $palletExist) {
                /**
                 * all cartons are new
                 */
                $msg = sprintf('Cannot rescan pallet %s without containing at least one carton scanned before', $pltRfid);
                Log::error($request, $whsId, [
                    'evt_code'      => 'EUC',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => \GuzzleHttp\json_encode($arrCtnRfid),
                    'response_data' => $this->getResponseData()
                ]);
                return $this->_responseMessage($msg, $arrCtnRfid);
            }

            // [WAP-885 - Inbound - Receiving] Create API to process mix SKUs, unscan/unknown cartons tag.
            $skus = $this->virtualCartonModel->getSkuByVtnRfid($whsId, $randRfid);
            if($isConfirm == 0){
                if($isMixedSku == true || $isUnscanned == true){
                    $data = [
                        'skus' => $skus,
                        'tablet_tag' => $tabletTag
                    ];
                    return $this->_responseMessage('Successfully!', $data, true, 1);
                }
            }

            // start transaction
            DB::beginTransaction();

            if ( $asnHdr->asn_type === 'RMA' ){
                $this->cartonModel->deletePutBackAndShippedCartons($arrCtnRfid);
            }

            $grHdrIds = [];
            $grDtlIds = [];
            // Update pallet && vtl_ctn if exists
            if ($palletExist) {
                $palletExist->dmg_ttl = 0;
                $palletExist->ctn_ttl = 0;
                $palletExist->save();

                // Update vtl(new & plt_id is null) by pallet, ansHdr, ctnrId

                $this->vtlCtnModel->updatePalletRFIDNullMixedSku($pltRfid, $asnHdrIds, array_keys($sortCtnrVtlCtns));
            }

            $dataForUpdatePallet = [];
            if ($ccNotify) {
                // insert cycle count notification
                // delete carton when belong to other pallets
                    $dataForUpdatePallet = $this->_executeOverreadCarton($whsId, $vtlCtnOverread);
            }

            foreach ($sortAsnDtlVtlCtns as $one_asn_dtl_vtl) {

                $this->createGrAndDtl($grHdrIds, $grDtlIds, $palletExist, $asnHdrs, $pltRfid, $randRfid,
                    $one_asn_dtl_vtl, $wrongRFIDS, $primaryAsnDtl);

            }

            //get return goods receipt hdr number and asn hdr number
            $returnData = null;
            if ($palletExist) {
                // Update GrDtl By GrHdrIds
                list($dmg_ttls, $ctn_ttls, $unscanned) = $this->dataJsonPallet($palletExist->data);

                $this->goodsReceiptDetailModel->updateGoodsReceiptDetailActCtn(array_keys($ctn_ttls));
                $ccn = null;
                $msgOther = '';
                $prefix = '';
                if (count($vtnOtherPlt)) {
                    $msgOther = count($vtnOtherPlt) . ' cartons on another pallet';
                    $prefix = ' and';
                    $ccn = $msgOther;
                }
                $ccn = $this->_setCcnPallet($ccn, $isMixedSku, $isUnscanned,$ccNotify, $prefix, $msgOther);

                // Update inform ctn_ttl & dmg_ttl on Pallet
                $this->palletModel->updatePalletCtnTtlAndDamaged($whsId, $grHdrIds, $palletExist->plt_id, $ccn,
                    $vtnOtherPlt, array_values($wrongRFIDS));

                // delelte unscanned  virtual carton previous scan

                $todeleted = array_diff(array_unique($unscanned), array_values($wrongRFIDS));
                if ($palletExist) {
                    $todeleted = array_diff(array_unique($todeleted), array_values($randRfid));
                }
                if (count($todeleted)) {
                    $this->virtualCartonModel->deleteUnscannedVtlCtns($whsId, $todeleted);
                }

                // update other pallet
                if (count($dataForUpdatePallet)) {
                    foreach ($dataForUpdatePallet as $palletUpdate) {
                        // Update inform ctn_ttl & dmg_ttl on other Pallet
                        $this->palletModel->updatePalletCtnTtlAndDamaged($whsId, $palletUpdate['gr_hdr_ids'], $palletUpdate['plt_id'], $palletUpdate['ccn']);
                    }
                }

                DB::commit();

                // [WAP-885 - Inbound - Receiving] Create API to process mix SKUs, unscan/unknown cartons tag.
                $sku = $this->asnDtlModel->getSkuByAsnDtl($primaryAsnDtl);
                foreach ($skus as $sk){
                    if($sk['item_id'] == 0){
                        $sku['asn_dtl_id'] = $primaryAsnDtl;
                        $sku['ctns_lst'] = $sk['ctns_lst'];
                    }
                }
                $data = [
                    'skus'          => $skus,
                    'tablet_tag'    => $tabletTag,
                    'new_skus'      => $sku
                ];
                $message = sprintf("%d cartons has been scanned with Pallet %s and %d Goods Receipt Details(s) has been created/updated!",
                    count($randRfid), $pltRfid, count($grDtlIds));
                return $this->_responseMessage($message, $data, true, 1);
            } else {
                DB::rollBack();
                $message = sprintf("No cartons has been scanned with Pallet %s",
                    $pltRfid);
                return $this->_responseMessage($message, [], true, 1);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => "/whs/{$whsId}/scan-pallet/",
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            if ($e->getCode() === '23000') {
                preg_match("/1062 Duplicate entry '([^\-]+)/i", $e->getMessage(), $dupCartons, PREG_OFFSET_CAPTURE);
                if (count($dupCartons) > 1) {
                    unset($dupCartons[0]);
                    $errorRfids = [];
                    foreach ($dupCartons as $dupCarton) {
                        $errorRfids[] = array_first($dupCarton);
                    }

                    $msg = sprintf("Duplicated Carton Rfid %s. Unfortunately, It exists in another warehouse.", implode(', ', $errorRfids));

                    return $this->_responseMessage($msg, [], true, 1);
                }
            }
            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    private function createGrAndDtl(
        &$grHdrIds,
        &$grDtlIds,
        &$palletExist,
        $asnHdrs,
        $pltRfid,
        $scanRfids,
        $vtlCtns,
        $wrongRFIDS,
        $primaryAsnDtlID
    ) {
        $vtlCtn     = array_shift($vtlCtns);
        $ansHdrId   = $vtlCtn['asn_hdr_id'];
        $asnHdr     = $asnHdrs[$ansHdrId];
        $asn_dtl_id = $vtlCtn['asn_dtl_id'];
        $cusId      = $vtlCtn['cus_id'];
        $whsId      = $vtlCtn['whs_id'];
        $ctnrId     = $vtlCtn['ctnr_id'];

        array_unshift($vtlCtns, $vtlCtn);

        $asnDtl  = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $asn_dtl_id]);
        if ($asnDtl && $asnDtl->asn_dtl_sts == "CC") {
                /**
                 * all cartons are new
                 */
                $this->setMessage(sprintf('Asn detail Id %s already canceled', $asn_dtl_id));
                // Log::error($request, $whsId, [
                //     'evt_code'      => 'EUC',
                //     'owner'         => $owner,
                //     'transaction'   => $transaction,
                //     'url_endpoint'  => $url,
                //     'message'       => \GuzzleHttp\json_encode($arrCtnRfid),
                //     'response_data' => $this->getResponseData()
                // ]);
                $this->setData($asn_dtl_id);

                DB::rollback();
                throw new \Exception($this->getMessage());
            }

        $ctnrNum = object_get($asnDtl, 'ctnr_num');

        /** Step by step
         * 1    Create Goods Receipt
         * 2    Create One Goods Receipt Details
         * 3    Insert carton RFID is not scanned.
         * 4    Check Pallet if not existed, create a new pallet and update carton total
         * 5    Check Virtual/Real Cartons if existed and not in current pallet, delete  virtual and real cartons
         * 6    Check If not define cartons, insert virtual/real cartons
         * 7    Update GR Dtl: real actual cartons, real damaged cartons, real pallet total
         * 8    Create real Cartons, real damaged cartons if needed
         * 9    Set real damaged Cartons if is_damaged = 1, insert damaged_cartons
         * 10    Update Virtual Carton status is Goods Receipt Created (carton not allowed to rescan)
         * 11    Update Pal_sug_loc, LPN, total Cartons per pallet(inconsistent data for each time printing)
         * 12    Update Inventory with available qty and damaged qty
         * 13    Create Event Tracking for Create/Update Goods Receipt
         * 14    Create Event Tracking for Assign Cartons to Pallet
         * 15    Update Goods receipt dtl Received when asn_dtl Received and count all virtual carton status GR Created
         * 16    Update Goods receipt Header when all ASN dtls of one container are received and all gr dtls are received too
         * 17    Update ASN header when all Grs/Containers Received
         * 18    Put pallet and cartons on Rack and update pal_sug_loc with actual location at step 11, pallet is not existed if not passed step 3
         * */

        // 1	Create Goods Receipt

        //set GR status to return for WAP
        $statusGR = false;
        $grHdr = $this->goodsReceiptModel->isGoodsReceiptExist($ansHdrId, $ctnrId);
        if ($grHdr) {
            $statusGR = false;
            $createdFrom = object_get($grHdr, 'created_from');
            if ($createdFrom != "WAP") {
                $msg= sprintf('Unable scan pallet for Goods Receipt that has been already created by %s', $createdFrom);

                DB::rollback();
                $this->_responseErrorMessage($msg, $vtlCtn);
            }
        } else {
            $statusGR = true;
            $expectDate = object_get($asnDtl, 'asn_dtl_ept_dt');
            $grHdr = $this->goodsReceiptModel->createGRHeader($asnHdr, $ctnrId, $ctnrNum, $expectDate);
        }
        $grHdrIds[$grHdr->gr_hdr_id] = $grHdr->gr_hdr_id;

        //2	    Create One Goods Receipt Details
        $grDtl = $this->goodsReceiptDetailModel->createGrDetail($grHdr, $asnDtl);

        $grDtlIds[$grDtl->gr_dtl_id] = $grDtl->gr_dtl_id;

        //3	Insert carton RFID is not scanned or not existed.
        if ((!empty($wrongRFIDS) && count($wrongRFIDS) > 0) && (!$primaryAsnDtlID || $asn_dtl_id == $primaryAsnDtlID)) {
            $asnDtl = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $asn_dtl_id]);
            $params = compact('asnDtl', 'wrongRFIDS', 'cusId', 'whsId');
            $this->vtlCtnModel->createVtlCartonIsNotScan($params);
            // if($primaryAsnDtlID == $asn_dtl_id)
            // get all carton again
            $vtlCtns = $this->vtlCtnModel->getCartonInRfIdNew($scanRfids, $whsId, $asn_dtl_id);
        }

        $pallet = $palletExist;
        if (!$palletExist) {
            //4. Check Pallet if not existed, create a new pallet and update carton total
            $primaryAsnDtlID = $primaryAsnDtlID ?? $asn_dtl_id;
            $asnDtlPrimary = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $primaryAsnDtlID]);
            $pallet = $this->palletModel->createPallet($grDtl, $grHdr, $asnDtlPrimary, $pltRfid, $scanRfids, 0);

        } else {
            /**
             * 1. Reduce damaged qty, avail, ttl inventory
             * 2. Delete different and missing cartons and virtual cartons in current pallet
             * 3. Check if pallet created and no existed carton rfids - all NEW
             */

            list($dmg_ttls, $ctn_ttls) = $this->dataJsonPallet($pallet->data);

            $dmg_ttl = $dmg_ttls[$grDtl['gr_dtl_id']] ?? 0;
            $ctn_ttl = $ctn_ttls[$grDtl['gr_dtl_id']] ?? 0;


            $dmgQty = ($dmg_ttl * $grDtl['pack']);
            $availQty = (($ctn_ttl * $grDtl['pack']) - $dmgQty);

            // WAP-583 - Update Scan Pallet about Inventory and Status
//            $this->goodsReceiptModel->reduceInventoryWhenRescanPallet($whsId, $cusId, $grDtl, $availQty, $dmgQty);

            $this->dmgCtnModel->deleteDamagedCartonExistInPallet($pallet->plt_id);
            $this->cartonModel->deleteCartonExistInPallet($pallet->plt_id, $scanRfids);

            // Update pallet RFID
            $this->vtlCtnModel->updateVtlCartonPalletRFIDSNotInCartonRescan($pallet->rfid, $scanRfids);
        }

        //update table vtl ctn set Pallet's RFID
        $cartonRfids = array_pluck($vtlCtns, 'ctn_rfid');
        $this->vtlCtnModel->updateVtlCtnPltRfidByVtlCtnRfid($cartonRfids, $pltRfid);

        //6	    Check If not define cartons, insert virtual/real cartons
        $params = compact('pallet', 'grHdr', 'grDtl', 'asnDtl', 'vtlCtns', 'palletExist');

        $cartonTtl = $this->cartonModel->createCartons($params);

        //8     Create real Cartons, real damaged cartons if needed
        // $damageCartonTtl = $this->cartonModel->setDamagedCartons($vtlCtns);
        // WAP-760 - [Inbound - Receiving] Create API to save reasons damage
        $damageCartonTtl = (new DamageCartonModel())->insertDamageCartons($vtlCtns);

        //9	    Set real damaged Cartons if is_damaged = 1, insert damaged_cartons

        //10	Update Virtual Carton status is Goods Receipt Created (carton not allowed to rescan)
        $this->vtlCtnModel->updateVtlCartonStatusIsGC($scanRfids);

        //update pallet
        // list($dmg_ttls, $ctn_ttls, $unscanned) = $this->dataJsonPallet($pallet->data);
        // $dmg_ttls[$grDtl['gr_dtl_id']] = $damageCartonTtl;
        // $ctn_ttls[$grDtl['gr_dtl_id']] = $cartonTtl;

        // $pallet->data = \json_encode([
        //     'dmg_ttls'  => $dmg_ttls,
        //     'ctn_ttls'  => $ctn_ttls,
        //     'unscanned' => array_merge($unscanned, $wrongRFIDS)
        // ]);
        // $pallet->save();

        //11	Update Pal_sug_loc, LPN, total Cartons per pallet(inconsistent data for each time printing)

        if ($palletExist) {
            //get pal_sug_loc and update ctn_ttl if not existed and then create pal_sug_loc
            $palSugLoc = $this->palletSuggestLocationModel->getPalletSugLocByPltId($pallet->plt_id,
                $grDtl->gr_dtl_id, $grHdr->gr_hdr_id);
            if ($palSugLoc) {
                $palSugLoc->ctn_ttl = $cartonTtl;
                $palSugLoc->save();
            }
        } else {
            $this->goodsReceiptModel->createPalletSuggestLocation($params);
        }

        //12	Update Inventory with available qty and damaged qty

        $dmgQty = $damageCartonTtl * $grDtl['pack'];
        $availQty = count($vtlCtns) * $grDtl['pack'] - $dmgQty;

        // WAP-583 - Update Scan Pallet about Inventory and Status
//        $this->goodsReceiptModel->updateInventory($whsId, $cusId, $grDtl, $availQty, $dmgQty);


        //13	Create Event Tracking for Create/Update Goods Receipt
        $info = sprintf("WAP - Goods Receipt Update %d cartons, %d damaged cartons", count($scanRfids),
            $damageCartonTtl);
        $evtCode = 'WGU';

        if ($statusGR) {
            $info = "WAP - Goods Receipt Created";
            $evtCode = 'WGN';
        }


        $evtGRCreated = [
            'whs_id'    => $whsId,
            'cus_id'    => $cusId,
            'owner'     => object_get($grHdr, 'gr_hdr_num', null),
            'evt_code'  => $evtCode,
            'trans_num' => $ctnrNum,
            'info'      => $info,
        ];
        $this->goodsReceiptModel->eventTracking($evtGRCreated);

        //14 Create Event Tracking for Assign Cartons to Pallet
        $evtCTNAssignToPallet = [
            'whs_id'    => $whsId,
            'cus_id'    => $cusId,
            'owner'     => object_get($grHdr, 'gr_hdr_num', null),
            'evt_code'  => 'WAP',
            'trans_num' => $ctnrNum,
            'info'      => sprintf("WAP - %d  cartons  to pallet RFID %s", count($scanRfids), $pltRfid),
        ];
        $this->goodsReceiptModel->eventTracking($evtCTNAssignToPallet);

        //15	Update Goods receipt dtl Received when asn_dtl Received and count all virtual carton status GR
        // Created

        $grDtlId = array_get($grDtl, 'gr_dtl_id');
        //gr_dtl_plt_ttl,  gr_dtl_act_ctn_ttl, gr_dtl_dmg_ttl, gr_dtl_is_dmg, gr_dtl_disc
        $this->goodsReceiptDetailModel->updateGrDtlWhenScanPallet($grDtlId);

        //update GR Dtail is received
        $this->goodsReceiptDetailModel->updateGoodsReceiptDetailReceived($grDtlId);

        //16	Update Goods receipt Header when all ASN dtls of one container are received and all gr dtls are received
        $grHdrId = array_get($grHdr, 'gr_hdr_id');
        $grHdrStatus = $this->goodsReceiptModel->updateGrHdrStatus($grHdrId);

        if ($grHdrStatus) {
            $evtGRCreated = [
                'whs_id'    => $whsId,
                'cus_id'    => $cusId,
                'owner'     => object_get($grHdr, 'gr_hdr_num', null),
                'evt_code'  => 'WGC',
                'trans_num' => $ctnrNum,
                'info'      => "WAP - Goods Receipt Completed",
            ];

            $this->goodsReceiptModel->eventTracking($evtGRCreated);
        }

        // Update ASN header when all Grs/Containers Received
        $asnHdrStatus = $this->asnHdrModel->updateASNHdrStatusRE($asnDtl['asn_hdr_id']);

        if ($asnHdrStatus) {
            $evtASNRE = [
                'whs_id'    => $whsId,
                'cus_id'    => $cusId,
                'owner'     => $asnHdr->asn_hdr_num,
                'evt_code'  => 'WAC',
                'trans_num' => $asnHdr->asn_hdr_num,
                'info'      => sprintf("WAP - %s received", $asnHdr->asn_hdr_num),
            ];

            $this->goodsReceiptModel->eventTracking($evtASNRE);
        }

        $palletExist = $pallet;
    }

    private function dataJsonPallet($data)
    {
        $data = \json_decode($data, true);
        $dmg_ttls = $data['dmg_ttls'] ?? [];
        $ctn_ttls = $data['ctn_ttls'] ?? [];
        $unscanned = $data['unscanned'] ?? [];

        return [
            $dmg_ttls,
            $ctn_ttls,
            $unscanned
        ];
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function scanPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        //$this->scanPallet->validate($input);
        $requireFieldsValidate = $this->scanPallet->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        //get an ctn rfid to get grdetail from table ctn
        $randRfid = array_get($input, 'pallet.ctn-rfid', null);
        $arrCtnRfid = array_get($input, 'pallet.ctn-rfid', null);
        $pltRfid = array_get($input, 'pallet.pallet-rfid', null);

        $owner = $transaction = "";
        try {

            $url = "/whs/{$whsId}/scan-pallet/";
            Log:: info($request, $whsId, [
                'evt_code'     => 'ACC',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Scan Pallet'
            ]);

            if (!$randRfid) {
                throw new \Exception(Message::get('BM142'));
            }

            //validate pallet RFID
            $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
            if (!$pltRFIDValid->validate()) {
                $data = [
                    'data'    => null,
                    'message' => $pltRFIDValid->error,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //check invalid code carton
            $ctnRfidInValid = [];
            $errorMsg = '';
            foreach ($arrCtnRfid as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }

            //if has errors, return all error data and message
            if ($ctnRfidInValid) {
                $data = [
                    'data'    => $ctnRfidInValid,
                    'message' => $errorMsg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //get return goods receipt hdr number and asn hdr number
            $returnData = null;

            $checkPalletExist = $this->vtlCtnModel->checkPalletExisted($pltRfid);

            if ($checkPalletExist) {
                //in case, no virtual carton is Assign "AS" status, return error
                $checkScanningPlt = $this->vtlCtnModel->isScanPalletAgain($pltRfid);

                //check pallet table have this pallet RFID
                $checkExistedRealPallet = (bool)$this->palletModel
                    ->getModel()
                    ->where('ctn_ttl', 0)
                    ->where('rfid', $pltRfid)
                    ->whereNotNull('zero_date')
                    ->where('storage_duration', '!=', 0)
                    ->count();

                if (!$checkExistedRealPallet) {
                    if (!$checkScanningPlt) {
                        $msg = sprintf("This pallet %s is not allowed to scanned again.", $pltRfid);
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }
                }
            }

            //get the first vtl ctn to get info by vtn ctn rfid
            $vtlCtns = $this->vtlCtnModel->getCartonInRfId($randRfid);

            $sortAsnDtlVtlCtns = [];
            foreach ($vtlCtns as $vtl) {
                $sortAsnDtlVtlCtns[$vtl['asn_dtl_id']][] = $vtl;
                $sortAsnHdrVtlCtns[$vtl['asn_hdr_id']][] = $vtl;
                $sortCtnrVtlCtns[$vtl['ctnr_id']][] = $vtl;
            }
            $vtlCtn = array_get($vtlCtns, '0', []);

            $detail = [
                'asn_hdr_id' => array_get($vtlCtn, 'asn_hdr_id', null),
            ];

            $statusGR = false;
            //Same ASN HDR ID and multiple SKUs
            if (count($sortAsnDtlVtlCtns) > 1) {
                $msg = sprintf("Mixed SKUs on pallet %s", $pltRfid);

                $this->setMessage($msg);
                $dataInput = [
                    'sortAsnDtlVtlCtns' => $sortAsnDtlVtlCtns,
                    'sortAsnHdrVtlCtns' => $sortAsnHdrVtlCtns,
                    'sortCtnrVtlCtns'   => $sortCtnrVtlCtns,
                    'pltRfid'           => $pltRfid,
                    'vtlCtns'           => $vtlCtns,
                    'url'               => $url,
                ];

                $dataReturn = $this->checkMixSKU($dataInput, $request, $whsId);

                $data = [
                    'data'    => $dataReturn,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //check vtl carton was deleted
            if (count($vtlCtns) < count($randRfid)) {
                // get rfid wrong
                $trueRFIDs = array_pluck($vtlCtns, 'ctn_rfid', null);
                $wrongRFIDS = array_diff($randRfid, $trueRFIDs);

                $this->setData($wrongRFIDS);
                $str = "";
                $count = 0;
                foreach ($wrongRFIDS as $rfid) {

                    if ($count < count($wrongRFIDS)) {
                        $str .= $rfid . ', ';
                    } else {
                        $str .= $rfid;
                    }
                    $count++;
                }
                $msg = sprintf("There is carton's rfid %s is/are not defined.", $str);

                $this->setMessage($msg);
                Log::error($request, $whsId, [
                    'evt_code'      => 'ERR',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => "/whs/{$whsId}/scan-pallet/",
                    'message'       => $msg,
                    'response_data' => $this->getResponseData()
                ]);

                return new Response($this->getResponseData(), 200, [], null);
            }
            $ansHdrId = array_get($vtlCtn, 'asn_hdr_id', null);
            $ctnrId = array_get($vtlCtn, 'ctnr_id', null);
            $containerInfo = $this->containerModel->getFirstWhere(['ctnr_id' => $ctnrId]);

            // start transaction
            DB::beginTransaction();
            //scan pallet multiple times
            $this->vtlCtnModel->updatePalletRFIDNull($pltRfid, $ansHdrId, $ctnrId);

            //update table vtl ctn set Pallet's RFID
            $this->vtlCtnModel->updateVtlCtnPltRfidByVtlCtnRfid(array_values($arrCtnRfid), $pltRfid);

            //check all ASN detail were completed or not
            $countLatestVtlCtn = $this->asnDtlModel->countAllAsnDtlReceived($ansHdrId, $ctnrId);

            // check all virtual carton have assigned to pallet
            $countCtnHaveNoAssignedToPallet = $this->virtualCartonModel->countVtlCtnNotHavePltByAsnHdrId($ansHdrId,
                $ctnrId);

            //if there is no vtl ctn of asn hdr has pallet's rfid is null, create GR
            if ($countLatestVtlCtn == 0 && $countCtnHaveNoAssignedToPallet == 0) {
                //call create GR
                $paramsGR = [
                    'asn_hdr_id' => array_get($vtlCtn, 'asn_hdr_id', null),
                    'ctnr_id'    => array_get($vtlCtn, 'ctnr_id', null),
                    'whs_id'     => array_get($vtlCtn, 'whs_id', null),
                    'cus_id'     => array_get($vtlCtn, 'cus_id', null),
                    'ctnr_num'   => array_get($containerInfo, 'ctnr_num', null),
                ];

                // $returnData = $this->createGR($paramsGR);
                $returnData = $this->goodsReceiptModel->createGRWhenScanAllCartons($paramsGR);

                $message = sprintf("Goods Receipt number %s was created for ASN header number %s successfully !",
                    $returnData['gr_hdr_num'], $returnData['asn_hdr_num']);
                $statusGR = true;
            }

            DB::commit();

            if (empty($message)) {
                $message = sprintf("Pallet number %s scanned successfully !", $pltRfid);
            }
            $data = [
                'message'   => $message,
                'data'      => [
                    'detail' => $detail,
                ],
                'status'    => true,
                'gr_status' => $statusGR,
            ];

            return new Response($data, 200, [], null);

        } catch (\Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => "/whs/{$whsId}/scan-pallet/",
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function logMultiAsnHdrMixSKU($request, $whsId, $asnHdrs, $asnHdrNums, $url, $pltRfid)
    {
        /*
         * 1 = 1 container (1 ASN)
         * 2 = at least 2 containers (1 ASN)
         * 3 = at least 2 containers - 2 ASNs
         */

        $msgLog = [
            "message" => sprintf("Mixed SKUs on pallet %s .", $pltRfid),
        ];

        $arrLoop = [];
        //get all vtl ctn and asn detail
        foreach ($asnHdrs as $asnHdrId => $containers) {

            foreach ($containers as $asnDtls) {
                foreach ($asnDtls as $key => $cartons) {
                    $msgLog['ASN DTL ' . $key] = array_pluck($cartons, 'ctn_rfid');
                }
            }
            //one ctnr - an asn hdr
            $arrLoop[] = [
                'owner'       => $asnHdrNums[$asnHdrId] ?? '',
                'transaction' => \json_encode(array_keys($containers)),
            ];
        }

        foreach ($arrLoop as $item) {
            Log::error($request, $whsId, [
                'evt_code'      => 'EMS',
                'owner'         => $item['owner'],
                'transaction'   => $item['transaction'],
                'url_endpoint'  => $url,
                'message'       => \GuzzleHttp\json_encode($msgLog),
                'response_data' => $this->getResponseData()
            ]);
        }
    }

    public function checkMixSKU($dataInput, $request, $whsId)
    {
        $sortAsnDtlVtlCtns = $dataInput['sortAsnDtlVtlCtns'];
        $sortAsnHdrVtlCtns = $dataInput['sortAsnHdrVtlCtns'];
        $sortCtnrVtlCtns = $dataInput['sortCtnrVtlCtns'];
        $pltRfid = $dataInput['pltRfid'];
        $vtlCtns = $dataInput['vtlCtns'];
        $url = $dataInput['url'];
        $mixedSkuType = 0;
        /*
         * 1 = 1 container (1 ASN)
         * 2 = at least 2 containers (1 ASN)
         * 3 = at least 2 containers - 2 ASNs
         */

        $vtlCtn = array_get($vtlCtns, '0', []);

        //one asn hdr
        if (count($sortAsnHdrVtlCtns) == 1) {
            //one ctnr - an asn hdr
            if (1 == count($sortCtnrVtlCtns)) {
                $owner = $this->setErrorLogCtnsMultipleItemsOnOneANS($vtlCtns);
                $transaction = $vtlCtn['ctnr_id'];

                $msgLog = [
                    "message" => sprintf("Mixed SKUs on pallet %s", $pltRfid),
                ];
                $arrASNDtlVtlCtns = [];
                foreach ($sortAsnDtlVtlCtns as $key => $values) {

                    $ctnRFID = array_pluck($values, 'ctn_rfid');

                    $msgLog['ASN DTL ' . $key] = $ctnRFID;

                    $arrASNDtlVtlCtns[$key][] = $values;
                }

                Log::error($request, $whsId, [
                    'evt_code'      => 'EMS',
                    'owner'         => $owner,
                    'transaction'   => $transaction,
                    'url_endpoint'  => $url,
                    'message'       => \GuzzleHttp\json_encode($msgLog),
                    'response_data' => $this->getResponseData()
                ]);

                $msgLog["mixed_sku_type"] = 1;
                //$msgLog["one_asn_hdr_vtls"] = $arrASNDtlVtlCtns;
                $msgLog["one_asn_hdr_vtls"] = $sortAsnDtlVtlCtns;

                return $msgLog;
            } //more than one ctnr - an asn hdr
            else if (count($sortCtnrVtlCtns) > 1) {
                $messageReturn = [];
                foreach ($sortCtnrVtlCtns as $key => $values) {
                    $owner = $this->setErrorLogCtnsMultipleItemsOnOneANS($values);
                    $transaction = $key;

                    $msgLog = [
                        "message" => sprintf("Mixed SKUs on pallet %s .", $pltRfid),
                    ];

                    foreach ($sortAsnDtlVtlCtns as $key => $values) {
                        $ctnRFID = array_pluck($values, 'ctn_rfid');

                        $msgLog['ASN DTL ' . $key] = $ctnRFID;
                    }

                    $messageReturn[] = $msgLog;

                    Log::error($request, $whsId, [
                        'evt_code'      => 'EMS',
                        'owner'         => $owner,
                        'transaction'   => $transaction,
                        'url_endpoint'  => $url,
                        'message'       => \GuzzleHttp\json_encode($msgLog),
                        'response_data' => $this->getResponseData()
                    ]);
                }

                $messageReturn["mixed_sku_type"] = 2;
                $messageReturn["one_asn_hdr_vtls"] = [];

                return $messageReturn;
            }
        } //else Multiple ASN HDRs
        else if (count($sortAsnHdrVtlCtns) > 1) {

            $msgLog = [
                "message" => sprintf("Mixed SKUs on pallet %s .", $pltRfid),
            ];
            $ctnRFID = "";
            $arrLoop = [];
            $arrOneAsnHdrVtls = [];

            //get all vtl ctn and asn detail
            foreach ($sortAsnHdrVtlCtns as $asnHdrId => $vtlCtnsAllAsnDtl) {

                //get carton follow ans detail
                $arrASNDtlVtlCtns = [];
                $transaction = '';
                foreach ($vtlCtnsAllAsnDtl as $vtl) {
                    $arrASNDtlVtlCtns[$vtl['asn_dtl_id']][] = $vtl;
                    $arrCntrVtlCtns[$vtl['ctnr_id']][] = $vtl;
                    $arrAsnHdrs[$vtl['asn_hdr_id']][] = $vtl;

                }
                if (count($arrCntrVtlCtns) > 1) {
                    $mixedSkuType = 2;
                } else {
                    $arrOneAsnHdrVtls[] = $arrASNDtlVtlCtns;
                }

                //one ctnr - an asn hdr
                $owner = $this->setErrorLogCtnsMultipleItemsOnOneANS($vtlCtnsAllAsnDtl);
                $transaction = $vtlCtnsAllAsnDtl[0]['ctnr_id'];
                $arrLoop[] = [
                    'owner'       => $owner,
                    'transaction' => $transaction,
                ];

                foreach ($arrASNDtlVtlCtns as $key => $values) {
                    $ctnRFID = array_pluck($values, 'ctn_rfid');

                    $msgLog['ASN DTL ' . $key] = $ctnRFID;
                }
            }

            if (count($sortAsnHdrVtlCtns) > 1) {
                $mixedSkuType = 3;
            }

            foreach ($arrLoop as $item) {
                Log::error($request, $whsId, [
                    'evt_code'      => 'EMS',
                    'owner'         => $item['owner'],
                    'transaction'   => $item['transaction'],
                    'url_endpoint'  => $url,
                    'message'       => \GuzzleHttp\json_encode($msgLog),
                    'response_data' => $this->getResponseData()
                ]);
            }

            $dataReturn = [
                "msgLog"           => $msgLog,
                "transaction"      => $arrLoop,
                "mixed_sku_type"   => $mixedSkuType,
                "one_asn_hdr_vtls" => $arrOneAsnHdrVtls
            ];

            return $dataReturn;
        }
    }

    public function setErrorLogCtnsMultipleItemsOnOneANS($vtlCtns)
    {
        $arrFirtCtn = $vtlCtns[0];
        $ansHdrObj = $this->asnHdrModel->getFirstWhere(['asn_hdr_id' => $arrFirtCtn['asn_hdr_id']]);
        $owner = $ansHdrObj['asn_hdr_num'];

        return $owner;
    }

    public function getASNDetailByVtlCtnRFID($arrCtnRfid)
    {
        $arrReturn = [];
        foreach ($arrCtnRfid as $rfid) {
            $arrVtlCtn = $this->vtlCtnModel->getFirstWhere(['ctn_rfid' => $rfid]);
            if (empty($arrVtlCtn)) {
                $msg = sprintf("There is carton's rfid %s is not defined.", $rfid);
                throw new \Exception($msg);
            }

            $asnDtl = $this->asnDtlModel->getFirstWhere(['asn_dtl_id' => $arrVtlCtn->asn_dtl_id]);
            if (empty($asnDtl)) {
                $msg = sprintf("ASN Detail %d is not existed.", $arrVtlCtn->asn_dtl_id);
                throw new \Exception($msg);
            }
            $arrReturn[$arrVtlCtn->asn_dtl_id]['rfids'][] = $rfid;
            $arrReturn[$arrVtlCtn->asn_dtl_id]['sku'] = 'SKU ' . $asnDtl->asn_dtl_sku . ' and Lot ' .
                $asnDtl->asn_dtl_lot;
        }

        return $arrReturn;
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param LocationModel $locationModel
     * @param SuggestLocationTransformer $suggestLocationTransformer
     * @param Request $request
     *
     * @deprecated  DO NOT USE IT
     *
     * @return Response|void
     */
    public function suggestEmptyRackLocation(
        $whsId,
        $cusId,
        LocationModel $locationModel,
        SuggestLocationTransformer $suggestLocationTransformer,
        Request $request
    ) {
        try {
            $params = [
                'whs_id' => $whsId,
                'cus_id' => $cusId,
                'type'   => 'RAC'
            ];

            /*
             * start logs
             */
            $input = $request->getParsedBody();

            $url = "/whs/$whsId/cus/$cusId/location/rack/get-empty-location";
            $owner = $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'ERL',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Suggest empty rack location'
            ]);
            /*
             * end logs
             */

            $locations = $locationModel->getEmptyRackLocation($params);
            if (empty($locations)) {
                $msg = Message::get("BM017", "Rack location");
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            return $this->response->item($locations, $suggestLocationTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getEmptyLocations($whsId, $rfid, Request $request, LocationModel $locationModel)
    {
        $input = $request->getQueryParams();

        /*
         * start logs
         */
        $url = "/whs/$whsId/pallet/$rfid/location/rack/get-empty-locations";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'ACC',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'get empty locations (RAC)'
        ]);

        /*
         * end logs
         */

        // check pallet in table pallet, vtl_ctn
        $pallet = $this->palletModel->getPalletByRfidUsing($whsId, $rfid);


        if (empty($pallet)) {
            $pallet = $this->vtlCtnModel->getFirstWhere([
                'plt_rfid'    => $rfid,
                'vtl_ctn_sts' => Status::getByValue('ASSIGNED', 'VIRTUAL-CARTON-STATUS')
            ]);

            if (empty($pallet)) {
                $dataReturn['message'] = sprintf("The pallet's RFID %s is not defined", $rfid);
                $palletOnRACK = $this->vtlCtnModel->getFirstWhere([
                    'plt_rfid'    => $rfid,
                    'vtl_ctn_sts' => Status::getByValue('RACKED', 'VIRTUAL-CARTON-STATUS')
                ]);
                if (!empty($palletOnRACK)) {
                    $dataReturn['message'] = sprintf("The pallet's RFID %s has been already on RACK.", $rfid);
                }

                $dataReturn['status'] = false;

                return $this->response->noContent()->setContent($dataReturn)
                    ->setStatusCode(Response::HTTP_OK);
            }
        }

        $cusId = $pallet->cus_id;

        $location = $locationModel->getEmptyLocationByCusId($whsId, $cusId);

        if (empty($location)) {
            $msg = [];
            $msg['status'] = false;
            $msg['message'] = sprintf("Current location is not defined in WMS system. Please contact Administrator");

            return $this->response->noContent()->setContent($msg)
                ->setStatusCode(Response::HTTP_OK);
        }

        $code = $location->loc_alternative_name;

        $locCode = explode("-", $code);

        $count = count($locCode);
        if ($count < 4 || $count > 5) {
            throw new \Exception("Loc Code is invalid!");
        }

        $aisle = $locCode[0];
        if ($count == 5) {
            $aisle = "{$locCode[0]}-{$locCode[1]}";
        }

        try {
            $locations = $this->locationModel->readWhsLayout($whsId, $aisle,
                ['pallet', 'zone.customerZone.customer', 'locationType'])
                ->toArray();

            $locIds = array_pluck($locations, 'loc_id');

            array_push($locIds, $location->loc_id);

            $carton = $this->cartonModel->getCartonsByLocIds($whsId, $locIds)->toArray();

            $vtCartons = $this->vtlCtnModel->getCartonsByLocIds($whsId, $locIds)->toArray();

            if ($vtCartons) {
                $carton = array_merge($vtCartons, $carton);
            }

            $carton = array_pluck($carton, null, 'loc_id');

            $layouts = null;

            $cusLocs = array_pluck($locations, 'zone.customer_zone.customer', 'loc_id');

            $customerColor = (new CustomerColorModel())->getAllCustomer()->toArray();

            $colors = array_pluck($customerColor, null, 'cus_id');

            $ec = false;

            $layouts = $this->makeLayouts($locations, $carton, $cusLocs, $colors, $ec, $code, true, false);

            $index = $this->findLocationIndex($layouts, $code);
            $useCus = [];
            $result = $this->getLocationsNearBy($layouts, $index, $useCus);

            $cl = null;
            if (!empty($useCus)) {
                $cl = array_map(function ($cl) use ($useCus) {
                    $cusId = array_get($cl, 'customer.cus_id', null);

                    if (empty($useCus[$cusId]) || empty($cusId)) {
                        return null;
                    } else {
                        return [
                            'cus_id'   => $cusId,
                            'cus_code' => array_get($cl, 'customer.cus_code', null),
                            'cus_name' => array_get($cl, 'customer.cus_name', null),
                            'cl_name'  => $cl['cl_name'],
                            'cl_code'  => $cl['cl_code'],
                        ];
                    }
                }, $colors);

                $cl = array_filter($cl);
            }

            if ($ec) {
                $cl["EC"] = [
                    'cus_id'   => null,
                    'cus_name' => null,
                    'cus_code' => "Eccomerce",
                    'cl_name'  => "Black",
                    'cl_code'  => "#000000",
                ];
            }

            $temp = [];
            if (!empty($result)) {
                foreach ($result as $key => $value) {
                    $temp[] = [
                        'level' => $key,
                        'data'  => $value,
                    ];
                }
                $result = $temp;
            }

            $locs = [];

            foreach ($cl as $item) {
                $locs[] = $item;
            }

            foreach ($result as $key => $val) {
                foreach ($val['data'] as $subKey => $item) {
                    if ($item['ctn_ttl'] > 0) {
                        $item['pallet'] = 1;
                        $val['data'][$subKey] = $item;
                    }
                }
                $result[$key] = $val;
            }

            $currentLoc = $this->getCurrentLocInfo($result);

            $data = [
                'data'    => [
                    'layout'               => $result,
                    'customer'             => $locs,
                    'cus_id'               => $cusId,
                    "pallet"               => $currentLoc['pallet'],
                    "loc_sts_code"         => $currentLoc['loc_sts_code'],
                    "ctn_ttl"              => $currentLoc['ctn_ttl'],
                    "piece_ttl"            => $currentLoc['piece_ttl'],
                    "sku_ttl"              => $currentLoc['sku_ttl'],
                    "sku"                  => $currentLoc['sku'],
                    "size"                 => $currentLoc['size'],
                    "color"                => $currentLoc['color'],
                    "lot"                  => $currentLoc['lot'],
                    "loc_alternative_name" => $currentLoc['loc_alternative_name'],
                    "loc_id"               => $currentLoc['loc_id'],
                    "loc_rfid"             => $currentLoc['loc_rfid'],
                    "scan"                 => $currentLoc['scan'],
                    "suggested"            => $currentLoc['suggested'],
                ],
                'message' => '',
                'status'  => true,
            ];

            return $this->response->noContent()->setContent($data)->setStatusCode(Response::HTTP_OK);

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    private function getSkuByPalletID($whsId, $pltId = null, $pltRFID)
    {
        $flag = true;
        $item = $this->cartonModel->getFirstWhere(
            [
                'whs_id' => $whsId,
                'plt_id' => $pltId,
            ]
        );

        if (!$item) {
            $item = $this->virtualCartonModel->getFirstWhere(
                [
                    'whs_id'   => $whsId,
                    'plt_rfid' => $pltRFID,
                ]
            );

            if (!$item) {
                $flag = false;
            }

            $itemObj = $this->itemModel->getFirstWhere(
                [
                    'item_id' => object_get($item, 'item_id', null)
                ]
            );
        }

        return object_get($item, 'sku', null);
    }

    public function simulatePalletOnRack($whsId, $rfid, Request $request, LocationModel $locationModel)
    {
        $input = $request->getQueryParams();
        $locRFID = array_get($input, 'loc_rfid', null);

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($rfid, RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $pltRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //validate location RFID
        $locRFIDValid = new RFIDValidate($locRFID, RFIDValidate::TYPE_LOCATION, $whsId);
        if (!$locRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $locRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        /*
         * start logs
         */

        $url = "/whs/$whsId/pallet/$rfid/drop-location";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'ACC',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'drop locations (RAC)'
        ]);

        /*
         * end logs
         */

        // check pallet in table pallet, vtl_ctn
        $pallet = $this->palletModel->getPalletByRfidUsing($whsId, $rfid);

        if (empty($pallet)) {
            $pallet = $this->vtlCtnModel->getFirstWhere([
                'plt_rfid'    => $rfid,
                'vtl_ctn_sts' => Status::getByValue('ASSIGNED', 'VIRTUAL-CARTON-STATUS')
            ]);
        }
        if (empty($pallet)) {
            $dataReturn['message'] = sprintf("The pallet's RFID %s is not defined", $rfid);
            $palletOnRACK = $this->vtlCtnModel->getFirstWhere([
                'plt_rfid'    => $rfid,
                'vtl_ctn_sts' => Status::getByValue('RACKED', 'VIRTUAL-CARTON-STATUS')
            ]);
            if (!empty($palletOnRACK)) {
                $dataReturn['message'] = sprintf("The pallet's RFID %s has been already on RACK.", $rfid);
            }

            $dataReturn['status'] = false;

            return $this->response->noContent()->setContent($dataReturn)
                ->setStatusCode(Response::HTTP_OK);
        }

        $cusId = $pallet->cus_id;
        $palletID = $pallet->plt_id;
        $params = [
            'whs_id'   => $whsId,
            'cus_id'   => $cusId,
            'type'     => 'RAC',
            'loc_rfid' => $locRFID,
        ];

        $scan = false;

        $location = $locationModel->getLocByRfId($whsId, $params['loc_rfid']);
        if (empty($location)) {

            $msg = [];
            $msg['status'] = false;
            $msg['message'] = sprintf("Current location is not defined in WMS system. Please contact Administrator");

            return $this->response->noContent()->setContent($msg)
                ->setStatusCode(Response::HTTP_OK);
        }

        $code = $location->loc_alternative_name;
        $scan = $params['loc_rfid'];

        $locCode = explode("-", $code);

        $count = count($locCode);
        if ($count < 4 || $count > 5) {
            throw new \Exception("Loc Code is invalid!");
        }

        $zone = $locCode[0];
        if ($count == 5) {
            $zone = "{$locCode[0]}-{$locCode[1]}";
        }

        try {
            $locations = $this->locationModel->readWhsLayout($whsId, $zone,
                ['pallet', 'zone.customerZone.customer', 'locationType'])
                ->toArray();
            $locIds = array_pluck($locations, 'loc_id');

            array_push($locIds, $location->loc_id);

            $carton = $this->cartonModel->getCartonsByLocIds($whsId, $locIds)->toArray();

            $vtCartons = $this->vtlCtnModel->getCartonsByLocIds($whsId, $locIds)->toArray();
            if ($vtCartons) {
                $carton = array_merge($vtCartons, $carton);
            }

            $carton = array_pluck($carton, null, 'loc_id');

            $layouts = null;

            $cusLocs = array_pluck($locations, 'zone.customer_zone.customer', 'loc_id');

            $customerColor = (new CustomerColorModel())->getAllCustomer()->toArray();

            $colors = array_pluck($customerColor, null, 'cus_id');

            $ec = false;
            $layouts = $this->makeLayouts($locations, $carton, $cusLocs, $colors, $ec, $code, true, $scan);
            $index = $this->findLocationIndex($layouts, $code);
            $useCus = [];
            $result = $this->getLocationsNearBy($layouts, $index, $useCus);

            $cl = [];
            if (!empty($useCus)) {
                $cl = array_map(function ($cl) use ($useCus) {
                    $cusId = array_get($cl, 'customer.cus_id', null);
                    if (empty($useCus[$cusId]) || empty($cusId)) {
                        return null;
                    } else {
                        return [
                            'cus_id'   => $cusId,
                            'cus_code' => array_get($cl, 'customer.cus_code', null),
                            'cus_name' => array_get($cl, 'customer.cus_name', null),
                            'cl_name'  => $cl['cl_name'],
                            'cl_code'  => $cl['cl_code'],
                        ];
                    }
                }, $colors);
                $cl = array_filter($cl);
            }

            if ($ec) {
                $cl["EC"] = [
                    'cus_id'   => null,
                    'cus_code' => "Eccomerce",
                    'cl_name'  => "Black",
                    'cl_code'  => "#000000",
                ];
            }

            $temp = [];
            if (!empty($result)) {
                foreach ($result as $key => $value) {
                    $temp[] = [
                        'level' => $key,
                        'data'  => $value,
                    ];
                }
                $result = $temp;
            }

            $locs = [];
            foreach ($cl as $item) {
                $locs[] = $item;
            }

            foreach ($result as $key => $val) {
                foreach ($val['data'] as $subKey => $item) {
                    if ($item['ctn_ttl'] > 0) {
                        $item['pallet'] = 1;
                        $val['data'][$subKey] = $item;
                    }
                }
                $result[$key] = $val;
            }

            $currentLoc = $this->getCurrentLocInfo($result);

            $data = [
                'data'    => [
                    'layout'               => $result,
                    'customer'             => $locs,
                    'cus_id'               => $cusId,
                    "pallet"               => $currentLoc['pallet'],
                    "loc_sts_code"         => $currentLoc['loc_sts_code'],
                    "ctn_ttl"              => $currentLoc['ctn_ttl'],
                    "piece_ttl"            => $currentLoc['piece_ttl'],
                    "sku_ttl"              => $currentLoc['sku_ttl'],
                    "sku"                  => $currentLoc['sku'],
                    "size"                 => $currentLoc['size'],
                    "color"                => $currentLoc['color'],
                    "lot"                  => $currentLoc['lot'],
                    "loc_alternative_name" => $currentLoc['loc_alternative_name'],
                    "loc_id"               => $currentLoc['loc_id'],
                    "loc_rfid"             => $currentLoc['loc_rfid'],
                    "scan"                 => $currentLoc['scan'],
                    "suggested"            => $currentLoc['suggested'],
                ],
                'message' => '',
                'status'  => true,
            ];

            return new Response($data, 200, [], null);

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function getCurrentLocInfo($layout)
    {
        $result = [];
        foreach ($layout as $val) {
            foreach ($val['data'] as $item) {
                if ($item['suggested'] == 1) {
                    $result = $item;
                    break;
                }
            }
        }

        return $result;
    }

    private function countCtnTtlCtnByPldt($whsId, $palletID = null, $rfid)
    {
        $count = $this->cartonModel->getModel()->where(
            [
                'whs_id' => $whsId,
                'plt_id' => $palletID,
            ]
        )->count();

        if (!$count) {
            $count = $this->virtualCartonModel->getModel()->where(
                [
                    'whs_id'   => $whsId,
                    'plt_rfid' => $rfid,
                ]
            )->count();
        }

        return $count;
    }

    /**
     * @param $whsId
     * @param LocationModel $locationModel
     * @param SuggestLocationTransformer $suggestLocationTransformer
     *
     * @return Response|void
     */
    public function suggestEmptyPutawayLocation(
        $whsId,
        Request $request,
        LocationModel $locationModel,
        SuggestLocationTransformer $suggestLocationTransformer
    ) {
        try {

            /*
             * start logs
             */
            $url = "/whs/$whsId/location/put-away/get-empty-location";
            $owner = $transaction = '';
            Log:: info($request, $whsId, [
                'evt_code'     => 'ACC',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'get empty put away locations (PAW)'
            ]);

            /*
             * end logs
             */

            $location = $locationModel->suggestAnEmptyPutAwayLocation($whsId);
            if (empty($location)) {
                $msg = Message::get("BM017", "Putaway location");
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            return $this->response->item($location, $suggestLocationTransformer);

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    /**
     * @param $asnDtlId
     *
     * @return mixed
     */
    private function getActCtn($asnDtlId)
    {
        return $this->vtlCtnModel->countCtnByASNDtlId($asnDtlId);
    }

    /**
     * @param $asnDtlId
     *
     * @return mixed
     */
    private function getActPlt($asnDtlId)
    {
        return $this->vtlCtnModel->countPltByASNDtlId($asnDtlId);
    }

    /**
     * @param $asnDtlId
     *
     * @return bool
     */
    private function checkDamage($asnDtlId)
    {
        return (bool)$this->vtlCtnModel->countCtnIsDamageByASNDtlId($asnDtlId);
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    private function eventTracking($params)
    {

        // event tracking asn
        $this->eventTrackingModel->refreshModel();

        return $this->eventTrackingModel->create([
            'whs_id'    => $params['whs_id'],
            'cus_id'    => $params['cus_id'],
            'owner'     => $params['owner'],
            'evt_code'  => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info'      => $params['info'],
        ]);
    }

    private function checkPltRFIDIsUsing($pltRFID)
    {
        $data = $this->palletModel->findWhere(
            [
                'rfid' => $pltRFID,
            ]
        );

        //if plt RFID not in using
        if (empty($data)) {
            return false;
        }

        //this RFID is existed on pallet table
        $pltIDs = [];
        foreach ($data as $plt) {
            $pltIDs[] = $plt->plt_id;
        }

        //check pallet is using
        $ctnObj = $this->cartonModel->getCartonByPltID($pltIDs);

        if (!empty($ctnObj)) {
            return true;
        }

        return false;
    }

    private function generatePalletNum($grHdrNum)
    {

        $palletCode = str_replace('GDR', 'LPN', $grHdrNum);

        $countPallet = \DB::table('pallet')->select('plt_num')
            ->where('plt_num', 'like', $palletCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();
        $max = 0;
        if ($countPallet) {
            $pltCode = array_get($countPallet, 'plt_num', null);
            $arrMax = explode('-', $pltCode);
            $max = (int)end($arrMax);
        }

        return $palletCode . "-" . str_pad($max + 1, 3, "0", STR_PAD_LEFT);
    }

    /**
     * @param Request $request
     * @param $whsId
     * @param $cusId
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function pickPalletOnPutAwayArea(Request $request, $whsId, $cusId)
    {
        $input = $request->getParsedBody();

        $rfid = array_get($input, 'pallet-rfid', null);
        $locCode = array_get($input, 'loc-code', null);

        $pallet = $this->palletModel->getFirstWhere([
            'rfid'   => $rfid,
            'cus_id' => $cusId,
            'whs_id' => $whsId
        ]);

        try {

            /*
            * start logs
            */

            $url = "/whs/$whsId/cus/$cusId/location/put-away/pick-pallet";
            $owner = $transaction = "";
            Log::info($request, $whsId, [
                'evt_code'     => 'POP',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Pick pallet on put away area'
            ]);
            /*
            * end logs
            */

            $msg = 'Pick pallet processing failed. Please contact admin !';
            DB::beginTransaction();
            $location = $this->locationModel->getLocWithType([
                'loc_code'   => $locCode,
                'loc_whs_id' => $whsId
            ], 'PAW');

            if (empty($location)) {
                throw new \Exception(Message::get("BM017", "Location Rfid"));
            }

            if ($pallet) {
                // Update Pallet
                $this->palletModel->refreshModel();
                $this->palletModel->update([
                    'plt_id'   => $pallet->plt_id,
                    'loc_id'   => null,
                    'loc_code' => null,
                    'loc_name' => null
                ]);

                // Update Carton
                $this->cartonModel->refreshModel();
                $this->cartonModel->updateWhere([
                    'loc_id'        => null,
                    'loc_code'      => null,
                    'loc_name'      => null,
                    'loc_type_code' => null,
                ], [
                    'plt_id' => $pallet->plt_id
                ]);

            } else {
                $this->virtualCartonModel->updateWhere(
                    [
                        'loc_rfid' => null,
                        'loc_id'   => null,
                        'loc_code' => null,
                    ],
                    ['plt_rfid' => $input['pallet-rfid']]
                );

            }
            $msg = sprintf("Picked pallet's RFID %s from location code %s successfully !", $rfid, $locCode);
            DB::commit();

            return $this->response->noContent()
                ->setContent(['Status' => $msg])
                ->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function getLocationPalletListByWvDtlId(Request $request)
    {
        $input = $request->getQueryParams();

        $validate = new LocationPalletListValidator();
        //$validate->validate($input);
        $requireFieldsValidate = $validate->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        //1 select primary_loc_id by wv_dtl_id
        $locId = null;
        $locObj = $this->wvDtlModel->getFirstWhere(
            [
                'wv_dtl_id' => $input['wv_dtl_id']
            ]
        );
        if (is_null($locObj)) {
            $msg = sprintf("Wave pick detail %d is not existed", $input['wv_dtl_id']);
            throw new \Exception($msg);
        }

        $returnData = [
            'sku'   => $locObj->sku,
            'size'  => $locObj->size,
            'color' => $locObj->color
        ];
        $locId = $locObj->primary_loc_id;

        //2. select pallet table where pallet.loc_id = $locID
        $arrPalletLoc = $this->palletModel->findWhere(
            [
                'loc_id' => $locId
            ]
        );
        if (is_null($arrPalletLoc)) {
            $msg = sprintf("There is no assigned on location %d .", $locId);
            throw new \Exception($msg);
        }
        $returnData['location'] = $arrPalletLoc->toArray();

        return $returnData;
    }

    /*
     *get list pallet picking
     */
    public function getPalletPickingList($whsId, Request $request)
    {
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        // get pallet have location picking
        $locations = $this->locationModel->getPalletLocPick($input);
        $data['data'] = $this->formatArr($locations, true);

        return new Response($data, 200, [], null);
    }

    /**
     * @param $layouts
     * @param $index
     * @param $useCus
     *
     * @return array
     */
    private function getLocationsNearBy($layouts, $index, &$useCus)
    {
        $result = [];

        foreach ($layouts as $level => $layout) {

            // Index - 1
            if (!empty($layout[$index - 1])) {
                $result[$level][$index - 1] = $layout[$index - 1];
                $cus_id = array_get($layout, ($index - 1) . ".customer.cus_id", null);
                $useCus[$cus_id] = $cus_id;
            } else {
                $result[$level][$index + 3] = array_get($layout, $index + 3, null);
                $cus_id = array_get($layout, ($index + 3) . ".customer.cus_id", null);
                $useCus[$cus_id] = $cus_id;
            }

            // Index - 2
            if (!empty($layout[$index - 2])) {
                $result[$level][$index - 2] = $layout[$index - 2];
                $cus_id = array_get($layout, ($index - 2) . ".customer.cus_id", null);
                $useCus[$cus_id] = $cus_id;
            } else {
                if (!isset($result[$level][$index + 3])) {
                    $result[$level][$index + 3] = array_get($layout, $index + 3, null);
                    $cus_id = array_get($layout, ($index + 3) . ".customer.cus_id", null);
                    $useCus[$cus_id] = $cus_id;
                } else {
                    $result[$level][$index + 4] = array_get($layout, $index + 4, null);
                    $cus_id = array_get($layout, ($index + 4) . ".customer.cus_id", null);
                    $useCus[$cus_id] = $cus_id;
                }
            }

            // Index
            //$result[$level][$index] = $layout[$index];
            $result[$level][$index] = !empty($layout[$index]) ? $layout[$index] : null;

            $cus_id = array_get($layout, "$index.customer.cus_id", null);
            $useCus[$cus_id] = $cus_id;
            // Index + 1
            if (!empty($layout[$index + 1])) {
                $result[$level][$index + 1] = $layout[$index + 1];
                $cus_id = array_get($layout, ($index + 1) . ".customer.cus_id", null);
                $useCus[$cus_id] = $cus_id;
            } else {
                $result[$level][$index - 3] = array_get($layout, $index - 3, null);
                $cus_id = array_get($layout, ($index - 3) . ".customer.cus_id", null);
                $useCus[$cus_id] = $cus_id;


            }
            // Index + 2
            if (!empty($layout[$index + 2])) {
                $result[$level][$index + 2] = $layout[$index + 2];
                $cus_id = array_get($layout, ($index + 2) . ".customer.cus_id", null);
                $useCus[$cus_id] = $cus_id;
            } else {
                if (!isset($result[$level][$index - 3])) {
                    $result[$level][$index - 3] = array_get($layout, $index - 3, null);
                    $cus_id = array_get($layout, ($index - 3) . ".customer.cus_id", null);
                    $useCus[$cus_id] = $cus_id;
                } else {
                    $result[$level][$index - 4] = array_get($layout, $index - 4, null);
                    $cus_id = array_get($layout, ($index - 4) . ".customer.cus_id", null);
                    $useCus[$cus_id] = $cus_id;
                }

            }

            ksort($result[$level]);
            $result[$level] = array_values($result[$level]);
        }

        return $result;
    }

    /**
     * @param $layouts
     * @param $findCode
     *
     * @return int|null
     */
    private function findLocationIndex($layouts, $findCode)
    {
        $index = null;

        foreach ($layouts as $level => $layout) {
            $keySearch = array_search($findCode, array_column($layout, "loc_alternative_name"));
            if ($keySearch === 0 || $keySearch > 0) {

                $index = $keySearch;
                break;
            }
        }

        return $index;
    }

    /**
     * @param $locations
     * @param $carton
     * @param $cusLocs
     * @param $colors
     * @param $ec
     * @param string $curLoc
     * @param bool|false $check
     * @param bool|false $scan
     *
     * @return array
     */
    private function makeLayouts(
        $locations,
        $carton,
        $cusLocs,
        $colors,
        &$ec,
        $curLoc = "",
        $check = false,
        $scan = false
    ) {

        $layouts = [];
        foreach ($locations as $location) {

            $loc = explode("-", $location['loc_alternative_name']);

            $count = count($loc);
            if ($count < 4 || $count > 5) {
                continue;
            }

            end($loc);
            $endKey = key($loc);

            if (empty($layouts[$loc[$endKey - 1]])) {
                $layouts[$loc[$endKey - 1]] = [];
            }

            $customer = null;
            $locId = $location['loc_id'];
            $locRfid = $location['rfid'];

            if (!empty($cusLocs[$locId])) {
                $cusId = $cusLocs[$locId]['cus_id'];
                $customer = [
                    'cus_id'   => array_get($cusLocs, "$locId.cus_id", null),
                    'cus_name' => array_get($cusLocs, "$locId.cus_name", null),
                    'color'    => array_get($colors, "$cusId.cl_code", null)
                ];
            }

            $locType = array_get($location, 'location_type.loc_type_code', null);
            if ($locType == "ECO") {
                $ec = true;
                $customer = [
                    'cus_id'   => null,
                    'cus_name' => null,
                    'color'    => "#000000"
                ];
            }

            if (!$check) {

                $layouts[$loc[$endKey - 1]][] = [

                    'customer'             => $customer,
                    'pallet'               => ($location['pallet'] ? 1 : 0),
                    'loc_sts_code'         => $this->palletModel->formatStatus('location', $location['loc_sts_code']),
                    'ctn_ttl'              => (int)array_get($carton, $locId . '.ctn_ttl', 0),
                    'piece_ttl'            => (int)array_get($carton, $locId . '.piece_ttl', 0),
                    'sku_ttl'              => (int)array_get($carton, $locId . '.sku_ttl', 0),
                    'sku'                  => array_get($carton, $locId . '.sku', null),
                    'size'                 => array_get($carton, $locId . '.size', null),
                    'color'                => array_get($carton, $locId . '.color', null),
                    'lot'                  => array_get($carton, $locId . '.lot', null),
                    'loc_alternative_name' => $location['loc_alternative_name'],
                    'loc_id'               => $locId,
                    'loc_rfid'             => $locRfid,
                    'suggested'            => $curLoc == $location['loc_alternative_name'] ? 1 : 0,
                ];
            } else {
                $parScan = false;
                if ($scan) {
                    $parScan = $locRfid == $scan ? true : false;
                }

                $layouts[$loc[$endKey - 1]][] = [
                    'customer'             => $customer,
                    'pallet'               => ($location['pallet'] ? 1 : 0),
                    'loc_sts_code'         => $this->palletModel->formatStatus('location', $location['loc_sts_code']),
                    'ctn_ttl'              => (int)array_get($carton, $locId . '.ctn_ttl', 0),
                    'piece_ttl'            => (int)array_get($carton, $locId . '.piece_ttl', 0),
                    'sku_ttl'              => (int)array_get($carton, $locId . '.sku_ttl', 0),
                    'sku'                  => array_get($carton, $locId . '.sku', null),
                    'size'                 => array_get($carton, $locId . '.size', null),
                    'color'                => array_get($carton, $locId . '.color', null),
                    'lot'                  => array_get($carton, $locId . '.lot', null),
                    'loc_alternative_name' => $location['loc_alternative_name'],
                    'loc_id'               => $locId,
                    'loc_rfid'             => $locRfid,
                    'scan'                 => $parScan,
                    'suggested'            => $curLoc == $location['loc_alternative_name'] ? 1 : 0,
                ];
            }
        }

        krsort($layouts);

        return $layouts;
    }


    /**
     * @param Request $request
     * @param $whsId
     * @param $wvDtlID
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function updateWavePick(Request $request, $whsId, $wvDtlID)
    {
        set_time_limit(0);
        // get data from HTTP
        $input = $request->getParsedBody();
        $type = $input['type'];
        $codes = array_get($input, 'codes', null);

        if (!$type) {
            $msg = "Need input type of picking.";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            switch ($type) {
                //pick full carton
                case "CT":
                    if (!$codes) {
                        $msg = "Need input location codes.";
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }
                    $dataWaveByCartons = [
                        'whs_id'    => $whsId,
                        'wv_dtl_id' => $wvDtlID,
                        'codes'     => $codes
                    ];
                    $this->updateWavePickByCarton($dataWaveByCartons);
                    break;

                //pick full pallet
                case "LC":
                    if (!$codes) {
                        $msg = "Need input location codes.";
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }
                    $dataWaveByPallets = [
                        'whs_id'    => $whsId,
                        'wv_dtl_id' => $wvDtlID,
                        'codes'     => $codes
                    ];

                    $cartonCode = $this->updateWavePickByLocation($dataWaveByPallets);

                    // check number
                    $dataWaveByCartons = $dataWaveByCartons = [
                        'whs_id'    => $whsId,
                        'wv_dtl_id' => $wvDtlID,
                        'codes'     => $cartonCode

                    ];
                    $this->updateWavePickByCarton($dataWaveByCartons);
                    break;

                //pick piece
                case "EA":
                    $dataWaveByEA = [
                        'whs_id'    => $whsId,
                        'ctn_id'    => array_get($input, 'ctn_id', null),
                        'ctnr_rfid' => array_get($input, 'ctnr_rfid', null),
                        'piece_qty' => array_get($input, 'piece_qty', null),
                        'wv_dtl_id' => $wvDtlID
                    ];
                    $wvPickEA = new WavePickEAValidator();
                    $wvPickEA->validate($dataWaveByEA);

                    $this->updateWavePickByEA($dataWaveByEA);
                    break;
                default:
                    break;
            }

            $wvDtl = $this->wvDtlModel->getFirstWhere(['wv_dtl_id' => $wvDtlID]);

            $event = [
                'whs_id'    => $whsId,
                'cus_id'    => object_get($wvDtl, 'cus_id'),
                'owner'     => object_get($wvDtl, 'wv_num'),
                'evt_code'  => 'RFW',
                'trans_num' => 'RFG',
                'info'      => sprintf("Update Wave Pick by %s", ($type == 'CT') ? 'Carton' : 'Location'),
            ];

            //  Evt tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($event);

            return $this->response->noContent()
                ->setContent(['data' => ['message' => 'Update wave pick successfully']])
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * Update wave pick by carton
     *
     * @param $data
     */
    private function updateWavePickByCarton($data)
    {
        $whs_id = $data['whs_id'];
        $wv_dtl_id = $data['wv_dtl_id'];
        $codes = $data['codes'];

        $ctnLists = $this->cartonModel->checkActiveCartonByCtnNum($codes)->toArray();

        if (count($ctnLists) == 0) {
            $msg = Message::get("BM155");
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        // Check active & existed carton
        $dataCheckActive = [
            'ctnNums'  => $codes,
            'ctnLists' => $ctnLists
        ];
        $this->checkActiveCarton($dataCheckActive);

        // Get wavepick detail info
        $dataWaveDtlInfo = $this->wvDtlModel->getFirstWhere([
            'wv_dtl_id' => $wv_dtl_id
        ]);

        // Check wave pick detail existed
        if (empty($dataWaveDtlInfo)) {
            $msg = Message::get("BM017", "Wavepick detail " . $wv_dtl_id);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $itemId = array_get($dataWaveDtlInfo, 'item_id', null);
        $cusId = array_get($dataWaveDtlInfo, 'cus_id', null);
        $wv_id = array_get($dataWaveDtlInfo, 'wv_id', null);
        $piece_qty = array_get($dataWaveDtlInfo, 'piece_qty', null);
        $act_piece_qty = array_get($dataWaveDtlInfo, 'act_piece_qty', 0);

        // Get all carton by item id
        $dataGetCtnInfo = [
            'whsId'   => $whs_id,
            'itemId'  => $itemId,
            'cusId'   => $cusId,
            'ctnNums' => $codes
        ];

        $dataCartonInfos = $this->cartonModel->getAllCartonByItemId($dataGetCtnInfo)->toArray();

        if (count($dataCartonInfos) == 0) {
            $msg = Message::get("BM017", "Carton with item " . $itemId);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $dataCheckCartonHasItem = [
            'item_id' => $itemId,
            'ctnNums' => $codes,
            'cartons' => $dataCartonInfos,
        ];

        // Check carton has item_id or not
        $this->checkCartonHasItemId($dataCheckCartonHasItem);

        // Start to handle valid cartons
        $sumPieceRemainFromCtns = 0;
        foreach ($dataCartonInfos as $dataCartonInfo) {
            $sumPieceRemainFromCtns += $dataCartonInfo['piece_remain'];
        }

        $actPicked = $act_piece_qty + $sumPieceRemainFromCtns;

        if ($actPicked > $piece_qty) {
            $msg = Message::get("BM156", $actPicked, $piece_qty);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        // Update wave pick detail
        $dataUpdateWvDtl = [
            'act_piece_qty' => $actPicked,
            'wv_dtl_id'     => $wv_dtl_id,
            'wv_dtl_sts'    => ($actPicked < $piece_qty
                ? Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS")
                : Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS"))
        ];

        \DB::beginTransaction();
        $this->wvDtlModel->updateWvDtlPickCtn($dataUpdateWvDtl);

        /*
         * ==================================================================
         * HANDLE TABLES: CARTONS, PALLET, ORDER CARTON WHEN PICK FULL CARTON
         * ==================================================================
         * */

        // insert order carton
        $dataOrderCarton = [
            'dataCartonInfos' => $dataCartonInfos,
        ];

        $this->processOrderCarton($dataOrderCarton);

        // update carton status, release all carton from pallet & location
        $this->updateCartonStatusToPicked($dataCartonInfos);

        // update total carton in pallet table, also release pallet from location if has no carton
        $this->updateCtnTtl($dataCartonInfos);

        $this->updateWaveHeaderPicked($wv_id);

        /*
         * ==================================================================
         * END PROCESS
         * ==================================================================
         * */
        \DB::commit();
    }

    /**
     * Update wave pick by loction
     *
     * @param $params
     *
     * @return array
     */
    private function updateWavePickByLocation($params)
    {
        // Wave Pick Detail
        $wvDtl = $this->wvDtlModel->getFirstWhere(['wv_dtl_id' => $params['wv_dtl_id']]);

        if (empty($wvDtl)) {
            $msg = "Wave Pick detail is empty!";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $ctnTotal = ceil(($wvDtl->piece_qty - $wvDtl->act_piece_qty) / $wvDtl->pack_size);

        $locations = $this->locationModel->getByLocCode($params['codes'], $params['whs_id'])->toArray();
        $locationCodes = array_pluck($locations, 'loc_id', 'loc_code');
        foreach ($params['codes'] as $code) {
            if (empty($locationCodes[$code])) {
                $msg = "$code is invalid!";
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }

        $result = $this->cartonModel->getCartonsByLocs($locationCodes)->toArray();

        $result = array_column($result, 'ctn_num');
        if (count($result) == 0) {
            $msg = "There're no cartons in this warehouse!";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }
        // only pick total cartons of location <= total carton
        if (count($result) > $ctnTotal) {
            $msg = "Total pieces of locations larger needed pick!";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        return $result;
    }

    private function checkActiveCarton($dataCheckActive)
    {
        $ctnNums = $dataCheckActive['ctnNums'];
        $ctnLists = $dataCheckActive['ctnLists'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id'  => $e['ctn_id'],
                'ctn_num' => $e['ctn_num'],
            ];
        }, $ctnLists);
        $allCtns = array_pluck($tempCtns, null, "ctn_num");

        foreach ($ctnNums as $key => $value) {
            if (!isset($allCtns[$value])) {
                $msg = "Carton " . $value . " is not active or existed";
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }
    }

    private function checkCartonHasItemId($dataCheckCarton)
    {
        $itemId = $dataCheckCarton['item_id'];
        $ctnNums = $dataCheckCarton['ctnNums'];
        $ctnLists = $dataCheckCarton['cartons'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id'  => $e['ctn_id'],
                'ctn_num' => $e['ctn_num'],
            ];
        }, $ctnLists);
        $allCtns = array_pluck($tempCtns, null, "ctn_num");

        foreach ($ctnNums as $key => $value) {

            if (!isset($allCtns[$value])) {
                $msg = "Carton " . $value . " does not has item " . $itemId;
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }
    }

    private function processOrderCarton($dataOrderCarton)
    {
        $dataCartonInfos = $dataOrderCarton['dataCartonInfos'];

        foreach ($dataCartonInfos as $dataCartonInfo) {
            $item = [
                'ctn_num'  => $dataCartonInfo['ctn_num'],
                'ctn_id'   => $dataCartonInfo['ctn_id'],
                'ctn_rfid' => $dataCartonInfo['ctn_rfid'],
            ];
            $dataOrderCarton = [
                'item'       => $item,
                'picked_qty' => array_get($dataCartonInfo, 'piece_remain', null),
                'is_storage' => 0,
            ];
            $this->insertOrderCarton($dataOrderCarton);
        }
    }

    private function insertOrderCarton($data)
    {
        $item = $data['item'];
        $picked_qty = $data['picked_qty'];
        $isStorage = $data['is_storage'];

        $dataOrderCartonInsert = [
            'ctn_num'    => $item['ctn_num'],
            'ctn_id'     => $item['ctn_id'],
            'piece_qty'  => $picked_qty,
            'ship_dt'    => 0,
            'ctn_rfid'   => $item['ctn_rfid'],
            'sts'        => Status::getByKey('CTN_STATUS', 'PICKED'),
            'is_storage' => $isStorage
        ];

        $this->orderCartonModel->refreshModel();
        $this->orderCartonModel->create($dataOrderCartonInsert);
    }

    private function updateCartonStatusToPicked($dataCartonInfos)
    {
        foreach ($dataCartonInfos as $ctn) {
            $created_at = (int)array_get($ctn, 'created_at', 0);

            $picked_dt = time();

            // Calculate storage_duration
            $date1 = date("Y-m-d", is_int($created_at) || is_string($created_at) ? (int)$created_at :
                $created_at->timestamp);

            $date2 = date("Y-m-d");

            $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }
            $dataCartonUpdate = [
                'ctn_sts'          => Status::getByKey('CTN_STATUS', 'PICKED'),
                'picked_dt'        => $picked_dt,
                'storage_duration' => $storageDuration,
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'plt_id'           => null,
            ];

            $this->cartonModel->updateWhere($dataCartonUpdate, [
                'ctn_num' => $ctn['ctn_num']
            ]);

            // Update ctn_ttl in pallet table
            $plt_id = array_get($ctn, 'plt_id', null);

            $palletInfo = $this->palletModel->getFirstWhere(['plt_id' => $plt_id]);

            $this->palletModel->updateWhere([
                'ctn_ttl' => array_get($palletInfo, 'ctn_ttl', null) - 1
            ], [
                'plt_id' => $plt_id
            ]);
        }
    }

    private function updateCtnTtl($dataCartonInfos)
    {
        $arrLocIds = [];
        $arrPltIds = [];
        foreach ($dataCartonInfos as $dataCartonInfo) {
            if (!in_array($dataCartonInfo['loc_id'], $arrLocIds)) {
                array_push($arrLocIds, $dataCartonInfo['loc_id']);
            }

            if (!in_array($dataCartonInfo['plt_id'], $arrPltIds)) {
                array_push($arrPltIds, $dataCartonInfo['plt_id']);
            }
        }
        $this->updatePalletWhenPickCarton($arrPltIds);
    }

    private function updatePalletWhenPickCarton($pltIds)
    {
        $allPalletHasNoCartons = $this->palletModel->getAllPalletHasNoCarton($pltIds);
        foreach ($allPalletHasNoCartons as $allPalletHasNoCarton) {
            $this->palletModel->updatePallet($allPalletHasNoCarton);
        }
    }

    private function updateWaveHeaderPicked($wvHdrId)
    {
        $allDetail = DB::table('wv_dtl')->select(['wv_dtl_sts'])->where(['wv_id' => $wvHdrId])->get();
        if (!empty($allDetail)) {
            $picked = 1;
            $picking = 0;
            foreach ($allDetail as $detail) {

                if ($detail['wv_dtl_sts'] == Status::getByValue('Picking', 'WAVE_DETAIL_STATUS')) {
                    $picking = true;
                    break;
                }

                if ($detail['wv_dtl_sts'] != Status::getByValue('Picked', 'WAVE_DETAIL_STATUS')) {
                    $picked = 0;
                }
            }

            if ($picking) {
                $this->waveHdrModel->update(
                    [
                        'wv_id'  => $wvHdrId,
                        'wv_sts' => Status::getByValue('Picking', 'WAVE_HDR_STATUS')
                    ]);
            } else if ($picked) {
                $this->waveHdrModel->update(
                    [
                        'wv_id'  => $wvHdrId,
                        'wv_sts' => Status::getByValue('Picked', 'WAVE_HDR_STATUS')
                    ]
                );
            }
        }
    }

    private function updateWavePickByEA($data)
    {
        try {
            \DB::beginTransaction();

            $checkExistOdrCtn = $this->orderCartonModel->getFirstWhere([
                'ctn_rfid' => $data['ctnr_rfid'],
                'is_ctnr'  => 1,
                'ctn_sts'  => Status::getByValue('PICKING', 'ODR_CTN_STATUS')
            ]);
            if (!empty($checkExistOdrCtn)) {
                $msg = sprintf("This carton's id : %d is using picking.", $data['ctn_rfid']);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
            //1 get all table ctn where ctn_id = param, and sts: AC
            $carton = $this->cartonModel->getFirstWhere(
                [
                    'ctn_id'  => $data['ctn_id'],
                    'ctn_sts' => Status::getByValue('ACTIVE', 'CTN_STATUS'),
                ]);

            if (is_null($carton)) {
                $msg = sprintf("Carton's id : %d is not existed.", $data['ctn_id']);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //get wave dtl info
            $wv_dtl = $this->wvDtlModel->getFirstBy('wv_dtl_id', $data['wv_dtl_id']);

            //check item id in carton same with wv dtl's item_id
            if ($wv_dtl->item_id != $carton->item_id) {
                $msg = sprintf("The carton's SKU is not in wave pick's SKU.");
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $isStorage = 1;
            if ($carton->piece_remain < $data['piece_qty']) {
                $msg = sprintf("Current carton %s quantity is less than picked quantity", $carton->rfid);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            if ($carton->piece_remain == $data['piece_qty']) {
                $isStorage = 0;

                //update carton status
                $cartonUpdated = $this->cartonModel->updateWhere(
                    [
                        'ctn_sts' => Status::getByKey('CTN_STATUS', 'PICKED'),
                    ],
                    [
                        'ctn_id' => $data['ctn_id']
                    ]
                );
            } else {
                //update piece_remain of carton
                $cartonUpdated = $this->cartonModel->updateWhere(
                    [
                        'piece_remain' => $carton->piece_remain - $data['piece_qty'],
                    ],
                    [
                        'ctn_id' => $data['ctn_id']
                    ]
                );
            }

            //insert carton result by step 1 to table ord_carton set piece_remain = old number -  piece_qty (param)
            $dataOrdCtn = [
                'ctn_num'    => $carton->ctn_num,
                'ctn_rfid'   => $data['ctnr_rfid'],
                'piece_qty'  => $data['piece_qty'],
                'sts'        => 'i',
                'ctn_sts'    => Status::getByKey('CTN_STATUS', 'PICKED'), //picking
                'ctn_id'     => $carton->ctn_id,
                'is_storage' => $isStorage,
                'wv_hdr_id'  => $wv_dtl->wv_id,
                'wv_dtl_id'  => $data['wv_dtl_id'],
                'wv_num'     => $wv_dtl->wv_num,
                'created_at' => time(),
                'is_ctnr'    => 1
            ];

            $this->orderCartonModel->create($dataOrdCtn);

            //3. update table int_sum, set  allocated_qty = (old) allocated_qty - piece_qty (param)
            $invt_smr = $this->InventorySummaryModel->getFirstBy('item_id', $wv_dtl->item_id);

            $this->InventorySummaryModel->updateWhere(
                [
                    'allocated_qty' => $invt_smr->allocated_qty - $data['piece_qty'],
                    'picked_qty'    => $invt_smr->picked_qty + $data['piece_qty']
                ],
                [
                    'item_id' => $wv_dtl->item_id,
                    'whs_id'  => $data['whs_id'],
                ]
            );

            // update status Wv_dt table
            if ($wv_dtl->act_piece_qty + $data['piece_qty'] > $wv_dtl->piece_qty) {
                $msg = sprintf("Current carton %s quantity is more than picked quantity", $carton->rfid);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $vvDtlSts = Status::getByValue('Picking', 'WAVE_DETAIL_STATUS');
            if ($wv_dtl->act_piece_qty + $data['piece_qty'] == $wv_dtl->piece_qty) {
                $vvDtlSts = Status::getByValue('Picked', 'WAVE_DETAIL_STATUS');
            }

            $this->wvDtlModel->updateWhere([
                'wv_dtl_sts'    => $vvDtlSts,
                'act_piece_qty' => $data['piece_qty'] + $wv_dtl->act_piece_qty
            ], [
                'wv_dtl_id' => $data['wv_dtl_id']
            ]);

            //6. update status Wv_dtl_hdr table
            $check = $this->wvDtlModel->checkStaCO($wv_dtl->wv_id);
            $vvHdrSts = Status::getByValue('Picking', 'WAVE_HDR_STATUS');
            if (empty($check)) {
                $vvHdrSts = Status::getByValue('Picked', 'WAVE_HDR_STATUS');
            }
            $this->waveHdrModel->updateWhere([
                'wv_sts' => $vvHdrSts
            ], [
                'wv_id' => $wv_dtl->wv_id
            ]);

            \DB::commit();

            return true;
        } catch (Exception $e) {
            \DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function putPalletShippingLane($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $plt_num = array_get($input, 'plt_num', null);
        $loc_rfid = array_get($input, 'loc_rfid', null);
        $checkPlt = $this->outPalletModel->getFirstBy('plt_num', $plt_num);
        $checkLoc = $this->locationModel->getLocShippingLane($whsId, $loc_rfid);

        if (empty($checkPlt)) {
            $msg = sprintf("The pallet num %s is not exists", $plt_num);
            throw new \Exception($msg);
        }
        if (empty($checkLoc)) {
            $msg = sprintf("The location rfid %s is not exists", $loc_rfid);
            throw new \Exception($msg);
        }

        try {

            // start transaction
            DB::beginTransaction();

            // update location in out_pallet
            $dataPlt = [
                'loc_id'     => $checkLoc->loc_id,
                'loc_name'   => $checkLoc->loc_alternative_name,
                'loc_code'   => $checkLoc->loc_code,
                'updated_at' => time()
            ];

            $this->outPalletModel->updateWhere($dataPlt, ['plt_id' => $checkPlt->plt_id]);

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['message'] = sprintf("Pallet number %s updated successfully on location %s!", $plt_num, $loc_rfid);

            return $this->response->noContent()
                ->setContent(['data' => $msg])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function putCartonPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $pallet = array_get($input, 'pallet', null);
        $ctns = array_get($input, 'ctns', null);

        // check pallet num
        if (is_numeric(strpos($pallet, 'LPN'))) {
            $checkPlt = $this->outPalletModel->getFirstWhere([
                'whs_id'      => $whsId,
                'plt_num'     => $pallet,
                'out_plt_sts' => Status::getByValue('Active', 'PACK-TYPE')
            ]);
        } else {
            $checkPlt = $this->outPalletModel->getFirstWhere([
                'whs_id'      => $whsId,
                'rfid'        => $pallet,
                'out_plt_sts' => Status::getByValue('Active', 'PACK-TYPE')
            ]);
        }

        if (empty($checkPlt)) {
            $msg = sprintf("The pallet num %s is not existed", $pallet);
            throw new \Exception($msg);
        }

        // check ctns
        $str = '';
        foreach ($ctns as $ctn) {
            if (is_numeric(strpos($ctn, 'CTN'))) {
                $checkCtn = $this->cartonModel->getFirstWhere([
                    'ctn_num' => $ctn,
                    // pending waiting Mr Thanh or Cuong sida
                    // 'ctn_sts' => Status::getByKey('CTN_STATUS', 'ACTIVE')
                ]);
            } else {
                $checkCtn = $this->cartonModel->getFirstWhere([
                    'rfid' => $ctn,
                    // pending waiting Mr Thanh or Cuong sida
                    // 'ctn_sts' => Status::getByKey('CTN_STATUS', 'ACTIVE')
                ]);
            }

            if (empty($checkCtn)) {
                $str = $str . ', ' . $ctn;
            }

        }
        $str = trim(substr($str, 1));
        if (!empty($str)) {
            $msg = sprintf("The ctns %s is not existed", $str);
            throw new \Exception($msg);
        }

        try {
            // start transaction
            DB::beginTransaction();

            // update location in out_pallet
            foreach ($ctns as $ctn) {
                if (is_numeric(strpos($ctn, 'CTN'))) {
                    $ctnDt = $this->cartonModel->getFirstWhere([
                        'ctn_num' => $ctn
                    ]);
                } else {
                    $ctnDt = $this->cartonModel->getFirstWhere([
                        'rfid' => $ctn
                    ]);
                }

                $dataPlt = [
                    'out_plt_id' => $checkPlt->plt_id,
                    // pending waiting Mr Thanh or Cuong sida
                    //'pack_sts'
                    'updated_at' => time()
                ];
                $this->packHdrModel->updateWhere($dataPlt, ['ctn_id' => $ctnDt->ctn_id]);
            }
            // count totalCtn
            $totalCtn = $this->packHdrModel->countCtnByPalletId($checkPlt->plt_id);

            $this->outPalletModel->updateWhere([
                'ctn_ttl'    => $totalCtn,
                'updated_at' => time()
            ],
                [
                    'plt_id' => $checkPlt->plt_id
                ]);

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['message'] = sprintf("Pallet number %s updated successfully!", $pallet);

            return $this->response->noContent()
                ->setContent(['data' => $msg])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function checkPalletIsOnRack($whsId, $rfid, Request $request)
    {

        $url = "/whs/$whsId/pallet/$rfid/location/rack/check-pallet-on-rack";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Check pallet has loc code'
        ]);

        try {
            $pltObj = $this->palletModel->checkPalletHasLocation($whsId, $rfid);
            if ($pltObj) {
                $msg = sprintf("Pallet %s is existing.", $rfid);
                $data = [
                    'data'    => [
                        'onRack' => true,
                    ],
                    'message' => $msg,
                    'status'  => true,
                ];

                return new Response($data, 200, [], null);
            }

            $msg = sprintf("Pallet %s is not on RACK.", $rfid);
            $data = [
                'data'    => [
                    'onRack' => false,
                ],
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    public function checkPalletExist($whsId, $rfid, Request $request)
    {
        $url = "/whs/$whsId/pallet/$rfid/check-pallet-exist";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'ULR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Check pallet exist or not'
        ]);

        try {
            $pltObj = $this->palletModel->checkPalletExist($whsId, $rfid);
            $vtlObj = $this->virtualCartonModel->checkPalletExist($whsId, $rfid);
            if ($pltObj || $vtlObj) {
                $msg = sprintf("Pallet %s is existing.", $rfid);
                $data = [
                    'data'    => [
                        'pallet' => $rfid,
                    ],
                    'message' => $msg,
                    'status'  => true,
                ];

                return new Response($data, 200, [], null);
            }

            $msg = sprintf("Pallet %s is not existing.", $rfid);
            $data = [
                'data'    => [
                    'pallet' => $rfid,
                ],
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    /**
     * Update virtual carton
     *
     * @param String $ctnRfid
     *
     * @return boolean
     */
    public function updateVirtualCartons($ctnRfid)
    {

        try {
            DB::beginTransaction();
            $this->goodsReceiptModel->updateVtlCtnStatusGRCreatedByRfid($ctnRfid);
            DB::commit();

            return true;
        } catch (Exception $ex) {

            return false;
        }

    }

    public function checkPalletExistAndCreateCarton($whsId, $pltRfid, $trueRFIDs)
    {
        /**
         * Check pallet existed or not
         * existed, delete cartons/virtual cartons
         * 1. delete cartons and virtual cartons where plt_id = and carton rfid not in($trueRFIDs)
         *
         * $wrongRFIDS = undefined rfids, insert virtual carton and cartons with same asn, palllet
         */


        $pallet = $this->palletModel->getModel()
            ->where([
                'rfid'   => $pltRfid,
                'whs_id' => $whsId
            ])
            ->first();

        if ($pallet) {
            $test = $this->virtualCartonModel->getModel()
                ->where([
                    'plt_rfid' => $pltRfid
                ])
                ->whereNotIn('ctn_rfid', $trueRFIDs)
                ->delete();

            $test = $this->cartonModel->getModel()
                ->where([
                    'plt_id' => object_get($pallet, 'plt_id')
                ])
                ->whereNotIn('rfid', $trueRFIDs)
                ->delete();
        }
    }

    private function _executeOverreadCarton($whsId, $vtlCtnOverread)
    {
        /*
        Everytime a pallet goes through the carton count should update to correct count and carton from other pallet should be deleted:
        + New pallet:  Set CCN for it, pallet.ccn = “Overread”
        + Old pallet:  if pallet on rack, => insert cycle count notification
        ++ otherwise, insert pallet.ccn = "Reduced by Overread"
         */

        $pltRfids = array_unique(array_pluck($vtlCtnOverread, 'plt_rfid'));
        $ctnRfids = array_pluck($vtlCtnOverread, 'ctn_rfid');

        $grHdrIdsByPallet = [];
        foreach ($vtlCtnOverread as $vtlCtn) {
            $grDtlObj = $this->goodsReceiptDetailModel->getFirstWhere(['asn_dtl_id' => $vtlCtn['asn_dtl_id']]);
            if ($grDtlObj) {
                $grHdrIdsByPallet[$vtlCtn['plt_rfid']][] = $grDtlObj->gr_hdr_id;
            }
        }

        // WAP-583 - Update Scan Pallet about Inventory and Status
        // // reduce Inventory summary when carton of other pallet
        // foreach ($ctnRfids as $ctnRfid) {
        //     $ctnObj = $this->cartonModel->getFirstWhere(['rfid' => $ctnRfid]);
        //     $grDt = [
        //         'item_id' => $ctnObj->item_id,
        //         'lot'     => $ctnObj->lot,
        //     ];
        //     $availQty = $ctnObj->is_damaged == 1 ? 0 : $ctnObj->piece_ttl;
        //     $dmgQty   = $ctnObj->is_damaged == 1 ? $ctnObj->piece_ttl : 0;

        //     $this->goodsReceiptModel->reduceInventoryWhenRescanPallet($ctnObj->whs_id, $ctnObj->cus_id, $grDt,
        //         $availQty, $dmgQty);
        // }

        $dataForUpdatePallet = [];
        // update other pallets
        foreach ($pltRfids as $pltRfid) {
            $pltObj = $this->palletModel->getFirstWhere([
                'rfid' => $pltRfid,
            ]);

            if (object_get($pltObj, 'ccn')) {
                if (strpos($pltObj->ccn, self::REDUCED_BY_OVERREAD) === false)
                {
                    $pltObj->ccn .= " and ".self::REDUCED_BY_OVERREAD;
                }
            } else {
                $pltObj->ccn = self::REDUCED_BY_OVERREAD;
            }

            if (object_get($pltObj, 'loc_id')) {
                $this->_insertCycleNotify($pltObj, $whsId);
                $pltObj->ccn = null;
            }

            // delete cartons belong other pallet
            $ctnRfidsOverread = [];
            foreach ($vtlCtnOverread as $vtlCtn) {
                if ($vtlCtn['plt_rfid'] == $pltRfid) {
                    $ctnRfidsOverread[] = $vtlCtn['ctn_rfid'];
                }
            }
            $this->cartonModel->getModel()
                ->whereIn('rfid', $ctnRfidsOverread)
                ->where('deleted', 0)
                ->update([
                    'plt_id'     => null,
                    'loc_id'     => null,
                    'loc_name'   => null,
                    'loc_code'   => null,
                    'deleted'    => 1,
                    'deleted_at' => time(),
                ]);

            // compute carton total on other pallet
            $countCartons = $this->cartonModel->getModel()
                                ->where('plt_id' , $pltObj->plt_id)
                                ->where('deleted', 0)
                                ->count();
            $pltObj->ctn_ttl      = $countCartons;
            $pltObj->init_ctn_ttl = $countCartons;
            if ($countCartons == 0) {
                $pltObj->deleted    = 1;
                $pltObj->deleted_at = time();
                $pltObj->loc_id     = null;
                $pltObj->loc_code   = null;
                $pltObj->loc_name   = null;
            }
            $pltObj->save();
            if($pltObj->deleted  == 1) {
                // Deleted old location sugg for pallet
                DB::table('pal_sug_loc')
                    ->where('plt_id', $pltObj->plt_id)
                    ->where('deleted', 0)
                    ->update(['deleted' => 1, 'deleted_at' => time()]);
            } else {
                // update carton total for pallet
                DB::table('pal_sug_loc')
                    ->where('plt_id', $pltObj->plt_id)
                    ->where('deleted', 0)
                    ->update(['ctn_ttl' => $countCartons]);
            }
            // Update inform ctn_ttl & dmg_ttl on Pallet
            if ($grHdrIds = array_get($grHdrIdsByPallet, $pltRfid)) {
                $dataForUpdatePallet[] = [
                    'plt_id'     => $pltObj->plt_id,
                    'gr_hdr_ids' => $grHdrIds,
                    'ccn'        => $pltObj->ccn,
                ];
            }
        }

        return $dataForUpdatePallet;
    }

    private function _insertCycleNotify($pltObj, $whsId)
    {
        $location   = $this->locationModel->getFirstWhere(['loc_id' => $pltObj->loc_id]);
        $cartons    = $this->cartonModel->getModel()
                        ->where('plt_id', $pltObj->plt_id)
                        ->groupBy('item_id')
                        ->groupBy('lot')
                        ->get();

        $listReason = (new Reason())->getModel()->lists('r_name', 'r_id');
        $userId     = Data::getCurrentUserId();

        $dataCCN = [];
        foreach ($cartons as $carton) {
            $dataCCN[] = [
                'whs_id'      => $whsId,
                'cus_id'      => $pltObj->cus_id,
                'loc_id'      => $location->loc_id,
                'loc_code'    => $location->loc_code,
                'cc_ntf_sts'  => 'NW',
                'cc_ntf_date' => time(),
                'created_at'  => time(),
                'created_by'  => $userId,
                'updated_at'  => time(),
                'updated_by'  => $userId,
                'deleted_at'  => 915148800,
                'deleted'     => 0,
                'reason'      => isset($listReason[$location->reason_id]) ? $listReason[$location->reason_id] : 'Inactive setting',
                'item_id'     => $carton['item_id'],
                'sku'         => $carton['sku'],
                'lot'         => $carton['lot'],
                'pack'        => $carton['ctn_pack_size'],
                'size'        => $carton['size'],
                'color'       => $carton['color'],
                'uom_code'    => $carton['uom_code'],
                'uom_name'    => $carton['uom_name'],
                'remain_qty'  => $carton['piece_remain'],
                'des'         => $pltObj->ccn,
            ];
        }

        DB::table('cc_notification')->insert($dataCCN);
    }

    private function _setCcnPallet($ccn, $isMixedSku, $isUnscanned,$ccNotify, $prefix, $msgOther)
    {
        if ($isMixedSku) {
            $ccn = 'Mixed SKUs' . $prefix . $msgOther;
        }
        if ($isUnscanned) {
            $ccn = 'Unscanned Cartons' . $prefix . $msgOther;
        }
        if ($ccNotify) {
            $ccn = 'Overread' . $prefix . $msgOther;
        }
        if ($isMixedSku && $isUnscanned) {

            $ccn = 'Mixed SKU and Unscanned Cartons' . $prefix . $msgOther;
        }
        if ($isMixedSku && $ccNotify) {

            $ccn = 'Mixed SKU and Overread' . $prefix . $msgOther;
        }
        if ($isUnscanned && $ccNotify) {

            $ccn = 'Unscanned Cartons and Overread' . $prefix . $msgOther;
        }
        if ($isMixedSku && $isUnscanned && $ccNotify) {

            $ccn = 'Mixed SKU and Unscanned Cartons and Overread' . $prefix . $msgOther;
        }

        return $ccn;
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        $this->setMessage($msg);
        Log::error($this->_request, $this->_whsId, [
            'evt_code'      => 'EUC',
            'owner'         => $this->_transaction,
            'transaction'   => $this->_transaction,
            'url_endpoint'  => $this->_url,
            'message'       => \GuzzleHttp\json_encode($data),
            'response_data' => $this->getResponseData()
        ]);
        $this->setData($data);
        throw new \Exception($this->getMessage());
    }

    private function _responseMessage($msg, $data = [], $status = false, $statusCode = -1)
    {
        $data = [
            'status'    => $status,
            'iat'       => 123432454,
            'messages'  => [[
                            'status_code'   => $statusCode,
                            'msg'           => $msg
                            ]],
            'data' => $data
        ];

        return new Response($data, 200, [], null);
    }
}
