<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Models;



use Seldat\Wms2\Models\WarehouseStatus;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class WarehouseStatusModel extends AbstractModel
{
    /**
     * @var int
     */
    protected $limit = 10;

    /**
     * @var WarehouseStatus
     */
    protected $model;

    /**
     * WarehouseStatusModel constructor.
     *
     * @param WarehouseStatus|null $model
     */
    public function __construct(WarehouseStatus $model = null)
    {
        $this->model = ($model) ?: new WarehouseStatus();
    }

    /**
     * Search WarehouseStatus
     *
     * @param array $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [])
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'whs_sts_desc') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                } elseif ($key === 'whs_sts_code') {
                    $query->where($key, $value);
                }
            }
        }
        $this->model->filterData($query);

        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($this->limit);

        return $models;
    }
}