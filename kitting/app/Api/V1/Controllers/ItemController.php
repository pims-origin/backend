<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ItemModel;

use App\Api\V1\Transformers\ItemTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;


class ItemController extends AbstractController
{
    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * InventorySummaryController constructor.
     *
     * @param ItemModel $itemModel
     */
    public function __construct(
        ItemModel $itemModel
    ) {

        $this->itemModel = $itemModel;
    }

    /**
     * @param Request $request
     * @param ItemTransformer $itemTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function searchItem(Request $request, ItemTransformer $itemTransformer)
    {
        $input = $request->getQueryParams();

        try {
            // get list asn
            $items = $this->itemModel->search($input, [], array_get($input, 'limit', 20));

            return $this->response->paginator($items, $itemTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}