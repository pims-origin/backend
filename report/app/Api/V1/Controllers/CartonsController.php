<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 23-June-2017
 * Time: 18:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\CartonsModel;
use App\Api\V1\Transformers\InventoryTransformer;
use App\Api\V1\Transformers\LocationTypeTransformer;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\DB;
use App\Api\V1\Transformers\CartonsTransformer;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\DocBlock\StandardTagFactoryTest;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\Cache;


class CartonsController extends AbstractController
{
    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var CartonsModel
     */
    protected $cartonsModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;


    /**
     * CartonsController constructor.
     *
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param CartonsModel $cartonsModel
     */
    public function __construct
    (
        GoodsReceiptModel $goodsReceiptModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        CartonsModel $cartonsModel
    ) {
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
        $this->cartonsModel = $cartonsModel;
    }


    /**
     * @param $whs_id
     * @param Request $request
     * @param CartonsTransformer $cartonsTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        $whs_id,
        Request $request,
        CartonsTransformer $cartonsTransformer
    ) {

        // get data from HTTP
        $input = $request->getQueryParams();

        //get total record
        if (!empty($input['getTotalRec']) && $input['getTotalRec'] == 1) {
            $cartonsInfos = $this->cartonsModel->search($whs_id, $input, [
                'goodReceiptDtl.goodsReceipt',
                'AsnDtl.AsnHdr.SystemMeasurement',
                'customer',
                'pallet',
                'location',
            ], null, true)->toArray();

            return response()->json(['status' => 'OK', 'data' => ['total' => $cartonsInfos[0]['total']]]);
        }

        if (!empty($input['export']) && $input['export'] == 1) {
            $result = $this->export($whs_id, $input);
            if(!empty($result)){
                return response()->json(['status' => 'OK', 'data' => ['md5' => $result]]);
            }
            die;
        }

        try {
            $cartonsInfos = $this->cartonsModel->search($whs_id, $input, [
                'goodReceiptDtl.goodsReceipt',
                'AsnDtl.AsnHdr.SystemMeasurement',
                'customer',
                'pallet',
                'location',
            ], array_get($input, 'limit'));

            return $this->response->paginator($cartonsInfos, $cartonsTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param $input
     *
     * @return $this|void
     * @throws \Exception
     */
    private function export(
        $whs_id,
        $input,
        $getTotal = false
    ) {
        //DB::setFetchMode(\PDO::FETCH_OBJ);
        $warehouse = DB::table('warehouse')->where([
            'whs_id'     => $whs_id,
            'deleted_at' => 915148800,
            'deleted'    => 0
        ])->first();

        //get current login user id
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        if (empty($warehouse)) {
            throw new \Exception(Message::get("BM017", "Warehouse"));
        }

        try {

            $cartonsInfos = $this->cartonsModel->search($whs_id, $input, [
                'goodReceiptDtl.goodsReceipt',
                'AsnDtl.AsnHdr.SystemMeasurement',
                'customer',
                'pallet',
                'location',
            ], null, true)->toArray();
                
            if(!$getTotal) {
                //get part number
                if (isset($input['part'])) {
                    $part = $input['part'];
                }

                //get total part
                if (isset($input['totalPart'])) {
                    $totalPart = $input['totalPart'];
                }
                //get reference number
                if (isset($input['ref'])) {
                    $ref = $input['ref'];
                }
                //if part == totalPart && part = 1
                if($totalPart == 1 ){
                    //implment download, no need save to cache
                    $storedValue = $this->saveFileCSV($cartonsInfos, $part, $totalPart, true);
                    $filePath = "Report_Cartons.csv";
                    $this->saveAllFile($storedValue, $filePath);

                } else {
                    //Process to download with checking md5 key
                    if($part == 1) {
                        $md5 = md5($userId.date('YmdHis'));
                        $storedValue = $this->saveFileCSV($cartonsInfos, $part, $totalPart);
                        Cache::put($md5.$part, $storedValue, 1000);
                        Cache::put($userId.'md5', $md5, 1000);
                        //return with md5
                        return $md5;
                    } else {
                        //get old md5 from Cache
                        $md5 = Cache::get($userId.'md5');
                        if($ref != $md5 ){
                            //not allowed
                            return $this->response->errorBadRequest(['status' => 403, 'msg' => 'Not Allowed when have wrong reference key.']);
                        } else {
                            if($part <= $totalPart ) {
                                $newStoredValue = $this->saveFileCSV($cartonsInfos, $part, $totalPart);
                                Cache::put($md5.$part, $newStoredValue, 1000);
                                return $md5;
                            }
                        }
                    }
                    if($part == ($totalPart+1))  {
                        //retrieve cache and download
                        $cartonsInfos = [];
                        for($i=1; $i<=$totalPart; $i++){
                            $cartonsInfos = array_merge($cartonsInfos, Cache::get($md5.$i));
                        }
                        $filePath = "Report_Cartons.csv";
                        $this->saveAllFile($cartonsInfos, $filePath);
                        //forget cache key
                        for($i=1; $i<=$totalPart; $i++){
                            Cache::forget($md5.$i);
                        }
                        Cache::forget($userId.'md5');

                    }
                }


            }


        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        $writer->openToBrowser($filePath);

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case "*":
                            $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                            break;
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                        case "getByValue()":
                            $value = Status::getByValue(array_get($item, $values[0], null), $values[2]);
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }


        $writer->addRows($dataSave);
        $writer->close();

        //Excel::create($filePath, function ($file) use ($dataSave) {
        //
        //    $file->sheet('Report', function ($sheet) use ($dataSave) {
        //        //$sheet->setAutoSize(true);
        //        //$sheet->setHeight(1, 30);
        //
        //        //$countData = count($dataSave);
        //        // Fill suggest Data
        //        $sheet->fromArray($dataSave, null, 'A1', true, false);
        //
        //        // Wrap Text
        //        //$sheet->getStyle("N1:M" . ($countData))->getAlignment()->setWrapText(true);
        //
        //        //$sheet->setBorder('A1:M' . ($countData), 'thin');
        //        //$sheet->cell("A1:M1", function ($cell) {
        //        //    $cell->setAlignment('center');
        //        //    $cell->setFontWeight();
        //        //    $cell->setBackground("#3399ff");
        //        //    $cell->setFontColor("#ffffff");
        //        //});
        //
        //        //$sheet->cell("A1:M" . ($countData), function ($cell) {
        //        //    $cell->setValignment('center');
        //        //});
        //
        //        //$columsAlign = [
        //        //    "A1:C" . $countData => "left",
        //        //    "D1:J" . $countData => "center",
        //        //    "L1:M" . $countData => "center",
        //        //    "K1:K" . $countData => "left",
        //        //    "N1:M" . $countData => "left"
        //        //];
        //
        //        //foreach ($columsAlign as $cols => $align) {
        //        //    $sheet->cell($cols, function ($cell) use ($align) {
        //        //        $cell->setAlignment($align);
        //        //    });
        //        //}
        //
        //    });
        //})->store('xlsx', storage_path());
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function customersCartonsDropDown($whs_id, Request $request)
    {
        //$input = $request->getQueryParams();
        try {
            //$cartonsCustomers = $this->cartonsModel->cartonCustomers($whs_id);
            //$cusIds = array_pluck($cartonsCustomers, 'cus_id');
            $cartonsCustomers = DB::table('customer')
                ->join('cus_in_user', 'cus_in_user.cus_id', '=', 'customer.cus_id')
                ->where('cus_in_user.whs_id', '=', $whs_id)
                ->select(['customer.cus_id', 'customer.cus_name', 'cus_in_user.whs_id'])
                ->distinct()
                ->get();

//            foreach ($cartonsCustomers as &$cartonsCustomer) {
//                foreach ($customers as $customer) {
//                    if ($cartonsCustomer['cus_id'] === $customer['cus_id']) {
//                        $cartonsCustomer = array_merge($cartonsCustomer, $customer);
//                    }
//                }
//            }
            sort($cartonsCustomers);
            return ['data' => $cartonsCustomers];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function cartonsStatusDropDown(Request $request)
    {
        try {
            $cartonsStatus = $this->cartonsModel->cartonsStatusDropDown();

            return ['data' => $cartonsStatus];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }


    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function skuCartonsAutoComplete($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $cartonsSkus = $this->cartonsModel->skuCartonsAutoComplete($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $cartonsSkus];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoCtnrNum($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $ctnrNumInfos = $this->cartonsModel->autoCtnrNum($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $ctnrNumInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoGrNum($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $GrInfos = $this->goodsReceiptModel->autoGrNum($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $GrInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoCtnNum($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $CtnInfos = $this->cartonsModel->autoCtnNum($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $CtnInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoLocCode($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $CtnInfos = $this->cartonsModel->autoLocCode($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $CtnInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoRFID($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $CtnInfos = $this->cartonsModel->autoRFID($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $CtnInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoPalletRFID($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $CtnInfos = $this->cartonsModel->autoPalletRFID($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $CtnInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     *
     */
    public function getLocType() {
        try {
            $locationType = $this->cartonsModel->getLocType();

            return $this->response->collection($locationType, new LocationTypeTransformer());

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function saveFileCSV($cartonsInfos, $part, $totalPart, $finalFile = false){

        for($i = 0; $i < count($cartonsInfos); $i++) {
            $cartonsInfos[$i]['length'] = round($cartonsInfos[$i]['length'], 2);
            $cartonsInfos[$i]['width'] = round($cartonsInfos[$i]['width'], 2);
            $cartonsInfos[$i]['height'] = round($cartonsInfos[$i]['height'], 2);
            $cartonsInfos[$i]['weight'] = round($cartonsInfos[$i]['weight'], 1);
        }

        $title = [
            'customer.cus_code'                      => 'Customer Code',
            'customer.cus_name'                      => 'Customer Name',
            'ctnr_num'                               => 'Carrier',
            'gr_hdr_num' => 'GR Num',
            //'AsnDtl.AsnHdr.SystemMeasurement.sys_mea_name' => 'Measurement System',
            'piece_remain'                           => 'Pieces Remain',
            'upc'                                    => 'UPC',
            'sku'                                    => 'SKU',
            'item_id'                                => 'Item Id',
            'size'                                   => 'Size',
            'color'                                  => 'Color',
            'po'                                     => 'PO',
            'lot'                                    => 'Lot',
            'length'                                 => 'Length',
            'width'                                  => 'Width',
            'height'                                 => 'Height',
            'weight'                                 => 'Weight',
            'ctn_pack_size'                          => 'Pack Size',
            'ctn_num'                                => 'Carton Num',
            'rfid'                                   => 'Carton RFID',
            'pallet.plt_num'                         => 'Pallet Num',
            'plt_rfid'                               => 'Pallet RFID',
            'loc_code'                               => 'Location',
            'ctn_sts|getByValue()|CTN_STATUS'        => 'Status',
            'created_at|format()|m/d/Y  H:i:s'       => 'Created Date',
        ];

        // $filePath = storage_path() . "/Report_Cartons_{$warehouse['whs_name']}.csv";
        $filePath = storage_path() . "/Report_Cartons.csv";
        $dataSave = array();

        if(($finalFile) && ($part ==1)) {
            $dataSave[] = array_values($title);
        }
        if((!$finalFile) && ($part ==1)) {
            $dataSave[] = array_values($title);
        }


        foreach ($cartonsInfos as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case "*":
                            $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                            break;
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                        case "getByValue()":
                            $value = Status::getByValue(array_get($item, $values[0], null), $values[2]);
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }
        return $dataSave;

    }

    
    private function saveAllFile( array $data, $filePath)
    {
        if (empty($data)) {
            return false;
        }
        $writer = WriterFactory::create(Type::CSV);
        $writer->openToBrowser($filePath);

        $writer->addRows($data);
        $writer->close();

    }
}
