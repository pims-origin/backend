- WAP-583: [Inbound - Pallet] Update Scan Pallet about Inventory and Status
    + Don't update inventory.
    + Pallet's status and Carton's status are RG (Receiving).
    + Inventory and Pallet's status will be changed when Complete Goods Receipt