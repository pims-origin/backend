<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\OrderServiceModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;

/**
 * Class OrderHdrController
 *
 * @package App\Api\V1\Controllers
 */
class OrderHdrController extends AbstractController
{
    protected $serviceOrder;
    /**
     * OrderHdrController constructor.
     */
    public function __construct(Request $request) {
        $this->serviceOrder = new OrderServiceModel($request);
    }

    /**
     * @param Request $request
     * @param OrderHdrTransformer $orderHdrTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function firstOrderAllocated(Request $request)
    {
        $input = $request->getUri()->getQuery();

        try {

             $order = $this->serviceOrder->getFirstOrderAllocated($input);

             return ['data' => $order ? array_first($order) : []];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}