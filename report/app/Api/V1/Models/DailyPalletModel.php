<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\DailyPalletSummary;

class DailyPalletModel extends AbstractModel
{
    public function __construct(DailyPalletSummary $model = null)
    {
        $this->model = ($model) ?: new DailyPalletSummary();
    }

    /*
     ****************************************************************************
     */
    //Daily Pallet On Rack for RM Kayu
    public function dailyPalletOnRackInfos(
        $whs_id,
        $cus_id,
        $curDate,
        $attributes = [],
        $with = [],
        $limit =
        PAGING_LIMIT
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_plt_rpt')
            ->select([
                'daily_plt_rpt.whs_id',
                'daily_plt_rpt.cus_id',
                'daily_plt_rpt.item_id',
                'daily_plt_rpt.description',
                'daily_plt_rpt.sku',
                'daily_plt_rpt.size',
                'daily_plt_rpt.color',
                'daily_plt_rpt.pack',
                'daily_plt_rpt.uom',
                'daily_plt_rpt.lot',
                'daily_plt_rpt.is_rack',
                'daily_plt_rpt.plt_id',
                'daily_plt_rpt.plt_num',
                'daily_plt_rpt.loc_id',
                'daily_plt_rpt.loc_code',

                'daily_plt_rpt.gr_dt',
                'daily_plt_rpt.exp_dt',
                'daily_plt_rpt.created_at',
                'daily_plt_rpt.updated_at',
                'daily_plt_rpt.inv_dt',

                DB::raw('SUM(daily_plt_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.init_qty) AS init_qty'),

                DB::raw('SUM(daily_plt_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.in_qty) AS in_qty'),

                DB::raw('SUM(daily_plt_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.out_qty) AS out_qty'),

                DB::raw('SUM(daily_plt_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.other_qty) AS other_qty'),

                DB::raw('SUM(daily_plt_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.cur_qty) AS cur_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_plt_rpt.inv_dt', $curDate);

        $query->where('daily_plt_rpt.whs_id', $whs_id);
        $query->where('daily_plt_rpt.cus_id', $cus_id);
        $query->where('daily_plt_rpt.is_rack', 1);

        $query->groupBy('daily_plt_rpt.item_id', 'daily_plt_rpt.plt_id');

        $query->orderBy('in_qty', 'DESC');
        $query->orderBy('out_qty', 'DESC');
        $query->orderBy('other_qty', 'DESC');

        $row = $query->get();

        return $row;
    }

    //Subtotal RM Kayu for Daily Pallet On Rack
    public function subTotalForDailyPalletOnRack(
        $whs_id,
        $cus_id,
        $curDate,
        $attributes = [],
        $with = [],
        $limit = PAGING_LIMIT
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_plt_rpt')
            ->select([
                'daily_plt_rpt.whs_id',
                'daily_plt_rpt.cus_id',
                'daily_plt_rpt.item_id',
                'daily_plt_rpt.description',
                'daily_plt_rpt.sku',
                'daily_plt_rpt.size',
                'daily_plt_rpt.color',
                'daily_plt_rpt.pack',
                'daily_plt_rpt.uom',
                'daily_plt_rpt.lot',
                'daily_plt_rpt.is_rack',
                'daily_plt_rpt.plt_id',
                'daily_plt_rpt.plt_num',

                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.gr_dt), '%Y-%m-%d') AS gr_dt"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.exp_dt), '%Y-%m-%d') AS exp_dt"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.created_at), '%Y-%m-%d') AS created_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.updated_at), '%Y-%m-%d') AS updated_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d') AS inv_dt"),

                DB::raw('SUM(daily_plt_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.init_qty) AS init_qty'),

                DB::raw('SUM(daily_plt_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.in_qty) AS in_qty'),

                DB::raw('SUM(daily_plt_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.out_qty) AS out_qty'),

                DB::raw('SUM(daily_plt_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.other_qty) AS other_qty'),

                DB::raw('SUM(daily_plt_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.cur_qty) AS cur_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_plt_rpt.inv_dt', $curDate);

        $query->where('daily_plt_rpt.whs_id', $whs_id);
        $query->where('daily_plt_rpt.cus_id', $cus_id);
        $query->where('daily_plt_rpt.is_rack', 1);

        $query->groupBy('daily_plt_rpt.cus_id');

        $row = $query->first();

        return $row;
    }

    //Daily Pallet Block Stack
    public function dailyPalletBlockStackInfos(
        $whs_id,
        $cus_id,
        $curDate,
        $attributes = [],
        $with = [],
        $limit = PAGING_LIMIT
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_plt_rpt')
            ->select([
                'daily_plt_rpt.whs_id',
                'daily_plt_rpt.cus_id',
                'daily_plt_rpt.item_id',
                'daily_plt_rpt.description',
                'daily_plt_rpt.sku',
                'daily_plt_rpt.size',
                'daily_plt_rpt.color',
                'daily_plt_rpt.pack',
                'daily_plt_rpt.uom',
                'daily_plt_rpt.lot',
                'daily_plt_rpt.is_rack',
                'daily_plt_rpt.plt_id',
                'daily_plt_rpt.plt_num',
                'daily_plt_rpt.loc_id',
                'daily_plt_rpt.loc_code',

                'daily_plt_rpt.gr_dt',
                'daily_plt_rpt.exp_dt',
                'daily_plt_rpt.created_at',
                'daily_plt_rpt.updated_at',
                'daily_plt_rpt.inv_dt',

                DB::raw('SUM(daily_plt_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.init_qty) AS init_qty'),

                DB::raw('SUM(daily_plt_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.in_qty) AS in_qty'),

                DB::raw('SUM(daily_plt_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.out_qty) AS out_qty'),

                DB::raw('SUM(daily_plt_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.other_qty) AS other_qty'),

                DB::raw('SUM(daily_plt_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.cur_qty) AS cur_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_plt_rpt.inv_dt', $curDate);

        $query->where('daily_plt_rpt.whs_id', $whs_id);
        $query->where('daily_plt_rpt.cus_id', $cus_id);
        $query->where('daily_plt_rpt.is_rack', 0);

        $query->groupBy('daily_plt_rpt.item_id', 'daily_plt_rpt.plt_id');

        $query->orderBy('in_qty', 'DESC');
        $query->orderBy('out_qty', 'DESC');
        $query->orderBy('other_qty', 'DESC');

        $row = $query->get();

        return $row;
    }

    //Subtotal Block Stack for Daily Pallet
    public function subtotalBlockStackForDailyPallet(
        $whs_id,
        $cus_id,
        $curDate,
        $attributes = [],
        $with = [],
        $limit =
        PAGING_LIMIT
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_plt_rpt')
            ->select([
                'daily_plt_rpt.whs_id',
                'daily_plt_rpt.cus_id',
                'daily_plt_rpt.item_id',
                'daily_plt_rpt.description',
                'daily_plt_rpt.sku',
                'daily_plt_rpt.size',
                'daily_plt_rpt.color',
                'daily_plt_rpt.pack',
                'daily_plt_rpt.uom',
                'daily_plt_rpt.lot',
                'daily_plt_rpt.is_rack',
                'daily_plt_rpt.plt_id',
                'daily_plt_rpt.plt_num',

                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.gr_dt), '%Y-%m-%d') AS gr_dt"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.exp_dt), '%Y-%m-%d') AS exp_dt"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.created_at), '%Y-%m-%d') AS created_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.updated_at), '%Y-%m-%d') AS updated_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d') AS inv_dt"),

                DB::raw('SUM(daily_plt_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.init_qty) AS init_qty'),

                DB::raw('SUM(daily_plt_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.in_qty) AS in_qty'),

                DB::raw('SUM(daily_plt_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.out_qty) AS out_qty'),

                DB::raw('SUM(daily_plt_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.other_qty) AS other_qty'),

                DB::raw('SUM(daily_plt_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.cur_qty) AS cur_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_plt_rpt.inv_dt', $curDate);

        $query->where('daily_plt_rpt.whs_id', $whs_id);
        $query->where('daily_plt_rpt.cus_id', $cus_id);
        $query->where('daily_plt_rpt.is_rack', 0);

        $query->groupBy('daily_plt_rpt.cus_id');

        $row = $query->first();

        return $row;
    }

    //Total Daily Pallet
    public function totalForDailyPallet(
        $whs_id,
        $cus_id,
        $curDate,
        $attributes = [],
        $with = [],
        $limit =
        PAGING_LIMIT
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_plt_rpt')
            ->select([
                'daily_plt_rpt.whs_id',
                'daily_plt_rpt.cus_id',
                'daily_plt_rpt.item_id',
                'daily_plt_rpt.description',
                'daily_plt_rpt.sku',
                'daily_plt_rpt.size',
                'daily_plt_rpt.color',
                'daily_plt_rpt.pack',
                'daily_plt_rpt.uom',
                'daily_plt_rpt.lot',
                'daily_plt_rpt.is_rack',
                'daily_plt_rpt.plt_id',
                'daily_plt_rpt.plt_num',

                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.gr_dt), '%Y-%m-%d') AS gr_dt"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.exp_dt), '%Y-%m-%d') AS exp_dt"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.created_at), '%Y-%m-%d') AS created_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.updated_at), '%Y-%m-%d') AS updated_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d') AS inv_dt"),

                DB::raw('SUM(daily_plt_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.init_qty) AS init_qty'),

                DB::raw('SUM(daily_plt_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.in_qty) AS in_qty'),

                DB::raw('SUM(daily_plt_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.out_qty) AS out_qty'),

                DB::raw('SUM(daily_plt_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.other_qty) AS other_qty'),

                DB::raw('SUM(daily_plt_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.cur_qty) AS cur_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now() - INTERVAL 3 DAY, '%Y-%m-%d')"));
        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_plt_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_plt_rpt.inv_dt', $curDate);

        $query->where('daily_plt_rpt.whs_id', $whs_id);
        $query->where('daily_plt_rpt.cus_id', $cus_id);

        $query->groupBy('daily_plt_rpt.cus_id');

        $row = $query->first();

        return $row;
    }

    /**
     * [getDailyPalletReport description]
     * @param  [type] $whs_id     [description]
     * @param  [type] $cus_id     [description]
     * @param  array  $curDate    [description]
     * @param  array  $attributes [description]
     * @param  array  $with       [description]
     * @param  [type] $limit      [description]
     * @return [type]             [description]
     */
    public function getDailyPalletReport(
        $whs_id,
        $cus_id,
        $curDate = [],
        $attributes = [],
        $with = [],
        $limit =
        PAGING_LIMIT,
        array $isRack
    ) {
        return $this->model
            ->select([
                'daily_plt_rpt.whs_id',
                'daily_plt_rpt.cus_id',
                'daily_plt_rpt.item_id',
                'daily_plt_rpt.description',
                'daily_plt_rpt.sku',
                'daily_plt_rpt.size',
                'daily_plt_rpt.color',
                'daily_plt_rpt.pack',
                'daily_plt_rpt.uom',
                'daily_plt_rpt.lot',
                'daily_plt_rpt.is_rack',
                'daily_plt_rpt.plt_id',
                'daily_plt_rpt.plt_num',
                'daily_plt_rpt.loc_id',
                'daily_plt_rpt.loc_code',
                'daily_plt_rpt.gr_dt',
                'daily_plt_rpt.exp_dt',
                'daily_plt_rpt.created_at',
                'daily_plt_rpt.updated_at',
                'daily_plt_rpt.inv_dt',
                DB::raw('SUM(daily_plt_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.init_qty) AS init_qty'),
                DB::raw('SUM(daily_plt_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.in_qty) AS in_qty'),
                DB::raw('SUM(daily_plt_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.out_qty) AS out_qty'),
                DB::raw('SUM(daily_plt_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.other_qty) AS other_qty'),
                DB::raw('SUM(daily_plt_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_plt_rpt.cur_qty) AS cur_qty'),

                'i.length',
                'i.width',
                'i.height',
                'i.weight',

            ])
            ->join('item as i', 'daily_plt_rpt.item_id', '=', 'i.item_id')
            ->whereBetween('daily_plt_rpt.inv_dt', $curDate)
            ->where('daily_plt_rpt.whs_id', $whs_id)
            ->where('daily_plt_rpt.cus_id', $cus_id)
            /*->when(count($isRack), function($q)use($isRack){
                return $q->whereIn('daily_plt_rpt.is_rack', $isRack);
            })*/
            ->groupBy('daily_plt_rpt.item_id', 'daily_plt_rpt.plt_id', 'daily_plt_rpt.is_rack')
            ->orderBy('in_qty', 'DESC')
            ->orderBy('out_qty', 'DESC')
            ->orderBy('other_qty', 'DESC')
            ->latest('updated_at')
            ->get();
    }
}
