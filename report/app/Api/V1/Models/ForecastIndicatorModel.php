<?php

namespace App\Api\V1\Models;


use Seldat\Wms2\Models\ForecastIndicator;

class ForecastIndicatorModel extends AbstractModel
{
    public function __construct(ForecastIndicator $model = null)
    {
        $this->model = ($model) ?: new ForecastIndicator();
    }
}