<?php

namespace App\Api\V5\Inbound\Pallet\ScanPallet\Clients;

use GuzzleHttp\Client;
use Psr\Http\Message\ServerRequestInterface as Request;

abstract class AbstractClient
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * BaseService constructor.
     * @param $request
     * @param string $baseUrl
     */
    public function __construct(Request $request, $baseUrl = '')
    {
        $this->client = new Client([
            'base_uri' => $baseUrl
        ]);
    }

    /**
     * @param $json
     * @param string $type
     * @return array
     */
    public function responseToArray($json, $type = 'data')
    {
        return \json_decode($json, true)[$type];
    }
}
