<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class OrderDtlModel extends AbstractModel
{

    protected $model;
    const LOT_ANY = 'ANY';

    /**
     * OrderDtlModel constructor.
     *
     * @param OrderDtl|null $model
     */
    public function __construct(OrderDtl $model = null)
    {
        $this->model = ($model) ?: new OrderDtl();
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getListOrderDtlInfo($odr_ids)
    {
        return $this->model
            ->whereIn('odr_id', $odr_ids)
            ->where('alloc_qty', '>', 0)
            ->get();
    }

    /**
     * @param $itm_id
     * @param $wv_id
     *
     * @deprecated DONT USE IT
     * @return mixed
     */
    public function getOrderDtlOfWavePick($itm_id, $wv_id)
    {
        return $this->model
            ->where('item_id', $itm_id)
            ->where('wv_id', $wv_id)
            ->first();
    }

    /**
     * @param $wv_id
     *
     * @deprecated DONT USE IT
     * @return mixed
     */
    public function getListOrderDtlOfWavePick($wv_id)
    {
        return $this->model
            ->leftJoin('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->leftJoin('wv_dtl', 'wv_dtl.wv_id', '=', 'odr_hdr.wv_id')
            ->where('odr_dtl.wv_id', $wv_id)
            ->groupBy('odr_dtl.odr_dtl_id')
            ->get();
    }

    public function getOrderDtlsByWaveDtl($wv_id, $itemId, $lot)
    {
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);

        return $this->model
            ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->where('odr_dtl.wv_id', $wv_id)
            ->where('item_id', $itemId)
            ->whereIn('lot', [$lot, self::LOT_ANY])
            ->where('itm_sts', '<>', 'CC')
            ->where('odr_hdr.odr_sts', '<>', 'CC')
            ->select(['odr_hdr.odr_num', 'odr_dtl.*'])
            ->get();
    }

    public function updateOdrDtlPickedQty($item, $cartons)
    {
        $data = new Data();
        $userInfo = $data->getUserInfo();

        $odrDtls = $this->getOrderDtlsByWaveDtl($item['wv_id'], $item['item_id'], $item['lot']);
        $pickedTtl = $item['picked_ttl'];
        $orderCartons = [];
        $odrIds = [];
        $discrepancyTtl = 0;

        $odrSortDtls = [];

        $packs = [];
        $eventData = [];

        foreach ($odrDtls as $odrDtl) {
            /**
             * 1. Check pickedTtl >= piece_qty
             * 1.1. set all_qty = piece_qty
             * 1.2 pickedTtl = pickedTtl - piece_qty
             * 1.3 update odr dtl, all_qty = piece_qty and status = PD, back_odr = false, back_odr_qty = 0
             * 1.4 $odrDtl['backorder'] = false;
             * 2. Check pickedTtl < piece_qty
             * 2.1 update odr dtl, all_qty = pickedTtl and status = PD, back_odr_qty = piece_qty - pickedTtl,
             * back_odr = true
             * 2.2 pickedTtl = 0
             */
            $odrId = $odrDtl->odr_id;
            $odrIds[] = $odrId;
            $allQty  = 0;
            $cus_back_order = (new CustomerConfigModel())->getCusBackOrder($odrDtl->whs_id, $odrDtl->cus_id);

            if (!array_key_exists($odrId, $eventData)) {
                $eventData[$odrId] = [
                    'whs_id' => $odrDtl->whs_id,
                    'cus_id' => $odrDtl->cus_id,
                    'evt_code' => Status::getByKey("EVENT", "ORDER-PICKED"),
                    'info' => sprintf(Status::getByKey("EVENT-INFO", "ORDER-PICKED"), $odrDtl->odr_num),
                    'owner' => $odrDtl->odr_num,
                    'trans_num' => $odrDtl->odr_num,
                    'created_at' => time(),
                    'created_by' => JWTUtil::getPayloadValue('jti') ?: 1
                ];
            }


            $orderCarton = [
                'odr_hdr_id' => $odrDtl->odr_id,
                'odr_dtl_id' => $odrDtl->odr_dtl_id,
                'wv_hdr_id'  => $item['wv_id'],
                'wv_dtl_id'  => $item['wv_dtl_id'],
                'odr_num'    => $odrDtl->odr_num,
                'wv_num'     => $item['wv_num'],
                'ctnr_rfid'  => null,
                'ctn_num'    => null,
                'piece_qty'  => 0,
                'ctn_id'     => null,
                'is_storage' => 0,
                'ctn_rfid'   => null,
                'ctn_sts'    => 'PD',
                'sts'        => 'I',
                'created_at' => time(),
                'updated_at' => time(),
                'deleted'    => 0,
                'deleted_at' => '915148800',
                'created_by' => $userInfo['user_id'],
                'updated_by' => $userInfo['user_id'],
            ];

            $remaining = $odrDtl->alloc_qty;

            foreach ($cartons as $key => $carton) {
                $remaining -= $carton['picked_qty'];
                $allQty += $carton['picked_qty'];
                if ($carton['pick_piece']) {
                    $orderCarton['is_storage'] = 1;
                }

                $orderCarton['loc_id']    = $carton['loc_id'];
                $orderCarton['loc_code']  = $carton['loc_code'];
                $orderCarton['piece_qty'] = $carton['picked_qty'];
                $orderCarton['ctn_id']    = $carton['ctn_id'];
                $orderCarton['ctn_num']   = $carton['ctn_num'];
                $orderCarton['ctn_rfid']  = $carton['rfid'];
                $orderCarton['item_id']   = $carton['item_id'];
                $orderCarton['sku']       = $carton['sku'];
                $orderCarton['size']      = $carton['size'];
                $orderCarton['color']     = $carton['color'];
                $orderCarton['lot']       = $carton['lot'];
                $orderCarton['upc']       = $carton['upc'];
                $orderCarton['pack']      = $carton['ctn_pack_size'];
                $orderCarton['uom_id']    = $carton['ctn_uom_id'];
                $orderCarton['uom_code']  = $carton['uom_code'];
                $orderCarton['uom_name']  = $carton['uom_name'];
                $orderCarton['whs_id']    = $carton['whs_id'];
                $orderCarton['cus_id']    = $carton['cus_id'];
                $orderCarton['length']    = $carton['length'];
                $orderCarton['width']     = $carton['width'];
                $orderCarton['height']    = $carton['height'];
                $orderCarton['weight']    = $carton['weight'];
                $orderCarton['volume']    = $carton['volume'];
                $orderCarton['plt_id']    = $carton['plt_id'];
                $orderCartons[] = $orderCarton;
                $packs[$odrDtl->odr_id][$carton['ctn_id']] = [
                    'orderCarton' => $orderCarton,
                    'carton'      => $carton,
                ];

                $eventData[$odrId]['whs_id'] = $carton['whs_id'];
                $eventData[$odrId]['cus_id'] = $carton['cus_id'];

                unset($cartons[$key]);
                if ($remaining <= 0) {
                    break;
                }
            }

            if ($pickedTtl >= $odrDtl->alloc_qty) {
                $odrDtl->back_odr = false;
                $odrDtl->back_odr_qty = 0;
                $pickedTtl -= $odrDtl->piece_qty;
            } else {
                if ($cus_back_order != 'full') {
                    $allQty = $remaining;
                }
                $discrepancyQty = $odrDtl->alloc_qty - $pickedTtl;
                $discrepancyTtl += $discrepancyQty;
                $odrDtl->alloc_qty = $allQty;
                $odrDtl->back_odr = true;
                //$odrDtl->back_odr_qty = abs($pickedTtl);
                $odrDtl->back_odr_qty  =  $odrDtl->piece_qty - $allQty;
                $pickedTtl = 0;
            }

            $odrDtl->picked_qty = $odrDtl->alloc_qty;
            $odrDtl->itm_sts = 'PD';
            $odrDtl->save();

            $odrSortDtls[$odrDtl->odr_dtl_id] = $odrDtl;


        }

        $orderCartonsData = serialize($orderCartons);
        DB::table('wv_queue')->insert([
            'wv_id' => $item['wv_id'],
            'type' => 'UP',
            'data' => $orderCartonsData,
            'queue_sts' => 'NW'
        ]);

        return [
            'odrDtls'        => $odrSortDtls,
            'packs'          => $packs,
            'odrIds'         => $odrIds,
            'discrepancyTtl' => $discrepancyTtl,
            'eventData'      => $eventData,
        ];
    }

    public function sortOrderDtlByOrderID($odrDtlList)
    {
        $orders = [];
        foreach ($odrDtlList as $item) {
            foreach ($item as $id => $odrDtl) {
                $orders[$odrDtl->odr_id][$id] = $odrDtl;
            }
        }

        return $orders;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateOrtDtl($data)
    {
        return $this->model->where('odr_id', '=', $data['odr_id'])
            ->update([
                'wv_id' => $data['wv_id'],
            ]);
    }
}
