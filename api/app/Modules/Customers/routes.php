<?php

$middleware = ['app','user', 'cus'];
$router->group(['prefix' => '/rest/wms360/seldat/v1', 'middleware' => $middleware, 'namespace' => 'App\Modules\Customers\Controllers'], function () use ($router) {

    // Show list customer assigned to user of selecting warehouse
    $router->get('/warehouses/{whsId:[a-zA-Z0-9]+}/customers', [
        'uses'      => 'CustomerController@index'
    ]);

    // Show detail of customer
    $router->get('/customers/{cusId:[a-zA-Z0-9]+}', [
        'uses'      => 'CustomerController@read'
    ]);

    // Create customer
    $router->post('/customers', [
        'uses'      => 'CustomerController@store'
    ]);
});