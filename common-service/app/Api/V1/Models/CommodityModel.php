<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Commodity;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class CommodityModel extends AbstractModel
{
    public function __construct(Commodity $model = null)
    {
        $this->model = ($model) ?: new Commodity();
    }

    /**
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function search($attributes, array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['cmd_name'])) {
            $query->where('cmd_name', 'like', "%" . SelStr::escapeLike($attributes['cmd_name']) . "%");
        }
        if (isset($attributes['commodity_nmfc'])) {
            $query->where('commodity_nmfc', 'like', "%" . SelStr::escapeLike($attributes['commodity_nmfc']) . "%");
        }
        if (isset($attributes['cmd_cls'])) {
            $query->where('cmd_cls', 'like', "%" . SelStr::escapeLike($attributes['cmd_cls']) . "%");
        }

        //$this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }


}
