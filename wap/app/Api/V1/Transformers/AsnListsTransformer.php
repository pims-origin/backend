<?php

namespace App\Api\V1\Transformers;


use App\Api\V1\Models\AsnDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;

class AsnListsTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform($data)
    {
        $expDate = object_get($data, 'asnHdr.asn_hdr_ept_dt', '');
        $expDate = ($expDate) ? date('m/d/Y', $expDate) : '';

        $user = '';
        if (object_get($data, 'asnHdr.createdUser')) {
            $user = object_get($data, 'asnHdr.createdUser.first_name', '')
                . ' ' .
                object_get($data, 'asnHdr.createdUser.last_name', '');
        }

        $asnDetail = new AsnDtlModel();
        $xDock = AsnDtl::where('asn_hdr_id', $data->asn_hdr_id)
            ->where('ctnr_id', $data->ctnr_id)->sum('asn_dtl_crs_doc');

        return [
            'asn_hdr_id'      => $data->asnHdr->asn_hdr_id,
            'asn_hdr_num'     => $data->asnHdr->asn_hdr_num,
            'asn_hdr_ref'     => $data->asnHdr->asn_hdr_ref,
            "asn_date"        => $data->asnHdr->created_at->format('m/d/Y'),
            'cus_name'        => $data->asnHdr->customer->cus_name,
            'ctnr_id'         => object_get($data, 'container.ctnr_id', ''),
            'ctnr_num'        => object_get($data, 'container.ctnr_num', ''),
            "dtl_crs_doc"     => $xDock,
            'asn_hdr_ept_dt'  => $expDate,
            'dtl_po'          => $data->asn_dtl_po,
            'dtl_po_date'     => ($data->asn_dtl_po_dt) ? date("m/d/Y", $data->asn_dtl_po_dt) : "",
            'asn_dtl_pack'    => object_get($data, 'asn_dtl_pack', 0),
            'asn_dtl_cus_upc' => object_get($data, 'asn_dtl_cus_upc', ''),
            'asn_dtl_lot'     => object_get($data, 'asn_dtl_lot', ''),
            'asn_sts'         => $data->asnHdr->asn_sts,
            'asn_sts_name'    => object_get($data, 'asnHdr.asnStatus.asn_sts_name', ''),
            'asn_sts_des'     => object_get($data, 'asnHdr.asnStatus.asn_sts_des', ''),
            'user'            => $user,
            'containers'      => $asnDetail->getContainersByAsn($data->asnHdr->asn_hdr_id)

        ];


    }

}
