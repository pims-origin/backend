<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\ReceivingReportModel;
use App\Api\V1\Transformers\ReceivingReportTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class ReceivingReportController extends AbstractController
{
    
    /*
    ****************************************************************************
    */ 
    
    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);
        
        parent::__construct($request, $object);
        
        $this->model = new ReceivingReportModel();
        $this->transformer = new ReceivingReportTransformer();
    }
    
    /*
    ****************************************************************************
    */
}
