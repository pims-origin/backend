<?php

namespace App\Api\V1\Validators;


class CommodityValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cmd_name'       => 'required',
            'commodity_nmfc' => 'required',
            'cmd_cls'        => 'required',
        ];
    }
}
