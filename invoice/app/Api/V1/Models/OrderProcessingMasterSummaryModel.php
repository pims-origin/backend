<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use \DateTime;
use App\Invoice;

use App\Api\V1\Models\SummaryModel;
use App\Api\V1\Models\CommonInvoiceModel;
use App\Api\V1\Models\VolumeRatesModel;

use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\InvoicingSummary;
use Seldat\Wms2\Utils\Status;


class OrderProcessingMasterSummaryModel extends AbstractModel
{
    /**
    * @var
    */
    protected $ord;
    protected $common;
    protected $inv;
    protected $invSum;
    protected $vol;
    protected $ivcSumModel;

    public $volRates = [];

    public $commonUoms;
    public $workUoms;
    public $carrierUoms;
    public $labelUoms;
    public $cancelUoms;
    public $eventOrder;


    /**
     * Constructor
    */

    public function __construct()
    {
        $this->model = new OrderHdr();
        $this->inv = new InvoicingSummary();
        $this->ivcSumModel = new InvoicingSummaryModel();

        $this->invSum = new SummaryModel();
        $this->vol = new VolumeRatesModel();
        $this->common = new CommonInvoiceModel();
    }

    /**
     * Populate Order Processing Charge codes for each customer when order shipped / cancelled
    */

    public function orderProcessingChargeCodes()
    {
        $type = Invoice::OP;

        $volRangeUOMs = $this->ivcSumModel->getVolRangeUOMs();
        
        $commonUOMs = $this->getCommonUOMs();

        $this->commonUoms = array_merge($commonUOMs, $volRangeUOMs);

        $this->workUoms = [
            Invoice::$unit
        ];

        $this->carrierUoms = [
            Invoice::$ups,
            Invoice::$fedex
        ];

        $this->labelUoms = [
            Invoice::$label
        ];

        $this->cancelUoms = [
            Invoice::$orderCancel
        ];

        $this->eventOrder = [
            config('constants.event.ORD-SHIPPED'),
            config('constants.event.PR-SHIPPED')
        ];

        $carrierData = $labelData = $workData = $cancelData = [];

        //get all order processing charge codes
        $chgCodes = $this->invSum->getChargeCodes($type);

        //get max date from invoice_summary for order processing charge codes
        $maxRcv = $this->ivcSumModel->getMaxDate($type);

        $lastDate = $maxRcv->maxDate;

        $prevDate = date('Y-m-d', strtotime('-1 days'));

        if ($lastDate && $lastDate == $prevDate) {
            return [];
        }

        //get the order numbers for order shipped
        $orderNums = $this->getEventsOrder($lastDate, config('constants.event.ORD-SHIPPED'));

        //get the order numbers for order cancelled
        $cancelOrderNums = $this->getEventsOrder($lastDate, config('constants.event.ORD-CANCELLED'));


        if (! $orderNums && ! $cancelOrderNums) {
            return;
        }

        //order shipped
        if ($orderNums) {
            //1 - cartons,pieces,plates,volume,order and monthly charge
            $commonResult =  $this->commonQuery($chgCodes, $orderNums);

            if ($commonResult) {
                $ordResult = $commonResult['ordResult'];

                $ordCharge = $commonResult['chgResults'];

                $orderData = $this->getData($ordResult, $ordCharge, $orderNums);
            }


            //2 - work order - unit
            $workResult =  $this->workOrderQuery($chgCodes, $orderNums);

            if ($workResult) {
                $wrkResult = $workResult['wrkResult'];
                $wrkCharge = $workResult['chgResults'];

                $workData = $this->getWorkData($wrkResult,$wrkCharge);
            }


            //3 - carrier - fedex/ups
            $carrierResult =  $this->carrierQuery($chgCodes, $orderNums);

            if ($carrierResult) {
                $cariResult = $carrierResult['cariResult'];
                $cariCharge = $carrierResult['chgResults'];

                $carrierData = $this->getCarrierData($cariResult, $cariCharge);
            }


            //4 - label - online orders
            $labelResult =  $this->labelQuery($chgCodes, $orderNums);

            if ($labelResult) {
                $lblResult = $labelResult['lblResult'];
                $lblCharge = $labelResult['chgResults'];

                $labelData = $this->getData($lblResult, $lblCharge);
            }
        }


        //order cancelled
        if ($cancelOrderNums) {
            $cancelResult =  $this->cancelQuery($chgCodes, $cancelOrderNums);

            if ($cancelResult) {
                $cnclResult = $cancelResult['cnclResult'];
                $cnclCharge = $cancelResult['chgResults'];

                $cancelData = $this->getData($cnclResult, $cnclCharge);
            }
        }

        //insert into invoice_summary table
        DB::beginTransaction();

        //1 - cartons,pieces,plates,volume,order and monthly charge
        if (! empty($orderData)) {
            foreach($orderData as $row) {
                //check integrity vialotion
                if (! $this->invSum->getFirstWhere([
                    'cus_id'      => $row['cus_id'],
                    'whs_id'      => $row['whs_id'],
                    'chg_type_id' => $row['chg_type_id'],
                    'chg_code_id' => $row['chg_code_id'],
                    'dt' => $row['dt']
                ])){
                    $this->inv->create($row);
                }
            }
        }

        //2 - work order - unit
        if (! empty($workData)) {
            foreach($workData as $row) {
                //check integrity vialotion
                if (! $this->invSum->getFirstWhere([
                    'cus_id'      => $row['cus_id'],
                    'whs_id'      => $row['whs_id'],
                    'chg_type_id' => $row['chg_type_id'],
                    'chg_code_id' => $row['chg_code_id'],
                    'dt' => $row['dt']
                ])){
                    $this->inv->create($row);
                }
            }
        }

        //3 - carrier - fedex/ups
        if (! empty($carrierData)) {
            foreach($carrierData as $row) {
                //check integrity vialotion
                if (! $this->invSum->getFirstWhere([
                    'cus_id'      => $row['cus_id'],
                    'whs_id'      => $row['whs_id'],
                    'chg_type_id' => $row['chg_type_id'],
                    'chg_code_id' => $row['chg_code_id'],
                    'dt' => $row['dt']
                ])){
                    $this->inv->create($row);
                }
            }
        }

        //4 - label - online orders
        if (! empty($labelData)) {
            foreach($labelData as $row) {
                //check integrity vialotion
                if (! $this->invSum->getFirstWhere([
                    'cus_id'      => $row['cus_id'],
                    'whs_id'      => $row['whs_id'],
                    'chg_type_id' => $row['chg_type_id'],
                    'chg_code_id' => $row['chg_code_id'],
                    'dt' => $row['dt']
                ])){
                    $this->inv->create($row);
                }
            }
        }

        //order cancel
        if (! empty($cancelData)) {
            foreach($cancelData as $row) {
                //check integrity vialotion
                if (! $this->invSum->getFirstWhere([
                    'cus_id'      => $row['cus_id'],
                    'whs_id'      => $row['whs_id'],
                    'chg_type_id' => $row['chg_type_id'],
                    'chg_code_id' => $row['chg_code_id'],
                    'dt' => $row['dt']
                ])){
                    $this->inv->create($row);
                }
            }
        }

var_dump('order processing charge codes populated');

        DB::commit();
    }

    /**
     * @lastDate date
     * @eventCode string
     * return array
    */

    public function getEventsOrder($lastDate, $eventCode)
    {
        $curDate = date('Y-m-d');

        $logQuery = DB::table('evt_tracking')
                        ->select('trans_num')
                        ->where(function ($q) use($lastDate, $eventCode, $curDate) {
                            if ($lastDate) {
                                $q->where(DB::raw('FROM_UNIXTIME(created_at, "%Y-%m-%d")'), '>', $lastDate);
                                $q->where(DB::raw('FROM_UNIXTIME(created_at, "%Y-%m-%d")'), '<', $curDate);
                            }

                            if ($eventCode) {
                                $q->where('evt_code', $eventCode);
                            }
                        });

        $orderNums = $logQuery->lists('trans_num');

        return $orderNums;
    }

    /**
     * @chgCodes orderNums array
     * return array
    */

    public function commonQuery($chgCodes, $orderNums=[])
    {
        $chgResults = $this->chargeCodeData($chgCodes, $this->commonUoms);

        //get the order data from odr_hdr and bol_odr_dtl table
        $query = $this->model
//                       ->join('shipment AS sh', 'sh.ship_id', '=', 'odr_hdr.ship_id')
                       ->join('bol_odr_dtl AS sh', 'sh.odr_id', '=', 'odr_hdr.odr_id')
                        ->select('odr_hdr.cus_id',
                                'odr_hdr.whs_id',
                                DB::raw('FROM_UNIXTIME(odr_hdr.`shipped_dt`, "%Y-%m-%d") AS dt'),
                                DB::raw('SUM(ctn_qty) AS ctn'),
                                DB::raw('SUM(piece_qty) AS pcs'),
                                DB::raw('SUM(cube_qty_ttl) AS vol'),
                                DB::raw('ROUND(SUM(cube_qty_ttl) * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8) AS volMetric'),
                                DB::raw('SUM(plt_qty) AS plt'),
                                DB::raw('COUNT(DISTINCT(odr_hdr.odr_num)) AS ord'),
                                DB::raw('COUNT(DISTINCT(odr_hdr.ship_id)) AS bol')
                                )
                        ->whereIn('odr_num', $orderNums)
                        ->whereNotNull('odr_hdr.ship_id')
                        ->where('odr_hdr.odr_sts', Status::getByValue('Shipped', 'ORDER-STATUS'))
                        ->where(function($q) {
                           $q->where('back_odr', '=', 0);
                           $q->orWhereNull('back_odr');
                        })
                        ->groupBy('odr_hdr.cus_id', 'odr_hdr.whs_id', 'dt');


        $ordResult =  $query->get();

        $result = [
            'ordResult' => $ordResult,
            'chgResults' => $chgResults
        ];

        return !$ordResult->isEmpty() ? $result : [];
    }

    /**
     * @param array row
     * @return array
    */

    public function getShipCartonCount($param, $volField)
    {
        $volume = $param['vol'];
        $val = $param['val'];
        $row = $param['row'];

        $volResults = $this->model
            ->join('pack_hdr AS ph', 'ph.odr_hdr_id', '=', 'odr_hdr.odr_id')
            ->join('evt_tracking AS et', 'et.trans_num', '=', 'odr_hdr.odr_num')
            ->join('inv_vol_rates AS iv', 'iv.cus_id', '=', 'ph.cus_id')
            ->join('charge_type AS ct', 'ct.chg_type_id', '=', 'iv.chg_type_id')
            ->join('system_uom AS su', 'su.sys_uom_id', '=', 'iv.chg_uom_id')
            ->where('su.sys_uom_name', $param['uom'])
            ->where('ct.chg_type_name', Invoice::OP)
            ->whereIn('odr_hdr.odr_num', $param['orderNums'])
            ->select(
                'ph.cus_id',
                'ph.whs_id',
                DB::raw(
                    'IF(act_cmpl_dt,
                        FROM_UNIXTIME(act_cmpl_dt, "%Y-%m-%d"),
                        FROM_UNIXTIME(et.created_at, "%Y-%m-%d")
                    ) AS orderDt'
                ),
               'su.sys_uom_name',
                DB::raw('COUNT(DISTINCT pack_hdr_id) AS ctnCount')
            )
            ->where(function ($query) use ($volume, $volField) {

                $max = getDefault($volume->max_vol);

                $query->where($volField, '>', $volume->min_vol);

                if (floatVal($max)) {
                    $query->where($volField, '<=', $max);
                }
            })
            ->where('iv.chg_type_id', $val['chg_type_id'])
            ->whereIn('iv.chg_code_id', $val['chg_code_id'])
            ->where(DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d")'), $row['dt'])
            ->whereIn('evt_code', $this->eventOrder)
            ->where('ph.cus_id', $row['cus_id'])
            ->where('ph.whs_id', $row['whs_id'])
            ->groupBy('ph.cus_id', 'ph.whs_id', 'orderDt')
            ->first();

        return $volResults ? $volResults->ctnCount : [];

    }


    /**
     * @chgCodes orderNums array
     * return array
    */

    public function workOrderQuery($chgCodes, $orderNums=[])
    {
        $chgResults = $this->chargeCodeData($chgCodes, $this->workUoms);

        //get the work order data from odr_hdr , wo_hdr and wo_dtl table
        $query = $this->model
                       ->join('wo_hdr AS wh', 'wh.odr_hdr_id', '=', 'odr_hdr.odr_id')
                       ->join('wo_dtl AS wd', 'wd.wo_hdr_id', '=', 'wh.wo_hdr_id')
                        ->select('odr_hdr.cus_id',
                                'odr_hdr.whs_id',
                                'chg_code_id',
                                DB::raw('FROM_UNIXTIME(odr_hdr.`shipped_dt`, "%Y-%m-%d") AS dt'),
                                DB::raw('SUM(qty) AS qty')
                                )
                        ->whereIn('odr_num', $orderNums)
                        ->where('odr_hdr.odr_sts', Status::getByValue('Shipped', 'ORDER-STATUS'))
                        ->whereNotNull('ship_id')
                        ->where(function($q) {
                           $q->where('back_odr', '=', 0);
                           $q->orWhereNull('back_odr');
                        })
                        ->groupBy('odr_hdr.cus_id', 'odr_hdr.whs_id',
                                  'chg_code_id', 'dt');

        $wrkResult =  $query->get();

        $result = [
            'wrkResult' => $wrkResult,
            'chgResults' => $chgResults
        ];

        return !$wrkResult->isEmpty() ? $result : [];
    }


    /**
     * @chgCodes orderNums array
     * return array
    */

    public function carrierQuery($chgCodes, $orderNums=[])
    {
        $chgResults = $this->chargeCodeData($chgCodes, $this->carrierUoms);

        //get the carrier data from odr_hdr shipment table
        $query = $this->model
                       ->join('shipment AS sh', 'sh.ship_id', '=', 'odr_hdr.ship_id')
                        ->select('odr_hdr.cus_id',
                                 'odr_hdr.whs_id',
                                  DB::raw('FROM_UNIXTIME(odr_hdr.`shipped_dt`, "%Y-%m-%d") AS dt'),
                                  'sh.carrier',
                                  DB::raw('COUNT(odr_hdr.odr_num) AS qty')
                                )
                        ->whereIn('odr_num', $orderNums)
                        ->where('odr_hdr.odr_sts', Status::getByValue('Shipped', 'ORDER-STATUS'))
                        ->whereNotNull('odr_hdr.ship_id')
                        ->where(function($q) {
                           $q->where('back_odr', '=', 0);
                           $q->orWhereNull('back_odr');
                        })
                        ->groupBy('odr_hdr.cus_id', 'odr_hdr.whs_id',
                                  'sh.carrier', 'dt');

        $cariResult =  $query->get();

        $result = [
            'cariResult' => $cariResult,
            'chgResults' => $chgResults
        ];

        return !$cariResult->isEmpty() ? $result : [];
    }

    /**
     * @chgCodes orderNums array
     * return array
    */

    public function labelQuery($chgCodes, $orderNums=[])
    {
        $chgResults = $this->chargeCodeData($chgCodes, $this->labelUoms);

        //get the label data from odr_hdr and odr_dtl table
        $query = $this->model
                       ->join('odr_dtl AS od', 'od.odr_id', '=', 'odr_hdr.odr_id')
                        ->select('odr_hdr.cus_id',
                                'odr_hdr.whs_id',
                                DB::raw('FROM_UNIXTIME(odr_hdr.`shipped_dt`, "%Y-%m-%d") AS dt'),
                                DB::raw('COUNT(DISTINCT(ship_track_id)) AS label')
                                )
                        ->whereIn('odr_num', $orderNums)
                        ->where('odr_hdr.odr_sts', Status::getByValue('Shipped', 'ORDER-STATUS'))
                        ->where('odr_type', config('constants.odr_type.ECOM'))
                        ->where('is_ecom', '1')
                        ->whereNotNull('odr_hdr.ship_id')
                        ->whereNotNull('ship_track_id')
                        ->where(function($q) {
                           $q->where('odr_hdr.back_odr', '=', 0);
                           $q->orWhereNull('odr_hdr.back_odr');
                        })
                        ->groupBy('odr_hdr.cus_id', 'odr_hdr.whs_id', 'dt');


        $lblResult =  $query->get();

        $result = [
            'lblResult' => $lblResult,
            'chgResults' => $chgResults
        ];

        return !$lblResult->isEmpty() ? $result : [];
    }


     /**
     * @chgCodes orderNums array
     * return array
    */

    public function cancelQuery($chgCodes, $orderNums=[])
    {
        $chgResults = $this->chargeCodeData($chgCodes, $this->cancelUoms);

        //get the cancel order data from odr_hdr table
        $query = $this->model
                        ->select('odr_hdr.cus_id',
                                 'odr_hdr.whs_id',
                                 DB::raw('FROM_UNIXTIME(odr_hdr.`act_cancel_dt`, "%Y-%m-%d") AS dt'),
                                 DB::raw('COUNT(odr_hdr.odr_num) AS cancel')
                                )
                        ->whereIn('odr_num', $orderNums)
                        ->where('odr_sts', config('constants.odr_status.CANCELLED'))
                        ->where(function($q) {
                           $q->where('odr_hdr.back_odr', '=', 0);
                           $q->orWhereNull('odr_hdr.back_odr');
                        })
                        ->groupBy('odr_hdr.cus_id', 'odr_hdr.whs_id', 'dt');

        $cnclResult =  $query->get();

        $result = [
            'cnclResult' => $cnclResult,
            'chgResults' => $chgResults
        ];

        return !$cnclResult->isEmpty() ? $result : [];
    }


    /**
     * @result charge array
     * return array
    */

    public function getData($result, $charge, $orderNums=[])
    {
        $data = [];

        $monthlyCharges = $this->ivcSumModel->getVolRangeUOMs();

        $monthlyChargeKeys = array_flip($monthlyCharges);

        foreach ($result->toArray() as $row) {
            $data = $this->getDataExecute([
                'return' => $data,
                'charge' => $charge,
                'row' => $row,
                'orderNums' => $orderNums,
                'monthlyChargeKeys' => $monthlyChargeKeys,
            ]);
        }

        return $data;
    }

    /*
    ****************************************************************************
    */

    public function getDataExecute($data)
    {
        $return = $data['return'];
        $charge = $data['charge'];
        $row = $data['row'];
        $orderNums = $data['orderNums'];
        $monthlyChargeKeys = $data['monthlyChargeKeys'];

        $cube = 'ph.width * ph.height * ph.length';

        foreach ($charge as $uom => $val) {

            $field = $volField = NULL;

            switch ($uom) {
                case Invoice::$carton:
                    $field = 'ctn';
                    break;
                case Invoice::$piece:
                    $field = 'pcs';
                    break;
                case Invoice::$volume:
                case Invoice::$volumeImperial:
                    $field = 'vol';
                    break;
                case Invoice::$volumeMetric:
                    $field = 'volMetric';
                    break;
                case Invoice::$pallet:
                    $field = 'plt';
                    break;
                case Invoice::$order:
                    $field = 'ord';
                    break;
                case Invoice::$bol:
                    $field = 'bol';
                    break;
                case Invoice::$label:
                    $field = 'label';
                    break;
                case Invoice::$orderCancel:
                    $field = 'cancel';
                    break;
                case Invoice::$smallCarton:
                case Invoice::$mediumCarton:
                case Invoice::$largeCarton:
                case Invoice::$smallCartonImperial:
                case Invoice::$mediumCartonImperial:
                case Invoice::$largeCartonImperial:

                    $volField = DB::raw('ROUND(' . $cube . ' / 1728, 8)');

                    $field = 'ctnCount';
                    break;
                case Invoice::$smallCartonMetric:
                case Invoice::$mediumCartonMetric:
                case Invoice::$largeCartonMetric:

                    $volField = DB::raw(
                            'ROUND(' . $cube . ' * ' . Invoice::CUBIC_INCH_TO_CUBIC_METER . ', 8)'
                    );

                    $field = 'ctnCount';
                    break;
                default:
                   continue 2;
            }

            $row['ctnCount'] = isset($monthlyChargeKeys[$uom]) ?
                    $this->getCtnCount([
                        'uom' => $uom,
                        'orderNums' => $orderNums,
                        'val' => $val,
                        'row' => $row,
                        'volField' => $volField,
                    ]) : 0;

            foreach ($val['chg_code_id'] as $code) {
                $return[] = [
                    'whs_id' => $row['whs_id'],
                    'cus_id' => $row['cus_id'],
                    'qty' => floatVal($row[$field]),
                    'chg_type_id' => $val['chg_type_id'],
                    'chg_uom_id' => $val['sys_uom_id'],
                    'chg_code_id' => $code,
                    'dt' => $row['dt'],
                ];
            }
        }

        return $return;
    }

    /*
    ****************************************************************************
    */

    public function getCtnCount($data)
    {
        $uom = $data['uom'];
        $orderNums = $data['orderNums'];
        $val = $data['val'];
        $row = $data['row'];
        $volField = $data['volField'];

        $ctnCount = 0;

        //query from inv_vol_rates  for each vol uom
        $volume = $this->ivcSumModel->checkVolumeRates($row, $val);

        if (count($volume) > 0) {

            $param = [
                'uom' => $uom,
                'orderNums' => $orderNums,
                'vol' => $volume,
                'val' => $val,
                'row' => $row
            ];

            //query to calcualte cartons based on the total volume
            $ctnCount = $this->getShipCartonCount($param, $volField);
        }

        return $ctnCount;
    }

    /**
     * @result charge array
     * return array
    */

    public function getWorkData($result,$charge)
    {
        $data = [];

        foreach ($result->toArray() as $row) {
            foreach ($charge as $uom => $val) {
                $data[] = [
                    'whs_id' => $row['whs_id'],
                    'cus_id' => $row['cus_id'],
                    'qty' => floatVal($row['qty']),
                    'chg_type_id' => $val['chg_type_id'],
                    'chg_uom_id' => $val['sys_uom_id'],
                    'chg_code_id' => $row['chg_code_id'],
                    'dt' => $row['dt'],
                ];
            }
        }

        return $data;
    }

    /**
     * @result charge array
     * return array
    */

    public function getCarrierData($result, $charge)
    {
        $data = [];

        foreach ($result->toArray() as $row) {
            foreach ($charge as $uom => $val) {

                switch ($uom) {
                    case Invoice::$ups:
                        $carrier = 'UPS';
                        break;
                    case Invoice::$fedex:
                        $carrier = 'FEDEX';
                        break;
                    default:
                       break;
                }

                if (strtoupper($row['carrier']) != $carrier) {
                    continue;
                }

                foreach ($val['chg_code_id'] as $code) {

                    $data[] = [
                        'whs_id' => $row['whs_id'],
                        'cus_id' => $row['cus_id'],
                        'qty' => floatVal($row['qty']),
                        'chg_type_id' => $val['chg_type_id'],
                        'chg_uom_id' => $val['sys_uom_id'],
                        'chg_code_id' => $code,
                        'dt' => $row['dt'],
                    ];
                }

            }
        }

        return $data;
    }


    /**
     * @chgCodes uoms array
     * return array
    */

    public function chargeCodeData($chgCodes, $uoms)
    {
        $chgResults = [];

        foreach($chgCodes as $row) {
            $uom = $row['system_uom']['uom'];
            if (in_array($uom, $uoms)) {
                $chgResults[$uom]['chg_code_id'][] = $row['chg_code_id'];
                $chgResults[$uom]['chg_type_id'] = $row['charge_type']['chg_type_id'];
                $chgResults[$uom]['sys_uom_id'] = $row['system_uom']['sys_uom_id'];
            }
        }

        return $chgResults;
    }
    
    /**
    * @return array
    */
    
    public function getCommonUOMs() 
    {
        return [
            Invoice::$carton,
            Invoice::$piece,
            Invoice::$volume,
            Invoice::$volumeImperial,
            Invoice::$volumeMetric,
            Invoice::$pallet,
            Invoice::$order,
            Invoice::$bol
         ];
    }
}