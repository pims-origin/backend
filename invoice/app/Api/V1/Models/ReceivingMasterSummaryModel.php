<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use \DateTime;
use App\Invoice;

use App\Api\V1\Models\SummaryModel;
use App\Api\V1\Models\CommonInvoiceModel;
use App\Api\V1\Models\VolumeRatesModel;
use App\Api\V1\Models\InvoicingSummaryModel;

use Seldat\Wms2\Models\RcvSum;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\InvoicingSummary;
use Seldat\Wms2\Models\Pallet;


class ReceivingMasterSummaryModel extends AbstractModel
{
    /**
    * @var
    */
    protected $rcv;
    protected $inv;
    protected $invSum;
    protected $vol;
    protected $ivcSumModel;
    public $volRates = [];


    /**
     * Constructor
    */

    public function __construct()
    {
        $this->rcv = new RcvSum();
        $this->inv = new InvoicingSummary();
        $this->ivcSumModel = new InvoicingSummaryModel();

        $this->invSum = new SummaryModel();
        $this->vol = new VolumeRatesModel();
    }

    /**
     * Populate Receiving Charge codes for each customer when container received
    */

    public function receivingChargeCodes()
    {
        $chgResults = $data = [];
                
        $type = Invoice::RC;
        
        //get max date from invoice_summary for receiving charge codes
        $maxRcv = $this->ivcSumModel->getMaxDate($type);

        $lastDate = $maxRcv->maxDate;

        $prevDate = date('Y-m-d', strtotime('-1 days'));
        $curDate = date('Y-m-d');

        if ($lastDate && $lastDate == $prevDate) {
            return [];
        }

        //get all receiving charge codes
        $chgCodes = $this->invSum->getChargeCodes($type);

        foreach($chgCodes as $row) {
            $uom = $row['system_uom']['uom'];

            $chgResults[$uom]['chg_code_id'][] = $row['chg_code_id'];
            $chgResults[$uom]['chg_type_id'] = $row['charge_type']['chg_type_id'];
            $chgResults[$uom]['sys_uom_id'] = $row['system_uom']['sys_uom_id'];
        }


        //get the container data from rcv_sum table
        $query = $this->rcv
                        ->select('cus_id', 'whs_id', 'dt',
                                 DB::raw('SUM(cntr_val) AS cntr'),
                                 DB::raw('ROUND(SUM(vol_val) * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8) AS volMetric')
                                )
                        ->selectRaw('1 AS month, 0 AS ctnCount,
                                    SUM(ctn_val) AS ctn, SUM(pcs_val) AS pcs,
                                    SUM(plt_val) AS plt, SUM(vol_val) AS vol,
                                    SUM(vol_val) AS vol'
                                    )
                        ->where(function ($q) use ($lastDate, $curDate) {
                            if ($lastDate) {
                                $q->where('dt', '>', $lastDate);
                                $q->where('dt', '<', $curDate);
                            }
                        })
                        ->groupBy('cus_id', 'whs_id', 'dt');

        $rcvResult =  $query->get();


        if ($rcvResult->isEmpty()) {
             return [];
        }
        
        $data = $this->chargeCodeData($rcvResult, $chgResults);

        //insert into invoice_summary table
        DB::beginTransaction();

        if (! empty($data)) {
            foreach($data as $row) {
                //check integrity vialotion
                if (! $this->invSum->getFirstWhere([
                    'cus_id'      => $row['cus_id'],
                    'whs_id'      => $row['whs_id'],
                    'chg_type_id' => $row['chg_type_id'],
                    'chg_code_id' => $row['chg_code_id'],
                    'dt' => $row['dt']
                ])){
                    $this->inv->create($row);
                }
            }
        }
var_dump('receiving charge codes populated');

        DB::commit();
    }
    
    
    /**
     * @param rcvResult array
     * @param chgResults array
     * @return array
    */
    
    public function chargeCodeData($rcvResult, $chgResults)
    {
        $data = [];
        
        //query from inv_vol_rates for Receiving type
        $volumeModel = $this->ivcSumModel->volumeRanges(Invoice::RC);
       
        $monthlyCharges = $this->ivcSumModel->getVolRangeUOMs();
       
        foreach ($rcvResult->toArray() as $row) {
            foreach ($chgResults as $uom => $val) {
                switch ($uom) {
                    case Invoice::$container:
                        $field = 'cntr';
                        break;
                    case Invoice::$month:
                        $field = 'month';
                        break;
                    case Invoice::$volume:
                    case Invoice::$volumeImperial:
                        $field = 'vol';
                        break;
                    case Invoice::$volumeMetric:
                        $field = 'volMetric';
                        break;
                    case Invoice::$piece:
                        $field = 'pcs';
                        break;
                    case Invoice::$pallet:
                        $field = 'plt';
                        break;
                    case Invoice::$smallCarton:
                    case Invoice::$mediumCarton:
                    case Invoice::$largeCarton:
                    case Invoice::$smallCartonImperial:
                    case Invoice::$mediumCartonImperial:
                    case Invoice::$largeCartonImperial:
                        $field = 'ctnCount';
                        $volField = 'ca.cube';
                        break;
                    case Invoice::$smallCartonMetric:
                    case Invoice::$mediumCartonMetric:
                    case Invoice::$largeCartonMetric:
                        $field = 'ctnCount';
                        $volField = DB::raw('ROUND(ca.cube * ' . Invoice::CUBIC_FOOT_TO_CUBIC_METER . ', 8)');
                        break;
                    case Invoice::$carton:
                    default:
                        $field = 'ctn';
                        $volField = NULL;
                        break;
                }

                //Based on carton size - get cartons count
                $row['ctnCount']  = in_array($uom, $monthlyCharges) ?
                            $this->checkVolumeRange([
                                'uom' => $uom,
                                'volModel' => $volumeModel,
                                'val' => $val,
                                'row' => $row,
                                'volField' =>  $volField
                            ]) : 0;


                //populate receiving charge codes
                foreach ($val['chg_code_id'] as $code) {

                    $data[] = [
                        'whs_id' => $row['whs_id'],
                        'cus_id' => $row['cus_id'],
                        'qty' => floatVal($row[$field]),
                        'chg_type_id' => $val['chg_type_id'],
                        'chg_uom_id' => $val['sys_uom_id'],
                        'chg_code_id' => $code,
                        'dt' => $row['dt'],
                    ];
                }
            }
        }
        
        return $data;
    }

    /**
     * @param array row
     * @return array
    */

    public function getContainerID($row) {

        $containers = $this->rcv
                            ->select('cus_id', 'whs_id', 'dt', 'gr_hdr_id')
                            ->where('dt', $row['dt'])
                            ->where('cus_id', $row['cus_id'])
                            ->where('whs_id', $row['whs_id'])
                            ->get();

       $grHdrIDs = $containers->lists('gr_hdr_id');

       return $grHdrIDs ? $grHdrIDs : [];
    }

    /**
     * @param array row
     * @return array
    */
    
    public function checkVolumeRange($param)
    {
        $ctnCount = 0;
        
        $uom = $param['uom'];
        $volumeModel = $param['volModel'];
        $val = $param['val'];
        $row = $param['row'];
        $type = Invoice::RC;
        $volField = $param['volField'];
        
        //get containers for the date
        $grHdrIDs = $this->getContainerID($row);

        if (count($volumeModel->volume) > 0) {

            $param = [
                'uom' => $uom,
                'grHdrID' => $grHdrIDs,
                'volModel' => $volumeModel,
                'val' => $val,
                'row' => $row,
                'type' => $type,
                'volField' =>  $volField
            ];

            //query to calcualte cartons based on the total volume
            $ctnCount = $this->getRcvCartonCount($param);
        }
        
        return $ctnCount;
    }
    
    
    /**
     * @param array row
     * @return array
    */

    public function getRcvCartonCount($param)
    {
        $volField = $param['volField'];
        $cusField = 'ca.cus_id';
        $whsField = 'ca.whs_id';
        $dateField = 'created';

        $volModel = $param['volModel'];
        $val = $param['val'];
        $row = $param['row'];
        $size = $param['uom'];
        $type = $param['type'];

        $custVolRate = $volModel->custVolumeRange($type, $row);


        if (! getDefault($custVolRate[$size]['min_vol'])) {
            return [];
        }

        $query = DB::table('rcv_sum AS rs')
                        ->join('cartons AS ca', 'ca.gr_hdr_id', '=', 'rs.gr_hdr_id')
                        ->join('inv_vol_rates AS iv', 'iv.cus_id', '=', 'ca.cus_id')
                        ->join('charge_type AS ct', 'ct.chg_type_id', '=', 'iv.chg_type_id')
                        ->join('system_uom AS su', 'su.sys_uom_id', '=', 'iv.chg_uom_id')
                        ->where('su.sys_uom_name', $param['uom'])
                        ->where('ct.chg_type_name', $type)
                        ->whereIn('ca.gr_hdr_id', $param['grHdrID'])
                        ->select('ca.cus_id',
                                 'ca.whs_id',
                                  DB::raw('rs.dt AS created'),
                                  DB::raw('COUNT(DISTINCT ctn_id) AS ctnCount')
                        )
                        ->where(function ($q) use( $custVolRate, $size, $volField ){
                            $max = getDefault($custVolRate[$size]['max_vol']);
                            $q->where($volField, '>', getDefault($custVolRate[$size]['min_vol']));

                            if (floatVal($max)) {
                                $q->where($volField, '<=', $max);
                            }
                        })
                        ->where('rs.dt', $row['dt'])
                        ->where('iv.chg_type_id', $val['chg_type_id'])
                        ->whereIn('iv.chg_code_id', $val['chg_code_id'])
                        ->where($cusField, $row['cus_id'])
                        ->where($whsField, $row['whs_id'])
                        ->where('iv.deleted', 0)
                        ->groupBy($cusField, $whsField, $dateField);

        $volResults = $query->first();

        return $volResults ? $volResults->ctnCount : [];
    }
}



