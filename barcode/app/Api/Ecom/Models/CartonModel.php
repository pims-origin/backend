<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\Ecom\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class CartonModel extends AbstractModel
{
    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    public function getSugLocByWvDtl($wvDtl)
    {
        $whsId = Data::getCurrentWhsId();

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons')
            ->select([
                'loc_id',
                'loc_code',
                'rfid',
                DB::raw('SUM(piece_remain) as avail_qty'),
                DB::raw('COUNT(ctn_id) AS ctns')
            ])
            ->where('is_damaged', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'ECO')
            ->where('item_id', $wvDtl->item_id)
            ->where('lot', 'ECO')
            ->where('is_ecom', 1)
            ->where('whs_id', $whsId)
            ->groupBy('loc_id');

        return $query->get();
    }

    public function checkLocByWvDtl($locId, $itemId, $lot)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        return $this->model->select([
            'loc_id',
            'loc_code',
            DB::raw('COALESCE(SUM(piece_remain), 0) as avail_qty'),
            DB::raw('COUNT(ctn_id) as ctns')
        ])
            ->where('whs_id', Data::getCurrentWhsId())
            ->where('loc_id', $locId)
            ->where('item_id', (int)$itemId)
            ->where('lot', $lot)
            ->first();
    }

    public function getAvailableQuantity($itemID, $lot, $locId, $whs_id)
    {
        $query = $this->getModel()
            ->where('loc_id', $locId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 1)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'ECO')
            ->where('lot', $lot);

        $qty = $query->sum('piece_remain');

        return $qty;


    }

    public function getAllCartonsByWvDtl($wvDtl, $algorithm = 'FIFO', $locIds, $actLocs)
    {
        $packSize = $wvDtl['pack_size'];

        $whsId = $wvDtl['whs_id'];
        $itemId = $wvDtl['item_id'];
        $lot = $wvDtl['lot'];
        $packSizes = $this->getAvailablePacksizes($whsId, $itemId, $lot, $locIds, $algorithm);

        if (!in_array($packSize, $packSizes)) {
            $packSize = array_shift($packSizes);
        } else {
            $packSizes = $this->array_remove($packSize, $packSizes);
        }

        array_unshift($packSizes, $packSize);
        $pickedcartons = [];

        foreach ($actLocs as $actLoc) {
            $pickedQty = $actLoc['picked_qty'];
            foreach ($packSizes as $packSize) {
                $ctns = ceil($pickedQty / $packSize) + 50;
                $cartons = $this->getCartonsByPack($whsId, $itemId, $lot, $actLoc['act_loc_id'], $packSize, $ctns,
                    $algorithm);

                $qtys = array_column($cartons, 'piece_remain');
                $total = array_sum($qtys);
                $pickedcartons = array_merge($pickedcartons, $this->getPickCartons($cartons, $pickedQty));
                $pickedQty = $pickedQty - $total;

                //break if enough cartons
                if ($pickedQty <= 0) {
                    break;
                }
            }
        }

        return $pickedcartons;
    }

    public function getAvailablePacksizes($whs_id, $itemID, $lot, $locIds, $algorithm)
    {
        $query = $this->getModel()
            ->whereIn('loc_id', $locIds)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 1)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'ECO')
            ->where('lot', $lot)
            ->groupBy('ctn_pack_size');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }


        $res = $query->get(['ctn_pack_size'])->toArray();

        return array_column($res, 'ctn_pack_size');
    }

    public function array_remove($remove, &$array)
    {
        foreach ((array)$array as $key => $value) {
            if ($value == $remove) {
                unset($array[$key]);
                break;
            }
        }

        return $array;
    }

    public function getCartonsByPack($whs_id, $itemID, $lot, $locId, $packSize, $ctns, $algorithm)
    {
        $this->getModel()->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->where('loc_id', $locId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 1)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'ECO')
            ->where('lot', $lot)
            ->where('ctn_pack_size', $packSize)
            ->limit($ctns)
            ->orderBy('ctn_pack_size');


        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        $query->orderBy('cartons.piece_remain', 'ASC');

        $res = $query->get()->toArray();

        return $res;
    }

    public function getPickCartons($cartons, $pickedQty)
    {
        $pickedcartons = [];
        foreach ($cartons as $carton) {
            //pick cartons
            $carton['pick_piece'] = false;
            if ($pickedQty < $carton['piece_remain']) {
                $carton['pick_piece'] = true;
                $carton['picked_qty'] = $pickedQty;
                $pickedQty = 0;
            } elseif ($pickedQty == $carton['piece_remain']) {
                $carton['picked_qty'] = $carton['piece_remain'];
                $pickedQty = 0;
            } else {
                $pickedQty = $pickedQty - $carton['piece_remain'];
                $carton['picked_qty'] = $carton['piece_remain'];
            }
            $pickedcartons[] = $carton;

            if ($pickedQty <= 0) {
                break;
            }
        }

        return $pickedcartons;
    }

    public function updateCartonByLocs($cartons)
    {
        /**
         * 1. Update full carton
         * 2. Update piece_remain and piece_ttl
         */
        $fullCtns = [0];
        foreach ($cartons as $idx => $carton) {
            if ($carton['pick_piece']) {
                $cartons[$idx] = $this->updatePickedPieceCarton($carton);
            } else {
                $fullCtns[$carton['ctn_id']] = $carton;
            }
        }

        $this->updatePickedFullCartons($fullCtns);

        return $cartons;
    }

    public function updatePickedPieceCarton($carton)
    {
        $remainQty = $carton['piece_remain'] - $carton['picked_qty'];
        $res = DB::table('cartons')->where('ctn_id', $carton['ctn_id'])->update(
            ['piece_remain' => $remainQty]
        );

        //change business clone new carton
        $origCtnId = $carton['ctn_id'];
        $origLocId = $carton['loc_id'];
        $maxCtnNum = $this->getMaxCtnNum($carton['ctn_num']);
        unset($carton['ctn_id']);

        $carton['inner_pack'] = 0;

        $newCarton = (new Carton())->fill($carton);
        $newCarton->piece_remain = $carton['picked_qty'];
        $newCarton->origin_id = $origCtnId;
        $newCarton->loc_id = $newCarton->loc_code = null;
        $newCarton->ctn_sts = 'PD';
        $newCarton->picked_dt = time();
        $newCarton->ctn_num = ++$maxCtnNum;
        $newCarton->save();

        $return = $newCarton->toArray();
        $return['pick_piece'] = true;
        $return['loc_id'] = $origLocId;
        $return['loc_code'] = $return['loc_name'];
        $return["picked_qty"] = $carton['picked_qty'];

        return $return;
    }

    public function updatePickedFullCartons($fullCtns)
    {
        $ctnIDs = array_keys($fullCtns);

        $res = DB::table('cartons')->whereIn('ctn_id', $ctnIDs)->update(
            [
                'ctn_sts'          => 'PD',
                'loc_id'           => null,
                'loc_code'         => null,
                //'loc_name'         => null,
                //'plt_id'           => null,
                'picked_dt'        => time(),
                'storage_duration' => self::getCalculateStorageDurationRaw()
            ]
        );

        return $res;
    }

    public function getMaxCtnNum($ctnNum)
    {
        $maxCtnNum = $this->model->where('ctn_num', 'LIKE', $ctnNum . '-%')->max('ctn_num');
        if ($maxCtnNum) {
            return $maxCtnNum;
        }

        return $ctnNum . '-C00';
    }

    public static function getCalculateStorageDurationRaw($startDate = 'created_at')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    public function putawaySuggestLocation($pltNum)
    {
        $this->model
            ->join('pallets AS pl', 'pl.plt_id', '=', 'cartons.plt_id')
            ->where('cartons.ctn_sts', 'TF')
            ->where('pl.plt_num', $pltNum);
    }
}
