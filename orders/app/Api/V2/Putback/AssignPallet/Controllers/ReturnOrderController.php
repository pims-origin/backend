<?php

namespace App\Api\V2\Putback\AssignPallet\Controllers;

use App\Api\V2\Putback\AssignPallet\Validators\UpdatePutBackValidator;
use App\Api\V2\Putback\Models\CartonModel;
use App\Api\V2\Putback\Models\LocationModel;
use App\Api\V2\Putback\Models\PalletModel;
use App\Api\V2\Putback\Models\PutBackModel;
use App\Api\V2\Putback\Models\ReturnDtlModel;
use App\Api\V2\Putback\Models\ReturnHdrModel;
use App\Jobs\AutoCreateBillable;
use App\Jobs\AutostorageChargeCode;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class ReturnOrderController extends AbstractController
{

    public function __construct(
        ReturnHdrModel $returnOrderModel,
        ReturnDtlModel $returnOrderDtlModel,
        PutBackModel $putBackModel
    ) {
        $this->returnHdrModel = $returnOrderModel;
        $this->returnDtlModel = $returnOrderDtlModel;
        $this->putBackModel = $putBackModel;
        $this->locationModel = new LocationModel();
        $this->cartonModel = new CartonModel();
        $this->palletModel = new PalletModel();
    }

    public function ReturnOrdersDetail($returnId)
    {
        $returnHdr = $this->returnHdrModel->getFirstBy('return_id', $returnId);
        if (empty($returnHdr)) {
            throw new \Exception(Message::get("BM017", "Return Order"));
        }
        $orderId = object_get($returnHdr, 'odr_hdr_id', '');
        try {
            $items = [];
            if ($orderId) {
                $returnDetails = $this->returnDtlModel->findWhere(
                    ['odr_hdr_id' => $orderId], ['createdUser']
                );

                foreach ($returnDetails as $returnDetail) {
                    $items[] = [
                        'item_id'      => array_get($returnDetail, 'item_id', ''),
                        'itm_sts'      => array_get($returnDetail, 'return_dtl_sts', ''),
                        'itm_sts_name' => Status::getByKey("RETURN-ORDER",
                            array_get($returnDetail, 'return_dtl_sts', '')),
                        'sku'          => array_get($returnDetail, 'sku', ''),
                        'size'         => array_get($returnDetail, 'size', ''),
                        'color'        => array_get($returnDetail, 'color', ''),
                        'uom_id'       => array_get($returnDetail, 'uom_id', ''),
                        'uom_name'     => object_get($returnDetail, "uom_name", null),
                        'pack'         => array_get($returnDetail, 'pack', ''),
                        'lot'          => array_get($returnDetail, 'lot', ''),
                        'piece_qty'    => $returnDetail->piece_qty,
                        'origin_ctns'  => object_get($returnDetail, "original_ctns", 0),
                        'new_ctns'     => object_get($returnDetail, "new_ctns", 0),
                        'ctns'         => object_get($returnDetail, "ctn_ttl", 0),
                        'created_at'   => array_get($returnDetail, 'created_at', '')->format('Y/m/d'),
                        'created_by'   => trim(object_get($returnDetail, "createdUser.first_name", null) . " " .
                            object_get($returnDetail, "createdUser.last_name", null)),
                    ];
                }
            }

            return [
                "data" => [
                    'return_id'       => object_get($returnHdr, 'return_id', ''),
                    'return_num'      => object_get($returnHdr, 'return_num', ''),
                    'odr_id'          => object_get($returnHdr, 'odr_hdr_id', ''),
                    'odr_num'         => object_get($returnHdr, 'odr_hdr_num', ''),
                    'cus_id'          => object_get($returnHdr, 'odrHdr.customer.cus_id', ''),
                    'cus_name'        => object_get($returnHdr, 'odrHdr.customer.cus_name', ''),
                    'return_sts'      => object_get($returnHdr, 'return_sts', ''),
                    'return_sts_name' => Status::getByKey("RETURN-ORDER", object_get($returnHdr, 'return_sts', '')),
                    'num_of_sku'      => count($items),
                    'return_qty'      => object_get($returnHdr, 'return_qty', ''),
                    'created_at'      => object_get($returnHdr, 'created_at', '')->format('Y-m-d'),
                    'created_by'      => trim(object_get($returnHdr, "createdUser.first_name", null) . " " .
                        object_get($returnHdr, "createdUser.last_name", null)),
                    'putter_id'       => object_get($returnHdr, 'putter', 0),
                    'putter_name'     => trim(object_get($returnHdr, "putterUser.first_name", null) . " " .
                        object_get($returnHdr, "putterUser.last_name", null)),
                    'csr'             => object_get($returnHdr, 'csr', 0),
                    //get item for return order detail
                    'items'           => $items
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getUpdatePutBack($returnId)
    {
        $returnHdr = $this->returnHdrModel->getFirstBy('return_id', $returnId);
        if (empty($returnHdr)) {
            throw new \Exception(Message::get("BM017", "Return Order"));
        }
        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $putbackList = $this->putBackModel->getModel()
                ->select([
                    "put_back.pb_id",
                    "put_back.return_dtl_id",
                    "put_back.item_id",
                    "put_back.sku",
                    "put_back.size",
                    "put_back.color",
                    "put_back.lot",
                    "put_back.suggest_loc_id AS suggested_loc_id",
                    "put_back.suggest_loc_code AS suggested_loc_code",
                    "put_back.actual_loc_id",
                    "put_back.actual_loc_code",
                    DB::raw("IF(put_back.pb_sts = 'NW', 'NEW', 'COMPLETE') AS status"),
                    "put_back.ctn_ttl",
                    "return_dtl.piece_qty",
                    "return_dtl.pack AS pack_size",
                    "put_back.is_new",
                    "return_dtl.uom_code AS uom",
                    "pallet.plt_num",
                    "pallet.rfid",
                    "pallet.plt_id",
                    "pallet.cus_id",
                    DB::raw("COALESCE(item.spc_hdl_code, 'NA') AS spc_hdl_code"),
                    DB::raw("SUBSTRING_INDEX(SUBSTRING_INDEX(pallet.rfid, '-',2), '-',-1) AS plt_type")

                ])
                ->join('return_dtl', 'return_dtl.return_dtl_id', '=', 'put_back.return_dtl_id')
                ->join('item', 'item.item_id', "=", "put_back.item_id")
                ->join('pallet', 'put_back.pallet_id', "=", "pallet.plt_id")
                ->where("put_back.return_id", $returnId)
                ->get();

            return [
                "data" => [
                    'return_id'       => object_get($returnHdr, 'return_id', ''),
                    'return_num'      => object_get($returnHdr, 'return_num', ''),
                    'odr_id'          => object_get($returnHdr, 'odr_hdr_id', ''),
                    'odr_num'         => object_get($returnHdr, 'odr_hdr_num', ''),
                    'cus_id'          => object_get($returnHdr, 'odrHdr.customer.cus_id', ''),
                    'cus_name'        => object_get($returnHdr, 'odrHdr.customer.cus_name', ''),
                    'return_sts'      => object_get($returnHdr, 'return_sts', ''),
                    'return_sts_name' => Status::getByKey("RETURN-ORDER", object_get($returnHdr, 'return_sts', '')),
                    'num_of_sku'      => OrderDtl::where('odr_id',
                        $returnHdr->odr_hdr_id)->distinct('item_id')->count('item_id'),
                    'return_qty'      => object_get($returnHdr, 'return_qty', ''),
                    //get item for return order detail
                    'details'         => $putbackList
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function updatePutBack(
        $returnId,
        Request $request,
        UpdatePutBackValidator $updatePutBackValidator
    ) {
        set_time_limit(0);
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['return_id'] = $returnId;

        $updatePutBackValidator->validate($input);

        //validate plt with locations
        $items = $this->locationModel->validateLocation($input['items']);
        //Create Ref
        $ref = md5('PB' . time());

        try {

            DB::beginTransaction();
            $locIds = array_pluck($input['items'], 'loc_id');
            $pallets = DB::table('pallet')->whereIn('loc_id', $locIds)->first();

            if (!empty($pallets)) {
                throw new \Exception("Some locations already have pallets.");
            }

            $dataUpdateIvt = [];
            foreach ($items as $data) {
                // Update PutBack
                $putBack = $this->putBackModel->getFirstWhere(['pb_id' => $data['pb_id']]);

                if (!empty($putBack)) {

                    $putBackParam = [
                        'pb_id'           => $putBack->pb_id,
                        'actual_loc_id'   => $data['loc_id'],
                        'actual_loc_code' => $data['loc_code'],
                        'sts'             => 'u',
                        'pb_sts'          => 'CO',
                    ];

                    $this->putBackModel->refreshModel();
                    $this->putBackModel->update($putBackParam);

                    // Update Carton
                    $data['plt_id'] = $putBack->pallet_id;
                    $data['item_id'] = $putBack->item_id;
                    $data['lot'] = object_get($putBack, 'lot', 'NA');
                    $data['ref'] = $ref;
                    $this->updateCarton($data);

                    // Update Pallet
                    $this->updatePallet($data);

                    //update location to dynamic zone
                    if (isset($data['dynamic_zone_id'])) {
                        DB::table('location')->where('loc_id', $data['loc_id'])
                            ->update(['loc_zone_id' => $data['dynamic_zone_id']]);
                    }

                }


                // Update Status Return Detail
                $this->returnDtlModel->refreshModel();
                $returnDtl = $this->returnDtlModel->update([
                    'return_dtl_id'  => $data['return_dtl_id'],
                    'return_dtl_sts' => 'CO'
                ]);

                $data['return_qty'] = $returnDtl->piece_qty;
                $dataUpdateIvt[$data['item_id'] . "-" . $data['lot']] = $data;
            }

            // Update Return Hdr
            $this->returnHdrModel->refreshModel();
            $this->returnHdrModel->update([
                'return_id'  => $returnId,
                'return_sts' => 'CO'
            ]);

            // Update Order Cartons
            DB::commit();

            //$this->createBillableViaJob($returnId, $request);

            return $this->response->noContent()->setContent(["message" => "Update Successful!"])->setStatusCode
            (Response::HTTP_OK);
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function locationSuggestion($cusId, $pltType, $spcHdlCode, Request $request)
    {
        $input = $request->getQueryParams();
        $locCode = $input['loc_code'] ?? null;
        $whsId = Data::getCurrentWhsId();
        $dataSuggest = $this->locationModel->suggestLocation(
            $cusId,
            $whsId,
            [$pltType . "-" . $spcHdlCode => 20],
            $locCode);

        return ['data' => $dataSuggest];
    }

    private function updateCarton($data)
    {
        $this->cartonModel->refreshModel();
        $this->cartonModel->updateWhere([
            'loc_id'        => $data['loc_id'],
            'loc_code'      => $data['loc_code'],
            'loc_name'      => $data['loc_code'],
            'ctn_sts'       => 'AC',
            'loc_type_code' => "RAC",
            'ref'           => $data['ref']
        ], ['plt_id' => $data['plt_id']]);

    }

    private function updatePallet($pallet)
    {
        $palletParram = [
            'plt_id'   => $pallet['plt_id'],
            'loc_id'   => $pallet['loc_id'],
            'loc_code' => $pallet['loc_code'],
            'loc_name' => $pallet['loc_code'],
            'plt_sts'  => 'AC',
            'rfid'     => null,
            'ref'      => $pallet['ref']
        ];

        $this->palletModel->refreshModel();
        $this->palletModel->update($palletParram);
    }

    public function createBillableViaJob($returnId, $request)
    {
        $returnHdr = DB::table('return_hdr')
            ->select('odr_hdr.whs_id', 'odr_hdr.cus_id')
            ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'return_hdr.odr_hdr_id')
            ->where('return_hdr.return_sts', 'CO')
            ->where('return_hdr.return_id', $returnId)
            ->first();
        if (isset($returnHdr['whs_id']) && isset($returnHdr['cus_id'])) {
            dispatch(new AutoCreateBillable($returnHdr['whs_id'], $returnHdr['cus_id'], $returnId, $request));
        }
    }

}