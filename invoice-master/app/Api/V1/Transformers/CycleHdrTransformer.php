<?php

namespace App\Api\V1\Transformers;

use App\CycleHdr;
use League\Fractal\TransformerAbstract;

class CycleHdrTransformer extends TransformerAbstract
{

    /**
     * ps:Show GoodsReceipt
     *
     * @param CycleHdr $cycleHdr
     *
     * @return array
     */
    public function transform(CycleHdr $cycleHdr)
    {
        return [
            'cycle_hdr_id' => $cycleHdr->cycle_hdr_id,
            'cycle_num' => $cycleHdr->cycle_num,
        ];
    }
}
