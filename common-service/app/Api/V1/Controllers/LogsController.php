<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\LogsModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Models\EventLookupModel;
use App\Api\V1\Transformers\SystemCountryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use App\Api\V1\Transformers\LogsTransformer;
use App\Api\V1\Validators\SystemCountryValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\SystemBug;

/**
 * Class CountryController
 *
 * @package App\Api\V1\Controllers
 */
class LogsController extends AbstractController
{

    /**
     * @var SystemStateModel
     */
    protected $systemStateModel;

    /**
     * @var SystemCountryModel
     */
    protected $systemCountryModel;

    /**
     * @var SystemCountryValidator
     */
    protected $systemCountryValidator;

    /**
     * @var SystemCountryTransformer
     */
    protected $systemCountryTransformer;

    /**
     * @var SystemStateTransformer
     */
    protected $systemStateTransformer;

    /**
     * @var SystemCountryModel
     */
    protected $model;

    /**
     * @var logsModel
     */
    protected $logsModel;

    /**
     * @var EventLookupModel
     */
    protected $eventLookupModel;

    /**
     * @var LogsTransformer
     */
    protected $logsTransformer;

    /**
     * CountryController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->model = new SystemCountryModel();
        $this->systemCountryModel = new SystemCountryModel();
        $this->systemStateModel = new SystemStateModel();
        $this->systemCountryValidator = new SystemCountryValidator();
        $this->systemCountryTransformer = new SystemCountryTransformer();
        $this->systemStateTransformer = new SystemStateTransformer();
        $this->logsModel = new LogsModel();
        $this->logsTransformer = new LogsTransformer();
        $this->eventLookupModel = new EventLookupModel();
    }


    /**
     * @param $whs_id
     * @param Request $request
     * @param LogsTransformer $logsTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function logsList(
        $whs_id,
        Request $request,
        LogsTransformer $logsTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            // get list Cmd
            $logsInfos = $this->logsModel->search($input, $whs_id,
                [],
                array_get($input, 'limit'));

            return $this->response->paginator($logsInfos, $logsTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'logsList', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function typeDropDown(
        $whs_id,
        Request $request
    ) {
        try {
            $input = $request->getQueryParams();
            // get list Cmd
            $typeDropDownInfos = $this->logsModel->typeDropDown($input, $whs_id);

            return ['data' => $typeDropDownInfos];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'logsList', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function evtCodeDropDown(
        $whs_id,
        Request $request
    ) {
        try {
            $input = $request->getQueryParams();
            // get list Cmd
            $evtCodeDropDownInfos = $this->eventLookupModel->evtCodeDropDown($input, $whs_id);

            return ['data' => $evtCodeDropDownInfos];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'logsList', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
