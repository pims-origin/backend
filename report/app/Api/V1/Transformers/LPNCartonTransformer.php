<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-June-2017
 * Time: 09:27
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;


class LPNCartonTransformer extends TransformerAbstract
{
    public function transform(Carton $carton)
    {
        $location = \DB::table('location')
        ->where('loc_whs_id', object_get($carton, 'whs_id'))
        ->where('is_block_stack', 1)
        ->where('parent_id', object_get($carton, 'loc_id'))
        ->first();

        return [
            'ctn_id'            => object_get($carton, 'ctn_id'),
            'plt_id'            => object_get($carton, 'plt_id'),
            'cus_id'            => object_get($carton, 'cus_id'),
            'cus_name'          => object_get($carton, 'cus_name'),
            'cus_code'          => object_get($carton, 'cus_code'),
            'loc_id'            => empty($location) ? object_get($carton, 'loc_id') : $location['loc_id'],
            'loc_name'          => empty($location) ? object_get($carton, 'loc_name') : $location['loc_alternative_name'],
            'loc_code'          => empty($location) ? object_get($carton, 'loc_code') : $location['loc_code'],
            'sku'               => object_get($carton, 'sku'),
            'size'              => object_get($carton, 'size'),
            'color'             => object_get($carton, 'color'),
            'ctn_pack_size'     => object_get($carton, 'pack'),
            'item_id'           => object_get($carton, 'item_id'),
            'lot'               => object_get($carton, 'lot'),
            'des'               => object_get($carton, 'des'),
            'upc'               => object_get($carton, 'upc'),
            'init_ctn_ttl'      => object_get($carton, 'init_ctn_ttl'),
            'init_piece_ttl'    => object_get($carton, 'init_piece_ttl'),
            'dmg_ctns'          => object_get($carton, 'dmg_ctn_ttl'),
            'current_ctns'      => object_get($carton, 'current_ctn_ttl'),
            'current_pieces'    => object_get($carton, 'current_piece_ttl'),
            'created_at'        => date("m/d/Y H:i:s", strtotime(object_get($carton, 'created_at', null))),
            // 'plt_sts_code'      => object_get($carton, 'plt_sts'),
            // 'plt_sts_name'      => object_get($carton, 'plt_sts') ? Status::getByValue(object_get($carton, 'plt_sts'), 'PALLET_STATUS') : null,
            'loc_sts_code'      => empty($location) ? object_get($carton, 'loc_sts_code') : $location['loc_sts_code'],
            'loc_sts_name'      => empty($location) ? (
                                        object_get($carton, 'loc_sts_code') ? Status::getByValue(object_get($carton, 'loc_sts_code'), 'LOCATION_STATUS') : null
                                    ) : (Status::getByValue($location['loc_sts_code'], 'LOCATION_STATUS')),
            'lpn_num'           => object_get($carton, 'lpn_carton', null),
            'row'               => object_get($carton, 'row', null),
            'level'             => object_get($carton, 'level', null),
            'aisle'             => object_get($carton, 'aisle', null),
        ];
    }

}
