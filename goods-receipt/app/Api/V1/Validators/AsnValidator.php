<?php

namespace App\Api\V1\Validators;

class AsnValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id' => 'required|integer',
            'cus_id' => 'required|integer',
            "measurement_code" => 'required',
            //not require anymore
            //'exp_date'              => 'required|Date',

            'asn_dtl_ept_dt' => 'required|Date',
            'ctnr_num' => 'required',
            'details.*.dtl_sku' => 'required',
            'details.*.dtl_length' => 'required|numeric|max:999.99',
            'details.*.dtl_width' => 'required|numeric|max:999.99',
            'details.*.dtl_height' => ' required|numeric|max:999.99',
            'details.*.dtl_weight' => ' required|numeric|max:999.99',
            'details.*.dtl_pack' => ' required|integer|min:1',
            'details.*.dtl_uom_id' => 'required|integer|min:1',
            'details.*.dtl_po' => 'required|string',
            //'details.*.dtl_po_date' => 'required|Date',
            'details.*.dtl_ctn_ttl' => 'required|integer|min:1',
            'details.*.dtl_crs_doc' => 'integer|greater_equal_than_zero'
        ];

        $api->get('/event-reports/{userId:[0-9]+}',
            ['action' => "inboundEventTracking", 'uses' => 'EventTrackingController@report']);
    }

    /**
     * @return array
     */
    protected function messages()
    {
        return [
            'details.*.dtl_ctn_ttl.min' => 'Exp QTY must > 0'
        ];
    }
}
