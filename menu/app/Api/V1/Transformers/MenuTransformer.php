<?php

namespace App\Api\V1\Transformers;

use App\Menu;
use League\Fractal\TransformerAbstract;

/**
 * Class MenuTransformer
 *
 * @package App\Api\V1\Transformers
 */
class MenuTransformer extends TransformerAbstract
{
    /**
     * @param Menu $menu
     *
     * @return array
     */
    public function transform(Menu $menu)
    {
        return [
            'menu_id'         => $menu->menu_id,
            'menu_group_id'   => $menu->menu_group_id,
            'name'            => $menu->name,
            'description'     => $menu->description,
            'url'             => $menu->url,
            'target'          => $menu->target,
            'icon_class'      => $menu->icon_class,
            'permission_name' => $menu->permission_name,
            'display_order'   => $menu->display_order,
            'nodes'           => $menu->nodes,
        ];
    }
}