<?php

namespace App\Api\V3\Inbound\ScanCartons\Validators;


class InsertVirtualCartonValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            "asn_dtl_id" => 'required|integer',
            "ctns_rfid"  => 'required',
            // "asn_hdr_id" => 'required|integer',
            // "item_id"    => 'required|integer',
            // "ctnr_id"    => 'required|integer',
            //"gate_code" => 'required',
        ];
    }

}