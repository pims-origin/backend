<?php
namespace Repository;

use Auth;
use Helper\Logs;

class LogRepository extends AbstractRepository {

    public static $logActivity = [
        'login' => [
            'action'    =>  'login',
            'label' => 'login',
        ],
        'logout' => [
            'action'    =>  'logout',
            'label' => 'logout',
        ],
    ];


    public static function write($log, $data, $desc = '')
    {
        // bb = B2B, wh = WMS, scc = SCC
        $moduleType = 'bb.wms';
        // bb.buyer = Buyer, bb.sl: Salesperson, bb.wh : warehouse
        $system = 'wms';
        $payload = array(
            'log_date' => date('Y-m-d H:i:s'),
            'id_user' => "ID USER",
            'system' => $system,
            'module_type' => $moduleType,
            'action' => $log['action'],
            'description' => $desc ? $desc : $log['label'],
            'user_name' => "Test",
            'user_type' => 'WMS',
            'user_ip' => "xx.xx.xx.xx",
            'data' => [$log['action'] => $data],
        );

        Logs::log($payload);
    }

}
