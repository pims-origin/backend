<?php

namespace App\Api\V2\Outbound\Order\AssignCartonsWithBarcode\Models;


use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Utils\SelStr;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * @param EventTracking $model
     */
    public function __construct(EventTracking $model = null)
    {
        $this->model = ($model) ?: new EventTracking();
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function eventTracking($params)
    {
        // event tracking asn
        $this->refreshModel();

        return $this->create([
            'whs_id' => $params['whs_id'],
            'cus_id' => $params['cus_id'],
            'owner' => $params['owner'],
            'evt_code' => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info' => $params['info'],
        ]);
    }
}
