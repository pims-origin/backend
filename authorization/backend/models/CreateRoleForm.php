<?php
namespace backend\models;

use backend\models\Form;
use common\models\Authority;
use yii\helpers\VarDumper;
use Yii;
use yii\web\BadRequestHttpException;

class CreateRoleForm extends Form
{
    public $name;
    public $code;
    public $description;
    public $rule_name;
    public $data;
    public $created_at;
    public $updated_at;
    public $type;
    public $permissions = [];

    public function safeAttributes()
    {
        $attributes = parent::safeAttributes();

        return array_merge($attributes, [
            'permissions'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'name',
                'unique',
                'targetClass' => '\common\models\Authority',
                'message'     => 'The Role Name is existed'
            ],
            [['name'], 'string', 'max' => 64],
            [['code'], 'string', 'max' => 20],
            [
                'code',
                'unique',
                'targetClass'     => '\common\models\Authority',
                'targetAttribute' => 'code',
                'message'         => 'The Role Code is existed.'
            ],
            [['description'], 'safe'],
        ];
    }


    public function createRole()
    {
        if (!$this->validate()) {
            return false;
        }

        try {
            $transaction = Yii::$app->db->beginTransaction();

            $model = new Authority();
            $model->name = $this->name;
            $model->code = $this->code;
            $model->type = Authority::ROLE_TYPE;
            $model->description = $this->description;
            $model->rule_name = $this->rule_name;
            $model->data = $this->data;
            $model->created_at = time();
            $model->updated_at = time();
            $result = $model->save();

            $auth = Yii::$app->authManager;
            $parent = $auth->getRole($model->name);
            $permissions = $this->permissions;

            if (!$parent) {
                $transaction->rollBack();
                throw new BadRequestHttpException('Role not found');
            }

            foreach ($permissions as $permission) {
                $child = $auth->getPermission($permission);
                $auth->addChild($parent, $child);
            }

            $transaction->commit();
        } catch (Yii\base\Exception $ex) {
            $transaction->rollBack();
            throw new Yii\base\Exception($ex->getMessage() . 'Have Error In Create Role.', 404);
        }


        return $result;
    }
}
