<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ItemModel;
//use Ite
use App\Api\V1\Models\SystemUomModel;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Request as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Utils\Helper;
use Swagger\Annotations as SWG;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\File;
use App\Api\V1\Mail\ItemMaster;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class ImageController extends AbstractController
{

    /**
     * @param  $request->itm_img
     * @param  $request->required
     * @param  $request->sku
     * @return string           image file name
     */
    public function uploadItemImage(Request $request, $itemId)
    {
        $input = $request->getParsedBody();
        $file = $request->getUploadedFiles();

        // Image file upload
        if(!empty($file['itm_img'])){
            $path = 'uploads/';

            if(!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }

            $exts = last(explode('.', $file['itm_img']->getClientFilename()));
            $fileName = preg_replace('/[^A-Za-z0-9]/', '', $input['sku']).time().'.'.$exts;

            $file['itm_img']->moveTo($path . $fileName);

            DB::table('item')
            ->where('item_id', $itemId)
            ->update(['itm_img' => base64_encode($path.$fileName)]);

            // Send notification email
            //self::sendNotificationEmail($itemId);

            $res['data'] = [
                'message'   => "Update image successfully!",
            ];

            return response()->json($res, 201);
        }

        return $this->response->errorBadRequest("This file itm_img is required!");
    }

    public static function sendNotificationEmail($itemId, $isEdit = false)
    {
        $item = DB::table('item')
            ->where('item_id', $itemId)
            ->first();

        $sendToDB = DB::table('cus_meta')->where('cus_id', $item['cus_id'])->where('qualifier', 'EEX')->where('deleted', 0)->value('value');

        if(!empty($sendToDB)) {

            $sendToInfo = json_decode($sendToDB);

            $emailDB = DB::table('settings')->where('setting_key', 'EC')->value('setting_value');

            $emailConfig = json_decode($emailDB);

            config([
                'mail.host'         => $emailConfig->host,
                'mail.port'         => $emailConfig->port,
                'mail.encryption'   => $emailConfig->encryption,
                'mail.username'     => $emailConfig->username,
                'mail.password'     => $emailConfig->password,
                'mail.from.name'    => $emailConfig->name,
                'mail.from.address' => 'seldat-support@gmail.com'
            ]);

            $emailInfo = [
                'toAddress' =>  $sendToInfo->email,
                'toName'    =>  null,
                'subject'   =>  $isEdit ? 'Edit item notification' : 'New item notification'
            ];

            try {
                $item['link'] = env('IM_URL') . $item['item_id'] . '/edit';
                $item['is_edit'] = $isEdit;

                $attachFile = base64_decode($item['itm_img']);
                $filePath = public_path($attachFile);

                Mail::send('emails.item-master', ['data' => $item], function($message) use ($emailInfo, $filePath){
                    $message->to($emailInfo['toAddress'], $emailInfo['toName'])->subject($emailInfo['subject']);
                    $message->attach($filePath);
                });
            } catch (\Exception $e) {
                // return \Dingo\Api\Http\Response::errorBadRequest('Email is not sent!');
            }
        }
    }

    public function readItemImage($key){
        $content = base64_decode($key);

        $filePath = public_path($content);

        if(!File::exists($filePath)) {
            return $this->response->errorNotFound("File doesn't exist!");
        }

        header('Content-type: ' . $this->mimeType($filePath));
        readfile($filePath);
    }

    private function mimeType($path) {
        preg_match("|\.([a-z0-9]{2,4})$|i", $path, $fileSuffix);
        switch(strtolower($fileSuffix[1])) {
            case 'js' :
                return 'application/x-javascript';
            case 'json' :
                return 'application/json';
            case 'jpg' :
            case 'jpeg' :
            case 'jpe' :
                return 'image/jpg';
            case 'png' :
            case 'gif' :
            case 'bmp' :
            case 'tiff' :
                return 'image/'.strtolower($fileSuffix[1]);
            case 'css' :
                return 'text/css';
            case 'xml' :
                return 'application/xml';
            case 'doc' :
                return 'application/msword';
            case 'docx':
                return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            case 'xlsx':
                return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            case 'xls' :
            case 'xlt' :
            case 'xlm' :
            case 'xld' :
            case 'xla' :
            case 'xlc' :
            case 'xlw' :
            case 'xll' :
                return 'application/vnd.ms-excel';
            case 'ppt' :
            case 'pps' :
                return 'application/vnd.ms-powerpoint';
            case 'rtf' :
                return 'application/rtf';
            case 'pdf' :
                return 'application/pdf';
            case 'html' :
            case 'htm' :
            case 'php' :
                return 'text/html';
            case 'txt' :
                return 'text/plain';
            case 'mpeg' :
            case 'mpg' :
            case 'mpe' :
                return 'video/mpeg';
            case 'mp3' :
                return 'audio/mpeg3';
            case 'wav' :
                return 'audio/wav';
            case 'aiff' :
            case 'aif' :
                return 'audio/aiff';
            case 'avi' :
                return 'video/msvideo';
            case 'wmv' :
                return 'video/x-ms-wmv';
            case 'mov' :
                return 'video/quicktime';
            case 'zip' :
                return 'application/zip';
            case 'tar' :
                return 'application/x-tar';
            case 'swf' :
                return 'application/x-shockwave-flash';
            default :
                if(function_exists('mime_content_type')) {
                    $fileSuffix = mime_content_type($path);
                }
                return 'unknown/' . trim($fileSuffix[0], '.');
        }
    }
}
