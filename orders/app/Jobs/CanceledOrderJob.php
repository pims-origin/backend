<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;

class CanceledOrderJob extends IMSJob
{
    protected $orderId;

    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    public function handle()
    {
        try {
            $token = $this->login();

            $endpoint = env('IMS_API_ORDER');

            // Check IMS Configuration
            if (empty($endpoint)) {
                $this->writeLogment('IMS Canceled Order', 'IMS Canceled Order Endpoint is not define!');
                return false;
            }

            $orderHeader = DB::table('odr_hdr')
                ->where('odr_hdr.odr_id', $this->orderId)
                ->select([
                    'odr_id',
                    'cus_id',
                    'whs_id',
                    'odr_num',
                    'cus_odr_num',
                    'ref_cod',
                    'cus_po',
                    'odr_type',
                    'odr_sts',
                    'req_cmpl_dt',
                    'rush_odr',
                    'cus_notes',
                    'in_notes',
                    'created_at',
                    'routed_dt',
                    'csr_notes',
                    'ship_by_dt'
                ])
                ->first();

            $orderHeader = $this->transformData($orderHeader);

            $odrNum = array_get($orderHeader, 'order_num');

            $data = [
                'type'      => 'orders',
                'id'        => array_get($orderHeader, 'id'),
                'cus_id'    => array_get($orderHeader, 'cus_id'),
                'whs_id'    => array_get($orderHeader, 'whs_id'),
                'attributes' => [
                    'order_type'    => array_get($orderHeader, 'order_type'),
                    'order_num'     => array_get($orderHeader, 'order_num'),
                    'cus_order_num' => array_get($orderHeader, 'cus_odr_num'),
                    'po_num'        => array_get($orderHeader, 'po_num'),
                    'pick_ref'      => null,
                    'dept_ref'      => null,
                    'rush_priority' => array_get($orderHeader, 'rush_priority'),
                    'order_date'    => array_get($orderHeader, 'order_date'),
                    'ship_date'     => array_get($orderHeader, 'ship_date'),
                    'cancel_date'   => date('Y-m-d'),
                    'additional_information'  => null,
                    'special_instruction'     => null,
                    'meta' => [
                        'CPP' => null,
                        'INP' => null
                    ]
                ]
            ];

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-type: application/json"
            ];

            $endpoint = $endpoint . $odrNum . '/cancel';

            $result = $this->call($endpoint, 'POST', json_encode(['data' => $data]), $header);

            $this->writeLogment('IMS Canceled Order ' . $odrNum, 'URL: '. $endpoint .' --- Request: ' . json_encode(['data' => $data]) . ' --- Response: ' . $result);
        } catch (\Exception $e) {
            $this->writeLogment('IMS Canceled Order ' . $odrNum, $e->getMessage());
        }
    }

    public function transformData($orderHeader)
    {
        return [
            'id' => array_get($orderHeader, 'odr_id'),
            'cus_id' => array_get($orderHeader, 'cus_id'),
            'whs_id' => array_get($orderHeader, 'whs_id'),
            'order_num' => array_get($orderHeader, 'odr_num', null),
            'po_num' => array_get($orderHeader, 'cus_po', null),
            'cus_odr_num' => array_get($orderHeader, 'cus_odr_num', null),
            'ref_cod' => array_get($orderHeader, 'ref_cod', null),
            'order_type' => array_get($orderHeader, 'odr_type', null),
            'order_status' => array_get($orderHeader, 'odr_sts', null),
            'complete_date' => empty(array_get($orderHeader, 'req_cmpl_dt', null)) ? null : date('Y-m-d', array_get($orderHeader, 'req_cmpl_dt')),
            'rush_priority' => array_get($orderHeader, 'rush_odr', null),
            'wms_comment' => array_get($orderHeader, 'in_notes', null),
            'external_comment' => array_get($orderHeader, 'cus_notes', null),
            'order_date' => empty(array_get($orderHeader, 'created_at', null)) ? null : date('Y-m-d', array_get($orderHeader, 'created_at')),
            'customer_service_note' => array_get($orderHeader, 'csr_notes', null),
            'route_date' => empty(array_get($orderHeader, 'routed_dt', null)) ? null : date('Y-m-d', array_get($orderHeader, 'routed_dt')),
            'ship_date' => empty(array_get($orderHeader, 'ship_by_dt', null)) ? null : date('Y-m-d', array_get($orderHeader, 'ship_by_dt'))
        ];
    }
}