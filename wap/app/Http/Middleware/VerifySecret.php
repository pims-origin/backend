<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Exception;
use Namshi\JOSE\JWS;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Utils;
use App\Utils\JWTUtil;

class VerifySecret
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws JWTException
     * @throws TokenInvalidException
     */
    public function handle($request, Closure $next)
    {
        $token = JWTAuth::getToken();

        if (!$token) {
            $result = $this->returnError('A token is required');
            return new \Symfony\Component\HttpFoundation\JsonResponse($result);
        }

        try {
            $jws = JWS::load($token);
            $iat = JWTUtil::getPayloadValue('iat');
            $jti = JWTUtil::getPayloadValue('jti');
            $user_info = DB::table('last_login')->where('user_id', $jti)->first();
            $status = array_get($user_info, 'status');
            $loginAt = array_get($user_info, 'login_at', 0);

            if ($iat - $loginAt <> 0 || $status == '07') {
                $result = $this->returnError('Token has expired');
                return new \Symfony\Component\HttpFoundation\JsonResponse($result);
            }

        } catch (Exception $e) {
            $result = $this->returnError('Could not decode token: ' . $e->getMessage());
            return new \Symfony\Component\HttpFoundation\JsonResponse($result);
        }

        if (!$jws->verify(config('jwt.secret'), config('jwt.algo'))) {
            $result = $this->returnError('Token Signature could not be verified.');
            return new \Symfony\Component\HttpFoundation\JsonResponse($result);

        }

        if (Utils::timestamp(JWTUtil::getPayloadValue('exp'))->isPast()) {
            $result = $this->returnError('Token has expired');
            return new \Symfony\Component\HttpFoundation\JsonResponse($result);
        }

        return  $next($request);
        /*
        $response = $next($request);

        if (empty($response->getContent())) {
            return $response;
        }

        try {
            $content = \GuzzleHttp\json_decode($response->getContent(), true);
        } catch (\Exception $e) {
            return $response;
        }

        if (env('API_DEBUG')) {
            return $response;
        }

        if (!env('API_DEBUG') && -1 == $content['message'][0]['status_code']) {
            if(str_contains( $content['message'][0]['msg'], 'SQLSTATE')) {
                if (str_contains( $content['message'][0]['msg'], 'Deadlock')) {
                    $content['message'][0]['msg'] = 'The system is busy, please try again !';
                } else {
                    $content['message'][0]['msg'] = 'Data error, please contact WMS support or email to wms_support@seldatinc.com!';
                }
                $response->setContent(\GuzzleHttp\json_encode($content));
            }
        }

        return $response;
        */

    }

    public function returnError($message)
    {
        $arr = [
            'errors' => [
                "message" => $message,
                "status_code" => 200,
                "status" => false
            ]
        ];
        return $arr;
    }
}
