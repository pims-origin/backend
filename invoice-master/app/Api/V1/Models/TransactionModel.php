<?php

namespace App\Api\V1\Models;

use App\InvoiceTransaction;
use App\InvoiceHeader;
use Seldat\Wms2\Utils\SelStr;

class TransactionModel extends AbstractModel
{
    /**
     * Constructor.
     */

    public function __construct()
    {
        $this->model = new InvoiceTransaction();
    }

    public function search($input, $with = [], $limit = 10)
    {
        $query = $this->make($with);

        $query->when(!empty($input['inv_id']), function ($q) use ($input) {
            return $q->where('inv_id', $input['inv_id']);
        });

        $this->sortBuilder($query, $input);
        
        return $query->paginate($limit);
    }
    
    public function add($input = [])
    {
        $inv = InvoiceHeader::find($input['inv_id']);
        $input['inv_num'] = $inv->inv_num;
        $input['ref_num'] = $inv->ref_num;
        $input['inv_tran_num'] = '';
        $transaction = new InvoiceTransaction($input);
        if (!empty($inv)) {
            if($input['amount'] + $inv->paid_amount > $inv->total) return false;
            $inv->transaction()->save($transaction);
            $this->updateInvSts($input['inv_id']);
            return true;
        }
    }

    public function updateInvSts($invId)
    {
        $inv = InvoiceHeader::find($invId);
        if ($inv->total <= $inv->paid_amount) {
            $inv->inv_sts = 'PA';
            $inv->paid_dt = time();
            $inv->save();
        }
    }
}
