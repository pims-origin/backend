<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Transformers\ContainerTransformer;
use App\MyHelper;
use Psr\Http\Message\ServerRequestInterface as Request;
use Swagger\Annotations as SWG;
use App\Api\V1\Models\Log;

class ContainerController extends AbstractController
{
    /**
     * @var ContainerModel
     */
    protected $containerModel;


    /**
     * ContainerController constructor.
     *
     * @param ContainerModel $containerModel
     */
    public function __construct(ContainerModel $containerModel)
    {
        $this->containerModel = $containerModel;
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param ContainerTransformer $containerTransformer
     * @return \Dingo\Api\Http\Response|void
     */
    public function search($whsId, Request $request, ContainerTransformer $containerTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        /*
         * start logs
         */

        $url = "/whs/$whsId/containers";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'LCL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Load container list'
        ]);
        /*
         * end logs
         */
        
        try {
            $container = $this->containerModel->loadCTNRList($input, [], array_get($input, 'limit'));

            Log::respond($request, $whsId, [
                'evt_code' => 'LCL',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'response_data' => $this->response->collection($container, $containerTransformer)
            ]);

            return $this->response->collection($container, $containerTransformer);

        } catch (\PDOException $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return new Response($this->getResponseData(), 200, [], null);
        }
    }
}
