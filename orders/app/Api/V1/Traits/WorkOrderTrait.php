<?php

namespace App\Api\V1\Traits;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\ChargeCode;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

trait WorkOrderTrait
{
    /**
     * get Order info for work order by order number and customer id
     *
     * @param string $odrNum
     *
     * return array
     */
    public function getOrderInfo($cusId, $odrNum)
    {
        $usrInfo = new Data();
        $curWhs = $usrInfo->getCurrentWhs();

        $query = $this->make()
            ->leftJoin('users', 'users.user_id', '=', 'odr_hdr.csr')
            ->select([
                    'odr_type',
                    'ref_cod',
                    'odr_sts',
                    'csr',
                    'rush_odr',
                    'users.first_name',
                    'users.last_name',
                    'cus_odr_num',
                    'cus_po'
                ])
            ->where('odr_num', $odrNum)
            ->where('cus_id', $cusId)
            ->where('whs_id', $curWhs)
            ->where('odr_hdr.deleted', 0)
            ->first();

        return $query;
    }

    public function getSku($cusId, $odrNum)
    {
        $usrInfo = new Data();
        $curWhs = $usrInfo->getCurrentWhs();

        DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);
        if (empty($odrNum)) {
            $query = DB::table('item')
                ->select([
                    'item.item_id',
                    'item.sku',
                    'item.size',
                    'item.color',
                    DB::raw('SUM(invt_smr.avail) AS qty')
                ])
                ->leftJoin('invt_smr', 'item.item_id', '=', 'invt_smr.item_id')
                ->where('item.deleted', 0)
                ->where('invt_smr.item_id', '!=', null)
                ->where('item.cus_id', $cusId)
                ->where('invt_smr.whs_id', $curWhs)
                ->groupBy('item.sku', 'item.size', 'item.color');
        } else {

            $arrOdrPartials = "'" . implode("', '", array_keys(Status::get("PARTIAL-ORDER-STATUS"))) . "'";

            $query = DB::table('odr_hdr')
                ->select([
                    'odr_dtl.item_id',
                    'odr_dtl.sku',
                    'odr_dtl.size',
                    'odr_dtl.color',
                    'odr_dtl.lot',
                    DB::raw('SUM(odr_dtl.alloc_qty) AS qty')
                ])
                ->leftJoin('odr_dtl', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
                ->where('odr_hdr.deleted', 0)
                ->where('odr_dtl.deleted', 0)
                ->where('odr_dtl.odr_dtl_id', '!=', null)
                ->where('odr_hdr.cus_id', $cusId)
                ->where('odr_hdr.whs_id', $curWhs)
                ->where('odr_hdr.odr_num', $odrNum)
                ->groupBy('odr_dtl.sku', 'odr_dtl.size', 'odr_dtl.color');

        }

        return $query->get();
    }

    public function getCmpValue($cusId, $odrNum = false)
    {
        $usrInfo = new Data();
        $curWhs = $usrInfo->getCurrentWhs();

        DB::connection()->setFetchMode(\PDO::FETCH_KEY_PAIR | \PDO::FETCH_GROUP);

        if ($odrNum) {

            $arrOdrPartials = "'" . implode("', '", array_keys(Status::get("PARTIAL-ORDER-STATUS"))) . "'";

            $query = DB::table('odr_hdr')
                ->select([
                    'odr_dtl.item_id',
                    DB::raw('SUM(odr_dtl.alloc_qty) AS qty')
                ])
                ->leftJoin('odr_dtl', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
                ->where('odr_hdr.deleted', 0)
                ->where('odr_dtl.deleted', 0)
                ->where('odr_dtl.odr_dtl_id', '!=', null)
                ->where('odr_hdr.cus_id', $cusId)
                ->where('odr_hdr.whs_id', $curWhs)
                ->where('odr_hdr.odr_num', $odrNum)
                ->groupBy('odr_dtl.sku', 'odr_dtl.size', 'odr_dtl.color');
        } else {
            $query = DB::table('invt_smr')
                ->select(['item_id', 'qty' => 'avail']);
        }

        $return = $query->get('item_id');
        DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);

        return $return;
    }

    public function validatWoDtl($cmpValue, $itmId, $qty)
    {
        if (!isset($cmpValue[$itmId])) {
            return false;
        }

        if ($cmpValue[$itmId] < $qty) {
            return false;
        }

        return true;
    }
}