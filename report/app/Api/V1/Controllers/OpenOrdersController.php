<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Response as IlluminateResponse;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Transformers\OpenOrdersTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

use Carbon\Carbon;

class OpenOrdersController extends AbstractController
{
    public $orderHdrModel;

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->orderHdrModel = new OrderHdrModel();
        $this->transformer = new OpenOrdersTransformer();
    }

    /*
    ****************************************************************************
    */

    public function index(Request $request, $data=[])
    {
        $this->transformer->today = Carbon::today(TIME_ZONE)->timestamp;

        return parent::index($request, [
            'model' => $this->orderHdrModel,
            'method' => 'searchOpenOrders',
        ]);
    }

    /*
    ****************************************************************************
    */

    public function updateStatus($orderID, $userID, Request $request)
    {
        $input = $request->getParsedBody();

        try {

            $results = $this->orderHdrModel->updateOpenOrderStatus($orderID,
                    $userID, $input['value']);

            return $this->response->item($results, $this->transformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
    ****************************************************************************
    */

}
