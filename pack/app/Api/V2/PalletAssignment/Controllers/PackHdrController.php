<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V2\PalletAssignment\Controllers;


use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Transformers\ViewAssignPalletTransformer;
use App\Jobs\PalletAssignmentJob;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;

use App\Jobs\BOLJob;
use Wms2\UserInfo\Data;

/**
 * Class PackHdrController
 *
 * @package App\Api\V1\Controllers
 */
class PackHdrController extends AbstractController
{
    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**

    /**
     * @param PackHdrModel $packHdrModel
     */
    public function __construct(
        PackHdrModel $packHdrModel

    ) {
        $this->packHdrModel = $packHdrModel;
    }


    /**
     * @param Request $request
     * @param ViewAssignPalletTransformer $viewAssignPalletTransformer
     *
     * @return Response|mixed|void
     */
    public function assignPallet(
        $print,
        Request $request,
        ViewAssignPalletTransformer $viewAssignPalletTransformer
    ) {
        try {
            // get data from HTTP
            $input = $request->getParsedBody();
            foreach ($input['data'] as $data) {
                //Check order have not been packed yet...
                $odr_hdr_id = array_get($data, 'odr_id', 0);
                $packInfo = $this->packHdrModel->checkWhere(
                    [
                        'odr_hdr_id' => $odr_hdr_id
                    ]);
                if (empty($packInfo)) {
                    throw new \Exception(Message::get('BM130', 'This order', 'packed'));
                }
            }

            // dispatch(new PalletAssignmentJob($input['data'], $request));

            $whsID = Data::getCurrentWhsId();
            dispatch(new BOLJob($odr_hdr_id, $whsID, $request));

            return $this->response->noContent()
                ->setContent(['data' => ['message' => Message::get('BM129', 'Pallets assigned')]])
                ->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

}
