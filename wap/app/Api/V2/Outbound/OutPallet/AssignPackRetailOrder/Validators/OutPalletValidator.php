<?php

/**
 * Created by NetBeans.
 * User: Dao.TH
 * Date: 5/30/2017
 * Time: 1:42 PM
 */

namespace App\Api\V2\Outbound\OutPallet\AssignPackRetailOrder\Validators;

class OutPalletValidator {

    const PRE_FIX = 'LPN';

    const MIN_CHUNK = 4;

    const ERROR_MSG = 'Out pallet format is invalid!';

    /**
     * Validate out pallet format
     *
     * @param string $outPalletCode
     *
     * @return boolean
     */
    public static function validate($outPalletCode) {

        $codeArr = explode('-', $outPalletCode);
        if (count($codeArr) < self::MIN_CHUNK) {

            return false;
        }

        $first = array_shift($codeArr);
        if ($first !== self::PRE_FIX) {

            return false;
        }

        foreach ($codeArr as $code) {
            if (!is_numeric($code)) {

                return false;
            }
        }

        return true;
    }

}
