<?php

namespace App\Api\V1\Validators;

class PackRefValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'width'     => 'required',
            'height'    => 'required',
            'length'    => 'required',
            'pack_type' => 'required'
        ];
    }
}
