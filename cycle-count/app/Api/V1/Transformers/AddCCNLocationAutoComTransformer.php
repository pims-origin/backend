<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CycleHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\CycleCountNotification;

class AddCCNLocationAutoComTransformer extends TransformerAbstract
{
    /*
     * Transform \Seldat\Wms2\Models\Location
     * 
     * @param object $ccNotice
     * 
     * @return array
     */

    public function transform(\Seldat\Wms2\Models\Location $ccNotice)
    {
        return [
            'loc_id' => $ccNotice->loc_id,
            'loc_code' => $ccNotice->loc_code,
            'loc_whs_id' => $ccNotice->loc_whs_id
        ];
    }

}
