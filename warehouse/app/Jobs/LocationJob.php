<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Artisan;

class LocationJob extends Job
{
    /**
     * @var
     */
    protected $action;

    /**
     * @var
     */
    protected $hash;

    /**
     * @var
     */
    protected $offset;

    /**
     * @var
     */
    protected $limit;

    /**
     * LocationJob constructor.
     * @param $action
     * @param $hash
     * @param $offset
     * @param $limit
     */
    public function __construct($action, $hash, $offset = 0, $limit = 0)
    {
        $this->action = $action;
        $this->hash = $hash;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     *
     */
    public function handle()
    {
        Artisan::call("location:import", [
            'action' => $this->action,
            'hash' => $this->hash,
            'offset' => $this->offset,
            'limit' => $this->limit,
        ]);
    }
}