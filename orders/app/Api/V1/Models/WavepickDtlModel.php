<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 31-Oct-16
 * Time: 16:36
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\WavepickDtl;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderDtlModel
 *
 * @package App\Api\V1\Models
 */
class WavepickDtlModel extends AbstractModel
{
    /**
     * WavepickDtlModel constructor.
     *
     * @param WavepickDtl|null $model
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $wvIds
     *
     * @return mixed
     */
    public function countSku($wvIds)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->whereIn('wv_id', $wvIds)
            ->count(['item_id']);

        return $skuCount;
    }

    public function updatedDetailsPicked($wvId)
    {
        $query = $this->model
            ->where('wv_id', $wvId)
            ->where('deleted', 0)
            ->where('act_piece_qty', '>', 0)
            ->update([
                'wv_dtl_sts' => DB::raw('IF(wv_dtl.piece_qty <= wv_dtl.act_piece_qty, "PD", "PK")'),
            ]);

        return $query;
    }

}
