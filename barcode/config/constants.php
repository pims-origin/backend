<?php

return [
    'INBOUND'             => 'ib',
    'OUTBOUND'            => 'ob',
    'WAREHOUSE-OPERATING' => 'wo',
    'ACTIVE'              => 'AC',
    'INACTIVE'            => 'IA',
    'LOCKED'              => 'LK',
    'RESERVED'            => 'RS',
    'asn_prefix'          => 'ASN',
    'gr_prefix'           => 'GDR',
    'relocation_prefix'   => 'REL',
    'asn_status'          => [
        'NEW'          => 'NW',
        'RECEIVING'    => 'RG',
        'RCVD-PARTIAL' => 'RP',
        'COMPLETED'    => 'CO',
    ],
    'gr_status'           => [
        'RECEIVING'       => 'RG',
        'RECEIVED'        => 'RE',
        'RCVD-DISCREPANT' => 'RD',
    ],
    'item_status'         => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'location_status'     => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'ctn_status'          => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'event'               => [
        'ASN-NEW'          => "ANW",
        'ASN-RECEIVING'    => "ARV",
        'ASN-RCVD-PARTIAL' => "ARP",
        'ASN-COMPLETE'     => "ARC",
        'GR-RECEIVING'     => "GRP",
        'GR-EXCEPTION'     => "GRX",
        'GR-COMPLETE'      => "GRC",
        'GR-CANCEL'        => 'GRU',
        'PL-AWAY'          => "PUT",
        'REL-CTN-LOC'      => "LTL",
        'REL-LP-LOC'       => "LPL",
        'COMPLETE-RELOC'   => "RLC",
        'CON-CTN-LOC'      => "CTL",
        'CON-LP-LOC'       => "CTP",
        'COMPLETE-CON'     => "CNC",
    ],
    'event-info'          => [
        'ASN-NEW'          => "%s created",
        'ASN-RECEIVING'    => "%s receiving",
        'ASN-RCVD-PARTIAL' => "%s received partial",
        'ASN-COMPLETE'     => "%s received",
        'GR-RECEIVING'     => "Good Receipt started for %s",
        'GR-EXCEPTION'     => "% received partial",
        'GR-COMPLETE'      => "Goods receipt %s created for %s",
        'GR-CANCEL'        => '',
        'PL-AWAY'          => "Move to %s",
        'REL-CTN-LOC'      => "Change carton %s location from %s to %s",
        'REL-LP-LOC'       => "Change License Plate %s location from %s to %s",
        'COMPLETE-RELOC'   => "Complete Relocation %s",
        'CON-CTN-LOC'      => "Change carton %s location from %s to %s",
        'CON-LP-LOC'       => "Change carton %s licence plate from %s to %s",
        'COMPLETE-CON'     => "Complete Consolidation %s",

    ],
];