<?php

namespace App\Api\Ecom\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WavepickHdr;

class PackingTransformer extends TransformerAbstract
{
    public function transform(WavepickHdr $wvHdr)
    {
        return [
            'wv_id'   => object_get($wvHdr, 'wv_id'),
            'wv_num'  => object_get($wvHdr, 'wv_num'),
            'odr_id'  => object_get($wvHdr, 'odr_id'),
            'odr_num' => object_get($wvHdr, 'odr_num'),
        ];
    }
}
