<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class DailyReportCorrection extends AbstractCommand
{
    protected $signature   = 'daily:report:correction {whsId} {cusId?} {fromday?} {today?}';
    protected $description = 'Daily Report Correction.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {
            if ($this->confirm('Do you wish to continue? [y|N]')) {
                $this->info('Command is running.' . PHP_EOL);

                $fromday = $this->argument('fromday') ? strtotime($this->argument('fromday')) : false;

                $today = $this->argument('today') ? strtotime($this->argument('today')) : false;

                $whsId = $this->argument('whsId');

                $cusId = $this->argument('cusId') ?: false;

                if (!$fromday || !$today) {
                    return;
                }

                $days = $this->dateFromTo($fromday, $today);

                if (count($days) < 1) {
                    return;
                }

                $customerWhss = DB::table('customer_warehouse AS cw')
                    ->select('cw.cus_id', 'cw.whs_id')
                    ->join('customer as c', 'c.cus_id', '=', 'cw.cus_id')
                    ->when($whsId, function ($q) use ($whsId) {
                        return $q->where('cw.whs_id', $whsId);
                    })
                    ->when($cusId, function ($q) use ($cusId) {
                        return $q->where('cw.cus_id', $cusId);
                    })
                    ->where("c.deleted", 0)
                    ->where("cw.deleted", 0)
                    ->get();
                if ($customerWhss) {
                    $progress = $this->output->createProgressBar(count($customerWhss));
                    foreach ($customerWhss as $cusWhs) {
                        $this->processTask($days, $cusWhs);
                        $progress->advance();
                    }
                    $progress->finish();
                    $this->info(PHP_EOL);
                } else {
                    $this->info('Ware house or customer does\'t exist.' . PHP_EOL);
                }

                $this->info('Command stopped.' . PHP_EOL);
            }
        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }
    }

    private function dateFromTo($from, $to)
    {
        while ($from < $to) {
            $arr[] = [
                'today'     => $from,
                'yesterday' => $from - 86400,
            ];
            $from += 86400;
        }

        return $arr ?? [];
    }

    private function processTask($days, $cusWhs)
    {
        foreach ($days as $value) {
            //Calculator yesterday
            $this->generateSql('update_daily_inv_rpt', $cusWhs['whs_id'], $cusWhs['cus_id'], $value['yesterday']);

            $this->generateSql('update_daily_plt_rpt', $cusWhs['whs_id'], $cusWhs['cus_id'], $value['yesterday']);

            //Insert today
            $this->generateSql('insert_daily_inv_rpt', $cusWhs['whs_id'], $cusWhs['cus_id'], $value['today']);

            $this->generateSql('insert_daily_plt_rpt', $cusWhs['whs_id'], $cusWhs['cus_id'], $value['today']);
        }
    }

    private function generateSql($procedure, int $whsId, int $cusId, int $date)
    {
        return DB::statement(sprintf("CALL %s(%d, %d, %d)", $procedure, $whsId, $cusId, $date));
    }
}
