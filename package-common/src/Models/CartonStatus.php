<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace Seldat\Wms2\Models;

class CartonStatus extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ctn_status';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'status';

    /**
     * @var array
     */
    protected $fillable = [
        'ctn_sts_name',
        'ctn_sts_des'
    ];

}
