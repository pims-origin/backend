<?php

namespace App\Modules\Customers\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\Customers\Resources\CustomerDetailResource;

class WarehouseResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
            'type' => 'warehouses',
            'id' => (int) $this->whs_id,
            'attributes' => [
                'code'          => $this->whs_code,
                'name'          => $this->whs_name
            ]
        ];
    }
}