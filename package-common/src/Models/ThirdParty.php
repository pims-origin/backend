<?php
namespace Seldat\Wms2\Models;

/**
 * Class ThirdParty
 *
 * @package Seldat\Wms2\Models
 */
class ThirdParty extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'third_party';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'tp_id';
    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'type',
        'name',
        'des',
        'phone',
        'mobile',
        'ct_first_name',
        'ct_last_name',
        'longtitude',
        'latitude',
        'add_1',
        'add_2',
        'city',
        'state',
        'zip',
        'country'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }
}
