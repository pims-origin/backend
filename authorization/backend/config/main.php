<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => ['log'],
    'modules'             => [],
    'components'          => [
        'httpclient'   => [
            'class'          => 'understeam\httpclient\Client',
            'detectMimeType' => false,
            'requestOptions' => [
                'exceptions' => false,
                'verify'     => false
            ],
            'requestHeaders' => [
                // specify global request headers (can be overrided with $options on making request)
            ],
        ],
        'user'         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => false,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'   => [
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => false,
            'showScriptName'      => false,
            'rules'               => [
                'POST authorize'                    => 'authorize/check',
                'POST assign-role-to-user'          => 'authorize/assign-role-to-user',
                'GET menu'                          => 'authorize/menu',
                'GET permissions'                   => 'permission/list',
                'POST permissions'                  => 'permission/create',
                'GET users/<id>/permissions'        => 'permission/by-user',
                'GET users/<id>/groups'             => 'permission/groups',
                'GET users/csr'                     => 'permission/get-csr-user',
                'GET users/picker'                  => 'permission/get-picker-user',
                'GET users/putter'                  => 'permission/get-putter-user',
                'GET users/put-backer'              => 'permission/get-put-backer-user',
                'GET permission/<permission>/users' => 'permission/users',
                'GET roles/<roleName>'              => 'role/view',
                'GET roles/<roleName>/permissions'  => 'role/permissions',
                'POST roles/<roleName>/permissions' => 'role/assign-permission',
                'GET users/<id>/roles'              => 'role/by-user',
                'POST users/<id>/roles'             => 'role/assign-to-user',
                'PUT roles'                         => 'role/update',
                'POST roles'                        => 'role/create',
                'GET roles/<roleName>/users'        => 'role/users',
                [
                    'class'      => 'yii\rest\UrlRule',
                    'controller' => 'role',
                ],
                'POST roles/delete'                 => 'role/deletes'
            ],
        ],
        'request'      => [
            'enableCsrfValidation' => false,
            'parsers'              => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response'     => [
            'class'   => 'yii\web\Response',
            'format'  => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
    ],
    'params'              => $params,
];
