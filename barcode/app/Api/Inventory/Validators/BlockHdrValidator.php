<?php

namespace App\Api\Inventory\Validators;


class BlockHdrValidator extends AbstractValidator
{
    /**
     * @return array
     */

    protected function rules()
    {
        return [
            'whs_id'       => 'required|exists:warehouse,whs_id',
            'block_rsn_id'   => 'required|exists:block_rsn,block_rsn_id',
            'block_type'     => 'required|max:3',
            'block_detail' => 'required',
        ];
    }
}
