<?php

namespace App\Api\V2\Putback\Models;

use Seldat\Wms2\Models\OrderHdrMeta;

class OdrHdrMetaModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    protected  $odrHdrMeta;
    public function __construct(OrderHdrMeta $model = null)
    {
        $this->model = ($model) ?: new OrderHdrMeta();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function insertOdrHdrMeta($data){
        return $this->model->create($data);
    }

}
