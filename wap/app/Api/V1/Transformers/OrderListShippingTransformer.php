<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\OrderDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

class OrderListShippingTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $totalSKU = (new OrderDtlModel())->countSKUInOdr($orderHdr->odr_id);
        return [
            'odr_id'       => $orderHdr->odr_id,
            'cus_id'       => $orderHdr->cus_id,
            'odr_num'      => $orderHdr->odr_num,
            'cus_name'     => $orderHdr->cus_name,
            'whs_id'       => $orderHdr->whs_id,
            'odr_sts'      => $orderHdr->odr_sts,
            'odr_sts_name' => Status::getByKey('ORDER-STATUS', $orderHdr->odr_sts),
            'created_at'   => $orderHdr->created_at->format('m/d/Y'),
            'total-sku'    => $totalSKU,
        ];

    }
}
