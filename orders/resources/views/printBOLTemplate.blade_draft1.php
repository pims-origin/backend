<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>BOL pdf</title>
</head>
<body style="color: #000; font-size: 12px; font-family: Arial">
<div style="margin: 0 auto;">
    <table style="width: 100%; background-color: #fff; padding: 0; border: 1px solid #000;
    border-spacing: 0;">
        <tbody>
        <tr>
            <td style="text-align: left;border-bottom: 1px solid #000;padding: 5px; width: 20%">
                Date: {{date('m/d/Y', time())}}</td>
            <td colspan="2"
                style="text-align: center;text-transform: uppercase; font-weight: bold; font-size: 18px;
                border-bottom: 1px solid #000;padding: 5px; width: 60%">
                bill of lading
            </td>
            <td style="text-align: right;border-bottom: 1px solid #000;padding: 5px; width: 20%">Page 1</td>
        </tr>
        <tr>
            <td colspan="2" style="width: 50%; padding: 0; margin: 0;vertical-align: top">
                <table style="width: 100%; padding: 0; margin: 0; border-right: 1px solid #000;
                border-spacing: 0">
                    <thead>
                    <tr>
                        <th colspan="2"
                            style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff; font-size: 18px;font-weight: 500; padding: 2.5px">
                            ship from
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width: 30%; padding: 5px">Name:</td>
                        <td style="font-style: italic">{{$shipment['ship_from_name']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Address:</td>
                        <td style="font-style: italic">{{$shipment['ship_from_addr_1']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">City/Sate/Zip:</td>
                        <td style="font-style: italic">{{$shipment['ship_from_city'].", ".(array_get($shipment,'ship_from_state', null))
                        .","
                        .$shipment['ship_from_zip']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">SID#: {{$shipment['ship_from_sid']}}</td>
                        <td style="text-align: right;padding: 10px; font-style: italic">FOB:
                            <input type="checkbox"
                                   style="padding-top: 5px" {{$shipment['ship_from_fob']?"checked=\"checked\"":""}}>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 100%; padding: 0; margin: 0;border-right: 1px solid #000;
                border-spacing: 0">
                    <thead>
                    <tr>
                        <th colspan="2"
                            style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff; font-size: 18px;font-weight: 500; padding: 2.5px">
                            ship to
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width: 30%; padding: 5px">Name:</td>
                        <td>{{$shipment['ship_to_name']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Address:</td>
                        <td>{{$shipment['ship_to_addr_1']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">City/Sate/Zip:</td>
                        <td>{{$shipment['ship_to_city'].", ".(array_get($shipment,'ship_from_state', null)).", ".$shipment['ship_to_zip']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">TEL#: {{$shipment['ship_to_tel']}}</td>
                        <td style="text-align: right; padding: 5px">FOB:
                            <input type="checkbox"
                                   style="padding-top: 5px" {{$shipment['ship_to_fob']?"checked=\"checked\"":""}}>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 100%; padding: 0; margin: 0;border-right: 1px solid #000; border-bottom: 1px solid
                       #000;
                       border-spacing: 0">
                    <thead>
                    <tr>
                        <th colspan="2"
                            style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff; font-size: 18px;font-weight: 500; padding: 2.5px">
                            third party freight charges bill to
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width: 30%; padding: 5px">Name:</td>
                        <td>{{$shipment['bill_to_name']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Address:</td>
                        <td>{{$shipment['bill_to_addr_1']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">City/Sate/Zip:</td>
                        <td>{{$shipment['bill_to_city'].", ".(array_get($shipment,'bill_to_state', null)).", ".$shipment['bill_to_zip']}}</td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 100%; padding: 0; margin: 0;border-right:1px solid #000 ">
                    <tbody>
                    <tr>
                        <td style="text-transform: uppercase; padding: 10px">special instructions:</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px; text-transform: uppercase">{{$shipment['special_inst']}}</td>
                    </tr>
                    <tr style="height: 30px">
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="2" style="width: 50%; margin: 0; vertical-align: top">
                <table style="width: 100%; padding: 0; margin: 0;border-bottom: 1px solid #000">
                    <tbody>
                    <tr>
                        <td style="padding-left: 5px; width: 50%">Bill of Lading Number:</td>
                        <td style="font-weight: bold">{{$shipment['bo_label']}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 100%; padding: 0; margin: 0;border-bottom: 1px solid #333">
                    <tbody>
                    <tr>
                        <td style="text-transform: uppercase; padding: 5px; width: 40%">carrier name:</td>
                        <td style="font-style: italic">{{$shipment['carrier']}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Trailer Number:</td>
                        <td style="font-style: italic">{{$shipment['trailer_num']}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Seal Number(s):</td>
                        <td>{{$shipment['seal_num']}}</td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 100%; padding: 0; margin: 0;border-bottom: 1px solid #333">
                    <tbody>
                    <tr>
                        <td style="text-transform: uppercase;padding: 5px; width: 40%">scac:</td>
                        <td style="text-transform: uppercase; padding: 5px; font-style: italic">{{$shipment['scac']}}</td>
                    </tr>
                    <tr>
                        <td style="padding-left:5px">Pro Number:</td>
                        <td style="font-style: italic">{{$shipment['pro_num']}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 100%; padding: 0; margin: 0;border-bottom: 1px solid #333">
                    <tbody>
                    <tr>
                        <td colspan="3" style="font-size:10px; padding-bottom: 25px">Freight Charge Terms: (freight
                            charges are prepaid unless marked otherwise)
                        </td>
                    </tr>
                    <tr>
                        <td style="font-style: italic">
                            <input type="checkbox" {{$shipment['freight_charge_terms'] == "PR"?"checked=\"checked\"":""}}>Prepaid
                        </td>
                        <td style="font-style: italic">
                            <input type="checkbox" {{$shipment['freight_charge_terms'] == "CO"?"checked=\"checked\"":""}}>Collect
                        </td>
                        <td style="font-style: italic">
                            <input type="checkbox" {{$shipment['freight_charge_terms'] == "3P"?"checked=\"checked\"":""}}>3rdParty
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 100%">
                    <tbody>
                    <tr>
                        <td style="text-align: center; border-right: 1px solid #000; width: 30%">
                            <p style="text-align: center">
                                <input type="checkbox" {{$shipment['is_attach']?"checked=\"checked\"":""}}>
                            </p>

                            <p>(checkbox)</p>
                        </td>
                        <td style="vertical-align: top; padding: 11px">Master Bill of Lading with attached underlying
                            Bills of Lading
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table style="width: 100%; border-spacing: 0">
                    <thead>
                    <tr>
                        <td colspan="9" style="background-color: #000;">
                            <h3 style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff; font-size: 18px;font-weight: 500;margin: 0; padding: 2.5px;">
                                customer order information</h3>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                            PO#
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                            # PKGS
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                            UNITS
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                            WEIGHT
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom:
                         1px solid #000">
                            DEPT #
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom:
                         1px solid #000">
                            PLTS
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom:
                         1px solid #000">
                            PICK TICKET
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom:
                         1px solid #000">
                            ORDER REF #
                        </th>
                        <th style="text-align: center; font-weight: 300;border-bottom: 1px solid #000">
                            ADDITIONAL
                            SHIPPER INFO
                        </th>
                    </tr>
                    @php
                    $pkgs = 0;
                    $units = 0;
                    $weight = 0;
                    @endphp
                    @foreach ($shipment['bol_odr_dtl'] as $detail)
                        @php
                        $pkgs += $detail['ctn_qty'];
                        $units += $detail['piece_qty'];
                        $weight += $detail['weight'];
                        @endphp
                        <tr>
                            <td style="padding: 2.5px; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$detail['cus_po']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$detail['ctn_qty']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$detail['piece_qty']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$detail['weight']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$detail['cus_dept']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$detail['plt_qty']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$detail['cus_ticket']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$detail['cus_odr']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-bottom: 1px solid #000">
                                {{$detail['add_shipper_info']}}
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td style="padding: 2.5px; font-weight: 300; border-right: 1px solid #000;">GRAD TOTAL</td>
                        <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$pkgs}}</td>
                        <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$units}}</td>
                        <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$weight}}</td>
                        <td colspan="5" style="background-color: #7d7d7d"></td>
                    </tr>
                    </thead>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table style="border: 1px solid #000; border-spacing: 0; width:100%;">
                    <thead>
                    <tr>
                        <td colspan="9" style="background-color: #000">
                            <h3
                                    style="text-align: center; text-transform: uppercase; color: #fff; font-size: 18px;font-weight: 500;margin: 0; padding: 2.5px">
                                carrier information</h3>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2"
                            style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase; white-space: nowrap">
                            handling unit
                        </th>
                        <th colspan="2"
                            style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">
                            package
                        </th>
                        <th rowspan="2"
                            style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase">
                            weight
                        </th>
                        <th rowspan="2"
                            style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase">
                            H.M.<br>(X)
                        </th>
                        <th style="padding:2.5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase">
                            commodity description
                        </th>
                        <th colspan="2"
                            style="padding:2.5px;text-align: center; font-weight: 300; border-bottom: 1px solid #000;text-transform: uppercase">
                            ltl only
                        </th>
                    </tr>
                    <tr>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">
                            qty
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">
                            type
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">
                            qty
                        </th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">
                            type
                        </th>
                        <th style="padding: 5px; font-size: 12px; font-weight: 500; text-align: left;border-right: 1px solid #000; border-bottom: 1px solid #000;">
                            Commodities requiring special or additional care orattention in handling or stowing must
                            be
                            so marked andpackages as ensure sage transportation with ordinarycare.See Section 2(e)
                            of
                            nmfc Item 360
                        </th>
                        <th style="padding:5px; text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase;white-space: nowrap">
                            nmfc #
                        </th>
                        <th style="padding:5px; text-align: center; font-weight: 300; border-bottom: 1px solid #000; text-transform: uppercase">
                            class
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{(int)array_get($shipment, 'bol_carrier_dtl.hdl_qty', null)}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{array_get($shipment, 'bol_carrier_dtl.hdl_type', null)}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{(int)array_get($shipment, 'bol_carrier_dtl.pkg_qty', null)}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{array_get($shipment, 'bol_carrier_dtl.pkg_type', null)}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{(float)array_get($shipment, 'bol_carrier_dtl.weight', null)}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">{{!empty($shipment['bol_carrier_dtl']['hm']) && $shipment['bol_carrier_dtl']['hm'] == 1?'x':''}}</td>
                        <td style="text-transform:uppercase;padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{array_get($shipment, 'bol_carrier_dtl.cmd_des', null)}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{array_get($shipment, 'bol_carrier_dtl.nmfc_num', null)}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-bottom: 1px solid #000;
                        font-size: 18px">
                            {{array_get($shipment, 'bol_carrier_dtl.cls_num', null)}}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{(int)array_get($shipment, 'bol_carrier_dtl.hdl_qty', null)}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px; background-color: #7d7d7d"></td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{$shipment['ctn_qty_ttl']}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px;background-color: #7d7d7d"></td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            {{$shipment['weight_ttl']}}
                        </td>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px;background-color: #7d7d7d"></td>
                        <td style="text-transform:uppercase;padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 18px">
                            GRAND TOTAL
                        </td>
                        <td colspan="2"
                            style="padding:5px;text-align: center; font-weight: 300;border-bottom: 1px solid #000;font-size: 18px; background-color: #7d7d7d"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2"
                style="border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 5px; text-align: center">
                <p style="margin: 0; font-size: 14px"> Where the rate is dependent on value, shippers are required to
                    state
                    specially in writing the agreed or declared value of the property as follows:</p>

                <p style="margin: 0; font-size: 14px">"The agreed or declared value of the property is specially stated
                    by the
                    shipper to be not exceeding __________ per __________"</p>
            </td>
            <td colspan="2" style="border-bottom: 1px solid #000; padding: 5px; vertical-align: top">
                <p style="font-size: 18px; margin: 5px">COD Amount ${{!empty($shipment['freight_charge_cost'])
                        ?$shipment['freight_charge_cost']:"_________________"}}</p>

                <p style="font-size: 18px; margin: 5px">Fee terms:
                    <input type="checkbox" {{$shipment['freight_charge_terms'] == "CO"?"checked=\"checked\"":""}}>Collect
                    <input type="checkbox" {{$shipment['freight_charge_terms'] == "PR"?"checked=\"checked\"":""}}>Prepaid
                </p>

                <p style="font-size: 18px;padding-left: 30px; margin: 5px">Customer check acceptable <input
                            type="checkbox" {{$shipment['cus_accept']?"checked=\"checked\"":""}}></p>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="border-bottom: 1px solid #000; font-size: 18px;padding: 5px">NOTE: Liability
                Limitation for loss or damage in this shipment may be applicable. See 49 U.S.C.
                - 14706(c)(1)(A) and (B)
            </td>
        </tr>
        <tr>
            <td colspan="2"
                style="border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 5px; text-align: center">
                <p style="margin: 0; font-size: 14px"> RECEIVED, subject to individually determined rates or contracts
                    that have been
                    agreed upon in writing between the carrier and shipper, otherwise to the
                    rates,classifications and rules that have been established by the carrier and are
                    available to the shipper, on request, and to all applicable states and federal regulations</p>
            </td>
            <td colspan="2" style="border-bottom: 1px solid #000; padding-left: 5px; vertical-align: top ">
                The carrier shall not make delivery of this shipment without payment
                of freight and all other lawful charges<br>

                <p style="margin-top: 45px; margin-bottom: 0">_____________________________ Shipper Signature</p>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table style="border-spacing: 0">
                    <tbody>
                    <tr>
                        <td style="width: 35%; border-left: 1px solid #000; vertical-align: top; padding-left: 2.5px">
                            SHIPPER SIGNATURE / DATE
                            <p style="margin: 0">This is to certify that the above named materials
                                are properly classified, packaged, marked and
                                labeled, and are proper condition for transportation
                                according to the applicable regulations of the DOT.</p>

                            <p style="margin-top: 16px; margin-bottom: 0">_____________________ / _____________</p>
                        </td>
                        <td style="width: 30%; border-left: 1px solid #000; vertical-align: top">
                            <table style="width: 100%">
                                <thead>
                                <tr>
                                    <th style="font-weight: 500; font-size: 18px">Trailer Loaded</th>
                                    <th style="font-weight: 500;font-size: 18px">Freight Counted</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td style="width: 40%">
                                        <p style="margin: 0; font-size: 16px;"><input type="checkbox"
                                                    {{$shipment['trailer_loaded_by']=="BS"?"checked=\"checked\"":"" }}>By
                                            Shipper
                                        </p>

                                        <p style="margin: 0; font-size: 16px;"><input type="checkbox"
                                                    {{$shipment['trailer_loaded_by']=="BD"?"checked=\"checked\"":"" }}>By
                                            Driver</p>
                                    </td>
                                    <td>
                                        <p style="margin: 0; font-size: 16px;"><input type="checkbox"
                                                    {{$shipment['freight_counted_by']=="BS"?"checked=\"checked\"":"" }}>By
                                            Shipper
                                        </p>

                                        <p style="margin: 0; font-size: 16px;"><input type="checkbox"
                                                    {{$shipment['freight_counted_by']=="BD"?"checked=\"checked\"":"" }}>
                                            By Driver/pallets said to contain
                                        </p>

                                        <p style="margin: 0; font-size: 16px;"><input type="checkbox"
                                                    {{$shipment['freight_counted_by']=="BP"?"checked=\"checked\"":"" }}>
                                            By Driver/Pieces</p>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td style="width: 35%; border-left: 1px solid #000; vertical-align: top; padding-left: 2.5px">
                            CARRIER SIGNATURE / PICKUP DATE
                            <p style="margin: 0; font-size: 12px">Carrier acknowledges receipt of packaged and
                                required placards. Carrier certifies emergency response
                                information was made available and/or carrier has the
                                U.S. DOT emergency response guidebook or equivalent
                                documentation in the vehicle.</p>

                            <p style="margin-top: 10px; margin-bottom: 0">_____________________ / _____________</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
