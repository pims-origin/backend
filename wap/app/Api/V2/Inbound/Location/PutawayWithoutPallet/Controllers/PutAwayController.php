<?php

namespace App\Api\V2\Inbound\Location\PutawayWithoutPallet\Controllers;

use App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models\AsnDtlModel;
use App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models\CartonModel;
use App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models\EventTrackingModel;
use App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models\GoodsReceiptDetailModel;
use App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models\GoodsReceiptModel;
use App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models\LocationModel;
use App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models\PalletModel;
use App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models\Log;
use App\libraries\RFIDValidate;

use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\MyHelper;

class PutAwayController extends AbstractController
{

    protected $cartonModel;
    protected $locationModel;
    protected $palletModel;
    protected $eventTrackingModel;

    /**
     * PutAwayController constructor.
     *
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     */
    public function __construct() {
        $this->cartonModel        = new CartonModel();
        $this->palletModel        = new PalletModel();
        $this->locationModel      = new LocationModel();
        $this->eventTrackingModel = new EventTrackingModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function putAway($whsId, Request $request)
    {
        $url = "/v2/whs/{$whsId}/location/rack/put-cartons";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'PCR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Put Cartons to RACK'
        ]);

        $input = $request->getParsedBody();

        $type     = (int)array_get($input, 'type');
        $location = array_get($input, 'location');
        $ctnRfids = array_get($input, 'ctn_rfids');
        $cusId    = (int)array_get($input, 'cus_id');

        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }

        //Check existed for The loc RFID
        $locationInfo = '';
        if ($type == 1) {
            $locationInfo = $this->locationModel->getRackLocationByLocRFID($whsId, $input['location']);
        } else {
            $locationInfo = $this->locationModel->getFirstWhere([
                                'loc_whs_id' => $whsId,
                                'loc_code'   => $input['location']]);
        }
        if (empty($locationInfo)) {
            $msg = sprintf("The location %s doesn't exist!", $input['location']);
            return $this->_responseErrorMessage($msg);
        }

        // if ($locationInfo && (!$locationInfo->rfid || $locationInfo->rfid == '')) {
        //     $msg = sprintf("Location %s does not be Rfid",
        //         $locationInfo->loc_code);
        //     return $this->_responseErrorMessage($msg);
        // }

        $location = $this->locationModel->getRackLocationByCustomerById($whsId, $locationInfo->loc_id, $cusId);

        if (empty($location)) {
            $msg = sprintf("The location %s does not belong to this customer.", $input['location']);
            return $this->_responseErrorMessage($msg);
        } elseif ($location->loc_sts_code != 'AC') {
            $msg = sprintf("Location %s is not active. Current Status %s",
                $location->loc_code,
                $location->loc_sts_code);
            return $this->_responseErrorMessage($msg);
        }

        $palletObj = $this->palletModel->getModel()
            ->where('loc_id', $location->loc_id)
            ->whereNotIn('plt_sts', ['CC', 'AJ'])
            ->first();

        $userId = Data::getCurrentUserId();

        $returnData = [];
        try {
            DB::beginTransaction();

            $ctnsTmp = [];
            $grHdrIds = [];
            foreach ($ctnRfids as $ctnRfid) {
                $ctnObj = $this->cartonModel->getFirstWhere(['rfid' => $ctnRfid]);
                if (!$ctnObj) {
                    $msg = sprintf("Carton RFID %s do not exist. Please create Goods Receipt first.", $ctnRfid);
                    return $this->_responseErrorMessage($msg);
                }

                if ($ctnObj && $ctnObj->whs_id != $whsId) {
                    $msg = sprintf("Carton RFID %s do not belong to current warehouse.", $ctnRfid);
                    return $this->_responseErrorMessage($msg);
                }

                if ($ctnObj && $ctnObj->cus_id != $cusId) {
                    $msg = sprintf("Carton RFID %s do not belong to customer.", $ctnRfid);
                    return $this->_responseErrorMessage($msg);
                }

                if ($ctnObj && $ctnObj->loc_id != null) {
                    $msg = sprintf("Carton RFID %s already put on a location.", $ctnRfid);
                    return $this->_responseErrorMessage($msg);
                }

                if (!in_array($ctnObj->gr_hdr_id, $grHdrIds)) {
                    $ctnsTmp[] = $ctnObj;
                }
            }

            // create a virtual pallet
            if (!$palletObj) {
                // $pltNum = $this->palletModel->createPalletNum();
                $pltNum = $this->createPalletNum();
                $data = [
                    'cus_id'       => $cusId,
                    'whs_id'       => $whsId,
                    'plt_num'      => $pltNum,
                    'rfid'         => null,
                    'created_at'   => time(),
                    'created_by'   => $userId,
                    'updated_at'   => time(),
                    'updated_by'   => $userId,
                    'deleted_at'   => 915148800,
                    'deleted'      => 0,
                    'ctn_ttl'      => 0,
                    'dmg_ttl'      => 0,
                    'init_ctn_ttl' => 0,
                    'plt_sts'      => 'RG',
                    'loc_id'       => $location->loc_id,
                    'loc_code'     => $location->loc_code,
                    'loc_name'     => $location->loc_alternative_name,

                ];

                $palletObj = $this->palletModel->create($data);
            }
            $this->cartonModel->getModel()
                ->whereIn('rfid', $ctnRfids)
                ->update([
                    'plt_id'        => $palletObj->plt_id,
                    'loc_id'        => $location->loc_id,
                    'loc_code'      => $location->loc_code,
                    'loc_name'      => $location->loc_alternative_name,
                    'loc_type_code' => Status::getByValue('RACK', 'LOC_TYPE_CODE'),
                    'updated_at'    => time(),
                    'updated_by'    => $userId,
                ]);

            $this->palletModel->updatePalletCtnTtlByPltId($palletObj->plt_id);

            // turn on putaway when enough pallets have put on rack
            // $this->_turnOnPutAway($pallet->gr_hdr_id);

            //unlock this location
            $this->locationModel->updateWhere(
                [
                    'loc_sts_code' => Status::getByKey('LOCATION_STATUS', 'ACTIVE'),
                ],
                [
                    'loc_id' => $location->loc_id
                ]
            );

            //write even tracking
            // $dataEvt = [
            //     'whs_id'    => object_get($vtlObj, 'whs_id', null),
            //     'cus_id'    => object_get($vtlObj, 'cus_id', null),
            //     'owner'     => object_get($vtlObj, 'gr_hdr_num', ''),
            //     'evt_code'  => 'PUT',
            //     'trans_num' => object_get($vtlObj, 'ctnr_num', ''),
            //     'info'      => sprintf('Move %s to %s', $input['pallet_rfid'], $location->loc_code)
            // ];

            // //call Event tracking
            // $this->eventTrackingModel->refreshModel();
            // $this->eventTrackingModel->create($dataEvt);

            DB::commit();

            $msg = sprintf("%s cartons has been put on location %s", count($ctnRfids), $location->loc_code);

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => $msg
                    ]
                ],
                'data' => []
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    /**
     * checking enough pallets have put on rack then turn on putaway within gr_hdr table
     *
     * @param $grHdrId
     *
     * @throws \Exception
     */
    private function _turnOnPutAway($grHdrId)
    {
        try {
            $grHdrObj = (new GoodsReceiptModel)->getModel()->where('gr_hdr_id', $grHdrId)->first();
            if ($grHdrObj) {
                $grStatus = object_get($grHdrObj, 'gr_sts');
                if ($grStatus == Status::getByKey('GR_STATUS', 'RECEIVED')) {
                    // // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                    // if ($grHdrObj && $grHdrObj->putaway != 2) {
                    //     $grHdrObj->putaway = 2;
                    //     $grHdrObj->save();
                    // }
                    return;
                }
                $totalPallet = (new GoodsReceiptDetailModel())->palletTotalOfGrHdr($grHdrId);
                $countPallet = $this->palletModel->countPalletOnRackByGRHdrId($grHdrId);

                // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                $grHdrObj->putaway = 2;
                if ($totalPallet == $countPallet) {
                    $grHdrObj->putaway = 1;
                }
                $grHdrObj->save();
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }


    private function _validateParams($attributes, $whsId)
    {
        $location = array_get($attributes, 'location');
        $type     = (int)array_get($attributes, 'type');
        $ctnRfids = array_get($attributes, 'ctn_rfids');
        $cusId    = (int)array_get($attributes, 'cus_id');

        $names = [
            'location'  => 'Location',
            'ctn_rfids' => 'Carton Array',
            'type'      => 'Type',
            'cus_id'    => 'Customer Id',
        ];

        // Check Required
        $requireds = [
            'location'  => $location,
            'ctn_rfids' => $ctnRfids,
            'cus_id'    => $cusId,
        ];

        // Check int type and greater than 0
        $intGreaterThan0 = [
            'type'   => $type,
            'cus_id' => $cusId,
        ];

        if (!in_array($type, [1, 2])) {
            $msg = "Type is required. Just including 1(RFID) / 2(Barcode)";
            return $this->_responseErrorMessage($msg);
        }

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // pallet rfid is string
        // if ($pltRfid && !is_string($pltRfid)) {
        //     $msg = $names['pallet_rfid'] . " must be string type.";
        //     return $this->_responseErrorMessage($msg);
        // }

        // cartons is array
        if (!is_array($ctnRfids) || !count($ctnRfids)) {
            $msg = $names['ctn_rfids'] . " must be array type and not be empty.";
            return $this->_responseErrorMessage($msg);
        }

        foreach ($intGreaterThan0 as $key => $field) {
            if (!is_int($field) || $field < 1) {
                $errorDetail = "{$names[$key]} must be integer type and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        //check invalid code carton
        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($ctnRfids as $ctnRfid)
        {
            $ctnRFIDValid = new RFIDValidate($ctnRfid, RFIDValidate::TYPE_CARTON, $whsId);
            if (!$ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $ctnRfid;
                if ('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }

//            if (!is_int($pieceRemain) || $pieceRemain < 1 || $pieceRemain > $packSize) {
//                $errorDetail = sprintf("The piece remain of carton Rfid %s must be integer type,
//                greater than 0 and not greater than pack size %s.", $ctnRfid, $packSize);
//                return $this->_responseErrorMessage($errorDetail);
//            }
        }

        if (count($ctnRfids) != count(array_unique($ctnRfids))) {
            $errorDetail = sprintf("It is duplicated carton.");
            return $this->_responseErrorMessage($errorDetail);
        }

        if (count($ctnRfidInValid)) {
            return $this->_responseErrorMessage($errorMsg, $ctnRfidInValid);
        }

        //validate location RFID
        if ($type == 1) {
            $locRFIDValid = new RFIDValidate($location, RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                $msg = $locRFIDValid->error;
                return $this->_responseErrorMessage($msg);
            }
        }



        $customerWarehouseArr = DB::table('customer_warehouse')
            ->where('cus_id', $cusId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->first();

        if (!count($customerWarehouseArr)) {
            $customerArr = DB::table('customer')
                ->where('cus_id', $cusId)
                ->where('deleted', 0)
                ->first();
            if (!count($customerArr)) {
                $msg = sprintf("The customer doesn't exist!");
                return $this->_responseErrorMessage($msg);
            }

            $msg = sprintf("The customer doesn't belong to current warehouse!");
            return $this->_responseErrorMessage($msg);
        }
    }

        /**
     * [createPalletNum description]
     * @return [type] [description]
     */
    private function createPalletNum()
    {
        $today_ddmm = date('dm');
        $palletCode = "LPN-TMP-" . $today_ddmm;

        $countPallet = \DB::table('pallet')->select('plt_num')
            ->where('plt_num', 'like', $palletCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();
        $max = 0;
        if ($countPallet) {
            $pltCode = array_get($countPallet, 'plt_num', null);
            $arrMax = explode('-', $pltCode);
            $max = (int)end($arrMax);
        }

        $pltNum = $palletCode . "-" . str_pad($max + 1, 5, "0", STR_PAD_LEFT);

        return $pltNum;
    }

}
