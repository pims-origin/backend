<?php
namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Inventories;

class InventoryReportTransformer extends TransformerAbstract
{
    public function transform(Inventories $inventorySummary)
    {
        $statusData=$this->getAvailableIncludes();
        $key=$inventorySummary->item_id.'.'.$inventorySummary->cus_id.'.'.$inventorySummary->whs_id;
        return [
            'item_id'       => object_get($inventorySummary, 'item_id', 0),
            'sku'           => object_get($inventorySummary, 'sku', ''),
            'size'          => object_get($inventorySummary, 'size', ''),
            'color'         => object_get($inventorySummary, 'color', ''),
            //'lot'           => object_get($inventorySummary, 'lot', ''),
            'cus_id'        => object_get($inventorySummary, 'cus_id', ''),
            'cus_name'      => object_get($inventorySummary, 'cus_name', ''),
            'cus_code'      => object_get($inventorySummary, 'cus_code', ''),
            'avail'         => round(object_get($inventorySummary, 'avail', '')),
            //'allocated_qty' => object_get($inventorySummary, 'allocated_qty', ''),
            'picked_qty'    => round(object_get($inventorySummary, 'picked_qty', '')),
            //'lock_qty'      => object_get($inventorySummary, 'lock_qty', ''),
            //'dmg_qty'       => object_get($inventorySummary, 'dmg_qty', ''),
            //'crs_doc_qty'   => object_get($inventorySummary, 'crs_doc_qty', ''),
            'ttl'           => round(object_get($inventorySummary, 'ttl', '')),
            'upc'           => object_get($inventorySummary, 'upc', ''),

            'pack'           => object_get($inventorySummary, 'pack', ''),
            'description'    => object_get($inventorySummary, 'description', ''),
            'ctns'           => round(object_get($inventorySummary, 'ctns', '')),
            'gr_sts'=>$statusData->contains($key)?1:0,
        ];
    }
}
