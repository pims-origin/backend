<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnStatusModel;
use App\Api\V1\Transformers\AsnStatusTransformer;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

class AsnStatusController extends AbstractController
{

    public function __construct()
    {
    }

    /**
     * @SWG\Get(
     *     path="/asn-statuses",
     *     tags={"Asns statuses"},
     *     summary="Get Asns status",
     *     description="Get Asns status",
     *     operationId="asnStatuses",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Return object of Asn",
     *         @SWG\Schema(
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
     *                      ref="#/definitions/AsnStatuses"
     *                  ),
     *              ),
     *          )
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *          @SWG\Schema(ref="#/definitions/ErrorModel")
     *      )
     * )
     *
     * @param AsnStatusModel $asnStatusModel
     * @param AsnStatusTransformer $asnStatusTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function index(AsnStatusModel $asnStatusModel, AsnStatusTransformer $asnStatusTransformer, Request $request)
    {
        try {
            $input =  $request->getQueryParams();
            $inputType = array_get($input, 'type', 'asn');
            // get list asn
            $statuses = $asnStatusModel->search($input);
            $statuses = $statuses->keyBy('asn_sts_code');

            $userId = Data::getCurrentUserId();
            $userMeta = DB::table('user_meta')
                            ->where('user_id', $userId)
                            ->where('qualifier', config('constants.qualifier.ASN_STATUS'))
                            ->first();
            $userValue = collect(json_decode($userMeta['value']));
            $userValue = $userValue->get($inputType, []);
            if($userValue){
                $userValue = collect($userValue)->keyBy('key');
            }

            foreach($userValue as $value){
                $status = $statuses[$value->key];
                $status->checked = true;
            }

            return $this->response->collection($statuses, $asnStatusTransformer)
                                    ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
