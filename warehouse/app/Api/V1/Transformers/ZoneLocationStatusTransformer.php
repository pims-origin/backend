<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\ZoneLocationStatus;


class ZoneLocationStatusTransformer extends TransformerAbstract
{
    /**
     * @param ZoneLocationStatus $zoneLocationStatus
     *
     * @return array
     */
    public function transform(ZoneLocationStatus $zoneLocationStatus)
    {
        return [
            'loc_sts_code' => $zoneLocationStatus->loc_sts_code,
            'loc_sts_name' => $zoneLocationStatus->loc_sts_name,
            'loc_sts_desc' => $zoneLocationStatus->loc_sts_desc,
        ];
    }
}