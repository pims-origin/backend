<?php
/**
 * Created by PhpStorm.
 * User: duong.tran
 * Date: 7/17/2018
 * Time: 10:14 AM
 */

namespace Seldat\Wms2\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;


class SetWarehouseTimezone
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $redis = new Data();

        $whsId = $redis->getCurrentWhs();

        $warehouseTimezone = DB::table('whs_meta')
            ->where('whs_qualifier', 'wtz')
            ->where('whs_id', $whsId)
            ->first();

        if ($warehouseTimezone) {
            $timezone = is_array($warehouseTimezone) ? $warehouseTimezone['whs_meta_value'] : object_get($warehouseTimezone, 'whs_meta_value');
            config(['app.timezone' => $timezone]);
            ini_set("date.timezone", $timezone);
            date_default_timezone_set( $timezone);
        }

        return $next($request);
    }
}