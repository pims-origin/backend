<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 22/July/2016
 * Time: 11:05
 */

namespace App\Api\V1\Validators;

class RelocationValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'from_loc_id' => 'required|alpha_dash',
            'to_loc_id'   => 'required|alpha_dash',
        ];
    }
}
