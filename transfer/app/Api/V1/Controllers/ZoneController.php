<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\ZoneModel;
use App\Api\V1\Transformers\ItemLocationTransformer;
use App\Api\V1\Transformers\LocationTransformer;
use DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;

class ZoneController extends AbstractController
{

    protected $loc;
    protected $zoneModel;

    /**
     * LocationController constructor.
     */
    public function __construct()
    {
        $this->loc = new LocationModel();
        $this->zoneModel = new ZoneModel();

    }

    public function transfer(Request $request)
    {

        $input = $request->getParsedBody();

        try {
            $zoneId = array_get($input, 'zone_id', 0);
            $locIds = array_get($input, 'loc_ids', []);
            $result = "";
            if ($zoneId > 0 && count($locIds) > 0) {
                DB::table('location as l')
                    ->leftJoin('pallet as p', 'p.loc_id', "=", 'l.loc_id')
                    ->whereNull('p.loc_id')
                    ->where('l.loc_whs_id', 0)
                    ->where('l.loc_code_sts', 'AC')
                    ->whereIn('l.loc_id', $locIds)
                    ->update('l.loc_zone_id', array_get($input, 'zone_id'));
                $result = sprintf("%d location(s) was transferred!");
            } else {
                $result = "No location was transferred!";
            }

            return [
                "data" => $result
            ];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }
}
