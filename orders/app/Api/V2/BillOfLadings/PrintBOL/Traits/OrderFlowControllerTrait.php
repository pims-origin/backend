<?php

namespace App\Api\V2\BillOfLadings\PrintBOL\Traits;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\ShipmentModel;
use App\Api\V1\Models\OutPalletModel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

trait OrderFlowControllerTrait
{

    protected $req;

    public function orderFlow(
        $order_id,
        $dependency,
        $cus_id,
        $request,
        CustomerMetaModel $customerMetaModel,
        OrderHdrMetaModel $hdrMetaModel = null
    ) {
        $this->req = $request;
        try {
            $dpcy = $dependency;
            // get customer meta
            if (is_null($hdrMetaModel)) {
                $cus_meta = $customerMetaModel->getOrderFlow($cus_id);
            } else // get customer meta
            {
                $cus_meta = $hdrMetaModel->getOrderFlow($order_id);
            }

            $orderHdrModel = new OrderHdrModel();
            $csrFlow = $customerMetaModel->getFlow($cus_meta, 'CSR');
            $alcFlow = $customerMetaModel->getFlow($cus_meta, 'ALC');
            $NWPFlow = $customerMetaModel->getFlow($cus_meta, 'NWP');
            if ($dependency == 1) {
                // csr

                if (array_get($csrFlow, 'usage', -1) == 0) {
                    $odr = $orderHdrModel->getFirstWhere(['odr_id' => $order_id]);
                    $csr_default = $customerMetaModel->getPNP($cus_id, 'CSR', object_get($odr, 'whs_id', 0));
                    if ($csr_default != 0) {
                        $csr_status = $this->CSRFlow($order_id, $csr_default);

                        if ($csr_status != 200) {
                            //$orderHdrModel->deleteOrderHdr($order_id);
                           // throw new \Exception($csr_status);

                        }
                    }


                }
            }

            if ($dependency == 2) {
                // allocated

                if (array_get($alcFlow, 'usage', -1) == 0) {
                    $alc_status = $this->ALCFlow($order_id);
                    if ($alc_status != 200) {
                        if (array_get($csrFlow, 'usage', -1) == 0) {
                            //$orderHdrModel->deleteOrderHdr($order_id);
                        }
                        //throw new \Exception($alc_status);
                    }

                }
            }
            if ($dependency == 3) {

                if (array_get($NWPFlow, 'usage', -1) == 0) {
                    $wp_status = $this->NWPFlow($order_id);
                    if ($wp_status != 200 && $dpcy == 1) {
                        $orderHdrModel->deleteOrderHdr($order_id);
                      //  throw new \Exception('Error occurred during create wave pick or assign picker');
                    }
                }
            }


        } catch (\Exception $exception) {
            throw  new \Exception($exception->getMessage());
        }

    }

    public function CSRFlow($order_id, $user_id)
    {
        $arrOrder[] = $order_id;
        $client = new Client();
        try {
            $client->request('PUT', env('API_ORDER') . 'assign-csr',
                [
                    'headers'     => ['Authorization' => $this->req->getHeader('Authorization')],
                    'form_params' => ["user_id" => $user_id, "odr_id" => $arrOrder]

                ]
            );

        } catch (RequestException $requestException) {
            return $this->getMessageError($requestException->getMessage());

        }

        return 200;
    }


    public function ALCFlow($order_id)
    {
        $arrOrder[] = $order_id;
        $ordDtl = new OrderDtlModel();
        $listDtl = $ordDtl->loadByOrdIds([$order_id]);
        $param = [];
        foreach ($listDtl as $dtl) {
            $dt = $dtl->toArray();
            $dt['itm_id'] = $dt['item_id'];
            $param[] = $dt;
        }

        $client = new Client();

        try {
            $client->request('PUT', env('API_ORDER') . 'allocates/' . $order_id,
                [
                    'headers'     => ['Authorization' => $this->req->getHeader('Authorization')],
                    'form_params' => $param
                ]
            );
        } catch (RequestException $requestException) {
            return $this->getMessageError($requestException->getMessage());

        }

        return 200;
    }

    public function NWPFlow($order_id)
    {
        $param[] = $order_id;

        $client = new Client();
        try {
            $res = $client->request('POST', env('API_WAVEPICK') . 'create-wave-pick',
                [
                    'headers'     => ['Authorization' => $this->req->getHeader('Authorization')],
                    'form_params' => ["odr_ids" => $param]

                ]
            );
        } catch (\Exception $exception) {

            //echo $exception->getMessage();

            return 400;
        }

        return $res->getStatusCode();
    }

    public function shippedFlow($odrIds, $request)
    {
        try {
            $arrOrder = [];
            foreach ($odrIds as $odrId) {
                $hdrMetaModel = new OrderHdrMetaModel();
                $customerMetaModel = new CustomerMetaModel();
                $cus_meta = $hdrMetaModel->getOrderFlow($odrId);
                $Flow = $customerMetaModel->getFlow($cus_meta, 'SH');
                if (array_get($Flow, 'usage', -1) == 0) {
                    $arrOrder[] = $odrId;
                }
            }

            if (!empty($arrOrder)) {
                $this->processShip($arrOrder);
            }

        } catch (\Exception $exception) {
            throw  new \Exception($exception->getMessage());
        }
    }

    public function checkPartial($order_id)
    {
        $hdrMetaModel = new OrderHdrMetaModel();
        $customerMetaModel = new CustomerMetaModel();
        $order_meta = $hdrMetaModel->getOrderFlow($order_id);
        $Flow = $customerMetaModel->getFlow($order_meta, 'POD');
        if (array_get($Flow, 'usage', -1) == 1) {
            return true;
        }

        return false;
    }

    private function processShip($orderIds, $ship_date = null)
    {
        if (!is_array($orderIds)) {
            $orderIds = [$orderIds];
        }
        $orderHdrs = $this->orderHdrModel->loadByOrdIds($orderIds)->toArray();
        if (!$orderHdrs) {
            throw new \Exception(Message::get("BM017", "Orders"));
        }

        $stsStg = [
            Status::getByValue("Staging", "Order-Status"),
            Status::getByValue("Partial Staging", "Order-Status")
        ];

        $arrShpIds = [];
        foreach ($orderHdrs as $orderHdr) {
            $arrShpIds[] = $orderHdr['ship_id'];
        }
        if (empty($arrShpIds)) {
            throw new \Exception(Message::get("BM017", "Shipping"));
        }

        $shpMnts = (new ShipmentModel())->loadByShpIds($arrShpIds)->toArray();

        if (!$shpMnts) {
            throw new \Exception(Message::get('BM126'));
        }
        $arrShpFnlIds = [];
        foreach ($shpMnts as $shpMnt) {
            if ($shpMnt['ship_sts'] === Status::getByValue("Final", "SHIP-STATUS")) {
                $arrShpFnlIds[] = $shpMnt['ship_id'];
            }
        }

        try {
            //
            DB::beginTransaction();

            $currentWH = Data::getCurrentWhsId();
            $odrIds = [];

            foreach ($orderHdrs as $orderHdr) {
                if (!in_array($orderHdr['odr_sts'], $stsStg)
                    || empty($orderHdr['ship_id'])
                    || !in_array($orderHdr['ship_id'], $arrShpFnlIds)
                ) {
                    throw new \Exception(Message::get("BM017", "Orders Shipping"));
                }

                $odrSts = Status::getByValue("Shipped", "Order-Status");

                $this->orderHdrModel->updateWhere([
                    'act_cmpl_dt' => time(),
                    'shipped_dt'  => $ship_date ? $ship_date : time(),
                    'updated_by'  => Data::getCurrentUserId(),
                    'odr_sts'     => $odrSts
                ], ['odr_id' => $orderHdr['odr_id']]);

                $odrIds[] = $orderHdr['odr_id'];

                $odrNum = array_get($orderHdr, 'odr_num');
                $currentWH = !empty($currentWH) ? $currentWH : array_get($orderHdr, 'whs_id');

                $evtCode = Status::getByKey("event", "PARTIAL-SHIPPED");
                $evtInfo = Status::getByKey("EVENT-INFO", "PRC");

                if ($odrSts === Status::getByValue("Shipped", "Order-Status")) {
                    $evtCode = Status::getByKey("event", "ORDER-SHIPPED");
                    $evtInfo = Status::getByKey("EVENT-INFO", "ORDER-SHIPPED");
                }

                //Remove location of out pallet
                $outPalletModel = new OutPalletModel();
                $listOutPallet = $outPalletModel->getOutPalletByOdrHdrId($orderHdr['odr_id']);
                if (count($listOutPallet) > 0) {
                    foreach ($listOutPallet as $outPallet) {
                        $outPallet->update([
                            'loc_id' => null,
                            'loc_name' => null,
                            'loc_code' => null
                        ]);
                    }
                }
                // Insert Event Tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $currentWH,
                    'cus_id'    => array_get($orderHdr, 'cus_id'),
                    'owner'     => $odrNum,
                    'evt_code'  => $evtCode,
                    'trans_num' => $odrNum,
                    'info'      => sprintf($evtInfo, $odrNum)
                ]);
            }

            //  Get Odr_dtl by OrderIds
            $orderDtls = $this->orderDtlModel->loadByOrdIds($odrIds)->toArray();
            if (!$orderDtls) {
                throw new \Exception(Message::get("BM017", "Order Details"));
            }

            $arrItms = [];

            foreach ($orderDtls as $orderDtl) {
                $itmId = array_get($orderDtl, 'item_id');
                $Itemlots[$itmId][] = array_get($orderDtl, 'lot');
                $arrItms[$itmId] = array_get($orderDtl, 'alloc_qty', 0);
            }

            // Change request: update order carton & carton status - 25-11-2016
            $this->updateOrderCarton($orderIds);

            // Update inventory summary
            $strOdrIds = implode(',', $odrIds);
            $sqlStr = "IF(`%s` <=
                COALESCE(
                    ( SELECT SUM(odr_cartons.piece_qty)
                    FROM odr_cartons
                    WHERE invt_smr.item_id = odr_cartons.item_id
                        AND odr_cartons.lot = invt_smr.lot
                        AND odr_cartons.ctn_sts = 'SH'
                        AND odr_cartons.odr_hdr_id IN(%s) )
                ,0), 0 , `%s` -
                COALESCE(
                    ( SELECT SUM(odr_cartons.piece_qty)
                    FROM odr_cartons
                    WHERE invt_smr.item_id = odr_cartons.item_id
                        AND odr_cartons.lot = invt_smr.lot
                        AND odr_cartons.ctn_sts = 'SH'
                        AND odr_cartons.odr_hdr_id IN(%s) )
                ,0))
            ";

            $sqlPickQty = sprintf($sqlStr, 'picked_qty', $strOdrIds, 'picked_qty', $strOdrIds);
            $sqlTtl = sprintf($sqlStr, 'ttl', $strOdrIds, 'ttl', $strOdrIds);

            foreach ($Itemlots as $itemId => $lots) {
                $this->invtSmrModel->getModel()
                    ->where('whs_id', $currentWH)
                    ->where('item_id', $itemId)
                    ->whereIn('lot', $lots)
                    ->update([
                        'picked_qty' => DB::raw($sqlPickQty),
                        'ttl'        => DB::raw($sqlTtl)
                    ]);
            }

            /*$this->invtSmrModel->updateWhere([
                'picked_qty' => DB::raw($sqlPickQty),
                'ttl'        => DB::raw($sqlTtl)
            ], [
                'whs_id' => $currentWH
            ]);*/

            DB::commit();

            //push MQ receiving
            if ($this->customerConfigModel->checkWhere([
                'whs_id'       => $currentWH,
                'cus_id'       => array_get($orderHdrs[0], 'cus_id'),
                'config_name'  => config('constants.cus_config_name.EDI_INTEGRATION'),
                'config_value' => config('constants.cus_config_value.EDI945'),
                'ac'           => config('constants.cus_config_active.YES')
            ])
            ) {
                $reportDetail = $this->reportModel->shipped($orderIds);
                $this->reportService->init($reportDetail);
                $this->reportService->process();
            }

            return ['data' => 'Selected Orders are shipped successfully'];

        } catch (\PDOException $e) {
            DB::rollback();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function updateOrderCarton($orderIds)
    {
        if (!is_array($orderIds)) {
            $orderIds = [$orderIds];
        }
        $currDate = time();
        foreach ($orderIds as $orderId) {
            $this->orderCartonModel->refreshModel();
            $this->orderCartonModel->updateWhere([
                'ship_dt' => time(),
                'sts'     => 'u',
                'ctn_sts' => Status::getByKey('CTN_STATUS', 'SHIPPED')
            ], [
                'odr_hdr_id' => $orderId
            ]);

            // check each order in order carton
            $this->orderCartonModel->refreshModel();
            $orderCartons = $this->orderCartonModel->findWhere(['odr_hdr_id' => $orderId]);
            if (count($orderCartons) == 0) {
                throw new Exception(Message::get("BM017", "Order Carton"));
            }

            $ctn_ids = [];
            foreach ($orderCartons as $orderCarton) {
                if ($orderCarton['is_storage'] == 0) {
                    $ctn_ids[] = $orderCarton['ctn_id'];
                }
            }

            $this->cartonModel->getModel()
                ->whereIn('ctn_id', $ctn_ids)
                ->update([
                    'ctn_sts'          => Status::getByKey('CTN_STATUS', 'SHIPPED'),
                    'shipped_dt'       => $currDate,
                    'storage_duration' => CartonModel::getCalculateStorageDurationRaw()
                ]);
        }
    }

    public function getMessageError($message)
    {
        $error = explode('"message":"', $message);
        $error = explode('","', array_get($error, '1', ''));

        return array_get($error, '0', '');
    }
}