<?php
/*pt: the Unit test*/
use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use \Seldat\Wms2\Models\WarehouseMeta;
use Seldat\Wms2\Utils\Message;

class CustomerWarehouseTest extends TestCase
{
    use DatabaseTransactions;

    protected $customerId;
    protected $warehouseId;

    public function setUp()
    {
        parent::setUp();

        // create data into DB
        $this->customerId = factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        $this->warehouseId = factory(\Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
    }

    private function getEndPoint()
    {
        return "/v1/customers/{$this->customerId}/warehouses";
    }

    public function testSearch_Exist_Ok()
    {
        $endPoint = $this->getEndPoint();

        $response = $this->call('GET', $endPoint);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case2: Data is not null
        $this->assertNotEmpty($responseData);
    }

    public function testSearch_NotExit_Ok()
    {
        $this->customerId = 999999999999;

        $response = $this->call('GET', $this->getEndPoint());
        $responseData = json_decode($response->getContent());

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is empty
        $this->assertEmpty($responseData->data);
    }

    public function test_Store_Ok()
    {
        $data = $this->call('POST', $this->getEndPoint(), [
            'whs_id' => $this->warehouseId,
        ]);

        // Case 1: Response code is 201 Create
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_CREATED);

        // Case 2: Data is not null
        $this->assertNotNull(json_decode($data->content(), true)['data']);
    }

    public function test_Store_Duplicate_whs_id_Fail()
    {
        //Create a CustomerWarehouse and the get whs_id
        $data = $this->call('POST', $this->getEndPoint(), [
            'whs_id' => $this->warehouseId,
        ]);

        $data_array2 = json_decode($data->content(), true)['data'];
        $warehouseID = ($data_array2["whs_id"]);

        // Get whs_id from creating CustomerWarehouse before Show
        $resultTest = $this->call('POST', $this->getEndPoint(), [
            'whs_id' => $warehouseID,
        ]);

        // Case 1: Response code is 400 Bad Request
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(Message::get("BM006", "Warehouse"), json_decode($resultTest->content(), true)['errors']['message']);
    }

    public function test_Store_NotParam_Fail()
    {
        $data = $this->call('POST', $this->getEndPoint());

        // Case 1: Response code is 422 Unprocessable Entity
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            'The whs id field is required.',
            json_decode($data->content(), true)['errors']['errors']['whs_id'][0]
        );
    }

    public function test_Store_WrongParam_Fail()
    {
        $data = $this->call('POST', $this->getEndPoint(), [
            'whs_id' => ['abc']
        ]);

        // Case 1: Response code is 422 Unprocessable Entity
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            'The selected whs id is invalid.',
            json_decode($data->content(), true)['errors']['errors']['whs_id'][0]
        );
    }

    public function test_Store_NotExit_whs_id_Param_Fail()
    {
        $data = $this->call('POST', $this->getEndPoint(), [
            'whs_id' => '999999999999999999999999999'
        ]);

        // Case 1: Response code is 422 Unprocessable Entity
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            'The selected whs id is invalid.',
            json_decode($data->content(), true)['errors']['errors']['whs_id'][0]
        );
    }

    public function testDelete_SingleItem_Ok()
    {
        $endPoint = $this->getEndPoint() . '/' . $this->warehouseId;
        $this->call('DELETE', $endPoint);

        // Case 1: Response code is 204 No Content
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDestroy_WarehouseIdNotExisted_Fail()
    {
        // make data
        $endPoint = $this->getEndPoint() . '/999999';

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }


}
