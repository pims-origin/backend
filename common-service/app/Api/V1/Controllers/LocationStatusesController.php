<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\LocationStatusesService;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class LocationStatusesController extends AbstractController
{
    protected $service;

    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->service = new LocationStatusesService();
    }

    public function getLocationStatuses()
    {
        try {
            return $this->service->getLocationStatuses();
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
