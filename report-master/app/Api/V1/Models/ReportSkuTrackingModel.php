<?php

namespace App\Api\V1\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\ReportSkuTracking;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ReportSkuTrackingModel extends AbstractModel
{
    protected $reportConfigsModel;

    /**
     * ReportSkuSummarysModel constructor.
     *
     * @param ReportSkuSummarys|null $model
     * @param \App\Api\V1\Models\ReportConfigsModel $reportConfigsModel
     */
    public function __construct() {
        $this->model = new ReportSkuTracking();
        $this->reportConfigsModel = new ReportConfigsModel();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null, $export = false)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        //----------Create query statements
        $cusMeta = $this->reportConfigsModel->getFirstWhere([
            'qualifier' => 'SSR'
        ],
            [],
            ['level' => "desc"]);

        $configsValJson = array_get($cusMeta, 'data', '');
        $configsValues = json_decode($configsValJson, true);
        //$configsValues = json_decode(json_decode($configsValJson, true), true);

        if (!empty($configsValues)) {
            foreach ($configsValues as $configsKey => $configsValue) {
                if (($configsValue['show'] == true)
                    && ($configsValue['condition'] != 'between')
                    && ($configsValue['condition'] != 'like')
                ) {
                    if (isset($attributes[$configsKey])) {
                        $query->where($configsKey, $attributes[$configsKey]);
                    }

                } elseif (($configsValue['show'] == true)
                    && ($configsValue['condition'] == 'like')
                ) {
                    if (isset($attributes[$configsKey])) {
                        $query->where($configsKey, 'like', "%" . SelStr::escapeLike($attributes[$configsKey]) . "%");
                    }

                } elseif (($configsValue['show'] == true)
                    && ($configsValue['condition'] == 'between')
                ) {
                    if (isset($attributes[$configsKey])
                        && $configsKey != 'updated_at' && $configsKey != 'created_at'
                    ) {
                        //input [1,2000]
                        $query->whereBetween($configsKey, \GuzzleHttp\json_decode($attributes[$configsKey]));
                    } elseif (isset($attributes[$configsKey])
                        && ($configsKey == 'updated_at' || $configsKey == 'created_at')
                    ) {
                        $dateStr = $attributes[$configsKey];
                        $commaPos = strpos($dateStr, ',');
                        $strLen = strlen($dateStr);
                        $dateFrom = substr($attributes[$configsKey], 0, $commaPos);
                        $dateTo = substr($attributes[$configsKey], $commaPos + 1, $strLen);

                        //input: 2017-06-15,2017-06-17
                        $query->whereBetween($configsKey,
                            [
                                $dateFrom,
                                $dateTo . ' 23:59:59'
                            ]);

                    }
                }

            }
        }
        //----------/Create query statements

        // Get
        $this->sortBuilder($query, $attributes);
        if ($export) {
            $models = $query->get();
        } else {
            $models = $query->paginate($limit);
        }

        return $models;
    }

    public function filter($filters)
    {
        $query = $this->model->newQuery();
        if($filters == null) {
            $filters = [];
        }
        $filters = SelArr::removeNullOrEmptyString($filters);
        $query->where(function($q) use($filters) {
            foreach ($filters as $filter) {
                $filter = new \ArrayObject($filter);
                $whereValue = 'where';
                $where = $filter['where'];
                if ($where == 'or') {
                    $whereValue = 'orWhere';
                }
                if(strtolower($filter['condition']) == 'like') {
                    $filter['value'] = "%".SelStr::escapeLike($filter['value']) . "%";
                } else if(strtolower($filter['condition']) == 'in' || strtolower($filter['condition']) == 'not in' ) {
                    $filter['value'] = "( ".$filter['value'] . " )";
                }
                if( $filter['condition'] != "between" && $filter['condition'] != "in" && $filter['condition'] != "not in") {
                    $q->$whereValue($filter['field_name'], $filter['condition'], $filter['value']);
                } else {
                    $where = $where."WhereRaw";
                    $q->$where("{$filter['field_name']} {$filter['condition']} {$filter['value']}");
                }
            }
        });

        return $query;
    }

    public function orderBy(Builder $query, $orderBys)
    {

        if($orderBys == null) {
            $orderBys = [];
        }
        $orderBys = SelArr::removeNullOrEmptyString($orderBys);

        foreach ($orderBys as $orderBy) {
            $orderBy = new \ArrayObject($orderBy);
            $query->orderBy($orderBy['field_name'], $orderBy['ordering']);
        }
        return $query;
    }

    public function searches(Builder $query, $searches, $configSearch)
    {
        if($searches == null) {
            $searches = [];
        }
        if($configSearch == null) {
            $configSearch = [];
        }
        $searches       = SelArr::removeNullOrEmptyString($searches);
        $configSearch   = SelArr::removeNullOrEmptyString($configSearch);
        $configSearch   = collect($configSearch)->keyBy('field_name');
        $query->where(function($q) use($searches, $configSearch) {
            foreach ($searches as $fieldName => $value) {
                if (!isset($configSearch[$fieldName])) {
                    continue;
                }
                $dataType = $configSearch[$fieldName]->data_type;
                $condition = $configSearch[$fieldName]->condition;
                if (!in_array($dataType, ['string', 'date', 'datetime', 'number'])) {
                    continue;
                }
                if(strtolower($condition) == 'like') {
                    $q->where($fieldName, $condition, "%".$value. "%");
                    continue;
                } else if(strtolower($condition) == 'in' || strtolower($condition) == 'not in' ) {
                    $value = "( " . $value . " )";
                }
                if ($dataType === 'date') {
                    if(strtotime($value) != false) {
                        $value = strtotime($value);
                    } else if (strpos($value, "and") !== false) {
                        $arrayValue = explode(' and ',$value );
                        $value = strtotime($arrayValue[0]). ' and ' . strtotime($arrayValue[1]);
                    }

                    if ($condition == ">=" || $condition == "<=") {
                        $q->where($fieldName, $condition, $value);
                    } else if ($condition == "="){
                        $q->whereBetween($fieldName,
                            [
                                $value,
                                $value + 86400
                            ]);
                    } else  if ($condition == ">" || $condition == "<") {
                        $q->where($fieldName, $condition . '=', $value + 86400);
                    } else {
                        $q->whereRaw("$fieldName $condition $value");
                    }
                }

                if ($dataType === 'datetime') {
                    $value = strtotime($value);
                    $q->where($fieldName, $condition, $value);
                }

                if ($dataType === 'string') {
                    $q->where($fieldName, $condition, $value);
                }
                if ($dataType === 'number') {
                    $q->whereRaw("$fieldName $condition $value");
                }
            }
        });

        return $query;
    }

    public function groupBy(Builder $query, $config)
    {
        $selects = ["*"=>'*'];

        $cloneQuery = clone $query;

        foreach ( $config->group_by as $value ) {
            $selects[] = $value;
            $cloneQuery->groupBy($value);
        }
        $cloneQuery->addSelect($selects);

        return $cloneQuery;
    }

    public function aggregate(Builder $query, $config)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $havings = [];
        $selects = ["*"=>'*'];
        foreach ( $config->aggregate as $aggregate ) {

            $selects[] = DB::raw(sprintf('%s(%s) as %s', $aggregate->function, $aggregate->field_name, $aggregate->field_name));

            if ( $aggregate->condition && $aggregate->having ) {
                $havings[] = DB::raw(sprintf('%s(%s) %s %s', $aggregate->function, $aggregate->field_name, $aggregate->condition, $aggregate->having));
            }
        }

        if ( $selects ) {
            $query->addSelect($selects);
        }

        if ( $havings ) {
            foreach ( $havings as $raw ) {
                $query->havingRaw($raw);
            }
        }

        $result = $query->get();
        $result->transform(function ($item) use($config){
            $key = [];
            foreach ( $config->group_by as $value ) {
                $key[] = $item[$value];
            }
            $item['key'] = implode('|', $key);
            return $item;
        });

        $result = $result->keyBy('key');

        return $result;
    }

    public function filter_searches(Builder $query, $searches, $fields)
    {
        if($searches == null) {
            $searches = [];
        }
        if($fields == null) {
            $fields = [];
        }
        $searches       = SelArr::removeNullOrEmptyString($searches);
        $fields   = SelArr::removeNullOrEmptyString($fields);
        $fields   = collect($fields)->keyBy('field_name');
        $query->where(function($q) use($searches, $fields) {
            foreach ($searches as $fieldName => $value) {
                if (!isset($fields[$fieldName])) {
                    continue;
                }
                $dataType = $fields[$fieldName]->data_type;
                $condition = $fields[$fieldName]->condition;
                if (!in_array($dataType, ['string', 'date', 'datetime', 'number'])) {
                    continue;
                }
                if(strtolower($condition) == 'like') {
                    $q->where($fieldName, $condition, "%".SelStr::escapeLike($value). "%");
                    continue;
                } else if(strtolower($condition) == 'in' || strtolower($condition) == 'not in' ) {
                    $value = "( " . $value . " )";
                }
                if ($dataType === 'date') {
                    if(strtotime($value) != false) {
                        $value = strtotime($value);
                    } else if (strpos($value, "and") !== false) {
                        $arrayValue = explode(' and ',$value );
                        $value = strtotime($arrayValue[0]). ' and ' . strtotime($arrayValue[1]);
                    }

                    if ($condition == ">=" || $condition == "<=") {
                        $q->where($fieldName, $condition, $value);
                    } else if ($condition == "="){
                        $q->whereBetween($fieldName,
                            [
                                $value,
                                $value + 86400
                            ]);
                    } else  if ($condition == ">" || $condition == "<") {
                        $q->where($fieldName, $condition . '=', $value + 86400);
                    } else {
                        $q->whereRaw("$fieldName $condition $value");
                    }
                }

                if ($dataType === 'datetime') {
                    $value = strtotime($value);
                    $q->where($fieldName, $condition, $value);
                }

                if ($dataType === 'string') {
                    $q->where($fieldName, $condition, $value);
                }

                if ($dataType === 'number') {
                    $q->whereRaw("$fieldName $condition $value");
                }
            }
        });

        return $query;
    }
}
