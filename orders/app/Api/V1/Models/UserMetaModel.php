<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\Status;

class UserMetaModel extends AbstractModel
{
    public function __construct()
    {

    }

    public function modifyUserMeta(Array $identify, Array $statuses, String $type)
    {
        $userMeta = DB::table('user_meta')
            ->where('user_id', $identify['user_id'])
            ->where('qualifier', $identify['qualifier'])
            ->first();

        if (!$userMeta) {
            DB::table('user_meta')->insert([
                'user_id'   => $identify['user_id'],
                'qualifier' => $identify['qualifier'],
                'value'     => json_encode([])
            ]);
        }

        $userMetaValue = collect(json_decode($userMeta['value']));

        $statuses = $this->formatStatuses($statuses, $identify['qualifier']);

        $userMetaValue[$type] = $statuses;

        DB::table('user_meta')
            ->where('user_id', $identify['user_id'])
            ->where('qualifier', $identify['qualifier'])
            ->update([
                'value'     => json_encode($userMetaValue)
            ]);

        return $userMetaValue[$type];
    }

    private function formatStatuses(Array $statuses, String $qualifierValue)
    {
        $result = [];

        $qualifier = config('constants.qualifier');

        foreach($qualifier as $key => $value){
            if ($value === $qualifierValue){
                $statusKey = $key;
                break;
            }
        }

        $allStatuses = Status::get($statusKey);
        foreach($statuses as $status){
            $result[] = [
                'key'       => $status,
                'value'     => $allStatuses[$status],
                'checked'   => true
            ];
        }
        return $result;
    }
}
