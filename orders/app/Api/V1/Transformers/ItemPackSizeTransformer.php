<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Item;


class ItemPackSizeTransformer extends TransformerAbstract
{
    //return result
    public function transform(Carton $carton)
    {

        return [
            'pack_size' => object_get($carton, 'ctn_pack_size')
        ];
    }


}
