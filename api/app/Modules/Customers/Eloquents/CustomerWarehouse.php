<?php

namespace App\Modules\Customers\Eloquents;

use Illuminate\Database\Eloquent\Model;

class CustomerWarehouse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_warehouse';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id'
    ];
}
