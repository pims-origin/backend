<?php

namespace App\Api\OUTBOUND\Transformers;

use League\Fractal\TransformerAbstract;

class ActiveLocationTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform($location)
    {
        return [
            'ctn_ttl'      => $location['ctn_ttl'],
            'avail_qty'    => $location['avail_qty'],
            'sku'          => $location['sku'],
            'size'         => $location['size'],
            'color'        => $location['color'],
            'lot'          => $location['lot'],
            'loc_id'       => $location['loc_id'],
            'loc_code'     => $location['loc_code'],
            'item_id'      => $location['item_id'],
            'loc_sts_code' => $location['loc_sts_code'],
        ];

    }
}
