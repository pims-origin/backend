<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\OrderHdrMeta;
use League\Fractal\TransformerAbstract;

class OrderHdrMetaTransformer extends TransformerAbstract
{
    public function transform(OrderHdrMeta $orderHdrMeta)
    {
        return $orderHdrMeta->val_json_decode;
    }
}