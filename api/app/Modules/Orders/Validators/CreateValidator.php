<?php

namespace App\Modules\Orders\Validators;

use Validator;

class CreateValidator
{
    public function rules()
    {
        return [
            'order_type'        => 'required|in:bulk,retail,ecom,smallparcel',
            'order_num'         => 'required',
            'po_num'            => 'required',
            'carrier.carrier'   => 'nullable|in:fedex,ups',
            'order_date'        => 'required|date_format:Y-m-d',
            // 'ship_date'         => 'required',
            'items.*.sku'       => 'required',
            'items.*.qty'       => 'required|numeric|min:1',
            'contacts.*.name'   => 'required',
            'contacts.*.email'  => 'required',
            'addresses.*.address_line_1' => 'required',
            'addresses.*.city'  => 'required',
            'addresses.*.state' => 'required',
            'addresses.*.zip'   => 'required'
        ];
    }

    public function validate($input)
    {
        $input['order_type'] = strtolower(array_get($input, 'order_type'));
        $input['carrier']['carrier'] = strtolower(array_get(array_get($input, 'carrier'), 'carrier'));
        $validator = Validator::make($input, $this->rules(), $this->messages());

        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                }
            }
        }

        return $validator;
    }

    public function messages() {
        return [
            'order_type.required'           => 'Order type is required',
            'order_type.in'                 => 'Order type is not supported',
            'order_num.required'            => 'Order number is required',
            'po_num.required'               => 'PO number is required',
            'carrier.carrier.in'            => 'Carrier is not supported',
            'order_date.required'           => 'Order date is required',
            'order_date.date_format'        => 'Order date does not match format (Y-m-d)',
            // 'ship_date.required'            => 'Ship date is required',
            'items.*.sku.required'          => 'Sku is required',
            'items.*.qty.required'          => 'Quantity is required',
            'contacts.*.name.required'      => 'Contact name is required',
            'contacts.*.email.required'     => 'Contact email is required',
            'addresses.*.address_line_1.required' => 'Shipping address line 1 is required',
            'addresses.*.city.required'     => 'Shipping city is required',
            'addresses.*.state.required'    => 'Shipping state is required',
            'addresses.*.zip.required'      => 'Shipping zip code is required'
        ];
    }
}