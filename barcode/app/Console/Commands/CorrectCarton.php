<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CorrectCarton extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correct:carton {whsId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create new carton ';


    /**
     * Create a new command instance.
     *
     * @param DripEmailer $drip
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $whsId = $this->argument('whsId');
        DB::table('cartons_bk_20190715')
            ->where('piece_remain', '>', 1)
            ->where('ctn_sts', 'AC')->where('whs_id', $whsId)
            ->whereNull('rfid')
            ->groupBy('item_id')
            ->havingRaw('count(ctn_id)=1')
            ->where('cus_id', '<>', 362)
            ->chunk(100, function ($cts) {
            foreach ($cts as $ct) {
                $tem = $ct['piece_remain'];
                $data4Insert = [];
                while ($tem > 1) {
                    $newCt = [];
                    $newCt['whs_id'] = $ct['whs_id'];
                    $newCt['cus_id'] = $ct['cus_id'];
                    $newCt['ctn_num'] = $ct['ctn_num'] . "-NW" . str_pad($tem, 5, "0", STR_PAD_LEFT);
                    $newCt['ctn_sts'] = 'AC';
                    $newCt['ctn_uom_id'] = $ct['ctn_uom_id'];
                    $newCt['plt_id'] = $ct['plt_id'];
                    $newCt['loc_id'] = $ct['loc_id'];
                    $newCt['loc_code'] = $ct['loc_code'];
                    $newCt['loc_name'] = $ct['loc_name'];
                    $newCt['loc_type_code'] = $ct['loc_type_code'];
                    $newCt['is_damaged'] = $ct['is_damaged'];
                    $newCt['piece_remain'] = 1;
                    $newCt['piece_remain'] = 1;
                    $newCt['piece_ttl'] = 1;
                    $newCt['ctn_pack_size'] = 1;
                    $newCt['created_at'] = $ct['created_at'];
                    $newCt['created_by'] = $ct['created_by'];
                    $newCt['updated_at'] = $ct['updated_at'];
                    $newCt['updated_by'] = $ct['updated_by'];
                    $newCt['deleted'] = $ct['deleted'];
                    $newCt['deleted_at'] = $ct['deleted_at'];
                    $newCt['item_id'] = $ct['item_id'];
                    $newCt['sku'] = $ct['sku'];
                    $newCt['size'] = $ct['size'];
                    $newCt['color'] = $ct['color'];
                    $newCt['lot'] = $ct['lot'];
                    $newCt['uom_code'] = $ct['uom_code'];
                    $newCt['uom_name'] = $ct['uom_name'];
                    $newCt['spc_hdl_code'] = $ct['spc_hdl_code'];
                    $newCt['spc_hdl_name'] = $ct['spc_hdl_name'];
                    $data4Insert[] = $newCt;
                    $tem = $tem - 1;
                }
                try {

                    foreach (array_chunk($data4Insert, 500) as $dataIs) {
                        DB::table('cartons')->insert($dataIs);
                    }

                } catch (\Exception $e) {
                    dd($e);
                }
            }
        });

    }

}