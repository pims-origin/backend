<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\PalletAssignment\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class PackHdrModel extends AbstractModel
{
    protected $packRef;

    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
        $this->packRef = [];
    }

    /**
     * @param $packHdrId
     *
     * @return mixed
     */
    public function deletePackHdr($packHdrId)
    {
        return $this->model
            ->where('ord_hdr_id', $packHdrId)
            ->delete();
    }



    /**
     * @param $packIds
     *
     * @return mixed
     */
    public function checkWhereIn($packIds)
    {
        return $this->model
            ->whereIn('odr_id', $packIds)
            ->count();
    }

    public function getFistPackNotAssignByCheckSum($checksum, $odr_id, $with = [])
    {
        $query = $this->make($with);
        $result = $query->where('pack_dt_checksum', $checksum)
            ->where('odr_hdr_id', $odr_id)
            ->whereNull('out_plt_id')
            ->first();

        return $result;
    }

}
