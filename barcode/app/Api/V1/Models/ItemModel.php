<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class ItemModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new Item();
    }

    public function search($attribute) {
        $query = $this->getModel()->where("cus_id", $attribute['cus_id']);

        if(!empty($attribute['sku'])) {
            $query->where("sku",'like','%' . $attribute['sku'] . '%');
        }

        if(!empty($attribute['upc'])) {
            $query->where("cus_upc", $attribute['upc']);
        }

        return $query->orderBy('item_id', 'DESC')->get();
    }

    public function addNewItem($attribute) {
        DB::beginTransaction();

        $attribute['item_code'] = $this->generateItemByCusCode($attribute['cus_id']);

        if(!empty($attribute['item_code']['is_error']) && $attribute['item_code']['is_error'] == 1) {
            return $attribute['item_code'];
        }

        $itemParams = [
            'item_code'         => $attribute['item_code'],
            'description'       => (!empty($attribute['description']))?($attribute['description']):'',
            'sku'               => (!empty($attribute['sku']))?($attribute['sku']):$attribute['item_code'],
            'cus_upc'           => (!empty($attribute['upc']))?($attribute['upc']):'',
            'size'              => 'NA',
            'color'             => 'NA',
            'pack'              => (!empty($attribute['pack']))?($attribute['pack']):1,
            'length'            => 1,
            'width'             => 1,
            'height'            => 1,
            'weight'            => 1,
            'volume'            => 1,
            'cus_id'            => $attribute['cus_id'],
            'status'            => 'AC',
            'inner_pack'        => 0,
            'level_id'          => 1,
            'uom_id'            => 2,
            'uom_name'          => 'PIECE',
            'spc_hdl_code'      => $attribute['spc_hdl_code'],
            'uom_code'          => 'PC'
        ];
        $newItem = $this->create($itemParams);

        DB::commit();

        return $newItem;
    }

    public function validateSku($attribute) {
        $query = $this->getModel()->where("cus_id", $attribute['cus_id'])
            ->where("sku",$attribute['sku']);

        return $query->first();
    }

    public function validateUpc($attribute) {
        $query = $this->getModel()->where("cus_id", $attribute['cus_id'])
            ->where("cus_upc",$attribute['upc']);

        return $query->first();
    }

    public function generateItemCode($prefix = 'COS',$master = 0){
        $item = $this->getModel()->where('item_code', 'LIKE', $prefix.$master.'%')
            ->orderBy("item_id", "DESC")->first();

        if (!$item){
            return $prefix.$master.'000001';
        }

        return ++$item->item_code;
    }

    protected function generateItemByCusCode($cus_id,$master = 0) {
        $customer = DB::table("customer")->where("cus_id",$cus_id)
            ->where("deleted",0)->first();
        if(empty($customer)) {
            return [
                "is_error" => 1,
                "message"  => "Customer does not exist"
            ];
        }
        return $this->generateItemCode($customer['cus_code'],$master);
    }

    public function createOrUpdate($attribute) {
        if(!empty($attribute['item_id'])) {
            return $this->getModel()->where("item_id",$attribute['item_id'])->update($attribute);
        }
        return $this->create($attribute);
    }

    public function makeChildItem($attribute, $item_id) {
        if(count($attribute) <= 1) {
            return 0;
        }
        $type = "SGL";
        if(count($attribute) > 1) {
            $type = "MIX";
        }
        $parentItem = $this->getFirstWhere(['$item_id' => $item_id]);
        DB::table('item_child')->where('parent', $parentItem->item_id)->delete();

        $attribute = array_values(collect($attribute)->sortBy('item_id')->toArray());

        $checkSum = '';
        foreach ($attribute as $itemArr) {
            $pack = $itemArr['pack'] != 1 ? '_' . $itemArr['pack'] : ''; //avoid impact with pack create new sku

            $childLevel = DB::table('system_uom')->select('level_id')->where('sys_uom_id', $itemArr['uom_id'])->first();

            // check parentItemLevel must be greater than childItemLevel
            if ($parentItem->level_id <= $childLevel['level_id']) {
                return $this->response->errorBadRequest('Level of parent Item must be greater than level of child Item');
            }

            //create or get item
            $itemObj = $this->upsertItem([
                'item_id'  => $itemArr['item_id'],
                'sku'      => $itemArr['sku'],
                'size'     => $itemArr['size'] ?? 'NA',
                'color'    => $itemArr['color'] ?? 'NA',
                'cus_id'   => $itemArr['cus_id'],
                'pack'     => $itemArr['inner_pack'],
                'level_id' => $childLevel['level_id']
            ]);

            //create item children
            DB::table('item_child')->insert([
                'parent' => $parentItem['item_id'],
                'child'  => $itemObj->item_id,
                'type'   => $type,
                'origin' => $itemArr['item_id'],
                'pack'   => $itemArr['pack']

            ]);

            $checkSum .=
                $itemObj['sku'] . "_" .
                $itemObj['size'] . "_" .
                $itemObj['color'] . "_" .
                $itemObj['cus_id'] . "_" .
                $itemObj['pack'] .
                $pack . "&";
        }

        if ($checkSum != '') {
            $checkSum = md5(strtoupper($checkSum));
            $chkParentItem = Item::where('children_checksum', $checkSum)
                ->where('item_id', '!=', $parentItem['item_id'])
                ->first();

            if ($chkParentItem) {
                throw new HttpException(400, "This item pack is existed in item: " . $chkParentItem->item_id);
            }

            //update checksum to parent item
            Item::where('item_id', $parentItem['item_id'])->update([
                'children_checksum' => $checkSum
            ]);
        }
        return 0;

    }
}
