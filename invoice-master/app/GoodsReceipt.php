<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App;

/**
 * @property mixed ctnr_id
 * @property mixed asn_hdr_id
 * @property mixed gr_hdr_seq
 * @property mixed gr_hdr_ept_dt
 * @property mixed gr_hdr_num
 * @property mixed whs_id
 * @property mixed cus_id
 * @property mixed gr_in_note
 * @property mixed gr_ex_note
 * @property mixed gr_sts
 * @property mixed gr_hdr_id
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property mixed goodsReceiptStatus
 */
class GoodsReceipt extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gr_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gr_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'ctnr_id',
        'asn_hdr_id',
        'gr_hdr_seq',
        'gr_hdr_ept_dt',
        'gr_hdr_num',
        'whs_id',
        'cus_id',
        'gr_in_note',
        'gr_ex_note',
        'gr_sts',
        'putter',
        'ctnr_num',
        'ref_code'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    /**
     * @return mixed
     */
    public function updatedBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'updated_by');
    }

    /**
     * @return mixed
     */
    public function putterUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'putter');
    }
}
