<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/17/2016
 * Time: 9:31 AM
 */
namespace backend\models;

use yii\db\Query;
use Yii;

class OfficeLocation
{
    public static function getOfficeLoc()
    {
        $query = (new Query());
        $loc = $query->from('office_loc')
            ->select('*')
            ->all();
        if (null === $loc) {
            return false;
        }

        return $loc;
    }
}