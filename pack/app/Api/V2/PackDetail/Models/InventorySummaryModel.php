<?php

namespace App\Api\V2\PackDetail\Models;

use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\SelStr;

class InventorySummaryModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        // search sku
        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        // search sku
        if (isset($attributes['color'])) {
            $query->where('color', 'like', "%" . SelStr::escapeLike($attributes['color']) . "%");
        }

        // search sku
        if (isset($attributes['size'])) {
            $query->where('size', 'like', "%" . SelStr::escapeLike($attributes['size']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * @param $item_ids
     *
     * @return mixed
     */
    public function getByItemIds($item_ids)
    {
        $item_ids = is_array($item_ids) ? $item_ids : [$item_ids];

        $rows = $this->model
            ->whereIn('item_id', $item_ids)
            ->get();

        return $rows;
    }


}
