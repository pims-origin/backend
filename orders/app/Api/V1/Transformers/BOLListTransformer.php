<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderShippingInfo;
use Seldat\Wms2\Models\Shipment;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderShippingTransformer
 *
 * @package App\Api\V1\Transformers
 */
class BOLListTransformer extends TransformerAbstract
{
    /**
     * @param OrderShippingInfo $orderShipping
     *
     * @return array
     */
    public function transform(Shipment $shipment)
    {
        $ship_to_name = array_pluck(object_get($shipment, 'odr_hdr', ''), 'ship_to_name');
        if (count($ship_to_name)) {
            $ship_to_name = array_unique($ship_to_name);
        }

        $arr_schd_ship_dt = array_pluck(object_get($shipment, 'odr_hdr', ''), 'schd_ship_dt');
        if (count($arr_schd_ship_dt)) {
            $arr_schd_ship_dt = array_unique($arr_schd_ship_dt);
        }

        $schd_ship_dt = array_map(function ($d) {
            if (isset($d) && !empty($d)) {
                return date('m/d/Y', $d);
            }
        }
            , $arr_schd_ship_dt);
        $shipToAdd1 = ($shipment->ship_to_addr_1) ? $shipment->ship_to_addr_1 . ', ' : '';
        $shipToAdd2 = ($shipment->ship_to_addr_2) ? $shipment->ship_to_addr_2 . ', ' : '';
        $shipToCity = ($shipment->ship_to_city) ? $shipment->ship_to_city . ', ' : '';
        $shipToState = ($shipment->ship_to_state) ? $shipment->ship_to_state . ' ' : '';
        $shipToZip = ($shipment->ship_to_zip) ? $shipment->ship_to_zip . ', ' : '';
        $shipToCountry = ($shipment->ship_to_country) ? $shipment->ship_to_country : '';

        return [
            'ship_id'       => $shipment->ship_id,
            'cus_id'        => $shipment->cus_id,
            'whs_id'        => $shipment->whs_id,
            'cus_name'      => object_get($shipment, 'customer.cus_name', ''),
            'schd_ship_dt'  => (substr(implode(",", $schd_ship_dt), 0, 1) == ",") ?
                trim(substr(implode(",", $schd_ship_dt), 1)) : trim(implode(",", $schd_ship_dt)),
            'ship_dt'       => date('m/d/Y', $shipment->ship_dt),
            'ship_to_name'  => implode(", ", $ship_to_name),
            'address'       => $shipToAdd1 . $shipToAdd2 . $shipToCity . $shipToState . $shipToZip . $shipToCountry,
            'bol_num'       => $shipment->bo_num,
            'bol_label'     => $shipment->bo_label,
            'odr_count'     => $shipment->odr_hdr()->count(),
            'ship_sts'      => $shipment->ship_sts,
            'ship_sts_name' => Status::getByKey("Ship-Status", $shipment->ship_sts)
        ];
    }
}
