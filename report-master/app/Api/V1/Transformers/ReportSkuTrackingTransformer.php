<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportSkuTracking;

class ReportSkuTrackingTransformer extends AbstractTransformer
{

    /**
     * @param ReportSkuTracking $reportSkuTracking
     * @return array
     */
    public function transform(ReportSkuTracking $reportSkuTracking)
    {
    	return [
    		"id" 			=>	array_get($reportSkuTracking, 'id', null),
    		"cus_id" 		=>	array_get($reportSkuTracking, 'cus_id', null),
    		"cus_name" 		=>	array_get($reportSkuTracking, 'cus_name', null),
    		"cus_code" 		=>	array_get($reportSkuTracking, 'cus_code', null),
    		"trans_num" 	=>	array_get($reportSkuTracking, 'trans_num', null),
    		"whs_id" 		=>	array_get($reportSkuTracking, 'whs_id', null),
    		"ref_cus_order"	=>	array_get($reportSkuTracking, 'ref_cus_order', null),
    		"po_ctnr" 		=>	array_get($reportSkuTracking, 'po_ctnr', null),
    		"actual_date" 	=>	$this->setTimeZone(array_get($reportSkuTracking, 'whs_id', null), array_get($reportSkuTracking, 'actual_date', null)),
    		"item_id" 		=>	array_get($reportSkuTracking, 'item_id', null),
    		"sku" 			=>	array_get($reportSkuTracking, 'sku', null),
    		"size" 			=>	array_get($reportSkuTracking, 'size', null),
    		"color" 		=>	array_get($reportSkuTracking, 'color', null),
    		"lot" 			=>	array_get($reportSkuTracking, 'lot', null),
    		"pack" 			=>	array_get($reportSkuTracking, 'pack', null),
    		"ctns"			=>	array_get($reportSkuTracking, 'ctns', null),
    		"qty" 			=>	array_get($reportSkuTracking, 'qty', null),
    		"created_at" 	=>	$this->setTimeZone(array_get($reportSkuTracking, 'whs_id', null), array_get($reportSkuTracking->toArray(), 'created_at', null)),
    		"updated_at" 	=>	$this->setTimeZone(array_get($reportSkuTracking, 'whs_id', null), array_get($reportSkuTracking->toArray(), 'updated_at', null)),
    		"created_by" 	=>	array_get($reportSkuTracking, 'created_by', null),
    		"updated_by" 	=>	array_get($reportSkuTracking, 'updated_by', null),
    		"deleted_at" 	=>	$this->setTimeZone(array_get($reportSkuTracking, 'whs_id', null), array_get($reportSkuTracking, 'deleted_at', null)),
    		"deleted" 		=>	array_get($reportSkuTracking, 'deleted', null),
    		"gr_hdr_id" 	=>	array_get($reportSkuTracking, 'gr_hdr_id', null),
    		"odr_id" 		=>	array_get($reportSkuTracking, 'odr_id', null),
    		"cycle_hdr_id" 	=>	array_get($reportSkuTracking, 'cycle_hdr_id', null),
    		"cube" 			=>	array_get($reportSkuTracking, 'cube', null),
    		'timezone'      => $this->getWhsTimeZone(array_get($reportSkuTracking, 'whs_id', null)) ?: 'UTC±0',
    	];
    }
}
