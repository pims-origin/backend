<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CycleHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\CycleHdr;

class CycleHdrTransformer extends TransformerAbstract
{
    /*
     * Transform CycleHdr
     * 
     * @param object $CycleHdr
     * @return array
     */
    public function transform(CycleHdr $cycleHdr)
    {
        return [
            'cycle_hdr_id'              => $cycleHdr->cycle_hdr_id,
            'cycle_name'                => $cycleHdr->cycle_name,
            'cycle_num'                 => $cycleHdr->cycle_num,
            'whs_id'                    => $cycleHdr->whs_id,
            'count_by'                  => $cycleHdr->count_by,
            'cycle_method'              => $cycleHdr->cycle_method,
            'cycle_type'                => $cycleHdr->cycle_type,
            'cycle_detail'              => $cycleHdr->cycle_detail,
            'description'               => $cycleHdr->description,
            'assignee_id'               => $cycleHdr->assignee_id,
            // 'assignee'                  => object_get($cycleHdr, 'assigneeUser.first_name', null),
            'assignee'                  => $cycleHdr->first_name??null,
            'due_dt'                    => date('m/d/Y', strtotime($cycleHdr->due_dt)),
            'has_color_size'            => $cycleHdr->has_color_size,
            'cycle_sts'                 => $cycleHdr->cycle_sts,
            'sts'                       => $cycleHdr->sts,
            'created_by'                => $cycleHdr->created_by,
            'updated_by'                => $cycleHdr->updated_by,
            'created_at'                => object_get($cycleHdr, 'created_at', '') ? date('m/d/Y',
                strtotime(object_get($cycleHdr, 'created_at', ''))) : '',
            'updated_at'                => $cycleHdr->updated_at,
            // 'whs_name'                  => object_get($cycleHdr, 'warehouse.whs_name', ''),
            'whs_name'                  => $cycleHdr->whs_name,
            // 'assignee_to_name'          => object_get($cycleHdr, 'first_name',
            //         '') . ' ' . object_get($cycleHdr, 'last_name', ''),
            'assignee_to_name'          => $cycleHdr->first_name. ' ' .$cycleHdr->last_name,
            'assignee_by_name'          => $cycleHdr->first_name. ' ' .$cycleHdr->last_name,
            // 'assignee_by_name'          => object_get($cycleHdr, 'createdBy.first_name',
            //         '') . ' ' . object_get($cycleHdr, 'createdBy.last_name', ''),
            'cycle_has_size_color_name' => $cycleHdr->has_color_size == 0 ? 'No' : 'Yes',
            'cycle_sts_name'            => CycleHdrModel::$arrCycleStatus[$cycleHdr->cycle_sts],
            'cycle_type_name'           => strtoupper(CycleHdrModel::$arrCycleType[$cycleHdr->cycle_type]),
            'cycle_method_name'         => CycleHdrModel::$arrCycleMethod[$cycleHdr->cycle_method],
            'completed_at'              => object_get($cycleHdr, 'completed_at', '') ?
                date('m/d/Y', object_get($cycleHdr, 'completed_at', '')) : '',
            'cc_type'                   => (strtoupper(object_get($cycleHdr, 'cycle_type', '')) == 'CCN')
                ? 'CC Notification' : 'Scheduled',
        ];
    }
}
