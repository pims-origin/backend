<?php

namespace App\Api\V2\BillOfLadings\CreateBOL\Models;

use \Seldat\Wms2\Models\Warehouse;

class WarehouseModel extends AbstractModel
{
    /**
     * WarehouseModel constructor.
     */
    public function __construct(Warehouse $model = null)
    {
		 $this->model = ($model) ?: new Warehouse();
    }

}
