<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SystemUomTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * SystemUomTest constructor.
     */

    public function getEndPoint()
    {
        return "/v1/system-uoms";
    }


    protected $searchParams = [
        'sys_uom_code' => 'abc',
        'sys_uom_name' => 1,
    ];

    protected $systemUomParams = [
        'sys_uom_code' => 'xx',
        'sys_uom_name' => 'sys_uom_name',
        'sys_uom_des'  => '$this is desc',
        'created_by'   => 14,
        'updated_by'   => 15,
    ];

    public function setUp()
    {
        parent::setUp();
    }

    public function testSearchSystemUom_Ok()
    {
        $this->call('GET', $this->getEndPoint(), $this->searchParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_NoParam_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testRead_Ok()
    {
        $systemUomId = factory(\App\SystemUom::class)->create()->sys_uom_id;

        $this->call('GET', $this->getEndPoint() . "/{$systemUomId}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $this->systemUomParams['sys_uom_id'] = rand(100000, 100000000);
        $this->call('POST', $this->getEndPoint(), $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    public function testStore_EmptyField_Fail()
    {
        unset($this->systemUomParams['sys_uom_code']);
        $this->call('POST', $this->getEndPoint(), $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_InvalidField_Fail()
    {
        $this->systemUomParams['sys_uom_name'] = null;

        $this->call('POST', $this->getEndPoint(), $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        $sys_uom_id = factory(\App\SystemUom::class)->create()->sys_uom_id;

        $this->call('PUT', $this->getEndPoint() . "/{$sys_uom_id}", $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_NotExist_Fail()
    {
        $abc = $this->call('PUT', $this->getEndPoint() . "/999999", $this->systemUomParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

//    public function testUpdate_ExistUniqueFields_Fail()
//    {
//        $systemUom = factory(\App\SystemUom::class)->create();
//        $this->systemUomParams['sys_uom_code'] = $systemUom->sys_uom_code;
//
//        $sys_uom_id = factory(\App\SystemUom::class)->create()->sys_uom_id;
//        $this->call('PUT', $this->getEndPoint() . "/{$sys_uom_id}", $this->systemUomParams);
//
//        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
//    }

    public function testUpdate_EmptyField_Fail()
    {
        $sys_uom_id = factory(\App\SystemUom::class)->create()->sys_uom_id;

        unset($this->systemUomParams['sys_uom_code']);
        $this->call('PUT', $this->getEndPoint() . "/{$sys_uom_id}", $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_InvalidField_Fail()
    {
        $sys_uom_id = factory(\App\SystemUom::class)->create()->sys_uom_id;

        $this->systemUomParams['sys_uom_name'] = null;
        $this->call('PUT', $this->getEndPoint() . "/{$sys_uom_id}", $this->systemUomParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDelete_Ok()
    {
        $sys_uom_id = factory(\App\SystemUom::class)->create()->sys_uom_id;

        $this->call('DELETE', $this->getEndPoint() . "/{$sys_uom_id}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDelete_NotExit_Fail()
    {
        $this->call('DELETE', $this->getEndPoint() . "/9999999999999999");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $sys_uom_id1 = factory(\App\SystemUom::class)->create()->sys_uom_id;
        $sys_uom_id2 = factory(\App\SystemUom::class)->create()->sys_uom_id;
        $sys_uom_id3 = factory(\App\SystemUom::class)->create()->sys_uom_id;
        $sys_uom_id4 = factory(\App\SystemUom::class)->create()->sys_uom_id;
        $sys_uom_id5 = factory(\App\SystemUom::class)->create()->sys_uom_id;

        $this->call('DELETE', $this->getEndPoint(),
            ['sys_uom_id' => [$sys_uom_id1, $sys_uom_id2, $sys_uom_id3, $sys_uom_id4, $sys_uom_id5]]);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }
}