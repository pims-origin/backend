<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class AsnHdrModel extends AbstractModel
{

    /**
     * GoodsReceiptModel constructor.
     *
     * @param AsnHdr|null $model
     */
    public function __construct(AsnHdr $model = null)
    {
        $this->model = ($model) ? : new AsnHdr();
    }

    public function search($attribute, $whsId) {
        $user_id = Data::getCurrentUserId();
        $listCusIds = DB::table("cus_in_user")->select("cus_id")->where('user_id', $user_id)->where('whs_id', $whsId)->get()->toArray();
        $listCusIds = array_column($listCusIds , "cus_id");
        $query = $this->getModel()->join("customer","customer.cus_id","=","asn_hdr.cus_id")
                ->leftJoin("asn_dtl","asn_dtl.asn_hdr_id","=","asn_hdr.asn_hdr_id")
                ->leftJoin("gr_hdr","gr_hdr.asn_hdr_id","=","asn_hdr.asn_hdr_id")
                ->leftJoin("cus_meta",function ($query) {
                    $query->on("cus_meta.cus_id","=","customer.cus_id");
                    $query->where("cus_meta.qualifier","=","IBC");
                })
                ->where('asn_dtl.deleted', 0)
                ->where("asn_hdr.whs_id", $whsId)
                ->whereIn("asn_hdr.cus_id", $listCusIds)
                ->whereIn("asn_hdr.asn_sts",["RG","NW"]);

        if(!empty($attribute['ref_code'])) {
            $query->where("asn_hdr.asn_hdr_ref","like","%".$attribute['ref_code']."%");
        }

        if(!empty($attribute['cus_name'])) {
            $query->where("customer.cus_name","like","%".$attribute['cus_name']."%");
        }

        if(!empty($attribute['exp_date'])) {
            $exp_date = strtotime($attribute['exp_date']);
            $query->where("asn_hdr.asn_hdr_ept_dt",$exp_date);
        }
        $query->orderBy("asn_hdr_ept_dt","desc")
                ->groupBy("asn_hdr.asn_hdr_id");
        $model = $query->select(
                        "asn_hdr.asn_hdr_id",
                        "asn_hdr.cus_id",
                        "customer.cus_name",
                        "customer.cus_code",
                        "asn_hdr.asn_hdr_num",
                        "asn_hdr.asn_hdr_ept_dt",
                        "asn_hdr.asn_hdr_act_dt",
                        "cus_meta.value",
                        "asn_hdr.asn_sts",
                        "asn_hdr.asn_hdr_ref",
                        "asn_hdr.asn_type",
                        "asn_dtl.ctnr_num as asn_ctnr_num",
                        "asn_dtl.ctnr_id as asn_ctnr_id",
                        "gr_hdr.ctnr_num as gr_ctnr_num",
                        "gr_hdr.ctnr_id as gr_ctnr_id"
                        )->get();
        return $model;
    }

    public function generateAsnNumAndSeq($prefix = 'ASN')
    {
        $currentYearMonth = date('ym');
        $asn = AsnHdr::where('asn_hdr_num', 'LIKE', '%-' . $currentYearMonth . '-%')
            ->orderBy('asn_hdr_seq', 'DESC')
            ->first();

        if (!$asn){
            return collect([
                'asn_num' => $prefix . '-' . $currentYearMonth . '-' . '00001',
                'asn_seq' => 1
            ]);
        }

        $asn->asn_hdr_seq++;
        return collect([
            'asn_num' => $prefix . '-' . $currentYearMonth . '-' . sprintf('%05d', $asn->asn_hdr_seq),
            'asn_seq' => $asn->asn_hdr_seq
        ]);
    }

    public function getRefByCus($attribute) {
        return AsnHdr::where('cus_id', $attribute['cus_id'])
            ->where('asn_hdr_ref', $attribute['ref_code'])
            ->whereIn('asn_sts', ['NW', 'RG'])
            ->first();
    }
}