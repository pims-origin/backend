<?php
//pt: list Items belong to carton

namespace App\Api\V3\Outbound\AssignPallet\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Location;

class SuggestLocationTransformer extends TransformerAbstract
{
    //return result
    public function transform(Location $loc)
    {
        return [
            'loc_id'               => array_get($loc, 'loc_id', null),
            'loc_code'             => array_get($loc, 'loc_code', null),
            'loc_alternative_name' => array_get($loc, 'loc_alternative_name', null),
            'loc_whs_id'           => array_get($loc, 'loc_whs_id', null),
            'rfid'                 => array_get($loc, 'rfid', null)
        ];
    }
}
