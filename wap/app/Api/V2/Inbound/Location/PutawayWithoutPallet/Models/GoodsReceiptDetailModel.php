<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Inbound\Location\PutawayWithoutPallet\Models;

use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\Status;

class GoodsReceiptDetailModel extends AbstractModel
{
    protected $eventTrackingModel;
    /**
     * GoodsReceiptDetailModel constructor.
     *
     * @param GoodsReceiptDetail|null $model
     */
    public function __construct(GoodsReceiptDetail $model = null)
    {
        $this->model = ($model) ?: new GoodsReceiptDetail();
        $this->eventTrackingModel = new EventTrackingModel();
    }

    /**
     * @param int $goodsReceiptDetailId
     *
     * @return int
     */
    public function deleteGoodsReceiptDetail($goodsReceiptDetailId)
    {
        return $this->model
            ->where('gr_dtl_id', $goodsReceiptDetailId)
            ->delete();
    }

    /**
     * @param array $goodsReceiptDetailIds
     *
     * @return mixed
     */
    public function deleteMassGoodsReceiptDetail(array $goodsReceiptDetailIds)
    {
        return $this->model
            ->whereIn('gr_dtl_id', $goodsReceiptDetailIds)
            ->delete();
    }


    /**
     * Search GoodsReceipt
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['gr_dtl_is_dmg'])) {
            $query->where('gr_dtl_is_dmg', $attributes['gr_dtl_is_dmg']);
        }

        // Search Goods Receipt Number
        $query->whereHas('goodsReceipt', function ($query) use ($attributes) {

            if (isset($attributes['gr_hdr_id'])) {
                $query->where('gr_hdr_id', $attributes['gr_hdr_id']);
            }
        });

        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    public function getTotalItemDamaged($item_id, $with = [])
    {
        $query = $this->make($with);
        $query->where('gr_dtl_is_dmg', 1);
        $query->whereHas('asnDetail', function ($query) use ($item_id) {
            $query->where('item_id', $item_id);
        });

        $result = $query->get()->transform(function ($item) {
            return [
                'itm_dmg_ttl' => -1 * object_get($item, 'gr_dtl_disc', 0) *
                    object_get($item, 'asnDetail.asn_dtl_pack', 0),
            ];
        });

        return $result->sum('itm_dmg_ttl');
    }


    /**
     * @param $grDtlId
     *
     * @return mixed
     */
    public function getGrByGrDtlId($grDtlId)
    {
        return $this->model->select('gr_hdr_id')->where('gr_dtl_id', $grDtlId)->first();
    }

    public function getGrDtlByGrDtlId($grDtlId)
    {
        return $this->model->where('gr_dtl_id', $grDtlId)->first();
    }

    public function getGrDtlByGrHdrId($grHdrId)
    {
        return $this->model->where('gr_hdr_id', $grHdrId)->get();
    }

    public function isGRDtlExist($grHdrId, $asnDtlId)
    {
        $grDtl = $this->getFirstWhere([
            'gr_hdr_id' => $grHdrId,
            'asn_dtl_id' => $asnDtlId
        ]);

        return $grDtl;
    }

    /**
     * @param $grHdr
     * @param $asnDtl
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|null
     * @throws \Exception
     */
    public function createGrDetail($grHdr, $asnDtl)
    {
        /**
         * Check goods receipt detail is existed
         */

        $asnDtlId = array_get($asnDtl, 'asn_dtl_id', null);
        $grHdrId = array_get($grHdr, 'gr_hdr_id', null);

        $grDtl = $this->isGRDtlExist($grHdrId, $asnDtlId);

        if ($grDtl) {
            return $grDtl;
        }


        $paramsDetail = [
            'asn_dtl_id' => $asnDtl['asn_dtl_id'],
            'gr_hdr_id' => $grHdrId,
            'gr_dtl_ept_ctn_ttl' => $asnDtl['asn_dtl_ctn_ttl'],
            'gr_dtl_act_ctn_ttl' => 0,
            'gr_dtl_plt_ttl' => 0,
            'gr_dtl_disc' => 0,
            'gr_dtl_is_dmg' => 0,
            'sku' => $asnDtl['asn_dtl_sku'],
            'size' => $asnDtl['asn_dtl_size'],
            'color' => $asnDtl['asn_dtl_color'],
            'lot' => $asnDtl['asn_dtl_lot'],
            'po' => $asnDtl['asn_dtl_po'],
            'uom_code' => $asnDtl['uom_code'],
            'uom_name' => $asnDtl['uom_name'],
            'uom_id' => $asnDtl['uom_id'],
            'upc' => $asnDtl['asn_dtl_cus_upc'],
            'ctnr_id' => $asnDtl['ctnr_id'],
            'ctnr_num' => $asnDtl['ctnr_num'],
            'item_id' => $asnDtl['item_id'],
            'pack' => $asnDtl['asn_dtl_pack'],
            'length' => array_get($asnDtl, 'asn_dtl_length', null),
            'width' => array_get($asnDtl, 'asn_dtl_width', null),
            'height' => array_get($asnDtl, 'asn_dtl_height', null),
            'weight' => array_get($asnDtl, 'asn_dtl_weight', null),
            'cube' => array_get($asnDtl, 'asn_dtl_cube', null),
            'volume' => array_get($asnDtl, 'asn_dtl_volume', null),
            'expired_dt' => array_get($asnDtl, 'expired_dt', null),
            'gr_dtl_dmg_ttl' => 0,
            'gr_dtl_sts' => 'RG',
        ];


        $this->refreshModel();
        $grDtl = $this->create($paramsDetail);

        return $grDtl;
    }

    /**
     * 15. Update Goods receipt dtl Received when asn_dtl Received and count all virtual carton status GR Created
     * Tech: UPDATE gr_dtl SET gr_dtl.gr_dtl_sts
          JOIN asn_dtl ON
          WHERE gr_dtl.id =
          AND asn_dtl.asn_dtl_sts = 'RE'
          AND gr_dtl.`gr_dtl_act_ctn_ttl` = (SELECT (ctn.ctn_id) FROM cartons WHERE cartons.ctn_sts = 'AC' AND cartons.`deleted` = 0 AND gr_dtl)
     * @param type $grDtlId
     * @return boolean
     * @deprecated  NOT IN USE
     */
    public function updateGoodsReceiptReceived($grDtlId)
    {
        $ttlCtnsSql = "(SELECT count(cartons.ctn_id) FROM cartons WHERE cartons.ctn_sts = 'AC' AND cartons.deleted = 0 AND cartons.gr_dtl_id = gr_dtl.gr_dtl_id)";
        $num = $this->getModel()
                ->join('asn_dtl', 'asn_dtl.asn_dtl_id', '=', 'gr_dtl.asn_dtl_id')
                ->where([
                    'gr_dtl.gr_dtl_id' => $grDtlId,
                    'asn_dtl.asn_dtl_sts' => 'RE',
                    'gr_dtl.gr_dtl_act_ctn_ttl' => DB::raw($ttlCtnsSql)
                        ]
                )
                ->first();
        //ambigous column updated_at
        if ($num) {
            $this->getModel()
                    ->find($grDtlId)
                    ->update([
                        'gr_dtl_sts' => 'RE'
            ]);
        }
    }

    /**
     * Create Event Tracking
     *
     * @param Array $params
     *
     * @return type
     */
    public function eventTracking($params)
    {

        // event tracking asn
        $this->eventTrackingModel->refreshModel();

        return $this->eventTrackingModel->create([
            'whs_id'    => $params['whs_id'],
            'cus_id'    => $params['cus_id'],
            'owner'     => $params['owner'],
            'evt_code'  => $params['evt_code'],
            'trans_num' => $params['trans_num'],
            'info'      => $params['info'],
        ]);
    }

    /**
     *  7. Update GR Dtl: real actual cartons, real damaged cartons, real pallet total
     *  Tech UPDATE gr_dtl SET
        gr_dtl_plt_ttl = gr_dtl_plt_ttl + PHP $pallet
        gr_dtl_act_ctn_ttl = gr_dtl_act_ctn_ttl + PHP COUNT($rfids)
        gr_dtl_is_dmg = PHP
        gr_dtl_disc = gr_dtl_act_ctn_ttl - gr_dtl_ept_ctn_ttl
        gr_dtl_dmg_ttl = PHP COUNT($dmgrfids)
        WHERE
     *
     * @param type $grDtlId
     * @return type
     */
    public function updateGrDtlWhenScanPallet($grDtlId)
    {
        $ttlCtnDmgsSql = '(SELECT COUNT(cartons.gr_dtl_id) FROM  cartons WHERE gr_dtl.gr_dtl_id = cartons.gr_dtl_id AND cartons.`deleted` = 0 AND cartons.is_damaged = 1)';
        $ttlActCtnsSql = '(SELECT COUNT(cartons.gr_dtl_id) FROM cartons WHERE gr_dtl.gr_dtl_id = cartons.gr_dtl_id AND cartons.`deleted` = 0)';
        $ttlPltsSql = '(SELECT COUNT(pallet.gr_dtl_id) FROM pallet WHERE gr_dtl.gr_dtl_id = pallet.gr_dtl_id)';
        $grDtl = $this->getModel()
            ->where([
                'gr_dtl_id' => $grDtlId
            ]);

        $result = $grDtl->update([
            'gr_dtl_plt_ttl'     => DB::raw($ttlPltsSql),
            'gr_dtl_act_ctn_ttl' => DB::raw($ttlActCtnsSql),
            'gr_dtl_dmg_ttl'     => DB::raw($ttlCtnDmgsSql),
            'gr_dtl_is_dmg'      => DB::raw('IF(gr_dtl_dmg_ttl > 0, 1, 0)'),
        ]);
        $grDtlObj = $grDtl->first();
        $expCtn = object_get($grDtlObj, 'gr_dtl_ept_ctn_ttl');
        $actCtn = object_get($grDtlObj, 'gr_dtl_act_ctn_ttl');
        $disc = intval($actCtn) - intval($expCtn);
        $grDtl->update([
            'gr_dtl_disc' => $disc,
        ]);

        return $result ? $grDtl->first() : false;
    }

    public function updateGoodsReceiptDetailReceiving(int $grDtlId):bool
    {
        $result = $this->model
            ->where(['gr_dtl.gr_dtl_id' => $grDtlId])
            ->update([
                'gr_dtl_sts' => 'RG'
            ]);

        return (bool) $result;
    }

    public function updateGoodsReceiptDetailReceived(int $grDtlId)
    {
        $ttlCtnsSql = "(SELECT count(vtl_ctn.vtl_ctn_id)
        FROM vtl_ctn
        WHERE vtl_ctn.deleted = 0
        AND vtl_ctn.asn_dtl_id = asn_dtl.asn_dtl_id)";

        $grDtl = $this->model
            ->join('asn_dtl', 'asn_dtl.asn_dtl_id', '=', 'gr_dtl.asn_dtl_id')
            ->where([
                    'gr_dtl.gr_dtl_id'          => $grDtlId,
                    'asn_dtl.asn_dtl_sts'       => 'RE',
                    'gr_dtl.gr_dtl_act_ctn_ttl' => DB::raw($ttlCtnsSql)
                ]
            )->first();

        if ($grDtl) {
            $grDtl->gr_dtl_sts = 'RE';
            $grDtl->save();
        }

        return $grDtl;
    }

    public function palletTotalOfGrHdr($grHdrId)
    {
        return $this->model->where('gr_hdr_id', $grHdrId)->sum('gr_dtl_plt_ttl');
    }
}
