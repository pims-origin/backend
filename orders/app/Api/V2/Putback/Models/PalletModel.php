<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\Putback\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PalletModel
 *
 * @package App\Api\V1\Models
 */
class PalletModel extends AbstractModel
{
    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
    }

    /**
     * @param $attributes
     * @param array $width
     *
     * @return mixed
     */
    public function suggestPallet($attributes, $width = [])
    {
        $query = $this->make($width);

        if (!empty($attributes['ctn_ttl'])) {
            $query->where('ctn_ttl', '>=', $attributes['ctn_ttl']);
        }

        $query->orderBy('ctn_ttl');
        $query->orderBy('plt_id');

        return $query->get();
    }

    public function updatePalletCtnTtl($locIds)
    {
        foreach (array_chunk(array_unique($locIds), 200) as $chunkLocIds) {
             $this->model
                ->whereIn('loc_id', $chunkLocIds)
                ->update([
                    'ctn_ttl' => DB::raw("(SELECT COUNT(c.ctn_id) FROM cartons c WHERE pallet.plt_id = c.plt_id)")
                ]);
        }
    }

    public function validateLPNFormat($lpn, $whsCode)
    {
        //mbs-pl-000001
        $pattern = "/^{$whsCode}-PL([F,B,G]{0,1})-([0-9]{6})$/";
        if (!preg_match($pattern, $lpn)) {
            throw new HttpException(403, "Invalid pallet id format");
        }
    }
}
