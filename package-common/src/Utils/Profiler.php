<?php
namespace Seldat\Wms2\Utils;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
class Profiler
{
    const KEY_TRACE='trace';
    static $debug='';
    static $time;
    static $options=[];
    static $total=0;
    static $dir;
    static function start($options=[]){
        if(strpos(($_SERVER['HTTP_REFERER']??""),"pims.staging.wms247go.com")!==false
        || strpos(($_SERVER['HTTP_REFERER']??""),"localhost")!==false)
        {
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Headers: Authorization");
            header("Access-Control-Allow-Methods: GET,PUT,POST,DELETE");
        }
        self::$options['minTime']=$options['minTime']??0;
        self::$options['trace']=$options['trace']??false;
        self::$time = microtime(true);
        $dir=dirname(dirname(dirname(dirname(__DIR__)))).'/frontend/dist/logs';
        if(!file_exists($dir))
            mkdir($dir,0777);
        self::$dir = $dir;
        if(!empty($options['debug']) || !empty($_GET['debug'])) {
            self::$debug = '#' . self::_pad($_GET['debug']??date('His',time()+7*3600), 'right',6) . ' - ';
            $temp=explode("/",$_SERVER["SCRIPT_FILENAME"]??"");
            $temp=$temp[count($temp)-3]??"";
            self::_log(self::_pad(number_format(self::$total,3).' sec','left').' - [START] - '.$temp.' - '.$_SERVER['REQUEST_METHOD'].': '.$_SERVER['REQUEST_URI'].' - payload: '.self::_getPayload().' - IP: '.$_SERVER['REMOTE_ADDR']);
            DB::enableQueryLog();
        }
    }
    static function _getPayload(){
        try
        {
            return file_get_contents('php://input');
        }
        catch (\Exception $e){
            return "empty";
        }
    }
    static function end(){
        self::_log(self::_pad('','right').' - [END] Time request: '.number_format(self::$total,3).' sec');
        $logs=DB::getQueryLog();
        if(($_GET['dump']??0)==1)
        {
            echo "<pre>";
            var_dump($logs);
            die();
        }
        self::_log($_SERVER['REQUEST_METHOD'].': '.$_SERVER['REQUEST_URI'].' - payload: '.self::_getPayload().' - IP: '.$_SERVER['REMOTE_ADDR'],date('d').'.query.txt');
        self::_log(json_encode($logs),date('d').'.query.txt');
    }
    static function _log($message,$fileName=null){
        try {
            $fileName=$fileName?:(date('d').'.txt');
            $f=fopen(self::$dir.'/'.$fileName,'a+');
            fwrite($f,self::$debug.$message.PHP_EOL);
            fclose($f);
        }
        catch (\Exception $e){

        }
    }
    static function _pad($string,$direct,$num=10){
        $str=substr($string,0,$num);
        $pad=str_repeat(' ',max(0,$num-strlen($string)));
        if($direct=='right')
            return $str.$pad;
        else
            return $pad.$str;
    }
    static function getQueryLog($quiet=true){

    }
    static function log($message,$options=[]){
        if(empty(self::$debug))
            return;
        $time = microtime(true);
        $second = $time - self::$time;
        self::$total+=$second;
        $minTime=$options['minTime']??self::$options['minTime'];
        $trace=$options['trace']??self::$options['trace'];
        if($trace) {
            if(!$trace instanceof \Exception)
                $trace=new \Exception('');
            $message .=' - message: '.$trace->getMessage().' - trace: '.str_replace(['\r\n',PHP_EOL],['____'],$trace->getTraceAsString());
        }
        if($second>=$minTime)
            self::_log(self::_pad(number_format($second,3).' sec','left').' - '.$message);
        self::$time = $time;
    }
}