<?php

namespace backend\forms;

use common\models\User;
use common\models\CusInUser;
use common\models\CusExUser;
use common\models\UserWarehouse;
use Yii;

class UpdateUserForm extends SignupForm
{
    public $user_id;
    protected $_updateFields = [
        'usr_dpm_id',
        'email',
        'first_name',
        'last_name',
        'status',
        'emp_code',
        'off_loc',
        'dept',
        'phone',
        'phone_extend',
        'mobile'
    ];

    public function rules()
    {
        $required = ['email', 'status', 'first_name', 'last_name', 'phone', 'usr_dpm_id'];

        return [
            [$required, 'required'],
            ['status', 'validateStatus'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 64],
            ['usr_dpm_id', 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 35],
            [['emp_code', 'off_loc', 'dept'], 'string', 'max' => 50],
            ['phone', 'string', 'max' => 20],
            ['phone_extend', 'string', 'max' => 5],
            ['mobile', 'string', 'max' => 20],
        ];
    }

    public function updateUser(User $user, $isProfile = false)
    {
        try {
            $transaction = Yii::$app->db->beginTransaction();

            $this->user_id = $user->user_id;

            if (!$this->validate()) {
                return null;
            }

            foreach ($this->_updateFields as $field) {
                if (is_null($this->$field)) {
                    continue;
                }

                $user->$field = $this->$field;
            }

            $user->updated_at = time();
            $result = $user->save();

            if ($isProfile) {
                $transaction->commit();

                return $result;
            }

            $cusUserClass = CusInUser::class;
            $cusUserLink = 'cusInUser';

            //remove old items
            $cusUserClass::deleteAll(['user_id' => $user->user_id]);
            //add user's customer
            foreach ($this->user_customers as $customer) {
                $cusUser = new $cusUserClass();
                $cusUser->user_id = $user->user_id;
                $cusUser->cus_id = $customer['cus_id'];
                $cusUser->whs_id = $customer['whs_id'];

                $user->link($cusUserLink, $cusUser);
            }

            //remove old items
            UserWarehouse::deleteAll(['user_id' => $user->user_id]);
            //add user's warehouses
            foreach ($this->user_warehouses as $warehouser) {
                $userWarehouse = new UserWarehouse();
                $userWarehouse->user_id = $user->user_id;
                $userWarehouse->whs_id = $warehouser['whs_id'];

                $user->link('userWarehouse', $userWarehouse);
            }

            $transaction->commit();

            //add user's roles
            if ($this->user_roles) {
                $res = $this->roleUserCreate($this->user_roles, $user->user_id);
                if ($res->getStatusCode() !== 200) {
                    $msg = \GuzzleHttp\json_decode($res->getBody()->getContents());
                    throw new \yii\base\Exception($msg->message, 404);
                }

            }
        } catch (\yii\base\Exception $ex) {
            $transaction->rollBack();
            throw new \yii\base\Exception($ex->getMessage() . 'Have Error In Signup.', 404);
        }

        return $result;
    }
}