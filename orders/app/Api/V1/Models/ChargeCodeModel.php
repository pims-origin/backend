<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 27-Sep-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ChargeCode;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class ChargeCodeModel extends AbstractModel
{
    /**
     * ChargeCodeModel constructor.
     *
     * @param ChargeCode|null $model
     */
    public function __construct(ChargeCode $model = null)
    {
        $this->model = ($model) ?: new ChargeCode();
    }


    /**
     * @param $arrChargeCodeIds
     *
     * @return mixed
     */
    public function getChargeCodeById($arrChargeCodeIds)
    {
        $arrChargeCodeIds = is_array($arrChargeCodeIds) ? $arrChargeCodeIds : [$arrChargeCodeIds];

        $rows = $this->model
            ->whereIn('chg_code_id', $arrChargeCodeIds)
            ->whereHas('systemUom', function ($uom) {
                $uom->where('sys_uom_code', "UN");
            })
            ->get();

        return $rows;
    }

    /**
     * @param $ChargeCodeId
     *
     * @return mixed
     */
    public function getChargeCodeAccordingToId($ChargeCodeId)
    {
        $rows = $this->model
            ->where('chg_code_id', $ChargeCodeId)
            ->first();

        return $rows;
    }

}
