<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\SystemService;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Validator;
use Illuminate\Http\Response as LumenResponse;

class SystemController extends AbstractController
{
    protected $service;

    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new SystemService($request);
    }

    public function getSMTPInfo(IRequest $request)
    {
        // check permission
        $this->checkPermission($request, 'configSystem');

        return $this->getSystemInfo(['EC', 'EA', 'ER', 'EI']);
    }

    public function getGeneralInfo(IRequest $request)
    {
        // check permission
        $this->checkPermission($request, 'configSystem');

        return $this->getSystemInfo(['TZ', 'MM', 'SW', 'DF', 'MS']);
    }

    private function getSystemInfo($keys)
    {
        $keys = is_array($keys) ? $keys : [$keys];

        $result = [];
        foreach ($keys as $key) {
            $response = $this->service->getSystemInfo($key);

            $responseData = (string) $response->getBody();
            $responseData = json_decode($responseData, true);

            $result[] = [
                'setting_key' => $key,
                'setting_value' => array_get($responseData, 'data.setting_value', null),
            ];
        }

        return new LumenResponse(['data' => $result], 200);
    }

    public function postSystemInfo(IRequest $request)
    {
        // check permission
        $this->checkPermission($request, 'configSystem');

        if ($input = $request->getParsedBody()) {
            foreach ($input as $key => $item) {
                $key = strtoupper($key);
                try {
                    $response = $this->service->getSystemInfo($key);
                    $responseData = (string) $response->getBody();

                    $action = ($responseData) ? 'updateSystemInfo' : 'createSystemInfo';

                    $response = $this->service->{$action}($key, $item);

                    // error
                    if ($response->getStatusCode() != 201 && $response->getStatusCode() != 200) {
                        return $this->convertResponse($response);
                    }
                } catch (\Exception $e) {
                    return new LumenResponse(['errors' => [
                        'message' => $e->getMessage(),
                        'status_code' => 400
                    ]], 400);
                }
            }
        }
    }
}
