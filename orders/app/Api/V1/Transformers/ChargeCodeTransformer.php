<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 27-Sep-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\chargeCode;
use League\Fractal\TransformerAbstract;

class ChargeCodeTransformer extends TransformerAbstract
{
    public function transform(ChargeCode $chargeCode)
    {
        return [
            'chg_code_id'  => $chargeCode->chg_code_id,
            'chg_code'     => $chargeCode->chg_code,
            'chg_code_des' => $chargeCode->chg_code_des,
            'chg_uom_id'   => $chargeCode->chg_uom_id,
            'chg_type_id'  => $chargeCode->chg_type_id,
            'created_at'   => $chargeCode->created_at,
            'updated_at'   => $chargeCode->updated_at,
            'deleted_at'   => $chargeCode->deleted_at,
        ];
    }
}
