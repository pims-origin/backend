<?php
/**
 * Created by PhpStorm.
 * User: admn
 * Date: 3/15/2017
 * Time: 3:32 PM
 */

namespace App\Jobs;


use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\WavePickHdrModel;
use Wms2\UserInfo\Data;
use DB;

class WavePickJob extends  Job
{
    protected $wvId;
    protected $request;

    public function __construct($wvId, $request)
    {
        $this->wvId = $wvId;
        $this->request = $request;
    }

    public function handle(){
        $data = DB::table('wv_queue')
            ->where('queue_sts', 'NW')
            ->where('wv_id', $this->wvId)
            ->where('type', 'UP')
            ->get();

        try {
            DB::beginTransaction();
            foreach($data as $rw) {
                if ($rw['data']) {
                    $orderCartons = unserialize($rw['data']);
                    $insertBatchs = array_chunk($orderCartons, 2000);
                    foreach ($insertBatchs as $batch) {
                        DB::table('odr_cartons')->insert($batch);
                    }
                    DB::table('wv_queue')->where('wv_id', $rw['wv_id'])
                        ->delete();

                    (new OrderHdrModel())->updateOrderPickedByWv($rw['wv_id']);
                    (new WavePickHdrModel())->updateWvComplete($rw['wv_id']);
                }

                // Update wv_ticket to CO (completed) with input wv_id
                // if((new WavePickHdrModel())->isWvCompleted($this->wvId)){
                //     DB::table('wv_ticket')
                //         ->where('wv_id', $this->wvId)
                //         ->update([
                //             'status'        => 'CO',
                //             'picker_id'     => Data::getCurrentUserId(),
                //             'updated_at'    => time(),
                //             'updated_by'    => Data::getCurrentUserId()
                //         ]);
                // }

            }
            DB::commit();
            dispatch(new AutoPackJob($this->wvId, $this->request));
        } catch (\Exception $e) {
            DB::rollBack();
            DB::table('sys_bugs')->where('id', 8 )->update(['error' => $e->getMessage()]);
            //  return $this->response->errorBadRequest($e->getMessage());
        }
    }
}