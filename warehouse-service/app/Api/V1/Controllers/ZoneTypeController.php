<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\BaseService;
use App\Api\V1\Models\WarehouseService;
use App\Api\V1\Models\ZoneService;
use App\Api\V1\Models\ZoneTypeService;
use App\Api\V1\Traits\ZoneTypeLocTypeController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;
use GuzzleHttp\Psr7\Response;


class ZoneTypeController extends AbstractController
{
    use ZoneTypeLocTypeController;
    /**
     * @var MenuService
     */
    protected $service;

    /**
     * ZoneTypeController constructor.
     *
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new ZoneTypeService($request);
    }
}
