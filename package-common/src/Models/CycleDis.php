<?php

namespace Seldat\Wms2\Models;

class CycleDis extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cycle_discrepancy';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'dicpy_ctn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cycle_hdr_id',
        'cycle_dtl_id',
        'ctn_id',
        'dicpy_qty',
        'dicpy_sts',
        'sts'
    ];

    /**
     * @return mixed
     */
    public function cycleHdr()
    {
        return $this->belongsTo(CycleHdr::class, 'cycle_hdr_id', 'cycle_hdr_id');
    }

    /**
     * @return mixed
     */
    public function cycleDtl()
    {
        return $this->belongsTo(CycleDtl::class, 'cycle_dtl_id', 'cycle_dtl_id');
    }

    /**
     * @return mixed
     */
    public function carton()
    {
        return $this->hasMany(Carton::class, 'ctn_id', 'ctn_id');
    }
}
