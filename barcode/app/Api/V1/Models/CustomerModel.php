<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class CustomerModel extends AbstractModel
{

    /**
     * GoodsReceiptModel constructor.
     *
     * @param AsnHdr|null $model
     */
    public function __construct(Customer $model = null)
    {
        $this->model = ($model) ? : new Customer();
    }

    public function search($attribute, $whsId) {
        $query = $this->getModel()->join("customer_warehouse","customer.cus_id","=","customer_warehouse.cus_id")
            ->where("customer_warehouse.whs_id", $whsId)
            ->where("customer_warehouse.deleted", 0);

        if(!empty($attribute['cus_name'])) {
            $query->where("cus_name","like","%".$attribute['cus_name']."%");
        }

        $query->groupBy('customer.cus_id')
              ->orderBy('customer.cus_name');

        $model = $query->get();
        return $model;
    }
}