<?php

namespace App\Api\V2\Putback\AssignPallet\Controllers;

use App\Api\V2\Putback\Models\CartonModel;
use App\Api\V2\Putback\Models\InventorySummaryModel;
use App\Api\V2\Putback\Models\LocationModel;
use App\Api\V2\Putback\Models\OrderCartonModel;
use App\Api\V2\Putback\Models\OrderDtlModel;
use App\Api\V2\Putback\Models\OrderHdrModel;
use App\Api\V2\Putback\Models\PalletModel;
use App\Api\V2\Putback\Models\PutBackModel;
use App\Api\V2\Putback\Models\ReturnDtlModel;
use App\Api\V2\Putback\Models\ReturnHdrModel;
use App\Api\V2\Putback\Models\ItemModel;
use App\Api\V2\Putback\AssignPallet\Traits\ReturnControllerTrait;
use App\Api\V2\Putback\AssignPallet\Transformers\ItemAssignTransformer;
use App\Api\V2\Putback\AssignPallet\Transformers\PalletSuggestTransformer;
use App\Api\V2\Putback\AssignPallet\Transformers\ReturnHdrTransformer;
use App\Api\V2\Putback\AssignPallet\Validators\PalletAssignValidator;
use App\Utils\CompareFloatNumbers;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SystemBug;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

/**
 * Class ReturnHdrController
 *
 * @package App\Api\V2\Putback\AssignPallet\Controllers
 */
class ReturnHdrController extends AbstractController
{
    use ReturnControllerTrait;

    protected $returnHdrModel;
    protected $returnDtlModel;
    protected $orderHdrModel;
    protected $orderDtlModel;
    protected $orderCartonModel;
    protected $palletModel;
    protected $cartonModel;
    protected $locationModel;
    protected $putbackModel;
    protected $itemModel;
    protected $invtSmrModel;

    /**
     * ReturnHdrController constructor.
     *
     * @param ReturnHdrModel $returnHdrModel
     * @param ReturnDtlModel $returnDtlModel
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param OrderCartonModel $orderCartonModel
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param PutBackModel $putBackModel
     * @param ItemModel $itemModel
     * @param InventorySummaryModel $inventorySummaryModel
     */
    public function __construct(
        ReturnHdrModel $returnHdrModel,
        ReturnDtlModel $returnDtlModel,
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        OrderCartonModel $orderCartonModel,
        PalletModel $palletModel,
        CartonModel $cartonModel,
        LocationModel $locationModel,
        PutBackModel $putBackModel,
        ItemModel $itemModel,
        InventorySummaryModel $inventorySummaryModel
    ) {
        $this->returnHdrModel = $returnHdrModel;
        $this->returnDtlModel = $returnDtlModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->orderCartonModel = $orderCartonModel;
        $this->palletModel = $palletModel;
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->putbackModel = $putBackModel;
        $this->itemModel = $itemModel;
        $this->invtSmrModel = $inventorySummaryModel;
    }

    /**
     * @param Request $request
     *
     * @return Response|void
     * @throws \Exception
     */
    public function validationPalletRfid(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $pltRfid = array_get($input, 'plt_rfid');
        if (!$pltRfid || $pltRfid == "") {
            throw new \Exception("The pallet RFID is required!");
        }

        //validate pallet RFID
        $this->palletModel->validateLPNFormat($pltRfid, $this->_getCurrentWhsCode());

        try {

            $checkPltExist = $this->palletModel->getFirstWhere(['rfid' => $pltRfid]);
            if ($checkPltExist) {
                throw new \Exception("The pallet RFID existed!");
            }

            return [];
            // return $this->response->noContent()->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $returnId
     * @param ItemAssignTransformer $itemAssignTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function getItem($returnId)
    {
        try {
            $returnHdr = $this->returnHdrModel->getFirstBy("return_id", $returnId);

            if (empty($returnHdr)) {
                return $this->response->noContent()->setStatusCode(Response::HTTP_OK);
            }
            $items = $this->returnHdrModel->getItemForAssign($returnHdr->odr_hdr_id);

            return [
                'picked_ttl' => object_get($returnHdr, 'return_qty', 0),
                'data'       => array_values($items),
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $returnId
     * @param Request $request
     * @param PalletAssignValidator $palletAssignValidator
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function assignPallet(
        $returnId,
        Request $request,
        PalletAssignValidator $palletAssignValidator
    ) {
        set_time_limit(0);
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['return_id'] = $returnId;

        foreach ($input['items'] as $itemKey => $item) {
            $input['items'][$itemKey] = SelArr::removeNullOrEmptyString($item);
        }

        $palletAssignValidator->validate($input);
        $input['items'] = $this->_changeDecimalNumberInput($input['items']);

        try {
            $returnHdr = $this->returnHdrModel->getFirstBy("return_id", $input['return_id'], [
                'createdUser',
                'odrHdr.customer',
                'putterUser'
            ]);

            //validate pallet RFID
            $pltRfids = array_pluck($input['items'], 'plt_rfid');

            foreach ($pltRfids as $pltRfid) {
                $this->palletModel->validateLPNFormat($pltRfid, $this->_getCurrentWhsCode());
            }
            // check exist pallet
            $checkPltExist = $this->palletModel->getModel()
                ->whereIn('rfid', $pltRfids)
                ->get();
            if (count($checkPltExist) > 0) {
                $rfidExists = $checkPltExist->pluck('rfid')->toArray();
                $msg = sprintf("The pallet RFID %s existed!", implode(', ', array_unique($rfidExists)));
                throw new \Exception($msg);
            }

            $enoughQty = [];

            foreach ($input['items'] as $idx => $item) {
                // check enough return qty
                $enoughQty[$item['item_id']][] = $item['qty_kg'];
                $input['items'][$idx]['spc_hdl_code'] = 'NA';
                $input['items'][$idx]['spc_hdl_name'] = 'NA';
            }

            $odrItemQty = $this->orderDtlModel->getModel()
                ->where('odr_id', array_get(array_first($input['items']), 'odr_hdr_id'))
                ->select([
                    'item_id',
                    DB::raw('SUM(picked_qty - packed_qty) AS picked_qty')
                ])
                ->having('picked_qty', '>', 0)
                ->groupBy('item_id')
                ->union(
                    PackHdr::select([
                        'pack_hdr.item_id',
                        DB::raw('SUM(item.pack) AS picked_qty')
                    ])
                        ->join('item', 'item.item_id', '=', 'pack_hdr.item_id')
                        ->where('odr_hdr_id', $returnHdr->odr_hdr_id)
                        ->groupBy('item_id')
                )
                ->pluck('picked_qty', 'item_id')
                ->toArray()
            ;

            if (count($enoughQty) != count($odrItemQty)) {
                $diffItem = array_diff(array_keys($odrItemQty), array_keys($enoughQty));
                $msg = sprintf("Missing item id %s", implode(', ', $diffItem));
                throw new \Exception($msg);
            }

            foreach ($odrItemQty as $itemId => $checkQty) {
                $enoughQtyItem = array_sum($enoughQty[$itemId]);
                if (CompareFloatNumbers::compareFloatNumbers($checkQty , $enoughQtyItem , '<' )) {
                    $msg = sprintf("Not greater than QTY of item id %s, picked QTY: %s", $itemId, $enoughQtyItem);
                    throw new \Exception($msg);
                } elseif (CompareFloatNumbers::compareFloatNumbers($checkQty , $enoughQtyItem , '>')) {
                    $msg = sprintf("Not enough QTY of item id %s, picked QTY: %s", $itemId, $enoughQtyItem);
                    throw new \Exception($msg);
                }
            }

            // Assign
            DB::beginTransaction();

            //return order picked qty
            $this->returnPickedQty($returnHdr->odr_hdr_id);

            //$this->_assignPB($returnHdr, $input);
            $this->__assignPB($returnHdr, $input);

            DB::commit();

            // Print
            $printed = $this->_printPB($returnHdr);
            if ($printed !== true) {
                return $this->response->noContent()->setContent([
                    'message' => 'Assign Successful!. But ' . $printed,
                    'data'    => []
                ])->setStatusCode(Response::HTTP_CREATED);
            }

            return $this->response->noContent()->setContent([
                'message' => 'Assign Successful!',
                'data'    => []
            ])->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function getCalculateStorageDurationRaw($startDate = 'created_at')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(%s)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (%s)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    protected function returnPickedQty($odrId)
    {
        DB::table('invt_smr')
            ->join('odr_dtl', function ($join) {
                $join->on('odr_dtl.item_id', '=', 'invt_smr.item_id')
                    ->on('odr_dtl.lot', '=', 'invt_smr.lot');
            })
            ->where([
                'odr_dtl.odr_id'  => $odrId,
                'odr_dtl.deleted' => 0
            ])
            ->where('odr_dtl.lot', '!=', 'ANY')
            ->update([
                'invt_smr.picked_qty' => DB::raw('invt_smr.picked_qty - odr_dtl.picked_qty'),
                'invt_smr.ttl'        => DB::raw('invt_smr.ttl - odr_dtl.picked_qty'),
                'odr_dtl.picked_qty'  => 0
            ]);

        //update odr_cartons and cartons to PB
        DB::table('odr_cartons')
            ->join('cartons', 'odr_cartons.ctn_id', '=', 'cartons.ctn_id')
            ->where('odr_cartons.odr_hdr_id', $odrId)
            ->update([
                'odr_cartons.ctn_sts'      => 'PB',
                'cartons.ctn_sts'          => DB::raw("IF(odr_cartons.is_storage = 0, 'PB', cartons.ctn_sts)"),
                'cartons.storage_duration' => $this->getCalculateStorageDurationRaw('cartons.created_at'),
                'cartons.updated_at'       => time()
            ]);
    }

    private function __assignPB($returnHdr, $input)
    {
        if ($returnHdr->return_sts !== 'NW') {
            throw new HttpException(400, "PutBack Assigned!");
        }

        //update return hdr
        $returnHdr->return_sts = 'AS';
        $returnHdr->save();

        foreach ($input['items'] as $itemInput) {
            // Update or Create Return Detail
            $returnDtl = $this->updateOrCreateReturnDtl($returnHdr, $itemInput);

            // Assign Carton to Pallet
            $dataCheckPallet = [
                'rfid' => $itemInput['plt_rfid'],
            ];
            $pallet = $this->palletModel->getFirstWhere($dataCheckPallet);

            if (!$pallet) {
                // Create Pallet
                $pallet = $this->createPallet($returnHdr, $itemInput, $returnDtl);
            } else {
                //validate duplicate plt uom KG
                if ($itemInput['uom_code'] === 'KG' || $pallet['uom'] === 'PL') {
                    $msg = 'Pallet ' . $itemInput['plt_rfid'] . " don't allow mixed SKU";

                    return $this->response->errorBadRequest($msg);
                }

                // update Pallet
                $pallet->ctn_ttl += $itemInput['ctns'];
                $pallet->init_ctn_ttl += $itemInput['ctns'];
                $pallet->init_piece_ttl += $itemInput['qty_kg'];
                if ($pallet->item_id != $itemInput['item_id']) {
                    $pallet->mixed_sku = 1;
                }

                $pallet->save();
            }

            $this->createNewCartonAndAssignPallet($returnHdr, $pallet, $itemInput);
        }

    }

    public function printPutBack($returnId)
    {
        try {
            $returnHdr = $this->returnHdrModel->getFirstBy("return_id", $returnId, ['putterUser']);

            if (empty($returnHdr->return_id)) {
                throw new \Exception(Message::get("BM017", "PutBack"));
            }

            // Print
            $printed = $this->_printPB($returnHdr);
            if ($printed !== true) {
                return $this->response->errorBadRequest($printed);
            }

            return $this->response->noContent()->setStatusCode(Response::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _printPB($returnHdr)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        $info = [
            'cus_name'    => object_get($returnHdr, 'odrHdr.customer.cus_name'),
            'cus_code'    => object_get($returnHdr, 'odrHdr.customer.cus_code'),
            'user_name'   => trim(object_get($returnHdr, 'createdUser.first_name') . " " .
                object_get($returnHdr, 'createdUser.last_name')),
            'whs_id'      => $whsId,
            'return_num'  => $returnHdr->return_num,
            'return_id'   => $returnHdr->return_id,
            'putter_id'   => object_get($returnHdr, 'putterUser.user_id'),
            'putter_name' => trim(object_get($returnHdr, 'putterUser.first_name') . " " .
                object_get($returnHdr, 'putterUser.last_name')),
        ];

        $putBacks = $this->putbackModel->findWhere(['return_id' => $returnHdr->return_id], ['pallet'])->toArray();
        try {
            $pallets = [];
            if (!empty($putBacks[0]) && in_array($returnHdr->return_sts, ['SG', 'CO'])) {
                foreach ($putBacks as $putBack) {
                    $pallets[] = [
                        'plt_num'          => array_get($putBack, 'pallet.plt_num'),
                        'ctn_ttl'          => array_get($putBack, 'ctn_ttl'),
                        'suggest_loc_code' => array_get($putBack, 'suggest_loc_code'),
                        'actual_loc_code'  => array_get($putBack, 'actual_loc_code')
                    ];
                }
            } else {
                //get spc_hdl_code from cartons
                $spcHdl = Pallet::join('cartons', 'pallet.plt_id', '=', 'cartons.plt_id')
                    ->where('cartons.return_id', $returnHdr->return_id)
                    ->whereNotNull('cartons.spc_hdl_code')
                    ->groupBy("cartons.item_id", "cartons.lot", "cartons.spc_hdl_code", "cartons.plt_id")
                    ->get(['cartons.item_id', 'cartons.lot', 'cartons.spc_hdl_code', 'pallet.rfid']);

                $chkCodes = [];
                $spcHdlCount = []; //for suggest location
                foreach ($spcHdl as $spc) {
                    $chkCodes[$spc['item_id'] . '-' . $spc['lot']] = $spc['spc_hdl_code'];

                    //processing VIET
                    $pltType = explode('-', $spc['rfid'])[1];
                    $spcHdlCode = $spc['spc_hdl_code'];
                    $spcKey = $pltType . "-" . $spcHdlCode;
                    if (isset($spcHdlCount[$spcKey])) {
                        $spcHdlCount[$spcKey]++;
                    } else {
                        $spcHdlCount[$spcKey] = 1;
                    }
                }


                // Suggest Data
                $pallets = $this->palletModel->findWhere(['return_id' => $returnHdr->return_id])->toArray();

                $dataSuggest = $this->locationModel->suggestLocation(object_get($returnHdr, 'odrHdr.cus_id'), $whsId,
                    $spcHdlCount);

                if (count($dataSuggest) < count($pallets)) {
                    return "There are not enough locations to suggest! Please add more location.";
                }

                DB::beginTransaction();

                // Suggest Location
                foreach ($pallets as $key => $pallet) {
                    //get putback
                    $pubBack = $this->putbackModel->getFirstWhere([
                        'return_id' => $returnHdr->return_id,
                        'pallet_id' => $pallet['plt_id']
                    ]);
                    $spcHdlCode = $chkCodes[$pubBack->item_id . '-' . $pubBack->lot];
                    $pltType = explode('-', $pallet['rfid'])[1];
                    // Pallet get one location.
                    foreach ($dataSuggest as $idx => $location) {
                        if ($pltType . "-" . $spcHdlCode == $location['plt_type_spc_hdl']) {
                            unset($dataSuggest[$idx]);

                            // Update PutBack Data Suggested
                            $this->putbackModel->refreshModel();
                            $this->putbackModel->update([
                                'pb_id'            => $pubBack->pb_id,
                                'suggest_loc_id'   => array_get($location, 'loc_id', null),
                                'suggest_loc_code' => array_get($location, 'loc_code', null),
                            ]);

                            //update data for print
                            $pallets[$key]['ctn_ttl'] = $pubBack->ctn_ttl;
                            $pallets[$key]['suggest_loc_code'] = array_get($location, 'loc_code', null);
                            $pallets[$key]['actual_loc_code'] = '';

                            continue 2;
                        }
                    }
                    $this->response->errorBadRequest('Not enough location to suggest for pallet ' . $pallet['rfid']);
                    $this->response->errorBadRequest('Not enough location to suggest for pallet ' . $pallet['rfid']);

                }

                // Update Return
                $this->returnHdrModel->refreshModel();
                $this->returnHdrModel->update([
                    'return_id'  => $returnHdr->return_id,
                    'return_sts' => 'SG',
                    'sts'        => 'u',
                ]);

                DB::commit();
            }

            // Print PDF
            $this->printPDF($pallets, $info);

            return true;
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function _getCurrentWhsCode()
    {
        $userData = (Data::getInstance())->getUserInfo();
        $userWarehouse = array_get($userData, 'user_warehouses');
        $currentWhsId = array_get($userData, 'current_whs');
        foreach ($userWarehouse as $warehouse) {
            if ($warehouse['whs_id'] == $currentWhsId) {
                return $warehouse['whs_code'];
            }
        }
    }

    private function _changeDecimalNumberInput($items)
    {
        foreach ($items as $itemKey => $itemData) {
            if ($itemData['uom_code'] != 'KG')
            {
                $items[$itemKey]['qty_kg'] = (int)$itemData['qty_kg'];
            }
        }

        return $items;
    }

}
