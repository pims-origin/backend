<?php


namespace App\Api\Gate\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GateDtl;
use Seldat\Wms2\Utils\Status;

class GateDtlTransformer extends TransformerAbstract
{
    //return result
    public function transform(GateDtl $gateDtl)
    {
        $open = object_get($gateDtl, 'open', '');
        $close = object_get($gateDtl, 'close', '');

        return [
            'gate_id'   => object_get($gateDtl, 'gate.gate_id', null),
            'gate_name' => object_get($gateDtl, 'gate.gate_name', null),

            'ctnr_num' => object_get($gateDtl, 'ctnr_num', null),
            'ctnr_id'  => object_get($gateDtl, 'ctnr_id', null),

            'cus_id'   => object_get($gateDtl, 'cus_id', null),
            'cus_name' => object_get($gateDtl, 'cus_name', null),

            'whs_id' => object_get($gateDtl, 'whs_id', null),

            'open'  => date("Y-m-d H:i", strtotime($open)),
            'close' => ($close) ? date("Y-m-d H:i", strtotime($close)) : '',

            'duration'   => object_get($gateDtl, 'duration', null),
            'created_by' => trim(object_get($gateDtl, 'createdUser.first_name', null) . " " .
                object_get($gateDtl, 'createdUser.last_name', null)),
            'created_at' => $gateDtl->created_at->format("Y-m-d H:i"),

            'gate_dtl_sts'      => object_get($gateDtl, 'gate_dtl_sts', null),
            'gate_dtl_sts_name' => Status::getByKey('GATE_DTL_STATUS',
                object_get($gateDtl, 'gate_dtl_sts', null)),
        ];
    }

}