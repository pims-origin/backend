<?php

namespace App\Utils;

use iio\libmergepdf\Exception;
use iio\libmergepdf\Merger;
use Illuminate\Support\Facades\Storage;
use Spatie\PdfToImage\Pdf;

class Helpers
{
    protected static $expire_time;

    public static function uploadFileS3 ($filePath, $content) {
        Storage::disk('s3')->put($filePath, file_get_contents($content));
    }

    public static function downloadS3 ($fileName, $pathDest = null) {
        $file = Storage::disk('s3')->exists($fileName);
        if ($file) {
            $content = Storage::disk('s3')->get($fileName);
            $dest = is_null($pathDest) ? public_path('s3' . DIRECTORY_SEPARATOR . $fileName) : $pathDest;
            file_put_contents($dest, $content);
        }
    }
}