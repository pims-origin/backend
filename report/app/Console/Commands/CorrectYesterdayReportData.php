<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class CorrectYesterdayReportData extends AbstractCommand
{
    protected $signature   = 'correct-yesterday-report-data';
    protected $description = 'Correct yesterday daily inventory and pallet report if necessary';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            $this->logSuccess('Start correct-yesterday-report-data');

            $yesterdayTimestamp = strtotime(date('Y-m-d', time())) - 86400;

            $customerWarehouses = DB::table('customer_warehouse AS cw')
                ->select('cw.cus_id', 'cw.whs_id')
                ->join('customer as c', 'c.cus_id', '=', 'cw.cus_id')
                ->where("c.deleted", 0)
                ->where("cw.deleted", 0)
                ->get();
            if ($customerWarehouses) {
                foreach ($customerWarehouses as $customerWarehouse) {
                    if (!DB::table('daily_inv_rpt')->where([
                        'whs_id' => $customerWarehouse['whs_id'],
                        'cus_id' => $customerWarehouse['cus_id'],
                        'inv_dt' => $yesterdayTimestamp,
                    ])->first()) {
                        $this->processCorrect($yesterdayTimestamp, $customerWarehouse);
                    }
                }
                $this->info(PHP_EOL);
            } else {
                $this->info('Ware house or customer does\'t exist.' . PHP_EOL);
            }
            $this->logSuccess('End correct-yesterday-report-data');
        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            $this->logPdo($e->getMessage());
        }
    }

    private function processCorrect($yesterdayTimestamp, $customerWarehouse) {
        $this->generateSql('insert_daily_inv_rpt', $customerWarehouse['whs_id'], $customerWarehouse['cus_id'], $yesterdayTimestamp);

        $this->generateSql('insert_daily_plt_rpt', $customerWarehouse['whs_id'], $customerWarehouse['cus_id'], $yesterdayTimestamp);
    }

    private function generateSql($procedure, $whsId, $cusId, $date)
    {
        return DB::statement(sprintf("CALL %s(%d, %d, %d)", $procedure, $whsId, $cusId, $date));
    }
}
