<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 7-Sep-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class PickerValidator
 *
 * @package App\Api\V1\Validators
 */
class PickerMultiValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'user_id' => 'required|integer',
            'wv_dtl_ids'  => 'required',
        ];
    }

    public function checkUserStatus($userStatus)
    {
        switch ($userStatus) {
            case config('constants.INACTIVE'):
                throw new UnknownVersionException('The user is being inactive.', null, 5);

            default:
                break;
        }
    }

}
