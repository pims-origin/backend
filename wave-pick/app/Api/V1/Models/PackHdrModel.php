<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

class PackHdrModel extends AbstractModel
{

    protected $model;

    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
    }

    public function autoPacks($packs)
    {
        /**
         * Get order flow
         * Check packing = 0, auto pack
         * create pack from order carton;
         */
        $packHdr = [];
        $packedOdrIds = [];
        $eventData = [];

        foreach ($packs as $pack) {
            foreach ($pack as $odrId => $data) {
                $seq = 0;
                if ($this->isAutoPack($odrId)) {
                    $packedOdrIds[] = $odrId;
                    foreach ($data as $ctnId => $cartonInfo) {
                        $eventData[$odrId] = [
                            'evt_code'   => Status::getByKey("EVENT", "ORDER-PACKING-COMPLETE"),
                            'created_at' => time(),
                            'created_by' => JWTUtil::getPayloadValue('jti') ?: 1
                        ];

                        $carton = $cartonInfo['carton'];
                        $orderCarton = $cartonInfo['orderCarton'];
                        $eventData[$odrId]['whs_id'] = $carton['whs_id'];
                        $eventData[$odrId]['cus_id'] = $carton['cus_id'];
                        $eventData[$odrId]['owner'] = $orderCarton['odr_num'];
                        $eventData[$odrId]['trans_num'] = $orderCarton['odr_num'];
                        $eventData[$odrId]['info'] = sprintf(Status::getByKey("EVENT-INFO", "ORDER-PACKING-COMPLETE"),
                            $orderCarton['odr_num']);

                        $pack_type = "CT";
                        $width = $carton['width'];
                        $height = $carton['height'];
                        $length = $carton['length'];
                        $pack_ref_id = $this->insertDimension($length, $width, $height, $pack_type);

                        $packHdr[] = [
                            'pack_hdr_num'     => $carton['ctn_num'],
                            'seq'              => ++$seq,
                            'odr_hdr_id'       => $odrId,
                            'cnt_id'           => $ctnId,
                            'whs_id'           => $carton['whs_id'],
                            'cus_id'           => $carton['cus_id'],
                            'carrier_name'     => '',
                            'sku_ttl'          => 1,
                            'piece_ttl'        => $carton['picked_qty'],
                            'pack_sts'         => 'NW',
                            'created_at'       => time(),
                            'updated_at'       => time(),
                            'created_by'       => $orderCarton['created_by'],
                            'updated_by'       => $orderCarton['updated_by'],
                            'deleted_at'       => $orderCarton['deleted_at'],
                            'sts'              => 'I',
                            'ship_to_name'     => '',
                            'out_plt_id'       => null,
                            'pack_type'        => $pack_type,
                            'width'            => $carton['width'],
                            'height'           => $carton['height'],
                            'length'           => $carton['length'],
                            'pack_ref_id'      => $pack_ref_id,
                            'deleted'          => 0,
                            'is_print'         => 0,
                            'pack_dt_checksum' => $carton['item_id'],
                        ];
                    }
                }
            }
        }

        //auto pack process
        if (!empty($packHdr)) {
            DB::table('pack_hdr')->insert($packHdr);

            $ctnIds = array_column($packHdr, 'cnt_id');
            $ctnIds = array_unique($ctnIds);

            //create packDtl

            // get pack hdr
            $phList = DB::table('pack_hdr as ph')
                ->join('cartons as c', 'c.ctn_id', '=', 'ph.cnt_id')
                ->join('odr_cartons as oc', 'oc.ctn_id', '=', 'c.ctn_id')
                ->whereIn('c.ctn_id', $ctnIds)
                ->groupBy('c.ctn_id')
                ->select('ph.pack_hdr_id', 'ph.odr_hdr_id', 'c.whs_id', 'c.item_id', 'c.cus_id',
                    'c.lot', 'c.sku', 'c.size', 'c.color', 'oc.upc', 'c.ctn_uom_id',
                    'oc.piece_qty', 'ph.created_by', 'ph.updated_by'
                )->get();
            $packDtls = [];

            foreach ($phList as $ph) {
                $packDtls[] = [
                    'pack_hdr_id' => array_get($ph, 'pack_hdr_id'),
                    'odr_hdr_id'  => array_get($ph, 'odr_hdr_id'),
                    'whs_id'      => array_get($ph, 'whs_id'),
                    'cus_id'      => array_get($ph, 'cus_id'),
                    'item_id'     => array_get($ph, 'item_id'),
                    'lot'         => array_get($ph, 'lot'),
                    'sku'         => array_get($ph, 'sku'),
                    'size'        => array_get($ph, 'size'),
                    'color'       => array_get($ph, 'color'),
                    'cus_upc'     => array_get($ph, 'upc'),
                    'uom_id'      => array_get($ph, 'ctn_uom_id'),
                    'piece_qty'   => array_get($ph, 'piece_qty'),
                    'sts'         => 'i',
                    'created_at'  => time(),
                    'updated_at'  => time(),
                    'deleted'     => 0,
                    'deleted_at'  => 915148800,
                    'created_by'  => array_get($ph, 'created_by'),
                    'updated_by'  => array_get($ph, 'updated_by'),

                ];
            }

            if (!empty($packDtls)) {
                DB::table('pack_dtl')->insert($packDtls);
            }
        }

        return [
            'eventData'    => $eventData,
            'packedOdrIds' => $packedOdrIds
        ];
    }

    public function insertDimension($length, $width, $height, $pack_type)
    {
        $dimension = sprintf("%sx%sx%s", $length, $width, $height);
        $result = DB::table("pack_ref")
            ->select('pack_ref_id')
            ->where('dimension', $dimension)
            ->where('pack_type', $pack_type)
            ->first();

        if (empty($result)) {
            $packRefModel = new PackRefModel();
            $result = $packRefModel->create([
                'width'      => $width,
                'height'     => $height,
                'length'     => $length,
                'dimension'  => $dimension,
                'pack_type'  => $pack_type,
                'created_at' => time(),
                'updated_at' => time()
            ])->toArray();
        }

        return $result['pack_ref_id'];
    }

    public function isAutoPack($order_id)
    {
        $odrMetaModel = new OdrHdrMetaModel();
        $customerMetaModel = new CustomerMetaModel();
        $order_meta = $odrMetaModel->getOrderFlow($order_id);

        $Flow = $customerMetaModel->getFlow($order_meta, 'PAK');

        if (array_get($Flow, 'usage', -1) == 0) {
            return true;
        }

        return false;
    }


}
