<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\GoodsReceipt;

class CorrectDataJob extends Job
{
    protected $request;
    protected $grHdrId;
    protected $userId;

    public function __construct($request, $grHdrId, $userId)
    {
        $this->request      = $request;
        $this->grHdrId      = $grHdrId;
        $this->userId       = $userId;
    }

    public function handle()
    {
        $grHdr = GoodsReceipt::find($this->grHdrId);

        try {
            $this->writeLogment('Goods Receipt: Auto Correct Data', 'Auto correct data for good receipt: ' . $grHdr->gr_hdr_id);

            $this->correctGoodReceiptAndPalletInfo($grHdr);

            $this->correctPalSugLocInfo($grHdr, $this->userId);

            $this->updateStatusLocationPutAwayHistory();

            $this->writeLogment('Goods Receipt: Auto Correct Data', 'Complete auto correct data job');

        } catch (\Exception $e) {
            $this->writeLogment('Goods Receipt: Auto Correct Data', $e->getMessage());
        }
    }

    private function correctGoodReceiptAndPalletInfo($grHdr)
    {
        DB::statement("
            UPDATE gr_dtl d
            JOIN
            (SELECT
                COUNT(c.ctn_id) AS ctn_ttl,
                sum(c.piece_remain) AS qty_ttl,
                COUNT(DISTINCT(p.plt_id)) AS plt_ttl,
                SUM(IF(c.is_damaged = 1, 1, 0)) AS ctn_dmg_ttl,
                d.gr_dtl_plt_ttl,
                d.gr_dtl_id
            FROM
                gr_dtl d
            JOIN gr_hdr h ON h.gr_hdr_id = d.gr_hdr_id
            JOIN cartons c ON d.gr_dtl_id = c.gr_dtl_id
            JOIN pallet p ON p.plt_id = c.plt_id
            WHERE
                h.whs_id = ?
                AND d.gr_hdr_id = ?
                AND p.deleted = 0
                AND c.deleted = 0
                AND d.deleted = 0
                AND c.ctn_sts IN ('AC')
            GROUP BY
                d.gr_dtl_id
            ) AS t ON d.gr_dtl_id = t.gr_dtl_id
            SET
                d.gr_dtl_act_ctn_ttl = t.ctn_ttl,
                d.gr_dtl_plt_ttl = t.plt_ttl,
                d.gr_dtl_dmg_ttl = t.ctn_dmg_ttl,
                d.gr_dtl_act_qty_ttl = t.qty_ttl;
        ", [$grHdr->whs_id, $grHdr->gr_hdr_id]);

        DB::statement("
            UPDATE gr_dtl d
            SET d.gr_dtl_disc_qty = d.gr_dtl_act_qty_ttl/(-1)*(-1) - d.gr_dtl_ept_qty_ttl,
                d.gr_dtl_disc = d.gr_dtl_act_ctn_ttl/(-1)*(-1) - d.gr_dtl_ept_ctn_ttl
            WHERE d.gr_hdr_id = ?;
        ", [$grHdr->gr_hdr_id]);

        DB::statement("
            UPDATE pallet
            SET pallet.ctn_ttl =
            IFNULL((
             SELECT count(*)
             FROM cartons
             WHERE cartons.whs_id = pallet.whs_id
             AND cartons.plt_id = pallet.plt_id
             AND cartons.ctn_sts IN ( 'AC', 'LK', 'AL', 'RG')
             AND cartons.deleted = 0
            ), 0)
            WHERE pallet.whs_id = ? AND pallet.gr_hdr_id= ?;
        ", [$grHdr->whs_id, $grHdr->gr_hdr_id]);

        DB::statement("
            UPDATE pallet
            SET pallet.dmg_ttl =
            IFNULL((
             SELECT count(*)
             FROM cartons
             WHERE cartons.whs_id = pallet.whs_id
             AND cartons.plt_id = pallet.plt_id
             AND cartons.ctn_sts IN ( 'AC', 'LK', 'AL', 'RG')
             AND cartons.deleted = 0
             AND cartons.is_damaged = 1
            ), 0)
            WHERE pallet.whs_id = ? AND pallet.gr_hdr_id = ?;
        ", [$grHdr->whs_id, $grHdr->gr_hdr_id]);
    }

    private function correctPalSugLocInfo($grHdr, $userId)
    {
        $time = time();

        DB::statement("
            INSERT INTO pal_sug_loc (
                id,
                plt_id,
                loc_id,
                `data`,
                ctn_ttl,
                qty_ttl,
                item_id,
                sku,
                size,
                color,
                lot,
                putter,
                gr_hdr_id,
                gr_dtl_id,
                gr_hdr_num,
                whs_id,
                created_at,
                updated_by,
                updated_at,
                created_by,
                deleted_at,
                deleted,
                put_sts,
                act_loc_id,
                act_loc_code
            )
            SELECT
                NULL as pal_sug_loc_id,
                p.plt_id as plt_id,
                p.loc_id as loc_id,
                p.loc_code as `data`,
                COUNT(DISTINCT(c.ctn_id)) AS ctn_ttl,
                COUNT(DISTINCT(c.ctn_id)) * c.ctn_pack_size AS qty_ttl,
                c.item_id as item_id,
                c.sku as sku,
                c.size as size,
                c.color as color,
                c.lot as lot,
                NULL as putter,
                c.gr_hdr_id as gr_hdr_id,
                c.gr_dtl_id as gr_dtl_id,
                gr.gr_hdr_num as gr_hdr_num,
                p.whs_id as whs_id,
                ? as created_at,
                ? as updated_by,
                ? as updated_at,
                ? as created_by,
                ? as deleted_at,
                0 as deleted,
                'CO' as put_sts,
                p.loc_id as act_loc_id,
                p.loc_code as act_loc_code
            FROM
                pallet p
                JOIN gr_hdr gr ON gr.gr_hdr_id = p.gr_hdr_id
                JOIN cartons c ON c.plt_id = p.plt_id
            WHERE
                p.whs_id = ?
                AND p.cus_id = ?
                AND p.gr_hdr_id = ?
                AND p.deleted = 0
                AND c.deleted = 0
            GROUP BY
                c.item_id,
                c.lot,
                c.plt_id
            ON DUPLICATE KEY UPDATE
                ctn_ttl = VALUES(ctn_ttl),
                qty_ttl = VALUES(qty_ttl),
                act_loc_id = VALUES(act_loc_id),
                act_loc_code = VALUES(act_loc_code),
                put_sts = VALUES(put_sts)
        ", [$time, $userId, $time, $userId, 915148800, $grHdr->whs_id, $grHdr->cus_id, $grHdr->gr_hdr_id]);
    }

    public function updateStatusLocationPutAwayHistory()
    {
        DB::statement('
            UPDATE putaway_hist
            JOIN location ON location.loc_id = putaway_hist.loc_id 
            SET putaway_hist.loc_sts_code = location.loc_sts_code 
            WHERE location.deleted = 0;
        ');
    }
}
