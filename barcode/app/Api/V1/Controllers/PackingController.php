<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2/19/2019
 * Time: 11:46 AM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\PackHdrModel;
use Carbon\Carbon;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Request as IRequest;
use Seldat\Wms2\Models\PackRef;
use Seldat\Wms2\Utils\JWTUtil;
use GuzzleHttp\Client;
use Seldat\Wms2\Utils\Message;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Models\SysBug;
use App\Jobs\BOLJob;

class PackingController extends AbstractController
{
    protected $odrHdrModel;
    protected $odrDtlModel;
    protected $itemModel;
    protected $packHdrModel;
    protected $packDtlModel;
    protected $eventTrackingModel;

    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel,
        EventTrackingModel $eventTrackingModel,
        ItemModel $itemModel
    )
    {
        $this->odrHdrModel      = $orderHdrModel;
        $this->odrDtlModel      = $orderDtlModel;
        $this->itemModel        = $itemModel;
        $this->packHdrModel     = $packHdrModel;
        $this->packDtlModel     = $packDtlModel;
        $this->eventTrackingModel = $eventTrackingModel;
    }


    public function getList(Request $request, $whsId) {
        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $result = $this->odrHdrModel->getModel()
                ->join("odr_dtl","odr_dtl.odr_id","=","odr_hdr.odr_id")
                ->join("customer","customer.cus_id","=","odr_hdr.cus_id")
                ->whereIn("odr_hdr.odr_sts", ["PD","KD","PN"])
                ->where("odr_hdr.whs_id", $whsId)
                ->groupBy("odr_hdr.odr_id")
                ->select(
                    "odr_hdr.*",
                    "customer.cus_name",
                    "customer.cus_code" ,
                    DB::raw("COUNT(odr_dtl.item_id) AS num_sku")
                )
                ->get()->toArray();

            return [
                "data" => [
                    "items" => $result
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function storePacking(Request $request, $whsId) {
        try {

            $input = $request->getParsedBody();

            $this->validatePacking($input);

            $input['sku_ttl'] = count($input['items']);

            $userId = Data::getCurrentUserId();

            DB::beginTransaction();

            // Make data of wave pick detail
            $odrHdr = $input['odrHdr'];
            $seq = 1;

            $firstpackHdrOdr = $this->packHdrModel->getModel()
                ->where("odr_hdr_id", $odrHdr->odr_id)->orderBy("pack_hdr_num", "DESC")->first();
            $prefix = str_replace_first("ORD","CTN",$odrHdr->odr_num);
            $prefix .= "-";
            $packHdrNum = $prefix."0001";

            if($firstpackHdrOdr) {
                $seq += $firstpackHdrOdr->seq;
                $packHdrNum = $this->nextPackHdrNum($firstpackHdrOdr->pack_hdr_num, $prefix);
            }

            $packRef = PackRef::where("pack_ref_id", $input['pack_ref_id'])->first();

            $packHdrParams = [
                'pack_hdr_num'     => $packHdrNum,
                'seq'              => $seq,
                'odr_hdr_id'       => $odrHdr->odr_id,
                'whs_id'           => $odrHdr->whs_id,
                'cus_id'           => $odrHdr->cus_id,
                'carrier_name'     => $odrHdr->carrier,
                'sku_ttl'          => $input['sku_ttl'],
                'piece_ttl'        => $input['total_qty'],
                'pack_sts'         => "AC",
                'sts'              => "i",
                'ship_to_name'     => $odrHdr->ship_to_name,
                'pack_ref_id'      => object_get($packRef, 'pack_ref_id', null),
                'width'            => object_get($packRef, 'width', null),
                'height'           => object_get($packRef, 'height', null),
                'length'           => object_get($packRef, 'length', null),
                'weight'           => (!empty($input['weight']))?$input['weight']:0,
                'pack_type'        => object_get($packRef, 'pack_type', 0),
                'created_at'       => time(),
                'updated_at'       => time(),
                'created_by'       => $userId,
                'updated_by'       => $userId,
                'deleted'          => 0,
                'deleted_at'       => 915148800,
            ];
            $packHdr = $this->packHdrModel->create($packHdrParams);
            $current_time = Carbon::now()->timestamp;
            $dataLaborTracking = [
                'user_id'       => $userId,
                'cus_id'        => object_get($odrHdr, 'cus_id'),
                'whs_id'        => $whsId,
                'trans_num'     => $packHdrNum,
                'trans_dtl_id'  => $packHdr->pack_hdr_id,
                'start_time'    => strtotime('-5 minutes'),
                'end_time'      => time(),
                'lt_type'       => "OB",
                'owner'         => object_get($odrHdr, "odr_num", "")
            ];
            DB::table('rpt_labor_tracking')->insert($dataLaborTracking);
            $paramPackDtl = [];

            foreach ($input['items'] as $value) {
                $item = $value['item'];
                $paramDtl = [
                    'pack_hdr_id' => $packHdr->pack_hdr_id,
                    'odr_hdr_id'  => $odrHdr->odr_id,
                    'whs_id'      => $odrHdr->whs_id,
                    'cus_id'      => $odrHdr->cus_id,
                    'item_id'     => $item->item_id,
                    'cus_upc'     => $item->cus_upc,
                    'uom_id'      => $item->uom_id,
                    'sku'         => $item->sku,
                    'size'        => $item->size,
                    'color'       => $item->color,
                    'lot'         => (!empty($value['lot']))?$value['lot']:"NA",
                    'piece_qty'   => (!empty($value['qty']))?$value['qty']:0,
                    'weight'      => $item->weight,
                    'sts'         => 'i',
                    'created_at'  => time(),
                    'updated_at'  => time(),
                    'deleted'     => 0,
                    'deleted_at'  => 915148800,
                    'created_by'  => $userId,
                    'updated_by'  => $userId,
                ];

                $paramPackDtl[] = $paramDtl;
            }

            DB::table("pack_dtl")->insert($paramPackDtl);

            $checks = DB::table("odr_dtl")
                ->leftJoin("pack_dtl", function ($q) {
                    $q->on("pack_dtl.odr_hdr_id", "=", "odr_dtl.odr_id");
                    $q->on("pack_dtl.item_id", "=", "odr_dtl.item_id");
                    $q->where("pack_dtl.deleted" , 0);
                })
                ->where("odr_dtl.deleted", 0)
                ->where("odr_dtl.odr_id", $input['odr_id'])->where("odr_dtl.whs_id", $whsId)
                ->where("odr_dtl.cus_id", $input['cus_id'])->groupBy("odr_dtl.item_id")
                ->select(
                    "odr_dtl.item_id",
                    "odr_dtl.sku",
                    "odr_dtl.lot", "odr_dtl.piece_qty",
                    DB::raw("SUM(pack_dtl.piece_qty) as pack_qty")
                )->get()->toArray();
            
            $isPacked = true;

            foreach ($checks as $check) {
                if(empty($check['pack_qty']) || $check['pack_qty'] < $check['piece_qty']) {
                    $isPacked = false;
                    break;
                }
            }
            $event = [
                'whs_id'    => $odrHdr->whs_id,
                'cus_id'    => $odrHdr->cus_id,
                'owner'     => $odrHdr->odr_num,
                'evt_code'  => 'OPS',
                'trans_num' => $packHdrNum,
                'info'      => sprintf("Create pack carton"),
            ];

            //  Evt tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($event);

            $odrHdr->odr_sts = "PN";
            if($isPacked) {
                $odrHdr->odr_sts = "PA";

                $event = [
                    'whs_id'    => $odrHdr->whs_id,
                    'cus_id'    => $odrHdr->cus_id,
                    'owner'     => $odrHdr->odr_num,
                    'evt_code'  => 'OPC',
                    'trans_num' => $odrHdr->odr_num,
                    'info'      => sprintf("Packed Complete"),
                ];

                //  Evt tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create($event);
            }
            $odrHdr->save();

            DB::commit();

            // if($odrHdr->odr_type == 'RTL' ) {
            //     $this->updatePackTrackNum($request, $packHdr);
            // }

            // Auto Create BOL
            dispatch(new BOLJob($odrHdr->odr_id, $whsId, $request));

            return [
                "data" => [
                    "message" => "Submit Successfully!",
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getListItemPack(Request $request, $whsId) {
        try {

            $input = $request->getQueryParams();

            $this->validateListItemPack($input);

            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $results = $this->itemModel->getModel()->join("odr_dtl","odr_dtl.item_id","=","item.item_id")
                ->leftJoin("pack_dtl", function ($q) {
                    $q->on("pack_dtl.odr_hdr_id", "=", "odr_dtl.odr_id");
                    $q->where("pack_dtl.deleted" , 0);
                })
                ->where("odr_dtl.deleted", 0)
                ->where("odr_dtl.odr_id", $input['odr_id'])->where("odr_dtl.whs_id", $whsId)
                ->where("odr_dtl.cus_id", $input['cus_id'])->groupBy("item.item_id")
                ->select(
                    "item.item_id",
                    "item.sku",
                    "item.cus_upc",
                    "odr_dtl.lot",
                    "odr_dtl.piece_qty",
                    DB::raw("SUM(pack_dtl.piece_qty) as pack_qty")
                )->get()->toArray();

            foreach ($results as $key => $result) {
                $results[$key]['pack_qty'] = ($result['pack_qty'])?$result['pack_qty']:0;
                $results[$key]['qty'] = $result['piece_qty'] - $result['pack_qty'];
                $results[$key]['upc'] = ($result['cus_upc'])?$result['cus_upc']:"";
            }

            return [
                "data" => [
                    "items" => $results
                ]

            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getListOutsideBox(Request $request, $whsId) {
        try {
            $result = DB::table("pack_ref")->where("deleted",0)
                ->select(
                    "pack_ref_id",
                    "width",
                    "height",
                    "length",
                    "dimension",
                    "pack_type"
                    )->get();

            return [
                "data" => [
                    "items" => $result
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function validateListItemPack(&$attribute) {
        if(empty($attribute['odr_id'])) {
            return $this->response->errorBadRequest('Field odr_id is missing!');
        }

        if(empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest('Field cus_id is missing!');
        }

        $orderHdr = $this->odrHdrModel->getFirstWhere([
            'odr_id'    => $attribute['odr_id'],
            'cus_id'    => $attribute['cus_id']
        ]);

        if(empty($orderHdr)) {
            return $this->response->errorBadRequest('Order is not exit!');
        }

        $attribute['odrHdr'] = $orderHdr;
    }

    protected function validatePacking(&$attribute) {
        if(empty($attribute['odr_id'])) {
            return $this->response->errorBadRequest('Field odr_id is missing!');
        }

        if(empty($attribute['cus_id'])) {
            return $this->response->errorBadRequest('Field cus_id is missing!');
        }

        if(empty($attribute['pack_ref_id'])) {
            return $this->response->errorBadRequest('Field out_box_num is missing!');
        }

        if(empty($attribute['weight'])) {
            return $this->response->errorBadRequest('Field weight is missing!');
        }

        if(empty($attribute['items']) || count($attribute['items']) == 0) {
            return $this->response->errorBadRequest('Field items is missing!');
        }

        $orderHdr = $this->odrHdrModel->getFirstWhere([
            'odr_id'    => $attribute['odr_id'],
            'cus_id'    => $attribute['cus_id']
        ]);

        if(empty($orderHdr)) {
            return $this->response->errorBadRequest('Order is not exit!');
        }

        if(!in_array($orderHdr->odr_sts, ["PD", "PN", "KD"])) {
            return $this->response->errorBadRequest('Order status does not allow to pack!');
        }

        $attribute['odrHdr'] = $orderHdr;
        $total_qty = 0;
        foreach ($attribute['items'] as $key => $value) {
            if(empty($value['item_id'])) {
                return $this->response->errorBadRequest('Field items is missing!');
            }

            $item = $this->itemModel->getFirstWhere([
                "item_id" => $value['item_id']
            ]);

            if(empty($value['item_id'])) {
                $msn = 'Items '.$value["sku"].' is not exit!';
                return $this->response->errorBadRequest($msn);
            }
            $attribute['items'][$key]['item'] = $item;
            $total_qty += $value['qty'];
        }
        $attribute['total_qty'] = $total_qty;

        return 0;
    }

    protected function nextPackHdrNum($code , $prefix) {
        $num = str_replace_first($prefix, "",$code);
        ++$num;
        $newnum = str_pad($num, 4, STR_PAD_LEFT);
        return $prefix.$newnum;
    }

    public function updatePackTrackNum($request, $packHdr) {
        $client = new Client(['headers' => $request->getHeaders()]);
        $url = env('API_PACKING') . '/v2/packs/get-track-num/'.$packHdr->pack_hdr_id;
        $conn = $client->get($url,[
            'json' => $request
        ]);

        $respon = \GuzzleHttp\json_decode($conn->getBody()->getContents());
        $track_num = $respon->data->track_num;

        $this->packHdrModel->getModel()->where("pack_hdr_id",$packHdr->pack_hdr_id)
            ->update(['tracking_number' => $track_num]);
    }

    public function autoStorePacking(Request $request, $whsId) {
        $input = $request->getParsedBody();
        $item_ids = array_column($input, 'item_id');

        $param = $this->odrDtlModel->getModel()->select('odr_dtl_id', 'item_id as itm_id')->where("odr_id",$input[0]['odr_id'])
            ->whereIn("item_id",$item_ids)->get();
        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $item_ids = array_column($input, 'item_id');

            $param = $this->odrDtlModel->getModel()->select('odr_dtl_id', 'item_id as itm_id')->where("odr_id",$input[0]['odr_id'])
                ->whereIn("item_id",$item_ids)->get()->toArray();
            $client = new Client();

            $response = $client->post(env('API_PACK')  . $input[0]['odr_id'],
                [
                    'headers'     => ['Authorization' => $request->getHeader('Authorization')],
                    'form_params' => $param
                ]
            );

            // Auto Create BOL
            dispatch(new BOLJob($input[0]['odr_id'], $whsId, $request));
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }




}