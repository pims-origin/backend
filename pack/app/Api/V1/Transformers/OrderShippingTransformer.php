<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderShippingInfo;

/**
 * Class OrderShippingTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderShippingTransformer extends TransformerAbstract
{
    /**
     * @param OrderShippingInfo $orderShipping
     *
     * @return array
     */
    public function transform(OrderShippingInfo $orderShipping)
    {
        return [
            'ord_shipping_id'  => $orderShipping->ord_shipping_id,
            'ship_to_cus_name' => $orderShipping->ship_to_cus_name,
            'ship_to_addr'     => $orderShipping->ship_to_addr,
            'ship_to_city'     => $orderShipping->ship_to_city,
            'ship_to_state'    => $orderShipping->ship_to_state,
            'ship_to_zip'      => $orderShipping->ship_to_zip,
            'ship_to_country'  => $orderShipping->ship_to_country,
            'ship_by_dt'       => $orderShipping->ship_by_dt,
            'ship_dt'          => $orderShipping->ship_dt,
        ];
    }
}
