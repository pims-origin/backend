<?php
/**
 * Created by PhpStorm.
 * User: PhucTran
 * Date: 3/30/2017
 * Time: 3:23 PM
 */

namespace App\Api\V1\Transformers;


use App\Api\V1\Models\ItemMeta;
use League\Fractal\TransformerAbstract;

class ItemImageTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     * @return array
     */
    public function transform(ItemMeta $item)
    {
        $selectUser = [
            'user_id',
            'first_name',
            'last_name',
            'email',
            'phone',
            'phone_extend',
            'mobile'
        ];
        $item->createdByUser = $item->createdByUser()->select($selectUser)->first();
        $item->updatedByUser = $item->updatedByUser()->select($selectUser)->first();
        return array_merge($item->toArray(), [

        ]);
    }
}