<?php


namespace App\Api\V1\Models;

use phpDocumentor\Reflection\Types\Null_;
use Seldat\Wms2\Models\Carton;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\LocationType;

class LockModel extends AbstractModel
{
    const STATUS_LOCK = 'LK';
    const STATUS_ACTIVE = 'AC';
    const STATUS_INACTIVE = 'IA';
    const STATUS_ADJUSTED = 'AJ';

    /**
     * LockModel constructor.
     *
     * @param CycleHdr $cycleHdr
     */
    public function __construct($cycleHdr = null)
    {
        $this->model = $cycleHdr ? $cycleHdr : new CycleHdr();
    }

    /**
     * @param null $type
     *
     * @return int
     */
    public static function getLocTypeId($type = null)
    {
        $locTypeId = 0;
        $type = $type ? $type : LocationModel::TYPE_RACK;

        $result = LocationType::select('loc_type_id')
            ->where('loc_type_code', $type)
            ->first();

        $locTypeId = $result ? $result->loc_type_id : $locTypeId;

        return $locTypeId;
    }

    /**
     * @param $locIds
     *
     * @return bool
     */
    public static function lockLocationByIds($whsId, $locIds)
    {
        if (!$locIds) {
            return false;
        }

        $locTypeId = self::getLocTypeId();
        $locIds = array_unique($locIds);
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            Location::whereIn('loc_id', $chunkLocIds)
                ->where('loc_whs_id', $whsId)
                ->where('loc_type_id', $locTypeId)
                ->where('loc_sts_code', self::STATUS_ACTIVE)
                ->update([
                    'loc_sts_code' => self::STATUS_LOCK
                ]);
        }
        return true;
    }

    /**
     * @param $whsId
     * @param $itemIds
     * @param $locIDs
     *
     * @return mixed
     */
    public static function lockCartonsByCycleHdr($whsId, $itemIds, $locIDs)
    {
        if (!($whsId && $itemIds && $locIDs)) {
            return false;
        }
        $itemIds = array_unique($itemIds);
        $locIDs = array_unique($locIDs);
        $locTypeId = self::getLocTypeId();

        foreach (array_chunk($locIDs, 200) as $chunkLocIds) {
            DB::table('cartons')
                ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
                ->join('item', 'item.item_id', '=', 'cartons.item_id')
                ->where('location.loc_type_id', $locTypeId)
                ->where('cartons.whs_id', $whsId)
                ->where('cartons.deleted', 0)
                ->where('location.deleted', 0)
                ->where('cartons.ctn_sts', '=', self::STATUS_ACTIVE)
                ->where('item.status', '=', self::STATUS_ACTIVE)
                ->where('location.loc_sts_code', '=', self::STATUS_ACTIVE)
                ->whereIn('cartons.item_id', $itemIds)
                ->whereIn('cartons.loc_id', $chunkLocIds)
                ->update([
                    'cartons.ctn_sts' => self::STATUS_LOCK
                ]);
        }

        return true;
    }

    public static function updateLockQtyInInventorySummary($whsId, $itemIds, $locIDs) {
        if (empty($itemIds) && empty($locIDs)) {
            return false;
        }
        $itemIds = array_unique($itemIds);
        $locIDs = array_unique($locIDs);
        $itemIdsByLoc = DB::table('cartons')
            ->select(DB::raw('DISTINCT item_id'))
            ->whereIn('loc_id', $locIDs)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('cartons.lot', "!=", "ECO")
            ->where('cartons.is_damaged', 0)
            ->get();
        $itemIdsByLoc = array_column($itemIdsByLoc, 'item_id');
        $itemIds = array_unique(array_merge($itemIds, $itemIdsByLoc));
        if (empty($itemIds)) {
            return false;
        }
        // Update lock_qty for invt_smr
        $query = DB::table('cartons')
            ->select([
                'cartons.item_id',
                'cartons.lot',
                DB::raw("SUM(if(cartons.ctn_sts = 'LK', cartons.piece_remain, 0)) as lock_qty")
            ])
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('cartons.lot', "!=", "ECO")
            ->where('cartons.is_damaged', 0);
        if (!empty($itemIds)) {
            $query->whereIn('cartons.item_id', $itemIds);
        }
        $query->groupBy([
            'cartons.item_id',
            'cartons.lot'
        ]);

        $lockQtyByItemLot = $query->get();

        foreach($lockQtyByItemLot as $item) {
            DB::table('invt_smr')
                ->where('invt_smr.item_id', $item['item_id'])
                ->where('invt_smr.lot', $item['lot'])
                ->where('invt_smr.whs_id', $whsId)
                ->update([
                    'invt_smr.lock_qty' => (int)$item['lock_qty']
                ]);
        }
    }

    /**
     * @param $whsId
     * @param string $by
     * @param null $data
     * @param string $newStatus
     *
     * @return bool
     */
    public static function updateStatusCartonsByType($params)
    {
        $whsId = $params['whsId'];
        $by = isset($params['by']) ? $params['by'] : CycleHdrModel::CYCLE_TYPE_CUSTOMER;
        $itemIDs = $params['itemIDs'];
        $newStatus = isset($params['newStatus']) ? $params['newStatus'] : self::STATUS_ACTIVE;

        if (!($itemIDs && $whsId)) {
            return false;
        }

        $oldStatus = ($newStatus == self::STATUS_ACTIVE) ? self::STATUS_LOCK : self::STATUS_ACTIVE;

        $locTypeId = self::getLocTypeId();

        $query = DB::table('cartons')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->where('location.loc_type_id', $locTypeId)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('location.deleted', 0)
            ->where('cartons.ctn_sts', '=', $oldStatus)
//            ->where('location.loc_sts_code', '=', $oldStatus)
            ->whereIn('cartons.item_id', $itemIDs);
        $locIds = [];
        if ($by == CycleHdrModel::CYCLE_TYPE_LOCATION) {
            $locIds = array_unique($params['locIds']);
            $query->whereIn('cartons.loc_id', $locIds);
        }

        $ctnIds = $query->pluck('ctn_id');

        //$result = $query->update([
            //'cartons.ctn_sts'       => $newStatus,
            //'location.loc_sts_code' => $newStatus
        //]);

        $ctnIds = array_unique($ctnIds);
        foreach (array_chunk($ctnIds, 200) as $chunkCtnIds) {
            DB::table('cartons')->whereIn('ctn_id', $chunkCtnIds)->update(['ctn_sts' => $newStatus]);
        }

        self::updateLockQtyInInventorySummary($whsId, $itemIDs, $locIds);
        //Fix bug swis-840
        if ($by == CycleHdrModel::CYCLE_TYPE_LOCATION) {
            LockModel::updateStatusLocations($params['locIds'], $whsId);
        }

        //Fix bug swis-840
        if ($by == CycleHdrModel::CYCLE_TYPE_LOCATION) {
            LockModel::updateStatusLocations($params['locIds'], $whsId);
        }

        return true;
    }

    /**
     * @param $locIDs
     * @param $whsId
     * @param string $newStatus
     *
     * @return bool
     */
    public static function updateStatusLocations($locIDs, $whsId, $newStatus = self::STATUS_ACTIVE)
    {
        if (!($locIDs && $whsId)) {
            return false;
        }
        $oldStatus = ($newStatus == self::STATUS_ACTIVE) ? self::STATUS_LOCK : self::STATUS_ACTIVE;

        $locIDs = (array)$locIDs;
        $locIDs = array_unique($locIDs);

        $locTypeRacId = LockModel::getLocTypeId();

        foreach (array_chunk($locIDs, 200) as $chunkLocIds) {
            DB::table('location')
                ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
                ->where('location.loc_type_id', $locTypeRacId)
                ->where('location.loc_whs_id', $whsId)
                ->where('location.deleted', 0)
                ->where('location.loc_sts_code', $oldStatus)
                ->whereIn('location.loc_id', $chunkLocIds)
                ->update([
                    'location.loc_sts_code' => $newStatus
                ]);
        }

        return true;
    }

    /**
     * @param $whsId
     * @param null $data
     * @param string $newStatus
     *
     * @return bool
     */
    public static function updateStatusCartons($whsId, $data = null, $newStatus = self::STATUS_ACTIVE)
    {
        if (!($data && $whsId)) {
            return false;
        }

        $oldStatus = ($newStatus == self::STATUS_ACTIVE) ? self::STATUS_LOCK : self::STATUS_ACTIVE;

        $result = DB::table('cartons')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('location.deleted', 0)
            ->where('cartons.ctn_sts', '=', $oldStatus)
            ->whereIn('cartons.item_id', $data)
            ->where('loc_type.loc_type_code', LocationModel::TYPE_RACK)
            ->update([
                'cartons.ctn_sts' => $newStatus
            ]);
        self::updateLockQtyInInventorySummary($whsId, $data, []);
        return $result;
    }

    /**
     * @param $cycleHdrObj
     *
     * @return null
     */
    public function unClockAllLoc($cycleHdrObj)
    {
        $result = null;
        $locIdsActive = [];

        if ($cycleHdrObj->cycle_type === CycleHdrModel::CYCLE_TYPE_CUSTOMER) {

            $locIdsActive = LocationModel::getLocationIdsByCustomer(
                $cycleHdrObj->whs_id, $cycleHdrObj->cycle_detail, self::STATUS_LOCK
            );

        } elseif ($cycleHdrObj->cycle_type === CycleHdrModel::CYCLE_TYPE_LOCATION) {

            $locIdsActive = explode(',', $cycleHdrObj->cycle_detail);
        }

        if (!$locIdsActive) {
            return $result;
        }

        $locIdsActive = array_unique($locIdsActive);
        foreach (array_chunk($locIdsActive, 200) as $chunkLocIdsActive) {
            Location::whereIn('loc_id', $chunkLocIdsActive)
                ->update([
                    'loc_sts_code' => self::STATUS_ACTIVE
                ]);
        }

        return $result;
    }

    /**
     * @param $whsId
     * @param $itemIds
     * @param $locIds
     *
     * @return bool
     */
    public static function unLockCartonByItems($whsId, $itemIds, $locIds)
    {
        if (!($itemIds && $whsId && $locIds)) {
            return false;
        }

        $oldStatus = self::STATUS_LOCK;
        $newStatus = self::STATUS_ACTIVE;

        $result = DB::table('cartons')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->join('item', 'item.item_id', '=', 'cartons.item_id')
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('location.deleted', 0)
            ->where('cartons.ctn_sts', '=', $oldStatus)
            ->where('loc_type.loc_type_code', LocationModel::TYPE_RACK)
            ->whereIn('location.loc_sts_code', [$oldStatus, $newStatus])
            ->whereIn('cartons.item_id', $itemIds)
            ->whereIn('cartons.loc_id', $locIds)
            ->update([
                'cartons.ctn_sts' => $newStatus
            ]);

        return $result;
    }

    /**
     * @param $locIds
     *
     * @return bool
     */
    public static function unLockLocations($locIds)
    {
        if (!$locIds) {
            return false;
        }

        //unclock location
        $locIds = array_unique($locIds);
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            DB::table('location')
                ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                ->where('loc_type.loc_type_code', LocationModel::TYPE_RACK)
                ->whereIn('location.loc_id', $chunkLocIds)
                ->where('location.loc_sts_code', self::STATUS_LOCK)
                ->update([
                    'location.loc_sts_code' => self::STATUS_ACTIVE
                ]);
        }

        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            //unclock cartons
            Carton::whereIn('loc_id', $chunkLocIds)
                ->where('ctn_sts', 'LK')
                ->update(['ctn_sts' => 'AC']);
        }

        return true;
    }
}