<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ReasonModel;
use App\Api\V1\Transformers\ReasonTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class ReasonController extends AbstractController
{
    /**
     * @var ReasonModel
     */
    protected $reasonModel;

    /**
     * @var $reasonTransformer
     */
    protected $reasonTransformer;

    /**
     * ReasonController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->reasonModel = new ReasonModel();
        $this->reasonTransformer = new ReasonTransformer();
    }

    /**
     * API Search Location Type
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $Reason = $this->reasonModel->search($input, [], array_get($input, 'limit'));

            return $this->response->paginator($Reason, $this->reasonTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
