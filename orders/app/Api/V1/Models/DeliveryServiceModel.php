<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\DeliveryService;
use Seldat\Wms2\Utils\SelArr;

class DeliveryServiceModel extends AbstractModel
{
    /**
     * @param Carton $model
     */
    public function __construct(DeliveryService $model = null)
    {
        $this->model = ($model) ?: new DeliveryService();
    }

    /**
     * @return mixed
     */
    public function getList()
    {
        $query = $this->model
                ->select(['ds_id', 'ds_key', 'ds_value'])
                ->get();
        return $query;
    }
}
