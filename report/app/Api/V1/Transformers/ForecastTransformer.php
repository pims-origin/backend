<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-June-2017
 * Time: 09:27
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Forecast;


class ForecastTransformer extends TransformerAbstract
{
    public function transform(Forecast $forecast)
    {
        $fc_ib              = (float)object_get($forecast, 'fc_ib' , 0);
        $fc_ib_act          = (float)object_get($forecast, 'fc_ib_act' , 0);

        $fc_ob              = (float)object_get($forecast, 'fc_ob' , 0);
        $fc_ob_act          = (float)object_get($forecast, 'fc_ob_act' , 0);

        $fc_st              = (float)object_get($forecast, 'fc_st' , 0);
        $fc_st_act          = (float)object_get($forecast, 'fc_st_act' , 0);

        if ($forecast['fc_ind_code'] != 'CUB'){
            $fc_ib_act = round($fc_ib_act);
            $fc_ob_act = round($fc_ob_act);
            $fc_st_act = round($fc_st_act);
        }

        $fc_ib_var          = $fc_ib_act - $fc_ib;
        $fc_ib_percent      = $fc_ib != 0 ? $fc_ib_act / $fc_ib * 100 : 0;

        $fc_ob_var          = $fc_ob_act - $fc_ob;
        $fc_ob_percent      = $fc_ob != 0 ? $fc_ob_act / $fc_ob * 100 : 0;

        $fc_st_var          = $fc_st_act - $fc_st;
        $fc_st_percent      = $fc_st != 0 ? $fc_st_act / $fc_st * 100 : 0;

        $fc_st_1_period_ago = number_format(object_get($forecast, 'fc_st_1_period_ago' , 0), 2);
        $fc_st_2_period_ago = number_format(object_get($forecast, 'fc_st_2_period_ago' , 0), 2);
        $fc_st_3_period_ago = number_format(object_get($forecast, 'fc_st_3_period_ago' , 0), 2);

        $data = [
            "fc_ib"                     => number_format($fc_ib, 2),
            "fc_ib_act"                 => number_format($fc_ib_act, 2),
            "fc_ib_var"                 => number_format($fc_ib_var, 2),
            "fc_ib_percent"             => number_format($fc_ib_percent, 2),

            "fc_ob"                     => number_format($fc_ob, 2),
            "fc_ob_act"                 => number_format($fc_ob_act, 2),
            "fc_ob_var"                 => number_format($fc_ob_var, 2),
            "fc_ob_percent"             => number_format($fc_ob_percent, 2),

            "fc_st"                     => number_format($fc_st, 2),
            "fc_st_act"                 => number_format($fc_st_act, 2),
            "fc_st_var"                 => number_format($fc_st_var, 2),
            "fc_st_percent"             => number_format($fc_st_percent, 2),

            "fc_st_1_period_ago"         => $fc_st_1_period_ago,
            "fc_st_2_period_ago"         => $fc_st_2_period_ago,
            "fc_st_3_period_ago"         => $fc_st_3_period_ago,

            "fc_ind_id"                 => object_get($forecast, 'fc_ind_id' , 0),
            "fc_ind_code"               => object_get($forecast, 'fc_ind_code' , ''),
            "fc_ind_name"               => object_get($forecast, 'fc_ind_name' , ''),
        ];

        return $data;
    }

}
