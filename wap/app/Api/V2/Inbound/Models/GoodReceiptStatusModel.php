<?php

namespace App\Api\V2\Inbound\Models;

use Seldat\Wms2\Models\GoodsReceiptStatus;

class GoodReceiptStatusModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model =  new GoodsReceiptStatus();
    }



}
