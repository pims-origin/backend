<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\GoodsReceiptDetailModel;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;


class GoodsReceiptListTransformer extends TransformerAbstract
{
    public function transform(GoodsReceipt $goodsReceipt)
    {
        $grDtl = GoodsReceiptDetail::select([
            DB::raw("
                sum(case when gr_dtl_disc != 0 then 1 else 0 end)  AS countIsDisrep,
                sum(gr_dtl_is_dmg)  AS countIsDamage,
                sum(case when gr_dtl_sts = 'PD' then 1 else 0 end)  AS pending,
                sum(gr_dtl_ept_ctn_ttl) AS gr_ctn_ttl,
                sum(gr_dtl_act_ctn_ttl) AS gr_act_ctn_ttl,
                count(gr_dtl_id) as item_ttl
            ")
        ])
            ->orWhere('gr_hdr_id', $goodsReceipt->gr_hdr_id)
            ->groupBy('gr_hdr_id')
            ->first()
        ;

        $xDock = AsnDtl::where('asn_hdr_id', object_get($goodsReceipt, 'asn_hdr_id', 0))
            ->where('ctnr_id', object_get($goodsReceipt, 'ctnr_id', 0))->sum('asn_dtl_crs_doc');

        $created_at = object_get($goodsReceipt, 'created_at', '');

        $created_at = ($created_at) ? date('m/d/Y', is_int($created_at) ?: $created_at->timestamp) : '';

        $act_date = object_get($goodsReceipt, 'gr_hdr_act_dt', '');
        $act_date = ($act_date) ? date('m/d/Y', is_int($act_date) ?$act_date: $act_date->timestamp) : '';

        //#5980 get expected date
        $grExpDt = object_get($goodsReceipt, 'gr_hdr_ept_dt', '');
        $grExpDt = ($grExpDt) ? date('m/d/Y', is_int($grExpDt) ?$grExpDt: $grExpDt->timestamp) : '';

        $putawaySts = object_get($goodsReceipt, 'putaway', 0);
        $putawaySts = $putawaySts == 1 ? "CO" : "NW";

        // $palletSuggestLocation = object_get($goodsReceipt, 'palletSuggestLocation', null);

        // if(!empty($palletSuggestLocation)){
        //     $totalCount = array_pluck($palletSuggestLocation->toArray(), 'put_sts');
        //     $totalCount = array_map(function ($e) {
        //         return $e === null ? "NULL" : $e;
        //     }, $totalCount);

        //     $count = array_count_values($totalCount);
        //     $putawaySts = "NW";
        //     if (!empty($count["CO"]) && $count["CO"] == count($totalCount)) {
        //         $putawaySts = "CO";
        //     }
        // }

        return [
            'asn_type'       => object_get($goodsReceipt, 'asnHdr.asn_type', null),
            'asn_hdr_num'    => object_get($goodsReceipt, 'asnHdr.asn_hdr_num', null),
            'asn_hdr_ref'    => object_get($goodsReceipt, 'asnHdr.asn_hdr_ref', null),
            'gr_hdr_id'      => object_get($goodsReceipt, 'gr_hdr_id', null),
            'ctnr_num'       => object_get($goodsReceipt, 'ctnr_num', ''),
            'gr_hdr_num'     => object_get($goodsReceipt, 'gr_hdr_num', null),
            'gr_sts_name'    => object_get($goodsReceipt, 'goodsReceiptStatus.gr_sts_name'),
            'whs_id'         => object_get($goodsReceipt, 'whs_id', null),
            'cus_id'         => object_get($goodsReceipt, 'cus_id', null),
            'cus_name'       => object_get($goodsReceipt, 'customer.cus_name', null),
            'cus_code'       => object_get($goodsReceipt, 'customer.cus_code', null),
            'gr_hdr_act_dt'  => $act_date,
            'putaway_dt' => empty($goodsReceipt->putaway_dt)?null: date('m/d/Y', $goodsReceipt->putaway_dt) ,
            'gr_ctn_ttl'     => $grDtl->gr_ctn_ttl,
            "gr_act_ctn_ttl" => $grDtl->gr_act_ctn_ttl,
            'created_at'     => $created_at,
            'gr_hdr_ept_dt'   => $grExpDt,
            // User
            'putter_id'      => $goodsReceipt->putter,
            'putter_name'    => trim(object_get($goodsReceipt, 'putterUser.first_name', null) . " " .
                object_get($goodsReceipt, 'putterUser.last_name', null)),
            "gr_crs_doc"     => $xDock,
            "gr_dtl_is_dmg"  => $grDtl->countIsDamage == 0 ? 0 : 1,
            "gr_dtl_disc"    => $grDtl->countIsDisrep == 0 ? 0 : 1,

            "user_id"     => $goodsReceipt->created_by,
            "user_name"   => trim(object_get($goodsReceipt, "user.first_name", null) . " " .
                object_get($goodsReceipt, "user.last_name", null)),
            'item_ttl'    => $grDtl->item_ttl,
            'is_pending'  => $grDtl->pending > 0 ? 1 : 0,
            'putaway_sts' => $putawaySts,
            'created_from' => object_get($goodsReceipt, 'created_from', null),
            'asn_hdr_act_dt' => (object_get($goodsReceipt, 'asnHdr.asn_hdr_act_dt', null)) ? date('m/d/Y', object_get($goodsReceipt, 'asnHdr.asn_hdr_act_dt', null)) : '',
            'is_approve_all' => object_get($goodsReceipt, 'is_approve_all', null),
        ];
    }
}
