<?php

namespace App\Api\Respositories;

use App\BillableDtl;
use App\InvoiceHeader;
use Illuminate\Http\Request as IlluminateRequest;
use Illuminate\Support\Facades\DB;

abstract class BaseRepository
{
    /**
     * [getById description]
     * @param  [type] $id      [description]
     * @param  array  $with    [description]
     * @param  array  $columns [description]
     * @return [type]          [description]
     */
    public function getById($id, $with = [], $columns = ['*'])
    {
        return $this->model
            ->when(count($with) > 0, function ($q) use ($with) {
                return $q->with($with);
            })
            ->find($id, $columns);
    }
    /**
     * [getAll description]
     * @param  IlluminateRequest $request [description]
     * @param  array             $with    [description]
     * @return [type]                     [description]
     */
    public function getAll(IlluminateRequest $request, $with = [])
    {
        return $this->model
            ->when(count($with) > 0, function ($q) use ($with) {
                return $q->with($with);
            })
            ->get();
    }
    /**
     * [getPagination description]
     * @param  IlluminateRequest $request [description]
     * @param  array             $with    [description]
     * @return [type]                     [description]
     */
    public function getPagination(IlluminateRequest $request, $with = [])
    {
        $query    = $this->model;
        $table    = $query->getTable();
        $fillAble = $query->getFillAble();
        $columns  = [$table . '.*'];

        if ($query instanceof BillableDtl) {
            $query = $this->buildBillableDetailQuery($query, $request, $table);
        }

        if ($query instanceof InvoiceHeader) {
            $query = $this->buildInvoiceHeaderQuery($query, $request, $table);
        }

        if ($sort = $request->get('sort', ['updated_at' => 'DESC'])) {
            $query = $this->buildSortQuery($query, $sort, $fillAble);
        }

        return $query->when($request->get('cus_id'), function ($q) use ($request, $table) {
            return $q->where($table . '.cus_id', $request->get('cus_id'));
        })
            ->when(count($with) > 0, function ($q) use ($with) {
                return $q->with($with);
            })
            ->paginate($request->get('limit', 20), $columns);
    }
    /**
     * [buildSortQuery description]
     * @param  [type] $query    [description]
     * @param  array  $sort     [description]
     * @param  array  $fillAble [description]
     * @return [type]           [description]
     */
    private function buildSortQuery($query, array $sort, array $fillAble)
    {
        return $query->when(count($sort) > 0, function ($q) use ($sort, $fillAble) {
            foreach ($sort as $key => $value) {
                if (in_array($key, $fillAble)) {
                    return $q->orderBy($key, $value);
                } elseif ($key === 'updated_at') {
                    return $q->orderBy($key, $value);
                } elseif ($key === 'amount') {
                    return $q->orderBy('total', $value);
                } elseif ($key === 'issue_date') {
                    return $q->orderBy('issue_dt', $value);
                } elseif ($key === 'due_date') {
                    return $q->orderBy('due_dt', $value);
                } elseif ($key === 'created_name') {
                    if ($q->getModel() instanceof InvoiceHeader) {
                        return $q->leftJoin('users', 'users.user_id', '=', 'inv_hdr.created_by')->orderBy(DB::raw('concat(users.first_name,\' \',users.last_name)'), $value);
                    }
                } elseif ($key === 'approved_name') {
                    if ($q->getModel() instanceof InvoiceHeader) {
                        return $q->leftJoin('users', 'users.user_id', '=', 'inv_hdr.approved_by')->orderBy(DB::raw('concat(users.first_name, \' \', users.last_name)'), $value);
                    }
                } elseif ($key === 'cus_name') {
                    if ($q->getModel() instanceof InvoiceHeader) {
                        return $q->leftJoin('customer', 'customer.cus_id', '=', 'inv_hdr.cus_id')->orderBy('customer.cus_name', $value);
                    }
                }
            }
        });
    }
    /**
     * [buildInvoiceHeaderQuery description]
     * @param  [type] $query   [description]
     * @param  [type] $request [description]
     * @param  [type] $table   [description]
     * @return [type]          [description]
     */
    private function buildInvoiceHeaderQuery($query, $request, $table)
    {
        return $query->when($request->get('inv_num'), function ($q) use ($request) {
            return $q->where('inv_num', 'LIKE', '%' . $request->get('inv_num') . '%');
        })
            // ->when($request->get('whs_id'), function ($q) use ($request, $table) {
            //     return $q->where($table . '.whs_id', $request->get('whs_id'));
            // })
            ->when($request->get('cus_id'), function ($q) use ($request, $table) {
                return $q->where($table . '.cus_id', $request->get('cus_id'));
            })
            ->when($request->get('issue_dt'), function ($q) use ($request) {
                return $q->where('issue_dt', strtotime($request->get('issue_dt')));
            })
            ->when(($request->get('inv_from') && $request->get('inv_to')), function ($q) use ($request, $table) {
                return $q->whereBetween($table . '.created_at', [
                    strtotime($request->get('inv_from')),
                    strtotime($request->get('inv_to')),
                ]);
            })
            ->when(($request->get('inv_from') && !$request->get('inv_to')), function ($q) use ($request, $table) {
                return $q->where($table . '.created_at', '>=', strtotime($request->get('inv_from')));
            })
            ->when((!$request->get('inv_from') && $request->get('inv_to')), function ($q) use ($request) {
                return $q->where($table . '.created_at', '<=', strtotime($request->get('inv_to')));
            })
            ->when($request->get('created_by'), function ($q) use ($request) {
                return $q->where('created_by', $request->get('created_by'));
            })
            ->when(count(array_filter($request->get('inv_sts', []))), function ($q) use ($request) {
                return $q->whereIn('inv_sts', $request->get('inv_sts'));
            })
            ->when($request->get('list_type') == 'approve' && empty($request->get('inv_sts')), function ($q) use ($request) {
                return $q->whereIn('inv_sts', ['AP', 'PE']);
            })
            ->when($request->get('email_sts'), function ($q) use ($request, $table) {
                return $q->where($table . '.email_sts', $request->get('email_sts'));
            });
    }
    /**
     * [buildBillableDetailQuery description]
     * @param  [type] $query   [description]
     * @param  [type] $request [description]
     * @param  array  $table   [description]
     * @return [type]          [description]
     */
    private function buildBillableDetailQuery($query, $request, $table)
    {
        $where = $request->get('is_selected') == 'true' ? 'whereIn' : 'whereNotIn';
        
        return $query->when($request->get('sac'), function ($q) use ($request) {
            return $q->where(function ($q) use ($request) {
                return $q->where('sac', 'LIKE', '%' . $request->get('sac') . '%')
                    ->orWhere('sac_name', 'LIKE', '%' . $request->get('sac') . '%');
            });
        })
            ->when($request->get('whs_id'), function ($q) use ($request, $table) {
                return $q->where($table . '.whs_id', $request->get('whs_id'));
            })
            ->when($request->get('ba_dtl_ids'), function ($q) use ($request, $where) {
                return $q->{$where}('ba_dtl_id', $request->get('ba_dtl_ids'));
            });
    }
}
