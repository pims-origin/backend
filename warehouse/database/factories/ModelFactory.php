<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name'  => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(App\Warehouse::class, function ($faker) {
    return [
        'whs_name'       => $faker->name,
        'whs_code'       => $faker->name,
        'whs_status'     => 'AC',
        'whs_short_name' => $faker->name,
    ];
});

$factory->define(App\WarehouseContact::class, function ($faker) {
    return [
        'whs_con_fname'      => $faker->name,
        'whs_con_lname'      => $faker->name,
        'whs_con_email'      => $faker->email,
        'whs_con_phone'      => $faker->phoneNumber,
        'whs_con_mobile'     => $faker->phoneNumber,
        'whs_con_ext'        => $faker->randomNumber,
        'whs_con_position'   => $faker->word,
        'whs_con_whs_id'     => function () {
            return factory(App\Warehouse::class)->create()->whs_id;
        },
        'whs_con_department' => $faker->text,
    ];
});

$factory->define(App\WarehouseAddress::class, function ($faker) {
    return [
        'whs_add_line_1'       => $faker->streetAddress,
        'whs_add_line_2'       => $faker->streetAddress,
        'whs_add_country_code' => $faker->countryCode,
        'whs_add_city_name'    => $faker->city,
        'whs_add_state_code'   => $faker->stateAbbr,
        'whs_add_postal_code'  => $faker->postcode,
        'whs_add_whs_id'       => function () {
            return factory(App\Warehouse::class)->create()->whs_id;
        },
        'whs_add_type'         => 'SH',
    ];
});

$factory->define(App\WarehouseQualifier::class, function ($faker) {
    return [
        'qualifier' => strtoupper(str_random(2)),
        'desc'      => $faker->text
    ];
});

$factory->define(App\WarehouseMeta::class, function ($faker) {
    return [
        'whs_id'         => $faker->numberBetween(1000, 9000),
        'whs_qualifier'  => function () {
            return factory(App\WarehouseQualifier::class)->create()->qualifier;
        },
        'whs_meta_value' => $faker->text,
    ];
});

$factory->define(App\ZoneLocationStatus::class, function ($faker) {
    return [
        'loc_sts_code' => strtoupper($faker->randomLetter . $faker->randomLetter),
        'loc_sts_desc' => $faker->text,
    ];
});

$factory->define(App\LocationType::class, function ($faker) {
    return [
        'loc_type_name' => $faker->name,
        'loc_type_code' => uniqid(),
        'loc_type_desc' => $faker->text,
    ];
});

$factory->define(App\WarehouseZone::class, function ($faker) {
    $whs_id = factory(App\Warehouse::class)->create()->whs_id;
    $code = '';
    $warehouseZone = 1;
    while (!empty($warehouseZone)) {
        $code = strtoupper($faker->randomLetter . $faker->randomLetter);
        $warehouseZone = DB::table('zone')
            ->where('zone_whs_id', $whs_id)
            ->where('zone_code', $code)
            ->get();
    }

    return [
        'zone_whs_id'      => $whs_id,
        'zone_name'        => $faker->name,
        'zone_code'        => $code,
        'zone_type_id'     => function () {
            return factory(App\WarehouseZoneType::class)->create()->zone_type_id;
        },
        'zone_description' => $faker->text,
        'zone_num_of_loc'  => 5, // need to replace,
        'zone_min_count'   => 1,
        'zone_max_count'   => 999
    ];
});

$factory->define(App\WarehouseZoneType::class, function ($faker) {
    return [
        'zone_type_name' => $faker->name,
        'zone_type_code' => strtoupper($faker->randomLetter . $faker->randomLetter) . $faker->numberBetween(1000, 9000),
        'zone_type_desc' => $faker->text,
    ];
});

$factory->define(App\Location::class, function ($faker) {
    return [
        'loc_whs_id'           => function () {
            return factory(App\Warehouse::class)->create()->whs_id;
        },
        'loc_alternative_name' => $faker->name,
        'loc_code'             => strtoupper($faker->randomLetter . $faker->randomLetter),
        'loc_zone_id'          => function () {
            return factory(App\WarehouseZone::class)->create()->zone_id;
        },
        'loc_type_id'          => function () {
            return factory(App\LocationType::class)->create()->loc_type_id;
        },
        'loc_sts_code'         => 'AC',
    ];
});
