<?php
/**
 * Created by PhpStorm.
 * User: vinhpham
 * Date: 11/6/18
 * Time: 5:17 PM
 */

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../../../../')
);

$router = $app->router;

$router->group(['prefix' => 'ecom/inventory'], function () use ($router) {
    $router->get('/', function ()    {
        echo "Ecom Orders module";
    });
});