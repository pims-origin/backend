<?php

namespace App\Api\V2\Outbound\PackCarton\SkuList\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\Status;

/**
 * Class PackHdrModel
 *
 * @package App\Api\V2\Outbound\PackCarton\SkuList\Models
 */
class PackHdrModel extends AbstractModel
{
    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
    }

    public function getCartonsByOutPltId($out_plt_id, $whsId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
            ->where('out_plt_id', $out_plt_id)
            ->where('whs_id', $whsId)
            ->get();
    }

    public function getCartonsByOrderId($odrid, $whsId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
            ->where('odr_hdr_id', $odrid)
            ->where('whs_id', $whsId)
            ->get()
            ->toArray();
    }

    /**
     * @param $whsId
     * @param $odrId
     *
     * @return mixed
     */
    public function packTotal($whsId, $odrId)
    {
        return $this->model
                        ->where('whs_id', $whsId)
                        ->where('odr_hdr_id', $odrId)
                        ->count();
    }

    /**
     * @param $whsId
     * @param $outPltId
     *
     * @return mixed
     */
    public function scannedByPalletTotal($whsId, $outPltId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
                        ->where('whs_id', $whsId)
                        ->where('out_plt_id', $outPltId)
                        ->get()
                        ->toArray();
    }

    public function scannedByOrderTotal($whsId, $odrId)
    {
        return $this->model
            ->where('whs_id', $whsId)
            ->where('odr_hdr_id', $odrId)
            ->whereNotNull('out_plt_id')
            ->count();
    }


    /**
     * Get sku for hint on order page on app mobile
     *
     * @param Integer $whsId
     * @param String $sku
     * @param Integer $limit
     *
     * @return Collection
     */
    public function getSkuForSuggestion($whsId, $sku, $limit = null, $odrNum) {

        $odrStsList = [
                    // Status::getByValue("New", "ORDER-STATUS"),
                    // Status::getByValue("Picked", "ORDER-STATUS"),
                    // Status::getByValue("Picking", "ORDER-STATUS"),
                    Status::getByValue("Packing", "ORDER-STATUS"),
                    Status::getByValue("Packed", "ORDER-STATUS"),
                    Status::getByValue("Palletizing", "ORDER-STATUS"),
                ];

        $query = $this->model
                ->select([
                    'pack_hdr.*',
                    'item.pack',
                    DB::raw(" count(*) AS pack_remain"),
                ])
                ->join('odr_hdr', 'pack_hdr.odr_hdr_id', '=', 'odr_hdr.odr_id')
                ->leftjoin('item', 'pack_hdr.item_id', '=', 'item.item_id')
                ->where('odr_hdr.whs_id', $whsId)
                ->whereIn('odr_hdr.odr_sts', $odrStsList)
                ->where('pack_hdr.sku', 'LIKE', "{$sku}%")
                ->whereNull('pack_hdr.out_plt_id')
                ->groupBy('pack_hdr.item_id')
                ->orderBy('pack_hdr.sku');
        if ($limit) {
            $query->limit($limit);
        }

        // WAP-770 - [Outbound - Assign Ctn to Pallet] Improvement api get sku list by order number
        if(trim($odrNum) !== '') {
            $odrNum = trim($odrNum);
            $query->where('odr_hdr.odr_num', $odrNum);
        }

        $result = $query->get();

        return $result;
    }
}
