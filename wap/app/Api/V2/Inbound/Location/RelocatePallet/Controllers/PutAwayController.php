<?php

namespace App\Api\V2\Inbound\Location\RelocatePallet\Controllers;

use App\Api\V2\Inbound\Location\RelocatePallet\Models\CartonModel;
use App\Api\V2\Inbound\Location\RelocatePallet\Models\LocationModel;
use App\Api\V2\Inbound\Location\RelocatePallet\Models\PalletModel;
use App\Api\V2\Inbound\Location\RelocatePallet\Models\ItemModel;
use App\Api\V2\Inbound\Location\RelocatePallet\Models\InventorySummaryModel;
use App\Api\V2\Inbound\Location\RelocatePallet\Models\Log;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\MyHelper;

class PutAwayController extends AbstractController
{

    const PREFIX_CARTON_RFID_RELOCATE = 'CCTC';

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * PutAwayController constructor.
     *
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param ItemModel $itemModel
     */
    public function __construct(
        PalletModel $palletModel,
        LocationModel $locationModel
    ) {
        $this->palletModel   = $palletModel;
        $this->cartonModel   = new CartonModel();
        $this->locationModel = $locationModel;
        $this->itemModel     = new ItemModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function relocate($whsId, Request $request)
    {
        // WAP-703 - [Inbound - Relocate] Create API to submit relocate

        $url = "/v2/whs/{$whsId}/location/rack/relocate";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'PAR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Relocate Pallet'
        ]);

        $input = $request->getParsedBody();
        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }
        $pltRfid  = array_get($input, 'pallet_rfid');
        $location = array_get($input, 'location');
        $type     = array_get($input, 'type');
        $ctnTtl   = array_get($input, 'carton_total');
        $itemId   = array_get($input, 'item_id');

        // Get Pallet by Rfid
        $pallet = $this->palletModel->getFirstWhere([
            'rfid' => $pltRfid,
        ]);

        if (!$pallet) {
            $msg = "Pallet does not exist.";
            return $this->_responseErrorMessage($msg);
        }

        if ($pallet && object_get($pallet, 'whs_id') != $whsId) {
            $msg = "Pallet does not belong to current warehouse.";
            return $this->_responseErrorMessage($msg);
        }

        $cusId = object_get($pallet, 'cus_id');

        //Check existed for The loc RFID
        $locationInfo = '';
        if ($type == 1) {
            $locationInfo = $this->locationModel->getRackLocationByLocRFID($whsId, $input['location']);
        } else {
            $locationInfo = $this->locationModel->getFirstWhere([
                    'loc_whs_id' => $whsId,
                    'loc_code'   => $location
                ]);
        }
        if (empty($locationInfo) || !count($locationInfo)) {
            $msg = "Location does not exist.";
            if ($type == 1) {
                $locationInfo = $this->locationModel->getFirstWhere([
                        'rfid'   => $location
                    ]);
            } else {
                $locationInfo = $this->locationModel->getFirstWhere([
                        'loc_code'   => $location
                    ]);
            }

            if (count($locationInfo)) {
                $msg = "Location does not belong to current warehouse.";
            }

            return $this->_responseErrorMessage($msg);
        }

        $locationObj = $this->locationModel->getRackLocationByCustomerById($whsId, $locationInfo->loc_id, $cusId);

        if (empty($locationObj)) {
            $msg = "Location does not belong to this customer.";
            return $this->_responseErrorMessage($msg);
        } elseif ($locationObj->loc_sts_code != 'AC') {
            $msg = sprintf("Location %s is not active. Current Status %s",
                $locationObj->loc_code,
                $locationObj->loc_sts_code);
            return $this->_responseErrorMessage($msg);
        }

        if ($locationObj && !$locationObj->rfid) {
            $msg = sprintf("Location %s does not be Rfid",
                $locationObj->loc_code);
            return $this->_responseErrorMessage($msg);
        }

        // $chkPlt = $this->palletModel->getModel()->where([
        //         'loc_id' => $locationObj->loc_id,
        //         'whs_id' => $whsId
        //     ])
        //     ->first();

        // if ($chkPlt && ($chkPlt->rfid <> $input['pallet_rfid'] || $chkPlt->rfid == null)) {
        //     $msg = sprintf("There is a pallet %s in this location %s", $chkPlt->plt_num, $locationObj->loc_code);

        //     return $this->_responseErrorMessage($msg);
        // }

        $itemObj = $this->itemModel->getFirstWhere(['item_id' => $itemId]);

        if (!$itemObj) {
            $msg = sprintf("Item Id %s does not exist.", $itemId);
            return $this->_responseErrorMessage($msg);
        }

        if ($itemObj && $itemObj->cus_id != $cusId) {
            $msg = sprintf("Item Id %s does not belong to this customer.", $itemId);
            return $this->_responseErrorMessage($msg);
        }

        //WAP-742 - [Migration data] Unable relocate when location is mixed SKU
        $checkMixSku = $this->_checkMixSkuOnLoc($whsId, $itemId, $locationObj->loc_id);

        if ($checkMixSku) {
            $msg = sprintf("Unable relocate on location contains cartons are mixed SKU.");
            return $this->_responseErrorMessage($msg);
        }

        //WAP-749 - [Migration data] Unable relocate when location is empty
        $checkEmptyLoc = $this->_checkEmptyLoc($whsId, $locationObj->loc_id);

        if ($checkEmptyLoc) {
            $msg = sprintf("Unable relocate on location does not contain any cartons.");
            return $this->_responseErrorMessage($msg);
        }

        $userId = Data::getCurrentUserId();

        try {
            DB::beginTransaction();
            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $lot = 'NA';
            // Remove fake carton belong to pallet
            $this->cartonModel->getModel()
                ->where('plt_id', $pallet->plt_id)
                ->where('rfid', 'like', self::PREFIX_CARTON_RFID_RELOCATE. '%')
                ->update([
                    'deleted'    => 1,
                    'deleted_at' => time(),
                    'plt_id'     => null,
                    'loc_id'     => null,
                    'loc_code'   => null,
                ]);
            // Remove relationship carton belong to pallet
            $this->cartonModel->getModel()
                ->where('plt_id', $pallet->plt_id)
                ->update([
                    'plt_id' => null,
                ]);

            // Reduce in inventory
            $this->_updateInventory($itemObj->item_id, $whsId);

            // Remove relationship pallet belong to location
            $otherPallets = $this->palletModel->getModel()
                ->where('loc_id', $locationObj->loc_id)
                ->where('plt_id', '!=', $pallet->plt_id)
                ->get();
            $otherPltIds = [];
            if (count($otherPallets)) {
                $otherPltIds = $otherPallets->pluck('plt_id')->toArray();
                $this->palletModel->getModel()
                    ->whereIn('plt_id', $otherPltIds)
                    ->update([
                        'loc_id'   => null,
                        'loc_code' => null,
                    ]);
            }

            $packSize = $itemObj->pack;
            $locTypeArr = DB::table('loc_type')
                ->where('loc_type_id', $locationObj->loc_type_id)
                ->first();
            $locTypeCode = array_get($locTypeArr, 'loc_type_code', 'RAC');

            $dataCarton = [
                'plt_id'            => $pallet->plt_id,
                'whs_id'            => $whsId,
                'cus_id'            => $cusId,
                // 'ctn_num'           => $ctnNum,
                // 'rfid'              => $ctnRfid,
                'ctn_sts'           => 'AC',
                'ctn_uom_id'        => $itemObj->uom_id,
                'loc_id'            => $locationObj->loc_id,
                'loc_code'          => $locationObj->loc_code,
                'loc_name'          => $locationObj->loc_alternative_name,
                'loc_type_code'     => $locTypeCode,
                'is_damaged'        => 0,
                'piece_remain'      => $packSize,
                'piece_ttl'         => $packSize,
                'created_at'        => time(),
                'created_by'        => $userId,
                'updated_at'        => time(),
                'updated_by'        => $userId,
                'deleted'           => 0,
                'deleted_at'        => 915148800,
                'is_ecom'           => 0,
                'picked_dt'         => 0,
                'storage_duration'  => 0,
                'item_id'           => $itemId,
                'sku'               => $itemObj->sku,
                'size'              => $itemObj->size,
                'color'             => $itemObj->color,
                'lot'               => $lot,
                'ctn_pack_size'     => $packSize,
                'uom_code'          => $itemObj->uom_code,
                'uom_name'          => $itemObj->uom_name,
                'inner_pack'        => 0,
                'length'            => $itemObj->length,
                'width'             => $itemObj->width,
                'height'            => $itemObj->height,
                'weight'            => $itemObj->weight,
                'volume'            => $itemObj->volume,
                'cube'              => $itemObj->cube,
                'upc'               => $itemObj->cus_upc,
                'spc_hdl_code'      => $itemObj->spc_hdl_code,
                'spc_hdl_name'      => $itemObj->spc_hdl_name,
                'cat_code'          => $itemObj->cat_code,
                'cat_name'          => $itemObj->cat_name,
            ];

            $ctnsMappingRfidAj = DB::table('cartons')
                ->where('loc_id', $locationObj->loc_id)
                ->where('item_id', $itemId)
                // ->where('ctn_sts', 'AJ')
                ->whereNotNull('rfid')
                ->where('rfid', 'not like', self::PREFIX_CARTON_RFID_RELOCATE. '%')
                ->where('is_damaged', 0)
                ->where('deleted', 0)
                ->where('is_ecom', 0)
                ->where('piece_remain', '>', 0)
                ->get();

            $ctnsMappingBarcodeAc = DB::table('cartons')
                ->where('loc_id', $locationObj->loc_id)
                ->where('item_id', $itemId)
                ->where('ctn_sts', 'AC')
                ->whereNull('rfid')
                ->where('is_damaged', 0)
                ->where('deleted', 0)
                ->where('is_ecom', 0)
                ->where('piece_remain', '>', 0)
                ->get();
            $dataCtnInserts = [];
            $pltIdsRemove = [];
            for($i = 0; $i < $ctnTtl; $i++)
            {
                // Mapping data
                // if (count($ctnsMappingRfidAj) && count($ctnsMappingBarcodeAc)) {
                //     $ctnRfidAj = array_shift($ctnsMappingRfidAj);
                //     $ctnBarcodeAc = array_shift($ctnsMappingBarcodeAc);

                //     $ctnBarcodeAc['plt_id'] = $pallet->plt_id;
                //     // $ctnBarcodeAc['rfid']   = $ctnRfidAj['rfid'];
                //     $ctnBarcodeAc['rfid']   = $this->_createCtnRfidFromPltNum($pallet, $i + 1);

                //     $ctnRfidAj['plt_id']     = null;
                //     $ctnRfidAj['loc_id']     = null;
                //     $ctnRfidAj['loc_code']   = null;
                //     // $ctnRfidAj['deleted']    = 1;
                //     // $ctnRfidAj['deleted_at'] = time();

                //     $pltIdsRemove[] = $ctnRfidAj['plt_id'];
                //     $pltIdsRemove[] = $ctnBarcodeAc['plt_id'];

                //     $this->cartonModel->updateWhere(
                //         $ctnRfidAj,
                //         ['ctn_id' => $ctnRfidAj['ctn_id']]
                //     );
                //     $this->cartonModel->updateWhere(
                //         $ctnBarcodeAc,
                //         ['ctn_id' => $ctnBarcodeAc['ctn_id']]
                //     );

                //     continue;
                // }

                // if (count($ctnsMappingRfidAj)) {
                //     $ctnRfidAj = array_shift($ctnsMappingRfidAj);
                //     $pltIdsRemove[] = $ctnRfidAj['plt_id'];
                //     $ctnRfidAj['ctn_sts'] = 'AC';
                //     $ctnRfidAj['plt_id'] = $pallet->plt_id;
                //     $this->cartonModel->updateWhere(
                //         $ctnRfidAj,
                //         ['ctn_id' => $ctnRfidAj['ctn_id']]
                //     );
                //     continue;
                // }

                if (count($ctnsMappingBarcodeAc)) {
                    $ctnBarcodeAc = array_shift($ctnsMappingBarcodeAc);
                    $pltIdsRemove[] = $ctnBarcodeAc['plt_id'];
                    $ctnBarcodeAc['rfid'] = $this->_createCtnRfidFromPltNum($pallet, $i + 1);
                    $ctnBarcodeAc['ctn_sts'] = 'AC';
                    $ctnBarcodeAc['plt_id'] = $pallet->plt_id;
                    $this->cartonModel->updateWhere(
                        $ctnBarcodeAc,
                        ['ctn_id' => $ctnBarcodeAc['ctn_id']]
                    );
                    continue;
                }

                // if (!count($ctnsMappingRfidAj) && !count($ctnsMappingBarcodeAc)) {
                if (!count($ctnsMappingBarcodeAc)) {
                    // $pltIdsRemove[] = $ctnBarcodeAc['plt_id'];
                    $ctnNum  = $this->cartonModel->createCtnNumFromPltNum($pallet, $i + 1);
                    $ctnRfid = $this->_createCtnRfidFromPltNum($pallet, $i + 1);

                    $dataTmp = $dataCarton;

                    $dataTmp['ctn_num'] = $ctnNum;
                    $dataTmp['rfid']    = $ctnRfid;

                    $dataCtnInserts[] = $dataTmp;
                }
            }

            if (count($ctnsMappingRfidAj)) {
                foreach ($ctnsMappingRfidAj as $keyCtn => $carton) {
                    $carton['plt_id']     = null;
                    $carton['loc_id']     = null;
                    $carton['loc_code']   = null;
                    // $carton['deleted']    = 1;
                    // $carton['deleted_at'] = time();

                    $this->cartonModel->updateWhere(
                        $carton,
                        ['ctn_id' => $carton['ctn_id']]
                    );
                }
            }

            if (count($ctnsMappingBarcodeAc)) {
                foreach ($ctnsMappingBarcodeAc as $keyCtn => $carton) {
                    $carton['plt_id']     = null;
                    $carton['loc_id']     = null;
                    $carton['loc_code']   = null;
                    $carton['deleted']    = 1;
                    $carton['deleted_at'] = time();

                    $this->cartonModel->updateWhere(
                        $carton,
                        ['ctn_id' => $carton['ctn_id']]
                    );
                }
            }

            if (count($dataCtnInserts)) {
                $chunkData = array_chunk($dataCtnInserts, 300);
                foreach ($chunkData as $data) {
                    DB::table('cartons')->insert($data);
                }
            }

            // Update Carton
            $data = [
                'loc_id'        => $locationObj->loc_id,
                'loc_code'      => $locationObj->loc_code,
                'loc_name'      => $locationObj->loc_alternative_name,
                'loc_type_code' => Status::getByValue('RACK', 'LOC_TYPE_CODE'),
                'plt_id'        => $pallet->plt_id
            ];

            $this->cartonModel->updateCartonWithPltID($data);

            // update carton total of other pallet
            if (count($otherPallets)) {
                foreach ($otherPallets as $otherPallet) {
                    $countCartonByOtherPallet = $this->cartonModel->getModel()
                        ->where('plt_id', $pallet->plt_id)
                        ->count();
                }
                if ($countCartonByOtherPallet == 0) {
                    $this->palletModel->getModel()
                    ->where('plt_id', $otherPallet->plt_id)
                    ->update([
                        'ctn_ttl'    => $countCartonByOtherPallet,
                        'deleted'    => 1,
                        'deleted_at' => time(),
                    ]);
                } else {
                    $this->palletModel->getModel()
                    ->where('plt_id', $otherPallet->plt_id)
                    ->update([
                        'ctn_ttl'    => $countCartonByOtherPallet,
                    ]);
                }

            }

            $countCartonByPallet = $this->cartonModel->getModel()
                ->where('plt_id', $pallet->plt_id)
                ->count();

            // Update Pallet
            $this->palletModel->updateWhere([
                    'loc_id'   => $locationObj->loc_id,
                    'loc_code' => $locationObj->loc_code,
                    'loc_name' => $locationObj->loc_alternative_name,
                    'plt_sts'  => 'AC',
                    'ctn_ttl'  => $countCartonByPallet,
                    'item_id'  => $itemObj->item_id,
                    'sku'      => $itemObj->sku,
                    'color'    => $itemObj->sku,
                    'size'     => $itemObj->sku,
                    'pack'     => $itemObj->pack,
                ],
                [
                    // 'rfid'   => $pltRfid,
                    'plt_id' => $pallet->plt_id,
                    // 'storage_duration' => 0
                ]);

            //unlock this location
            $this->locationModel->updateWhere(
                [
                    'loc_sts_code' => Status::getByKey('LOCATION_STATUS', 'ACTIVE'),
                ],
                [
                    'loc_id' => $locationObj->loc_id
                ]
            );

            // Update inventory summary
            $this->_insertInvtSmr($whsId, $itemObj, $lot);
            $this->_updateInventory($itemObj->item_id, $whsId);

            DB::commit();
            $msg = sprintf("Relocate the pallet successfully.");

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => $msg
                    ]
                ],
                'data' => []
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _validateParams($attributes, $whsId)
    {
        $pltRfid  = array_get($attributes, 'pallet_rfid');
        $location = array_get($attributes, 'location');
        $type     = array_get($attributes, 'type');
        $ctnTtl   = array_get($attributes, 'carton_total');
        $itemId   = array_get($attributes, 'item_id');

        $names = [
            'pallet_rfid'  => 'Pallet rfid',
            'location'     => 'Location',
            'type'         => 'Type',
            'carton_total' => 'Carton total',
            'item_id'      => 'SKU',
        ];

        // Check Required
        $requireds = [
            'pallet_rfid'  => $pltRfid,
            'location'     => $location,
            'type'         => $type,
            'carton_total' => $ctnTtl,
            'item_id'      => $itemId,
        ];

        // Check int type
        $integers = [
            'type'         => $type,
            'carton_total' => $ctnTtl,
            'item_id'      => $itemId,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        foreach ($integers as $key => $field) {
            if (!is_int($field) || $field < 1 || $field > 999999999) {
                $errorDetail = "{$names[$key]} must be integer and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        if (!in_array($type, [1, 2])) {
            $msg = "Type is required. Just including 1(RFID) / 2(Barcode)";
            return $this->_responseErrorMessage($msg);
        }

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            return $this->_responseErrorMessage($pltRFIDValid->error);
        }

        //validate location RFID
        if ($type == 1) {
            $locRFIDValid = new RFIDValidate($location, RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                return $this->_responseErrorMessage($locRFIDValid->error);
            }
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _createCtnRfidFromPltNum($pltObj, $seq = 0)
    {
        //create CTN number by Pallet number
        $ctnCode = str_replace('LPN', self::PREFIX_CARTON_RFID_RELOCATE, $pltObj->plt_num);
        $ctnCode = str_replace('-', '', $ctnCode);
        if (!$seq) {
            //get current carton number, count null is 0 so need add 1 for the first
            $seq = $this->model->where('plt_id', $pltObj->plt_id)->count() + 1;
        }

        // max length of carton rfid is 24
        if (strlen($ctnCode) > 20) {
            $ctnCode = substr($ctnCode, 4 + strlen($ctnCode) - 20);
            $ctnCode = self::PREFIX_CARTON_RFID_RELOCATE . $ctnCode;
        }

        $ctnRfid = $ctnCode . str_pad($seq, 24 - strlen($ctnCode), "0", STR_PAD_LEFT);

        return $ctnRfid;
    }

    private function _insertInvtSmr($whsId, $itemObj, $lot)
    {
        $itemId = $itemObj->item_id;

        $ivt = $this->inventorySummaryModel
            ->getFirstWhere([
                'item_id' => $itemId,
                'lot'     => $lot,
                'whs_id'  => $whsId
            ]);

        if (!$ivt) {
            $insertInvtSmr[] = [
                'item_id'       => $itemId,
                'cus_id'        => $itemObj->cus_id,
                'whs_id'        => $whsId,
                'color'         => $itemObj->color,
                'sku'           => $itemObj->sku,
                'size'          => $itemObj->size,
                'lot'           => $lot,
                'ttl'           => 0,
                'allocated_qty' => 0,
                'dmg_qty'       => 0,
                'avail'         => 0,
                'back_qty'      => 0,
                'upc'           => $itemObj->cus_upc,
                'crs_doc_qty'   => 0,
                'created_at'    => time(),
                'updated_at'    => time(),
            ];

            DB::table('invt_smr')->insert($insertInvtSmr);
        }

    }

    private function _updateInventory($itemId, $whsId)
    {
        return DB::table('invt_smr AS iv')
            ->join(DB::raw("(
                SELECT item_id, lot,
                    SUM(IF(!is_damaged, piece_remain, 0)) AS avail,
                    SUM(IF(loc_type_code = 'XDK', piece_remain, 0)) AS xdock_qty,
                    SUM(IF(is_damaged, piece_remain, 0)) AS dmg_qty
                FROM cartons
                WHERE item_id = {$itemId}
                    AND deleted = 0
                    AND ctn_sts = 'AC'
                GROUP BY item_id,lot
            ) tt"), function ($join) {
                $join->on('iv.item_id', '=', 'tt.item_id')
                    ->on('iv.lot', '=', 'tt.lot');
            })
            ->where([
                // 'iv.whs_id' => Data::getCurrentWhsId()
                'iv.whs_id' => $whsId
            ])
            ->update([
                "iv.ttl"         => DB::raw("tt.avail + tt.dmg_qty"),
                "iv.avail"       => DB::raw("tt.avail - tt.xdock_qty"),
                "iv.dmg_qty"     => DB::raw("tt.dmg_qty"),
                "iv.crs_doc_qty" => DB::raw("tt.xdock_qty"),
            ]);
    }

    private function _reduceInventory($pltId, $whsId)
    {
        $prefix = self::PREFIX_CARTON_RFID_RELOCATE;
        return DB::table('invt_smr AS iv')
            ->join(DB::raw("(
                SELECT item_id, lot,
                    SUM(IF(!is_damaged, piece_remain, 0)) AS avail,
                    SUM(IF(loc_type_code = 'XDK', piece_remain, 0)) AS xdock_qty,
                    SUM(IF(is_damaged, piece_remain, 0)) AS dmg_qty
                FROM cartons
                WHERE plt_id = {$pltId}
                    AND deleted = 0
                    AND ctn_sts = 'AC'
                    AND rfid like '{$prefix}%'
                GROUP BY item_id,lot
            ) tt"), function ($join) {
                $join->on('iv.item_id', '=', 'tt.item_id')
                    ->on('iv.lot', '=', 'tt.lot');
            })
            ->where([
                // 'iv.whs_id' => Data::getCurrentWhsId()
                'iv.whs_id' => $whsId
            ])
            ->update([
                "iv.ttl"         => DB::raw("iv.ttl - tt.avail - tt.dmg_qty"),
                "iv.avail"       => DB::raw("iv.avail - tt.avail + tt.xdock_qty"),
                "iv.dmg_qty"     => DB::raw("iv.dmg_qty - tt.dmg_qty"),
                "iv.crs_doc_qty" => DB::raw("iv.crs_doc_qty - tt.xdock_qty"),
            ]);
    }

    private function _checkMixSkuOnLoc($whsId, $itemId, $locId)
    {
        $count = DB::table('cartons')
            ->where('whs_id', $whsId)
            ->where('item_id', '!=', $itemId)
            ->where('loc_id', $locId)
            // ->whereIn('ctn_sts', ['AC', 'LK'])
            ->where('deleted', 0)
            ->count();

        return $count > 0 ? true : false;
    }

    private function _checkEmptyLoc($whsId, $locId)
    {
        $count = DB::table('cartons')
            ->where('whs_id', $whsId)
            ->where('loc_id', $locId)
            // ->whereIn('ctn_sts', ['AC', 'LK'])
            ->where('deleted', 0)
            ->count();

        return $count == 0 ? true : false;
    }
}
