<?php

namespace Seldat\Wms2\Models;


class Setting extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'setting_id';

    protected $fillable = [
        'setting_key',
        'setting_value'
    ];
}
