<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OdrHdrMetaModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Models\WavePickDtlLocModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Traits\OrderFlowControllerTrait;
use App\Api\V1\Transformers\WavePickHdrTransformer;
use App\Api\V1\Validators\PickerMultiValidator;
use App\Api\V1\Validators\PickerValidator;
use App\Helpers\PushNotification;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\OrderHdrMeta;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use mPDF;
use App\Api\V1\Traits\WavePickControllerTrait;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

use App\Api\V1\Transformers\PickerTransformer;

class WavePickController extends AbstractController
{
    use WavePickControllerTrait;
    use OrderFlowControllerTrait;
    /**
     * @var Wave pick header
     */
    protected $wv_hdr;

    /**
     * @var Wave pick detail
     */
    protected $wv_dtl;

    /**
     * @var Order Header
     */
    protected $odrHdr;

    /**
     * @var Order Detail
     */
    protected $odrDtl;

    /**
     * @var Order Detail Loc
     */
    protected $odrDtlLoc;

    /**
     * @var Location
     */
    protected $loc;

    protected $carton;
    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var $userModel
     */
    protected $userModel;

    protected $STR_PAD_LEFT = 0;

    protected $customerConfigModel;
    private $_skulistNotPicked;
    private $_skulistNotPickedGR;

    /**
     * WavePickController constructor.
     */
    public function __construct()
    {
        $this->wv_hdr = new WavePickHdrModel();
        $this->wv_dtl = new WavePickDtlModel();
        $this->odrHdr = new OrderHdrModel();
        $this->odrDtl = new OrderDtlModel();
        $this->loc = new LocationModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->carton = new CartonModel();
        $this->userModel = new UserModel();
        $this->wv_dtl_loc = new WavePickDtlLocModel();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->_skulistNotPicked = [];
        $this->_skulistNotPickedGR = [];
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function createWavePick(Request $request)
    {
        return $this->upsert($request);
    }

    /**
     * @param $request
     */
    private function upsert($request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $odrLsts = $input['odr_ids'];

        // Parse to integer
        $odrLstAfters = [];
        foreach ($odrLsts as $odrLst) {
            array_push($odrLstAfters, (int)$odrLst);
        }

        // Load odr hdr data
        $odrHdrDt = $this->odrHdr->getListOrderHdrInfo($odrLstAfters, "");

        // Check valid order list
        if ($this->checkInvalidOdr($odrLstAfters, $odrHdrDt) == false) {
            throw new Exception(Message::get("BM035"));
        }

        // Detect to make sure that all order only be normal or online
        $detectOrdType = $this->checkOnlineOrNormalOdr($odrHdrDt);
        if ($detectOrdType['checkNormalOdr'] == count($odrLstAfters)) {
            $isOnline = false;
        } elseif ($detectOrdType['checkOnlineOdr'] == count($odrLstAfters)) {
            $isOnline = true;
        } else {
            throw new Exception(Message::get('BM051'));
        }

        try {
            DB::beginTransaction();
            // load data
            $odrDtlList = $this->odrDtl->getListOrderDtlInfo($odrLstAfters);

            if ( !$odrDtlList->count() ) {
                throw new Exception(Message::get("BM017", "Orders"));
            }

            // group the same
            $data = $this->groupSameItem($odrDtlList);

            // save wave pick header
            $wvHdr = $this->saveWvHdr($data[0][0], $isOnline);
            $tem = [];

            // make data to save to db
            foreach ($data as $ds) {
                $cus_id = $ds[0]->cus_id;

                $pickingAlgorithm = $this->customerConfigModel->getPickingAlgorithm(array_get($wvHdr, 'whs_id', 0),
                    $cus_id);

                // Detect pack size, carton qty , picking qty & alloc_qty
                $sumQty = $this->detectQty($ds);

                // Detect primary &  backup location
                $loc = $this->detectFIFOLIFOLocation($ds, $pickingAlgorithm, $sumQty['ctn_qty']);

                if (!count($this->_skulistNotPicked)) {
                    // Save wave pick detail
                    $wvDtl = $this->saveWvDtl($ds, $wvHdr, $sumQty, $loc);

                    // Save wave pick suggest loc detail\
                    $this->saveWvDtlLoc($wvHdr, $wvDtl, $loc['loc']);
                }

            }

            if (count($this->_skulistNotPicked)) {
                $msg = sprintf("Number of locations is insufficient to pick. There is no active carton of SKU(s) %s in any location.", implode(', ', $this->_skulistNotPicked));
                $errMsg = [];
                foreach ($this->_skulistNotPicked as $item) {
                    $checkCycleCount = $this->checkCycleCount($item);
                    if($checkCycleCount) {
                        $msg = array_get($checkCycleCount, 'cycle_name');
                        $errMsg[] = "[{$item}]($msg)";
                    }
        
        
                }
    
    
                if(count($errMsg) > 0) {
                    $msg = sprintf("Locations are not available to pick the Order because of these open Cycle Count: %s", implode(', ', $errMsg));
                    throw new Exception($msg);
                }
    
            }
    
            if (count($this->_skulistNotPickedGR)) {
                $msg = sprintf("Locations are not available to pick the Order because of these receiving : %s", implode(', ', $this->_skulistNotPickedGR));
                throw new Exception($msg);
        
            }
    
            // update order detail
            $this->updateOdrDtl($odrLstAfters, $wvHdr);

            // update order header
            $this->updateOdrHdr($odrHdrDt, $wvHdr);

            //Load data order header to set title to excel
            $odrHdrs = $this->odrHdr->getOrderHdrNums($odrLstAfters);
            $odrNum = implode(", ", $odrHdrs->toArray());

            // Load data for excel
            $dataWvDtl = $this->wv_dtl->loadData($wvHdr['wv_id']);

            // Get Location for excel
            $tempCartons = $this->getLoationsNumberCartons($wvHdr['wv_id'], $dataWvDtl);

            $dataWvDtl = $dataWvDtl->toArray();

            // Prepare data to load excel
            $pickQty = $this->prepareDataForExcel($dataWvDtl);
            $temp = [];
            foreach ($pickQty as $qty) {
                $temp[$qty['ITEM_ID'] . "-" .
                $qty['SKU'] . "-" .
                $qty['Color'] . "-" .
                $qty['SIZE'] . "-" .
                $qty['Lot']][$qty['Pack_size']] = $qty;
            }

            // Change request --------------------------------------------------------------
            // If carton_qty is null, find num carton with sum (piece_remain) <= picking qty
            // $updateCtnQty = $this->updateCartonQty($temp);

            $odrHdrDtAfter = $this->odrHdr->getListOrderHdrInfo($odrLstAfters, "updated");

            $this->logEventTracking($odrHdrDtAfter, $wvHdr, $data[0][0]);
            DB::commit();
            $this->orderFlow($wvHdr['wv_id'], array_get($odrHdrDt, '0.odr_id', 0), 5, array_get($odrHdrDt, '0.cus_id',
                0), $request, new OdrHdrMetaModel(), new CustomerMetaModel());

            // Export excel
            //return $this->writeExcel($temp, $wvHdr['wv_num'], $odrNum, $tempCartons);
            $whsId = array_get($odrHdrDt, '0.whs_id',
                0);
            if (!file_exists(storage_path("WavePick/$whsId"))) {
                mkdir(storage_path("WavePick/$whsId"), 0777, true);
            }

            $picker = "";

            return $this->printDpfData($temp, $wvHdr['wv_num'], $odrNum, $tempCartons, $whsId, $picker);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }
    
    private function checkCycleCount($sku )
    {
        return Carton::join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->join('cycle_dtl', 'cycle_dtl.sys_loc_id', '=', 'location.loc_id')
            ->join('cycle_hdr', 'cycle_hdr.cycle_hdr_id', '=', 'cycle_dtl.cycle_hdr_id')
            ->where('cartons.sku', $sku)
            ->where('cycle_hdr.cycle_sts', '!=', 'CP')
            ->where('cycle_hdr.deleted', '0')
            ->groupBy('cycle_hdr.cycle_num')
            ->first([
                'cycle_hdr.cycle_name',
                // 'cycle_hdr.cycle_num'
            ]);
    }
    
    
    
    /**
     * @param $temp
     *
     * @return int
     */
    private function updateCartonQty($temp)
    {
        $iCount = 0;
        foreach ($temp as $tem) {
            $pickSum = 0;
            foreach ($tem as $te) {
                $pickSum += $te['Picking_QTY'];
            }

            foreach ($tem as $te) {
                if ($te['Cartons'] == 0 || is_null($te['Cartons'])) {
                    $ctn_qty = $this->getNumCartonNearlyPickingQty([
                        'item_id'     => $te['ITEM_ID'],
                        'cus_id'      => $te['cus_id'],
                        'picking_qty' => $pickSum,
                    ]);

                    $this->wv_dtl->updateWhere([
                        'ctn_qty' => $ctn_qty,
                    ], [
                        'wv_dtl_id' => $te['wv_dtl_id']
                    ]);
                    $iCount++;
                }
            }
        }

        return $iCount;
    }

    /**
     * Change request 15-11-2016 - when carton_qty is null, carton will be calculated by query in table "cartons",
     * only get carton with piece_remain nearly picking qty
     *
     * @param $data
     *
     * @return int
     */
    private function getNumCartonNearlyPickingQty($data)
    {
        $ctnLists = $this->carton->getNumCartonNearlyPickingQty($data);
        $ctnQty = 0;
        $ctnPackSizeQty = 0;
        foreach ($ctnLists as $ctn) {
            if ($ctnPackSizeQty <= $data['picking_qty']) {
                if ($ctn['ctn_pack_size'] <= $data['picking_qty']) {
                    $ctnPackSizeQty += $ctn['ctn_pack_size'];
                    $ctnQty++;
                }
            }
        }

        return $ctnQty;
    }

    /**
     * @param $odrHdrDt
     * @param $wvHdr
     * @param $data
     */
    private function logEventTracking($odrHdrDt, $wvHdr, $data)
    {
        $arrOdrNum = [];
        // event tracking - ORDER
        foreach ($odrHdrDt as $odrHdr) {
            // Partial allocated --> Partial Picking
            if ($odrHdr['odr_sts'] == Status::getByValue("Partial Picking", "ORDER-STATUS")) {
                $evt_code = Status::getByKey("EVENT", "PARTIAL-PICKING");
                $info = sprintf(Status::getByKey("EVENT-INFO", "PPK"), $odrHdr['odr_num']);
            } // Full allocated --> Full Picking
            elseif ($odrHdr['odr_sts'] == Status::getByValue("Picking", "ORDER-STATUS")) {
                $evt_code = Status::getByKey("EVENT", "ORDER-PICKING");
                $info = sprintf(Status::getByKey("EVENT-INFO", "ORDER-PICKING"), $odrHdr['odr_num']);
            } else {
                // event tracking - WAVE PICK
                $evt_code = Status::getByKey("EVENT", "WAVE-PICK-NEW");

                $info = sprintf(Status::getByKey("EVENT-INFO", "WAVE-PICK-NEW"), $wvHdr['wv_num'], $odrHdr['odr_num']);

                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $wvHdr['whs_id'],
                    'cus_id'    => $data->cus_id,
                    'owner'     => $odrHdr['odr_num'],
                    'evt_code'  => $evt_code,
                    'trans_num' => $wvHdr['wv_num'],
                    'info'      => $info
                ]);

                return;
            }

            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $wvHdr['whs_id'],
                'cus_id'    => $data->cus_id,
                'owner'     => $odrHdr['odr_num'],
                'evt_code'  => $evt_code,
                'trans_num' => $odrHdr['odr_num'],
                'info'      => $info

            ]);
        }


    }

    /**
     * @param $odrLst
     *
     * @return bool
     */
    private function checkInvalidOdr($odrLst, $odrHdrDt)
    {
        $check = true;
        if (count($odrHdrDt) != count($odrLst)) {
            $check = false;
        }

        return $check;
    }

    /**
     * @param $odrHdrDt
     *
     * @return array
     */
    public function checkOnlineOrNormalOdr($odrHdrDt)
    {
        $checkOnline = 0;
        $checkNormal = 0;
        foreach ($odrHdrDt as $arr) {
            if (strtolower($arr['type']) == "ec") {
                $checkOnline++;
            } else {
                $checkNormal++;
            }
        }

        return [
            'checkOnlineOdr' => $checkOnline,
            'checkNormalOdr' => $checkNormal
        ];
    }

    /**
     * @param $dataWvDtl
     *
     * @return array
     */
    private function prepareDataForExcel($dataWvDtl)
    {
        $pickQty = array_map(function ($e) {
            return [
                'ITEM_ID'     => $e['ITEM_ID'],
                'SKU'         => $e['SKU'],
                'Color'       => $e['Color'],
                'SIZE'        => $e['SIZE'],
                'Lot'         => $e['Lot'],
                'Pack_size'   => $e['Pack_size'],
                'Cartons'     => $e['Cartons'],
                'Picking_QTY' => $e['Picking_QTY'],
                'primary_loc' => $e['primary_loc'],
                'bu_loc_1'    => $e['bu_loc_1'],
                'bu_loc_2'    => $e['bu_loc_2'],
                'bu_loc_3'    => $e['bu_loc_3'],
                'bu_loc_4'    => $e['bu_loc_4'],
                'bu_loc_5'    => $e['bu_loc_5'],
                'cus_id'      => $e['cus_id'],
                'wv_dtl_id'   => $e['wv_dtl_id']
            ];
        }, $dataWvDtl);

        return $pickQty;
    }

    /**
     * @param $odrDtlList
     *
     * @return array
     */
    private function groupSameItem($odrDtlList)
    {
        // group the same
        $data = [];
        $map = [];
        $groupNum = -1;
        for ($i = 0; $i < count($odrDtlList); $i++) {
            $flag = false;
            for ($j = 0; $j < $i; $j++) {
                // Start to compare
                $comSkuColr = ($odrDtlList[$i]['item_id'] == $odrDtlList[$j]['item_id'] && $odrDtlList[$i]['lot'] ==
                    $odrDtlList[$j]['lot']);

                if ($comSkuColr) {
                    $data[$map[$j]][] = $odrDtlList[$i];
                    $map[$i] = $map[$j];
                    $flag = true;
                    break;
                }
            }
            if (!$flag) {
                $groupNum++;
                $data[$groupNum][] = $odrDtlList[$i];
                $map[$i] = $groupNum;
            }
        }

        return $data;
    }

    /**
     * @param $data
     * @param $type
     *
     * @return array
     */
    private function detectFIFOLIFOLocation($data, $algorithm, $ctn_qty = null)
    {
        if ($algorithm == "FIFO") {
            $sort = "ASC";
        } else {
            $sort = "DESC";
        }

        // if all is online order
        // Detect location for normal order
        return $this->findNormalOrderLocation($data, $algorithm, $ctn_qty);
    }

    /**
     * @param $data
     * @param $sort
     *
     * @return array
     */
    private function findNormalOrderLocation($data, $algorithm, $ctn_qty = null)
    {
        $itemId = object_get($data[0], 'item_id', 0);
        $lot = object_get($data[0], 'lot', null);

        // Only get data with location type is rack
        $loc = $this->loc->getLocationList($itemId, $algorithm, $ctn_qty, $lot);

        $result = [];
        $locs = [];
        if ($loc) {
            foreach ($loc as $item) {
                $isExistGr =  DB::table('pal_sug_loc')
                    ->join('gr_hdr', 'pal_sug_loc.gr_hdr_id', "=", 'gr_hdr.gr_hdr_id')
                    ->where('act_loc_id', $item['loc_id'])->where('item_id', $itemId)
                    ->where('gr_sts', "RG")->first();
                if($isExistGr) {
                    $this->_skulistNotPickedGR[] = "[" . object_get($data[0], 'sku') ."](" . array_get($isExistGr, 'gr_hdr_num') . ")";
        
                }else {
                    $locs[$item['loc_id']] = $item;
        
                }
            }
            $result['loc'] = $locs;
        }

        if (count($locs) > 0) {
            $primary = array_shift($locs);

            $result['primary_loc_id'] = array_get($primary, 'loc_id', null);
            $result['primary_loc'] = array_get($primary, 'loc_code', null);

            $result['bu_loc_1_id'] = array_get($locs, '1.loc_id', null);
            $result['bu_loc_1'] = array_get($locs, '1.loc_code', null);
            $result['bu_loc_2_id'] = array_get($locs, '2.loc_id', null);
            $result['bu_loc_2'] = array_get($locs, '2.loc_code', null);
            $result['bu_loc_3_id'] = array_get($locs, '3.loc_id', null);
            $result['bu_loc_3'] = array_get($locs, '3.loc_code', null);
            $result['bu_loc_4_id'] = array_get($locs, '4.loc_id', null);
            $result['bu_loc_4'] = array_get($locs, '4.loc_code', null);
            $result['bu_loc_5_id'] = array_get($locs, '5.loc_id', null);
            $result['bu_loc_5'] = array_get($locs, '5.loc_code', null);
        } else {
            $this->_skulistNotPicked[] = object_get($data[0], 'sku');
        }

        return $result;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function saveWvHdr($data, $isOnline)
    {
        $wvNum = $this->generateWavNumAndSeq();

        // Make data of wave pick header
        $wavePickHdr = [
            'whs_id'  => $data->whs_id,
            'wv_num'  => $wvNum['wave_num'],
            'wv_sts'  => Status::getByValue("New", "WAVEPICK-STATUS"),
            'seq'     => $wvNum['seq'],
            'is_ecom' => intval($isOnline)
        ];

        // save to db: wave pick header
        $wvHdr = $this->wv_hdr->saveWavePickHdr($wavePickHdr);

        return [
            'wv_id'  => $wvHdr->wv_id,
            'wv_num' => $wvNum['wave_num'],
            'whs_id' => $data->whs_id
        ];
    }

    /**
     * @param $ds
     * @param $wvHdr
     * @param $sumQty
     * @param $loc
     */
    private function saveWvDtl($ds, $wvHdr, $sumQty, $loc)
    {

        // Make data of wave pick detail
        $wavePickDtl = [
            'cus_id'         => $ds[0]->cus_id,
            'whs_id'         => $ds[0]->whs_id,
            'wv_id'          => $wvHdr['wv_id'],
            'item_id'        => $ds[0]->item_id,
            'uom_id'         => $ds[0]->uom_id,
            'wv_num'         => $wvHdr['wv_num'],
            'color'          => $ds[0]->color,
            'sku'            => $ds[0]->sku,
            'size'           => $ds[0]->size,
            'lot'            => $ds[0]->lot,
            'cus_upc'        => $ds[0]->cus_upc,
            'wv_dtl_sts'     => Status::getByValue("New", "WAVEPICK-STATUS"),
            'pack_size'      => $ds[0]->pack,
            'ctn_qty'        => $sumQty['ctn_qty'],
            'piece_qty'      => $sumQty['piece_qty'],
            'primary_loc_id' => (!is_null($loc) ? $loc['primary_loc_id'] : null),
            'primary_loc'    => (!is_null($loc) ? $loc['primary_loc'] : null),
            'bu_loc_1_id'    => (!is_null($loc) ? $loc['bu_loc_1_id'] : null),
            'bu_loc_1'       => (!is_null($loc) ? $loc['bu_loc_1'] : null),
            'bu_loc_2_id'    => (!is_null($loc) ? $loc['bu_loc_2_id'] : null),
            'bu_loc_2'       => (!is_null($loc) ? $loc['bu_loc_2'] : null),
            'bu_loc_3_id'    => (!is_null($loc) ? $loc['bu_loc_3_id'] : null),
            'bu_loc_3'       => (!is_null($loc) ? $loc['bu_loc_3'] : null),
            'bu_loc_4_id'    => (!is_null($loc) ? $loc['bu_loc_4_id'] : null),
            'bu_loc_4'       => (!is_null($loc) ? $loc['bu_loc_4'] : null),
            'bu_loc_5_id'    => (!is_null($loc) ? $loc['bu_loc_5_id'] : null),
            'bu_loc_5'       => (!is_null($loc) ? $loc['bu_loc_5'] : null),
        ];

        return $this->wv_dtl->saveWavePickDtl($wavePickDtl);
    }

    /**
     * @param $wvHdr
     * @param $wvDtl
     * @param $loc
     */
    private function saveWvDtlLoc($wvHdr, $wvDtl, $loc)
    {

        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        $data = [
            'wv_id'       => $wvHdr['wv_id'],
            'wv_dtl_id'   => $wvDtl['wv_dtl_id'],
            'sug_loc_ids' => (!is_null($loc) ? implode(',', array_keys($loc)) : null),
            'created_at'  => time(),
            'updated_at'  => time(),
            'created_by'  => $userId,
            'updated_by'  => $userId,
        ];

        // Make data of wave pick detail
        $this->wv_dtl_loc->saveWavePickDtlLoc($data);
    }

    /**
     * @param $data
     * @param $wvHdr
     */
    private function updateOdrDtl($data, $wvHdr)
    {
        // Make data to update order detail
        foreach ($data as $d) {
            $odrDtl = [
                'odr_id' => $d,
                'wv_id'  => $wvHdr['wv_id']
            ];

            $this->odrDtl->updateOrtDtl($odrDtl);
        }
    }

    /**
     * @param $odrHdrDt
     * @param $wvHdr
     */
    private function updateOdrHdr($odrHdrDt, $wvHdr)
    {
        // Make data to update order detail
        foreach ($odrHdrDt as $odrHdr) {
            $odrHdr = [
                'odr_id'  => $odrHdr['odr_id'],
                'wv_num'  => $wvHdr['wv_num'],
                'wv_id'   => $wvHdr['wv_id'],
                'odr_sts' => $odrHdr['odr_sts']
            ];

            $this->odrHdr->updateOrdHdr($odrHdr);
        }
    }

    /**
     * @param $odrDtls
     *
     * @return array
     */
    private function detectQty($odrDtls)
    {
        $ctn_qty = 0;
        $piece_qty = 0;
        $packSize = 0;
        foreach ($odrDtls as $odrDtl) {
            $piece_qty += (int)$odrDtl->alloc_qty;
            $packSize = $odrDtl->pack;
        }

        $ctn_qty = ceil($piece_qty / $packSize);

        return ([
            'ctn_qty'   => $ctn_qty,
            'piece_qty' => $piece_qty
        ]);
    }


    // when qty carton in order detail is null
    // caculate # cartons by piece remain in  table cartons
    private function getNumberCarton($odrDetail)
    {
        $itemId = $odrDetail->item_id;
        $pieceQty = $odrDetail->piece_qty;

        $ttl = 0;
        $ctn_ttl = 0;
        $ctn_ids = [0];
        while (true) {
            $carton = $this->carton->getCartonByItmId($itemId, $ctn_ids);
            if (!$carton) {
                break;
            }

            $ctn_ids[] = $carton->ctn_id;
            $ttl += $carton->piece_remain;
            $ctn_ttl++;
            if ($ttl >= $pieceQty) {
                break;
            }
        }

        return $ctn_ttl;
    }

    /**
     * @return array
     */
    private function generateWavNumAndSeq()
    {
        $wavNumber = $this->wv_hdr->getLatestWavePickNumber();

        $result = [
            'wave_num' => '',
            'seq'      => '',
        ];

        $currentYearMonth = date('ym');
        if ($wavNumber) {
            list(, $yymm, $result['seq']) = explode('-', $wavNumber->wv_num);
            // whether reset asmNum
            $wavNumber = ($yymm != $currentYearMonth) ? '' : $wavNumber;
        }

        $result['seq'] = (!$wavNumber) ? 1 : ((int)$result['seq']) + 1;

        $result['wave_num'] = sprintf('%s-%s-%s',
            'WAV',
            $currentYearMonth,
            str_pad($result['seq'], 5, '0', $this->STR_PAD_LEFT));

        return $result;
    }

    /**
     * @param $wvId
     * @param $dataWvDtls
     *
     * @return array
     */
    public function getLoationsNumberCartons($wvId, $dataWvDtls)
    {
        if (empty($dataWvDtls)) {
            return [];
        }
        $itemIds = [];
        $itemLots = [];

        foreach ($dataWvDtls as $dataWvDtl) {
            $itemIds[$dataWvDtl['ITEM_ID']] = $dataWvDtl['ITEM_ID'];
            $itemLots[$dataWvDtl['ITEM_ID']] = $dataWvDtl['Lot'];
        }

        // Load data location for excel
        $wvHdrInfo = $this->wv_hdr->getFirstWhere(['wv_id' => $wvId]);
        $wvSts = object_get($wvHdrInfo, 'wv_sts', '');
        if ($wvSts == 'CO') {
            $SugLocByWvIds = $this->wv_dtl_loc->getSugLocByWvId($wvId);
            $cartons = [];
            foreach ($SugLocByWvIds as $SugLocByWvId) {

                $act_loc_ids = !empty(array_get($SugLocByWvId, 'act_loc_ids', '')) ? (\GuzzleHttp\json_decode(array_get($SugLocByWvId, 'act_loc_ids', ''))) : '';

                if (!empty($act_loc_ids)) {
                    $loc_ids = [];
                    foreach ($act_loc_ids as $act_loc_id) {
                        if (!empty($loc_ids[object_get($act_loc_id, 'loc_id', '')])) {
                            $loc_ids[object_get($act_loc_id, 'loc_id', '')]['picked_qty'] += object_get($act_loc_id, 'picked_qty', '');
                        } else {
                            $loc_ids[object_get($act_loc_id, 'loc_id', '')] = [
                                "item_id" => array_get($SugLocByWvId, 'item_id', ''),
                                "lot" => array_get($SugLocByWvId, 'lot', 'NA'),
                                "loc_id" => object_get($act_loc_id, 'loc_id', ''),
                                "loc_code" => object_get($act_loc_id, 'loc_code', ''),
                                "plt_num" => object_get($act_loc_id, 'plt_num', ''),//LPN
                                "numCarton" => "getWvDtlLoc",//CTNS = QTY/pack size
                                "pieceRemain" => object_get($act_loc_id, 'avail_qty', ''),//QTY, pieceRemain
                                "picked_qty" => object_get($act_loc_id, 'picked_qty', ''),//ActualQTY =>
                                //ActulCTNS = ActualQTY/packsize
                                "act_loc" => object_get($act_loc_id, 'act_loc', ''),//Actual location
                                "act_loc_id" => object_get($act_loc_id, 'act_loc_id', ''),

                            ];
                        }
                    }

                    foreach ($loc_ids as $key => $val) {
                        $cartons[] = $val;
                    }
                } else {
                    $checkFromWap = DB::table('odr_cartons')
                        ->select([
                            'loc_id',
                            'loc_code',
                            DB::raw('SUM(piece_qty) as picked_qty')
                        ])
                        ->whereIn('wv_dtl_id', array_pluck($dataWvDtls, 'wv_dtl_id'))
                        ->whereNotNull('ctn_rfid')
                        ->where('deleted', 0)
                        ->groupBy('loc_id')
                        ->get();

                    if (!empty($checkFromWap)) {
                        $byWap = true;
                        $locIds = array_pluck($checkFromWap, 'loc_id');

                        $suggestLocation = $this->carton->getSugLocByLocIds($locIds, array_get($SugLocByWvId, 'item_id', ''), array_get($SugLocByWvId, 'lot', 'NA'), $byWap);
                        $data = [];
                        foreach ($checkFromWap as $locWap) {
                            $avail_qty = 0;
                            $plt_num = '';
                            if (!empty($suggestLocation)) {
                                foreach ($suggestLocation as $loc) {
                                    if ($loc['loc_id'] == $locWap['loc_id']) {
                                        $avail_qty = $loc['avail_qty'];
                                        $plt_num = $loc['plt_num'];
                                        break;
                                    }
                                }
                            }
                            $data[] = [
                                "item_id" => array_get($SugLocByWvId, 'item_id', ''),
                                "lot" => array_get($SugLocByWvId, 'lot', 'NA'),
                                "loc_id" => $locWap['loc_id'],
                                "loc_code" => $locWap['loc_code'],
                                "plt_num" => $plt_num,//LPN
                                "numCarton" => "getWvDtlLoc",//CTNS = QTY/pack size
                                "pieceRemain" => $avail_qty,//QTY, pieceRemain
                                "picked_qty" => $locWap['picked_qty'],
                                "act_loc" => $locWap['loc_code'],//Actual location
                                "act_loc_id" => $locWap['loc_id']
                            ];
                        }

                        foreach ($data as $key => $val) {
                            $cartons[] = $val;
                        }
                    }
                }
            }
        } else {
            $wvDtlLocs = $this->wv_dtl_loc->getSugLocIdByWvId($wvId)->toArray();

            $locIds = [];
            foreach ($wvDtlLocs as $wvDtlLoc) {
                $ids = array_filter(explode(',', $wvDtlLoc['sug_loc_ids']));
                $locIds = array_merge($locIds, $ids);
            }

            $cartons = $this->carton->getCartonForWavePick($locIds, $itemIds);
        }

        $tempCartons = [];
        if ($cartons) {
            foreach ($cartons as $carton) {
                $tempCartons[$carton['item_id']][$carton['lot']][] = $carton;
            }
        }


        return $tempCartons;
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function printWavepick(Request $request)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whsId = array_get($userInfo, 'current_whs', 0);

        //$input = $request->getParsedBody();
        $input = $request->getQueryParams();
        if (empty($input['wv_id']) && empty($input['odr_id'])) {
            throw new \Exception("The odr_id or wv_id input is required!");
        }

        if (empty($input['wv_id'])) {
            $odr = $this->odrHdr->getFirstBy("odr_id", $input['odr_id']);
            if (empty($odr)) {
                throw new \Exception(Message::get("BM017", "Order Wavepick"));
            }

            $input['wv_id'] = $odr->wv_id;
        }

        $wave = $this->wv_hdr->getFirstBy('wv_id', $input['wv_id'], ['pickerUser']);
        if (empty($wave)) {
            throw new \Exception(Message::get("BM017", "Wavepick"));
        }

        $wvNum = $wave->wv_num;
        $picker_firstName = object_get($wave, 'pickerUser.first_name', '');
        $picker_lastName = object_get($wave, 'pickerUser.last_name', '');
        $picker = $picker_firstName . " " . $picker_lastName;
        //print pdf file
        if (!file_exists(storage_path("WavePick/$whsId"))) {
            mkdir(storage_path("WavePick/$whsId"), 0777, true);
        }

        $this->odrHdr->refreshModel();
        $orders = $this->odrHdr->allBy('wv_id', $input['wv_id'])->toArray();
        if (empty($orders)) {
            throw new \Exception(Message::get("BM017", "Wavepick Order"));
        }

        $odrHdrNum = trim(implode(", ", array_pluck($orders, 'odr_num')));

        // Load data for excel
        $dataWvDtl = $this->wv_dtl->loadData($input['wv_id']);

        //dd($dataWvDtl);
        // Get Location for excel
        $tempCartons = $this->getLoationsNumberCartons($input['wv_id'], $dataWvDtl);

        $dataWvDtl = $dataWvDtl->toArray();

        // Prepare data to load excel
        $pickQty = $this->prepareDataForExcel($dataWvDtl);
        $temp = [];
        foreach ($pickQty as $qty) {
            $temp[$qty['ITEM_ID'] . "-" .
            $qty['SKU'] . "-" .
            $qty['Color'] . "-" .
            $qty['SIZE'] . "-" .
            $qty['Lot']][$qty['Pack_size']] = $qty;
        }

        //Export to DPF File
        return $this->printDpfData($temp, $wvNum, $odrHdrNum, $tempCartons, $whsId, $picker);

    }

    /**
     * @param $temp
     * @param $wvNum
     * @param $odrHdrNum
     * @param $tempCartons
     * @param $whsId
     * @param $picker
     *
     * @throws \MpdfException
     */
    private function printDpfData($temp, $wvNum, $odrHdrNum, $tempCartons, $whsId, $picker)
    {
        $pdf = new Mpdf();

        $html = (string)view('WavePickBarCodeTemplate', [
            'temp'        => $temp,
            'wvNum'       => $wvNum,
            'odrHdrNum'   => $odrHdrNum,
            'tempCartons' => $tempCartons,
            'picker'      => $picker,

        ]);

        $pdf->WriteHTML($html);
        //$pdf->Output("WavePickBarcode.pdf", "D");
        ///$dir = storage_path("WavePick/$whsId");
        //$pdf->Output("$dir/$wvNum.pdf", 'F');
        //$pdf->Output("$wvNum.pdf", 'D');
        $pdf->Output();
    }

    /**
     * @param Request $request
     * @param PickerValidator $pickerValidator
     * @param WavePickHdrTransformer $wavePickHdrTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function updatePicker(
        Request $request,
        PickerValidator $pickerValidator,
        WavePickHdrTransformer $wavePickHdrTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $pickerValidator->validate($input);

        $params = [
            'picker' => $input['user_id']
        ];

        try {
            DB::beginTransaction();
            // Load User
            $userInfo = $this->userModel->getUserById($input['user_id']);
            if (empty($userInfo)) {
                throw new \Exception(Message::get("BM017", "User"));
            }
            //check user status
            $pickerValidator->checkUserStatus(object_get($userInfo, 'status', null));

            // Check Exist Goods Receipt
            $wvHdrs = $this->wv_hdr->getWvHdrId($input['wv_id']);
            if (empty($wvHdrs)) {
                throw new \Exception(Message::get("BM017", "Goods Receipt"));
            }

            // update picker in wv_hdr
            $this->wv_hdr->updateWhereIn($params, $input['wv_id'], 'wv_id');

            // update picker_id in wv_dtl
            $this->wv_dtl->updateWhereIn(
                ['picker_id' => $input['user_id']],
                $input['wv_id'],
                'wv_id'
            );

            foreach($input['wv_id'] as $wvId) {
                $wvHdr = $this->wv_hdr->getModel()
                        ->find($wvId);

                $pickerIds = [$input['user_id']];
                $pickerInserts = [];
                collect($pickerIds)->map(function($id) use (&$pickerInserts) {
                    $pickerInserts[$id] =  [
                        'created_at'    => time(),
                        'created_by'    => Data::getCurrentUserId(),
                        'updated_at'    => time(),
                        'updated_by'    => Data::getCurrentUserId()
                    ];
                });

                $wvHdr->pickers()->sync($pickerInserts);

                PushNotification::alertToPicker($input['user_id'],
                    'New wave pick ' . $wvHdr['wv_num'],
                    'New wave pick have been assigned to you');
            }

            DB::commit();

            if ($wvHdrInfo = $this->wv_hdr->getWvHdrId($input['wv_id'])) {
                return $this->response->item($wvHdrInfo, $wavePickHdrTransformer);
            }

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAVE_PICK, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param PickerValidator $pickerValidator
     * @return array
     *
     */
    public function updateMultiPicker(
        Request $request,
        PickerMultiValidator $pickerMultiValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $pickerMultiValidator->validate($input);

        $wv_dt_ids = is_array($input['wv_dtl_ids']) ? $input['wv_dtl_ids'] : [$input['wv_dtl_ids']];

        try {
            DB::beginTransaction();
            // Load User
            $userInfo = $this->userModel->getUserById($input['user_id']);
            if (empty($userInfo)) {
                throw new \Exception(Message::get("BM017", "User"));
            }
            //check user status
            $pickerMultiValidator->checkUserStatus(object_get($userInfo, 'status', null));

            // Check Exist Wave pick detail
            $wvDtls = $this->wv_dtl->getWvDtlById($wv_dt_ids);
            if (empty($wvDtls)) {
                throw new \Exception(Message::get("BM017", "Wave Pick Detail"));
            }

            if (count($wvDtls) != count($wv_dt_ids)) {
                return $this->response->errorBadRequest("Wave pick detail(s) is not exists");
            }

            $this->wv_dtl->updateWhereIn(['picker_id' => $input['user_id']], $wv_dt_ids, 'wv_dtl_id');

            DB::commit();

            return ['data' => 'Successfully'];

        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAVE_PICK, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * Get Assigned Pickers
     */
    public function getAssignedPickers($wvHdrId, Request $request)
    {
        $input = $request->getParsedBody();

        $wvHdr = $this->wv_hdr->getModel()
            ->find($wvHdrId);

        if (!$wvHdr) {
            return $this->response->errorBadRequest('Could not find wavepick');
        }

        return $this->response->collection($wvHdr->pickers, new PickerTransformer());
    }

    /**
     * Assign Pickers
     * @param  pickers[ids]
     * @return success message
     */
    public function assignPickers($wvHdrId, Request $request)
    {
        $input = $request->getParsedBody();
        $pickerIds = array_get($input, 'pickers', []);

        if ( !$pickerIds ) {
            return $this->response->errorBadRequest('Pickers is required');
        }

        $wvHdr = $this->wv_hdr->getModel()
                    ->find($wvHdrId);

        if ( !$wvHdr ) {
            return $this->response->errorBadRequest('Wavepick is not exists');
        }

        $pickerInserts = [];
        collect($pickerIds)->map(function($id) use (&$pickerInserts) {
            $pickerInserts[$id] =  [
                'created_at'    => time(),
                'created_by'    => Data::getCurrentUserId(),
                'updated_at'    => time(),
                'updated_by'    => Data::getCurrentUserId()
            ];
        });

        $wvHdr->pickers()->sync($pickerInserts);


        $wvHdr->picker = $pickerIds[0];
        $wvHdr->save();

        $wvHdr->details()->update([
            'picker_id' => $pickerIds[0]
        ]);

        $event = [
            'evt_code'   => 'APS',
            'info'       => sprintf('WAP - Assign Picker(s) To WavePick %s', $wvHdr->wv_num),
            'owner'      => $wvHdr->wv_num,
            'trans_num'  => $wvHdr->wv_num,
            'whs_id'     => Data::getCurrentWhsId(),
            'cus_id'     => $wvHdr->orderHdr->first()->cus_id,
            'created_at' => time(),
            'created_by' => Data::getCurrentUserId()
        ];

        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->getModel()->insert($event);

        return ['data' => 'Assign pickers successfully'];
    }
}
