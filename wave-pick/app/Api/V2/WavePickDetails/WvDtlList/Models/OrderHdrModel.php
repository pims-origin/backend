<?php

namespace App\Api\V2\WavePickDetails\WvDtlList\Models;

use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

class OrderHdrModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getListOrderHdrInfo($odr_ids, $type)
    {
        $whs_id = self::getCurrentWhsId();
        if ($type == "updated") {
            $arr = [
                Status::getByValue('Picking', 'ORDER-STATUS'),
                Status::getByValue('Partial Picking', 'ORDER-STATUS')
            ];
        } else {
            $arr = [
                Status::getByValue('Allocated', 'ORDER-STATUS'),
                Status::getByValue('Partial Allocated', 'ORDER-STATUS')
            ];
        }

        $res = $this->model
            ->leftJoin('shipping_odr', 'shipping_odr.so_id', '=', 'odr_hdr.so_id')
            ->where('csr', '!=', 0)
            ->where('odr_hdr.whs_id', $whs_id)
            ->whereIn('odr_id', $odr_ids)
            ->whereIn('odr_sts', $arr)
            ->get();

        return $res;
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getOrderHdrNums($odr_ids)
    {
        return $this->model
            ->whereIn('odr_id', $odr_ids)
            ->lists('odr_num');
    }

    /**
     * @param $wvId
     *
     * @return mixed
     */
    public function getOrderHdrNumsByWvId($wvId, $whsId)
    {
        return $this->model
            ->join('odr_dtl', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->where('odr_hdr.whs_id', $whsId)
            ->where('odr_hdr.odr_sts', '<>', 'CC')
            ->where('odr_dtl.wv_id', $wvId)
            ->get(['odr_num', 'cus_po', 'cus_odr_num', 'odr_hdr.cus_id']);
    }

}
