<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AbstractModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\KittingPutawayModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Transformers\LocationByCustomerTransformer;
use App\Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class PackHdrController
 *
 * @package App\Api\V1\Controllers
 */
class PutBackController extends AbstractController
{
    protected $orderHdrModel;
    protected $orderDtlModel;
    protected $cartonModel;
    protected $inventorySummaryModel;
    protected $palletModel;
    protected $locationModel;

    public function __construct() {
        $this->orderHdrModel = new OrderHdrModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->cartonModel = new CartonModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
        $this->palletModel = new PalletModel();
        $this->locationModel = new LocationModel();
    }

    public function putBackDetail($odrHdrId) {
        try {
            $orderHdr = $this->orderHdrModel->getFirstWhere([
                'odr_id' => $odrHdrId,
                'whs_id' => AbstractModel::getCurrentWhsId()
            ], ['details']);

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            $kittingOdrDtl = $orderHdr->details()->with('systemUom')->where('is_kitting', 1)->get();

            $pickedTotal = 0;
            foreach ($kittingOdrDtl as &$item) {
                $item->picked_ctns = $item->qty;
                $pickedTotal += $item->picked_ctns;
            }

            $data = $kittingOdrDtl->toArray();

            foreach ($data as &$item) {
                $item['uom_code'] = !empty($item['system_uom']) ? $item['system_uom']['sys_uom_code'] : null;
                $item['uom_name'] = !empty($item['system_uom']) ? $item['system_uom']['sys_uom_name'] : null;
                unset($item['system_uom']);
            }

            return [
                // Customer
                'cus_id'               => $orderHdr->cus_id,
                'cus_name'             => object_get($orderHdr, 'customer.cus_name', null),
                'ref_cod'              => $orderHdr->ref_cod,
                'odr_sts'              => Status::getByKey("Order-status", $orderHdr->odr_sts),
                'odr_sts_name'         => $orderHdr->odr_sts,
                'odr_num'              => $orderHdr->odr_num,
                'picked_ttl'           => $pickedTotal,
                'data'                 => $data,
            ];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function updatePutBack($orderHdrId, Request $request) {
        try {
            $input = $request->getParsedBody();
            $orderHdr = $this->orderHdrModel->getFirstWhere([
                'odr_id' => $orderHdrId,
                'whs_id' => AbstractModel::getCurrentWhsId()
            ]);

            if ($orderHdr->odr_sts !== 'PA') {
                return $this->response->errorBadRequest("Only Packed order can be put back");
            }
            DB::beginTransaction();
            $kittingItems = $input['items'];
            $arrLocationIdByPallet = $arrPalletIdByLocation = [];
            $index = 0;
            foreach ($kittingItems as &$kittingItem) {
                $this->palletModel->refreshModel();
                $locId = $kittingItem['loc_id'];
                if (!empty($kittingItem['lpn'])) {
                    $rfid = $kittingItem['lpn'];
                    $checkExist = $this->palletModel->getFirstBy('rfid', $rfid);
                    if ($checkExist && !in_array($checkExist->plt_id, $arrPalletIdByLocation)) {
                        return $this->response->errorBadRequest("Pallet rfid: {$rfid} already existed.");
                    }

                    $pallet = $this->palletModel->generateNewPallet($orderHdr, $kittingItem, $index, $rfid);
                    $pallet->ctn_ttl += $kittingItem['qty'];
                    $pallet->save();

                    $pltId = $pallet->plt_id;
                } else {
                    if (isset($arrPalletIdByLocation[$locId])) {
                        $pltId = $arrPalletIdByLocation[$locId];
                    } else {
                        $index++;
                        $pallet = $this->palletModel->generateNewPallet($orderHdr, $kittingItem, $index);
                        $pltId = $pallet->plt_id;
                    }
                }

                if (isset($arrLocationIdByPallet[$pltId]) && $arrLocationIdByPallet[$pltId] != $locId) {
                    return $this->response->errorBadRequest("Pallet must belong to only 1 location");
                }
                $arrLocationIdByPallet[$pltId] = $locId;

                if (isset($arrLocationIdByPallet[$locId]) && $arrLocationIdByPallet[$locId] != $pltId) {
                    return $this->response->errorBadRequest("Location must contain only 1 pallet");
                }
                $arrPalletIdByLocation[$locId] = $pltId;
                $kittingItem['plt_id'] = $pltId;
            }
            //Update Put Back cartons
            $this->cartonModel->updatePutBackCartons($kittingItems);

            //Update Inventory Summary
            $this->inventorySummaryModel->updateKittingInventory($kittingItems);

            $kittingPutawayModel = new KittingPutawayModel();
            $kittingPutawayModel->createKittingPutaway($kittingItems, $orderHdrId);

            //Update Order Status
            $orderHdr->odr_sts = 'CO';
            $orderHdr->save();

            //Update Order Detail Status
            $this->orderDtlModel->updateWhere([
                'itm_sts' => 'CO'
            ], [
                'odr_id' => $orderHdrId,
                'is_kitting' => 1
            ]);

            DB::commit();
            return ['data' => 'Updated successfully'];
        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function locationsByCustomer($whsId, $cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['cus_id'] = $cusId;
        $limit = array_get($input, 'limit', 20);

        try {
            $location = $this->locationModel->getLocationsByCustomer($input, [], $limit);

            return $this->response->paginator($location, new LocationByCustomerTransformer());

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function validationPallet(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $pltRfid = strtoupper(array_get($input, 'plt_rfid'));
        if (!$pltRfid || $pltRfid == "") {
            throw new \Exception("The pallet RFID is required!");
        }

        //validate pallet RFID
        $this->palletModel->validateLPNFormat($pltRfid, $this->_getCurrentWhsCode());

        try {

            $checkPltExist = $this->palletModel->getFirstWhere(['rfid' => $pltRfid]);
            if ($checkPltExist) {
                throw new \Exception("The pallet RFID existed!");
            }

            return [];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _getCurrentWhsCode()
    {
        $userData = (Data::getInstance())->getUserInfo();
        $userWarehouse = array_get($userData, 'user_warehouses');
        $currentWhsId = array_get($userData, 'current_whs');
        foreach ($userWarehouse as $warehouse) {
            if ($warehouse['whs_id'] == $currentWhsId) {
                return $warehouse['whs_code'];
            }
        }
    }
}
