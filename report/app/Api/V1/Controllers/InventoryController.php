<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\InventoryModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Models\CustomersModel;
use App\Api\V1\Transformers\InventoryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;


class InventoryController extends AbstractController
{

    protected $systemStateModel;
    protected $InventoryModel;
    protected $InventoryTransformer;
    protected $systemStateTransformer;

    protected $orderCartonModel;
    protected $customersModel;

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->InventoryModel = new InventoryModel();
        $this->InventoryTransformer = new InventoryTransformer();
        $this->systemStateModel = new SystemStateModel();
        $this->systemStateTransformer = new SystemStateTransformer();

        $this->orderCartonModel = new OrderCartonModel();
        $this->customersModel = new CustomersModel();
    }

    /*
    ****************************************************************************
    */

    public function search(Request $request)
    {
        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', PAGING_LIMIT);

        $model = $this->InventoryModel;
        $transformer = $this->InventoryTransformer;

        try {

            $Inventory = $model->search($input, [], $limit);

            return $this->response->paginator($Inventory, $transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /*
    ****************************************************************************
    */
    public function autoSku($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $orderCartons = $this->orderCartonModel->autoSku($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $orderCartons];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    public function autoOrderNum($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $orderCartons = $this->orderCartonModel->autoOrderNum($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $orderCartons];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    public function customers($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $orderCartons = $this->orderCartonModel->customers($whs_id, $input);

            return ['data' => $orderCartons];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }
    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array
     */
    public function customersInvDropDown($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            //$inventoryCustomers = $this->InventoryModel->customers($whs_id, $input);
            $inventoryCustomers = $this->customersModel->customers($whs_id, $input, []);

            return ['data' => $inventoryCustomers];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function skuInvAutoComplete($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $inventorySkus = $this->InventoryModel->skuInvAutoComplete($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $inventorySkus];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    public function exportInventory(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $getBaseUrl = env('S3_PIMS_DIR').'Inventory.xlsx';
            $file = Storage::disk('s3')->get($getBaseUrl );
            $headers = [
                'Content-Type' => 'xlsx',
                'Content-Description' => 'File Transfer',
                'Content-Disposition' => "attachment; filename='Inventory.xlsx'",
                'filename'=> 'Inventory.xlsx'
            ];
            return response($file, 200, $headers);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }



}
