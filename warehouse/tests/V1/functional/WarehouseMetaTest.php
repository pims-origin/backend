<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Warehouse;
use App\WarehouseMeta;

class WarehouseMetaTest extends TestCase
{
    use DatabaseTransactions;

    protected $warehouseId;

    public function setUp()
    {
        parent::setUp();

        // create data into DB
        $warehouse = factory(App\Warehouse::class)->create();

        $this->warehouseId = $warehouse->whs_id;
    }

    private function getEndPoint()
    {
        return "/v1/warehouses/{$this->warehouseId}/meta";
    }

    public function testIndex_NoAnyRecords_Ok()
    {
        $endPoint = $this->getEndPoint();

        $response = $this->call('GET', $endPoint);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertEmpty($responseData['data']);
    }

    public function testIndex_HaveRecords_Ok()
    {
        // make data
        factory(App\WarehouseMeta::class, 3)->create(['whs_id' => $this->warehouseId]);

        $endPoint = $this->getEndPoint();

        $response = $this->call('GET', $endPoint);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
        $this->assertCount(3, $responseData['data']);
    }

    public function testStore_NoParam_Fail()
    {
        $endPoint = $this->getEndPoint();
        $response = $this->call('POST', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_HasParam_Ok()
    {
        // make data
        $warehouseQualifier = factory(App\WarehouseQualifier::class)->create();

        $endPoint = $this->getEndPoint();
        $params = [
            'whs_id'         => $this->warehouseId,
            'whs_qualifier'  => $warehouseQualifier->qualifier,
            'whs_meta_value' => ''
        ];

        $response = $this->call('POST', $endPoint, $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
        $responseMeta = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('whs_id', $responseMeta['data']);
        $this->assertArrayHasKey('whs_qualifier', $responseMeta['data']);
        $this->assertArrayHasKey('whs_meta_value', $responseMeta['data']);
    }

    public function testStore_QualifierNotExisted_Fail()
    {
        // make data
        $warehouseQualifier = factory(App\WarehouseQualifier::class)->create();

        $endPoint = $this->getEndPoint();
        $params = [
            'whs_id'         => $this->warehouseId,
            'whs_qualifier'  => '999999',
            'whs_meta_value' => ''
        ];

        $response = $this->call('POST', $endPoint, $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testSaveMassAndDeleteUnused_HasParam_Ok()
    {
        $qualifiers = factory(App\WarehouseQualifier::class, 2)->create()->toArray();
        $endPoint = $this->getEndPoint() . '/save-mass-and-delete-unused';
        // make data
        foreach ($qualifiers as $key => $val) {
            $params['whs_meta_value'][$key] = 'Test';
            $params['whs_qualifier'][$key] = $val['qualifier'];
        }

        $response = $this->call('POST', $endPoint, $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testSaveMassAndDeleteUnused_NoParam_Fail()
    {
        $endPoint = $this->getEndPoint() . '/save-mass-and-delete-unused';
        $response = $this->call('POST', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testShow_Ok()
    {
        $warehouseQualifier = factory(App\WarehouseQualifier::class)->create();
        $endPoint = $this->getEndPoint() . '/' . $warehouseQualifier->qualifier;
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testShow_QualifierNotExisted_Ok()
    {
        $endPoint = $this->getEndPoint() . '/999999999';
        $this->call('GET', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_Ok()
    {
        // make data
        $warehouseQualifier = factory(App\WarehouseQualifier::class)->create();
        $warehouseMeta = factory(App\WarehouseMeta::class)->create(['whs_id'        => $this->warehouseId,
                                                                    'whs_qualifier' => $warehouseQualifier->qualifier
        ]);
        $endPoint = $this->getEndPoint() . '/' . $warehouseQualifier->qualifier;

        $params = [
            'whs_id'         => $warehouseMeta->whs_id,
            'whs_qualifier'  => $warehouseMeta->whs_qualifier,
            'whs_meta_value' => 'Test'
        ];

        $response = $this->call('PUT', $endPoint, $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseMeta = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('whs_id', $responseMeta['data']);
        $this->assertArrayHasKey('whs_qualifier', $responseMeta['data']);
        $this->assertArrayHasKey('whs_meta_value', $responseMeta['data']);
    }

    public function testUpdate_QualifierNotExisted_Fail()
    {
        $endPoint = $this->getEndPoint() . '/' . 999999;

        $params = [
            'whs_id'         => $this->warehouseId,
            'whs_meta_value' => ''
        ];
        $response = $this->call('PUT', $endPoint, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDestroy_SingleItem_Ok()
    {

        $warehouseQualifier = factory(App\WarehouseQualifier::class)->create();
        $warehouseMeta = factory(App\WarehouseMeta::class)->create(['whs_id'        => $this->warehouseId,
                                                                    'whs_qualifier' => $warehouseQualifier->qualifier
        ]);
        $endPoint = $this->getEndPoint() . '/' . $warehouseMeta->whs_qualifier;

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDestroy_QualifierNotExisted_Fail()
    {
        // make data
        $warehouseQualifier = factory(App\WarehouseQualifier::class)->create();

        $endPoint = $this->getEndPoint() . '/999999';

        $this->call('DELETE', $endPoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }
}
