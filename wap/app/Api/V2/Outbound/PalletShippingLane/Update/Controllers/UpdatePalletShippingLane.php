<?php

namespace App\Api\V2\Outbound\PalletShippingLane\Update\Controllers;

use App\Api\V2\Outbound\PalletShippingLane\Update\Models\OutPalletModel;
use App\Api\V2\Outbound\PalletShippingLane\Update\Validators\OutPalletValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Wms2\UserInfo\Data;

class UpdatePalletShippingLane extends AbstractController
{
    protected $outPalletModel;


    public function __construct(OutPalletModel $outPalletModel)
    {
        $this->outPalletModel = $outPalletModel;

    }//end __construct()


    public function update(Request $request, $whs_id)
    {
        $params = $request->getParsedBody();
        if (!isset($params['plt_num']) || empty($params['plt_num']) || trim($params['plt_num']) === '') {
            return $this->_responseMessage($params, false, 'Can not delete this pallet !');
        }

        $params['whs_id'] = $whs_id;
        $outPallet = $this->outPalletModel->getOutPalletByWhsIdAndPltNum($params);
        if (count($outPallet) == 0) {
            $msg = sprintf("The Pallet num %s doesn't exist.", $params['plt_num']);
            $chkPallet = $this->outPalletModel->getFirstWhere(['plt_num' => $params['plt_num']]);
            if ($chkPallet) {
                if (!$chkPallet->loc_id) {
                    $msg = 'The Pallet is not on any location.';
                }
                if ($chkPallet->whs_id != $whs_id) {
                    $msg = 'The Pallet does not belong current warehouse.';
                }
            }
            return $this->_responseMessage($params, false, $msg);
        }

        $userInfo = new Data();

        $data = [
                 'loc_id'     => null,
                 'loc_name'   => null,
                 'loc_code'   => null,
                 'updated_by' => $userInfo->getCurrentUserId(),
                ];

        if (!$updateOutPallet = $this->outPalletModel->updateOutPalletByWhsIdAndPltNum($params, $data)) {
            return $this->_responseMessage($params, false, 'Can not delete this pallet !');
        }

        return $this->_responseMessage($params, true, 'Successfully !');

    }//end update()


    private function _responseMessage($data=[], $status=true, $msg=null)
    {
        return [
                'status'   => $status,
                'iat'      => time(),
                'data'     => $data,
                'messages' => [[
                                'status_code' => $status ? 1 : (-1),
                                'msg'         => $msg,
                               ],
                              ],
               ];

    }//end _responseMessage()


}//end class
