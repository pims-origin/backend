<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\Bay\Models;

use Seldat\Wms2\Models\ContainerCharge;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SelArr;

class ContainerChargeModel extends AbstractModel
{
    /**
     * ContainerChargeModel constructor.
     *
     * @param ContainerCharge|null $model
     */
    public function __construct(ContainerCharge $model = null)
    {
        $this->model = ($model) ?: new ContainerCharge();
    }

    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $this->sortBuilder($query, $attributes);
        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }

    public function getCtnrChargeOpenByBayIds($bayIds)
    {
        $bayIds = is_array($bayIds) ? $bayIds : [$bayIds];

        $rows = $this->model
            ->where('cc_sts', Status::getByValue('Open', 'Container_Charge_Status'))
            ->whereIn('bay_id', $bayIds)
            ->get();

        return $rows;
    }

    public function report($attributes, array $with, $limit)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes['bay_id'])) {
            $query->where('bay_id', $attributes['bay_id']);
        }

        if (!empty($attributes['cus_id'])) {
            $query->where('cus_id', $attributes['cus_id']);
        }

        if (!empty($attributes['ctnr_id'])) {
            $query->where('ctnr_id', $attributes['ctnr_id']);
        }

        if (!empty($attributes['start_from'])) {
            $query->where('start', ">=", date("Y-m-d H:i:s", strtotime($attributes['start_from'] . " 00:00:00")));
        }

        if (!empty($attributes['start_to'])) {
            $query->where('start', "<=", date("Y-m-d H:i:s", strtotime($attributes['start_to'] . " 23:59:59")));
        }

        if (!empty($attributes['end_from'])) {
            $query->where('end', ">=", date("Y-m-d H:i:s", strtotime($attributes['end_from'] . " 00:00:00")));
        }

        if (!empty($attributes['end_to'])) {
            $query->where('end', "<=", date("Y-m-d H:i:s", strtotime($attributes['end_to'] . " 23:59:59")));
        }

        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }
}
