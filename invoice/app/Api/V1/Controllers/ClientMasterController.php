<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Response as IlluminateResponse;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\ClientMasterModel;
use App\Api\V1\Transformers\ClientMasterTransformer;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

class ClientMasterController extends AbstractController
{
    protected $clientMasterModel;
    protected $clientMasterTransformer;

    /*
    ****************************************************************************
    */

    public function __construct(IRequest $request)
    {
//        $object = new AuthenticationService($request);
//
//        parent::__construct($request, $object);

        $this->clientMasterModel = new ClientMasterModel();
        $this->clientMasterTransformer = new ClientMasterTransformer();
    }

    /*
    ****************************************************************************
    */

    public function getClientInfo(Request $request)
    {
        $input = $request->getQueryParams();

        try {
            return $this->clientMasterModel->getClientInfo($input['cus_id']);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /*
    ****************************************************************************
    */

    public function update($custID, Request $request)
    {
        $customerInfo = $request->getParsedBody();

        $model = $this->clientMasterModel;
        $transformer = $this->clientMasterTransformer;

        try {

            $results = $model->updateCustomerInfo($custID, $customerInfo);

            return $this->response->item($results, $transformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /*
    ****************************************************************************
    */

}
