<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CycleHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\CycleCountNotification;

class CompleteLocationTransformer extends TransformerAbstract
{
    /*
     * Transform CycleCountNotification
     * 
     * @param object $ccNotice
     * 
     * @return array
     */

    public function transform(CycleCountNotification $ccNotice)
    {
        return [
            'ccn_id' => $ccNotice->ccn_id,
            'whs_id' => $ccNotice->whs_id,
            'loc_id' => $ccNotice->loc_id,
            'loc_code' => $ccNotice->loc_code,
            'created_at' => $ccNotice->created_at->format("Y-m-d"),
        ];
    }

}
