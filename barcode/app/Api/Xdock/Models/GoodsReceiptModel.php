<?php
namespace App\Api\Xdock\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\Pallet;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;

class GoodsReceiptModel extends AbstractModel
{
    const ROLE = ['admin', 'manager'];
    public function __construct($model = null)
    {

    }
    public function validateLPNFormat($lpn, $whsInfo)
    {
        //mbs-pl-000001
        $whsCode = $whsInfo['whs_code'];
        $whsId = $whsInfo['whs_id'];

        $pattern = "/^$whsCode-PL([F,B,G]{0,1})-([0-9]{6})$/";
        if (!preg_match($pattern, $lpn)) {
            throw new HttpException(403, "Invalid pallet id format");
        }
    }

    public function validateLpnGR($lpn, $grNum)
    {
        $l = str_replace('GDR', 'LPN', $grNum);
        $pattern = sprintf('/^%s-([0-9]{3})$/', $l);
        if (!preg_match($pattern, $lpn)) {
            throw new HttpException(403, "Invalid LPN with current GR num");
        }

    }

    public function validateLpnInRange($lpn, $lpnTtl)
    {
        $parts = explode('-', $lpn);
        if(end($parts) > $lpnTtl){
            throw new HttpException(403, "LPN out of range,, please reprint LPN");
        }

    }

    public function validateRfidExisted($rfid)
    {
        $plt = Pallet::where('plt_num', 'LIKE', $rfid . '-%')
            ->orderBy('plt_id', 'DESC')
            ->first();

        return $plt;
    }

    public function updateInventory($whsId, $cusId, $grDtl, $availQty, $dmgQty) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $invt = InventorySummary::where([
                'item_id' => $grDtl['item_id'],
                'lot' => $grDtl['lot'],
                'whs_id' => $whsId,
                'cus_id' => $cusId
            ])
            ->first();
        $ttl = ($availQty + $dmgQty);
        if($invt) {
            $invt->avail += $availQty;
            $invt->dmg_qty += $dmgQty;
            $invt->ttl +=  $ttl;
            return  $invt->save();
        } else {
            $arrInput = [
                'item_id' => $grDtl['item_id'],
                'cus_id' => $cusId,
                'whs_id' => $whsId,
                'color' => $grDtl['color'],
                'size' => $grDtl['size'],
                'lot' => $grDtl['lot'],
                'ttl' => $ttl,
                'picked_qty' => 0,
                'allocated_qty' => 0,
                'dmg_qty' => $dmgQty,
                'avail' => $availQty,
                'sku' => $grDtl['sku'],
                'upc' => $grDtl['upc'],
                'back_qty' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ];
            return InventorySummary::insert($arrInput);
        }
    }

    public function createInventory($whsId, $cusId, $grDtl, $lot) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $invt = InventorySummary::where([
            'item_id' => $grDtl['item_id'],
            'lot' => $lot,
            'whs_id' => $whsId,
            'cus_id' => $cusId
        ])
            ->first();

        if($invt) {
            return;
        } else {
            $arrInput = [
                'item_id' => $grDtl['item_id'],
                'cus_id' => $cusId,
                'whs_id' => $whsId,
                'color' => $grDtl['color'],
                'size' => $grDtl['size'],
                'lot' => $lot,
                'ttl' => 0,
                'picked_qty' => 0,
                'allocated_qty' => 0,
                'dmg_qty' => 0,
                'avail' => 0,
                'sku' => $grDtl['sku'],
                'upc' => $grDtl['upc'],
                'back_qty' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ];
            return InventorySummary::insert($arrInput);
        }
    }

    public function getPutAwayList($whsId)
    {
        $sqlScan = '(SELECT COUNT(pallet.plt_id)  
                           FROM pallet 
                           WHERE pallet.gr_hdr_id = gr_hdr.gr_hdr_id 
                                AND pallet.loc_id IS NOT NULL 
                                AND pallet.ctn_ttl > 0 
                     ) AS pallet_scan';
        $sqlTtl = '(SELECT COUNT(pallet.plt_id)  
                           FROM pallet 
                           WHERE pallet.gr_hdr_id = gr_hdr.gr_hdr_id 
                           AND pallet.ctn_ttl > 0 
                     ) AS pallet_total';

        $sql = '(SELECT COUNT(pallet.plt_id)  
                           FROM pallet 
                           WHERE pallet.gr_hdr_id = gr_hdr.gr_hdr_id 
                           AND pallet.ctn_ttl > 0 
                     ) > (SELECT COUNT(pallet.plt_id)  
                           FROM pallet 
                           WHERE pallet.gr_hdr_id = gr_hdr.gr_hdr_id 
                                AND pallet.loc_id IS NOT NULL 
                                AND pallet.ctn_ttl > 0 
                     )';

        /*
         * package common
         * update putaway in GUI
         * Update putaway with last pallet
         */
        $query = DB::table('gr_hdr')
            ->select([
                'gr_hdr_id',
                'gr_hdr_num',
                'ctnr_num',
                DB::raw($sqlScan),
                DB::raw($sqlTtl),
                'gr_hdr.updated_at'
            ])
            ->where('whs_id', $whsId)
            //->where('putaway', 0)
            ->whereRaw($sql) // Remove it
            ;

        $query->orderBy('gr_hdr.updated_at', 'DESC');
        $query->orderBy('pallet_scan', 'DESC');

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $result = $query->get();
        return $result->toArray();
    }

    /**
     * @return bool
     */
    public function checkPermissionAdminManager($userId)
    {
        //  Role Amin & Manager are get pull data
        $role = DB::table('user_role')->select('item_name')
            ->where('user_id', $userId)->first();

        $flag = false;
        if(in_array(strtolower($role['item_name']), self::ROLE)) {
            $flag = true;
        }

        return $flag;
    }


}