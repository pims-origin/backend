<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\IndicatorModel;
use App\Api\V1\Transformers\EventTrackingListsTransformer;
use App\Api\V1\Transformers\EventTrackingAutoTransformer;
use App\Api\V1\Transformers\EventTrackingUserTransformer;
use App\Api\V1\Transformers\EventTrackingReportsTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;

class EventTrackingController extends AbstractController
{

    /**
     * @param Request $request
     * @param EventTrackingModel $eventTrackingModel
     * @param EventTrackingListsTransformer $eventTrackingListsTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        Request $request,
        EventTrackingModel $eventTrackingModel,
        EventTrackingListsTransformer $eventTrackingListsTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            // get list event tracking
            $input = array_filter($input); //remove empty search conditions
            $evt = $eventTrackingModel->search($input, ['event'], array_get($input, 'limit'));

            return $this->response->paginator($evt, $eventTrackingListsTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param IndicatorModel $indicatorModel
     *
     * @return array|void
     */
    public function indicatorDropDown(
        Request $request,
        IndicatorModel $indicatorModel
    ) {
        try {
            $input = $request->getQueryParams();
            $indicatorDropDownInfos = $indicatorModel->indicatorDropDown($input);

            return ['data' => $indicatorDropDownInfos];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'eventTracking', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $userId
     * @param EventTrackingModel $eventTrackingModel
     * @param EventTrackingReportsTransformer $eventTrackingReportsTransformer
     *
     * @return mixed
     */
    public function report(
        Request $request,
        $userId,
        EventTrackingModel $eventTrackingModel,
        EventTrackingReportsTransformer $eventTrackingReportsTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            // get list event tracking
            $evt = $eventTrackingModel->evtReports($userId, $input, ['event'], array_get($input, 'limit'));

            return $this->response->paginator($evt, $eventTrackingReportsTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $userId
     * @param Request $request
     * @param EventTrackingModel $eventTrackingModel
     *
     * @return mixed
     */
    public function performance(
        $userId,
        Request $request,
        EventTrackingModel $eventTrackingModel
    ) {
        $input = $request->getQueryParams();

        try {
            // get list event tracking
            $performance = $eventTrackingModel->performance($userId, $input);

            return $performance;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param EventTrackingModel $eventTrackingModel
     *
     * @return mixed
     */
    public function getListUser(
        Request $request,
        EventTrackingModel $eventTrackingModel,
        EventTrackingUserTransformer $eventTrackingUserTransformer
    ) {
        $input = $request->getQueryParams();
        try {
            // get list user event tracking
            $userData = $eventTrackingModel->getListUser($input, [], array_get($input, 'limit', 999999));
            return $this->response->paginator($userData, $eventTrackingUserTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param EventTrackingModel $eventTrackingModel
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function autoOwner(
        Request $request,
        EventTrackingModel $eventTrackingModel,
        EventTrackingAutoTransformer $eventTrackingAutoTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            // get list event tracking by owner
            $eventTracking = $eventTrackingModel->autocomplete($input, [], array_get($input, 'limit', 10));
            return $this->response->paginator($eventTracking, $eventTrackingAutoTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
