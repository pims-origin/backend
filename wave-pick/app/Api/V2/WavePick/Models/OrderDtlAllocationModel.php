<?php

namespace App\Api\V2\WavePick\Models;

use \Seldat\Wms2\Models\OrderDtlAllocation;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;

class OrderDtlAllocationModel extends AbstractModel
{
    /**
     * @param OrderDtlAllocation $model
     */
    public function __construct(OrderDtlAllocation $model = null)
    {
        $this->model = ($model) ?: new OrderDtlAllocation();
    }

    /**
     * @param $wvDtlId
     *
     * @return mixed
     */
    public function getPickPltByWvDtlId($wvDtlId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
            ->select([
                'odr_dtl_allocation.loc_id',
                'odr_dtl_allocation.loc_code',
                'odr_dtl_allocation.qty as avail_qty',
                'pallet.rfid'
            ])
            ->leftJoin('pallet', 'pallet.plt_id', '=', 'pallet.plt_id')
            ->where('odr_dtl_allocation.wv_dtl_id', $wvDtlId)
            ->get();
    }

    public function getLocationPickPalletByOrderDtl($odrDtlIds)
    {
        $odrDtlIds = explode(',', $odrDtlIds);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
            ->whereIn('odr_dtl_allocation.odr_dtl_id', (array)$odrDtlIds)
            ->get()->toArray();
    }
}