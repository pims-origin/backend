<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function freshTimestamp()
    {
        return time();
    }

    public function fromDateTime($value)
    {
        return $value;
    }

    public function getDateFormat()
    {
        return 'U'; // PHP date() Seconds since the Unix Epoch
    }
}
