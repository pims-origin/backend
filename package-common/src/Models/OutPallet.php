<?php

namespace Seldat\Wms2\Models;

class OutPallet extends BaseSoftModel
{
    protected $table = 'out_pallet';

    /**
     * @var array
     */
    protected $fillable = [
        'loc_id',
        'cus_id',
        'whs_id',
        'plt_num',
        'plt_block',
        'plt_tier',
        'ctn_ttl',
        'loc_name',
        'loc_code',
        'created_by',
        'updated_by',
        'bol_id',
        'parent',
        'item_id',
        'sku',
        'size',
        'color',
        'cus_upc',
        'volume',
        'length',
        'width',
        'height',
        'weight',

    ];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'plt_id';

    public function shipment()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Shipment', 'bol_id', 'ship_id');
    }

    public function packHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\PackHdr', 'plt_id', 'out_plt_id');
    }
}
