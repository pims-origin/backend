<?php
namespace Seldat\Wms2\Models;

/**
 * Class WorkOrderSku
 *
 * @package Seldat\Wms2\Models
 */
class WorkOrderSku extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wo_sku';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wo_sku_id';
    /**
     * @var array
     */
    protected $fillable = [
        'wo_hdr_id',
        'odr_hdr_id',
        'odr_dtl_id',
        'whs_id',
        'cus_id',
        'old_item_id',
        'item_id'
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function orderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_hdr_id');
    }

    public function item()
    {
        return $this->hasOne(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }
}
