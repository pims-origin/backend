<?php

/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\chargeCode;
use League\Fractal\TransformerAbstract;

class CustomerDamageTransformer extends TransformerAbstract
{

    public function transform($data)
    {
        return [
            'cus_id' => object_get($data, 'cus_id', ''),
            'cus_name' => object_get($data, 'cus_name', ''),
            'whs_id' => object_get($data, 'whs_id', ''),
        ];
    }

}
