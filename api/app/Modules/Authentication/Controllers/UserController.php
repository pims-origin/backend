<?php

namespace App\Modules\Authentication\Controllers;

use App\Modules\Authentication\Models\UserModel;
use App\Modules\Authentication\Resources\TokenResource;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use App\Modules\Authentication\Validators\LoginValidator;


class UserController extends Controller
{
    public function loginUser(Request $request, LoginValidator $validator)
    {
        $validated = $validator->validate($request->all());
        $params = $request->post();
        $user = (new UserModel())->getUserbyUsername($params['username']);
        $validator->checkUserExistAndActive($user);
        $validator->checkPasswordMatch($params['password'], $user->password);
        $credentials = $request->only('username', 'password');
        if (!$token = app('tymon.jwt.auth')->attempt($credentials)) {
            throw new \Exception('Unauthorized');
        }
        $loginData = (object)["token" => $token, "user_id" => $user->user_id];
        return new TokenResource($loginData);
    }

}
