<?php


namespace App\Modules\Orders\Eloquents;


class OrderCarton extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'odr_cartons';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_ctn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'odr_ctn_id',
        'odr_hdr_id',
        'odr_dtl_id',
        'wv_hdr_id',
        'wv_dtl_id',
        'odr_num',
        'wv_num',
        'ctnr_rfid',
        'ctn_num',
        'piece_qty',
        'ship_dt',
        'ctn_id',
        'is_storage',
        'ctn_rfid',
        'ctn_sts',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'upc',
        'pack',
        'uom_id',
        'uom_code',
        'uom_name',
        'loc_id',
        'loc_code',
        'length',
        'width',
        'height',
        'weight',
        'volume',
        'plt_id',
        'plt_rfid',
    ];
}
