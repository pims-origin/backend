<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ThirdPartyModel;
use App\Api\V1\Transformers\ThirdPartyTransformer;
use App\Api\V1\Validators\ThirdPartyValidator;
use Dingo\Api\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Models\SystemState;
use Seldat\Wms2\Utils\Message;

class ThirdPartyController extends AbstractController
{
    /**
     * @var thirdPartyModel
     */
    protected $thirdPartyModel;

    /**
     * AsnController constructor.
     *
     * @param ThirdPartyModel $thirdPartyModel
     */
    public function __construct(
        ThirdPartyModel $thirdPartyModel
    ){
        $this->thirdPartyModel = $thirdPartyModel;
    }

    public function search(
        $cusId,
        IRequest $request,
        ThirdPartyTransformer $thirdPartyTransformer
    ){
        $input = $request->getQueryParams();

        try {

            $thirdParty = $this->thirdPartyModel->search($cusId, $input, array_get($input, 'limit', 20));
            return $this->response->paginator($thirdParty, $thirdPartyTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function store(
        $cusId,
        IRequest $request,
        ThirdPartyValidator $thirdPartyValidator,
        ThirdPartyTransformer $thirdPartyTransformer
    ){
        $input = $request->getParsedBody();
        // validation
        $input['cus_id'] = $cusId;
        $thirdPartyValidator->validate($input);

        // check address
//        $checkAddr = $this->checkAddress($input);
//        if (!$checkAddr) {
//            $this->response->errorBadRequest('Address not existed');
//        }

        //Valid name duplicate
        // if ($this->thirdPartyModel->getFirstWhere([
        //     'cus_id' => $cusId,
        //     'type' => $input['type'],
        //     'name' => $input['name']
        // ])) {
        //     $this->response->errorBadRequest(Message::get('BM006', 'Name'));
        // }

        try {

            $data = [
                'cus_id'        => $cusId,
                'type'          => array_get($input, 'type'),
                'name'          => array_get($input, 'name'),
                'des'           => array_get($input, 'des', null),
                'phone'         => array_get($input, 'phone', null),
                'mobile'        => array_get($input, 'mobile', null),
                'ct_first_name' => array_get($input, 'ct_first_name', null),
                'ct_last_name'  => array_get($input, 'ct_last_name', null),
                'add_1'         => array_get($input, 'add_1'),
                'add_2'         => array_get($input, 'add_2', null),
                'city'          => array_get($input, 'city'),
                'state'         => array_get($input, 'state'),
                'zip'           => array_get($input, 'zip'),
                'country'       => array_get($input, 'country')
            ];

            $thirdParty = $this->thirdPartyModel->create($data);

            return $this->response->item($thirdParty, $thirdPartyTransformer)
                    ->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function update(
        $cusId,
        $tpId,
        IRequest $request,
        ThirdPartyValidator $thirdPartyValidator,
        ThirdPartyTransformer $thirdPartyTransformer
    ){
        $input = $request->getParsedBody();
        
        // validation
        $input['cus_id'] = $cusId;
        $thirdPartyValidator->validate($input);

        // check address
//        $checkAddr = $this->checkAddress($input);
//        if (!$checkAddr) {
//            $this->response->errorBadRequest('Address not existed');
//        }

        //Valid Third Party exist
        $thirdParty = $this->thirdPartyModel->getFirstWhere([
            'tp_id'  => $tpId,
            'cus_id' => $cusId
        ]);

        if (! $thirdParty) {
            $this->response->errorBadRequest(Message::get('BM017', 'Third Party'));
        }

        //Valid name duplicate
        $checkDuplicateName = ($thirdParty->name != $input['name']) ? true : false;

        if ($checkDuplicateName && $this->thirdPartyModel->getFirstWhere([
            'cus_id' => $cusId,
            'type' => $input['type'],
            'name' => $input['name']
        ])) {
            $this->response->errorBadRequest(Message::get('BM006', 'Name'));
        }

        try {

            $data = [
                'tp_id'         => $tpId,
                'cus_id'        => $cusId,
                'type'          => array_get($input, 'type'),
                'name'          => array_get($input, 'name'),
                'des'           => array_get($input, 'des', null),
                'phone'         => array_get($input, 'phone', null),
                'mobile'        => array_get($input, 'mobile', null),
                'ct_first_name' => array_get($input, 'ct_first_name', null),
                'ct_last_name'  => array_get($input, 'ct_last_name', null),
                'add_1'         => array_get($input, 'add_1'),
                'add_2'         => array_get($input, 'add_2', null),
                'city'          => array_get($input, 'city'),
                'state'         => array_get($input, 'state'),
                'zip'           => array_get($input, 'zip'),
                'country'       => array_get($input, 'country')
            ];

            $thirdParty = $this->thirdPartyModel->update($data);

            return $this->response->item($thirdParty, $thirdPartyTransformer)
                ->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } 
    }

    public function show(
        $cusId,
        $tpId,
        IRequest $request,
        ThirdPartyTransformer $thirdPartyTransformer
    ){
        try {
            
            if (! ($thirdParty = $this->thirdPartyModel->getFirstWhere([
                'tp_id'  => $tpId,
                'cus_id' => $cusId
            ]))) {
                $this->response->errorBadRequest(Message::get('BM017', 'Third Party'));
            }
            return $this->response->item($thirdParty, $thirdPartyTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function checkAddress($input)
    {
        $systemState = (new SystemState())->where('sys_state_code', array_get($input, 'state'))->first();
        if(!empty($systemState)) {
            $state = $systemState->sys_state_name;
        } else {
            $state = '';
        }

        $address = array_get($input, 'add_1').', '.array_get($input, 'add_2', null)
            .', '.array_get($input, 'city').', '.$state.', '.array_get($input, 'zip');

        $check = false;
        $client = new Client();
        try {
            $response = $client->get("https://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false");
            if($response->getStatusCode() == "200"){
                $result = $response->getBody()->getContents();
                $data = json_decode($result);
                if(!empty($data->results)){
                    $check = true;
                }
            }
        } catch (\Exception $e) {
            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            $messageHandle =
                'Message: ' . $e->getMessage(). "\n".
                'File:' . __FILE__ . "\n".
                'Class:' . __CLASS__ . "\n".
                'Function:' . __FUNCTION__ . "\n".
                'Line Number:' . __LINE__ . "\n"
            ;

            Log::error($messageHandle);

            //Show error when run CLI
            Log::error($e->getMessage());
            Log::error($errorInfo);
        }

        return $check;
    }
}
