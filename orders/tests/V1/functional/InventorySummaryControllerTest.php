<?php
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Utils\Status;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Message;

class InventorySummaryControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function getEndPoint()
    {
        return "/v1/invt-smr/";
    }

    public function getPoint()
    {
        return "/v1/";
    }

    //public function test_updateAllocate_Ok()
    //{
    //    // Create orderHdr before Show
    //    $orderHdrInfo = factory(OrderHdr::class)->create();
    //    $odr_id = $orderHdrInfo->odr_id;
    //
    //    // Create orderDtl before Show
    //    $orderDtlInfo = factory(OrderDtl::class)->create(
    //        [
    //            'odr_id' => $odr_id,
    //        ]
    //    );
    //    $itm_id = $orderDtlInfo->itm_id;
    //
    //    // Create invt_smr before Show
    //    $invt_smrInfo = factory(InventorySummary::class)->create(
    //        [
    //            'itm_id' => $itm_id,
    //        ]
    //    );
    //
    //    // Test update
    //    $data = $this->call('PUT', $this->getPoint() . "allocates/" . $odr_id, [
    //        'allocated_qty' => '12',
    //    ]);
    //
    //    // Case 1: Response code is 200 ok
    //    $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    //
    //    // Case 2: Data is not null.
    //    $this->assertNotNull(json_decode($data->content(), true)['invt_smrs']);
    //}
    //
    //public function test_updateAllocate_NotExist_Order_Fail()
    //{
    //    // Create orderDtl before Show
    //    $orderDtlInfo = factory(OrderDtl::class)->create(
    //        [
    //            'odr_id' => 999999,
    //        ]
    //    );
    //    $itm_id = $orderDtlInfo->itm_id;
    //
    //    // Create invt_smr before Show
    //    $invt_smrInfo = factory(InventorySummary::class)->create(
    //        [
    //            'itm_id' => $itm_id,
    //        ]
    //    );
    //
    //    // Test update
    //    $data = $this->call('PUT', $this->getPoint() . "allocates/" . 999999, [
    //        'allocated_qty' => '12',
    //    ]);
    //
    //    // Case 1: Response code is 500
    //    $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_INTERNAL_SERVER_ERROR);
    //
    //    // Case 2: Show message: "The Order is not existed!"
    //    $this->assertEquals(
    //        Message::get("BM017", "Order"),
    //        json_decode($data->content(), true)['errors']['message']
    //    );
    //
    //}
    //
    //public function test_updateCsr_NotYetAssign_CSR_Fail()
    //{
    //    // Create orderHdr before Show
    //    $orderHdrInfo = factory(OrderHdr::class)->create(
    //        [
    //            'csr' => 0,
    //        ]
    //    );
    //    $odr_id = $orderHdrInfo->odr_id;
    //    $odr_num = $orderHdrInfo->odr_num;
    //
    //    // Create orderDtl before Show
    //    $orderDtlInfo = factory(OrderDtl::class)->create(
    //        [
    //            'odr_id' => $odr_id,
    //        ]
    //    );
    //    $itm_id = $orderDtlInfo->itm_id;
    //
    //    // Create invt_smr before Show
    //    $invt_smrInfo = factory(InventorySummary::class)->create(
    //        [
    //            'itm_id' => $itm_id,
    //        ]
    //    );
    //
    //    // Test update
    //    $data = $this->call('PUT', $this->getPoint() . "allocates/" . $odr_id, [
    //        'allocated_qty' => '12',
    //    ]);
    //
    //    // Case 1: Response code is 400
    //    $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);
    //
    //    // Case 2: Show message: "Order {0}  need to be assign CSR! Do you want to assign  now?"
    //    $this->assertEquals(
    //        Message::get("BM019", $odr_num),
    //        json_decode($data->content(), true)['errors']['message']
    //    );
    //
    //}
    //
    //public function test_updateCsr_only_NewOrders_Fail()
    //{
    //    // Create orderHdr before Show
    //    $orderHdrInfo = factory(OrderHdr::class)->create(
    //        [
    //            'odr_sts' => "AI",
    //        ]
    //    );
    //    $odr_id = $orderHdrInfo->odr_id;
    //    $odr_num = $orderHdrInfo->odr_num;
    //
    //    // Create orderDtl before Show
    //    $orderDtlInfo = factory(OrderDtl::class)->create(
    //        [
    //            'odr_id' => $odr_id,
    //        ]
    //    );
    //    $itm_id = $orderDtlInfo->itm_id;
    //
    //    // Create invt_smr before Show
    //    $invt_smrInfo = factory(InventorySummary::class)->create(
    //        [
    //            'itm_id' => $itm_id,
    //        ]
    //    );
    //
    //    // Test update
    //    $data = $this->call('PUT', $this->getPoint() . "allocates/" . $odr_id, [
    //        'allocated_qty' => '12',
    //    ]);
    //
    //    // Case 1: Response code is 400
    //    $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);
    //
    //    // Case 2: Show message: Only New Orders can be allocated!
    //    $this->assertEquals(
    //        Message::get("BM034"),
    //        json_decode($data->content(), true)['errors']['message']
    //    );
    //
    //}
    //
    //public function test_updateCsr_AvailQty_NotSufficient_Fail()
    //{
    //    // Create orderHdr before Show
    //    $orderHdrInfo = factory(OrderHdr::class)->create();
    //    $odr_id = $orderHdrInfo->odr_id;
    //    $odr_num = $orderHdrInfo->odr_num;
    //
    //    // Create orderDtl before Show
    //    $orderDtlInfo = factory(OrderDtl::class)->create(
    //        [
    //            'odr_id' => $odr_id,
    //        ]
    //    );
    //    $itm_id = $orderDtlInfo->itm_id;
    //
    //    // Create invt_smr before Show
    //    $invt_smrInfo = factory(InventorySummary::class)->create(
    //        [
    //            'itm_id' => $itm_id,
    //        ]
    //    );
    //
    //    // Test update
    //    $data = $this->call('PUT', $this->getPoint() . "allocates/" . $odr_id, [
    //        'allocated_qty' => 999999999,
    //    ]);
    //
    //    // Case 1: Response code is 400
    //    $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);
    //
    //    // Case 2: Show message: Customer not allow to partially allocate. The avail qty of some items not sufficient!
    //    $this->assertEquals(
    //        Message::get("BM033"),
    //        json_decode($data->content(), true)['errors']['message']
    //    );
    //
    //}

    public function test_Show_Ok()
    {
        // Test Show
        $data = $this->call('GET', $this->getEndPoint());

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);

    }

}
