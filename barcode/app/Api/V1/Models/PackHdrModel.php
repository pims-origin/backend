<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class PackHdrModel extends AbstractModel
{
    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
    }

    public function getPackHdrByOdrHdrIds($attributes, $with = [])
    {

        $query = $this->make($with);

        $query->where('whs_id', $attributes['whs_id'])
            ->where('picker', $attributes['picker']);

        $limit = (!empty($day=$attributes['day']) && is_numeric($day)) ? $day : 1;
        $query->where('created_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));
        return $query->get();

    }
}
