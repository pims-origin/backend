<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use App\Invoice;

use App\Api\V1\Models\InvoiceListModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\InvoiceHeaderModel;
use App\Api\V1\Models\ClientMasterModel;
use App\Api\V1\Models\InvoiceDetailsModel;
use App\Api\V1\Models\OrderModel;
use App\Api\V1\Models\CommonInvoiceModel;

use Seldat\Wms2\Models\InvoicingSummary;
use Seldat\Wms2\Models\InvoiceHeader;
use Seldat\Wms2\Models\InvoiceDetail;

use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\JWTUtil;

class ProcessInvoiceModel extends AbstractModel
{
    /**
    * @var
    */
    protected $invList;
    protected $container;
    protected $invHeader;
    protected $invHeaderModel;
    protected $clientModel;
    protected $invDetail;
    protected $order;

    /**
    *  Constructor
    */

    public function __construct()
    {
        $this->invList = new InvoiceListModel();
        $this->container = new ContainerModel();
        $this->invHeaderModel = new InvoiceHeaderModel();
        $this->invDtlModel = new InvoiceDetailsModel();
        $this->clientModel = new ClientMasterModel();

        $this->invoiceSum =  new InvoicingSummary();
        $this->invHeader = new InvoiceHeader();
        $this->invDetail = new InvoiceDetail();
        $this->order = new OrderModel();

        $this->grHdr = new GoodsReceiptModel();
    }

    /**
    * @params array
    * @return array
    */

    public function getProcessData($params)
    {
        $cancelNums = [];

        $containerName = $referenceCode = NULL;

        //check summary date
        $checkSummary =  $this->checkSummaryDate($params);

        if ($checkSummary->isEmpty()) {
            throw new \Exception(Message::get("IN001", "No Data available"));
        }

        //store the items for each key -  receiving , storage and Order Processing
        $storeItems = [];
        if (getDefault($params['items'])) {
            foreach ($params['items'] as $key => $values) {
                $storeItems[$key] = array_keys($values);
            }
        }

        $openCust = getDefault($params['openCust']);
        $passedVendorID = getDefault($params['custID']);

        $custID = $params['custID'] = $passedVendorID ? $passedVendorID : $openCust;
        $whsID = $params['whs_id'];

        $startDate = $params['startDate'] = getDefault($params['fromDate']);
        $endDate = $params['endDate'] =  getDefault($params['toDate']);

        $params['details'] = TRUE;

        //get open items, cc details and items IDS
        $invoInfo =  $this->invList->updateInvoTables($params);

        //get the storage range
        $storRange = $params['startDate'] . ' TO ' . $params['endDate'];

        //get the container name
        $recNums = getDefault($invoInfo['invItemsIDs']['Receiving']);

        if ($recNums && count($recNums) == 1) {
          $containerRes = $this->grHdr->getContainerData($recNums);

          $containerName = $containerRes['containerName'];
          $referenceCode  = $containerRes['referenceCode'];
        }

        //get the order number
        $orderIDs = getDefault($invoInfo['invItemsIDs']['Order Processing']);

        $clientOrdersNums = $this->order->getClientOrderNums($orderIDs);

        $cancelOrderIDs = $this->order->getCancelOrderIDs($orderIDs);

        if ($cancelOrderIDs) {

            $cancelNums =  $this->order->getOrderNumber($cancelOrderIDs);

            $cancelClientOrders = $this->order->getClientOrderNums($cancelOrderIDs);

            $clientOrdersNums = array_diff($clientOrdersNums, $cancelClientOrders);
        }

        $orderNumber = $clientOrdersNums && count($clientOrdersNums) === 1 ?
                        implode('', $clientOrdersNums) : [];


        //get next invoice number
        $invoiceNo = $this->invHeaderModel->getNextInvoiceNumber();

        //get userID
        $userID = JWTUtil::getPayloadValue('jti') ?: 1;

        //insert into invoice_hdr table
        $this->invHeaderModel->insertInvHeader([
            'invoiceNo' => $invoiceNo,
            'userID' => $userID,
            'cusID' => $custID,
            'whsID' => $whsID
        ]);

        //get the billTo address
        $billToValues = $this->clientModel->getBillTo($custID);


        //get the billTo address
        $shipToValues = $this->clientModel->getShipTo($custID);

        $summary = ProcessInvoiceModel::money([
            'netOrder' => $invoInfo['sums'][$custID],
            'discount' => 0,
            'freight' => 0,
            'salesTax' => 0,
            'balanceDue' => $invoInfo['sums'][$custID],
        ]);

        //get currency for the passed cus_id and whs_id
        $currencyCode = 'USD';

        //invoice date
        $inv_dt = CommonInvoiceModel::getDateTime('date');

        return [
            'cus_id' => $custID,
            'whs_id' => getDefault($params['whs_id']),
            'startDate' => $startDate,
            'endDate' => $endDate,
            'items' => $params['items'],
            'storeItems' => $storeItems,
            'invoInfo' => $invoInfo,
            'storRange' => $storRange,
            'containerName' => $containerName,
            'referenceCode' => $referenceCode,
            'cancelNums' => $cancelNums,
            'orderNumber' => $orderNumber,
            'inv_num' => $invoiceNo,
            'inv_dt' => $inv_dt,
            'billTo' => $billToValues,
            'shipTo' => $shipToValues,
            'summary' => $summary,
            'currencyCode' => $currencyCode
        ];
    }

    /**
    * @invoiceNo int
    * @return array
    */

    public function getInvoicedData($invoiceNo)
    {
        $containerName = $referenceCode = NULL;

        $invoInfo = $orderNumber = $cancelNums = $results =
                $items = $storeItems = [];

        //get the invoice header data
        $hdrResult = $this->invHeader
            ->select(
                'cus_id',
                'whs_id',
                'inv_id',
                'inv_dt',
                'inv_typ',
                'inv_sts',
                'bill_to_add1 AS bill_to_add_line_1',
                'bill_to_add2 AS bill_to_add_line_2',
                'bill_to_city',
                'bill_to_state',
                'bill_to_cnty AS bill_to_country',
                'bill_to_zip',
                'net_terms',
                'inv_amt'
            )
            ->with([
                'customer' => function ($query) {
                    $query->select(
                        'cus_id',
                        'cus_code',
                        'cus_name',
                        'cus_type'
                    );
                },
                'customer.customerContact' => function ($query) {
                    $query->select(
                        'cus_ctt_cus_id',
                        'cus_ctt_phone'
                    )
                    ->selectRaw('
                        CONCAT(cus_ctt_fname, " ", cus_ctt_lname) AS ctc_name
                    ')
                    ->where('cus_ctt_dft', 1);
                },
                'customer.customerAddress' => function ($query) {
                    $query->select(
                        'cus_add_cus_id',
                        'cus_add_city_name AS ship_to_city',
                        'cus_add_postal_code AS ship_to_zip',
                        'cus_add_state_id AS ship_to_state_id',
                        'cus_add_country_id AS ship_to_country_id',
                        'cus_add_line_1 AS ship_to_add_line_1',
                        'cus_add_line_2 AS ship_to_add_line_2',
                        'cus_add_state_id',
                        'cus_add_country_id'
                    )
                    ->where('cus_add_type', '=', 'ship');
                },
                'customer.customerAddress.systemState' => function ($query) {
                    $query->select(
                        'sys_state_id',
                        'sys_state_name as ship_to_state'
                    );
                },
                'customer.customerAddress.systemCountry' => function ($query) {
                    $query->select(
                        'sys_country_id',
                        'sys_country_name as ship_to_country'
                    );
                },
            ])
            ->where('inv_num', $invoiceNo)
            ->first()
            ->toArray();

        $ship = $hdrResult['customer']['customer_address'];
        $contact = $hdrResult['customer']['customer_contact'];
        $custID = $hdrResult['customer']['cus_id'];

        $billTo = [
            'cus_code' => $hdrResult['customer']['cus_code'],
            'cus_id' => $custID,
            'cus_type' => $hdrResult['customer']['cus_type'],
            'cus_name' => $hdrResult['customer']['cus_name'],
            'bill_to_add_line_1' => $hdrResult['bill_to_add_line_1'],
            'bill_to_add_line_2' => $hdrResult['bill_to_add_line_2'],
            'bill_to_city' => $hdrResult['bill_to_city'],
            'bill_to_state' => $hdrResult['bill_to_state'],
            'bill_to_country' => $hdrResult['bill_to_country'],
            'bill_to_zip' => $hdrResult['bill_to_zip'],
            'net_terms' => $hdrResult['net_terms'],
            'ctc_ph' => getDefault($contact[0]['cus_ctt_phone'], NULL),
            'ctc_nm' => getDefault($contact[0]['ctc_name'], NULL),
        ];

        $shipTo = [
            'cus_name' => $hdrResult['customer']['cus_name'],
            'ship_to_add_line_1' => $ship['ship_to_add_line_1'],
            'ship_to_add_line_2' => $ship['ship_to_add_line_2'],
            'ship_to_city' => $ship['ship_to_city'],
            'ship_to_state_id' => $ship['system_state']['sys_state_id'],
            'ship_to_state' => $ship['system_state']['ship_to_state'],
            'ship_to_country_id' => $ship['system_country']['sys_country_id'],
            'ship_to_country' => $ship['system_country']['ship_to_country'],
            'ship_to_zip' => $ship['ship_to_zip'],
        ];

        //get the invoice details data
        $detailSql = $this->invDtlModel->getDetailsData($invoiceNo);

        foreach ($detailSql as $row) {

            $uom = $row->systemUom;
            $charge = $row->chargeCode;
            $type = $row->chargeCode->chargeType;

            $results[] = [
                'inv_id' => $row->inv_id,
                'chg_cd' => $charge->chg_code,
                'chg_cd_name' => $charge->chg_code_name,
                'sys_uom_code' => $uom->sys_uom_code,
                'sys_uom_name' => $uom->sys_uom_name,
                'chg_cd_desc' => $row->chg_cd_desc,
                'chg_cd_type' => $type->chg_type_name,
                'chg_cd_cur' => $row->chg_cd_cur,
                'quantity' => number_format($row->chg_cd_qty, 3),
                'rate' => $row->chg_cd_price,
                'ccTotal' => $row->chg_cd_amt
            ];
        }

        $invoInfo['details'][$custID] = $results;

        $invoInfo['invItemsIDs'] = [];

        //get the container name
        $recNums = $this->invHeader
            ->join('inv_his_rcv', 'invoice_hdr.inv_id', '=', 'inv_his_rcv.inv_id')
            ->select('gr_hdr_id')
            ->where('invoice_hdr.inv_num', intVal($invoiceNo))
            ->lists('gr_hdr_id')
            ->toArray();

        if ($recNums) {

            $storeItems['Receiving'] =
                    $invoInfo['invItemsIDs']['Receiving']
                          = $recNums;

            if (count($recNums) == 1) {
                $containerRes = $this->grHdr->getContainerData($recNums);

                $containerName = $containerRes['containerName'];
                $referenceCode  = $containerRes['referenceCode'];
            }
        }

        //get the order number
        $orderIDs = $this->invHeader
            ->join('inv_his_ord_prc', 'invoice_hdr.inv_id', '=', 'inv_his_ord_prc.inv_id')
            ->select('ord_id')
            ->where('invoice_hdr.inv_num', intVal($invoiceNo))
            ->lists('ord_id')
            ->toArray();

        if ($orderIDs) {
            $storeItems['Order Processing'] =
                    $invoInfo['invItemsIDs']['Order Processing']
                                = $orderIDs;

            $clientOrdersNums = $this->order->getClientOrderNums($orderIDs);

            $cancelOrderIDs = $this->order->getCancelOrderIDs($orderIDs);

            if ($cancelOrderIDs) {

                $cancelNums =  $this->order->getOrderNumber($cancelOrderIDs);

                $cancelClientOrders = $this->order->getClientOrderNums($cancelOrderIDs);

                $clientOrdersNums = array_diff($clientOrdersNums, $cancelClientOrders);
            }

            $orderNumber = $clientOrdersNums && count($clientOrdersNums) === 1 ?
                        implode('', $clientOrdersNums) : [];
        }

        foreach ($storeItems as $key => $row) {
            foreach ($row as $num) {
               $items[$key][$num] = 'on';
            }
        }

        //get the storage range date
        $stor = $this->invHeader
            ->join('inv_his_month', 'invoice_hdr.inv_id', '=', 'inv_his_month.inv_id')
            ->select(
                DB::raw('MIN(inv_date) AS startDate'),
                DB::raw('MAX(inv_date) AS endDate')
            )
            ->where('invoice_hdr.inv_num', intVal($invoiceNo))
            ->groupBy('invoice_hdr.inv_id')
            ->first();

        $startDate = $stor ? $stor->startDate : NULL;
        $endDate = $stor ? $stor->endDate : NULL;
        $storRange = $stor ? $stor->startDate . ' TO ' . $stor->endDate : NULL;

        $summary = ProcessInvoiceModel::money([
            'netOrder' => $hdrResult['inv_amt'],
            'discount' => 0,
            'freight' => 0,
            'salesTax' => 0,
            'balanceDue' => $hdrResult['inv_amt'],
        ]);

        //get currency for the passed cus_id and whs_id
        $currencyCode = Invoice::CURRENCY;

        return [
            'cus_id' => $custID,
            'cus_name' => $hdrResult['customer']['cus_name'],
            'whs_id' => $hdrResult['whs_id'],
            'startDate' => $startDate,
            'endDate' => $endDate,
            'inv_num' => $invoiceNo,
            'inv_dt' => $hdrResult['inv_dt'],
            'inv_typ' => $hdrResult['inv_typ'],
            'inv_sts' => $hdrResult['inv_sts'],
            'billTo'  => $billTo,
            'shipTo' => $shipTo,
            'items' => $items,
            'storeItems' => $storeItems,
            'invoInfo' => $invoInfo,
            'containerName' => $containerName,
            'referenceCode' => $referenceCode,
            'orderNumber' => $orderNumber,
            'cancelNums' => $cancelNums,
            'storRange' => $storRange,
            'summary'  => $summary,
            'currencyCode' => $currencyCode
        ];
    }

    /**
    * @invoiceNo int
    * @return array
    */

    public function cancelInvoice($data)
    {
        $invoiceNo = $data['invoiceNo'];

        if (! $invoiceNo) {
            return FALSE;
        }

        //get userID
        $userID = JWTUtil::getPayloadValue('jti') ?: 1;


        $historyTables = [
            'inv_his_month',
            'inv_his_ord_prc',
            'inv_his_rcv'
        ];

        //get cancelling data from invoice header
        $cancellingHeaders = $this->invHeaderModel->getCancellingData($invoiceNo);

        if ($cancellingHeaders['inv_sts'] != 'i') {

            $status = NULL;

            switch ($cancellingHeaders['inv_sts']) {
                case 'o':

                    $status = '"Open"';

                    break;
                case 'c':

                    $status = '"Canceled"';

                    break;
                case 'p':

                    $status = '"Paid"';

                    break;
                default:
                    break;
            }

            // only an Invoice with invoiced status can be cancelled
            return (object) [
                'errors' => [$status . ' Invoice can not be cancled'],
            ];
        }

        //get cancelling data from invoice details
        $cancellingDetails = $this->invDtlModel->getCancellingData($invoiceNo);

        $invoiceID = $this->invHeaderModel->getByInvoiceNumber($invoiceNo);

        $nextInvoiceNumber = $this->invHeaderModel->getNextInvoiceNumber();

        DB::beginTransaction();

        //update history tables
        foreach ($historyTables as $table) {

            DB::table($table)
                    ->where('inv_id', $invoiceID)
                    ->update(['inv_sts' => 0]);
        }

        //update labor table
        DB::table('labor')
            ->where('inv_id', $invoiceID)
            ->update(
                      [
                        'inv_id' => 0,
                        'inv_num' => NULL,
                        'sts' => 'u'
                      ]
            );

        //update "invoiced" entries in invoice_summary table
        $this->invoiceSum
                    ->where('inv_id', $invoiceID)
                    ->where('sum_sts', 'invoiced')
                    ->update(
                        [
                            'inv_sts' => 0,
                            'updated_by' => $userID
                        ]
                     );

        //update "billable" entries in invoice_summary table
       $this->invoiceSum
                    ->where('inv_id', $invoiceID)
                    ->where('sum_sts', 'billable')
                    ->update(
                      [
                        'inv_id' => NULL,
                        'inv_num' => NULL,
                        'updated_by' => $userID
                      ]
                   );

        //update original invoice to cancel status in invoice_hdr table
        $params = [
            'inv_id' => $invoiceID,
            'inv_sts' => 'c',
            'sts' => 'u',
            'update_by' => $userID
        ];

        $this->invHeaderModel->update($params);


        //insert cancel entry into invoice_hdr table
        $headerInsert = [
            'inv_num' => $nextInvoiceNumber,
            'inv_dt' => date('Y-m-d'),
            'inv_org_id' => $invoiceID,
            'inv_org' => $invoiceNo,
            'inv_typ' => 'c',
            'whs_id' => $cancellingHeaders['whs_id'],
            'cus_id' => $cancellingHeaders['cus_id'],
            'inv_sts' => $cancellingHeaders['inv_sts'],
            'inv_cur' => $cancellingHeaders['inv_cur'],
            'inv_amt' => $cancellingHeaders['inv_amt'],
            'inv_tax' => $cancellingHeaders['inv_tax'],
            'cust_ref' => $cancellingHeaders['cust_ref'],
            'net_terms' => $cancellingHeaders['net_terms'],
            'bill_to_add1' => $cancellingHeaders['bill_to_add'],
            'bill_to_state' => $cancellingHeaders['bill_to_state'],
            'bill_to_city' => $cancellingHeaders['bill_to_city'],
            'bill_to_cnty' => $cancellingHeaders['bill_to_cnty'],
            'bill_to_zip' => $cancellingHeaders['bill_to_zip'],
            'bill_to_contact' => $cancellingHeaders['bill_to_contact'],
            'create_by' => $userID,
            'update_by' => $userID
        ];

        $this->invHeader->create($headerInsert);

        //insert cancel entries into invoice_dtls table
        foreach ($cancellingDetails as $values) {
            $detailInsert = [
                'inv_num' => $nextInvoiceNumber,
                'whs_id' => $values['whs_id'],
                'cus_id' => $values['cus_id'],
                'chg_code_id' => $values['chg_code_id'],
                'chg_cd_desc' => $values['chg_cd_desc'],
                'chg_cd_qty' => $values['chg_cd_qty'],
                'chg_uom_id' => $values['chg_uom_id'],
                'chg_cd_price' => $values['chg_cd_price'],
                'chg_cd_cur' => $values['chg_cd_cur'],
                'chg_cd_amt' => $values['chg_cd_amt'],
                'create_by' => $userID,
                'update_by' => $userID
            ];

            $this->invDetail->create($detailInsert);
        }

        DB::commit();

        return (object) [
            'errors' => NULL,
        ];
    }


    /**
    * @params array
    * @return array
    */

    public function checkSummaryDate($params)
    {
        $custID = $params['custID'] ? $params['custID']
                         : $params['openCust'];

        $endDate = $params['toDate'];

        $results = $this->invoiceSum
                        ->join('charge_type AS ch', 'ch.chg_type_id', '=', 'invoice_summary.chg_type_id')
                        ->select('dt')
                        ->where('dt', '>=', $endDate)
                        ->where('chg_type_name', Invoice::ST)
                        ->where('cus_id', $custID)
                        ->groupBy('cus_id')
                        ->get();

        return $results;
    }

    /**
     * @getVars array
     * @return mixed
    */

    static function money($amount, $noCurreny=FALSE)
    {
        $currency = $noCurreny ? NULL : Invoice::CURRENCY;
        if (! is_array($amount)) {
            return number_format($amount, 2).' '.$currency;
        }

        foreach ($amount as &$each) {
            $each = number_format($each, 2).' '.$currency;
        }

        return $amount;
    }
}

