<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:00
 */

namespace App\Api\V1\Validators;

class ItemDeleteMassValidator extends AbstractValidator
{
    /*
     * Define rules
     * 
     * @return array
     */
    protected function rules()
    {
        return [
            'item_id'   => 'required',
            'item_id.*' => 'required'
        ];
    }
}
