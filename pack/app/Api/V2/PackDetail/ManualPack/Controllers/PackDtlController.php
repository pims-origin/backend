<?php

namespace App\Api\V2\PackDetail\ManualPack\Controllers;

use App\Api\V2\PackDetail\ManualPack\Transformers\PackViewDtlTransformer;
use App\Api\V2\PackDetail\ManualPack\Validators\PackHdrValidator;
use App\Api\V2\PackDetail\Models\CustomerAddressModel;
use App\Api\V2\PackDetail\Models\CustomerConfigModel;
use App\Api\V2\PackDetail\Models\CustomerContactModel;
use App\Api\V2\PackDetail\Models\CustomerMetaModel;
use App\Api\V2\PackDetail\Models\CustomerModel;
use App\Api\V2\PackDetail\Models\EventTrackingModel;
use App\Api\V2\PackDetail\Models\ItemChildModel;
use App\Api\V2\PackDetail\Models\ItemModel;
use App\Api\V2\PackDetail\Models\OrderDtlModel;
use App\Api\V2\PackDetail\Models\OrderHdrMetaModel;
use App\Api\V2\PackDetail\Models\OrderHdrModel;
use App\Api\V2\PackDetail\Models\PackDtlModel;
use App\Api\V2\PackDetail\Models\PackHdrModel;
use App\Api\V2\PackDetail\Models\PackRefModel;
use App\Api\V2\PackDetail\Models\SystemStateModel;
use App\Api\V2\PackDetail\Models\SystemUomModel;
use App\Api\V2\PackDetail\Models\WavePickDtlModel;
use App\Jobs\BOLJob;
use App\Jobs\IntegrateFilesWithDMS;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use MultiShip\Address\Address;
use MultiShip\Package\Package;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WarehouseAddress;
use Seldat\Wms2\Models\WarehouseContact;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Profiler;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;

/**
 * Class PackDtlController
 *
 * @package App\Api\V1\Controllers
 */
class PackDtlController extends AbstractController
{
    protected $orderHdrModel;
    protected $orderDtlModel;
    protected $packDtlModel;
    protected $packHdrModel;
    protected $packRefModel;

    protected $eventTrackingModel;
    protected $wavePickDtlModel;
    protected $itemModel;
    protected $itemChildModel;
    protected $systemUomModel;
    protected $orderHdrMetaModel;
    protected $customerModel;
    protected $systemStateModel;

    protected $request;

    /**
     * PackDtlController constructor.
     *
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param PackDtlModel $packDtlModel
     * @param PackHdrModel $packHdrModel
     * @param PackRefModel $packRefModel
     * @param EventTrackingModel $eventTrackingModel
     * @param WavePickDtlModel $wavePickDtlModel
     * @param ItemModel $itemModel
     * @param ItemChildModel $itemChildModel
     * @param SystemUomModel $systemUomModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        PackDtlModel $packDtlModel,
        PackHdrModel $packHdrModel,
        PackRefModel $packRefModel,
        EventTrackingModel $eventTrackingModel,
        WavePickDtlModel $wavePickDtlModel,
        ItemModel $itemModel,
        ItemChildModel $itemChildModel,
        SystemUomModel $systemUomModel,
        CustomerModel $customerModel,
        SystemStateModel $systemStateModel
    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->packDtlModel = $packDtlModel;
        $this->packHdrModel = $packHdrModel;
        $this->packRefModel = $packRefModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->wavePickDtlModel = $wavePickDtlModel;
        $this->itemModel = $itemModel;
        $this->itemChildModel = $itemChildModel;
        $this->systemUomModel = $systemUomModel;
        $this->orderHdrMetaModel = new OrderHdrMetaModel();
        $this->customerModel = $customerModel;
        $this->systemStateModel = $systemStateModel;
    }

    /**
     * @param $orderHdrId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($orderHdrId, Request $request)
    {
        try {
            $input = $request->getQueryParams();
            $orderDtls = '';
            if (!empty($input['odr_dtl_id'])) {
                $input['odr_dtl_id'] = array_filter(explode(",", $input["odr_dtl_id"]));
                $orderDtls = $this->orderDtlModel->getOrderDtlById($input['odr_dtl_id'])->toArray();
            }
            $orderHdr = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderHdrId]);

            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Order"));
            }

            $itemOrder = [];
            if ($orderDtls) {
                foreach ($orderDtls as $orderDtl) {
                    $key = $orderDtl['item_id'] . '-' . $orderDtl['lot'];
                    $itemOrder[$key] = $orderDtl;
                }
            }

            if (!empty($input['pack_hdr_id'])) {
                $packHdr = $this->packHdrModel->getFirstBy("pack_hdr_id", $input['pack_hdr_id'],
                    ['systemUom', 'orderHdr']);
                $packDtl = $this->packDtlModel
                    ->findWhere(['pack_hdr_id' => $input['pack_hdr_id'], 'odr_hdr_id' => $orderHdrId])
                    ->toArray();
                if (!$packDtl) {
                    throw new \Exception(Message::get("BM017", "Carton"));
                }
            } else if (!empty($input['odr_dtl_id'])) {
                $packHdr = new PackHdr();
                $packHdr->sku_ttl = count($input['odr_dtl_id']);
                $packDtl = $orderDtls;
            } else {
                throw new \Exception(Message::get("BM017", "Order Detail input"));
            }

            $packHdr->order_type_key = $orderHdr->odr_type;
            $packHdr->origin_odr_type = object_get($orderHdr, 'shippingOrder.type', '');
            $packHdr->odr_hdr_num = $orderHdr->odr_num;
            $packHdr->odr_hdr_id = $orderHdr->odr_id;

            $itemParentId = object_get($packHdr, 'item_id', null);
            if ($itemParentId) {
                $itemObjMix = $this->itemModel->getFirstWhere(['item_id' => $itemParentId]);

                $packHdr->weight = object_get($itemObjMix, 'weight');
                $packHdr->net_weight = object_get($itemObjMix, 'net_weight');
                $packHdr->uom_id = object_get($itemObjMix, 'uom_id');
            }

            \DB::setFetchMode(\PDO::FETCH_ASSOC);
            $items = array_map(function ($pack) use ($itemOrder) {
                $requested = null;
                $piece_qty = array_get($pack, 'piece_qty', 0);
                $ship_track_id = null;
                $remain = 0;
                if (empty($pack['pack_hdr_id'])) {
                    $odr_hdr_id = $itemOrder[$pack['item_id'] . '-' . $pack['lot']]['odr_id'];
                    $ship_track_id = $itemOrder[$pack['item_id'] . '-' . $pack['lot']]['ship_track_id'];
                    $piece_qty = 0;

                    // // get all items and sum when using new sku
                    // $itemChildArr = DB::table('item_child')
                    //                 ->where('origin', $pack['item_id'])
                    //                 ->get();
                    // $itemChildIds = array_pluck($itemChildArr, 'child');
                    // $itemChildIds = array_prepend($itemChildIds, $pack['item_id']);
                    $packQty = (int)$this->packDtlModel->sumPieceQty($pack['lot'], $pack['item_id'], $odr_hdr_id);

                    $odr_dtl_id = array_get($pack, 'odr_dtl_id', -1);
                    $act_piece_qty = DB::table('odr_cartons')
                        ->select(DB::raw('SUM(piece_qty) AS piece_qty'))
                        ->where('odr_dtl_id', $odr_dtl_id)->first();
                    $remain = $act_piece_qty['piece_qty'] - $packQty;
                    $requested = $act_piece_qty['piece_qty'];
                }

                if ($remain < 0) {
                    $remain = 0;
                }
                $itemId = array_get($pack, 'item_id', null);
                $itemObj = $this->itemModel->getFirstWhere(['item_id' => $itemId]);
                $itemPack = object_get($itemObj, 'pack');
                $itemWeight = object_get($itemObj, 'weight');
                $itemUomId = object_get($itemObj, 'uom_id');

                return [
                    'odr_dtl_id'    => array_get($pack, 'odr_dtl_id', null),
                    'pack_dtl_id'   => array_get($pack, 'pack_dtl_id', null),
                    'itm_id'        => $itemId,
                    'sku'           => array_get($pack, 'sku', null),
                    'color'         => array_get($pack, 'color', null),
                    'size'          => array_get($pack, 'size', null),
                    'pack'          => $itemPack,
                    'lot'           => array_get($pack, 'lot', null),
                    'requested'     => $requested,
                    'piece_qty'     => $piece_qty,
                    'remain_qty'    => $remain,
                    'ship_track_id' => $ship_track_id,
                    'weight'        => $itemWeight,
                    'uom_id'        => $itemUomId,
                ];
            }, $packDtl);

            $packHdr['items'] = $items;

            return $this->response->item($packHdr, new PackViewDtlTransformer());

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderHdrId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function store($orderHdrId, Request $request)
    {
        // get data from HTTP
        $this->request = $request;
        $input = $request->getParsedBody();
        $input['odr_hdr_id'] = $orderHdrId;
        $input = SelArr::removeNullOrEmptyString($input);

        (new PackHdrValidator())->validate($input);

        // validation   
        $pieceTotal = array_sum(array_column($input['items'], 'piece_qty'));
        if (empty($pieceTotal)) {
            throw new \Exception(Message::get("VR001", "Pack Qty"));
        }

        $orderHdrObj = $this->orderHdrModel->getFirstBy("odr_id", $orderHdrId, ['details.item']);
        $orderHdr = $orderHdrObj->toArray();
        $itemData = array_pluck($orderHdr['details'], 'item', 'item.item_id');

        if ($orderHdrObj->odr_sts === 'PA') {
            return $this->response->errorBadRequest('This order is Pack');
        }


        //update sts PN
        $orderHdrObj->odr_sts = 'PN';
        $orderHdrObj->save();
        try {
            $numberOfPack = 1;
            $packHdrIds = [];
            if ($input['number_of_pack']) {
                $numberOfPack = $input['number_of_pack'];
            }

            // prepare data for insert batch
            $itemIds = array_column($input['items'], 'itm_id');
            if (!$this->orderDtlModel->checkDtlHasItem($orderHdrId, $itemIds)) {
                throw new \Exception(Message::get("BM017", "Order-Item"));
            }

            $packHdrCreate = [];
            $packRef = $this->packRefModel->getFirstBy("pack_ref_id", $input['pack_ref_id']);
            // dtt pack
            $Pack_Dt_Checksum = '';
            $itemChildChecksum = '';
            $pacKDtls = [];
            $pacKDtlCreate = [];
            $skuTotal = 0;

            DB::beginTransaction();
            $userId = $this->packHdrModel->getUserId();

            // check duplicate sku with new and old
            $singleDupSkuItem = null;
            // relationship between origin and child item
            $itemOriginChildIds = [];

            $itemDataInputs = collect($input['items'])->sortBy('itm_id')->toArray();

            // the same item but difference lot
            $itemidsCheckUnique = array_unique(array_pluck($itemDataInputs, 'itm_id'));
            $itemDataFirst = array_first($itemDataInputs);

            // SWIS-1386 - [New SKU] adding more is_new_sku optional feature
            if ($input['is_new_sku']) {
                // unique item data by item_id
                $itemDataInputUniques = $this->_executeTheSameItem($itemDataInputs);

                foreach ($itemDataInputUniques as $item) {
                    $realItemId = 0;
                    // Add new SKU
                    if ($item['pack'] != $item['piece_qty']) {
                        $itemChild = $this->_insertOrGetNewItemPack($item, $item['piece_qty'], $input);
                        $realItemId = object_get($itemChild, 'item_id');
                        $singleDupSkuItem = $itemChild;
                    } else {
                        $realItemId = $item['itm_id'];
                    }
                    // itemOriginal => item child
                    // $itemOriginChildIds[$item['itm_id']] = $realItemId;

                    $itemOriginChildIds[$item['itm_id']]['origin'] = $item['itm_id'];
                    $itemOriginChildIds[$item['itm_id']]['child'] = $realItemId;
                    $itemOriginChildIds[$item['itm_id']]['pack'] = $item['piece_qty'];

                    $itemChildChecksum .= $this->generateItemChecksum($item['sku'], $item['size'], $item['color'], $input['cus_id'], $item['piece_qty']);
                }
            }

            foreach ($itemDataInputs as $item) {
                if (empty($item['piece_qty'])) {
                    continue;
                }

                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $odrDtls = $this->orderDtlModel->getModel()->where([
                        'odr_id'  => $orderHdrId,
                        'item_id' => $item['itm_id']
                    ]
                )->get();

                if (!count($odrDtls)) {
                    throw new \Exception(Message::get("BM017", "Order-Item"));
                }

                if ($input['is_new_sku']) {
                    // set packed_qty to order detail
                    $packedQty = intval($item['piece_qty']);

                    foreach($odrDtls as $odrDtl) {
                        $packedRemainQty = $odrDtl->picked_qty - $odrDtl->packed_qty;

                        if ($packedRemainQty < $packedQty) {
                            $odrDtl->packed_qty += $packedRemainQty;
                            $packedQty -= $packedRemainQty;
                        } else {
                            $odrDtl->packed_qty += $packedQty;
                        }
                        $odrDtl->save();
                    }
                }

                $paramDtl = [
                    'pack_hdr_id' => 0,
                    'odr_hdr_id'  => $orderHdrId,
                    'whs_id'      => $orderHdr['whs_id'],
                    'cus_id'      => $orderHdr['cus_id'],
                    'item_id'     => $item['itm_id'],
                    'cus_upc'     => array_get($itemData, $item['itm_id'] . ".cus_upc", null),
                    'uom_id'      => array_get($itemData, $item['itm_id'] . ".uom_id", null),
                    'sku'         => $item['sku'],
                    'size'        => $item['size'],
                    'color'       => $item['color'],
                    'lot'         => $item['lot'],
                    'piece_qty'   => $item['piece_qty'],
                    'weight'      => $item['weight'],
                    'sts'         => 'i',
                    'created_at'  => time(),
                    'updated_at'  => time(),
                    'deleted'     => 0,
                    'deleted_at'  => 915148800,
                    'created_by'  => $userId,
                    'updated_by'  => $userId,
                ];

                $Pack_Dt_Checksum .= $item['sku'] . '_'
                    . $item['size'] . '_'
                    . $item['color'] . '_'
                    . $input['cus_id'] . '_'
                    . $item['piece_qty'] . "&";

                $pacKDtls[] = $paramDtl;

                $skuTotal++;
            }

            if (count($itemidsCheckUnique) == 1) {
                // re checksum item
                $itemChildChecksum = '' . '_' . $this->generateItemChecksum($itemDataFirst['sku'], $itemDataFirst['size'], $itemDataFirst['color'], $input['cus_id'], $pieceTotal);

            }

            $parentId = null;
            $firstPackSize = array_get($itemDataFirst, 'pack');
            $firstPieceQty = array_get($itemDataFirst, 'piece_qty');
            $firstSku = array_get($itemDataFirst, 'sku');
            // SWIS-1386 - [New SKU] adding more is_new_sku optional feature
            if ($input['is_new_sku']) {
                // SWIS 1155 Create Order Packing with New SKU
                if ((count($itemDataInputs) > 1) ||
                    (count($itemDataInputs) == 1 && $firstPackSize != $firstPieceQty)) {
                    $dataNewSkuItem = null;
                    if ((count($itemDataInputs) == 1 && $firstSku == $input['new_sku']) || (count($itemDataInputs) > 1 && $firstSku == $input['new_sku'] && count($itemidsCheckUnique) == 1)) {
                        // Single duplicate Sku Item
                        // them same item but difference lot
                        $dataNewSkuItem = $this->_updateOrGetSingleDupSkuItem($input, md5($itemChildChecksum),
                            $singleDupSkuItem, $pieceTotal);
                    } else {
                        // normal
                        $dataNewSkuItem = $this->_insertOrGetNewSkuItem($input, md5($itemChildChecksum));
                    }

                    $parentId = $dataNewSkuItem['item_id'];
                    // insert item child table
                    if (!$dataNewSkuItem['isExist']) {
                        $data = [];
                        // foreach ($itemOriginChildIds as $itemOriginalId => $itemChildId) {
                        //     $data[] = [
                        //         "parent" => $parentId,
                        //         "child"  => $itemChildId,
                        //         "type"   => "MIX",
                        //         "origin" => $itemOriginalId,
                        //         "pack"   => 1
                        //     ];
                        // }
                        foreach ($itemOriginChildIds as $itemChild) {
                            $data[] = [
                                "parent" => $parentId,
                                "child"  => array_get($itemChild, 'child'),
                                "type"   => "MIX",
                                "origin" => array_get($itemChild, 'origin'),
                                "pack"   => array_get($itemChild, 'pack')
                            ];
                        }
                        // insert to relationship to item child
                        $this->itemChildModel->getModel()->insert($data);
                    }
                }
            }
            else {
                $temID = array_get($input, 'item_id', null);
                if(is_null($temID)) {
                    if (count($itemidsCheckUnique) == 1) {
                        $temID = $itemDataFirst['itm_id'];
                    }
                }
                $dataNewSkuItem = $this->itemModel->getFirstWhere(['item_id' => $temID]);

            }

            for ($i = 0; $i < $numberOfPack; $i++) {
                $packHdrCreate[] = $this->_createPackHdr($orderHdr, $packRef, $skuTotal, md5($Pack_Dt_Checksum),
                    $pieceTotal, $parentId, $input, $dataNewSkuItem);
            }

            $isFirst = true;
            foreach (array_chunk($packHdrCreate, 1500) as $key => $phrCreate) {
                if ($key > 0) {
                    $isFirst = false;
                }
                $pacKDtlCreate = [];
                $events = [];

                if ($orderHdr['odr_type'] == 'RTL') {
                    foreach ($phrCreate as &$packHeader) {
                        // Remove generate Tracking number
                        // $packHeader['tracking_number'] = $this->createShipment($orderHdrId, $packHeader);
                        $packHeader['tracking_number'] = $this->createShipment($orderHdrId, $packHeader);

                    }
                }

                $pacKHdrInserted = $this->packHdrModel->insertBatch($phrCreate, $orderHdrId);
                foreach ($pacKHdrInserted as $pack) {
                    foreach ($pacKDtls as $pacKDtl) {
                        $pacKDtl['pack_hdr_id'] = object_get($pack, 'pack_hdr_id');
                        $pacKDtlCreate[] = $pacKDtl;
                        $packHdrIds[] = object_get($pack, 'pack_hdr_id');
                    }
                    $this->_createEvent($orderHdr, object_get($pack, 'pack_hdr_num'), $events);
                }
                $this->packDtlModel->createBatch($pacKDtlCreate);
                $this->packHdrModel->updateOrderStatus($orderHdrId, $events, $isFirst);
                $this->eventTrackingModel->createBatch($events);
            }

            $this->_updateOrderFlow($orderHdrId, $orderHdr['whs_id']);

            if(!$this->callJobBOL($orderHdrId, $orderHdr['whs_id'], $request))
            {
                throw new \Exception("Create BOL failed");
            }
            DB::commit();
            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'pack_hdr_id' => $packHdrIds
                ]
            ])->setStatusCode
            (Response::HTTP_CREATED);
        } catch (\PDOException $e) {
            DB::rollBack();

            //    return $this->response->errorBadRequest(
            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function callJobBOL($orderId, $whsId, $request)
    {
        $order = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderId]);
        $odr_sts = object_get($order, 'odr_sts', '');

        if (in_array($odr_sts, ['PA', 'PTD', 'RS'])) {
            return (new BOLJob($orderId, $whsId, $request))->handle();
        }
        return true;
    }

    /**
     * @param $request
     *
     * @return mixed
     */
    public function isExistNewSku(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $itemDataInputs = collect($input['items'])->sortBy('itm_id')->toArray();
        $pieceTotal = array_sum(array_column($input['items'], 'piece_qty'));

        // them same item but difference lot
        $itemidsCheckUnique = array_unique(array_pluck($itemDataInputs, 'itm_id'));
        $itemDataFirst = array_first($itemDataInputs);

        $itemChildChecksum = "";
        foreach ($itemDataInputs as $item) {
            if (empty($item['piece_qty'])) {
                continue;
            }

            $itemMaster= $this->itemModel->getFirstWhere([
                "item_id" => $item['itm_id']
            ]);
            $itemChildChecksum .= $this->generateItemChecksum($itemMaster['sku'], $itemMaster['size'], $itemMaster['color'],
                $input['cus_id'], $item['piece_qty']);
        }

        if (count($itemidsCheckUnique) == 1) {
            // re checksum item
            $itemChildChecksum = '' . '_' . $this->generateItemChecksum($itemDataFirst['sku'], $itemDataFirst['size'], $itemDataFirst['color'], $input['cus_id'], $pieceTotal);

        }

        \DB::setFetchMode(\PDO::FETCH_ASSOC);

        $data = ['data' => null];
        // check exist new sku by checksum
        $checkItem = $this->itemModel->getFirstWhere(['children_checksum' => md5($itemChildChecksum)]);
        if ($checkItem) {
            $data = ['data' => $checkItem];
        }

        return $this->response->noContent()->setContent($data)->setStatusCode
        (Response::HTTP_CREATED);
    }

    /**
     * @param $orderHdr
     * @param $packRef
     * @param $skuTotal
     * @param $pack_dtl_checksum
     * @param $piece_ttl
     * @param $itemParentId
     * @param $input
     * @param $dataNewSkuItem
     *
     * @return array
     */
    private function _createPackHdr(
        $orderHdr,
        $packRef,
        $skuTotal,
        $pack_dtl_checksum,
        $piece_ttl,
        $itemParentId,
        $input,
        $dataNewSkuItem
    ) {
        $paramPackHdr = [
            'odr_hdr_id'       => $orderHdr['odr_id'],
            'whs_id'           => $orderHdr['whs_id'],
            'cus_id'           => $orderHdr['cus_id'],
            'carrier_name'     => $orderHdr['carrier'],
            'sku_ttl'          => $skuTotal,
            'piece_ttl'        => $piece_ttl,
            'pack_sts'         => "AC",
            'sts'              => "i",
            'ship_to_name'     => $orderHdr['ship_to_name'],
            'pack_ref_id'      => object_get($packRef, 'pack_ref_id', null),
            'width'            => object_get($packRef, 'width', null),
            'height'           => object_get($packRef, 'height', null),
            'length'           => object_get($packRef, 'length', null),
            'created_at'       => time(),
            'updated_at'       => time(),
            'created_by'       => $this->packHdrModel->getUserId(),
            'updated_by'       => $this->packHdrModel->getUserId(),
            'deleted'          => 0,
            'deleted_at'       => 915148800,
            'pack_dt_checksum' => $pack_dtl_checksum,
            'weight'           => array_get($input, 'net_weight', 0),
            'pack_type'        => object_get($packRef, 'pack_type', 0),
            //'item_id'          => $itemParentId,
            'sku'              => array_get($dataNewSkuItem, 'sku', null),

            //'item_id' => $itemParentId ? $itemParentId : array_get($pacKDtls, '0.item_id', ''),
            //'sku'     => array_get($input, 'new_sku', null) ? array_get($input, 'new_sku', null) : array_get($pacKDtls, '0.sku', ''),
            'item_id' => array_get($dataNewSkuItem, 'item_id', null),
            'size'    => array_get($dataNewSkuItem, 'size', null),
            'lot'     => array_get($dataNewSkuItem, 'lot', 'NA'),
            'color'   => array_get($dataNewSkuItem, 'color', null),
            //'cus_upc' => array_get($orderHdr, "details.0.item.cus_upc", ''),
            'cus_upc' => array_get($dataNewSkuItem, "cus_upc", null),


        ];

        return $paramPackHdr;
    }

    private function _createEvent($orderHdr, $packNum, &$events)
    {
        // Create Event Tracking
        $events[] = [
            'whs_id'     => $orderHdr['whs_id'],
            'cus_id'     => $orderHdr['cus_id'],
            'owner'      => $orderHdr['odr_num'],
            'evt_code'   => Status::getByKey("Event", "PACKING"),
            'trans_num'  => $packNum,
            'created_at' => time(),
            'created_by' => $this->packDtlModel->getUserId(),
            'info'       => sprintf(Status::getByKey("Event-Info", "PACKING"), $packNum),
        ];
        $events[] = [
            'whs_id'     => $orderHdr['whs_id'],
            'cus_id'     => $orderHdr['cus_id'],
            'owner'      => $orderHdr['odr_num'],
            'evt_code'   => Status::getByKey("Event", "PACKING-COMPLETE"),
            'created_at' => time(),
            'created_by' => $this->packDtlModel->getUserId(),
            'trans_num'  => $packNum,
            'info'       => sprintf(Status::getByKey("Event-Info", "PACKING-COMPLETE"), $packNum),
        ];
    }

    private function _insertOrGetNewItemPack($oldItem, $pieceQty, $input)
    {
        $dataGet = [
            'sku'    => $oldItem['sku'],
            'size'   => $oldItem['size'],
            'color'  => $oldItem['color'],
            'pack'   => $pieceQty,
            'cus_id' => $input['cus_id'],
        ];
        $checkItem = $this->itemModel->getFirstWhere($dataGet);

        // not exist
        if (!$checkItem) {
            \DB::setFetchMode(\PDO::FETCH_ASSOC);
            $item = $this->itemModel->getFirstWhere([
                'item_id' => $oldItem['itm_id'],
            ])->toArray();
            unset($item['item_id']);

            $item['pack'] = $pieceQty;
            $item['is_new_sku'] = 1;
            $item['item_code'] = $this->itemModel->generateItemCode($input['cus_id']);

            $checkItem = $this->itemModel->getModel()->create($item);
        }

        return $checkItem;
    }

    private function _insertOrGetNewSkuItem($input, $itemChildChecksum)
    {
        // check exist mix sku by checksum
        $checkItem = $this->itemModel->getFirstWhere(['children_checksum' => $itemChildChecksum]);
        $isExist = true;
        if (!$checkItem) {
            $isExist = false;
            $uomId = array_get($input, 'uom_id', -1);
            $uomObj = $this->_getDefaultUomObj($uomId);
            $checkItem = $this->itemModel->getFirstWhere([
                'sku'    => $input['new_sku'],
                'size'   => 'NA',
                'color'  => 'NA',
                'cus_id' => $input['cus_id'],
                'pack'   => count($input['items']),
            ]);


            if ($checkItem) {
                $msg = 'The tuble {SKU, SIZE, COLOR, CUSTOMER, PACK} is duplicated, please choose another SKU!';
                throw new \Exception($msg);
            }

            $pack_size = 0;
            foreach($input['items'] as $item) {
                $pack_size += $item['piece_qty'];
            }

            $data = [
                "item_code"         => $this->itemModel->generateItemCode($input['cus_id']),
                "sku"               => $input['new_sku'],
                "size"              => "NA",
                "color"             => "NA",
                "uom_id"            => object_get($uomObj, 'sys_uom_id'),
                "uom_name"          => object_get($uomObj, 'sys_uom_name'),
                "uom_code"          => object_get($uomObj, 'sys_uom_code'),
//                "pack"              => count($input['items']),
                "pack"              => $pack_size,
                "cus_id"            => $input['cus_id'],
                "weight"            => array_get($input, 'weight', null),
                "net_weight"        => array_get($input, 'net_weight', null),
                "status"            => 'AC',
                "children_checksum" => $itemChildChecksum,
            ];

            $checkItem = $this->itemModel->getModel()->create($data);
        }

        return [
            'isExist' => $isExist,
            'item_id' => object_get($checkItem, 'item_id'),
            'sku' => object_get($checkItem, 'sku'),
            "size"              => "NA",
            "color"             => "NA",
            "cus_upc"           => "",
            "lot"               => "NA",
        ];
    }

    private function _updateOrGetSingleDupSkuItem($input, $itemChildChecksum, $singleDupSkuItem, $packNewTotal)
    {
        // check exist mix sku by checksum
        $checkItem = $this->itemModel->getFirstWhere(['children_checksum' => $itemChildChecksum]);
        $isExist = true;
        if (!$checkItem) {
            \DB::setFetchMode(\PDO::FETCH_ASSOC);
            $isExist = false;
            $uomId = array_get($input, 'uom_id', -1);
            $uomObj = $this->_getDefaultUomObj($uomId);

            if ($singleDupSkuItem->pack != $packNewTotal) {
                $checkItem = $this->itemModel->getFirstWhere([
                    'sku'    => $input['new_sku'],
                    'size'   => 'NA',
                    'color'  => 'NA',
                    'cus_id' => $input['cus_id'],
                    'pack'   => $packNewTotal,
                ]);
                if ($checkItem) {
                    $msg = 'The tuble {SKU, SIZE, COLOR, CUSTOMER, PACK} is duplicated, please choose another SKU!';
                    throw new \Exception($msg);
                }
            }

            $singleDupSkuItem->uom_id = object_get($uomObj, 'sys_uom_id');
            $singleDupSkuItem->uom_name = object_get($uomObj, 'sys_uom_name');
            $singleDupSkuItem->uom_code = object_get($uomObj, 'sys_uom_code');
            $singleDupSkuItem->pack = $packNewTotal;
            $singleDupSkuItem->weight = array_get($input, 'weight', null);
            $singleDupSkuItem->net_weight = array_get($input, 'net_weight', null);
            if ($singleDupSkuItem->children_checksum) {
                throw new \Exception('Existed checksum, please choose an other SKU');
            }
            $singleDupSkuItem->children_checksum = $itemChildChecksum;

            $singleDupSkuItem->save();

            return [
                'isExist' => $isExist,
                'item_id' => object_get($singleDupSkuItem, 'item_id'),
                'sku' => object_get($singleDupSkuItem, 'sku'),
                'size' => object_get($singleDupSkuItem, 'size'),
                'color' => object_get($singleDupSkuItem, 'color'),
                'cus_upc' => object_get($singleDupSkuItem, 'cus_upc'),
                'lot' => 'NA',
            ];
        }

        return [
            'isExist' => $isExist,
            'item_id' => object_get($checkItem, 'item_id'),
            'sku' => object_get($checkItem, 'sku'),
            'size' => object_get($checkItem, 'size'),
            'color' => object_get($checkItem, 'color'),
            'cus_upc' => object_get($checkItem, 'cus_upc'),
            'lot' => 'NA',
        ];
    }

    /**
     * param $uomId
     */
    private function _getDefaultUomObj($uomId)
    {
        if ($uomId < 1 || !isset($uomId) || empty($uomId)) {
            // CARTON
            return $this->systemUomModel->getFirstWhere(["sys_uom_code" => 'CT']);
        } else {
            return $this->systemUomModel->getFirstWhere(["sys_uom_id" => $uomId]);
        }
    }

    /**
     *
     */
    private function _executeTheSameItem($itemDatas)
    {
        $itemIds = [];
        $itemDataTmps = [];
        foreach ($itemDatas as $keyItem => $itemData) {
            if (in_array($itemData['itm_id'], $itemIds)) {
                $itemDataTmps[$itemData['itm_id']]['lot'] = 'NA';
                $itemDataTmps[$itemData['itm_id']]['piece_qty'] += $itemData['piece_qty'];
            } else {
                $itemDataTmps[$itemData['itm_id']] = $itemData;
                $itemIds[] = $itemData['itm_id'];
            }
        }

        return array_values($itemDataTmps);
    }

    private function generateItemChecksum($sku, $size, $color, $cus_id, $piece_qty) {
        $result = $sku . '_'
            . $size . '_'
            . $color . '_'
            . $cus_id . '_'
            . $piece_qty . "&";
        return strtoupper($result);
    }

    private function _updateOrderFlow($odrId, $whsId)
    {
        $odrObj = $this->orderHdrModel->getFirstWhere(['odr_id' => $odrId]);
        $odrSts = object_get($odrObj, 'odr_sts');

        if ($odrSts == 'PA') {
            $orderFlow = $this->orderHdrMetaModel->getOrderFlow($odrId);
            $isAutoPAM = $this->orderHdrMetaModel->getFlow($orderFlow, 'PAM');
            // $isAutoBOL = $this->orderHdrMetaModel->getFlow($orderFlow, 'BOL');
            $isAutoARS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ARS');
            // $isAutoASS = $this->orderHdrMetaModel->getFlow($orderFlow, 'ASS');

            $flagStatus = $odrObj->odr_sts;
            if (array_get($isAutoPAM, 'usage', -1) == 1) {
                $flagStatus = 'PTD';
            }

            if (array_get($isAutoPAM, 'usage', -1) == 1 &&
                    array_get($isAutoARS, 'usage', -1) == 1) {
                $flagStatus = 'RS';
            }

            if ($odrObj->odr_sts != $flagStatus) {
                $odrObj->odr_sts = $flagStatus;
                $odrObj->save();
            }
        }
    }

    /**
     * @param $orderHdrId
     * @param $phrCreate
     * @return mixed
     * @throws \MultiShip\Exceptions\MultiShipException
     */
    public function createShipment($orderHdrId, $phrCreate)
    {
        $orderInfo = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderHdrId]);
        return $this->configShipment($orderInfo, $phrCreate, strtoupper($orderInfo->carrier));
    }

    /**
     * @param $orderInfo
     * @param $phrCreate
     * @param string $carrier
     * @return mixed
     * @throws \MultiShip\Exceptions\MultiShipException
     */
    public function configShipment($orderInfo, $phrCreate, $carrier = 'FEDEX')
    {
        $cusId = object_get($orderInfo, 'cus_id', '');
        $upsData = '';
        $fdxData = '';
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $carrierService = DB::table('cus_meta')
            ->where('cus_id', $cusId)
            ->whereIn('qualifier', ['FDX', 'UPS'])->get();

        if (count($carrierService)> 0) {
            foreach ($carrierService as $key => $item) {
                if($item['qualifier'] == 'UPS') {
                    $upsData = json_decode($item['value'], true);
                } else if($item['qualifier'] == 'FDX') {
                    $fdxData = json_decode($item['value'], true);
                }
            }
        } else {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $whs_meta_data = DB::table('whs_meta')
                ->select([
                    'whs_id',
                    'whs_qualifier',
                    'whs_meta_value'
                ])
                ->where('whs_id', Data::getCurrentWhsId())
                ->whereIn('whs_qualifier', ['FDX', 'UPS'])
                ->get();
            if (count($whs_meta_data)> 0) {
                foreach ($whs_meta_data as $key => $item) {
                    if($item['whs_qualifier'] == 'UPS') {
                        $upsData = json_decode($item['whs_meta_value'], true);
                    } else if($item['whs_qualifier'] == 'FDX') {
                        $fdxData = json_decode($item['whs_meta_value'], true);
                    }
                }
            }
        }
//        if($carrier == "FEDEX") {
//            if (count($fdxData)> 0) {
//                foreach ($fdxData as $value) {
//                    if ($value == "") {
//                        throw new \Exception('Please config carrier service!');
//                    }
//                }
//            }
//        }
//        if($carrier == "UPS") {
//            if (count($upsData)> 0) {
//                foreach ($upsData as $value) {
//                    if ($value == "") {
//                        throw new \Exception('Please config carrier service!');
//                    }
//                }
//            }
//        }
        
        $serviceCode = $carrier != 'FEDEX' ? '03' : 'FEDEX_GROUND'; //UPS/FEDEX: 03 / FEDEX_GROUND

        $options = [
            'FEDEX' => [
                'accessKey'     => array_get($fdxData, 'FEDEX_KEY', ''),
                'accountNumber' => array_get($fdxData, 'FEDEX_ACCOUNT_NUMBER', ''),
                'meterNumber'   => array_get($fdxData, 'FEDEX_METER_NUMBER', ''),
                'password'      => array_get($fdxData, 'FEDEX_PASSWORD', ''),
                'debug'         => true
            ],
            'UPS' => [
                'accessKey'     => array_get($upsData, 'UPS_LICENSE_NUM', ''),
                'userId'        => array_get($upsData, 'UPS_USERNAME', ''),
                'password'      => array_get($upsData, 'UPS_PASSWORD', ''),
                'accountNumber' => array_get($upsData, 'UPS_ACCOUNT_NUMBER', ''),
                'debug'         => true
            ]
        ];

        $fdxVisible = array_get($fdxData, 'visible', false);
        if ($fdxVisible) {
            if($carrier == "FEDEX") {
                foreach ($options['FEDEX'] as $key => $value) {
                    if ($value == "") {
                        throw new \Exception($key . 'is empty. Please config carrier(FEDREX)service!');
                    }
                }
            }
        }

        $upsVisible = array_get($upsData, 'visible', false);
        if ($upsVisible) {
            if($carrier == "UPS") {
                foreach ($options['UPS'] as $key => $value) {
                    if ($value == "") {
                        throw new \Exception($key . 'is empty. Please config carrier(UPS) service!');
                    }
                }
            }
        }

        if ($fdxVisible || $upsVisible) {
            try {
                $shipService = new \MultiShip\MultiShip([$carrier => $options[$carrier]]);

                //$customer = $this->customerModel->findWhere(['cus_id' => $cusId])->first();
                //$objCusAddress = $this->customerAddressModel->getShippingAddressByCustomer($cusId);
                //$objCusContact = $this->customerContactModel->getFirstByCustomerId($cusId);
                $whsAddr = WarehouseAddress::where('whs_add_whs_id', Data::getCurrentWhsId())->first();
                $whsContact = WarehouseContact::where('whs_con_whs_id', Data::getCurrentWhsId())->first();
                $state = $this->systemStateModel->getFirstBy('sys_state_id', $whsAddr->whs_add_state_id)->sys_state_code;

                //----------------ADDRESS-----------------
                $fromAddress = new Address();
                $fromAddress->setName($whsContact->whs_con_fname . ' ' . $whsContact->whs_con_lname);
                $fromAddress->setPhoneNumber($whsContact->whs_con_phone);
                $fromAddress->setLine1($whsAddr->whs_add_line_1);
                $fromAddress->setCity($whsAddr->whs_add_city_name);
                $fromAddress->setRegion($state);
                $fromAddress->setPostalCode($whsAddr->whs_add_postal_code);
                $fromAddress->setCountry('US');
                $fromAddress->setResidentialAddress(false);

                $toAddress = new Address();
                $toAddress->setName($orderInfo->ship_to_name);
                $toAddress->setPhoneNumber($orderInfo->ship_to_phone);
                $toAddress->setLine1($orderInfo->ship_to_add_1);
                $toAddress->setCity($orderInfo->ship_to_city);
                $toAddress->setRegion($orderInfo->ship_to_state);
                $toAddress->setPostalCode($orderInfo->ship_to_zip);
                $toAddress->setCountry('US');
                $toAddress->setResidentialAddress(false);

                //--------------Package-------------------
                $package1 = new Package();
                $package1->setHeight(array_get($phrCreate, 'height', 0));
                $package1->setWidth(array_get($phrCreate, 'width', 0));
                $package1->setLength(array_get($phrCreate, 'length', 0));
                $package1->setDimensionUnitOfMeasure('in');
                $package1->setWeight(array_get($phrCreate, 'weight', 0));
                $package1->setWeightUnitOfMeasure('lbs');

                //Set data
                $shipService->setFromAddress($fromAddress);
                $shipService->setToAddress($toAddress);
                $shipService->addPackage($package1);

                //Set service code
                $shipService->setServiceCode($serviceCode);
                //Process ship
                $return = $shipService->processShipment();

                //Save label image file
                $trackingNum = $return->getShipmentPackage()->getTrackingNumber();

                // $odrNum = object_get($orderInfo, 'odr_num', time());

                // Remove generate Tracking number
                $fileName = $carrier . '_LABELS_SELDAT_' . $trackingNum;
                //$fileName = $carrier . '_LABELS_SELDAT_';

                $path = storage_path(strtolower($carrier) . DIRECTORY_SEPARATOR . "pdf" . DIRECTORY_SEPARATOR . "{$fileName}.pdf");

                // Remove generate Tracking number
                // $return->getShipmentPackage()->getLabel()->saveImage($path, $trackingNum);
                $return->getShipmentPackage()->getLabel()->saveImage($path);

                dispatch(new IntegrateFilesWithDMS($this->request, $path, $orderInfo));

                return $trackingNum;
            }
            catch(\Exception $ex){
                /*Profiler::log(json_encode([
                    'fromAddress'=>$fromAddress??null,
                    'toAddress'=>$toAddress??null,
                    'package'=>$package1??null,
                    'shipService'=>$shipService??null
                ]));*/
                throw $ex;
            }
        }

        return null;
    }

    /**
     * @param $orderHdrId
     *
     * @return bool
     */
    public function createFedexShipment($orderHdrId, $phrCreate)
    {
        $pdf_sample_file = storage_path('fedex/shipexpresslabel.pdf');
        $config = [
            'name'    => 'fedex',
            'service' => 'ShippingLabel'
        ];
        $objFedex = app('Carrier', $config);
        $objFedex->setRequest($this->configShipment($orderHdrId, $phrCreate));
        $response = $objFedex->createShipment();
        $status = object_get($response, 'HighestSeverity', '');
        if (empty($status)) {
            throw new Exception('Communications Script Error...please try again later.');
        }

        if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR') {
            try {
                $file_tmp_name = 'FEDEX_LABELS_SELDAT_' . date('YmdHis');
                $pdf_shipment = storage_path("fedex/pdf/{$file_tmp_name}.pdf");
                copy($pdf_sample_file, $pdf_shipment);
                $fp = fopen($pdf_shipment, 'wb');
                fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
                fclose($fp);

                // Update tracking number
                $ship_track_id = $response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
                $file_name = 'FEDEX_LABELS_SELDAT_' . $ship_track_id;
                rename($pdf_shipment, storage_path("fedex/pdf/{$file_name}.pdf"));

                return $ship_track_id;
            } catch (Exception $exception) {
                throw new Exception($exception->getMessage());
            }
        }

        return false;
    }

    /**
     *
     */
    public function createUpsShipment()
    {
        $config = [
            'name'    => 'ups',
            'service' => 'ShippingLabel'
        ];
        $objUps = app('Carrier', $config);
        $objUps->createShipment();
    }

    public function downloadShippingLabel($packHdrId, Request $request)
    {
        if (!is_numeric($packHdrId)) {
            throw new \Exception(Message::get("VR029", "Pack Header Ids"));
        }
        try {
            $packHdr = $this->packHdrModel->byId($packHdrId);
            if (empty($packHdr)) {
                return $this->response->noContent();
            }
            $tracking_num = $packHdr->tracking_number;
            $packHdr->is_print = 1;
            $packHdr->save();

            $carrier = $packHdr->carrier_name;

            $fileName = $carrier . '_LABELS_SELDAT_' . $tracking_num;
            $filePath = storage_path(strtolower($carrier) . DIRECTORY_SEPARATOR ."pdf" . DIRECTORY_SEPARATOR . "{$fileName}.pdf");

            if (file_exists($filePath)) {
                return response()->download($filePath);
            } else {
                return $this->response->errorBadRequest('File not exist.');
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
        }
    }

    public function getTrackNumByPackHdr(Request $request, $packHdrId) {
        try {
            $packHdr = $this->packHdrModel->getFirstWhere([
                "pack_hdr_id" => $packHdrId
            ])->toArray();
            if(empty($packHdr)) {
                throw new Exception("Pack Header is not exit");
            }
            $track_num = $this->createShipment($packHdr['odr_hdr_id'], $packHdr);

            return [
                "data" => [
                    "track_num" =>$track_num
                ]
            ];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            throw $this->response->errorBadRequest($e->getMessage());
        }
    }
}
