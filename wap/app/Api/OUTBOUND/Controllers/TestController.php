<?php

namespace App\Api\OUTBOUND\Controllers;

use App\Api\OUTBOUND\Models\OrderCartonModel;
use App\Api\OUTBOUND\Models\OrderDtlModel;
use App\Api\OUTBOUND\Models\OrderHdrModel;
use App\Api\OUTBOUND\Models\WavePickHdrModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Utils\JWTUtil;
use Psr\Http\Message\ServerRequestInterface as Request;

class TestController extends AbstractController
{
    /**
     * TestController constructor.
     */
    public function __construct(){
        if (env('APP_ENV') != 'testing') {
            return "Access denied!";
        }
    }

    public function testUpdatePickOdrDtl ($odrId) {
        $odrDtl = new OrderDtlModel();
        return $odrDtl->updateOrderDetailPicked($odrId);
    }

    public function testUpdateOdrHdrToStaging ($odrId) {
        $odrDtl = new OrderHdrModel();
        return $odrDtl->updateOrderStaging($odrId);
    }

    public function testUpdatePickOdrHdr ($odrId) {
        $odrHdr = new OrderHdrModel();
        return $odrHdr->updateOrderPicked($odrId);
    }

    public function getVirtualCtn($whsId)
    {
        return (new CartonModel())->loadViewLayout($whsId);
        return (new VirtualCartonModel())->loadViewLayoutNew($whsId);

    }

    public function updateInventory(Request $request, $whsId, $cusId)
    {
        $input = $request->getParsedBody();
        $cnt = new CartonModel();
        //return $cnt->sumDamagedQtyByAsnDtlId($input['asn_dtl_id']);
        //return $cnt->sumAvailQtyByAsnDtlId($input['asn_dtl_id']);
        return (New InventorySummaryModel())->updateQtyByGRDtl($whsId, $cusId, $input);
    }

    public function countPalletInVtlCtn($asnDtlId) {
        return (new VirtualCartonModel())->countPltByASNDtlId($asnDtlId);
    }

    public function updateWPHdr($wvHdrId) {

        return (new WavePickHdrModel())->updatePicked($wvHdrId);
    }

    public function testInsertOrderCarton($wvHdrId) {

        $wv =  (new WavePickHdrModel())->byId($wvHdrId)->toArray();
        $ctns = (new CartonModel())->findWhere(['ctn_sts' => 'PD'])->toArray();

         (new OrderCartonModel())->insertOrderCartons($ctns, $wv);
        dd($ctns);
        return $wv;
    }

    public function testLPN($odrId) {

        $lpn = 'LPN-1702-00002-0001';
        $num = explode('-', $lpn);
        array_shift($num);
        $seq = array_pop($num);
        array_unshift($num, 'ORD');
        //return $num;
        $odrNum = implode('-', $num);
       //return $odrNum;
        //ORD-1702-00002
        $wv =  (new OrderHdrModel())->isLPNinOrder($odrId, $lpn);

if($wv){
    return 'ngu';
}
        return "dd";
    }
}
