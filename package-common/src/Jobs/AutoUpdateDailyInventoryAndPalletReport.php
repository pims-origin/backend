<?php

namespace Seldat\Wms2\Jobs;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Services\DailyInventoryService;

class AutoUpdateDailyInventoryAndPalletReport extends Job
{
    public $whs_id;

    public $cus_id;

    public $inv_date;

    protected $dailyInventoryService;

    public function __construct($whs_id, $cus_id, $inv_date)
    {
        $this->whs_id   = $whs_id;
        $this->cus_id   = $cus_id;
        $this->inv_date = $inv_date;
        $this->dailyInventoryService = new DailyInventoryService();
    }

    public function handle()
    {
        $whsId   = $this->whs_id;
        $cusId   = $this->cus_id;
        $InvDate = date('Y-m-d', $this->inv_date);

        try {
            $this->dailyInventoryService->correctCustomerDataByDay($cusId, ['select_day' => $InvDate]);

            DB::statement("CALL update_daily_plt_rpt({$whsId}, {$cusId}, {$InvDate})");
            Log::info(date('Y-m-d') . ' Update Daily Inventory and Pallet success with warehouse ID: ' . $whsId . ' Customer ID: ' . $cusId . 'Inventory date: ' . $InvDate);
        } catch (\PDOException $e) {
            Log::error($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
