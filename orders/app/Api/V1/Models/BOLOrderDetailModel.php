<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\BOLOrderDetail;

/**
 * Class BOLOrderDetailModel
 *
 * @package App\Api\V1\Models
 */
class BOLOrderDetailModel extends AbstractModel
{
    /**
     * @var BOLOrderDetail
     */
    protected $model;

    /**
     * BOLOrderDetailModel constructor.
     *
     * @param BOLOrderDetail|null $model
     */
    public function __construct(BOLOrderDetail $model = null)
    {
        $this->model = ($model) ?: new BOLOrderDetail();
    }

    /**
     * @return mixed
     */
    public function loadExcept()
    {
        $query = $this->model->where('ship_id', '>', 0)
            ->orWhereNotNull('ship_id')
            ->get();

        return $query;
    }
}
