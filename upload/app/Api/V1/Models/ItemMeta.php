<?php
/**
 * Created by PhpStorm.
 * User: PhucTran
 * Date: 3/30/2017
 * Time: 3:00 PM
 */

namespace App\Api\V1\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemMeta extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'item_meta';
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    protected function setValueAttribute($value){
        if(is_array($value)){
            $this->attributes['value'] = \GuzzleHttp\json_encode($value);
        }
    }

    protected function getValueAttribute($value){
        return \GuzzleHttp\json_decode($value, true);
    }

    protected function getCreatedAtAttribute($value){
        return date('m-d-Y H:i:s', $value);
    }

    public function createdByUser(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedByUser(){
        return $this->belongsTo(User::class, 'updated_by');
    }
}