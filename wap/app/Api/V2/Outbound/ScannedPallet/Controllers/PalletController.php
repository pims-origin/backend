<?php

namespace App\Api\V2\Outbound\ScannedPallet\Controllers;

use App\Api\V2\Outbound\Models\EventTrackingModel;
use App\Api\V2\Outbound\Models\CartonModel;
use App\Api\V2\Outbound\Models\PalletModel;
use App\Api\V2\Outbound\Models\ItemModel;
use App\Api\V2\Outbound\Models\CustomerModel;
use App\Api\V2\Outbound\Models\InventorySummaryModel;

use App\libraries\RFIDValidate;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\Helper;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;
use violuke\Barcodes\BarcodeValidator;

class PalletController extends AbstractController
{

    protected $cartonModel;
    protected $palletModel;
    protected $itemModel;
    protected $customerModel;
    protected $eventTrackingModel;
    protected $inventorySummaryModel;

    public function __construct()
    {
        $this->cartonModel           = new CartonModel();
        $this->palletModel           = new PalletModel();
        $this->itemModel             = new ItemModel();
        $this->customerModel         = new CustomerModel();
        $this->eventTrackingModel    = new EventTrackingModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function customerList($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/whs/{$whsId}/pallet/customer-list";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'PCL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Pallet customer list'
        ]);

        /*
         * end logs
         */

        try {
            $data = $this->customerModel->getCusListByWhsId($whsId, $input);

            $msg = [];
            $msg['status']  = true;
            $msg['iat']     = time();
            $msg['message'][] = [
                'status_code' => 1,
                'msg'         => sprintf("Successfully!")
            ];
            $msg['data']['customers'] = $data;
            return new Response($msg, 200, [], null);

        } catch (\Exception $e) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'][] = [
                'status_code' => -1,
                'msg' => $e->getMessage()
            ];

            return new Response($msg, 200, [], null);
        }

    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function scannedPallet($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/whs/{$whsId}/pallet/scanned-pallet-migrate";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'SPO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Scanned Pallet Outbound'
        ]);

        /*
         * end logs
         */

        $ctnRfids = array_get($input, 'ctn_rfids', null);
        $pltRfid  = array_get($input, 'plt_rfid', null);

        $lot = array_get($input, 'lot');
        if ($lot === "" || $lot === null) {
            $input['lot'] = "NA";
        }

        $error = $this->_validateParams($input, $whsId);
        if ($error) {
            return $error;
        }

        try {
            // start transaction
            DB::beginTransaction();

            $this->_createCartonAndAssignToPallet($input, $ctnRfids, $pltRfid, $whsId);

            DB::commit();

            $msg = [];
            $msg['status']    = true;
            $msg['iat']       = time();
            $msg['message'][] = [
                'status_code' => 1,
                'msg'         => sprintf("Successfully!")
            ];
            $msg['data'] = [];
            return $msg;

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'][] = [
                'status_code' => -1,
                'msg' => $e->getMessage()
            ];

            return new Response($msg, 200, [], null);
        }

    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'message' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _createCartonAndAssignToPallet($input, $ctnRfids, $pltRfid, $whsId)
    {
        $pltNum = $this->palletModel->generatePalletNumFromRfid($pltRfid);

        $data = [
            "cus_id"           => $input['cus_id'],
            "whs_id"           => $whsId,
            "plt_num"          => $pltNum,
            "rfid"             => $pltRfid,
            "ctn_ttl"          => 0,
            "storage_duration" => 0,
            "plt_sts"          => "AC",
            "zero_date"        => time(),
        ];
        $pallet = $this->palletModel->create($data);

        // create carton
        $itemObj  = $this->itemModel->getFirstBy('item_id', $input['item_id'], ['systemUom']);

        $cus_puc = array_get($input, 'cus_upc');

        // update or get UPC
        if(!empty($cus_puc)) {
            if ($cus_puc != object_get($itemObj, 'cus_upc')) {
                $itemObj->cus_upc = $cus_puc;
                $itemObj->save();
            }
        } else {
            $cus_puc = $itemObj->cus_upc;
            $input['cus_upc'] = $itemObj->cus_upc;
        }

        $pieceTtl = array_get($input, 'pack_size');
        foreach ($ctnRfids as $keyCtn => $ctnRfid) {
            $checkCarton = $this->cartonModel->getFirstWhere(['rfid' => $ctnRfid]);
            if ($checkCarton) {
                continue;
            }

            $ctnNum = $this->cartonModel->generateCtnNum();

            $length      = array_get($itemObj, "length", 0);
            $width       = array_get($itemObj, "width", 0);
            $height      = array_get($itemObj, "height", 0);
            $volume      = Helper::calculateVolume($length, $width, $height);
            $cube        = Helper::calculateCube($length, $width, $height);
            $grDate      = time();
            $lot         = array_get($input, "lot", "NA");

            $dataCarton = [
                'plt_id'        => $pallet->plt_id,
                'item_id'       => $input['item_id'],
                'cus_id'        => $input['cus_id'],
                'whs_id'        => $whsId,
                'ctn_num'       => $ctnNum,
                'rfid'          => $ctnRfid,
                'ctn_sts'       => 'AC',
                'ctn_uom_id'    => object_get($itemObj, 'systemUom.sys_uom_id', 0),
                'uom_name'      => object_get($itemObj, 'systemUom.sys_uom_name', ''),
                'uom_code'      => object_get($itemObj, 'systemUom.sys_uom_code', ''),
                'piece_remain'  => $pieceTtl,
                'piece_ttl'     => $pieceTtl,
                'sku'           => object_get($itemObj, "sku", null),
                'size'          => object_get($itemObj, "size", null),
                'color'         => object_get($itemObj, "color", null),
                'lot'           => $lot,
                'upc'           => $cus_puc,
                'length'        => $length,
                'width'         => $width,
                'height'        => $height,
                'weight'        => object_get($itemObj, "weight", null),
                'ctn_pack_size' => $pieceTtl,
                'is_damaged'    => 0,
                'cube'          => $cube,
                'volume'        => $volume,
                'gr_dt'         => $grDate,
                'cat_code'      => object_get($itemObj, "cat_code", null),
                'cat_name'      => object_get($itemObj, "cat_name", null),
                'des'           => object_get($itemObj, "description", null),
                'spc_hdl_code'  => object_get($itemObj, "spc_hdl_code", null),
                'spc_hdl_name'  => object_get($itemObj, "spc_hdl_name", null),
                'ucc128'        => object_get($itemObj, "ucc128", null),
                'expired_dt'    => object_get($itemObj, "expired_dt", null),
            ];

            $this->cartonModel->refreshModel();
            $this->cartonModel->create($dataCarton);
        }

        $ctnTtl = count($ctnRfids);
        // update pallet
        $pallet->init_ctn_ttl = $ctnTtl;
        $pallet->ctn_ttl      = $ctnTtl;

        $pallet->save();

        // update inventory summary
        $this->_updateInventorySummary($input, $whsId, $ctnTtl*$pieceTtl, $itemObj);
    }

    private function _updateInventorySummary($input, $whsId, $avail, $itemObj)
    {
        $ivt = $this->inventorySummaryModel->getFirstWhere(
            [
                'item_id' => $input['item_id'],
                'whs_id'  => $whsId,
                'lot'     => $input['lot'],
            ]
        );
        if (!$ivt) {
            $data = [
                'item_id'       => $input['item_id'],
                'cus_id'        => $input['cus_id'],
                'whs_id'        => $whsId,
                'color'         => object_get($itemObj, "color", null),
                'size'          => object_get($itemObj, "size", null),
                'lot'           => $input['lot'],
                'ttl'           => $avail,
                'picked_qty'    => 0,
                'allocated_qty' => 0,
                'dmg_qty'       => 0,
                'avail'         => $avail,
                'sku'           => object_get($itemObj, "sku", null),
                'upc'           => $input['cus_upc'],
                'created_at'    => time(),
                'updated_at'    => time(),
                'back_qty'      => 0,
                'ecom_qty'      => 0,
            ];

            $this->inventorySummaryModel->refreshModel();
            $this->inventorySummaryModel->create($data);
        } else {
            $ivt->avail += $avail;
            $ivt->ttl   += $avail;
            $ivt->upc    = $input['cus_upc'];
            $ivt->save();
        }
    }

    private function _validateParams($input, $whsId)
    {
        $names = [
            'cus_upc'   => 'UPC',
            'cus_id'    => 'Customer id',
            'item_id'   => 'Item id',
            'pack_size' => 'Pack size',
            'plt_rfid'  => 'Pallet RFID',
            'ctn_rfids' => 'Carton RFID array',
        ];

        // Check Required
        $requireds = [
            'cus_id'    => array_get($input, 'cus_id'),
            'item_id'   => array_get($input, 'item_id'),
            'pack_size' => array_get($input, 'pack_size'),
            'plt_rfid'  => array_get($input, 'plt_rfid'),
            'ctn_rfids' => array_get($input, 'ctn_rfids'),
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        // check exist customer
        $checkCustomer = $this->customerModel->getFirstWhere(['cus_id' => $input['cus_id']]);
        if (!$checkCustomer) {
            $msg = sprintf("Customer id %s doesn't exist.", $input['cus_id']);
            return $this->_responseErrorMessage($msg);
        }

        // validate customer belong warehouse
        $checkCusWhs = \DB::table('customer_warehouse')
                        ->where('whs_id', $whsId)
                        ->where('cus_id', $input['cus_id'])
                        ->where('deleted', 0)
                        ->first();
        if (!$checkCusWhs) {
            $msg = "The customer doesn't belong to warehouse.";
            return $this->_responseErrorMessage($msg);
        }

        // check exist item and belong to customer
        $checkItem = $this->itemModel->getFirstWhere([
                        'item_id' => $input['item_id'],
                        'cus_id' => $input['cus_id'],
                    ]);
        if (!$checkItem) {
            $msg = sprintf("Item id %s doesn't exist.", $input['item_id']);
            $checkItem = $this->itemModel->getFirstWhere(['item_id' => $input['item_id']]);
            if ($checkItem) {
                $msg = "Item id doesn't belong to customer.";
            }
            return $this->_responseErrorMessage($msg);
        }

        // Check Pack size integer and > 0 and <=9999
        if (!is_numeric($input['pack_size']) || $input['pack_size'] < 1 || $input['pack_size'] > 9999) {
            $msg = "Pack size must be between [1, 9999]";
            return $this->_responseErrorMessage($msg);
        }

        // Check Lot is string type and  <=50
        if (!is_string($input['lot']) || strlen($input['lot']) > 50) {
            $msg = "Lot must be not greater than 50 characters";
            return $this->_responseErrorMessage($msg);
        }

        // Check ctn_rfids array
        if (!is_array($input['ctn_rfids']) || count($input['ctn_rfids']) == 0) {
            $msg = "{$names['ctn_rfids']} is not empty.";
            return $this->_responseErrorMessage($msg);
        }

        $pltRfid  = $input['plt_rfid'];
        $ctnRfids = $input['ctn_rfids'];
        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            return $this->_responseErrorMessage($pltRFIDValid->error);
        }

        $pallet = $this->palletModel->getFirstWhere([
            'whs_id' => $whsId,
            'rfid'   => $pltRfid,
        ]);

        if ($pallet) {
            $msg = "The Pallet RFID existed.";
            return $this->_responseErrorMessage($msg);
        }
        // Check duplicate cartons rfid
       $uniqueRFIDs =  array_unique($ctnRfids);
       if(count($uniqueRFIDs) != count($ctnRfids)){
           $duplicateRFID = array_unique(array_diff_assoc($ctnRfids, $uniqueRFIDs));

           $msg = 'Duplicate Carton: ' . implode(', ', $duplicateRFID);
           return $this->_responseErrorMessage($msg);
       }

        //validate carton RFID
        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($ctnRfids as $item) {
            $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
            if (! $ctnRFIDValid->validate()) {
                $ctnRfidInValid[] = $item;
                if('' == $errorMsg) {
                    $errorMsg = $ctnRFIDValid->error;
                }
            }
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            return $this->_responseErrorMessage($errorMsg);
        }

        $checkCartonNotExist = $this->cartonModel->checkCartonNotExist($ctnRfids);
        if (count($checkCartonNotExist) == count($ctnRfids)) {
            $msg = "All Carton RFID existed.";
            return $this->_responseErrorMessage($msg);
        }
        $cus_upc = array_get($input, 'cus_upc');
        if(!empty($cus_upc)) {
            // Class instantation
            $bcValidator = new BarcodeValidator($input['cus_upc']);

            // Check barcode is in valid format
            if (!$bcValidator->isValid()) {
                $msg = "{$names['cus_upc']} is invalid!";
                return $this->_responseErrorMessage($msg);
            }

            if ($cus_upc != object_get($checkItem, 'cus_upc')) {
                // validate tuple {sku, size, color, upc}
                $cusUpc  = $cus_upc;
                $sku     = object_get($checkItem, 'sku');
                $size    = object_get($checkItem, 'size');
                $color   = object_get($checkItem, 'color');
                $sqlItem = "item.cus_upc = {$cusUpc} AND (item.sku != '{$sku}' OR item.size != '{$size}' OR item.color != '{$color}')";
                $checkUpcExist = $this->itemModel->getModel()
                            ->join('customer as ct', 'ct.cus_id', '=', 'item.cus_id')
                            ->join('customer_warehouse as cw', 'cw.cus_id', '=', 'ct.cus_id')
                            ->where('ct.deleted', 0)
                            ->where('cw.deleted', 0)
                            ->where('cw.whs_id', $whsId)
                            ->whereRaw(\DB::raw($sqlItem))
                            ->first();

                if ($checkUpcExist) {
                    $msg = sprintf("UPC existed in {SKU: %s, SIZE: %s, COLOR: %s}.",
                        object_get($checkUpcExist, 'sku'),
                        object_get($checkUpcExist, 'size'),
                        object_get($checkUpcExist, 'color')
                    );
                    return $this->_responseErrorMessage($msg);
                }
            }
        }

    }
}