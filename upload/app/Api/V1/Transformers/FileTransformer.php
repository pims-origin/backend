<?php
/**
 * Created by PhpStorm.
 * User: PhucTran
 * Date: 3/16/2017
 * Time: 1:14 PM
 */

namespace App\Api\V1\Transformers;


use App\Api\V1\Models\File;
use League\Fractal\TransformerAbstract;

class FileTransformer extends TransformerAbstract
{
    /**
     * @param $sample
     * @return array
     */
    public function transform(File $file)
    {
        $arr = array_merge($file->toArray(), [
            'key' => base64_encode($file->file_path),
            'doc_date' => date('Y-m-d H:i:s', $file->doc_date),
            'created_at' => date('Y-m-d H:i:s', $file->created_at),
        ]);

        unset($arr['file_path']);
        return $arr;
    }
}