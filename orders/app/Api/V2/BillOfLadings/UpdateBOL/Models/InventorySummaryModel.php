<?php

namespace App\Api\V2\BillOfLadings\UpdateBOL\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class InventorySummaryModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        // search sku
        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        // search sku
        if (isset($attributes['color'])) {
            $query->where('color', 'like', "%" . SelStr::escapeLike($attributes['color']) . "%");
        }

        // search sku
        if (isset($attributes['size'])) {
            $query->where('size', 'like', "%" . SelStr::escapeLike($attributes['size']) . "%");
        }

        if (isset($attributes['cus_id'])) {
            $query->where('cus_id', (int)$attributes['cus_id']);
        }
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * @param $item_ids
     *
     * @return mixed
     */
    public function getByItemIds($item_ids)
    {
        $userInfo = $this->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $item_ids = is_array($item_ids) ? $item_ids : [$item_ids];

        $rows = $this->model
            ->whereIn('item_id', $item_ids)
            ->where('whs_id', '=', $currentWH)
            ->get();

        return $rows;
    }

    public function getAvailableQty($itemId)
    {
        $query = $this->make([]);

        $query->where('item_id', $itemId);
        $query->where('avail', '>', 0);
        $this->model->filterData($query);

        return $query->first();
    }

    public function getInvSumbyOdrDlt($itemId, $lot, $whs_id, $get = 'avail')
    {
        $query = $this->make([]);

        $query = $query->where('item_id', $itemId);
        if (strtoupper($lot) != 'ANY') {
            $query->where('lot', $lot);
        }

        $query->where('whs_id', $whs_id);

        //$this->model->filterData($query);

        return $query->sum($get);
    }

    public function getUserInfo()
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();

        return $userInfo;
    }


    public function updateAllocatedOrderInventory($whs_id, $odrIds = [])
    {
        $field = 'allocated_qty';

        if (!is_array($odrIds)) {
            $odrIds [] = (int)$odrIds;
        }

        $strOdrIds = implode(',', $odrIds);

        $sqlStr = sprintf(" `%s` -
            COALESCE(
                ( SELECT SUM(odr_dtl.alloc_qty)
                FROM odr_dtl
                WHERE invt_smr.item_id = odr_dtl.item_id
                    AND odr_dtl.lot = invt_smr.lot
                    AND odr_dtl.odr_id IN (%s)
                    AND odr_dtl.deleted = 0
                )
            ,0)
            ", $field, $strOdrIds);

        $availSql = sprintf(" `avail` +
            COALESCE(
                ( SELECT SUM(odr_dtl.alloc_qty)
                FROM odr_dtl
                WHERE invt_smr.item_id = odr_dtl.item_id
                    AND odr_dtl.lot = invt_smr.lot
                    AND odr_dtl.odr_id IN (%s)
                    AND odr_dtl.deleted = 0
                )
            ,0)
            ", $strOdrIds);

        $res = $this->updateWhere([
            'allocated_qty' => DB::raw($sqlStr),
            'avail'         => DB::raw($availSql)
        ], [
            'whs_id' => $whs_id
        ]);

        return $res;
    }

}
