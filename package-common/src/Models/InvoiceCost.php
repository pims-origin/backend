<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:49
 */

namespace Seldat\Wms2\Models;

/**
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property mixed whs_id
 * @property mixed chg_code_id
 * @property mixed chg_cd_price
 * @property mixed created_by
 * @property mixed updated_by
 */

class InvoiceCost extends BaseSoftModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice_cost';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'inv_cost_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'chg_code_id',
        'chg_cd_cur',
        'chg_cd_price',
        'chg_cd_des',
    ];

    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function chargeCode()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ChargeCode', 'chg_code_id', 'chg_code_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    /*
    ****************************************************************************
    */

    public function chargeType()
    {
        return $this->hasOne(__NAMESPACE__ . '\ChargeType', 'chg_type_id', 'chg_type_id');
    }

    /*
    ****************************************************************************
    */

}