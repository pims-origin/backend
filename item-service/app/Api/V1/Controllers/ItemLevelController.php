<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\ItemLevelService;
use Illuminate\Http\Request as Request;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class ItemLevelController extends AbstractController
{

    /**
     * @var ItemCatService
     */
    protected $service;

    /**
     * ItemCatController constructor.
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new ItemLevelService($request);
    }

    /**
     * @param IRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getList($queryStr);
        $response = $this->convertResponse($result);
        return $response;
    }

}
