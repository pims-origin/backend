<?php
/**
 * Created by Duy Nguyen.
 * User: Duy Nguyen
 * Date: 02/12/2016
 * Time: 16:10
 */

namespace Seldat\Wms2\Models;

class UserNotification extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_ntf';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_ntf_id';

    /**
     * @var array
     */
    protected $fillable = [
        'ntf_id',
        'sts',
        'start_date'
    ];

    /**
     * @return cycleHdr
     */
    public function notification()
    {
        return $this->belongsTo(Notification::class, 'ntf_id', 'ntf_id');
    }

    /**
     * @return create by
     */
    public function createBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return update by
     */
    public function updateBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'user_id');
    }
}