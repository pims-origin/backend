<?php

namespace Seldat\Wms2\Models;

class CustomerContact extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_contact';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cus_ctt_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_ctt_id',
        'cus_ctt_dft',
        'cus_ctt_fname',
        'cus_ctt_lname',
        'cus_ctt_email',
        'cus_ctt_phone',
        'cus_ctt_mobile',
        'cus_ctt_ext',
        'cus_ctt_position',
        'cus_ctt_cus_id',
        'cus_ctt_department',
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_ctt_cus_id', 'cus_id');
    }
}
