<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 1/8/2019
 * Time: 9:20 AM
 */

namespace App\Api\V1\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Request as IRequest;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;
use Dingo\Api\Http\Response;
use Seldat\Wms2\Utils\Status;

class DeliveryController extends AbstractController
{
    public function getAssignedOrders(Request $request)
    {
        $input = $request->getQueryParams();
        $limit = (!empty($input['limit'])) ? $input['limit'] : 20;
        $page = (!empty($input['page'])) ? $input['page'] : 1;

        $odrIds = DB::table('odr_hdr_meta')
                    ->where('qualifier', 'DL0')
                    ->pluck('odr_id');

        $result = DB::table('odr_hdr')
                    ->whereIn('odr_id', $odrIds)
                    ->select(['odr_id', 'odr_num', 'cus_odr_num', 'odr_sts'])
                    ->orderBy('cus_odr_num')
                    ->paginate($limit)
                    ->toArray();

        // Update status Shipped to Delivering
        $updateSts = DB::table('odr_hdr')
                    ->whereIn('odr_id', $odrIds)
                    ->update(['odr_sts' => 'DG']);

        return response()->json($result);
    }

    public function showDeliveryDetail($orderId)
    {
        $result = DB::table('odr_hdr')
            // ->leftJoin('shipment', 'odr_hdr.ship_id', '=', 'shipment.ship_id')
            ->where('odr_hdr.odr_id', $orderId)
            ->select([
                'odr_hdr.odr_num',
                'odr_hdr.cus_odr_num',
                'odr_hdr.ship_to_name',
                'odr_hdr.ship_to_add_1',
                'odr_hdr.ship_to_city',
                'odr_hdr.ship_to_state',
                'odr_hdr.ship_to_country',
            ])
            ->first();

        $res['data'] = [
            'odr_num'           => $result['odr_num'],
            'cus_odr_num'       => $result['cus_odr_num'],
            'ship_to_name'      => $result['ship_to_name'],
            'ship_to_city'      => $result['ship_to_city'],
            'ship_to_state'     => $result['ship_to_state'],
            'ship_to_country'   => $result['ship_to_country'],
        ];

        return response()->json($res);
    }

    public function submitDelivery(IRequest $request, $orderId)
    {
        try {
            $input = $request->all();

            $this->validateDeliveryDetails($input);

            // Upload orderImage
            $picName = $request->file('ul_picture')->getClientOriginalName();
            $request->file('ul_picture')->move('uploads/', $picName);

            $value = [
                'ship_info' =>  $input['ship_info'],
                'cus_sign'  =>  $input['cus_sign'],
                'dlvr_note' =>  !empty($input['dlvr_note']) ? $input['dlvr_note'] : null,
                'ul_picture'  =>  $picName
            ];

            $params = [
                "value"     =>  json_encode($value),
                "updated_at"=>  time(),
            ];

            $result = DB::table('odr_hdr_meta')
                ->where('odr_id', $orderId)
                ->where('qualifier', 'DL0')
                ->update($params);

            // Update status Delivering to Delivered
            $odrStsUpdate = DB::table('odr_hdr')
                ->where('odr_id', $orderId)
                ->update(['odr_sts' => 'DD']);
                // ->update(['odr_sts', Status::getByValue("Delivered", "Order-status")]);

            return [
                'data' => 'Submit delivery successfully!'
            ];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    protected function validateDeliveryDetails($attribute) {
        if(empty($attribute['ship_info'])) {
            return $this->response->errorBadRequest("This field ship_info is required!");
        }
        if(empty($attribute['cus_sign'])) {
            return $this->response->errorBadRequest("This field cus_sign is required!");
        }
        if(empty($attribute['ul_picture'])) {
            return $this->response->errorBadRequest("This file ul_picture is required!");
        }
    }
}