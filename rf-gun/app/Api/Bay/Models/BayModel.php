<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\Bay\Models;

use Seldat\Wms2\Models\Bay;
use Seldat\Wms2\Utils\SelArr;

class BayModel extends AbstractModel
{
    /**
     * BayModel constructor.
     *
     * @param Bay|null $model
     */
    public function __construct(Bay $model = null)
    {
        $this->model = ($model) ?: new Bay();
    }

    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    public function getBayByIds($bayId)
    {
        $rows = $this->model
            ->where('bay_id', $bayId)
            ->first();

        return $rows;
    }
}
