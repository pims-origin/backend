<?php

namespace App\Api\V1\Models;



use Seldat\Wms2\Models\Warehouse;

class WarehouseModel extends AbstractModel
{
    protected $model;

    public function __construct(Warehouse $model = null)
    {
        $this->model = ($model) ?: new Warehouse();
    }


}
