<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
	{
		if(env('QUERY_LOG', false)) {
			DB::listen(function($query) {
				File::append(
					storage_path('/logs/query.log'),
					$query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
				);
			});
		}
	}
}
