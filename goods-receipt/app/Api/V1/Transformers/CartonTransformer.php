<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 25-Jul-16
 * Time: 3:05
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;

class CartonTransformer extends TransformerAbstract
{
    public function transform(Carton $carton)
    {
        return [
            'ctn_id'        => $carton->ctn_id,
            'plt_id'        => $carton->plt_id,
            'asn_dtl_id'    => $carton->asn_dtl_id,
            'gr_dtl_id'     => $carton->gr_dtl_id,
            'item_id'       => $carton->item_id,
            'whs_id'        => $carton->whs_id,
            'cus_id'        => $carton->cus_id,
            'loc_id'        => $carton->loc_id,
            'dmg_id'        => $carton->dmg_id,
            'ctn_num'       => $carton->ctn_num,
            'ctn_sts'       => $carton->ctn_sts,
            'ctn_uom_id'    => $carton->ctn_uom_id,
            'ctn_pack_size' => $carton->ctn_pack_size,
            'loc_code'      => $carton->loc_code,
            'loc_name'      => $carton->loc_name,
            'is_damaged'    => $carton->is_damaged,
            'is_split'      => $carton->is_split,
            'is_unsplit'    => $carton->is_unsplit,
            'created_at'    => $carton->created_at,
            'updated_at'    => $carton->updated_at,
            'deleted_at'    => $carton->deleted_at,
        ];
    }
}
