<?php

namespace App\Api\Inbound\Services;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class GoodsReceiptService extends BaseService
{
    const LEFT_MENU_ID = 1;
    const FAVOURITE_MENU_ID = 1;

    /**
     * AuthorizationService constructor.
     *
     * @param $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_GOODSRECEIPT');
        parent::__construct($request, $api);
    }

    public function createGr($data)
    {
        $options = [
            'form_params' => $data,
        ];

        $uri = 'goods-receipts';
        try {
            $response = $this->client->post($uri, $options);

            return $response;
        } catch (RequestException $e) {
            var_dump($e->getResponse());exit;
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

}