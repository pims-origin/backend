<?php

namespace App\Api\V2\WavePickDetails\WvDtlList\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class WavePickHdrModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickHdr($data){
        return $this->model->create($data);
    }

    /**
     * @return mixed
     */
    public function getLatestWavePickNumber()
    {
        return $this->model->orderBy('wv_num', 'desc')->take(1)->first();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getWvHdrInfo($wvId, $whsId){
        return $this->model
            ->where('wv_id', $wvId)
            ->where('whs_id', $whsId)
            ->select(
                'wv_id',
                'wv_num',
                'wv_sts',
                'whs_id',
                'picker',
                'created_at'
            )
            ->first();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $cusLsts = array_get($userInfo, 'user_customers', 0);
        $cusIds = array_column($cusLsts, 'cus_id');
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        //foreach($cusLsts as $cusLst){
        //    if(!in_array($cusLst['cus_id'], $cusIds)){
        //        array_push($cusIds, $cusLst['cus_id']);
        //    }
        //}

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['wv_num','wv_sts'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }
        // concat all cus_id of order of wave pick
        // select wave has concat column in customer current user
        $query->whereHas("orderHdr", function ($query) use ($cusIds) {
            $query->whereIn("cus_id", $cusIds);
        });
        if (!empty($attributes['picker_id'])) {
            $count = DB::table('user_role')->where('user_id', $userId)->whereIn('item_name', ['Admin', 'manager'])
                ->count();
            if (!$count) {
                $query->where('picker', $userId);
            }
        }
        if (!empty($attributes['picker'])) {

            $query->whereHas("pickerUser", function ($q) use ($attributes) {
                $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['picker']) . "%");
                $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['picker']) . "%");
                $q->orWhere(
                    DB::raw("concat(first_name, ' ', last_name)"),
                    'like',
                    "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['picker'])) . "%"
                );
            });
        }


        $this->model->filterData($query);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     * @deprecated  DONt USE it
     */
    public function updateStatusWvHdr($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    public function updateWvComplete($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function updateWvPicking($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Picking", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function getWvHdrId($wvHdrIds)
    {
        $wvHdrIds = is_array($wvHdrIds) ? $wvHdrIds : [$wvHdrIds];

        $rows = $this->model
            ->whereIn('wv_id', $wvHdrIds)
            ->get();

        return $rows;
    }
}
