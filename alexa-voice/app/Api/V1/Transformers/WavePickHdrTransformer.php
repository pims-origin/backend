<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\Status;

class WavePickHdrTransformer extends TransformerAbstract
{

    /**
     * @param WavepickHdr $wvHdr
     *
     * @return array
     */
    public function transform(WavepickHdr $wvHdr)
    {
        // Change request: show number of order & sku in wave pick list
        $numOdr = OrderHdr::where('wv_id', $wvHdr->wv_id)->count();
        $numSku = WavepickDtl::where('wv_id', $wvHdr->wv_id)->count();

        return [
            'wv_id'      => $wvHdr->wv_id,
            'wv_num'     => $wvHdr->wv_num,
            'wv_sts'     => Status::getByKey("WAVEPICK-STATUS", $wvHdr->wv_sts),
            'created_at' => date_format($wvHdr->created_at, "m/d/Y"),
            'user'       => trim(object_get($wvHdr, "userCreated.first_name", null)
                . " " .
                object_get($wvHdr, "userCreated.last_name", null)),
            // User
            'picker_id'          => $wvHdr->picker,
            'picker_name'        => trim(object_get($wvHdr, 'pickerUser.first_name', null) . " " .
                object_get($wvHdr, 'pickerUser.last_name', null)),

            'num_odr'    => $numOdr,
            'num_sku'    => $numSku
        ];
    }
}
