<?php

namespace App\Api\Bay\Validators;


class ContainerChargeValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'cc_sts'  => 'required',
            'bay_id'  => 'required|integer',
            'cus_id'  => 'required|integer',
            'ctnr_id' => 'required|integer',
            'start'   => 'required'
        ];
        
    }
}
