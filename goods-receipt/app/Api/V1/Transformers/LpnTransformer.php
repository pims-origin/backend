<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Lpn;

class LpnTransformer extends TransformerAbstract
{

    /**
     * @param Lpn $lpn
     *
     * @return array
     */
    public function transform(Lpn $lpn)
    {
        return [
            'lpn_to_print' => object_get($lpn, 'lpn_qty', 0),
            'from'         => object_get($lpn, 'from', 0),
            'to'           => object_get($lpn, 'to', 0),
        ];
    }
}
