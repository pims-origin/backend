<?php

namespace Seldat\Wms2\Models;

class CustomerColor extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_color';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cl_name',
        'cl_code',
        'cus_id',
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
}
