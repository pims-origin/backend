<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\CustomerContactModel;
use Illuminate\Http\Response as IlluminateResponse;

class CustomerContactTest extends TestCase
{
    use DatabaseTransactions;

    protected $customerId;

    protected $customerContactParams = [
        'cus_ctt_fname'      => 'fname test',
        'cus_ctt_lname'      => 'lname test',
        'cus_ctt_email'      => 'email@email.com',
        'cus_ctt_phone'      => '012122212',
        'cus_ctt_mobile'     => '34234234',
        'cus_ctt_ext'        => '2344',
        'cus_ctt_position'   => '1',
        'cus_ctt_cus_id'     => 1,
        'cus_ctt_department' => '1',
    ];

    public function setUp()
    {
        parent::setUp();

        $customer = factory(\Seldat\Wms2\Models\Customer::class)->create();
        $this->customerId = $customer->cus_id;
        $this->customerContactParams['cus_ctt_cus_id'] = $this->customerId;
    }

    private function getEndPoint($customerContactId = null)
    {
        return "/v1/customers/{$this->customerId}/contacts" . ($customerContactId ? '/' . $customerContactId : '');
    }

    public function testList_Ok()
    {
        // make data
        factory(\Seldat\Wms2\Models\CustomerContact::class, 3)->create(['cus_ctt_cus_id' => $this->customerId]);

        $uri = $this->getEndPoint();
        $response = $this->call('GET', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
        $this->assertCount(3, $responseData['data']);
    }

    public function testShow_Ok()
    {
        // make data
        $contact = factory(\Seldat\Wms2\Models\CustomerContact::class)->create();
        $customerContactId = $contact->cus_ctt_id;

        $uri = $this->getEndPoint($customerContactId);

        $this->call('GET', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $uri = $this->getEndPoint();

        $response = $this->call('POST', $uri, $this->customerContactParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testStore_NumberContactIdArrayMoreThanFive_Fail()
    {
        $customerId = factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        factory(\Seldat\Wms2\Models\CustomerContact::class, 5)->create(['cus_ctt_cus_id' => $customerId]);

        $this->customerContactParams['cus_ctt_cus_id'] = $customerId;
        // make data
        $endPoint = "v1/customers/{$customerId}/contacts";
        $this->call('POST', $endPoint, $this->customerContactParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

    }

    public function testUpdate_Ok()
    {
        // make data
        $contact = factory(\Seldat\Wms2\Models\CustomerContact::class)->create();
        $customerContactId = $contact->cus_ctt_id;

        $uri = $this->getEndPoint($customerContactId);

        $response = $this->call('PUT', $uri, $this->customerContactParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testUpdate_WithZero_Fail()
    {
        $uri = $this->getEndPoint(99999);

        $this->call('PUT', $uri, $this->customerContactParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDelete_Ok()
    {
        // make data
        $contact = factory(\Seldat\Wms2\Models\CustomerContact::class)->create(['cus_ctt_cus_id' => $this->customerId]);
        $customerContactId = $contact->cus_ctt_id;

        $uri = $this->getEndPoint($customerContactId);

        $this->call('DELETE', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        $this->seeInDatabase((new \Seldat\Wms2\Models\CustomerContact())->getTable(),
            ['cus_ctt_id' => $contact->cus_ctt_id, 'deleted' => 1]);
    }

    public function testDelete_NotExit_Fail()
    {
        $uri = $this->getEndPoint(99999);

        $this->call('DELETE', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $contacts = factory(\Seldat\Wms2\Models\CustomerContact::class,
            2)->create(['cus_ctt_cus_id' => $this->customerId]);

        $params = [];

        foreach ($contacts as $key => $contact) {
            $params['cus_ctt_id'][$key] = $contact->cus_ctt_id;
        }

        $this->call('DELETE', $this->getEndPoint(), $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testsaveMassAndDeleteUnused_Ok()
    {
        // make data
        $contacts = factory(\Seldat\Wms2\Models\CustomerContact::class,
            2)->create(['cus_ctt_cus_id' => $this->customerId]);

        $uri = "/v1/customers/{$this->customerId}/contacts/save-mass-and-delete-unused";

        $params = [
            'cus_ctt_id'         => [
                '',
                $contacts[0]->cus_ctt_id,
                ''
            ],
            'cus_ctt_fname'      => [
                'fname1',
                'fname2',
                'fname3'
            ],
            'cus_ctt_lname'      => [
                'lname1',
                'lname2',
                'lname3'
            ],
            'cus_ctt_email'      => [
                'foo@gmail.com',
                'fab@gmail.com',
                'far@gmail.com'
            ],
            'cus_ctt_phone'      => [
                '11111',
                '22222',
                '33333'
            ],
            'cus_ctt_mobile'     => [
                '11111',
                '22222',
                '33333'
            ],
            'cus_ctt_ext'        => [
                '11111',
                '22222',
                '33333'
            ],
            'cus_ctt_position'   => [
                'A',
                'B',
                'C'
            ],
            'cus_ctt_department' => [
                '',
                'A',
                ''
            ],
        ];
        $this->call('POST', $uri, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        $tableName = (new \Seldat\Wms2\Models\CustomerContact())->getTable();
        $this->seeInDatabase($tableName, ['cus_ctt_id' => $contacts[0]->cus_ctt_id, 'deleted' => 0]);
        $this->seeInDatabase($tableName, ['cus_ctt_id' => $contacts[1]->cus_ctt_id, 'deleted' => 1]);
        $this->seeInDatabase($tableName, ['cus_ctt_fname' => 'fname1', 'deleted' => 0]);
        $this->seeInDatabase($tableName, ['cus_ctt_fname' => 'fname2', 'deleted' => 0]);
        $this->seeInDatabase($tableName, ['cus_ctt_fname' => 'fname3', 'deleted' => 0]);

    }

    public function testsaveMass_NumberContactIdArrayMoreThanFive_Ok()
    {
        // make data
        $contacts = factory(\Seldat\Wms2\Models\CustomerContact::class,
            6)->create(['cus_ctt_cus_id' => $this->customerId]);

        $uri = "/v1/customers/{$this->customerId}/contacts/save-mass-and-delete-unused";

        $params = [];

        foreach ($contacts as $key => $contact) {
            $params['cus_ctt_id'][$key] = $contact['cus_ctt_id'];
            $params['cus_ctt_fname'][$key] = $contact['cus_ctt_fname'];
            $params['cus_ctt_lname'][$key] = $contact['cus_ctt_lname'];
            $params['cus_ctt_email'][$key] = $contact['cus_ctt_email'];
            $params['cus_ctt_phone'][$key] = $contact['cus_ctt_phone'];
            $params['cus_ctt_ext'][$key] = $contact['cus_ctt_ext'];
            $params['cus_ctt_mobile'][$key] = $contact['cus_ctt_mobile'];
            $params['cus_ctt_position'][$key] = $contact['cus_ctt_position'];
            $params['cus_ctt_cus_id'][$key] = $contact['cus_ctt_cus_id'];
            $params['cus_ctt_department'][$key] = $contact['cus_ctt_department'];
        }

        $this->call('POST', $uri, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

    }
}