<?php

namespace App\Api\V2\Inbound\Pallet\Validators;


class PalletValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'whs_id' => 'required|integer|exists:warehouse,whs_id'
        ];
    }
}
