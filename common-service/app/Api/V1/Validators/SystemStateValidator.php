<?php

namespace App\Api\V1\Validators;


class SystemStateValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'sys_state_code'       => 'required',
            'sys_state_name'       => 'required',
            'sys_state_country_id' => 'required|exists:system_country,sys_country_id'
        ];
    }
}
