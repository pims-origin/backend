<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Outbound\CartonConsolidate\Create\Models;

use App\MyHelper;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\Status;

class LocationModel extends AbstractModel
{
    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    public function getActiveLocation($whsId, $wv_dtl_id, $loc_rfid)
    {
        $result = $this->model
            ->select([
                    DB::raw('COUNT(1) AS ctn_ttl'),
                    DB::raw('SUM(piece_remain) AS avail_qty'),
                    'ct.sku',
                    'ct.size',
                    'ct.color',
                    'ct.lot',
                    'location.loc_id',
                    'location.loc_code',
                    'ct.item_id',
                    'location.loc_sts_code'
                ]
            )
            ->join('cartons AS ct', 'ct.loc_id', '=', 'location.loc_id')
            ->join('wv_dtl', function ($join) {
                $join->on('wv_dtl.item_id', '=', 'ct.item_id')
                    ->on('wv_dtl.lot', '=', 'ct.lot');
            })
            ->where('location.loc_whs_id', $whsId)
            ->where('location.loc_sts_code', 'AC')
            ->where('location.rfid', $loc_rfid)
            ->where('ct.loc_type_code', 'RAC')
            ->where('ct.is_damaged', 0)
            ->where('ct.is_ecom', 0)
            ->whereNotNull('ct.rfid')
            ->where('wv_dtl.wv_dtl_id', $wv_dtl_id)
            ->groupBy('ct.item_id', 'ct.lot')
            ->get();

        return $result;
    }

    public function getLocation($whsId, $loc_rfid)
    {
        $result = $this->model
            ->select([
                    DB::raw('0 AS ctn_ttl'),
                    DB::raw('0 AS avail_qty'),
                    'location.loc_id',
                    'location.loc_code',
                    'location.loc_sts_code'
                ]
            )
            ->where('location.loc_whs_id', $whsId)
            ->where('location.rfid', $loc_rfid)
            ->get();

        return $result;
    }

    public function getActiveLocationWithoutWavePick($whsId, $loc_rfid)
    {
        $result = $this->model
            ->select([
                    DB::raw('COUNT(is_damaged) AS ctn_ttl'),
                    DB::raw('SUM(piece_remain) AS avail_qty'),
                    'ct.sku',
                    'ct.size',
                    'ct.color',
                    'ct.lot',
                    'location.loc_id',
                    'location.loc_code',
                    'ct.item_id',
                    'location.loc_sts_code'
                ]
            )
            ->leftJoin('cartons AS ct', 'ct.loc_id', '=', 'location.loc_id')
            ->where('location.loc_whs_id', $whsId)
            ->where('location.rfid', $loc_rfid)
            ->where('ct.is_damaged', 0)
            ->groupBy('ct.item_id', 'ct.lot')
            ->get();

        return $result;
    }

    /**
     * Get exist location for wave pick
     *
     * @param type $whsId
     * @param type $loc_rfid
     *
     * @return type
     */
    public function getExistLocationForWavePickWhenWrong($whsId, $loc_rfid)
    {
        $result = $this->model
            ->select([
                    DB::raw('SUM(IF(is_damaged = 0, 1, 0)) AS ctn_ttl'),
                    DB::raw('SUM(piece_remain) AS avail_qty'),
                    'ct.sku',
                    'ct.size',
                    'ct.color',
                    'ct.lot',
                    'location.loc_id',
                    'location.loc_code',
                    'ct.item_id',
                    'location.loc_sts_code'
                ]
            )
            ->leftJoin('cartons AS ct', 'ct.loc_id', '=', 'location.loc_id')
            ->where('location.loc_whs_id', $whsId)
            ->where('location.rfid', $loc_rfid)
            ->groupBy('ct.item_id', 'ct.lot')
            ->get();

        return $result;
    }

    public function getLocRFIDByLocId($locId)
    {
        return $this->model
            ->select('rfid')
            ->where('loc_id', $locId)
            ->first();
    }

    public function getLocIdByRFID($locRFID)
    {
        return $this->model
            ->select('loc_id', 'loc_code', 'loc_alternative_name', 'loc_sts_code')
            ->where('rfid', $locRFID)
            ->first();
    }

    public function getNotActiveLocByIds($locIds)
    {
        return $this->model
            ->whereIn('loc_id', $locIds)
            ->where('loc_sts_code', '!=', 'AC')
            ->get()
            ->toArray();
    }

    public function getFirstBy($key, $value, array $with = [], $filter = '', $filterCus = false)
    {
        $query = $this->make($with);
        if ($filter == 'current') {
            $this->model->filterData($query, $filterCus);
        } elseif ($filter == 'filterIn') {
            $this->model->filterDataIn($query, $filterCus);
        }

        return $query->where($key, '=', $value)->first();
    }

    public function getFirstWhere($where, array $with = [], array $orderBy = [], $columns = ['*'], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {

            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {

                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        foreach ($orderBy as $column => $sortType) {
            $query->orderBy($column, $sortType);
        }

        return $query->first($columns);
    }

    public function findWhere($where, array $with = [], array $orderBy = [], $columns = ['*'], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        foreach ($orderBy as $column => $sortType) {
            $query->orderBy($column, $sortType);
        }

        return $query->get($columns);
    }

    public function getLocationByIds($oldLocIdArr)
    {
        $oldLocIdArr = is_array($oldLocIdArr) ? $oldLocIdArr : [$oldLocIdArr];

        $rows = $this->model
            ->whereIn('loc_id', $oldLocIdArr)
            ->get();

        return $rows;
    }

}
