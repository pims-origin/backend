<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LocationTypeTest extends TestCase
{
    use DatabaseTransactions;

    protected $endpoint;

    /**
     * LocationTypeTest constructor.
     */
    public function __construct()
    {
        $this->endpoint = '/v1/location-types';
    }

    protected $searchParams = [
        'loc_type_id'   => 1,
        'loc_type_code' => 'abc',
        'loc_type_name' => 'location type name',
        'loc_type_desc' => 'This is the description',
    ];

    protected $locationTypeParams = [
        'loc_type_name' => 'type name 1',
        'loc_type_code' => 'type code 1',
        'loc_type_desc' => 'type description 1',
        'created_at'    => 1465961195,
        'updated_at'    => 1465961195,
        'deleted_at'    => 915148800,
        'deleted'       => 0,
    ];

    public function testSearch_Ok()
    {
        $this->call('GET', $this->endpoint, $this->searchParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testRead_Ok()
    {

        $locationTypeId = factory(\App\LocationType::class)->create()->loc_type_id;

        $this->call('GET', "{$this->endpoint}/{$locationTypeId}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $this->locationTypeParams['loc_type_code'] = uniqid() . uniqid();
        $this->call('POST', $this->endpoint, $this->locationTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    public function testStore_ExistUniqueLocType_Fail()
    {
        $locTypeCode = factory(\App\LocationType::class)->create()->loc_type_code;

        $this->locationTypeParams['loc_type_code'] = $locTypeCode;

        $this->call('POST', $this->endpoint, $this->locationTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_Ok()
    {
        $locationTypeId = factory(\App\LocationType::class)->create()->loc_type_id;

        $this->locationTypeParams['loc_type_code'] = uniqid() . uniqid();
        $this->locationTypeParams['loc_type_name'] = 'Name was edited by Functional test';
        $this->call('PUT', "{$this->endpoint}/{$locationTypeId}", $this->locationTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_UniqueLocType_Fail()
    {
        $locTypeCode = factory(\App\LocationType::class)->create()->loc_type_code;
        $locTypeId = factory(\App\LocationType::class)->create()->loc_type_id;
        $this->locationTypeParams['loc_type_code'] = $locTypeCode;
        $this->call('PUT', "{$this->endpoint}/{$locTypeId}", $this->locationTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_WithZero_Fail()
    {
        $this->call('PUT', "{$this->endpoint}/0", $this->locationTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_NotExist_Fail()
    {
        $this->call('PUT', "{$this->endpoint}/999999999999999999", $this->locationTypeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDelete_Ok()
    {
        $locationTypeId = factory(\App\LocationType::class)->create()->loc_type_id;

        $this->call('DELETE', "{$this->endpoint}/{$locationTypeId}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDelete_NotExit_Fail()
    {
        $this->call('DELETE', "{$this->endpoint}/9999999999999999");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDelete_WithZero_Fail()
    {
        $this->call('DELETE', "{$this->endpoint}/0");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $locationTypes = factory(App\LocationType::class, 3)->create();
        $params = [];
        foreach ($locationTypes as $key => $locationType) {
            $params['loc_type_id'][$key] = $locationType->loc_type_id;
        }
        $this->call('DELETE', $this->endpoint, $params);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDeleteMass_NoParams_Fail()
    {
        $this->call('DELETE', $this->endpoint);
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
