<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Validators;


class LocationValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_code'               => 'required',
            'loc_whs_id'             => 'required|integer|exists:warehouse,whs_id',
            'loc_type_id'            => 'required|integer|exists:loc_type,loc_type_id',
            'loc_sts_code'           => 'required|string|exists:loc_status,loc_sts_code',
            'loc_available_capacity' => 'integer',
//            'loc_min_count'          => 'integer|min:0',
            'loc_max_count'          => 'integer',
            'aisle'                  => 'required|string|max:3',
            'row'                    => 'required|string|max:3',
            'level'                  => 'required|integer|min:1|max:9',
            'bin'                    => 'required|integer|min:1|max:9',
            'spc_hdl_code'           => 'required',
        ];
    }
}
