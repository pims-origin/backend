<?php

namespace App;

use App\Utils\Database\Eloquent\SoftDeletes;

class Report extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reports';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'rp_id';

    /**
     * @var array
     */
    protected $fillable = [
        'rp_id',
        'grp_id',
        'disp_odr',
        'rp_sts',
        'rp_type',
        'sts',
        'des',
    ];

    public function reportGroup()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ReportGroup', 'grp_id', 'grp_id');
    }
}