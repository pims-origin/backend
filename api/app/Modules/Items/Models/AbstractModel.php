<?php

namespace App\Modules\Items\Models;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\Modules\Items\Eloquents\User;
use App\Modules\Items\Eloquents\Customer;
use App\Modules\Items\Eloquents\Item;
use App\Modules\Items\Eloquents\CusExUser;
use App\Modules\Items\Eloquents\SystemUom;

abstract class AbstractModel
{
    public function __construct()
    {
        //
    }

    protected function getCurrentUserId()
    {
        $key = env("JWT_SECRET");
        $auth = app('request')->header('Authorization');
        $jwt = str_replace("Bearer ", "", $auth);
        $pub_key = openssl_pkey_get_private(FIREBASE_PRIVATE_KEY);
        $keyData = openssl_pkey_get_details($pub_key);
        $decoded =  JWT::decode($jwt, $keyData['key'], array('RS256'));
        $userId = $decoded->sub;
        
        return $userId;
    }

    protected function checkCusExUser($cusId, $userId)
    {
        $check = CusExUser::where('cus_id', $cusId)
            ->where('user_id', $userId)
            ->first();

        return $check ? true : false;
    }

    protected function getUomByUomCode($uomCode)
    {
        $uomId =  SystemUom::where('sys_uom_code', $uomCode)
            ->where('deleted', 0)
            ->first();

        return $uomId;
    }
}