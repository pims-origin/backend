<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\PackRef;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class PackRefModel extends AbstractModel
{
    public function __construct(PackRef $model = null)
    {
        $this->model = ($model) ?: new PackRef();
    }

    /**
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function search($attributes, array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['width'])) {
            $query->where('width', $attributes['width']);
        }
        if (isset($attributes['height'])) {
            $query->where('height', $attributes['height']);
        }
        if (isset($attributes['length'])) {
            $query->where('length', $attributes['length']);
        }

        if (isset($attributes['pack_type'])) {
            $query->where('pack_type', $attributes['pack_type']);
        }

        //$this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }


}
