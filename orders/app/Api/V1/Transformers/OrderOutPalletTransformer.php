<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\PackHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OutPallet;
use Seldat\Wms2\Utils\Status;

/**
 * Class MasterPalletTransformer
 *
 * @package App\Api\V2\BillOfLadings\MasterPalletList\Transformers
 */
class OrderOutPalletTransformer extends TransformerAbstract
{
    /**
     * @param OutPallet $outPallet
     *
     * @return array
     */
    public function transform(OutPallet $outPallet)
    {
        $mixedSKU  = object_get($outPallet, 'sku_ttl', 0) > 1 ? "Yes" : "No";
        // Calculate volume if volume = null
        $volume = object_get($outPallet, 'volume');
        if (!$volume) {
            $packHdrModel = new PackHdrModel();
            $packHdrs =  $packHdrModel->findWhere(['out_plt_id' => $outPallet->plt_id]);
            $volume = 0;
            foreach($packHdrs as $packHdr) {
                // $volume += ($packHdr->width * $packHdr->height * $packHdr->length) / 1728;
                $volume += ($packHdr->width * $packHdr->height * $packHdr->length);
            }
        }
        return [
            'plt_id'        => object_get($outPallet, 'plt_id'),
            'plt_num'       => object_get($outPallet, 'plt_num'),
            'mixed_sku'     => $mixedSKU,
            'loc_code'      => object_get($outPallet, 'loc_code'),
            'loc_name'      => object_get($outPallet, 'loc_name'),
            'ctns'          => object_get($outPallet, 'ctn_ttl'),
            'volume'        => round($volume, 2),
            'length'        => object_get($outPallet, 'length'),
            'width'         => object_get($outPallet, 'width'),
            'height'        => object_get($outPallet, 'height'),
            'weight'        => object_get($outPallet, 'weight'),
        ];
    }
}
