<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\BlockDtlModel;
use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Transformers\BlockDtlTransformer;
use App\Api\V1\Transformers\CycleDtlTransformer;
use App\Api\V1\Validators\CycleDtlValidator;
use App\Api\V1\Validators\UpdateActualCycleDtlValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V1\Validators\CycleHdrValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\CycleHdr;
use App\Api\V1\Transformers\CycleHdrTransformer;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class BlockDtlController extends AbstractController
{
    protected $model;
    protected $blockDtlTransform;
    protected $request;
    protected $error;

    /**
     * BlockDtlController constructor.
     * @param BlockDtlModel $model
     * @param Request $request
     */
    public function __construct(BlockDtlModel $model, Request $request)
    {
        $this->model = $model;
        $this->blockDtlTransform = new BlockDtlTransformer();
        $this->request = $request;
    }

    /**
     * @param $blockHdrId
     * @return \Dingo\Api\Http\Response|string
     */
    public function listDtl($blockHdrId)
    {
        $input = $this->request->getQueryParams();
        $input['sort']['block_dtl_id'] = 'ASC';

        try {
            $data = $this->model->search($blockHdrId, null, $input, [], array_get($input, 'limit', 20));

            return $this->response->paginator($data, $this->blockDtlTransform);

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $id
     */
    public function unblockItem($id)
    {
        $input = $this->request->getParsedBody();
    }

    /**
     * @param $blockHdrId
     * @param $blockDtlId
     * @return \Dingo\Api\Http\Response|string
     */
    public function view($blockHdrId, $blockDtlId)
    {
        $input = $this->request->getQueryParams();

        try {

            $data = $this->model->search($blockHdrId, $blockDtlId, $input);

            return $this->response->paginator($data, $this->blockDtlTransform);

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }
}
