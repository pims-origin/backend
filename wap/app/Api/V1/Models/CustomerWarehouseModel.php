<?php

namespace App\Api\V1\Models;

//use App\Api\V1\Traits\CusZoneCusWarehouseService;
use \Seldat\Wms2\Models\CustomerWarehouse;
use Seldat\Wms2\Utils\Status;

class CustomerWarehouseModel extends AbstractModel
{
    //use CusZoneCusWarehouseService;
    protected $model;

    public function __construct(CustomerWarehouse $model = null)
    {
        $this->model = ($model) ?: new CustomerWarehouse();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @deprecated  NOT IN USE
     * @return mixed
     */
    public function searchCustomerByWarehouse1($attributes = [], $with = [], $limit = null)
    {
        $statusNew = Status::getByKey("ASN_STATUS", "NEW");
        $statusRC = Status::getByKey("ASN_STATUS", "RECEIVING");

        $query = $this->make($with)
            ->where('whs_id', $attributes['whs_id']);

        $query = $query->whereHas('asnHdr', function ($query) use ($statusNew, $statusRC) {
            $query->where('asn_sts', $statusNew)
                    ->orWhere('asn_sts', $statusRC);
        });

        $this->sortBuilder($query, $attributes);

        if (!$limit) {
            $limit = 20;
        }

        $models = $query->paginate($limit);

        return $models;
    }

    public function searchCustomerByWarehouse($attributes = [], $limit = null)
    {
        $statusNew = Status::getByKey("ASN_STATUS", "NEW");
        $statusRC = Status::getByKey("ASN_STATUS", "RECEIVING");

        $query = $this->model
            ->join('asn_hdr', 'asn_hdr.cus_id', '=', 'customer_warehouse.cus_id')
            ->where('asn_hdr.whs_id', $attributes['whs_id'])
            ->whereIn('asn_hdr.asn_sts', [$statusNew, $statusRC]);

        /*if (!$limit) {
            $limit = 20;
        }*/

        $query = $query->groupBy('customer_warehouse.cus_id');

        //$models = $query->paginate($limit);
        $models = $query->get();

        return $models;
    }
}
