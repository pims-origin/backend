<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\WarehouseLocations;
use League\Fractal\TransformerAbstract;

class WarehouseLocationsTransformer extends TransformerAbstract
{
    public function transform(WarehouseLocations $warehouseLocations)
    {
        return [
            'loc_code' => $warehouseLocations->loc_code,
            'loc_alternative_name' => $warehouseLocations->loc_alternative_name,
            'whs_name' => object_get($warehouseLocations, 'Warehouse.whs_name', ''),
            'zone_name' => object_get($warehouseLocations, 'Zone.zone_name', ''),
            'loc_type_name' => object_get($warehouseLocations, 'LocationType.loc_type_name', ''),
            'loc_sts_name' => object_get($warehouseLocations, 'LocationStatus.loc_sts_name', ''),
            'loc_available_capacity' => $warehouseLocations->loc_available_capacity,
            'loc_length' => $warehouseLocations->loc_length,
            'loc_width' => $warehouseLocations->loc_width,
            'loc_height' => $warehouseLocations->loc_height,
            'loc_max_weight' => $warehouseLocations->loc_max_weight,
            'loc_weight_capacity' => $warehouseLocations->loc_weight_capacity,
            'loc_min_count' => $warehouseLocations->loc_min_count,
            'loc_max_count' => $warehouseLocations->loc_max_count,
        ];
    }

    /*
    ****************************************************************************
    */

}
