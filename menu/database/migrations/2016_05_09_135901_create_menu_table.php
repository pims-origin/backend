<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('menu_id');
            $table->string('name', 50);
            $table->string('description', 255)->nullable()->default('NULL');
            $table->string('url', 255)->nullable()->default('NULL');
            $table->string('target', 10)->nullable()->default('NULL');
            $table->string('permission_name', 50)->nullable()->default('NULL');
            $table->integer('menu_group_id');
            $table->integer('parent_id')->nullable()->default(null);
            $table->integer('display_order');
            $table->string('icon_class', 255)->nullable()->default('NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu');
    }
}
