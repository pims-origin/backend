<?php

namespace App\Modules\Customers\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Modules\Customers\Resources\CustomerResource;

class CustomerCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => CustomerResource::collection($this->collection)
        ];
    }
}