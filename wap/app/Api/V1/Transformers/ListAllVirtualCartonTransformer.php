<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Utils\Status;

class ListAllVirtualCartonTransformer extends TransformerAbstract
{
    //return result
    public function transform(VirtualCarton $vtlCarton)
    {
        $vtlCtnSts = object_get($vtlCarton, 'vtl_ctn_sts', null);
        return [
            'vtl_ctn_id' => object_get($vtlCarton, 'vtl_ctn_id', null),
            'ctn_rfid'   => object_get($vtlCarton, 'ctn_rfid', null),
            /*'asn_hdr_id'       => object_get($vtlCarton, 'asn_hdr_id', null),
            'asn_hdr_data'     => object_get($vtlCarton, 'asnHdr', null),
            'vtl_ctn_sum_id'   => object_get($vtlCarton, 'vtl_ctn_sum_id', null),
            'asn_dtl_id'       => object_get($vtlCarton, 'asn_dtl_id', null),
            'asn_dtl_data'     => object_get($vtlCarton, 'asnDtl', null),
            'item_id'          => object_get($vtlCarton, 'item_id', null),
            'item_data'        => object_get($vtlCarton, 'item', null),
            'ctnr_id'          => object_get($vtlCarton, 'ctnr_id', null),
            'ctnr_data'        => object_get($vtlCarton, 'ctnr', null),
            'cus_id'           => object_get($vtlCarton, 'cus_id', null),
            'cus_name'         => array_get($vtlCarton, 'customer.cus_name'),
            'whs_id'           => object_get($vtlCarton, 'whs_id', null),*/
            'is_damage'        => object_get($vtlCarton, 'is_damage', 0),
            'vtl_ctn_sts'      => $vtlCtnSts,
            'vtl_ctn_sts_name' => Status::getByKey("VIRTUAL-CARTON-STATUS", $vtlCtnSts),
        ];
    }


}
