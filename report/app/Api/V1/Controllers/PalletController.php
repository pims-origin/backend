<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-June-2017
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;

use App\Api\V1\Models\PalletModel;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\DB;
use App\Api\V1\Transformers\PalletTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Utils\Message;

class PalletController extends AbstractController
{
    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;


    /**
     * PalletController constructor.
     *
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param PalletModel $palletModel
     */
    public function __construct
    (
        GoodsReceiptModel $goodsReceiptModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        PalletModel $palletModel
    ) {
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
        $this->palletModel = $palletModel;
    }


    /**
     * @param $whs_id
     * @param Request $request
     * @param PalletTransformer $palletTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        $whs_id,
        Request $request,
        PalletTransformer $palletTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        if (!empty($input['export']) && $input['export'] == 1) {
            $this->export($whs_id, $input);
            die;
        }
        try {
            $palletInfos = $this->palletModel->search($whs_id, $input, [], array_get($input, 'limit'));

            return $this->response->paginator($palletInfos, $palletTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param $input
     *
     * @return $this|void
     * @throws \Exception
     */
    private function export(
        $whs_id,
        $input
    ) {
        $warehouse = DB::table('warehouse')->where([
            'whs_id'     => $whs_id,
            'deleted_at' => 915148800,
            'deleted'    => 0
        ])->first();

        if (empty($warehouse)) {
            throw new \Exception(Message::get("BM017", "Warehouse"));
        }

        try {
            $palletInfos = $this->palletModel->search($whs_id, $input, [], null, true)->toArray();

            $title = [
                'cus_code'                  => 'Customer Code',
                'cus_name'                  => 'Customer Name',
                'rfid'                      => 'Pallet RFID',
                'loc_code'                  => 'Location',
                'sku'                       => 'SKU',
                'size'                      => 'Size',
                'color'                     => 'Color',
                'ctn_pack_size'             => 'Pack Size',
                'lot'                       => 'Lot',
                'des'                       => 'Description',
                'upc'                       => 'UPC',
                //'init_ctn_ttl'              => 'Initial CTNS',
                'current_ctn_ttl'           => 'CTNS',
                'current_piece_ttl'         => 'Qty',
                'dmg_ctn_ttl'               => 'Damaged CTNS',
                'created_at|format()|m/d/Y H:i:s' => 'Created at',
            ];

            $filePath = storage_path() . "/Report_GR_{$warehouse['whs_name']}.csv";
            $filePath = "Report_GR_{$warehouse['whs_name']}.csv";
            $this->saveFile($title, $palletInfos, $filePath);

            //$output = fopen($filePath, 'w+');
            //fputcsv($output, ["Number", "Description", "test"]);
            //fputcsv($output, ["100", "testDescription", "10"]);
            //
            //header('Content-Type: application/octet-stream');
            //header('Content-Disposition: attachment; filename="' . $filePath . '"');
            //header('Content-Length: ' . filesize($filePath));
            //echo readfile($filePath);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        $writer->openToBrowser($filePath);

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case "*":
                            $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                            break;
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();

    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function customersPalletDropDown($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $palletCustomers = $this->palletModel->customers($whs_id, $input);

            return ['data' => $palletCustomers];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function skuPalletAutoComplete($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $palletSkus = $this->palletModel->skuPalletAutoComplete($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $palletSkus];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }


}
