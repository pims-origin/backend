<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 22-Jul-2016
 * Time: 3:02
 */

namespace App\Api\Inbound\Models;

use Seldat\Wms2\Models\DamageType;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class DamageTypeModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     * @param DamageType $model
     */
    public function __construct(DamageType $model = null)
    {
        $this->model = ($model) ? : new DamageType();
    }

    /**
     * Search damageType
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'dmg_id') {
                    $query->where($key, $value);
                }
                elseif (in_array($key, ['dmg_name', 'dmg_code'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);
        return $models;
    }
}
