<?php

namespace App\Api\V1\Validators;


class WarehouseZoneValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'zone_name'      => 'required',
            'zone_code'      => 'required|max:6',
            'zone_type_id'   => 'required|integer|exists:zone_type,zone_type_id',
            'zone_min_count' => 'integer|min:0|max:999',
            'zone_max_count' => 'integer|greater_than:zone_min_count|min:0|max:999',
        ];
    }
}
