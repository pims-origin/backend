<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportLaborTracking;
use League\Fractal\TransformerAbstract;

class LaborTrackingReportTransformer extends TransformerAbstract
{
    public function transform(ReportLaborTracking $reportLaborTracking)
    {
        return [
            'fullname' => object_get($reportLaborTracking, 'fullname', ''),
            'customer' => object_get($reportLaborTracking, 'cus_name', ''),
            'owner' => object_get($reportLaborTracking, 'owner', ''),
            'trans_num' => object_get($reportLaborTracking, 'trans_num', ''),
            'start_time' => date('Y/m/d H:i:s', object_get($reportLaborTracking, 'start_time', '')),
            'end_time' => date('Y/m/d H:i:s', object_get($reportLaborTracking, 'end_time', '')),
            'act_hours' => number_format(round(object_get($reportLaborTracking, 'total_time', ''), 3), 2),
            'total_hours' => $this->roundUp(round(object_get($reportLaborTracking, 'total_time', ''), 3)),
            'type' => object_get($reportLaborTracking, 'lt_type', ''),
            'po_num' => object_get($reportLaborTracking, 'po_num', null),
            'user_id' => $reportLaborTracking->user_id
        ];
    }

    public function roundUp($number)
    {
        $whole = floor(floatval($number));
        $fraction = $number - $whole;
        if ($fraction == 0) {
            return number_format($whole, 1);
        }
        if ($fraction <= 0.5) {
            $whole += 0.5;
        } else {
            $whole += 1;
        }
        return number_format($whole, 1);
    }
}
