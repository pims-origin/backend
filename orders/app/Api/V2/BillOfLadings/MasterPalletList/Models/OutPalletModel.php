<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 1-Sep-16
 * Time: 09:25
 */

namespace App\Api\V2\BillOfLadings\MasterPalletList\Models;

use Seldat\Wms2\Models\OutPallet;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;


/**
 * Class OutPalletModel
 *
 * @package App\Api\V1\Models
 */
class OutPalletModel extends AbstractModel
{
    /**
     * @param OutPallet $model
     */
    public function __construct(OutPallet $model = null)
    {
        $this->model = ($model) ?: new OutPallet();
    }

    public function getMasterPallet($whsId, $shipId, $limit = 20)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->model
            ->select([
                    'opp.plt_id',
                    'opp.plt_num',
                    'opp.loc_code',
                    'opp.loc_name',
                    'opp.ctn_ttl',
                    'sp.ship_sts',
                    'oh.odr_sts',
                    DB::raw('count(distinct ph.odr_hdr_id) as mixed_order'),
                ])
            ->join('out_pallet as opp' , 'opp.plt_id'       , '=', 'out_pallet.parent')
            ->join('shipment as sp'    , 'opp.bol_id'       , '=', 'sp.ship_id')
            ->join('pack_hdr as ph'    , 'out_pallet.plt_id', '=', 'ph.out_plt_id')
            ->join('odr_hdr as oh'     , 'ph.odr_hdr_id'    , '=', 'oh.odr_id')
            ->where('out_pallet.whs_id', $whsId)
            ->where('out_pallet.bol_id', $shipId)
            ->where('sp.deleted', 0)
            ->where('ph.deleted', 0)
            ->groupBy('out_pallet.parent');

        return $query->paginate($limit);
    }

}
