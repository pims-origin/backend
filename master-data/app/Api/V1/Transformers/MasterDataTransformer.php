<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\MasterData;
use League\Fractal\TransformerAbstract;

class MasterDataTransformer extends TransformerAbstract
{
    public function transform(MasterData $masterData)
    {
        $mdtName   = trim(object_get($masterData, "masterDataType.mdt_name", null));
        $createdBy = trim(object_get($masterData, "createdBy.first_name", null) . " " . object_get($masterData, "createdBy.last_name", null));
        $updatedBy = trim(object_get($masterData, "updatedBy.first_name", null) . " " . object_get($masterData, "updatedBy.last_name", null));

        return [
            'md_code'    => $masterData->md_code,
            'md_type'    => $masterData->md_type,
            'mdt_name'   => $mdtName,
            'md_name'    => $masterData->md_name,
            'md_des'     => $masterData->md_des,
            'md_data'    => $masterData->md_data,
            'md_sts'     => $masterData->md_sts,
            'created_at' => date("Y-m-d H:i:s", strtotime($masterData->created_at)),
            'created_by' => $createdBy,
            'updated_at' => date("Y-m-d H:i:s", strtotime($masterData->updated_at)),
            'updated_by' => $updatedBy,
        ];
    }
}