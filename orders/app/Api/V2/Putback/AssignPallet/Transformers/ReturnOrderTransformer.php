<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V2\Putback\AssignPallet\Transformers;

use Seldat\Wms2\Models\ReturnHdr;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class ReturnOrderTransformer extends TransformerAbstract
{
    /**
     * @param ReturnHdr $returnHdr
     *
     * @return array
     */
    public function transform(ReturnHdr $returnHdr)
    {
        return [
            'return_id'       => object_get($returnHdr, 'return_id', ''),
            'return_num'      => object_get($returnHdr, 'return_num', ''),
            'odr_id'          => object_get($returnHdr, 'odr_hdr_id', ''),
            'odr_num'         => object_get($returnHdr, 'odr_hdr_num', ''),
            'cus_id'          => object_get($returnHdr, 'odrHdr.customer.cus_id', ''),
            'cus_name'        => object_get($returnHdr, 'odrHdr.customer.cus_name', ''),
            'return_sts'      => object_get($returnHdr, 'return_sts', ''),
            'return_sts_name' => Status::getByKey("RETURN-ORDER", object_get($returnHdr, 'return_sts', '')),
            'num_of_sku'      => OrderDtl::where('odr_id',
                $returnHdr->odr_hdr_id)->distinct('item_id')->count('item_id'),
            'return_qty'      => object_get($returnHdr, 'return_qty', ''),
            'created_at'      => object_get($returnHdr, 'created_at', '')->format('Y-m-d'),
            'created_by'      => trim(object_get($returnHdr, "odrHdr.createdBy.first_name", null) . " " .
                object_get($returnHdr, "odrHdr.createdBy.last_name", null)),
            'putter_id'       => object_get($returnHdr, 'putter', 0),
            'putter_name'     => trim(object_get($returnHdr, "putterUser.first_name", null) . " " .
                object_get($returnHdr, "putterUser.last_name", null)),
            'csr'             => object_get($returnHdr, 'csr', 0),
            //get item for return order detail
            'items'           => $returnHdr->items
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("Y-m-d", $date) : null;
    }
}
