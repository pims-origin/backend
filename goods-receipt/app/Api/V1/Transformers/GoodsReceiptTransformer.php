<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GoodsReceipt;

class GoodsReceiptTransformer extends TransformerAbstract
{
    public function transform(GoodsReceipt $goodsReceipt)
    {
        return [
            'asn_hdr_num'          => object_get($goodsReceipt, 'asnHrd.asn_hdr_num', null),
            'asn_hdr_ref'          => object_get($goodsReceipt, 'asnHrd.asn_hdr_ref', null),
            'sys_measurement_code' => object_get($goodsReceipt, 'asnHrd.sys_measurement_code', null),
            'gr_hdr_id'            => object_get($goodsReceipt, 'gr_hdr_id', null),
        ];
    }
}
