<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 20-Sep-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;

/**
 * Class WavePickStatusController
 *
 * @package App\Api\V1\Controllers
 */
class WavePickStatusController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return array|null|string
     */
    public function show(Request $request)
    {
        $input = $request->getQueryParams();
        $inputType = array_get($input, 'type', '');
        if (!$inputType){
            return $this->response->errorBadRequest('Type is required');
        }

        $types = Status::get("WAVEPICK-STATUS");
        //$types = WAVEPICK_STATUS;
        $data = [];
        if (!empty($types) && is_array($types)) {
            foreach ($types as $key => $value) {
                $data[] = [
                    "key"   => $key,
                    "value" => $value,
                    "checked" => false
                ];
            }
        }

        $userId = Data::getCurrentUserId();
        $userMeta = DB::table('user_meta')
                            ->where('user_id', $userId)
                            ->where('qualifier', 'WSF')
                            ->first();
        if ($userMeta && $inputType){
            $userValue = collect(json_decode($userMeta['value']));
            $userValue = $userValue->get($inputType, []);
            if ($userValue){
                $userValue = collect($userValue)->keyBy('key');
            }
            $data = collect($data)->keyBy('key');
            $data = $data->merge($userValue)->values();
        }

        return ['data' => $data];
    }
}
