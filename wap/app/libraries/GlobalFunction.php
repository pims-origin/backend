<?php
function get($object, $value, $defaultValue = null, $callback = null) {
    $value = explode('.', $value);

    $dataReturn =  \App\MyHelper::get($object, $value, $defaultValue);

    if(is_callable($callback) && $dataReturn !== null && $dataReturn != '') {
        $callback($dataReturn);
    }
    return $dataReturn;

}

function getValue($value)
{
    return $value instanceof Closure ? $value() : $value;
}

function convertToTradeCost($cost, $currency = null, $isHundred = false) {
    $user = \Oauth2Resource::getUser()->user;
    $currencyUser = get($user,'id_currency');

    /*if ($currencyUser == Currency::DEFAULT_CURRENCY && !in_array(\Repository\UserRepository::getCurrentUserType(), [7,8])) {
        return $cost;
    }*/
    if(is_null($currency) && (empty($currencyUser) || $currencyUser == Currency::DEFAULT_CURRENCY)) {
        return $cost;
    }
    $rounding = Config::get('custom.rounding_decimals');
    $cost = (double)MyHelper::convertCostByClient($cost, $rounding, false, true, true, $currency);

    if($currency==null)
        $currency=MyHelper::getCurrencyByUser();

    return MyHelper::roundUp($cost,getRoundingByCurrency($currency, $isHundred));
}

function addQueryStringURL($url, $queryString)
{
    $pound = "";
    $poundPos = -1;

    //Is there a #?
    if (($poundPos = strpos($url, "#")) !== false) {
        $pound = substr($url, $poundPos);
        $url = substr($url, 0, $poundPos);
    }

    $parse = parse_url($url);

    if(isset($parse['query'])) {

        parse_str($parse['query'], $listParse);
        $parseQueryString = explode('=', $queryString);
        if (in_array($parseQueryString[0], array_keys($listParse))) {
            $listParse[$parseQueryString[0]] = $parseQueryString[1];
            $finalQueryString = '?'.http_build_query($listParse);
        }
        else{
            $separator = '&';
            $finalQueryString = $parse['query'].$separator.$queryString;
        }
    }
    else{
        $separator = '?';
        $finalQueryString = $separator.$queryString;
    }

    $finalUrl = $parse['scheme'].'://'.$parse['host'].$parse['path'].$finalQueryString;
    return $finalUrl;
}

function showNewLine()
{
    if (PHP_SAPI === 'cli') {
        return "\n";
    } else {
        return '<br>';
    }
}

function getFormatByLanguage($key){
    $lang = Session::get('lang');
    $lang = strtolower($lang);
    $config = Config::get("app.format.{$lang}.{$key}");
    return $config;
}

function getRoundingByCurrency($currency = Currency::DEFAULT_CURRENCY, $isHundred = false){
    $currency = strtoupper($currency);
    if ($isHundred) {
        $currency .= "2";
        $config = Config::get("custom.rounding_currency.{$currency}");
    } else {
        $config = Config::get("custom.rounding_currency.{$currency}");
    }
    if (! $config) {
        if ($currency != Currency::DEFAULT_CURRENCY) {
            return getRoundingByCurrency(Currency::DEFAULT_CURRENCY);
        }
        else {
            throw new Exception('Lack of config rounding Currency.');
        }
    }
    return $config;
}

function getDecimalByCurrency($currency = Currency::DEFAULT_CURRENCY){

    $currency = strtoupper($currency);
    $config = Config::get("custom.decimal_currency");
    if (! $config) {
        if ($currency != Currency::DEFAULT_CURRENCY) {
            return getRoundingByCurrency(Currency::DEFAULT_CURRENCY);
        }
        else {
            throw new Exception('Lack of config rounding Currency.');
        }
    }
    return $config[$currency];
}

function getClientIp(){
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
        $clientIpAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $clientIpAddress = $_SERVER['REMOTE_ADDR'];
    }
    return $clientIpAddress;
}

function assetFace($link = '') {
    return asset(autoTagVersion($link));
}

function _unset(&$object, $keyArr)
{
    if (is_string($keyArr)) {
        _unset($object, [$keyArr]);
    } else {
        foreach($keyArr as $key) {
            if(is_array($object)) {
                unset($object[$key]);
            } else {
                unset($object->$key);
            }
        }
    }
}
/*
 * Get url with cdn
 */
function cdn( $asset )
{
    return MyHelper::cdn($asset);
}

function isJsonString($string){
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

/*
 * Generate url with route name & language
 */
function routeLang($routeName, $lang = null, $routeParameters = []){
    $url = route($routeName, $routeParameters);
    $patterns = Config::get('app.available_language');

    if(!$lang){
        $lang = App::getLocale();
    }

    foreach($patterns as &$p){
        $p = "/\/{$p}\//";
    }

    return preg_replace($patterns, "/{$lang}/", $url);
}

/*
 * Function for debug
 */
function pr($data = null, $die = true)
{
    $trace = debug_backtrace();
    $caller = array_shift($trace);
    echo '<pre>';
    echo "called by [" . $caller['file'] . "] line: " . $caller['line'] . "\n";
    var_dump($data);
    echo '</pre>';
    if ($die) {
        exit;
    }
}

function autoTagVersion($path)
{
    $file = public_path($path);
    if (file_exists($file)) {
        $parts = explode('.', $path);
        $extension = array_pop($parts);
        array_push($parts, filemtime($file), $extension);
        $path = implode('.', $parts);
        return $path;
    }
    else {
        if(getenv('debug')) {
            throw new \Exception('The file "' . $path . '" cannot be found in the public folder');
        } else {
            \Log::info('Missing file '.$path);
        }
        return $path;
    }
}