<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;

class CartonModel extends AbstractModel
{

    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    public function getSuggest($attributes, $limit = null, $with = [])
    {
        if (!is_array($attributes['loc_id'])) {
            $attributes['loc_id'] = [$attributes['loc_id']];
        }

        $query = $this->make($with)
            ->select(['ctn_id', 'ctn_num'])
            ->where('whs_id', $attributes['whs_id'])
            ->where('item_id', $attributes['item_id'])
            ->whereIn('loc_id', $attributes['loc_id'])
            ->where('ctn_sts', "AC");

        if (!empty($limit)) {
            $model = $query->take($limit)->get();
        } else {
            $model = $query->get();
        }

        return $model;
    }

    /**
     * @param $ctnNum
     *
     * @return mixed
     */
    public function checkActiveCartonByCtnNum($ctnNum)
    {
        $ctnNums = is_array($ctnNum) ? $ctnNum : [$ctnNum];

        return $this->model->whereIn('ctn_num', $ctnNums)
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('loc_id')
            ->select('ctn_id', 'ctn_num')
            ->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function getAllCartonByItemId($data)
    {
        return $this->model->whereIn('ctn_num', $data['ctnNums'])
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('item_id', $data['itemId'])
            ->where('whs_id', $data['whsId'])
            ->where('cus_id', $data['cusId'])
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('loc_id')
            ->select('ctn_id', 'ctn_num', 'piece_remain', 'created_at', 'loc_id', 'loc_code', 'plt_id')
            ->get();
    }

    /**
     * @param $locId
     *
     * @return mixed
     */
    public function getAllCartonByLocIds($locId)
    {

        $locIds = is_array($locId) ? $locId : [$locId];

        return $this->model
            ->leftJoin('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->whereIn('cartons.loc_id', $locIds)
            ->get();
    }

    public function getCartonsByLocs($locIds, $limit = null)
    {
        $query = $this->model->select(['ctn_id', 'ctn_num'])
            ->whereIn('loc_id', $locIds)
            ->where('ctn_sts', "AC")
            ->where('loc_type_code', 'RAC')
            ->where(function ($q) {
                $q->orWhere('is_damaged', 0);
                $q->orWhereNull('is_damaged');
            })
            ->where('is_ecom', 0);

        if (!empty($limit)) {
            return $query->limit($limit)->get();
        } else {
            return $query->get();
        }
    }
}
