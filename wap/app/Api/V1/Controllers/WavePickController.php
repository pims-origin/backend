<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\WaveDtlLocModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\WavePickHdrModel;
use App\Api\V1\Transformers\WvHdrDetailsTransformer;
use App\Api\V1\Transformers\WvHistorySkuTransformer;
use App\Api\V1\Validators\WaveValidator;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\PalletSuggestLocation;
use Seldat\Wms2\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Seldat\Wms2\Utils\Status;

class WavePickController extends AbstractController
{
    /**
     * @var Wave pick header
     */
    protected $waveHdrModel;

    /**
     * @var Wave pick detail
     */
    protected $waveDtlModel;

    /**
     * @var Wave pick detail
     */
    protected $waveDtlLocModel;

    /**
     * @var Order Header
     */
    protected $odrHdr;
    protected $odrCtn;

    /**
     * @var Order Detail
     */
    protected $odrDtl;
    protected $ctnModel;

    /**
     * @var Location
     */
    protected $loc;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    protected $STR_PAD_LEFT = 0;
    protected $userId;

    /**
     * WavePickController constructor.
     */
    public function __construct()
    {
        $this->waveHdrModel = new WavePickHdrModel();
        $this->waveDtlModel = new WavePickDtlModel();
        $this->waveDtlLocModel = new WaveDtlLocModel();
        $this->odrHdr = new OrderHdrModel();
        $this->odrDtl = new OrderDtlModel();
        $this->loc = new LocationModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->ctnModel = new CartonModel();
        $this->loc = new LocationModel();
        $this->userId = JWTUtil::getPayloadValue('jti');
    }

    public function getWavePickList($whsId, Request $request, WaveValidator $waveValidator)
    {
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['picker'] = $this->userId;
        //$waveValidator->validate($input);
        $requireFieldsValidate = $waveValidator->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        try {
            $arrWvs = $this->waveHdrModel->findWhere([
                'whs_id' => $input['whs_id'],
                'picker' => $input['picker']
            ], ['details']);

            if (!empty($arrWvs)) {
                foreach ($arrWvs as $key => $arrWv) {
                    $arrWv = $this->getWvHdr($arrWv);

                    $arrWvs[$key] = $arrWv;
                }
            } else {
                return null;
            }

            return $this->response->collection($arrWvs, new WvHdrDetailsTransformer());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function getWvHdr($wvHdr)
    {
        $wvHdr['of_order'] = $this->odrHdr->countOdrHdrByWvId($wvHdr['wv_id']);
        $wvHdr['total_sku'] = count($wvHdr['details']);
        $actual = $this->waveDtlModel->getActualWvDtl($wvHdr['wv_id']);
        $wvHdr['actual'] = 0;
        if (!empty($actual[0])) {
            $wvHdr['actual'] = array_get($actual[0], 'actual', 0);
        }

        $statusWvDtl = $this->waveDtlModel->getStatusWvHdr($wvHdr['wv_id']);
        $status = 'NOT';
        if (!empty($statusWvDtl[0])) {
            $sumActualWvdtl = array_get($statusWvDtl[0], 'actual', 0);
            $sumPieceQtyWvdtl = array_get($statusWvDtl[0], 'piece_qty', 0);

            if ($sumActualWvdtl === $sumPieceQtyWvdtl) {
                $status = 'DONE';
            } else if ($sumActualWvdtl > 0 && $sumActualWvdtl < $sumPieceQtyWvdtl) {
                $status = 'IN';
            }
        }

        $wvHdr['status'] = $status;

        return $wvHdr;
    }

    public function getSuggestPalletLocation($whsId = false, $wvDtlID = false, Request $request)
    {
        if (!$whsId || !$wvDtlID) {
            return $this->response->errorNotFound();
        }
        //1. select sug_loc_ids where wv_dtl_id = $wvDtlID on table wv_dt_loc
        $location = $this->waveDtlLocModel->getFirstWhere(
            [
                'wv_dtl_id' => $wvDtlID
            ]
        );
        if (empty($location)) {
            $msg = sprintf("This wv_dtl_id %s doesn't exist", $wvDtlID);

            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }
        //2. get location: loc_id, loc_code, loc_name from location table where 1.
        $arrIds = explode(',', $location->sug_loc_ids);
        $loc = [];

        foreach ($arrIds as $id) {
            $locDt = [];
            $ctn = $this->ctnModel->getCtnByLocationId($id);
            if (!empty($ctn)) {
                // code support team wap need loc rfid
                $locObj = $this->loc->getFirstWhere([
                    'loc_id' => $ctn->loc_id
                ]);

                $countCtn = $this->ctnModel->countCtnByLocationId($id);
                $sumPiece = $this->ctnModel->sumPieceOfCtnByLocationId($id);
                $locDt['loc_id'] = $ctn->loc_id;
                $locDt['loc_rfid'] = $locObj->rfid;
                $locDt['loc_code'] = $ctn->loc_code;
                $locDt['cartons'] = $countCtn;
                $locDt['pieces'] = $sumPiece->pieces;
                $loc[] = $locDt;
            }

        }

        //3. select * from wv_dtl where wv_dt_id =  $wvDtlID,
        $detail = $this->waveDtlModel->getFirstWhere(
            [
                'wv_dtl_id' => $wvDtlID
            ]
        );
        $data = [];

        $t1 = ceil($detail->piece_qty / $detail->pack_size);
        $t2 = ceil($detail->act_piece_qty / $detail->pack_size);
        $wv_hdr = $this->waveHdrModel->getFirstBy('wv_id', $detail->wv_id);
        $data['wv_dtl'] = [
            'wv_dtl_id'        => $detail->wv_dtl_id,
            'wv_id'            => $detail->wv_id,
            'item_id'          => $detail->item_id,
            'wv_num'           => $detail->wv_num,
            'color'            => $detail->color,
            'sku'              => $detail->sku,
            'size'             => $detail->size,
            'pack_size'        => $detail->pack_size,
            'wv_dtl_sts'       => $detail->wv_dtl_sts,
            'wv_sts'           => $wv_hdr->wv_sts,
            'allocate_cartons' => $t1 . '/' . $t2,
            'allocate_pieces'  => $detail->piece_qty . '/' . $detail->act_piece_qty
        ];
        $data['location'] = $loc;
        $data['histories'] = $this->getHistorySKUs($detail->wv_id);
        $array['data'] = $data;

        return new Response($array, 200, [], null);
    }

    private function getHistorySKUs($wvHdrID)
    {
        $wvDtls = $this->waveDtlModel->findWhere(['wv_id' => $wvHdrID]);
        $data = [];
        if (!is_null($wvDtls)) {
            foreach ($wvDtls as $wvDtl) {
                $item = [];
                $item['sku'] = $wvDtl->sku;
                $item['color'] = $wvDtl->color;
                $item['size'] = $wvDtl->size;
                $item['pack_size'] = $wvDtl->pack_size;
                $t1 = ceil($wvDtl->piece_qty / $wvDtl->pack_size);
                $t2 = ceil($wvDtl->act_piece_qty / $wvDtl->pack_size);
                $item['allocate_cartons'] = $t1 . '/' . $t2;
                $item['allocate_pieces'] = $wvDtl->piece_qty . '/' . $wvDtl->act_piece_qty;
                $data[] = $item;
            }
        }

        return $data;
    }
}
