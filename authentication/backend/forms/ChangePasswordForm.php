<?php

namespace backend\forms;

use common\models\User;
use Yii;

class ChangePasswordForm extends UpdatePasswordForm
{
    const PASSWORD_MIN_LENGTH = 8;

    public $user_id;
    public $old_password;
    public $new_password;
    public $repeat_new_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['old_password', 'findPasswords'],
            ['new_password', 'validatePassword'],
            [['user_id', 'old_password', 'new_password', 'repeat_new_password'], 'required'],
            ['repeat_new_password', 'compare', 'compareAttribute' => 'new_password']
        ];
    }

    public function findPasswords($attribute, $params)
    {
        $security = Yii::$app->security;
        $user = User::find()->where([
            'user_id' => $this->user_id
        ])->one();
        $password = $user->password;
        $verify = $security->validatePassword($this->old_password, $password);
        if (!$verify) {
            $this->addError($attribute, 'Old password is incorrect');
        }
    }

    public function changePassword(User $user)
    {
        return $this->updatePassword($user);
    }
}