<?php

// Pack detail
$api->group(['prefix' => '/v2/packs', 'namespace' => 'App\Api\V2\PackDetail\ManualPack\Controllers'], function ($api) {

    $api->post('/{orderHdrId:[0-9]+}', [
        'action' => "orderPacking",
        'uses'   => 'PackDtlController@store'
    ]);

    $api->get('/get-track-num/{packHdrId:[0-9]+}', [
        'action' => "orderPacking",
        'uses'   => 'PackDtlController@getTrackNumByPackHdr'
    ]);

    $api->post('/get-sku', [
        'action' => "orderPacking",
        'uses'   => 'PackDtlController@isExistNewSku'
    ]);

    $api->get('/{orderHdrId:[0-9]+}/details', [
        'action' => "orderPacking",
        'uses'   => 'PackDtlController@show'
    ]);

    // order
    $api->get('/{orderId:[0-9]+}', [
        'action' => "orderPacking",
        'uses'   => 'PackHdrController@show'
    ]);

    // download FEDEX/UPS shipping document
    $api->get('/download-shipping-label/{packHdrId:[0-9]+}', [
        'action' => "orderPacking",
        'uses'   => 'PackDtlController@downloadShippingLabel'
    ]);
});
$api->group(['prefix' => '/v2', 'namespace' => 'App\Api\V2\PalletAssignment\Controllers'], function ($api) {
    $api->put('/packs-assign-pallets/{print:[0-9]+}', [
        'action' => "palletAssignment",
        'uses'   => 'PackHdrController@assignPallet'
    ]);
});
