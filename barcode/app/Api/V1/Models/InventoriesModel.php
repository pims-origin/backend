<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\Inventories;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\Warehouse;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class InventoriesModel extends AbstractModel
{
    protected $warehouse;
    protected $customer;
    protected $item;
    public function __construct()
    {
        $this->model = new Inventories();
        $this->warehouse = new Warehouse();
        $this->customer = new Customer();
        $this->item = new Item;
    }

    public function updateInventory($whs_id, $cus_id, $item_id, $qty) {
        $inventory = $this->getFirstWhere([
            "whs_id"    => $whs_id,
            "cus_id"    => $cus_id,
            "item_id"   => $item_id,
            "type"      => "I",
        ]);
        if(empty($inventory)) {
            $warehouse = $this->warehouse->where("whs_id",$whs_id)->first();
            $customer = $this->customer->where("cus_id", $cus_id)->first();
            $item = $this->item->where("item_id",$item_id)->first();
             return $this->create([
                    "whs_id"        => $whs_id,
                    "cus_id"        => $cus_id,
                    "whs_code"      => $warehouse->whs_code,
                    "cus_code"      => $customer->cus_code,
                    "item_id"       => $item->item_id,
                    "upc"           => $item->cus_upc,
                    "sku"           => $item->sku,
                    "pack"          => $item->pack,
                    "size"          => $item->size,
                    "color"         => $item->color,
                    "uom"           => $item->uom_code,
                    "type"          => "I",
                    "in_hand_qty"   => $qty,
                    "in_pick_qty"   => 0,
                ]);
        }
        $inventory->in_hand_qty += $qty;
        return $inventory->save();
    }
}