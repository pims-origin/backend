<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Warehouse;
use App\WarehouseZone;
use App\LocationType;
use Illuminate\Http\UploadedFile;

class LocationControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $warehouseId;
    protected $zoneId;
    protected $typeId;
    protected $statusCode;

    /**
     * LocationTest constructor.
     */

    public function getEndPointWarehouse()
    {
        return "/v1/warehouses/{$this->warehouseId}";
    }

    public function getEndPoint()
    {
        return $this->getEndPointWarehouse() . "/locations";
    }

    public function getEndPointZone()
    {
        return $this->getEndPointWarehouse() . '/zones';
    }


    protected $searchParams = [
        'loc_type_id'  => [1, 2, 3],
        'zone_type_id' => [1, 2, 3],
        'loc_zone_id'  => [1, 2, 3],
    ];

    protected $locationParams = [
        'loc_code'               => 'lotcoc',
        'loc_alternative_name'   => 'alternative name',
        'loc_whs_id'             => '',
        'loc_zone_id'            => '',
        'loc_type_id'            => '',
        'loc_sts_code'           => 'AC',
        'loc_available_capacity' => 1,
        'loc_width'              => 2,
        'loc_length'             => 3,
        'loc_height'             => 4,
        'loc_max_weight'         => 5,
        'loc_weight_capacity'    => 6,
        'loc_min_count'          => 7,
        'loc_max_count'          => 8,
        'loc_description'        => 'Created from Functional test',
        'created_at'             => 1465961195,
        'updated_at'             => 1465961195,
        'deleted_at'             => 915148800,
        'deleted'                => 0,
    ];

    public function setUp()
    {
        parent::setUp();
        // create data into DB
        $warehouse = factory(Warehouse::class)->create();
        $zone = factory(WarehouseZone::class)->create();
        $locationType = factory(LocationType::class)->create();

        $this->warehouseId = $warehouse->whs_id;
        $this->zoneId = $zone->zone_id;
        $this->typeId = $locationType->loc_type_id;

        $this->locationParams['loc_whs_id'] = $this->warehouseId;
        $this->locationParams['loc_zone_id'] = $this->zoneId;
        $this->locationParams['loc_type_id'] = $this->typeId;
    }

    public function testListByZone_Ok()
    {
        $this->call('GET', $this->getEndPointZone() . "/{$this->zoneId}/locations");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testListByZone_ZoneIdAndWarehouseIdNotExits_Ok()
    {
        $this->call('GET', "v1/warehouses/10/zones/0/locations");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testListWareHouse_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testListExceptZone_Ok()
    {
        $location = factory(\App\Location::class)->create();

        $uri = $this->getEndPointWarehouse() . "/free-locations";

        $this->call('GET', $uri);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testListExceptZone_WithZone_Ok()
    {
        $location = factory(\App\Location::class)->create();

        $uri = $this->getEndPointWarehouse() . "/free-locations?zone_id=1";
        $this->call('GET', $uri);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testListZone_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearchExceptZone_Ok()
    {
        $location = factory(\App\Location::class)->create();
        $this->warehouseId = $location->loc_whs_id;
        $this->call('GET', $this->getEndPointWarehouse() . '/free-locations?loc_zone_id=');
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_Ok()
    {
        $location = factory(\App\Location::class)->create();
        $warehouseId = $location->loc_whs_id;

        $this->call('GET', $this->getEndPoint(), $this->searchParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_NoParam_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testRead_Ok()
    {

        $locationId = factory(\App\Location::class)->create()->loc_id;

        $this->call('GET', $this->getEndPoint() . "/{$locationId}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {

        $result = $this->call('POST', $this->getEndPoint(), $this->locationParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_EmptyField_Fail()
    {
        unset($this->locationParams['loc_code']);
        $this->call('POST', $this->getEndPoint(), $this->locationParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_UniqueLocCode_Fail()
    {
        $location = factory(\App\Location::class)->create();
        $locCode = $location->loc_code;
        $logWarehouseId = $location->loc_whs_id;
        $this->locationParams['loc_code'] = $locCode;
        $this->locationParams['loc_whs_id'] = $logWarehouseId;
        $this->warehouseId = $location->loc_whs_id;
        $this->call('POST', $this->getEndPoint(), $this->locationParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_UniqueLocCode2_Fail()
    {
        $location = factory(\App\Location::class)->create();
        $locName = $location->loc_alternative_name;
        $logWarehouseId = $location->loc_whs_id;
        $this->locationParams['loc_alternative_name'] = $locName;
        $this->locationParams['loc_whs_id'] = $logWarehouseId;
        $this->warehouseId = $location->loc_whs_id;
        $this->call('POST', $this->getEndPoint(), $this->locationParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_Ok()
    {
        $location = factory(\App\Location::class)->create();
        //reset warehouseId
        $this->warehouseId = $location->loc_whs_id;
        $this->locationParams['loc_alternative_name'] = 'Name was edited by Functional test';

        $this->call('PUT', $this->getEndPoint() . "/{$location->loc_id}", $this->locationParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_UniqueLogCode_Fail()
    {
        $whsId = factory(\App\Warehouse::class)->create()->whs_id;
        $locationCode = factory(\App\Location::class)->create(['loc_whs_id' => $whsId])->loc_code;
        $location = factory(\App\Location::class)->create(['loc_whs_id' => $whsId]);
        $locationId = $location->loc_id;
        $locWhsId = $location->loc_whs_id;
        $this->warehouseId = $locWhsId;

        $this->locationParams['loc_code'] = $locationCode;

        $response = $this->call('PUT', $this->getEndPoint() . "/{$locationId}", $this->locationParams);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Status is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Message is: "Customer Code is existed."
        $this->assertEquals(
            "The Location Code is duplicated!",
            $responseData['error']['message']
        );
    }

    public function testUpdate_UniqueLogName_Fail()
    {
        $whsId = factory(\App\Warehouse::class)->create()->whs_id;
        $locationName = factory(\App\Location::class)->create(['loc_whs_id' => $whsId])->loc_alternative_name;
        $location = factory(\App\Location::class)->create(['loc_whs_id' => $whsId]);
        $locationId = $location->loc_id;
        $locWhsId = $location->loc_whs_id;
        $this->warehouseId = $locWhsId;

        $this->locationParams['loc_alternative_name'] = $locationName;

        $response = $this->call('PUT', $this->getEndPoint() . "/{$locationId}", $this->locationParams);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Status is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Message is: "Customer Code is existed."
        $this->assertEquals(
            "The Location Name is duplicated!",
            $responseData['error']['message']
        );
    }

    public function testUpdate_EmptyField_Fail()
    {
        $locationId = factory(\App\Location::class)->create()->loc_id;

        unset($this->locationParams['loc_code']);
        $this->call('PUT', $this->getEndPoint() . "/{$locationId}", $this->locationParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_WithZero_Fail()
    {
        $this->call('PUT', $this->getEndPoint() . "/0", $this->locationParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_NotExist_Fail()
    {
        $this->call('PUT', $this->getEndPoint() . "/999999999999999999", $this->locationParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDelete_Ok()
    {
        $location = factory(\App\Location::class)->create();
        $this->warehouseId = $location->loc_whs_id;
        $response = $this->call('DELETE', $this->getEndPoint() . "/{$location->loc_id}");
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 204
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        // Case 2: Data is empty
        $this->assertEmpty($responseData);
    }

    public function testDelete_NotExit_Fail()
    {
        $this->call('DELETE', $this->getEndPoint() . "/9999999999999999");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDelete_WithZero_Fail()
    {
        $this->call('DELETE', $this->getEndPoint() . "/0");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testSaveMass_Ok()
    {
        $whsId = factory(\App\Warehouse::class)->create()->whs_id;
        $zoneId = factory(WarehouseZone::class)->create()->zone_id;
        $arrLocationId = factory(\App\Location::class, 3)->create([
            'loc_zone_id' => $zoneId,
            'loc_whs_id'  => $whsId
        ])->toArray();
        $params = [];
        $n = 0;
        foreach ($arrLocationId as $key => $locationId) {
            if ($n == 2) {
                break;
            }
            $params['loc_id'][$key] = $locationId['loc_id'];
            $n++;
        }

        $this->call('POST', $this->getEndPointZone() . "/{$zoneId}/locations/save-mass-and-delete-unused", $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testSaveMass_NoParams_Fail()
    {
        $zoneId = factory(WarehouseZone::class)->create()->zone_id;
        $this->call('POST', $this->getEndPointZone() . "/{$zoneId}/locations/save-mass-and-delete-unused");
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testChangeStatus_Ok()
    {
        $locationId = factory(\App\Location::class)->create()->loc_id;

        $this->call('POST', $this->getEndPoint() . "/{$locationId}/change-status", [
            'loc_sts_dtl_sts_code'  => 'ac',
            'loc_sts_dtl_from_date' => '123',
            'loc_sts_dtl_to_date'   => '456',
            'loc_sts_dtl_reason'    => 'I like',
            'loc_sts_dtl_remark'    => 'abc',
            'updated_by'            => 1,

        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testChangeStatus_MissingField_Fail()
    {
        $locationId = factory(\App\Location::class)->create()->loc_id;

        $this->call('POST', $this->getEndPoint() . "/{$locationId}/change-status", [
            'loc_sts_dtl_sts_code'  => 'ac',
            'loc_sts_dtl_from_date' => '123',
            'loc_sts_dtl_to_date'   => '456',
            'loc_sts_dtl_reason'    => 'I like',
            'loc_sts_dtl_remark'    => 'abc',
        ]);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDeleteMass_Ok()
    {
        $locations = factory(App\Location::class, 3)->create();
        $params = [];
        foreach ($locations as $key => $location) {
            $params['loc_id'][$key] = $location->loc_id;
        }
        $response = $this->call('DELETE', $this->getEndPoint(), $params);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 204
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        // Case 2: Data is empty
        $this->assertEmpty($responseData);
    }

    public function testDeleteMass_NoParams_Fail()
    {
        $this->call('DELETE', $this->getEndPoint());

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testGetStatus_Ok()
    {
        $locationId = factory(\App\Location::class)->create()->loc_id;

        $this->call('POST', $this->getEndPoint() . "/{$locationId}/change-status", [
            'loc_sts_dtl_sts_code'  => 'ac',
            'loc_sts_dtl_from_date' => '123',
            'loc_sts_dtl_to_date'   => '456',
            'loc_sts_dtl_reason'    => 'I like',
            'loc_sts_dtl_remark'    => 'abc',
            'updated_by'            => 1,

        ]);

        $this->call('GET', $this->getEndPoint() . "/{$locationId}/change-status");

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    /**
     * Will Do. Don't Delete.
     */
    public function testValidateImport_Ok()
    {
        //$import_file = realpath(__DIR__ . '/../_data/import-test.xlsx');
        //$uploaded = new \Symfony\Component\HttpFoundation\File\UploadedFile($import_file, 'import-test.xlsx');
        //$uploadedFile = UploadedFile::createFromBase($uploaded);
        //
        //$whsId = factory(\App\Warehouse::class)->create()->whs_id;
        //
        //$response = $this->call('POST', "/v1/locations/validate-import", [
        //    'whs_id'    => $whsId,
        //    'file_type' => 'xlsx',
        //], [], ['file' => $uploadedFile]);
        //$responseData = json_decode($response->getContent(), true);
        //
        //dd($responseData);
        //$this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }
}
