<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 22/July/2016
 * Time: 11:05
 */

namespace App\Api\V4\Inbound\Pallet\ScanPallet\Validators;

class ScanPalletValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'pallet.pallet-rfid' => 'required',
            'pallet.ctn-rfid' => 'required',
        ];
    }
}
