<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\CommodityModel;
use App\Api\V1\Models\PackRefModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Models\PackTypeModel;
use App\Api\V1\Transformers\SystemCountryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use App\Api\V1\Transformers\CommodityTransformer;
use App\Api\V1\Transformers\PackRefTransformer;
use App\Api\V1\Transformers\PackTypeTransformer;
use App\Api\V1\Validators\CommodityValidator;
use App\Api\V1\Validators\SystemCountryValidator;
use App\Api\V1\Validators\PackRefValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class CountryController
 *
 * @package App\Api\V1\Controllers
 */
class PackTypeController extends AbstractController
{

    /**
     * @var SystemStateModel
     */
    protected $systemStateModel;

    /**
     * @var SystemCountryModel
     */
    protected $systemCountryModel;

    /**
     * @var SystemCountryValidator
     */
    protected $systemCountryValidator;

    /**
     * @var SystemCountryTransformer
     */
    protected $systemCountryTransformer;

    /**
     * @var SystemStateTransformer
     */
    protected $systemStateTransformer;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var CommodityModel
     */
    protected $commodityModel;

    /**
     * @var PackRefModel
     */
    protected $packRefModel;

    /**
     * @var PackTypeModel
     */
    protected $packTypeModel;

    /**
     * @var CommodityTransformer
     */
    protected $commodityTransformer;

    /**
     * @var PackRefTransformer
     */
    protected $packRefTransformer;

    /**
     * @var PackTypeTransformer
     */
    protected $packTypeTransformer;

    /**
     * CountryController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->model = new SystemCountryModel();
        $this->systemCountryModel = new SystemCountryModel();
        $this->systemStateModel = new SystemStateModel();
        $this->systemCountryValidator = new SystemCountryValidator();
        $this->systemCountryTransformer = new SystemCountryTransformer();
        $this->systemStateTransformer = new SystemStateTransformer();
        $this->commodityModel = new CommodityModel();
        $this->commodityValidator = new CommodityValidator();
        $this->commodityTransformer = new CommodityTransformer();
        $this->packRefModel = new PackRefModel();
        $this->packRefValidator = new PackRefValidator();
        $this->packRefTransformer = new PackRefTransformer();
        $this->packTypeTransformer = new PackTypeTransformer();
        $this->packTypeModel = new PackTypeModel();
    }


    /**
     * @param PackTypeTransformer $packTypeTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getDropDownList(
        PackTypeTransformer $packTypeTransformer
    ) {

        try {
            $packTypeInfo = $this->packTypeModel->all();
            if (empty($packTypeInfo)) {
                throw new \Exception(Message::get("BM017", "PackType"));
            }

            return $this->response->item($packTypeInfo, $packTypeTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                $e->getMessage() . " " . $e->getLine()
            //SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
