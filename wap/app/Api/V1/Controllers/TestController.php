<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\DamageCartonModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\LocationTypeModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\WavePickDtlModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\PalletModel;
use App\MessageCode;
use App\MyHelper;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Laravel\Lumen\Routing\Controller;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\VirtualCarton;

class TestController extends Controller
{

    public function __construct()
    {
        if (env('APP_ENV') != 'testing') {
            return "Access denied!";
        }
    }

    public function index()
    {
        return "Unit test API.";
    }

    public function testLog()
    {
        $msg = MessageCode::get('WAP001');
        $data = [
            'data' => null,
            'message' => $msg,
            'status' => false,
        ];

        return new Response($data, 200, [], null);

        MyHelper::log("test", [1, 2, 8], ['fileName' => 'cuong']);
        MyHelper::logException("test", [1, 2, 8]);
        return "Wrote file done.";
    }

    public function testCreateGRHdr($asnHdrId, $ctnrId)
    {
        $grModel = new GoodsReceiptModel();
        //$result = $grModel->createGRHeader($asnHdrId, $ctnrId);
        dd($result);
    }

    public function testCreateGRDtl($grHdrId, $asnDtlId)
    {
        $asnDtlModel = new AsnDtlModel();
        $grDtlModel = new GoodsReceiptDetailModel();
        $grModel = new GoodsReceiptModel();
        $grHdr = $grModel->getModel()->find($grHdrId);
        $asnDtl = $asnDtlModel->getModel()->find($asnDtlId);
        $result = $grDtlModel->createGrDtlNew($grHdr, $asnDtl);
        dd($result);
    } 

    public function testCreatePalletIfNotExist($grDtlId, $grHdrId, $pltRfid)
    {

//        $this->vtlCtnModel->updatePalletRFIDNull($pltRfid, $ansHdrId, $ctnrId);
//        $this->vtlCtnModel->updateVtlCtnPltRfidByVtlCtnRfid(array_values($arrCtnRfid), $pltRfid);
        $asnDtlModel = new AsnDtlModel();
        $grDtlModel = new GoodsReceiptDetailModel();
        $grModel = new GoodsReceiptModel();
        $palletModel = new PalletModel();
        
        $grDtl = $grDtlModel->getModel()->find($grDtlId);
        $grHdr = $grModel->getModel()->find($grHdrId);
        $asnDetailId = object_get($grDtl, 'asn_dtl_id');
        $asnDetail = $asnDtlModel->getModel()->find($asnDetailId);
        $result = $palletModel->createPalletIfNotExist($pltRfid, $grDtl, $grHdr, $asnDetail);
        dd($result);
    }

    public function testDeleteExistNotInPltRfid(Request $request)
    {
        $cartonModel = new CartonModel();
        $vtlCtnModel = new VirtualCartonModel();
        $input = $request->getParsedBody();
        $arrCtnRfid = array_get($input, 'pallet.ctn-rfid', null);
        $pltRfid = array_get($input, 'pallet.pallet-rfid', null);
        $grDtlId = array_get($input, 'gr_dtl_id', null);
        $result1 = $vtlCtnModel->deleteExistNotInPallet($pltRfid, $arrCtnRfid);
        $result2 = $cartonModel->deleteExistNotInPallet($pltRfid, $grDtlId, $arrCtnRfid);
        var_dump($result1, $result2);
    }

    /**
     * 6. Check If not define cartons, insert virtual/real cartons
     * 
     * @param Request $request
     */
    public function testCreateCtnIfNotDefined(Request $request)
    {
        $input = $request->getParsedBody();
        $whsId = array_get($input, 'whs_id');
        $pltRfid = array_get($input, 'plt_rfid');
        $wrongRFIDS = array_get($input, 'wrong_rfid');
        $asnDtlId = array_get($input, 'asn_dtl_id');
        $grDtlId = array_get($input, 'gr_dtl_id');
        $grDtlObj = (new GoodsReceiptDetailModel())->getModel()
                ->find($grDtlId)
                ;
        $asnDetailArr = (new AsnDtlModel())->getModel()
                ->find($asnDtlId)
                ->toArray();
        $grHdrId = object_get($grDtlObj, 'gr_hdr_id');
        $grHdr = (new GoodsReceiptModel())->getModel()
                ->find($grHdrId);
        $grHdrNum = object_get($grHdr, 'gr_hdr_num');
        $grModel = new GoodsReceiptModel();
        $grModel->createCtnIfNotDefined($whsId, $pltRfid, $wrongRFIDS, $grDtlObj, $asnDetailArr, $grHdrNum);
    }
    
    public function testSetRealDamageCarton(Request $request) {
        $input = $request->getParsedBody();
        $ctnsRfid = array_get($input, 'ctns_rfid');
        $asnDtlId = array_get($input, 'asn_dtl_id');
        
        $grModel = new GoodsReceiptModel();
        $asnDtl = (new AsnDtlModel())->getModel()
                ->find($asnDtlId);
        
        $result = $grModel->setRealDamageCarton($ctnsRfid, $asnDtl);
        dd($result);
    }
    
    public function testUpdateVtlCtnGRCreated(Request $request) {
        $input = $request->getParsedBody();
        $asnHdrId = array_get($input, 'asn_hdr_id');
        $ctnrId = array_get($input, 'ctnr_id');
        
        $grModel = new GoodsReceiptModel();
        $result = $grModel->updateVtlCtnGRCreated($asnHdrId, $ctnrId);
        dd($result);
    }

    public function updateASNHdrStatus() {

        $grModel = new GoodsReceiptModel();
        $result = $grModel->updateVtlCtnGRCreated($asnHdrId, $ctnrId);
        dd($result);
    }

}
