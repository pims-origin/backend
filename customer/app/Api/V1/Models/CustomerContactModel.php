<?php

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\CustomerContact;
use Illuminate\Database\Query\Builder;

class CustomerContactModel extends AbstractModel
{
    protected $model;

    public function __construct(CustomerContact $model = null)
    {
        $this->model = ($model) ?: new CustomerContact();
    }

    public function deleteCustomerContact($customerId, $contactId)
    {
        $contactId = is_array($contactId) ? $contactId : [$contactId];

        return $this->model
            ->where('cus_ctt_cus_id', $customerId)
            ->whereIn('cus_ctt_id', $contactId)
            ->delete();
    }

    public function getByCustomerId($customerId)
    {
        return $this->allBy('cus_ctt_cus_id', $customerId);
    }

    public function getContactByCustomer($customerId, $contactId)
    {
        $query = $this->model->where('cus_ctt_cus_id', $customerId)
            ->where('cus_ctt_id', $contactId);
        $this->model->filterData($query);

        return $query->first();
    }

    public function deleteMassContact($customerId, array $contactIds)
    {
        return $this->model->whereIn('cus_ctt_id', $contactIds)
            ->where('cus_ctt_cus_id', $customerId)->delete();
    }

    public function countContact($customerId)
    {
        return $this->model->where('cus_ctt_cus_id', $customerId)
            ->count();
    }
}
