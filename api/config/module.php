<?php
define("FIREBASE_PRIVATE_KEY", file_get_contents(base_path() . "/config/jwtRS256.key"));

return [
    'modules' => [
        'Ecom' => [
            "Inventory",
            "Orders",
            "Authentication",
            "Customers",
            "Warehouses",
            "Items",
            "Inventories",
            "Asns",
            "GoodsReceipts"
        ]
    ],
    'data' => [
        'whsOfUser',
        'cusOfUser',
        'userId'
    ]
];
