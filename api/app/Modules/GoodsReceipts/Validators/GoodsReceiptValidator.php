<?php

namespace App\Modules\GoodsReceipts\Validators;

use Validator;

class GoodsReceiptValidator
{
    public function rules()
    {
        
    }

    public function validate($input)
    {
        $validator = Validator::make($input, $this->rules(), $this->messages());

        if($validator->fails()) {
            foreach ($validator->messages()->toArray() as $error) {
                foreach ($error as $title) {
                    throw new \Exception(json_encode(['title' =>$title, 'detail' => 'Please try']));
                }
            }
        }

        return $validator;
    }

    public function messages()
    {
        
    }

    public function checkGR($gr)
    {
        if(empty($gr))
        {
            throw new \Exception(json_encode(['title' =>'Goods Receipt is not existed.', 'detail' => 'Please try']));
        }
    }
}