<?php

return [
    'INBOUND'             => 'ib',
    'OUTBOUND'            => 'ob',
    'WAREHOUSE-OPERATING' => 'wo',
    'ACTIVE'              => 'AC',
    'INACTIVE'            => 'IA',
    'LOCKED'              => 'LK',
    'RESERVED'            => 'RS',
    'asn_prefix'          => ['ASN','RMA'],
    'gr_prefix'           => 'GDR',
    'relocation_prefix'   => 'REL',
    'asn_status'          => [
        'NEW'                   => 'NW',
        'RECEIVING'             => 'RG',
        'RECEIVED'              => 'RE',
        'RCVD-PARTIAL'          => 'RP',
        'RECEIVING_COMPLETE'    => 'CO',
    ],
    'gr_status'           => [
        'RECEIVING'       => 'RG',
        'RECEIVED'        => 'RE',
        'RCVD-DISCREPANT' => 'RD',
    ],
    'item_status'         => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'location_status'     => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'ctn_status'          => [
        'ACTIVE'   => 'AC',
        "INACTIVE" => 'IA'
    ],
    'event'               => [
        'ASN-NEW'           => "ANW",
        'ASN-RECEIVING'     => "ARV",
        'ASN-RCVD-PARTIAL'  => "ARP",
        'ASN-COMPLETE'      => "ARC",
        'GR-RECEIVING'      => "GRP",
        'GR-EXCEPTION'      => "GRX",
        'GR-COMPLETE'       => "GRC",
        'GR-CANCEL'         => 'GRU',
        'PL-AWAY'           => "PUT",
        'REL-CTN-LOC'       => "LTL",
        'REL-LP-LOC'        => "LPL",
        'COMPLETE-RELOC'    => "RLC",
        'CON-CTN-LOC'       => "CTL",
        'CON-LP-LOC'        => "CTP",
        'COMPLETE-CON'      => "CNC",
        'SET-DAMAGED'       => 'GRS',
        'PALLET-ASSIGNED'   => 'GRA',
        'SUGGEST-LOCATION'  => 'GSL',
        'PUTAWAY-COMPLETED' => 'GPU',
        'UNSET-DAMAGED'     => 'UDC',
    ],
    'event-info'          => [
        'ASN-NEW'           => "%s created",
        'ASN-RECEIVING'     => "%s receiving",
        'ASN-RCVD-PARTIAL'  => "%s received partial",
        'ASN-COMPLETE'      => "%s received",
        'GR-RECEIVING'      => "Goods Receipt started for %s",
        'GR-EXCEPTION'      => "% received partial",
        'GR-CREATED'        => "%s created",
        //'GR-COMPLETE'       => "Goods Receipt %s created for %s",
        'GR-COMPLETE'       => 'Received %d Cartons',
        'GR-CANCEL'         => '',
        'PL-AWAY'           => "Move to %s",
        'REL-CTN-LOC'       => "Change carton %s location from %s to %s",
        'REL-LP-LOC'        => "Change License Plate %s location from %s to %s",
        'COMPLETE-RELOC'    => "Complete Relocation %s",
        'CON-CTN-LOC'       => "Change carton %s location from %s to %s",
        'CON-LP-LOC'        => "Change carton %s licence plate from %s to %s",
        'COMPLETE-CON'      => "Complete Consolidation %s",
        'SET-DAMAGED'       => "There are %d carton(s) damaged in %s",
        'PALLET-ASSIGNED'   => "Pallet assigned for %s",
        'SUGGEST-LOCATION'  => "Suggest %d location(s) for %d pallet(s)",
        'PUTAWAY-COMPLETED' => "Put %d pallet(s) on Rack completed",
        'UNSET-DAMAGED'     => "%d damaged carton(s) have been unset and pickable now",
    ],
    'cus_config_name'     => [
        'EDI_INTEGRATION' => 'edi_integration'
    ],
    'cus_config_value'    => [
        'EDI888' => '888',
        'EDI846' => '846',
        'EDI856' => '856',
        'EDI861' => '861',
        'EDI940' => '940',
        'EDI943' => '943',
        'EDI945' => '945'
    ],
    'cus_config_active'   => [
        'YES' => 'Y',
        'NO'  => 'N'
    ],
    'putaway_status'      => [
        'NEW'       => 'NW',
        'COMPLETED' => 'CO',
    ],
    'plt_status'          => [
        'AC' => 'ACTIVE',
        'AJ' => 'AJUST',
        'IA' => 'INACTIVE',
        'MV' => 'MOVED',
        'NW' => 'NEW',
        'PD' => 'PICKED',
        'TF' => 'TRANSFER',
        'AL' => 'ALLOCATED',
        'RG' => 'RECEIVING',
        'CC' => 'CANCELLED',
    ],
    'qualifier' =>  [
        'ORDER-STATUS'      => 'OSF',
        'WAVEPICK-STATUS'   => 'WSF',
        'ASN_STATUS'        => 'ASF',
        'GR_STATUS'         => 'GSF'
    ]
];