<?php

namespace App\Api\V2\Order\CancelOrder\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class OrderCancelValidator
 *
 * @package App\Api\V1\Validators
 */
class OrderCancelValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'odr_id' => 'required|integer|exists:odr_hdr,odr_id',
        ];
    }
}
