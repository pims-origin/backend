<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\Warehouse;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\Message;

class PutAwayControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $goodsReceiptId;
    protected $warehouseId;
    protected $customerId;
    protected $asnHeaderId;

    public function getEndPoint()
    {
        return "/v1/putaway/";
    }


    protected $params = [];

    public function setUp()
    {
        parent::setUp();
        // create data into DB
        $warehouse = factory(Warehouse::class)->create();
        $customer = factory(Customer::class)->create();
        $gr_hdr_id = factory(GoodsReceipt::class)->create()->gr_hdr_id;

        $this->warehouseId = $warehouse->whs_id;
        $this->customerId = $customer->cus_id;

        $this->params['whs_id'] = $this->warehouseId;
        $this->params['cus_id'] = $this->customerId;
        $this->params['gr_hdr_id'] = $gr_hdr_id;

    }

    public function testSearch_Ok()
    {
        // create param search
        $params = [
            'whs_id'        => $this->params['whs_id'],
            'cus_id'        => $this->params['cus_id'],
            'gr_hdr_num'    => str_random(10),
            'ctn_num'       => str_random(),
            'asn_dtl_po'    => str_random(),
            'asn_dtl_po_dt' => date('Y-m-d', strtotime('+' . mt_rand(0, 30) . ' days')),
        ];
        $search = [];
        $index = rand(-1, 6);
        if ($index > 0) {
            switch ($index) {
                case 1 :
                    $search = array_except($params, 'gr_hdr_num');
                    break;
                case 2 :
                    $search = array_except($params, 'whs_id');
                    break;
                case 3 :
                    $search = array_except($params, 'ctn_num');
                    break;
                case 4 :
                    $search = array_except($params, 'po');
                    break;
                case 5 :
                    $search = array_except($params, 'asn_dtl_po_dt');
                    break;
                case 6 :
                    $search = array_except($params, 'cus_id');
                    break;
                default :
                    break;
            }
        }


        $response = $this->call('GET', $this->getEndPoint(), $search);

        // Case 1: Response code is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is not empty
        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData);
    }

    public function test_getItemUpdate_OK()
    {

        // Test Show
        $data = $this->call('GET', $this->getEndPoint() . "itemupdate/" . $this->params['gr_hdr_id']);
        // Case 1: Response code is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);

    }

    public function test_UpdateITem_NoParams_Fail()
    {
        $this->call('PUT', $this->getEndPoint() . "itemupdate/" . PHP_INT_MAX);

        // Case 1: Response code is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_UpdateItem_GrHdrExisted_Fail()
    {
        $params[] = [
            'plt_id'     => 1,
            'old_loc_id' => 0,
            'loc_id'     => 1
        ];

        // Test Show
        $data = $this->call('PUT', $this->getEndPoint() . "itemupdate/" . PHP_INT_MAX, $params);

        // Case 1: Response code is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "Goods Receipt"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_UpdateItem_LocationNotExisted_Fail()
    {
        $params[] = [
            'plt_id'     => 1,
            'old_loc_id' => 0,
            'loc_id'     => PHP_INT_MAX
        ];

        // Test Show
        $data = $this->call('PUT', $this->getEndPoint() . "itemupdate/" . $this->params['gr_hdr_id'], $params);

        // Case 1: Response code is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            'The location is not existed!',
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function test_UpdateItem_OK()
    {
        $plt_id = factory(Pallet::class)->create()->plt_id;
        $loc_id = factory(Location::class)->create()->loc_id;
        $params[] = [
            'plt_id'     => $plt_id,
            'old_loc_id' => 0,
            'loc_id'     => $loc_id
        ];

        // Test Show
        $data = $this->call('PUT', $this->getEndPoint() . "itemupdate/" . $this->params['gr_hdr_id'], $params);

        // Case 1: Response code is 400
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);
    }
}
