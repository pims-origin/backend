<?php

namespace App\Api\Ecom\Transformers\ToteInfo;

use League\Fractal\TransformerAbstract;

class OrderHdrTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'details'
    ];

    public function transform($odrHdr)
    {
        $wvNum = array_get($odrHdr, 'waveHdr.wv_num', '');
        $wvId  = array_get($odrHdr, 'waveHdr.wv_id', '');

        return [
            'odr_num'   => $odrHdr->odr_num,
            'odr_id'    => $odrHdr->odr_id,
            'wv_num'    => $wvNum,
            'wv_id'     => $wvId
        ];
    }

    public function includeDetails($odrHdr)
    {
        $details = $odrHdr->details;
        $packs = $this->getPacks($odrHdr);

        if (!$details) {
            return [];
        }

        $details->transform(function($detail) use ($packs){
            $detail->remain_qty = $detail->piece_qty;
            $detail->pack_qty = 0;

            if ( isset($packs[$detail->item_id]) ) {
                $detail->pack_qty += $packs[$detail->item_id]['piece_ttl'];
                $detail->remain_qty -= $packs[$detail->item_id]['piece_ttl'];
            }

            return $detail;
        });

        return $this->collection( $details, new OrderDtlTransformer() );
    }

    private function getPacks($odrHdr)
    {
        $packs = array_get($odrHdr, 'packHdr', []);

        if ( !$packs ) {
            return [];
        }

        $packs = $packs->transform(function($pack){
            return [
                'item_id'       => $pack->item_id,
                'odr_hdr_id'    => $pack->odr_hdr_id,
                'piece_ttl'     => $pack->piece_ttl
            ];
        })->keyBy('item_id');

        return $packs;
    }
}
