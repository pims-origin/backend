<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddBlockForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `block_dtl` CHANGE `block_hdr_id` `block_hdr_id` INT(11) UNSIGNED NOT NULL;");

        Schema::table('block_dtl', function (Blueprint $table) {
            $table->foreign('block_hdr_id')->references('block_hdr_id')->on('block_hdr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
