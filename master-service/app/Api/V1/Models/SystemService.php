<?php
namespace App\Api\V1\Models;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use GuzzleHttp\Exception\RequestException;

class SystemService extends BaseService
{
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_SYSTEM');
        parent::__construct($request, $api);
    }

    protected function _sendResetPasswordEmail($endpoint, $user, $url)
    {
        $tpl = 'setup_password_url';
        if ($endpoint === 'mails/reset-password') {
            $tpl = 'reset_password_url';
        }

        $options = [
            'form_params' => [
                $tpl         => $url,
                'username'   => $user->username,
                'first_name' => $user->first_name,
                'last_name'  => $user->last_name,
                'email'      => $user->email,
            ]
        ];

        return $this->client->post($endpoint, $options);
    }

    public function sendUserResetPasswordEmail($user)
    {
        $endpoint = 'mails/user-reset-password';

        $options = [
            'form_params' => [
                'first_name' => $user->first_name,
                'last_name'  => $user->last_name,
                'email'      => $user->email
            ]
        ];

        $this->client->post($endpoint, $options);
    }

    public function sendForgetPasswordEmail($user, $url)
    {
        $endpoint = 'mails/reset-password';

        return $this->_sendResetPasswordEmail($endpoint, $user, $url);
    }

    public function sendSetupPasswordEmail($user, $url)
    {
        $endpoint = 'mails/setup-password';

        return $this->_sendResetPasswordEmail($endpoint, $user, $url);
    }

    public function getSystemUri($identifier)
    {
        return env('SYSTEM_URI') . ($identifier ? '/' . $identifier : '');
    }

    public function getSystemInfo($key)
    {
        $uri = $this->getSystemUri($key);

        try {
            $response = $this->client->get($uri);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function createSystemInfo($key, $params)
    {
        $uri = $this->getSystemUri($key);

        $options = [
            'form_params' => [
                'setting_value' => $params
            ]
        ];

        try {
            $response = $this->client->post($uri, $options);

            return $response;
        } catch (RequestException $e) {
            throw $e;
        }
    }

    public function updateSystemInfo($key, $params)
    {
        $uri = $this->getSystemUri($key);

        $options = [
            'form_params' => [
                'setting_value' => $params
            ]
        ];

        try {
            $response = $this->client->put($uri, $options);

            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    public function reportIssue($params)
    {
        try {
            $uri = "mails/report-issue";
            $ext = !empty($params['file']) ? $_FILES['file']['name'] : "";
            $options = [
                [
                    'name'     => 'other_email',
                    'contents' => empty($params['other_email']) ? "" : $params['other_email']
                ],
                [
                    'name'     => 'email_from',
                    'contents' => empty($params['email_from']) ? "" : $params['email_from']
                ],
                [
                    'name'     => 'phone',
                    'contents' => empty($params['phone']) ? "" : $params['phone']
                ],
                [
                    'name'     => 'content',
                    'contents' => empty($params['content']) ? "" : $params['content']
                ],
                [
                    'name'     => 'file',
                    'contents' => empty($params['file']) ? null : fopen($params['file']->getPathName(), 'rb')
                ],
                [
                    'name'     => 'ext',
                    'contents' => $ext
                ],
                [
                    'name'     => 'img_attach_screen',
                    'contents' => empty($params['img_attach_screen']) ? "" : $params['img_attach_screen']
                ]
            ];

            return $this->client->post($uri, ['multipart' => $options]);

        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }
}
