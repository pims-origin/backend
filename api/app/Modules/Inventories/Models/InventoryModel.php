<?php

namespace App\Modules\Inventories\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Modules\Inventories\Eloquents\Inventory;

class InventoryModel extends AbstractModel
{
    protected $cusOfUser;
    protected $curUserId;
    protected $inventory;

    public function __construct(
        Inventory $inventory
    )
    {
        $this->cusOfUser    = Config::get('data.cusOfUser');
        $this->curUserId    = Config::get('data.userId');

        $this->inventory    = $inventory ? $inventory : new Inventory;
    }

    public function getInvenByCus($whsId, $cusId)
    {
        $cusIds = [];
        $whsIds = [];

        foreach ($this->cusOfUser as $data) {
            if ($data->whs_id == $whsId && $data->cus_id == $cusId)
            {
                $whsIds[] = $data->whs_id;
                $cusIds[] = $data->cus_id;
            }
        }

        $query = $this->inventory->whereIn('whs_id', $whsIds)->whereIn('cus_id', $cusIds);

        return $query->get();
    }
}