<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-June-2017
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LocHisModel;
use App\Api\V1\Models\PalletModel;

use App\Api\V1\Transformers\LocHisTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Utils\Message;

class LocationHistoryController extends AbstractController
{
    /**
     * @var LocHisModel
     */
    protected $locHisModel;

    /**
     * PalletController constructor.
     *
     * @param LocHisModel $locHisModel
     */
    public function __construct
    (
        LocHisModel $locHisModel
    ) {
        $this->locHisModel = $locHisModel;
    }


    /**
     * @param $whs_id
     * @param Request $request
     * @param LocHisTransformer $locHistTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        $whs_id,
        Request $request,
        LocHisTransformer $locHistTransformer
    ) {

        $input = $request->getQueryParams();
        try {

            $locHisInfos = $this->locHisModel->search($whs_id, $input, array_get($input, 'limit', 12));

            return $this->response->paginator($locHisInfos, $locHistTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_REPORT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
