<?php

namespace App\Api\V1\Traits;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\InventorySummary;

trait XferControllerTrait
{
    private function validateUpdateTicketXfer($arr)
    {
        $fromLocCode = $arr['from_loc_code'];
        $toLocCode = $arr['to_loc_code'];
        $mezloc = $arr['mez_loc'];
        $error = "";
        unset($arr['to_loc_code'], $arr['from_loc_code'], $arr['mez_loc']);
        foreach ($arr as $key => $val) {
            if (!is_numeric($val) || $val < 0) {
                throw new \Exception($key . ' invalid');
            }
        }

        //check loc code
        if (strpos($fromLocCode, ' ') !== false) {
            throw new \Exception('Request loc code invalid');
        }
        if (strpos($toLocCode, ' ') !== false) {
            throw new \Exception('To loc code invalid');
        }

        //check location
        $locChk = \DB::table('location')
            ->select('loc_id')
            ->where('deleted', 0)
            ->where('loc_sts_code', 'AC')
            ->whereIn('loc_id', [$arr['from_loc'], $arr['to_loc']])
            ->pluck('loc_id');
        if (!in_array($arr['from_loc'], $locChk)) {
            $error .= " Request location " . $fromLocCode . " not available.";
        }
        if (!in_array($arr['to_loc'], $locChk)) {
            $error .= "To location " . $toLocCode . " not available.";
        }
        if ($error) {
            throw new \Exception($error);
        }

        //check item
        $itemArr = \DB::table('item')
            ->where('item_id', $arr['item_id'])
            ->where('status', 'AC')
            ->where('deleted', 0)
            ->first();
        if (empty($itemArr)) {
            throw new \Exception('Item ' . $arr['item_id'] . ' not available');
        }

        // check actual to location already has item
        if ($mezloc != $toLocCode) {
            $dataLocCheck = [
                'item_id' => $arr['item_id'],
                'loc_id'  => $arr['to_loc'],
            ];

            $checkCtn = $this->cartonModel->checkLocationAlreadyHasItem($dataLocCheck, "create");
            if (count($checkCtn) > 0) {
                $arrLoc = [];
                foreach ($checkCtn as $check) {
                    array_push($arrLoc, $check['loc_code']);
                }
                $strLoc = implode(', ', $arrLoc);
                throw new \Exception("Item is currently processing in location " . $strLoc);
            }
        }
    }

    private function processOddItem($oddCtn, &$oddItem)
    {
        $iaCtn = [];
        foreach ($oddCtn as $ctn) {
            if ($oddItem >= $ctn['piece_remain']) {
                $iaCtn[] = $ctn['ctn_id'];
            } else {
                //update ctn piece
                \DB::table('cartons')
                    ->where('ctn_id', $ctn['ctn_id'])
                    ->update(['piece_remain' => $ctn['piece_remain'] - $oddItem]);
            }
            $oddItem -= $ctn['piece_remain'];
        }

        return $iaCtn;
    }

    private function processMezLoc($userId, $cloneCtn, $mezLoc, $actQty)
    {
        //update mezLoc (to loc) item inventory
        $invtObj = InventorySummary::generateInvtItem($cloneCtn['sku'], $cloneCtn['size'], $cloneCtn['color'], 1, 'ECO',
            $cloneCtn['whs_id'], $cloneCtn['cus_id']);
        $invtObj->ttl += $actQty;
        $invtObj->avail += $actQty;
        $invtObj->save();

        //update from loc item inventory
        InventorySummary::where([
            'item_id' => $cloneCtn['item_id'],
            'lot'     => $cloneCtn['lot']
        ])
            ->update([
                'ttl'   => DB::raw('ttl - ' . $actQty),
                'avail' => DB::raw('avail - ' . $actQty),
            ]);
        //----------------------------------------------

        //clone cartons to mezLoc
        $itemId = $invtObj['item_id'];
        $locId = $mezLoc['loc_id'];
        $locCode = $mezLoc['loc_code'];
        $freDate = date('ym');

        $rs = DB::table('cartons')
            ->where([
                'item_id' => $itemId,
                'lot'     => 'ECO',
                'is_ecom' => 1,
                'deleted' => 0,
                'ctn_sts' => 'AC',
                'loc_id'  => $locId
            ])
            ->where('ctn_num', 'LIKE', "CTN-$freDate-Z-%")
            ->first();

        if ($rs) {
            $rs['piece_remain'] = $rs['piece_ttl'] = $rs['ctn_pack_size'] = $rs['piece_remain'] + $actQty;
            $rs['updated_by'] = $userId;
            $rs['updated_at'] = time();

            $this->cartonModel->updateTable('cartons', $rs, "ctn_id = " . $rs['ctn_id']);
        } else {
            $cntMaxNum = $this->getMaxCtnNum();

            $cloneCtn['piece_remain'] = $cloneCtn['piece_ttl'] = $cloneCtn['ctn_pack_size'] = $actQty;
            $cloneCtn['ctn_num'] = ++$cntMaxNum;
            $cloneCtn['loc_id'] = $locId;
            $cloneCtn['loc_name'] = $cloneCtn['loc_code'] = $locCode;
            $cloneCtn['plt_id'] = null;
            $cloneCtn['is_damaged'] = 0;
            $cloneCtn['item_id'] = $itemId;
            $cloneCtn['lot'] = "ECO";

            $cloneCtn['is_ecom'] = 1;
            $cloneCtn['loc_type_code'] = 'ECO';
            $cloneCtn['updated_by'] = $cloneCtn['created_by'] = $userId;
            $cloneCtn['created_at'] = $cloneCtn['updated_at'] = time();
            $cloneCtn['rfid'] = null;
            $cloneCtn['ucc128'] = str_pad($itemId, 5, '0', STR_PAD_LEFT) . str_pad($cloneCtn['cus_id'], 8, '0', STR_PAD_LEFT);

            unset($cloneCtn['ctn_id']);
            \DB::table('cartons')->insert($cloneCtn);
        }

    }

    private function getMaxCtnNum()
    {
        $freDate = date('ym');
        $dfCtnNum = "CTN-$freDate-Z-00000";
        $query = "SELECT MAX(ctn_num) AS max_ctn_num FROM cartons WHERE ctn_num LIKE 'CTN-$freDate-Z-%'";

        $ctnNum = \DB::selectOne($query);

        if (!$ctnNum['max_ctn_num']) {
            return $dfCtnNum;
        }

        return $ctnNum['max_ctn_num'];
    }

    private function getPltFromCtn($ctnIds)
    {
        if (empty($ctnIds)) {
            return [];
        }

        $ctnIds = implode(',', $ctnIds);
        $query = "
            SELECT DISTINCT plt_id FROM cartons WHERE ctn_id IN ($ctnIds)
        ";
        \DB::setFetchMode(\PDO::FETCH_COLUMN);
        $return = \DB::select($query);
        \DB::setFetchMode(\PDO::FETCH_ASSOC);

        return $return;
    }

    private function iaPlt($pltIds)
    {
        if (empty($pltIds)) {
            return true;
        }

        $pltIds = implode(',', $pltIds);
        $date = strtotime(date("Y-m-d", time()));
        $query = "
            UPDATE pallet p 
            LEFT JOIN cartons c ON c.plt_id = p.plt_id
            SET p.loc_id = NULL, p.loc_code = NULL, p.zero_date = $date
            WHERE p.plt_id IN ($pltIds) AND c.plt_id IS NULL";

        // Change request: update carton status to transferred & pallet, 25-11-2016
        $this->palletModel->updatePallet([$pltIds]);

        return $this->cartonModel->naturalExec($query);
    }

    private function rmCntNupdatePltTtl($ctns)
    {
        if (empty($ctns)) {
            return true;
        }

        $ctnIds = implode(',', $ctns);
        $query = "
            UPDATE cartons c, pallet p
            INNER JOIN (
                SELECT plt_id, count(*) as tt FROM cartons 
                WHERE ctn_id IN ($ctnIds) 
                    AND deleted = 0
                GROUP BY plt_id
            ) as j ON p.plt_id = j.plt_id
            SET c.ctn_sts = 'TF', c.loc_id = NULL,  c.loc_code = NULL, c.plt_id = NULL,
                p.ctn_ttl = p.ctn_ttl - j.tt,
                c.piece_remain = 0, c.piece_ttl = 0
            WHERE c.plt_id = p.plt_id 
                AND c.ctn_id IN ($ctnIds) 
        ";

        return $this->cartonModel->naturalExec($query);
    }
}