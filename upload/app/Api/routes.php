<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V1'];
        });

        $api->post('/sample', 'SampleController@store');
        $api->get('/sample', 'SampleController@index');

        $api->group(['prefix' => 'file'], function ($api) {
            $api->post('/', 'FileController@postUpload');
            $api->get('/', 'FileController@getList');

            $api->delete('/{id}', 'FileController@delete');

            $api->group(['prefix' => 'doc-type'], function ($api) {
                $api->get('/', 'FileController@getListDocType');
                $api->get('/{code}', 'FileController@getDetailDocType');
                $api->post('/', 'FileController@postAddEditDocType');
                $api->delete('/{code}', 'FileController@deleteDocType');
            });
        });

        $api->group(['prefix' => 'item-image'], function ($api) {
            $api->post('/', 'ItemImageController@postUpload');
            $api->get('/', 'ItemImageController@getList');

            $api->delete('/{idItemMeta}', 'ItemImageController@delete');
        });

        $api->post('/verify', 'VerifyController@postVerify');
        $api->get('/view/{key}', 'ViewController@view');
    });
});
