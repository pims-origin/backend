<?php

namespace Seldat\Wms2\Models;

use Seldat\Wms2\Utils\JWTUtil;

class Logs extends BaseModel
{
    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        $userId = JWTUtil::getPayloadValue('jti') ?: 0;

        // create a event to happen on saving
        static::saving(function ($table) use ($userId) {
            $table->created_by = $table->created_by ?: $userId;
        });
    }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'type',
        'evt_code',
        'owner',
        'transaction',
        'url_endpoint',
        'message',
        'header',
        'request_data',
        'response_data',
        'des',
        'created_by',
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(__NAMESPACE__ . '\EventLookup', 'evt_code', 'evt_code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }


}
