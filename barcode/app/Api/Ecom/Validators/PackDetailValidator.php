<?php

namespace App\Api\Ecom\Validators;


class PackDetailValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'whs_id'             => 'required|integer|exists:warehouse,whs_id',
            'tracking_number'    => 'required',
            'details'            => 'required',
            'details.*.item_id'  => 'required',
            'details.*.pack_qty' => 'required',
        ];

    }


}
