<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\Inventory\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Item;

class ItemTransformer extends TransformerAbstract
{
    /*
     * Transform item
     * 
     * @param object $item
     * @return array
     */
    public function transform(Item $item)
    {
        return [
            'item_id'       => $item->item_id,
            'upc'           => $item->upc,
            'description'   => $item->description,
            'lot'           => object_get($item, 'lot', null),
            'sku'           => $item->sku,
            'size'          => $item->size,
            'color'         => $item->color,
            'uom_id'        => $item->uom_id,
            'uom_code'      => object_get($item, 'uom.sys_uom_code', null),
            'uom_name'      => object_get($item, 'uom.sys_uom_name', null),
            'pack'          => $item->pack,
            'length'        => $item->length,
            'width'         => $item->width,
            'height'        => $item->height,
            'weight'        => $item->weight,
            'volume'        => $item->volume,
            'cus_id'        => $item->cus_id,
            'customer_name' => object_get($item, 'customer.cus_name', null),
            'cus_upc'       => $item->cus_upc,
            'status'        => $item->status,
            'condition'     => $item->condition,
            'created_at'    => $item->created_at,
            'updated_at'    => $item->updated_at,
            'deleted_at'    => $item->deleted_at,
            'ctn_pack_size'     => $this->getInfoFromCarton($item,'pack'),
            'piece_remain'     => $this->getInfoFromCarton($item,'remain')
        ];

    }

    function getInfoFromCarton($item,$flag) {
        foreach ($item->carton as $obj) {
            return $flag === 'pack' ? $obj->ctn_pack_size : $obj->piece_remain;
        }
    }
}
