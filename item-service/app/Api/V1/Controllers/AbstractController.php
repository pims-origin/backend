<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthorizationService;
use App\Api\V1\Models\BaseService;
use Laravel\Lumen\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;
use Illuminate\Http\Response as LumenResponse;
use Wms2\UserInfo\Data;

/**
 * Class BaseController
 * @package App\Api\V1\Controllers
 *
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="seldat.seldat-wms20.menu.dev",
 *     basePath="/",
 *     definitions={},
 *     @SWG\Info(
 *         version="WMS 2.0.0",
 *         title="Dynamic Menu APIs",
 *         description="Total Dynamic menu APIs",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="dai.ho@seldatinc.com"
 *         ),
 *         @SWG\License(
 *             name="WMS2.0",
 *             url="seldatinc.com"
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="data",
 *         type="object",
 *         properties={
 *             @SWG\Property(property="menu_group_id", type="integer"),
 *             @SWG\Property(property="name", type="string"),
 *             @SWG\Property(property="description", type="string"),
 *         },
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */
abstract class AbstractController extends Controller
{
    protected $userId;

    public function __construct(IRequest $request, BaseService $service = null)
    {
        $token = $request->getHeader('Authorization');
        if(empty($token[0])) {
            throw new UnauthorizedHttpException('Unauthorized');
        }

        $result = Data::getCurrentUserId();
        /*$service->getUserIdByToken($token[0]);
        if (! $result) {
            throw new UnauthorizedHttpException('Unauthorized');
        }*/

        $this->userId = $result;
    }
    /**
     * Check permission for every action request
     *
     * @param IRequest $request
     * @param $permission
     * @param array $data
     */
    public function checkPermission(IRequest $request, $permission, $data = [])
    {
        $token = $request->getHeader('Authorization');
        if(empty($token[0])) {
            throw new UnauthorizedHttpException('Token is empty');
        }
        $client = new AuthorizationService($request);
        $client->check($permission, $data);
    }

    /**
     * @param IResponse $response
     * @return LumenResponse
     */
    protected function convertResponse(IResponse $response)
    {
        $luResponse = new LumenResponse((string)$response->getBody(), $response->getStatusCode(), []);
        return $luResponse;
    }
}
