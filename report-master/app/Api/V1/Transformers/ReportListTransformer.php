<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportConfigs;
use League\Fractal\TransformerAbstract;

class ReportListTransformer extends TransformerAbstract
{
    public function transform(ReportConfigs $reportConfigs)
    {

         return [
             'id'		=>  object_get($reportConfigs, 'qualifier', null),
             'name'		=>  object_get($reportConfigs, 'name', null),
             'level'	=>  object_get($reportConfigs, 'level', null),
        ];
    }

}
