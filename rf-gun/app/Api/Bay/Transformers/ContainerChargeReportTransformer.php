<?php


namespace App\Api\Bay\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\ContainerCharge;

class ContainerChargeTransformer extends TransformerAbstract
{
    //return result
    public function transform(ContainerCharge $ctnrCharge)
    {
        return [
            'cus_id'      => object_get($ctnrCharge, 'cus_id', null),
            'cus_name'    => object_get($ctnrCharge, 'cus_name', null),
            'whs_id'      => object_get($ctnrCharge, 'whs_id', null),
            'ctnr_id'     => object_get($ctnrCharge, 'ctnr_id', null),
            'ctnr_num'    => object_get($ctnrCharge, 'ctnr_num', null),
            'start'       => object_get($ctnrCharge, 'start', null),
            'end'         => object_get($ctnrCharge, 'end', null),
            'duration'    => object_get($ctnrCharge, 'duration', null),
            'cc_sts'      => object_get($ctnrCharge, 'cc_sts', null),
            'cc_sts_name' => object_get($ctnrCharge, 'cc_sts', null),
            'by_id'       => object_get($ctnrCharge, 'by_id', null),
            'by_name'     => object_get($ctnrCharge, 'by_name', null)
        ];
    }

}
