<?php

namespace Seldat\Wms2\Models;

class BlockRsn extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'block_rsn';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'block_rsn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'reason_num',
        'block_rsn_name',
        'block_rsn_des',
        'sts'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'whs_id', 'whs_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }
}
