<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/13/2018
 * Time: 3:44 PM
 */
namespace App\Modules\Warehouses\Validators;
use App\Modules\Warehouses\Eloquent\Warehouse;
use Validator;

class WarehouseValidator
{

    public function checkWhsExist($whs_id) {
        $whsModel  = new Warehouse();
        $whs = $whsModel->where('whs_id',$whs_id)
            ->where('deleted' , 0)->first();
        if(!$whs) {
            throw new \Exception(json_encode(['title' =>'Warehouse is not existed.', 'detail' => 'Please try']));
        }
    }
}