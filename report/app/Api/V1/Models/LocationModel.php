<?php

namespace App\Api\V1\Models;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Location;

class LocationModel extends AbstractModel
{
    public function __construct(Location $model = NULL)
    {
        $this->model = ($model) ?: new Location();
    }

    public function getCountLocationHasPallet($data) {
        return $this->model
            ->join('customer_zone', 'location.loc_zone_id', '=', 'customer_zone.zone_id')
            ->where('customer_zone.cus_id', $data['cus_id'])
            ->where('location.loc_whs_id', $data['whs_id'])
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('pallet')
                    ->whereRaw('pallet.loc_id = location.loc_id');
            })
            ->count();
    }
}
