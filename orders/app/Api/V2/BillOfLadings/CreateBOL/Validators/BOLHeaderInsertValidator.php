<?php

namespace App\Api\V2\BillOfLadings\CreateBOL\Validators;

/**
 * Class BOLHeaderInsertValidator
 *
 * @package App\Api\V1\Validators
 */
class BOLHeaderInsertValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'cus_id'                           => 'required|integer|exists:customer',
            'ship_ready'                       => 'required',
            'ship_to_addr'                     => 'required',
            'bol_label'                        => 'required',
            'ship_to_addr'                     => 'required',
            'third_party.special_instructions' => 'required',
            'carrier.carrier_name'             => 'required',
            'carrier.delivery_service'         => 'required',
            'carrier.rd3_party_account'        => 'required',
            'carrier.commodity_description'    => 'required',
            'carrier.trailer_load'             => 'required',
            'carrier.freight_counted'          => 'required',
            'carrier.freight_charge_terms'     => 'required',
            'carrier.fee_terms'                => 'required',
        ];
    }
}
