<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Illuminate\Support\Facades\DB;

$factory->define(Seldat\Wms2\Models\User::class, function ($faker) {
    return [
        'name'  => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(Seldat\Wms2\Models\Customer::class, function ($faker) {
    return [
        'cus_name'   => $faker->name,
        'cus_code'   => $faker->name,
        'cus_status' => 'AC',
    ];
});

$factory->define(Seldat\Wms2\Models\CustomerZone::class, function ($faker) {
    return [
        'zone_id' => function () {
            return factory(\Seldat\Wms2\Models\Zone::class)->create()->zone_id;
        },
        'cus_id'  => function () {
            return factory(\Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        }
    ];
});

$factory->define(Seldat\Wms2\Models\Warehouse::class, function ($faker) {
    return [
        'whs_name'       => $faker->name,
        'whs_code'       => $faker->name,
        'whs_status'     => 'AC',
        'whs_short_name' => $faker->name,
    ];
});

$factory->define(Seldat\Wms2\Models\Container::class, function ($faker) {
    return [
        'ctnr_num'  => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+[A-Z]{2,4}'),
        'ctnr_note' => $faker->name,
    ];
});

$factory->define(Seldat\Wms2\Models\SystemUom::class, function ($faker) {
    return [
        'sys_uom_code' => $faker->randomLetter . $faker->randomLetter . $faker->randomLetter . $faker->randomLetter .
            $faker->randomLetter,
    ];
});


$factory->define(Seldat\Wms2\Models\Item::class, function ($faker) {
    return [
        'sku'       => $faker->randomLetter,
        'size'      => $faker->randomNumber,
        'color'     => $faker->randomNumber,
        'uom_id'    => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        },
        'width'     => $faker->randomNumber,
        'pack'      => $faker->randomNumber,
        'length'    => $faker->randomNumber,
        'height'    => $faker->randomNumber,
        'weight'    => $faker->randomNumber,
        'status'    => 'AC'
    ];
});

$factory->define(Seldat\Wms2\Models\AsnHdr::class, function ($faker) {
    return [
        'asn_hdr_seq'    => $faker->randomNumber,
        'asn_hdr_ept_dt' => date('Y-m-d'),
        'asn_hdr_num'    => uniqid(),
        'asn_hdr_ref'    => $faker->randomLetter,
        'cus_id'         => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'whs_id'         => function () {

            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'asn_sts'        => config('constants.asn_status.NEW'),
        'sys_mea_code'   => 'CM',
        'asn_hdr_des'    => $faker->randomLetter
    ];
});

$factory->define(Seldat\Wms2\Models\AsnDtl::class, function ($faker) {
    return [
        'asn_hdr_id'      => function () {
            return factory(Seldat\Wms2\Models\AsnHdr::class)->create()->asn_hdr_id;
        },
        'ctnr_id'         => function () {
            return factory(Seldat\Wms2\Models\Container::class)->create()->ctnr_id;
        },
        'item_id'          => function () {
            return factory(Seldat\Wms2\Models\Item::class)->create()->item_id;
        },
        'asn_dtl_po'      => $faker->randomNumber,
        'asn_dtl_po_dt'   => $faker->randomLetter,
        'asn_dtl_ctn_ttl' => 3,
        'asn_dtl_crs_doc' => $faker->randomNumber,
        'asn_dtl_des'     => $faker->randomLetter,
        'uom_id'          => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        },
    ];
});

$factory->define(Seldat\Wms2\Models\GoodsReceipt::class, function ($faker) {
    $asnHeader = factory(Seldat\Wms2\Models\AsnHdr::class)->create();
    $containerId = factory(Seldat\Wms2\Models\Container::class)->create()->ctnr_id;

    return [
        'ctnr_id'       => $containerId,
        'asn_hdr_id'    => $asnHeader->asn_hdr_id,
        'gr_hdr_seq'    => $faker->randomNumber,
        'gr_hdr_ept_dt' => $faker->randomNumber,
        'gr_hdr_num'    => $faker->randomLetter,
        'cus_id'        => $asnHeader->cus_id,
        'whs_id'        => $asnHeader->whs_id,
        'gr_in_note'    => 'Gr_in_note',
        'gr_ex_note'    => 'Gr_ex_note',
        'gr_sts'        => 'RG',
    ];
});

$factory->define(Seldat\Wms2\Models\GoodsReceiptDetail::class, function ($faker) {
    $asnHeader = factory(Seldat\Wms2\Models\AsnHdr::class)->create();
    $containerId = factory(Seldat\Wms2\Models\Container::class)->create()->ctnr_id;
    $asnDtl = factory(Seldat\Wms2\Models\AsnDtl::class)->create([
        'asn_hdr_id' => $asnHeader->asn_hdr_id,
        'ctnr_id'    => $containerId
    ]);

    $goodsReceiptId = factory(Seldat\Wms2\Models\GoodsReceipt::class)->create([
        'ctnr_id'    => $containerId,
        'asn_hdr_id' => $asnHeader->asn_hdr_id,
        'cus_id'     => $asnHeader->cus_id,
        'whs_id'     => $asnHeader->whs_id
    ])->gr_hdr_id;

    return [
        'asn_dtl_id'         => $asnDtl->asn_dtl_id,
        'gr_hdr_id'          => $goodsReceiptId,
        'gr_dtl_ept_ctn_ttl' => $asnDtl->asn_dtl_ctn_ttl,
        'gr_dtl_act_ctn_ttl' => $asnDtl->asn_dtl_ctn_ttl,
        'gr_dtl_cus_note'    => $faker->randomLetter,
        'gr_dtl_disc'        => 1,
        'gr_dtl_is_dmg'      => 1
    ];
});

$factory->define(Seldat\Wms2\Models\Zone::class, function ($faker) {
    return [
        'zone_name'        => $faker->name,
        'zone_code'        => uniqid(),
        'zone_whs_id'      => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'zone_type_id'     => function () {
            return factory(Seldat\Wms2\Models\ZoneType::class)->create()->zone_type_id;
        },
        'zone_description' => $faker->text,
        'zone_num_of_loc'  => 5, // need to replace,
        'zone_min_count'   => 1,
        'zone_max_count'   => 999
    ];
});

$factory->define(Seldat\Wms2\Models\ZoneType::class, function ($faker) {
    return [
        'zone_type_name' => $faker->name,
        'zone_type_code' => uniqid(),
        'zone_type_desc' => $faker->text,
    ];
});
$factory->define(Seldat\Wms2\Models\Location::class, function ($faker) {
    return [
        'loc_whs_id'           => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'loc_alternative_name' => $faker->name,
        'loc_code'             => uniqid(),
        'loc_zone_id'          => function () {
            return factory(Seldat\Wms2\Models\Zone::class)->create()->zone_id;
        },
        'loc_type_id'          => function () {
            return factory(Seldat\Wms2\Models\LocationType::class)->create()->loc_type_id;
        },
        'loc_sts_code'         => 'AC',
    ];
});

$factory->define(Seldat\Wms2\Models\LocationType::class, function ($faker) {
    return [
        'loc_type_name' => $faker->name,
        'loc_type_code' => uniqid(),
        'loc_type_desc' => $faker->text,
    ];
});

$factory->define(Seldat\Wms2\Models\Pallet::class, function ($faker) {
    return [
        'whs_id'   => function () {
            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'loc_id'   => function () {
            return factory(Seldat\Wms2\Models\Location::class)->create()->loc_id;
        },
        'loc_name' => $faker->name,
        'loc_code' => uniqid(),
        'plt_num' => $faker->text,
    ];
});

$factory->define(\Seldat\Wms2\Models\DamageType::class, function ($faker) {
    return [
        'dmg_code' => $faker->randomLetter . $faker->randomLetter . $faker->randomLetter,
        'dmg_name' => 'WET' . $faker->randomLetter . $faker->randomNumber . $faker->randomLetter,
        'dmg_des'  => $faker->name,
    ];
});

$factory->define(\Seldat\Wms2\Models\Carton::class, function ($faker) {
    $asnHeader = factory(Seldat\Wms2\Models\AsnHdr::class)->create();
    $containerId = factory(Seldat\Wms2\Models\Container::class)->create()->ctnr_id;

    $itemId = factory(Seldat\Wms2\Models\Item::class)->create()->item_id;

    $asnDtl = factory(Seldat\Wms2\Models\AsnDtl::class)->create([
        'asn_hdr_id' => $asnHeader->asn_hdr_id,
        'ctnr_id'    => $containerId,
        'item_id'     => $itemId
    ]);

    $goodsReceiptId = factory(Seldat\Wms2\Models\GoodsReceipt::class)->create([
        'ctnr_id'    => $containerId,
        'asn_hdr_id' => $asnHeader->asn_hdr_id,
        'cus_id'     => $asnHeader->cus_id,
        'whs_id'     => $asnHeader->whs_id,
        'gr_sts'     => 'RD'
    ])->gr_hdr_id;

    $grDtl = factory(Seldat\Wms2\Models\GoodsReceiptDetail::class)->create([
        'asn_dtl_id'         => $asnDtl->asn_dtl_id,
        'gr_hdr_id'          => $goodsReceiptId,
        'gr_dtl_ept_ctn_ttl' => $asnDtl->asn_dtl_ctn_ttl,
        'gr_dtl_act_ctn_ttl' => ($asnDtl->asn_dtl_ctn_ttl) - 2
    ]);
    return [
        'asn_dtl_id'    => $grDtl->asn_dtl_id,
        'item_id'       => $asnDtl->item_id,
        'cus_id'        => $asnHeader->cus_id,
        'ctn_num'       => uniqid(),
        'ctn_sts'       => DB::table('ctn_status')->select('status')->first()['status'],
        'ctn_uom_id'    => function () {
            return factory(Seldat\Wms2\Models\SystemUom::class)->create()->sys_uom_id;
        },
        'ctn_pack_size' => $faker->randomNumber,
        'loc_id'        => factory(Seldat\Wms2\Models\Location::class)->create()->loc_id,
        'plt_id'        => factory(Seldat\Wms2\Models\Pallet::class)->create()->plt_id,
    ];
});


$factory->define(\Seldat\Wms2\Models\InventorySummary::class, function ($faker) {
    $item = factory(Seldat\Wms2\Models\Item::class)->create();
    return [
        'item_id'        => $item->item_id,
        'ttl'           => 1010,
        'cus_id'         => function () {
            return factory(Seldat\Wms2\Models\Customer::class)->create()->cus_id;
        },
        'whs_id'         => function () {

            return factory(Seldat\Wms2\Models\Warehouse::class)->create()->whs_id;
        },
        'color'         => array_get($item, 'color', ''),
        'size'          => array_get($item, 'size', ''),
        'lot'           => array_get($item, 'lot', ''),
        'allocated_qty' => 0,
        'dmg_qty'       => 10,
        'sku'           => array_get($item, 'sku', ''),
        'avail'         => 1000,
    ];
});