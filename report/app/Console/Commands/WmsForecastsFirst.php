<?php

namespace App\Console\Commands;

use App\Api\V1\Models\DailyInventoryModel;
use App\Api\V1\Models\ForecastIndicatorModel;
use App\Api\V1\Models\ForecastModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Traits\ForecastControllerTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WmsForecastsFirst extends AbstractCommand
{
    use ForecastControllerTrait;
    protected $signature = 'wms-forecast-first {dateFrom} {dateTo}';
    protected $description = 'Update Forecast report first';
    protected $goodReceiptModel;
    protected $dailyInventoryModel;
    protected $odrHdrModel;
    protected $locationModel;
    protected $forecastModel;

    public function __construct()
    {
        parent::__construct();
        $this->goodReceiptModel = new GoodsReceiptModel();
        $this->dailyInventoryModel = new DailyInventoryModel();
        $this->odrHdrModel = new OrderHdrModel();
        $this->locationModel = new LocationModel();
        $this->forecastModel = new ForecastModel();
    }

    public function handle()
    {
        $dateTo = date('Y-m-t', strtotime($this->argument('dateTo')));
        $dateFrom = date('Y-m-01', strtotime($this->argument('dateFrom')));

        $arrDates = [];
        for ($i = $dateFrom; $i <= $dateTo; $i = date('Y-m', strtotime('+1 month', strtotime($i)))) {
            $arrDates[$i] = $i;
        }

        DB::setFetchMode(\PDO::FETCH_ASSOC);

        try {

            DB::beginTransaction();

            $customersWhs = DB::table('customer_warehouse')
                ->where('whs_id', 1)
                ->whereIn('cus_id', [1,2,3,4,5])
                ->where('deleted', 0)->get();
            $cus_ids = array_pluck($customersWhs, 'cus_id', 'cus_id');
            $whs_ids = array_pluck($customersWhs, 'whs_id', 'whs_id');

            //$arrInserts = [];

            //$arrYears = [];
            foreach ($arrDates as $date) {

                $year = date('Y', strtotime($date));
                $month = date('m', strtotime($date));

                //$arrYears[$year] = $year;

                // Delete Old data
                /*DB::table('forecast')
                    ->whereIn('whs_id', $whs_ids)
                    ->whereIn('cus_id', $cus_ids)
                    ->where('fc_month', $month)
                    ->where('fc_year', $year)
                    ->delete();*/

                $forecastIndicatorModel = new ForecastIndicatorModel();
                $fcIndicators = $forecastIndicatorModel->getModel()->get();
                $groupedForecasts = [];
                foreach ($customersWhs as $cusWhs) {
                    foreach ($fcIndicators as $fcIndicator) {
                        $fcIndicator->cus_id = $cusWhs['cus_id'];
                        $fcIndicator->whs_id = $cusWhs['whs_id'];
                        $fcIndicator->fc_ib = 0;
                        $fcIndicator->fc_ib_act = 0;
                        $fcIndicator->fc_ob = 0;
                        $fcIndicator->fc_ob_act = 0;
                        $fcIndicator->fc_st = 0;
                        $fcIndicator->fc_st_act = 0;
                        $fcIndicator->fc_month = $month;
                        $fcIndicator->fc_year = $year;
                        $groupedForecasts[$cusWhs['whs_id'] . $cusWhs['cus_id'] . $fcIndicator->fc_ind_id][] = $fcIndicator;
                    }
                }

                foreach ($groupedForecasts as &$groupedForecast) {
                    $groupedForecast = collect($groupedForecast)->keyBy('fc_ind_code');
                    $this->calculateActual($groupedForecast, date('Y-m-d', strtotime($date)));
                }

                $groupedForecasts = collect($groupedForecasts)->flatten();

                foreach ($groupedForecasts as $key => $groupedForecast) {
                    /*$arrInserts[$key] = [
                        "fc_ind_id" => $groupedForecast->fc_ind_id,
                        "created_at" => time(),
                        "created_by" => 1,
                        "updated_by" => 1,
                        "deleted_at" => 915148800,
                        "deleted" => 0,
                        "cus_id" => $groupedForecast->cus_id,
                        "whs_id" => $groupedForecast->whs_id,
                        "fc_ib" => $groupedForecast->fc_ib,
                        "fc_ib_act" => $groupedForecast->fc_ib_act,
                        "fc_ob" => $groupedForecast->fc_ob,
                        "fc_ob_act" => $groupedForecast->fc_ob_act,
                        "fc_st" => $groupedForecast->fc_st,
                        "fc_st_act" => $groupedForecast->fc_st_act,
                        "fc_month" => (int)$groupedForecast->fc_month,
                        "fc_year" => $groupedForecast->fc_year
                    ];*/
                    DB::table('forecast')->where([
                        'fc_ind_id' => $groupedForecast->fc_ind_id,
                        'cus_id' => $groupedForecast->cus_id,
                        'whs_id' => $groupedForecast->whs_id,
                        'fc_month' => $groupedForecast->fc_month,
                        'fc_year' => $groupedForecast->fc_year
                    ])
                    ->update([
                        'fc_ib_act' => (int)ceil($groupedForecast->fc_ib_act),
                        'fc_ob_act' => (int)ceil($groupedForecast->fc_ob_act),
                        'fc_st_act' => (int)ceil($groupedForecast->fc_st_act)
                    ]);
                }
            }

            /*if (!empty($arrInserts)) {
                foreach (array_chunk($arrInserts, 300) as $data) {
                    DB::table('forecast')->insert($data);
                }
            }

            $fcYearAvgs = DB::table('forecast')
                ->select(
                    DB::raw('avg(fc_ib_act) AS fc_ib'),
                    DB::raw('avg(fc_ob_act) AS fc_ob'),
                    DB::raw('avg(fc_st_act) AS fc_st'),
                    'whs_id',
                    'cus_id',
                    'fc_year'
                )
                ->whereIn('fc_year', $arrYears)
                ->groupBy('whs_id', 'cus_id', 'fc_year')
                ->get();

            foreach ($fcYearAvgs as $fcYearAvg) {
                DB::table('forecast')
                    ->where([
                        'cus_id' => $fcYearAvg['cus_id'],
                        'fc_year' => $fcYearAvg['fc_year'],
                        'whs_id' => $fcYearAvg['whs_id'],
                    ])
                    ->update([
                        'fc_ib' => ceil($fcYearAvg['fc_ib']),
                        'fc_ob' => ceil($fcYearAvg['fc_ob']),
                        'fc_st' => ceil($fcYearAvg['fc_st'])
                    ]);
            }*/

            DB::commit();

            $this->logSuccess("WMS Forecasts First: successfully, from $dateFrom to $dateTo" );

        } catch (\PDOException $e) {
            DB::rollback();
            $this->logPdo($e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            $this->logPdo($e->getMessage());
        }

    }
}
