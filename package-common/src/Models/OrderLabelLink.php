<?php
namespace Seldat\Wms2\Models;

class OrderLabelLink extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'odr_lbl_link';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_lbl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'odr_lbl_id',
        'whs_id',
        'cus_id',
        'odr_hdr_id',
        'odr_hdr_num',
        'ucc128',
        'qty',
    ];

    /**
     * @return mixed
     */
    public function orderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_hdr_id');
    }

    public function Customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function Warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

}
