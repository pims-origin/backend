<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 5/24/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\PalletAssignment\Models;

use Seldat\Wms2\Models\EventLookup;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class EventLookupModel extends AbstractModel
{
    /**
     * @param EventLookup $model
     */
    public function __construct(EventLookup $model = null)
    {
        $this->model = ($model) ?: new EventLookup();
    }
}