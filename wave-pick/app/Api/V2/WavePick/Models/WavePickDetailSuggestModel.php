<?php

namespace App\Api\V2\WavePick\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WaveDtlLoc;
use Wms2\UserInfo\Data;

class WavePickDetailSuggestModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WaveDtlLoc $model = null)
    {
        $this->model = ($model) ?: new WaveDtlLoc();
    }

    /**
     * @param $whsId
     * @param $wv_id
     *
     * @return mixed
     */
    public function loadData($whsId, $wv_id)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        return DB::table('wv_dtl_suggest')->select([
            'wv_dtl_suggest.wv_id',
            'wv_dtl_suggest.wv_dtl_id',
            'wv_hdr.wv_num',
            'wv_dtl_suggest.item_id',
            'wv_dtl_suggest.sku',
            'wv_dtl_suggest.color',
            'wv_dtl_suggest.size',
            'wv_dtl_suggest.lot',
            'wv_dtl_suggest.pack',
            'wv_dtl_suggest.ctns',
            'wv_dtl_suggest.qty',
            'wv_dtl_suggest.loc_id',
            'wv_dtl_suggest.loc_code',
            'wv_dtl_suggest.cus_id',
            'wv_dtl_suggest.uom',
            'wv_dtl_suggest.description'
        ])
            ->join('wv_hdr', 'wv_hdr.wv_id', '=', 'wv_dtl_suggest.wv_id')
            ->where('wv_dtl_suggest.wv_id', '=', $wv_id)
            ->where('wv_dtl_suggest.whs_id', '=', $whsId)
            ->orderBy('item_id', 'lot')
            ->get();
    }


}
