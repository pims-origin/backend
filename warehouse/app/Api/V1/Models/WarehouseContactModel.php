<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:32
 */

namespace App\Api\V1\Models;


use Illuminate\Database\Query\Builder;
use Seldat\Wms2\Models\WarehouseContact;

class WarehouseContactModel extends AbstractModel
{
    /**
     * @var Builder
     */
    protected $model;

    /**
     * EloquentContact constructor.
     *
     * @param WarehouseContact $model
     */
    public function __construct(WarehouseContact $model = null)
    {
        $this->model = ($model) ?: new WarehouseContact();
    }

    /**
     * @param int $warehouseId
     * @param int|array $contactId
     *
     * @return int
     */
    public function deleteWarehouseContact($warehouseId, $contactId)
    {
        $contactId = is_array($contactId) ? $contactId : [$contactId];

        return $this->model
            ->where('whs_con_whs_id', $warehouseId)
            ->whereIn('whs_con_id', $contactId)
            ->delete();
    }

    public function getByWarehouseId($warehouseId)
    {
        return $this->allBy('whs_con_whs_id', $warehouseId);
    }
}
