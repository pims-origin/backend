<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycleDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('cycle_dtl')) {
            Schema::create('cycle_dtl', function (Blueprint $table) {
                $table->increments('cycle_dtl_id');
                $table->integer('cycle_hdr_id');
                $table->integer('whs_id');
                $table->integer('cus_id');
                $table->integer('item_id');
                $table->string('sku', 50);
                $table->string('size', 30);
                $table->string('color', 30);
                $table->integer('pack');
                $table->integer('remain');
                $table->integer('sys_qty');
                $table->integer('act_qty')->nullable();
                $table->integer('sys_loc_id');
                $table->string('sys_loc_name', 50);
                $table->integer('act_loc_id')->nullable();
                $table->string('act_loc_name', 30)->nullable();
                $table->char('cycle_dtl_sts', 2);
                $table->integer('created_by');
                $table->integer('updated_by');
                $table->integer('created_at');
                $table->integer('updated_at');
                $table->char('sts', 1);
                $table->tinyInteger('deleted');
                $table->integer('deleted_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cycle_dtl')) {
            Schema::drop('cycle_dtl');
        }
    }
}
