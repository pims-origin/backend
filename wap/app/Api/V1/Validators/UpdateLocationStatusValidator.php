<?php

namespace App\Api\V1\Validators;


class UpdateLocationStatusValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_code' => 'required'
        ];
    }
}
