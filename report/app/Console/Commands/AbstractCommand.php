<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

abstract class AbstractCommand extends Command
{
    public function __construct()
    {
        parent::__construct();
    }

    public function logPdo($error)
    {
        $date = date('Y-m-d');
        $error = '[' . date('Y-m-d H:i:s')  . '] ' .$error;
        file_put_contents(storage_path("logs/log_pdo_{$date}.log"), "\n{$error}", FILE_APPEND);
        // Save logs, Insert Successfully For One Request
    }

    public function logSuccess($error)
    {
        $date = date('Y-m-d');
        $error = '[' . date('Y-m-d H:i:s')  . '] ' .$error;
        file_put_contents(storage_path("logs/log_success_{$date}.log"), "\n{$error}", FILE_APPEND);
        // Save logs, Insert Successfully For One Request
        //Log::info($error);
    }
}
