<?php

namespace App\Api\V2\PalletAssignment\Models;


use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Utils\SelStr;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * @param EventTracking $model
     */
    public function __construct(EventTracking $model = null)
    {
        $this->model = $model ?: new EventTracking();
    }


}
