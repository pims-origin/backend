<?php
namespace Seldat\Wms2\Models;

/**
 * Class OrderSoldTo
 *
 * @property mixed odr_id
 * @property mixed tax_vat
 * @property mixed contact_name
 * @property mixed telephone_no
 * @property mixed com_name_address
 * @property mixed state
 * @property mixed zip
 * @property mixed country
 *
 * @package Seldat\Wms2\Models
 */
class OrderSoldTo extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'odr_sold_to';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $fillable = [
		"odr_id",
		"name",
		"street_address",
		"city",
		"zip",
		"country",
		"contact_name",
    ];
}
