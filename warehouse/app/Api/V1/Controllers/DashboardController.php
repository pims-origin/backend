<?php
/**
 * Created by PhpStorm.
 *
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\CycleCountNotificationModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\GoodReceiptHdrModel;
use App\Api\V1\Models\WaveHdrModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\ZoneModel;
use App\Api\V1\Transformers\CycleCountStatisticDashboardTransformer;
use App\Api\V1\Transformers\WavePickStatisticDashboardTransformer;
use App\Api\V1\Transformers\ZoneCapacityTransformer;
use App\Api\V1\Transformers\RoomCapacityTransformer;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;

/**
 * Class DashboardController
 *
 * @package App\Api\V1\Controllers
 */
class DashboardController extends AbstractController
{

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var AsnHdrModel
     */
    protected $asnHdrModel;

    /**
     * @var GoodReceiptHdrModel
     */
    protected $goodReceiptHdrModel;

    /**
     * @var WaveHdrModel
     */
    protected $waveHdrModel;

    protected $locationModel;

    /**
     * @var ZoneModel
     */
    protected $zoneModel;

    protected $cycleCountNotificationModel;

    protected $cycleHdrModel;

    /**
     * @param AsnHdrModel $asnHdrModel
     * @param OrderHdrModel $orderHdrModel
     * @param GoodReceiptHdrModel $goodReceiptHdrModel
     * @param WaveHdrModel $waveHdrModel
     */
    public function __construct(
        AsnHdrModel $asnHdrModel,
        OrderHdrModel $orderHdrModel,
        GoodReceiptHdrModel $goodReceiptHdrModel,
        LocationModel $locationModel,
        WaveHdrModel $waveHdrModel,
        ZoneModel $zoneModel,
        CycleCountNotificationModel $cycleCountNotificationModel,
        CycleHdrModel   $cycleHdrModel
    ) {
        $this->asnHdrModel = $asnHdrModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->goodReceiptHdrModel = $goodReceiptHdrModel;
        $this->locationModel = $locationModel;
        $this->waveHdrModel = $waveHdrModel;
        $this->zoneModel = $zoneModel;
        $this->cycleCountNotificationModel = $cycleCountNotificationModel;
        $this->cycleHdrModel = $cycleHdrModel;
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function asnDashboard(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboard = $this->asnHdrModel->asnDashboard($input);

            return ['data' => $dashboard];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function odrDashboard(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboardOdrs = $this->orderHdrModel->odrDashboard($input);

            return ['data' => $dashboardOdrs[0]];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function inprocessDashboard(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboardInprocess = $this->orderHdrModel->inprocessDashboard($input);

            return ['data' => $dashboardInprocess[0]];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function asnAdminDashboard(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboard = $this->asnHdrModel->asnAdminDashboard($input);

            return ['data' => $dashboard];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function odrAdminDashboard(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboardOdrs = $this->orderHdrModel->odrAdminDashboard($input);
            $data = array_map(
                function($value) { return (int)$value; },
                $dashboardOdrs[0]
            );

            return ['data' => $data];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function inprocessAdminDashboard(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboardInprocess = $this->orderHdrModel->inprocessAdminDashboard($input);
            $data = array_map(
                function($value) { return (int)$value; },
                $dashboardInprocess[0]
            );

            return ['data' => $data];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function putawayDashboard(Request $request)
    {
        $input = $request->getQueryParams();
        try {

            $result = $this->goodReceiptHdrModel->putawayDashBoard($input);

            return $this->response->array($result);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function putawayAdminDashboard(Request $request)
    {
        $input = $request->getQueryParams();
        try {

            $result = $this->goodReceiptHdrModel->putawayAdminDashBoard($input);

            return $this->response->array($result);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function wavepickDashboard(Request $request)
    {
        $input = $request->getQueryParams();
        try {

            $wavepick = $this->waveHdrModel->getWaveDashBoard($input, ['details']);

            $result = ["wavepick" => 0, "picking" => 0, "picked" => 0];
            if (!empty($wavepick)) {
                foreach ($wavepick as $key => $allWave) {
                    $total = count($allWave['details']);
                    $picking = 0;
                    foreach ($allWave['details'] as $detail) {
                        if (empty($detail['act_piece_qty'])) {
                            $picking++;
                        }
                    }
                    $result['wavepick'] = $result['wavepick'] + $total;
                    $result['picked'] = $result['picked'] + $total - $picking;
                    $result['picking'] = $result['picking'] + $picking;
                }

            }

            return $this->response->array($result);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function goodReceiptDashboard(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $dashboard = $this->goodReceiptHdrModel->grDashBoard($input);

            return ['data' => $dashboard];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function putawayReceivedDashboard(Request $request)
    {
        $input = $request->getQueryParams();
        try {

            $result = $this->goodReceiptHdrModel->putawayReceivedDashBoard($input);

            return $this->response->array($result);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function OccupancyPCTDashboard(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboard = $this->locationModel->OccupancyPCTDashboard($input);
            return ['data' => $dashboard];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function occupancyPCTDashboardByCus(Request $request)
    {

        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dashboard = $this->locationModel->occupancyPCTDashboardByCus($input);
            return ['data' => $dashboard];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function occupancyPCTDashboardStatus(Request $request)
    {
        $input = $request->getQueryParams();

        try{
            $data = $this->locationModel->occupancyPCTDashboardStatus($input);
            return ['data' => $data];
        } catch (\Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
        return $input;
    }

    /**
     * @param WavePickStatisticDashboardTransformer $wavePickStatisticDashboardTransformer
     *
     * @return Response|void
     */

    public function wavePickStatisticDashboard(
        Request $request,
        WavePickStatisticDashboardTransformer $wavePickStatisticDashboardTransformer
    ) {
        $input = $request->getQueryParams();
        try {
            $wvHdrInfo = $this->waveHdrModel->wavePickStatisticDashboard($input, [], array_get($input, 'limit'));

            return $this->response->paginator($wvHdrInfo, $wavePickStatisticDashboardTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param CycleCountStatisticDashboardTransformer $cycleCountStatisticDashboardTransformer
     *
     * @return Response|void
     */
    public function cycleCountStatisticDashboard(
        Request $request,
        CycleCountStatisticDashboardTransformer $cycleCountStatisticDashboardTransformer
    ) {
        $input = $request->getQueryParams();
        try {
            $ccHdrInfo = $this->cycleHdrModel->cycleCountStatisticDashboard($input, [], array_get($input, 'limit'));

            $arrItems = [];
            if ($ccHdrInfo){
                $ccHdrInfoArrPage=$ccHdrInfo->toArray();
                $ccHdrInfoArr=array_get($ccHdrInfo->toArray(),'data', '');
                foreach($ccHdrInfoArr as $ccHdrInf){

                    //get Used Location
                    $cycleHdrId = array_get($ccHdrInf, 'cycle_hdr_id', '');
                    $usedLocInfo = $this->cycleHdrModel->countCycleCountUsedLoc($cycleHdrId);
                    $usedLocTotal = array_get($usedLocInfo, 'used_loc_total', 0);

                    //get day_aging
                    $dueDt = array_get($ccHdrInf, 'due_dt', '');
                    $currentDate=date('Y-m-d', time());
                    if ($dueDt > $currentDate)
                    {
                        $compare_date = $dueDt;
                    }else{
                        $compare_date = $currentDate;
                    }

                    $completedAt = array_get($ccHdrInf, "completed_at", null) ? date('Y-m-d', array_get
                    ($ccHdrInf, "completed_at", null)) : '';
                    $createAt = array_get($ccHdrInf, "created_at", null) ? date('Y-m-d', array_get
                    ($ccHdrInf, "created_at", null)) : '';

                    $diff = abs(strtotime($compare_date) - strtotime($createAt));

                    $years = floor($diff / (365 * 60 * 60 * 24));
                    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                    $dayNum = floor(($diff) / (60 * 60 * 24));
                    $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                    $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
                    $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));

                    //get status name
                    $statusKey = array_get($ccHdrInf, 'cycle_sts', null);
                    $statusName = $this->cycleHdrModel->getCycleStatus($statusKey);

                    $arrItems[] = [
                        'cycle_hdr_id'   => array_get($ccHdrInf, 'cycle_hdr_id', null),
                        'cycle_num'      => array_get($ccHdrInf, 'cycle_num', null),
                        'cycle_type'     => array_get($ccHdrInf, 'cycle_type', null),
                        'cycle_sts'      => $statusName,
                        'loc_total'      => array_get($ccHdrInf, 'loc_total', 'NA'),
                        'used_loc_total' => $usedLocTotal,

                        'day_aging'      => !empty($dayNum) ? $dayNum : 1,

                        "due_dt"       => array_get($ccHdrInf, "due_dt", null),
                        "completed_at" => array_get($ccHdrInf, "completed_at", null) ? date('Y-m-d', array_get
                        ($ccHdrInf, "completed_at", null)) : '',
                        "created_at" => array_get($ccHdrInf, "created_at", null) ? date('Y-m-d', array_get
                        ($ccHdrInf, "created_at", null)) : '',
                    ];
                }
            }

            $models = $arrItems;
            $attributes = $input;

            $page = array_get($attributes, 'page', 1);
            $paginate = array_get($attributes, 'limit', 20);

            $offSet = ($page * $paginate) - $paginate;
            $itemsForCurrentPage = array_slice($models, $offSet, $paginate, true);
            $data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($models), $paginate,
                $page);

            $data = $data->toArray();

            $meta = [
                "pagination" => [
                    "total"         => (int)array_get($ccHdrInfoArrPage, 'total', null),
                    "per_page"      => (int)array_get($ccHdrInfoArrPage, 'per_page', null),
                    "count"         => (int)array_get($ccHdrInfoArrPage, 'per_page', null),
                    "current_page"  => (int)array_get($ccHdrInfoArrPage, 'current_page', null),
                    "total_pages"   => (int)array_get($ccHdrInfoArrPage, 'last_page', null),
                    "next_page_url" => array_get($ccHdrInfoArrPage, 'next_page_url', null),
                    "prev_page_url" => array_get($ccHdrInfoArrPage, 'prev_page_url', null),
                    "from"          => array_get($ccHdrInfoArrPage, 'from', null),
                    "to"            => array_get($ccHdrInfoArrPage, 'to', null),
                    "last_page"     => (int)array_get($ccHdrInfoArrPage, 'last_page', null),
                ]
            ];
            unset($ccHdrInfoArrPage['total']);
            unset($ccHdrInfoArrPage['per_page']);
            unset($ccHdrInfoArrPage['current_page']);
            unset($ccHdrInfoArrPage['total_pages']);
            unset($ccHdrInfoArrPage['next_page_url']);
            unset($ccHdrInfoArrPage['prev_page_url']);
            unset($ccHdrInfoArrPage['from']);
            unset($ccHdrInfoArrPage['to']);
            unset($ccHdrInfoArrPage['last_page']);

            return [
                'data' => array_values($data['data']),
                'meta' => $meta
            ];

            //return $this->response->paginator($arrItems, $cycleCountStatisticDashboardTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function zoneCapaticyAdminDashboard(Request $request, ZoneCapacityTransformer $zoneCapacityTransformer)
    {
        return [
            'data' => []
        ];

        $input = $request->getQueryParams();
        $cus_id = array_get($input, 'cus_id', null);
        $ztotalLocation = $this->locationModel->getAllLocationHasZone($cus_id)->sum('total');

        $zusedLocation = $this->locationModel->countLocationUsed($cus_id);

        try {
            set_time_limit(0);
            $zones = $this->zoneModel->getzones($cus_id, ['customerZone']);
            foreach ($zones as $zone) {
                $usedLocation = 0;
                $zoneId = object_get($zone, 'zone_id', 0);
                $countLocationByZone = $this->locationModel->countLocationByZone($zoneId);
                $zone->totalLocation = !empty($countLocationByZone) ? $countLocationByZone[0]['used'] : 0;
                $locationIds = $this->locationModel->getLocationListByZoneId($zoneId);

                if (!empty($locationIds)) {
                    if (count($locationIds) > 20000) {
                        $listLocationIds = array_chunk($locationIds->toArray(), 20000);
                        foreach ($listLocationIds as $itemLocationIds) {
                            $usedLocation += $this->locationModel->countLocationByIds($itemLocationIds, $cus_id);
                        }
                    } else {
                        $usedLocation = $this->locationModel->countLocationByIds($locationIds->toArray(), $cus_id);
                    }

                    $zone->usedLocation = $usedLocation;
                }

                $zone->percentLocation = ($zone->totalLocation) ? ($zone->usedLocation / $zone->totalLocation) * 100 : 0;
            }

            $percentLocation = 0;
            if ($ztotalLocation) {
                $percentLocation = $zusedLocation / $ztotalLocation;
            }

            return $this->response->collection($zones->sortByDesc('percentLocation'), $zoneCapacityTransformer)
                ->addMeta('totalLocation', intval($ztotalLocation))
                ->addMeta('usedLocation', intval($zusedLocation))
                ->addMeta('percentLocation', floatval($percentLocation * 100))
                ->addMeta('pagination', ['total' => $zones->count()]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function roomAdminDashboard(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $total = 0;
            $used = 0;
            $page = array_get($input, 'page', 0);
            $limit = array_get($input, 'limit', 999);
            $start = $limit * ($page - 1);
            $results = $this->locationModel->roomReport([], $total, $used, $start, $limit);

            return $this->response->paginator($results, new RoomCapacityTransformer())
                ->addMeta('roomTotal', intval($total))
                ->addMeta('used', intval($used))
                ->addMeta('percent', ($total != 0) ? floatval(($used / $total) * 100) : 0);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function alertBarDashboard(){
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $cus_ids = $predis::getCurrentCusIds();
        $whsId = array_get($userInfo, 'current_whs', 0);

        \DB::setFetchMode(\PDO::FETCH_ASSOC);

        try {
            $capacity = $this->zoneModel->getCapacity($cus_ids, $whsId);

            $asn_late = $this->asnHdrModel->getAsnLate($cus_ids, $whsId);

            $items_bo = $this->orderHdrModel->getItemBO($cus_ids, $whsId);

            $orders_late = $this->orderHdrModel->getOrdersLate($cus_ids, $whsId);

            $cycle_count_notify = $this->cycleHdrModel->getForDashboard($cus_ids, $whsId);

            $non_interactive_orders = $this->orderHdrModel->getNonInteractiveOrders($cus_ids, $whsId);

            return ['data' =>
                [
                    'capacity' => $capacity['zone_name'] == null ? 'None' : $capacity,
                    'asn_late' => $asn_late <= 0 ? 'None' : $asn_late,
                    'items_bo' => $items_bo <= 0 ? 'None' : $items_bo,
                    'orders_late' => $orders_late <= 0 ? 'None' : $orders_late,
                    'cycle_count_notify' => $cycle_count_notify <= 0 ? 'None' : $cycle_count_notify,
                    'non_interactive_orders' => $non_interactive_orders <= 0 ? 'None' : $non_interactive_orders
                ]
            ];
        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
