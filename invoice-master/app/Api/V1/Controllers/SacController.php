<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\Sac;
use App\Api\V1\Models\SacModel;
use App\Api\V1\Transformers\ClientMasterTransformer;
use App\Api\V1\Transformers\SacTransformer;
use App\Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class SacController extends AbstractController
{
    protected $sacModel;
    protected $transformer;
    /**
     * Init.
     *
     * @return void
     */
    public function __construct()
    {
        $this->sacModel = new SacModel();
        $this->transformer = new SacTransformer();
    }

    /**
     * @SWG\Get(
     *     path="/sac/customer",
     *     tags={"Sacs"},
     *     summary="Get Sacs List For Customer With Search Not Including The Added Ones",
     *     description="Get Sacs",
     *     operationId="getSacs",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cus_id",
     *         in="query",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="whs_id",
     *         in="query",
     *         description="ID of Warehouse",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="sac",
     *         in="query",
     *         description="Sac of Charge Code",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="sac_name",
     *         in="query",
     *         description="Sac Name of Charge Code",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Limit",
     *         type="integer",
     *         default="20",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page",
     *         type="integer",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Sacs",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    /**
     * Display a listing of the resource.
     * @param  string  $search
     * @return Response
     */
    public function index(IRequest $request)
    {
        $input = $request->getQueryParams();
        $sacs = Sac::whereNotIn('sac_id', function($q) use ($input){
                $q->select('sac_id')
                    ->from('cus_sac')
                    ->where([
                        ['cus_id', '=', $input['cus_id']],
                        ['whs_id', '=', $input['whs_id']],
                        ['deleted', '=', 0],
                    ]);
            })
            ->where(function ($q) use ($input){
                $q->when(!empty($input['sac']), function($query) use ($input){
                    return $query->where('sac', 'LIKE', '%'.$input['sac'].'%')
                        ->orWhere('sac_name', 'LIKE', '%'.$input['sac'].'%');
                });
            })
            ->when(!empty($input['sac_ids']), function($q)use($input){
                return $q->whereNotIn('sac_id', $input['sac_ids']);
            })
            ->paginate(array_get($input,'limit', 20));

        return $this->response->paginator($sacs, new ClientMasterTransformer)
            ->setStatusCode(app()->make('StatusCode')::HTTP_OK);
    }

    public function import(Request $request)
    {
        if ($request->hasFile('imported_file')) {
            $path = $request->file('imported_file')->getRealPath();
            $excel = app()->make('excel');
            $data = $excel->load($path, function($reader) {})->get();
            return [$data];
        }
        return ['false'];
    }

    public function update(IRequest $request, $sacId)
    {
        $input = $request->getParsedBody();
        $input['sac_id'] = $sacId;
        try {
            // update sac(s)
            $this->sacModel->update($input);

            return $this->response->created(Null, '{}');
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/sac/search",
     *     tags={"Sacs"},
     *     summary="Search Sac",
     *     description="Get Sacs",
     *     operationId="getSacsList",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cus_id",
     *         in="query",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="whs_id",
     *         in="query",
     *         description="ID of Warehouse",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="sac",
     *         in="query",
     *         description="Sac of Charge Code",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Limit",
     *         type="integer",
     *         default="20",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page",
     *         type="integer",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Sacs",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    public function search(IRequest $request)
    {
        $input = $request->getQueryParams();

        try {
            // get list SAC
            $sac = $this->sacModel->search($input,[], array_get($input, 'limit'));

            return $this->response->paginator($sac, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show($sacId)
    {
        try {
            $sac = $sac = $this->sacModel->getFirstWhere(['sac_id' => $sacId]);

            return $this->response->item($sac, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getListUnitUOM()
    {
        $list = $this->sacModel->getListUnitUOM('UT');
        $list->transform(function($item){
            return [
                'code'  => $item->inv_uom_code,
                'name'  => $item->inv_uom_des
            ];
        });
        return ['data' => $list];
    }

    public function createChargeCode(IRequest $request)
    {
        try{
            $data = $this->validateChargeCode($request);

            $type = 'WO';
            if($data['sac_name'] == "NA"){
                $type = "NA";
            }
            $data = collect($data)->merge([
                'whs_id'            => Data::getCurrentWhsId(),
                'type'              => $type,
                'sac'               => $this->sacModel->generateSacByType($type),
                'block'             => null,
                'range_from'        => null,
                'range_to'          => null,
                'sac_period'        => '',
                'range_uom'         => $data['range_uom'],
                'period_uom'        => $data['range_uom']
            ]);
            $sac = Sac::create($data->toArray());

            return ['data' => $sac];
        }catch(\Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function updateChargeCode($sacId, IRequest $request)
    {
        $sac = Sac::find($sacId);

        if (!$sac){
            return $this->response->errorBadRequest('Not found charge code');
        }

        if ($sac->type !== 'WO'){
            return $this->response->errorBadRequest('Charge code can not update');
        }

        try{
            $data = $this->validateChargeCode($request);

            if (!$sac->update($data)){
                return ['data' => 'Update fails'];
            }

            return ['data' => 'Update success'];
        }catch(\Exception $e){
            return $this->response->errorBadRequest($g->getMessage());
        }
    }

    private function validateChargeCode(IRequest $request)
    {
        $input = $request->getParsedBody();

        $validator = Validator::make($input, [
            'sac_name'          => 'required',
            'price'             => 'required|numeric',
            'unit_uom'          => 'required'
        ]);

        if($validator->fails()){
           $messages = $validator->errors();
           return $this->response->errorBadRequest( implode(", ", $messages->all()) );
        }

        return $input;
    }
}
