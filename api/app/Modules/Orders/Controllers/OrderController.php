<?php

namespace App\Modules\Orders\Controllers;

use App\Modules\Orders\Models\OrderHdrModel;
use App\Modules\Orders\Models\OrderCartonModel;
use App\Modules\Orders\Models\OrderDtlModel;
use App\Modules\Orders\Models\WavepickDtlModel;
use App\Modules\Orders\Models\WavepickHdrModel;
use App\Modules\Orders\Traits\OrderHdrControllerTrait;
use App\Modules\Orders\Models\InventorySummaryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;
use App\Modules\Orders\Resources\OrderCollection;
use App\Modules\Orders\Resources\OrderShippedCollection;
use App\Modules\Orders\Resources\OrderReadResource;
use App\Modules\Orders\Resources\OrderResource;
use App\Modules\Orders\Validators\OrderValidator;
use App\Modules\Orders\Validators\UpdateValidator;
use GuzzleHttp\Client;

class OrderController extends Controller
{
    protected $orderModel;
    protected $orderDetailModel;
    protected $orderCartonModel;
    protected $wavepickHdrModel;
    protected $wavepickDtlModel;
    protected $orderValidator;
    protected $updateValidator;
    protected $inventorySummaryModel;

    use OrderHdrControllerTrait;

    public function __construct()
    {
        $this->orderModel       = new OrderHdrModel();
        $this->orderDetailModel = new OrderDtlModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->wavepickHdrModel = new WavepickHdrModel();
        $this->wavepickDtlModel = new WavepickDtlModel();
        $this->orderValidator   = new OrderValidator();
        $this->updateValidator   = new UpdateValidator();
        $this->inventorySummaryModel = new InventorySummaryModel();
    }

    public function index($whsId, $cusId, Request $request)
    {
        $input = $request->all();

        $limit = array_get($request, 'per_page', 20);
        $baseUrl = str_replace( $request->root() , env('APP_URL'), \URL::current());
        $orders = $this->orderModel->search($input, $whsId, $cusId, $limit, $baseUrl);

        $this->orderValidator->checkOrder($orders);

        return new OrderCollection($orders);
    }

    public function read($whsId, $cusId, $odrHdrId, Request $request)
    {
        $order = $this->orderModel->show($whsId, $cusId, $odrHdrId);

        $this->orderValidator->checkOrder($order);
        
        return new OrderReadResource($order);
    }

    public function cancel($whsId, $cusId, $odrNum, Request $request)
    {
        //$order = $this->orderModel->cancelOrder($whsId, $cusId, $odrNum);

        //$this->orderValidator->checkCancelOrder($order);

        //return new OrderResource($order);

        $order = DB::table('odr_hdr')->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->where('odr_num', $odrNum)
            ->first();

        if (empty($order)) {
            return ['data' => ['message' => 'Order not found']];
        }

        $client = new Client();
        $response = $client->request('POST', env('API_LOGIN'), [
                'form_params' => [
                    'username' => env('USER_LOGIN'),
                    'password' => env('PWD_LOGIN')
                ]
            ]
        );
        $content = $response->getBody()->getContents();
        $token = \GuzzleHttp\json_decode($content)->data->token;

        if (empty($token)) {
            return ['data' => ['message' => 'Access Denied']];
        }

        $apiCancelOrder = env('API_ORDER_V2') . 'whs/' . $whsId . '/order/cancel?odr_id=' . $order->odr_id;
        $result = $client->request('GET', $apiCancelOrder, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]
        );
        $result = $result->getBody()->getContents();

        return [
            'data' => \GuzzleHttp\json_decode($result)->data
        ];
    }

    public function cancelOrder($whsId, $cusId, $odrNum, Request $request)
    {
        //DB::setFetchMode(\PDO::FETCH_ASSOC);
        $odrHdr = $this->orderModel->getFirstWhere([
            'odr_num' => $odrNum,
            'cus_id' => $cusId,
            'whs_id' => $whsId
        ]);

        if (! $odrHdr) {
            return [
                'data' => [
                    'message' => 'Cancel order unsuccessfully'
                ]
            ];
        }

        $odrHdrId = array_get($odrHdr, 'odr_id');
        $actCancelDt = date("m/d/Y", time());
        $odrChk = $this->orderModel->byId($odrHdrId);

        $odrIds = [];
        $subOdrIds = [];
        if ($odrChk->odr_sts == 'NW') {
            $subOdrIds[] = $odrHdrId;
        } else {
            $odrIds[] = $odrHdrId;
        }

        if ($odrChk->odr_type !== 'BAC') {
            // get back order != new
            $odrIdTmp = \DB::table('odr_hdr')
                // ->where('odr_num', 'LIKE', $parentOdrNum . '%')
                ->where('deleted', 0)
                ->where('odr_type', 'BAC')
                ->where('org_odr_id', $odrHdrId)
                ->where('odr_sts', '!=', 'NW')
                ->pluck('odr_id')->toArray();
            $odrIds = array_merge($odrIds, $odrIdTmp);

            // get back order new
            $subOdrIdsTmp= \DB::table('odr_hdr')
                // ->where('odr_num', 'LIKE', $parentOdrNum . '%')
                ->where('deleted', 0)
                ->where('odr_type', 'BAC')
                ->where('org_odr_id', $odrHdrId)
                ->where('odr_sts', 'NW')
                ->pluck('odr_id')->toArray();
            $subOdrIds = array_merge($subOdrIds, $subOdrIdsTmp);
        }

        $evtTracking = [];
        $userId = 334;

        try {
            DB::beginTransaction();
            $error = true;
            $cancelOdr = [];
            if ($odrIds) {


                foreach ($odrIds as $idx => $odrId) {
                    // WMS2-5062 - [Outbound - Putback][WEB] Show error message if it exist RFID carton
                    //$this->_executionOrderRfidCarton($odrId);

                    $odrObj = $this->orderModel->byId($odrId, ['details']);

                    /* WMS2-4477 START we will allow to cancel order which is picking */
                    // order is picking
                    if (object_get($odrObj, 'odr_sts', '') == 'PK') {

                        $notAssignCarton2Pallet = false;
                        $odrDtls = $odrObj->details()->where('lot', '!=', 'Any')->whereNotNull('wv_id')->get();
                        $ctnPicked = 0;
                        $returnIvtOrdDtl = [];
                        foreach ($odrDtls as $odrDtl) {
                            $odrDtlId = object_get($odrDtl, 'odr_dtl_id');
                            $itemId = object_get($odrDtl, 'item_id');
                            $lot = object_get($odrDtl, 'lot');

                            $wvdtl = DB::table('wv_dtl')
                                ->where('item_id', $itemId)
                                ->where('wv_id', object_get($odrDtl, 'wv_id'))
                                ->where('lot', $lot)
                                ->where('pack_size', object_get($odrDtl, 'pack'))
                                ->where('deleted', 0)
                                ->where('wv_dtl_sts', '!=', 'NW')
                                // ->select(DB::raw('SUM(ctn_qty) as ctn_qty_ttl'))->first();
                                ->select(DB::raw('SUM(act_piece_qty)'))->first();

                            // $ctnPicked += array_get($wvdtl, 'ctn_qty_ttl', 0);
                            // if($ctnPicked > 0) {
                            //     $notAssignCarton2Pallet = true;
                            // }
                            $actQty = array_get($wvdtl, 'act_piece_qty', 0);
                            $returnIvtCtnQty = object_get($odrDtl, 'alloc_qty', 0) - $actQty;
                            if ($returnIvtCtnQty > 0) {
                                $this->inventorySummaryModel->updateAllocInvtWhenOdrPicking(
                                    $whsId, $itemId, $lot, $returnIvtCtnQty
                                );
                            }
                        }
                        // if($notAssignCarton2Pallet) {
                        //     $msg = sprintf('All cartons should be picked and assign to this order first before cancelling');
                        //     return $this->response->errorBadRequest($msg);
                        // }
                    }
                    /* WMS2-4477 END we will allow to cancel order which is picking */

                    if ($this->processCancel($odrObj)) {
                        $error = false;
                        $cancelOdr[] = $odrId;

                        // Remove ShipId
                        $this->removeShipId($odrId);
                    } else {
                        continue;
                    }

                    //event tracking
                    $evtTracking[] = [
                        'whs_id'     => $odrObj->whs_id,
                        'cus_id'     => $odrObj->cus_id,
                        'owner'      => $odrObj->odr_num,
                        'evt_code'   => Status::getByKey("event", "ORDER-CANCELED"),
                        'trans_num'  => $odrObj->odr_num,
                        'info'       => sprintf(Status::getByKey("event-info", "ORDER-CANCELED"), $odrObj->odr_num),
                        'created_at' => time(),
                        'created_by' => $userId
                    ];
                    //update status cancel
                    $this->orderModel->getModel()
                        ->where('odr_id', $odrId)
                        ->update([
                            'odr_sts'       => 'CC',
                            'wv_id'         => null,
                            'act_cancel_dt' => strtotime($actCancelDt),
                            'updated_by'    => $userId,
                            'updated_at'    => time()
                        ]);
                    if (!empty($odrObj->wv_id) && $odrObj->odr_sts != "CC") {
                        $this->changeStatusWavepick($odrObj->wv_id, $odrObj->odr_sts, $cancelOdr);
                    }
                }


                if ($error) {
                    return [
                        'data' => [
                            'message' => 'Order has been shipped or finalized BOL cannot be cancelled!'
                        ]
                    ];
                }
            } else {
                foreach ($subOdrIds as $idx => $NewOdrId) {
                    $NewOdrObj = $this->orderModel->byId($NewOdrId, ['details']);
                    //event tracking
                    $evtTracking[] = [
                        'whs_id'     => $NewOdrObj->whs_id,
                        'cus_id'     => $NewOdrObj->cus_id,
                        'owner'      => $NewOdrObj->odr_num,
                        'evt_code'   => Status::getByKey("event", "ORDER-CANCELED"),
                        'trans_num'  => $NewOdrObj->odr_num,
                        'info'       => sprintf(Status::getByKey("event-info", "ORDER-CANCELED"), $NewOdrObj->odr_num),
                        'created_at' => time(),
                        'created_by' => $userId
                    ];
                }
            }

            // update order cartons
            $this->orderCartonModel->getModel()
                ->whereIn('odr_hdr_id', $cancelOdr)
                ->update([
//                    'odr_hdr_id' => null,
//                    'odr_dtl_id' => null,
//                    'odr_num'    => null,
                    'updated_by' => $userId,
                    'updated_at' => time()
                ]);

            //update status cancel for subOdrIds
            $this->orderModel->getModel()
                ->whereIn('odr_id', $subOdrIds)
                ->update([
                    'odr_sts'       => 'CC',
                    'wv_id'         => null,
                    'act_cancel_dt' => strtotime($actCancelDt),
                    'updated_by'    => $userId,
                    'updated_at'    => time()
                ]);
            //update status cancel for odr_dtl
            $this->orderDetailModel->getModel()
                ->whereIn('odr_id', $subOdrIds)
                ->update([
                    'itm_sts'       => 'CC',
                    'updated_by'    => $userId,
                    'updated_at'    => time()
                ]);

            //Event Tracking
            DB::table('evt_tracking')->insert($evtTracking);

            //$this->updateInvetorySmrOdr($odrHdrId);
            $this->updateInvetoryOdr($odrHdrId);
            $this->updateInvetoryReportOdr($odrHdrId);
            DB::commit();

            return [
                'data' => [
                    'message' => 'Order canceled successfully!'
                ]
            ];

        } catch (\PDOException $e) {
            DB::rollBack();
            return [
                'data' => [
                    'message' => $e->getMessage()
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'data' => [
                    'message' => $e->getMessage()
                ]
            ];
        }
    }

    public function shipped($whsId, $cusId, Request $request)
    {
        $input = $request->all();

        $orders = $this->orderModel->getOrderShipped($input, $whsId, $cusId);

        $this->orderValidator->checkOrder($orders);

        return new OrderShippedCollection($orders);
    }

    public function update($whsId, $cusId, $odrHdrId, Request $request)
    {
        $input = $request->all();
        $input = array_merge($input, ['whs_id' => (int) $whsId, 'cus_id' => (int) $cusId, 'odr_id' => (int) $odrHdrId]);

        $this->updateValidator->validate($input);
        $order = $this->orderModel->update($input);

        return new OrderReadResource($order);
    }

    public function updateInvetorySmrOdr($odrHdrID) {
        // Update picked_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS picked_qty
                FROM
                    cartons c
                WHERE
                    c.ctn_sts = 'PD'
                AND c.deleted = 0
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            SET i.picked_qty = IFNULL(t.picked_qty, 0)
            WHERE i.picked_qty != IFNULL(t.picked_qty, 0)
            AND d.odr_id = $odrHdrID
        ");

        // Update allocated_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    h.odr_sts,
                    d.whs_id,
                    d.item_id,
                    d.lot,
                    SUM(

                        IF (
                            d.alloc_qty < d.picked_qty,
                            0,
                            d.alloc_qty - d.picked_qty
                        )
                    ) AS allocated_qty
                FROM
                    odr_dtl d
                JOIN odr_hdr h ON h.odr_id = d.odr_id
                WHERE
                    h.odr_sts IN ('AL', 'PK')
                AND d.alloc_qty > 0
                AND d.deleted = 0
                GROUP BY
                    d.whs_id,
                    d.item_id,
                    d.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            SET i.allocated_qty = IFNULL(t.allocated_qty, 0)
            WHERE i.allocated_qty != IFNULL(t.allocated_qty, 0)
            AND d.odr_id = $odrHdrID
        ");

        // Update avail_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS available_qty
                FROM
                    cartons c
                WHERE
                    c.deleted = 0
                AND c.ctn_sts IN ('AC', 'LK')
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            SET i.avail = GREATEST(
                IFNULL(
                    t.available_qty - i.allocated_qty,
                    0
                ),
                0
            ),
             i.allocated_qty =
            IF (
                IFNULL(
                    t.available_qty - i.allocated_qty,
                    0
                ) < 0,
                ABS(
                    IFNULL(
                        t.available_qty - i.allocated_qty,
                        0
                    )
                ),
                i.allocated_qty
            )
            WHERE
                (i.avail != GREATEST(
                        IFNULL(
                            t.available_qty - i.allocated_qty,
                            0
                        ),
                        0
                    )
                OR i.allocated_qty !=
                IF (
                    IFNULL(
                        t.available_qty - i.allocated_qty,
                        0
                    ) < 0,
                    ABS(
                        IFNULL(
                            t.available_qty - i.allocated_qty,
                            0
                        )
                    ),
                    i.allocated_qty
                ))
            AND d.odr_id = $odrHdrID
        ");

        // Update total_qty
        DB::statement("
            UPDATE invt_smr i
            JOIN odr_dtl d ON i.whs_id = d.whs_id AND i.item_id = d.item_id
            LEFT JOIN (
                SELECT
                    c.whs_id,
                    c.item_id,
                    c.lot,
                    SUM(c.piece_remain) AS total_qty
                FROM
                    cartons c
                WHERE
                    c.deleted = 0
                AND c.ctn_sts IN ('AC', 'LK', 'PD')
                GROUP BY
                    c.whs_id,
                    c.item_id,
                    c.lot
            ) AS t ON t.whs_id = i.whs_id
            AND t.item_id = i.item_id
            SET i.ttl = IFNULL(t.total_qty, 0)
            WHERE i.ttl != IFNULL(t.total_qty, 0)
            AND d.odr_id = $odrHdrID
        ");
    }

    public function updateInvetoryOdr($odrHdrID) {
        DB::select(DB::raw("UPDATE inventory s
            JOIN (
                SELECT
                    h.whs_id,
                    d.item_id,
                    SUM( d.alloc_qty) AS qty 
                FROM
                    odr_hdr h
                JOIN odr_dtl d ON h.odr_id = d.odr_id 
                WHERE h.odr_id = $odrHdrID
                    AND d.deleted = 0
                    AND d.alloc_qty > 0 
                GROUP BY
                    h.whs_id, d.item_id 
            )   AS t
            ON t.whs_id = s.whs_id AND s.item_id = t.item_id
            SET s.in_pick_qty = s.in_pick_qty - t.qty,
                s.in_hand_qty = s.in_hand_qty + t.qty
            WHERE s.type = 'I'"));
        return 0;
    }

    public function updateInvetoryReportOdr($odrHdrID) {
        DB::select(DB::raw("UPDATE rpt_inventory s
            JOIN (
                SELECT
                    h.whs_id,
                    d.item_id,
                    SUM( d.alloc_qty) AS qty 
                FROM
                    odr_hdr h
                JOIN odr_dtl d ON h.odr_id = d.odr_id 
                WHERE h.odr_id = $odrHdrID
                    AND d.deleted = 0
                    AND d.alloc_qty > 0 
                GROUP BY
                    h.whs_id, d.item_id 
            )   AS t
            ON t.whs_id = s.whs_id AND s.item_id = t.item_id
            SET s.in_pick_qty = s.in_pick_qty - t.qty,
                s.in_hand_qty = s.in_hand_qty + t.qty
            WHERE s.type = 'I'"));
        return 0;
    }
}
