<?php

namespace App;

class SystemState extends BaseSoftModel
{
    protected $table = 'system_state';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_state_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sys_state_code',
        'sys_state_name',
        'sys_state_country_id',
        'sys_state_desc'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function systemCountry()
    {
        return $this->belongsTo('App\SystemCountry', 'sys_state_country_id', 'sys_country_id');
    }
}
