<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Transformers\ContainerTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class ContainerController extends AbstractController
{
    /**
     * @var ContainerModel
     */
    protected $containerModel;


    /**
     * ContainerController constructor.
     *
     * @param ContainerModel $containerModel
     */
    public function __construct(ContainerModel $containerModel)
    {
        $this->containerModel = $containerModel;
    }

    /**
     *
     * Container Search
     *
     * @param Request $request
     * @param ContainerTransformer $containerTransformer
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int
     */
    public function search(Request $request, ContainerTransformer $containerTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $container = $this->containerModel->search($input, [

            ], array_get($input, 'limit'));

            return $this->response->paginator($container, $containerTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
