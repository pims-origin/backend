<?php

namespace App\Wms2\UserInfo;

use \Predis\Client;
use \Firebase\JWT\JWT;
use App\Wms2\UserInfo\HttpService\UserService;
use function GuzzleHttp\json_decode;

class Data
{
    protected $_redis;
    protected $_cacheKey;
    protected $_data;
    private static $__instance;

    public static function getInstance()
    {
        if (self::$__instance === null) {
            self::$__instance = new Data();
        }

        return self::$__instance;
    }

    function __construct()
    {
        $config = config('database.redis.default');
        $this->_cacheKey = config('database.redis.cache-key') ?? 'awms2_dev:cache.user.';
        $this->_redis = new Client($config);

        $this->__getUserInfo();
    }

    private function __getUserInfo()
    {
        $key = env("JWT_SECRET"); //"CGunzjKYdoErvLvz83pk0ucNTnRL9sC5";
        $auth = app('request')->header('Authorization');
        $jwt = str_replace("Bearer ", "", $auth);
        $decoded = JWT::decode($jwt, $key, ['HS256']);
        $userId = $decoded->jti;

        $rs = $this->_redis->get($this->_cacheKey . $userId);

        if (empty($rs)) {
            //call api
            $http = new UserService($auth);
            $resp = $http->getUserCache();

            if ($resp->getStatusCode() === 200) {
                return $this->_data = json_decode($resp->getBody()->getContents(), true)['data'];
            } else {
                throw new \Exception($resp->getBody());
            }
        }

        return $this->_data = unserialize($rs)[0]; //for array alway get value[0]
    }

    public function getUserInfo()
    {
        return $this->_data;
    }

    public function getCustomersByWhs($whsId=false)
    {
        if (!$whsId) {
            $whsId = $this->getCurrentWhs();
        }

        if (empty($this->_data['user_customers'])) {
            return false;
        }

        $rs = [];
        foreach ($this->_data['user_customers'] as $userCus) {
            if ($userCus['whs_id'] === $whsId) {
                $rs[] = (int)$userCus['cus_id'];
            }
        }

        return $rs;
    }

    public function getCurrentWhs()
    {
        return $this->_data['current_whs'];
    }


    //------------------static function-------------------------------

    public static function getCurrentWhsId()
    {
        $userInfo = self::getInstance()->getUserInfo();

        return $userInfo['current_whs'];
    }

    public static function getCurrentUserId()
    {
        $userInfo = self::getInstance()->getUserInfo();

        return $userInfo['user_id'];
    }

    public static function getCurrentCusIds()
    {
        $userInfo = self::getInstance();

        return (array) $userInfo->getCustomersByWhs();
    }

    final public static function isAccessWhsAndCus($cusId, $whsId){
        $cusIds = self::getCurrentCusIds();
        if( $whsId == self::getCurrentWhsId() && in_array($cusId, $cusIds)){
            return true;
        }
        return false;
    }

}