<?php

namespace App\Modules\Orders\Models;

use App\Modules\Orders\Eloquents\WavepickDtl;

class WavepickDtlModel extends AbstractModel
{
    /**
     * WavepickDtlModel constructor.
     *
     * @param WavepickDtl|null $model
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $wvIds
     *
     * @return mixed
     */
    public function countSku($wvIds)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->whereIn('wv_id', $wvIds)
            ->count(['item_id']);

        return $skuCount;
    }
}