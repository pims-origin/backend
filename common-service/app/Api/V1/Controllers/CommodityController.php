<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\CommodityModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Transformers\SystemCountryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use App\Api\V1\Transformers\CommodityTransformer;
use App\Api\V1\Validators\CommodityValidator;
use App\Api\V1\Validators\SystemCountryValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\SystemBug;

/**
 * Class CountryController
 *
 * @package App\Api\V1\Controllers
 */
class CommodityController extends AbstractController
{

    /**
     * @var SystemStateModel
     */
    protected $systemStateModel;

    /**
     * @var SystemCountryModel
     */
    protected $systemCountryModel;

    /**
     * @var SystemCountryValidator
     */
    protected $systemCountryValidator;

    /**
     * @var SystemCountryTransformer
     */
    protected $systemCountryTransformer;

    /**
     * @var SystemStateTransformer
     */
    protected $systemStateTransformer;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var CommodityModel
     */
    protected $commodityModel;

    /**
     * @var CommodityTransformer
     */
    protected $commodityTransformer;

    /**
     * CountryController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->model = new SystemCountryModel();
        $this->systemCountryModel = new SystemCountryModel();
        $this->systemStateModel = new SystemStateModel();
        $this->systemCountryValidator = new SystemCountryValidator();
        $this->systemCountryTransformer = new SystemCountryTransformer();
        $this->systemStateTransformer = new SystemStateTransformer();
        $this->commodityModel = new CommodityModel();
        $this->commodityValidator = new CommodityValidator();
        $this->commodityTransformer = new CommodityTransformer();
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    /**
     * @param Request $request
     * @param $cmdId
     *
     * @return array|void
     */
    public function update(Request $request, $cmdId)
    {
        return $this->upsert($request, $cmdId);
    }

    /**
     * @param $request
     * @param bool $cmdId
     *
     * @return array|void
     * @throws \Exception
     */
    protected function upsert($request, $cmdId = false)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->commodityValidator->validate($input);

        //$valueArrJson = [];
        $inputParam = [
            'cmd_name'       => array_get($input, 'cmd_name', ''),
            'cmd_des'        => array_get($input, 'cmd_des', null),
            'commodity_nmfc' => array_get($input, 'commodity_nmfc', ''),
            'cmd_cls'        => array_get($input, 'cmd_cls', ''),

        ];


        try {
            if ($cmdId) {
                //update
                //check not existed
                $commodity = $this->commodityModel->getFirstWhere(['cmd_id' => $cmdId]);
                if (empty($commodity)) {
                    throw new \Exception(Message::get("BM017", "Commodity"));
                }

                //check duplicated in updating
                $checkCommodity = $this->commodityModel->getFirstWhere(
                    ['cmd_name' => array_get($input, 'cmd_name', '')]
                );
                if (!empty($checkCommodity) && $checkCommodity->cmd_id != $cmdId) {
                    throw new \Exception(Message::get("BM006", "Commodity Name"));
                }

                $inputParam['cmd_id'] = $cmdId;
                $this->commodityModel->update($inputParam);
            } else {
                //create
                //check duplicated
                $checkCommodity = $this->commodityModel->getFirstWhere(
                    ['cmd_name' => array_get($input, 'cmd_name', '')]
                );
                if (!empty($checkCommodity)) {
                    throw new \Exception(Message::get("BM006", "Commodity Name"));
                }

                $this->commodityModel->create($inputParam);
            }

            return ["data" => "successful"];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param CommodityTransformer $commodityTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function commodityList(
        Request $request,
        CommodityTransformer $commodityTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            if (empty($input['sort'])) {
                $input['sort'] = [
                    'cmd_name' => 'asc'
                ];
            }
            // get list Cmd
            $commodityInfos = $this->commodityModel->search($input,
                [],
                array_get($input, 'limit'));

            return $this->response->paginator($commodityInfos, $commodityTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'commodityList', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $cmdId
     * @param CommodityTransformer $commodityTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(
        $cmdId,
        CommodityTransformer $commodityTransformer
    ) {

        try {
            $cmdInfo = $this->commodityModel->getFirstBy('cmd_id', $cmdId, []);
            if (empty($cmdInfo)) {
                throw new \Exception(Message::get("BM017", "Commodity"));
            }

            return $this->response->item($cmdInfo, $commodityTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                $e->getMessage() . " " . $e->getLine()
            //SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
