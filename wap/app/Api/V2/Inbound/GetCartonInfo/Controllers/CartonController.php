<?php
namespace App\Api\V2\Inbound\GetCartonInfo\Controllers;

use App\Api\V2\Inbound\Models\CartonModel;
use App\Api\V2\Inbound\Models\Log;
use App\Api\V2\Inbound\Models\VirtualCartonModel;
use App\libraries\RFIDValidate;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Support\Facades\DB;

/**
 * Class CartonController
 *
 * @package App\Api\V2\Inbound\GetCartonInfo\Controllers
 */
class CartonController extends AbstractController
{
    protected  $cartonModel;
    protected  $vtlCartonModel;
    public function __construct() {
        $this->cartonModel = new CartonModel();
        $this->vtlCartonModel = new VirtualCartonModel();
    }

    public function getCartonInfo($whsId, Request $request)
    {
        $url = "/inbound/whs/$whsId/carton/carton-information";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'CTI',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get Carton information'
        ]);

        /*
         * end logs
         */

        $input = $request->getQueryParams();

        $carton_rfid = array_get($input, 'carton_rfid', null);
        if (!$carton_rfid) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['message'][] = [
                'status_code' => -1,
                'msg' => 'Please input carton RFID.'
            ];

            return new Response($msg, 200, [], null);
        }

        $mess = "Carton's RFID must be valid pattern '/^([A-F0-9]{24})$/' and length 24";
        //validate carton RFID
        $ctnRFIDValid = new RFIDValidate($carton_rfid, RFIDValidate::TYPE_CARTON, $whsId);
        if (!$ctnRFIDValid->validate()) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['messages'][] = [
                'status_code' => -1,
                'msg' => $mess
            ];

            return new Response($msg, 200, [], null);
        }

        try {
            //get data from carton table

            $data = $this->cartonModel->getCartonInfoByRFID($whsId, $carton_rfid);
            //if there is not carton on carton table, try to get from vtl ctn table
            if (! $data) {
                $data = $this->vtlCartonModel->getCartonInfoByRFID($whsId, $carton_rfid);
                if  ($data) {
                    if (!is_array($data)) {
                        $data = $data->toArray();
                    }
                    $data['status'] = 'New';
                }
            }

            if ($data) {

                $msg = [];
                $msg['status']  = true;
                $msg['iat']     = time();
                $msg['messages'][] = [
                    'status_code' => 1,
                    'msg'         => sprintf("Successfully!")
                ];
                if (!is_array($data)) {
                    $data = $data->toArray();
                }

                $data['cus_po'] = '';
                $data['cus_odr_num'] = '';
                if (array_get($data, 'ctn_sts') == "PD") {
                    $orderArr = DB::table('odr_hdr')
                        ->join('odr_cartons', 'odr_hdr.odr_id', '=', 'odr_cartons.odr_hdr_id')
                        ->where('odr_cartons.deleted', 0)
                        ->where('odr_hdr.deleted', 0)
                        ->where('odr_cartons.ctn_rfid', $carton_rfid)
                        ->first();
                    $data['cus_po'] = array_get($orderArr, 'cus_po', '');
                    $data['cus_odr_num'] = array_get($orderArr, 'cus_odr_num', '');
                }

                $msg['data'] = [$data];

                return new Response($msg, 200, [], null);
            } else {
                $error = sprintf("Carton %s does not existed", $carton_rfid);
                $carton = $this->cartonModel->getFirstWhere(['rfid' => $carton_rfid]);
                if ($carton && $carton->whs_id != $whsId) {
                    $error = sprintf("Carton %s doesn't belong to current warehouse.", $carton_rfid);
                }

                $msg = [];
                $msg['status']  = false;
                $msg['iat']     = time();
                $msg['messages'][] = [
                    'status_code' => -1,
                    'msg'         => $error
                ];
                $msg['data'] = [$carton_rfid];
                return new Response($msg, 200, [], null);
            }

        } catch (\Exception $e) {
            $msg = [];
            $msg['status'] = false;
            $msg['iat'] = time();
            $msg['data'] = [];
            $msg['messages'][] = [
                'status_code' => -1,
                'msg' => $e->getMessage()
            ];

            return new Response($msg, 200, [], null);
        }

    }

}
