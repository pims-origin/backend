<?php

namespace App\Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderShippedResource extends JsonResource
{

    public function toArray($request)
    {
        $shipByDt   = ($this->ship_by_dt && strtotime($this->ship_by_dt) != 0) ? $this->ship_by_dt->format('Y-m-d') : NULL;
        $packs = $this->packHdr;

        $result = [];
        foreach ($packs as $pack) {
            $result[] = [
                'type' => 'orders',
                'id' => (int) $this->odr_id,
                'attributes' => [
                    'cus_odr_num'   => $this->cus_odr_num,
                    'cus_po'        => $this->cus_po,
                    'tracking_num'  => $pack->tracking_number,
                    'truck_num'     => NULL,
                    'status'        => $this->odrHdrSts->name,
                    'ship_by_dt'    => $shipByDt
                ]
            ];
        }

        return $result;
    }
}