<?php

namespace App\Api\V1\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\Status;

class ItemLocationTransformer extends TransformerAbstract
{

    /**
     * @param Location $itemLoc
     *
     * @return array
     */
    public function transform(Location $itemLoc)
    {
//        $pallet = $itemLoc->pallet()->first();
//        $carton = isset($pallet) ? $pallet->carton()->first() : null;
//        $ctnTtl = Carton::where('loc_id', object_get($itemLoc, 'loc_id', 0))
//            ->where('whs_id', object_get($itemLoc, 'loc_whs_id', 0))
//            ->where('cus_id', object_get($carton, 'cus_id', 0))
//            ->where('item_id', object_get($carton, 'item_id', 0))
//            ->where(function ($q) {
//                $q->where('is_damaged', 0);
//                $q->orWhereNull('is_damaged');
//            })
//            ->count();

        return [
            'loc_sts'      => $itemLoc->loc_sts_code,
            'loc_sts_name' => Status::getByValue($itemLoc->loc_sts_code, 'LOCATION_STATUS'),
            'loc_id'       => $itemLoc->loc_id,
            'loc_code'     => $itemLoc->loc_code,
            'ctn_ttl'      => $itemLoc->ctn_ttl,
            'ttl_pieces'   => $itemLoc->ttl_pieces,
        ];
    }
}
