<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\WarehouseMeta;
use Illuminate\Support\Facades\DB;

class EmailConfigurationModel extends AbstractModel
{
    protected $code = 'wie';

    /**
     * Constructor.
     */

    public function __construct()
    {
        $this->model = new WarehouseMeta();
    }

    public function getSmtpInfo($whsId)
    {
        $smtpInfo = $this->getFirstWhere([
            'whs_id'        => $whsId,
            'whs_qualifier' => $this->code
        ]);
        
        return $smtpInfo;
    }
    
    public function add($input = [], $whsId)
    {
        $smtpInfo = WarehouseMeta::where([
            'whs_id'        => $whsId,
            'whs_qualifier' => $this->code
        ])->first();
        
        if ($smtpInfo) {
            WarehouseMeta::where([
                'whs_id'        => $whsId,
                'whs_qualifier' => $this->code
            ])->update(['whs_meta_value' => json_encode($input)]);
        } else {
            $this->create([
                'whs_id' => $whsId,
                'whs_qualifier'  => $this->code,
                'whs_meta_value' => json_encode($input)
            ]);
        }
    }
}
