<?php
/**
 * Created by PhpStorm.
 * User: chicu
 * Date: 9/14/2016
 * Time: 10:47 AM
 */

namespace Seldat\Wms2\Models;


use Illuminate\Database\Eloquent\Model;
use Seldat\Wms2\Utils\JWTUtil;

class SysBug extends BaseModel
{
    protected $table = 'sys_bugs';
    protected $primaryKey = 'id';
    public $timestamps = true;

    const MESSAGE_ERR = 'There is some error when load data, please contact admin !';
    const API_ITEM_MASTER = 'item-master';
    const API_AUTHENTICATION = 'authentication';
    const API_AUTHORIZATION = 'authorization';
    const API_CUSTOMER = 'customer';
    const API_DATA_LAYER = 'data-layer';
    const API_GOOD_RECEIPT = 'goods-receipt';
    const API_MENU = 'menu';
    const API_ORDER = 'orders';
    const API_REPORT = 'report';
    const API_SYSTEM = 'system';
    const API_WAREHOUSE = 'warehouse';
    const API_WAVE_PICK = 'wave-pick';
    const API_RF_GUN = 'rf-gun';

    protected $fillable = [
        'date',
        'api_name',
        'error'
    ];

    public static function boot()
    {
        parent::boot();

        $userId = JWTUtil::getPayloadValue('jti') ?: 0;


        // create a event to happen on saving
        static::creating(function ($table) use ($userId) {
            $table->created_by = $table->created_by ?: $userId;
            $table->created_at = time();
        });

    }
    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }
}