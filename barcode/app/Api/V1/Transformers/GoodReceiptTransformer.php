<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnHdr;

class GoodReceiptTransformer extends TransformerAbstract
{
    public function transform(AsnHdr $asnHdr)
    {
        $ept_dt = (!empty($asnHdr->asn_hdr_ept_dt))?date('Y/m/d',$asnHdr->asn_hdr_ept_dt):'';
        $act_dt = (!empty($asnHdr->asn_hdr_act_dt))?date('Y/m/d',$asnHdr->asn_hdr_act_dt):'';
        $type = 0;
        $expire_dt = false;
        if(!empty($asnHdr->value)) {
            $value = json_decode($asnHdr->value);
            $type = (!empty($value->step))?$value->step:0;
            $expire_dt = boolval($value->expire_dt);
        }
        return [
            'asn_hdr_id'            => object_get($asnHdr, 'asn_hdr_id'),
            'cus_id'                => object_get($asnHdr, 'cus_id'),
            'cus_name'              => object_get($asnHdr, 'cus_name'),
            'cus_code'              => object_get($asnHdr, 'cus_code'),
            'asn_hdr_num'           => object_get($asnHdr, 'asn_hdr_num'),
            'asn_hdr_ept_dt'        => $ept_dt,
            'asn_hdr_act_dt'        => $act_dt,
            'asn_sts'               => object_get($asnHdr, 'asn_sts'),
            'ref_code'              => object_get($asnHdr, 'asn_hdr_ref'),
            'asn_type'              => object_get($asnHdr, 'asn_type'),
            "step"                  => $type,
            'ctnr_num'              => object_get($asnHdr, 'asn_ctnr_num',object_get($asnHdr, 'gr_ctnr_num','')),
            'ctnr_id'               => object_get($asnHdr, 'asn_ctnr_id',object_get($asnHdr, 'gr_ctnr_id','')),
            'expire_dt'             => $expire_dt
        ];
    }
}
