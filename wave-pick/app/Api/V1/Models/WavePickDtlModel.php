<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Status;

class WavePickDtlModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickDtl($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function loadData($wv_id)
    {
        return $this->model->select([
            'item_id as ITEM_ID',
            'sku as SKU',
            'color as Color',
            'size as SIZE',
            'lot as Lot',
            'pack_size as Pack_size',
            'ctn_qty as Cartons',
            'piece_qty as Picking_QTY',
            'primary_loc_id',
            'primary_loc',
            'bu_loc_1',
            'bu_loc_2',
            'bu_loc_3',
            'bu_loc_4',
            'bu_loc_5',
            'bu_loc_1_id',
            'bu_loc_2_id',
            'bu_loc_3_id',
            'bu_loc_4_id',
            'bu_loc_5_id',
            'cus_id',
            'wv_dtl_id'
        ])
            ->where('wv_id', '=', $wv_id)
            ->where('piece_qty', '>', 0)
            ->distinct()
            ->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     * @deprecated DONT USE THIS FUNCTION
     */
    public function updateWvDtl($data)
    {
        return $this->model
            ->where('item_id', '=', $data['item_id'])
            ->where('wv_id', '=', $data['wv_id'])
            ->update([
                'act_piece_qty' => $data['act_piece_qty'],
                'wv_dtl_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }


    public function updateWaveDtl($wvDtlID, $actQty)
    {
        $res =  $this->model
            ->where('wv_dtl_id', '=', $wvDtlID)
            ->update([
                'act_piece_qty' => $actQty,
                'wv_dtl_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);

        return $res;
    }
    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function loadWvDetail($wv_id)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return DB::table('wv_dtl as wd')
            ->select([
            'wd.*',
            DB::raw("SUM(wd.piece_qty) as s_piece_qty")
        ])
            ->join('wv_dtl_loc as wdl', 'wdl.wv_dtl_id', '=', 'wd.wv_dtl_id')
            ->addSelect('wdl.act_loc_ids' )
            ->where('wd.wv_id', '=', $wv_id)
            ->where('deleted', 0)
            ->groupBy('wd.item_id', 'wd.sku', 'wd.color', 'wd.size', 'wd.lot')
            ->get();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getWvDtlById($wv_dtl_ids)
    {
        $wv_dtl_ids = is_array($wv_dtl_ids) ? $wv_dtl_ids : [$wv_dtl_ids];

        return $this->model
            ->whereIn('wv_dtl_id', $wv_dtl_ids)
            ->get()
            ;
    }
}
