<?php


namespace App\Api\Bay\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\ContainerCharge;
use Seldat\Wms2\Utils\Status;

class ContainerChargeTransformer extends TransformerAbstract
{
    //return result
    public function transform(ContainerCharge $ctnrCharge)
    {
        $start = object_get($ctnrCharge, 'start', null);
        $end = object_get($ctnrCharge, 'end', null);

        return [
            'bay_id'   => object_get($ctnrCharge, 'bay.bay_id', null),
            'bay_name' => object_get($ctnrCharge, 'bay.bay_name', null),

            'ctnr_num' => object_get($ctnrCharge, 'ctnr_num', null),
            'ctnr_id'  => object_get($ctnrCharge, 'ctnr_id', null),

            'cus_id'   => object_get($ctnrCharge, 'cus_id', null),
            'cus_name' => object_get($ctnrCharge, 'cus_name', null),

            'whs_id' => object_get($ctnrCharge, 'whs_id', null),

            'start' => date("m/d/Y H:i", strtotime($start)),
            'end'   => date("m/d/Y H:i", strtotime($end)),

            'duration'   => object_get($ctnrCharge, 'duration', null),
            'created_by' => object_get($ctnrCharge, 'createdUser.user_name', null),
            'created_at' => $ctnrCharge->created_at->format("d/m/Y"),

            'cc_sts'      => object_get($ctnrCharge, 'cc_sts', null),
            'cc_sts_name' => Status::getByKey('CONTAINER_CHARGE_STATUS', object_get($ctnrCharge, 'cc_sts', null)),
            'temp'        => object_get($ctnrCharge, 'temp', null)
        ];
    }

}
