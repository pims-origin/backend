<?php

namespace App\Modules\Orders\Controllers;

use App\Modules\Customers\Eloquents\Customer;
use App\Modules\Orders\Lib\BulkOrderImport;
use App\Modules\Orders\Lib\EcomOrderImport;
use App\Modules\Orders\Lib\RetailOrderImport;
use App\Modules\Orders\Resources\OrderReadResource;
use App\Modules\Orders\Validators\CreateValidator;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ServerRequestInterface as Request;
use Laravel\Lumen\Routing\Controller;
use App\Modules\Orders\Validators\UpdateValidator;
use Illuminate\Support\Facades\DB;
use App\Jobs\OrderFlowJob;

class OrderImportController extends Controller
{
    public function store($whsId, $cusId, Request $request)
    {
        $input = $request->getParsedBody();
        $input['whs_id'] = $whsId;
        $input['cus_id'] = $cusId;

        $validator = new CreateValidator;
        $validator->validate($input);

        $customer = Customer::where('cus_id', $cusId)->first();
        if (empty($customer)) {
            throw new \Exception(sprintf('Customer ID: %s is not exists', $cusId));
        }

        Log::info('IMS Create Order. Data: ' . \GuzzleHttp\json_encode($input));

        $time = time();
        /*
        DB::table('sys_bugs')->insert([
            'date'       => time(),
            'api_name'   => 'WMS Create Order Request' . $time,
            'error'      =>  json_encode($input) ,
            'created_at' => time(),
            'created_by' => null,
            'updated_at' => time()
        ]);
        */

        DB::beginTransaction();

        $this->setOrderImport($input['order_type']);

        $order = $this->orderImport->import($input);

        $this->insertOrderFlow($order->odr_id, $order->odr_type, $order->cus_id);

        $response = new OrderReadResource($order);

        /*
        DB::table('sys_bugs')->insert([
            'date'       => time(),
            'api_name'   => 'WMS Create Order Response' . $time,
            'error'      =>  json_encode($response) ,
            'created_at' => time(),
            'created_by' => null,
            'updated_at' => time()
        ]);
        */

        DB::commit();

        // Auto Order Flow
        dispatch(new OrderFlowJob($order->odr_id, $cusId, $request));

        return $response;
    }

    public function setOrderImport($orderType = null)
    {
        $orderType = $orderType ?: 'bulk';

        if ( strtolower($orderType) === 'bulk' ) {
            $this->orderImport = new BulkOrderImport();
        } else if ( strtolower($orderType) === 'retail' ||  strtolower($orderType) === 'smallparcel') {
            $this->orderImport = new RetailOrderImport();
        } else if ( strtolower($orderType) === 'ecom' ) {
            $this->orderImport = new EcomOrderImport();
        }

        // $this->orderImport = new BulkOrderImport();
    }

    public function insertOrderFlow($odrId, $odrType, $cusId)
    {
        try {
            // Insert data to table odr_hdr_meta
            DB::table('odr_hdr_meta')->insert([
                'odr_id'     => $odrId,
                'qualifier'  => 'OFP',
                'value'      => 0,
                'created_at' => time()
            ]);

            // get oder flow from order meta
            $qualifier = "OFF";
            if (strtolower($odrType) === 'smallparcel') {
                $qualifier = "OSF";
            }

            $cusMeta = DB::table('cus_meta')->where('cus_id', $cusId)->where('qualifier', $qualifier)->first();

            if(!empty($cusMeta)) {
                $value = $cusMeta->value;
            }else{
                $value = $this->getOrderFlow($odrType);
            }

            DB::table('odr_hdr_meta')->insert([
                'odr_id'     => $odrId,
                'qualifier'  => $qualifier,
                'value'      => $value,
                'created_at' => time()
            ]);
        } catch (Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getOrderFlow($odrType)
    {
        $odrFlowMaster = DB::table('odr_flow_master')->get();

        $orderFlow = [];

        foreach ($odrFlowMaster as $value) {
            $odrFlowItem = array (
                'odr_flow_id' => $value->odr_flow_id,
                'odr_sts' => $value->odr_sts,
                'type' => $value->type,
                'description' => $value->description,
                'flow_code' => $value->flow_code,
                'dependency' => $value->dependency,
                'name' => $value->name,
                'step' => $value->step,
                'usage' => 0,
                'disabled' => false,
            );

            if(strtolower($odrType) === 'smallparcel') {
                // Auto check Assign CSR
                if(in_array($value->flow_code, ['CSR']))
                    $odrFlowItem['usage'] = 1;
            }

            $orderFlow[] = json_encode($odrFlowItem);
        }

        return json_encode($orderFlow);
    }
}