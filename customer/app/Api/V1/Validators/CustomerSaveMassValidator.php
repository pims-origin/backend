<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 29-Jun-16
 * Time: 14:37
 */
namespace App\Api\V1\Validators;

class CustomerSaveMassValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            // Customer
            'cus_id'                          => 'exists:customer,cus_id',
            'cus_name'                        => 'required',
            'cus_billing_account'             => 'required',
            'cus_status'                      => 'required|exists:cus_status,cus_sts_code',
            'cus_code'                        => 'required',
            'zones'                           => 'array',
            'zones.*.zone_id'                 => 'exists:zone,zone_id',
            'warehouses'                      => 'array',
            'warehouses.*.whs_id'             => 'exists:warehouse,whs_id',

            // Address
            'addresses'                       => 'required|array',
            'addresses.*.cus_add_type'        => 'required',
            'addresses.*.cus_add_line_1'      => 'required',
            'addresses.*.cus_add_city_name'   => 'required',
            'addresses.*.cus_add_state_id'    => 'required',
            'addresses.*.cus_add_postal_code' => 'required',
            'addresses.*.cus_add_country_id'  => 'required',

            // Contact
            'contacts'                        => 'required|array',
            'contacts.*.cus_ctt_fname'        => 'required',
            'contacts.*.cus_ctt_lname'        => 'required',
            'contacts.*.cus_ctt_email'        => 'required|email',
            'contacts.*.cus_ctt_phone'        => 'required',
            //'contacts.*.cus_ctt_department'   => 'required',

            // Charge Code
            /*'charge_codes'                    => 'required|array',
            'charge_codes.*.whs_id'           => 'required',
            'charge_codes.*.chg_code_id'      => 'required',*/
        ];
    }
}