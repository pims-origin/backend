<?php

namespace Seldat\Wms2\Models;

class Warehouse extends BaseSoftModel
{

    protected $table = 'warehouse';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'whs_id';
    /**
     * @var array
     */
    protected $fillable = [
        'whs_name',
        'whs_status',
        'whs_short_name',
        'whs_code',
        'whs_country_id',
        'whs_state_id',
        'whs_city_name'
    ];

    public function customer()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\Customer', 'customer_warehouse', 'warehouse_id', 'cus_id');
    }


    public function warehouseAddress()
    {
        return $this->belongsTo(__NAMESPACE__ . '\WarehouseAddress', 'whs_id', 'whs_add_whs_id');
    }

    public function warehouseContact()
    {
        return $this->belongsTo(__NAMESPACE__ . '\WarehouseContact', 'whs_id', 'whs_con_whs_id');
    }

    public function warehouseStatus()
    {
        return $this->belongsTo(__NAMESPACE__ . '\WarehouseStatus', 'whs_status', 'whs_sts_code');
    }

    public function systemCountry()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemCountry', 'whs_country_id', 'sys_country_id');
    }

    public function systemState()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemState', 'whs_state_id', 'sys_state_id');
    }
    
    public function cycleHdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\CycleHdr', 'whs_id', 'whs_id');
    }
    
    public function cycleDtl()
    {
        return $this->hasMany(__NAMESPACE__, '\CycleDtl', 'whs_id', 'whs_id');
    }
}
