<?php

namespace App\Http\Middleware\Warehouses;

use Illuminate\Support\Facades\DB;

class WarehouseTimeZone
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        $whsId = array_get($request->route()[2], 'whsId', null);

        $warehouseTimezone = DB::table('whs_meta')
            ->where('whs_qualifier', 'wtz')
            ->where('whs_id', $whsId)
            ->first();

        if ($warehouseTimezone) {
            $timezone = is_array($warehouseTimezone) ? $warehouseTimezone['whs_meta_value'] : object_get($warehouseTimezone, 'whs_meta_value');
            config(['app.timezone' => $timezone]);
            ini_set("date.timezone", $timezone);
            date_default_timezone_set( $timezone);
        }

        return $next($request);
    }
}
