<?php

namespace App\Api\OUTBOUND\Transformers;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\WavePickDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\Status;

class OrderListTransformer extends TransformerAbstract
{
    private $params = [];
    
    public function __construct($params = []) {
        $this->params = $params;
    }
    /**
     * @param $sample
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {

        $ordDetail = new OrderDtlModel();
        $condition = ['odr_id' => $orderHdr->odr_id];
        $sku = array_get($this->params, 'sku', '');
        if(trim($sku) !== '') {
            $sku = trim($sku);
            $condition[] = ['sku', 'LIKE', "%{$sku}%"];
        }
        
        $items = $ordDetail->findWhere($condition);
        $items = $items->toArray();

        $ordCtn = new OrderCartonModel();
        $item_ = [];
        $totalItem = [];

        foreach ($items as $it) {
            $sts = $it['itm_sts'];
            if($sts == NULL){
                $sts = 'NW';
            }
            $item_['ord_dtl_id'] = $it['odr_dtl_id'];
            $item_['sku'] = $it['sku'];
            $item_['size'] = $it['size'];
            $item_['color'] = $it['color'];
            $item_['lot'] = $it['lot'];
            $item_['pack'] = $it['pack'];
            $item_['itm_sts'] = $sts;
            $item_['itm_sts_name'] = Status::getByKey('ORDER-STATUS', $sts);
            $item_['item_id'] = $it['item_id'];

            $actCTNS = (int)$ordCtn->findWhere([
                'odr_dtl_id'   => $it['odr_dtl_id']
            ])->count();

            $actQty = (int)$ordCtn->findWhere([
                'odr_dtl_id'   => $it['odr_dtl_id']
            ])->sum('piece_qty');



            $numCtn = ceil($it['alloc_qty'] / $it['pack']);
            $item_['alloc_piece'] = $it['alloc_qty'] . '/' . $actQty;
            $item_['alloc_carton'] = $numCtn . '/' . $actCTNS;

            $totalItem[] = $item_;
        }

        return [
            'odr_id'       => $orderHdr->odr_id,
            'cus_id'       => $orderHdr->cus_id,
            'odr_num'      => $orderHdr->odr_num,
            'wv_id'        => $orderHdr->wv_id,
            'wv_num'       => $orderHdr->wv_num,
            "cus_odr_num"  => $orderHdr->cus_odr_num,
            'cus_name'     => $orderHdr->cus_name,
            'whs_id'       => $orderHdr->whs_id,
            'odr_sts'      => $orderHdr->odr_sts,
            'odr_sts_name' => Status::getByKey('ORDER-STATUS', $orderHdr->odr_sts),
            'created_at'   => $orderHdr->created_at->format('m/d/Y'),
            'total-sku'    => count($items),
            'items'        => $totalItem
        ];

    }
}
