<?php
/**
 * Created by PhpStorm.
 * User: Xuan Nguyen
 * Date: 12/28/2018
 * Time: 9:43 AM
 */
namespace App\Api\V1\Models;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Setting;

class SettingModel extends AbstractModel
{
    public function __construct(Setting $model = null)
    {
        $this->model = ($model) ?: new Setting();
    }
    public function getSettingByKey($key){
        $res = DB::table("settings")->where('setting_key',$key)->first();
        return $res;
    }
    public function upsertPrinter($key,$input){
        $settingPrint = DB::table("settings")->where('setting_key',$key)->first();
        if(empty($settingPrint)){
            $settingPrint = DB::table("settings")->insert([
                "setting_key"=>$key,
                "setting_value"=>$input
            ]);
        }
        else{
            $data = $settingPrint['setting_value']??"[]";
            $data = json_decode($data,true);
            $dataIns = $input;
            if(count($data)){
                $dataIns = $data;
                for($i=0; $i<count($input); $i++){
                    $isExist = false;
                    for($j=0;$j<count($dataIns);$j++){
                        if($input[$i]['whs_id'] == $dataIns[$j]['whs_id'] && $input[$i]['cus_id'] == $dataIns[$j]['cus_id']){
                            if($input[$i]['printer_ip'] == $dataIns[$j]['printer_ip']){
                                $dataIns[$j]['name'] = $input[$i]['name'];
                            }
                            if($input[$i]['name'] == $dataIns[$j]['name']){
                                $dataIns[$j]['printer_ip'] = $input[$i]['printer_ip'];
                            }
                            $isExist = true;
                            break;
                        }
                    }
                    if(!$isExist){
                        $dataIns[] = $input[$i];
                    }
                }
            }
            DB::table("settings")
                ->where('setting_key',$key)
                ->update([
                    "setting_value"=>json_encode($dataIns)
                ]);
        }
        $settingPrint = DB::table("settings")
            ->where('setting_key',$key)->first();
        if(!empty($settingPrint)){
            $res = [
                "setting_id"=>$settingPrint['setting_id'],
                "setting_key"=>$settingPrint['setting_key'],
                "setting_value"=>!empty($settingPrint['setting_value'])?json_decode($settingPrint['setting_value'],true):[],
            ];
            return $res;
        }
        return $settingPrint;
    }
}