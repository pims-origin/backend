<?php

namespace App\Api\Inventory\Validators;


class RecountValidator extends AbstractValidator
{
    public function rules()
    {
        return [
            'cycle_hdr_id'    => 'required|exists:cycle_hdr,cycle_hdr_id',
        ];

    }
}