<?php

namespace App\Modules\GoodsReceipts\Eloquents;

class GoodsReceipt extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gr_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gr_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'ctnr_id',
        'asn_hdr_id',
        'gr_hdr_seq',
        'gr_hdr_ept_dt',
        'gr_hdr_num',
        'whs_id',
        'cus_id',
        'gr_in_note',
        'gr_ex_note',
        'gr_sts',
        'putter',
        'ctnr_num',
        'ref_code',
        'putaway',
        'created_from'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function asnHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnHdr', 'asn_hdr_id', 'asn_hdr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goodsReceiptStatus()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptStatus', 'gr_sts', 'gr_sts_code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function goodsReceiptDetail()
    {
        return $this->hasMany(__NAMESPACE__ . '\GoodsReceiptDetail', 'gr_hdr_id', 'gr_hdr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function container()
    {
        return $this->hasOne(__NAMESPACE__ . '\Container', 'ctnr_id', 'ctnr_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    /**
     * @return mixed
     */
    public function updatedBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'updated_by');
    }

    /**
     * @return mixed
     */
    public function putterUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'putter');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function palletSuggestLocation()
    {
        return $this->hasMany(__NAMESPACE__ . '\PalletSuggestLocation', 'gr_hdr_id', 'gr_hdr_id');
    }

    public function cartons()
    {
        return $this->hasMany(__NAMESPACE__ . '\Carton', 'gr_hdr_id', 'gr_hdr_id');
    }

}
