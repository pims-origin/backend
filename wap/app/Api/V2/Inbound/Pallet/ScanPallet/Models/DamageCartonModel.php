<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 7/25/2016
 * Time: 5:36 PM
 */

namespace App\Api\V2\Inbound\Pallet\ScanPallet\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\DamageCarton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class DamageCartonModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     * @param DamageCarton $model
     */
    public function __construct(DamageCarton $model = null)
    {
        $this->model = ($model) ? : new DamageCarton();
    }

    public function checkExistedCartons($ctn_ids)
    {
        $resultCount = $this->model->whereIn('ctn_id', $ctn_ids)->count();

        return $resultCount > 0 ? true: false;
    }

    public function deleteDamagedCartonExistInPallet($pltId) {
        $res = DB::table('damage_carton')
            ->join('cartons', 'cartons.ctn_id', '=', 'damage_carton.ctn_id')
            ->where([
                'cartons.deleted' => 1,
                'cartons.plt_id' => $pltId,
            ])
            ->update([
              'damage_carton.deleted' => 1,
              'damage_carton.deleted_at' => time(),
              'damage_carton.updated_at' => time(),
              'damage_carton.updated_by' => $this->getUserId(),
            ]);
        return $res;
    }

    public function insertDamageCartons($vtlCtns)
    {
        $userId = Data::getCurrentUserId();
        $rfids = array_pluck($vtlCtns, 'ctn_rfid');
        $vtnCtnDamages = DB::table('vtl_ctn')
            ->join('cartons', 'cartons.rfid', '=','vtl_ctn.ctn_rfid')
            ->join('damage_type', 'damage_type.dmg_id', '=', 'vtl_ctn.dmg_id')
            ->where('vtl_ctn.is_damaged', 1)
            ->where('vtl_ctn.deleted', 0)
            ->whereRaw(DB::raw('not exists (select * from damage_carton where damage_carton.ctn_id = cartons.ctn_id)'))
            ->whereIn('vtl_ctn.ctn_rfid', $rfids)
            ->select([
                'damage_type.dmg_id',
                'damage_type.dmg_name',
                'cartons.ctn_id',
            ])
            ->get();

        $time = time();
        $dataTheme = [
            'created_at' => $time,
            'created_by' => $userId,
            'updated_at' => $time,
            'updated_by' => $userId,
            'deleted'    => 0,
            'deleted_at' => 915148800,
        ];
        $dataInsert = [];
        foreach ($vtnCtnDamages as $vtnCtnDamage) {
           $dataTheme['ctn_id']   = $vtnCtnDamage['ctn_id'];
           $dataTheme['dmg_id']   = $vtnCtnDamage['dmg_id'];
           $dataTheme['dmg_note'] = $vtnCtnDamage['dmg_name'];

           $dataInsert[] = $dataTheme;
        }

        if (count($dataInsert)) {
            if(DB::table('damage_carton')->insert($dataInsert)){
               return count($dataInsert);
           }
           return 0;
        }
        return 0;
    }
}
