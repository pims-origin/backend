<?php

namespace App\Api\V1\Transformers;

use App\Item;
use League\Fractal\TransformerAbstract;


class ItemTransformer extends TransformerAbstract
{
    //return result
    public function transform(Item $item)
    {

        return [
            'item_id'       => object_get($item, 'item_id', null),
            'item_code'     => object_get($item, 'item_code', null),
            'color'         => object_get($item, 'color', null),
            'size'          => object_get($item, 'size', null),
            'pack'          => object_get($item, 'pack', null),
            'des'           => object_get($item, 'description', null),
            'lot'           => object_get($item, 'lot', null),
            'itm_sts'       => object_get($item, 'status', null),
            'sts'           => object_get($item, 'sts', null),
            'ttl'           => object_get($item, 'ttl', 0),
            'cus_upc'       => object_get($item, 'cus_upc', null),
            'width'         => object_get($item, 'width', null),
            'weight'        => object_get($item, 'weight', null),
            'height'        => object_get($item, 'height', null),
            'length'        => object_get($item, 'length', null),
            'uom_id'        => !empty($item->uom_id) ? $item->uom_id : null,
            'uom_name'      => object_get($item, 'systemUom.sys_uom_name', null),
            'uom_code'      => object_get($item, 'systemUom.sys_uom_code', null),
            'allocated_qty' => object_get($item, 'allocated_qty', null),
            'dmg_qty'       => object_get($item, 'dmg_qty', null),
            'sku'           => object_get($item, 'sku', null),
            'level_id'      => object_get($item, 'level_id', null),
        ];
    }


}
