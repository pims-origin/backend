<?php

namespace App\Modules\GoodsReceipts\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    public function toArray($request)
    {
        $poDate = ($this->asnDetail->asn_dtl_po_dt && $this->asnDetail->asn_dtl_po_dt != 0) ? date('Y-m-d', $this->asnDetail->asn_dtl_po_dt) : NULL;
        $exPiredDate = ($this->expired_dt && $this->expired_dt != 0) ? date('Y-m-d', $this->expired_dt) : NULL;

        return [
            'PO'            => $this->po,
            'PO_date'       => $poDate,
            'SKU'           => $this->sku,
            'Size'          => $this->size,
            'Color'         => $this->color,
            'Lot'           => $this->lot,
            'Description'   => $this->gr_dtl_des,
            'UOM'           => $this->uom_code,
            'Pack'          => $this->pack,
            'expected_qty'  => (int) $this->gr_dtl_ept_qty_ttl,
            'expected_ctns' => (int) $this->gr_dtl_ept_ctn_ttl,
            'actual_qty'    => (int) $this->gr_dtl_act_qty_ttl,
            'actual_ctns'   => (int) $this->gr_dtl_act_ctn_ttl,
            'damaged_qty'   => (int) $this->gr_dtl_dmg_ttl * $this->pack,
            'damaged_ctns'  => (int) $this->gr_dtl_dmg_ttl,
            'xdock_qty'     => (int) $this->crs_doc * $this->pack,
            'xdock_ctns'    => (int) $this->crs_doc,
            'expired_date'  => $exPiredDate,
        ];
    }
}