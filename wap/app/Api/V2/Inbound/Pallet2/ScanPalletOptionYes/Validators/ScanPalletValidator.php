<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 22/July/2016
 * Time: 11:05
 */

namespace App\Api\V2\Inbound\Pallet2\ScanPalletOptionYes\Validators;

class ScanPalletValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'data.asn_dtl_id'  => 'required',
            'data.pallet-rfid' => 'required',
            'data.ctn-rfid'    => 'required',
        ];
    }
}
