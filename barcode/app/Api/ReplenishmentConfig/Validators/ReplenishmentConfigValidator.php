<?php

namespace App\Api\ReplenishmentConfig\Validators;

use App\Api\ReplenishmentConfig\Models\CartonModel;
use App\Api\ReplenishmentConfig\Models\LocationModel;
use App\Api\ReplenishmentConfig\Models\PalletModel;
use Validator;
use Dingo\Api\Exception\ValidationHttpException;
use Dingo\Api\Exception\UnknownVersionException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Seldat\Wms2\Utils\Message;

/**
 * Class SourceMessageValidator
 *
 * @package App\Api\V1\Validators
 */
class ReplenishmentConfigValidator extends AbstractValidator
{
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * ReplenishmentConfigValidator constructor.
     *
     * @param PalletModel $palletModel
     * @param LocationModel $locationModel
     * @param CartonModel $cartonModel
     */
    //public function __construct(
    //    PalletModel $palletModel,
    //    LocationModel $locationModel,
    //    CartonModel $cartonModel
    //) {
    //    $this->palletModel = $palletModel;
    //    $this->locationModel = $locationModel;
    //    $this->cartonModel = $cartonModel;
    //
    //}

    protected function rules()
    {
        return [
            'loc_code_from' => 'required',
            'picking_qty'   => 'required',

        ];
    }

    /**
     * @param $fromLocId
     * @param $toLocId
     *
     * @return mixed
     * @throws \Exception
     */
    public function check($fromLocId, $toLocId)
    {
        $this->palletModel = new PalletModel;
        $this->locationModel = new LocationModel;
        $this->cartonModel = new CartonModel;

        // Check from Location status if is allocated (AL)
        $fromLocInfo = $this->locationModel->getFirstWhere(['loc_id' => $fromLocId]);
        if (empty($fromLocInfo)) {
            throw new HttpException(400, Message::get("BM017", "Old Location"));
        }
        if ($fromLocInfo->loc_sts_code != 'AC') {
            throw new \Exception('Status of Old Location is not Active!');
        }

        // Check to Location status if is allocated (AL)
        $toLocInfo = $this->locationModel->getFirstWhere(['loc_id' => $toLocId]);
        if (empty($toLocInfo)) {
            throw new HttpException(400, Message::get("BM017", "New Location"));
        }
        if ($toLocInfo->loc_sts_code != 'AC') {
            throw new \Exception('Status of New Location is not Active!');
        }

        // Load loction (Current location id) to get name..
        $locationInfo = $this->cartonModel->getFirstWhere(['loc_id' => $fromLocId], ['location']);

        if (empty($locationInfo->location)) {
            //throw new HttpException(400, Message::get("BM017", "Current Location"));
            throw new HttpException(400, "The Old Location don't have any pallet");
        }

        // Load loction (new location id) to get name..
        $newLocationInfo = $this->cartonModel->getFirstWhere(['loc_id' => $toLocId], ['location']);

        //Check new location is not existed.'
        if (empty($newLocationInfo->location)) {
            throw new HttpException(400, "The New Location don't have any pallet");
        }

        // Check Location Type
        if ($newLocationInfo->location->loc_type_id != $locationInfo->location->loc_type_id) {
            throw new HttpException(400, Message::get("VR064", "New Location Type", "Old Location Type"));
        }

        // Check Location Special Handle
        if ($newLocationInfo->location->spc_hdl_code != $locationInfo->location->spc_hdl_code) {
            throw new HttpException(400, Message::get("VR064", "New Location Temperature", "Old Location Temperature"));
        }

        //check New location status
        $this->checkPalletStatus(object_get($newLocationInfo->location, 'loc_sts_code', null), "New Location");

        return $newLocationInfo->location;
    }

    /**
     * @param $loc_sts_code
     * @param $msgLocation
     *
     * @throws \Exception
     */
    public function checkPalletStatus($loc_sts_code, $msgLocation)
    {
        switch ($loc_sts_code) {
            case config('constants.INACTIVE'):
                throw new UnknownVersionException($msgLocation . ' is not active.', null, 1);

            case config('constants.LOCKED'):
                throw new UnknownVersionException($msgLocation . ' is being locked.', null, 2);

            case config('constants.RESERVED'):
                throw new UnknownVersionException($msgLocation . ' is being reserved.', null, 3);

            case 'RG':
                throw new UnknownVersionException($msgLocation . ' is being receiving.', null, 3);

            default:
                break;
        }
    }


}
