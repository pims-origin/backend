<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\BillableHdrModel;
use App\Api\V1\Models\BillableDtlModel;
use Seldat\Wms2\Models\Item;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

class StorageBillModel extends AbstractModel
{
    protected $startDate;
    protected $endDate;
    protected $lastDate;
    protected $cusId;
    protected $whsId;
    protected $type;
    protected $quantity;
    protected $amount;
    protected $refNum;
    protected $billableHdr;
    protected $billableDtl;
    protected $item;
    protected $description;

    public function __construct($cusId, $whsId, $startDate = null, $endDate = null)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->cusId = $cusId;
        $this->whsId = $whsId;
        $this->description = $this->_getLastDateOfMonth(date_format(date_create($this->endDate), 'Y/m/d'));
        $this->type = 'STR'; // Inbound
        $this->billableHdr = new BillableHdrModel();
        $this->billableDtl = new BillableDtlModel();
        $this->lastDate = $this->getLastDate();
        $this->refNum = $this->getRefNum();
        $this->prepareTimeRange();
    }

    private function _getLastDateOfMonth($date)
    {
        $now = Carbon::now();
        $billableDate = new Carbon($date);

        $result = time();
        if ( $now->month != $billableDate->month ){
            $result = strtotime("{$billableDate->year}-{$billableDate->month}-01");
            $result = strtotime('-1 second', strtotime('+1 month', $result));
        }

        return date('Y/m/d', $result);
    }

    public function prepareTimeRange()
    {
        if ( $this->startDate ) {
            $this->startDate = strtotime($this->startDate);
            $this->startDate = ($this->startDate >= $this->lastDate) ? $this->startDate : $this->lastDate;
        } else {
            $this->startDate = $this->getDefaultStartDate();
        }

        if ( $this->endDate ) {
            $this->endDate = strtotime($this->endDate);
        } else {
            $this->endDate = strtotime(date("Y-m-d"));
        }
    }

    public function isValidTimeRange()
    {
        // if ($this->startDate > $this->endDate) {
        //     return false;
        // }

        return true;
    }

    public function getDefaultStartDate()
    {
        // default start date will be equal to lasted billable storage item.
        if ($this->lastDate) {
            return $this->lastDate;
        }

        //if not, get the created date of customer
        $customer = DB::table('customer')->select('created_at')->where('cus_id', $this->cusId)->first();
        return $customer->created_at;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function getLastDate()
    {
        $billDtl = DB::table('billable_dtl')
        ->select('description')
        ->where([
            ['cus_id', $this->cusId],
            ['whs_id', $this->whsId],
            ['ba_type', $this->type]
        ])
        ->orderBy('created_at', 'desc')
        ->first();

        if ( $billDtl && $billDtl->description ) {
            $lastRangeStrArr = explode(' - ', $billDtl->description);
            return strtotime(end($lastRangeStrArr));
        }

        return false;
    }

    public function getChargeCodeList()
    {
        $items = DB::table('cus_sac')
            ->select(DB::raw('sac_id, sac, sac_name, unit_uom, price, discount, ROUND((price * (100 - discount)/100), 2) AS discount_price,range_from, range_to, block, range_uom, calculate_method'))
            ->where([
                ['cus_id', $this->cusId],
                ['whs_id', $this->whsId],
                ['type', $this->type],
                ['deleted', 0],
            ])
            ->orderBy('range_from', 'decs')
            ->get();

        return $items;
    }

    public function getItem($charge)
    {
        $items =  DB::table('daily_inv_rpt as rpt')
                ->select(DB::raw('sum(rpt.cur_ctns) as ctn, sum(rpt.cur_plt) as pl, rpt.created_at as created_at, sum(rpt.cur_volume) as vol, sum(rpt.cur_qty) as pc, item.cube, rpt.item_id, rpt.sku as sku, carton_type.min as min, carton_type.max as max'))
                ->where([
                    ['rpt.cus_id', $this->cusId],
                    ['rpt.whs_id', $this->whsId]
                ])
                ->leftJoin('item', 'item.item_id', '=', 'rpt.item_id')
                ->leftJoin('carton_type', function($join) {
                    $join->on('carton_type.ctt_code', '=', 'rpt.ctn_type')
                         ->on('carton_type.whs_id', '=', 'rpt.whs_id');
                })
                ->where('rpt.inv_dt', $this->endDate)
                ->groupBy('rpt.ctn_type')
                ->get();

        return $items;
    }

    public function generate()
    {
        if ($this->lastDate >= $this->endDate) {
            // throw new \Exception('Last date of storage bill cannot greater than invoice date');
            return [];
        }

        $charges = $this->getChargeCodeList();
        $charges = $this->convertChargeCode($charges);
        if (!$charges->count()) {
            // throw new \Exception('Customer must have charge code');
            return [];
        }

        $detail = [];
        $i = 0;
        $qty = 0;

        foreach ($charges as $key => $charge) {
            if ($charge->sac_name == "STO_FIX"){
                if ($charge->unit_uom == 'MON') {
                    $qty = 1;
                    $detail[$i] = [
                        'cus_id'        => $this->cusId,
                        'whs_id'        => $this->whsId,
                        'ba_type'       => $this->type,
                        'sac_id'        => $charge->sac_id,
                        'sac'           => $charge->sac,
                        'sac_name'      => $charge->sac_name,
                        'ref_num'       => $this->refNum,
                        'qty'           => $qty,
                        'unit_price'    => $charge->price,
                        'discount'      => $charge->discount,
                        'amount'        => $qty * $charge->discount_price,
                        'item'          => "Vol:MONTH" ,
                        'description'   => $this->description
                    ];
                    $this->quantity += $qty;
                    $this->amount += $detail[$i]['amount'];
                    $i++;
                }
                continue;
            }

            $items = $this->getItem($charge);

            if (!$items->count()) {
                // $msg = sprintf('Charge code %s does not charges to any order', $charge->sac_name);
                // throw new \Exception($msg);
                return [];
            }

            $qty = $items->sum('ctn');

            if ($charge->unit_uom == 'CTN' && $charge->range_uom == 'SKU') {
                $qty = $items->filter(function ($item, $key) use ($charge, $qty) {
                    if ($item->max !== Null) {
                        return (float)$item->max <= (float)$charge->range_to && (float)$item->min >= (float)$charge->range_from;
                    } else {
                        return ((float)$item->min <= (float)$charge->range_to && (float)$item->min >= (float)$charge->range_from);
                    }
                })->sum('ctn');
            }

            if ($charge->unit_uom == 'CTN' && $charge->range_uom == 'IH' && $charge->sac == 'S016') {
                $qty = $items->filter(function ($item, $key) {
                    return substr($item->sku, 0, 2) == 'IH';
                })->sum('ctn');
            }

            if ($charge->unit_uom == 'CTN' && $charge->range_uom == 'IH' && $charge->sac == 'S017') {
                $qty = $items->filter(function ($item, $key) {
                    return substr($item->sku, 0, 2) != 'IH';
                })->sum('ctn');
            }

            if ($charge->unit_uom == 'PL') {
                $qty = $items->sum('pl');
            }

            if ($charge->unit_uom == 'PC') {
                $qty = $items->sum('pc');
            }

            if ($charge->unit_uom == 'CUB') {
                $qty = $items->sum('cube') * $items->sum('pc');
            }

            if ( $qty > 0 ) {
                $detail[$i] = [
                    'cus_id'        => $this->cusId,
                    'whs_id'        => $this->whsId,
                    'ba_type'       => $this->type,
                    'sac_id'        => $charge->sac_id,
                    'sac'           => $charge->sac,
                    'sac_name'      => $charge->sac_name,
                    'ref_num'       => $this->refNum,
                    'qty'           => $qty,
                    'unit_price'    => $charge->price,
                    'discount'      => $charge->discount,
                    'amount'        => $qty * $charge->discount_price,
                    'item'          => "Vol:" . $items->sum('vol'),
                    'description'   => $this->description
                ];
                $this->quantity += $qty;
                $this->amount += $detail[$i]['amount'];
                $i++;
            }
        }

        return $detail;
    }

    public function prepareHeaderData()
    {
        # billable header
        return [
            'cus_id' => $this->cusId,
            'whs_id' => $this->whsId,
            'ref_num' => $this->refNum,
            'ba_from' => $this->startDate,
            'ba_to' => $this->endDate,
            'ba_ttl' => $this->quantity,
            'amount_ttl' => $this->amount,
            'ba_type' => $this->type,
            'ba_period' => 'DAY',
        ];
    }

    public function getRefNum()
    {
        $lastRefNum = $this->billableHdr->getFirstWhere([
            // ['cus_id', '=', $this->cusId],
            ['whs_id', '=', $this->whsId],
            ['ref_num', 'like', 'STR%']
        ]);

        if ( !$lastRefNum ) {
            return $this->generateRefNum();
        }

        return $this->generateRefNum($lastRefNum->ref_num);
    }

    public function generateRefNum($refNum = null)
    {
        if (empty($refNum) || date('ym') != explode('-', $refNum)[1]) {
            $prefix = $this->type;
            return $prefix . '-' . date('ym') . '-' . str_pad(1, 5, '0', STR_PAD_LEFT);
        } else {
            return ++$refNum;
        }
    }

    public function saveBillableItem()
    {
        $detail = $this->generate();

        if (empty($detail)) {
            return false;
        }

        $headerData = $this->prepareHeaderData();

        DB::transaction(function () use ($detail, $headerData) {
            $header = $this->billableHdr->create($headerData);
            $array = array_chunk($detail, 500);
            foreach ($array as $value) {
                $header->detail()->createMany($value);
            }
        });
    }

    public function convertChargeCode($charges)
    {
        $charges = $charges->each(function ($item, $key){
            if ($item->block) {
                $item = data_fill($item, 'range_from', $item->block);
                $item = data_fill($item, 'range_to', $item->block);
            }
        });
        return $charges;
    }
}
