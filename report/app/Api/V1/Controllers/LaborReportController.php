<?php
namespace App\Api\V1\Controllers;
use App\Api\V1\Models\AuthenticationService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use App\Api\V1\Transformers\LaborTrackingReportTransformer;
use App\Api\V1\Models\LaborTrackingReportModel;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class LaborReportController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);
        
        parent::__construct($request, $object);

        $this->model = new LaborTrackingReportModel();
    }

    public function popupDetail($whs_id,$owner,$trans_num,$user_id,Request $request)
    {
        try{
            $input = $request->getQueryParams();
            $data= $this->model->popupDetail($whs_id,$owner,$trans_num,$user_id);
            if (!empty($input['export']) && $input['export'] == 1) {
                $title = [
                    'start_time'    => 'Start Time',
                    'end_time'      => 'End Time',
                    'total_time'    => 'Hours',
                ];
                $filePath = "Labor_Tracking_Detail_".$owner."_".$trans_num."_".$user_id."_".time().".csv";
                $this->saveFile($title, $data->toArray(), $filePath);
                die;
            }
            return $data;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
    
    public function search(Request $request, $whsId)
    {
        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', 20);

        $input['whs_id'] = $whsId;

        try {
            $report = $this->model->search($input, [], $limit);

            if (!empty($input['export']) && $input['export'] == 1) {
                
                $title = [
                    'fullname'      => 'User',
                    'cus_name'      => 'Customer',
                    'owner'         => 'Owner',
                    'trans_num'     => 'Trans Number',
                    'po_num'        => 'PO Number',
                    'start_time'    => 'Start Time',
                    'end_time'      => 'End Time',
                    'total_time'    => 'Total Hours',
                    'lt_type'       => 'Type'
                ];

                $filePath = "Labor_Tracking_Report_".time().".csv";
                $reports = $report->toArray();
                foreach ($reports as &$report) {
                    $report['total_time'] = $this->roundUp($report['total_time']);
                    $report['start_time'] = date('Y/m/d H:i:s', array_get($report, 'start_time', 0));
                    $report['end_time'] = date('Y/m/d H:i:s', array_get($report, 'end_time', 0));
                }

                $this->saveFile($title, $reports, $filePath);

                die;
            }

            return $this->response->paginator($report, new LaborTrackingReportTransformer());
           

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function roundUp($number)
    {
        $whole = floor(floatval($number));
        $fraction = $number - $whole;
        if ($fraction == 0) {
            return number_format($whole, 1);
        }
        if ($fraction <= 0.5) {
            $whole += 0.5;
        } else {
            $whole += 1;
        }
        return number_format($whole, 1);
    }

    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->openToBrowser($filePath); // stream data directly to the browser

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();
    }

}
