<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class LocationService extends BaseService
{
    /**
     * @param $params
     * @param $warehouseId
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create($params, $warehouseId)
    {
        $uri = $this->getUri($warehouseId);

        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    public function getDetail($warehouseId, $id)
    {
        $uri = $this->getUri($warehouseId) . '/' . $id;

        $response = $this->client->get($uri);

        return $response;
    }

    public function edit($params, $warehouseId, $locationId)
    {
        $uri = $this->getUri($warehouseId) . '/' . $locationId;

        $options = [
            'form_params' => $params
        ];

        return $this->client->put($uri, $options);
    }

    public function search($queryStr, $warehouseId, $pre = '')
    {
        $uri = $this->getUri($warehouseId, $pre);
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }


    public function getPalletInfo($queryStr, $warehouseId, $locId, $pre = '')
    {
        $uri = $this->getUriPallet($warehouseId)."/locations/$locId/get-pallet";
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }


    public function destroy($warehouseId, $locationId)
    {
        $uri = $this->getUri($warehouseId) . '/' . $locationId;

        return $this->client->delete($uri);
    }


    public function destroyMultiple($input, $warehouseId)
    {
        $uri = $this->getUri($warehouseId);

        $options = empty($input['ids']) ? [] : [
            'form_params' => [
                'loc_id' => explode(',', $input['ids'])
            ]
        ];

        return $this->client->delete($uri, $options);
    }

    public function getLocations($zoneId, $queryStr)
    {
        $uri = env('API_WAREHOUSE') . 'zones/' . $zoneId . '/locations';
        $uri = sprintf('%s?%s', $uri, $queryStr);

        return $this->client->get($uri);
    }

    public function addLocations($params, $zoneId)
    {
        $uri = env('API_WAREHOUSE') . 'zones/' . $zoneId . '/locations/save-mass-and-delete-unused';

        if (!empty($params['loc_id'])) {
            $params['loc_id'] = explode(',', $params['loc_id']);
        }

        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    public function validateImportFile($params, $whsId)
    {
        set_time_limit(0);
        $ext = pathinfo($params['file']->getClientOriginalName(), PATHINFO_EXTENSION);
        $uri = env('API_WAREHOUSE') . 'locations/validate-import';
        $options = [
            [
                'name'     => 'warehouse_id',
                'contents' => $whsId
            ],
            [
                'name'     => 'file_type',
                'contents' => $ext
            ],
            [
                'name'     => 'file',
                'contents' => fopen($params['file']->getPathName(), 'rb')
            ]
        ];

        return $this->client->post($uri, ['multipart' => $options]);
    }

    public function getErrFile($fileName)
    {
        $uri = env('API_WAREHOUSE') . 'locations/download/' . $fileName;
        $this->client->get($uri, ['sink' => storage_path($fileName)]);
    }

    public function processImport()
    {
        $uri = env('API_WAREHOUSE') . 'locations/process-import';

        return $this->client->get($uri);
    }

    public function getStatusImport()
    {
        $uri = env('API_WAREHOUSE') . 'locations/import-status';

        return $this->client->get($uri);
    }

    public function changeStatus($input, $warehouseId, $locationId, $userId)
    {
        $uri = $this->getUri($warehouseId) . '/' . $locationId . '/change-status';

        $input['updated_by'] = $input['created_by'] = $userId;
        $input['loc_sts_dtl_from_date'] = strtotime($input['loc_sts_dtl_from_date']);
        $input['loc_sts_dtl_to_date'] = strtotime($input['loc_sts_dtl_to_date']);

        $options = [
            'form_params' => $input
        ];

        return $this->client->post($uri, $options);
    }

    public function getChangeStatus($warehouseId, $locationId)
    {
        $uri = $this->getUri($warehouseId) . '/' . $locationId . '/change-status';

        return $this->client->get($uri);
    }

    protected function getUri($warehouseId, $pre = '')
    {
        return env('API_WAREHOUSE') . 'warehouses/' . $warehouseId . '/'. $pre .'locations';
    }

    protected function getUriPallet($warehouseId, $pre = '')
    {
        return env('API_WAREHOUSE') . 'warehouses/' . $warehouseId;
    }

}
