<?php

namespace Seldat\Wms2\Models;

class AvailableInventory extends BaseSoftModel
{
    protected $table = 'cartons';

    protected $primaryKey = 'ctn_id';

    protected $fillable = [
        'cus_name',
        'ctnr_num',
        'gr_hdr_num',
        'created_at',
        'item_code',
        'sku',
        'length',
        'width',
        'height',
        'weight',
        'prefix',
        'suffix',
        'size',
        'color',
        'actual_cartons',
        'piece_remain',
    ];

    public function AsnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }
}
