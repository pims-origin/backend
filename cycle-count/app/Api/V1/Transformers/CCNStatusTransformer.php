<?php
/**
 * Created by PhpStorm.
 * User: Dao Tran
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;


class CCNStatusTransformer extends TransformerAbstract
{
    /*
     * Transform item
     * 
     * @param object $item
     * @return array
     */
    public function transform($item)
    {
        return [
            'code'       => $item->code,
            'name'       => $item->name,
        ];

    }

}
