<?php

namespace App\Modules\Inventories\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;
use App\Modules\Inventories\Models\InventoryModel;
use App\Modules\Inventories\Resources\InventoryResource;
use App\Modules\Inventories\Resources\InventoryCollection;

class InventoryController extends Controller
{
    protected $invenModel;

    public function __construct(InventoryModel $invenModel)
    {
        $this->invenModel = $invenModel;
    }

    public function index($whsId, $cusId, Request $request)
    {
        $input = $request->all();

        $inventories = $this->invenModel->getInvenByCus($whsId, $cusId);

        return new InventoryCollection($inventories);
    }
}