<?php

namespace App\Api\V1\Models;

use App\BillableDtl;
use Illuminate\Support\Facades\DB;

class BillableDtlModel extends AbstractModel
{
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new BillableDtl();
    }

    public function deleteMultiByWorkOrderHdrIds($vasNums)
    {
        $billableDtlIds = DB::table('billable_dtl')->join('billable_hdr', 'billable_hdr.ba_id', '=', 'billable_dtl.ba_id')
                        ->whereIn('billable_hdr.ref_num', $vasNums)
                        ->pluck('billable_dtl.ba_dtl_id')
                        ->toArray();

        if (!empty($billableDtlIds))
        {
            $this->deleteById($billableDtlIds);
        }
    }

    public function deleteByBillableItemHdrId($id)
    {
        $query = $this->model->where('ba_id', $id)->pluck('ba_dtl_id')->toArray();

        return $this->model->destroy($query);
    }

    public function forcedeleteMultiByBillableItemHdrIds($ids)
    {
        /*if (!empty($ids))
        {
            $this->model->destroy($ids);
        }*/

        $this->model->whereIn('ba_dtl_id', $ids)->forceDelete();
    }

    public function deleteByOrderNums($orderNums)
    {
        $this->model->whereIn('ref_num', $orderNums)->delete();
        /*$billableDtlIds = DB::table('billable_dtl')->join('billable_hdr', 'billable_hdr.ba_id', '=', 'billable_dtl.ba_id')
                        ->where('billable_hdr.ref_num', $vasNum)
                        ->pluck('billable_dtl.ba_dtl_id')
                        ->toArray();

        if (!empty($billableDtlIds))
        {
            $this->deleteById($billableDtlIds);
        }*/
    }
}
