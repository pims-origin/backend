<?php

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Utils\Message;

class GoodsReceiptControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $goodsReceiptId;
    protected $containerId;
    protected $asnHeaderId;
    protected $warehouseId;
    protected $customerId;

    public function getEndPoint()
    {
        return "/v1/goods-receipts/";
    }

    protected $goodsReceiptParams = [
        'ctnr_id'       => 0,
        'asn_hdr_id'    => 0,
        'gr_hdr_seq'    => 1,
        'gr_hdr_ept_dt' => 1,
        'gr_hdr_num'    => 999,
        'whs_id'        => 0,
        'cus_id'        => 0,
        'gr_in_note'    => 'This is the in note',
        'gr_ex_note'    => 'This is the ex note',
        'gr_sts'        => 'AC',
        'created_at'    => 1465961195,
        'updated_at'    => 1465961195,
        'deleted_at'    => 915148800,
        'deleted'       => 0,
    ];

    public function setUp()
    {
        parent::setUp();
        // create data into DB
        $container = factory(Container::class)->create();
        $ansHeader = factory(AsnHdr::class)->create();

        $this->containerId = $container->ctnr_id;
        $this->asnHeaderId = $ansHeader->asn_hdr_id;
        $this->warehouseId = $ansHeader->whs_id;
        $this->customerId = $ansHeader->cus_id;

        $this->goodsReceiptParams['ctnr_id'] = $this->containerId;
        $this->goodsReceiptParams['asn_hdr_id'] = $this->asnHeaderId;
        $this->goodsReceiptParams['whs_id'] = $this->warehouseId;
        $this->goodsReceiptParams['cus_id'] = $this->customerId;
    }

    public function testShow_Ok()
    {
        $goodsReceiptId = factory(GoodsReceipt::class)->create()->gr_hdr_id;

        // Test Show
        $data = $this->call('GET', $this->getEndPoint() . $goodsReceiptId);

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);
    }

    public function testShow_NotExist_EmptyData()
    {
        // Test Show
        $data = $this->call('GET', $this->getEndPoint() . "999999999999999999999999");

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertEmpty($data->content());
    }

    public function testStore_ExistData_Fail()
    {
        $goodsReceipt = factory(GoodsReceipt::class)->create();

        $asnHeaderId = $goodsReceipt->asn_hdr_id;
        $containerId = $goodsReceipt->ctnr_id;

        factory(AsnDtl::class)->create([
            'asn_hdr_id' => $asnHeaderId,
            'ctnr_id'    => $containerId,
        ]);

        $data = $this->call('POST', $this->getEndPoint(), [
            'asn_hdr_id' => $asnHeaderId,
            'ctnr_id'    => $containerId
        ]);

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM007", "container", "Good receipt"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function testStore_EmptyAsnDetail_Fail()
    {
        $goodsReceipt = factory(GoodsReceipt::class)->create();

        $asnHeaderId = $goodsReceipt->asn_hdr_id;
        $containerId = $goodsReceipt->ctnr_id;

        $data = $this->call('POST', $this->getEndPoint(), [
            'asn_hdr_id' => $asnHeaderId,
            'ctnr_id'    => $containerId
        ]);

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("VR028", "ASN"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function testStore_EmptyField_Fail()
    {
        $data = $this->call('POST', $this->getEndPoint());

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertGreaterThan(0, count(json_decode($data->content(), true)['errors']['errors']));
    }

    public function testStore_AsnHdrIdInvalid_Fail()
    {
        $data = $this->call('POST', $this->getEndPoint(), [
            'asn_hdr_id' => 'abc',
            'ctnr_id'    => 1
        ]);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            'The asn hdr id must be an integer.',
            json_decode($data->content())->errors->errors->asn_hdr_id[0]
        );
    }

    public function testStoreNotExitAsnHrId_Fail()
    {

        $data = $this->call('POST', $this->getEndPoint(), [
            'asn_hdr_id' => '98979',
            'ctnr_id'    => 1
        ]);

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "ASN"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function testStore_CtnrIdInvalid_Fail()
    {
        $data = $this->call('POST', $this->getEndPoint(), [
            'asn_hdr_id' => 1,
            'ctnr_id'    => 'abc',
        ]);

        // Case 1: Response code is 422
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        // Case 2: Show message
        $this->assertEquals(
            'The ctnr id must be an integer.',
            json_decode($data->content())->errors->errors->ctnr_id[0]
        );
    }

    public function testUpdate_Ok()
    {
        //$goodsReceipt = factory(GoodsReceipt::class)->create();
        //
        //$asnHeaderId = $goodsReceipt->asn_hdr_id;
        //$containerId = $goodsReceipt->ctnr_id;
        //
        //factory(AsnDtl::class)->create([
        //    'asn_hdr_id' => $asnHeaderId,
        //    'ctnr_id'    => $containerId,
        //]);
        //
        //$data = $this->call('PUT', $this->getEndPoint() . $goodsReceipt->gr_hdr_id, [
        //    'gr_in_note' => 'internal note',
        //    'gr_ex_note' => 'external note',
        //    'gr_sts'     => 'RG',
        //]);dd($data);
        //
        //// Case 1: Response code is 200
        //$this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
        //
        //// Case 2: Data is not null.
        //$this->assertNotNull(json_decode($data->content(), true)['data']);
    }


    public function testUpdate_NotExistGr_Fail()
    {
        $data = $this->call('PUT', $this->getEndPoint() . '99999999999999999999999', ['gr_sts' => 'RG']);

        // Case 1: Response code is 400
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_BAD_REQUEST);

        // Case 2: Show message
        $this->assertEquals(
            Message::get("BM017", "Goods Receipt"),
            json_decode($data->content(), true)['errors']['message']
        );
    }

    public function testSearch_Ok()
    {
        // Test Search
        $this->call('GET', $this->getEndPoint());

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }

    public function testSearch_WithParams_Ok()
    {
        // Test Search
        $this->call('GET', $this->getEndPoint(), [
            'sku'     => 'a',
            'cus_id'  => '',
            'item_id' => '',
        ]);

        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $asnHdr = factory(AsnHdr::class)->create();
        $asnDtl = factory(AsnDtl::class)->create([
            'asn_hdr_id' => $asnHdr->asn_hdr_id
        ]);

        $data = $this->call('POST', $this->getEndPoint(), [
            'asn_hdr_id' => $asnDtl->asn_hdr_id,
            'ctnr_id'    => $asnDtl->ctnr_id,
            'gr_in_note' => 'internal note',
            'gr_ex_note' => 'external note',
            'gr_sts'     => 'RG',
        ]);
        // Case 1: Response code is 200
        $this->assertResponseStatus(\Dingo\Api\Http\Response::HTTP_CREATED);

        // Case 2: Data is not null.
        $this->assertNotNull(json_decode($data->content(), true)['data']);
    }

    public function test_getGrStatus_OK()
    {
        $url = "/v1/gr-statuses";

        $response = $this->call('GET', $url);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);


        $this->assertNotEmpty($responseData['data']);
    }
}
