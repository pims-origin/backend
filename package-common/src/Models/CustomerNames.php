<?php

namespace Seldat\Wms2\Models;

class CustomerNames extends BaseSoftModel
{
    protected $table = 'customer';
    protected $primaryKey = 'cus_id';
    protected $fillable = [
        'cus_id',
        'cus_name',
    ];

}
