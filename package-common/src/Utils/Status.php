<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 02-Aug-16
 * Time: 16:40
 */

namespace Seldat\Wms2\Utils;

/**
 * Class Status
 *
 * @package Seldat\Wms2\Utils
 */
class Status
{
    /**
     * @var array
     */
    protected static $configs = [
        'ACTIVE'                        => 'AC',
        'INACTIVE'                      => 'IA',
        'LOCKED'                        => 'LK',
        'RESERVED'                      => 'RS',
        'ASN_PREFIX'                    => 'ASN',
        'GR_PREFIX'                     => 'GDR',
        'RELOCATION_PREFIX'             => 'REL',
        'ASN_STATUS'                    => [
            'NEW'                   => 'NW',
            'RECEIVING'             => 'RG',
            'RCVD-PARTIAL'          => 'RP',
            'RECEIVED'              => 'RE',
            'CANCELLED'             => 'CC',
            'RECEIVING-COMPLETE'    => 'CO'
        ],
        'ASN_DTL_STS'                   => [
            'RECEIVING' => 'RG',
            'NEW'       => 'NW',
            'RECEIVED'  => 'RE',
            'CANCELLED' => 'CC',
        ],
        'GR_STATUS'                     => [
            'RECEIVING'       => 'RG',
            'RECEIVED'        => 'RE',
            'RCVD-DISCREPANT' => 'RD',
            'PENDING'         => 'PD',
            'CANCELLED' => 'CC'
        ],
        'ITEM_STATUS'                   => [
            'ACTIVE'   => 'AC',
            "INACTIVE" => 'IA'
        ],
        'LOCATION_STATUS'               => [
            'ACTIVE'    => 'AC',
            'INACTIVE'  => 'IA',
            'LOCKED'    => 'LK',
            'CYCLE LOC' => 'CL',
            'RESERVED'  => 'RS',
            'RECEIVING' => 'RG'
        ],
        'PALLET_STATUS'                 => [
            'ACTIVE'    =>	'AC',
            'ADJUSTED'  =>	'AJ',
            'ALLOCATED' =>	'AL',
            'CANCELLED' =>	'CC',
            'INACTIVE'  =>	'IA',
            'MOVED'     =>	'MV',
            'NEW'       =>	'NW',
            'PICKED'    =>	'PD',
            'RECEIVING' =>	'RG',
            'TRANSFER'  =>	'TF',
        ],
        'LOCATION_STATUS_REASON'        => [
            'RAC' => 'ACTIVE',
            'RIA' => 'INACTIVE',
            'RLK' => 'LOCKED',
            'RCL' => 'CYCLE LOC',
            'RRS' => 'RESERVED'
        ],
        'CTN_STATUS'                    => [
            'ACTIVE'      => 'AC',
            'INACTIVE'    => 'IA',
            'ADJUSTED'    => 'AJ',
            'LOCKED'      => 'LK',
            'PICKED'      => 'PD',
            'PICKING'     => 'PK',
            'SHIPPED'     => 'SH',
            'TRANSFERRED' => 'TF',
            'PUTBACK'     => 'PB',
            'RECEIVING'   => 'RG',
        ],
        'EVENT'                         => [
            // ASN
            'ASN-NEW'                  => "ANW",
            'ASN-EDITED'               => "AED",
            'ASN-RECEIVING'            => "ARV",
            'ASN-RCVD-PARTIAL'         => "ARP",
            'ASN-COMPLETE'             => "ARC",
            // Goods Receipt
            'GR-RECEIVING'             => "GRP",
            'GR-EXCEPTION'             => "GRX",
            'GR-COMPLETE'              => "GRC",
            'GR-CANCEL'                => 'GRU',
            'PL-AWAY'                  => "PUT",
            'REL-CTN-LOC'              => "LTL",
            'REL-LP-LOC'               => "LPL",
            'COMPLETE-RELOC'           => "RLC",
            'CON-CTN-LOC'              => "CTL",
            'CON-LP-LOC'               => "CTP",
            'COMPLETE-CON'             => "CNC",
            // Order
            'ORDER-NEW'                => "ONW",
            'WORK-ORDER-CREATED'       => "OWN",
            'ORDER-IN-PROGRESS'        => "OIP",
            'ORDER-PICKED'             => "OPI",
            'ORDER-PACKING-COMPLETE'   => "OAC",
            'ORDER-PACKING'            => "OPA",
            'PACKING-COMPLETE'         => "OPC",
            'PACKING'                  => "OPS",
            'ORDER-PALLETIZING'        => "OPG",
            'ORDER-PALLETIZED'         => "OPD",
            'ORDER-STAGING'            => "ORS",
            'ORDER-SHIPPED'            => "OSH",
            'ORDER-COMPLETE'           => "ORC",
            'ORDER-BACK-ORDERED'       => "OBO",
            'ORDER-CANCELED'           => "OCN",
            'ORDER-CANCELED-RMA'       => "OCR",
            'ORDER-ON-HOLD'            => "OHD",
            'ORDER-PICKING'            => "OPK",
            'ORDER-COMMENT-EDIT'       => 'OCE',
            'ORDER-UNDO-PACK'          => 'OUP',
            'WAVE-PICK-NEW'            => "WPN",
            'WAVE-PICK-CANCELED'       => "WCA",
            'WAVE-PICK-COMPLETED'      => "WPC",
            'ORDER-ALLOCATED'          => "ORA",
            "PARTIAL-ALLOCATED"        => 'PAL',
            "PARTIAL-PICKING"          => 'PPK',
            "PARTIAL-PICKED"           => 'PIP',
            "PARTIAL-PACKED"           => 'PPA',
            "PARTIAL-STAGING"          => 'PSH',
            "PARTIAL-SHIPPED"          => 'PRC',
            "NEW-BACK-ORDER"           => 'BNW',
            "PACK-ASSIGN-PALLET"       => 'WOP',
            "ASSIGN-ORDER-COMPLETED"   => 'WOH',
            'ASSIGN-PICKER'            => "APR",
            'ASSIGN-PICKERS'           => "APS",
            // XFER
            'NEW-WHS-XFER'             => 'XFN',
            'WHS-XFER-PICKING'         => 'XFP',
            'WHS-XFER-PICKED'          => 'XFD',
            'WHS-XFER-COMPLETE'        => 'XFC',
            'NEW-WHS-XFER-TICKET'      => 'WVP',
            'WHS-XFER-TICKET-PICKED'   => 'WVD',
            'WHS-XFER-TICKET-COMPLETE' => 'WVC',
            'PALLET-ASSIGNED'          => 'GRA',
            'SUGGEST-LOCATION'         => 'GSL',
            'PUTAWAY-COMPLETED'        => 'GPU'

        ],
        'EVENT-INFO'                    => [
            // ASN
            'ASN-NEW'                  => "%s created",
            'ASN-EDITED'               => "%s Edited",
            'ASN-RECEIVING'            => "%s receiving",
            'ASN-RCVD-PARTIAL'         => "%s received partial",
            'ASN-COMPLETE'             => "%s received",
            'ASN-ITEM-CANCEL'          => "%s cancel %d item",
            // GOODS RECEIPT
            'GR-RECEIVING'             => "Goods Receipt started for %s",
            'GR-EXCEPTION'             => "% received partial",
            //'GR-COMPLETE'            => "Goods Receipt %s created for %s",
            'GR-COMPLETE'              => 'Received %d Cartons',
            'GR-RECEIVED'              => "%s received",
            'GR-CANCEL'                => '',
            'PL-AWAY'                  => "Move to %s",
            'REL-CTN-LOC'              => "Change carton %s location from %s to %s",
            'REL-LP-LOC'               => "Change License Plate %s location from %s to %s",
            'COMPLETE-RELOC'           => "Completed Relocation %s",
            'CON-CTN-LOC'              => "Change carton %s location from %s to %s",
            'CON-LP-LOC'               => "Change carton %s licence plate from %s to %s",
            'COMPLETE-CON'             => "Completed Consolidation %s",
            // ORDER
            'ORDER-NEW'                => "New order %s created",
            'WORK-ORDER-CREATED'       => "Work Order %s created",
            'ORDER-IN-PROGRESS'        => "",
            'ORDER-PICKING'            => "%s Picking",
            'ORDER-PICKED'             => "%s Picking  Completed",
            'ORDER-PACKING-COMPLETE'   => "%s Packing Completed",
            'ORDER-PACKING'            => "%s Packing",
            'PACKING-COMPLETE'         => "Pack Carton %s Completed",
            'PACKING'                  => "Pack Carton %s Created",
            'ORDER-PROCESSING'         => "",
            'ORDER-PALLETIZING'        => "Order %s palletizing",
            'ORDER-PALLETIZED'         => "Order %s palletized",
            'ORDER-STAGING'            => "Order %s Staging",
            'ORDER-SHIPPED'            => "Order %s shipped",
            'ORDER-COMPLETE'           => "Order %s Completed",
            'ORDER-CANCELED'           => "Order %s Cancel",
            'ORDER-CANCELED-RMA'       => "Order %s Cancel RMA",
            'WAVE-PICK-NEW'            => "Wave Pick %s for Order %s created",
            'WAVE-PICK-CANCELED'       => "Wave Pick %s for Order %s canceled",
            'WAVE-PICK-COMPLETED'      => "Wave Pick %s for Order %s completed",
            'ORDER-ALLOCATED'          => "%s Allocated",
            'PALLET-ASSIGNED'          => "Pallet assigned for %s",
            'SUGGEST-LOCATION'         => "Suggest %d location(s) for %d pallet(s)",
            'PUTAWAY-COMPLETED'        => "Put %d pallet(s) on Rack completed",
            'ASSIGN-PICKER'            => "Assign picker %s to wavepick %s",
            'ASSIGN-PICKERS'           => "Assign picker %s to SKU(s) %s",
            // Partial
            'PAL'                      => "%s Allocated Partial",
            'PPK'                      => "%s Picking Partial",
            'PIP'                      => "%s Picked Partial",
            'PPA'                      => "%s Packed Partial",
            'PSH'                      => "%s Stagged Partial",
            'PRC'                      => "%s Shipped Partial",
            'BNW'                      => "%s Created from %s",
            // XFER
            'NEW-WHS-XFER'             => 'WHS created for %s pieces (%s, %s) to be transfered from location %s
            to location %s',
            'WHS-XFER-PICKING'         => 'Xfer picking  for %s pieces  (%s, %s,%s, %s) transfered from location %s to location %s',
            'WHS-XFER-PICKED'          => '%s pieces (%s) transfered from location %s to pallet %s',
            'WHS-XFER-COMPLETE'        => '%s pieces (%s) transfered from location %s to location %s',
            'NEW-WHS-XFER-TICKET'      => 'WHS transfer ticket created for %s Xfers',
            'WHS-XFER-TICKET-PICKED'   => 'WHS transfer ticket picked for %s Xfers',
            'WHS-XFER-TICKET-COMPLETE' => 'Xfer ticket completed for %s Xfers'
        ],
        'ORDER-TYPE'                    => [
            'BU'  => 'Bulk',
            'EC'  => 'Ecomm',
            'TK'  => 'Truck',
            'PC'  => 'Processing',
            'BAC' => 'BackOrder',
            'RTL' => 'SmallParcel',
            'XDK' => 'CrossDock',
            'WHS' => 'Transfer Warehouse',
        ],
        'ORDER-STATUS'                  => [
            'NW'  => 'New',
            'AL'  => 'Allocated',
            'PK'  => 'Picking',
            'PD'  => 'Picked',
            'PN'  => 'Packing',
            'PA'  => 'Packed',
            'PTG' => 'Palletizing',
            'PTD' => 'Palletized',
            'ST'  => 'Staging',
            'SH'  => 'Shipped',
            'RS'  => 'Ready to Ship',
            'SS'  => 'Scheduled to Ship',
            'SP'  => 'Shipping',
            'CO'  => 'Completed',
            //'HO'  => 'Hold',
            'CC'  => 'Canceled',
            'MG'  => 'Merged',
            'RMA' => 'Canceled RMA',
            'PCC' => 'Partial Canceled',
            'PAL' => 'Partial Allocated',
            'PPK' => 'Partial Picking',
            'PIP' => 'Partial Picked',
            'PPA' => 'Partial Packing',
            'PAP' => 'Partial Packed',
            'PSH' => 'Partial Staging',
            'PRC' => 'Partial Shipped',
            'XDP' => 'Cross Dock Processing',
            'DG'  => 'Delivering',
            'DD'  => 'Delivered',
        ],
        'PARTIAL-ORDER-STATUS'          => [
            'PAL' => 'Partial Allocated',
            'PPK' => 'Partial Picking',
            'PPD' => 'Partial Picked',
            'PPA' => 'Partial Packed',
            'PST' => 'Partial Staging',
            'PSH' => 'Partial Shipped',
            'PHO' => 'Partial Hold',
            'PCC' => 'Partial Canceled',
        ],
        'WAVEPICK-STATUS'               => [
            'NW' => 'New',
            'AS' => 'Assigned',
            'PK' => 'Picking',
            'CO' => 'Completed'
        ],
        'WAVEPICK-DETAIL-STATUS'        => [
            'NW' => 'New',
            'PK' => 'Picking',
            'PD' => 'Picked',
            'CO' => 'Completed',
            'CC' => 'Canceled',
        ],
        'PACK-TYPE'                     => [
            'AC' => 'Active',
            'IA' => 'Inactive',
            'CT' => 'Carton',
            'PL' => 'Pallet'
        ],
        'PACK-STATUS'                   => [
            'AC' => 'Active',
            'IA' => 'Inactive',
            'NW' => 'New',
            'AS' => 'Assigned'
        ],
        'SHIP-STATUS'                   => [
            'NW' => 'New',
            'FN' => 'Final'
        ],
        'XFER'                          => [
            'XFN' => 'Xfer Created',
            'XFP' => 'Xfer Picking',
            'XFC' => 'Xfer Completed',
            'WVP' => 'Xfer Ticket Created',
            'WVC' => 'Xfer Ticket Completed'
        ],
        'XFER-STATUS'                   => [
            'PD' => 'Pending',
            'PK' => 'Picking',
            'PI' => 'Picked',
            'CO' => 'Completed'
        ],
        'TICKET-STATUS'                 => [
            'PK' => 'Picking',
            'PI' => 'Picked',
            'CO' => 'Completed'
        ],
        //DROPDOWN
        'ship-ready'                    => [
            '1' => 'Today',
            '3' => 'Within 3 days',
            '7' => 'Within 1 week',
        ],
        'delivery-service'              => [
            'NA' => 'NA',
            'FE'  => 'FedEx',
            'UPS' => 'UPS',
            'LTL' => 'LTL',
        ],
        'trailer-load'                  => [
            'NA' => 'NA',
            'BS' => 'By Shipper',
            'BD' => 'By Driver',
        ],
        'freight-counted'               => [
            'NA' => 'NA',
            'BS' => 'By Shipper',
            'BD' => 'By Driver/Pallets said contain',
            'BP' => 'By Driver/Pieces',
        ],
        'freight-charge-terms'          => [
            'NA' => 'NA',
            'PR' => 'Prepaid',
            '3P' => '3rd Party',
            'CO' => 'Collect',
        ],
        'fee-terms'                     => [
            'NA' => 'NA',
            'PR' => 'Prepaid',
            'CO' => 'Collect',
        ],
        'shipment-method'               => [
            'CA' => 'Advance Collect',
            'CC' => 'Collect',
            'CD' => 'Collect on Delivery',
            'CF' => 'Collect, Freight Credited Back to Customer',
            'CN' => 'Cost and Freight',
            'DD' => 'Delivered Duty Paid',
            'DE' => 'Per Contract',
        ],
        'VIRTUAL-CARTON-SUMMARY-STATUS' => [
            'NW' => 'NEW',
            'RG' => 'RECEIVING',
            'AD' => 'ADJUSTED',
            'RE' => 'RECEIVED',
        ],
        'VIRTUAL-CARTON-STATUS'         => [
            'NW' => 'NEW',
            'GC' => 'GOODS-RECEIPT-COMPLETE',
            'RE' => 'RECEIVED', //Wap completed asn details may have discrepancy
            'AS' => 'ASSIGNED', //update pallet rfid
            'RC' => 'RACKED',  //update loc_rfid
            'AD' => 'ADJUSTED' //deleted = 1
        ],
        'ASN-DETAIL-CARTON-STATUS'      => [
            'NW' => 'New',
            'GC' => 'Complete',
            'RE' => 'Received', //Wap completed asn details may have discrepancy
            'AS' => 'Assigned', //update pallet rfid
            'RC' => 'Racked',  //update loc_rfid
            'AD' => 'Adjusted' //deleted = 1
        ],
        'WO_STATUS'                     => [
            'NW' => 'New',
            'CO' => 'Completed',
            'FN' => 'Final',
            'PC' => 'Processing'
        ],
        'LOC_TYPE_CODE'                 => [
            'SHP' => 'SHIPPING',
            'RAC' => 'RACK',
            'STG' => 'SHIPPING-STAGING',
            'REC' => 'RECEIVING',
            'PIC' => 'PICKING',
            'ECO' => 'E-COMMERCE',
            'PRO' => 'PROCESSING',
            'PAW' => 'PUT-AWAY',
        ],
        'BAY_STATUS'                    => [
            'A' => 'Available',
            'U' => 'Using',
        ],
        'CONTAINER_CHARGE_STATUS'       => [
            'O' => 'Open',
            'C' => 'Closed',
        ],
        'GATE_STATUS'                   => [
            'A' => 'Available',
            'U' => 'Using',
        ],
        'GATE_DTL_STATUS'               => [
            'O' => 'Open',
            'C' => 'Closed',
        ],
        'ODR_CTN_STATUS'                => [
            'PK' => 'Picking', // WAP pick and not assign to order
            'PD' => 'Picked', // assign to order
            'SP' => 'Shipped', // update order to be shipped
            'AS' => 'Assigned', //Assign carton to pallet in put back feature
            'PB' => 'Put-Back', //Assign carton to pallet in put back feature
        ],
        'WAVE_DETAIL_STATUS'            => [
            'NW' => 'New',
            'PK' => 'Picking',
            'PD' => 'Picked',
        ],
        'WAVE_HDR_STATUS'               => [
            'NW' => 'New',
            'PD' => 'Picked',
            'PK' => 'Picking',
            'CO' => 'Completed',
        ],
        'RETURN-ORDER'                  => [
            'NW' => 'New',
            'CO' => 'Completed',
            'AS' => 'Assigned',
            'PT' => 'Putting',
            'SG' => 'Suggested'
        ],
        'PUT-BACK-STATUS'               => [
            'NW' => 'New',
            'CO' => 'Completed',
        ],
        'CCN_STATUS'               => [
            'NW' => 'New',
            'CC' => 'Cycle Count',
            'CE' => 'Cancel',
            'CP' => 'Completed',
        ],
        'ITEM-CHARGE-BY'    =>[
            'CF' => 'Cube Feet',
            'CT' => 'Carton',
            'PC' => 'Piece',
        ],
        'GOODS-RECEIPT-TYPE'     =>[
            'TRK' => 'Truck',
            'CTNR' => 'Container'
        ],
        'CUSTOMER-CONFIG-BACKORDER'     =>[
            'SHORT' => 'Short',
            'FULL' => 'Full'
        ],
        'TIS_STATUS' => [
            'NW' => 'New',
            'SC' => 'Scanning',
            'CO' => 'Completed',
            'CC' => 'Canceled',
        ],

    ];

    /**
     * @param $keyOrType
     *
     * @return array|string
     */
    public static function get($keyOrType)
    {
        $configs = array_change_key_case(self::$configs, CASE_UPPER);

        return array_get($configs, strtoupper($keyOrType), null);
    }

    /**
     * @param $keyOrType $key or $type
     * @param null $key
     *
     * @return string|null
     */
    public static function getByKey($keyOrType, $key = null)
    {
        $configs = array_change_key_case(self::$configs, CASE_UPPER);

        if (empty($configs[strtoupper($keyOrType)])) {
            return null;
        }

        if (!empty($key)) {
            $config = is_array($configs[strtoupper($keyOrType)]) ?
                array_change_key_case($configs[strtoupper($keyOrType)], CASE_UPPER) : [];

            return array_get($config, strtoupper($key), null);
        }

        $status = self::$configs[strtoupper($keyOrType)] ?: null;

        return !is_array($status) ? $status : null;
    }

    /**
     * @param $value
     * @param null $type
     *
     * @return string|null
     */
    public static function getByValue($value, $type = null)
    {
        //$configs = array_change_key_case(self::$configs, CASE_UPPER);

        if (!empty($type)) {
            if (empty(self::$configs[strtoupper($type)])) {
                return null;
            }

            $array = self::$configs[strtoupper($type)];

            return array_search(strtoupper($value), array_map('strtoupper', $array));
        }


        return array_search(strtoupper($value), array_map(function ($e) {
            if (!is_array($e)) {
                return strtoupper($e);
            }
        }, self::$configs));
    }
}
