<?php
namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Zone;
use Seldat\Wms2\Utils\SelArr;
use Wms2\UserInfo\Data;

class ZoneModel extends AbstractModel
{
    /**
     * ZoneModel constructor.
     *
     * @param Zone|null $model
     */
    public function __construct(Zone $model = null)
    {
        $this->model = ($model) ?: new Zone();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchAllLocation($attributes = [], $with = [], $limit = null)
    {
        $pallets = (new PalletModel())->loadAllLoc()->toArray();
        $exceptLocIds = array_pluck($pallets, 'loc_id');

        $query = $this->make($with);

        if (!empty($attributes)) {
            $query->whereHas("locationZone.locationType", function ($q) use ($attributes) {
                $q->where('loc_type_code', 'like', $attributes['loc_type']);
            });
        }

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        $query->whereHas("locationZone.locationType", function ($q) use ($exceptLocIds) {
            // Status = AC
            $q->orWhere(function ($q2) use ($exceptLocIds) {
                $q2->where('loc_sts_code', 'AC');
                $q2->whereNotIn('loc_id', $exceptLocIds);
            });

            $q->orWhere(function ($q2) use ($exceptLocIds) {
                $q2->where('loc_sts_code', 'RS');
                $q2->whereIn('loc_id', $exceptLocIds);
            });
        });

        $query->orderBy('zone_name', 'DESC');

        $query->whereHas("customerZone", function ($q) use ($attributes) {
            $q->orderBy('cus_id', array_get($attributes, 'cusId', null));
        });

        $query->whereHas("locationZone", function ($q) use ($attributes) {
            $q->where('loc_whs_id', array_get($attributes, 'whsId', null));
        });

        // Get
        $models = $query->paginate($limit);

        return $models;
    }
}
