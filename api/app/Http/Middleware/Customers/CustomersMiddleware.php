<?php

namespace App\Http\Middleware\Customers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class CustomersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        $cusId = array_get($request->route()[2], 'cusId', null);
        if($cusId != null && is_numeric($cusId) == false) {
            $cusCode = mb_strtoupper($cusId);
            $modelCus = new Customer();
            $cusId = $modelCus->where('cus_code', $cusCode)
                ->where('deleted' , 0)
                ->select('cus_id')
                ->first();
            $cusId = array_get($cusId, 'cus_id', 1);
            $request->merge(['cus_id' => $cusId]);
        }

        $whsId = array_get($request->route()[2], 'whsId', null);
        if($whsId != null && is_numeric($whsId) == false) {
            $whsCode = mb_strtoupper($whsId);
            $modelCus = new Warehouse();
            $whsId = $modelCus->where('whs_code', $whsCode)
                ->where('deleted' , 0)
                ->select('whs_code')
                ->first();
            $whsId = array_get($whsId, 'whs_id', 1);
            $request->merge(['whs_id' => $whsId]);
        }

        // middleware cus of user
        try {
            $userId = Config::get('data.userId');
            //get list
            // $cusExUsModel = new CusExUser();
            $cusInUsModel = new CusInUser();
            $modelCus = new Customer();

            // $listData =  DB::table($cusExUsModel->getTable())
            //     ->join($modelCus->getTable(), $modelCus->getTable() . '.cus_id', '=', $cusExUsModel->getTable() .'.cus_id')
            //     ->where($cusExUsModel->getTable() . '.user_id',$userId)
            //     ->where($modelCus->getTable(). '.deleted' , 0)
            //     ->select($cusExUsModel->getTable() . '.cus_id', $cusExUsModel->getTable() . '.whs_id')
            //     ->get();
            $listData =  DB::table($cusInUsModel->getTable())
                ->join($modelCus->getTable(), $modelCus->getTable() . '.cus_id', '=', $cusInUsModel->getTable() .'.cus_id')
                ->where($cusInUsModel->getTable() . '.user_id',$userId)
                ->where($modelCus->getTable(). '.deleted' , 0)
                ->select($cusInUsModel->getTable() . '.cus_id', $cusInUsModel->getTable() . '.whs_id')
                ->get();
            Config::set('data.cusOfUser', $listData);
        }
        catch (\Exception $e) {
            throw new \Exception('Unauthorized');
        }

        $response = $next($request);
        
        //response time
        $lumen_end = microtime(true);
        LogsResponseTime::create([
            'rpt_url'           => $request->url(),
            'user_id'       => Config::get('data.userId'),
            'rpt_start'         => LUMEN_START,
            'rpt_end'           => $lumen_end,
            'rpt_time'          => $lumen_end - LUMEN_START
        ]);
        return $response;
    }
}
