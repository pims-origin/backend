<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 29-August-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Carton;

class CartonModel extends AbstractModel
{
    /**
     * @param Carton $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    /**
     * @param $locationIds
     *
     * @return mixed
     */
    public function checkWhereIn($locationIds)
    {
        return $this->model
            ->whereIn('loc_id', $locationIds)
            ->count();
    }

}
