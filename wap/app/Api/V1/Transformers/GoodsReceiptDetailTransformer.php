<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GoodsReceipt;

class GoodsReceiptDetailTransformer extends TransformerAbstract
{
    public function transform(GoodsReceipt $goodsReceipt)
    {
        return [
            'asn_hdr_num'          => object_get($goodsReceipt, 'asnHdr.asn_hdr_num', null),
            'asn_hdr_ref'          => object_get($goodsReceipt, 'asnHdr.asn_hdr_ref', null),
            'sys_measurement_code' => object_get($goodsReceipt, 'asnHdr.sys_mea_code', null),
            'gr_hdr_id'            => object_get($goodsReceipt, 'gr_hdr_id', null),
            "user_id"              => $goodsReceipt->created_by,
            "user_name"            => trim(object_get($goodsReceipt, "user.first_name", null) . " " .
                object_get($goodsReceipt, "user.last_name", null)),

            // Container
            'ctnr_id'              => object_get($goodsReceipt, 'ctnr_id', null),
            'ctnr_num'             => object_get($goodsReceipt, 'container.ctnr_num', ''),
            'ctnr_note'            => object_get($goodsReceipt, 'container.ctnr_note', ''),

            'gr_hdr_ept_dt' => date('m/d/Y', object_get($goodsReceipt, 'gr_hdr_ept_dt', 0)),
            'gr_hdr_num'    => object_get($goodsReceipt, 'gr_hdr_num', null),
            // Warehouse Info
            'whs_id'        => object_get($goodsReceipt, 'whs_id', null),

            // Customer Info
            'cus_id'        => object_get($goodsReceipt, 'cus_id', null),
            'cus_code'      => object_get($goodsReceipt, 'customer.cus_code', null),
            'cus_name'      => object_get($goodsReceipt, 'customer.cus_name', null),

            'gr_in_note'  => object_get($goodsReceipt, 'gr_in_note', null),
            'gr_ex_note'  => object_get($goodsReceipt, 'gr_ex_note', null),

            // Goods Receipt Status
            'gr_sts_code' => object_get($goodsReceipt, 'gr_sts', null),
            'gr_sts_name' => object_get($goodsReceipt, 'goodsReceiptStatus.gr_sts_name', ''),
            'gr_sts_desc' => object_get($goodsReceipt, 'goodsReceiptStatus.gr_sts_desc', ''),

            // Goods Receipt Details
            'gr_details'  => object_get($goodsReceipt, 'details', null),
        ];
    }
}

