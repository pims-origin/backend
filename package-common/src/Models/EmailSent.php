<?php

namespace Seldat\Wms2\Models;


class EmailSent extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_sent';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'esent_id';

    /**
     * @var array
     */
    protected $fillable = [
        'owner',
        'func',
        'esent_sts',
        'created_at'
    ];

    public function invEmailed()
    {
        return $this->hasMany(__NAMESPACE__ . '\InvEmailed', 'owner', 'inv_email_id');
    }
}
