<?php

namespace App\Api\V5\Inbound\Pallet\ScanPallet\Models;


use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Utils\SelStr;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * EventTrackingModel constructor.
     */
    public function __construct()
    {
        $this->model = new EventTracking();
    }
}
