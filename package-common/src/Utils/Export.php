<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 02-Aug-16
 * Time: 16:40
 */

namespace Seldat\Wms2\Utils;

use Box\Spout\Writer\WriterFactory;

class Export
{
    /**
     * @param $fileName
     * @param array $title
     * @param array $data
     * @param string $type
     *
     * @return bool
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    public static function showFile($fileName, array $title, array $data, $functionInput = null, $type = "csv")
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create($type);

        $writer->openToBrowser($fileName);

        $dataSave[] = array_values($title);
        set_time_limit(0);
        foreach ($data as $item) {
            $temp = [];
            $functionUsing = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case "*":
                            $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                            break;
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], (int)array_get($item, $values[0], 0));
                            break;
                        case "div":
                            $value = round(array_get($item, $values[0], null) /
                                array_get($item, $values[2], null));
                            break;
                        case "status":
                            $value = Status::getByKey("ORDER-TYPE", array_get($item, $values[0], 0));
                            break;
                        case "function()":
                            $functionName = explode("(", $values[2]);

                            if (empty($functionUsing[$functionName[0]])) {
                                $params = str_replace(" ", "", trim($functionName[1], ")"));
                                $params = explode(",", $params);
                                $args = [];
                                foreach ($params as $fields) {
                                    $args[$fields] = array_get($item, $fields, null);
                                }

                                $functionUsing[$functionName[0]] = call_user_func($functionInput[$functionName[0]],
                                    $args);
                            }

                            $value = array_get($functionUsing[$functionName[0]], $values[0], null);

                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();

        //Excel::create($filePath, function ($file) use ($dataSave) {
        //
        //    $file->sheet('Report', function ($sheet) use ($dataSave) {
        //        //$sheet->setAutoSize(true);
        //        //$sheet->setHeight(1, 30);
        //
        //        //$countData = count($dataSave);
        //        // Fill suggest Data
        //        $sheet->fromArray($dataSave, null, 'A1', true, false);
        //
        //        // Wrap Text
        //        //$sheet->getStyle("N1:M" . ($countData))->getAlignment()->setWrapText(true);
        //
        //        //$sheet->setBorder('A1:M' . ($countData), 'thin');
        //        //$sheet->cell("A1:M1", function ($cell) {
        //        //    $cell->setAlignment('center');
        //        //    $cell->setFontWeight();
        //        //    $cell->setBackground("#3399ff");
        //        //    $cell->setFontColor("#ffffff");
        //        //});
        //
        //        //$sheet->cell("A1:M" . ($countData), function ($cell) {
        //        //    $cell->setValignment('center');
        //        //});
        //
        //        //$columsAlign = [
        //        //    "A1:C" . $countData => "left",
        //        //    "D1:J" . $countData => "center",
        //        //    "L1:M" . $countData => "center",
        //        //    "K1:K" . $countData => "left",
        //        //    "N1:M" . $countData => "left"
        //        //];
        //
        //        //foreach ($columsAlign as $cols => $align) {
        //        //    $sheet->cell($cols, function ($cell) use ($align) {
        //        //        $cell->setAlignment($align);
        //        //    });
        //        //}
        //
        //    });
        //})->store('xlsx', storage_path());
    }

    /**
     * @param $filePath
     * @param array $title
     * @param array $data
     * @param string $type
     *
     * @return bool
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    public static function saveFile($filePath, array $title, array $data, $type = "csv")
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create($type);

        $writer->openToFile($filePath);

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case "*":
                            $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                            break;
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();
    }
}