<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\StyleLocations;
use Seldat\Wms2\Utils\SelArr;

class StyleLocationsModel extends AbstractModel
{
    public function __construct(StyleLocations $model = null)
    {
        $this->model = ($model) ?: new StyleLocations();
    }

    /*
    ***************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with)
            ->where('cartons.whs_id', $attributes['whs_id']);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $attributes['compare_operator']['ctnr_num'] = 'like';
        $attributes['compare_operator']['loc_code'] = 'like';
        $attributes['compare_operator']['upc'] = 'like';
        $attributes['compare_operator']['sku'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'cus_id',
                'whs_id',
                'ctnr_num',
                'loc_code',
                'item_code',
                'sku',
            ],
            'attributes' => $attributes,
            'tablePreface' => [
                'cus_id' => NULL,
            ],
        ]);

        if (! isset($attributes['autocomplete'])) {
            $query->groupBy([
                'cus_id',
                'plt_id',
                'loc_id',
                'ctn_sts',
                'ctnr_id',
                'item_id',
                'ctn_pack_size',
                'po',
                'lot',
                'color',
                'size',
                'length',
                'width',
                'height',
                'weight',
            ]);
        }

        return $this->runSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

    /*
    ***************************************************************************
    */

}
