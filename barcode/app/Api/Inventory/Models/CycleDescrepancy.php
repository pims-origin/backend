<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 20/10/2016
 * Time: 14:13
 */

namespace App\Api\Inventory\Models;

use App\Api\Inventory\Models\CycleDtlModel;
use App\Api\Inventory\Models\CycleHdrModel;

class CycleDescrepancy extends AbstractModel
{
    /**
     * @var CycleHdrModel
     */
    protected $cycleHdrModel;

    /**
     * @var CycleDtlModel
     */
    protected $cycleDtlModel;

    public function __construct(CycleHdrModel $cycleHdrModel, CycleDtlModel $cycleDtlModel)
    {
        $this->cycleHdrModel = $cycleHdrModel;
        $this->cycleDtlModel = $cycleDtlModel;
    }

}