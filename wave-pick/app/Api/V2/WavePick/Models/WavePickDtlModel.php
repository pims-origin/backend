<?php

namespace App\Api\V2\WavePick\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\Status;

class WavePickDtlModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickDtl $model = null)
    {
        $this->model = ($model) ?: new WavepickDtl();
    }

    public function loadData($wv_id)
    {
        return $this->model->select([
            'wv_dtl.item_id as ITEM_ID',
            'wv_dtl.sku as SKU',
            'wv_dtl.color as Color',
            'wv_dtl.size as SIZE',
            'wv_dtl.lot as Lot',
            'wv_dtl.pack_size as Pack_size',
            'wv_dtl.ctn_qty as Cartons',
            'wv_dtl.piece_qty as Picking_QTY',
            'wv_dtl.primary_loc_id',
            'wv_dtl.primary_loc',
            'wv_dtl.bu_loc_1',
            'wv_dtl.bu_loc_2',
            'wv_dtl.bu_loc_3',
            'wv_dtl.bu_loc_4',
            'wv_dtl.bu_loc_5',
            'wv_dtl.bu_loc_1_id',
            'wv_dtl.bu_loc_2_id',
            'wv_dtl.bu_loc_3_id',
            'wv_dtl.bu_loc_4_id',
            'wv_dtl.bu_loc_5_id',
            'wv_dtl.cus_id',
            'wv_dtl.wv_dtl_id',
            'wv_dtl.pick_pallet',
            //'wv_dtl.uom_code',
            'i.uom_code',
            'wv_dtl.wv_num',
            'i.description'
        ])
            ->join('item AS i', 'i.item_id', '=', 'wv_dtl.item_id')
            ->where('wv_dtl.wv_id', '=', $wv_id)
            ->where('wv_dtl.piece_qty', '>', 0)
            ->distinct()
            ->get();
    }

}
