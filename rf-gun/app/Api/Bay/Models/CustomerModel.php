<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\Bay\Models;

use Seldat\Wms2\Models\Customer;

class CustomerModel extends AbstractModel
{
    /**
     * BayModel constructor.
     *
     * @param Bay|null $model
     */
    public function __construct(Customer $model = null)
    {
        $this->model = ($model) ?: new Customer();
    }

    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $this->sortBuilder($query, $attributes);
        $this->model->filterData($query, true);

        return $query->paginate($limit);
    }

    public function getCusByIds($cusId)
    {
        $rows = $this->model
            ->where('cus_id', $cusId)
            ->first();

        return $rows;
    }
}
