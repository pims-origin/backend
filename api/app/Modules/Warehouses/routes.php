<?php
$middleware = ['app', 'user', 'whs'];
$router->group(['prefix' => '/rest/wms360/seldat/v1', 'middleware' => $middleware, 'namespace' => 'App\Modules\Warehouses\Controllers'], function () use ($router) {
    $router->get('/warehouses','WarehouseController@index');
    $router->get('/warehouses/{whsId:[a-zA-Z0-9]+}','WarehouseController@show');
   });


