<?php
/**
 * Created by PhpStorm.
 * User: dai.ho
 * Date: 09/05/2016
 * Time: 09:35
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MenuFavoriteModel;
use App\Api\V1\Validators\MenuFavoriteValidator;
use App\Api\V1\Transformers\MenuFavoriteTransformer;
use App\Api\V1\Transformers\MenuTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Dingo\Api\Exception\ValidationHttpException;

class MenuFavoriteController extends AbstractController
{
    /**
     * @var $menuFavoriteModel
     */
    protected $menuFavoriteModel;
    /**
     * @var $menuTransformer
     */
    protected $menuTransformer;
    /**
     * @var $menuFavoriteValidator
     */
    protected $menuFavoriteValidator;
    /**
     * @var $menuFavoriteTransformer
     */
    protected $menuFavoriteTransformer;

    public function __construct()
    {
        parent::__construct();

        $this->menuFavoriteModel = new MenuFavoriteModel();
        $this->menuTransformer = new MenuTransformer();
        $this->menuFavoriteValidator = new MenuFavoriteValidator();
        $this->menuFavoriteTransformer = new MenuFavoriteTransformer();
    }

    /**
     * @SWG\Get(
     *     tags={"Favorite Menu"},
     *     path="/v1/favorites/{userId}",
     *     summary="Menu Favorite List",
     *     description="List All Menu Favorite for User",
     *     operationId="listMenuFavorite",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="userId",
     *         description="User Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Menu List Group",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 description="Menu Item",
     *                 properties={
     *                     @SWG\Property(property="menu_id", type="integer"),
     *                     @SWG\Property(property="name", type="string"),
     *                     @SWG\Property(property="description", type="string"),
     *                     @SWG\Property(property="url", type="string"),
     *                     @SWG\Property(property="target", type="string"),
     *                     @SWG\Property(property="display_order", type="integer"),
     *                     @SWG\Property(property="icon_class", type="string"),
     *                     @SWG\Property(property="permission_name", type="string"),
     *                 },
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_id": 1,
     *                     "name": "Product Page",
     *                     "description": "This is product page from Swagger",
     *                     "url": "http://wms2.seldat.com",
     *                     "target": "Target",
     *                     "display_order": 1,
     *                     "icon_class": "ico",
     *                     "permission_name": "viewMenuList",
     *                 }
     *             }
     *         },
     *     ),
     * )
     *
     * Menu Favorite List
     *
     * @param $userId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($userId)
    {
        $favoriteMenus = $this->menuFavoriteModel->getAllMenuDetailByUserId($userId);

        $result = [];
        foreach ($favoriteMenus as $favoriteMenu) {
            if ($menu = $favoriteMenu->menu) {
                $result[] = $menu;
            }
        }

        return $this->response->collection(collect($result), $this->menuTransformer);
    }

    /**
     * @SWG\Post(
     *     tags={"Favorite Menu"},
     *     path="/v1/favorites/{userId}",
     *     summary="Create Menu Favorite",
     *     description="Create Menu Favorite",
     *     operationId="createMenuFavorite",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="userId",
     *         description="Menu Favorite's User Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="menu_id",
     *         description="Menu Favorite's Menu Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="display_order",
     *         description="Menu Favorite's Display Order",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Return array of Menu Favorite",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 description="Menu Favorite Fields",
     *                 properties={
     *                     @SWG\Property(property="user_id", type="integer"),
     *                     @SWG\Property(property="menu_id", type="integer"),
     *                     @SWG\Property(property="display_order", type="integer"),
     *                 }
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "user_id": 1,
     *                     "menu_id": 1,
     *                     "display_order": 1,
     *                 }
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Menu Favorite Create
     *
     * @param int $userId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(
        $userId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->menuFavoriteValidator->validate($input);

        $params = [
            'user_id'       => $userId,
            'menu_id'       => $input['menu_id'],
            'display_order' => $input['display_order'],
        ];

        try {

            if ($this->menuFavoriteModel->getByUserIdAndMenuId($params['user_id'], $params['menu_id'])) {
                throw new \Exception('Cannot save Favorite Menu, Exist Menu ID belongs user ID.');
            }

            if ($menuFavorite = $this->menuFavoriteModel->create($params)) {
                return $this->response->item($menuFavorite,
                    $this->menuFavoriteTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (ValidationHttpException $e) {
            return $this->response->error(implode("\n", $e->getErrors()->all()),
                IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    /**
     * @SWG\Delete(
     *     tags={"Favorite Menu"},
     *     path="/v1/favorites/{userId}/{menuId}",
     *     summary="Delete Menu Favorite",
     *     description="Delete a Menu Favorite",
     *     operationId="MenuFavoriteId",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="userId",
     *         description="User Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="menuId",
     *         description="Menu Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Menu Favorite was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param int $userId
     * @param int $menuId
     *
     * @return \Dingo\Api\Http\Response
     */
    public function destroy($userId, $menuId)
    {
        try {
            if ($this->menuFavoriteModel->deleteUserMenu($userId, $menuId)) {
                return $this->response->noContent();
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Cannot Delete this item');
    }
}
