<?php

namespace Seldat\Wms2\Models;

class Indicator extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'indicator';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ind_id';


    public $timestamps = false;




}
