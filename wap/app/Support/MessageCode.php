<?php
namespace App;
class MessageCode
{
    protected static $messages = [
        'WAP001'=>'Need input param type for check.',
        'WAP002'=>'There are some thing wrong in input data.',
        'WAP003'=>'There is a %s is invalid.',
        'WAP004'=>'Pallet code %s is invalid.',
        'WAP005'=>'This asn details %d is received.',
        ''=>'',
    ];

    public static function get($code, ...$params)
    {
        $message = array_get(self::$messages, "$code", null);
        foreach ($params as $key => $param) {
            $message = str_replace("{{$key}}", $param, $message);
        }

        return $message;
    }
}