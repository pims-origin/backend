<?php

namespace App\Api\V2\Order\CancelOrder\Traits;

use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

trait OrderHdrControllerTrait
{
    protected $_allow = [
        'NW'  => 'New',
        'AL'  => 'Allocated',
        'PK'  => 'Picking',
        'PN'  => 'Packing',
        'PA'  => 'Packed',
        'ST'  => 'Staging',
        'PAL' => 'Partial Allocated',
        'PPK' => 'Partial Picking',
        'PIP' => 'Partial Picked',
        'PPA' => 'Partial Packing',
        'PSH' => 'Partial Staging',
    ];

    protected $_notAllow = [
        'SH'  => 'Shipped',
        'PRC' => 'Partial Shipped',
    ];

    private function processCancel($odrObj)
    {
        $odrStatus = $odrObj->odr_sts;
        if (!$this->orderHdrModel->validateCancelOdr($odrObj)) {
            return false;
        }

        $isNew = true;
        //Not care BAC
        if ($odrStatus !== "NW") {
            $isNew = false;
        }

        //process revert data
        $this->orderDtlModel->revertOrder($odrObj, $isNew);

        return true;
    }


    /**
     * @param $waveId
     * @param $odrDtl
     */
    private function changeStatusWavepick($waveId, $odrSts, $cancelOdrId)
    {
        $ignoreSts = [
            // Status::getByValue('Picked'         , 'Order-Status'),
            // Status::getByValue('Partial Picked' , 'Order-Status'),
            // Status::getByValue('Packing'        , 'Order-Status'),
            // Status::getByValue('Partial Packing', 'Order-Status'),
            // Status::getByValue('Packed'         , 'Order-Status'),
            // Status::getByValue('Partial Packed' , 'Order-Status'),
            // Status::getByValue('Staging'        , 'Order-Status'),
            // Status::getByValue('Partial Staging', 'Order-Status'),
        ];

        $waveHdr = $this->wavepickHdrModel->getFirstWhere(['wv_id' => $waveId], ['details']);

        if (!empty($waveHdr)) {
            if (!in_array($odrSts, $ignoreSts)) {
                // update wavepick detail
                \DB::table('wv_dtl')
                    ->join('odr_dtl', function ($join) {
                        $join->on('wv_dtl.wv_id'  , '=', 'odr_dtl.wv_id')
                             ->on('wv_dtl.item_id', '=', 'odr_dtl.item_id')
                             ->on('wv_dtl.lot'    , '=', 'odr_dtl.lot');
                    })
                    ->where('wv_dtl.wv_id', $waveId)
                    ->whereIn('odr_dtl.odr_id', $cancelOdrId)
                    ->update([
                        'wv_dtl.piece_qty'  => DB::raw('IF(wv_dtl.piece_qty < odr_dtl.alloc_qty, 0, wv_dtl.piece_qty  - odr_dtl.alloc_qty)'),
                        'wv_dtl.updated_at' => time(),
                        'wv_dtl.updated_by' => Data::getCurrentUserId()
                    ]);
            }

            // update wave pick detail when PD
            $this->wavepickDtlModel->getModel()
                ->where('deleted', 0)
                ->where('wv_id', $waveId)
                ->where('act_piece_qty', ">=", "piece_qty")
                ->where('act_piece_qty', "!=", 0)
                ->update(
                    [
                        'wv_dtl_sts' => "PD",
                    ]);
            // update wave pick detail when CC
            $this->wavepickDtlModel->updateWhere(
                [
                    'wv_dtl_sts' => "CC",
                ],
                [
                    'wv_id'     => $waveId,
                    'piece_qty' => 0,
                ]);

            //check for cancel wavepick with status is Return
            $chkWv = \DB::table('odr_hdr')
                ->where('wv_id', $waveId)
                ->where('deleted', 0)
                ->where('odr_sts', '!=', 'CC')
                ->whereNotIn('odr_id', $cancelOdrId)
                ->count();
            if ($chkWv == 0) {
                // update wave pick is return
                $this->wavepickHdrModel->updateWhere(['wv_sts' => 'RT'], ['wv_id' => $waveId]);
                // update wave pick is canceled
                $this->wavepickHdrModel->updateCanceled($waveId);
            } else {
                // update wave pick is completed
                $this->wavepickHdrModel->updatePicked($waveId);
            }
        }
    }

    private function removeShipId($odrId)
    {
        // Remove ship_id
        $this->orderHdrModel->updateWhere(['ship_id' => null], ['odr_id' => $odrId]);

        // Remove ship_id in bol_odr_dtl
        DB::table('bol_odr_dtl')->where('odr_id', $odrId)->update(['deleted' => 1, 'deleted_at' => time()]);
    }

}