<?php

namespace App\Api\V2\Outbound\OutPallet\AssignCartons\Controllers;

use App\Api\V2\Outbound\OutPallet\AssignCartons\Models\OrderHdrModel;
use App\Api\V2\Outbound\OutPallet\AssignCartons\Models\EventTrackingModel;
use App\Api\V2\Outbound\OutPallet\AssignCartons\Models\OutPalletModel;
use App\Api\V2\Outbound\OutPallet\AssignCartons\Models\PackHdrModel;
use App\Api\V2\Outbound\OutPallet\AssignCartons\Models\CartonModel;
use App\Api\V2\Outbound\OutPallet\AssignCartons\Validators\OutPalletValidator;
use App\libraries\RFIDValidate;

use App\Api\V1\Models\Log;

use App\Jobs\BOLJob;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OutPalletController extends AbstractController
{

    protected $orderHdrModel;
    protected $eventTrackingModel;
    protected $outPalletModel;
    protected $packHdrModel;
    protected $cartonModel;

    public function __construct()
    {
        $this->orderHdrModel      = new OrderHdrModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->outPalletModel     = new OutPalletModel();
        $this->packHdrModel       = new PackHdrModel();
        $this->cartonModel        = new CartonModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function assignCartonToPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $palletCode = array_get($input, 'pallet', null);
        $packs = array_get($input, 'packs', null);

        /*
         * start logs
         */

        $url = "v2/{$whsId}/outpallet/assign-cartons";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'ACP',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Assign Packed Cartons to Out pallet'
        ]);

        /*
         * end logs
         */

        if (!is_array($packs)) {
            $packs = [$packs];
        }
        if (empty($packs) || empty($palletCode)) {
            $msg = sprintf("Pallet and packs field are required");

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        /**
         * WMS2-3891: [WAP API - OUTBOUND] Able to assign packed cartons to invalid out-pallet
         */
        if (!OutPalletValidator::validate($palletCode)) {
            $msg = OutPalletValidator::ERROR_MSG;

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }
        // Check exists Order hdl num
        $pltNumArr = explode('-', $palletCode);
        if (count($pltNumArr) < 4) {
            $msg = sprintf("The pallet num %s is invalid", $palletCode);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }
        $odrNum = "ORD-" . $pltNumArr[1] . "-" . $pltNumArr[2];
        $order = $this->orderHdrModel->getFirstWhere(['odr_num' => $odrNum]);
        if (!$order) {
            $msg = sprintf("No order has been found for the pallet number %s", $palletCode);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        } else {
            $odrSts = object_get($order, 'odr_sts', '');
            if ($odrSts == 'CC') {
                $msg = sprintf('Unable to assign cartons to out pallet for canceled Order!');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }

        // check pallet num
        $pallet = $this->outPalletModel->getFirstWhere([
            'whs_id'  => $whsId,
            'plt_num' => $palletCode,
        ]);

        if ($pallet && $pallet->out_plt_sts != 'AC') {
            $msg = sprintf("The pallet num %s is %s and not active", $palletCode, $pallet->out_plt_sts);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        // Check duplicate Pack hdl num
        $uniquePacks = array_unique($packs);
        if (count($uniquePacks) != count($packs)) {
            $duplicatePack = array_unique(array_diff_assoc($packs, $uniquePacks));
            $msg = 'Duplicate Pack: ' . implode(', ', $duplicatePack);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        $ctnRfidInValid = [];
        $errorMsg = '';
        $isAssignRfid = 0; //0: is assigned with barcode, 1: is assigned with rfid
        foreach ($packs as $key => $pack) {
            //check invalid carton rfid when auto packs
            $checkRfid = substr($pack, 0, 3);
            if (!in_array($checkRfid, ["CTN", "CYC"])) {
                $isAssignRfid = 1;
                $ctnRFIDValid = new RFIDValidate($pack, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $pack;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }

                $ctnObj = $this->cartonModel->getFirstWhere(['rfid' => $pack]);
                if (!$ctnObj) {
                    $msg = sprintf("The Carton Rfid %s doesn't exist.", $pack);
                    return [
                        "iat"     => time(),
                        "status"  => false,
                        "message" => [
                            "msg"         => $msg,
                            "status_code" => -1,
                        ],
                        "data"    => []
                    ];
                }
                $ctnId = object_get($ctnObj, 'ctn_id', null);
                $packHdr = $this->packHdrModel->getFirstWhere(['cnt_id' => $ctnId]);

                $packs[$key] = object_get($packHdr, 'pack_hdr_num', null);
            }
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $errorMsg,
                    "status_code" => -1,
                ],
                "data"    => $ctnRfidInValid
            ];
        }

        try {
            // start transaction
            DB::beginTransaction();

            // update location in out_pallet
            $ttlCtn = 0;
            $odrId = $order->odr_id;
            $odrNum = $order->odr_num;

            $sts = false;

            foreach ((array)$packs as $packCode) {
                $packHdr = null;
                if (is_numeric(strpos($packCode, 'CTN'))) {
                    $packHdr = $this->packHdrModel->getFirstWhere([
                        'pack_hdr_num' => $packCode,
                        'whs_id'       => $whsId,
                    ]);
                } else {
                    $packHdr = $this->packHdrModel->getFirstWhere([
                        'pack_hdr_num' => $packCode,
                        'whs_id'       => $whsId,
                    ]);
                }

                if (!$packHdr) {
                    $msg = sprintf("Packed Carton %s is not found!", $packCode);

                    return [
                        "iat"     => time(),
                        "status"  => false,
                        "message" => [
                            "msg"         => $msg,
                            "status_code" => -1,
                        ],
                        "data"    => []
                    ];
                }

                if ($packHdr->out_plt_id) {
                    $pltId = object_get($pallet, 'plt_id', -1);
                    if ($packHdr->out_plt_id != $pltId) {
                        $msg = sprintf("The carton %s has been assigned to other pallet already!", $packCode);

                        return [
                            "iat"     => time(),
                            "status"  => false,
                            "message" => [
                                "msg"         => $msg,
                                "status_code" => -1,
                            ],
                            "data"    => []
                        ];
                    } else {
                        $msg = sprintf("The carton %s has been assigned to this pallet already!", $packCode);

                        return [
                            "iat"     => time(),
                            "status"  => false,
                            "message" => [
                                "msg"         => $msg,
                                "status_code" => -1,
                            ],
                            "data"    => []
                        ];
                    }

                }


                // if (is_numeric(strpos($palletCode, 'LPN'))) {
                //     $res = (new OrderHdrModel())->isLPNinOrder($packHdr->odr_hdr_id, $palletCode, $odrNum);
                //     if (!$res) {
                //         $msg = sprintf("%s is not existed", $palletCode);

                //         return [
                //             "iat"     => time(),
                //             "status"  => false,
                //             "message" => [
                //                 "msg"         => $msg,
                //                 "status_code" => -1,
                //             ],
                //             "data"    =>  []
                //         ];
                //     }
                // }

                if ($odrId != $packHdr->odr_hdr_id) {
                    $msg = sprintf("Please scan cartons the same as order", $odrNum);

                    return [
                        "iat"     => time(),
                        "status"  => false,
                        "message" => [
                            "msg"         => $msg,
                            "status_code" => -1,
                        ],
                        "data"    => []
                    ];
                }
                if (!$pallet) {
                    $pallet = $this->outPalletModel->createNewPallet($packHdr, $palletCode, 0);
                }

                //Update Pack Status after asigned Pallet
                $packHdr->out_plt_id = $pallet->plt_id;
                $packHdr->pack_sts = 'AS';
                $packHdr->is_assigned_rfid = $isAssignRfid;
                $packHdr->save();
                $ttlCtn++;

                $sts = $ttlCtn;
            }

            // Update total pack carton
            $this->outPalletModel->updatePalletCtnTtl($pallet->plt_id);

            $staging = $this->orderHdrModel->updateOrderStaging($odrId);

            if ($odrId) {
                $odrObj = $this->orderHdrModel->getFirstWhere(
                    [
                        'odr_id' => $odrId
                    ]
                );
                $this->eventTrackingModel->eventTracking([
                    'whs_id'    => $whsId,
                    'cus_id'    => object_get($odrObj, 'cus_id', null),
                    'owner'     => object_get($odrObj, 'odr_num', null),
                    'evt_code'  => 'WOP',
                    'trans_num' => object_get($odrObj, 'odr_num', null),
                    'info'      => "WAP - Assigning Carton(s) to Pallet"
                ]);
            }

            DB::commit();
            dispatch(new BOLJob($odrId, $whsId, $request));
            $msg = sprintf("%d carton(s) assigned to pallet %s successfully!", count($packs),
                $palletCode);

            return [
                "iat"     => time(),
                "status"  => true,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => 1,
                ],
                "data"    => []
            ];

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $e->getMessage(),
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}