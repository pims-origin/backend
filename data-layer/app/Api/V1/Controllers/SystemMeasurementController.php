<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 29-Jun-16
 * Time: 10:11
 */

namespace App\Api\V1\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V1\Models\SystemMeasurementModel;
use App\Api\V1\Transformers\SystemMeasurementTransformer;

class SystemMeasurementController extends AbstractController
{
    /**
     * @var $systemMeasurementModel
     */
    protected $systemMeasurementModel;
    /**
     * @var $systemMeasurementTransformer
     */
    protected $systemMeasurementTransformer;

    public function __construct()
    {
        parent::__construct();

        $this->systemMeasurementModel = new SystemMeasurementModel();
        $this->systemMeasurementTransformer = new SystemMeasurementTransformer();
    }

    /**
     * API Read System Measurement
     *
     * @param $systemMeasurementCode
     *
     * @return \Dingo\Api\Http\Response
     */
    public function show($systemMeasurementCode)
    {
        $SystemMeasurement = $this->systemMeasurementModel->getFirstBy('sys_mea_code', $systemMeasurementCode);
        if ($SystemMeasurement) {
            return $this->response->item($SystemMeasurement, $this->systemMeasurementTransformer);
        } else {
            return $this->response->noContent();
        }
    }

    /**
     * API Search System Measurement
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request)
    {
        $input = $request->getQueryParams();

        try {
            $systemMeasurement = $this->systemMeasurementModel->search($input, [], array_get($input, 'limit'));

            return $this->response->paginator($systemMeasurement, $this->systemMeasurementTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }
}