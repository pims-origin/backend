<?php

namespace App\Jobs;

use App\Api\V1\Models\Log;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderHdr;

class AssignOverPickingCartonToProcessingLocation extends Job
{
    protected $orderId;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
    }

    public function handle()
    {
        try{
            \Log::critical('Start Assign Over Picking Carton To Processing Location ');
            DB::beginTransaction();
            $orderHdr = OrderHdr::with(['waveHdr.details'])
                                ->where('odr_id', $this->orderId)
                                ->first();

            $wvHeader = object_get($orderHdr, 'waveHdr', null);
            $wvDetails = object_get($orderHdr, 'waveHdr.details', []);

            if (!$wvHeader || !$wvDetails){
                $info = sprintf('Order %s missed information', $this->orderId);
                \Log::info($info);
                return false;
            }

            $wvHeader->update(['wv_sts' => 'CO']);

            foreach($wvDetails as $wvDetail){
                if ($wvDetail->act_piece_qty > $wvDetail->piece_qty){
                    $wvDetail->update(['act_piece_qty' => $wvDetail->piece_qty]);
                }
            }

            $cartonsOverPicking = OrderCarton::whereNull('odr_hdr_id')
                                            ->where('wv_hdr_id', $orderHdr->wv_id)
                                            ->get();

            $processingLocation = Location::where('loc_type_id', 6)->first();

            foreach($cartonsOverPicking as $odrCarton){
                $cartonRfid = $odrCarton->ctn_rfid;

                DB::table('odr_cartons')->where('odr_ctn_id', $odrCarton->odr_ctn_id)
                                        ->update([
                                            'wv_num'    => null,
                                            'wv_hdr_id' => null,
                                            'wv_dtl_id' => null,
                                        ]);

                Carton::where('rfid', $cartonRfid)
                        ->update([
                            'loc_type_code' => 'PRO',
                            // 'loc_name'      => $processingLocation->loc_alternative_name,
                            'loc_code'      => $processingLocation->loc_code,
                            'loc_id'        => $processingLocation->loc_id,
                        ]);
            }
            DB::commit();
            \Log::critical('Assign Over Picking Carton To Processing Location success');
        }catch(\Exception $e){
            \Log::error($e->getMessage());
        }

    }
}
