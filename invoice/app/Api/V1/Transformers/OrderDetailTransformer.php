<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class OrderDetailTransformer extends TransformerAbstract
{
    public function transform($detail)
    {
        return [
            'odr_id' => $detail->odr_id,
            'cus_id' => $detail->cus_id,
            'cus_name' => $detail->cus_name,
            'csr' => $detail->csr,
            'user_id' => $detail->user_id,
            'first_name' => $detail->first_name,
            'last_name' => $detail->last_name,
            'user_name' => trim($detail->first_name . " " .  $detail->last_name),
            'odr_num' => $detail->odr_num,
            'cus_odr_num' => $detail->cus_odr_num,
            'cus_po' => $detail->cus_po,
            'ship_by_dt' => $detail->ship_by_dt,
            'act_cmpl_dt' => $detail->act_cmpl_dt,
            'odr_sts' => $detail->odr_sts
        ];
    }

    /*
    ****************************************************************************
    */

}

