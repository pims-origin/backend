<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/17/2016
 * Time: 1:53 PM
 */

namespace backend\forms;

use backend\forms\Form;
use backend\models\UserDepartment;
use common\models\Department;
use common\models\User;
use common\models\CusInUser;
use common\models\CusExUser;
use common\models\UserWarehouse;
use common\models\CustomerWarehouse;
use yii\helpers\VarDumper;
use Yii;


class SignupForm extends Form
{
    const PASSWORD_MIN_LENGTH = 8;
    const ROLE_USER_CREATE = 'User';

    public $usr_dpm_id;
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $status;
    public $emp_code;
    public $off_loc;
    public $dept;
    public $phone;
    public $phone_extend;
    public $mobile;
    public $setup_password_url;

    public $user_warehouses = [];
    public $user_customers = [];
    public $user_roles = "";

    private $userStatuses = [User::STATUS_ACTIVE, User::STATUS_INACTIVE, USER::STATUS_LOCKED];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $required = ['email', 'status', 'first_name', 'last_name', 'phone', 'usr_dpm_id'];

        $return = [
            [$required, 'required'],
            ['email', 'email'],
            [['email'], 'string', 'max' => 64],
            [
                'email',
                'unique',
                'targetClass' => '\common\models\User',
                'message'     => 'This email has already been existed.'
            ],
            ['usr_dpm_id', 'integer'],
            ['status', 'validateStatus'],
            [['first_name', 'last_name'], 'string', 'max' => 35],
            [['emp_code', 'off_loc', 'dept'], 'string', 'max' => 50],
            ['phone', 'string', 'max' => 20],
            ['phone_extend', 'string', 'max' => 5],
            ['mobile', 'string', 'max' => 20],
        ];

        return $return;
    }

    /**
     * add safe extra fields
     *
     * @return array
     */
    public function safeAttributes()
    {
        $attributes = parent::safeAttributes();

        return array_merge($attributes, [
            'user_customers',
            'user_warehouses',
            'user_roles',
            'setup_password_url'
        ]);
    }

    public function validateStatus($attribute, $params)
    {
        if (!in_array($this->status, $this->userStatuses, true)) {
            $this->addError($attribute, 'Status should be [' . implode(', ', $this->userStatuses) . ']');
        }
    }

    public function createActiveUser()
    {
        if (!$this->validate()) {
            return null;
        }

        try {
            $dept = UserDepartment::getUserDeptById($this->usr_dpm_id);

            $transaction = Yii::$app->db->beginTransaction();

            $model = new User();
            $model->usr_dpm_id = $this->usr_dpm_id;
            $model->username = $this->generateUsername();
            $model->email = $this->email;
            $model->first_name = $this->first_name;
            $model->last_name = $this->last_name;
            $model->status = $this->status;
            $model->emp_code = $this->emp_code;
            $model->off_loc = $this->off_loc;
            $model->setPassword(time());
            $model->dept = $dept['usr_dpm_name'];
            $model->phone = $this->phone;
            $model->phone_extend = $this->phone_extend;
            $model->mobile = $this->mobile;
            $model->created_at = time();
            $model->updated_at = time();
            $model->deleted_at = 915148800;
            $model->save();

            $cusUserClass = CusInUser::class;
            $cusUserLink = 'cusInUser';
            $cusIds = [];
            //add user's customers
            foreach ($this->user_customers as $customer) {
                $cusUser = new $cusUserClass();
                $cusUser->user_id = $model->user_id;
                $cusUser->cus_id = $customer['cus_id'];
                $cusUser->whs_id = $customer['whs_id'];

                $cusIds[] = $customer['cus_id'];
                $model->link($cusUserLink, $cusUser);
            }
            //add user's warehouses
            foreach ($this->user_warehouses as $warehouser) {
                $userWarehouse = new UserWarehouse();
                $userWarehouse->user_id = $model->user_id;
                $userWarehouse->whs_id = $warehouser['whs_id'];

                $model->link('userWarehouse', $userWarehouse);

                if (!in_array($this->user_roles, ['CSR', 'CSR Manager'])) {
                    // WMS2-5381 - Auto assign customers to users
                    $customerWarehouse = CustomerWarehouse::find()
                        ->where(['whs_id' => $warehouser['whs_id']])
                        ->all();
                    foreach ($customerWarehouse as $customer) {
                        if (!in_array($customer->cus_id, $cusIds)) {
                            $cusUser = new $cusUserClass();
                            $cusUser->user_id = $model->user_id;
                            $cusUser->cus_id = $customer->cus_id;
                            $cusUser->whs_id = $warehouser['whs_id'];

                            $cusIds[] = $customer->cus_id;
                            $model->link($cusUserLink, $cusUser);
                        }
                    }
                }
            }

            $transaction->commit();

            //add user's roles
            if ($this->user_roles) {
                $this->roleUserCreate($this->user_roles, $model->user_id);
            }
        } catch (Yii\base\Exception $ex) {
            $transaction->rollBack();
            throw new Yii\base\Exception($ex->getMessage() . 'Have Error In Signup.', 404);
        }

        $user = $model->attributes;
        unset($user['password'], $user['deleted_at']);

        //send email
        $url = Yii::$app->params['api_domain'] . 'setup-password';
        $options = [
            'username'           => $user['email'],
            'email'              => $user['email'],
            'setup_password_url' => $this->setup_password_url
        ];
        Yii::$app->httpclient->post($url, $options, [], false);

        return $user;
    }

    public function roleUserCreate($role, $user_id)
    {
        $url = Yii::$app->params['authorization_api'] . 'users/' . $user_id . '/roles';
        $options = [
            'roles'   => $role,
            'user_id' => Yii::$app->user->getId()
        ];

        return Yii::$app->httpclient->post($url, $options, [], false);
    }

    public function generateUsername()
    {
        $department = new Department();
        $department = $department->findOne($this->usr_dpm_id);

        $usernameNew =  $department->usr_dpm_code
            . substr($this->first_name, 0, 1) . substr($this->last_name, 0, 2);
        //
        $userCount = User::find()
            ->where('username LIKE :username')
            ->addParams([':username'=> "{$usernameNew}%"])
            ->count();

        if ($userCount && $userCount > 0) {
            $usernameNew = $usernameNew . $userCount;
        }

        return strtolower($usernameNew);
    }
}