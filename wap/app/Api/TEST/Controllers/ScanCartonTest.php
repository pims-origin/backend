<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2/25/2017
 * Time: 10:00 AM
 */

namespace App\Api\TEST\Controllers;




use PHPUnit\Framework\TestCase;

class ScanCartonTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function completeAsnDtl()
    {
   
        $response = $this->json('POST',
            'http://wms-wap.local/inbound-test/whs/1/cus/1/add-virtual-cartons',
            [
                'asn_hdr_id' => 373,
                'asn_dtl_id' => 541,
                'ctns_rfid'  => [''],
                'type'       => 1
            ]
        );

        $response->assertStatus(200);
    }
}