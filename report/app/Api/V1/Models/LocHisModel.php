<?php

namespace App\Api\V1\Models;


use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class LocHisModel extends AbstractModel
{
    public function __construct()
    {

    }

    /*
    ****************************************************************************
    */

    public function search($whsId, $attributes = [], $limit = PAGING_LIMIT)
    {
        DB::setFetchMode(\PDO::FETCH_CLASS);
        $query = DB::table('loc_his')
            ->where('whs_id', $whsId)
            ->select(
                DB::raw('loc_avai_ttl AS loc_avai_ttl'),
                DB::raw('loc_ttl AS loc_ttl'),
                'whs_id'
            );

        $type = strtoupper($attributes['type'] ?? 'DAY');
        $vieBy = strtoupper($attributes['view-by'] ?? 'MIN');
        switch ($type){
            case 'WEEK':
                $query->addSelect(
                        DB::raw("(CASE WHEN '$vieBy' = 'MIN' THEN MIN(loc_used_ttl)
                                        WHEN '$vieBy' = 'MAX' THEN MAX(loc_used_ttl)
                                        ELSE ROUND((CAST(MAX(loc_used_ttl) AS SIGNED) + CAST(MIN(loc_used_ttl) AS SIGNED))/2) 
                                    END ) AS loc_used_ttl"),
                        DB::raw('DATE(ADDDATE(his_dt, INTERVAL 1 - DAYOFWEEK(his_dt) DAY) + 1) AS cur_his'),
                        DB::raw("CONCAT('W', WEEK(his_dt)) AS cur_day")
                    )
                    ->groupBy(DB::raw('WEEK(his_dt)'));
                break;
            case 'MONTH':
                $query->addSelect(
                        DB::raw("(CASE WHEN '$vieBy' = 'MIN' THEN MIN(loc_used_ttl)
                                        WHEN '$vieBy' = 'MAX' THEN MAX(loc_used_ttl)
                                        ELSE ROUND((CAST(MAX(loc_used_ttl) AS SIGNED) + CAST(MIN(loc_used_ttl) AS SIGNED))/2) 
                                    END ) AS loc_used_ttl"),
                        DB::raw("CONCAT(LEFT(DATE_FORMAT(his_dt, '%M'), 3), '-',  DATE_FORMAT(his_dt, '%y')) AS cur_his")/*,
                            DB::raw('LEAST(his_dt, ADDDATE(his_dt, INTERVAL 1 - DAYOFWEEK(his_dt) DAY)) end_day')*/
                    )
                    ->groupBy(DB::raw("DATE_FORMAT(his_dt, '%Y-%m')"));
                break;
            case 'YEAR':
                $query->addSelect(
                        DB::raw("(CASE WHEN '$vieBy' = 'MIN' THEN MIN(loc_used_ttl)
                                        WHEN '$vieBy' = 'MAX' THEN MAX(loc_used_ttl)
                                        ELSE ROUND((CAST(MAX(loc_used_ttl) AS SIGNED) + CAST(MIN(loc_used_ttl) AS SIGNED))/2) 
                                    END ) AS loc_used_ttl"),
                        DB::raw('YEAR(his_dt) AS cur_his')
                    )
                    ->groupBy(DB::raw('YEAR(his_dt)'));
                break;
            default:
                // DAY
                $query->addSelect(
                        'his_dt AS cur_his',
                        DB::raw('loc_used_ttl'),
                        DB::raw("(CASE WHEN WEEKDAY(his_dt) = 0 THEN 'Mon'
                                       WHEN WEEKDAY(his_dt) = 1 THEN 'Tue'
                                       WHEN WEEKDAY(his_dt) = 2 THEN 'Wed'
                                       WHEN WEEKDAY(his_dt) = 3 THEN 'Thu'
                                       WHEN WEEKDAY(his_dt) = 4 THEN 'Fri'
                                       WHEN WEEKDAY(his_dt) = 5 THEN 'Sat'
                                    ELSE 'Sun' END) AS cur_day")
                    )
                    ->groupBy('his_dt')
                ;
                break;
        }

        $query->orderBy('his_dt', 'ASC');
        if (!empty($attributes['page_last'])) {
            $last_page = $query->orderBy('his_dt', 'ASC')->paginate($limit)->lastPage();
            Paginator::currentPageResolver(function() use ($last_page){
                return $last_page;
            });
        }

        $result = $query->orderBy('his_dt', 'ASC')->paginate($limit);

        return $result;
    }

    /*
    ****************************************************************************
    */

}
