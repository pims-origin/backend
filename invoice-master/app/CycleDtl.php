<?php

namespace App;

class CycleDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cycle_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cycle_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cycle_hdr_id',
        'whs_id',
        'cus_id',
        'item_id',
        'sku',
        'size',
        'color',
        'pack',
        'lot',
        'remain',
        'sys_qty',
        'act_qty',
        'sys_loc_id',
        'sys_loc_name',
        'act_loc_id',
        'act_loc_name',
        'cycle_dtl_sts',
        'is_new_sku',
        'sts',
        'plt_rfid'
    ];

    /**
     */
    public function cycleHdr()
    {
        return $this->belongsTo(CycleHdr::class, 'cycle_hdr_id', 'cycle_hdr_id');
    }

    /**
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }
}
