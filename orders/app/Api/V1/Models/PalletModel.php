<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Wms2\UserInfo\Data;

/**
 * Class PalletModel
 *
 * @package App\Api\V1\Models
 */
class PalletModel extends AbstractModel
{
    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
    }

    /**
     * @param $attributes
     * @param array $width
     *
     * @return mixed
     */
    public function suggestPallet($attributes, $width = [])
    {
        $query = $this->make($width);

        if (!empty($attributes['ctn_ttl'])) {
            $query->where('ctn_ttl', '>=', $attributes['ctn_ttl']);
        }

        $query->orderBy('ctn_ttl');
        $query->orderBy('plt_id');

        return $query->get();
    }

    public function updatePalletCtnTtl($locIds)
    {
        $locIds = array_unique($locIds);
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            $this->model
                ->whereIn('loc_id', $chunkLocIds)
                ->update([
                    'ctn_ttl' => DB::raw("(
                    SELECT COUNT(c.ctn_id) FROM cartons c 
                    WHERE pallet.plt_id = c.plt_id AND c.ctn_sts = 'AC'
                )")
                ]);
        }

        //remove loc_id from pallet and remove loc_zone_id
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            DB::table('pallet')->leftJoin('location', 'pallet.loc_id', '=', 'location.loc_id')
                ->whereIn('pallet.loc_id', $chunkLocIds)
                ->where('pallet.ctn_ttl', 0)
                ->update([
                    'location.loc_zone_id' => null,
                    'pallet.loc_id' => null,
                    'pallet.loc_code' => null,
                    'pallet.loc_name' => null,
                    'pallet.rfid' => null,
                ]);
        }
    }
	
	public function getLot($itemId)
    {
        $lots=DB::table('cartons')
		->where('item_id',$itemId)
		->where('ctn_sts', 'AC')
		->where('deleted',0)
		->where('whs_id',Data::getInstance()->getUserInfo()['current_whs'])
		->groupBy('lot')->select([
			'item_id',
			'lot',
			DB::raw('COALESCE(SUM(ctn_pack_size),0) AS avail'),
		])
		->get();
		$avail=0;
		if(!empty($lots))
		{
			foreach($lots as $key=>$lot)
			{
				if($lot['lot']=='NA')
				{
					unset($lots[$key]);
					$avail=$lot['avail'];
					break;
				}
			}
		}
		else
		{
			$lots=[];
		}
		$lots=array_merge([['item_id'=>$itemId,'avail'=>0,'lot'=>'NA']],$lots);
		return $lots;
    }
}
