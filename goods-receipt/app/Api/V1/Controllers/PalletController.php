<?php
/**
 * Created by PhpStorm.
 *
 * Date: loc_id-July-16
 * Time: 10:00
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\LocationStatusDetailModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Traits\PalletControllerTrait;
use App\Api\V1\Transformers\ConsolidationLocTransformer;
use App\Api\V1\Transformers\ConsolidationTransformer;
use App\Api\V1\Transformers\LocationStatusDetailTransformer;
use App\Api\V1\Transformers\PalletTransformer;
use App\Api\V1\Transformers\RelocationTransformer;
use App\Api\V1\Validators\ConsolidationLocValidator;
use App\Api\V1\Validators\ConsolidationPltValidator;
use App\Api\V1\Validators\PalletValidator;
use App\Api\V1\Validators\RelocationValidator;
use Dingo\Api\Exception\UnknownVersionException;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use TCPDF;
use mPDF;
use Wms2\UserInfo\Data;

/**
 * Class PalletController
 *
 * @package App\Api\V1\Controllers
 */
class PalletController extends AbstractController
{
    use PalletControllerTrait;
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var LocationStatusDetailModel
     */
    protected $locationStatusDetailModel;
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var PalletSuggestLocationModel
     */
    protected $palletSuggestLocationModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var AsnHdrModel
     */
    protected $asnHdrModel;

    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;


    /**
     * PalletController constructor.
     *
     * @param PalletModel $palletModel
     * @param PalletSuggestLocationModel $palletSuggestLocationModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param EventTrackingModel $eventTrackingModel
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param AsnHdrModel $asnHdrModel
     * @param AsnDtlModel $asnDtlModel
     */
    public function __construct(
        PalletModel $palletModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        CartonModel $cartonModel,
        LocationModel $locationModel,
        EventTrackingModel $eventTrackingModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        GoodsReceiptModel $goodsReceiptModel,
        InventorySummaryModel $inventorySummaryModel,
        AsnHdrModel $asnHdrModel,
        AsnDtlModel $asnDtlModel,
        ItemModel $itemModel
    ) {
        $this->palletModel = $palletModel;
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->cartonModel = $cartonModel;
        $this->locationModel = $locationModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->asnHdrModel = $asnHdrModel;
        $this->asnDtlModel = $asnDtlModel;
        $this->itemModel = $itemModel;
    }

    /**
     * @param $goodsReceiptId
     * @param Request $request
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param PalletModel $palletModel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function store(
        $goodsReceiptId,
        Request $request,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        PalletModel $palletModel
    ) {
        $whs_id = (int)self::getCurrentWhsId();
        // get data from HTTP
        $input = $request->getParsedBody();
        $goodsReceiptDetail = $goodsReceiptDetailModel->findWhere([
            'gr_hdr_id' => $goodsReceiptId,
        ], ['asnDetail.item.systemUom', 'goodsReceipt'])->toArray();

        $goodsReceipt = $goodsReceiptDetail[0]['goods_receipt'];
        if ($goodsReceipt['whs_id'] != $whs_id) {
            return $this->response->errorUnauthorized('Warehouse invalid');
        }

        $asnHdrId = array_get($goodsReceiptDetail, '0.asn_detail.asn_hdr_id', 0);

        $key = array_search('PE', array_column($goodsReceiptDetail, 'gr_dtl_sts'));
        if ($key === 0 || $key > 0) {
            throw new \Exception("Goods Receipt is pending!");
        }

        $asnDtlIds = array_pluck($goodsReceiptDetail, 'asn_dtl_id', 'asn_dtl_id');
        $grDtlIds = array_pluck($goodsReceiptDetail, 'gr_dtl_id', 'asn_dtl_id');

        $grActualCarton = array_column($goodsReceiptDetail, null, 'asn_dtl_id');
        $grActualCartonTtls = array_pluck($goodsReceiptDetail, 'gr_dtl_act_ctn_ttl');
        $itemInput = array_pluck($input, 'pallets', 'asn_dtl_id');

        $grActCarTtl = 0;
        foreach ($grActualCartonTtls as $grActualCartonTtl) {
            $grActCarTtl += $grActualCartonTtl;
        }

        $input = array_map(function ($item) use ($grDtlIds) {
            $item['gr_dtl_id'] = $grDtlIds[$item['asn_dtl_id']];

            return $item;
        }, $input);


        // Check Pallet assigned
        if ($this->cartonModel->checkPalletAssigned($asnDtlIds)) {
            return $this->response->errorBadRequest('This goods receipt assigned Pallet!');
        }

        // Check pallet and carton in put invalid
        if (!empty($itemInput)) {
            foreach ($itemInput as $asn_dtl_id => $item) {
                $numItem = 0;
                if (!empty($item)) {
                    foreach ($item as $record) {
                        $numItem += ($record['pallet'] * $record['carton_per_pallet']);
                    }
                }

                if (empty($grActualCarton[$asn_dtl_id])) {
                    return $this->response->errorBadRequest(Message::get("BZ005"));
                } else {
                    $actExceptXdox = $grActualCarton[$asn_dtl_id]['gr_dtl_act_ctn_ttl'] -
                        $grActualCarton[$asn_dtl_id]['crs_doc'];

                    if ($numItem != $actExceptXdox) {
                        return $this->response->errorBadRequest(Message::get("BZ005"));
                    }
                }
            }
        }

        try {
            ini_set('max_execution_time', 300);

            DB::beginTransaction();
            $assignStatus = $palletModel->assignPallet($input, $goodsReceiptDetail);

            //event tracking for assign pallet
            $gr_HdrNum = $goodsReceipt['gr_hdr_num'];
            $this->eventTrackingModel->refreshModel();
            $data = [
                'whs_id'    => $goodsReceipt['whs_id'],
                'cus_id'    => $goodsReceipt['cus_id'],
                'owner'     => $gr_HdrNum,
                //'evt_code'  => config('constants.event.PALLET-ASSIGNED'),
                'evt_code'  => Status::getByKey('EVENT', 'PALLET-ASSIGNED'),
                'trans_num' => $gr_HdrNum,
                'info'      => sprintf('Pallet assigned for %s', $gr_HdrNum)

            ];
            $this->eventTrackingModel->create($data);
            //WMS2-4889 remove
            $message = 'Pallets Assigned Successfully!';

            $goodsReceiptDetailModel->updatePltTtl($goodsReceiptId);

            DB::commit();

            return $this->response->noContent()
                ->setContent(['data' => ['message' => $message]])
                ->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function relocate(
        Request $request
    ) {
        $input = $request->getParsedBody();

        if(empty($input['type']) || $input['type'] === 'loc') {
            return $this->relocationLocation($input);
        } else if($input['type'] === 'lpn') {
            return $this->relocationLPN($input);
        }

        return $this->response->errorBadRequest("Relocation type is incorrect!");
    }

    public function relocationLocation($input)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $this->validateRelocation($input);

        $whsId = self::getCurrentWhsId();

        $fromLocId = $input['from_loc_id'];
        $toLocId = $input['to_loc_id'];
        $toLocCode = array_get($input, 'to_loc_code');

        // Check From Location
        $fromLoc = $this->locationModel->getFirstWhere([
            'deleted'   => 0,
            'loc_whs_id' => $whsId,
            'loc_id'    => $fromLocId
        ]);

        $this->checkLocation($fromLoc);

        $fromPallets = $this->palletModel->findWhere([
            'deleted'   => 0,
            'whs_id'    => $whsId,
            'loc_code'  => $fromLoc['loc_code'],
        ]);

//        $toPallet = DB::table('pallet')->where([
//            'whs_id'    => $whsId,
//            'loc_id'    => $toLocId,
//            'deleted'   => 0,
//        ])->first();
//
//
//        if ($toPallet) {
//            $msg = sprintf("You can only relocate to empty location");
//            return $this->response->errorBadRequest($msg);
//        }

        if (! $this->__checkLocationIsEmpty($whsId, $toLocId)) {
            return $this->response->errorBadRequest('You can only relocate to empty location');
        }

        if (count($fromPallets) == 0) {
            return $this->response->errorBadRequest("From Location is empty");
        }

        $toLoc = $this->getToLocation($toLocId, $whsId);

//        if ($fromLoc['spc_hdl_code'] != $toLoc['spc_hdl_code']) {
//            return $this->response->errorBadRequest("From Location and To Location are not same Special Handling!");
//        }

        try {
            DB::beginTransaction();
            foreach ($fromPallets as $fromPallet) {
                if (!$fromPallet) {
                    return $this->response->errorBadRequest("From Location is empty");
                }

                if ($fromPallet['plt_sts'] != 'AC') {
                    return $this->response->errorBadRequest("From Pallet status must be Active");
                }

                // Update Dymanic Zone
                if (!empty($fromPallet['loc_id'])) {
                    // Check and remove dynamic zone
                    DB::table('location')->join('zone', 'location.loc_zone_id', '=', 'zone.zone_id')
                        ->where([
                            'zone.dynamic' => 1,
                            'location.loc_id' => $fromPallet['loc_id']
                        ])
                        ->update([
                            'location.loc_zone_id' => null,
                            'zone.zone_num_of_loc' => DB::raw('zone.zone_num_of_loc - 1')
                        ]);
                }

                if (empty($toLoc['loc_zone_id'])) {
                    $dynZoneId = $this->locationModel->getDynZone($whsId, $fromPallet['cus_id']);

                    if (!$dynZoneId) {
                        return $this->response->errorBadRequest("To Location has't the zone");
                    }

                    // Update zone num of loc
                    DB::table('zone')->where('zone_id', $dynZoneId)
                        ->update([
                            'zone_num_of_loc' => DB::raw('zone_num_of_loc + 1')
                        ]);

                    // Update location dynamic zone
                    $this->locationModel->updateWhere(['loc_zone_id' => $dynZoneId], ['loc_id' => $toLoc['loc_id']]);
                } else {
                    $cusZone = DB::table('customer_zone')->where([
                        "cus_id" => $fromPallet['cus_id'],
                        "zone_id" => $toLoc['loc_zone_id']
                    ])
                        ->first();

                    if (empty($cusZone)) {
                        return $this->response->errorBadRequest("To Location doesn't belong to customer");
                    }
                }
                $this->palletModel->updateWhere([
                    'loc_id' => $toLoc['loc_id'],
                    'loc_code' => $toLoc['loc_code'],
                    'loc_name' => $toLoc['loc_code']
                ], [
                    'plt_id' => $fromPallet['plt_id']
                ]);
                $this->cartonModel->updateWhere([
                    'loc_id' => $toLoc['loc_id'],
                    'loc_code' => $toLoc['loc_code'],
                    'loc_name' => $toLoc['loc_code']
                ], [
                    'plt_id' => $fromPallet['plt_id']
                ]);

                $event = [
                    'whs_id' => $whsId,
                    'cus_id' => $fromPallet['cus_id'],
                    'owner' => $fromLoc['loc_code'],
                    'evt_code' => config('constants.event.REL-CTN-LOC'),
                    'trans_num' => $toLoc['loc_code'],
                    'info' => sprintf("RF Gun - Update Relocate from %s to %s", $fromLoc['loc_code'], $toLoc['loc_code']),
                ];

                //  Evt tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create($event);
            }
            DB::commit();

            return ['data' => [ 'message' => sprintf('Relocate location from %s to %s successfully!', $fromLoc['loc_code'], $toLoc['loc_code']) ]];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $locId
     * @return bool
     */
    private function __checkLocationIsEmpty($whsId, $locId)
    {
        $pallet = $this->palletModel->getFirstWhere([
            'whs_id' => $whsId,
            'loc_id' => $locId
        ]);

        if (! $pallet) {
            return true;
        }

        $totalCarton = $this->cartonModel->getModel()
            ->where('whs_id', $whsId)
            ->where('loc_id', $locId)
            ->where('plt_id', $pallet->plt_id)
            ->whereIn('ctn_sts', ['AC', 'RG', 'LK'])
            ->count();

        if ($totalCarton > 0) {
            return false;
        }

        $pallet->loc_id = null;
        $pallet->loc_code = null;
        $pallet->ctn_ttl = 0;
        $pallet->save();

        return true;
    }

    public function relocationLPN($input)
    {
        // Check from and to LPN is required
        if(empty($input['from_lpn'])) {
            return $this->response->errorBadRequest("From LPN is required!");
        }
        if(empty($input['to_loc_id'])) {
            return $this->response->errorBadRequest("To Location is required!");
        }

        $whsId = self::getCurrentWhsId();
        $frLPNCode = $input['from_lpn'];
        $toLocId = $input['to_loc_id'];
        $toLocCode = $input['to_loc_code'];

        // Get from LPN
        $fromLPN = DB::table('cartons')->where('lpn_carton', $frLPNCode)->where('whs_id', $whsId)->where('deleted', 0)->first();

        // Check from LPN is exists
        if(empty($fromLPN)) {
            return $this->response->errorBadRequest("From LPN is not exists!");
        }

        // Get to Location
        $toLoc = $this->getToLocation($toLocId, $whsId);

        // Check from LPN is same to LPN
        if($fromLPN['loc_id'] === $toLoc->loc_id) {
            return $this->response->errorBadRequest("Cannot relocate to original location!");
        }

        // Check from LPN cartons not Active
        $frLPNs = DB::table('cartons')->where('lpn_carton', $frLPNCode)->where('whs_id', $whsId)->where('deleted', 0)->get();

        foreach($frLPNs as $frCtn) {
            if($frCtn['ctn_sts'] !== 'AC') {
                return $this->response->errorBadRequest("From LPN location is not in Active status!");
            }

//            if ($frCtn['loc_type_code'] != $toLoc['spc_hdl_code']) {
//                return $this->response->errorBadRequest("From LPN and To Location are not same Special Handling!");
//            }
        }

        try {
            DB::beginTransaction();

            // Update From LPN to To LPN
            $affected = DB::table('cartons')
                ->where('lpn_carton', $frLPNCode)
                ->where('whs_id', $whsId)
                ->update([
                    'loc_id'      => $toLoc['loc_id'],
                    'loc_code'    => $toLoc['loc_code'],
                    'loc_name'    => $toLoc['loc_code']
                ]);

            $this->palletModel->updateWhere([
                'loc_id'   => $toLoc['loc_id'],
                'loc_code' => $toLoc['loc_code'],
                'loc_name' => $toLoc['loc_code'],
            ], [
                'plt_id'   => $fromLPN['plt_id']
            ]);

            $ctnIds = array_pluck($frLPNs, 'ctn_id');
            // Report Carton
            foreach (array_chunk($ctnIds, 200) as $chunkCtnIds) {
                DB::table('rpt_carton')
                    ->whereIn('ctn_id', $chunkCtnIds)
                    ->where('whs_id', $whsId)
                    ->update([
                        'loc_id' => $toLoc['loc_id'],
                        'loc_code' => $toLoc['loc_code'],
                        'loc_name' => $toLoc['loc_code']
                    ]);
            }

            $event = [
                'whs_id' => $whsId,
                'cus_id' => $fromLPN['cus_id'],
                'owner' => $frLPNCode,
                'evt_code' => config('constants.event.REL-CTN-LOC'),
                'trans_num' => $toLocCode,
                'info' => sprintf("RF Gun - Update Relocate from %s to %s", $frLPNCode, $toLocCode),
            ];

            // Evt tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($event);

            DB::commit();

            return ['data' => [ 'message' => sprintf('Relocate from LPN %s to Location %s successfully! %s carton(s) was affected!', $input['from_lpn'], $input['to_loc_code'], $affected) ]];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getToLocation($toLocId, $whsId)
    {
        // Check To Location
        $toLoc = $this->locationModel->getFirstWhere([
            'deleted' => 0,
            'loc_whs_id' => $whsId,
            'loc_id' => $toLocId
        ]);

        $this->checkLocation($toLoc, 'To');

//        $checkEmptyToLoc = $this->palletModel->checkWhere([
//            'deleted' => 0,
//            'whs_id' => $whsId,
//            'loc_id' => $toLocId
//        ]);
//
//        if ($checkEmptyToLoc > 0) {
//            return $this->response->errorBadRequest("To Location is not empty!");
//        }

        return $toLoc;
    }

    public function lpnAutoComplete(Request $request)
    {
        $input = $request->getParsedBody();

        if(empty($input['lpn'])) {
            return [];
        }

        $items = DB::table('cartons')->select('lpn_carton')->whereNotNull('lpn_carton')->where('lpn_carton', 'like', '%'.$input['lpn'].'%')->groupBy('lpn_carton')->take(20)->get();

        $data = [];
        foreach($items as $item) {
            $data[] = $item['lpn_carton'];
        }

        return [
            'data' => $data
        ];
    }

    public function checkLocation($location, $type = 'From')
    {
        if (!$location) {
            return $this->response->errorBadRequest($type . " Location is not existed");
        }

        if ($location['loc_sts_code'] == 'LK') {
            return $this->response->errorBadRequest($type . " Location is locked");
        }

        if ($location['loc_sts_code'] == 'IA') {
            return $this->response->errorBadRequest($type . " Location is inactive");
        }

        if ($location['loc_sts_code'] != 'AC') {
            return $this->response->errorBadRequest($type . " Location is not active");
        }
    }

    protected function validateRelocation($attribute) {

        if(empty($attribute['from_loc_id'])) {
            return $this->response->errorBadRequest("From Location is required!");
        }

        if(empty($attribute['to_loc_id'])) {
            return $this->response->errorBadRequest("To Location is required!");
        }

        if($attribute['from_loc_id'] === $attribute['to_loc_id']) {
            return $this->response->errorBadRequest("To Location must be different From Location!");
        }

    }

    /**
     * @param Request $request
     * @param ConsolidationLocValidator $consolidationLocValidator
     * @param ConsolidationTransformer $consolidationTransformer
     *
     * @return Response|void
     */
    public function consolidationLoc(
        Request $request,
        ConsolidationLocValidator $consolidationLocValidator,
        ConsolidationTransformer $consolidationTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $consolidationLocValidator->validate($input);
        $fromLocId = $input['from_loc_id'];
        $toLocId = $input['to_loc_id'];


        $newLocationInfo = $consolidationLocValidator->check($fromLocId, $toLocId, $input['ctn_id']);

        $toLocName = object_get($newLocationInfo, 'loc_alternative_name', '');
        $toLocCode = object_get($newLocationInfo, 'loc_code', null);

        try {

            // pallet from Location
            $palletFromLoc = $this->palletModel->getFirstWhere(['loc_id' => $fromLocId]);
            $CurrentWhsId = object_get($palletFromLoc, 'whs_id', 0);
            $CurrentCusId = object_get($palletFromLoc, 'cus_id', 0);
            $fromPlt_id = object_get($palletFromLoc, 'plt_id', 0);

            //get PalletId according to LocationID and check The new location does not have any pallet!
            $palletToLoc = $this->palletModel->getFirstWhere(['loc_id' => $toLocId]);
            if (empty($palletToLoc)
            ) {
                throw new \Exception(Message::get("BM024"));
            }
            $plt_id = object_get($palletToLoc, 'plt_id', 0);
            $data = [
                'loc_id'   => $toLocId,
                'loc_name' => $toLocName,
                'plt_id'   => $plt_id,
                'loc_code' => $toLocCode,
            ];

            // Get customer the same zone from location
            $cusIds = $this->locationModel->getCusIdsByLocIds($CurrentWhsId, $fromLocId)->toArray();
            $cusIds = array_column($cusIds, 'cus_id');

            $locations = $this->locationModel->getLocationByCus($CurrentWhsId, $toLocId, $cusIds)->toArray();
            if (empty($locations)) {
                //$msg = sprintf('The location %s not belonged to this customer', $input['to_loc_code']);
                $msg = 'Current Location and New Location do not belong to the same customer';
                throw new \InvalidArgumentException($msg);
            }

            $this->cartonModel->updateWhereIn($data, $input['ctn_id'], 'ctn_id');
            $fromLoc = $this->locationModel->getFirstBy('loc_id', $fromLocId);
            $fromLocCode = object_get($fromLoc, 'loc_code', '');
            //event tracking carton location (Pallet)
            foreach ($input['ctn_num'] as $ctn_num) {
                // tracking carton pallet
                $this->eventTrackingModel->refreshModel();
                $evt_owner = $this->palletModel->generateEventOwner('CON');
                $this->eventTrackingModel->create([
                    'whs_id'    => $CurrentWhsId,
                    'cus_id'    => $CurrentCusId,
                    'owner'     => $evt_owner,
                    'evt_code'  => config('constants.event.CON-LP-LOC'),
                    'trans_num' => $evt_owner,
                    'info'      => sprintf(config('constants.event-info.CON-LP-LOC'), $ctn_num,
                        object_get($palletFromLoc,
                            'plt_num', ''), object_get($palletToLoc, 'plt_num', ''))

                ]);

                // tracking carton location (carton)
                $evt_owner = $this->palletModel->generateEventOwner('CON');
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $CurrentWhsId,
                    'cus_id'    => $CurrentCusId,
                    'owner'     => $evt_owner,
                    'evt_code'  => config('constants.event.CON-CTN-LOC'),
                    'trans_num' => $evt_owner,
                    'info'      => sprintf(config('constants.event-info.CON-CTN-LOC'), $ctn_num, $fromLocCode,
                        $toLocCode)

                ]);
            }
            //event tracking carton location: (complete)
            $evt_owner = $this->palletModel->generateEventOwner('CON');
            $this->eventTrackingModel->refreshModel();

            $this->eventTrackingModel->create([
                'whs_id'    => $CurrentWhsId,
                'cus_id'    => $CurrentCusId,
                'owner'     => $evt_owner,
                'evt_code'  => config('constants.event.COMPLETE-CON'),
                'trans_num' => $evt_owner,
                'info'      => sprintf(config('constants.event-info.COMPLETE-CON'), $evt_owner)

            ]);

            // update total carton pallet_to
            $this->palletModel->updateWhere(
                [
                    "ctn_ttl" => $this->cartonModel->checkWhere([
                        'loc_id'  => $toLocId,
                        'plt_id'  => $plt_id,
                        'deleted' =>
                            0
                    ])
                ],
                ['plt_id' => $plt_id]
            );

            // update total carton pallet_from
            $this->palletModel->updateWhere(
                [
                    "ctn_ttl" => $this->cartonModel->checkWhere([
                        'loc_id'  => $fromLocId,
                        'plt_id'  => $fromPlt_id,
                        'deleted' =>
                            0
                    ])
                ],
                ['loc_id' => $fromLocId]
            );
            //Remove loc_id from pallet when pallet is empty
            $arr_loc_id = [$fromLocId];
            if (empty($this->palletModel->getFirstWhere(['loc_id' => $fromLocId])->ctn_ttl)
            ) {
                $this->palletModel->removeWhereInLocation($arr_loc_id);
            }

            $carton = $this->cartonModel->getCartonWithNewLocation($input['ctn_id']);
            if ($carton) {
                return $this->response->item($carton, $consolidationTransformer);
            }

            $this->palletModel->updatePalletZeroDateAndDurationDaysAuto();

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param ConsolidationPltValidator $consolidationPltValidator
     * @param ConsolidationTransformer $consolidationTransformer
     *
     * @return Response|void
     * @throws \Exception
     */
    public function consolidationPlt(
        Request $request,
        ConsolidationPltValidator $consolidationPltValidator,
        ConsolidationTransformer $consolidationTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $fromPltId = $input['from_plt_id'];
        $toPltId = $input['to_plt_id'];

        // validation
        $consolidationPltValidator->validate($input);
        $currentLocationAndPalletInfo = $consolidationPltValidator->checkCurrentLoc($fromPltId);
        $newLocationInfo = $consolidationPltValidator->check($toPltId, $input['ctn_id']);

        $palletFrom = $currentLocationAndPalletInfo[1];
        $palletTo = $this->palletModel->checkPltSameCustomer(
            $palletFrom->whs_id, $palletFrom->cus_id, $toPltId
        );
        $cartonsFrom = object_get($palletFrom, 'carton');
        $itemIdFrom =array_get($cartonsFrom, '0.item_id');
        $cartonsTo = object_get($palletTo, 'carton');
        $itemIdTo =array_get($cartonsTo, '0.item_id');
        $itemFrom = $this->itemModel->getModel()->where('item_id', $itemIdFrom)->first();
        $itemTo = $this->itemModel->getModel()->where('item_id', $itemIdTo)->first();
        if(!empty($itemTo)&& !empty($itemFrom)) {
            if($itemFrom->spc_hdl_code !== $itemTo->spc_hdl_code) {
                $msg = 'Current Pallet and New Pallet do not belong to the same Special Handling';
                throw new \Exception($msg);
            }
        }
        if (empty($palletTo)) {
            $msg = 'Current Pallet and New Pallet do not belong to the same customer';
            throw new \Exception($msg);
        }

        $toLocId = object_get($newLocationInfo, 'loc_id', 0);
        $toLocName = object_get($newLocationInfo, 'loc_alternative_name', '');
        $toLocCode = object_get($newLocationInfo, 'loc_code', '');

        try {
            //update carton to new location
            $data = [
                'loc_id'   => $toLocId,
                'loc_name' => $toLocName,
                'plt_id'   => $toPltId,
                'loc_code' => $toLocCode,
            ];

            $this->cartonModel->updateWhereIn($data, $input['ctn_id'], 'ctn_id');
            DB::table('rpt_carton')->whereIn('ctn_id', $input['ctn_id'])->update(
                $data
            );
            $fromLocCode = object_get($currentLocationAndPalletInfo[0], 'loc_code', '');
            $CurrentWhsId = object_get($currentLocationAndPalletInfo[1], 'whs_id', 0);
            $CurrentCusId = object_get($currentLocationAndPalletInfo[1], 'cus_id', 0);

            //event tracking carton location
            foreach ($input['ctn_num'] as $ctn_num) {
                // tracking carton location
                $evt_owner = $this->palletModel->generateEventOwner('CON');
                $this->eventTrackingModel->refreshModel();

                $this->eventTrackingModel->create([
                    'whs_id'    => $CurrentWhsId,
                    'cus_id'    => $CurrentCusId,
                    'owner'     => $evt_owner,
                    'evt_code'  => config('constants.event.CON-CTN-LOC'),
                    'trans_num' => $evt_owner,
                    'info'      => sprintf(config('constants.event-info.CON-CTN-LOC'), $ctn_num, $fromLocCode,
                        $toLocCode)

                ]);

                $evt_owner = $this->palletModel->generateEventOwner('CON');
                $this->eventTrackingModel->refreshModel();

                $this->eventTrackingModel->create([
                    'whs_id'    => $CurrentWhsId,
                    'cus_id'    => $CurrentCusId,
                    'owner'     => $evt_owner,
                    'evt_code'  => config('constants.event.CON-LP-LOC'),
                    'trans_num' => $evt_owner,
                    'info'      => sprintf(config('constants.event-info.CON-LP-LOC'), $ctn_num,
                        $input['from_plt_num'], $input['to_plt_num'])
                ]);
            }

            $evt_owner = $this->palletModel->generateEventOwner('CON');
            //event tracking carton location complete
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $CurrentWhsId,
                'cus_id'    => $CurrentCusId,
                'owner'     => $evt_owner,
                'evt_code'  => config('constants.event.COMPLETE-CON'),
                'trans_num' => $evt_owner,
                'info'      => sprintf(config('constants.event-info.COMPLETE-CON'), $evt_owner)
            ]);

            // update total carton pallet
            $this->palletModel->updateWhere(
                ["ctn_ttl" => $this->cartonModel->checkWhere(['plt_id' => $fromPltId])],
                ['plt_id' => $fromPltId]
            );
            DB::table('rpt_pallet')->where('plt_id', $fromPltId)->update(
                ["ctn_ttl" => $this->cartonModel->checkWhere(['plt_id' => $fromPltId])]
            );
            //Remove loc_id from pallet when pallet is empty
            $fromLocId = $currentLocationAndPalletInfo[0]->loc_id;
            $arr_loc_id = [$fromLocId];
            if (empty($this->palletModel->getFirstWhere(['loc_id' => $fromLocId])->ctn_ttl)
            ) {
                $this->palletModel->removeWhereInLocation($arr_loc_id);
            }

            // update total carton pallet
            $this->palletModel->updateWhere(
                ["ctn_ttl" => $this->cartonModel->checkWhere(['plt_id' => $toPltId])],
                ['plt_id' => $toPltId]
            );
            DB::table('rpt_pallet')->where('plt_id', $toPltId)->update(
                ["ctn_ttl" =>  $this->cartonModel->checkWhere(['plt_id' => $toPltId])]
            );

            $carton = $this->cartonModel->getCartonWithNewLocation($input['ctn_id']);
            if ($carton) {
                return $this->response->item($carton, $consolidationTransformer);
            }

            $this->palletModel->updatePalletZeroDateAndDurationDaysAuto();

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }


    /**
     * @param $lpnCarton
     * @param ConsolidationLocTransformer $consolidationLocTransformer
     * @param Request $request
     * @return Response|void
     */
    public function getCartonListByPallet(
        $lpnCarton,
        ConsolidationLocTransformer $consolidationLocTransformer,
        Request $request
    ) {
        try {
            $input = $request->getQueryParams();
            $cartonDetails = $this->cartonModel->searchLPNCarton($lpnCarton, ['item', 'AsnDtl.systemUom'],
                array_get($input, 'limit'));

            return $this->response->paginator($cartonDetails, $consolidationLocTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $palletId
     * @param ConsolidationLocTransformer $consolidationLocTransformer
     * @param Request $request
     *
     * @return Response|void
     */
    public function getCartonListByPalletForPalletReport(
        $palletId,
        ConsolidationLocTransformer $consolidationLocTransformer,
        Request $request
    ) {
        try {
            $input = $request->getQueryParams();
            //$transfer = $input['transfer'] ?? 0;
            $cartonDetails = $this->cartonModel->getCartonListByPalletForPalletReport($input, $palletId, ['item'],
                array_get($input, 'limit'));

            return $this->response->paginator($cartonDetails, $consolidationLocTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    /**
     * @param $goodsReceiptId
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param PalletModel $palletModel
     * @param Excel $excel
     */
    public function printPutAway(
        $goodsReceiptId,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        GoodsReceiptModel $goodsReceiptModel,
        PalletModel $palletModel,
        Excel $excel
    ) {
        try {
            $goodsReceipt = $goodsReceiptModel->getFirstBy('gr_hdr_id', $goodsReceiptId, ['customer', 'user']);

            if (empty($goodsReceipt)) {
                return $this->response
                    ->errorBadRequest(Message::get("BM017", 'goods receipt'));
            }

            if ($goodsReceipt->gr_sts == 'RE'){
                $dataSuggest = $this->palletSuggestLocationModel->getPutAwayPrintListV1($goodsReceiptId)->toArray();
            }else{
                $dataSuggest = $this->palletSuggestLocationModel->getPutAwayPrintList($goodsReceiptId)->toArray();
            }


            if (empty($dataSuggest)) {
                $grData = $this->palletModel->FindWhere(
                    [
                        'gr_hdr_id' => $goodsReceiptId,
                        'is_xdock'  => 0
                    ],
                    ['grDtl']
                );

                $numOfPallets = count($grData);

                $locations = $palletModel->suggestLocations($goodsReceipt->cus_id, $goodsReceipt->whs_id, $numOfPallets);

                if (count($locations) < $numOfPallets) {
                    $msg = sprintf("Number of locations is insufficient to assign to %d pallets.", $numOfPallets);

                    return $this->response
                        ->errorBadRequest($msg);
                }
                $this->palletSuggestLocationModel->createSuggestionLocation($goodsReceipt, $grData,
                    $locations);

                $dataSuggest = $this->palletSuggestLocationModel->GetPalSugLoc($goodsReceiptId)->toArray();

                /**
                 * event tracking Put Away update here
                 */
                $gr_HdrNum = $goodsReceipt->gr_hdr_num;
                $this->eventTrackingModel->refreshModel();
                $data = [
                    'whs_id'    => $goodsReceipt->whs_id,
                    'cus_id'    => $goodsReceipt->cus_id,
                    'owner'     => $gr_HdrNum,
                    //'evt_code'  => config('constants.event.SUGGEST-LOCATION'),
                    'evt_code'  => Status::getByKey('EVENT', 'SUGGEST-LOCATION'),
                    'trans_num' => $gr_HdrNum,
                    'info'      => sprintf('Suggest  %d location(s) for %d pallet(s)', count($locations), $numOfPallets)
                ];
                $this->eventTrackingModel->create($data);

            }

            $arrItems = [];
            if ($dataSuggest) {
                foreach ($dataSuggest as $palletSugLoc) {
                    // $ctnTtl = Carton::where('plt_id', $palletSugLoc['plt_id'])
                    //                 ->where('item_id', $palletSugLoc['item_id'])
                    //                 ->whereIn('ctn_sts', ['AC', 'LK', 'RG'])
                    //                 ->count();
                    // if ( !$ctnTtl ){
                    //     continue;
                    // }

                    $arrItems[] = [
                        'plt_id'           => array_get($palletSugLoc, 'plt_id', null),
                        'plt_name'         => array_get($palletSugLoc, 'plt_name', null),
                        'loc_id'           => array_get($palletSugLoc, 'loc_id', null),
                        'suggest_location' => array_get($palletSugLoc, 'suggest_location', 'NA'),
                        'item_id'          => array_get($palletSugLoc, 'item_id', 'NA'),
                        'ctn_ttl'          => array_get($palletSugLoc, 'ctn_ttl', 'NA'),
                        // 'ctn_ttl'          => $ctnTtl,
                        'sku'              => array_get($palletSugLoc, 'sku', null),
                        'color'            => array_get($palletSugLoc, 'color', null),
                        'size'             => array_get($palletSugLoc, 'size', null),
                        'putter'           => array_get($palletSugLoc, 'putter', null),
                        'act_loc_id'       => array_get($palletSugLoc, 'act_loc_id', null),
                        "actual"           => array_get($palletSugLoc, "actual", null),
                        "lot"              => array_get($palletSugLoc, "lot", null),
                        "created_at"       => array_get($palletSugLoc, "created_at", null) ? (date('m/d/Y H:i',
                            array_get($palletSugLoc, 'created_at', null))):'',
                        //"updated_at"              => array_get($palletSugLoc, "updated_at", null),
                        "updated_at"       => array_get($palletSugLoc, "updated_at", null) ? (date('m/d/Y H:i',
                            array_get($palletSugLoc, 'updated_at', null))):'',
                        "putter_name"      => array_get($palletSugLoc, "putter_name", null),
                        "rfid"      => array_get($palletSugLoc, "rfid", null),
                        "lpn_carton"       => array_get($palletSugLoc, 'lpn_carton', null),
                    ];
                }
            }

            //------Get max_dax, min_dax and duration between max_dax and min_dax
            $dateUpdated_arr = array_pluck($dataSuggest,'updated_at');
            $dateCreated_arr = array_pluck($dataSuggest,'created_at');

            $maxUpdated_date = 0;
            $minUpdated_date = 0;
            $maxCreated_date = 0;
            $minCreated_date = 0;

            //get Max date
            for ($i = 0; $i < count($dateUpdated_arr); $i++)
            {
                if ($i == 0)
                {
                    $maxUpdated_date = date('m/d/Y H:i', $dateUpdated_arr[$i]);
                }
                else if ($i != 0)
                {
                    $newUpdated_date = date('m/d/Y H:i', $dateUpdated_arr[$i]);
                    if ($newUpdated_date > $maxUpdated_date)
                    {
                        $maxUpdated_date = $newUpdated_date;
                    }
                }
            }

            //get min date
            for ($i = 0; $i < count($dateCreated_arr); $i++)
            {
                if ($i == 0)
                {
                    $minCreated_date = date('m/d/Y H:i', $dateCreated_arr[$i]);
                }
                else if ($i != 0)
                {
                    $newCreated_date = date('m/d/Y H:i', $dateCreated_arr[$i]);
                    if ($newCreated_date < $minCreated_date)
                    {
                        $minCreated_date = $newCreated_date;
                    }
                }
            }

            $max_date = $maxUpdated_date;
            $min_date = $minCreated_date;

            $diff = abs(strtotime($max_date) - strtotime($min_date));

            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
            $dayNum = floor(($diff) / (60*60*24));
            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
            $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
            $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

            $duration=$dayNum." day(s) ".$hours.":".$minutes;
            //------/Get max_dax, min_dax and duration between max_dax and min_dax

            $dataSuggests = $dataSuggest;

            $putterName = '';
            if ($dataSuggests) {
                foreach ($dataSuggests as $ds) {
                    if ($ds['putter_name']) {
                        $putterName = $ds['putter_name'];
                        break;
                    }

                }
            }

            // Write Excel File
            //$this->writeExcel($excel, $dataSuggest, $goodsReceipt);

            //Export to DPF File
            $grHdrNum = $goodsReceipt->gr_hdr_num;

            $userInfo = new \Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];
            //fix bug get date on FE then not equal with BE
            $dataSuggest = $arrItems;
            $this->printDpfData($dataSuggest, $goodsReceipt, $grHdrNum, $putterName, $printedBy, $max_date,
                $min_date, $duration);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $goodsReceiptId
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param PalletModel $palletModel
     * @param Excel $excel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @deprecated DO NOT USE IT
     *
     */
    public function printPutAwayBK(
        $goodsReceiptId,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        GoodsReceiptModel $goodsReceiptModel,
        PalletModel $palletModel,
        Excel $excel
    ) {
        try {
            $goodsReceipt = $goodsReceiptModel->getFirstBy('gr_hdr_id', $goodsReceiptId, ['customer', 'user']);
            if (empty($goodsReceipt)) {
                return $this->response
                    ->errorBadRequest(Message::get("BM017", 'goods receipt'));
            }
            $customerId = $goodsReceipt->cus_id;
            $warehouseId = $goodsReceipt->whs_id;

            $locations = $palletModel->suggestLocation($customerId, $warehouseId)->toArray();

            $goodsReceiptDetail = $goodsReceiptDetailModel->findWhere([
                'gr_hdr_id' => $goodsReceiptId,
            ], ['asnDetail'])->toArray();

            $asnDtlIds = array_pluck($goodsReceiptDetail, 'asn_detail.asn_dtl_id');

            $grDtlIds = array_pluck($goodsReceiptDetail, 'gr_dtl_id');

            $pallets_noUsing = $this->cartonModel->getPalletByAsn($asnDtlIds,
                ['pallet.location', 'pallet.palletSuggestLocation'])->toArray();


            $pallets = [];
            /**
             * query pallet by gr_dtl_id
             *
             */
            //$goodsReceiptDetailId = array_get($goodsReceiptDetail, 'gr_dtl_id', null);
//            $pallets = $this->palletModel->findWhere(
//                ['gr_dtl_id' => $grDtlIds],
//                ['Location', 'palletSuggestLocation']
//            )->toArray();
//

//            if (empty($pallets)) {
//                return $this->response
//                    ->errorBadRequest('This goods receipt doesn\'t assign pallet!');
//            }

            $this->createDataSuggest($pallets, $locations, $goodsReceipt);
            $dataSuggest = $this->palletSuggestLocationModel->GetPalSugLoc($goodsReceiptId);

            $dataSuggests = $dataSuggest->toArray();
            $putterName = '';
            if ($dataSuggests) {
                foreach ($dataSuggests as $ds) {
                    if ($ds['putter_name']) {
                        $putterName = $ds['putter_name'];
                    }

                }
            }

            // Write Excel File
            //$this->writeExcel($excel, $dataSuggest, $goodsReceipt);

            //Export to DPF File
            $grHdrNum = $goodsReceipt->gr_hdr_num;
            $this->printDpfData($dataSuggest, $goodsReceipt, $grHdrNum, $putterName);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $dataSuggest
     * @param $goodsReceipt
     * @param $grHdrNum
     * @param $putterName
     * @param $printedBy
     * @param $max_date
     * @param $min_date
     * @param $duration
     */
    private function printDpfData($dataSuggest, $goodsReceipt, $grHdrNum, $putterName, $printedBy, $max_date,
                                  $min_date, $duration)
    {
        $pdf = new Mpdf();
        $html = (string)view('PutAwayListPrintoutTemplate', [
            'dataSuggest'  => $dataSuggest,
            'goodsReceipt' => $goodsReceipt,
            'putterName'   => $putterName,
            'printedBy'    => $printedBy,
            'max_date'     => $max_date,
            'min_date'     => $min_date,
            'duration'     => $duration,
        ]);

        $pdf->WriteHTML($html);
        //$dir = storage_path("PutAway/$whsId");
        //$pdf->Output("$dir/$grHdrNum.pdf", 'F');
        $pdf->Output("$grHdrNum.pdf", "D");
    }

    /**
     * @param $pallets
     * @param $locations
     *
     * @return array
     */
    private function createDataSuggest($pallets, $locations, $goodsReceipt)
    {
        $errorMessage = '';
        $numBk = count($locations) - count($pallets);

        $dataSuggest = [];

        $itemIds = array_pluck($pallets, 'item_id', 'item_id');

        $carton = $this->cartonModel->getItemLocation($itemIds, ['location'])->toArray();
        $code = array_pluck($carton, 'location.loc_code', 'item_id');

        $items = $this->itemModel->getItemById($itemIds);
        $arrItem = [];
        if ($items) {
            foreach ($items as $item) {
                $arrItem[$item->item_id] = [
                    'color' => $item->color,
                    'size'  => $item->size,
                    'sku'   => $item->sku
                ];
            }
        }

        $gr_hdr_num = $goodsReceipt->gr_hdr_num;
        if (!empty($pallets)) {
            $order = 0;
            $locNums = array_pluck($locations, "loc_code", "loc_id");

            foreach ($pallets as $pallet) {
                $order++;
                $itemId = $pallet['item_id'];
                if (!empty($pallet['pallet']['location'])) {
                    $dataSuggest[] = [
                        'order'            => $order,
                        'plt_name'         => array_get($pallet, 'pallet.plt_num', null),
                        'suggest_location' => null,
                        'data'             => null,
                        'actual'           => array_get($pallet, 'pallet.location.loc_code', null),
                        'ctn_ttl'          => array_get($pallet, 'pallet.pallet_suggest_location.ctn_ttl', null),
                    ];
                    continue;
                }

                $suggested = null;
                $data = null;
                $loc_id = null;
                if (!empty($locNums)) {
                    $loc_id1 = 0;
                    if (!empty($code[$itemId])) {
                        foreach ($locNums as $locId => $locNum) {
                            if (strpos($code[$itemId], $locNum) !== false) {
                                if (!$loc_id) {
                                    $loc_id = $locId;
                                    if ($numBk > 0) {
                                        continue;
                                    }
                                    break;
                                }
                                $loc_id1 = $locId;
                                break;
                            }
                        }
                    }

                    if (!$loc_id) {
                        $loc_id = array_rand($locNums);
                    }

                    $suggested = $locNums[$loc_id];
                    unset($locNums[$loc_id]);

                    if ($numBk > 0 && !$loc_id1) {
                        $loc_id1 = array_rand($locNums);
                        $data = $locNums[$loc_id1];
                        $loc_id = $loc_id1;
                        unset($locNums[$loc_id1]);
                        $numBk--;
                    }

                }

                if (!$errorMessage && empty($suggested)) {
                    $errorMessage = 'Not Enough Location to Put Pallet In!';
                }

                $dataSuggest[$pallet['plt_id']] = [
                    'order'            => $order,
                    'plt_name'         => array_get($pallet, 'pallet.plt_num', null),
                    'suggest_location' => $suggested,
                    'data'             => $data,
                    'ctn_ttl'          => array_get($pallet, 'pallet.pallet_suggest_location.ctn_ttl', null),

                ];

                $grDtlInfo = $this->goodsReceiptDetailModel->getFirstWhere([
                    'gr_dtl_id' =>
                        $pallet['gr_dtl_id']
                ])->toArray();

                $this->palletSuggestLocationModel->replace([
                    'loc_id'     => $loc_id,
                    'plt_id'     => $pallet['plt_id'],
                    'data'       => $data,
                    'item_id'    => $itemId,
                    'ctn_ttl'    => array_get($pallet, 'pallet.ctn_ttl', null),
                    'sku'        => array_get($grDtlInfo, 'sku', null),
                    'size'       => array_get($grDtlInfo, 'size', null),
                    'color'      => array_get($grDtlInfo, 'color', null),
                    'gr_hdr_id'  => $pallet['gr_hdr_id'],
                    'gr_dtl_id'  => $pallet['gr_dtl_id'],
                    'gr_hdr_num' => $gr_hdr_num,
                    'whs_id'     => array_get($pallet, 'whs_id', null),
                    'put_sts'    => config('constants.putaway_status.NEW'),
                    'lot'        => array_get($grDtlInfo, 'lot', null),
                ]);
            }
        }

        return ['message' => $errorMessage, 'data' => $dataSuggest];
    }

    /**
     * @param $excel
     * @param $data
     * @param $goodsReceipt
     *
     * @internal param $goodsReceiptNum
     */
    private function writeExcel(
        $excel,
        $data,
        $goodsReceipt
    ) {
        $customerName = object_get($goodsReceipt, 'customer.cus_name', null);
        $goodsReceiptNum = $goodsReceipt->gr_hdr_num;
        $admin = object_get($goodsReceipt, 'user.first_name', null) . " " .
            object_get($goodsReceipt, 'user.last_name', null);

        if (!empty($data['data'])) {

            // Export to Excel
            storage_path('print-putaway/TemplatePutAwayList.xls');

            $excel->create('file', function ($file) use ($goodsReceiptNum, $customerName, $admin, $data) {

                // Set File Name
                $file->setFilename("PutAway|$goodsReceiptNum");

                $file->sheet("Template", function ($sheet) use ($goodsReceiptNum, $customerName, $admin, $data) {
                    $total = count($data['data']);

                    // Default Sheet
                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Arial',
                            'size' => 9
                        ]
                    ]);

                    $sheet->setWidth('A', 5);
                    $sheet->setWidth('B', 30);
                    $sheet->setWidth('C', 30);
                    $sheet->setWidth('D', 30);
                    $sheet->setWidth('E', 30);
                    $sheet->setHeight('2', 40);

                    // Set Image
                    $objDrawing = new \PHPExcel_Worksheet_Drawing();
                    $objDrawing->setPath(public_path('img/mgm.png'));
                    $objDrawing->setCoordinates('D1');
                    $objDrawing->setOffsetX(200);
                    $objDrawing->setOffsetY(5);
                    $objDrawing->setWidth(200);
                    $objDrawing->setWorksheet($sheet);

                    // Title
                    $sheet->mergeCells('A5:E5');
                    $sheet->mergeCells('A6:E6');
                    $sheet->setCellValue('A5', "PUTAWAY List");
                    $sheet->setCellValue('A6', "Customer: $customerName    GR #: $goodsReceiptNum");

                    $sheet->cell('A5', function ($cell) {
                        // Set font
                        $cell->setFontSize(20);
                        $cell->setFontWeight();
                        $cell->setAlignment('center');
                    });

                    $sheet->cell('A6', function ($cell) {
                        // Set font
                        $cell->setAlignment('center');

                    });

                    $sheet->cell('A1:A' . (16 + $total), function ($cell) {
                        // Set font
                        $cell->setAlignment('center');
                    });

                    // Admin
                    $sheet->setCellValue('B8', "Handler: ");

                    // Error Message
                    if (!empty($data['message'])) {
                        $sheet->cell('B10', function ($cell) use ($data) {
                            // Set font
                            $cell->setFontColor('#ff0000');
                            $cell->setValue("Errors: " . $data['message']);
                        });
                    }
                    // Title
                    $sheet->row(15, ['No', 'LPN', 'Assigned Location', 'Backup Location', 'Actual Location']);
                    $sheet->cell('A15:E15', function ($cell) {
                        // Set font
                        $cell->setAlignment('center');
                        $cell->setFontWeight();
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    // Fill suggest Data
                    $sheet->fromArray($data['data'], null, 'A16', false, false);
                    $sheet->setBorder('A16:E' . (16 + $total), 'dotted');

                    // Fill Total
                    $labelTotal = 16 + $total;
                    $sheet->mergeCells("A$labelTotal:E$labelTotal");
                    $sheet->setCellValue("A$labelTotal", "Grand Total ($total)");
                    $sheet->cell("A$labelTotal", function ($cell) {
                        // Set font
                        $cell->setAlignment('left');
                        $cell->setFontWeight();
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    // Footer
                    $now = date('m/d/Y h:i:s A', time());
                    $sheet->cell("A" . ($labelTotal + 10) . ":E" . ($labelTotal + 10), function ($cell) {
                        $cell->setBorder('thin');
                    });
                    $sheet->setCellValue("A" . ($labelTotal + 10), "$now - By $admin");
                });
            })->download('xlsx');
        }
    }

    /**
     * @param Request $request
     * @param PalletTransformer $palletTransformer
     *
     * @return Response|void
     */
    public function search
    (
        Request $request,
        PalletTransformer $palletTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $pallet = $this->palletModel->search($input, [

            ], array_get($input, 'limit'));

            return $this->response->paginator($pallet, $palletTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function printLpn(
        $goodsReceiptId,
        GoodsReceiptDetailModel $goodsReceiptDetailModel,
        GoodsReceiptModel $goodsReceiptModel
    ) {
        $goodsReceipt = $goodsReceiptModel->getFirstBy('gr_hdr_id', $goodsReceiptId, ['customer', 'user', 'container']);
        if (empty($goodsReceipt)) {
            return $this->response->errorBadRequest(Message::get("BM017", 'goods receipt'));
        }

        $goodsReceiptDetail = $goodsReceiptDetailModel->findWhere([
            'gr_hdr_id' => $goodsReceiptId,
        ], ['asnDetail'])->toArray();

        $asnDtlIds = array_pluck($goodsReceiptDetail, 'asn_detail.asn_dtl_id');
        $pallets = $this->cartonModel->getPalletByAsn($asnDtlIds, ['item', 'AsnDtl', 'pallet.location'])->toArray();

        if (empty($pallets)) {
            return $this->response
                ->errorBadRequest('This goods receipt doesn\'t assign pallet!');
        }

        //Export to Excel File
        $this->createBarcode($pallets, $goodsReceipt);
    }


    private function createBarcode($pallets, $goodsReceipt)
    {
        $pdf = new TCPDF("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set font
        $pdf->SetFont('helvetica', '', 20);

        // add a page
        $pdf->AddPage();

        $admin = object_get($goodsReceipt, 'user.first_name', null) . " " .
            object_get($goodsReceipt, 'user.last_name', null);
        $container = object_get($goodsReceipt, 'container.ctnr_num', null);
        $clientName = object_get($goodsReceipt, 'customer.cus_name', null);
        $dateCreated = date("Y-m-d", time());
        foreach ($pallets as $pallet) {
            $licensePlate = array_get($pallet, "pallet.plt_num", null);
            $location = array_get($pallet, "pallet.location.loc_code", null);
            $po = array_get($pallet, "asn_dtl.asn_dtl_po", null);
            $itemID = array_get($pallet, "item.item_id", null);
            $sku = array_get($pallet, "item.sku", null);
            $size = array_get($pallet, "item.size", null);
            $color = array_get($pallet, "item.color", null);
            $carton = ((int)array_get($pallet, "pallet.ctn_ttl", 0)) . "x{$pallet['ctn_pack_size']}";
            $txt = "Administrator: $admin\n";
            $txt .= "Customer Name: $clientName\n";
            $txt .= "Date Created: $dateCreated\n";
            $txt .= "License Plate: $licensePlate\n";
            $txt .= "\n\n\n\n\n\n\n\n";
            $txt .= "Container #: $container\n";
            $txt .= "Location #: $location\n";
            $txt .= "Item ID #: $itemID\n";
            $txt .= "PO #: $po\n";
            $txt .= "SKU #: $sku\n";
            $txt .= "Size #: $size\n";
            $txt .= "Color #: $color\n";
            $txt .= "Carton #: $carton\n";
            $pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
            $pdf->write1DBarcode($licensePlate, 'C128', '100%', 50, 100, 25, 4, '', 'C');
        }

        //Close and output PDF document
        $pdf->Output("License_Plate_{$goodsReceipt->gr_hdr_num}.pdf", 'D');
    }

    /**
     * @param Request $request
     * @param RelocationValidator $relocationValidator
     * @param RelocationTransformer $relocationTransformer
     * @param PalletModel $palletModel
     * @param LocationModel $locationModel
     * @param CartonModel $cartonModel
     *
     * @return Response
     * @throws \Exception
     */
    public function overWriteNewLocation(
        Request $request,
        RelocationValidator $relocationValidator,
        RelocationTransformer $relocationTransformer,
        PalletModel $palletModel,
        LocationModel $locationModel,
        CartonModel $cartonModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $fromLocId = $input['from_loc_id'];
        $newLocId = $input['to_loc_id'];

        // Load loction_id (New location id)
        $locationNew = $locationModel->getFirstWhere(['loc_id' => $newLocId]);
        if (empty($locationNew)) {
            throw new \Exception(Message::get("BM017", "new location"));
        }
        $toLocName = $locationNew->toArray()['loc_alternative_name'];
        $toLocCode = $locationNew->toArray()['loc_code'];

        // get Pallet ID from Current Location ID
        $palletId = $palletModel->getPltIdsByLocIds($fromLocId);
        if (empty($palletId)
        ) {
            throw new \Exception('The current location does not have any pallet!');
        }
        $count_pallets = $this->palletModel->countPalletWhereInLocation($toLocCode);
        if ($count_pallets > 0) {
            throw new UnknownVersionException('Location already have pallets', null, 4);
        }

        $params = [
            'plt_id'   => $palletId[0],
            'loc_id'   => $newLocId,
            'loc_name' => $toLocName,
            'loc_code' => $toLocCode,
        ];

        // Locations already have pallets. Allow to overwrite current pallets by the new ones.
        if (($pallet = $this->palletModel->getFirstBy('loc_id', $params['loc_id'])) &&
            $pallet->plt_id != $params['plt_id']
        ) {

            //update location in Carton
            $updatingParams = [
                'loc_id'   => $newLocId,
                'loc_name' => $toLocName,
                'loc_code' => $toLocCode,
            ];
            $cartonModel->updateWhere($updatingParams, ['loc_id' => $fromLocId]);

            //update location in Pallet
            if ($palletdata = $this->palletModel->update($params)) {
                return $this->response->item($palletdata, $relocationTransformer);
            }
        }
    }

    /**
     * @param Request $request
     * @param ConsolidationLocValidator $consolidationLocValidator
     * @param RelocationTransformer $relocationTransformer
     * @param PalletModel $palletModel
     * @param LocationModel $locationModel
     * @param CartonModel $cartonModel
     * @param InventorySummaryModel $inventorySummaryModel
     *
     * @return Response|void
     * @throws \Exception
     */
    public function updatePallet(
        Request $request,
        //RelocationValidator $relocationValidator,
        ConsolidationLocValidator $consolidationLocValidator,
        RelocationTransformer $relocationTransformer,
        PalletModel $palletModel,
        LocationModel $locationModel,
        CartonModel $cartonModel,
        InventorySummaryModel $inventorySummaryModel
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $newLocId = $input['new_loc_id'];

        // Check exist for pallet_id
        $pallet = $palletModel->getFirstWhere(['plt_id' => $input['pallet_id']]);
        if (empty($pallet)) {
            throw new \Exception(Message::get("BM017", "Pallet"));
        }

        try {
            if ($newLocId) {
                // Load loction_id (New location id)
                $locationNew = $locationModel->getFirstWhere(['loc_id' => $newLocId]);
                if (empty($locationNew)) {
                    throw new \Exception(Message::get("BM017", "new location"));
                }

                //check New location status
                $consolidationLocValidator->checkPalletStatus(object_get($locationNew, 'loc_sts_code', null),
                    "New Location");
                $toLocName = $locationNew->toArray()['loc_alternative_name'];
                $toLocCode = $locationNew->toArray()['loc_code'];

                $params = [
                    'plt_id'   => $input['pallet_id'],
                    'loc_id'   => $newLocId,
                    'loc_name' => $toLocName,
                    'loc_code' => $toLocCode,
                ];

                //New locations already have pallets. Allow to overwrite current pallets by the new ones.
                if (($pallet = $this->palletModel->getFirstBy('loc_id', $params['loc_id'])) &&
                    $pallet->plt_id != $params['plt_id']
                ) {
                    throw new UnknownVersionException(Message::get("BM029", "$pallet->loc_name"), null, 4);
                }

                //update location in Carton
                $cartonInfos = $this->cartonModel->findWhere(
                    ['plt_id' => $input['pallet_id']]
                );
                if (!empty($cartonInfos)) {
                    foreach ($cartonInfos as $cartonInfo) {
                        $paramCartons = [
                            'ctn_id'   => $cartonInfo['ctn_id'],
                            'loc_id'   => $newLocId,
                            'loc_name' => $toLocName,
                            'loc_code' => $toLocCode,
                        ];
                        $this->cartonModel->refreshModel();
                        $this->cartonModel->update($paramCartons);
                    }
                }

                //update location in Pallet
                $this->palletModel->refreshModel();
                $this->palletModel->update($params);
            }

            //delete or Add more a Carton
            foreach ($input['cartons'] as $carton) {
                if (array_get($carton, 'action', '') == "add") {
                    // dd('add');
                    //get location name and location code of carton
                    $locationInfo = $locationModel->getFirstWhere(['loc_id' => array_get($carton, 'loc_id', null)]);
                    if (empty($locationInfo)) {
                        throw new \Exception(Message::get("BM017", "Location"));
                    }

                    //get goods receipt num
                    $grDtlInfo = $this->goodsReceiptDetailModel->getFirstBy('gr_dtl_id', array_get($carton, 'gr_dtl_id',
                        null));
                    if (empty($grDtlInfo)) {
                        throw new \Exception(Message::get("BM017", "Goods Receipt"));
                    }
                    $grHdrId = object_get($grDtlInfo, 'gr_hdr_id', '');
                    $grInfo = $this->goodsReceiptModel->getFirstBy('gr_hdr_id', $grHdrId);
                    if (empty($grInfo)) {
                        throw new \Exception(Message::get("BM017", "Goods Receipt"));
                    }
                    $grNum = object_get($grInfo, 'gr_hdr_num', '');
                    //get Carton Number
                    $ctnNum = $this->cartonModel->getCartonNumber($grNum);

                    $addParams = [
                        'loc_id'        => array_get($carton, 'loc_id', null),
                        'plt_id'        => array_get($carton, 'plt_id', null),
                        'loc_name'      => array_get($locationInfo, 'loc_alternative_name', ''),
                        'loc_code'      => array_get($locationInfo, 'loc_code', ''),
                        'asn_dtl_id'    => array_get($carton, 'asn_dtl_id', null),
                        'gr_dtl_id'     => array_get($carton, 'gr_dtl_id', null),
                        'item_id'       => array_get($carton, 'item_id', null),
                        'whs_id'        => array_get($carton, 'whs_id', null),
                        'cus_id'        => array_get($carton, 'cus_id', null),
                        'ctn_num'       => $ctnNum,
                        'rfid'          => array_get($carton, 'rfid', ''),
                        'ctn_sts'       => array_get($carton, 'ctn_sts', ''),
                        'ctn_uom_id'    => array_get($carton, 'ctn_uom_id', null),
                        'ctn_pack_size' => array_get($carton, 'ctn_pack_size', 0),
                        'is_damaged'    => array_get($carton, 'is_damaged', 0),
                        'is_split'      => array_get($carton, 'is_split', 0),
                        'is_unsplit'    => array_get($carton, 'is_unsplit', 0),
                        'piece_remain'  => array_get($carton, 'piece_remain', 0),
                        'piece_ttl'     => array_get($carton, 'piece_ttl', 0),
                        'is_ecom'       => array_get($carton, 'is_ecom', 0),
                    ];

                    //add more carton
                    $cartonModel->refreshModel();
                    $cartonModel->create($addParams);

                    //update ctn_ttl in pallet
                    $paramsPallet = [
                        'plt_id'  => $input['pallet_id'],
                        'ctn_ttl' => array_get($pallet, 'ctn_ttl', '') + 1,
                    ];
                    $palletModel->refreshModel();
                    if ($updatePallet = $palletModel->update($paramsPallet)) {
                        //update ttl and avail in invt_smr
                        $inventorySummaryInfo = $inventorySummaryModel->getFirstWhere(
                            [
                                "item_id" => array_get($carton, 'item_id', null),
                                "cus_id"  => array_get($carton, 'cus_id', null),
                                "whs_id"  => array_get($carton, 'whs_id', null),
                            ]
                        );
                        if ($inventorySummaryInfo) {
                            //get info
                            $paramsInventorySummary = [
                                'item_id' => array_get($carton, 'item_id', null),
                                'cus_id'  => array_get($carton, 'cus_id', null),
                                'whs_id'  => array_get($carton, 'whs_id', null),
                                'ttl'     => array_get($inventorySummaryInfo, 'ttl', ''),
                                'avail'   => array_get($inventorySummaryInfo, 'avail', ''),
                            ];
                            //update
                            $inventorySummaryModel->refreshModel();
                            $inventorySummaryModel->updateWhere([
                                "ttl"   => $paramsInventorySummary['ttl'] +
                                    array_get($carton, 'piece_remain', 0),
                                "avail" => $paramsInventorySummary['avail'] +
                                    array_get($carton, 'piece_remain', 0),
                            ], [
                                "item_id" => $paramsInventorySummary['item_id'],
                                "cus_id"  => $paramsInventorySummary['cus_id'],
                                "whs_id"  => $paramsInventorySummary['whs_id']
                            ]);
                        }

                    }

                } elseif (array_get($carton, 'action', '') == "del") {
                    $delParams = [
                        'ctn_id' => array_get($carton, 'ctn_id', 0),
                    ];

                    //delete carton
                    $cartonModel->refreshModel();
                    if ($cartonModel->deleteById($delParams['ctn_id'])) {
                        //update ctn_ttl in pallet
                        $paramsPallet = [
                            'plt_id'  => $input['pallet_id'],
                            'ctn_ttl' => array_get($pallet, 'ctn_ttl', '') - 1,
                        ];
                        $palletModel->refreshModel();
                        $palletModel->update($paramsPallet);

                        //update ttl and avail in invt_smr
                        $inventorySummaryInfo = $inventorySummaryModel->getFirstWhere(
                            [
                                "item_id" => array_get($carton, 'item_id', null),
                                "cus_id"  => array_get($carton, 'cus_id', null),
                                "whs_id"  => array_get($carton, 'whs_id', null),
                            ]
                        );
                        if ($inventorySummaryInfo) {
                            //get info
                            $paramsInventorySummary = [
                                'item_id' => array_get($carton, 'item_id', null),
                                'cus_id'  => array_get($carton, 'cus_id', null),
                                'whs_id'  => array_get($carton, 'whs_id', null),
                                'ttl'     => array_get($inventorySummaryInfo, 'ttl', ''),
                                'avail'   => array_get($inventorySummaryInfo, 'avail', ''),
                            ];
                            //update
                            $inventorySummaryModel->refreshModel();
                            $inventorySummaryModel->updateWhere([
                                "ttl"   => $paramsInventorySummary['ttl'] -
                                    array_get($carton, 'piece_remain', 0),
                                "avail" => $paramsInventorySummary['avail'] -
                                    array_get($carton, 'piece_remain', 0),
                            ], [
                                "item_id" => $paramsInventorySummary['item_id'],
                                "cus_id"  => $paramsInventorySummary['cus_id'],
                                "whs_id"  => $paramsInventorySummary['whs_id']
                            ]);
                        }
                    }

                }

            }

            return $this->response->noContent()
                ->setContent(['data' => ['message' => 'Updated successfully']])
                ->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }


    /**
     * @param Request $request
     * @param PalletTransformer $palletTransformer
     *
     * @return Response|void
     */
    public function palletList(Request $request, PalletTransformer $palletTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $getCartonInfo = $this->cartonModel->getDistinctPalletIdByGrIds(
                $input['gr_id']
            );

            $pltIds = array_pluck($getCartonInfo, 'plt_id');

            if ($orderInfo = $this->palletModel->getPalletById($pltIds, array_get($input, 'limit'))
            ) {
                return $this->response->paginator($orderInfo, $palletTransformer);
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $grHdrId
     *
     * @deprecated DON'T USE THIS FUNCTION
     */
    public function updateInventoryByGRReceived($grHdrId)
    {
        $allCartonByItem = $this->cartonModel->getByGRGroupItem($grHdrId, ['item']);

        foreach ($allCartonByItem as $carton) {
            $this->inventorySummaryModel->refreshModel();
            $ivt = $this->inventorySummaryModel->getFirstWhere([
                'item_id' => $carton->item_id,
                'whs_id'  => $carton->whs_id
            ]);

            $ttl = $carton->piece_remain_ttl;
            $dmg = $carton->piece_remain_dmg;

            if ($ivt) {
                $this->inventorySummaryModel->updateWhere([
                    "ttl"     => $ivt->ttl + $ttl,
                    'dmg_qty' => $ivt->dmg_qty + $dmg,
                    "avail"   => $ivt->ttl + $ttl - $ivt->dmg_qty - $dmg,
                ], [
                    "item_id" => $carton->item_id,
                    "whs_id"  => $carton->whs_id
                ]);
            } else {
                $this->inventorySummaryModel->create([
                    'item_id'       => $carton->item_id,
                    'cus_id'        => $carton->cus_id,
                    'whs_id'        => $carton->whs_id,
                    'color'         => object_get($carton, "item.color", null),
                    'sku'           => object_get($carton, "item.sku", null),
                    'size'          => object_get($carton, "item.size", null),
                    'lot'           => object_get($carton, "item.lot", null),
                    'ttl'           => $carton->piece_remain_ttl,
                    'allocated_qty' => 0,
                    'dmg_qty'       => $carton->piece_remain_dmg,
                    'avail'         => $carton->piece_remain_ttl - $carton->piece_remain_dmg,
                    'back_qty'      => 0,
                ]);
            }
        }
    }

    /**
     * @param $grDtls
     */
    public function updateInventory($grDtls)
    {
        foreach ($grDtls as $grDtl) {
            $itemId = array_get($grDtl, 'item_id', null);
            $whsId = array_get($grDtl, 'goods_receipt.whs_id', null);
            $cusId = array_get($grDtl, 'goods_receipt.cus_id', null);
            $lot = array_get($grDtl, 'lot', null);

            $this->inventorySummaryModel->refreshModel();
            $ivt = $this->inventorySummaryModel->getFirstWhere([
                'item_id' => $itemId,
                'lot'     => $lot,
                'whs_id'  => $whsId
            ]);

            $ttl = array_get($grDtl, 'gr_dtl_act_ctn_ttl', 0) * array_get($grDtl, 'pack', 0);
            $xdock = array_get($grDtl, 'crs_doc', 0) * array_get($grDtl, 'pack', 0);
            $dmg = array_get($grDtl, 'gr_dtl_dmg_ttl', 0) * array_get($grDtl, 'pack', 0);
            $avail = $ttl - ($xdock + $dmg);

            if ($ivt) {
                $this->inventorySummaryModel->updateWhere([
                    "ttl"     => $ivt->ttl + $avail,
                    'dmg_qty' => $ivt->dmg_qty + $dmg,
                    "avail"   => $ivt->avail + $avail
                ], [
                    "item_id" => $itemId,
                    "whs_id"  => $whsId,
                    'lot'     => $lot,
                ]);
            } else {
                $this->inventorySummaryModel->create([
                    'item_id'       => $itemId,
                    'cus_id'        => $cusId,
                    'whs_id'        => $whsId,
                    'color'         => array_get($grDtl, "color", null),
                    'sku'           => array_get($grDtl, "sku", null),
                    'size'          => array_get($grDtl, "size", null),
                    'lot'           => $lot,
                    'ttl'           => $ttl,
                    'allocated_qty' => 0,
                    'dmg_qty'       => $dmg,
                    'avail'         => $avail,
                    'back_qty'      => 0,
                    'upc'           => array_get($grDtl, "upc", 'NA'),

                ]);
            }
        }
    }

    /**
     * @param $asnHdrId
     * @param string $grHdrNum
     * @param $grActCarTtl
     */
    private function updateStatusAsn($asnHdrId, $grHdrNum = "", $grActCarTtl)
    {
        $this->asnDtlModel->refreshModel();
        $asnDtls = $this->asnDtlModel->findWhere(['asn_hdr_id' => $asnHdrId], ['asnHdr'])->toArray();

        $asnNum = array_get($asnDtls, "0.asn_hdr.asn_hdr_num", null);
        $whsId = array_get($asnDtls, "0.asn_hdr.whs_id", null);
        $cusId = array_get($asnDtls, "0.asn_hdr.cus_id", null);

        // Add Event Tracking Goods Receipt
        $this->eventTrackingModel->refreshModel();
        $this->eventTrackingModel->create([
            'whs_id'    => $whsId,
            'cus_id'    => $cusId,
            'owner'     => $grHdrNum,
            'evt_code'  => Status::getByKey("Event", "GR-COMPLETE"),
            'trans_num' => $grHdrNum,
            'info'      => sprintf(Status::getByKey("Event-info", "GR-COMPLETE"), $grActCarTtl)
        ]);

        $update = true;
        foreach ($asnDtls as $asnDtl) {
            if ($asnDtl['asn_dtl_sts'] != "RE") {
                $update = false;
            }
        }

        if ($update) {
            $this->asnHdrModel->updateWhere(['asn_sts' => 'RE'], ['asn_hdr_id' => $asnHdrId]);

            // Add Event Tracking ASN Complete

            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $whsId,
                'cus_id'    => $cusId,
                'owner'     => $asnNum,
                'evt_code'  => Status::getByKey("Event", "ASN-COMPLETE"),
                'trans_num' => $asnNum,
                'info'      => sprintf(Status::getByKey("Event-info", "ASN-COMPLETE"), $asnNum)
            ]);
        }
    }

    public function consolidation(
        Request $request,
        ConsolidationPltValidator $consolidationPltValidator
    ) {

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        // get data from HTTP
        $input = $request->getParsedBody();
        $whsId = Data::getCurrentWhsId();
        $itemIds = explode(',', array_get($input, 'item_ids'));
        $itemIds = array_filter($itemIds, function($itemId) {
            return $itemId;
        });

        // Validate 1 lpn 1 sku
        $this->validateLpnSku($input['lpn_from'], $input['lpn_to']);

        if(strpos($input['lpn_from'], "P-") === false){
            //$listCartonFrom = $this->cartonModel->getModel()->where("loc_code", $input['lpn_from'])->where("whs_id", $whsId)->get()->toArray();
            $query_from = $this->cartonModel->getModel()->where("loc_code", $input['lpn_from'])->where("whs_id", $whsId);
        } else {
            //$listCartonFrom = $this->cartonModel->getModel()->where("lpn_carton", $input['lpn_from'])->where("whs_id", $whsId)->get()->toArray();
            $query_from = $this->cartonModel->getModel()->where("lpn_carton", $input['lpn_from'])->where("whs_id", $whsId);
        }

        if ($itemIds) {
            $query_from->whereIn("item_id", $itemIds);
        }

        $listCartonFrom = $query_from->select(['plt_id', 'loc_id', 'loc_code', 'ctn_id', 'ctn_num'])->get()->toArray();

        $is_location = false;
        $cartonTo = [];
        if(strpos($input['lpn_to'], "P-") === false){
            $is_location = true;
            $listCartonTo = $this->cartonModel->getModel()
                                ->where("loc_code", $input['lpn_to'])
                                ->where("whs_id", $whsId)
                                ->select(['plt_id', 'loc_id', 'loc_code', 'ctn_id', 'ctn_num'])
                                ->get()
                                ->toArray();
            if(!empty($listCartonTo)){
                $cartonTo = $listCartonTo[0];
            }
        } else {
            $listCartonTo = $this->cartonModel->getModel()
                                ->where("lpn_carton", $input['lpn_to'])
                                ->where("whs_id", $whsId)
                                ->select(['plt_id', 'loc_id', 'loc_code', 'ctn_id', 'ctn_num'])
                                ->get()
                                ->toArray();
            if(!empty($listCartonTo)){
                $cartonTo = $listCartonTo[0];
            }
        }
        if (empty($listCartonFrom)) {
            throw new \Exception("LPN or Location From is not exsit!");
        }

//        $itemIdsFrom = array_unique(array_column($listCartonFrom, 'item_id'));
//        $itemIdsTo = array_unique(array_column($listCartonTo, 'item_id'));
//        if(!empty(array_diff($itemIdsFrom,$itemIdsTo))){
//            $msg = sprintf("From Item does not contain in To Item ");
//            throw new \Exception($msg);
//        }
        $fromPltId = array_get($listCartonFrom, "0.plt_id");
        $fromLocId = array_get($listCartonFrom, "0.loc_id");
        $ctnIds = array_column($listCartonFrom, 'ctn_id');
        $ctnNums = array_column($listCartonFrom, 'ctn_num');
        // validation
        $consolidationPltValidator->validate($input);
        $currentLocationAndPalletInfo = $consolidationPltValidator->checkCurrentLoc($fromPltId);

        $toPltId = $fromPltId;
        $is_location_empty = false;
        if(!empty($cartonTo)) {
            $toPltId = array_get($cartonTo, "plt_id");
            $newLocationInfo = $consolidationPltValidator->check($toPltId, $ctnIds);
        } else {
            $msg = sprintf("Location %s is empty!", $input['lpn_to']);
            throw new \Exception($msg);
            /*$is_location_empty = true;
            $newLocationInfo = $this->locationModel->getModel()->where("loc_code", $input['lpn_to'])->where("loc_whs_id", $whsId)->first();
            if (empty($newLocationInfo->loc_zone_id)) {
                $msg = sprintf("This Location %s is not exited",$input['lpn_to']);
                throw new \Exception($msg);
            }*/
        }
        $palletFrom = $currentLocationAndPalletInfo[1];
        /*$palletTo = $this->palletModel->checkPltSameCustomer(
            $palletFrom->whs_id, $palletFrom->cus_id, $toPltId
        );*/

        $palletTo = $this->palletModel->checkPalletByPltId(
            $palletFrom->whs_id, $toPltId
        );

//        $cartonPallet = DB::table('cartons')
//                        ->where('cartons.whs_id', $palletFrom->whs_id)
//                        ->where('cartons.plt_id', $toPltId)
//                        ->first();
//
//        $palletTo = DB::table('pallet')
//                    ->where('plt_id', $cartonPallet['plt_id'])
//                    ->where('pallet.deleted', '=', 0)
//                    ->where('pallet.plt_sts', 'AC')
//                    ->first();
//        $palletTo
        $cartonsFrom = object_get($palletFrom, 'carton');
        $itemIdFrom =array_get($cartonsFrom, '0.item_id');
        $cartonsTo = object_get($palletTo, 'carton');
        $itemIdTo =array_get($cartonsTo, '0.item_id');

        $itemFrom = DB::table('item')->where('item_id', $itemIdFrom)->first();
        $itemTo = DB::table('item')->where('item_id', $itemIdTo)->first();

        if(empty($cartonTo)) {
            throw new \Exception(sprintf("Location %s is empty!", $input['lpn_to']));
        }

        /*if(($itemFrom['spc_hdl_code'] == "BIN" || $itemFrom['spc_hdl_code'] == "SHE") &&  $is_location == false) {
            $msg = 'Special handling code ' . $itemFrom['spc_hdl_code'] . " must scans Location";
            throw new \Exception($msg);
        }*/

        /*if (empty($palletTo)) {
            $msg = 'Source and destination must be belonged to a same customer';
            throw new \Exception($msg);
        }*/

        if(empty($itemFrom)) {
            $msg = 'Current LPN not have item';
            throw new \Exception($msg);
        }
//        if(empty($itemTo)) {
//            $msg = 'To LPN not have item';
//            throw new \Exception($msg);
//        }

        if ($palletTo['plt_sts'] == "RG" || $palletFrom['plt_sts'] == "RG") {
            $msg = 'Can not consolidate pallet with status Receiving';
            throw new \Exception($msg);
        }
        $toLocId = object_get($newLocationInfo, 'loc_id', 0);
        if($toLocId == $fromLocId) {
            $msg = 'Can not consolidate same location';
            throw new \Exception($msg);
        }
        $toLocName = object_get($newLocationInfo, 'loc_alternative_name', '');
        $toLocCode = object_get($newLocationInfo, 'loc_code', '');
        $lpn_carton = array_get($cartonTo, 'lpn_carton', '');
        $fromLoc = DB::table("location")->where("loc_id",$fromLocId)->first();
//        if(object_get($newLocationInfo, 'spc_hdl_code', '') !== array_get($fromLoc, "spc_hdl_code","")) {
//            $msg = 'Current LPN and New LPN do not belong to the same Special Handling';
//            throw new \Exception($msg);
//        }

        try {
            //update carton to new location
            $data_rpt_pallet = [
                'loc_id'   => $toLocId,
                'loc_name' => $toLocName,
                'plt_id'   => $toPltId,
                'loc_code' => $toLocCode,
            ];
            if($is_location == true) {
                $data = [
                    'loc_id'   => $toLocId,
                    'loc_name' => $toLocName,
                    'plt_id'   => $toPltId,
                    'loc_code' => $toLocCode,
                ];
            } else {
                $data = [
                    'loc_id'   => $toLocId,
                    'loc_name' => $toLocName,
                    'plt_id'   => $toPltId,
                    'loc_code' => $toLocCode,
                    'lpn_carton' => $lpn_carton
                ];
            }
            DB::beginTransaction();
//            $this->cartonModel->updateWhereIn($data, $ctnIds, 'ctn_id');
            foreach (array_chunk($ctnIds, 200) as $dataIds) {
                $this->cartonModel->updateWhereIn($data, $dataIds, 'ctn_id');
            }
//            DB::table('rpt_carton')->whereIn('ctn_id', $ctnIds)->update(
//                $data_rpt_pallet
//            );
            foreach (array_chunk($ctnIds, 200) as $dataIds) {
                DB::table('rpt_carton')->whereIn('ctn_id', $dataIds)->update(
                    $data_rpt_pallet
                );
            }
            $fromLocCode = object_get($currentLocationAndPalletInfo[0], 'loc_code', '');
            $CurrentWhsId = object_get($currentLocationAndPalletInfo[1], 'whs_id', 0);
            $CurrentCusId = object_get($currentLocationAndPalletInfo[1], 'cus_id', 0);

            //event tracking carton location
//            foreach ($ctnNums as $ctn_num) {
//                // tracking carton location
//                $evt_owner = $this->palletModel->generateEventOwner('CON');
//                $this->eventTrackingModel->refreshModel();
//
//                $this->eventTrackingModel->create([
//                    'whs_id'    => $CurrentWhsId,
//                    'cus_id'    => $CurrentCusId,
//                    'owner'     => $evt_owner,
//                    'evt_code'  => config('constants.event.CON-CTN-LOC'),
//                    'trans_num' => $evt_owner,
//                    'info'      => sprintf(config('constants.event-info.CON-CTN-LOC'), $ctn_num, $fromLocCode,
//                        $toLocCode)
//
//                ]);
//
//                $evt_owner = $this->palletModel->generateEventOwner('CON');
//                $this->eventTrackingModel->refreshModel();
//
//                $this->eventTrackingModel->create([
//                    'whs_id'    => $CurrentWhsId,
//                    'cus_id'    => $CurrentCusId,
//                    'owner'     => $evt_owner,
//                    'evt_code'  => config('constants.event.CON-LP-LOC'),
//                    'trans_num' => $evt_owner,
//                    'info'      => sprintf(config('constants.event-info.CON-LP-LOC'), $ctn_num,
//                        $palletTo['plt_num'], $palletTo['plt_num'])
//                ]);
//            }

            $evt_owner = $this->palletModel->generateEventOwner('CON');
            //event tracking carton location complete
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create([
                'whs_id'    => $CurrentWhsId,
                'cus_id'    => $CurrentCusId,
                'owner'     => $evt_owner,
                'evt_code'  => config('constants.event.COMPLETE-CON'),
                'trans_num' => $evt_owner,
                'info'      => sprintf(config('constants.event-info.COMPLETE-CON'), $evt_owner)
            ]);

            //Remove loc_id from pallet when pallet is empty
            $fromLocId = $currentLocationAndPalletInfo[0]->loc_id;
            $arr_loc_id = [$fromLocId];


            $ctn_ttl = DB::table('cartons')->where('plt_id', $toPltId)->count();
            // update total carton pallet
            $this->palletModel->updateWhere(
                ["ctn_ttl" => $ctn_ttl],
                ['plt_id' => $toPltId]
            );
            $ctn_ttl = DB::table('cartons')->where('plt_id', $fromPltId)->count();
            $this->palletModel->updateWhere(
                ["ctn_ttl" => $ctn_ttl],
                ['plt_id' => $fromPltId]
            );


            if (empty($this->palletModel->getFirstWhere(['loc_id' => $fromLocId])->ctn_ttl)
            ) {
                $this->palletModel->removeWhereInLocation($arr_loc_id);
                DB::table("location")
                    ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
                    ->join('zone_type', 'zone_type.zone_type_id', '=', 'zone.zone_type_id')
                    ->where("location.loc_id", $fromLocId)
                    ->where("zone_type.zone_type_code", "DZ")
                    ->update(['location.loc_zone_id' => null]);
            }
            /*$loc_zone_id = DB::table("location")->where("location.loc_id", $fromLocId)->where("location.loc_whs_id", $whsId)->value("loc_zone_id");
            DB::table("location")->where("loc_id", $toLocId)->update(["loc_zone_id" =>$loc_zone_id]);*/

            /*DB::table("pal_sug_loc")
                ->where("plt_id", $palletFrom->plt_id)
                ->where("loc_id", $palletFrom->loc_id)
                ->update([
                    'plt_id' => $toPltId,
                    'loc_id' => $toLocId,
                    'data' => $toLocCode,
                    'act_loc_id' => $toLocId,
                    'act_loc_code' => $toLocCode
                ]);*/

            DB::table('rpt_pallet')->where('plt_id', $toPltId)->update(
                ["ctn_ttl" =>  $this->cartonModel->checkWhere(['plt_id' => $toPltId])]
            );
            $this->palletModel->updatePalletZeroDateAndDurationDaysAuto();
            DB::commit();
            return response()->json([
                'message' => "Consolidate Successfully!"
            ]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param ConsolidationPltValidator $consolidationPltValidator
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function consolidate(Request $request, ConsolidationPltValidator $consolidationPltValidator)
    {
        $request = $request->getParsedBody();
        $consolidationPltValidator->validate($request);

        $whsId = Data::getCurrentWhsId();
        $itemIds = explode(',', array_get($request, 'item_ids'));

        if (empty($itemIds)) {
            return $this->response()->errorBadRequest('Please select one or more SKU');
        }

        $currentLocOrLPN = array_get($request, 'lpn_from');
        $toLocOrLPN = array_get($request, 'lpn_to');
        $curLPNCarton = null;
        $toLPNCarton = null;

        if ($currentLocOrLPN === $toLocOrLPN) {
            return $this->response()->errorBadRequest('Cannot consolidate to original LPN or Location');
        }

        $curCartons = $this->cartonModel->getModel()
            ->where('ctn_sts', 'AC')
            ->where('whs_id', $whsId)
            ->whereIn('item_id', $itemIds)
            ->select(['plt_id', 'whs_id', 'cus_id', 'loc_id', 'loc_code', 'loc_name', 'lpn_carton']);

        if ($this->isLPN($currentLocOrLPN)) {
            $curLPNCarton = $currentLocOrLPN;
            $curCartons = $curCartons->where('lpn_carton', $currentLocOrLPN);
            $currentLocOrLPN = $this->cartonModel->getModel()
                ->where('lpn_carton', '=', $currentLocOrLPN)
                ->where('whs_id', '=', $whsId)
                ->value('loc_code');
        } else {
            $curCartons = $curCartons->where('loc_code', $currentLocOrLPN);
        }

        $curCartons = $curCartons->groupBy('plt_id', 'cus_id', 'loc_id')->get()->toArray();
        if (empty($curCartons)) {
            return $this->response()->errorBadRequest(sprintf('LPN Or Location %s dose not have any cartons', $currentLocOrLPN));
        }
        $curCusIds = array_unique(array_pluck($curCartons, 'cus_id'));

        if ($this->isLPN($toLocOrLPN)) {
            return $this->response()->errorBadRequest('You can only consolidate to location');
//            $toLPNCarton = $toLocOrLPN;
//            $toItemIds = $this->cartonModel->getModel()
//                ->where('lpn_carton', $toLocOrLPN)
//                ->where('whs_id', $whsId)
//                ->groupBy('item_id')
//                ->pluck('item_id')
//                ->toArray();
//            if (! empty(array_diff($itemIds, $toItemIds))) {
//                return $this->response()->errorBadRequest(sprintf('The LPN %s does not have any cartons of this SKU', $toLocOrLPN));
//            }
//            $toLocOrLPN = $this->cartonModel->getModel()
//                ->where('lpn_carton', '=', $toLocOrLPN)
//                ->where('whs_id', '=', $whsId)
//                ->value('loc_code');
        } else {
            $checkLocation = $this->locationModel->getModel()->where([
                'loc_code' => $toLocOrLPN,
                'loc_whs_id' => $whsId,
                'parent_id' => null,
                'loc_sts_code' => 'AC'
            ])->value('loc_sts_code');
            if (! $checkLocation) {
                return $this->response()->errorBadRequest(sprintf('Location %s is not active', $toLocOrLPN));
            }

            // fix consolidate live site - without feature consolidate to location empty
//            $countCarton = $this->cartonModel->getModel()
//                ->where('whs_id', $whsId)
//                ->where('loc_code', $toLocOrLPN)
//                ->groupBy('whs_id', 'loc_code')
//                ->count();
//            if ($countCarton === 0) {
//                return $this->response()->errorBadRequest('Cannot consolidate to empty location');
//            }
            // end fix consolidate live site - without feature consolidate to location empty
        }

        // check missing pallet
//        $cusNotHasPallets = [];
//        foreach ($curCusIds as $key => $cusId) {
//            $pallet = $this->palletModel->getFirstWhere([
//                'whs_id' => $whsId,
//                'cus_id' => $cusId,
//                'loc_code' => $currentLocOrLPN,
//                //'plt_sts' => 'AC'
//            ]);
//            if (empty($pallet)) {
//                $cusNotHasPallets[] = $cusId;
//            }
//        }
//        if (count($cusNotHasPallets) > 0) {
//            return $this->response()->errorBadRequest('Missing pallet for customer');
//        }
        // end check missing pallet

        try {
            DB::beginTransaction();

            if (! empty($curCusIds)) {
                foreach ($curCusIds as $cusId) {
                    $toLocation = $this->locationModel->getFirstWhere([
                        'loc_code' => $toLocOrLPN,
                        'loc_whs_id' => $whsId,
                        'parent_id' => null,
                        'loc_sts_code' => 'AC'
                    ]);

                    $toPallet = $this->palletModel->getFirstWhere([
                        'whs_id' => $whsId,
                        'cus_id' => $cusId,
                        'loc_code' => $toLocOrLPN,
                        //'plt_sts' => 'AC'
                    ]);

                    if (empty($toPallet)) {
                        // create pallet
                        $palletNum = $this->palletModel->generatePltNum();
                        $this->palletModel->refreshModel();
                        $toPallet = $this->palletModel->create([
                            'plt_num' => $palletNum,
                            'loc_id' => object_get($toLocation, 'loc_id'),
                            'loc_code' => object_get($toLocation, 'loc_code'),
                            'loc_name' => object_get($toLocation, 'loc_code'),
                            'whs_id' => object_get($toLocation, 'loc_whs_id'),
                            'cus_id' => $cusId,
                            'plt_sts' => 'AC',
                            'created_at' => time(),
                            'created_by' => Data::getCurrentUserId(),
                            'updated_at' => time(),
                            'updated_by' => Data::getCurrentUserId(),
                            'ctn_ttl' => 0
                        ]);
                    }

                    // update cartons
                    $cartonModel = [
                        'plt_id' => $toPallet->plt_id,
                        'loc_id' => $toPallet->loc_id,
                        'loc_code' => $toPallet->loc_code,
                        'loc_name' => $toPallet->loc_code,
                        'updated_at' => time(),
                        'updated_by' => Data::getCurrentUserId()
                    ];

                    if ($toLPNCarton) {
                        $cartonModel['lpn_carton'] = $toLPNCarton;
                    }

                    $whereCarton = [
                        'whs_id' => $whsId,
                        'cus_id' => $cusId,
                        'ctn_sts' => 'AC'
                    ];

                    if ($curLPNCarton) {
                        $whereCarton['lpn_carton'] = $curLPNCarton;
                    } else {
                        $whereCarton['loc_code'] = $currentLocOrLPN;
                    }

                    // consolidate
                    $this->cartonModel->refreshModel();
                    foreach (array_chunk($itemIds, 200) as $chunkIds) {
                        $this->cartonModel->getModel()->where($whereCarton)->whereIn('item_id', $chunkIds)->update($cartonModel);
                    }

                    // update to pallet
                    $this->updateCartonTotalInPallet($toPallet);

                    // update current pallet
                    $currentPallet = $this->palletModel->getFirstWhere([
                        'whs_id' => $whsId,
                        'cus_id' => $cusId,
                        'loc_code' => $currentLocOrLPN,
                        //'plt_sts' => 'AC'
                    ]);

                    if (! empty($currentPallet)) {
                        $this->updateCartonTotalInPallet($currentPallet);
                    }
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'Consolidate Successfully'
            ]);

        } catch (\PDOException $e) {
            DB::rollback();
            dd($e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $lpnCarton
     * @return bool|void
     */
    public function isLPN($lpnCarton)
    {
        if (strpos($lpnCarton, 'P-') === false) {
            return false;
        }
        if (! $this->validateLPNFormat($lpnCarton)) {
            return $this->response()->errorBadRequest(sprintf('LPN %s is not valid', $lpnCarton));
        }
        return true;
    }

    /**
     * @param $pallet
     */
    public function updateCartonTotalInPallet($pallet)
    {
        $cartonTotal = $this->cartonModel->getModel()
            ->where('whs_id', '=', $pallet->whs_id)
            ->where('cus_id', '=', $pallet->cus_id)
            ->where('plt_id', '=', $pallet->plt_id)->count();

        $this->palletModel->refreshModel();
        $pallet->ctn_ttl = $cartonTotal;
        $pallet->updated_at = time();
        $pallet->updated_by = Data::getCurrentUserId();

        if ($cartonTotal === 0) {
            $pallet->loc_id = null;
            $pallet->loc_code = null;
        }

        $pallet->save();
    }

    private function validateLpnSku($lpn_from, $lpn_to) {
        if($this->validateLPNFormat($lpn_from)) {
            $carton_from = DB::table("cartons")
                ->where("lpn_carton", $lpn_from)->where("deleted", 0)
                ->select("item_id")
                ->groupBy("item_id")->get();
            if(count($carton_from) < 1) {
                $msg = sprintf('The LPN %s does not use for any items.', $lpn_from);
                throw new \Exception($msg);
            }
        }else {

            $location = DB::table("location")
                ->where("loc_code", $lpn_from)->where("deleted", 0)->get();

            if(empty($location)){
                $msg = sprintf('The Location %s does not exist.', $lpn_from);
                throw new \Exception($msg);
            }
            $carton_from = DB::table("cartons")
                ->where("loc_code", $lpn_from)->where("deleted", 0)->select("item_id")
                ->groupBy("item_id")->get();
            if(count($carton_from) < 1) {
                $msg = sprintf('The Location %s does not empty.', $lpn_from);
                throw new \Exception($msg);
            }
        }

        $item_ids = array_pluck($carton_from,"item_id");

        if(empty($carton_from)) {
            $msg = sprintf('The LPN %s does not valid.', $lpn_from);
            throw new \Exception($msg);
        }

        /*if($this->validateLPNFormat($lpn_to)) {
            $carton_to = DB::table("cartons")->where("lpn_carton", $lpn_to)
                ->whereIn("item_id", $item_ids)
                ->where("deleted", 0)->select("item_id")->first();
            if(!empty($carton_to)) {
                $msg = sprintf('The LPN %s is used for another SKU.', $lpn_to);
                throw new \Exception($msg);
            }
            return 0;
        }*/
        if ($this->validateLPNFormat($lpn_to)) {

            $carton_to = DB::table("cartons")
                ->where(function ($query) use ($lpn_to) {
                    $query->where('loc_code', $lpn_to);
                    $query->orwhere('lpn_carton', $lpn_to);
                })
                ->where("deleted", 0)->select("item_id")->groupBy("item_id")->get();
            $item_ids_to = array_pluck($carton_to,"item_id");

            $result=array_diff($item_ids,$item_ids_to);
            if (!empty($result)) {
                $msg = sprintf('The Location/LPN %s does not have any cartons of this SKU.', $lpn_to);
                throw new \Exception($msg);
            }
        }


        return 0;
    }
    public function validateLPNFormat($plt_num){
        if((bool)preg_match("/^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }
        if((bool)preg_match("/^T-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }

        if((bool)preg_match("/^V-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/", $plt_num)) {
            return true;
        }
        return false;
    }
}
