<?php

namespace App\Api\V1\Validators;


class ReportConfigsValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'qualifier'         => 'required|string|max:3',
            'data'              => 'required',
            'ref'               => 'required',
            'level'             => 'required'
        ];
    }
}
