<?php

namespace App\Api\Inventory\Controllers;

use App\Api\Inventory\Models\ReasonModel;
use App\Api\Inventory\Validators\ReasonValidator;
use Illuminate\Support\Facades\Validator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\Inventory\Transformers\ReasonTransformer;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\Inventory\Models\EventTrackingModel;

class ReasonController extends AbstractController
{
    protected $model;
    protected $validator;
    protected $request;
    protected $error;
    protected $eventTrackingModel;

    protected $reasonTransform;

    /**
     * ReasonController constructor.
     * @param ReasonModel $model
     * @param ReasonValidator $validator
     * @param Request $request
     * @param EventTrackingModel $eventTrackingModel
     * @param ReasonTransformer $reasonTransformer
     */

    public function __construct(
        ReasonModel $model,
        ReasonValidator $validator,
        Request $request,
        EventTrackingModel $eventTrackingModel,
        ReasonTransformer $reasonTransformer
    )
    {
        $this->model = $model;
        $this->validator = $validator;
        $this->request = $request;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->reasonTransform = $reasonTransformer;
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response|void
     */

    public function store(Request $request)
    {
        $input = $request->getParsedBody();

        $this->validator->validate($input);

        try {

            $data = $this->model->processCreateBlockReason($input);

            if (! $data) {
                return $this->response->errorBadRequest(implode($this->model->errors));
            }

            return $this->response->item($data, new ReasonTransformer());

        } catch (\Exception $e) {

            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);

        }
    }

    /**
     * @return mixed
     */

    public function reasonList()
    {
        $input = $this->request->getQueryParams();

        try {
            $data = $this->model->search($input, ['createdBy'], array_get($input, 'limit', 20));

            return $this->response->paginator($data, $this->reasonTransform);

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }
}
