<?php
//pt: list Items belong to carton

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Zone;

class ListAllLocationTransformer extends TransformerAbstract
{
    //return result
    public function transform(Zone $zone)
    {
        return [
            'zone_name' => object_get($zone, 'zone_name', null),
            'zone_id'   => object_get($zone, 'zone_id', null),
            'zone_code' => object_get($zone, 'zone_code', null),
            'positions' => $zone->locationZone,
        ];
    }


}
