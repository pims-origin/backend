<?php

namespace App\Modules\Orders\Eloquents;

use Illuminate\Database\Eloquent\Model;

class OdrHdrMeta extends Model
{
    protected $table = 'odr_hdr_meta';

    protected $primaryKey = 'odr_id';

    protected $fillable = [
        'odr_id',
        'qualifier',
        'value'
    ];

    public function getDateFormat()
    {
        return 'U';
    }
}
