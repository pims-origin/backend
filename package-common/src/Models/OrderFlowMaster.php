<?php

namespace Seldat\Wms2\Models;


class OrderFlowMaster extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'odr_flow_master';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_flow_id';

    /**
     * @var array
     */
    protected $fillable = [
        'odr_flow_id',
        'step',
        'flow_code',
        'odr_sts',
        'name',
        'description',
        'dependency',
        'type'
    ];


}
