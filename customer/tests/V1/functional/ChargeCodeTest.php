<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 13:10
 */

use Illuminate\Http\Response as IlluminateResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ChargeCodeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * ChargeCodeTest constructor.
     */

    public function getEndPoint()
    {
        return "/v1/charge-codes";
    }


    protected $searchParams = [
        'chg_type_code' => 'abc',
        'chg_type_name' => 1,
        'chg_uom_name'  => 2,
    ];

    protected $chargeCodeParams = [
        'chg_code'    => 'c0d3',
        'description' => 'description'
    ];

    public function setUp()
    {
        parent::setUp();

        $chgType = factory(\Seldat\Wms2\Models\ChargeType::class)->create();
        $uom = factory(\Seldat\Wms2\Models\SystemUom::class)->create();

        $this->chargeCodeParams['chg_type_id'] = $chgType->chg_type_id;
        $this->chargeCodeParams['chg_uom_id'] = $uom->sys_uom_id;
    }

    public function testSearchChargeCode_Ok()
    {
        $this->call('GET', $this->getEndPoint(), $this->searchParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSearch_NoParam_Ok()
    {
        $this->call('GET', $this->getEndPoint());
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testRead_Ok()
    {
        $chargeCodeId = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;

        $this->call('GET', $this->getEndPoint() . "/{$chargeCodeId}");
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $this->call('POST', $this->getEndPoint(), $this->chargeCodeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);
    }

    public function testStore_EmptyField_Fail()
    {
        unset($this->chargeCodeParams['chg_code']);
        $this->call('POST', $this->getEndPoint(), $this->chargeCodeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStore_ExistUniqueField_Fail()
    {
        $chargeCode = factory(\Seldat\Wms2\Models\ChargeCode::class)->create();
        $this->chargeCodeParams['chg_code'] = $chargeCode->chg_code;

        $this->call('POST', $this->getEndPoint(), $this->chargeCodeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testStore_InvalidField_Fail()
    {
        $this->chargeCodeParams['chg_uom_id'] = "abc";

        $this->call('POST', $this->getEndPoint(), $this->chargeCodeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        $chg_code_id = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;

        $this->call('PUT', $this->getEndPoint() . "/{$chg_code_id}", $this->chargeCodeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdate_NotExist_Fail()
    {
        $abc = $this->call('PUT', $this->getEndPoint() . "/999999", $this->chargeCodeParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_ExistUniqueFields_Fail()
    {
        $chargeCode1 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create();
        $chargeCode2 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create();

        $this->chargeCodeParams['chg_code'] = $chargeCode1->chg_code;

        $this->call('PUT', $this->getEndPoint() . "/{$chargeCode2->chg_code_id}", $this->chargeCodeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testUpdate_EmptyField_Fail()
    {
        $chg_code_id = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;

        unset($this->chargeCodeParams['chg_uom_id']);
        $this->call('PUT', $this->getEndPoint() . "/{$chg_code_id}", $this->chargeCodeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_InvalidField_Fail()
    {
        $chg_code_id = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;

        $this->chargeCodeParams['chg_uom_id'] = "abc";
        $this->call('PUT', $this->getEndPoint() . "/{$chg_code_id}", $this->chargeCodeParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDelete_Ok()
    {
        $chg_code_id = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;

        $this->call('DELETE', $this->getEndPoint() . "/{$chg_code_id}");

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }

    public function testDelete_NotExit_Fail()
    {
        $this->call('DELETE', $this->getEndPoint() . "/9999999999999999");

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDeleteMass_Ok()
    {
        $chg_code_id1 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;
        $chg_code_id2 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;
        $chg_code_id3 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;
        $chg_code_id4 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;
        $chg_code_id5 = factory(\Seldat\Wms2\Models\ChargeCode::class)->create()->chg_code_id;

        $this->call('DELETE', $this->getEndPoint(),
            ['chg_code_id' => [$chg_code_id1, $chg_code_id2, $chg_code_id3, $chg_code_id4, $chg_code_id5]]);
        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);
    }
}