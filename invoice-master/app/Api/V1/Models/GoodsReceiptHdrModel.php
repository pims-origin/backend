<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use App\GoodsReceipt;
use App\Wms2\UserInfo\Data;

/**
 * Class GoodsReceiptHdrModel
 *
 * @package App\Api\V1\Models
 */
class GoodsReceiptHdrModel extends AbstractModel
{
    /**
     * OrderHdrModel constructor.
     *
     * @param GoodsReceipt|null $model
     */
    public function __construct(GoodsReceipt $model = null)
    {
        $this->model = ($model) ?: new GoodsReceipt();
    }
    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);

        $searchEqualFields = [
            'cus_id'
        ];

        $searchLikeFields = [
            'gr_hdr_num'
        ];

        foreach ($attributes as $key => $value) {
            //search equal
            if (in_array($key, $searchEqualFields)) {
                if ($value != '') {
                    $query->where($this->getTable().'.' . $key, $value);
                }
            }
            //search like
            if (in_array($key, $searchLikeFields)) {
                if ($value != '') {
                    $query->where($this->getTable().'.' . $key, 'like', '%' . escape_like($value) . '%');
                }
            }
        }
        //filter GR that gr_hdr_num not in ref_num_arr array
        if (!empty($attributes['ref_num_arr'])) {
            $query->whereNotIn($this->getTable().'.gr_hdr_num', $attributes['ref_num_arr']);
        }

        //Filter whs_id and cus_id
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where($this->getTable().'.whs_id', $currentWH);
        $cus_ids = $predis->getCurrentCusIds();
        $query->whereIn($this->getTable().'.cus_id', $cus_ids);

        $models = $query->paginate($limit);

        return $models;
    }
}
