<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

/**
 * Class PackHdr
 *
 * @package Seldat\Wms2\Models
 */
class PackHdr extends BaseInvoiceModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pack_hdr';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pack_hdr_id';
    /**
     * @var array
     */
    protected $fillable = [
        'pack_hdr_num',
        'seq',
        'odr_hdr_id',
        'whs_id',
        'cus_id',
        'carrier_name',
        'sku_ttl',
        'piece_ttl',
        'pack_sts',
        'sts',
        'ship_to_name',
        'pallet_id',
        'pack_type',
        'width',
        'height',
        'length',
        'pack_ref_id',
        'pack_dt_checksum',
        'is_print',
        'is_scan',
        'tracking_number'
    ];

    public function delete()
    {
        $this->details()->delete();

        return parent::delete();
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function details()
    {
        return $this->hasMany(__NAMESPACE__ . '\PackDtl', 'pack_hdr_id', 'pack_hdr_id');
    }

    public function item()
    {
        return $this->hasOne(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function systemUom()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemUom', 'sys_uom_id', 'uom_id');
    }

    public function orderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_hdr_id');
    }

    public function packRef()
    {
        return $this->hasOne(__NAMESPACE__ . '\PackRef', 'pack_ref_id', 'pack_ref_id');
    }

    public function outPallet()
    {
        return $this->hasOne(__NAMESPACE__ . '\outPallet', 'plt_id', 'out_plt_id');
    }
}
