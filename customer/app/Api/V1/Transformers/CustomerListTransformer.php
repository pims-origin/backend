<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\Customer;
use League\Fractal\TransformerAbstract;

class CustomerListTransformer extends TransformerAbstract
{
    public function transform(Customer $customer)
    {
        $firstName = object_get($customer, 'customerContact.cus_ctt_fname', null);
        $lastName = object_get($customer, 'customerContact.cus_ctt_lname', null);
        $customerName = $firstName . ' ' . $lastName;

        return [
            'cus_id'              => $customer->cus_id,
            'cus_name'            => $customer->cus_name,
            'cus_status'          => $customer->cus_status,
            'cus_billing_account' => $customer->cus_billing_account,
            'cus_des'             => $customer->cus_des,
            'cus_status_name'     => object_get($customer, 'customerStatus.cus_sts_name', null),
            'cus_status_desc'     => object_get($customer, 'customerStatus.cus_sts_desc', null),
            'cus_code'            => $customer->cus_code,
            'cus_country_id'      => $customer->cus_country_id,
            'cus_country_name'    => object_get($customer, 'systemCountry.sys_country_name', null),
            'cus_state_id'        => $customer->cus_state_id,
            'cus_state_name'      => object_get($customer, 'systemState.sys_state_name', null),
            'cus_city_name'       => $customer->cus_city_name,
            'cus_postal_code'     => $customer->cus_postal_code,
            'cus_zone'            => object_get($customer, 'customerZone', null),

            // Customer Warehouse Info
            'whs_id'              => $customer->customerWarehouse,
            // 'whs_name' => $customer->cus_whs,

            // Customer Contact Info
            'cus_ctt_full_name'   => $customerName,
            'cus_ctt_email'       => object_get($customer, 'customerContact.cus_ctt_email', null),
            'cus_ctt_phone'       => object_get($customer, 'customerContact.cus_ctt_phone', null),
            // Customer Address Info
            'addresses'           => [
                'cus_add_id'          => object_get($customer, 'customerAddress.cus_add_id', null),
                'cus_add_city_name'   => object_get($customer, 'customerAddress.cus_add_city_name', null),
                'cus_add_state_name'  => object_get($customer, 'customerAddress.systemState.sys_state_name', null),
                'cus_add_postal_code' => object_get($customer, 'customerAddress.cus_add_postal_code', null),
            ]
        ];
    }
}