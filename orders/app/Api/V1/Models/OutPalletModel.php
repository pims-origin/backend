<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 1-Sep-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OutPallet;


/**
 * Class OutPalletModel
 *
 * @package App\Api\V1\Models
 */
class OutPalletModel extends AbstractModel
{
    /**
     * @param OutPallet $model
     */
    public function __construct(OutPallet $model = null)
    {
        $this->model = ($model) ?: new OutPallet();
    }

    /**
     * Generate new wo number
     *
     * @return string
     */
    public function generateLPNNum()
    {
        $currentYearMonth = date('ym');
        $defaultWoNum = "LPN-${currentYearMonth}-00001";

        $lastPL = $this->model->orderBy('plt_id', 'desc')->first();
        $lastLPN = object_get($lastPL, 'plt_num', '');

        if (empty($lastLPN) || strpos($lastLPN, "-${currentYearMonth}-") === false) {
            return $defaultWoNum;
        }

        return ++$lastLPN;
    }

    public function getOutPalletByOdrHdrId($odrHdrId, $limit = null)
    {
        $query = $this->model
            ->select([
                'pack_hdr.odr_hdr_id',
                'out_pallet.plt_id',
                'out_pallet.loc_id',
                'out_pallet.loc_code',
                'out_pallet.loc_name',
                'out_pallet.cus_id',
                'out_pallet.whs_id',
                'out_pallet.plt_num',
                'out_pallet.plt_block',
                'out_pallet.plt_tier',
                'out_pallet.ctn_ttl',
                'out_pallet.is_movement',
                'out_pallet.out_plt_sts',
                'out_pallet.created_at'
            ])
            ->join('pack_hdr', 'pack_hdr.out_plt_id', '=', 'out_pallet.plt_id')
            ->where('pack_hdr.odr_hdr_id', $odrHdrId)
            ->groupBy('out_pallet.plt_num')
            ->where('out_pallet.deleted', 0)
            ->where('out_pallet.out_plt_sts', 'AC');

        if ($limit) {
            $models = $query->paginate($limit);    
        } else {
            $models = $query->get();    
        }
        
        return $models;
    }

    public function editOutPallet($location, $pltId)
    {
        $result = $this->model
            ->where('plt_id', $pltId)
            ->where('whs_id', $location->loc_whs_id)
            ->update([
                'loc_id' => $location->loc_id,
                'loc_code' => $location->loc_code,
                'loc_name' => $location->loc_alternative_name
            ]);

        return $result;
    }
    /**
     * @param $whsId
     * @param $odrId
     * @param $attributes
     * @param $limit
     *
     * @return Paginator
     */
    public function getOutPalletByOrder($whsId, $odrId, $attributes = [], $limit = 20) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $query = $this->model
            ->select([
                'out_pallet.plt_id',
                'out_pallet.plt_num',
                'out_pallet.loc_code',
                'out_pallet.loc_name',
                'out_pallet.ctn_ttl',
                'odr_hdr.odr_sts',
                'out_pallet.volume',
                'out_pallet.length',
                'out_pallet.width',
                'out_pallet.height',
                'out_pallet.weight',
                DB::raw('COUNT(DISTINCT pack_dtl.item_id) as sku_ttl'),
            ])
            ->join('pack_hdr', 'out_pallet.plt_id', '=', 'pack_hdr.out_plt_id')
            ->join('pack_dtl', 'pack_hdr.pack_hdr_id', '=', 'pack_dtl.pack_hdr_id')
            ->join('odr_hdr' , 'pack_hdr.odr_hdr_id', '=', 'odr_hdr.odr_id')
            ->where('out_pallet.whs_id', $whsId)
            ->where('odr_hdr.odr_id', $odrId)
            ->where('pack_hdr.deleted', 0)
            ->groupBy('out_pallet.plt_id');
        $this->sortBuilder($query, $attributes);
        return $query->paginate($limit);
    }
}
