<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'first_name' => $faker->name,
        'last_name'  => $faker->name,
        'email'      => $faker->email,
    ];
});

$factory->define(App\MenuGroup::class, function ($faker) {
    return [
        'name'        => $faker->name,
        'description' => $faker->text,
    ];
});

$factory->define(App\Menu::class, function ($faker) {
    return [
        'name'            => $faker->name,
        'description'     => $faker->text,
        'url'             => '#',
        'target'          => '',
        'permission_name' => function () {
            return factory(App\Authority::class)->create()->name;
        },
        'menu_group_id'   => 1,
        'parent_id'       => 0,
        'display_order'   => 1,
    ];
});

$factory->define(\App\Authority::class, function ($faker) {
    return [
        'name'        => "Menu_". $faker->numberBetween(1000, 9000),
        'code'        => strtoupper($faker->randomLetter . $faker->randomLetter) . $faker->numberBetween(1000, 9000),
        'type'        => strtoupper($faker->randomLetter . $faker->randomLetter) . $faker->numberBetween(1000, 9000),
        'description' => $faker->text,
        'rule_name'   => null,
        'data'        => ''
    ];
});
