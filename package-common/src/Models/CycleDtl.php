<?php

namespace Seldat\Wms2\Models;

class CycleDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cycle_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cycle_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cycle_hdr_id',
        'whs_id',
        'cus_id',
        'item_id',
        'sku',
        'size',
        'color',
        'pack',
        'lot',
        'remain',
        'sys_qty',
        'act_qty',
        'sys_loc_id',
        'sys_loc_name',
        'act_loc_id',
        'act_loc_name',
        'cycle_dtl_sts',
        'is_new_sku',
        'sts',
        'plt_rfid'
    ];

    /**
     * @return cycleHdr
     */
    public function cycleHdr()
    {
        return $this->belongsTo(CycleHdr::class, 'cycle_hdr_id', 'cycle_hdr_id');
    }

    /**
     * @return warehouse
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'whs_id', 'whs_id');
    }

    /**
     * @return customer
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'cus_id', 'cus_id');
    }

    /**
     * @return item
     */
    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id', 'item_id');
    }

    /**
     * @return system Location
     */
    public function systemLocation()
    {
        return $this->belongsTo(Location::class, 'sys_loc_id', 'loc_id');
    }

    /**
     * @return Created user
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }
}
