<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/11/2016
 * Time: 09:43
 */

namespace App\Api\Inventory\Validators;

class BlockDtlValidator extends AbstractValidator
{
    /*
     * Define rules
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'block_hdr_id'   => 'required|exists:block_hdr,block_hdr_id',
            'whs_id'       => 'required|exists:warehouse,whs_id',
            'cus_id'       => 'required|exists:customer,cus_id',
            'item_id'       => 'required|exists:item,item_id',
            'sku'            => 'required|max:50',
            'size'       => 'required|max:30',
            'color'       => 'required|max:30',
            'loc_id'       => 'required|exists:location,loc_id',
            'loc_name'       => 'required',
            'block_dtl_sts'       => 'required',
            'sts'       => 'required'
        ];
    }
}