<?php
namespace console\rbac;

use  yii\rbac\Rule;

class OwnerRule extends Rule
{
    public $name = 'isOwner';

    public function execute($userId, $item, $params)
    {
        return isset($params['obj']) ? $params['obj']['created_by'] == $userId : false;
    }
}
