<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseMeta;

class EmailConfigurationTransformer extends TransformerAbstract
{

    /**
     * ps:Show SAC
     *
     * @param 
     *
     * @return array
     */
    public function transform(WarehouseMeta $smtpInfo)
    {
        if ($smtpInfo->whs_meta_value) {
            $value = json_decode($smtpInfo->whs_meta_value);
            return [
                "smtp_name"        => $value->smtp_name,
                "smtp_host"        => $value->smtp_host,
                "smtp_port"        => $value->smtp_port,
                "user_name"        => $value->user_name,
                "password"         => $value->password,
                "encryption"       => $value->encryption,
                "authentication"   => $value->authentication,
                "use_system_email" => $value->use_system_email,
            ];
        }
    }
}
