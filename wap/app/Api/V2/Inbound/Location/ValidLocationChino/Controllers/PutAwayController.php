<?php

namespace App\Api\V2\Inbound\Location\ValidLocationChino\Controllers;

use App\Api\V2\Inbound\Location\ValidLocationChino\Models\CartonModel;
use App\Api\V2\Inbound\Location\ValidLocationChino\Models\LocationModel;
use App\Api\V2\Inbound\Location\ValidLocationChino\Models\ItemModel;
use App\Api\V2\Inbound\Location\ValidLocationChino\Models\Log;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\MyHelper;

class PutAwayController extends AbstractController
{

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * PutAwayController constructor.
     *
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param ItemModel $itemModel
     */
    public function __construct(
        LocationModel $locationModel
    ) {
        $this->cartonModel   = new CartonModel();
        $this->locationModel = $locationModel;
        $this->itemModel     = new ItemModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function validateLocation($whsId, Request $request)
    {
        // WAP-736 - [Inbound - Relocate] Create API to check & get location code

        $url = "/v2/whs/{$whsId}/location/rack/valid-location-chino";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'PAR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Validate Location Chino'
        ]);

        $input = $request->getParsedBody();
        $errors = $this->_validateParams($input, $whsId);
        if ($errors) {
            return $errors;
        }
        $location = array_get($input, 'location');
        $type     = (int)array_get($input, 'type');
        $cusId    = (int)array_get($input, 'cus_id');

        //Check existed for The loc RFID
        $locationInfo = '';
        if ($type == 1) {
            $locationInfo = $this->locationModel->getRackLocationByLocRFID($whsId, $input['location']);
        } else {
            $locationInfo = $this->locationModel->getFirstWhere([
                    'loc_whs_id' => $whsId,
                    'loc_code'   => $location
                ]);
        }
        if (empty($locationInfo) || !count($locationInfo)) {
            $msg = "Location does not exist.";
            if ($type == 1) {
                $locationInfo = $this->locationModel->getFirstWhere([
                        'rfid'   => $location
                    ]);
            } else {
                $locationInfo = $this->locationModel->getFirstWhere([
                        'loc_code'   => $location
                    ]);
            }

            if (count($locationInfo)) {
                $msg = "Location does not belong to current warehouse.";
            }

            return $this->_responseErrorMessage($msg);
        }

        $locationObj = $this->locationModel->getRackLocationByCustomerById($whsId, $locationInfo->loc_id, $cusId);

        if (empty($locationObj)) {
            $msg = "Location does not belong to this customer.";
            return $this->_responseErrorMessage($msg);
        } elseif ($locationObj->loc_sts_code != 'AC') {
            $msg = sprintf("Location %s is not active. Current Status %s",
                $locationObj->loc_code,
                $locationObj->loc_sts_code);
            return $this->_responseErrorMessage($msg);
        }

        // if ($locationObj && !$locationObj->rfid) {
        //     $msg = sprintf("Location %s does not be Rfid",
        //         $locationObj->loc_code);
        //     return $this->_responseErrorMessage($msg);
        // }

        //WAP-742 - [Migration data] Unable relocate when location is mixed SKU
        // $checkMixSku = $this->_checkMixSkuOnLoc($whsId, $itemId, $locationObj->loc_id);

        // if ($checkMixSku) {
        //     $msg = sprintf("Unable relocate on location contains cartons are mixed SKU.");
        //     return $this->_responseErrorMessage($msg);
        // }

        //WAP-749 - [Migration data] Unable relocate when location is empty
        // $checkEmptyLoc = $this->_checkEmptyLoc($whsId, $locationObj->loc_id);

        // if ($checkEmptyLoc) {
        //     $msg = sprintf("Unable relocate on location does not contain any cartons.");
        //     return $this->_responseErrorMessage($msg);
        // }

        try {
            $msg = sprintf("Successfully!");

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => $msg
                    ]
                ],
                'data' => [[
                    'loc_id'   => $locationObj->loc_id,
                    'loc_code' => $locationObj->loc_code,
                    'rfid'     => $locationObj->rfid,
                ]]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->_responseErrorMessage($e->getMessage());
        }
    }

    private function _validateParams($attributes, $whsId)
    {
        $location = array_get($attributes, 'location');
        $type     = (int)array_get($attributes, 'type');
        // $ctnTtl   = array_get($attributes, 'carton_total');
        $cusId    = (int)array_get($attributes, 'cus_id');

        $names = [
            'location'     => 'Location',
            'type'         => 'Type',
            // 'carton_total' => 'Carton total',
            'cusId'        => 'Customer',
        ];

        // Check Required
        $requireds = [
            'location'     => $location,
            'type'         => $type,
            // 'carton_total' => $ctnTtl,
            'cus_id'       => $cusId,
        ];

        // Check int type
        $integers = [
            'type'         => $type,
//            'carton_total' => $ctnTtl,
           'cus_id'        => $cusId,
        ];

        foreach ($requireds as $key => $field) {
            if (!isset($field) || $field === "" || $field === null) {
                $errorDetail = "{$names[$key]} is required.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        foreach ($integers as $key => $field) {
            if (!is_int($field) || $field < 1 || $field > 999999999) {
                $errorDetail = "{$names[$key]} must be integer and greater than 0.";
                return $this->_responseErrorMessage($errorDetail);
            }
        }

        if (!in_array($type, [1, 2])) {
            $msg = "Type is required. Just including 1(RFID) / 2(Barcode)";
            return $this->_responseErrorMessage($msg);
        }

        //validate pallet RFID
        // $pltRFIDValid = new RFIDValidate($pltRfid, RFIDValidate::TYPE_PALLET, $whsId);
        // if (!$pltRFIDValid->validate()) {
        //     return $this->_responseErrorMessage($pltRFIDValid->error);
        // }

        //validate location RFID
        if ($type == 1) {
            $locRFIDValid = new RFIDValidate($location, RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                return $this->_responseErrorMessage($locRFIDValid->error);
            }
        }

        $customerArr = DB::table('customer')
            ->where('cus_id', $cusId)
            ->where('deleted', 0)
            ->first();

        $customerWarehouseArr = DB::table('customer_warehouse')
            ->where('cus_id', $cusId)
            ->where('whs_id', $whsId)
            ->where('deleted', 0)
            ->first();
        if (!count($customerArr)) {
            $msg = sprintf("The customer doesn't exist!");
            return $this->_responseErrorMessage($msg);
        }

        if (!count($customerWarehouseArr)) {
            $msg = sprintf("The customer doesn't belong to current warehouse!");
            return $this->_responseErrorMessage($msg);
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _checkMixSkuOnLoc($whsId, $itemId, $locId)
    {
        $count = DB::table('cartons')
            ->where('whs_id', $whsId)
            ->where('item_id', '!=', $itemId)
            ->where('loc_id', $locId)
            // ->whereIn('ctn_sts', ['AC', 'LK'])
            ->where('deleted', 0)
            ->count();

        return $count > 0 ? true : false;
    }

    private function _checkEmptyLoc($whsId, $locId)
    {
        $count = DB::table('cartons')
            ->where('whs_id', $whsId)
            ->where('loc_id', $locId)
            // ->whereIn('ctn_sts', ['AC', 'LK'])
            ->where('deleted', 0)
            ->count();

        return $count == 0 ? true : false;
    }
}
