<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use App\BillableDtl;

class InvoiceDetail extends BaseInvoiceModel
{
    protected $table = 'inv_dtl';
    protected $primaryKey = 'inv_dtl_id';
    protected $fillable = [
        'ref_num',
        'description',
        'unit_price',
        'qty',
        'amount',
        'inv_sts',
        'sac',
        'uom',
        'ba_id',
        'ba_dtl_id',
        'currency',
        'discount',
        'is_charge',
        'item'
    ];

    public function header()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceHeader', 'inv_id');
    }

    public function billableDetail()
    {
        return $this->belongsTo(BillableDtl::class, 'ba_dtl_id')->withTrashed();
    }

    public function getSacNameAttribute()
    {
        return $this->billableDetail->sacModel->sac_name ?? null;
    }

    public function getInvStsNameAttribute()
    {
        return $this->getInvoiceStatusName($this->inv_sts);
    }

    public function getUomNameAttribute()
    {
        $uomName = config('constants.unit_uom_name');

        if(array_key_exists(strtoupper($this->uom), $uomName)){
            return $uomName[strtoupper($this->uom)];
        }

        return null;
    }

    private function getInvoiceStatusName($invoiceStatusCode)
    {
        $invoiceStatusName = config('constants.INV_STATUS_NAME');
        if (array_key_exists(strtoupper($invoiceStatusCode), $invoiceStatusName)) {
            return $invoiceStatusName[strtoupper($invoiceStatusCode)];
        }

        return null;
    }

    public function setUomAttribute($value)
    {
        return $this->attributes['uom'] = strtoupper($value);
    }

    public function setAmountAttribute($value)
    {
        if(empty($value)){
            return $this->attributes['amount'] = 0;
        }

        return $this->attributes['amount'] = $value;
    }

    public function getInvNumAttribute()
    {
        return $this->header->inv_num ?? '';
    }

    public function getInvDtFormatAttribute()
    {
        return $this->header->inv_dt_format ?? '';
    }
}
