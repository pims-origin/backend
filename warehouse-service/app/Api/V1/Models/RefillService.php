<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Support\Facades\DB;

class RefillService extends BaseService
{
    /**
     * @param $refillId
     *
     * @return \Illuminate\Http\Response
     */
    public function show($refillId)
    {
        $uri = env('API_WAREHOUSE') . 'refill/' . $refillId;
        $response = $this->client->get($uri);

        return $response;
    }

    /**
     * @param $locationId
     *
     * @return Response|void
     */
    public function showRefillAccordingToLocation($locationId)
    {
        $uri = env('API_WAREHOUSE') . 'refill/location/' . $locationId;
        $response = $this->client->get($uri);

        return $response;
    }

    /**
     * @param $refillId
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($refillId)
    {
        $uri = env('API_WAREHOUSE') . 'refill/' . $refillId;
        $response = $this->client->get($uri);

        return $response;

    }

    /**
     * @param $params
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function store($params)
    {
        $uri = env('API_WAREHOUSE') . 'refill';

        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    /**
     * @param $params
     * @param $refillId
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update($params, $refillId)
    {
        $uri = env('API_WAREHOUSE') . 'refill/' . $refillId;

        $options = [
            'form_params' => $params
        ];

        return $this->client->put($uri, $options);
    }


}
