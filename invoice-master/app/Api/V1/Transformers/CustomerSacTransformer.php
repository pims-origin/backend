<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\CustomerMetaModel;
use App\Api\V1\Models\CustomerSac;
use League\Fractal\TransformerAbstract;

class CustomerSacTransformer extends TransformerAbstract
{

    /**
     * ps:Show SAC
     *
     * @param CustomerSac $cusSac
     *
     * @return array
     */
    public function transform(CustomerSac $cusSac)
    {
        return [
            "cus_sac_id"    => $cusSac->cus_sac_id,
            "sac_id"        => $cusSac->sac_id,
            "sac"           => $cusSac->sac,
            "sac_name"      => $cusSac->sac_name,
            "description"   => $cusSac->description,
            "type"          => $cusSac->type,
            "type_name"     => $cusSac->type_name,
            "range_from"    => $cusSac->range_from,
            "range_to"      => $cusSac->range_to,
            "range_uom"     => $cusSac->range_uom,
            "range_uom_name"=> $cusSac->range_uom_name,
            "period_uom"    => $cusSac->period_uom,
            "period_uom_name"=> $cusSac->period_uom_name,
            "block"         => $cusSac->block,
            "price"         => $cusSac->price,
            "discount"      => $cusSac->discount,
            "unit_uom"      => $cusSac->unit_uom,
            "unit_uom_name" => $cusSac->unit_uom_name,
            "sac_period"    => $cusSac->sac_period,
            "base_price"    => $cusSac->base_price,
            "currency"      => (new CustomerMetaModel())->getCurrencyByCurrentWhs($cusSac->cus_id)
        ];
    }
}
