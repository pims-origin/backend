<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Edi;
use Seldat\Wms2\Models\CustomerConfig;
use Seldat\Wms2\Models\Customer;
use League\Fractal\TransformerAbstract;
use App\Api\V1\Models\CustomerConfigModel;

class CustomerConfigTransformer extends TransformerAbstract
{
    public function transform(Customer $customer)
    {
        $this->customerConfigModel = new CustomerConfigModel();
        $ediTrans = array_pluck(
            Edi::select('edi_trans')
                ->where('edi_sts', 'AC')
                ->get(), 'edi_trans', 'edi_trans');

        $CusConfigInfo = $this->customerConfigModel->getCusConfigByIds(
            object_get($customer, 'customerConfig.whs_id', ''),
            $customer->cus_id,
            $ediTrans
        )->toArray();

        $ac2 = array_pluck($CusConfigInfo, 'ac', 'ac');

        $ediSts = 'No';
        if ($CusConfigInfo && isset($ac2["Y"])) {
            $ediSts = 'Yes';
        }

        return [
            'cus_id'   => $customer->cus_id,
            'cus_name' => trim(object_get($customer, "cus_name", null)),
            'edi'      => $ediSts,
        ];
    }
}
