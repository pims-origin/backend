<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Inbound\Pallet\ScanPallet\Models;

use App\MessageCode;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Symfony\Component\VarDumper\Cloner\Data;

class VirtualCartonModel extends AbstractModel
{
    /**
     * VirtualCartonModel constructor.
     *
     * @param VirtualCarton|null $model
     */
    public function __construct(VirtualCarton $model = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $this->model = ($model) ?: new VirtualCarton();
    }

    /**
     * @param array $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function listAllVirtualCarton($attributes = [], $with = [])
    {
        $query = $this->make($with);

        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }

        $query->where('whs_id', array_get($attributes, 'whsId', null));
        $query->where('asn_hdr_id', array_get($attributes, 'asn_hdr_id', null));
        $query->where('ctnr_id', array_get($attributes, 'ctnr_id', null));
        $query->where('cus_id', array_get($attributes, 'cusId', null));

        // Get
        $models = $query->get();

        return $models;
    }

    /**
     * @param array $attributes
     *
     * @return mixed
     */
    public function listAllRfidVtlCarton($attributes = [])
    {
        $query = $this->model->where('whs_id', array_get($attributes, 'whsId', null))
            ->where('asn_hdr_id', array_get($attributes, 'asn_hdr_id', null))
            ->where('ctnr_id', array_get($attributes, 'ctnr_id', null))
            ->where('cus_id', array_get($attributes, 'cusId', null));

        // Get
        $models = $query->select('ctn_rfid')->get();

        return $models;
    }

    public function countCtnByASNDtlIdNew($asnDtlId)
    {
        return $this->model->where('asn_dtl_id', $asnDtlId)
            ->where('vtl_ctn_sts','<>', Status::getByValue('ADJUSTED', 'VIRTUAL-CARTON-STATUS'))->count();
    }

    public function countCtnByASNDtlId($asnDtlId)
    {
        return $this->model->where('asn_dtl_id', $asnDtlId)->count();
    }

    public function getAllCtnByASNDtlId($asnDtlId)
    {
        return $this->model->where('asn_dtl_id', $asnDtlId)->get();
    }

    public function getFirstVtlCtnByPltRfid($pltRfid)
    {
        return $this->model->where('plt_rfid', $pltRfid)->first();
    }

    public function countPltByASNDtlId($asnDtlId)
    {
        $count = $this->model
            ->where('asn_dtl_id', $asnDtlId)
            ->select(
                DB::raw('count(DISTINCT(plt_rfid)) as ttl')
            )->groupBy('plt_rfid')
            ->value('ttl');

        return $count;
    }

    public function countCtnIsDamageByASNDtlId($asnDtlId)
    {
        return $this->model
            ->where('asn_dtl_id', $asnDtlId)
            ->where('is_damaged', 1)
            ->where('deleted', 0)
            ->count();
    }

    /**
     * @param $arrVtlCtn
     * @param $pltRfid
     *
     * @return mixed
     */
    public function updateVtlCtnPltRfidByVtlCtnRfid($arrVtlCtn, $pltRfid)
    {
        return $this->model
        ->whereIn('ctn_rfid', $arrVtlCtn)
        ->update(
            [
                'plt_rfid'    => $pltRfid,
                'vtl_ctn_sts' => Status::getByValue('ASSIGNED', 'VIRTUAL-CARTON-STATUS'),
                'updated_by'  => $this->getUserId(),
            ]
        );
    }

    public function isVtlCtnAssignPlt($arrVtlCtn)
    {
        return $this->model->whereIn('ctn_rfid', $arrVtlCtn)->whereNotNull('plt_rfid')->count();
    }


    public function isScanPalletAgain($pltRfid)
    {
        return (bool)$this->model->where('plt_rfid', $pltRfid)
            ->where('vtl_ctn_sts', 'AS')->count();
    }


    public function checkPalletExisted($pltRfid)
    {
        return (bool)$this->model->where('plt_rfid', $pltRfid)->count();
    }

    public function getVtlCtnStatus($vtlCtnRfid)
    {
        return $this->model->select('vtl_ctn_sts')->where('ctn_rfid', $vtlCtnRfid)->first();
    }

    public function getAllVtlToDelete($whsId, $vtlCtnRfids)
    {
        return $this->model->whereIn('ctn_rfid', $vtlCtnRfids)->where('whs_id', $whsId)->get();
    }

    public function getAllVtlToDeleteByASNDtlId($whsId, $cusId, $asnDtlId)
    {
        return $this->model->where(
            [
                'whs_id' => $whsId,
                'cus_id' => $cusId,
                'asn_dtl_id' => $asnDtlId,
                'deleted' => 0,
            ])
            ->get();
    }
    /**
     * @param $ansHdrId
     */
    public function countVtlCtnNotHavePltByAsnHdrId($ansHdrId, $ctnrId)
    {
        return $this->model->whereNull('plt_rfid')
            ->where('asn_hdr_id', $ansHdrId)
            ->where('ctnr_id', $ctnrId)->count();
    }

    public function getAllPalletRfidOnVtlCtnByAsnHdr($ansHdrId, $with = [])
    {
        $query = $this->make($with);
        $pltRfid = $query->where('asn_hdr_id', $ansHdrId)->get();

        return $pltRfid;
    }

    public function loadViewLayoutNew($whsId, $locIds)
    {
        $query = $this->model
            ->select([
                'vtl_ctn.loc_id',
                'asn_dtl.asn_dtl_sku as sku',
                'asn_dtl.asn_dtl_size as size',
                'asn_dtl.asn_dtl_color as color',
                'asn_dtl.asn_dtl_lot as lot',
                DB::raw('sum(asn_dtl.asn_dtl_pack) as piece_ttl'),
                DB::raw('count(1) as ctn_ttl'),
                DB::raw('count(distinct asn_dtl.asn_dtl_id) as sku_ttl')
            ])
            ->join('asn_dtl', 'asn_dtl.asn_dtl_id','=', 'vtl_ctn.asn_dtl_id')
            ->where('vtl_ctn.whs_id', $whsId)
            ->whereIn('vtl_ctn.loc_id', $locIds)
            ->whereNotNull('vtl_ctn.loc_id')
            ->groupBy('vtl_ctn.loc_id')
            ->get();

        return $query;
    }

    public function getCartonInRfId($arr_rfids, $whsId =null) {
         $query = $this->model
            ->whereIn('ctn_rfid', $arr_rfids)
            ->where('deleted', 0);
        if(!empty($whsId)) {
            $query->where('whs_id', $whsId);
        }
        return
            $query->groupBy(['asn_dtl_id', 'ctn_rfid'])
            ->get()
            ->toArray();
    }

    public function getCartonInRfIdNew($arr_rfids, $whsId =null, $asn_dtl_id) {
        $query = $this->model
            ->whereIn('ctn_rfid', $arr_rfids)
            ->where('deleted', 0)->where('asn_dtl_id', $asn_dtl_id);
        if(!empty($whsId)) {

            $query->where('whs_id', $whsId);
        }
        return
            $query->groupBy(['asn_dtl_id', 'ctn_rfid'])
                ->get()
                ->toArray();
    }


    public function countCtnrBuCartonInRfId($arr_rfids) {
        return $this->model
            ->whereIn('ctn_rfid', $arr_rfids)
            ->where('deleted', 0)
            ->groupBy('ctn_rfid')
            ->count();
    }

    public function getASNByVtlCtn($pltRFID) {
        return  $this->model
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'vtl_ctn.asn_hdr_id')
            ->join('container', 'container.ctnr_id', '=', 'vtl_ctn.ctnr_id')
            ->where([
                'plt_rfid' => $pltRFID,
            ])
            ->where('vtl_ctn_sts', '!=', 'AD')
            ->first();
    }

    public function getAsnByPltRfid($pltRFID) {
        return  $this->model
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'vtl_ctn.asn_hdr_id')
            ->where([
                'plt_rfid' => $pltRFID,
            ])
            ->first();
    }

    public function deleteVtlCtns($whsId, $ctnRFIDs) {
        return $this->model
            ->whereIn('ctn_rfid', $ctnRFIDs)
            ->where('whs_id', $whsId)
            ->update([
                'deleted'     => '1',
                'deleted_at'  => time(),
                'vtl_ctn_sts' => Status::getByValue('ADJUSTED', 'VIRTUAL-CARTON-STATUS')
            ]);
    }

    public function deleteUnscannedVtlCtns($whsId, $ctnRFIDs) {
        return $this->model
            ->whereIn('ctn_rfid', $ctnRFIDs)
            ->where('whs_id', $whsId)
            ->update([
                'deleted'     => '1',
                'deleted_at'  => time(),
                // 'ctn_rfid' => '',
                'vtl_ctn_sts' => Status::getByValue('ADJUSTED', 'VIRTUAL-CARTON-STATUS')
            ]);
    }

    public function deleteVtlCtnsByASNDtlId($whsId, $cusId, $asnDtlId){
        return $this->model
            ->where([
                'whs_id' => $whsId,
                'cus_id' => $cusId,
                'asn_dtl_id' => $asnDtlId,
            ])
            ->update([
                'deleted'     => '1',
                'deleted_at'  => time(),
                'vtl_ctn_sts' => Status::getByValue('ADJUSTED', 'VIRTUAL-CARTON-STATUS')
            ]);
    }

    public function getCustomerByRFId($pltRFID, $whsId) {
        return $this->model
            ->select('cus_id')
            ->where('plt_rfid', $pltRFID)
            ->where('whs_id', $whsId)
            ->first();
    }

    public function updatePalletRFIDNullMixedSku($pltRFID, $ansHdrIds, $ctnrIds)
    {
        $ansHdrIds  = is_array($ansHdrIds) ? $ansHdrIds : [$ansHdrIds];
        $ctnrIds  = is_array($ctnrIds) ? $ctnrIds : [$ctnrIds];
        $result = $this->model
           // ->whereIn('asn_hdr_id', $ansHdrIds)
         //   ->whereIn('ctnr_id', $ctnrIds)
            ->where('plt_rfid', $pltRFID)
            ->update([
                'plt_rfid' => null,
                'vtl_ctn_sts' => 'NW'
            ]);

        return $result;

    }

    public function updatePalletRFIDNull($pltRFID, $ansHdrId, $ctnrId)
    {
        $result = $this->model
            ->where(
                [
                    'asn_hdr_id' => $ansHdrId,
                    'ctnr_id' => $ctnrId,
                    'plt_rfid' => $pltRFID,
                ]
            )
            ->update([
                    'plt_rfid' => null,
                    'vtl_ctn_sts' => 'NW'
            ]);

        return $result;

    }

    public function updatePalletRFIDNullByCtnRfid(/*$pltRFID, $ansHdrId, $ctnrId, */$scanRfids)
    {
        $result = $this->model
            /*->where(
                [
                    'asn_hdr_id' => $ansHdrId,
                    'ctnr_id' => $ctnrId,
                    'plt_rfid' => $pltRFID,
                ]
            )*/
            ->whereIn('ctn_rfid', $scanRfids)
            ->update([
                'plt_rfid' => null,
                'vtl_ctn_sts' => 'NW'
            ]);

        return $result;

    }

    public function getCartonsByLocIds($whsId, $locIds){
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->join('asn_dtl', 'asn_dtl.asn_dtl_id', '=', 'vtl_ctn.asn_dtl_id')
                ->select([
                    'asn_dtl.asn_dtl_sku as sku',
                    'asn_dtl.asn_dtl_size as size',
                    'asn_dtl.asn_dtl_color as color',
                    'asn_dtl.asn_dtl_lot as lot',
                    'vtl_ctn.loc_id',
                    DB::raw('sum(asn_dtl.asn_dtl_pack) as piece_ttl'),
                    DB::raw('count(vtl_ctn.asn_dtl_id) as ctn_ttl'),
                    DB::raw('count(distinct asn_dtl.item_id,asn_dtl.asn_dtl_lot) as sku_ttl')
                ])
            ->whereIn('vtl_ctn.loc_id', $locIds)
            ->where('vtl_ctn.whs_id', $whsId)
            ->groupBy('vtl_ctn.loc_id');
        $res = $query->get();

        return $res;
    }

    public function getCartonsInfoByPalletRFID($whsId, $pltRFID){
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->join('asn_dtl', 'asn_dtl.asn_dtl_id', '=', 'vtl_ctn.asn_dtl_id')
            ->select([
                'asn_dtl.asn_dtl_sku as sku',
                'asn_dtl.asn_dtl_size as size',
                'asn_dtl.asn_dtl_color as color',
                'asn_dtl.asn_dtl_lot as lot',
                'vtl_ctn.loc_id',
                DB::raw('sum(asn_dtl.asn_dtl_pack) as piece_ttl'),
                DB::raw('count(vtl_ctn.asn_dtl_id) as ctn_ttl'),
                DB::raw('count(distinct asn_dtl.item_id,asn_dtl.asn_dtl_lot) as sku_ttl')
            ])
            ->where('vtl_ctn.whs_id', $whsId)
            ->where('vtl_ctn.plt_rfid', $pltRFID)
            ->groupBy('plt_rfid');

        $res = $query->get();

        return $res;
    }

    public function checkPalletExist($whsId, $pltRFID)
    {
        $model = $this->model
            ->where('whs_id', $whsId)
            ->where('plt_rfid', $pltRFID)
            ->where('deleted', 0)
            ->count();

        return (bool) $model;
    }

    /**
     * 5. Check Virtual/Real Cartons if existed and not in current pallet, delete  virtual and real cartons
     *
     * @param type $pltRfid
     * @param type $ctnsRfid
     *
     * @return type
     */
    public function deleteExistNotInPallet($pltRfid, $ctnsRfid)
    {
        $result = $this->getModel()
                ->whereIn('ctn_rfid', $ctnsRfid)
                ->whereNotIn('plt_rfid', [$pltRfid])
                ->delete();

        return $result;
    }

    /**
     * 5. Check Virtual/Real Cartons if existed and not in current pallet, delete  virtual and real cartons
     *
     * @param type $pltRfid
     * @param type $ctnsRfid
     *
     * @return type
     */
    public function deleteVtlCtnNotInExistPalletAndCtnsRfid($whs_id, $asn_hdr_id, $pltRfid, $ctnsRfid)
    {
        $pltRfid = is_array($pltRfid) ? $pltRfid : [$pltRfid];
        $asn_hdr_id = is_array($asn_hdr_id) ? $asn_hdr_id : [$asn_hdr_id];
        $result = $this->getModel()
            ->whereNotIn('ctn_rfid', $ctnsRfid)
            ->whereIn('plt_rfid', $pltRfid)
            ->where('whs_id', $whs_id)
            ->whereIn('asn_hdr_id', $asn_hdr_id)
            ->delete();

        return $result;
    }

    /**
     * @param $ansHdrId
     */
    public function countVtlCtnHavePltByAsnDtlId($ansDtlId, $ctnrId, $pltRfid)
    {
        return $this->model->where('plt_rfid', $pltRfid)
            ->where('asn_dtl_id', $ansDtlId)
            ->where('ctnr_id', $ctnrId)->count();
    }

    public function getAsnDtlIdByRfid($whsId, $cusId, $pltRfid, $ctnrId) {
        $asnDtlIdsObj = $this->model
                ->select(['asn_dtl_id'])
                ->where([
                    'plt_rfid' => $pltRfid,
                    'ctnr_id' => $ctnrId,
                    'deleted' => 0,
                    'whs_id' => $whsId,
                    'cus_id' => $cusId,
                ])
                ->groupBy('asn_dtl_id')
                ->get();
        $asnDtlIds = [];

        foreach($asnDtlIdsObj as $asnDtl) {
            $asnDtlIds[] = object_get($asnDtl, 'asn_dtl_id');
        }

        return $asnDtlIds;
    }

    public function updateVtlCartonStatusIsGC($arrCtnRfid)
    {
        $query = $this->model
            ->whereIn('ctn_rfid', $arrCtnRfid)
            ->update(
                [
                    'vtl_ctn_sts' => Status::getByValue('GOODS-RECEIPT-COMPLETE','VIRTUAL-CARTON-STATUS'),
                    'updated_by'  => $this->getUserId(),
                ]
            );

        return $query;
    }

    public function createVtlCartonIsNotScan($params)
    {
        list ($asnDtl, $wrongRFIDS, $cusId, $whsId) = array_values($params);

        $asnHdrId = array_get($asnDtl, 'asn_hdr_id', 0);
        $asnDtlId = array_get($asnDtl, 'asn_dtl_id', 0);
        $itemId = array_get($asnDtl, 'item_id', 0);
        $ctnrId = array_get($asnDtl, 'ctnr_id', 0);

        $vtlCtnDataCommon = [
            'asn_hdr_id'  => $asnHdrId,
            'asn_dtl_id'  => $asnDtlId,
            'item_id'     => $itemId,
            'ctnr_id'     => $ctnrId,
            'cus_id'      => $cusId,
            'whs_id'      => $whsId,
            'scanned'      => '0',
            'vtl_ctn_sts' => Status::getByValue('NEW', 'VIRTUAL-CARTON-STATUS'),
        ];

        $dataVtlCtnDtl = [];

        $userId = $this->getUserId();
        $vtlCtnSum = (new VirtualCartonSumModel())->getFirstWhere(['asn_dtl_id' => $asnDtlId]);
        foreach ($wrongRFIDS as $key => $ctn) {
            $vtlCtnDataCommon['vtl_ctn_sum_id'] = $vtlCtnSum['vtl_ctn_sum_id'];
            $vtlCtnDataCommon['ctn_rfid'] = $ctn;
            $vtlCtnDataCommon['created_at'] = time();
            $vtlCtnDataCommon['updated_at'] = time();
            $vtlCtnDataCommon['created_by'] = $userId;
            $vtlCtnDataCommon['updated_by'] = $userId;
            $vtlCtnDataCommon['deleted_at'] = 915148800;
            $vtlCtnDataCommon['deleted'] = 0;

            //init data array
            $dataVtlCtnDtl[] = $vtlCtnDataCommon;
        }

        try {
            //insert virtual carton
            $insertVtlCtn = DB::table('vtl_ctn')->insert($dataVtlCtnDtl);

            return $insertVtlCtn;
        } catch (\Exception $e) {
            $msg = MessageCode::get('WAP002');
            $data = [
                'data'    => null,
                'message' => $msg,
                'code'    => 'WAP002',
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

    }

    public function updateVtlCartonPalletRFIDSNotInCartonRescan($pltId, $arrCtnRfid)
    {
        $query = $this->model
            ->where('plt_rfid', $pltId)
            ->whereNotIn('ctn_rfid', $arrCtnRfid)
            ->update([
                    'plt_rfid'   => null,
                    'updated_by' => $this->getUserId(),
                ]);

        return $query;
    }

    public function getSkuByVtnRfid($whsId, $randRfid)
    {
        $skus = $this->model
            ->join('item', 'item.item_id', '=', 'vtl_ctn.item_id')
            ->whereIn('vtl_ctn.ctn_rfid', $randRfid)
            ->where('whs_id', $whsId)
            ->select([
                'vtl_ctn.ctn_rfid',
                'item.item_id',
                'item.sku'
            ])
            ->get();
        if($skus){
            $skus = $skus->toArray();
        }

        // Group
        $grouped_item_id = array();
        foreach($skus as $sku){
            $grouped_item_id[$sku['item_id']][] = $sku;
        }
        foreach($grouped_item_id as $key => $value){
            $grouped_item_id[$key] = array_pluck($value, 'ctn_rfid');
        }

        // Count
        $skusCount = $this->model
            ->join('item', 'item.item_id', '=', 'vtl_ctn.item_id')
            ->whereIn('vtl_ctn.ctn_rfid', $randRfid)
            ->where('whs_id', $whsId)
            ->groupBy('item.item_id')
            ->orderBy('ctns_qty', 'desc')
            ->select([
                'item.item_id',
                'item.sku',
                DB::raw('count(item.item_id) as ctns_qty')
            ])
            ->get();
        if($skusCount){
            $skusCount = $skusCount->toArray();
        }

        // Prepare
        foreach($skusCount as $key => &$sku){
            if($key == 0){
                $sku['type'] = 'Main sku';
            }
            else{
                $sku['type'] = 'Sku ' . $key;
            }
            $sku['ctns_lst'] = $grouped_item_id[$sku['item_id']];
        }

        // Unknown
        $nonSkusCtn = array_diff($randRfid, array_pluck($skus, 'ctn_rfid'));
        if($nonSkusCtn){
            $skusCount[] = [
                'item_id' => 0,
                'sku' => 'Unknown',
                'ctns_qty' => count($nonSkusCtn),
                'type' => 'Unknown',
                'ctns_lst' => array_values($nonSkusCtn)
            ];
        }

        return $skusCount;
    }
}
