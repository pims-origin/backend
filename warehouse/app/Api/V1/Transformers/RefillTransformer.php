<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Refill;
use Seldat\Wms2\Models\Reason;

class RefillTransformer extends TransformerAbstract
{
    /**
     * @param Refill $refill
     *
     * @return array
     */
    public function transform(Refill $refill)
    {
        return [
            'rf_id'    => $refill->rf_id,
            'loc_id'   => $refill->loc_id,
            'loc_code' => $refill->loc_code,
            'item_id'  => $refill->item_id,
            'cus_id'   => $refill->cus_id,
            'sku'      => $refill->sku,
            'color'    => $refill->color,
            'size'     => $refill->size,
            'cus_upc'  => $refill->upc,
            'lot'      => $refill->lot,
            'min'      => $refill->min,
            'max'      => $refill->max
        ];
    }
}
