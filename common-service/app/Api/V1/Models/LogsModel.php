<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Logs;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class LogsModel extends AbstractModel
{
    public function __construct(Logs $model = null)
    {
        $this->model = ($model) ?: new Logs();
    }

    /**
     * @param $attributes
     * @param $whs_id
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function search($attributes, $whs_id, array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->where('whs_id', $whs_id);

        if (isset($attributes['type'])) {
            $query->where('logs.type', SelStr::escapeLike($attributes['type']));
        }
        if (isset($attributes['evt_code'])) {
            $query->where('logs.evt_code', SelStr::escapeLike($attributes['evt_code']));
        }
        if (isset($attributes['owner'])) {
            $query->where('owner', 'like', "%" . SelStr::escapeLike($attributes['owner']) . "%");
        }
        
        $query->leftJoin('evt_lookup', 'evt_lookup.evt_code', '=', 'logs.evt_code');
        $query->leftJoin('users', 'users.user_id', '=', 'logs.created_by');
        $query->select(['logs.*', 
            DB::raw('IF(evt_lookup.des is null, logs.evt_code, evt_lookup.des) as des'),
            DB::raw('concat(users.first_name," ", users.last_name) as created_by')]);

        if (isset($attributes['created_at_from']) || isset($attributes['created_at_to'])) {
            if (isset($attributes['created_at_from'])) {
                $attributes['created_at_from'] = $attributes['created_at_from'];
            } else {
                $attributes['created_at_from'] = "01/01/1970";
            }

            if (isset($attributes['created_at_to'])) {
                $attributes['created_at_to'] = $attributes['created_at_to'];
            } else {
                $attributes['created_at_to'] = date("m/d/Y");
            }

            $query->whereBetween(DB::raw("DATE_FORMAT(FROM_UNIXTIME(created_at), '%m/%d/%Y')"),
                [
                    $attributes['created_at_from'],
                    $attributes['created_at_to']
                ]);
        }

        $this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * @param $attributes
     * @param $whs_id
     *
     * @return mixed
     */
    public function typeDropDown($attributes, $whs_id)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query = $this->model
            ->select([
                'type',
            ]);
        $query->where('whs_id', $whs_id);
        $query->orderBy('logs.type', 'desc');
        $query->groupBy('type');

        $row = $query->get();

        return $row;
    }


}
