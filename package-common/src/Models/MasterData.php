<?php
namespace Seldat\Wms2\Models;

/**
 * Class MasterData
 *
 * @property mixed md_code
 * @property mixed md_type
 * @property mixed md_name
 * @property mixed md_des
 * @property mixed md_data
 * @property mixed md_sts
 *
 * @package Seldat\Wms2\Models
 */
class MasterData extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'master_data';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;
    /**
     * @var array
     */
    protected $fillable = [
        'md_code',
        'md_type',
        'md_name',
        'md_des',
        'md_data',
        'md_sts',
    ];

    /**
     * @return Master Data Type
     */
    public function masterDataType()
    {
        return $this->hasOne(__NAMESPACE__ . '\MasterDataType', 'md_type', 'md_type');
    }

    /**
     * @return Created user
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return Updated user
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'user_id');
    }
}
