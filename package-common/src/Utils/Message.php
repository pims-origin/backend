<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 02-Aug-16
 * Time: 16:40
 */

namespace Seldat\Wms2\Utils;

use \Predis\Client;
use Wms2\UserInfo\Data;

class Message
{
    protected $_redis;
    protected $_cacheKey;

    protected static $messages = [
        'BM001' => "This changes may affect other in-used objects. Are you sure you want to udpate the info?",
        'BM002' => "Please add at least 1 {0}!",
        'BM003' => "The {0} cannot be deleted because it is referenced by others! ",
        'BM004' => "Import data has some errors, please download file to check!",
        'BM005' => "There will be {0} {1} imported to system. Are you sure you want to process? ",
        'BM006' => "The {0} is duplicated!",
        'BM007' => "The {1}  for {0} existed!",
        'BM008' => "Goods Receipt has discrepancy or damage. Please submit by RCVD-Pending!",
        'BM009' => "Email not found!",
        'BM010' => "The current server is busy, please wait a few minutes and try again",
        'BM011' => "The provided link is invalid or expired! ",
        'BM012' => "The {0} was updated successfully!",
        'BM013' => "Password was changed successfually! Please login again.",
        'BM014' => "The {0} was added successfully!",
        'BM015' => "The {1} for  {0} is added successfully!",
        'BM016' => "There are {0} location lacked. Please add more for this customer!",
        'BM017' => "The {0} is not existed!",
        'BM018' => "Order {0}  need to be allocated! Do you want to allocate it now?",
        'BM019' => "Order {0}  need to be assign CSR! Do you want to assign  now?",
        'BM020' => "Pallet ID {0} has been relocated from {1} to  {2}",
        "BM021" => "Carton ID {0} has been consoliated from {1} to  {2}",
        "BM022" => "Locations are not active. Do you want to activate them?",
        "BM023" => "{0} Locations were imported successfully! ",
        "BM024" => "The new location does not have any pallet!",
        "BM025" => "The Put Away List for GR {0} was printed successfully!",
        "BM026" => "Number of locations is insufficient to assign to all the pallets. Please review Putway List!",
        "BM027" => "Some locations already have pallets. Are you sure that you want to overwrite current pallets by the new ones.",
        "BM028" => "Pallet doesn't have location!",
        "BM029" => "location {0}  already has pallets. Are you sure that you want to overwrite the current pallet by
        the new one. ",
        "BM030" => "Locations cannot be deactivated while having inventories!",
        "BM031" => "Locations cannot be removed while having inventories!",
        "BM032" => "Only New ASNs can be updated!",
        "BM033" => "Customer does not allow to partially allocate. The avaiable quantity of some items  is not sufficient!",
        "BM034" => "Only New Orders can be allocated!",
        "BM035" => "Wave pick can be created for allocated orders only! ",
        "BM036" => "There is no CSR in charge of the selected Customers",
        "BM037" => "Order Pick can be executed for New Wave Pick only!",
        "BM038" => "Order Pick for Wave Pick {0} is executed successfully!",
        "BM039" => "Number of locations is insufficient to pick. There is no carton in any location to pick",
        "BM040" => "The location {0} does not match the itemid/sku. Do you still want to update?",
        "BM041" => "{0} pieces from Location {1} are to moved fto Location {2}!",
        "BM042" => "At least 1 item should have Pack Qty!",
        "BM043" => "The change may cause the input data. Do you still want to process?",
        "BM044" => "Work Order should have at least 1 item!",
        "BM045" => "At least 1 item should be allocated!",
        "BM046" => "This customer does not allow to have Partial Order!",
        "BM047" => "Allocated Qty cannot be greater than Available Qty!",
        "BM051" => "Please select orders belonging to 1 order type only!",
        "BM055" => "Total Pieces cannot be less than Requested Pieces/XFER",
        "BM056" => "Only Xfer records with Pending status can be saved and printed wave pick ticket!",
        "BM057" => "Only Wave Pick  with Picking status can be updated Inventories!",
        "BM060" => "Item {0} is being processed in XFER record {1}!",
        'BM061' => "Please Delete ChargeCode first.",
        'BM062' => "Some Input Missing!",
        'BM063' => "There are some zones was used by other customers.",
        'BM064' => "Zones cannot be removed while having inventories!",
        'BM065' => "{0} input doesn't belong to this customer",
        'BM066' => "{0} does not exist in {1}.",
        'BM067' => "{0} not found.",
        'BM068' => "SKU {0} with pack={1} and remain={2} in location {3} already exist in report. Please change quantity for this SKU.",
        'BM069' => "Location {0} not in range.",
        'BM070' => "SKU {0} was created by another report. Please complete this report.",
        'BM071' => "SKU has size color: {0}",
        'BM072' => "Update successfully.",
        'BM073' => "Update fail.",
        'BM074' => "Delete successfully.",
        'BM075' => "Save successfully.",
        'BM076' => "This customer has not been in this User.",
        'BM077' => "This customer has been in this User.",
        'BM078' => "Permission denied!",
        'BM079' => "{0} has completed.",
        'BM080' => "Cycle detail is processing or accepted",
        'BM081' => "{0} is invalid",
        'BM082' => "{0} is not exists on range locations of this Cycle.",
        'BM083' => "{0} must be a integer",
        'BM084' => "Cycle count detail invalid: {0}",
        'BM085' => "Cycle count detail: {0} not in cycle hdr {1}",
        'BM086' => "Cycle count detail: {0} not cycled or processed {1}",
        'BM087' => "{0} out of inventory for Cycle Count",
        'BM088' => "{0} is required",
        'BM089' => "{0} invalid: {1}",
        'BM090' => "Warehouse do not have customerId: {0}",
        'BM091' => "The SKU do not exist in warehouse: {}",
        'BM092' => "Locations have to LocationFrom and LocationTo",
        'BM093' => "Locations From cannot greater than location To!",
        'BM094' => "Locations have locked or disabled: {0}",
        'BM095' => "{0} has existed on the {1}",
        'BM096' => "Have some thing wrong",
        'BM097' => "{0} is existed!",
        'BM098' => "Cannot Delete this {0}",
        'BM099' => "Status of this Goods Receipt is received!",
        'BM100' => "Details input don't belong to this goods receipt",
        'BM101' => "This goods receipt assigned Pallet!",
        'BM102' => "{0} assigned successfully!",
        'BM103' => "The current location does not have any pallet!",
        'BM104' => "This goods receipt not assigned Pallet!",
        'BM105' => "Not Enough Location to Put Pallet In!",
        'BM106' => "There is no {0} has RFID {1} in system.",
        'BM107' => "Core Service is error now",
        'BM108' => "Invalid Token Or Expired",
        'BM109' => "{0} favorite menu fail",
        'BM120' => "Pass Authorization",
        'BM121' => "At least fill {0}",
        'BM122' => "Cannot save Favorite Menu, Exist Menu ID belongs user ID.",
        'BM123' => "{0} cannot {1}!",
        'BM124' => "The Order File was imported successfully",
        'BM125' => "Please choose 1 online order to print packing slip!",
        'BM126' => "Order's shipment has not been created yet!",
        'BM127' => "Selected Orders are shipped successfully",
        'BM128' => "Work Order {0} is created for the selected Order already!",
        'BM129' => "{0} successfully!",
        'BM130' => "{0} have not been {1} yet!",
        'BM131' => "{0} do not have {1}!",
        'BM132' => "Only staging or partial staging orders can be print LPN!",
        'BM133' => "Reset Password!",
        'BM134' => "Setup Password!",
        'BM135' => "{0} couldn't be removed",
        'BM136' => "Please select active locations to process XFER!",
        'BM137' => "To Location currently has different items!",
        'BM138' => "Please select active Actual From locations to process XFER!",
        'BM139' => "Actual From Location currently has different items!",
        'BM140' => "Only Xfer records with {0} status can be {1}",
        'BM141' => "{0} not enough {1}!",
        'BM142' => "Please input enough requested params!",
        'BM143' => "There is some errors, cant load good receipt header ID.",
        'BM144' => "{0} failed!",
        'BM145' => "This location is not Put Away Area!",
        'BM146' => "Picked pallet from put area location successfully!",
        'BM147' => "Picked pallet from put area location failed",
        'BM148' => "Can't get pallet by RFID: {0}",
        'BM149' => "Update location for pallet and carton successfully!",
        'BM150' => "Location belong to other zone!",
        'BM151' => "The Location File was imported successfully",
        'BM152' => "Max Count should be greater than Min Count.",
        'BM153' => "Original order {0} has not been allocated, therefore this backorder can't be allocated!",
        'BM154' => "{0} out of inventory for Block Stock",
        'BM155' => "Carton must be not found or picked!",
        'BM156' => "Picked QTY {0} is greater than requested QTY {1}",
        'BM157' => "The {0} belong to a {1} is new. The {1} need to be final first.",
        
        'BZ001' => "Zone has been added to warehouse successfully",
        'BZ002' => "No location was added to zone. Please confirm!",
        'BZ003' => "Zone code is duplicated. Please create another zone code.",
        'BZ004' => "Error: Zone min count should not greater than zone max count.",
        'BZ005' => "Input data is invalid. Please check again.",

        'IN001' => "Invoice summary table not created for the enddate selected",
        'IN002' => "Invoice Date Missing",

        'VR001'   => "The {0} cannot be blank",
        'VR002'   => "Please don't insert any of following character: \\ / * ? : < > |",
        'VR003'   => "Data is null",
        'VR004'   => "Data is not null",
        'VR005'   => "The input must be {0} type",
        'VR006'   => "The input must not be {0} type",
        'VR007'   => "The {0} is true",
        'VR008'   => "The {0} is false",
        'VR009'   => "",
        'VR010'   => "{0} is equal to {1}",
        'VR011'   => "{0} is not equal to {1}",
        'VR012'   => "{0} is greater than {1}",
        'VR013'   => "{0} is not greater than {1}",
        'VR014'   => "{0} is greater or equal {1}",
        'VR015'   => "{0} is not greater than or equal to {1}",
        'VR016'   => "{0} must be in the range of {1} and {2}",
        'VR017'   => "{0} is not be in the range between {1} and {2}",
        'VR018'   => "{0} is less than {1}",
        'VR019'   => "{0} is not less than {1}",
        'VR020'   => "{0} is not less than or equal to {1}",
        'VR021'   => "{0} is not less than or equal to {1}",
        'VR022'   => "{0} contains the following items {1}",
        'VR023'   => "{0} doesn't contain the following items {1}",
        'VR024'   => "{0} contains all following items: {1}, {2}, …",
        'VR025'   => "{0} doesn't not contain all following items: {1}, {2}",
        'VR026'   => "{0} contains any of following items: {1}, {2}…",
        'VR027'   => "{0} doesn't contain any of following items: {1}, {2}, …",
        'VR028'   => "{0} is empty",
        'VR029'   => "{0} is not empty",
        'VR030'   => "{0} has length of {1}",
        'VR031'   => "{0} is null",
        'VR032'   => "{0} has length longer than {1}",
        'VR033'   => "{0} has length not longer than {1}",
        'VR034'   => "{0} has length longer than or equal to {1}",
        'VR035'   => "{0} has length not longer than or equal to {1}",
        'VR036'   => "{0} has length shoter than {1}",
        'VR037'   => "{0} has length not shorter than {1}",
        'VR038'   => "{0} has length shorter than or equal to {1}",
        'VR039'   => "{0} has length not shorter than or equal to {1}",
        'VR040'   => "String {0} contains the following characters: {1}, {2}…",
        'VR041'   => "String {0} doesn't contains the following characters: {1}, {2}…",
        'VR042'   => "String {0} starts with string {1}",
        'VR043'   => "String {0} doesn't start with string {1}",
        'VR044'   => "String {0} ends with string {1}",
        'VR045'   => "String {0} doesn't end with string {1}",
        'VR046'   => "String {0} has length equal to {1}",
        'VR047'   => "String {0} is null",
        'VR048'   => "String {0} is empty",
        'VR049'   => "String {0} is not empty",
        'VR050'   => "String {0} is null or empty",
        'VR051'   => "String {0} is not null or empty",
        'VR052'   => "The length of string {0} is longer than {1}",
        'VR053'   => "The length of string {1] is longer than or equal to {0}",
        'VR054'   => "The length of string {0} is shorter than {1}",
        'VR055'   => "The length of string {0} is shorter than or equal to {1}",
        'VR056'   => "{0} is between {1} and {2}",
        'VR057'   => "{0} is equal to {1}",
        'VR058'   => "{0} is greater than {1}",
        'VR059'   => "{0} is greater than or equal to {1}",
        'VR060'   => "{0} is less than {1}",
        'VR061'   => "{0} is less than or equal to {1}",
        'VR062'   => "{0} is 0",
        'VR063'   => "{0} is not 0",
        'VR064'   => "{0} is different with {1}",
        'VR065'   => "{0} must be approximate to {1}",
        'VR066'   => "{0} must be equal to {1}% of {2}",
        'VR067'   => "This date {0} is earlier than {1}",
        'VR068'   => "This date {0} is later than {1}",
        'VR069'   => "This date {0} is not future date",
        'VR070'   => "This date {0} is not past date",
        'VR071'   => "This date {0} is not from {1} to {2}",
        'VR072'   => "This date {0} from {1} to {2}",
        'VR073'   => "This date {0} must be current date",
        'VR074'   => "This date {0} must be {1} day(s) later than {2}",
        'VR075'   => "This date {0} must be {1} day(s) earlier than {2}",
        'VR076'   => "Value must follow the partern {0}",
        'VR077'   => "Value must be not null",
        'VR078'   => "Value must be space ignore",
        'VR079'   => "Value must be case sensitive",
        'VR080'   => "Invalid e-mail",
        'VR081'   => "Uploaded file must not be empty",
        'VR082'   => "Uploaded files have maximum {0} files",
        'VR083'   => "Uploaded files have max size of {0} MB",
        'VR084'   => "Uploaded files have min size of {0} MB",
        'VR085'   => "Uploaded files are wrong types",
        'VR086'   => "Uploaded files are to invalid path",
        'VR087'   => "This number can have max {0} digits!",
        'VR088'   => "This Date {0} must not be in the past",
        'VR089'   => "{0} have to be after {1}",
        'VR090'   => "This code cannot be a number!",
        'VR091'   => "Please select more than one items!",
        'VR100'   => "The current server is busy, please wait a few minutes and try again",
        'VR101'   => "",
        'VR102'   => "",
        'VR103-1' => "Password at least 1 number",
        'VR103-2' => "Password at least 1 lowercase character (a-z)",
        'VR103-3' => "Password at least 1 uppercase character (A-Z)",
        'VR104'   => "",
        'VR105'   => "",
        'VR106'   => "Please enter new password and confirm",
        'VR107'   => "",
        'VR108'   => "",
        'VR109'   => "",
        'VR110'   => "",
        'VR111'   => "",
    ];

    public static function get($code, ...$params)
    {
        $message = array_get(self::$messages, "$code", null);
        foreach ($params as $key => $param) {
            $message = str_replace("{{$key}}", $param, $message);
        }

        return $message;
    }

    /**
     * @param $messageInput
     * @param array ...$params
     *
     * @return mixed|string
     * @throws \Exception
     */
    public static function getOnLang($messageInput, ...$params)
    {
        $config = config('database.redis.default');
        $predis = new Client($config);

        //get userID from UserInfo
        $predis_UserInfo = new Data();
        $userInfo = $predis_UserInfo->getUserInfo();
        $userId_redis = array_get($userInfo, 'user_id', 0);

        //Get Lang of User on Redis
        $userLang_Redis = $predis->get('user_lang');
        $userLang_Arr = json_decode($userLang_Redis, true);
        $userLangVar = "";

        //Check user Lang
        if (!$userLang_Arr) {
            throw new \Exception("Please check Redis server!");
        }

        foreach ($userLang_Arr as $userLang) {
            $userId = array_get($userLang, 'user_id', '');
            if ($userId == $userId_redis) {
                $userLangVar = trim(array_get($userLang, 'language', 'en_US'));
            }
        }

        //get multiLang from Redis
        $multiLang = $predis->get('multiLang');
        $multiLang_Arrs = json_decode($multiLang, true);
        $message = trim($messageInput);
        $translation_Redis = "";

        //Check message
        if (!$multiLang_Arrs) {
            throw new \Exception("Please check Redis server!");
        }

        foreach ($multiLang_Arrs as $multiLang_Arr) {
            $language_Redis = trim(array_get($multiLang_Arr, 'language', ''));
            $message_Redis = trim(array_get($multiLang_Arr, 'message', ''));
            $category_Redis = trim(array_get($multiLang_Arr, 'category', ''));
            $category_Lang = env('LANG_CATEGORY');
            if (($message == $message_Redis) && ($userLangVar == $language_Redis) && ($category_Lang == $category_Redis)) {
                $translation_Redis = array_get($multiLang_Arr, 'translation', '');
            }
        }
        if (!$translation_Redis) {
            $translation_Redis = $messageInput;
        }
        foreach ($params as $key => $param) {
            $translation_Redis = str_replace("{{$key}}", $param, $translation_Redis);
        }

        return $translation_Redis;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public static function userLang()
    {
        //get user Lang
        $config = config('database.redis.default');
        $predis = new Client($config);

        //get userID from UserInfo
        $predis_UserInfo = new Data();
        $userInfo = $predis_UserInfo->getUserInfo();
        $userId_redis = array_get($userInfo, 'user_id', 0);
        $userLang_Redis = $predis->get('user_lang');
        $userLang_Arr = json_decode($userLang_Redis, true);
        $userLangVar = "";

        //Check user Lang
        if (!$userLang_Arr) {
            throw new \Exception("Please check Redis server!");
        }

        foreach ($userLang_Arr as $userLang) {
            $userId = array_get($userLang, 'user_id', '');
            if ($userId == $userId_redis) {
                $userLangVar = trim(array_get($userLang, 'language', 'en_US'));
            }
        }

        return $userLangVar;
    }

    /**
     * @param $messageInput
     * @param $categoryLang
     *
     * @return string
     * @throws \Exception
     */
    public static function getOnLangFE($messageInput, $categoryLang)
    {
        $config = config('database.redis.default');
        $predis = new Client($config);

        //get userID from UserInfo
        $predis_UserInfo = new Data();
        $userInfo = $predis_UserInfo->getUserInfo();
        $userId_redis = array_get($userInfo, 'user_id', 0);

        //Get Lang of User on Redis
        $userLang_Redis = $predis->get('user_lang');
        $userLang_Arr = json_decode($userLang_Redis, true);
        $userLangVar = "";

        //Check user Lang
        if (!$userLang_Arr) {
            throw new \Exception("Please check Redis server!");
        }

        foreach ($userLang_Arr as $userLang) {
            $userId = array_get($userLang, 'user_id', '');
            if ($userId == $userId_redis) {
                $userLangVar = trim(array_get($userLang, 'language', 'en_US'));
            }
        }

        //get multiLang from Redis
        $multiLang = $predis->get('multiLang');
        $multiLang_Arrs = json_decode($multiLang, true);
        $message = trim($messageInput);
        $translation_Redis = "";

        //Check message
        if (!$multiLang_Arrs) {
            throw new \Exception("Please check Redis server!");
        }

        foreach ($multiLang_Arrs as $multiLang_Arr) {
            $language_Redis = trim(array_get($multiLang_Arr, 'language', ''));
            $message_Redis = trim(array_get($multiLang_Arr, 'message', ''));
            $category_Redis = trim(array_get($multiLang_Arr, 'category', ''));
            $category_Lang = $categoryLang;
            if (($message == $message_Redis) && ($userLangVar == $language_Redis) && ($category_Lang == $category_Redis)) {
                $translation_Redis = array_get($multiLang_Arr, 'translation', '');
            }
        }

        if (!$translation_Redis) {
            $translation_Redis = $messageInput;
        }

        return $translation_Redis;
    }

}