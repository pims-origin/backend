<?php

namespace App\Helpers;

use App\Setting;

class SettingMailHelper
{
    public static function setting($key)
    {
        $item = Setting::where('setting_key', $key)
            ->where('deleted', 0)
            ->get();

        if (!empty($item[0]->setting_value)) {
            $smtpInfo = json_decode($item[0]->setting_value, true);

            return $smtpInfo;
        }
    }

    public function setCustomConfig($data)
    {
        config(['mail.host' => $data->smtp_host]);
        config(['mail.port' => $data->smtp_port]);
        config(['mail.from' => [
            'address' => 'seldat.noreplpy@gmail.com',
            'name' => 'Seldat Inc'
        ]]);
        config(['mail.username' => $data->user_name]);
        config(['mail.password' => $data->password]);
    }
}