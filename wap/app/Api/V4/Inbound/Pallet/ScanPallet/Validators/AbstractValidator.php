<?php

namespace App\Api\V4\Inbound\Pallet\ScanPallet\Validators;

use Validator;
use Dingo\Api\Exception\ValidationHttpException;

abstract class AbstractValidator
{
    /**
     * @return array
     */
    abstract protected function rules();


    /**
     * @param $input
     * @return
     * @throws ApiValidateException
     */
    public function validate($input)
    {
        $validator =  Validator::make($input, $this->rules(), $this->messages());

        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->getMessages());
        }

        return $validator;
    }

    /**
     * @param $input
     *
     * @return bool|string
     */
    public function validateRequireFields($input)
    {
        $validator = Validator::make($input, $this->rules(), $this->messages());

        if ($validator->fails()) {
            $errMessages = '';
            foreach ($validator->errors()->getMessages() as $key => $errorMsg) {
                $errMessages = $errMessages . '  ' . $errorMsg[0];
            }

            return trim($errMessages);
        }

        return false;
    }


    /**
     * @return array
     */
    protected function messages()
    {
        return [];
    }
}
