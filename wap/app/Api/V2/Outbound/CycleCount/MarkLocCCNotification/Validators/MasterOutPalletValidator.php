<?php

namespace App\Api\V2\Outbound\CycleCount\MarkLocCCNotification\Validators;

class MasterOutPalletValidator {

    const PRE_FIX     = 'MPL';
    const TOTAL_CHUNK = 4;
    const ERROR_MSG   = 'Master out pallet format is invalid!';

    private $_errorMsg;

    /**
     * Validate out pallet format
     *
     * @param string $masterOutPalletCode
     * @param integer $whsId
     *
     * @return boolean
     */
    public function validate($masterOutPalletCode, $whsId)
    {
        $mplCorrect = sprintf("Your master out pallet's number %s is invalid.+ Please check Example: + First 3 %s: Pallet Pattern; + Next 2 columns: Belong to BOL Number + Last digits: Sequence Pallet Number (Decimal)",$masterOutPalletCode, self::PRE_FIX);

        $codeArr = explode('-', $masterOutPalletCode);
        if (count($codeArr) != self::TOTAL_CHUNK) {
            $this->_errorMsg = $mplCorrect;
            return false;
        }
        $first = array_shift($codeArr);
        if ($first !== self::PRE_FIX) {
            $this->_errorMsg = $mplCorrect;
            return false;
        }

        foreach ($codeArr as $code) {
            if (!is_numeric($code)) {
                $this->_errorMsg = $mplCorrect;
                return false;
            }
        }

        //  MPL-BOL-0001 =>  MPL-1709-00014-0001
        // $checkMmDd = array_first($codeArr);
        // if (strlen($checkMmDd) != 4) {
        //     $this->_errorMsg = $mplCorrect;
        //         return false;
        // }
        // $checkWhs   = array_last($codeArr);
        // if (strlen($checkWhs) != 7) {
        //     $this->_errorMsg = $mplCorrect;
        //         return false;
        // }

        // $checkWhsId = (int)substr($checkWhs, 0, 2);
        // if ($checkWhsId != $whsId) {
        //     $this->_errorMsg = "The master out pallet doesn't belong to current warehouse";
        //     return false;
        // }

        return true;
    }

    /**
     * @return array
     */
    public function getError() {
        return $this->_errorMsg;
    }

}
