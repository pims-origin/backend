<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/18/2016
 * Time: 2:23 PM
 */

namespace backend\models;

use yii\db\Query;
use yii\data\Pagination;
use backend\models\Form;
use yii\helpers\VarDumper;

class  ProfileSearch
{
    public static function getProfile($id)
    {
        $query = (new Query());
        $user = $query->from('users')
            ->where(['user_id' => $id])
            ->select(ProfileSearchForm::ALLOW_SELECT_FIELDS)
            ->one();
        if (null === $user) {
            return false;
        }

        return $user;
    }
}