<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Models;



use Seldat\Wms2\Models\ZoneLocationStatus;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ZoneLocationStatusModel extends AbstractModel
{
    /**
     * @var int
     */
    protected $limit = 10;

    /**
     * @var ZoneLocationStatus
     */
    protected $model;

    /**
     * ZoneLocationStatusModel constructor.
     *
     * @param ZoneLocationStatus|null $model
     */
    public function __construct(ZoneLocationStatus $model = null)
    {
        $this->model = ($model) ?: new ZoneLocationStatus();
    }

    /**
     * Search ZoneLocationStatus
     *
     * @param array $attributes
     * @param array $with
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [])
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'loc_sts_desc' || $key === 'loc_sts_code') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }
        $this->model->filterData($query);
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($this->limit);

        return $models;
    }
}