<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReportCarton;

class ReportCartonTransformer extends AbstractTransformer
{

    /**
     * @param ReportCarton $reportCarton
     * @return array
     */
    public function transform(ReportCarton $reportCarton)
    {
        return [
        	"ctn_id"			=>	array_get($reportCarton, 'ctn_id', null),
        	"plt_id"			=>	array_get($reportCarton, 'plt_id', null),
        	"asn_dtl_id"		=>	array_get($reportCarton, 'asn_dtl_id', null),
        	"gr_dtl_id"			=>	array_get($reportCarton, 'gr_dtl_id', null),
        	"whs_id"			=>	array_get($reportCarton, 'whs_id', null),
        	"cus_id"			=>	array_get($reportCarton, 'cus_id', null),
        	"cus_code"			=>	array_get($reportCarton, 'cus_code', null),
        	"gr_hdr_num"		=>	array_get($reportCarton, 'gr_hdr_num', null),
        	"ctn_num"			=>	array_get($reportCarton, 'ctn_num', null),
        	"rfid"				=>	array_get($reportCarton, 'rfid', null),
        	"ctn_sts"			=>	array_get($reportCarton, 'ctn_sts', null),
        	"ctn_uom_id"		=>	array_get($reportCarton, 'ctn_uom_id', null),
        	"loc_id"			=>	array_get($reportCarton, 'loc_id', null),
        	"loc_code"			=>	array_get($reportCarton, 'loc_code', null),
        	"loc_name"			=>	array_get($reportCarton, 'loc_name', null),
        	"loc_type_code"		=>	array_get($reportCarton, 'loc_type_code', null),
        	"is_damaged"		=>	array_get($reportCarton, 'is_damaged', null),
        	"piece_remain"		=>	array_get($reportCarton, 'piece_remain', null),
        	"piece_ttl"			=>	array_get($reportCarton, 'piece_ttl', null),
        	"created_at"		=>	$this->setTimeZone(array_get($reportCarton, 'whs_id', null), array_get($reportCarton->toArray(), 'created_at', null)),
        	"created_by"		=>	array_get($reportCarton, 'created_by', null),
        	"updated_at"		=>	$this->setTimeZone(array_get($reportCarton, 'whs_id', null), array_get($reportCarton->toArray(), 'updated_at', null)),
        	"updated_by"		=>	array_get($reportCarton, 'updated_by', null),
        	"deleted"			=>	array_get($reportCarton, 'deleted', null),
        	"deleted_at"		=>	$this->setTimeZone(array_get($reportCarton, 'whs_id', null), array_get($reportCarton, 'deleted_at', null)),
        	"is_ecom"			=>	array_get($reportCarton, 'is_ecom', null),
        	"gr_hdr_id"			=>	array_get($reportCarton, 'gr_hdr_id', null),
        	"gr_dt"				=>	array_get($reportCarton, 'gr_dt', null),
        	"picked_dt"			=>	$this->setTimeZone(array_get($reportCarton, 'whs_id', null), array_get($reportCarton, 'picked_dt', null)),
        	"storage_duration" 	=>	array_get($reportCarton, 'storage_duration', null),
        	"item_id"			=>	array_get($reportCarton, 'item_id', null),
        	"sku"				=>	array_get($reportCarton, 'sku', null),
        	"size"				=>	array_get($reportCarton, 'size', null),
        	"color"				=>	array_get($reportCarton, 'color', null),
        	"lot"				=>	array_get($reportCarton, 'lot', null),
        	"ctn_pack_size"		=>	array_get($reportCarton, 'ctn_pack_size', null),
        	"po"				=>	array_get($reportCarton, 'po', null),
        	"aisle"				=>	array_get($reportCarton, 'aisle', null),
        	"level"				=>	array_get($reportCarton, 'level', null),
        	"row"				=>	array_get($reportCarton, 'row', null),
        	"expired_dt"		=>	$this->setTimeZone(array_get($reportCarton, 'whs_id', null), array_get($reportCarton, 'expired_dt', null)),
        	"uom_code"			=>	array_get($reportCarton, 'uom_code', null),
        	"uom_name"			=>	array_get($reportCarton, 'uom_name', null),
        	"upc"				=>	array_get($reportCarton, 'upc', null),
        	"ctnr_id"			=>	array_get($reportCarton, 'ctnr_id', null),
        	"ctnr_num"			=>	array_get($reportCarton, 'ctnr_num', null),
        	"length"			=>	array_get($reportCarton, 'length', null),
        	"width"				=>	array_get($reportCarton, 'width', null),
        	"height"			=>	array_get($reportCarton, 'height', null),
        	"weight"			=>	array_get($reportCarton, 'weight', null),
        	"cube"				=>	array_get($reportCarton, 'cube', null),
        	"volume"			=>	array_get($reportCarton, 'volume', null),
        	"return_id"			=>	array_get($reportCarton, 'return_id', null),
        	"des"				=>	array_get($reportCarton, 'des', null),
        	"shipped_dt"		=>	$this->setTimeZone(array_get($reportCarton, 'whs_id', null), array_get($reportCarton, 'shipped_dt', null)),
        	"origin_id"			=>	array_get($reportCarton, 'origin_id', null),
        	"inner_pack"		=>	array_get($reportCarton, 'inner_pack', null),
        	"spc_hdl_code"		=>	array_get($reportCarton, 'spc_hdl_code', null),
        	"spc_hdl_name"		=>	array_get($reportCarton, 'spc_hdl_name', null),
        	"cat_code"			=>	array_get($reportCarton, 'cat_code', null),
        	"cat_name"			=>	array_get($reportCarton, 'cat_name', null),
        	"ref"				=>	array_get($reportCarton, 'ref', null),
        	"ucc128"			=>	array_get($reportCarton, 'ucc128', null),
        	"ctn_type"			=>	array_get($reportCarton, 'ctn_type', null),
        	"cus_name"			=>	array_get($reportCarton, 'cus_name', null),
        	"plt_num"			=>	array_get($reportCarton, 'plt_num', null),
        	"plt_rfid"			=>	array_get($reportCarton, 'plt_rfid', null),
        	'timezone'      	=> 	$this->getWhsTimeZone(array_get($reportCarton, 'whs_id', null)) ?: 'UTC±0',
        ];
    }
}
