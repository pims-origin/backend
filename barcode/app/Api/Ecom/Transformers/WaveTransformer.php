<?php

namespace App\Api\Ecom\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WavepickHdr;

class WaveTransformer extends TransformerAbstract
{
    public function transform(WavepickHdr $wave)
    {
        $total = object_get($wave, 'total', 0);
        $actual = object_get($wave, 'actual', 0);
        $status = object_get($wave, 'status', 'NOT');

        return [
            'wv_id'  => object_get($wave, 'wv_id'),
            'wv_num' => object_get($wave, 'wv_num'),
            'odr_ttl' => object_get($wave, 'odr_ttl'),
            'sku'    => $total . "/" . $actual,
            'status' => $status
        ];
    }
}
