<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\InventorySummary;
use Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;

class InventorySummaryModel extends AbstractModel
{

    protected $model;

    /**
     * InventorySummaryModel constructor.
     */
    public function __construct(InventorySummary $model = null)
    {
        $this->model = ($model) ?: new InventorySummary();
    }

    /**
     * Update inventory summary
     *
     * @param $data
     *
     * @return mixed
     */
    public function updateInvtSumr($data)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return $this->model
            ->where('item_id', '=', $data['item_id'])
            ->where('whs_id', '=', $currentWH)
            ->update([
                //'ttl'           => $data['ttl'],
                'allocated_qty' => $data['allocated_qty'],
                'avail'         => $data['avail'],
            ]);
    }

    /**
     * Get Item data
     *
     * @param $itm_id
     *
     * @return mixed
     */
    public function getItemData($itm_id)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return $this->model
            ->where('item_id', '=', $itm_id)
            ->where('whs_id', '=', $currentWH)
            ->first();
    }

    public function updateInventoryByLot($cond, $pickedQt){

        $sqlPickQty = sprintf('`picked_qty` + %d', $pickedQt);
        $sqlAvailQty = sprintf('IF(`allocated_qty` < %d, 0, `allocated_qty` - %d)', $pickedQt, $pickedQt);
        $res=  DB::table('invt_smr')
            ->where([
                'whs_id' => $cond['whs_id'],
                'item_id' => $cond['item_id'],
                'lot' => $cond['lot']
            ])
            ->update([
                    'picked_qty' =>    DB::raw($sqlPickQty),
                    'allocated_qty' => DB::raw($sqlAvailQty),
            ]
            );
        return $res;
    }

    public function updateInventoryByLotBackOrder($cond, $discrepancyQty){

        $sqlPickQty = sprintf('`avail` + %d', $discrepancyQty);
        $sqlAvailQty = sprintf('IF(`allocated_qty` < %d, 0, `allocated_qty` - %d)', $discrepancyQty, $discrepancyQty);
        $res=  DB::table('invt_smr')
            ->where([
                'whs_id' => $cond['whs_id'],
                'item_id' => $cond['item_id'],
                'lot' => $cond['lot']
            ])
            ->update([
                    'avail' =>    DB::raw($sqlPickQty),
                    'allocated_qty' => DB::raw($sqlAvailQty),
                ]
            );

        return $res;
    }


}
