<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Carton;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;

class CycleCountNotificationModel extends AbstractModel
{

    CONST CCN_STATUS_NEW = 'NW';
    CONST CCN_STATUS_CYCLE_COUNT = 'CC';
    CONST CCN_STATUS_CANCEL = 'CE';
    CONST CCN_STATUS_COMPLETED = 'CP';

    protected $_STATUS = [
        self::CCN_STATUS_NEW         => 'New',
        self::CCN_STATUS_CYCLE_COUNT => 'Cycle Count',
        self::CCN_STATUS_COMPLETED => 'Completed',
        self::CCN_STATUS_CANCEL      => 'Cancel'
    ];

    /**
     * CycleCountNotification constructor.
     *
     * @param CycleCountNotification $model
     */
    public function __construct(CycleCountNotification $model = null)
    {
        $this->model = $model ? $model : new CycleCountNotification();
    }

    /**
     * Search CycleCountNotification
     *
     * @param int $whsId
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return object|null $models
     */
    public function search_bk($whsId, $attributes = [], $with = [], $limit = null, $complete = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query->where('whs_id', $whsId);
        //$query->where('cc_ntf_sts', 'NW');
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key == 'cc-ntf-sts') {
                    $query->where('cc_ntf_sts', $value);
                }
                if ($key == 'loc-code') {
                    $query->where('loc_code', 'LIKE', "%" . SelStr::escapeLike($value) . "%");
                }
                if ($key == 'from-date') {
                    $query->where('created_at', '>=', strtotime($value));
                }
                if ($key == 'to-date') {
                    $query->where('created_at', '<=', strtotime($value . " 23:59:59"));
                }
            }
        }
        $case = "CASE\n";
        foreach ($this->_STATUS as $code => $name) {
            $case .= "WHEN cc_notification.cc_ntf_sts = '{$code}' THEN '{$name}'\n";
        }
        $case .= "END";

        $query->select(DB::raw("cc_notification.*, {$case} as cc_ntf_sts_name"));
        if ($complete) {
            $query->groupBy('loc_code');
        }
        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function makeSearchQuery($whs_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query->where('whs_id', $whs_id);
        // $query->where('remain_qty', '>', 0);
        //$query->where('cc_notification.remain_qty', '<', 'cc_notification.pack');

        if (isset($attributes['loc-code'])) {
            $query->where('loc_code', 'LIKE', "%" . SelStr::escapeLike($attributes['loc-code']) . "%");
        }

        if (isset($attributes['cc_ntf_sts'])) {
            $query->where('cc_ntf_sts', $attributes['cc_ntf_sts']);
        }

        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        //search more fields from Sku auto complete
        if (isset($attributes['item_id'])) {
            $query->where('item_id', $attributes['item_id']);
        }

        if (isset($attributes['lot'])) {
            $query->where('lot', $attributes['lot']);
        }

        //search according to from date to date
        if (!empty($attributes['created_at_from'])) {
            $query->where('created_at', ">=", strtotime($attributes['created_at_from']));
        }

        if (!empty($attributes['created_at_to'])) {
            $query->where('created_at', "<", strtotime($attributes['created_at_to'] . "+1 day"));
        }

        // Get
        $this->sortBuilder($query, $attributes);

        return $query;
    }

    public function search($whs_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        $query = $this->makeSearchQuery($whs_id, $attributes, $with, $limit, $export);
        if (isset($attributes['loc_code'])) {
            $query->where('loc_code', 'LIKE', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        if ($export) {
            $models = $query->get();
        } else {
            $models = $query->paginate($limit);
        }

        // dd($query->toSql());

        return $models;
    }

    public function getAutocompleteLocations($whs_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        $query = $this->makeSearchQuery($whs_id, $attributes, $with, $limit, $export)
                    ->groupBy('loc_code');

        return $query->paginate($limit);
    }

    /**
     * Create or update Cycle Count Notification
     *
     * @param integer $whsId
     * @param array $post
     * @param integer $ccnID
     *
     * @return \Illuminate\Support\Collection
     *
     * @throws \Exception
     */
    public function processCCN($whsId, $post, $ccnID = null)
    {
        $collection = new \Illuminate\Support\Collection();
        if ($ccnID) {
            $this->getModel()->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
            $data = $this->bindData($whsId, $post, $ccnID);
            $model = $this->model->find($ccnID);
            if($data['cc_ntf_sts'] === 'CE'){
                $this->setActiveCarton($data['item_id'], $data['loc_id'], $data['lot']);
                $this->updateInvSmrWhenActiveCarton($data['whs_id'], $data['item_id'], $data['loc_id'], $data['lot']);
                $this->setActiveLocation($data['loc_id']);
            }
            if($data['cc_ntf_sts'] === 'NW' && ($model->loc_code !== $data['loc_code'] || $model->item_id !== $data['item_id'])){
                $this->setActiveCarton($model->item_id, $model->loc_id, $model->lot);
                $this->updateInvSmrWhenActiveCarton($data['whs_id'], $model->item_id, $model->loc_id, $data['lot']);
                $this->setActiveLocation($model->loc_id);
                $this->setLockCarton($data['item_id'], $data['loc_id'], $data['lot']);
                $this->updateInvtSmrWhenLockCarton($data['whs_id'], $data['item_id'], $data['loc_id'], $data['lot']);
                $this->setLockLocation($data['loc_id']);
            }
            $model->update($data);
            $collection->push($model);
        } else {
            foreach ($post as $value) {
                $data = $this->bindData($whsId, $value, $ccnID = null);
                $ccn = new CycleCountNotification();
                $this->setLockCarton($data['item_id'], $data['loc_id'], $data['lot']);
                $this->updateInvtSmrWhenLockCarton($data['whs_id'], $data['item_id'], $data['loc_id'], $data['lot']);
                $this->setLockLocation($data['loc_id']);
                $model = $ccn->create($data);
                $collection->push($model);
            }
        }

        return $collection;
    }

    public function setLockCarton(Int $itemId, Int $locId, String $lot)
    {
        $cartons = Carton::where('item_id', $itemId)
                        ->where('loc_id', $locId)
                        ->where('lot', $lot)
                        ->update(['ctn_sts' => 'LK']);
    }

    public function setLockLocation(Int $locId)
    {
        $location = Location::where('loc_id', $locId)
                            ->update(['loc_sts_code' => 'LK']);
    }

    public function setActiveCarton(Int $itemId, Int $locId, String $lot)
    {
        $cartons = Carton::where('item_id', $itemId)
                        ->where('loc_id', $locId)
                        ->where('lot', $lot)
                        ->update(['ctn_sts' => 'AC']);
    }

    public function setActiveLocation(Int $locId)
    {
        $location = Location::where('loc_id', $locId)
                            ->update(['loc_sts_code' => 'AC']);
    }

    public function getPieceRemainOfCarton(Int $whsId, Int $itemId, Int $locId, String $lot)
    {
        $remain = DB::table('cartons')
                ->where('cartons.item_id', $itemId)
                ->where('cartons.loc_id', $locId)
                ->where('cartons.lot', $lot)
                ->where('cartons.whs_id', $whsId)
                ->where('cartons.is_damaged', 0)
                ->where('cartons.deleted', 0)
                // ->sum('cartons.piece_remain');
                // ->first();
                ->max('cartons.piece_remain');

        return $remain;
        // return array_get($remain, 'piece_remain');
    }

    public function updateInvtSmrWhenLockCarton(Int $whsId, Int $itemId, Int $locId, String $lot)
    {
        $remain = $this->getPieceRemainOfCarton($whsId ,$itemId, $locId, $lot) ?? 0;
        DB::table('invt_smr')
            ->where('invt_smr.item_id', $itemId)
            ->where('invt_smr.cus_id', $locId)
            ->where('invt_smr.whs_id', $whsId)
            ->where('invt_smr.lot', $lot)
            ->update(['invt_smr.lock_qty' => $remain]);
    }

    public function updateInvSmrWhenActiveCarton(Int $whsId, Int $itemId, Int $locId, String $lot)
    {
        $remain = $this->getPieceRemainOfCarton($whsId ,$itemId, $locId, $lot) ?? 0;

        $invtSmr = DB::table('invt_smr')
                    ->select([
                        'invt_smr.inv_sum_id',
                        'invt_smr.lock_qty'
                    ])
                    ->where('invt_smr.item_id', $itemId)
                    ->where('invt_smr.cus_id', $locId)
                    ->where('invt_smr.whs_id', $whsId)
                    ->where('invt_smr.lot', $lot)
                    ->first();


        $lock_qty = 0;
        if($invtSmr['lock_qty'] > $remain){
            $lock_qty =  $invtSmr['lock_qty'] - $remain;
        }

        DB::table('invt_smr')
            ->where('invt_smr.inv_sum_id', $invtSmr['inv_sum_id'])
            ->update(['lock_qty' => $lock_qty]);
    }


    public function findLocId($locCode)
    {
        $loc = Location::where('loc_code', $locCode)->first();

        if (!$loc) {

            throw new \Exception("Location {$locCode} not found");
        }

        return $loc->loc_id;
    }

    /**
     * Create Data for save/update
     *
     * @param int $whsId
     * @param int $value
     * @param int $ccnID
     *
     * @return array
     */
    private function bindData($whsId, $value, $ccnID)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);

        $locCode = array_get($value, 'loc_code', null);
        $locId = $this->findLocId($locCode);

        $remain_qty = $this->getPieceRemainOfCarton($whsId, $value['item_id'], $locId, $value['lot']) ?? 0;

        $data = [
            'whs_id'       => $whsId,
            'loc_id'       => $locId,
            'loc_code'     => $locCode,
            'cc_ntf_sts'   => array_get($value, 'cc_ntf_sts', null),
            'cc_ntf_date'  => time(),
            //'created_at'   => time(),
            //'created_by'   => $userId,
            //'updated_at'   => time(),
            //'updated_by'   => $userId,
            'deleted_at'   => 915148800,
            'deleted'      => 0,
            'reason'       => array_get($value, 'reason', null),
            'item_id'      => array_get($value, 'item_id', null),
            'sku'          => array_get($value, 'sku', null),
            'lot'          => array_get($value, 'lot', null),
            'pack'         => array_get($value, 'pack', null),
            'size'         => array_get($value, 'size', null),
            'color'        => array_get($value, 'color', null),
            'cus_id'       => array_get($value, 'cus_id', null),
            'uom_code'     => array_get($value, 'uom_code', null),
            'uom_name'     => array_get($value, 'uom_name', null),
            'remain_qty'   => $remain_qty,
            'des'          => array_get($value, 'des', null),
            'cycle_dtl_id' => array_get($value, 'cycle_dtl_id', null),
        ];

        if ($ccnID) {
            $data['updated_at'] = time();
            $data['updated_by'] = $userId;
        } else {
            $data['updated_at'] = time();
            $data['updated_by'] = $userId;
            $data['created_at'] = time();
            $data['created_by'] = $userId;
        }

        return $data;
    }

    /**
     * Get detail cycle count notification
     *
     * @param integer $ccnID
     *
     * @return array
     */
    public function findDetail($ccnID)
    {
        $this->getModel()->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $data = $this->getModel()->find($ccnID);

        return ($data) ? $data : null;
    }

    /**
     * Find location not in cc_notification
     *
     * @param integer $whsId
     * @param string $keyword
     *
     * @return Collection
     */
    public function completeLocationAddCCN($whsId, $keyword, $limit = 20)
    {
        /**
         * SELECT loc_id, loc_code
         * FROM location
         * WHERE loc_code NOT IN (
         * SELECT sys_loc_name FROM cycle_dtl cd, cycle_hdr ch
         * WHERE cd.`cycle_hdr_id` = ch.`cycle_hdr_id`
         * AND ch.`cycle_type` = 'LC'
         * AND ch.`cycle_sts` != 'CP'
         * AND ch.deleted = 0
         * )
         * AND loc_id NOT IN (SELECT DISTINCT loc_id FROM cc_notification ccn WHERE cc_ntf_sts = "NW" AND ccn.deleted = 0)
         */
        $locTypeRacId = LockModel::getLocTypeId();
        $query = Location::whereRaw('loc_id NOT IN (select distinct loc_id from cc_notification where cc_ntf_sts = "NW" AND deleted = 0)')
//                ->whereRaw("loc_code NOT IN (
//                    SELECT sys_loc_name FROM cycle_dtl cd, cycle_hdr ch
//                    WHERE cd.`cycle_hdr_id` = ch.`cycle_hdr_id`
//                    AND ch.`cycle_type` = 'LC'
//                    AND ch.`cycle_sts` != 'CP'
//                    AND ch.deleted = 0)")
            ->where('loc_type_id', $locTypeRacId)
            ->where('loc_code', 'LIKE', "%{$keyword}%")
            ->where('loc_whs_id', $whsId);

        //$data = $query->get();

        return $query->paginate($limit);
    }

    /**
     * @param $whsId
     * @param $keyword
     * @param int $limit
     *
     * @return mixed
     */
    public function autoCompleteLocationAddCCN($whsId, $attributes = [], $limit = 20)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $locTypeRacId = LockModel::getLocTypeId();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('location')
            ->select([
                'loc_id',
                'loc_code',
                'loc_alternative_name'
            ])
            ->where('location.loc_type_id', $locTypeRacId)
            ->where('location.loc_whs_id', $whsId);
        if (isset($attributes['keyword'])) {
            $query->where('location.loc_code', 'LIKE', "%{$attributes['keyword']}%");
        }

        $result = $query->groupBy('location.loc_code')
            ->orderBy('location.loc_code')->take($limit)->get();

        return $result;
    }

    /**
     * Delete Cycle Count Notification
     *
     * @param array $ccnIds
     *
     * @return boolean
     */
    public function deleteCycleCountNotification($ccnIds)
    {
        if (!($ccnIds && is_array($ccnIds))) {
            return false;
        }

        return $this->model
            ->whereIn('ccn_id', $ccnIds)
            ->delete();
    }

    public function getStatusList()
    {

        return $this->_STATUS;
    }

    /**
     * Update CCN by Cycle Count Hdr Id
     *
     * @param string $ccnIDs
     * @param string $status
     *
     * @return integer
     */
    public function updateCCNStatusByCCID($ccnIDs, $status = self::CCN_STATUS_CYCLE_COUNT)
    {
        if (!empty($ccnIDs)) {
            $arrCcnIDs = explode(',', $ccnIDs);
            $result = $this->getModel()
                ->whereIn('ccn_id', $arrCcnIDs)
                ->where("cc_ntf_sts", self::CCN_STATUS_NEW)
                ->update([
                    'cc_ntf_sts' => $status
                ]);

            return $result;
        }
    }

    /**
     * Check location is belong to warehouse
     * Return TRUE when all location belong to warehouse
     * Return ARRAY when have locations not belong to warehouse
     *
     * @param integer $whsId
     * @param array $locCode
     *
     * @return boolean|array
     */
    public function checkWhsLocationBelong($whsId, $locCode)
    {
        if (!is_array($locCode)) {
            $locCode = [$locCode];
        }
        $result = Location::where([
            'loc_whs_id' => $whsId
        ])
            ->whereIn('loc_code', $locCode)
            ->get();
        if (!$result->count()) {

            return $locCode;
        }

        $locBelongArr = $result->pluck('loc_code')->all();
        $locNotBelongArr = array_diff($locCode, $locBelongArr);
        if (!count($locNotBelongArr)) {

            return true;
        }

        return $locNotBelongArr;
    }

    /**
     * Check duplication when update/insert ccn
     * Return FALSE when don't have duplicate location
     * Return Collection when have duplication
     *
     * @param integer $whsId
     * @param array $locCode
     * @param integer $ccnId
     *
     * @return boolean
     */
    public function checkCCNDuplication($whsId, $locCode = [], $ccnId = null)
    {

        if (!is_array($locCode)) {
            $locCode = [$locCode];
        }
        $query = $this->getModel()->where([
            'whs_id' => $whsId,
        ])
            ->whereIn('loc_code', $locCode)
            ->where('cc_ntf_sts', '!=', self::CCN_STATUS_CANCEL);
        if ($ccnId !== null) {
            $query->whereNotIn('ccn_id', [$ccnId]);
        }

        $result = $query->get();

        return ($result->count()) ? $result : false;
    }

    /**
     * @param $whsId
     * @param $data
     * @param null $ccnId
     *
     * @return mixed
     */
    public function checkCcIdDuplication($whsId, $data, $ccnId = null)
    {
        $query = $this->getModel()->where([
            'whs_id' => $whsId,
        ])
            ->where('loc_code', $data['loc_code'])
            ->where('item_id', $data['item_id'])
            ->where('lot', $data['lot'])
            ->where('pack', $data['pack'])
            ->where('remain_qty', $data['remain_qty'])
            ->where('cc_ntf_sts', '!=', self::CCN_STATUS_CANCEL);
        if ($ccnId !== null) {
            $query->whereNotIn('ccn_id', [$ccnId]);
        }

        $result = $query->first();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function skuCcnInventoryComplete($whs_id, $attributes = [], $limit = null)
    {
        DB::setFetchMode(\PDO::FETCH_OBJ);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        $query = DB::table('item as item')
                    ->where('item.deleted', 0)
                    ->join('customer_warehouse as cw', 'item.cus_id', '=', 'cw.cus_id')
                    ->where('cw.whs_id', $whs_id)
                    ->leftJoin('cartons AS cartons', 'cartons.item_id', '=', 'item.item_id')
                    ->select([
                        'item.sku',
                        'item.size',
                        'item.color',
                        DB::raw(sprintf('COALESCE(cartons.lot, "NA") as lot')),
                        'item.item_id',
                        'item.pack as ctn_pack_size',
                        'cartons.loc_id',
                        'cartons.loc_code',
                        'cartons.loc_name',
                        'item.uom_code',
                        'item.uom_name',
                        //'cycle_dtl.cycle_dtl_id',
                        'item.pack as remain',
                        'item.cus_id',
                        'cartons.des',
                    ]);

        if (isset($attributes['loc_id'])) {
            $raw = sprintf("(cartons.loc_id is null or cartons.loc_id like %s%s%s)", '%',$attributes['loc_id'], '%');
            $query->whereRaw(DB::raw($raw));
        }

        // Search sku
        if (isset($attributes['sku'])) {
            $query->where('item.sku', 'LIKE', "%{$attributes['sku']}%");
        }

        return $query->groupBy('item.item_id')
                    ->groupBy('cartons.lot')
                    ->orderBy('item.sku')
                    ->take($limit)->get();
    }

    /**
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function getReasonDropDown($attributes = [], $limit = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('reason AS reason')
            ->where('deleted', 0)
            ->select([
                'reason.r_id',
                'reason.r_name',
                'reason.reason_desc',
            ]);

        $result = $query->orderBy('reason.r_id')->take($limit)->get();

        return $result;
    }

    /**
     * @param $dataUpdated
     * @param $where
     *
     * @return mixed
     */
    public function updateWhere($dataUpdated, $where)
    {
        $query = $this->make();
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query->where($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query->where($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query->where($field, '=', $search);
                }
            } else {
                $query->where($field, '=', $value);
            }
        }

        return $query->update($dataUpdated);
    }

    /**
     * @param $ccnIds
     *
     * @return mixed
     */
    public function getCCNByIds($ccnIds)
    {
        $ccnIds = is_array($ccnIds) ? $ccnIds : [$ccnIds];
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $rows = DB::table('cc_notification')
            ->select([
                'cc_notification.cc_ntf_sts',
                'cc_notification.ccn_id',
                'cc_notification.whs_id',
                'cc_notification.cus_id',
                'cc_notification.item_id',
                'cc_notification.sku',
                'cc_notification.size',
                'cc_notification.color',
                'cc_notification.lot',
                'cc_notification.pack as pack_size',
                'cc_notification.remain_qty as piece_remain',
                DB::raw('COALESCE(SUM(cartons.piece_remain), 0) AS sys_pieces_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS sys_carton_qty'),
                //'ctn_uom_id',
                'cc_notification.loc_id as sys_loc_id',
                'cc_notification.loc_code as sys_loc_name'
            ])
            ->leftJoin('cartons', function($join)
                {
                    $join->on('cartons.item_id', '=', 'cc_notification.item_id');
                    $join->where('cartons.deleted','=', 0);
                    $join->whereIn('cartons.ctn_sts', ['LK', 'RG', 'AC']);
                })
            ->whereIn('cc_notification.ccn_id', $ccnIds)
            ->where('cc_notification.cc_ntf_sts', 'NW')
            ->groupBy('cc_notification.ccn_id')
            ->get();

        return $rows;
    }

    /**
     * @param $ccnId
     *
     * @return mixed
     */
    public function getStatusCCNByIds($ccnId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $rows = DB::table('cc_notification')
            ->where('ccn_id', $ccnId)
            ->first();

        return $rows;
    }


}
