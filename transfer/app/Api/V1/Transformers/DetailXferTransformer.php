<?php

namespace App\Api\V1\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\XferDtl;
use Seldat\Wms2\Models\XferHdr;
use Seldat\Wms2\Utils\Status;

class DetailXferTransformer extends TransformerAbstract
{
    /**
     * @param XferHdr $xferHdr
     *
     * @return array
     */
    public function transform(XferHdr $xferHdr)
    {
        $xferDtlInfo = DB::table("xfer_dtl")->where('xfer_hdr_id', $xferHdr->xfer_hdr_id)->first();

        return [
            'xfer_hdr_id'       => $xferHdr->xfer_hdr_id,
            'xfer_hdr_num'      => $xferHdr->xfer_hdr_num,
            'xfer_hdr_sts'      => $xferHdr->xfer_sts,
            'xfer_hdr_sts_name' => Status::getByKey("XFER-STATUS", $xferHdr->xfer_sts),
            'cus_id'            => $xferHdr->cus_id,
            'request_qty'       => $xferHdr->piece_qty,
            'sku'               => $xferHdr->sku,
            'item_id'           => $xferHdr->item_id,
            'cus_upc'           => $xferHdr->cus_upc,
            'size'              => $xferHdr->size,
            'color'             => $xferHdr->color,
            'xfer_hdr_sts_name' => Status::getByKey("XFER-STATUS", $xferHdr->xfer_sts),
            'loc_id'            => array_get($xferDtlInfo, 'mez_loc_id', null),
            'loc_code'            => array_get($xferDtlInfo, 'mez_loc', null),
            'items'             => $xferHdr->details,
        ];

    }
}
