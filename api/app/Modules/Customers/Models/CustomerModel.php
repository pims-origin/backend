<?php

namespace App\Modules\Customers\Models;

use http\Url;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Modules\Customers\Eloquents\User;
use App\Modules\Customers\Eloquents\Customer;
use App\Modules\Customers\Eloquents\CusInUser;
use App\Modules\Customers\Eloquents\CusExUser;
use App\Modules\Customers\Eloquents\Warehouse;
use App\Modules\Customers\Eloquents\CustomerAddress;
use App\Modules\Customers\Eloquents\CustomerContact;
use App\Modules\Customers\Eloquents\CustomerWarehouse;
use App\Modules\Customers\Eloquents\SystemState;
use App\Modules\Customers\Eloquents\SystemCountry;

class CustomerModel extends AbstractModel
{
    protected $cusOfUser;

    protected $cusModel;
    protected $cusExUsModel;
    protected $cusInUserModel;
    protected $whsModel;
    protected $userModel;
    
    protected $cusAddress;
    protected $cusContact;
    
    protected $cusTable;
    protected $cusExUsTable;
    protected $whsTable;
    protected $userTable;
    
    protected $curUserId;

    protected $cusAddressTable;
    protected $cusContactTable;

    public function __construct(
        Customer $cusModel,
        CusExUser $cusExUsModel,
        Warehouse $whsModel,
        User $userModel
    )
    {
        $this->cusOfUser        = Config::get('data.cusOfUser');

        $this->cusModel         = $cusModel ? $cusModel : new Customer;
        $this->cusExUsModel     = $cusExUsModel ? $cusExUsModel : new CusExUser;
        $this->whsModel         = $whsModel ? $whsModel : new Warehouse;
        $this->userModel        = $userModel ? $userModel : new User;

        $this->cusAddressTable   = (new CustomerAddress)->getTable();
        $this->cusContactTable   = (new CustomerContact)->getTable();
        
        $this->cusTable         = $this->cusModel->getTable();
        $this->cusExUsTable     = $this->cusExUsModel->getTable();
        $this->whsTable         = $this->whsModel->getTable();
        $this->userTable        = $this->userModel->getTable();

        $this->curUserId        = Config::get('data.userId');

        $this->cusInUserModel   = new CusInUser;
    }

    public function getCustomerForUserByWhs($whsId, $limit = 20, $baseUrl)
    {
        $cusIds = [];
        foreach ($this->cusOfUser as $item)
        {
            if ($item->whs_id == $whsId)
            {
                $cusIds[] = $item->cus_id;
            }
        }

        $query = $this->cusModel->whereIn('cus_id', $cusIds);

        return $query->paginate($limit)->withPath($baseUrl);
    }

    public function getCustomerDetail($cusId)
    {
        $customer = $this->cusModel->find($cusId);

        foreach ($this->cusOfUser as $item)
        {
            if ($item->cus_id == $cusId)
            {
                return $customer;
            }
        }

        /*if (array_search($cusId, array_column($this->cusOfUser->toArray(), 'cus_id')) != -1)
        {
            return $customer;
        }*/
    }

    public function store($input)
    {
        $cusId = DB::table($this->cusTable)->insertGetId([
            'cus_name'              => $input['cus_name'],
            'cus_code'              => $input['cus_code'],
            'cus_status'            => $input['cus_status'],
            'cus_billing_account'   => $input['cus_billing_account'],
            'cus_country_id'        => $input['cus_country_id'],
            'cus_des'               => $input['cus_des'],
            'created_at'            => time(),
            'created_by'            => $this->curUserId,
            'updated_at'            => time(),
            'updated_by'            => $this->curUserId,
            'deleted_at'            => 915148800,
            'deleted'               => 0,
        ]);
            
        foreach($input['addresses'] as $address) {
            DB::table($this->cusAddressTable)->insert([
                "cus_add_cus_id"        => $cusId,
                "cus_add_line_1"        => $address['cus_add_line_1'],
                "cus_add_country_id"    => $address['cus_add_country_id'],
                "cus_add_type"          => $address['cus_add_type'],
                "cus_add_city_name"     => $address['cus_add_city_name'],
                "cus_add_line_2"        => $address['cus_add_line_2'],
                "cus_add_state_id"      => $address['cus_add_state_id'],
                "cus_add_postal_code"   => $address['cus_add_postal_code'],
                "created_at"            => time(),
                "created_by"            => $this->curUserId,
                "updated_at"            => time(),
                "updated_by"            => $this->curUserId,
                "deleted_at"            => 915148800,
                'deleted'               => 0,
            ]);
        }

        foreach ($input['contacts'] as $contact) {
            DB::table($this->cusContactTable)->insert([
                "cus_ctt_cus_id"    => $cusId,
                "cus_ctt_fname"     => $contact['cus_ctt_fname'],
                "cus_ctt_lname"     => $contact['cus_ctt_lname'],
                "cus_ctt_email"     => $contact['cus_ctt_email'],
                "cus_ctt_phone"     => $contact['cus_ctt_phone'],
                "cus_ctt_ext"       => $contact['cus_ctt_ext'],
                "cus_ctt_position"  => $contact['cus_ctt_position'],
                "cus_ctt_mobile"    => $contact['cus_ctt_mobile'],
                "created_at"        => time(),
                "created_by"        => $this->curUserId,
                "updated_at"        => time(),
                "updated_by"        => $this->curUserId,
                "deleted_at"        => 915148800,
                'deleted'           => 0,
            ]);
        }

        return $cusId;
    }

    public function getCustomerById($cusId)
    {
        return $this->cusModel->find($cusId);
    }
}
