<?php

namespace Seldat\Wms2\Models;

class VirtualCartonSummary extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vtl_ctn_sum';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'vtl_ctn_sum_id';

    /**
     * @var array
     */
    protected $fillable = [
        'asn_hdr_id',
        'asn_dtl_id',
        'cus_id',
        'whs_id',
        'item_id',
        'ctnr_id',
        'discrepancy',
        'vtl_ctn_sum_sts',
        'gate_code',
        'created_by',
        'updated_by'
    ];

    public function asnHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnHdr', 'asn_hdr_id', 'asn_hdr_id');
    }

    public function asnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }
}
