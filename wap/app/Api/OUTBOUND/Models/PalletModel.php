<?php

namespace App\Api\OUTBOUND\Models;

use DB;
use Seldat\Wms2\Models\Pallet;

class PalletModel extends AbstractModel
{
    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
    }

    public function updatePallet($pltInfo)
    {
        $pltId = array_get($pltInfo, 'plt_id', null);
        $created_at = array_get($pltInfo, 'created_at', 0);
        $zeroDt = time();

        // Calculate storage_duration
        if (is_string($created_at) || is_int($created_at)) {
            $created_at = (int)$created_at;
        } else {
            $created_at = $created_at->timestamp;
        }

        $storageDate = date("Y-m-d", $created_at);

        $pickedDate = date("Y-m-d");

        $dateDiff = (int)round(abs(strtotime($storageDate) - strtotime($pickedDate)) / 86400);

        if ($dateDiff == 0) {
            $storageDuration = 1;
        } else {
            $storageDuration = $dateDiff;
        }

        return $this->model
            ->where('plt_id', $pltId)
            ->update([
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'ctn_ttl'          => 0,
                'updated_at'       => time(),
                'zero_date'        => $zeroDt,
                'storage_duration' => $storageDuration
            ]);
    }

    public function updatePalletCtnTtl($locIds)
    {
        return $this->model
            ->whereIn('loc_id', $locIds)
            ->update([
                'ctn_ttl' => DB::raw("(SELECT COUNT(c.ctn_id) FROM cartons c WHERE pallet.plt_id = c.plt_id)")
            ]);
    }

    public function updateZeroPallet($locIds)
    {
        //$strSQL = sprintf("IF(DATEDIFF(NOW(), FROM_UNIXTIME(created_at)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        //(created_at)), 1)", date('Y-m-d'));

        return $this->model
            ->whereIn('loc_id', $locIds)
            ->where('ctn_ttl', 0)
            ->update([
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'is_movement'      => 0,
                'plt_sts'          => 'PD',
                'zero_date'        => time(),
                'storage_duration' => CartonModel::getCalculateStorageDurationRaw()
            ]);
    }

    public function getPalletIDWhereRFID($rfid)
    {
        return $this->model->where('rfid', $rfid)->value('plt_num');
    }

    public function getPalletMoveWhereRFID($rfid, $whsId)
    {
        return $this->model->where([
            'pallet.whs_id' => $whsId,
            'pallet.rfid'   => $rfid
        ])
            ->Join('location as l', 'l.loc_id', '=', 'pallet.loc_id')
            ->whereNotNull('pallet.loc_id')
            ->where('pallet.ctn_ttl', '>', 0)
            ->first();
    }

    public function getPalletPutBackWhereRFID($rfid, $whsId)
    {
        return $this->model->where([
            'whs_id'      => $whsId,
            'rfid'        => $rfid,
            'is_movement' => 1,
        ])
            ->whereNotNull('loc_id')
            ->where('ctn_ttl', '>', 0)
            ->first();
    }

    /**
     * @param $palletRFID
     * @param $whsId
     *
     * @return mixed
     */
    public function checkPalletPutBackWhereRFID($palletRFID, $whsId)
    {
        return $this->model->where([
            'whs_id' => $whsId,
            'rfid'   => $palletRFID
        ])
            //->whereNotNull('loc_id')
            ->orderBy('plt_id', 'desc')
            ->first();
    }

    public function getLocationIsContainPallet($locID)
    {
        return $this->model
            ->where('loc_id', $locID)
            ->where('plt_sts', 'AC')
            ->first();
    }

    public function updatePalletPutBackNewLoc(int $pltID, array $arrLoc)
    {
        return $this->model->where([
            'plt_id'      => $pltID,
            'is_movement' => 1,
        ])
            ->where('ctn_ttl', '>', 0)
            ->update(
                [
                    'loc_id'      => $arrLoc['loc_id'],
                    'loc_name'    => $arrLoc['loc_alternative_name'],
                    'loc_code'    => $arrLoc['loc_code'],
                    'is_movement' => 0,
                ]
            );
    }

    public function getPalletByGrHdrId($grHdrId)
    {
        return $this->model
            ->where('gr_hdr_id', $grHdrId)
            ->get();
    }

    /**
     * @param $pltRfid
     *
     * @return mixed
     */
    public function checkExistedPalletAccordingToRFID($pltRfid, $whsId)
    {
        $query = $this->model
            ->where('rfid', $pltRfid)
            // ->where('ctn_ttl', '>', 0)
            //->where('plt_sts', 'AC')
            ->where('whs_id', $whsId);
        $result = $query->first();

        return $result;
    }

    /**
     * @param $pltId
     *
     * @return mixed
     */
    public function getPalletById($pltId)
    {
        $query = $this->model
            ->where('plt_id', $pltId);
        $result = $query->first();

        return $result;
    }

    /**
     * @param $pltRfid
     * @param $whsId
     * @param $cus_ids
     *
     * @return mixed
     */
    public function createNewPallet($pltRfid, $whsId, $cus_id)
    {
        $pltNum = $this->createPalletNum();

        $palletData = [
            'whs_id'  => $whsId,
            'cus_id'  => $cus_id,
            'plt_num' => $pltNum,
            'rfid'    => $pltRfid,
            'ctn_ttl' => 0,
            'dmg_ttl' => 0,
        ];

        $this->refreshModel();
        $palletObj = $this->create($palletData);

        return $palletObj;
    }

    /**
     * @return string
     */
    public function createPalletNum()
    {
        $today_ddmm = date('dm');
        $palletCode = "LPN-" . $today_ddmm;
        //$palletCode = "LPN-1706";

        $countPallet = \DB::table('pallet')->select('plt_num')
            ->where('plt_num', 'like', $palletCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();
        $max = 0;
        $max2 = 0;
        if ($countPallet) {
            $pltCode = array_get($countPallet, 'plt_num', null);
            $arrMax = explode('-', $pltCode);
            $max = (int)end($arrMax);
            $max2 = $arrMax[2];
        }

        return $palletCode . "-" . str_pad($max2 + 1, 5, "0", STR_PAD_LEFT);
    }

    /**
     * @param $newPalletId
     * @param $ctnTtl
     *
     * @return mixed
     */
    public function updateCtnTtlForPallet($newPalletId, $ctnTtl)
    {
        $result = $this->model
            ->where('plt_id', $newPalletId)
            ->update([
                'ctn_ttl'      => $ctnTtl,
                'init_ctn_ttl' => $ctnTtl,
                'loc_id'       => null,
                'loc_code'     => null,
                'loc_name'     => null
            ]);

        return $result;
    }

    public function checkPltSameCustomer($whsId, $cusId, $pltId)
    {
        return $this->model
            ->where('plt_id', $pltId)
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->first();
    }


}
