<?php

namespace App\Modules\Inventories\Eloquents;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventory';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'item_id',
        'cus_id',
        'whs_code',
        'cus_code',
        'type',
        'upc',
        'des',
        'sku',
        'size',
        'color',
        'pack',
        'uom',
        'in_hand_qty',
        'in_pick_qty',
        'total_qty'
    ];
}
