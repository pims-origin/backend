<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\Status;

/**
 * Class PackViewDtlTransformer
 *
 * @package App\Api\V1\Transformers
 */
class PackViewDtlTransformer extends TransformerAbstract
{
    /**
     * @param PackHdr $packHdr
     *
     * @return array
     */
    public function transform(PackHdr $packHdr)
    {
        $pack_type = $packHdr->pack_type;
        $length = (float)object_get($packHdr, 'length', null);
        $width = (float)object_get($packHdr, 'width', null);
        $height = (float)object_get($packHdr, 'height', null);
        $weight = (float)object_get($packHdr, 'weight', null);

        $demension = !empty(object_get($packHdr, 'length', 0)) ? object_get($packHdr, 'length', 0). 'x' : '';
        $demension .= !empty(object_get($packHdr, 'width', 0)) ? object_get($packHdr, 'width', 0). 'x' : '';
        $demension .= !empty(object_get($packHdr, 'height', 0)) ? object_get($packHdr, 'height', 0) : '';

        return [
            'pack_type'       => $pack_type,
            'odr_hdr_id'      => $packHdr->odr_hdr_id,
            'pack_ref_id'     => $packHdr->pack_ref_id,
            'pack_ref'        => $packHdr->pack_ref_id,
            'length'          => $length,
            'width'           => $width,
            'height'          => $height,
            'weight'          => $weight,
            'dimension'       => sprintf("%sx%sx%s", $length, $width, $height),
            //'dimension'       => $demension,
            'sku_ttl'         => $packHdr->sku_ttl,
            'piece_ttl'       => $packHdr->piece_ttl,
            'pack_hdr_num'    => $packHdr->pack_hdr_num,
            'order_type_key'  => $packHdr->order_type_key,
            'order_type'      => Status::getByKey("Order-Type", $packHdr->order_type_key),
            'origin_odr_type' => $packHdr->origin_odr_type,
            'odr_num'         => $packHdr->odr_hdr_num,
            'item_id'         => $packHdr->item_id,
            'sku'             => $packHdr->sku,
            'lot'             => $packHdr->lot,
            'size'            => $packHdr->size,
            'color'           => $packHdr->color,
            'cus_upc'         => $packHdr->cus_upc,
            'pack'            => object_get($packHdr,'item.pack',''),
            'tracking_num'    => object_get($packHdr,'tracking_number',''),

            'items' => $packHdr->items,

        ];
    }
}
