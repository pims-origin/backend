<?php
namespace App\Api\V2\Outbound\Order\AssignCartonsWithBarcode\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderHdr;

class OrderHdrModel extends AbstractModel
{

    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }

    public function updatePKOdrHdr($wvHdrId)
    {
        $result = DB::table('odr_hdr as oh')
            ->where('oh.wv_id', $wvHdrId)
            ->update(['oh.odr_sts' => 'PK']);

        return $result;
    }

    public function updatePDOdrHdr($wvHdrId)
    {
        $result = DB::table('odr_hdr as oh')
            ->where('oh.wv_id', $wvHdrId)
            ->where('oh.odr_sts', 'PK')
            ->where(
                DB::raw("(
                    SELECT COUNT(1)
                    FROM `odr_dtl` as od
                    WHERE od.`odr_id` = oh.odr_id
                        AND od.`itm_sts` = 'PD'
                        AND od.deleted = 0
                )"),
                DB::raw("(
                    SELECT COUNT(1)
                    FROM `odr_dtl` as od
                    WHERE od.`odr_id` = oh.odr_id
                        AND od.deleted = 0
                )")
            )
            ->update(['oh.odr_sts' => 'PD']);

        return $result;
    }

    public function updatePAOdrHdr($odrHdrIds)
    {
        $result = DB::table('odr_hdr')
            ->whereIn('odr_id', $odrHdrIds)
            ->where('odr_sts', 'PD')
            ->update(['odr_sts' => 'PA']);

        return $result;
    }

    public function updateOrderPickedByWv($wvId)
    {
        $this->getModel()->where([
            'odr_sts' => 'PK',
            'wv_id'   => $wvId
        ])
            ->whereRaw("
                0 = (
                    SELECT count(*) from odr_dtl
                    WHERE odr_dtl.odr_id = odr_hdr.odr_id
                    AND itm_sts != 'PD'
                    AND alloc_qty > 0
                    AND odr_dtl.deleted = 0
                )
            ")
            ->update([
                // 'odr_sts' => DB::raw("IF(no_pack_bol = 1, 'ST', 'PD')"),
                'odr_sts' => 'PD',
                'sts'     => 'u'
            ]);
    }

    public function updateOrderPicked($odr_id)
    {
        $sql = "(SELECT COUNT(1) FROM odr_dtl WHERE odr_dtl.odr_id = odr_hdr.odr_id AND odr_dtl.itm_sts != 'PD' AND deleted =0  AND alloc_qty > 0) = 0";
        $result = $this->model
            ->where('odr_id', $odr_id)
            ->where('odr_sts', 'PK')
            ->whereRaw(DB::raw($sql))->update(["odr_sts" => 'PD']);
        return $result;

    }

}
