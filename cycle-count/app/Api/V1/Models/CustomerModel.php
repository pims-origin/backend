<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/11/2016
 * Time: 14:12
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\CustomerWarehouse;

class CustomerModel extends AbstractModel
{
    public function __construct($model = NULL)
    {
        $this->model = $model ? $model : new Customer();
    }

    /**
     * @param $cusIds
     */
    public function getCustomerInfo($cusIds, $select = NULL)
    {
        $result = [];

        if (! $cusIds) {
            return $result;
        }

        $select = $select ? $select : ['cus_id'];

        $cusIds = (array) $cusIds;

        $result = $this->model->select($select)
            ->whereIn('cus_id', $cusIds)
            ->get();

        return $result;
    }

    /**
     * @param $whsId
     * @param $cusIds
     *
     * @return mixed
     */
    public function getWarehouseHasCusIds($whsId, $cusIds)
    {
        $result = [];
        if (! ($whsId && $cusIds)) {
            return $result;
        }

        $cusIds = (array) $cusIds;

        $result = CustomerWarehouse::where('whs_id', $whsId)
            ->whereIn('cus_id', $cusIds)
            ->get();

        return $result;
    }
}