<?php

namespace App\Api\V3\Inbound\Location\Putaway\Models;


use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Utils\SelStr;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * EventTrackingModel constructor.
     */
    public function __construct()
    {
        $this->model = new EventTracking();
    }
}
