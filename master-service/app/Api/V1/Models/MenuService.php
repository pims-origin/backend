<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class MenuService extends BaseService
{
    const LEFT_MENU_ID = 1;
    const FAVOURITE_MENU_ID = 1;

    /**
     * AuthorizationService constructor.
     * @param $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_MENU');
        parent::__construct($request, $api);
    }

    /**
     * @return bool|null|\Psr\Http\Message\ResponseInterface
     */
    public function getLeftMenu()
    {
        try {
            // Get menu permission
            $menuPermissionUri = $this->getAuthorizedMenu();

            $menuPermissionResponse = $this->client->get($menuPermissionUri);
            $menuPermissionData = json_decode($menuPermissionResponse->getBody(), true);

            if ($menuPermissionResponse->getStatusCode() != 200
                || ! isset($menuPermissionData['data'])
                || empty($menuPermissionData['data'])
            ) {
                return false;
            }

            $menuPermission = $menuPermissionData['data'];
            if (isset($menuPermission['permission'])) {
                return false;
            }
            $queryMenuPermission = implode(',', $menuPermission);

            // Get all menu from Menu service
            $menuUri = $this->getMenuByGroupPermissionUri(self::LEFT_MENU_ID);
            $options['form_params'] = ['permission' => $queryMenuPermission];
            $menuResponse = $this->client->post($menuUri, $options);
            if ($menuResponse->getStatusCode() != 200) {
                return false;
            }
            return $menuResponse;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    /**
     * @param $groupId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getMenuItem($groupId)
    {
        $options = [];
        $uri = $this->getMenuByGroupUri($groupId);

        $response = $this->client->get($uri, $options);

        return $response;
    }

    /**
     * @param $userId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getFavouriteMenu($userId)
    {
        $options = [];
        $favouriteMenuUri = $this->getFavouriteMenuUri($userId);

        $menuResponse = $this->client->get($favouriteMenuUri, $options);

        return $menuResponse;
    }

    /**
     * @param IRequest $request
     * @param $userId
     * @return bool|null|\Psr\Http\Message\ResponseInterface
     */
    public function addFavouriteMenuItem(IRequest $request, $userId)
    {
        if (! $userId) {
            return false;
        }
        $body = $request->getParsedBody();
        $options = [
            'form_params' => $body,
        ];
        $favouriteMenuUri = $this->getFavouriteMenuUri($userId);
        try {
            $response = $this->client->post($favouriteMenuUri, $options);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function removeFavouriteMenuItem($id, $userId)
    {
        $favouriteMenuUri = $this->getFavouriteMenuUri($userId);
        $specificUri = $favouriteMenuUri . '/' . $id;
        try {
            $response = $this->client->delete($specificUri, []);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function getFullLeftMenu()
    {
        $uri = $this->getMenuByGroupUri(self::LEFT_MENU_ID);
        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function createMenuGroup(array $params)
    {
        $uri = $this->getMenuGroupInstanceUri();
        return $this->createMenu($uri, $params);
    }

    public function createMenuItem($groupId, array $params)
    {
        $uri = 'groups/' . $groupId . '/list_menu';

        return $this->createMenu($uri, $params);
    }

    public function createMenu($uri, array $params)
    {
        $options = [
            'form_params' => $params
        ];
        try {
            $response = $this->client->post($uri, $options);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    /** SESSION UTIL METHODS **/

    protected function getMenuGroupInstanceUri($groupId = '')
    {
        $uri = env('API_MENU_GROUP_URI') ?: false;
        if (! $uri) {
            return false;
            // throw Exception
        }
        // Transfer request
        if ($groupId == '') {
            return $uri;
        }
        $uri = $uri . '/' . $groupId;
        return $uri;
    }

    protected function getMenuByGroupUri($groupId)
    {
        $uri = env('API_MENU_BY_GROUP_URI') ?: false;
        if (! $uri) {
            return false;
            // throw Exception
        }
        $uri = str_replace('{groupId}', $groupId, $uri);
        return $uri;
    }

    protected function getMenuByGroupPermissionUri($groupId)
    {
        $uri = env('API_MENU_BY_PERMISSION') ?: false;
        if (! $uri) {
            return false;
            // throw Exception
        }
        $uri = str_replace('{groupId}', $groupId, $uri);
        return $uri;
    }

    public function getAuthorizedMenu()
    {
        if (empty(env('API_AUTHORIZATION')) || empty(env('MENU_AUTHORIZATION_URI'))) {
            return false;
        }
        $uri = env('API_AUTHORIZATION') . env('MENU_AUTHORIZATION_URI');
        return $uri;
    }

    public function getFavouriteMenuUri($userId)
    {
        $uri = env('API_MENU_FAVOURITE_URI') ?: false;
        if (! $uri) {
            return false;
        }
        $uri = str_replace('{userId}', $userId, $uri);
        return $uri;
    }

    public function getMenuList()
    {
        $uri = $this->getMenuGroupInstanceUri();
        try {
            $response = $this->client->get($uri, []);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }
}
