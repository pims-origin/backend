<?php

namespace App\Modules\Items\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Modules\Items\Resources\ItemResource;

class ItemCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => ItemResource::collection($this->collection)
        ];
    }
}