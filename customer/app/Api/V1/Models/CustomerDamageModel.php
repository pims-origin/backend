<?php

namespace App\Api\V1\Models;

use App\Api\V1\Traits\CusZoneCusWarehouseService;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Customer;
use Illuminate\Support\Facades\DB;

class CustomerDamageModel extends AbstractModel
{

    protected $model;

    public function __construct(Customer $model = null)
    {
        $this->model = ($model) ?: new Customer();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
    }

    public function getCustomerHaveDamagedCarton($userId, $input, $limit = 20)
    {
        $query = Carton::select(['customer.*', 'cus_in_user.whs_id'])
                ->where([
                    'is_damaged' => 1,
                    'cus_in_user.user_id' => $userId,
                    'customer.cus_status' => 'AC',
                    'customer.deleted' => 0,
                ])
                ->join('customer', 'customer.cus_id', '=', 'cartons.cus_id')
                ->join('cus_in_user', 'cus_in_user.cus_id', '=', 'customer.cus_id')
                ->join('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'cartons.gr_hdr_id')
                ->join('container', 'container.ctnr_id', '=', 'cartons.ctnr_id')
                ->join('damage_carton', 'damage_carton.ctn_id', '=', 'cartons.ctn_id')
                ->join('damage_type', 'damage_type.dmg_id', '=', 'damage_carton.dmg_id')
                ->join('item', 'item.item_id', '=', 'cartons.item_id')
                ->groupBy('cartons.cus_id')
                ->orderBy('cus_name');
        $whsId = array_get($input, 'whs_id', null);
        if($whsId) {
            $query->where('cus_in_user.whs_id', $whsId);
        }

        return $query->paginate($limit);
    }

}
