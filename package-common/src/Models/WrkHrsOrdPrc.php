<?php

namespace Seldat\Wms2\Models;

class WrkHrsOrdPrc extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wrk_hrs_ord_prc';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'ord_num',
        'dt',
        'type',
        'amount',
        'cat',
        'inv_sts'
    ];

    
    public function OrderHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\OrderHdr', 'ord_num', 'ord_num');
    }
    
    public function invoiceHeader()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceHeader', 'inv_id', 'inv_id');
    }
}

