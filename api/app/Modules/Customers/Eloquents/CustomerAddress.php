<?php

namespace App\Modules\Customers\Eloquents;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_address';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cus_add_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_add_cus_id',
        'cus_add_line_1',
        'cus_add_line_2',
        'cus_add_country_id',
        'cus_add_city_name',
        'cus_add_state_id',
        'cus_add_postal_code',
        'cus_add_type',
    ];

    public function systemState()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemState', 'cus_add_state_id', 'sys_state_id');
    }

    public function systemCountry()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemCountry', 'cus_add_country_id', 'sys_country_id');
    }
}
