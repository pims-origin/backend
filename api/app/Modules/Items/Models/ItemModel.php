<?php

namespace App\Modules\Items\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Modules\Items\Eloquents\User;
use App\Modules\Items\Eloquents\Customer;
use App\Modules\Items\Eloquents\Item;
use App\Modules\Items\Eloquents\CusExUser;
use App\Modules\Items\Eloquents\ItemMeta;

class ItemModel extends AbstractModel
{
    const STATUS_NEW = 'NW';
    const STATUS_RG = 'RG';
    const ITEM_QUALIFIER = 945;

    protected $cusOfUser;
    protected $curUserId;

    protected $cusModel;
    protected $userModel;
    protected $itemModel;
    protected $cusExUsModel;
    protected $itemMetaModel;

    public function __construct(
        Customer $cusModel,
        User $userModel,
        Item $itemModel,
        CusExUser $cusExUsModel,
        ItemMeta $itemMetaModel
    )
    {
        $this->cusOfUser        = Config::get('data.cusOfUser');
        $this->curUserId        = Config::get('data.userId');

        $this->cusModel         = $cusModel ? $cusModel : new Customer;
        $this->userModel        = $userModel ? $userModel : new User;
        $this->itemModel        = $itemModel ? $itemModel : new Item;
        $this->cusExUsModel     = $cusExUsModel ? $cusExUsModel : new CusExUser;
        $this->itemMetaModel    = $itemMetaModel ? $itemMetaModel : new ItemMeta;
    }

    public function getListItemByCus($cusId, $limit = 20, $baseUrl)
    {
        $items = $this->itemModel->where('deleted', 0)->where('cus_id', $cusId);

        foreach ($this->cusOfUser as $data) {
            if ($data->cus_id == $cusId)
            {
                return $items->paginate($limit)->withPath($baseUrl);
            }
        }
    }

    public function getItemDetail($itemId, $cusId)
    {
        $item = $this->itemModel->where('deleted', 0)
            ->where('cus_id', $cusId)
            ->find($itemId);

        foreach ($this->cusOfUser as $data) {
            if ($data->cus_id == $cusId)
            {
                return $item;
            }
        }
    }

    public function createItem($cusId, $input)
    {
        $check = false;
        foreach ($this->cusOfUser as $data) {
            if ($data->cus_id == $cusId)
            {
                $check = true;
                break;
            }
        }

        if ($check)
        {
            DB::beginTransaction();
            
            $item = $this->itemModel;

            $item->cus_id           = $cusId;
            $item->sku              = array_get($input, 'sku');
            $item->size             = array_get($input, 'size', 'NA');
            $item->color            = array_get($input, 'color', 'NA');
            $item->description      = array_get($input, 'description', NULL);
            $item->uom_code         = array_get($input, 'uom');
            $uomInfo                = $this->getUomByUomCode($item->uom_code);
            $item->uom_id           = object_get($uomInfo, 'sys_uom_id', NULL);
            $item->uom_name         = object_get($uomInfo, 'sys_uom_name', NULL);
            $item->pack             = array_get($input, 'pack');
            $item->cus_upc          = array_get($input, 'upc', NULL);
            $item->length           = array_get($input, 'length');
            $item->width            = array_get($input, 'width');
            $item->height           = array_get($input, 'height');
            $item->net_weight       = array_get($input, 'net_weight');
            $item->weight           = array_get($input, 'weight');
            $item->volume           = array_get($input, 'volume');
            $item->cat_name         = array_get($input, 'category', 'NA');
            $item->status           = array_get($input, 'status', self::STATUS_RG);
            $item->dim_uom          = array_get($input, 'dim_uom', 'IN');
            $item->weight_uom       = array_get($input, 'weight_uom', 'LB');
            $item->spc_hdl_name     = array_get($input, 'special_handling', 'NA');
            $item->created_at       = time();
            $item->updated_at       = time();
            $item->created_by       = $this->curUserId;
            $item->updated_by       = $this->curUserId;
            $item->deleted_at       = 915148800;
            $item->deleted          = 0;

            $item->save();

            // $this->createItemMeta($item->toArray(), $input);
            
            DB::commit();

            return $item;
        }
    }

    public function createItemMeta($data, $input)
    {
        $itemMeta = $this->itemMetaModel;

        $itemMeta->qualifier  = self::ITEM_QUALIFIER;
        $itemMeta->itm_id     = array_get($data, 'item_id');
        $itemMeta->value      = json_encode(array_get($input, 'meta', NULL));
        $itemMeta->created_at = time();
        $itemMeta->updated_at = time();
        $itemMeta->created_by = $this->curUserId;
        $itemMeta->updated_by = $this->curUserId;
        $itemMeta->deleted_at = 915148800;
        $itemMeta->deleted    = 0;

        return $itemMeta->save();
    }

    public function updateItem($cusId, $itemId, $input)
    {
        $check = false;
        foreach ($this->cusOfUser as $data) {
            if ($data->cus_id == $cusId)
            {
                $check = true;
                break;
            }
        }

        if ($check)
        {
            DB::beginTransaction();
            
            $item = $this->itemModel->find($itemId);

            if ($item)
            {
                $item->cus_id           = $cusId;
                $item->sku              = array_get($input, 'sku');
                $item->size             = array_get($input, 'size', 'NA');
                $item->color            = array_get($input, 'color', 'NA');
                $item->description      = array_get($input, 'description', NULL);
                $item->uom_code         = array_get($input, 'uom');
                $uomInfo                = $this->getUomByUomCode($item->uom_code);
                $item->uom_id           = object_get($uomInfo, 'sys_uom_id', NULL);
                $item->uom_name         = object_get($uomInfo, 'sys_uom_name', NULL);
                $item->pack             = array_get($input, 'pack');
                $item->cus_upc          = array_get($input, 'upc', NULL);
                $item->length           = array_get($input, 'length');
                $item->width            = array_get($input, 'width');
                $item->height           = array_get($input, 'height');
                $item->net_weight       = array_get($input, 'net_weight');
                $item->weight           = array_get($input, 'gross_weight');
                $item->cat_name         = array_get($input, 'category', 'NA');
                $item->status           = array_get($input, 'status', self::STATUS_RG);
                $item->dim_uom          = array_get($input, 'dim_uom', 'IN');
                $item->weight_uom       = array_get($input, 'weight_uom', 'LB');
                $item->spc_hdl_name     = array_get($input, 'special_handling', 'NA');
                $item->updated_at       = time();
                $item->updated_by       = $this->curUserId;

                $item->save();
                
                DB::commit();

                return $item;
            }

        }
    }
}
