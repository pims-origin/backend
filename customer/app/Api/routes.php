<?php
$app->get('/', function () use ($app) {
    return $app->version();
});
// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
    $middleware[] = 'setWarehouseTimezone';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {
        $api->get('/', function () {
            return ['Seldat API V1'];
        });

        // customer
        $api->post('/customers', ['action' => "createCustomer", 'uses' => 'CustomerController@storeWhole']);
        $api->get('/customers', ['action' => "viewCustomer", 'uses' => 'CustomerController@search']);
        $api->get('/customers-all', [
            'action' => "viewCustomer",
            'as'     => 'allCustomer',
            'uses'   =>
                'CustomerController@search'
        ]);
        //generateColor
        $api->get('/customers-generate-color', [
            'action' => "viewCustomer",
            'as'     => 'allCustomer',
            'uses'   =>
                'CustomerController@generateColor'
        ]);
//        $api->get('/customers-user', [
//            'action' => "viewCustomer",
//            'as'     => 'userCustomer',
//            'uses'   =>
//                'CustomerController@search'
//        ]);
        $api->get('/customers-user', [
            'action' => "viewCustomer",
            'as'     => 'userCustomer',
            'uses'   =>
                'CustomerController@dropDown'
        ]);
        $api->get('/customers/export', ['action' => "viewCustomer", 'uses' => 'CustomerController@export']);
        $api->get('/customers/{customerId}', ['action' => "viewCustomer", 'uses' => 'CustomerController@showWhole']);
        $api->put('/customers/{customerId}', ['action' => "editCustomer", 'uses' => 'CustomerController@updateWhole']);

        //Customer Warehouse
        $api->get('/customer-warehouses',
            ['action' => "viewCustomer", 'uses' => 'CustomerWarehouseController@loadBy']);
        $api->get('/customers-wh',
            ['action' => "viewWarehouse", 'as' => 'cusWH', 'uses' => 'CustomerWarehouseController@loadBy']);

        $api->get('/customers/{customerId}/warehouses', [
            'action' => "viewWarehouse",
            'uses'   => 'CustomerWarehouseController@search'
        ]);
        $api->post('/customers/{customerId}/warehouses', [
            'action' => "createWarehouse",
            'uses'   => 'CustomerWarehouseController@store'
        ]);
        $api->delete('/customers/{customerId}/warehouses/{warehouseId}', [
            'action' => "deleteWarehouse",
            'uses'   => 'CustomerWarehouseController@destroy'
        ]);

        //Customer Zone
        $api->get('/customer-zones/{zoneId}', [
            'action' => "viewZone",
            'uses'   => 'CustomerZoneController@loadCustomerByZone'
        ]);
        $api->get('/customers/{customerId}/zones/{zoneId}', [
            'action' => "viewZone",
            'uses'   => 'CustomerZoneController@show'
        ]);
        $api->get('/customers/{customerId}/zones', [
            'action' => "viewZone",
            'uses'   => 'CustomerZoneController@index'
        ]);
        $api->post('/customers/{customerId}/zones', [
            'action' => "createZone",
            'uses'   => 'CustomerZoneController@store'
        ]);
        $api->delete('/customers/{customerId}/zones/{zoneId}', [
            'action' => "deleteZone",
            'uses'   => 'CustomerZoneController@destroy'
        ]);
        $api->post(
            '/customers/{customerId}/zones/save-mass-and-delete-unused', [
                'action' => "createZone",
                'uses'   =>
                    'CustomerZoneController@saveMassAndDeleteUnused'
            ]
        );

        //Customer Location
        $api->get('/customers/{customerId}/locations', [
            'action' => "viewLocation",
            'uses'   => 'CustomerLocationController@search'
        ]);

        // Customer Address
        $api->get('/customers/{customerId}/addresses', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerAddressController@index'
        ]);
        $api->post('/customers/{customerId}/addresses', [
            'action' => "createCustomer",
            'uses'   => 'CustomerAddressController@store'
        ]);
        $api->get('/customers/{customerId}/addresses/{addressId}', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerAddressController@show'
        ]);
        $api->put('/customers/{customerId}/addresses/{addressId}', [
            'action' => "editCustomer",
            'uses'   => 'CustomerAddressController@update'
        ]);
        $api->delete('/customers/{customerId}/addresses/{addressId}', [
            'action' => "deleteCustomer",
            'uses'   => 'CustomerAddressController@destroy'
        ]);

        // Customer Contact
        $api->get('/customers/{customerId}/contacts', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerContactController@index'
        ]);
        $api->post('/customers/{customerId}/contacts', [
            'action' => "createCustomer",
            'uses'   => 'CustomerContactController@store'
        ]);
        $api->get('/customers/{customerId}/contacts/{contactId}', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerContactController@show'
        ]);
        $api->put('/customers/{customerId}/contacts/{contactId}', [
            'action' => "editCustomer",
            'uses'   => 'CustomerContactController@update'
        ]);
        $api->delete('/customers/{customerId}/contacts/{contactId}', [
            'action' => "deleteCustomer",
            'uses'   => 'CustomerContactController@destroy'
        ]);
        $api->delete('/customers/{customerId}/contacts', [
            'action' => "deleteCustomer",
            'uses'   => 'CustomerContactController@deleteMass'
        ]);
        $api->post('/customers/{customerId}/contacts/save-mass-and-delete-unused', [
            'action' => "createCustomer",
            'uses'   => 'CustomerContactController@saveMassAndDeleteUnused'
        ]);

        // Charge Code
        $api->get('/charge-codes', ['action' => "viewChargeCode", 'uses' => 'ChargeCodeController@search']);
        $api->get('/charge-codes/{chargeCodeId}', [
            'action' => "viewChargeCode",
            'uses'   => 'ChargeCodeController@show'
        ]);
        $api->post('/charge-codes', ['action' => "createChargeCode", 'uses' => 'ChargeCodeController@store']);
        $api->put('/charge-codes/{chargeCodeId}', [
            'action' => "editChargeCode",
            'uses'   => 'ChargeCodeController@update'
        ]);
        $api->delete('/charge-codes/{chargeCodeId}', [
            'action' => "deleteChargeCode",
            'uses'   => 'ChargeCodeController@destroy'
        ]);
        $api->delete('/charge-codes', ['action' => "deleteChargeCode", 'uses' => 'ChargeCodeController@deleteMass']);

        // Charge Uom
        $api->get('/system-uoms', ['action' => "viewUOM", 'uses' => 'SystemUomController@search']);
        $api->get('/system-uoms/list-all', ['action' => "viewUOM", 'uses' => 'SystemUomController@index']);
        $api->get('/system-uoms/{systemUomId}', ['action' => "viewUOM", 'uses' => 'SystemUomController@show']);
        $api->post('/system-uoms', ['action' => "createUOM", 'uses' => 'SystemUomController@store']);
        $api->put('/system-uoms/{systemUomId}', ['action' => "editUOM", 'uses' => 'SystemUomController@update']);
        $api->delete('/system-uoms/{systemUomId}', ['action' => "deleteUOM", 'uses' => 'SystemUomController@destroy']);
        $api->delete('/system-uoms', ['action' => "deleteUOM", 'uses' => 'SystemUomController@deleteMass']);

        // Charge Type
        $api->get('/charge-types', ['action' => "viewChargeType", 'uses' => 'ChargeTypeController@search']);
        $api->get('/charge-types/list-all', ['action' => "viewChargeType", 'uses' => 'ChargeTypeController@index']);
        $api->get('/charge-types/{chargeTypeId}', [
            'action' => "viewChargeType",
            'uses'   => 'ChargeTypeController@show'
        ]);
        $api->post('/charge-types', ['action' => "createChargeType", 'uses' => 'ChargeTypeController@store']);
        $api->put('/charge-types/{chargeTypeId}', [
            'action' => "editChargeType",
            'uses'   => 'ChargeTypeController@update'
        ]);
        $api->delete('/charge-types/{chargeTypeId}', [
            'action' => "deleteChargeType",
            'uses'   => 'ChargeTypeController@destroy'
        ]);
        $api->delete('/charge-types', ['action' => "deleteChargeType", 'uses' => 'ChargeTypeController@deleteMass']);

        // Customer Charge Detail
        $api->post('/customers/{customerId}/charge-details', [
            'action' => "createCustomer",
            'uses'   => 'CustomerChargeDetailController@saveMass'
        ]);

        //order flow
        $api->get('/customers/order-flow/{customerId}', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerMetaController@getOrderFlow'
        ]);
        $api->put('/customers/order-flow/{customerId}', [
            'action' => "editCustomer",
            'uses'   => 'CustomerMetaController@update'
        ]);
        $api->post('/customers/order-flow/{customerId}', [
            'action' => "createCustomer",
            'uses'   => 'CustomerMetaController@store'
        ]);

        //Inbound config in customer meta
        $api->get('/customers/{customerId}/inbound-config', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerMetaController@getInboundConfig'
        ]);
        $api->put('/customers/{customerId}/inbound-config', [
            'action' => "editCustomer",
            'uses'   => 'CustomerMetaController@updateInboundConfig'
        ]);
        $api->post('/customers/{customerId}/inbound-config', [
            'action' => "createCustomer",
            'uses'   => 'CustomerMetaController@storeInboundConfig'
        ]);

        //user csr or pkr
        $api->get('/customers/user-meta/{customerId}', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerMetaController@getUserMeta'
        ]);

        $api->get('/customers/user-of-customer/{customerId}', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerInUserController@getUser'
        ]);

        $api->put('/customers/user-of-customer/{customerId}', [
            'action' => "editCustomer",
            'uses'   => 'CustomerInUserController@update'
        ]);

        $api->post('/customers/user-of-customer/{customerId}', [
            'action' => "createCustomer",
            'uses'   => 'CustomerInUserController@store'
        ]);

        //edi
        $api->get('/customer-edi', [
            'action' => 'viewCustomer',
            'uses'   => 'CustomerConfigController@getCustomerEdi'
        ]);

        $api->get('/{customerId}/customer-email', [
            'action' => 'viewCustomer',
            'uses'   => 'CustomerConfigController@getCustomerEmail'
        ]);
        $api->post('/{customerId}/customer-email', [
            'action' => 'viewCustomer',
            'uses'   => 'CustomerConfigController@updateCustomerEmail'
        ]);

        $api->get('/customer-damage',
            ['action' => "viewCustomer", 'uses' => 'CustomerController@customerDamage']);

        //Warehouse to customer
        $api->get('/customers/{customerId}/whs-not-in-cus', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerWarehouseController@listWhsNotInCus'
        ]);
        $api->post('/customers/{customerId}/whs-to-cus', [
            'action' => "viewCustomer",
            'uses'   => 'CustomerWarehouseController@whsToCus'
        ]);

        $api->post('/customers/carrier-service/{cusId}', [
            'action' => "createCustomer",
            'uses'   => "CustomerMetaController@createCarrierService"
        ]);

        $api->get('/customers/carrier-service/{cusId}', [
            'action' => "viewCustomer",
            'uses'   => "CustomerMetaController@getCarrierService"
        ]);
    });
});
