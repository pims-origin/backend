<?php

namespace App\Api\V1\Models;

use App\InvoiceDetail;
use App\InvoiceHeader;
use App\BillableDtl;
use App\BillableHdr;
use Seldat\Wms2\Models\CustomerMeta;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Api\V1\Models\CustomerSac;

class PopulateInvoiceModel extends AbstractModel
{
    protected $invoiceDetail;
    protected $invoiceHeader;
    protected $billableDetail;
    protected $billableHeader;
    protected $period;
    protected $logMessage = 'Auto create Invoice successful.';

    public function __construct()
    {
        config(['services.cron' => true]);
        $this->invoiceDetail = new InvoiceDetail;
        $this->invoiceHeader = new InvoiceHeader;
        $this->billableDetail = new BillableDtl;
        $this->billableHeader = new BillableHdr;
    }

    public function createMonthly()
    {
        $this->generateInvoiceBy('month');       
    }

    public function createWeekly()
    {
        $this->generateInvoiceBy('week');
    }

    public function createHourly()
    {
        $this->generateInvoiceBy('hour');
    }

    public function generateInvoiceBy($period)
    {
        $this->period = $period;
        $records = [];
        $headers = [];
        if ($period == 'hour') {
            $period = 'week';
        }

        $customers = $this->getGroupCustomerBy($period);
        
        if ($customers) {
            foreach ($customers as $customer) {
                $records = $this->getBillaleByCusId($customer);
            }
        } 

        if ($records) {
            foreach ($records as $key => $value) {
                $key = explode('-', $key);
                $headers[] = $this->prepareHeader($key[0], $key[1], $value);
            }
        } else {
            $this->logMessage = 'There is any valid billable item for generating invoice';
        }

        try {
            DB::transaction(function() use($headers, $records){
                foreach ($headers as $headerData) {
                    $header = $this->invoiceHeader->create($headerData);
                    if ($header) {
                        $key = $header->cus_id . '-' . $header->whs_id; 
                        $header->detail()->createMany($records[$key]);

                        return $header;
                    }
                }
            });
            
            Log::info($this->logMessage);
        } catch(\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function getGroupCustomerBy($config)
    {
        $config = ($config == 'month') ? 1 : 2;
        $data = [];
        $customers = CustomerMeta::where('qualifier', 'CIO')->get();
        if($customers->count()) {
            foreach ($customers as $customer) {
                $value = json_decode($customer->value);
                if (data_get($value, 'create_option', 0) == $config) {
                    $data[] = $customer->cus_id;
                }                
            }
        }

        return $data;
    }

    public function getBillaleByCusId($cusId)
    {
        $details = [];
        $dateRange = $this->getDateRange();
        $bills = BillableDtl::where('cus_id', $cusId)
            ->where('deleted', 0)
            ->whereBetween('created_at',$dateRange)
            ->withoutGlobalScopes()
            ->get();

        $overlaps = $this->overlapSac($cusId);
        $fillterBills = $bills->filter(function ($item, $key) use ($overlaps) {
            return $overlaps[$item->whs_id][$item->ba_type] < 2;
        });

        foreach ($fillterBills as $bill) {
            $key = $bill->cus_id . '-' . $bill->whs_id;
            $details[$key][] = $this->prepareDetail($bill);
        }

        return $details ?? false;
    }

    public function getDateRange()
    {
        if ($this->period == 'week') {
            $firstDate = strtotime("last monday midnight");
            $lastDate = strtotime("+1 week",$firstDate);
        } elseif ($this->period == 'month') {
            $firstDate = strtotime("first day of last month");
            $lastDate = strtotime('last day of last month');
        } else {
            $firstDate = time() - 24 * 60 * 60;
            $lastDate = time();
        }

        return [$firstDate, $lastDate];
    }

    public function prepareHeader($cusId, $whsId, $data)
    {
        $total = array_sum(data_get($data, '*.amount', 0));

        return [
            'inv_dt'    => time(),
            'inv_num'   => '',
            'subtotal' => $total,
            'discount'  => 0,
            'total'     => $total,
            'tax'       => 0,
            'due_dt'    => strtotime('last day of this month'),
            'whs_id'    => (int)$whsId,
            'cus_id'    => (int)$cusId,
            'inv_sts'   => 'PE'
        ];
    }

    public function prepareDetail(BillableDtl $detail)
    {
        return [
            'sac'        => $detail->sac,
            'ref_num'    => $detail->ref_num,
            'uom'        => $detail->sac_unit_uom,
            'unit_price' => $detail->unit_price,
            'qty'        => $detail->qty,
            'amount'     => $detail->amount,
            'currency'   => 'USD',
            'ba_id'      => $detail->ba_id,
            'ba_dtl_id'  => $detail->ba_dtl_id,
            'discount'   => $detail->discount,
            'is_charge'  => 1
        ];
    }

    public function overlapSac($cusId)
    {
        $sacs = CustomerSac::where([
            ['cus_id', '=', $cusId],
        ])->get(['whs_id', 'type', 'unit_uom', 'period_uom']);
        $overlapGroup = [
            'CTN' => 'QTY',
            'PC'  => 'QTY',
            'CUB' => 'VOLM'
        ];
        $groups = $sacs->groupBy('whs_id');
        $type = [];
        $groups->each(function ($group, $parentKey) use ($overlapGroup, &$type) {
            $type[$parentKey] = [];
            $group->each(function ($item, $key) use ($parentKey, $overlapGroup, &$type) {
                if (data_get($overlapGroup, $item->unit_uom, '') == $item->period_uom) {
                    ${$item->type} = data_get($type[$parentKey], $item->type, 0) ?? 1;  
                    $type[$parentKey][$item->type] = ++${$item->type}; 
                }
            });
        });

        return $type; // sac type has overlap group by whs is
    }
}