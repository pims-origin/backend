<?php

namespace console\controllers;

use console\rbac\AccessWarehouseAndCustomerRule;
use console\rbac\ManagerRule;
use console\rbac\UserRule;
use Yii;

class RbacController extends BaseRbacController
{
    protected $auth;

    public function init()
    {
        $this->auth = Yii::$app->authManager;
    }

    public function actionUp()
    {
        //create admin role
        $admin = $this->role('Admin', 'Admin role');

        //------------------------------Begin Permissions----------------------------
        $config = $this->getConfig();
        $i = 0;
        foreach ($config as $grpName => $data) {
            $addChildren = isset($data['children']) ? $data['children'] : false;
            $this->createGrpPerm($data['permissions'], $addChildren, $grpName, ['Admin'], $order = ++$i);
        }
        //------------------------------End Permissions----------------------------

        //add rule temp disable
        $accessWarehouseAndCustomerRule = new AccessWarehouseAndCustomerRule();
        $auth = Yii::$app->authManager;
        $this->addRule($accessWarehouseAndCustomerRule);
    }

    //------------------------------------Group Configuration-------------------------------
    protected function getConfig()
    {
        return [
            "Menu Mangement" => [
                "permissions" => [
                    'viewLeftMenu'                       => 'View Left Menu',
                    'viewMenu'                           => 'View Menu',
                    'createMenu'                         => 'Create Menu',
                    'viewMenuConfiguration'              => 'View Menu Configuration',
                    'viewMenuWarehouse'                  => 'View Menu Warehouse',
                    'viewMenuItem'                       => 'view Menu Item',
                    'viewMenuGoodsReceipt'               => 'view Menu Goods Receipt',
                    'viewMenuDashboard'                  => 'view Menu Dashboard',
                    'viewMenuReceiving'                  => 'view Menu Receiving',
                    'viewMenuSystemConfiguration'        => 'view Menu SystemConfiguration',
                    'viewMenuOrder'                      => 'view Menu Order',
                    'viewMenuShipping'                   => 'view Menu Shipping',
                    'viewMenuRole'                       => 'view Menu Role',
                    "viewMenuUser"                       => "View Menu User Management",
                    "viewMenuZoneType"                   => "View Menu Zone Type",
                    "viewMenuLocationType"               => "View Menu Location Type",
                    "viewMenuUOM"                        => "View Menu UOM",
//                    "viewMenuChargeType"             => "View Menu Charge Type",
//                    "viewMenuChargeCode"             => "View Menu Charge Code",
                    "viewMenuDamageType"                 => "View Menu Damage Type",
                    "viewMenuCountry"                    => "View Menu Country",
                    "viewMenuState"                      => "View Menu State",
                    "viewMenuLocation"                   => "View Menu Location",
                    "viewMenuZone"                       => "View Menu Zone",
                    "viewMenuItemMaster"                 => "View Menu Item Master",
                    "viewMenuCustomer"                   => "View Menu Customer",
                    "viewMenuASN"                        => "View Menu ASN",
                    "viewMenuPutaway"                    => "View Menu Putaway",
                    "viewMenuPutawayIntentoryUpdate"     => "View Menu Putaway Inventory Update",
                    "viewMenuInboundEventTracking"       => "View Menu Inbound Event Tracking",
                    "viewMenuOutboundEventTracking"      => "View Menu Outbound Event Tracking",
                    "viewMenuRelocation"                 => "View Menu Relocation",
                    "viewMenuConsolidation"              => "View Menu Consolidation",
                    "viewMenuAssignCSR"                  => "View Menu Assign CSR",
                    "viewMenuAllocateOrder"              => "View Menu Allocate Order",
                    "viewMenuWavePick"                   => "View Menu Wave Pick",
                    "viewMenuOrderPicking"               => "View Menu Order Picking",
                    "viewMenuReport"                     => "View Menu Report",
                    "viewMenuWorkOrder"                  => "View Menu Work Order",
                    "viewMenuOrderPacking"               => "View Menu Order Packing",
                    "viewMenuPalletAssignment"           => "View Menu Pallet Assignment",
                    "viewMenuCycleCount"                 => "View Menu Cycle Count",
                    "viewMenuCycleCountNotification"     => "View Menu Cycle Count Notification",
                    "viewMenuXFER"                       => "View Menu XFER",
                    "viewMenuBOL"                        => "View Menu BOL",
                    "viewMenuXferInv"                    => "View Menu Xfer Inv",
                    "viewMenuWarehouseLayout"            => "View Menu Warehouse Layout",
//                    "viewMenuInvoice"                => "View Menu Invoice",
                    "viewMenuConfigMenu"                 => "View Menu Config Menu",
                    "viewMenuBlockStock"                 => "View Menu Block Stock",
                    "viewMenuBlockReason"                => "View Menu Block Reason",
                    "viewMenuDeliveryService"            => "View Menu Delivery Service",

                    //Master Data (hidden group)
                    "viewMenuMasterDataType"             => "View Menu Master Data Type",
                    "viewMasterDataType"                 => "View Master Data Type",
                    "createMasterDataType"               => "Create Master Data Type",
                    "editMasterDataType"                 => "Edit Master Data Type",
                    //----------------------------------------------
                    "viewMenuMasterDataList"             => "view Menu Master Data List",
                    "viewMasterDataList"                 => "view Master Data List",
                    "createMasterDataList"               => "create Master Data List",
                    "editMasterDataList"                 => "edit Master Data List",
                    //----------------------------------------------
                    "viewMenuTruckType"                  => "view Menu Truck Type",
                    "viewTruckType"                      => "view Truck Type",
                    "createTruckType"                    => "create Truck Type",
                    "editTruckType"                      => "edit Truck Type",
                    //----------------------------------------------
                    "viewMenuTransporter"                => "view Menu Transporter",
                    "viewTransporter"                    => "view Transporter",
                    "createTransporter"                  => "create Transporter",
                    "editTransporter"                    => "edit Transporter",

                    // Online Order
                    'viewMenuEcomOrder'                  => 'view Menu Ecom Order',
                    'viewMenuEcomShipping'               => 'view Menu Ecom Shipping',
                    "viewMenuEcomAssignCSR"              => "View Menu Ecom Assign CSR",
                    "viewMenuAllocateEcomOrder"          => "View Menu Ecom Allocate Order",
                    "viewMenuEcomWavePick"               => "View Menu Ecom Wave Pick",
                    "viewMenuEcomOrderPicking"           => "View Menu Ecom Order Picking",
                    "viewMenuEcomWorkOrder"              => "View Menu Ecom Work Order",
                    "viewMenuEcomOrderPacking"           => "View Menu Ecom Order Packing",
                    "viewMenuEcomPalletAssignment"       => "View Menu Ecom Pallet Assignment",
                    "viewMenuEcomWavepickAssignment"     => "view Menu Ecom WavepickAssignment",
                    "viewMenuEcomBOL"                    => "View Menu Ecom BOL",

                    //Other group
                    "assignPermissionToRole"             => "assign Permission To Role",
                    "userAssignAll"                      => "Assign Role, Warehouse, Customer To User",
                    "viewMenuRFGUN"                      => "View menu RFGUN",
                    "viewMenuPutawayAssignment"          => "view Menu PutawayAssignment",
                    "viewMenuOnlineOrder"                => "view Menu OnlineOrder",
                    "viewMenuWavepickAssignment"         => "view Menu WavepickAssignment",
                    "viewMenuPerformance"                => "view Menu Performance",
                    "viewMenuDepartment"                 => "view Menu Department",
                    "viewMenuPutbackOrder"               => "View Menu Put Back Order",
                    "viewMenuPutbackReceipt"             => "View Menu Put Back Receipt",
                    "viewMenuAssignPalletPutback"        => "View Menu Assign Pallet Putback",
                    "viewMenuUpdatePutback"              => "View Menu update Putback",
                    "viewMenuCommodity"                  => "View Menu Commodity",
                    "viewMenuCartonDimension"            => "View Menu CartonDimension",
                    "viewMenuEDI"                        => "View Menu EDI",
                    "viewMenuImportItem"                 => "View Menu Import Item",
                    "viewMenuImportCustomer"             => "View Menu Import Customer",
                    "viewMenuImportInventory"            => "View Menu Import Inventory",
                    "viewMenuImportASN"                  => "View Menu Import ASN",
                    "viewMenuImportOrder"                => "View Menu Import Order",
                    "viewMenuTransferPallet"             => "View Menu Transfer Pallet",
                    "viewMenuPalletReport"               => "view Menu Pallet Report",
                    "viewMenuInventoryReport"            => "view Menu Inventory Report",

                    //Invoice Master
                    'viewMenuBillableItems'              => 'view Menu Billable Items',
                    'viewMenuInvoiceList'                => 'view Menu Invoice List',
                    'viewMenuInvoiceReport'              => 'view Menu Invoice Report',
                    'viewMenuChargeCode'                 => 'view Menu Charge Code',
                    'viewMenuApprovalList'               => 'view Menu Approval List',
                    'viewMenuInvoiceSystemConfiguration' => 'view Menu Invoice System Configuration',
                    'viewMenuCustomerConfiguration'      => 'view Menu Customer Configuration',

                     //Gate Management
                    'viewMenuGateManagement'              => 'view Menu Gate Management',

                    //Master Data: replenishment Config
                    "viewMenuReplenishmentConfig"    => "View Menu Replenishment Configuration",

                ],
                "children"    => [
                    "viewMenuConfigMenu"   => ["viewMenu", "createMenu"],
                    //Master data
                    "viewMasterDataType"   => "viewMenuMasterDataType",
                    "createMasterDataType" => "viewMasterDataType",
                    "editMasterDataType"   => "viewMasterDataType",
                    //----------------------------------------------
                    "viewMasterDataList"   => "viewMenuMasterDataList",
                    "createMasterDataList" => "viewMasterDataList",
                    "editMasterDataList"   => "viewMasterDataList",
                    //----------------------------------------------
                    //----------------------------------------------
                    "viewTruckType"        => "viewMenuTruckType",
                    "createTruckType"      => ["viewTruckType", "createMasterDataType", "createMasterDataList"],
                    "editTruckType"        => ["viewTruckType", "editMasterDataType", "editMasterDataList"],
                    //----------------------------------------------
                    "viewTransporter"      => "viewMenuTransporter",
                    "createTransporter"    => ["viewTransporter", "createMasterDataType", "createMasterDataList"],
                    "editTransporter"      => ["viewMenuTransporter", "editMasterDataType", "editMasterDataList"],

                ]
            ],

            "ASN Management" => [
                "permissions" => [
                    'createASN' => 'create ASN',
                    'editASN'   => 'edit ASN',
                    'viewASN'   => 'view ASN',
                    'cancelASN' => 'cancel ASN',
                    'completeReceiving' => 'Complete Receiving',
                    'revertToReceiving' => 'Revert to Receiving'
                ],
                "children"    => [
                    "viewASN"   => ["viewMenuASN"],
                    "createASN" => ["viewASN", "viewItem"],
                    "editASN"   => ["viewASN", "viewItem"],
                    "cancelASN" => ["viewASN"],
                    "completeReceiving" => ["viewASN"],
                    "revertToReceiving" => ["viewASN"]
                ]
            ],
            "Putaway"        => [
                "permissions" => [
                    "viewPutaway"       => "viewMenuPutaway",
                    "updateInventory"   => "viewMenuPutawayIntentoryUpdate",
                    "putawayAssignment" => "putawayAssignment"
                ],
                "children"    => [
                    "viewPutaway"         => ["viewMenuPutaway"],
                    "updateInventory"     => ["configSystem"],
                    "putawayAssignment"   => ["viewMenuPutawayAssignment"],
                    "saveAndPrintPutaway" => ["viewPutaway"]
                ]
            ],
            "Goods Receipt"  => [
                "permissions" => [
                    'createGoodsReceipt'     => 'create Goods Receipt',
                    //'editGoodsReceipt'    => 'edit Goods Receipt',
                    'viewGoodsReceipt'       => 'view Goods Receipt',
                    'saveAndPrintPutaway'    => 'save And Print PutAway List',
                    'createGoodReceiptOnWeb' => 'create Goods Receipt on web'

                ],
                "children"    => [
                    "viewGoodsReceipt"       => ["viewMenuGoodsReceipt"],
                    "createGoodsReceipt"     => ["viewGoodsReceipt", "viewASN", "saveAndPrintPutaway"],
                    'createGoodReceiptOnWeb' => ["viewMenuPutawayIntentoryUpdate", "viewMenu", "viewMenuPutaway"]
                    // "editGoodsReceipt"   => ["viewGoodsReceipt"]
                ]
            ],


            "Commodity" => [
                "permissions" => [
                    'viewCommodity'   => 'view Commodity',
                    'createCommodity' => 'create Commodity',
                    'editCommodity'   => 'edit Commodity'
                ],
                "children"    => [
                    "viewCommodity"   => "viewMenuCommodity",
                    'createCommodity' => 'viewCommodity',
                    'editCommodity'   => 'viewCommodity'
                ]
            ],

            "CartonDimension" => [
                "permissions" => [
                    'viewCartonDimension'   => 'view CartonDimension',
                    'createCartonDimension' => 'create CartonDimension',
                    'editCartonDimension'   => 'edit CartonDimension'
                ],
                "children"    => [
                    "viewCartonDimension"   => "viewMenuCartonDimension",
                    'createCartonDimension' => 'viewCartonDimension',
                    'editCartonDimension'   => 'viewCartonDimension'
                ]
            ],
            "Outbound"        => [
                "permissions" => [
                    "viewOrder"                => "view Order List",
                    "viewOnlineOrder"          => "view Online Order",
                    "createOrder"              => "create Order",
                    "editOrder"                => "edit Order",
                    "assignCSR"                => "assign CSR",
                    "allocateOrder"            => "allocate Order",
                    "viewWavePick"             => "view Wave Pick",
                    "createWavePick"           => "create Wave Pick",
                    "wavepickAssignment"       => "wavepick Assignment",
                    "printWavePick"            => "print WavePick",
                    "updateOrderPicking"       => "update Order Picking",
                    "completePickingOrder"     => "complete Picking Order",
                    "cancelOrder"              => "cancel Order",
                    "holdOrder"                => "hold Order",
                    "viewWorkOrder"            => "view Work Order",
                    "editWorkOrder"            => "edit Work Order",
                    "editWorkOrderPrice"       => "edit Work Order Price",
                    "createWorkOrder"          => "create Work Order",
                    "deleteWorkOrder"          => "delete Work Order",
                    "orderShipping"            => "Order shipping",
                    "orderPacking"             => "order Packing",
                    "undoPackCartons"          => "unpack Cartons",
                    "palletAssignment"         => "Assign Carton To Pallet",
                    "importOnlineOrder"        => "Import Online Order",
                    "packingSlip"              => "packing Slip",
                    "viewBOL"                  => "view BOL",
                    "createBOL"                => "create BOL",
                    "editBOL"                  => "edit BOL",
                    'editOrderShipDate'        => "edit Order ShipDate",
                    "finalBOL"                 => "Final BOL",
                    "updateScheduleToShipDate" => "Update Schedule to Ship date",
                    "csrComment"               => "CSR Comment",
                    "whseComment"              => "WHSE comments",


                ],
                "children"    => [
                    "viewOrder"                => ["viewMenuOrder", "viewMasterDataList"],
                    "createOrder"              => ["viewOrder"],
                    "editOrder"                => ["viewOrder"],
                    "assignCSR"                => ["viewMenuAssignCSR", "viewOrder"],
                    "allocateOrder"            => ["viewMenuAllocateOrder", "viewOrder"],
                    "viewWavePick"             => ["viewMenuWavePick"],
                    "createWavePick"           => ["viewMenuWavePick", "viewOrder", "viewWavePick"],
                    "wavepickAssignment"       => ["viewMenuWavepickAssignment"],
                    "updateOrderPicking"       => ["viewMenuOrderPicking", "viewOrder"],
                    "completePickingOrder"     => ["viewMenuOrderPicking"],
                    "cancelOrder"              => ["viewOrder"],
                    "holdOrder"                => ["viewOrder"],
                    "viewWorkOrder"            => ["viewOrder", "viewMenuWorkOrder"],
                    "createWorkOrder"          => ["viewWorkOrder", "viewMasterDataList", "configSystem"],
                    "editWorkOrder"            => ["viewWorkOrder"],
                    "editWorkOrderPrice"       => ["viewWorkOrder"],
                    "deleteWorkOrder"          => ["viewWorkOrder"],
                    "orderShipping"            => ["viewOrder", "viewMenuShipping"],
                    "orderPacking"             => ["viewOrder", "viewMenuOrderPacking", "viewMasterDataList"],
                    "undoPackCartons"          => ["viewOrder"],
                    "palletAssignment"         => ["viewOrder", "viewMenuPalletAssignment"],
                    "viewBOL"                  => ["viewMenuBOL", "viewMenuShipping"],
                    "createBOL"                => ["viewBOL", "createCommodity", "editCommodity"],
                    "editBOL"                  => ["viewBOL"],
                    "importOnlineOrder"        => ["viewOrder"],
                    "updateScheduleToShipDate" => ["viewOrder"],
                    "finalBOL"                 => ["viewBOL", "editBOL", "createBOL"],
                    "csrComment"               => ["viewOrder", "editOrder", "createOrder"],
                    "whseComment"              => ["viewOrder", "editOrder", "createOrder"],

                ]
            ],
            "Transfer Pallet" => [
                "permissions" => [
                    "transferPallet" => "view Transfer Pallet"
                ],
                "children"    => [
                    "transferPallet" => ["viewMenuTransferPallet"],
                ]
            ],

            "Cycle Count" => [
                "permissions" => [
                    "viewCycleCount"               => "view Cycle Count",
                    "createCycleCount"             => "create Cycle Count",
                    "editCycleCount"               => "edit Cycle Count",
                    "addSKUCycleCount"             => "add SKU Cycle Count",
                    "recountCycleCount"            => "recount Cycle Count",
                    "approveCycleCount"            => "approve Cycle Count",
                    "deleteCycleCount"             => "delete Cycle Count",
                    "printCycleCount"              => "print Cycle Count",
                    "viewCycleCountNotification"   => "view Cycle Count Notification",
                    "createCycleCountNotification" => "create Cycle Count Notification",
                    "unsetRfidOnLocation"          => "unset Rfid On Location",
                ],
                "children"    => [
                    "viewCycleCount"               => ["viewMenuCycleCount"],
                    "createCycleCount"             => ["viewCycleCount"],
                    "editCycleCount"               => ["viewCycleCount"],
                    "addSKUCycleCount"             => ["viewCycleCount"],
                    "recountCycleCount"            => ["viewCycleCount"],
                    "approveCycleCount"            => ["viewCycleCount"],
                    "deleteCycleCount"             => ["viewCycleCount"],
                    "printCycleCount"              => ["viewCycleCount"],
                    "viewCycleCountNotification"   => ["viewMenuCycleCountNotification"],
                    "createCycleCountNotification" => ["viewCycleCountNotification"],
                    "unsetRfidOnLocation"          => ["viewCycleCount", "createCycleCount"],
                ]
            ],

            "Event Tracking" => [
                "permissions" => [
                    "inboundEventTracking"  => "Event Tracking For Inbound",
                    "outboundEventTracking" => "Event Tracking For Outbound"
                ],
                "children"    => [
                    "inboundEventTracking"  => ["viewMenuInboundEventTracking"],
                    "outboundEventTracking" => ["viewMenuOutboundEventTracking"]
                ]
            ],

            "Warehouse Operation" => [
                "permissions" => [
                    'consolidate'      => 'consolidate',
                    'relocation'       => 'Relocation',
                    "viewXFER"         => "view XFER",
                    'createXFER'       => 'create XFER',
                    'editXFER'         => 'edit XFER',
                    'updateXferInv'    => 'update xfer inventor',
                    'createXferTicket' => 'create xfer ticket'
                ],
                "children"    => [
                    "consolidate"   => ["viewMenuConsolidation"],
                    "relocation"    => ["viewMenuRelocation"],
                    "viewXFER"      => ["viewMenuXFER"],
                    "createXFER"    => ["viewXFER"],
                    "editXFER"      => ["viewXFER"],
                    "updateXferInv" => ["viewMenuXferInv"]
                ]
            ],

            "User Management" => [
                "permissions" => [
                    "createUser"        => "Create User",
                    "editUser"          => "Edit User",
                    "deleteUser"        => "Delete User",
                    "viewUser"          => "View User ",
                    "resetUserPassword" => "Reset User Password",
                    "isCSR"             => "CSR user"
                ],
                "children"    => [
                    "viewUser"          => ["viewMenuUser", "userAssignAll"],
                    "createUser"        => ["viewUser"],
                    "editUser"          => ["viewUser"],
                    "deleteUser"        => ["viewUser"],
                    "resetUserPassword" => ["viewUser"]
                ]
            ],

            "Role Management" => [
                "permissions" => [
                    "createRole" => "Create Role",
                    "viewRole"   => "View Role",
                    "editRole"   => "Edit Role",
                    "deleteRole" => "Delete Role",
                ],
                "children"    => [
                    "viewRole"   => ["viewMenuRole"],
                    "createRole" => ["viewRole", "assignPermissionToRole"],
                    "editRole"   => ["viewRole", "assignPermissionToRole"],
                    "deleteRole" => ["viewRole"]
                ]
            ],

            "Administration" => [
                "permissions" => [
                    "configSystem" => "Change System Configuration",
                    "configMenu"   => "Config Menu"
                ],
                "children"    => [
                    "configSystem" => ["viewMenuSystemConfiguration"],
                    "configMenu"   => ["viewMenuConfigMenu"],
                ]
            ],

            "Item Master" => [
                "permissions" => [
                    "createItem" => "Create Item",
                    "editItem"   => "Edit Item",
                    "viewItem"   => "View Item",
                    "deleteItem" => "Delete Item"
                ],
                "children"    => [
                    "viewItem"   => ["viewMenuItemMaster"],
                    "editItem"   => ["viewItem"],
                    "createItem" => ["viewItem"],
                    "deleteItem" => ["viewItem"]
                ]
            ],

            "Zone Type" => [
                "permissions" => [
                    'viewZoneType'   => 'view Zone Type',
                    'editZoneType'   => 'edit Zone Type',
                    'createZoneType' => 'create Zone Type',
                    'deleteZoneType' => 'delete Zone Type'
                ],
                "children"    => [
                    "viewZoneType"   => ["viewMenuZoneType"],
                    "createZoneType" => ["viewZoneType"],
                    "editZoneType"   => ["viewZoneType"],
                    "deleteZoneType" => ["viewZoneType"]
                ]
            ],

            "Location Type" => [
                "permissions" => [
                    'viewLocationType'   => 'view Location Type',
                    'editLocationType'   => 'edit Location Type',
                    'createLocationType' => 'create Location Type',
                    'deleteLocationType' => 'delete Location Type'
                ],
                "children"    => [
                    "viewLocationType"   => ["viewMenuLocationType"],
                    "createLocationType" => ["viewLocationType"],
                    "editLocationType"   => ["viewLocationType"],
                    "deleteLocationType" => ["viewLocationType"]
                ]
            ],

            "UOM Management" => [
                "permissions" => [
                    'viewUOM'   => 'view UOM',
                    'editUOM'   => 'edit UOM',
                    'createUOM' => 'create UOM',
                    'deleteUOM' => 'delete UOM'
                ],
                "children"    => [
                    "viewUOM"   => ["viewMenuUOM"],
                    "createUOM" => ["viewUOM"],
                    "editUOM"   => ["viewUOM"],
                    "deleteUOM" => ["viewUOM"]
                ]
            ],

//            "Charge Type" => [
//                "permissions" => [
//                    'viewChargeType'   => 'view Charge Type',
//                    'editChargeType'   => 'edit Charge Type',
//                    'createChargeType' => 'create Charge Type',
//                    'deleteChargeType' => 'delete Charge Type'
//                ],
//                "children"    => [
//                    "viewChargeType"   => ["viewMenuChargeType"],
//                    "createChargeType" => ["viewChargeType"],
//                    "editChargeType"   => ["viewChargeType"],
//                    "deleteChargeType" => ["viewChargeType"]
//                ]
//            ],

//            "Charge Code" => [
//                "permissions" => [
//                    'viewChargeCode'   => 'view Charge Code',
//                    'editChargeCode'   => 'edit Charge Code',
//                    'createChargeCode' => 'create Charge Code',
//                    'deleteChargeCode' => 'delete Charge Code'
//                ],
//                "children"    => [
//                    "viewChargeCode"   => ["viewMenuChargeCode"],
//                    "createChargeCode" => ["viewChargeCode"],
//                    "editChargeCode"   => ["viewChargeCode"],
//                    "deleteChargeCode" => ["viewChargeCode"]
//                ]
//            ],

            "Damage Type" => [
                "permissions" => [
                    'viewDamageType'   => 'view Damage Type',
                    'editDamageType'   => 'edit Damage Type',
                    'createDamageType' => 'create Damage Type',
                    'deleteDamageType' => 'delete Damage Type'
                ],
                "children"    => [
                    "viewDamageType"   => ["viewMenuDamageType"],
                    "createDamageType" => ["viewDamageType"],
                    "editDamageType"   => ["viewDamageType"],
                    "deleteDamageType" => ["viewDamageType"]
                ]
            ],

            "Country Management" => [
                "permissions" => [
                    'viewCountry'   => 'view Country',
                    'editCountry'   => 'edit Country',
                    'createCountry' => 'create Country',
                    'deleteCountry' => 'delete Country'
                ],
                "children"    => [
                    "viewCountry"   => ["viewMenuCountry"],
                    "createCountry" => ["viewCountry"],
                    "editCountry"   => ["viewCountry"],
                    "deleteCountry" => ["viewCountry"]
                ]
            ],

            "State Management" => [
                "permissions" => [
                    'viewState'   => 'view State Country',
                    'editState'   => 'edit State Country',
                    'createState' => 'create State Country',
                    'deleteState' => 'delete State Country'
                ],
                "children"    => [
                    "viewState"   => ["viewMenuState"],
                    "createState" => ["viewState"],
                    "editState"   => ["viewState"],
                    "deleteState" => ["viewState"]
                ]
            ],

            "Warehouse Config" => [
                "permissions" => [
                    "createWarehouse" => "Create Warehouse ",
                    "editWarehouse"   => "Edit Warehouse",
                    "deleteWarehouse" => "Delete Warehouse",
                    "viewWarehouse"   => "View Warehouse"
                ],
                "children"    => [
                    "viewWarehouse"   => ["viewMenuWarehouse"],
                    "createWarehouse" => ["viewWarehouse"],
                    "editWarehouse"   => ["viewWarehouse"],
                    "deleteWarehouse" => ["viewWarehouse"]
                ]
            ],

            "Location" => [
                "permissions" => [
                    "createLocation"       => "create Location",
                    "viewLocation"         => "view Location",
                    "editLocation"         => "edit location",
                    "deleteLocation"       => "delete Location",
                    "importLocation"       => "import Location",
                    "changeLocationStatus" => "change status of Location"
                ],
                "children"    => [
                    "viewLocation"         => ["viewMenuLocation"],
                    "createLocation"       => ["viewLocation"],
                    "editLocation"         => ["viewLocation"],
                    "deleteLocation"       => ["viewLocation"],
                    "changeLocationStatus" => ["viewLocation"],
                    "importLocation"       => ["viewLocation"]
                ]
            ],

            "Zone Management" => [
                "permissions" => [
                    'viewZone'   => 'view Zone',
                    'createZone' => 'create Zone',
                    'editZone'   => 'edit Zone',
                    'deleteZone' => 'view Zone'
                ],
                "children"    => [
                    "viewZone"   => ["viewMenuZone"],
                    "createZone" => ["viewZone"],
                    "editZone"   => ["viewZone"],
                    "deleteZone" => ["viewZone"]
                ]
            ],

            "Customer" => [
                "permissions" => [
                    "viewCustomer"   => "View detail Customer",
                    "createCustomer" => "create Customer",
                    "editCustomer"   => "edit Customer",
                    "deleteCustomer" => "delete Customer"
                ],
                "children"    => [
                    "viewCustomer"   => ["viewMenuCustomer"],
                    "createCustomer" => ["viewCustomer"],
                    "editCustomer"   => ["viewCustomer"],
                    "deleteCustomer" => ["viewCustomer"]
                ]
            ],
            "Report"   => [
                "permissions" => [
                    "viewReport"               => "View report",
                    "viewAvailableInventory"   => "view Available Inventory",
                    "viewInventory"            => "view Inventory",
                    "viewInventoryHistory"     => "view Inventory History",
                    "viewStyleLocations"       => "view Style Locations",
                    "viewWarehouseLocations"   => "view Warehouse Loacations",
                    "viewPerformanceActivity"  => "view performanceActivity",
                    "viewReceivingReport"      => "view Receiving Report",
                    "viewStorages"             => "view Storages",
                    "viewInventoryReport"      => "view Inventory Report",
                    "resetDataInventoryReport" => "reset data  Inventory Report",
                    "viewPalletReport"         => "view Pallet Report",
                    "viewActualReceiving"      => "view Actual Receiving",
                    "viewReceivingContainer"   => "view Receiving Container",
                    "viewOutboundOrder"        => "view Outbound Order",

                ],
                "children"    => [
                    "viewReport"               => ["viewMenuReport"],
                    "viewAvailableInventory"   => ["viewReport"],
                    "viewInventory"            => ["viewReport"],
                    "viewInventoryHistory"     => ["viewReport"],
                    "viewStyleLocations"       => ["viewReport"],
                    "viewWarehouseLocations"   => ["viewReport"],
                    "viewPerformanceActivity"  => "viewMenuPerformance",
                    "viewReceivingReport"      => ["viewReport"],
                    "viewStorages"             => ["viewReport"],
                    "viewInventoryReport"      => ["viewMenuInventoryReport"],
                    "resetDataInventoryReport" => ["viewInventoryReport"],
                    "viewPalletReport"         => ['viewMenuPalletReport'],
                    "viewActualReceiving"      => ["viewReport"],
                    "viewReceivingContainer"   => ["viewReport"],
                    "viewOutboundOrder"        => ["viewReport"],


                ]
            ],

//            "Invoice" => [
//                "permissions" => [
//                    "viewInvoice" => "View Invoice"
//                ],
//                "children"    => [
//                    "viewInvoice" => "viewMenuInvoice"
//                ]
//            ],

            "WarehouseGUI" => [
                "permissions" => [
                    "viewWarehouseLayout" => "View Warehouse Layout"
                ],
                "children"    => [
                    "viewWarehouseLayout" => "viewMenuWarehouseLayout"
                ]
            ],

            "BlockStock" => [
                "permissions" => [
                    "viewBlockStock"   => "view Block Stock",
                    "createBlockStock" => "create Block Stock"
                ],
                "children"    => [
                    "viewBlockStock"   => "viewMenuBlockStock",
                    "createBlockStock" => "viewBlockStock"
                ]
            ],

            "RFGUN" => [
                "permissions" => [
                    "viewRFGUN"    => "view RFGUN",
                    "accessGun"    => "access GUN",
                    "goodsReceipt" => "Goods Receipt",
                    "doPutAway"    => "Put Away For Rf Gun",
                    "relocate"     => "Relocate",
                    "wavePick"     => "Wave Pick",
                    "cycleCount"   => "Cycle Count",
                    "doPutBack"    => "Put Back For Rf Gun"
                ],
                "children"    => [
                    "wavePick"  => "updateOrderPicking",
                    "viewRFGUN" => "viewMenuRFGUN",
                ]
            ],

            "Department" => [
                "permissions" => [
                    "createDepartment" => "Create Department",
                    "editDepartment"   => "Edit Department",
                    "viewDepartment"   => "View Department",
                    "deleteDepartment" => "Delete Department"
                ],
                "children"    => [
                    "viewDepartment"   => "viewMenuDepartment",
                    "createDepartment" => "viewDepartment",
                    "editDepartment"   => "viewDepartment",
                ]

            ],
            "PutBack"    => [
                "permissions" => [
                    'viewPutbackOrder'     => 'view PutbackOrder',
                    'viewPutbackReceipt'   => 'view PutbackReceipt',
                    'createPutbackReceipt' => 'create PutbackReceipt',
                    'assignPalletPutback'  => 'assign Pallet Putback',
                    'updatePutback'        => "update Putback",
                    'printPutback'         => "print Putback"
                ],
                "children"    => [
                    "viewPutbackOrder"     => ["viewMenuPutbackOrder"],
                    "viewPutbackReceipt"   => ["viewMenuPutbackReceipt", "printPutback"],
                    "createPutbackReceipt" => ["viewPutbackReceipt"],
                    "assignPalletPutback"  => ['viewMenuAssignPalletPutback'],
                    "updatePutback"        => ['viewPutbackReceipt', 'viewMenuUpdatePutback'],
                ]
            ],
            "Dashboard"  => [
                "permissions" => [
                    'dashboardReceiving'         => 'dashboard Receiving',
                    'dashboardPutaway'           => 'dashboard Putaway',
                    'dashboardOrder'             => 'dashboard Order',
                    'dashboardInprogress'        => 'dashboard Inprogress',
                    "dashboardWarehouseCapacity" => 'dashboard Warehouse Capacity',
                    "dashboardProduct"           => 'dashboard Product',
                ],
            ],
            "EDI"        => [
                "permissions" => [
                    'configEDI' => 'config EDI',
                ],
                "children"    => [
                    "configEDI" => ["viewMenuEDI"],
                ]
            ],
            "Migrate"    => [
                "permissions" => [
                    'importItem'      => 'import Item',
                    'importCustomer'  => 'import Customer',
                    'importInventory' => 'import Inventory',
                    'importASN'       => 'import ASN',
                    'importOrder'     => 'import Order',
                ],
                "children"    => [
                    'importItem'      => ["viewMenuImportItem", "viewCustomer"],
                    'importCustomer'  => ["viewMenuImportCustomer"],
                    'importInventory' => ["viewMenuImportInventory", "viewCustomer"],
                    'importASN'       => ["viewMenuImportASN", "viewCustomer"],
                    'importOrder'     => ["viewMenuImportOrder", "viewCustomer"],
                ]
            ],

            "BlockReason" => [
                "permissions" => [
                    'viewBlockReason'   => 'view Block Reason',
                    'editBlockReason'   => 'edit Block Reason',
                    'createBlockReason' => 'create Block Reason',
                ],
                "children"    => [
                    'viewBlockReason'   => ["viewMenuBlockReason"],
                    'editBlockReason'   => ["viewBlockReason"],
                    'createBlockReason' => ["viewBlockReason"],
                ]
            ],


            "DeliveryService" => [
                "permissions" => [
                    'viewDeliveryService'   => 'view Delivery Service',
                    'editDeliveryService'   => 'edit Delivery Service',
                    'createDeliveryService' => 'create Delivery Service',
                ],
                "children"    => [
                    'viewDeliveryService'   => ["viewMenuDeliveryService"],
                    'editDeliveryService'   => ["viewDeliveryService"],
                    'createDeliveryService' => ["viewDeliveryService"],
                ]
            ],

            "Third Party" => [
                "permissions" => [
                    "viewThirdParty"   => "view Third Party",
                    "createThirdParty" => "create Third Party",
                    "editThirdParty"   => "edit Third Party"
                ],
                "children"    => [
                    "createThirdParty" => ['viewThirdParty'],
                    "editThirdParty"   => ['viewThirdParty']
                ]
            ],
            "Master Data" => [
                "permissions" => [
                    "manageTruckType"   => "manage Truck Type",
                    "manageTransporter" => "manage Transporter",
                    "viewMasterDataList" => "view Master DataList",

                    //Master Data: replenishment Config
                    "viewReplenishmentConfig"        => "View Replenishment Configuration",
                    "createReplenishmentConfig"      => "Create Replenishment Configuration",
                    "editReplenishmentConfig"        => "Edit Replenishment Configuration",
                ],
                "children"    => [
                    "manageTruckType"   => ['createTruckType', 'editTruckType'],
                    "manageTransporter" => ['createTransporter', 'editTransporter'],
                    "viewMasterDataList" => ['viewMenuMasterDataList'],

                    //----------------------------------------------
                    //Master Data: replenishment Config
                    "viewReplenishmentConfig"   => "viewMenuReplenishmentConfig",
                    "createReplenishmentConfig" => "viewReplenishmentConfig",
                    "editReplenishmentConfig"   => "viewReplenishmentConfig",
                ]
            ],

            "Ecom Order" => [
                "permissions" => [
                    "viewEcomOrder"          => "View Ecom Order List",
                    "createEcomOrder"        => "Create Ecom Order",
                    "editEcomOrder"          => "Edit Order",
                    "ecomAssignCSR"          => "Ecom Assign CSR",
                    "allocateEcomOrder"      => "Allocate Ecom Order",
                    "createEcomWavePick"     => "Create Ecom Wave Pick",
                    "ecomWavepickAssignment" => "Ecom wavepick Assignment",
                    "printEcomWavePick"      => "Print Ecom WavePick",
                    "updateEcomOrderPicking" => "Update Ecom Order Picking",
                    "cancelEcomOrder"        => "Cancel Ecom Order",
                    "holdEcomOrder"          => "Hold Ecom Order",
                    "viewEcomWorkOrder"      => "view Ecom Work Order",
                    "editEcomWorkOrder"      => "Edit Ecom Work Order",
                    "createEcomWorkOrder"    => "Create Ecom Work Order",
                    "ecomOrderShipping"      => "Ecom Order shipping",
                    "ecomOrderPacking"       => "Ecom Order Packing",
                    "ecomPalletAssignment"   => "Ecom Assign Carton To Pallet",
                    "importEcomOrder"        => "Import Ecom Order",
                    "ecomPackingSlip"        => "Ecom packing Slip",
                    "viewEcomBOL"            => "View Ecom BOL",
                    "createEcomBOL"          => "Create Ecom BOL",
                    "editEcomBOL"            => "Edit Ecom BOL",
                    'editEcomOrderShipDate'  => "Edit Ecom Order ShipDate"
                ],
                "children"    => [
                    "viewEcomOrder"          => ["viewMenuEcomOrder"],
                    "createEcomOrder"        => ["viewEcomOrder"],
                    "editEcomOrder"          => ["viewEcomOrder"],
                    "ecomAssignCSR"          => ["viewMenuEcomAssignCSR", "viewEcomOrder"],
                    "allocateEcomOrder"      => ["viewMenuAllocateEcomOrder", "viewEcomOrder"],
                    "createEcomWavePick"     => ["viewMenuEcomWavePick", "viewEcomOrder"],
                    "ecomWavepickAssignment" => "viewMenuEcomWavepickAssignment",
                    "updateEcomOrderPicking" => ["viewMenuEcomOrderPicking", "viewEcomOrder"],
                    "cancelEcomOrder"        => ["viewEcomOrder"],
                    "holdEcomOrder"          => ["viewEcomOrder"],
                    "viewEcomWorkOrder"      => ["viewEcomOrder", "viewMenuEcomWorkOrder"],
                    "createEcomWorkOrder"    => ["viewEcomWorkOrder"],
                    "editEcomWorkOrder"      => ["viewEcomWorkOrder"],
                    "ecomOrderShipping"      => ["viewEcomOrder", "viewMenuEcomShipping"],
                    "ecomOrderPacking"       => ["viewEcomOrder", "viewMenuEcomOrderPacking"],
                    "ecomPalletAssignment"   => ["viewEcomOrder", "viewMenuEcomPalletAssignment"],
                    "viewEcomBOL"            => ["viewMenuEcomBOL", "viewMenuEcomShipping"],
                    "createEcomBOL"          => ["viewEcomBOL", "createCommodity", "editCommodity"],
                    "editEcomBOL"            => ["viewEcomBOL"],
                    "importEcomOrder"        => ["viewEcomOrder"],
                ]
            ],

            "Invoice Master" => [
                "permissions" => [
                    'viewBillable'                 => 'view Billable',
                    'viewInvoice'                  => 'view Invoice ',
                    'createInvoice'                => 'create Invoice',
                    'editInvoice'                  => 'edit Invoice',
                    'deleteInvoice'                => 'delete Invoice',
                    'printInvoice'                 => 'print Invoice',
                    'sendInvoiceEmail'             => 'send Invoice Email',
                    'addInvoicePayment'            => 'add Invoice Payment',
                    // 'viewWorkOrder'                => 'view Work Order',
                    // 'createWorkOrder'              => 'create Work Order',
                    // 'deleteWorkOrder'              => 'delete Work Order',
                    // 'editWorkOrder'                => 'edit Work Order',
                    'viewChargeCode'               => 'view Charge Code',
                    'editChargeCode'               => 'edit Charge Code',
                    'createChargeCode'             => 'create Charge Code',
                    'deleteChargeCode'             => 'delete Charge Code',
                    'deleteBillableItem'           => 'delete Billable Item',
                    'viewInvoiceReport'            => 'view Invoice Report',
                    'viewInvoiceApprove'           => 'view Invoice Approve',
                    'changeInvoiceStatus'          => 'change Invoice Status',
                    'viewCustomerConfig'           => 'view Customer Config',
                    'updateCustomerChargeCode'     => 'update Customer Charge Code',
                    'deleteCustomerChargeCode'     => 'delete Customer Charge Code',
                    'updateCustomerEmailConfig'    => 'update Customer Email Config',
                    'updateCustomerOtherConfig'    => 'update Customer Other Config',
                    'updateCustomerCurrencyConfig' => 'update Customer Currency Config',
                    'viewInvoiceSystemConfig'      => 'view Invoice System Config',
                    'updateInvoiceSystemConfig'    => 'update Invoice System Config',
                ],
                "children"    => [
                    "viewBillable"                 => ["viewMenuBillableItems"],
                    "viewInvoice"                  => ["viewBillable", "viewMenuInvoiceList"],
                    "createInvoice"                => ["viewInvoice"],
                    "editInvoice"                  => ["viewInvoice"],
                    "deleteInvoice"                => ["viewInvoice"],
                    "printInvoice"                 => ["viewInvoice"],
                    "sendInvoiceEmail"             => ["viewInvoice"],
                    "addInvoicePayment"            => ["viewInvoice"],
                    // "viewWorkOrder"                => ["viewWorkOrder"],
                    // "createWorkOrder"              => ["viewWorkOrder"],
                    // "deleteWorkOrder"              => ["viewWorkOrder"],
                    // "editWorkOrder"                => ["viewWorkOrder"],
                    'viewChargeCode'               => ["viewMenuChargeCode", "viewInvoice"],
//                    'editChargeCode'               => ["viewChargeCode"],
                    "createChargeCode"             => ["viewChargeCode"],
                    "editChargeCode"               => ["viewChargeCode"],
                    "deleteChargeCode"             => ["viewChargeCode"],
                    "deleteBillableItem"           => ["viewBillable"],
                    'viewInvoiceReport'            => ["viewMenuInvoiceReport"],
                    'viewInvoiceApprove'           => ["viewMenuApprovalList", "viewInvoice"],
                    'changeInvoiceStatus'          => ["viewInvoiceApprove"],
                    'viewCustomerConfig'           => ["viewMenuCustomerConfiguration", "viewInvoice"],
                    'updateCustomerChargeCode'     => ["viewCustomerConfig"],
                    'deleteCustomerChargeCode'     => ["viewCustomerConfig"],
                    'updateCustomerEmailConfig'    => ["viewCustomerConfig"],
                    'updateCustomerOtherConfig'    => ["viewCustomerConfig"],
                    'updateCustomerCurrencyConfig' => ["viewCustomerConfig"],
                    'viewInvoiceSystemConfig'      => ["viewMenuInvoiceSystemConfiguration", "viewInvoice", "viewMasterDataList", "configSystem"],
                    'updateInvoiceSystemConfig'    => ["viewInvoiceSystemConfig", "viewInvoice"],
                ]
            ],

            "Gate Management" => [
                "permissions" => [
                    'gateManagement'   => 'view Gate Management'
                ],
                "children"    => [
                    "gateManagement" => ["viewMenuGateManagement"]
                ]
            ]
        ];
    }
}