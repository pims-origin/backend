<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\ReasonModel;
use App\Api\V1\Transformers\CycleCountReasonTransformer;
use App\Api\V1\Transformers\ReasonTransformer;
use App\Api\V1\Validators\CycleCountReasonValidator;
use App\Api\V1\Validators\ReasonValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\Validator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class ReasonController extends AbstractController
{
    protected $model;
    protected $validator;
    protected $request;
    protected $error;
    protected $eventTrackingModel;

    protected $reasonTransform;

    /**
     * ReasonController constructor.
     *
     * @param ReasonModel $model
     * @param ReasonValidator $validator
     * @param Request $request
     * @param EventTrackingModel $eventTrackingModel
     * @param ReasonTransformer $reasonTransformer
     */

    public function __construct(
        ReasonModel $model,
        ReasonValidator $validator,
        Request $request,
        EventTrackingModel $eventTrackingModel,
        ReasonTransformer $reasonTransformer
    ) {
        $this->model = $model;
        $this->validator = $validator;
        $this->request = $request;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->reasonTransform = $reasonTransformer;
    }

    /**
     * @param Request $request
     *
     * @throws \Exception
     */

    public function store(Request $request)
    {
        $input = $request->getParsedBody();

        //$this->validator->validate($input);

        //check duplicated
        $checkBlockStock = $this->model->getFirstWhere(
            [
                'block_rsn_name' => array_get($input, 'block_rsn_name', ''),
            ]
        );
        if (!empty($checkBlockStock)) {
            throw new \Exception(Message::get("BM006", "block reason name"));
        }

        try {

            $data = $this->model->processCreateBlockReason($input);

            if (!$data) {
                return $this->response->errorBadRequest(implode($this->model->errors));
            }

            return $this->response->item($data, new ReasonTransformer());

            //} catch (\Exception $e) {
            //
            //    return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
            //
            //}
        } catch (\Exception $e) {
            ThrowError::response($e->getMessage());
        }
    }

    /**
     * @return mixed
     */

    public function reasonList()
    {
        $input = $this->request->getQueryParams();

        try {
            $data = $this->model->search($input, ['createdBy'], array_get($input, 'limit', 20));

            return $this->response->paginator($data, $this->reasonTransform);

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    public function storeCycleCountReason(Request $request, CycleCountReasonValidator $validator)
    {
        $input = $request->getParsedBody();

        $validator->validate($input);

        try{
            $reason = Reason::create($input);            
            return $this->response->item($reason, new CycleCountReasonTransformer());
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function getCycleCountReasonList()
    {
        $input = $this->request->getQueryParams();

        try{
            $limit = array_get($input, 'limit', 20);
            $reasons = Reason::paginate($limit);

            return $this->response->paginator($reasons, new CycleCountReasonTransformer());
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}
