<?php

namespace App\Api\V2\Inbound\Carton\GetDamageType\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;

/**
 * Class BaseController
 * @package App\Api\V1\Controllers
 *
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="api.domain",
 *     basePath="/",
 *     definitions={},
 *     @SWG\Info(
 *         version="WMS 2.0.0",
 *         title="Your API Functional Name",
 *         description="Description for APIs",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="your@email.address"
 *         ),
 *         @SWG\License(
 *             name="WMS2.0",
 *             url="seldatinc.com"
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="data",
 *         type="object",
 *         properties={
 *             @SWG\Property(property="menu_group_id", type="integer"),
 *             @SWG\Property(property="name", type="string"),
 *             @SWG\Property(property="description", type="string"),
 *         },
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */
abstract class AbstractController extends Controller
{
    use Helpers;

    private $response = [
        'code' => 200,
        'status' => false,
        'message' => null,
        'data' => null,
    ];

    public function formatArr($arr = [], $check = false){
        if(empty($arr))
            return $arr;

        if(!is_array($arr) && is_object($arr))
            $arr = $arr->toArray();

        $data = [];
        foreach($arr as $k=>$v){
            if($check){
                foreach($v as $t=>$h) {
                    if (is_numeric($t)) {
                        unset($v[$t]);
                    }
                }
                $data[] = $v;
            } else
                if(is_numeric($k)) unset($arr[$k]);
        }
        if($check)
            return $data;

        return $arr;
    }

    final protected function setStatus($status){
        $this->response['status'] = $status;
    }

    final protected function setData($data = []){
        $this->response['data'] = $data;
    }

    final protected function setMessage($message = ''){
        $this->response['message'] = $message;
    }

    final protected function setCode($code = ''){
        $this->response['code'] = $code;
    }

    final protected function getStatus(){
        return $this->response['status'];
    }

    final protected function getData(){
        return $this->response['data'];
    }

    final protected function getMessage(){
        return $this->response['message'];
    }

    final protected function getCode(){
        return $this->response['code'];
    }

    final protected function getResponseData(){
        return $this->response;
    }
}
