<?php

namespace Seldat\Wms2\Models;

class CustomerZone extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_zone';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'zone_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'zone_id'
    ];

    public function zone()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Zone', 'zone_id', 'zone_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function location()
    {
        return $this->hasMany(__NAMESPACE__ . '\Location', 'loc_zone_id', 'zone_id');
    }
}
