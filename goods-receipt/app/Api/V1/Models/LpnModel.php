<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Lpn;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Seldat\Wms2\Models\Setting;
use Seldat\Wms2\Models\SystemMeasurement;

class LpnModel extends AbstractModel
{


    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new Lpn();
    }


}
