<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Transformers\InventorySummaryTransformer;
use App\Api\V1\Validators\InventorySummaryValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\SystemBug;

class InventorySummaryController extends AbstractController
{
    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var $asnDtlModel
     */
    protected $asnDtlModel;

    /**
     * @var $goodsReceiptDetail
     */
    protected $goodsReceiptDetailModel;

    /**
     * InventorySummaryController constructor.
     */
    public function __construct(
        InventorySummaryModel $inventorySummaryModel,
        AsnDtlModel $asnDtlModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel
    ) {
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->asnDtlModel = $asnDtlModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
    }

    public function search(Request $request, InventorySummaryTransformer $inventorySummaryTransformer)
    {
        $input = $request->getQueryParams();

        try {
            // get list asn
            $invt_sum = $this->inventorySummaryModel->search($input, ['item'], array_get($input, 'limit'));

            return $this->response->paginator($invt_sum, $inventorySummaryTransformer);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $item_id
     * @param $type
     * @param InventorySummaryTransformer $inventorySummaryTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function update(
        Request $request,
        $item_id,
        $type,
        InventorySummaryTransformer $inventorySummaryTransformer
    ) {
        $input = $request->getParsedBody();
        $allocated = array_get($input, 'allocated', 0);
        try {
            if (!$evt_summary = $this->inventorySummaryModel->checkWhere(['itm_id' => $item_id])) {
                throw  new \Exception(Message::get("BM017", "Item"));
            }

            if ($type == 'inbound') {
                $totalItem = $this->asnDtlModel->getTotalItem($item_id);
                $totalItemDamaged = $this->goodsReceiptDetailModel->getTotalItemDamaged($item_id, ['asnDetail']);
                $allocated_qty = object_get($evt_summary, 'allocated_qty', 0);
            } else {
                $totalItemDamaged = object_get($evt_summary, 'dmg_qty', 0);
                $allocated_qty = object_get($evt_summary, 'allocated_qty', 0) + $allocated;
                $totalItem = object_get($evt_summary, 'ttl', 0) - $allocated;
                $totalItem = $totalItem < 0 ? 0 : $totalItem;
            }

            $data = [
                'item_id'        => $item_id,
                'ttl'           => $totalItem,
                'dmg_qty'       => $totalItemDamaged,
                'allocated_qty' => $allocated_qty,
                'avail'         => $totalItem - $totalItemDamaged - $allocated_qty
            ];

            $item = $this->inventorySummaryModel->update($data);

            return $this->response->item($item, $inventorySummaryTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param Request $request
     * @param $item_id
     * @param InventorySummaryValidator $inventorySummaryValidator
     * @param InventorySummaryTransformer $inventorySummaryTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(
        Request $request,
        $item_id,
        InventorySummaryValidator $inventorySummaryValidator,
        InventorySummaryTransformer $inventorySummaryTransformer
    ) {
        $input = $request->getParsedBody();
        $inventorySummaryValidator->validate($input);
        try {
            if ($this->inventorySummaryModel->checkWhere(['item_id' => $item_id])) {
                throw  new \Exception('Item already existed');
            }
            $data = [
                'item_id'        => $item_id,
                'ttl'           => array_get($input, 'ttl', 0),
                'cus_id'        => array_get($input, 'cus_id', 0),
                'whs_id'        => array_get($input, 'whs_id', 0),
                'color'         => array_get($input, 'color', ''),
                'size'          => array_get($input, 'size', ''),
                'lot'           => array_get($input, 'lot', ''),
                'allocated_qty' => array_get($input, 'allocated_qty', 0),
                'dmg_qty'       => array_get($input, 'dmg_qty', 0),
                'sku'           => array_get($input, 'sku', ''),
                'avail'         => array_get($input, 'avail', 0),
            ];


            $item = $this->inventorySummaryModel->create($data);

            return $this->response->item($item, $inventorySummaryTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}
