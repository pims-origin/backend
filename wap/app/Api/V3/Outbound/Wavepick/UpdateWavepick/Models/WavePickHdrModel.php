<?php

namespace App\Api\V3\Outbound\Wavepick\UpdateWavepick\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class WavePickHdrModel extends AbstractModel
{

    protected $model;

    /**
     * WavePickModel constructor.
     */
    public function __construct(WavepickHdr $model = null)
    {
        $this->model = ($model) ?: new WavepickHdr();
    }

    /**
     * @param $data
     *
     * @return static
     */
    public function saveWavePickHdr($data){
        return $this->model->create($data);
    }

    /**
     * @return mixed
     */
    public function getLatestWavePickNumber()
    {
        return $this->model->orderBy('wv_num', 'desc')->take(1)->first();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getWvHdrInfo($wv_id){
        return $this->model
            ->where('wv_hdr.wv_id','=',$wv_id)
            ->leftJoin('odr_hdr','odr_hdr.wv_id','=','wv_hdr.wv_id')
            ->leftJoin('customer','customer.cus_id','=','odr_hdr.cus_id')
            ->select(
                'wv_hdr.wv_id',
                'wv_hdr.wv_num',
                'odr_hdr.cus_id',
                'customer.cus_name',
                'wv_hdr.wv_sts',
                'wv_hdr.whs_id'
            )
            ->first();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, ['wv_num','wv_sts'])) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        if (isset($attributes['sort']) && is_array($attributes['sort']) && !empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $order) {
                if (!empty($key)) {
                    $query->orderBy($key, $order);
                }
            }
        }
        $this->model->filterData($query);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function updateStatusWvHdr($wv_id)
    {
        return $this->model
            ->where('wv_id', '=', $wv_id)
            ->update([
                'wv_sts'    => Status::getByValue("Completed", "WAVEPICK-STATUS"),
            ]);
    }

    /**
     * @param string $wvId
     *
     * @return mixed
     */
    public function updatePicked($wvId)
    {
        $sqlOdrPicked = sprintf("(SELECT COUNT(1) FROM odr_dtl WHERE odr_dtl.wv_id = %s AND odr_dtl.itm_sts IN ('NW', 'PK') AND deleted =0 AND alloc_qty > 0 and odr_hdr.odr_id = odr_dtl.odr_id) = 0", $wvId);
        $sqlWvPickedStr = "(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty != wv_dtl.piece_qty AND deleted =0) = 0";
        return \DB::table('wv_hdr')
            ->join('odr_hdr', 'odr_hdr.wv_num', '=', 'wv_hdr.wv_num')
            ->where('wv_hdr.wv_id', $wvId)
            ->where('wv_hdr.deleted', 0)
            ->where('odr_hdr.deleted', 0)
            ->whereRaw(DB::Raw($sqlWvPickedStr))
            ->whereRaw(DB::Raw($sqlOdrPicked))
            ->update([
                'wv_hdr.wv_sts'     => Status::getByValue("Completed", "WAVEPICK-STATUS"),
                'wv_hdr.updated_at' => time(),
                'wv_hdr.updated_by' => Data::getCurrentUserId(),
            ]);
    }

    /**
     * @param string $wvId
     *
     * @return mixed
     */
    public function updateCanceled($wvId)
    {
        $sqlOdrCanceled = "(SELECT COUNT(1) FROM odr_hdr WHERE odr_hdr.wv_id = wv_hdr.wv_id AND deleted = 0) = 0";
        $sqlWvPickedStr = "(SELECT COUNT(1) FROM wv_dtl WHERE wv_dtl.wv_id = wv_hdr.wv_id AND wv_dtl.act_piece_qty != 0 AND deleted = 0) = 0";
        return \DB::table('wv_hdr')
            ->where('wv_hdr.wv_id', $wvId)
            ->where('wv_hdr.deleted', 0)
            ->whereRaw(DB::Raw($sqlOdrCanceled))
            ->whereRaw(DB::Raw($sqlWvPickedStr))
            ->update([
                'wv_hdr.wv_sts'     => Status::getByValue("Canceled", "WAVEPICK-STATUS"),
                'wv_hdr.updated_at' => time(),
                'wv_hdr.updated_by' => Data::getCurrentUserId(),
            ]);
    }


    public function getWaveList($attributes, array $with, $limit = 100) {
        $query = $this->make($with);

        $query->where('wv_hdr.whs_id', (int)$attributes['whs_id']);
        $query->where('wv_hdr.picker', $attributes['picker']);

        $query->whereIn('wv_sts', [
            Status::getByValue("New", "WAVEPICK-STATUS"),
            Status::getByValue("Picking", "WAVEPICK-STATUS")
        ]);

        // WMS2-4316 remove wavepick header when all wavepick detail is picked
        $sql = "wv_id in (select wv_id from wv_dtl where wv_dtl_sts in ('NW', 'PK') and wv_dtl.deleted = 0)";
        $query->whereRaw(\DB::raw($sql));

        $sqlOdrCanceled = "(SELECT COUNT(1) FROM odr_hdr WHERE odr_hdr.wv_id = wv_hdr.wv_id AND deleted = 0) != 0";
        $query->whereRaw(\DB::raw($sqlOdrCanceled));

        return $query->paginate($limit);
    }

    public function getNumOfOrders($wvId){
        $count = \DB::table('odr_hdr')->where('wv_id', $wvId)->count();
        return $count;
    }

    public function getNumOfSkus($wvId){
        $count = \DB::table('wv_dtl')->where('wv_id', $wvId)->count();
        return $count;
    }

}
