<?php

namespace App\Api\V1\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\XferHdr;
use Seldat\Wms2\Utils\Status;

class ListWxferTransformer extends TransformerAbstract
{

    /**
     * @param XferHdr $xferHdr
     *
     * @return array
     */
    public function transform(XferHdr $xferHdr)
    {
        $cus_info = DB::table('customer')->where('cus_id', $xferHdr->cus_id)->select('cus_name')->first();
        $xferDtl = DB::table('xfer_dtl')->where('xfer_hdr_id', $xferHdr->xfer_hdr_id)->first();

        $nw_loc = (!is_null(array_get($xferDtl, 'act_mez_loc', null)))
            ? array_get($xferDtl, 'act_mez_loc', null)
            : array_get($xferDtl, 'mez_loc', null);

        $nw_loc_id = (!is_null(array_get($xferDtl, 'act_mez_loc_id', null)))
            ? array_get($xferDtl, 'act_mez_loc_id', null)
            : array_get($xferDtl, 'mez_loc_id', null);

        return [
            'xfer_hdr_num'  => object_get($xferHdr, 'xfer_hdr_num', null),
            'xfer_sts'      => $xferHdr->xfer_sts,
            'xfer_hdr_id'   => object_get($xferHdr, 'xfer_hdr_id', null),
            'xfer_sts_name' => Status::getByKey("XFER-STATUS", $xferHdr->xfer_sts),
            'cus_id'        => $xferHdr->cus_id,
            'cus_name'      => array_get($cus_info, 'cus_name', null),
            'item_id'       => $xferHdr->item_id,
            'cus_upc'       => $xferHdr->cus_upc,
            'sku'           => $xferHdr->sku,
            'lot'           => $xferHdr->lot,
            'size'          => $xferHdr->size,
            'color'         => $xferHdr->color,
            'pieces'        => $xferHdr->piece_qty,
            'nw_loc'        => $nw_loc,
            'nw_loc_id'     => $nw_loc_id
        ];
    }
}
