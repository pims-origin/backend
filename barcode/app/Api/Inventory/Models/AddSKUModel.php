<?php

namespace App\Api\Inventory\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\User;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use App\Api\Inventory\Models\CycleHdrModel;
use Wms2\UserInfo\Data;

class AddSKUModel extends AbstractModel
{

    const STATUS_CYCLEDTL_NEW = 'NW';
    const STATUS_CYCLEDTL_OPEN = 'OP';
    const STATUS_CYCLEDTL_NOT_APPLICABLE = 'NA';
    const STATUS_CYCLEDTL_RECOUNT = 'RC';
    const STATUS_CYCLEDTL_ACCEPTED = 'AC';
    const STATUS_INSERT = 'I';
    const STATUS_UPDATE = 'U';
    const STATUS_DELETE = 'D';

    const CARTON_STATUS_ACTIVE = 'AC';
    const CARTON_STATUS_LOCK = 'CL';

    static $cycleCountInfo = [];
    /**
     * @var array
     */

    protected $arrNumberFields = [
        'cycle_hdr_id',
        'cycle_dtl_id'
    ];

    /**
     * @var array
     */

    protected $arrStringFields = [
        'sku',
        'size',
        'color'
    ];

    /**
     * AddSKUModel constructor.
     *
     * @param CycleDtl $model
     */

    public function __construct(CycleDtl $model)
    {
        $this->model = $model ? $model : new CycleDtl();
    }

    /**
     * @param $post
     *
     * @return bool|static
     */

    public function processAddSKU($post)
    {
        $cycleDtl = new CycleDtl();
        $validate = $this->validateDataInput($post);
        //$locTypeRacId = LockModel::getLocTypeId();

        if (!$validate) {
            return false;
        }
        try {
            DB::beginTransaction();

            $model = $cycleDtl->create([
                'cycle_hdr_id'  => $post['cycle_hdr_id'],
                'whs_id'        => $post['whs_id'],
                'cus_id'        => $post['cus_id'],
                'item_id'       => $post['item_id'],
                'sku'           => $post['sku'],
                'size'          => $post['size'],
                'color'         => $post['color'],
                'lot'           => array_get($post, 'lot', 'NA'),
                'pack'          => $post['pack'],
                'remain'        => $post['remain'],
                'is_new_sku'    => 1,
                'sys_qty'       => 0,
                'act_qty'       => $post['act_qty'],
                'sys_loc_id'    => $post['act_loc_id'],
                'sys_loc_name'  => $post['act_loc'],
                'act_loc_id'    => $post['act_loc_id'],
                'act_loc_name'  => $post['act_loc'],
                'cycle_dtl_sts' => self::STATUS_CYCLEDTL_NEW,
                'sts'           => self::STATUS_INSERT
            ]);

            DB::commit();

            return $model;
        } catch (\Exception $e) {
            \DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function validateDataInput(&$post)
    {
        self::$cycleCountInfo = $this->getCycleCountInfo($post['cycle_hdr_id']);

        $result = $this->getLocationIDByWarehouse($post['act_loc'], $post['whs_id']);

        if (!$result) {
            $this->errors['act_loc_name'] = Message::get('BM066', 'Location ' . $post['act_loc'], 'warehouse');

            return false;
        }

        $post['act_loc_id'] = $result[0]['loc_id'];

        //get item_id
        $itemID = $this->getItemID($post);

        if (!$itemID) {
            $this->errors['sku'] = Message::get('BM067', 'Item');

            return false;
        }

        if ($post['act_qty'] <= 0) {
            $this->errors['sku'] = "Actual quantity have to greater than 0";

            return false;
        }

        $post['item_id'] = $itemID[0]['item_id'];

        // Check Item has exists on the another cycle count
        $itemModel = new ItemModel();
        $itemExistCycleOrBlock = $itemModel->checkItemExistOnAnotherCycleOrBlock(
            $post['whs_id'], $post['cycle_hdr_id'], $post['item_id']);

        if (!$itemExistCycleOrBlock['status']) {
            $this->errors['item'] = 'SKU: ' . $post['sku'] . ' existed on the other' . $itemExistCycleOrBlock['type'];

            return false;
        }

        // Check count items is already exist in count items.
        $skuResults = $this->checkForExistingSKU($post);

        if ($skuResults) {
            $this->errors['act_loc_name'] = Message::get('BM068',
                $post['sku'], $post['pack'], $post['remain'], $post['act_loc']);

            return false;
        }

        // check sku have carton (all status).
        $result = $this->checkNewSKUHaveCarton($post);

        if ($result) {
            $this->errors['sku'] = 'SKU ' . $post['sku'] . ' existed in location ' . $post['act_loc'];

            return false;
        }

        if ($post['cycle_type'] == CycleHdrModel::CYCLE_TYPE_LOCATION) {

            // Check location in range?
            if (!in_array($post['act_loc_id'], explode(',', self::$cycleCountInfo['cycle_detail']))) {
                $this->errors['act_loc'] = Message::get('BM069', $post['act_loc']);

                return false;
            }

            // Check sku was cycled in other report
            $results = $this->checkSKUAlreadyExistOtherReport($post);

            if ($results['hasLock'] && !$results['isValid']) {
                $this->errors['sku'] = Message::get('BM070', $post['sku']);

                return false;
            }
        }

        return true;
    }

    /**
     * @param $loc
     * @param $wshID
     *
     * @return bool
     */

    public function getLocationIDByWarehouse($loc, $wshID)
    {
        return Location::select('loc_id')
            ->where('loc_code', $loc)
            ->where('loc_whs_id', $wshID)
            ->get()
            ->toArray();
    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function checkForExistingSKU($post)
    {
        return CycleDtl::where('cycle_hdr_id', $post['cycle_hdr_id'])
            ->where('item_id', $post['item_id'])
            ->where('cus_id', $post['cus_id'])
            ->where('act_loc_id', $post['act_loc_id'])
            ->where('pack', $post['pack'])
            ->where('remain', $post['remain'])
            ->count();
    }

    /**
     * @param $post
     *
     * @return mixed
     */

    public function checkNewSKUHaveCarton($post)
    {
        return Carton::where('item_id', $post['item_id'])
            ->where('cus_id', $post['cus_id'])
            ->where('loc_id', $post['act_loc_id'])
            ->where('deleted', '=', 0)
            ->exists();
    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function checkSKUAlreadyExistOtherReport($post)
    {
        $isValid = [];

        $results = $this->getCycleCountInfoHaveLockedCarton($post);

        foreach ($results as $row) {
            if (self::$cycleCountInfo['created_at'] < $row['created_at']) {
                $isValid[] = in_array($row['cycle_type'], [
                        CycleHdrModel::CYCLE_TYPE_SKU,
                        CycleHdrModel::CYCLE_TYPE_CUSTOMER
                    ]) || $row['cycle_type'] = CycleHdrModel::CYCLE_TYPE_LOCATION;
            }
        }

        return [
            'hasLock' => $results ? true : false,
            'isValid' => in_array(true, $isValid)
        ];
    }

    /**
     * @param $post
     *
     * @return bool
     */

    public function getItemID($post)
    {
        return Item::select('item_id')
            ->where('sku', $post['sku'])
            ->where('size', $post['size'])
            ->where('color', $post['color'])
            ->get()
            ->toArray();
    }

    /**
     * @param $post
     *
     * @return mixed
     */

    public function getCycleCountInfoHaveLockedCarton($post)
    {
        return Carton::select(
            'cycle_hdr.cycle_hdr_id',
            'cycle_hdr.cycle_type',
            'cycle_hdr.created_at'
        )
            ->join('cycle_dtl', 'cartons.item_id', '=', 'cycle_dtl.item_id')
            ->join('cycle_hdr', 'cycle_dtl.cycle_hdr_id', '=', 'cycle_hdr.cycle_hdr_id')
            ->where('cartons.item_id', '=', $post['item_id'])
            ->where('cycle_dtl.cus_id', '=', $post['cus_id'])
            ->where('cycle_hdr.cycle_hdr_id', '!=', $post['cycle_hdr_id'])
            ->where('cartons.ctn_sts', '=', self::CARTON_STATUS_LOCK)
            ->groupBy('cycle_hdr.cycle_hdr_id')
            ->get()
            ->toArray();
    }

    /**
     * @param $data
     *
     * @return bool
     */

    public function getInventoryCarton($data)
    {
        $cycleHdrModel = new CycleHdrModel();

        $cycleDetail = $data['cycle_type'] == CycleHdrModel::CYCLE_TYPE_CUSTOMER ? [$data['cus_id']]
            : $data['cycle_type'] == CycleHdrModel::CYCLE_TYPE_SKU ? [$data['sku']]
                : explode(',', self::$cycleCountInfo['cycle_detail']);

        $cartonData = $cycleHdrModel->getCycleData(
            $data['whs_id'],
            $data['cycle_type'],
            $cycleDetail
        );

        if (self::$cycleCountInfo['has_color_size']) {
            $hasColorSize = $cycleHdrModel->checkHasColorSize($cartonData);
            if ($hasColorSize) {
                $this->errors['sku'] = Message::get('BM071', implode(',', array_keys($hasColorSize)));

                return false;
            }
        }

        return $cartonData;
    }

    /**
     * @param $cycleID
     *
     * @return mixed
     */

    public function getCycleCountInfo($cycleID)
    {
        return CycleHdr::where('cycle_hdr_id', $cycleID)->first()->toArray();
    }
}