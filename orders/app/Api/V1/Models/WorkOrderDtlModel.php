<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use \Seldat\Wms2\Models\WorkOrderDtl;
use \Seldat\Wms2\Models\WorkOrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class WorkOrderDtlModel extends AbstractModel
{
    protected $workOrderHdr;

    /**
     * @param WorkOrderDtl $model
     */
    public function __construct(WorkOrderDtl $model = null)
    {
        $this->model = ($model) ?: new WorkOrderDtl();
        $this->workOrderHdr = new WorkOrderHdr();
    }

    /**
     * @param $wvIds
     *
     * @return mixed
     */
    public function countSku($workOrderIds)
    {
        $skuCount = $this->model
            ->distinct('item_id')
            ->whereIn('wo_hdr_id', $workOrderIds)
            ->count(['item_id']);

        return $skuCount;
    }


}

