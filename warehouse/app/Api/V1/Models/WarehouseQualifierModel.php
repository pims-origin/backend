<?php

namespace App\Api\V1\Models;



use Seldat\Wms2\Models\WarehouseQualifier;

class WarehouseQualifierModel
{
    public function __construct(WarehouseQualifier $model = null)
    {
        $this->model = ($model) ?: new WarehouseQualifier();
    }

    public function getDataByQualifier($qualifier)
    {
        $query = $this->model->where('qualifier', $qualifier);
        $this->model->filterData($query);

        return $query->first();
    }
}
