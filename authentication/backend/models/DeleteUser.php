<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/18/2016
 * Time: 2:23 PM
 */

namespace backend\models;

use common\models\User;
use yii\db\Query;
use yii\data\Pagination;
use yii\helpers\VarDumper;

class DeleteUser
{
    public function deleteUser(User $user)
    {
        $user->status = User::STATUS_DELETED;
        $user->deleted_at = time();
        $result = $user->save();

        return $result;
    }

    public function deleteMultiple($conditions)
    {
        $attributes = [
            'status'     => User::STATUS_DELETED,
            'deleted_at' => time()
        ];

        return User::updateAll($attributes, $conditions);
    }

}
