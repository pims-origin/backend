<?php

namespace App\Jobs;

use DB;
use Wms2\UserInfo\Data;

class GoodsReceiptRecevingJob extends IMSJob
{
    protected $grHdrId;

    public function __construct($grHdrId)
    {
        $this->grHdrId = $grHdrId;
    }

    public function handle()
    {
        $grHdr = DB::table('gr_hdr')->where('gr_hdr_id', $this->grHdrId)->first();
        $rmaNumber = DB::table('asn_hdr')
            ->where('deleted', 0)
            ->where('asn_hdr_id', $grHdr['asn_hdr_id'])
            ->where('asn_type', 'RMA')
            ->value('asn_hdr_num');

        try {
            $token = $this->login();

            $endpoint = env('IMS_GR_RECEIVING');

            // Check IMS Configuration
            if(empty($endpoint)) {
                $this->writeLogment('IMS Goods Receipt Receiving ' . $grHdr['gr_hdr_num'], 'IMS Goods Receipt Receiving Endpoint is not define!');
                return false;
            }

            $items = DB::table('gr_dtl')
                        ->join('item', 'item.item_id', '=', 'gr_dtl.item_id')
                        ->join('asn_dtl', 'asn_dtl.asn_dtl_id', '=', 'gr_dtl.asn_dtl_id')
                        ->where('gr_dtl.gr_hdr_id', '=', $grHdr['gr_hdr_id'])
                        ->select(
                            'gr_dtl.po', 
                            'item.sku', 
                            'item.size', 
                            'item.color', 
                            'item.description', 
                            'item.uom_code as uom', 
                            'item.pack',
                            'item.cus_upc',
                            'item.weight',
                            'item.length',
                            'item.width',
                            'item.height',
                            'item.itm_img',
                            'gr_dtl.gr_dtl_act_qty_ttl as actual_qty', 
                            'gr_dtl.gr_dtl_act_ctn_ttl as actual_ctns', 
                            'gr_dtl.gr_dtl_dmg_ttl as damaged_qty', 
                            'gr_dtl.expired_dt as expired_date',
                            'gr_dtl.lot',
                            'asn_dtl.prod_line',
                            'asn_dtl.cmp'
                        )
                        ->get();


            $items = $this->itemsTransform($items);

            $data = [
                'type' =>  'goods-receipts',
                'id' =>  $grHdr['gr_hdr_id'],
                'cus_id' => $grHdr['cus_id'],
                'whs_id' => $grHdr['whs_id'],
                'attributes'    =>  [
                    'gr_num'    =>  $grHdr['gr_hdr_num'],
                    'ref'       =>  $grHdr['ref_code'],
                    'ctnr_num'      =>  $grHdr['ctnr_num'],
                    'rma_num'   => $rmaNumber ? $rmaNumber : null,
                    'expected_date' =>  array_get($grHdr, 'gr_hdr_ept_dt') ? date('Y-m-d', array_get($grHdr, 'gr_hdr_ept_dt')) : null,
                    'actual_date'   =>  array_get($grHdr, 'gr_hdr_act_dt') ? date('Y-m-d', array_get($grHdr, 'gr_hdr_act_dt')) : null,
                    'created_at'    =>  date('Y-m-d', array_get($grHdr, 'created_at')),
                    'updated_at'    =>  date('Y-m-d', array_get($grHdr, 'updated_at')),
                    'items'         =>  $items
                ]
            ];

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-Type: application/json"
            ];

            $result = $this->call($endpoint, 'POST', json_encode(['data' => $data]), $header);

            $this->writeLogment('IMS Goods Receipt Receiving ' . $grHdr['gr_hdr_num'], 'URL: '. $endpoint .' --- Request: ' . json_encode(['data' => $data]) . ' --- Response: ' . $result);

        } catch (\Exception $e) {
            $this->writeLogment('IMS Goods Receipt Receiving ' . $grHdr['gr_hdr_num'], $e->getMessage());
        }
    }

    public function itemsTransform($data)
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = [
                "po"            => array_get($item, 'po', null),
                "po_date"       => array_get($item, 'po_date', null),
                "sku"           => array_get($item, 'sku', null),
                "size"          => array_get($item, 'size', null),
                "color"         => array_get($item, 'color', null),
                "description"   => array_get($item, 'description', null),
                "uom"           => array_get($item, 'uom', null),
                "pack"          => array_get($item, 'pack', null),
                "upc"           => array_get($item, 'cus_upc', null),
                "product_line"  => array_get($item, 'prod_line', null),
                "campaign"      => array_get($item, 'cmp', null),
                "weight"        => array_get($item, 'weight', null),
                "length"        => array_get($item, 'length', null),
                "width"         => array_get($item, 'width', null),
                "height"        => array_get($item, 'height', null),
                "lot"           => array_get($item, 'lot', null),
                "actual_qty"    => array_get($item, 'actual_qty', null),
                "actual_ctns"   => array_get($item, 'actual_ctns', null),
                "damaged_qty"   => array_get($item, 'damaged_qty', null),
                "damaged_ctns"  => array_get($item, 'damaged_ctns', null),
                "expired_date"  => array_get($item, 'expired_date') ? date('Y-m-d', array_get($item, 'expired_date')) : null,
                "image_url"     => env('APP_URL', null) . 'item-master/v1/view/' . array_get($item, 'itm_img', null)
            ];
        }
        return $result;
    }
}
