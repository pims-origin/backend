<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Models;

use App\MessageCode;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class CartonModel extends AbstractModel
{
    /**
     * CartonModel constructor.
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $this->model = ($model) ?: new Carton();
    }

    public function generateCtnNum()
    {
        $maxCarton = (new Carton())->where([
            'deleted'    => 0,
            'deleted_at' => 915148800
        ])->orderBy('ctn_id', 'desc')->first();
        if (!$maxCarton) {
            $today_ddmm = date('dm');
            $ctnNum = "CTN-{$today_ddmm}-0001";

            return $ctnNum;
        }

        $aNum = explode("-", $maxCarton->ctn_num);
        $aTemp = array_keys($aNum);
        $end = end($aTemp);
        $index = (int)$aNum[$end];
        $aNum[0] = "CTN";
        $aNum[$end] = str_pad(++$index, 5, "0", STR_PAD_LEFT);
        $ctnNum = implode("-", $aNum);

        return $ctnNum;
    }

    public function cloneCarton($carton)
    {
        $newCarton = (new Carton())->fill($carton->toArray());
        $newCarton->piece_remain = 0;
        $newCarton->origin_id = $carton['ctn_id'];
        $newCarton->rfid = null;
        $newCarton->ctn_num = $this->generateCtnNum();
        $newCarton->save();

        return $newCarton;
    }
}
