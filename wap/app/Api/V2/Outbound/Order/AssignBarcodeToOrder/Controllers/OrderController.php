<?php

namespace App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Controllers;

use App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models\EventTrackingModel;
use App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models\OrderCartonModel;
use App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models\OrderDtlModel;
use App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models\OrderHdrModel;
use App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models\CartonModel;
use App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models\WaveDtlModel;
use App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models\WaveHdrModel;
use App\Api\V2\Outbound\Order\AssignBarcodeToOrder\Models\WaveDtlLocModel;
use App\libraries\RFIDValidate;

use App\Jobs\AutoPackJob;
use Seldat\Wms2\Models\Carton;
use App\Api\V1\Models\Log;

use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;

class OrderController extends AbstractController
{

    protected $orderCartonModel;
    protected $orderDtlModel;
    protected $orderHdrModel;
    protected $cartonModel;
    protected $eventTrackingModel;
    protected $wavePickDtlModel;
    protected $wavePickHdrModel;
    protected $waveDtlLocModel;

    public function __construct()
    {
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->cartonModel = new CartonModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->wavePickDtlModel = new WaveDtlModel();
        $this->wavePickHdrModel = new WaveHdrModel();
        $this->waveDtlLocModel  = new WaveDtlLocModel();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $whsId
     * @param $odrId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function putBarcodeOrder($whsId, $odrId, Request $request)
    {
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/order/{$odrId}/cartons-without-rfid";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'APO',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Assign Pieces to Order with barcode type'
        ]);

        /*
         * end logs
         */

        $input['whs_id'] = $whsId;
        $input['odr_id'] = $odrId;

        $items = array_get($input, 'items');

        if (empty($items) || count($items) < 1) {
//            $msg = "Items array is required";
            $msg = "The input parameters are missing or incorrect!";
            return $this->_responseErrorMessage($msg);
        }

        $checkOdr = $this->orderHdrModel->getModel()
            ->where('whs_id', $whsId)
            ->where('odr_id', $odrId)
            // ->whereIn('odr_sts', ['NW', 'PK'])
            ->where('odr_sts', 'PK')
            ->first();

        if (empty($checkOdr)) {
            $errorMessage = sprintf("The order %s doesn't exist", $odrId);
            $checkOdr = $this->orderHdrModel->getFirstWhere([
                'whs_id' => $whsId,
                'odr_id' => $odrId,
            ]);

            if ($checkOdr) {
                $odrNum = object_get($checkOdr, 'odr_num');
                $odrSts = object_get($checkOdr, 'odr_sts');
                if ($odrSts == 'NW' || $odrSts == "AL") {
                    $errorMessage = sprintf("The order %s doesn't create wavepick yet", $odrNum);
                } else {
                    $errorMessage = sprintf("The order %s is %s", $odrNum, Status::getByKey('ORDER-STATUS', $odrSts));
                }
            } else {
                $checkOdr = $this->orderHdrModel->getFirstWhere([
                    'odr_id' => $odrId,
                ]);
                if ($checkOdr) {
                    $odrNum = object_get($checkOdr, 'odr_num');
                    $errorMessage = sprintf("The order %s doesn't belong to warehouse", $odrNum);
                }
            }
            return $this->_responseErrorMessage($errorMessage);
        }

        $itemTrackings = [];
        $dataInput = [];
        foreach ($items as $key => $item) {
            $odrDtlId = (int)array_get($item, 'odr_dtl_id');
            $pieceQty = (int)array_get($item, 'piece_qty');

            if (!$odrDtlId || !$pieceQty) {
                $msg = "Order detail Id and piece qty are required in each item";
                return $this->_responseErrorMessage($msg);
            }

            // Check piece qty integer and > 0
            if (!is_int($pieceQty) || $pieceQty < 1) {
                $msg = "Piece qty must be integer and greater than 0.";
                return $this->_responseErrorMessage($msg);
            }

            $pieceQtyTTl = $this->orderCartonModel->sumPieceQty($odrDtlId);
            $checkPieceQty = array_get($pieceQtyTTl, 'piece_ttl');

            if ($pieceQty > $checkPieceQty) {
                $msg = "The piece qty is not enough to assign!";
                if ($checkPieceQty == 0) {
                    $msg = "There is no any pieces qty. You must update Wavepick before doing this step!";
                }

                return $this->_responseErrorMessage($msg);
            }

            $wavepickdetail = $this->orderDtlModel->getWavepickDtlByOdrDtl($odrDtlId);
            $chkOdrId = array_get($wavepickdetail, 'odr_id');
            if ($odrId != $chkOdrId) {
                $msg = sprintf("The order detail %s is not belong to Order number %s", $odrDtlId, object_get($checkOdr, 'odr_num'));
                return $this->_responseErrorMessage($msg);
            }

            $limitQty = object_get($wavepickdetail, 'limit_qty', 0);
            if ($limitQty < $pieceQty) {
                $msg = sprintf("In order detail ID %s, the pieces qty %s is greater than allocated qty %s", $odrDtlId, $pieceQty, $limitQty);
                return $this->_responseErrorMessage($msg);
            }

            $dataInput[$odrDtlId]['piece_qty'] = $pieceQty;
            $dataInput[$odrDtlId]['whs_id']    = object_get($wavepickdetail, 'whs_id');
            $dataInput[$odrDtlId]['cus_id']    = object_get($wavepickdetail, 'cus_id');
            $dataInput[$odrDtlId]['wv_hdr_id'] = object_get($wavepickdetail, 'wv_id');
            $dataInput[$odrDtlId]['wv_num']    = object_get($wavepickdetail, 'wv_num');
            $dataInput[$odrDtlId]['wv_dtl_id'] = object_get($wavepickdetail, 'wv_dtl_id');
            $dataInput[$odrDtlId]['pack']      = object_get($wavepickdetail, 'pack');
            $dataInput[$odrDtlId]['lot']       = object_get($wavepickdetail, 'lot');
            $dataInput[$odrDtlId]['item_id']   = object_get($wavepickdetail, 'item_id');

            $itemTrackings[] = $wavepickdetail;
        }

        try {

            // start transaction
            DB::beginTransaction();

            $userId = Data::getCurrentUserId();
            $odrNum = object_get($checkOdr, 'odr_num');
            $wvStsFrom = null;
            foreach ($dataInput as $odrDtlId => $item) {
                $pieceQty = $item['piece_qty'];
                $wvDtlId  = $item['wv_dtl_id'];
                $pack     = $item['pack'];
                $itemId   = $item['item_id'];
                $lot      = $item['lot'];

                $odrCartons = $this->orderCartonModel->getCartonByWvDtl($wvDtlId);
                $pieceRemain = $pieceQty;
                foreach ($odrCartons as $key => $odrCarton) {
                    $ctnPieceQty = object_get($odrCarton, 'piece_qty', 0);
                    if ($pieceRemain < $ctnPieceQty) {
                        break;
                    }
                    $odrCarton->odr_hdr_id = $odrId;
                    $odrCarton->odr_dtl_id = $odrDtlId;
                    $odrCarton->odr_num    = $odrNum;
                    $odrCarton->updated_at = time();
                    $odrCarton->updated_by = $userId;
                    $odrCarton->save();

                    $pieceRemain -= $ctnPieceQty;
                }

                if ($pieceRemain > 0) {
                    // execution assign pieces remaining to order
                    $crossCtn = $this->_assignPieceRemaining($pieceRemain, $odrId, $odrNum, $odrDtlId, $wvDtlId);
                    // cross cartons between Wavepicks
                    if ($crossCtn > 0) {
                        // $msg = "The pieces quantity is not enough to assign pieces remaining";
                        // return $this->_responseErrorMessage($msg);
                        $result = $this->_assignPieceRemainingInCross($crossCtn, $odrId, $odrNum, $odrDtlId, $item);
                        if (is_array($result)) {
                            $wvStsFrom = $result;
                        }
                        if (($result > 0) && (is_numeric($result))) {
                            $msg = "The pieces quantity is not enough to assign pieces remaining";
                            return $this->_responseErrorMessage($msg);
                        }

                    }
                }

            }
            //Update Picking for order header
            $checkOdr->odr_sts = 'PK';
            $checkOdr->save();

            //update odr_sts to PD
            $dtlPicked = $this->orderDtlModel->updateOrderDetailPicked($odrId);
            if ($dtlPicked) {
                $dataTracking = [
                    'whs_id' => $whsId,
                    'cus_id' => $checkOdr->cus_id,
                    'owner' => $checkOdr->odr_num,
                    'evt_code' => 'WOD',
                    'trans_num' => $checkOdr->odr_num,
                ];
                foreach ($itemTrackings as $key => $itemTracking) {
                    $sku   = array_get($itemTracking, 'sku');
                    $size  = array_get($itemTracking, 'size');
                    $color = array_get($itemTracking, 'color');
                    $lot   = array_get($itemTracking, 'lot');
                    $dataTracking['info'] = sprintf('Wap - Assigning Carton(s) to Order Details %s - %s - %s - %s Completed', $sku, $size, $color, $lot);

                    $this->eventTrackingModel->eventTracking($dataTracking);
                }

            }

            // WMS2-4636 - Update picked_qty of order detail
            $this->orderDtlModel->updateOrderDetailPickedQty($odrId);

            $hdrPicked = $this->orderHdrModel->updateOrderPicked($odrId);
            if ($hdrPicked) {
                $this->eventTrackingModel->eventTracking([
                    'whs_id' => $whsId,
                    'cus_id' => $checkOdr->cus_id,
                    'owner' => $checkOdr->odr_num,
                    'evt_code' => 'WOH',
                    'trans_num' => $checkOdr->odr_num,
                    'info' => "Wap - Assigning Carton(s) to Order Completed"
                ]);
            }

            // update wave pick is picking when status is new
            $this->wavePickHdrModel->getModel()
                ->where('wv_id', $checkOdr->wv_id)
                ->where('wv_sts', 'NW')
                ->update(['wv_sts'=> 'PK']);

            // update wave pick completed
            $wvId = $checkOdr->wv_id;
            $wvSts = $this->wavePickHdrModel->updatePicked($wvId);
            // set status of both two wave pick header
            $dataWvSts[] = [
                'wv_id'  => $wvId,
                'picked' => $wvSts,
            ];
            if ($wvStsFrom) {
                $dataWvSts[] = $wvStsFrom;
            }

            DB::commit();

            //Call Auto Pack Job if pick successful
            if ($wvSts == 1) {
                dispatch(new AutoPackJob($wvId, $request));
            }

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['message'] = [
                'status_code' => 1,
                'msg' => sprintf("Successfully!")
            ];
            $data['wv_sts'] = $dataWvSts;
            $msg['data']   = $data;

            return $msg;

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return $this->_responseErrorMessage($this->getResponseData());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    private function _updateWvPickInCross($odrCtnFrom, $wvInfo, $odrNum, $pieceQty)
    {
        $wvHdrIdFrom = object_get($odrCtnFrom, 'wv_hdr_id');
        $wvHdrIdTo   = array_get($wvInfo, 'wv_hdr_id');

        $wvDtlIdFrom = object_get($odrCtnFrom, 'wv_dtl_id');
        $wvDtlIdTo   = array_get($wvInfo, 'wv_dtl_id');

        // $wvPieceQtyTo = $wvPieceQtyFrom = object_get($odrCtnFrom, 'piece_qty');
        $wvPieceQtyTo = $wvPieceQtyFrom = $pieceQty;

        $wvDtlFrom = $this->wavePickDtlModel->getFirstWhere(['wv_dtl_id' => $wvDtlIdFrom]);
        $wvDtlTo   = $this->wavePickDtlModel->getFirstWhere(['wv_dtl_id' => $wvDtlIdTo]);

        if ($wvDtlFrom->act_piece_qty < $wvPieceQtyFrom) {
            $msg = "Wave pick actual piece QTY [from] is not enough!";
            throw new \Exception($msg);
        }

        if (object_get($wvDtlTo, 'pack_size') != object_get($odrCtnFrom, 'pack')) {
            $msg = sprintf(
                'The Carton RFID %s pack size %d is not same pack size %d of Order %s!',
                object_get($odrCtnFrom, 'ctn_rfid'),
                object_get($odrCtnFrom, 'pack'),
                object_get($wvDtlTo, 'pack_size'),
                $odrNum
            );
            throw new \Exception($msg);
        }

        $userId = Data::getCurrentUserId();
        // wave pick [from]
        $wvDtlFrom->act_piece_qty -= $wvPieceQtyFrom;
        $wvDtlFrom->wv_dtl_sts = $wvDtlFrom->act_piece_qty >= $wvDtlFrom->piece_qty ? "PD" : "PK";
        $wvDtlFrom->updated_at = time();
        $wvDtlFrom->updated_by = $userId;

        $availChange     = 0;
        $allocatedChange = 0;
        // $remainingQty    = 0;
        // update wave pick [data]
        $wvDataArrFrom = json_decode($wvDtlFrom->data, true);
        if (!$wvDataArrFrom) {
            $wvDataArrFrom['avail']     = 0;
            $wvDataArrFrom['allocated'] = 0;
        }
        // over picking
        if ($wvDataArrFrom['avail'] > 0 || $wvDataArrFrom['allocated'] > 0) {
            // return available qty
            if ($wvDataArrFrom['avail'] > $wvPieceQtyFrom) {
                $availChange = $wvPieceQtyFrom;
            } elseif ($wvDataArrFrom['avail'] + $wvDataArrFrom['allocated']> $wvPieceQtyFrom) {
                $availChange     = $wvDataArrFrom['avail'];
                $allocatedChange = $wvPieceQtyFrom - $wvDataArrFrom['avail'];
            } else { // return over picking
                $allocatedChange = $wvDataArrFrom['allocated'];
                $availChange     = $wvDataArrFrom['avail'];
                // $remainingQty    = $wvPieceQtyFrom - ($allocatedChange + $availChange);
            }
        }

        $wvDataArrFrom['avail']     -= $availChange;
        $wvDataArrFrom['allocated'] -= $allocatedChange;
        $wvDtlFrom->data = json_encode($wvDataArrFrom);
        $wvDtlFrom->update();

        // wave pick [to]
        // over picking
        // update wave pick [data]
        $wvDataArrTo = json_decode($wvDtlTo->data, true);
        if (!$wvDataArrTo) {
            $wvDataArrTo['avail']     = 0;
            $wvDataArrTo['allocated'] = 0;
        }
        if ($wvDtlTo->act_piece_qty + $wvPieceQtyTo > $wvDtlTo->piece_qty) {
            // exist over picking
            if ($wvDataArrTo['avail'] > 0 || $wvDataArrTo['allocated'] > 0) {
                // $remainingQty == 0
                if ($availChange + $allocatedChange == $wvPieceQtyTo) {
                    $wvDataArrTo['avail']     += $availChange;
                    $wvDataArrTo['allocated'] += $allocatedChange;
                } else {
                    $wvDataArrTo['avail']     += $availChange;
                    $wvDataArrTo['allocated'] += $wvPieceQtyTo - $availChange;
                }
            } else { // a half over picking
                $overPicking = $wvDtlTo->act_piece_qty + $wvPieceQtyTo - $wvDtlTo->piece_qty;
                if ($overPicking > $availChange) {
                    $wvDataArrTo['avail']     = $availChange;
                    $wvDataArrTo['allocated'] = $overPicking - $allocatedChange;
                } else {
                    $wvDataArrTo['avail']     = $overPicking;
                    $wvDataArrTo['allocated'] = 0;
                }
            }
        }

        $wvDtlTo->data = json_encode($wvDataArrTo);
        $wvDtlTo->act_piece_qty += $wvPieceQtyTo;
        $wvDtlTo->wv_dtl_sts = $wvDtlTo->act_piece_qty >= $wvDtlTo->piece_qty ? "PD" : "PK";
        $wvDtlTo->updated_at = time();
        $wvDtlTo->updated_by = $userId;

        $wvDtlTo->save();

        //update wave header status
        $wvSts = $this->wavePickHdrModel->updatePicked($wvHdrIdFrom);

        return [
            'wv_id'  => $wvHdrIdFrom,
            'picked' => $wvSts,
        ];
    }

    private function _updateOdrCartonInCrossCartonCase($odrCtnFrom, $wvInfo)
    {
        $wvNumTo   = array_get($wvInfo, 'wv_num');
        $wvHdrIdTo = array_get($wvInfo, 'wv_hdr_id');
        $wvDtlIdTo = array_get($wvInfo, 'wv_dtl_id');

        $odrCtnFrom->wv_num    = $wvNumTo;
        $odrCtnFrom->wv_hdr_id = $wvHdrIdTo;
        $odrCtnFrom->wv_dtl_id = $wvDtlIdTo;
        $odrCtnFrom->updated_at = time();
        $odrCtnFrom->updated_by = Data::getCurrentUserId();
        $odrCtnFrom->save();
    }

    private function _getWvInfoByOdrId($odrCtnObj, $odrHdrObj)
    {
        $wvId   = object_get($odrHdrObj, 'wv_id');
        $wvNum  = object_get($odrHdrObj, 'wv_num');
        $itemId = object_get($odrCtnObj, 'item_id');

        $wvDtlObj = $this->wavePickDtlModel->getFirstWhere([
                                            "item_id" => $itemId,
                                            "wv_id"   => $wvId,
                                            ]);
        $wvDtlId = object_get($wvDtlObj, 'wv_dtl_id');

        if (!$wvDtlId) {
            $msg = sprintf("Carton RFID %s doesn't belong to Order %s!", object_get($odrCtnObj, 'ctn_rfid'), object_get($odrHdrObj, 'odr_num'));
            if (!$wvId) {
                $msg = sprintf("The Order %s need to be created wave pick before this step!", object_get($odrHdrObj, 'odr_num'));
            }
            throw new \Exception($msg);
        }

        return [
            "wv_hdr_id" => $wvId,
            "wv_dtl_id" => $wvDtlId,
            "wv_num"    => $wvNum,
        ];
    }

    private function _updateWvPickDtlLocInCross($odrCtnFrom, $wvInfo)
    {
        $wvDtlIdFrom = array_get($odrCtnFrom, 'wv_dtl_id');
        $ctnId       = array_get($odrCtnFrom, 'ctn_id');
        $pieceQty    = array_get($odrCtnFrom, 'piece_qty');
        $wvDtlIdTo   = array_get($wvInfo, 'wv_dtl_id');

        $wvDtlLocFrom = $this->waveDtlLocModel->getFirstwhere(['wv_dtl_id'=> $wvDtlIdFrom]);
        $wvDtlLocTo   = $this->waveDtlLocModel->getFirstwhere(['wv_dtl_id'=> $wvDtlIdTo]);

        $changeActLocData = null;
        $changeCartonData = null;
        if ($wvDtlLocFrom && $wvDtlLocFrom->act_loc_ids) {
            $actLocs = json_decode($wvDtlLocFrom->act_loc_ids, true);
            foreach ($actLocs as $keyLoc => $actLoc) {
                foreach ($actLoc['cartons'] as $keyCtn => $cartonOld) {
                    if ($cartonOld['ctn_id'] == $ctnId) {
                        $changeActLocData = $actLoc;
                        $changeCartonData = $cartonOld;
                        // remove carton
                        unset($actLocs[$keyLoc]['cartons'][$keyCtn]);
                        // update location
                        $actLocs[$keyLoc]['picked_qty'] -= $pieceQty;
                        break;
                    }
                }
                // update carton
                $actLocs[$keyLoc]['cartons'] = array_values($actLocs[$keyLoc]['cartons']);
                // update location
                if (count($actLocs[$keyLoc]['cartons']) == 0) {
                    // remove location
                    unset($actLocs[$keyLoc]);
                }
            }

            $wvDtlLocFrom->act_loc_ids = json_encode(array_values($actLocs));

            $wvDtlLocFrom->update();
        }

        if (!$changeActLocData || !$changeCartonData) {
            $msg = sprintf("Not found carton RFID %s in any location!", array_get($odrCtnFrom, 'ctn_id'));
            throw new \Exception($msg);
        }

        if ($wvDtlLocTo) {
            if ($wvDtlLocTo->act_loc_ids) {
                $actLocs = json_decode($wvDtlLocTo->act_loc_ids, true);
                $update = false;
                foreach ($actLocs as $keyLoc => $actLoc) {
                    if ($actLoc['loc_id'] == $changeActLocData['loc_id']) {
                        $update = true;
                        // update loc
                        $actLocs[$keyLoc]['cartons'][]   = $changeCartonData;
                        $actLocs[$keyLoc]['picked_qty'] += $pieceQty;
                        break;
                    }
                }

                if (!$update) {
                    $changeActLocData['cartons']    = [$changeCartonData];
                    $changeActLocData['picked_qty'] = $pieceQty;
                    $actLocs[] = $changeActLocData;
                }
                $wvDtlLocTo->act_loc_ids = json_encode($actLocs);
            } else {

                $changeActLocData['cartons']    = [$changeCartonData];
                $changeActLocData['picked_qty'] = $pieceQty;

                $wvDtlLocTo->act_loc_ids = json_encode([$changeActLocData]);
            }

            $wvDtlLocTo->update();
        }
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

    private function _assignPieceRemaining(
        $pieceRemain,
        $odrId,
        $odrNum,
        $odrDtlId,
        $wvDtlId)
    {
        $odrCartons = $this->orderCartonModel->getCartonByWvDtl($wvDtlId);

        if (count($odrCartons) == 0) {
            return $pieceRemain;
        }

        foreach ($odrCartons as $key => $odrCarton) {
            $ctnPieceQty = object_get($odrCarton, 'piece_qty', 0);

            if ($pieceRemain <= 0) {
                break;
            }
            if ($ctnPieceQty > $pieceRemain) {
                // pick pieces
                $ctnId = object_get($odrCarton, 'ctn_id');
                $carton = $this->cartonModel->getFirstwhere(['ctn_id' => $ctnId])->toArray();
                $carton['picked_qty'] = $pieceRemain;

                $newCarton = $this->_updatePickedPieceCarton($carton);

                $this->_updateOdrCartonWithPieceCarton($odrCarton, $newCarton, $carton, $odrId, $odrDtlId, $odrNum);
            } else {
                $this->orderCartonModel->updateWhere(
                    [
                        'odr_hdr_id' => $odrId,
                        'odr_dtl_id' => $odrDtlId,
                        'odr_num'    => $odrNum,
                    ],
                    [
                        'odr_ctn_id' => object_get($odrCarton, 'odr_ctn_id')
                    ]
                );
            }
            $pieceRemain -=$ctnPieceQty;
        }

        if ($pieceRemain > 0) {
            return $pieceRemain;
        }

        // success
        return 0;
    }

    private function _updatePickedPieceCarton($carton)
    {
        $remainQty = $carton['piece_remain'] - $carton['picked_qty'];
        $res = DB::table('cartons')->where('ctn_id', $carton['ctn_id'])->update(
            ['piece_remain' => $remainQty]
        );

        //change business clone new carton
        $origCtnId = $carton['ctn_id'];
        $origLocId = $carton['loc_id'];
        $maxCtnNum = $this->cartonModel->getMaxCtnNum($carton['ctn_num']);
        unset($carton['ctn_id']);

        // $carton['inner_pack'] = 0;

        $newCarton = (new Carton())->fill($carton);
        $newCarton->piece_remain = $carton['picked_qty'];
        $newCarton->origin_id = $origCtnId;
        $newCarton->loc_id = $newCarton->loc_code = null;
        $newCarton->ctn_sts = 'PD';
        $newCarton->picked_dt = time();
        $newCarton->ctn_num = ++$maxCtnNum;
        $newCarton->save();

        $return = $newCarton->toArray();
        $return['pick_piece'] = true;
        $return['loc_id'] = $origLocId;
        $return['loc_code'] = $return['loc_name'];
        $return["picked_qty"] = $carton['picked_qty'];

        return $return;
    }

    private function _updateOdrCartonWithPieceCarton(
        $odrCarton,
        $newCarton,
        $oldCarton,
        $odrId,
        $odrDtlId,
        $odrNum,
        $item = null
    )
    {
        $pieceRemain = array_get($oldCarton, 'piece_remain') - array_get($newCarton, 'piece_remain');

        $newOdrCarton = $odrCarton->toArray();
        unset($newOdrCarton['odr_ctn_id']);
        $newOdrCarton['piece_qty']  = $newCarton['piece_remain'];
        $newOdrCarton['ctn_id']     = $newCarton['ctn_id'];
        $newOdrCarton['ctn_num']    = $newCarton['ctn_num'];
        $newOdrCarton['odr_hdr_id'] = $odrId;
        $newOdrCarton['odr_dtl_id'] = $odrDtlId;
        $newOdrCarton['odr_num']    = $odrNum;
        if ($item) {
            $newOdrCarton['wv_num']    = array_get($item, 'wv_num');
            $newOdrCarton['wv_hdr_id'] = array_get($item, 'wv_hdr_id');
            $newOdrCarton['wv_dtl_id'] = array_get($item, 'wv_dtl_id');
        }

        $odrCtnId = DB::table('odr_cartons')->insertGetId($newOdrCarton);

        // update old order cartons
        $odrCarton->piece_qty = $pieceRemain;
        $odrCarton->ctn_id    = array_get($oldCarton, 'ctn_id');
        $odrCarton->ctn_num   = array_get($oldCarton, 'ctn_num');

        $odrCarton->save();

        // return $this->orderCartonModel->getFirstWhere(['odr_ctn_id' => $odrCtnId]);
    }

    private function _assignPieceRemainingInCross(
        $crossCtn,
        $odrId,
        $odrNum,
        $odrDtlId,
        $item)
    {
        $whsId     = array_get($item, 'whs_id');
        $cusId     = array_get($item, 'cus_id');
        $wvNumTo   = array_get($item, 'wv_num');
        $wvHdrIdTo = array_get($item, 'wv_hdr_id');
        $wvDtlIdTo = array_get($item, 'wv_dtl_id');
        $pack      = array_get($item, 'pack');
        $lot       = array_get($item, 'lot');
        $itemId    = array_get($item, 'item_id');
        $wvStsFrom = null;

        $odrCartons = $this->orderCartonModel->getCartonNotByWvDtl($item, $whsId, $cusId);

        if (count($odrCartons) == 0) {
            return $crossCtn;
        }

        $pieceRemain = $crossCtn;
        foreach ($odrCartons as $key => $odrCarton) {
            $ctnPieceQty = object_get($odrCarton, 'piece_qty', 0);

            if ($pieceRemain <= 0) {
                break;
            }



            if ($ctnPieceQty > $pieceRemain) {
                // update actual qty of wave pick
                $wvStsFrom = $this->_updateWvPickInCross(
                    $odrCarton,
                    [
                        'wv_hdr_id' => $wvHdrIdTo,
                        'wv_dtl_id' => $wvDtlIdTo,
                    ],
                    $odrNum,
                    $pieceRemain
                );
                // pick pieces
                $ctnId = object_get($odrCarton, 'ctn_id');
                $carton = $this->cartonModel->getFirstwhere(['ctn_id' => $ctnId])->toArray();
                $carton['picked_qty'] = $pieceRemain;

                $newCarton = $this->_updatePickedPieceCarton($carton);

                $this->_updateOdrCartonWithPieceCarton($odrCarton, $newCarton, $carton, $odrId, $odrDtlId, $odrNum, $item);

            } else {
                // update actual qty of wave pick
                $wvStsFrom = $this->_updateWvPickInCross(
                    $odrCarton,
                    [
                        'wv_hdr_id' => $wvHdrIdTo,
                        'wv_dtl_id' => $wvDtlIdTo,
                    ],
                    $odrNum,
                    $ctnPieceQty
                );

                $odrCarton->odr_hdr_id = $odrId;
                $odrCarton->odr_dtl_id = $odrDtlId;
                $odrCarton->odr_num    = $odrNum;
                $odrCarton->wv_num     = $wvNumTo;
                $odrCarton->wv_hdr_id  = $wvHdrIdTo;
                $odrCarton->wv_dtl_id  = $wvDtlIdTo;
                $odrCarton->updated_at = time();
                $odrCarton->updated_by = Data::getCurrentUserId();
                $odrCarton->save();

            }

            $pieceRemain -= $ctnPieceQty;
        }

        if ($pieceRemain > 0) {
            return $pieceRemain;
        }

        return $wvStsFrom;
    }
}