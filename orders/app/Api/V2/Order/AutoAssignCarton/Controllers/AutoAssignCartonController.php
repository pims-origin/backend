<?php

namespace App\Api\V2\Order\AutoAssignCarton\Controllers;

use App\Api\V2\Order\AutoAssignCarton\Models\CartonModel;
use App\Api\V2\Order\AutoAssignCarton\Models\EventTrackingModel;
use App\Api\V2\Order\AutoAssignCarton\Models\OrderCartonModel;
use App\Api\V2\Order\AutoAssignCarton\Models\OrderDtlModel;
use App\Api\V2\Order\AutoAssignCarton\Models\OrderHdrModel;
use App\Api\V2\Order\AutoAssignCarton\Models\WavePickDtlModel;
use App\Api\V2\Order\AutoAssignCarton\Models\WavePickHdrModel;
use App\Jobs\AssignOverPickingCartonToProcessingLocation;
use App\Jobs\AutoAssignRfidsJob;
use App\Jobs\AutoAssignOverPickingRfidsJob;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;
use mPDF;
use GuzzleHttp\Client;

/**
 * Class AutoAssignCartonController
 *
 * @package App\Api\V2\Order\AutoAssignCarton\Controllers
 */
class AutoAssignCartonController extends AbstractController
{
    const PREFIX_CARTON_RFID_CYCLE_COUNT = 'CCTC';

    protected $cartonModel;
    protected $orderCartonModel;
    protected $orderHdrModel;
    protected $orderDtlModel;
    protected $wavePickHdrModel;

    /**
     * AutoAssignCartonController constructor.
     *
     */
    public function __construct() {
        $this->cartonModel      = new CartonModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderHdrModel    = new OrderHdrModel();
        $this->orderDtlModel    = new OrderDtlModel();
        $this->wavePickHdrModel    = new WavePickHdrModel();
    }

    /**
     * @param $whsId
     * @param $odrId
     * @param $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function autoAssign($whsId, $wvId, Request $request)
    {
        $input = $request->getParsedBody();

        try {
            $wvObj = $this->wavePickHdrModel->getFirstWhere(['wv_id' => $wvId]);
            if (!$wvObj) {
                $msg = sprintf("Wave Id %s does not exist.", $wvId);
                    throw new \Exception($msg);
            }
            $isOverpicking = $this->_isOverpicking($wvId);
            $isAutoAssign  = $this->_isAutoAssignCartonsToOrder($wvId);
            if (!$isAutoAssign) {
                $msg = sprintf("The Order does not support auto assign cartons");
                throw new \Exception($msg);
            }

            // WMS2-5615 - Processing when overpicking
            if ($isAutoAssign) {
                DB::beginTransaction();
                DB::setFetchMode(\PDO::FETCH_ASSOC);
                // 1. Check If only one Order per wave pick
                // 2. Adjust all RFID cartons to be adjusted
                // 3. Generate Fake RFID cartons and assign to order

                $odrCartons = $this->orderCartonModel->getModel()
                ->where('wv_hdr_id', $wvId)
                ->whereNull('odr_hdr_id')
                ->whereNotNull('ctn_rfid')
                ->get();

                $rfids = array_pluck($odrCartons->toArray(), 'ctn_rfid');
                if (!count($rfids)) {
                    $msg = sprintf("Just support RFID type.");
                        throw new \Exception($msg);
                }

                if ($isOverpicking) {
                    $this->_processOverPicking($whsId, $wvObj, $odrCartons, $rfids);
                }

                $odrCartons = $this->orderCartonModel->getModel()
                ->where('wv_hdr_id', $wvId)
                ->whereNull('odr_hdr_id')
                ->whereNotNull('ctn_rfid')
                ->get();

                $rfids = array_pluck($odrCartons->toArray(), 'ctn_rfid');

                DB::commit();

                dispatch(new AutoAssignRfidsJob($whsId, $wvId, $rfids, $request));
            }

            $odrHdr = $this->orderHdrModel->getModel()
                            ->where('wv_id', $wvId)
                            ->first();
            if (!$odrHdr){
                throw new \Exception("Order information not exist");
            }
            if ( $this->_isAutoAssignCartonsToOrder($wvId) && $this->_isOverpicking($wvId) &&
                    in_array($odrHdr->odr_sts, ['PD', 'PA', 'PN', 'PTG', 'PTD', 'RS', 'SS', 'SH'] )) {
                dispatch(new AssignOverPickingCartonToProcessingLocation($odrHdr->odr_id));
            }

            // WAP-6117
            dispatch(new AutoAssignOverPickingRfidsJob($whsId, $wvId, $rfids, $odrHdr->odr_id, $request));

            return $this->response->noContent()->setContent([
                        'status' => 'OK'
                    ])->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    private function _processOverPicking($whsId, $wvObj, $odrCartons, $rfids)
    {
        $cartonArr = $this->cartonModel->getModel()
            ->whereIn('rfid', $rfids)->get()->toArray();

        $ctnTtl = count($rfids);
        $dataCtnInserts = [];
        // rfid => object
        $odrCartonsTmp = [];
        foreach ($odrCartons as $odrCarton) {
            $odrCartonsTmp[$odrCarton->ctn_rfid] = $odrCarton;
        }

        for($i = 0; $i < $ctnTtl; $i++)
        {
            $realCarton = array_shift($cartonArr);
            $dataTmp    = $realCarton;
            $rfidChange = $realCarton['rfid'];
            $ctnNumChange = $realCarton['ctn_num'];
            $dataTmp['ctn_sts'] = 'AJ';
            $dataTmp['des']     = 'overpicking';
            unset($dataTmp['ctn_id']);
            $dataCtnInserts[]   = $dataTmp;

            $odrCarton  = array_get($odrCartonsTmp, $rfidChange);
            $realCarton['rfid'] = $this->_createCtnRfidFromWavepick($wvObj, $i + 1);
            $realCarton['ctn_num'] = $this->_createCtnNumFromWavepick($wvObj, $i + 1);
            $realCarton['ctn_sts'] = 'PD';
            $realCarton['des']     = 'overpicking';
            $this->cartonModel->updateWhere(
                $realCarton,
                ['ctn_id' => $realCarton['ctn_id']]
            );
            if ($odrCarton) {
                $odrCarton->ctn_rfid = $realCarton['rfid'];
                $odrCarton->save();
            }
        }
        if (count($dataCtnInserts)) {
            $chunkData = array_chunk($dataCtnInserts, 300);
            foreach ($chunkData as $data) {
                DB::table('cartons')->insert($data);
            }
        }
    }

    private function _createCtnRfidFromWavepick($wvDtlObj, $seq = 0)
    {
        //create CTN number by Wavepick number number
        $ctnCode = str_replace('WAV', self::PREFIX_CARTON_RFID_CYCLE_COUNT, $wvDtlObj->wv_num);
        $ctnCode = str_replace('-', '', $ctnCode);
        if (!$seq) {
            //get current carton number, count null is 0 so need add 1 for the first
            $seq = $this->orderCartonModel->where('wv_hdr_id', $wvDtlObj->wv_id)->count() + 1;
        }

        // max length of carton rfid is 24
        if (strlen($ctnCode) > 10) {
            $ctnCode = substr($ctnCode, 4 + strlen($ctnCode) - 20);
            $ctnCode = self::PREFIX_CARTON_RFID_CYCLE_COUNT . $ctnCode;
        }

        $ctnRfid = $ctnCode . 'O' . time(). str_pad($seq, 24 - 11 - strlen($ctnCode), "0", STR_PAD_LEFT);

        return $ctnRfid;
    }

    private function _createCtnNumFromWavepick($wvDtlObj, $seq = 0)
    {
        //create CTN number by Pallet number
        $cartonCode = str_replace('WAV', 'CTN', $wvDtlObj->wv_num);
        if (!$seq) {
            //get current carton number, count null is 0 so need add 1 for the first
            $seq = $this->orderCartonModel->where('wv_hdr_id', $wvDtlObj->wv_id)->count() + 1;
        }

        $ctnNum = $cartonCode . "-". "O" . "-" . time() . '-'. str_pad($seq, 4, "0", STR_PAD_LEFT);

        return $ctnNum;
    }

    private function _isAutoAssignCartonsToOrder($wvId)
    {
        $countOdr = $this->orderHdrModel->getModel()
            ->where('wv_id', $wvId)
            ->whereNull('org_odr_id')
            ->count();

        if ($countOdr <= 1) {
            return true;
        }
        return false;
    }

    private function _isOverpicking($wvId)
    {
        $countOverpick = DB::table('wv_dtl')
            ->where('wv_id', $wvId)
            ->where('piece_qty', '<', 'act_piece_qty')
            ->where('deleted', 0);

        if ($countOverpick) {
            return true;
        }
        return false;
    }
}
