<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 6/2/16
 * Time: 3:23 PM
 */

namespace Seldat\Wms2\Models;

use Illuminate\Database\Eloquent\Model;

class CusInUserModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cus_in_user';

}