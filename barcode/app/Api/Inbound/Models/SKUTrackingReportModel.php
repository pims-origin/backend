<?php
namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\ReportSkuTracking;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class SKUTrackingReportModel extends AbstractModel
{
    /**
     * SKUTrackingReportModel constructor.
     *
     * @param ReportSkuTracking|null $model
     */
    public function __construct(ReportSkuTracking $model = null)
    {
        $this->model = ($model) ?: new ReportSkuTracking();
    }


}
