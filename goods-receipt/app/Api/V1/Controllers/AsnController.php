<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\AsnMetaModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\LogModel;
use App\Api\V1\Models\UserMetaModel;
use App\Api\V1\Models\LpnModel;
use App\Api\V1\Traits\AsnControllerTrait;
use App\Api\V1\Transformers\AsnDetailPalletVtlCtnTransformer;
use App\Api\V1\Transformers\AsnDetailTransformer;
use App\Api\V1\Transformers\AsnDetailVtlCtnTransformer;
use App\Api\V1\Transformers\AsnListsCanSortTransformer;
use App\Api\V1\Transformers\AsnListsTransformer;
use App\Api\V1\Transformers\AsnTransformer;
use App\Api\V1\Transformers\ContainerTransformer;
use App\Api\V1\Transformers\LogsTransformer;
use App\Api\V1\Transformers\LpnTransformer;
use App\Api\V1\Validators\AsnValidator;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Utils\Export;
use Seldat\Wms2\Utils\Helper;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Validator;
use Wms2\UserInfo\Data;
use mPDF;

class AsnController extends AbstractController
{

    use AsnControllerTrait;

    /**
     * @var AsnHdrModel
     */
    protected $asnHdrModel;
    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;
    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var
     */
    protected $validator;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var asnMetaModel
     */
    protected $asnMetaModel;

    protected $userMetaModel;

    const NA = 'NA';
    const MODE_ADD    = 0;
    const MODE_UPDATE = 1;

    protected $lpnModel;

    /**
     * AsnController constructor.
     *
     * @param AsnValidator $validator
     * @param AsnHdrModel $asnHdrModel
     * @param AsnDtlModel $asnDtlModel
     * @param ItemModel $itemModel
     * @param EventTrackingModel $eventTrackingModel
     * @param WarehouseMetaModel $warehouseMetaModel
     */
    public function __construct(
        AsnValidator $validator,
        AsnHdrModel $asnHdrModel,
        AsnDtlModel $asnDtlModel,
        ItemModel $itemModel,
        EventTrackingModel $eventTrackingModel,
        LpnModel $lpnModel
    ) {
        $this->validator = $validator;
        $this->asnHdrModel = $asnHdrModel;
        $this->asnDtlModel = $asnDtlModel;
        $this->itemModel = $itemModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->asnMetaModel = new AsnMetaModel();
        $this->userMetaModel = new UserMetaModel();
        $this->lpnModel = new LpnModel();
    }

    /**
     * @SWG\Post(
     *     path="/asns",
     *     tags={"Asns"},
     *     summary="Create Asn",
     *     description="Create Asn",
     *     operationId="createAsns",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Asns object that needs to be added to the store",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/InputAsnsData")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Asns",
     *         @SWG\Schema(ref="#/definitions/AsnsData")
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *          @SWG\Schema(ref="#/definitions/ErrorModel")
     *      )
     * )
     *
     * Create Asns
     *
     * @param Request $request
     * @param ContainerModel $containerModel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(Request $request, ContainerModel $containerModel)
    {
        $input = $request->getParsedBody();
        $paramURL = $request->getQueryParams();
        $whs_id = isset($paramURL['queue-whs']) ? $paramURL['queue-whs'] : self::getCurrentWhsId();
        $asn_type = 'ASN';
        if(isset($paramURL['queue-whs'])) {
            $asn_type = 'WHS';
        }
        //validate data input
        $this->validator->validate($input);
        $asnDtlSKUs = array_pluck(array_get($input, 'details'), 'dtl_sku');
        if (empty($asnDtlSKUs)) {
            return $this->response()->errorBadRequest('Please select at least one SKU');
        }
        $arraySKUsBelongToCustomer = $this->itemModel->getItemsBelongToCustomer($input['cus_id'], $asnDtlSKUs);
        $arraySKUsNotBelongToCustomer = array_diff($asnDtlSKUs, $arraySKUsBelongToCustomer);
        if (! empty($arraySKUsNotBelongToCustomer)) {
            $stringSKUs = implode(', ', $arraySKUsNotBelongToCustomer);
            return $this->response()->errorBadRequest(sprintf('SKU(s): %s not belong to customer', $stringSKUs));
        }

        $asnNumAndSeq = $this->asnHdrModel->generateAsnNumAndSeq();
        $expDt = strtotime($input['exp_date']);
        // data for create
        $dataAsnHdr = [
            'asn_hdr_seq'    => $asnNumAndSeq['asn_seq'],
            //'asn_hdr_ept_dt' => $expDt,
            'asn_hdr_ept_dt' => 0,
            'asn_hdr_num'    => $asnNumAndSeq['asn_num'],
            'asn_hdr_ref'    => trim(strtoupper($input['asn_ref'])),
            'cus_id'         => intval($input['cus_id']),
            'whs_id'         => intval($whs_id),
            'asn_sts'        => config('constants.asn_status.NEW'),
            'asn_hdr_des'    => array_get($input, 'asn_hdr_des', ''),
            'asn_type'       => $asn_type
        ];

        $measure = $this->asnHdrModel->getDefaultMeasurement();

        $dataAsnHdr['sys_mea_code'] = $measure;

        try {
            DB::beginTransaction();
            $errorDuplicateHrdNum = false;
            // try 3 times
            for ($i = 0; $i < 3; $i++) {
                if ($this->asnHdrModel->getFirstBy('asn_hdr_num', $dataAsnHdr['asn_hdr_num'])) {
                    // re-generate
                    $asnNumAndSeq = $this->asnHdrModel->generateAsnNumAndSeq();
                    $dataAsnHdr['asn_hdr_seq'] = $asnNumAndSeq['asn_seq'];
                    $dataAsnHdr['asn_hdr_num'] = $asnNumAndSeq['asn_num'];

                    $errorDuplicateHrdNum = true;
                    continue;
                } else {
                    $errorDuplicateHrdNum = false;
                    break;
                }
            }

            if ($errorDuplicateHrdNum) {
                throw new \Exception(Message::get("BM006", "ASN Num"));
            }

            // add asn header
            if (empty($asnHrd = $this->asnHdrModel->create($dataAsnHdr))) {
                throw new \Exception(Message::get("BM010"));
            }

            // create new container
            if (empty($container = $containerModel->getByNumber($input['ctnr_num']))) {
                $container = $containerModel->create(['ctnr_num' => $input['ctnr_num']]);
                $container->isNew = true;
            }

            // save ASN detail
            //$this->saveAsnDetails($asnHrd, $container, $input);
            $this->saveAsnDetails($asnHrd, $container, $input, 0, self::MODE_ADD);


            // update total
            $total_item = $this->saveTotalForAsn($asnHrd->asn_hdr_id);

            //add asn meta
            if (!empty($input['act_dt'])) {
                DB::table('asn_meta')->insert([
                    'asn_hdr_id' => $asnHrd->asn_hdr_id,
                    'qualifier'  => "GAD",
                    'value'      => $expDt
                ]);
            }

            // event tracking
            $this->eventTrackingModel->create([
                'whs_id'    => $whs_id,
                'cus_id'    => $dataAsnHdr['cus_id'],
                'owner'     => $asnNumAndSeq['asn_num'],
                'evt_code'  => config('constants.event.ASN-NEW'),
                'trans_num' => $asnNumAndSeq['asn_num'],
                'info'      => sprintf(config('constants.event-info.ASN-NEW'), $asnNumAndSeq['asn_num'])

            ]);

            DB::commit();
            $asnHrd->ctnr_id = $container->ctnr_id;
            $asnHrd->asn_hdr_itm_ttl = $total_item;

            return $this->response->item($asnHrd, new AsnTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (ValidationHttpException $e) {
            DB::rollBack();
            throw new ValidationHttpException($e->getErrors());
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/asns",
     *     tags={"Asns"},
     *     summary="Create Asn",
     *     description="Create Asn",
     *     operationId="createAsns",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Asns object that needs to be added to the store",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/InputAsnsData")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Asns",
     *         @SWG\Schema(ref="#/definitions/AsnsData")
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *          @SWG\Schema(ref="#/definitions/ErrorModel")
     *      )
     * )
     *
     * Create Asns
     *
     * @param Request $request
     * @param ContainerModel $containerModel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function correctData(Request $request)
    {
        $input = $request->getParsedBody();

        $cusId = intval(array_get($input, 'cus_id'));
        $asnDetail = array_get($input, 'details');
        $skuList = array_pluck($asnDetail, 'dtl_sku');

        $itemModel = new ItemModel();
        $itemList = $itemModel->getWhere($cusId, $skuList)->toArray();

        if(count($itemList) > 0) {

            foreach ($asnDetail as $key => &$detail) {

                $same = false;
                foreach ($itemList as &$item) {
                    if ($detail['dtl_sku'] == $item['sku']) {
                        $detail['dtl_itm_id'] = $item['item_id'];
                        $detail['dtl_des'] = $item['description'];
                        $detail['asn_dtl_sts'] = $item['status'];
                        $detail['dtl_size'] = $item['size'];
                        $detail['dtl_color'] = $item['color'];
                        $detail['dtl_uom_id'] = $item['uom_id'];
                        $detail['dtl_pack'] = $item['pack'];
                        $detail['dtl_length'] = number_format($item['length'], 2);
                        $detail['dtl_width'] = number_format($item['width'], 2);
                        $detail['dtl_height'] = number_format($item['height'], 2);
                        $detail['dtl_weight'] = number_format($item['weight'], 2);

                        $same = true;
                        continue;
                    }
                }

                if(!$same) unset($asnDetail[$key]);
            }

        } else {
            $asnDetail = [];
        }

        $input['details'] = array_values($asnDetail);

        return $input;
    }

    public function asnType(Request $request)
    {
        return [
            'data' => [
                [
                    'asn_type'=>"ASN",
                    'asn_type_name'=>"Normal"
                ],
                [
                    'asn_type'=>"WHS",
                    'asn_type_name'=>"Transfer Warehouse"
                ]
            ]
        ];
    }

    /**
     * Create Asns
     *
     * @param Request $request
     * @param ContainerModel $containerModel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function multiStore(Request $request, ContainerModel $containerModel)
    {
        $result = [
            'type' => false,
            'body' => [
                'message' => Message::get("BM010"),
                'data'    => []
            ]
        ];
        $input = $request->getParsedBody();
        $whs_id = intval($input['whs_id']);
        $asnNumAndSeq = $this->asnHdrModel->generateAsnNumAndSeq();
        // data for create
        $dataAsnHdr = [
            'asn_hdr_seq'    => $asnNumAndSeq['asn_seq'],
            'asn_hdr_ept_dt' => strtotime($input['exp_date']),
            'asn_hdr_num'    => $asnNumAndSeq['asn_num'],
            'asn_hdr_ref'    => $input['asn_ref'],
            'cus_id'         => intval($input['cus_id']),
            'whs_id'         => $whs_id,
            'asn_sts'        => config('constants.asn_status.NEW'),
            'asn_hdr_des'    => array_get($input, 'asn_hdr_des', ''),
            'sys_mea_code'   => $input['measurement_code'],
        ];

        try {

            DB::beginTransaction();

            $errorDuplicateHrdNum = false;
            // try 3 times
            for ($i = 0; $i < 3; $i++) {
                if ($this->asnHdrModel->getFirstBy('asn_hdr_num', $dataAsnHdr['asn_hdr_num'])) {
                    // re-generate
                    $asnNumAndSeq = $this->asnHdrModel->generateAsnNumAndSeq();
                    $dataAsnHdr['asn_hdr_seq'] = $asnNumAndSeq['asn_seq'];
                    $dataAsnHdr['asn_hdr_num'] = $asnNumAndSeq['asn_num'];

                    $errorDuplicateHrdNum = true;
                    continue;
                } else {
                    $errorDuplicateHrdNum = false;
                    break;
                }
            }

            if ($errorDuplicateHrdNum) {
                return [
                    'type' => false,
                    'body' => [
                        'message' => Message::get("BM006", "ASN Num"),
                        'data'    => ['asn_num' => $asnNumAndSeq['asn_num']]
                    ]
                ];
            }

            // add asn header
            if (empty($asnHrd = $this->asnHdrModel->create($dataAsnHdr))) {
                return [
                    'type' => false,
                    'body' => [
                        'message' => Message::get("BM010"),
                        'data'    => $dataAsnHdr
                    ]
                ];
            }
            foreach ($input['containers'] as $value) {
                // create new container
                if (empty($container = $containerModel->getByNumber($value['container']))) {
                    $containerModel->refreshModel();
                    $container = $containerModel->create(['ctnr_num' => $value['container']]);
                }
                // save ASN detail
                $result = $this->saveMultiAsnDetails($asnHrd, $container, $value);
                if (is_array($result) && $result['type'] == false) {
                    return $result;
                }

                // update total
                $total_item = $this->saveTotalForAsn($asnHrd->asn_hdr_id);

                // event tracking
                $this->eventTrackingModel->create([
                    'whs_id'    => $whs_id,
                    'cus_id'    => $dataAsnHdr['cus_id'],
                    'owner'     => $asnNumAndSeq['asn_num'],
                    'evt_code'  => config('constants.event.ASN-NEW'),
                    'trans_num' => $asnNumAndSeq['asn_num'],
                    'info'      => sprintf(config('constants.event-info.ASN-NEW'), $asnNumAndSeq['asn_num'])

                ]);

            }

            //#5980 get  minimum asn_dtl_ept_dt
            $minEpDate = DB::table('asn_dtl')->select('asn_dtl_ept_dt')
                                        ->where('asn_hdr_id', $asnHrd->asn_hdr_id)
                                        ->min('asn_dtl_ept_dt');
            //update asn_hdr_ept_dt with minimum asn_dtl_ept_dt
            if (!empty($minEpDate)) {
                DB::table('asn_hdr')
                    ->where('asn_hdr_id', $asnHrd->asn_hdr_id)
                    ->update(['asn_hdr_ept_dt' => $minEpDate]);
            }

            $result = [
                'type' => true,
                'body' => [
                    'message' => 'Your Asn has been created successfully',
                    'data'    => $dataAsnHdr
                ]
            ];

            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();

            return [
                'type' => false,
                'body' => [
                    'message' => SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__),
                    'data'    => $dataAsnHdr
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'type' => false,
                'body' => [
                    'message' => $e->getMessage(),
                    'data'    => $dataAsnHdr
                ]
            ];
        }

        return $result;

    }

    public function destroy($asnID){
        $asnHdr = $this->asnHdrModel->getAsnHdrById($asnID);
        if(!$asnHdr)
            return $this->response->errorBadRequest("ASN not found");
        if($asnHdr->asn_sts!='NW')
            return $this->response->errorBadRequest("ASN is only deleted at status: NEW");
        $this->asnHdrModel->deleteById($asnID);
        return response()->json(['data' => ['message' => 'Deleted ASN successfully']]);
    }

    /**
     * Save Asn details
     *
     * @param object $asnHrd
     * @param int $ctnrId
     * @param array $input
     * @param int $old_ctnrId
     *
     * @throws \Exception
     */
    private function saveMultiAsnDetails($asnHrd, $container, $input)
    {
        $arrDataAsnDtl = [];
        $inputItems = $input['items'];
        $splitItem = [];

        //split Item use for 943
        foreach ($inputItems as $key => $value) {

            if (isset($value['cartons'])) {
                break;
            }

            if (!is_numeric($value['quantity']) || $value['quantity'] <= 0) {
                ThrowError::response([
                    'message' => 'The quantity must be numeric or The quantity must be greater than 0',
                    'data'    => $value
                ]);
            }

            if (!is_numeric($value['pack']) || $value['pack'] <= 0) {
                ThrowError::response([
                    'message' => 'The pack must be numeric or The pack must be greater than 0',
                    'data'    => $value
                ]);
            }

            if ($value['quantity'] % $value['pack'] == 0) {
                $inputItems[$key]['cartons'] = $value['quantity'] / $value['pack'];
            } else {
                ThrowError::response([
                    'message' => 'Division quotient of quantity and carton must be integer',
                    'data'    => $value
                ]);
            }

        }

        $dataItems = array_merge($inputItems, $splitItem);

        //5980  get sooner exp_date for group of  "container"
        $minExpDate = min(array_column($dataItems, 'exp_date'));
        $oldUOMCode = '';
        $sysUom = null;
        //prepare data
        foreach ($dataItems as $detail) {
            // get Item
            $item = $this->loadItem($detail);
            if (is_array($item) && $item['type'] == false) {
                return $item;
            }


            $uomCode = data_get($detail, 'uom', 'EA');

            if ($oldUOMCode != $uomCode) {
                $sysUom = $this->getUOMByCode($uomCode);

                if (!$sysUom) {
                    throw new \Exception('There is no UOM, please check in system!');
                }
            }

            $oldUOMCode = $uomCode;

            $lot = array_get($detail, 'lot', '') ? array_get($detail, 'lot', '') : $item->lot;
            $po = array_get($detail, 'purchase_order', '') ? array_get($detail, 'purchase_order', '') : $item->po;

            $dtl_length = array_get($detail, 'length', '') ? array_get($detail, 'length',
                '') : $item->length;
            $dtl_width = array_get($detail, 'width', '') ? array_get($detail, 'width', '') : $item->width;
            $dtl_height = array_get($detail, 'height', '') ? array_get($detail, 'height',
                '') : $item->height;
            $dtl_weight = array_get($detail, 'weight', '') ? array_get($detail, 'weight',
                '') : $item->weight;

            $ucc128 = $this->generateUpc128($item->item_id, $item->cus_id);
            $dataAsnDtl = [
                'asn_hdr_id'      => $asnHrd->asn_hdr_id,
                'ctnr_id'         => $container->ctnr_id,
                'item_id'         => $item->item_id,
                'asn_dtl_lot'     => $lot ? $lot : $po,
                'asn_dtl_po'      => $po,
                'asn_dtl_po_dt'   => array_get($detail, 'po_date', '') ? strtotime($detail['po_date']) : 0,

                //'expired_dt'   => array_get($detail, 'expired_dt', '') ? strtotime($detail['expired_dt']) : 0,
                'asn_dtl_ept_dt'   => !empty($minExpDate) ? strtotime($minExpDate) : strtotime(date('m/d/Y')),//#5980 add more
                'uom_id'          => $sysUom->sys_uom_id,
                'uom_code'        => $sysUom->sys_uom_code,
                'uom_name'        => $sysUom->sys_uom_name,
                'asn_dtl_ctn_ttl' => array_get($detail, 'cartons', '') ? array_get($detail, 'cartons', '') : 0,
                'asn_dtl_des'     => array_get($detail, 'description', '') ? array_get($detail, 'description', '') :
                    $item->description,
                'asn_dtl_crs_doc' => !empty($crs_dock) ? $crs_dock : 0,
                'asn_dtl_length'  => $dtl_length,
                'asn_dtl_width'   => $dtl_width,
                'asn_dtl_height'  => $dtl_height,
                'asn_dtl_weight'  => $dtl_weight,
                'asn_dtl_volume'  => ($dtl_length * $dtl_width * $dtl_height),
                'asn_dtl_cube'    => (($dtl_length * $dtl_width * $dtl_height) / 1728),
                // 'asn_dtl_pack'    => array_get($detail, 'pack', '') ? array_get($detail, 'pack', '') : $item->pack,
                'asn_dtl_pack'    => $item->pack,
                'asn_dtl_cus_upc' => array_get($detail, 'customer_upc', '') ? array_get($detail, 'customer_upc',
                    '') : $item->cus_upc,
                'asn_dtl_sku'     => $item->sku,
                'asn_dtl_size'    => $item->size,
                'asn_dtl_color'   => $item->color,
                'ctnr_num'        => $container->ctnr_num,
                'ucc128'          => $ucc128
            ];

            $sku = $item->sku;
            $size = $item->size;
            $color = $item->color;
            $primeKey = $sku . $size . $color . $dataAsnDtl['asn_dtl_pack'] . $dataAsnDtl['asn_dtl_po'] . $dataAsnDtl['asn_dtl_lot'];
            $primeKey = str_replace(' ', '', $primeKey);
            $primeKey = strtolower($primeKey);

            if (!isset($arrDataAsnDtl[$primeKey])) {
                $arrDataAsnDtl[$primeKey] = $dataAsnDtl;
            } else {
                $arrDataAsnDtl[$primeKey]['asn_dtl_ctn_ttl'] += $dataAsnDtl['asn_dtl_ctn_ttl'];
            }
        }
        foreach ($arrDataAsnDtl as $value) {
            $this->asnDtlModel->refreshModel();
            // create ASN detail
            $asnDtl = $this->asnDtlModel->create($value);
        }

    }

    /**
     * Load Asn details
     *
     * @param array $input
     *
     * @return mixed
     * @throws \Exception
     */
    private function loadItem($input)
    {
        $sku = array_get($input, 'sku', '');
        $size = array_get($input, 'size', '');
        $color = array_get($input, 'color', '');

        try {
            if (!empty($input['sku'])) {

                $arrParam = ['sku' => $sku];

                if (!empty($size)) {
                    $arrParam['size'] = $size;
                }

                if (!empty($color)) {
                    $arrParam['color'] = $color;
                }

                if ($item = $this->itemModel->getFirstWhere($arrParam)) {

                    return $item;
                } else {

                    $data = [
                        'sku'   => $sku,
                        'size'  => $size,
                        'color' => $color
                    ];

                    return [
                        'type' => false,
                        'body' => [
                            'message' => 'The item does not exist in our WMS. Please setup EDI 888 for Item first',
                            'data'    => $data
                        ]
                    ];
                }
            }
        } catch (\Exception $e) {
            ThrowError::response($e->getMessage());
        }
    }

    /**
     * @SWG\Put(
     *     path="/asns/{asnID}/containers/{ctnID}",
     *     tags={"Asns"},
     *     summary="Update Asn",
     *     description="Update Asn",
     *     operationId="updateAsn",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="asnID",
     *         description="Asns Id",
     *         required=true,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="ctnID",
     *         description="containers Id",
     *         required=true,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Item object that needs to be updated to the store",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/InputAsnsData")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Item",
     *         @SWG\Schema(
     *           @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
     *                      ref="#/definitions/AsnsData"
     *                  )
     *              )
     *         )
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *          @SWG\Schema(ref="#/definitions/ErrorModel")
     *      ),
     * )
     *
     * Update Asn
     *
     * @param int $asnID
     * @param int $ctnID
     * @param Request $request
     * @param ContainerModel $containerModel
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function update
    (
        $asnID,
        $ctnID,
        Request $request,
        ContainerModel $containerModel
    ) {
        $whs_id = self::getCurrentWhsId();
        $input = $request->getParsedBody();

        //validate data input
        $this->validator->validate($input);


        // check exit asn by id
        $asnHrd = $this->getAsnByCurrentUser($asnID);
        if (empty($asnHrd)) {
            return $this->response->errorBadRequest(Message::get("BM017", "ASN"));
        }
        // check status asn New
        //if ($asnHrd['asn_sts'] !== config('constants.asn_status.NEW')) {
        //    return $this->response->errorBadRequest(Message::get("BM032"));
        //}

        // data for create
        $expDt = strtotime($input['exp_date']);
        $asn_hdr_ref = trim(strtoupper($input['asn_ref']));
        $dataAsnHdr = [
            'asn_hdr_id'     => $asnID,
            'asn_hdr_ept_dt' => $expDt,
            'asn_hdr_ref'    => $asn_hdr_ref,
            'cus_id'         => intval($input['cus_id']),
            'whs_id'         => intval($whs_id),
            'asn_hdr_des'    => array_get($input, 'asn_hdr_des', ''),
            'sys_mea_code'   => 'IN',
        ];

        try {

            DB::beginTransaction();

            // update ASN
            $this->asnHdrModel->update($dataAsnHdr);

            // create new container
            if (empty($container = $containerModel->getByNumber($input['ctnr_num']))) {
                $container = $containerModel->create(['ctnr_num' => $input['ctnr_num']]);
                $container->isNew = true;
            }

            //Set new data ASN Hdr
            $asnHrd->asn_hdr_ept_dt = strtotime($input['exp_date']);

            $asnHrd->asn_hdr_ref = $asn_hdr_ref;

            // save ASN detail
            //$asnDtlIds = $this->saveAsnDetails($asnHrd, $container, $input, intval($ctnID));
            $asnDtlIds = $this->saveAsnDetails($asnHrd, $container, $input, intval($ctnID), self::MODE_UPDATE);

            // update total
            $total_item = $this->saveTotalForAsn($asnHrd->asn_hdr_id);

            $asnHrd->ctnr_id = $container->ctnr_id;
            $asnHrd->asn_hdr_itm_ttl = $total_item;

            // event tracking for ASN Edit
            $asn_hdr_num = $asnHrd->asn_hdr_num;
            $cus_id = $asnHrd->cus_id;
            $whs_id = $asnHrd->whs_id;
            $this->eventTrackingModel->create([
                'whs_id'    => $whs_id,
                'cus_id'    => $cus_id,
                'owner'     => $asn_hdr_num,
                'evt_code'  => Status::getByKey("Event", "ASN-EDITED"),
                'trans_num' => $asn_hdr_num,
                'info'      => sprintf(Status::getByKey("Event-info", "ASN-EDITED"), $asn_hdr_num)

            ]);

            //add asn meta
            $asn_meta = $this->asnMetaModel->getFirstWhere([
                'asn_hdr_id' => $asnHrd->asn_hdr_id,
                'qualifier'  => "GAD"
            ]);
            $act_dt = !empty($input['act_dt']) ? true : false;
            if ($act_dt) {
                if (!$asn_meta) {
                    $this->asnMetaModel->create([
                        'asn_hdr_id' => $asnHrd->asn_hdr_id,
                        'qualifier'  => "GAD",
                        'value'      => $expDt
                    ]);
                } else {
                    $asn_meta->deleted = 0;
                    $asn_meta->value = $expDt;
                    $asn_meta->save();
                }
            } else if ($asn_meta) {
                $asn_meta->deleted = 1;
                $asn_meta->deleted_at = time();
                $asn_meta->save();
            }

            // Update GrHdr, GrDtl, Pallet, Cartons, PalSugLoc
            $this->updateAsnForRgAndCc($asnHrd, $asnDtlIds, $container);

            DB::commit();

            return $this->response->item($asnHrd, new AsnTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function updateAsnForRgAndCc($asnHdr, $asnDtlIds, $container)
    {
        $asnHdrId = $asnHdr->asn_hdr_id;

        DB::table('asn_hdr AS c')
            ->where('asn_hdr_id', $asnHdrId)
            ->whereRaw("(
                SELECT COUNT(asn_dtl.asn_dtl_id)
                FROM asn_dtl
                WHERE asn_dtl.asn_hdr_id = {$asnHdrId}
                    AND asn_dtl.`asn_dtl_sts` NOT IN ('RE', 'CC')
                    AND c.`asn_sts` != 'CC'
                    AND asn_dtl.deleted = 0
                ) = 0")
            ->update([
                'asn_sts' => 'RE'
            ]);

        // GR HDR
        DB::table('asn_hdr AS c')
            ->where('c.asn_hdr_id', $asnHdrId)
            ->whereRaw("(
                SELECT COUNT(1)
                FROM asn_dtl
                WHERE asn_dtl.asn_hdr_id = {$asnHdrId}
                    AND asn_dtl.asn_dtl_sts <> 'CC'
                    AND asn_dtl.deleted = 0
                ) = 0")
            ->update([
                'c.asn_sts' => 'CC'
            ]);

        // Gr Hdr Detail
        foreach (array_chunk($asnDtlIds, 200) as $chunkIds) {
            DB::table('gr_dtl as c')
                ->join('asn_dtl as a', 'a.asn_dtl_id', '=', 'c.asn_dtl_id')
                ->whereIn('a.asn_dtl_id', $chunkIds)
                ->update([
                    'c.gr_dtl_ept_ctn_ttl' => DB::raw('a.asn_dtl_ctn_ttl'),
                    'c.gr_dtl_ept_qty_ttl' => DB::raw('a.asn_dtl_qty_ttl'),
                    'c.gr_dtl_act_qty_ttl' => DB::raw("IF(a.uom_code = 'KG', c.gr_dtl_act_qty_ttl, c.gr_dtl_act_ctn_ttl * a.asn_dtl_pack)"),
                    'c.gr_dtl_des' => DB::raw('a.asn_dtl_des'),
                    'c.gr_dtl_disc' => DB::raw('CAST(c.gr_dtl_act_ctn_ttl AS SIGNED) - CAST(a.asn_dtl_ctn_ttl AS SIGNED)'),
                    "c.gr_dtl_disc_qty" => DB::raw("
                        CAST(c.gr_dtl_act_qty_ttl AS DECIMAL(15,2)) - CAST(a.asn_dtl_qty_ttl AS DECIMAL(15,2))
                    "),
                    //-- 'gr_dtl_plt_ttl' int(11) unsigned NOT NULL,
                    'c.updated_at' => DB::raw('a.updated_at'),
                    'c.updated_by' => DB::raw('a.updated_by'),
                    'c.cat_code' => DB::raw('a.cat_code'),
                    'c.cat_name' => DB::raw('a.cat_name'),
                    'c.item_id' => DB::raw('a.item_id'),
                    'c.pack' => DB::raw('a.asn_dtl_pack'),
                    'c.sku' => DB::raw('a.asn_dtl_sku'),
                    'c.size' => DB::raw('a.asn_dtl_size'),
                    'c.color' => DB::raw('a.asn_dtl_color'),
                    'c.lot' => DB::raw('a.asn_dtl_lot'),
                    'c.po' => DB::raw('a.asn_dtl_po'),
                    'c.uom_code' => DB::raw('a.uom_code'),
                    'c.uom_name' => DB::raw('a.uom_name'),
                    'c.uom_id' => DB::raw('a.uom_id'),
                    'c.expired_dt' => DB::raw('a.expired_dt'),
                    'c.upc' => DB::raw('a.asn_dtl_cus_upc'),
                    'c.ctnr_id' => DB::raw('a.ctnr_id'),
                    'c.ctnr_num' => DB::raw('a.ctnr_num'),
                    'c.length' => DB::raw('a.asn_dtl_length'),
                    'c.width' => DB::raw('a.asn_dtl_width'),
                    'c.height' => DB::raw('a.asn_dtl_height'),
                    'c.weight' => DB::raw("IF(a.uom_code = 'KG', c.weight, a.asn_dtl_weight)"),
                    'c.cube' => DB::raw('a.asn_dtl_cube'),
                    'c.volume' => DB::raw('a.asn_dtl_volume'),
                    'c.gr_dtl_des' => DB::raw('a.asn_dtl_des'),
                    'c.ucc128' => DB::raw('a.ucc128'),
                    'c.spc_hdl_code' => DB::raw('a.spc_hdl_code'),
                    'c.spc_hdl_name' => DB::raw('a.spc_hdl_name'),
                    'c.gr_dtl_sts' => DB::raw("IF(a.asn_dtl_sts = 'CC', 'CC', c.gr_dtl_sts)")
                ]);
        }

        $ctnrId = $container->ctnr_id;
        // GR HDR
        foreach (array_chunk($asnDtlIds, 200) as $chunkIds) {
            DB::table('gr_hdr AS c')
                ->join('asn_hdr AS a', 'a.asn_hdr_id', '=', 'c.asn_hdr_id')
                ->join('gr_dtl AS d', 'd.gr_hdr_id', '=', 'c.gr_hdr_id')
                ->where('a.asn_hdr_id', $asnHdrId)
                ->whereIn('d.asn_dtl_id', $chunkIds)
                ->update([
                    'c.updated_at' => DB::raw('a.updated_at'),
                    'c.updated_by' => DB::raw('a.updated_by'),
                    'c.ctnr_num' => $container->ctnr_num,
                    'c.ctnr_id' => $ctnrId
                ]);
        }

        // GR HDR
        DB::table('gr_hdr AS c')
            ->where('c.asn_hdr_id', $asnHdrId)
            ->where('c.ctnr_id', $ctnrId)
            ->update([
                'c.ref_code'      => $asnHdr->asn_hdr_ref,
                'c.gr_hdr_ept_dt' => $asnHdr->asn_hdr_ept_dt,
                'c.gr_sts'        => DB::raw("
                    IF(
                        (SELECT COUNT(1)
                            FROM asn_dtl
                            WHERE asn_dtl.asn_hdr_id = {$asnHdrId}
                                AND asn_dtl.ctnr_id = {$ctnrId}
                                AND asn_dtl.asn_dtl_sts <> 'CC'
                                AND asn_dtl.deleted = 0) = 0,
                        'CC',
                        c.gr_sts
                    )"),
                'c.putaway'       => DB::raw("
                    IF((
                        SELECT COUNT(pal_sug_loc.act_loc_id)
                        FROM pal_sug_loc
                        JOIN gr_dtl ON pal_sug_loc.`gr_dtl_id` = gr_dtl.`gr_dtl_id`
                        WHERE pal_sug_loc.act_loc_id IS NOT NULL
                            AND pal_sug_loc.gr_hdr_id = c.gr_hdr_id
                            AND gr_dtl.`gr_dtl_sts` = 'RG'
                    ) =
                    (
                        SELECT SUM(gr_dtl.gr_dtl_plt_ttl) FROM gr_dtl
                        WHERE gr_dtl.gr_hdr_id = c.gr_hdr_id
                        AND gr_dtl.`gr_dtl_sts` = 'RG'
                    ), 1, 0)
                ")
            ]);

        /*DB::table('gr_hdr AS c')
            ->where('c.asn_hdr_id', $asnHdrId)
            ->where('c.ctnr_id', $ctnrId)
            ->update([
                'c.gr_sts'         => DB::raw("
                    IF(
                        (SELECT COUNT(1)
                            FROM asn_dtl
                            WHERE asn_dtl.asn_hdr_id = {$asnHdrId}
                                AND asn_dtl.ctnr_id = {$ctnrId}
                                AND asn_dtl.asn_dtl_sts <> 'CC') = 0,
                        'CC',
                        c.gr_sts
                    )")
            ]);*/

        // Cartons
        foreach (array_chunk($asnDtlIds, 200) as $chunkIds) {
            DB::table('cartons AS c')
                ->join('gr_dtl AS a', 'a.gr_dtl_id', '=', 'c.gr_dtl_id')
                ->whereIn('a.asn_dtl_id', $chunkIds)
                ->update([
                    'c.ctn_uom_id' => DB::raw('a.uom_id'),
                    'c.piece_remain' => DB::raw("IF(c.uom_code = 'KG', c.piece_remain, a.pack)"),
                    'c.piece_ttl' => DB::raw("IF(c.uom_code = 'KG', c.piece_remain, a.pack)"),
                    'c.updated_at' => DB::raw('a.updated_at'),
                    'c.updated_by' => DB::raw('a.updated_by'),
                    'c.cat_code' => DB::raw('a.cat_code'),
                    'c.cat_name' => DB::raw('a.cat_name'),
                    'c.item_id' => DB::raw('a.item_id'),
                    'c.sku' => DB::raw('a.sku'),
                    'c.size' => DB::raw('a.size'),
                    'c.color' => DB::raw('a.color'),
                    'c.lot' => DB::raw('a.lot'),
                    'c.ctn_pack_size' => DB::raw('a.pack'),
                    'c.po' => DB::raw('a.po'),
                    'c.expired_dt' => DB::raw('a.expired_dt'),
                    'c.uom_code' => DB::raw('a.uom_code'),
                    'c.uom_name' => DB::raw('a.uom_name'),
                    'c.upc' => DB::raw('a.upc'),
                    'c.ctnr_id' => DB::raw('a.ctnr_id'),
                    'c.ctnr_num' => DB::raw('a.ctnr_num'),
                    'c.length' => DB::raw('a.length'),
                    'c.width' => DB::raw('a.width'),
                    'c.height' => DB::raw('a.height'),
                    'c.weight' => DB::raw("IF(a.uom_code = 'KG', c.weight, a.weight)"),
                    'c.cube' => DB::raw('a.cube'),
                    'c.volume' => DB::raw('a.volume'),
                    'c.des' => DB::raw('a.gr_dtl_des'),
                    'c.ucc128' => DB::raw('a.ucc128'),
                    'c.spc_hdl_code' => DB::raw('a.spc_hdl_code'),
                    'c.spc_hdl_name' => DB::raw('a.spc_hdl_name'),
                    //'c.origin_id'    => DB::raw('c.ctn_id'),
                    'c.ctn_sts' => DB::raw("IF(a.gr_dtl_sts = 'CC', 'CC', c.ctn_sts)"),
                    'c.loc_id' => DB::raw("IF(a.gr_dtl_sts = 'CC', NULL, c.loc_id)"),
                    'c.loc_code' => DB::raw("IF(a.gr_dtl_sts = 'CC', NULL, c.loc_code)")
                ]);
        }

        // Location Active When Status = CC
        foreach (array_chunk($asnDtlIds, 200) as $chunkIds) {
            DB::table('location AS c')
                ->join('pallet AS p', 'p.loc_id', '=', 'c.loc_id')
                ->join('gr_dtl AS a', 'a.gr_dtl_id', '=', 'p.gr_dtl_id')
                ->whereIn('a.asn_dtl_id', $chunkIds)
                ->where('a.gr_dtl_sts', 'CC')
                ->update([
                    'c.updated_at' => DB::raw('a.updated_at'),
                    'c.updated_by' => DB::raw('a.updated_by'),
                    'c.loc_sts_code' => 'AC',
//                'c.loc_zone_id'   => NULL
                ]);
        }

        // Pallet
        foreach (array_chunk($asnDtlIds, 200) as $chunkIds) {
            DB::table('pallet AS c')
                ->join('gr_dtl AS a', 'a.gr_dtl_id', '=', 'c.gr_dtl_id')
                ->whereIn('a.asn_dtl_id', $chunkIds)
                ->update([
                    'c.updated_at' => DB::raw('a.updated_at'),
                    'c.updated_by' => DB::raw('a.updated_by'),
                    'c.ctn_ttl' => DB::raw("IF(a.uom_code = 'KG', 1, (SELECT COUNT(ca.ctn_id) FROM cartons AS ca WHERE c.plt_id = ca.plt_id))"),
                    'c.plt_sts' => DB::raw("IF(a.gr_dtl_sts = 'CC', 'CC', c.plt_sts)"),
                    'c.rfid' => DB::raw("IF(a.gr_dtl_sts = 'CC', NULL, c.rfid)"),
                    'c.expired_date' => DB::raw("a.expired_dt"),
                    'c.lot' => DB::raw('a.lot'),
                    'c.item_id' => DB::raw('a.item_id'),
                    'c.sku' => DB::raw('a.sku'),
                    'c.size' => DB::raw('a.size'),
                    'c.color' => DB::raw('a.color'),
                    'c.pack' => DB::raw("IF(a.uom_code = 'KG', c.weight, a.pack)"),
                    'c.weight' => DB::raw("IF(a.uom_code = 'KG', c.weight, a.weight)"),
                    'c.spc_hdl_code' => DB::raw('a.spc_hdl_code'),
                    'c.loc_id' => DB::raw("IF(a.gr_dtl_sts = 'CC', NULL, c.loc_id)"),
                    'c.loc_code' => DB::raw("IF(a.gr_dtl_sts = 'CC', NULL, c.loc_code)"),
                    'c.init_piece_ttl' => DB::raw("IF(a.uom_code = 'KG', c.init_piece_ttl, CAST((SELECT COUNT(ca.ctn_id) FROM cartons AS ca WHERE c.plt_id = ca.plt_id) AS SIGNED) * a.pack)"),
                    'c.init_ctn_ttl' => DB::raw("IF(a.uom_code = 'KG', c.init_ctn_ttl, (SELECT COUNT(ca.ctn_id) FROM cartons AS ca WHERE c.plt_id = ca.plt_id))"),
                ]);
        }

        // Pag Sug Loc
        foreach (array_chunk($asnDtlIds, 200) as $chunkIds) {
            DB::table('pal_sug_loc AS c')
                ->join('gr_dtl AS a', 'a.gr_dtl_id', '=', 'c.gr_dtl_id')
                ->whereIn('a.asn_dtl_id', $chunkIds)
                ->update([
                    'c.updated_at' => DB::raw('a.updated_at'),
                    'c.updated_by' => DB::raw('a.updated_by'),
                    'c.ctn_ttl' => DB::raw("IF(a.uom_code = 'KG', 1, (SELECT COUNT(ca.ctn_id) FROM cartons AS ca WHERE c.plt_id = ca.plt_id))"),
                    'c.item_id' => DB::raw("IF(a.uom_code = 'KG', c.item_id, a.item_id)"),
                    'c.lot' => DB::raw("IF(a.uom_code = 'KG', c.lot, a.lot)"),
                    'c.sku' => DB::raw("IF(a.uom_code = 'KG', c.sku, a.sku)"),
                    'c.size' => DB::raw("IF(a.uom_code = 'KG', c.size, a.size)"),
                    'c.color' => DB::raw("IF(a.uom_code = 'KG', c.color, a.color)")
                ]);
        }
    }

    private function getUOMByID($uomId)
    {
        $sysUom = SystemUom::select('sys_uom_id', 'sys_uom_code', 'sys_uom_name')->where('sys_uom_id',
            $uomId)->where('sys_uom_type', 'Item')->first();

        return $sysUom;
    }

    private function getUOMByCode($uomCode)
    {
        $sysUom = SystemUom::select('sys_uom_id', 'sys_uom_code', 'sys_uom_name')->where('sys_uom_code',
            $uomCode)->where('sys_uom_type',
            'Item')->first();

        return $sysUom;
    }

    /**
     * Save Asn details
     *
     * @param object $asnHrd
     * @param int $ctnrId
     * @param array $input
     * @param int $old_ctnrId
     *
     * @throws \Exception
     */
    //private function saveAsnDetails($asnHrd, $container, $input, $old_ctnrId = 0)
    private function saveAsnDetails($asnHrd, $container, $input, $old_ctnrId = 0, $mode)
    {
        $ctnrId = $container->ctnr_id;
        $hasNewItem = $this->validateItem($input['details']);
        $IgnoreAsnDtlIds = array_filter(array_column($input['details'], 'asn_dtl_id'));
//        if (empty($container->isNew)) {
        //$this->validateContainer($container, $IgnoreAsnDtlIds, (bool)$old_ctnrId & $hasNewItem);
//        }

        $asnDtlEptDate = array_get($input, 'asn_dtl_ept_dt',0);

        // check (ans_hdr_id, $asnDtlEptDate) exist or not
        // Check ASN ID and date existed
        $isNotExistInDb = $this->asnDtlModel->getAsnDtlByCTNRAndASNEptDate($asnHrd->asn_hdr_id, $asnDtlEptDate, $mode, $old_ctnrId, $ctnrId);

        if (!$isNotExistInDb) {
            throw new \Exception("Expected date is unique for each container");
        }

        // delelete
        $dbAsnDtlIdItemIds = $this->asnDtlModel->getAsnDtlIdItemIdByHeaderAndContainer($asnHrd->asn_hdr_id, $ctnrId);

        if ($old_ctnrId > 0 && $ctnrId != $old_ctnrId) {
            $dbAsnDtlIdItemIds = array_merge(
                $dbAsnDtlIdItemIds,
                $this->asnDtlModel->getAsnDtlIdItemIdByHeaderAndContainer($asnHrd->asn_hdr_id, $old_ctnrId)
            );
        }

        $asnDtlIds = [];
        $sysUom = null;
        $measureKind = [
            'length-unit' => ['dtl_length', 'dtl_width', 'dtl_height'],
            'weight-unit' => ['dtl_weight']
        ];
        //add asn detail
        foreach ($input['details'] as $detail) {
            //convert measure if use metric
            if ($input['measurement_code'] === 'CM') {
                $detail = Helper::convertMeasure($detail, $measureKind, true);
            }

            $uomId = intval(array_get($detail, 'dtl_uom_id', null));
            if (!$sysUom || $sysUom->sys_uom_id != $uomId) {
                $sysUom = $this->getUOMByID($uomId);
                if (!$sysUom) {
                    throw new \Exception('There is no UOM, please check in system!');
                }
            }
            // total carton must be greater than total dock
            $ctn_total = array_get($detail, 'dtl_ctn_ttl', 0);
            $crs_dock = array_get($detail, 'dtl_crs_doc', 0);

            if ($ctn_total < $crs_dock) {
                throw new \Exception(Message::get("VR058", "Total carton", "total dock"));
            }

            // get Item ID
            $item = $this->saveAndLoadItemId($asnHrd->cus_id, $detail, $sysUom);
            $asn_dtl_cus_upc = array_get($detail, 'asn_dtl_cus_upc');

            $size = $this->normalizeNA(array_get($detail, 'dtl_size', 'NA'));
            $color = $this->normalizeNA(array_get($detail, 'dtl_color', 'NA'));
            $po = $this->normalizeNA(array_get($detail, 'dtl_po', 'NA'));
            $lot = $this->normalizeNA(array_get($detail, 'asn_dtl_lot', "NA"));

            // check duplicate itemId (Sku, Size, Color), Pack, PO
            if ($obj = $this->asnDtlModel->getFirstWhere(
                [
                    'item_id'      => $item->item_id,
                    'asn_dtl_po'   => $po,
                    'asn_dtl_pack' => array_get($detail, 'dtl_pack', ""),
                    'asn_dtl_lot'  => $lot,
                    'ctnr_id'      => $ctnrId,
                    'asn_hdr_id'   => $asnHrd->asn_hdr_id,
                    // 'cus_id'       => $asnHrd->cus_id
                ])
            ) {
                $isDuplicate = true;

                $checkAsnID = object_get($obj, 'asn_dtl_id', null);
                $checkCtnrID = object_get($obj, 'ctnr_id', null);
                $checkAsnHdrID = object_get($obj, 'asn_hdr_id', null);
                if (!empty($detail['asn_dtl_id']) && $detail['asn_dtl_id'] == $checkAsnID) {
                    $isDuplicate = false;
                }
                if ($checkCtnrID != $ctnrId) {
                    $isDuplicate = false;
                }
                if ($checkAsnHdrID != $asnHrd->asn_hdr_id) {
                    $isDuplicate = false;
                }

                if ($isDuplicate) {
                    throw new \Exception(Message::get("BM006", "Sku, Size, Color, Pack, PO, Lot"));
                }
            }

            // //check upc
            // if (!empty($asn_dtl_cus_upc)) {
            //     $asnHasUPC = $this->asnDtlModel->getFirstWhere([
            //         'asn_dtl_cus_upc' => $asn_dtl_cus_upc,
            //         'asn_hdr_id'      => $asnHrd->asn_hdr_id
            //     ]);

            //     $checkUPC = false;
            //     if ($asnHasUPC) {
            //         $checkUPC = ($item->item_id == $asnHasUPC->item_id) ? false : true;
            //     }
            //     if ($checkUPC) {
            //         throw new \Exception(Message::get("BM006", "UPC"));
            //     }
            // }

            $dtl_length = floatval(array_get($detail, 'dtl_length'));
            $dtl_width = floatval(array_get($detail, 'dtl_width'));
            $dtl_height = floatval(array_get($detail, 'dtl_height'));
            $dtl_vol = $dtl_length * $dtl_width * $dtl_height;

            $ucc128 = $this->generateUpc128($item->item_id, $item->cus_id);
            $qtyTtl = floatval(array_get($detail, 'asn_dtl_qty_ttl', 0));
            $asn_dtl_crs_doc_qty = $detail['asn_dtl_crs_doc_qty'] ?? $crs_dock * $detail['dtl_pack'];

            $dataAsnDtl = [
                'asn_hdr_id'          => $asnHrd->asn_hdr_id,
                'ctnr_id'             => $ctnrId,
                'item_id'             => $item->item_id,
                'asn_dtl_lot'         => $lot,
                'asn_dtl_po'          => $po,
                'asn_dtl_po_dt'       => !empty($detail['dtl_po_date']) ? strtotime($detail['dtl_po_date']) : 0,
                'expired_dt'          => !empty($detail['expired_dt']) ? strtotime($detail['expired_dt']) : 0,
                'uom_id'              => $sysUom->sys_uom_id,
                'uom_code'            => $sysUom->sys_uom_code,
                'uom_name'            => $sysUom->sys_uom_name,
                'asn_dtl_ctn_ttl'     => intval($ctn_total),
                'asn_dtl_crs_doc'     => !empty($crs_dock) ? $crs_dock : 0,
                'asn_dtl_crs_doc_qty' => !empty($asn_dtl_crs_doc_qty) ? $asn_dtl_crs_doc_qty : 0,
                'asn_dtl_des'         => array_get($detail, 'dtl_des', $item->description),
                'asn_dtl_length'      => $dtl_length,
                'asn_dtl_width'       => $dtl_width,
                'asn_dtl_height'      => $dtl_height,
                'asn_dtl_weight'      => floatval(array_get($detail, 'dtl_weight')),
                'asn_dtl_pack'        => floatval(array_get($detail, 'dtl_pack')),
                'asn_dtl_cus_upc'     => $asn_dtl_cus_upc,
                'asn_dtl_sku'         => $item->sku,
                'asn_dtl_qty_ttl'     => $sysUom->sys_uom_code !== 'KG' ? $detail['dtl_pack'] * intval($ctn_total) : $qtyTtl,
                'asn_dtl_size'        => $size,
                'asn_dtl_color'       => $color,
                'asn_dtl_ept_dt'      => !empty($asnDtlEptDate) ? strtotime($asnDtlEptDate) : 0,
                'ctnr_num'            => $container->ctnr_num,
                'asn_dtl_volume'      => $dtl_vol,
                'asn_dtl_cube'        => ($dtl_vol / 1728),
                'ucc128'              => $ucc128,
                'spc_hdl_code'        => array_get($detail, 'dtl_spc_hdl_code', 'NA'),
                'spc_hdl_name'        => array_get($detail, 'dtl_spc_hdl_name', 'NA'),
                'cat_code'            => array_get($detail, 'dtl_cat_code', 'NA'),
                'cat_name'            => array_get($detail, 'dtl_cat_name', 'NA'),
                'prod_line'           => array_get($detail, 'prod_line', ''),
                'cmp'                 => array_get($detail, 'cmp', ''),

            ];
            if (empty($dataAsnDtl['spc_hdl_code'])) {
                $dataAsnDtl['spc_hdl_code'] = null;
            }
            if (empty($dataAsnDtl['spc_hdl_name'])) {
                $dataAsnDtl['spc_hdl_name'] = null;
            }
            if (empty($dataAsnDtl['cat_code'])) {
                $dataAsnDtl['cat_code'] = null;
            }
            if (empty($dataAsnDtl['cat_name'])) {
                $dataAsnDtl['cat_name'] = null;
            }
            $this->asnDtlModel->refreshModel();

            if (empty($detail['asn_dtl_id'])) {
                // create ASN detail
                $asnDtl = $this->asnDtlModel->create($dataAsnDtl);
            } else {
                // update ASN detail
                //$dataAsnDtl['asn_dtl_id'] = $detail['asn_dtl_id'];
                //$asnDtl = $this->asnDtlModel->update($dataAsnDtl);

                $asn_dtl_sts = array_get($detail, 'asn_dtl_sts_code', '');
                if (empty($asn_dtl_sts)) {
                    return $this->response->errorBadRequest("Asn Detail Status is required!");
                }

                $asnDtl = $this->asnDtlModel->update($dataAsnDtl + [
                        'asn_dtl_id'  => $detail['asn_dtl_id'],
                        'asn_dtl_sts' => $asn_dtl_sts,
                    ]);

                if ($asn_dtl_sts == "CC") {
                    // WMS2-5199 - [Inbound - ASN] - Remove RFID of cartons and virtual cartons when ASN cancel
                    $cartonModel = new CartonModel();
                    $cartonModel->removeRfidCartonsByCancelAsnDetail($detail['asn_dtl_id']);
                    $cartonModel->deleteRfidVirtualCartonsByCancelAsnDetail($detail['asn_dtl_id']);

                }
            }

            //get the sooner Expected date
            $theSoonerExpectedDate = DB::table('asn_dtl')->where('asn_hdr_id', $asnHrd->asn_hdr_id)
                                                      ->select('asn_dtl_ept_dt')
                                                      ->orderBy('asn_dtl_ept_dt', 'asc')
                                                      ->first();
            //update asn_hdr.asn_hdr_ept_dt
            if (array_get($theSoonerExpectedDate, 'asn_dtl_ept_dt')) {
                DB::table('asn_hdr')->where('asn_hdr_id', $asnHrd->asn_hdr_id)
                    ->update(['asn_hdr_ept_dt' => $theSoonerExpectedDate['asn_dtl_ept_dt']]);
            }
            $asnDtlIds[] = $asnDtl->asn_dtl_id;
        }

        // remove old
        // delete non-use id
        if ($removingIds = array_diff(array_keys($dbAsnDtlIdItemIds), $asnDtlIds)) {
            //delete
            $this->asnDtlModel->deleteById($removingIds);
            $this->deleteGoodsReceipt($asnHrd, $removingIds);
        }

        return $asnDtlIds;
    }

    protected function deleteGoodsReceipt($asnHdr, $asnDtlIds)
    {
        $grHdr = DB::table('gr_hdr')
            ->where('deleted', 0)
            ->where('whs_id', $asnHdr->whs_id)
            ->where('cus_id', $asnHdr->cus_id)
            ->where('asn_hdr_id', $asnHdr->asn_hdr_id)
            ->where('created_from', 'GUN')
            ->first();

        if (! empty($grHdr)) {
            $dataUpdate = [
                'deleted' => 1,
                'updated_at' => time(),
                'deleted_at' => time(),
                'updated_by' => Data::getCurrentUserId()
            ];
            foreach (array_chunk($asnDtlIds, 200) as $removeIds) {
                DB::table('gr_dtl')->where('gr_hdr_id', $grHdr['gr_hdr_id'])->whereIn('asn_dtl_id', $removeIds)->update($dataUpdate);
                DB::table('cartons')->where('gr_hdr_id', $grHdr['gr_hdr_id'])->whereIn('asn_dtl_id', $removeIds)->update($dataUpdate);
            }
        }
    }

    /**
     * validate container for asn
     * Condition: container not in goods receipt which has sts: RG, PD, XD (processing xdock)
     *
     * @param $ctnrObj
     * @param array $IgnoreAsnDtlIds
     * @param bool $isUpdate
     *
     * @throws \HttpException
     */
    private function validateContainer($ctnrObj, $IgnoreAsnDtlIds, $isUpdate = false)
    {
        $chkCtnr = AsnDtl::join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
            ->leftJoin('gr_hdr', 'gr_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
            ->whereRaw("(gr_hdr.gr_hdr_id IS NULL OR gr_hdr.gr_sts != 'RE')")
            ->where('asn_dtl.ctnr_id', $ctnrObj->ctnr_id)
            ->where('asn_dtl.asn_dtl_sts', '!=', 'CC')
            ->where('asn_hdr.whs_id', Data::getCurrentWhsId());

        if (!empty($IgnoreAsnDtlIds)) {
            $chkCtnr->whereNotIn('asn_dtl.asn_dtl_id', $IgnoreAsnDtlIds);
        }

        $chkCtnr = $chkCtnr->first();
        if ($isUpdate && $chkCtnr) {
            throw new HttpException(403, 'Container: ' . $ctnrObj->ctnr_num . ' can\'t add new items');
        } elseif (!$isUpdate && $chkCtnr) {
            throw new HttpException(403, 'Container: ' . $ctnrObj->ctnr_num . ' has ASN in processing');
        }
    }


    /**
     * Validate Asn details
     *
     * @param array $input
     *
     * @throws \Exception
     */
    private function validateItem($input)
    {
        $hasNewItem = false;
        $rules = [
            'dtl_uom_id' => 'required|integer',
        ];
        $items = [];
        $itemIds = [];
        $skuSizeColors = [];
        foreach ($input as $each) {
            if (empty($each['asn_dtl_id'])) {
                $hasNewItem = true;
            }

            $validator = Validator::make($each, $rules);
            if ($validator->fails()) {
                throw new ValidationHttpException($validator->errors()->getMessages());
            }

            $items[] = [
                'lot'    => array_get($each, 'dtl_lot', ''),
                'sku'    => array_get($each, 'dtl_sku', ''),
                'size'   => array_get($each, 'dtl_size', ''),
                'color'  => array_get($each, 'dtl_color', ''),
                'uom_id' => intval(array_get($each, 'dtl_uom_id')),
                'pack'   => floatval(array_get($each, 'dtl_pack')),
                'length' => floatval(array_get($each, 'dtl_length')),
                'width'  => floatval(array_get($each, 'dtl_width')),
                'height' => floatval(array_get($each, 'dtl_height')),
                'weight' => floatval(array_get($each, 'dtl_weight')),
                'po'     => array_get($each, 'dtl_po', ''),
            ];

            $skuSizeColor = sprintf('%s-%s-%s-%s-%s-%s',
                array_get($each, 'dtl_sku', ''),
                array_get($each, 'dtl_size', ''),
                array_get($each, 'dtl_color', ''),
                array_get($each, 'dtl_pack', ''),
                array_get($each, 'dtl_lot', ''),
                array_get($each, 'dtl_po', ''));

            if (in_array($skuSizeColor, $skuSizeColors)) {

                throw new \Exception(Message::get("BM006", "Sku, Size, Color, Pack, PO, Lot"));
            }

            $skuSizeColors[] = $skuSizeColor;
        }

        return $hasNewItem;
    }

    private function normalizeNA($str)
    {
        $str = trim($str);
        if (strtoupper($str) === self::NA || empty($str)) {
            $str = self::NA;
        }

        return $str;
    }


    /**
     * Save and load Asn details
     *
     * @param int $cusId
     * @param array $input
     *
     * @return mixed
     * @throws \Exception
     */
    private function saveAndLoadItemId($cusId, $input, $sysUom = null)
    {
        $us_upc = array_get($input, 'asn_dtl_cus_upc');
        if (!empty($input['dtl_itm_id'])) {
            if (empty($item = $this->itemModel->getFirstBy('item_id', $input['dtl_itm_id']))) {
                throw new \Exception(Message::get("BM017", "item"));
            }

            if (!empty($us_upc) && trim($us_upc) != "") {
                $this->updateUpcItem($input['dtl_itm_id'], $us_upc);
            }

            return $item;
        }
        $size = $this->normalizeNA(array_get($input, 'dtl_size', 'NA'));
        $color = $this->normalizeNA(array_get($input, 'dtl_color', 'NA'));

        $itemHasUpc = $this->itemModel->getFirstWhere([
            'sku'    => trim(array_get($input, 'dtl_sku', '')),
            'size'   => $size,
            'color'  => $color,
            'cus_id' => $cusId

        ]);

        if ($itemHasUpc) {

            $itemId = object_get($itemHasUpc, 'item_id', 0);
            if ($itemId && trim($us_upc) != "") {
                $this->updateUpcItem($itemId, $us_upc);
            }

            return $itemHasUpc;
        }


        if (!empty($us_upc)) {
            $itemHasUpc = $this->itemModel->getFirstWhere([
                'cus_upc' => $us_upc,
                'sku'     => array_get($input, 'dtl_sku', ''),
                'size'    => $size,
                'color'   => $color,
            ]);

            if ($itemHasUpc) {
                throw new \Exception(Message::get("BM006", "UPC"));
            }

        }

        $length = floatval(array_get($input, 'dtl_length'));
        $width = floatval(array_get($input, 'dtl_width'));
        $height = floatval(array_get($input, 'dtl_height'));
        $volume = Helper::calculateVolume($length, $width, $height);
        $cube = Helper::calculateCube($length, $width, $height);

        $dataItem = [
            'description' => array_get($input, 'dtl_description'),
            // 'lot'         => array_get($input, 'dtl_lot'),
            'sku'         => array_get($input, 'dtl_sku', ''),
            'size'        => $size,
            'color'       => $color,
            'uom_id'      => $sysUom->sys_uom_id,
            'uom_code'    => $sysUom->sys_uom_code,
            'uom_name'    => $sysUom->sys_uom_name,
            'pack'        => intval(array_get($input, 'dtl_pack')),
            'length'      => $length,
            'width'       => $width,
            'height'      => $height,
            'weight'      => floatval(array_get($input, 'dtl_weight')),
            'volume'      => $volume,
            'cube'        => $cube,
            'cus_upc'     => array_get($input, 'asn_dtl_cus_upc'),
            'status'      => array_get($input, 'status', config('constants.item_status.ACTIVE')),
            'cus_id'      => $cusId,
            'item_code'   => Item::generateItemCode($cusId)
        ];

        $this->itemModel->refreshModel();
        try {
            if ($item = $this->itemModel->create($dataItem)) {
                return $item;
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $itemId
     * @param $upc
     */
    private function updateUpcItem($itemId, $upc)
    {
        //update total carton asn hdr
        $dataUpdate = [
            'item_id' => $itemId,
            'cus_upc' => $upc
        ];

        $this->itemModel->update($dataUpdate);
    }

    /**
     * Save total for Asn
     *
     * @param int $asnHrdId
     */
    private function saveTotalForAsn($asnHrdId)
    {
        $totals = $this->asnDtlModel->getTotalInfoFromAsnDtlByHeader($asnHrdId);

        //update total carton asn hdr
        $dataUpdateTotal = [
            'asn_hdr_id'       => $asnHrdId,
            'asn_hdr_ctn_ttl'  => $totals['ctn_ttl'],
            'asn_hdr_itm_ttl'  => $totals['itm_ttl'],
            'asn_hdr_ctnr_ttl' => $totals['ctnr_ttl'],
        ];

        $this->asnHdrModel->update($dataUpdateTotal);

        return $totals['itm_ttl'];
    }

    /**
     * Cancel Asn
     *
     * @param int $asnID
     */
    public function cancelASN($asnID)
    {
        // valid exist asn by id
        $asnHrd = $this->getAsnByCurrentUser($asnID);
        if (empty($asnHrd)) {
            return $this->response->errorBadRequest(Message::get("BM017", "ASN"));
        }
        if ($asnHrd->asn_sts != 'NW') {
            return $this->response->errorBadRequest('Please select ASN with status New to cancel.');
        }

        try {
            DB::beginTransaction();

            //Update asn hdr status cancel by id
            $dataHdr = [
                'asn_hdr_id' => $asnID,
                'asn_sts'    => 'CC'
            ];

            $this->asnHdrModel->update($dataHdr);

            //Update asn dtl status cancel by asn hdr id
            $dataDtl = [
                'asn_dtl_sts' => 'CC'
            ];
            $conditionDtl = [
                'asn_hdr_id' => $asnID
            ];

            $this->asnDtlModel->updateWhere($dataDtl, $conditionDtl);

            // event tracking
            $this->eventTrackingModel->create([
                'whs_id'    => $asnHrd->whs_id,
                'cus_id'    => $asnHrd->cus_id,
                'owner'     => $asnHrd->asn_hdr_num,
                'evt_code'  => 'ANC',
                'trans_num' => $asnHrd->asn_hdr_num,
                'info'      => sprintf('%s Cancelled', $asnHrd->asn_hdr_num)
            ]);

            // WMS2-5199 - [Inbound - ASN] - Remove RFID of cartons and virtual cartons when ASN cancel
            $cartonModel = new CartonModel();
            $cartonModel->removeRfidCartonsByCancelAsn($asnID);
            $cartonModel->deleteRfidVirtualCartonsByCancelAsn($asnID);

            DB::commit();

            return $this->response->item($asnHrd, new AsnTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_OK);

        } catch (\PDOException $e) {
            DB::rollBack();
            SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__);
            throw $e;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @SWG\Get(
     *     path="/asns",
     *     tags={"Asns"},
     *     summary="Search Asn",
     *     description="Search Asn",
     *     operationId="searchAsn",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="query",
     *         name="whs_id",
     *         description="Warehouse Id",
     *         required=false,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Parameter(
     *         in="query",
     *         name="cus_id",
     *         description="Customer Id",
     *         required=false,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Parameter(
     *         in="query",
     *         name="dtl_sku",
     *         description="Asns detail SKU",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         in="query",
     *         name="asn_hdr_num",
     *         description="Asn header number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         in="query",
     *         name="asn_sts",
     *         description="Asn status",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         in="query",
     *         name="asn_dtl_po",
     *         description="Asns detail PO",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         in="query",
     *         name="ctnr_num",
     *         description="Container Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return all found result of Asn",
     *         @SWG\Schema(
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
     *                      ref="#/definitions/AsnsHeader"
     *                  ),
     *              ),
     *              @SWG\Property(
     *                  property="meta",
     *                  @SWG\Property(
     *                      property="pagination",
     *                      @SWG\Property(property="total", type="integer", format="int32", description="Total"),
     *                      @SWG\Property(property="count", type="integer", format="int32", description="Count"),
     *                      @SWG\Property(property="per_page", type="integer", format="int32",
     *                          description="Per page"),
     *                      @SWG\Property(property="current_page", type="integer", format="int32",
     *                          description="Current page"),
     *                      @SWG\Property(property="total_pages", type="integer", format="int32",
     *                          description="Total pages"),
     *                      @SWG\Property(property="links", type="object", description="Links")
     *                  )
     *              )
     *         )
     *     )
     * )
     *
     * Search asn
     *
     * @param Request $request
     */
    public function search(Request $request)
    {
        $input = $request->getQueryParams();
        try {
            // get list asn
            $asns = $this->asnDtlModel->search($input,
                ['asnHdr', 'asnHdr.asnStatus', 'asnHdr.customer', 'item', 'container', 'asnHdr.createdUser'],
                array_get($input, 'limit'));

            return $this->response->paginator($asns, new AsnListsTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function countSearchCanSort(Request $request){
        $input=$request->getParsedBody();
        $asn_hdr_ids = $input['asn_hdr_id']??[];
        $asnDetail = new AsnDtlModel();
        return $asnDetail->getTotalInfoOfAsn($asn_hdr_ids);
    }

    public function searchCanSort(Request $request, AsnListsCanSortTransformer $asnListsCanSortTransformer)
    {
        set_time_limit(0);
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit', 20);
        $inputStatuses = array_get($input, 'asn_sts', []);

        $asnFlag = false;
        if (isset($input['type'])) {
            $asnFlag = true;
            $type = array_get($input, 'type', 'asn');
        }

        if (is_string($inputStatuses)) {
            $inputStatuses = [$inputStatuses];
            $input['asn_sts'] = $inputStatuses;
        }

        $input['asn_sts'] = SelArr::removeNullOrEmptyString(array_get($input, 'asn_sts', []));

        try {
            // get list asn

            if (!empty($input['export']) && $input['export'] == 1) {
                $asns = $this->asnHdrModel->search($input,
                    ['asnDtl', 'asnStatus', 'customer', 'asnDtl.item', 'asnDtl.container', 'createdUser'],
                    false, true)->toArray();

                if (!empty($asns)) {
                    $title = [
                        'asn_status.asn_sts_name'                          => 'Status',
                        'asn_hdr_num'                                      => 'ASN Number',
                        'asn_hdr_ref'                                      => 'Ref Code',
                        'created_at|format()|m/d/Y'                        => 'ASN Date',
                        'customer.cus_name'                                => 'Customer',
                        'itm_ttl|function()|getInfo(asn_hdr_id)'           => '# of SKUs',
                        'ctnr_ttl|function()|getInfo(asn_hdr_id)'          => '# of CTNRs',
                        'ctn_ttl|function()|getInfo(asn_hdr_id)'           => '# of Pack',
                        'asn_hdr_ept_dt|format()|m/d/Y'                    => 'Expected Date',
                        'created_user.first_name|.|created_user.last_name' => 'User',
                        'gr_hdr_created_at|format()|m/d/Y'                    => 'GR Created Date',
                        'gr_hdr_act_dt|format()|m/d/Y'                    => 'GR Completed Date',
                    ];

                    $getInfo = function ($params) {
                        $row = DB::table('asn_dtl')
                            ->select(DB::raw('count(distinct asn_dtl_id) as itm_ttl,
                                          count(distinct ctnr_id) as ctnr_ttl,
                                          sum(asn_dtl_ctn_ttl) as ctn_ttl'))
                            ->where('asn_hdr_id', $params['asn_hdr_id'])
                            ->first();

                        if (!$row) {
                            $row = [
                                'itm_ttl'  => 0,
                                'ctnr_ttl' => 0,
                                'ctn_ttl'  => 0,
                            ];
                        }

                        return $row;
                    };

                    Export::showFile("Report_Asn", $title, $asns, ['getInfo' => $getInfo]);
                    die;
                }

                return ['status' => 'OK', 'message' => 'No records found!'];
            }

            if ($asnFlag) {
                $userId = Data::getCurrentUserId();
                $this->userMetaModel->modifyUserMeta([
                    'user_id'   => $userId,
                    'qualifier' => config('constants.qualifier.ASN_STATUS')
                ], $inputStatuses, $type);
            }

            $asns = $this->asnHdrModel->search($input,
                ['asnDtl', 'asnStatus', 'customer', 'asnDtl.item', 'asnDtl.container', 'createdUser'],
                $limit);

            return $this->response->paginator($asns, $asnListsCanSortTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/asns/{asnID}/containers/{ctnID}",
     *     tags={"Asns"},
     *     summary="View Detail Asn",
     *     description="View Detail Asn",
     *     operationId="showAsn",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="asnID",
     *         description="Asns Id",
     *         required=true,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="ctnID",
     *         description="Containers Id",
     *         required=true,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return object of Asn",
     *         @SWG\Schema(ref="#/definitions/AsnsHeaderResponse"),
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *          @SWG\Schema(ref="#/definitions/ErrorModel")
     *      )
     * )
     *
     * Show ASN detail
     *
     * @param int $asnID
     * @param int $ctnID
     * @param AsnDetailTransformer $asnDetailTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($asnID, $ctnID, AsnDetailTransformer $asnDetailTransformer)
    {
        $asnHrd = $this->getAsnByCurrentUser($asnID);
        if (!$asnHrd) {
            return $this->response->errorBadRequest(Message::get("BM017", "ASN"));
        }

        $asnDtls = $this->asnDtlModel->findWhere(
            ['asn_hdr_id' => $asnID, 'ctnr_id' => $ctnID],
            ['grDtl', 'grDtl.goodsReceipt', 'systemUom'],
            ['asn_dtl_id' => 'asc']
        );

        //get asn_dtl.asn_dtl_ept_dt  for asn detail page//#5980
        $res = DB::table('asn_dtl')->where('ctnr_id', $ctnID)
            ->where('asn_hdr_id', $asnID)
            ->first();
        $expectedDate =  $res['asn_dtl_ept_dt'];
        //$asnHrd->asn_dtl_ept_dt = ($expectedDate) ? date('m/d/Y', is_int($expectedDate) ?$expectedDate: $expectedDate->timestamp) : '';
        $asnHrd->asn_dtl_ept_dt = ($expectedDate)
                                    ? date('m/d/Y', is_int($expectedDate) ? $expectedDate : $expectedDate->timestamp)
                                    : date('m/d/Y', is_int($asnHrd->asn_hdr_ept_dt) ? $asnHrd->asn_hdr_ept_dt: $asnHrd->asn_hdr_ept_dt->timestamp);

        $details = [];
        $checkStsRGCtnr = true;

        if ($asnDtls) {
            foreach ($asnDtls as $asnDtl) {

                if (!in_array($asnDtl->asn_dtl_sts, ['RG', 'CC', 'RE'])) {
                    $checkStsRGCtnr = false;
                }

                $details[] = [
                    'asn_dtl_id'       => $asnDtl->asn_dtl_id,
                    'asn_hdr_id'       => $asnDtl->asn_hdr_id,
                    'ctnr_id'          => $asnDtl->ctnr_id,
                    'ctnr_num'         => $asnDtl->ctnr_num,
                    'dtl_item_id'      => $asnDtl->item_id,
                    'asn_dtl_lot'      => $asnDtl->asn_dtl_lot,
                    'asn_dtl_cus_upc'  => $asnDtl->item['cus_upc'],
                    'asn_dtl_sts_code' => object_get($asnDtl, 'asn_dtl_sts', null),
                    'asn_dtl_sts'      => Status::getByValue(object_get($asnDtl, 'asn_dtl_sts', null),
                        'ASN_DTL_STS') ?: null,
                    'dtl_sku'          => $asnDtl->item['sku'],
                    'dtl_size'         => $asnDtl->item['size'],
                    'dtl_color'        => $asnDtl->item['color'],
                    'asn_dtl_qty_ttl'  => $asnDtl->asn_dtl_qty_ttl,

                    'dtl_item_status_code'    => $asnDtl->item['status'] ?? null,
                    'dtl_item_status'         => $asnDtl->item->itemStatus['item_sts_name'] ?? null,

                    'dtl_uom_id'             => $asnDtl->uom_id,
                    'dtl_uom_code'           => object_get($asnDtl, 'systemUom.sys_uom_code', ''),
                    'dtl_uom_name'           => object_get($asnDtl, 'systemUom.sys_uom_name', ''),
                    'dtl_po'                 => $asnDtl->asn_dtl_po,
                    'dtl_po_date'            => ($asnDtl->asn_dtl_po_dt) ? date('m/d/Y', $asnDtl->asn_dtl_po_dt) : '',
                    'expired_dt'             => ($asnDtl->expired_dt) ? date('m/d/Y', $asnDtl->expired_dt) : '',

                    'asn_dtl_ept_dt'         => ($asnDtl->asn_dtl_ept_dt) ? date('m/d/Y', $asnDtl->asn_dtl_ept_dt) : '',
                    'dtl_ctn_ttl'            => $asnDtl->asn_dtl_ctn_ttl,
                    'gr_hdr_num'             => object_get($asnDtl, 'grDtl.goodsReceipt.gr_hdr_num', ''),
                    'gr_sts'                 => object_get($asnDtl, 'grDtl.goodsReceipt.gr_sts', ''),
                    'created_from'           => object_get($asnDtl, 'grDtl.goodsReceipt.created_from', ''),
                    'dtl_gr_dtl_act_ctn_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_act_ctn_ttl', ''),
                    'gr_dtl_plt_ttl'         => object_get($asnDtl, 'grDtl.gr_dtl_plt_ttl', 0),
                    'dtl_gr_dtl_dmg_ttl'     => object_get($asnDtl, 'grDtl.gr_dtl_dmg_ttl', 0),
                    'dtl_gr_dtl_is_dmg'      => object_get($asnDtl, 'grDtl.gr_dtl_is_dmg', 0),
                    'dtl_gr_dtl_disc'        => object_get($asnDtl, 'grDtl.gr_dtl_disc', 0),

                    'gr_dtl_act_qty_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_act_qty_ttl', 0),
                    'gr_dtl_disc_qty'    => object_get($asnDtl, 'grDtl.gr_dtl_disc_qty', 0),
                    'gr_dtl_ept_qty_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_ept_qty_ttl'),

                    'dtl_gr_sts'       => object_get($asnDtl, 'grDtl.goodsReceipt.gr_sts', null),
                    'dtl_crs_doc'      => $asnDtl->asn_dtl_crs_doc,
                    'dtl_des'          => $asnDtl->item['description'],
                    'dtl_length'       => number_format($asnDtl->asn_dtl_length, 2),
                    'dtl_width'        => number_format($asnDtl->asn_dtl_width, 2),
                    'dtl_height'       => number_format($asnDtl->asn_dtl_height, 2),
                    'dtl_weight'       => number_format($asnDtl->asn_dtl_weight, 2),
                    'asn_dtl_pack'     => $asnDtl->asn_dtl_pack,
                    'dtl_spc_hdl_code' => object_get($asnDtl, 'spc_hdl_code', ''),
                    'dtl_spc_hdl_name' => object_get($asnDtl, 'spc_hdl_name', ''),
                    'dtl_cat_code'     => object_get($asnDtl, 'cat_code', ''),
                    'dtl_cat_name'     => object_get($asnDtl, 'cat_name', ''),
                    'ucc128'           => $asnDtl->ucc128,
                    'prod_line'           => $asnDtl->prod_line,
                    'cmp'           => $asnDtl->cmp,
                ];
            }
        }

        $asnHrd->ctnr_sts = $checkStsRGCtnr ? 1 : 0;
        $asnHrd->details = $details;
        //asn meta
        $asnMeta = $this->asnMetaModel->getFirstWhere([
            'asn_hdr_id' => $asnID,
            'qualifier'  => "GAD",
            'deleted'    => 0
        ]);
        $asnHrd->act_dt = !empty($asnMeta) ? 1 : 0;
        return $this->response->item($asnHrd, $asnDetailTransformer);
    }

    /**
     * @param $asnID
     * @param $ctnID
     * @param CartonModel $cartonModel
     * @param GoodsReceiptModel $goodsReceiptModel
     */
    public function printASN(
        $asnID,
        $ctnID,
        CartonModel $cartonModel,
        GoodsReceiptModel $goodsReceiptModel
    ) {
        try {
            $asnHrd = $this->getAsnByCurrentUser($asnID);

            if (!$asnHrd) {
                return $this->response->errorBadRequest(Message::get("BM017", "ASN"));
            }

            $asnDtls = $this->asnDtlModel->findWhere(
                ['asn_hdr_id' => $asnID, 'ctnr_id' => $ctnID],
                ['grDtl', 'grDtl.goodsReceipt', 'systemUom'],
                ['asn_dtl_id' => 'asc']
            );

            $details = [];
            if ($asnDtls) {
                foreach ($asnDtls as $asnDtl) {
                    $grHdrCreatedAt = object_get($asnDtl, 'grDtl.goodsReceipt.created_at', '');
                    $item = DB::table('item')->where('item_id', '=', $asnDtl->item_id)->first();
                    $details[] = [
                        'asn_dtl_id'             => $asnDtl->asn_dtl_id,
                        'asn_hdr_id'             => $asnDtl->asn_hdr_id,
                        'ctnr_id'                => $asnDtl->ctnr_id,
                        'ctnr_num'               => $asnDtl->ctnr_num,
                        'dtl_item_id'            => $asnDtl->item_id,
                        'asn_dtl_lot'            => $asnDtl->asn_dtl_lot,
                        'asn_dtl_cus_upc'        => $asnDtl->asn_dtl_cus_upc,
                        'asn_dtl_sts'            => Status::getByValue(object_get($asnDtl, 'asn_dtl_sts', null),
                            'ASN_DTL_STS') ?: null,
                        'dtl_sku'                => $asnDtl->asn_dtl_sku,
                        'dtl_size'               => $asnDtl->asn_dtl_size,
                        'dtl_color'              => $asnDtl->asn_dtl_color,
                        'dtl_uom_id'             => $asnDtl->uom_id,
                        'dtl_uom_code'           => object_get($asnDtl, 'systemUom.sys_uom_code', ''),
                        'dtl_uom_name'           => object_get($asnDtl, 'systemUom.sys_uom_name', ''),
                        'dtl_po'                 => $asnDtl->asn_dtl_po,
                        'dtl_po_date'            => ($asnDtl->asn_dtl_po_dt) ? date('m/d/Y',
                            $asnDtl->asn_dtl_po_dt) : '',
                        'expired_dt'             => ($asnDtl->expired_dt) ? date('m/d/Y', $asnDtl->expired_dt) : '',
                        'dtl_ctn_ttl'            => $asnDtl->asn_dtl_ctn_ttl,
                        'gr_hdr_num'             => object_get($asnDtl, 'grDtl.goodsReceipt.gr_hdr_num', ''),
                        'gr_hdr_created_at'      => (empty($grHdrCreatedAt)) ? '' : date("Y/m/d",
                            strtotime($grHdrCreatedAt)),
                        'gr_hdr_id'              => object_get($asnDtl, 'grDtl.goodsReceipt.gr_hdr_id', ''),
                        'dtl_gr_dtl_act_ctn_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_act_ctn_ttl', ''),
                        'gr_dtl_plt_ttl'         => object_get($asnDtl, 'grDtl.gr_dtl_plt_ttl', ''),
                        'gr_dtl_dmg_ttl'         => object_get($asnDtl, 'grDtl.gr_dtl_dmg_ttl', ''),
                        'dtl_gr_dtl_disc'        => object_get($asnDtl, 'grDtl.gr_dtl_disc', ''),
                        'gr_in_note'             => object_get($asnDtl, 'grDtl.goodsReceipt.gr_in_note', ''),
                        'gr_ex_note'             => object_get($asnDtl, 'grDtl.goodsReceipt.gr_ex_note', ''),
                        'gr_dtl_sts'             => Status::getByValue(object_get($asnDtl, 'grDtl.gr_dtl_sts', null),
                            'GR_STATUS') ?: null,
                        'dtl_crs_doc'            => $asnDtl->asn_dtl_crs_doc,
                        'dtl_des'                => $item['description'],
                        'dtl_length'             => number_format($asnDtl->asn_dtl_length, 2),
                        'dtl_width'              => number_format($asnDtl->asn_dtl_width, 2),
                        'dtl_height'             => number_format($asnDtl->asn_dtl_height, 2),
                        'dtl_weight'             => number_format($asnDtl->asn_dtl_weight, 2),
                        'asn_dtl_pack'           => $asnDtl->asn_dtl_pack
                    ];
                }
            }

            $asnHrd->details = $details;
            $asnHrdArr = $asnHrd->toArray();

            $asnNum = object_get($asnHrd, 'asn_hdr_num', '');
            $sysMeaCode = object_get($asnHrd, 'sys_mea_code', '');
            $asnSts = Status::getByValue(object_get($asnHrd, 'asn_sts', null), 'ASN_STATUS') ?: null;
            //$totalSku = $asnHrd->asnDtl()->count();
            $totalSku = count($details);

            //get damaged Carton list
            $whs_id = Data::getCurrentWhsId();
            $currentCusIds = Data::getCurrentCusIds();
            $damagedCartonInfos = '';
            if (!empty($asnHrdArr['details'][0]['gr_hdr_id'])) {
                $goodsReceiptId = $asnHrdArr['details'][0]['gr_hdr_id'];

                $query = $goodsReceiptModel->make([
                    'goodsReceiptDetail',
                    'warehouse'
                ]);
                $goodsReceipt = $query
                    ->where('gr_hdr_id', $goodsReceiptId)
                    ->where('gr_hdr.whs_id', $whs_id)
                    ->whereIn('gr_hdr.cus_id', $currentCusIds)
                    ->first();

                if ($goodsReceipt) {
                    $damagedCartonInfos = GoodsReceiptController::getDamagedCartonAccordingToGRDtl(
                        $goodsReceipt,
                        $cartonModel,
                        '',
                        '',
                        90000000
                    );

                }
            }
            $userInfo = new \Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            $gr_sts = DB::table('gr_hdr')->select('gr_sts')
                ->where('asn_hdr_id', $asnID)
                ->where('ctnr_id', $ctnID)
                ->first();
            $gr_status = Status::getByValue(array_get($gr_sts, 'gr_sts', ''), 'GR_STATUS');

            $this->printASNDpfData($asnHrd, $asnHrdArr, $asnNum, $asnSts, $totalSku, $sysMeaCode,
                $damagedCartonInfos, $printedBy, $gr_status);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'print ASN', __FUNCTION__)
            );
        } catch
        (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $asnHrd
     * @param $asnHrdArr
     * @param $asnNum
     * @param $asnSts
     * @param $totalSku
     * @param $sysMeaCode
     * @param $damagedCartonInfos
     * @param $printedBy
     *
     * @throws \MpdfException
     */
    private function printASNDpfData(
        $asnHrd,
        $asnHrdArr,
        $asnNum,
        $asnSts,
        $totalSku,
        $sysMeaCode,
        $damagedCartonInfos,
        $printedBy,
        $gr_status
    ) {
        $pdf = new Mpdf('c', 'A4-L');

        $CurrentDate = date('m/d/Y h:i:s a ', time());

        $html = (string)view('ASNReceivingPrintoutTemplate', [
            'asnHrd'             => $asnHrd,
            'asnHrdArr'          => $asnHrdArr,
            'asnNum'             => $asnNum,
            'asnSts'             => $asnSts,
            'totalSku'           => $totalSku,
            'sysMeaCode'         => $sysMeaCode,
            'damagedCartonInfos' => $damagedCartonInfos,
            'printedBy'          => $printedBy,
            'gr_status'          => $gr_status,
            'CurrentDate'        => $CurrentDate,

        ]);

        $pdf->WriteHTML($html);
        //$dir = storage_path("PutAway/$whsId");
        //$pdf->Output("$dir/$grHdrNum.pdf", 'F');
        $pdf->Output("$asnNum.pdf", "D");
    }


    private function getAsnByCurrentUser($asnID)
    {
        $whs_id = Data::getCurrentWhsId();
        $currentCusIds = Data::getCurrentCusIds();

        $asnHrd = $this->asnHdrModel->getModel()
            ->where('asn_hdr_id', $asnID)
            ->where('asn_hdr.whs_id', $whs_id)
            ->whereIn('asn_hdr.cus_id', $currentCusIds)
            ->first();

        return $asnHrd;
    }

    /**
     * @param Request $request
     * @param LpnTransformer $lpnTransformer
     *
     * @return array
     * @throws \Exception
     */
    public function showLPN(
        Request $request,
        LpnTransformer $lpnTransformer
    ) {
        $lpnInfo = $this->lpnModel->getFirstWhere(
            [
                "type" => 'P'
            ],
            [],
            ['lpn_id' => "desc"]);

        if (empty($lpnInfo)){
            $arrEmpty = [
                'lpn_to_print' => 0,
                'from'         => 0,
                'to'           => 0,
            ];
            return ['data' => $arrEmpty];
        }
        return $this->response->item($lpnInfo, $lpnTransformer);
    }

    /**
     * @param Request $request
     *
     * @throws \Exception
     */
    public function printLPN(
        Request $request
    ) {
        $input = $request->getQueryParams();
        $LPNToPrint = array_get($input, 'lpn_to_print', '');

        if (empty($LPNToPrint)) {
            throw new \Exception("The number of LPN To Print is required!");
        }

        if (!is_numeric($LPNToPrint) || $LPNToPrint <= 0) {
            throw new \Exception("The number of LPN To Print is invalid!");
        }

        $lpnInfo = $this->lpnModel->getFirstWhere(
            [
                "type" => 'P'
            ],
            [],
            ['lpn_id' => "desc"]);
        $lpnQty = object_get($lpnInfo, 'lpn_qty', '');
        $from = object_get($lpnInfo, 'from', '');
        $to = object_get($lpnInfo, 'to', '');

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $params = [
            'type'       => 'P',
            'month'      => date('ym'),
            'from'       => (int)$to + 1,
            'to'         => (int)$LPNToPrint + (int)$to,
            'lpn_qty'    => $LPNToPrint,
            'created_by' => $userId,
            'created_at' => time(),

        ];
        DB::beginTransaction();
        DB::table('lpn')->insert($params);
        DB::commit();

        //print LPN
        $this->createLPNPdfFile($params);
    }

    /**
     * @param $params
     */
    private function createLPNPdfFile(
        $params
    ) {
        //$pdf = new Mpdf();
        $pdf = new Mpdf(
            '',    // mode - default ''
            [152.4, 101.6],    // format - A4, for example, default '',(WxH)( 6 inch = 152.4 mm
            // X 4 inch = 101.6 mm X)
            0,     // font size - default 0
            '',    // default font family
            5,    // margin_left
            5,    // margin right
            46,     // margin top
            10,    // margin bottom
            1,     // margin header
            1,     // margin footer
            'p' // L - landscape, P - portrait
        );

        $html = (string)view('WarehouseLPNListPrintoutTemplate', [
            'lpn_qty' => $params['lpn_qty'],
            'month'   => $params['month'],
            'from'    => $params['from'],
            'to'      => $params['to'],

        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PackCarton/$whsId");
        //$pdf->Output("$dir/$odrNum.pdf", 'F');
        $pdf->Output("LPN.pdf", "D");
        //$pdf->Output();
    }

    /**
     * @SWG\Get(
     *     path="/asns/{asnID}/list-containers",
     *     tags={"Asns"},
     *     summary="Get list containers of Asn",
     *     description="Get list containers of Asn",
     *     operationId="listContainers",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="asnID",
     *         description="Asns Id",
     *         required=true,
     *         type="integer",
     *         format="int32"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return list containers",
     *         @SWG\Schema(
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
     *                      ref="#/definitions/ListContainers"
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *          @SWG\Schema(ref="#/definitions/ErrorModel")
     *      )
     * )
     *
     * pt: ASN Detail
     *
     * @param int $asnID
     * @param ContainerTransformer $containerTransformer
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int
     */
    public function listContainers($asnID, ContainerTransformer $containerTransformer)
    {
        try {
            $asnDetails = $this->asnDtlModel->findWhere(
                ['asn_hdr_id' => $asnID],
                ['container']
            );
            if (count($asnDetails) < 1) {
                $grModel = new GoodsReceipt();
                $asnDetails = $grModel->getModel()->where('asn_hdr_id', $asnID)->get();
            }
            $containers = [];
            foreach ($asnDetails as $asnDetail) {
                if (!isset($containers[$asnDetail->ctnr_id])) {
                    if(isset($asnDetail->container)) {
                        $containers[$asnDetail->ctnr_id] = $asnDetail->container;
                    } else {
                        $containers[$asnDetail->ctnr_id] = $asnDetail;
                    }

                }
            }

            $containers = Collection::make($containers);

            return $this->response->collection($containers, $containerTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function export(Request $request)
    {
        $input = $request->getQueryParams();
        $type = strtolower(array_get($input, 'type', 'csv'));
        try {
            if (!in_array($type, ['csv', 'pdf'])) {
                throw new \Exception(Message::get("VR005", "(.csv|.pdf)"));
            }
            // get list asn
            $asns = $this->asnDtlModel
                ->search($input, [
                    'asnHdr.asnStatus',
                    'asnHdr.customer',
                    'asnHdr.createdUser',
                    'container'
                ], array_get($input, 'limit', 999999999));
            // Get X-doc
            $input['xdoc'] = true;
            $xDocks = $this->asnDtlModel->search($input, [], array_get($input, 'limit', 999999999))->toArray()['data'];
            $xDocks = array_pluck($xDocks, 'xdoc', 'asn_ctnr');
            $data[] = [
                'ASN Number',
                'Ref Code',
                'Created Date',
                'Customer',
                'Container Number',
                'X-Dock',
                'Expected Date',
                'Status',
                'User'
            ];
            foreach ($asns as $asn) {
                $asnHdr = $asn->asnHdr;
                $createdDate = object_get($asnHdr, "created_at", null);
                $createdDate = $createdDate ? date("m/d/Y", strtotime($createdDate)) : '';
                $expDate = object_get($asnHdr, 'asn_hdr_ept_dt', null);
                $expDate = ($expDate) ? date('m/d/Y', $expDate) : '';
                $data[] = [
                    object_get($asnHdr, "asn_hdr_num", null),
                    object_get($asnHdr, "asn_hdr_ref", null),
                    $createdDate,
                    object_get($asnHdr, "customer.cus_name", null),
                    object_get($asn, "container.ctnr_num", null),
                    (int)array_get($xDocks, "$asn->asn_hdr_id-$asn->ctnr_id", 0),
                    $expDate,
                    object_get($asnHdr, "asnStatus.asn_sts_name", null),
                    object_get($asnHdr, 'createdUser.first_name', '') . ' ' .
                    object_get($asnHdr, 'createdUser.last_name', '')
                ];
            }
            SelExport::exportList("Export_ASN_List_" . date("Y-m-d", time()), $type, $data);

            return $this->response->noContent();
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $asnID
     * @param $vtlCtnrId
     * @param AsnDetailVtlCtnTransformer $asnDetailVtlCtnTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function listASNDtlAndCartonVTL(
        $asnID,
        $vtlCtnrId,
        AsnDetailVtlCtnTransformer $asnDetailVtlCtnTransformer,
        Request $request
    ) {

        $input = $request->getQueryParams();
        $search = [
            'ctn_rfid'   => array_get($input, 'ctn_rfid', null),
            'is_gateway' => array_get($input, 'is_gateway', null),
            'scanned'    => array_get($input, 'scanned', null),
            'sku'        => array_get($input, 'sku', null),
        ];

        try {
            $asnDetails = $this->asnDtlModel->listASNDtlAndCartonRFID($asnID, $vtlCtnrId, $search);
            $ttlCartons = count($asnDetails);

            return ['data' => $asnDetails, 'total' => $ttlCartons];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $asnID
     * @param $vtlCtnrId
     * @param AsnDetailPalletVtlCtnTransformer $asnDetailPalletVtlCtnTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function listASNDtlAndPalletRFID(
        $asnID,
        $vtlCtnrId,
        AsnDetailPalletVtlCtnTransformer $asnDetailPalletVtlCtnTransformer,
        Request $request
    ) {
        try {
            $input = $request->getQueryParams();
            $search = [
                'plt_rfid' => array_get($input, 'plt_rfid', null),
                'sku'      => array_get($input, 'sku', null)
            ];

            $asnDetails = $this->asnDtlModel->listASNDtlAndPalletRFID($asnID, $vtlCtnrId, $search);
            $pallets = array_pluck($asnDetails, 'plt_rfid');
            $totalPallet = count(array_unique($pallets));

            return ['data' => $asnDetails, 'total' => $totalPallet];
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Get asn log error
     *
     * @param Integer $whsId
     * @param Integer $asnId
     * @param Integer $ctnrId
     *
     * @return Paginator
     */
    public function asnErrorsLog(Request $request, $whsId, $asnId)
    {
        //get asn hdr
        $asnHdr = (new AsnHdrModel())->getModel()->find($asnId);
        //get log by asn_num
        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', 20);
        $ctnrId = array_get($input, 'ctnr_id', null);
        $logs = new LogModel();
        $data = $logs->getErrorLogs($whsId, $asnHdr->asn_hdr_num, $ctnrId, $limit);

        return $this->response->paginator($data, new LogsTransformer);
    }

    public function printASNBarcode($asnId, $ctnId, Request $request)
    {
        $input = $request->getQueryParams();

        $asn = $this->asnHdrModel->getFirstBy('asn_hdr_id', $asnId);
        $asnNum = object_get($asn, "asn_hdr_num", "NA");
        $asnDtl = $this->asnDtlModel->findWhere(['asn_hdr_id' => $asnId, 'ctnr_id' => $ctnId], ['grDtl.goodsReceipt']);

        if (empty($asnDtl)) {
            return $this->response->noContent();
        }

        // remove qty null
        $unsetKey = [];
        $asnIds = array_keys($input['qty']);
        foreach ($asnDtl as $key => $asnDtlObj) {
            $asnDtlId = object_get($asnDtlObj, 'asn_dtl_id');
            if (!in_array($asnDtlId, $asnIds)) {
                $unsetKey[] = $key;

            }
        }
        if ($unsetKey) {
            foreach ($unsetKey as $key => $value) {
                unset($asnDtl[$value]);
            }
        }

        //Export to Excel File
        $this->createASNBarcode($asnDtl, $asnNum, $input['qty']);
    }

    private function createASNBarcode($asnDtl, $asnNum, $qty)
    {
        //Printing carton barcode for ASN
        $userInfo = new \Wms2\UserInfo\Data();
        $userInfo = $userInfo->getUserInfo();
        $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
        $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

        $pdf = new Mpdf(
            '',    // mode - default ''
            [152.4, 101.6],    // format - A4, for example, default '' (W=6 x H=4 inch)(4 inch = 101.6 mm)
            0,     // font size - default 0
            '',    // default font family
            5,    // margin_left
            5,    // margin right
            6,     // margin top
            6,    // margin bottom
            1,     // margin header
            1,     // margin footer
            'p' // L - landscape, P - portrait
        );
        $html = (string)view('ASNCartonBarcodePrintoutTemplate', [
            'asnDtl'    => $asnDtl,
            'asnNum'    => $asnNum,
            'qty'       => $qty,
            'printedBy' => $printedBy
        ]);

        $pdf->WriteHTML($html);

        //$dir = storage_path("PutAway/$whsId");
        //$pdf->Output("$dir/$grHdrNum.pdf", 'F');
        $pdf->Output("ASN_{$asnNum}_Carton_Barcode.pdf", "D");
    }

    private function generateUpc128($itemId, $cusId)
    {
        return str_pad($cusId, 5, '0', STR_PAD_LEFT) . str_pad($itemId, 8, '0', STR_PAD_LEFT);
    }

    public function updateRefCode(Request $request)
    {
        $input = $request->getParsedBody();
        $asnRefList = array_get($input, 'asn_ref');
        if (empty($asnRefList)) {
            return $this->response()->errorBadRequest('ASN Ref Code cannot be empty');
        }
        try {
            DB::beginTransaction();
            $whsId = Data::getCurrentWhsId();
            foreach ($asnRefList as $asnRef) {
                $asnHdr = $this->asnHdrModel->getFirstWhere(['asn_hdr_id' => $asnRef['asn_hdr_id'], 'whs_id' => $whsId]);
                if ($asnHdr->asn_sts !== 'RE') {
                    $asnHdr->asn_hdr_ref = $asnRef['asn_hdr_ref'];
                    $asnHdr->save();
                }
                //$this->asnHdrModel->updateWhere(['asn_hdr_ref' => $asnRef['asn_hdr_ref']], [
                    //'asn_hdr_id' => $asnRef['asn_hdr_id'],
                    //'whs_id' => Data::getCurrentWhsId()
                //]);
            }
            DB::commit();
            return response()->json(['data' => ['message' => 'Update ASN Ref Code Successfully']]);
        } catch (\PDOException $e) {
            DB::rollback();
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }
}
