<?php

namespace backend\models;

use yii;

class RoleUsers
{
    public static function getUsers($user_ids, $requestHeader)
    {
        $url = Yii::$app->params['authentication_api'] . 'users/get-user-by-ids';
        $data = [
            'user_ids' => $user_ids
        ];
        $option = [
            'headers' => [
                'Authorization' => $requestHeader->get('authorization'),
            ]
        ];
        $response = Yii::$app->httpclient->post($url, $data, $option, false);

        if ($response->getStatusCode() == 200){
            return Yii::$app->httpclient->formatResponse($response)['data'];
        }
        return false;
    }
}
