<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\WarehouseLocationModel;
use App\Api\V1\Models\SystemCountryModel;
use App\Api\V1\Transformers\SystemCountryTransformer;
use App\Api\V1\Validators\DeleteMassSystemCountryValidator;
use App\Api\V1\Validators\DeleteMassZoneValidator;
use App\Api\V1\Validators\SystemCountryValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;

class SystemCountryController extends AbstractController
{
    /**
     * @var $systemCountryModel
     */
    protected $systemCountryModel;
    /**
     * @var $systemCountryValidator
     */
    protected $systemCountryValidator;
    /**
     * @var $systemCountryTransformer
     */
    protected $systemCountryTransformer;
    /**
     * @var $delMassSysCountryValidator
     */
    protected $delMassSysCountryValidator;

    public function __construct()
    {
        parent::__construct();

        $this->systemCountryModel = new SystemCountryModel();

        $this->systemCountryValidator = new SystemCountryValidator();
        $this->systemCountryTransformer = new SystemCountryTransformer();
        $this->delMassSysCountryValidator = new DeleteMassSystemCountryValidator();
    }

    public function store(
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemCountryValidator->validate($input);

        $params = [
            'sys_country_name' => $input['sys_country_name'],
            'sys_country_code' => $input['sys_country_code']
        ];
        try {
            // check duplicate code
            if ($this->systemCountryModel->checkWhere(['sys_country_code' => $params['sys_country_code']])) {
                throw new \Exception("Country Code is existed!");
            }
            // check duplicate name
            if ($this->systemCountryModel->checkWhere(['sys_country_name' => $params['sys_country_name']])) {
                throw new \Exception("Country Name is existed!");
            }

            if ($SystemCountry = $this->systemCountryModel->create($params)) {
                return $this->response->item($SystemCountry, $this->systemCountryTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    public function show($systemCountryId)
    {
        $SystemCountry = $this->systemCountryModel->getFirstBy('sys_country_id', $systemCountryId);
        if ($SystemCountry) {
            return $this->response->item($SystemCountry, $this->systemCountryTransformer);
        } else {
            return $this->response->noContent();
        }
    }

    public function update(
        $systemCountryId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemCountryValidator->validate($input);

        $params = [
            'sys_country_id'   => $systemCountryId,
            'sys_country_code' => $input['sys_country_code'],
            'sys_country_name' => $input['sys_country_name'],
        ];
        try {
            // check duplicate code
            $systemCountry = $this->systemCountryModel->getFirstBy('sys_country_code', $params['sys_country_code']);

            if (($systemCountry) && $systemCountry->sys_country_id != $systemCountryId) {
                throw new \Exception("Country Code is existed!");
            }

            // check duplicate name
            $systemCountry = $this->systemCountryModel->getFirstBy('sys_country_name', $params['sys_country_name']);
            if ($systemCountry && $systemCountry->sys_country_id != $systemCountryId) {

                throw new \Exception("Country Name is existed!");
            }

            if ($systemCountry = $this->systemCountryModel->update($params)) {
                return $this->response->item($systemCountry, $this->systemCountryTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_OK);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    public function destroy($systemCountryId)
    {
        try {
            if ($this->systemCountryModel->deleteSystemCountry($systemCountryId)) {
                return $this->response->noContent();
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Cannot Delete this item');
    }

    public function search(Request $request)
    {
        $input = $request->getQueryParams();

        try {
            $systemCountry = $this->systemCountryModel->search($input, [], array_get($input, 'limit', PAGING_LIMIT));

            return $this->response->paginator($systemCountry, $this->systemCountryTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        $validArray = is_array($input['sys_country_id']);
        $input['sys_country_id'] = $validArray ? $input['sys_country_id'] : [$input['sys_country_id']];

        // validation
        $this->delMassSysCountryValidator->validate($input);

        try {
            $this->systemCountryModel->deleteMassSystemCountry($input['sys_country_id']);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }
}
