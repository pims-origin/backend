<?php

namespace App\Library;

use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Models\WarehouseModel;
use Exception;
use Illuminate\Http\Request as IRequest;
use Seldat\Wms2\Models\OrderDtl;
use App\Api\V1\Validators\OrderImportValidator;
use App\Api\V1\Models\OrderHdrModel;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

define("DEFAULT_UOM_ID", 37564); // Piece

class OrderImport
{
    protected $_hash;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var ShippingOrderModel
     */
    protected $shippingOrderModel;

    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var SystemStateModel
     */
    protected $systemStateModel;

    /**
     * @var OrderImportValidator
     */
    protected $orderImportValidator;

    protected $_header = [];
    protected $required = [];
    protected $integers = [];
    protected $_notExisted = [];
    protected $maxLength = [];
    protected $keys = [];
    protected $whsId = 0;
    protected $cusId = 0;
    protected $trackingIds = [];
    protected $itemIds = [];
    protected $orders = [];
    protected $shipmentTrackingId = [];
    protected $states;
    protected $countries;

    /**
     * OrderImport constructor.
     *
     * @param $hash
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param ShippingOrderModel $shippingOrderModel
     * @param WarehouseModel $warehouseModel
     * @param ItemModel $itemModel
     * @param EventTrackingModel $eventTrackingModel
     * @param OrderImportValidator $orderImportValidator
     */
    public function __construct(
        $hash,
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        ShippingOrderModel $shippingOrderModel,
        WarehouseModel $warehouseModel,
        ItemModel $itemModel,
        EventTrackingModel $eventTrackingModel,
        OrderImportValidator $orderImportValidator
    ) {
        $this->_hash = $hash;
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->shippingOrderModel = $shippingOrderModel;
        $this->warehouseModel = $warehouseModel;
        $this->itemModel = $itemModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->orderImportValidator = $orderImportValidator;
        $this->systemStateModel = new SystemStateModel();
    }

    /**
     * @param IRequest $request
     *
     * @return array
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    public function validate($request)
    {
        $input = $request->all();
        $this->orderImportValidator->validate($input);

        // Get Current Warehouse and Current Customer
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $this->whsId = array_get($userInfo, 'current_whs', 0);
        $this->cusId = $input['cus_id'];

        $this->states = $this->systemStateModel->fetchColumn([
            'sys_country_code',
            'sys_state_code',
            'sys_state_id'
        ], 's.deleted = 0', [], true);

        $shipmentTrackingId = $this->orderDtlModel->all(['orderHdr'])->toArray();
        $this->shipmentTrackingId = array_filter(array_map(function ($e) {
            if (!empty($e['order_hdr'])) {
                return $e['item_id'] . "-" .
                $e['order_hdr']['cus_odr_num'] . "-" .
                $e['order_hdr']['cus_po'] . "-" .
                $e['ship_track_id'];
            } else {
                return 0;
            }
        }, $shipmentTrackingId));

        $existedChk = [
            'Warehouse Code' => $this->warehouseModel->fetchColumns(
                ['whs_code', 'whs_id'],
                'deleted = 0', [],
                true,
                true
            ),
            'SKU|Size|Color' => $this->itemModel->fetchColumn(
                ['sku', 'size', 'color', 'cus_id', 'item_id'],
                'deleted = 0', [],
                true
            ),
            'Order ID'       => $this->shippingOrderModel->fetchColumns(
                ['cus_odr_num', 'so_id'],
                'deleted = 0', [],
                true,
                true
            ),
            //Order ID|PO
            'Order ID|PO'    => $this->orderHdrModel->fetchColumn(
                ['cus_odr_num', 'cus_po', 'odr_id'],
                'deleted = 0', [],
                true
            )
        ];

        //---------------------------------------------------------------------

        // Validate input file
        $errors = [];
        $valids = [];
        $error = false;
        $empty = true;

        // Check file not empty
        $file = $input['file'];
        if (empty($input['file_type'])) {
            return ['status' => false, 'data' => ['code' => 422, 'message' => Message::get("VR029", "File Type")]];
        }

        // Check valid File type
        $fileType = $input['file_type'];
        if (!in_array($fileType, ['xlsx', 'ods', 'csv'])) {
            return [
                'status' => false,
                'data'   => ['code' => 422, 'message' => Message::get("VR005", "(.xlsx|.ods|.csv)")]
            ];
        }

        $file->move(storage_path(), $this->_hash . ".$fileType");

        $filePath = storage_path($this->_hash . ".$fileType");
        $reader = ReaderFactory::create($fileType);
        $reader->open($filePath);

        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $index => $row) {
                $row = array_map("trim", $row);

                if ($index === 1) {
                    $row = SelArr::removeNullOrEmptyString($row);
                    $row = array_filter($row);
                    $this->declareVariable($row);
                    $duplicates = array_intersect($this->_header, $row);

                    if (empty($this->_header) || array_diff($this->_header, $duplicates)) {
                        $reader->close();
                        unlink($filePath);

                        return [
                            'status' => false,
                            'data'   => ['code' => 422, 'message' => "Import file is invalid. Please check again!"]
                        ];
                    }

                    array_push($row, "Import Status", "Error Detail");
                    $this->addToKey(["Import Status", "Error Detail"], $row);
                    $errors[] = $row;

                    continue;
                }
                $empty = false;
                if (array_filter($row)) {
                    $row = array_intersect_key($row, $this->_header);

                    // Check Valid
                    $errorDetail = $this->checkValidRow($row, $existedChk);

                    $valid = $row;
                    $valid[] = $this->whsId;
                    $valid[] = $this->cusId;

                    if (!empty($this->keys["Error Detail"]) && !empty($row[$this->keys["Error Detail"] + 1])) {
                        unset($row[$this->keys["Error Detail"] + 1]);
                        unset($row[$this->keys["Error Detail"] + 2]);
                        unset($row[$this->keys["Error Detail"] + 3]);
                    }

                    $valids[] = $valid;

                    // Put error
                    if ($errorDetail) {
                        $error = true;
                        $row[] = "Failed";
                    } else {
                        $row[] = "";
                    }

                    $errorDetail = trim($errorDetail);
                    $row[] = $errorDetail;
                    $errors[] = $row;
                }
            }
            if (count($errors) == 1) {
                $empty = true;
            }
            // Only process one sheet
            break;
        }

        if ($empty) {
            return ['status' => false, 'data' => ['code' => 422, 'message' => Message::get("VR028", "File")]];
        }

        $reader->close();
        unlink($filePath);

        $return = [
            'status' => !$error,
            'data'   => [
                'message'    => 'The Order File is ' . ($error ? "Invalid" : "Valid"),
                'total_rows' => (count($this->orders))
            ]
        ];

        if ($error) {
            return $this->writeExcel(storage_path("Errors"), $errors, $return);
        } else {
            return $this->writeExcel(storage_path("valid-import"), $valids, $return);
        }
    }

    /**
     * @param $row
     * @param array $existedChk
     *
     * @return string
     */
    private function checkValidRow(&$row, $existedChk = [])
    {
        $k = $this->keys;
        $errorDetail = "";

        // Check Required
        foreach ($this->required as $key => $name) {
            if (!isset($row[$key]) || $row[$key] === "" || $row[$key] === null) {
                if ($key == $k['Warehouse Code']) {
                    $errorDetail .= "Warehouse Code is invalid.\n";
                } else {
                    $errorDetail .= "$name is required.\n";
                }
            }
        }

        // Check Data Type
        foreach ($this->integers as $key => $name) {
            if (!empty($row[$key]) && !is_numeric($row[$key]) || $row[$key] < 0) {
                $errorDetail .= Message::get("VR014", $name, "0") . PHP_EOL;
            }

            if (!$this->isInt($row[$key])) {
                $errorDetail .= "Piece Quantity must be an interger number." . PHP_EOL;
            }
        }

        // Check Not Exist
        foreach ($this->_notExisted as $key => $name) {
            if (empty($row[$key])) {
                continue;
            }

            $nameTmp = explode("|", $name);
            if (count($nameTmp) > 1) {
                $val = "";
                foreach ($nameTmp as $n) {
                    $val .= $row[$k[$n]] . "-";
                }
                $val = strtoupper(rtrim($val, "-"));

                if (!isset($existedChk[$name][$val])) {
                    $errorDetail .= "The $name is not existed.\n";
                }
                continue;
            }

            $val = strtoupper($row[$key]);

            if (!isset($existedChk[$name][$val])) {
                if ($name == "Warehouse Code") {
                    $errorDetail .= "The Warehouse code is invalid.\n";
                } else {
                    $errorDetail .= "The {$this->_header[$key]} is not existed.\n";
                }
            } else if ($name == "Warehouse Code" && $existedChk[$name][$val] != $this->whsId) {
                $errorDetail .= "The Warehouse code is invalid.\n";
            } else {
                $row[$key] = $val;
            }
        }

        // Check SKU - Size - Color
        if (empty($row[$k["Size"]])) {
            $row[$k["Size"]] = 'NA';
        }
        if (empty($row[$k["Color"]])) {
            $row[$k["Color"]] = "NA";
        }

        $sku_size_color = strtoupper($row[$k['SKU']] . "-" . $row[$k["Size"]] . "-" . $row[$k["Color"]] . "-" . $this->cusId);

        if (!isset($existedChk["SKU|Size|Color"][$sku_size_color])) {
            $errorDetail .= "The SKU-Size-Color is not existed.\n";
        } else {
            $itemId = $existedChk["SKU|Size|Color"][$sku_size_color]['item_id'];
            if (!empty($this->itemIds[$row[$k['Order ID']] . "|" . $row[$k['PO']] . "|" . $itemId])) {
                $errorDetail .= Message::get("BM006", "Sku-Size-Color") . PHP_EOL;
            } else if (in_array($itemId . "-" . $row[$k["Order ID"]] . "-" . $row[$k["PO"]] . "-" .
                $row[$k["Shipment Tracking ID"]], $this->shipmentTrackingId)) {
                $errorDetail .= Message::get("BM006", "Shipment Tracking ID") . PHP_EOL;
            } else {
                $uomId = $existedChk["SKU|Size|Color"][$sku_size_color]['uom_id'];
                if (empty($uomId)) {
                    $uomId = DEFAULT_UOM_ID;
                }
                $this->itemIds[$row[$k['Order ID']] . "|" . $row[$k['PO']] . "|" . $itemId] = $itemId;
                $row[$k["Error Detail"] + 1] = $itemId;
                $row[$k["Error Detail"] + 2] = $existedChk["SKU|Size|Color"][$sku_size_color]['lot'];
                $row[$k["Error Detail"] + 3] = $uomId;
            }
        }

        // Check valid date
        $date = date_parse($row[$k["Expected Ship Date"]]);
        if (
            $date["error_count"] != 0 ||
            !checkdate($date["month"], $date["day"], $date["year"]) ||
            !preg_match("/^(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])\/[0-9]{4}$/", $row[$k["Expected Ship Date"]])
        ) {
            $errorDetail .= "The Expected Ship Date must follow the pattern \"MM/DD/YYYY\"\n";
        }
        $now = date("Y/m/d", time());
        $now = strtotime($now . " 00:00:00");
        $ship = strtotime($date['year'] . "/" . $date['month'] . "/" . $date['day'] . " 00:00:00");
        if ($ship < $now) {
            $errorDetail .= "Expected Ship Date must be greater than or equal to current date\n";
        }

        // Check valid Order Date
        $odrDate = date_parse($row[$k["Order Date"]]);
        if (
            $odrDate["error_count"] != 0 ||
            !checkdate($odrDate["month"], $odrDate["day"], $odrDate["year"]) ||
            !preg_match("/^(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])\/[0-9]{4}$/", $row[$k["Order Date"]])
        ) {
            $errorDetail .= "The Order Date  must follow the pattern \"MM/DD/YYYY\"\n";
        }

        // Check duplicate Ref_num
        if (!empty($row[$k["Order ID"]])) {
            if (isset($existedChk["Order ID"][strtoupper($row[$k["Order ID"]])])) {
                $errorDetail .= Message::get("BM006", "Order ID") . PHP_EOL;
            }

            if (strlen($row[$k["Order ID"]]) > 64) {
                $errorDetail .= Message::get("VR011", "Order ID", "64 chars.") . PHP_EOL;
            }
        }

        // Check duplicate Warehouse|Order ID|PO|NW|ABC
        if (isset($existedChk["Order ID|PO"][strtoupper($row[$k["Order ID"]] . "-" . $row[$k["PO"]])])) {
            $errorDetail .= Message::get("BM006", "Order ID-PO") . PHP_EOL;
        }

        // Check duplicate Tracking Id
        $odr_po_sku_size_color_upc_track =
            $row[$k["Order ID"]] . "-" .
            $row[$k["PO"]] . "-" .
            $row[$k["SKU"]] . "-" .
            $row[$k["Size"]] . "-" .
            $row[$k["Color"]] . "-" .
            $row[$k["UPC"]] . "-" .
            $row[$k["Shipment Tracking ID"]];
        if (!empty($row[$k["Shipment Tracking ID"]]) &&
            !empty($this->trackingIds[$odr_po_sku_size_color_upc_track])
        ) {
            $errorDetail .= Message::get("BM006", $row[$k["Shipment Tracking ID"]]) . PHP_EOL;
        }

        $this->trackingIds[$odr_po_sku_size_color_upc_track] = $row[$k["Shipment Tracking ID"]];

        // Validate UPC 12 digits
        if (!empty($row[$k["UPC"]])) {
            if (!is_numeric($row[$k["UPC"]])) {
                $errorDetail .= "UPC must be number.\n";;
            } else if (strlen($row[$k["UPC"]]) !== 12) {
                $errorDetail .= Message::get("VR011", "UPC", "12 digits.") . PHP_EOL;
            }
        }

        // Check Piece Qty
        if (
            isset($row[$k["Piece Quantity"]]) &&
            $row[$k["Piece Quantity"]] !== "" &&
            $row[$k["Piece Quantity"]] !== null
        ) {
            if ($row[$k["Piece Quantity"]] == 0 || $row[$k["Piece Quantity"]] < 0) {
                $errorDetail .= Message::get("VR012", "Piece Quantity", "0") . PHP_EOL;
            } else if (!is_numeric($row[$k["Piece Quantity"]])) {
                $errorDetail .= "Piece Quantity must be integer.\n";
            }
        }

        // Check System State
        if (empty($row[$k['Ship To country']])) {
            $row[$k['Ship To country']] = 'US';
        }

        if (
            !empty($row[$k['Ship To State']]) &&
            empty($this->states[strtoupper($row[$k['Ship To country']] . "-" . $row[$k['Ship To State']])])
        ) {
            $errorDetail .= "Country and State is invalid.\n";
        }

        if (empty($this->orders[$row[$k["Order ID"]] . "-" . $row[$k["PO"]]])) {
            $this->orders[$row[$k["Order ID"]] . "-" . $row[$k["PO"]]] = 1;
        }

        foreach ($this->maxLength as $key => $length) {
            if (!empty($row[$k[$key]]) && strlen($row[$k[$key]]) > $length) {
                $errorDetail .= Message::get("VR033", $key, $length) . PHP_EOL;
            }
        }

        return $errorDetail;

    }

    /**
     * @param $dir
     * @param $data
     * @param $return
     *
     * @return mixed
     */
    private function writeExcel($dir, $data, $return)
    {
        $hashKey = $this->_hash;

        Excel::create($hashKey, function ($file) use ($data) {

            $file->sheet('Report', function ($sheet) use ($data) {
                $sheet->setAutoSize(true);
                $sheet->setHeight(1, 30);

                // Fill suggest Data
                $sheet->fromArray($data, null, 'A1', true, false);

                // Wrap Text
                $letter = $this->getLetterByNumber($this->keys["Error Detail"]);
                $sheet->getStyle("{$letter}1:$letter" . (count($data)))->getAlignment()->setWrapText(true);

            });
        })->store('xlsx', $dir);

        return $return;
    }

    /**
     * @param $cusId
     *
     * @return array
     * @throws Exception
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    public function processImport($cusId)
    {
        $hashKey = $this->_hash;
        $type = Type::XLSX;
        $filePath = storage_path('valid-import') . "/$hashKey.$type";
        $reader = ReaderFactory::create($type);
        $reader->open($filePath);

        $result = [];
        $i = 0;

        try {
            // Begin Transaction
            DB::beginTransaction();

            $deleted_at = getDefaultDatetimeDeletedAt();
            $created_by = JWTUtil::getPayloadValue('jti') ?? 0;
            $po_total = [];
            foreach ($reader->getSheetIterator() as $sheet) {
                $paramHdr = [];
                $paramSO = [];
                foreach ($sheet->getRowIterator() as $row) {
                    if (empty($paramSO[$row[1]])) {
                        $this->shippingOrderModel->refreshModel();
                        $so_id = $this->shippingOrderModel->create([
                            'whs_id'      => $row[26],
                            'cus_id'      => $cusId,
                            'cus_odr_num' => $row[1],
                            'po_total'    => 1,
                            'so_sts'      => Status::getByValue("New", "Order-Status"),
                            'type'        => Status::getByValue("Ecomm", "Order-Type"),
                        ])->so_id;
                        $paramSO[$row[1]] = $so_id;
                        $po_total[$so_id] = 1;
                    } else {
                        $po_total[$paramSO[$row[1]]]++;
                    }

                    $i++;
                    if (empty($paramHdr[$row[1]][$row[2]])) {
                        $this->orderHdrModel->refreshModel();
                        $odrNum = $this->orderHdrModel->getOrderNum();

                        $param = [
                            'so_id'           => $paramSO[$row[1]],
                            'odr_num'         => $odrNum,
                            'cus_odr_num'     => $row[1],
                            'ref_cod'         => $row[1],
                            'cus_po'          => $row[2],
                            'odr_type'        => Status::getByValue("Ecomm", "Order-Type"),
                            'odr_sts'         => Status::getByValue("New", "Order-Status"),
                            'ship_to_name'    => trim($row[3] . " " . $row[4]),
                            'ship_to_add_1'   => $row[5],
                            'ship_to_add_2'   => $row[6],
                            'ship_to_city'    => $row[7],
                            'ship_to_state'   => $row[8],
                            'ship_to_zip'     => $row[9],
                            'ship_to_country' => $row[10],
                            'odr_req_dt'      => strtotime($row[17]),
                            'req_cmpl_dt'     => strtotime($row[21]),
                            'carrier'         => $row[18],
                            'cus_notes'       => $row[19],
                            'ship_by_dt'      => strtotime($row[21]),
                            'cancel_by_dt'    => strtotime($row[21]),
                            'website'         => $row[22],
                            'whs_id'          => $row[26],
                            'cus_id'          => $cusId,
                            'created_at'      => time(),
                            'created_by'      => $created_by,
                            'updated_at'      => time(),
                            'updated_by'      => $created_by,
                            'deleted_at'      => $deleted_at,
                            'deleted'         => 0,
                            'sts'             => 'i',
                            'is_ecom'         => 1,
                        ];
                        $this->orderHdrModel->refreshModel();
                        $odrId = $this->orderHdrModel->create($param)->odr_id;
                        $paramHdr[$row[1]][$row[2]] = $odrId;
                        $paramHdr['sku_ttl'][$odrId] = 0;

                        // Create Event Tracking
                        $this->eventTrackingModel->refreshModel();
                        $this->eventTrackingModel->create([
                            'whs_id'    => $row[26],
                            'cus_id'    => $cusId,
                            'owner'     => $odrNum,
                            'evt_code'  => Status::getByKey("event", "Order-New"),
                            'trans_num' => $odrNum,
                            'info'      => sprintf(Status::getByKey("event-info", "Order-New"), $odrNum)
                        ]);
                    }

                    $odrId = $paramHdr[$row[1]][$row[2]];
                    $dtl = [
                        'odr_id'        => $odrId,
                        'whs_id'        => $row[26],
                        'cus_id'        => $cusId,
                        'item_id'       => $row[23],
                        'color'         => $row[13],
                        'sku'           => $row[11],
                        'size'          => $row[12],
                        'lot'           => $row[24],
                        'pack'          => 1,
                        'qty'           => $row[15],
                        'piece_qty'     => $row[15],
                        'cus_upc'       => $row[14],
                        'sts'           => 'i',
                        'ship_track_id' => $row[20],
                        'created_at'    => time(),
                        'created_by'    => $created_by,
                        'updated_at'    => time(),
                        'updated_by'    => $created_by,
                        'deleted_at'    => $deleted_at,
                        'deleted'       => 0,
                        'uom_id'        => $row[25]
                    ];

                    $result[] = $dtl;

                    if ($i >= 500) {
                        OrderDtl::insert($result);
                        $i = 0;
                        $result = [];
                    }

                    //update sku_tt
                    $paramHdr['sku_ttl'][$odrId]++;
                }
                // Only process one sheet
                break;
            }
            $reader->close();

            if (!empty($result)) {
                OrderDtl::insert($result);
            }

            // Update Shipping Order Po total
            if (!empty($po_total)) {
                $poTotal = [];
                foreach ($po_total as $soId => $count) {
                    $poTotal[$count][] = $soId;
                }

                if (!empty($poTotal)) {
                    $this->shippingOrderModel->updateImportShipping($poTotal);
                }
            }

            //update sku_total
            if (!empty($paramHdr['sku_ttl'])) {
                $skuTotal = [];
                foreach ($paramHdr['sku_ttl'] as $odrId => $total) {
                    $skuTotal[$total][] = $odrId;
                }

                if (!empty($poTotal)) {
                    $this->orderHdrModel->updateImportSku($skuTotal);
                }
            }

            // Commit transaction
            DB::commit();

        } catch (\Illuminate\Database\QueryException $ex) {
            if ($ex->getCode() === '23000') {
                var_dump($ex->getMessage());
                exit;
                throw new Exception('Please contact Admin!', 404);
            } else {
                throw $ex;
            }

            // Rollback transaction
            DB::rollBack();
        }

        // Remove import csv file
        unlink($filePath);

        return ['status' => true, 'message' => 'The Order file was imported successfully!'];
    }

    /**
     * @param $row
     * @param bool|true $full
     */
    private function declareVariable($row, $full = true)
    {
        $this->keys = $keys = [
            'Warehouse Code'         => array_search('Warehouse Code', $row),
            'Order ID'               => array_search('Order ID', $row),
            'PO'                     => array_search('PO', $row),
            'First Name'             => array_search('First Name', $row),
            'Last Name'              => array_search('Last Name', $row),
            'Ship To Address Line 1' => array_search('Ship To Address Line 1', $row),
            'Ship To Address Line 2' => array_search('Ship To Address Line 2', $row),
            'Ship To City'           => array_search('Ship To City', $row),
            'Ship To State'          => array_search('Ship To State', $row),
            'Ship To Zip'            => array_search('Ship To Zip', $row),
            'Ship To country'        => array_search('Ship To country', $row),
            'SKU'                    => array_search('SKU', $row),
            'Size'                   => array_search('Size', $row),
            'Color'                  => array_search('Color', $row),
            'UPC'                    => array_search('UPC', $row),
            'Piece Quantity'         => array_search('Piece Quantity', $row),
            'Product Name'           => array_search('Product Name', $row),
            'Order Date'             => array_search('Order Date', $row),
            'Carrier'                => array_search('Carrier', $row),
            'Third Party'            => array_search('Third Party', $row),
            'Shipment Tracking ID'   => array_search('Shipment Tracking ID', $row),
            'Expected Ship Date'     => array_search('Expected Ship Date', $row),
            'Website'                => array_search('Website', $row)
        ];

        if ($full) {
            if (array_filter($this->keys)) {
                $this->_header = [
                    $keys['Warehouse Code']         => 'Warehouse Code',
                    $keys['Order ID']               => 'Order ID',
                    $keys['PO']                     => 'PO',
                    $keys['First Name']             => 'First Name',
                    $keys['Last Name']              => 'Last Name',
                    $keys['Ship To Address Line 1'] => 'Ship To Address Line 1',
                    $keys['Ship To Address Line 2'] => 'Ship To Address Line 2',
                    $keys['Ship To City']           => 'Ship To City',
                    $keys['Ship To State']          => 'Ship To State',
                    $keys['Ship To Zip']            => 'Ship To Zip',
                    $keys['Ship To country']        => 'Ship To country',
                    $keys['SKU']                    => 'SKU',
                    $keys['Size']                   => 'Size',
                    $keys['Color']                  => 'Color',
                    $keys['UPC']                    => 'UPC',
                    $keys['Piece Quantity']         => 'Piece Quantity',
                    $keys['Product Name']           => 'Product Name',
                    $keys['Order Date']             => 'Order Date',
                    $keys['Carrier']                => 'Carrier',
                    $keys['Third Party']            => 'Third Party',
                    $keys['Shipment Tracking ID']   => 'Shipment Tracking ID',
                    $keys['Expected Ship Date']     => 'Expected Ship Date',
                    $keys['Website']                => 'Website',
                ];
            }

            $this->required = [
                $keys['Order ID']               => 'Order ID',
                $keys['PO']                     => 'PO',
                $keys['First Name']             => 'First Name',
                $keys['Last Name']              => 'Last Name',
                $keys['Ship To Address Line 1'] => 'Ship To Address Line 1',
                $keys['Ship To City']           => 'Ship To City',
                $keys['Ship To State']          => 'Ship To State',
                $keys['Ship To Zip']            => 'Ship To Zip',
                $keys['SKU']                    => 'SKU',
                $keys['Piece Quantity']         => 'Piece Quantity',
                $keys['Order Date']             => 'Order Date',
                $keys['Carrier']                => 'Carrier',
                $keys['Shipment Tracking ID']   => 'Shipment Tracking ID',
                $keys['Expected Ship Date']     => 'Expected Ship Date'
            ];

            $this->integers = [
                $keys['Piece Quantity'] => 'Piece Quantity'
            ];

            $this->_notExisted = [
                $keys['Warehouse Code'] => 'Warehouse Code',
            ];

            $this->maxLength = [
                'Order ID'               => 64,
                'PO'                     => 50,
                'First Name'             => 60,
                'Last Name'              => 60,
                'Ship To Address Line 1' => 127,
                'Ship To Address Line 2' => 127,
                'Ship To City'           => 63,
                'Ship To State'          => 63,
                'Ship To Zip'            => 10,
                'Ship To country'        => 50,
                'SKU'                    => 50,
                'Size'                   => 50,
                'Color'                  => 50,
                'UPC'                    => 20,
                'Piece Quantity'         => 11,
                'Product Name'           => 255,
                'Carrier'                => 63,
                'Third Party'            => 255,
                'Shipment Tracking ID'   => 30,
                'Website'                => 255,
            ];
        }
    }

    /**
     * @param $num
     *
     * @return string
     */
    private function getLetterByNumber($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getLetterByNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }

    /**
     * @param $data
     * @param $row
     */
    private function addToKey($data, $row)
    {
        foreach ($data as $item) {
            $this->keys[$item] = array_search($item, $row);
        }
    }

    protected function isInt($val)
    {
        return intVal($val) == $val;
    }
}

