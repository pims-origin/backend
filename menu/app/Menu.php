<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 11:04 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Utils\Database\Eloquent\SoftDeletes;

/**
 * Class Menu
 *
 * @package App\Api\V1\Entities
 * @property mixed menu_id
 * @property mixed name
 * @property mixed description
 * @property mixed url
 * @property mixed target
 *
 * @method get()
 * @method findOrFail(int)
 */
class Menu extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menu';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'menu_id';

    /**
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'menu_group_id',
        'name',
        'description',
        'url',
        'display_order',
        'target',
        'permission_name',
        'icon_class',
    ];

    public function menuGroup()
    {
        return $this->belongsTo('App\MenuGroup', 'menu_group_id', 'menu_group_id');
    }

    public function menuFavorite()
    {
        return $this->hasOne('\App\MenuFavorite', 'menu_id', 'menu_id');
    }

    public function authority(){
        return $this->hasOne('\App\Authority','permission_name', 'name' );
    }
}
