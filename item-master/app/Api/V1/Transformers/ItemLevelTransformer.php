<?php
/**
 * Created by PhpStorm.
 * User: Long Do
 * Date: 07-14-2017
 * Time: 13:48
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\ItemLevel;

class ItemLevelTransformer extends TransformerAbstract
{
    /*
     * Transform item
     *
     * @param object $item
     * @return array
     */

    public function transform(ItemLevel $item)
    {
        return [
            'level_id' => $item->level_id,
            'pkg_code' => $item->pkg_code,
            'pkg_name' => $item->pkg_name
        ];
    }

}
