<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class CartonModel extends AbstractModel
{
    const PICK_CARTON = 'CT';
    const PICK_PIECE = 'PC';
    const PICK_PALLET = 'PL';

    const MAX_TTL_CTN = 10000;

    protected $user_id;
    protected $time;
    protected $itemModel;

    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null, ItemModel $itemModel)
    {
        $this->model = ($model) ?: new Carton();
        $this->user_id = Data::getCurrentUserId();
        $this->time = time();
        $this->itemModel = $itemModel;
    }

    public function getSuggest($attributes, $limit = null, $with = [])
    {
        if (!is_array($attributes['loc_id'])) {
            $attributes['loc_id'] = [$attributes['loc_id']];
        }

        $query = $this->make($with)
            ->select(['ctn_id', 'ctn_num'])
            ->where('whs_id', $attributes['whs_id'])
            ->where('item_id', $attributes['item_id'])
            ->whereIn('loc_id', $attributes['loc_id'])
            ->where('ctn_sts', "AC")
            ->whereNull('rfid')
        ;

        if (!empty($limit)) {
            $model = $query->take($limit)->get();
        } else {
            $model = $query->get();
        }

        return $model;
    }

    /**
     * @param $ctnNum
     *
     * @return mixed
     */
    public function checkActiveCartonByCtnNum($ctnNum)
    {
        $ctnNums = is_array($ctnNum) ? $ctnNum : [$ctnNum];

        return $this->model->whereIn('ctn_num', $ctnNums)
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('loc_id')
            ->select('ctn_id', 'ctn_num')
            ->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function getAllCartonByItemId($data)
    {
        return $this->model->select([
                'cartons.*',
                'pallet.plt_num'
            ])
            ->leftJoin('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->whereIn('ctn_num', $data['ctnNums'])
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('cartons.item_id', $data['itemId'])
            ->where('cartons.whs_id', $data['whsId'])
            ->where('cartons.cus_id', $data['cusId'])
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('cartons.loc_id')
            ->get();
    }

    /**
     * @param $locId
     *
     * @return mixed
     */
    public function getAllCartonByLocIds($locId)
    {

        $locIds = is_array($locId) ? $locId : [$locId];

        return $this->model
            ->leftJoin('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->whereIn('cartons.loc_id', $locIds)
            ->get();
    }

    public function getCartonsByLocs($locIds, $limit = null)
    {
        $query = $this->model->select(['ctn_id', 'ctn_num'])
            ->whereIn('loc_id', $locIds)
            ->where('ctn_sts', "AC")
            ->where('loc_type_code', 'RAC')
            ->where(function ($q) {
                $q->orWhere('is_damaged', 0);
                $q->orWhereNull('is_damaged');
            })
            ->where('is_ecom', 0);

        if (!empty($limit)) {
            return $query->limit($limit)->get();
        } else {
            return $query->get();
        }
    }

    public function getAvailableQuantity($itemID, $lot, $locId, $whs_id, $lpnCarton = null)
    {
        $query = $this->getModel()
            ->where('loc_id', $locId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
//            ->whereNull('rfid')
            ->where('loc_type_code', 'RAC');
//            ->where('lot', $lot);
        if (! empty($lpnCarton)) {
            $query->where('lpn_carton', $lpnCarton);
        }

        $qty = $query->sum('piece_remain');

        return $qty;


    }

    public function getAllCartonsByWvDtl($wvDtl, $algorithm = 'FIFO', $locIds, $actLocs)
    {
        $packSize = $wvDtl['pack_size'];

        $whsId = $wvDtl['whs_id'];
        $itemId = $wvDtl['item_id'];
        $lot = $wvDtl['lot'];
        $packSizes = $this->getAvailablePacksizes($whsId, $itemId, $lot, $locIds, $algorithm);

        if (!in_array($packSize, $packSizes)) {
            $packSize = array_shift($packSizes);
        } else {
            $packSizes = $this->array_remove($packSize, $packSizes);
        }

        array_unshift($packSizes, $packSize);
        $pickedcartons = [];

        foreach ($actLocs as $actLoc) {
            $pickedQty = $actLoc['picked_qty'];
            foreach ($packSizes as $packSize) {
                $ctns = ceil($pickedQty / $packSize) + 50;
                $cartons = $this->getCartonsByPack($whsId, $itemId, $lot, $actLoc['act_loc_id'], $packSize, $ctns,
                    $algorithm, $actLoc['lpn_carton']);

                $qtys = array_column($cartons, 'piece_remain');
                $total = array_sum($qtys);
                $pickedcartons = array_merge($pickedcartons, $this->getPickCartons($cartons, $pickedQty));
                $pickedQty = $pickedQty - $total;

                //break if enough cartons
                if ($pickedQty <= 0) {
                    break;
                }
            }
        }

        return $pickedcartons;
    }

    public function getAvailablePacksizes($whs_id, $itemID, $lot, $locIds, $algorithm)
    {
        $query = $this->getModel()
            ->whereIn('loc_id', $locIds)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('loc_type_code', 'RAC')
//            ->where('lot', $lot)
            ->groupBy('ctn_pack_size');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        }


        $res = $query->get(['ctn_pack_size'])->toArray();

        return array_column($res, 'ctn_pack_size');
    }

    public function array_remove($remove, &$array)
    {
        foreach ((array)$array as $key => $value) {
            if ($value == $remove) {
                unset($array[$key]);
                break;
            }
        }

        return $array;
    }


    public function getCartonsByPack($whs_id, $itemID, $lot, $locId, $packSize, $ctns, $algorithm, $lpnCarton = null)
    {
        $this->getModel()->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->where('loc_id', $locId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
//            ->whereNull('rfid')
            ->where('loc_type_code', 'RAC')
//            ->where('lot', $lot)
            ->where('ctn_pack_size', $packSize)
            ->limit($ctns)
            ->orderBy('ctn_pack_size');

        if (! empty($lpnCarton)) {
            $query->where('lpn_carton', $lpnCarton);
        }

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        }

        $query->orderBy('cartons.piece_remain', 'ASC');

        $res = $query->get()->toArray();

        return $res;
    }


    public function getPickCartons($cartons, $pickedQty)
    {
        $pickedcartons = [];
        foreach ($cartons as $carton) {
            //pick cartons
            $carton['pick_piece'] = false;
            if ($pickedQty < $carton['piece_remain']) {
                $carton['pick_piece'] = true;
                $carton['picked_qty'] = $pickedQty;
                $pickedQty = 0;
            } elseif ($pickedQty == $carton['piece_remain']) {
                $carton['picked_qty'] = $carton['piece_remain'];
                $pickedQty = 0;
            } else {
                $pickedQty = $pickedQty - $carton['piece_remain'];
                $carton['picked_qty'] = $carton['piece_remain'];
            }
            $pickedcartons[] = $carton;

            if ($pickedQty <= 0) {
                break;
            }
        }

        return $pickedcartons;
    }

    public function updateCartonByLocs($cartons)
    {
        /**
         * 1. Update full carton
         * 2. Update piece_remain and piece_ttl
         */
        $fullCtns = [0];
        foreach ($cartons as $idx => $carton) {
            if ($carton['pick_piece']) {
                $cartons[$idx] = $this->updatePickedPieceCarton($carton);
            } else {
                $fullCtns[$carton['ctn_id']] = $carton;
            }
        }

        $this->updatePickedFullCartons($fullCtns);
        
        return $cartons;
    }

    public function updatePickedPieceCarton($carton)
    {
        $remainQty = $carton['piece_remain'] - $carton['picked_qty'];
        $res = DB::table('cartons')->where('ctn_id', $carton['ctn_id'])->update(
            ['piece_remain' => $remainQty]
        );

        //change business clone new carton
        $origCtnId = $carton['ctn_id'];
        $origLocId = $carton['loc_id'];
        $maxCtnNum = $this->getMaxCtnNum($carton['ctn_num']);
        unset($carton['ctn_id']);

        $carton['inner_pack'] = 0;

        $newCarton = (new Carton())->fill($carton);
        $newCarton->piece_remain = $carton['picked_qty'];
        $newCarton->origin_id = $origCtnId;
        $newCarton->loc_id = $newCarton->loc_code = null;
        $newCarton->rfid = null;
        $newCarton->ctn_sts = 'PD';
        $newCarton->picked_dt = time();
        $newCarton->ctn_num = ++$maxCtnNum;
        $newCarton->save();
        
        $return = $newCarton->toArray();
        $return['pick_piece'] = true;
        $return['loc_id'] = $origLocId;
        $return['loc_code'] = $return['loc_name'];
        $return["picked_qty"] = $carton['picked_qty'];
      
        return $return;
    }

    public function updatePickedFullCartons($fullCtns)
    {
        $ctnIDs = array_keys($fullCtns);
        foreach (array_chunk($ctnIDs, 200) as $chunkCtnIds) {
            DB::table('cartons')->whereIn('ctn_id', $chunkCtnIds)->update(
                [
                    'ctn_sts' => 'PD',
                    'loc_id' => null,
                    'loc_code' => null,
                    //'loc_name'         => null,
                    //'plt_id'           => null,
                    'picked_dt' => time(),
                    'storage_duration' => self::getCalculateStorageDurationRaw()
                ]
            );
        }
    }

    public static function getCalculateStorageDurationRaw($startDate = 'created_at')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    public function getFullPallet($whsId, $itemId, $lot)
    {
        $query = DB::table('cartons')
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->select([
                DB::raw('pallet.*')
            ])
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('pallet.ctn_ttl', '>', 0)
            ->whereRaw('pallet.init_ctn_ttl = pallet.ctn_ttl')
            ->orderBy('pallet.init_ctn_ttl', 'DESC');

        //var_dump($query->getBindings());exit($query->toSql());
        return $query->first();
    }

    public function sortDeepLocationPicking($data)
    {
        for ($i = 0; $i < count($data); $i++) {

            if (isset($data[$i + 1])) {

                $next = $data[$i + 1]['loc_code'];
                $cur = $data[$i]['loc_code'];
                $curloc = substr($cur, 0, strlen($cur) - 1);
                $nextLoc = substr($next, 0, strlen($next) - 1);
                if ($curloc == $nextLoc) {
                    if (substr($cur, -1, 1) > substr($next, -1, 1)) {
                        $next = $data[$i + 1];
                        $current = $data[$i];
                        $data[$i + 1] = $current;
                        $data[$i] = $next;
                        $i = $i + 1;
                    }

                }

            }
        }

        return $data;
    }

    public function getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take, $IgnorefirstLevel = true)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $query = DB::table('cartons')
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->select([
                'pallet.loc_id',
                'pallet.loc_code',
                'pallet.rfid',
                DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS ctns')
            ])
            ->where('location.loc_sts_code', 'AC')
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
//            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId)
            ->where('pallet.deleted', 0)
            ->whereRaw('pallet.init_ctn_ttl = pallet.ctn_ttl')
            ->groupBy('pallet.loc_id');

        if ($IgnorefirstLevel) {
            //Remove A = Level 1
            $query->whereRaw("pallet.loc_code REGEXP '([B-Z])[1-2]$' ");
        }

        $query->orderBy('pallet.ctn_ttl', 'DESC');
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                // $query->orderBy('cartons.gr_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        };
        $query->orderBy(DB::Raw("pallet.loc_code"), 'DESC');

        return $query->limit($take)->skip(0)->get();
    }

    public function getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl, $firstLevel = true)
    {
        $query = DB::table('cartons')
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->select([
                'pallet.loc_id',
                'pallet.loc_code',
                DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS ctns')
            ])
            ->where('location.loc_sts_code', 'AC')
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
//            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId)
            ->where('pallet.deleted', 0)
            ->groupBy('pallet.loc_id');

        if ($pltTtl < 1 && $firstLevel) {
            $query->whereRaw("pallet.loc_code REGEXP '([A])[1-2]$' ");
            $query->whereRaw('pallet.init_ctn_ttl != pallet.ctn_ttl');

        }

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        }


        if ($pltTtl >= 1) {
            $query->orderBy("pallet.ctn_ttl", 'DESC');
            $query->orderBy(DB::Raw("pallet.loc_code"), 'DESC');
        } else {
            $query->orderBy("pallet.ctn_ttl", 'ASC');
            $query->orderBy(DB::Raw("pallet.loc_code"), 'ASC');
        }

        return $query->limit($take)->skip(0)->get();
    }

    /**
     * @param array $locIds
     * @param int $itemId
     *
     * @return mixed
     */
    public function getMoreSugLocByWvDtl($wvDtl, $pickFullPallet, $take = 8)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();
        $itemId = $wvDtl->item_id;
        $lot = $wvDtl->lot;
        $cusId = $wvDtl->cus_id;
        $pickQty = $wvDtl->piece_qty - $wvDtl->act_piece_qty;
        $pickedCTNS = ceil($pickQty / $wvDtl->pack_size);

        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cusId);

        $fullPallet = $this->getFullPallet($whsId, $itemId, $lot);
        $pltTtl = 0;


        if ($fullPallet) {
            $pltTtl = intval($pickedCTNS / $fullPallet['ctn_ttl']);
        }

        if ($pickFullPallet) {
            if ($pltTtl >= 1) {
                $ignoreFirstLevel = true;
                $locs = $this->getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take,
                    $ignoreFirstLevel);
                if (count($locs) == 0) {
                    $locs = $this->getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take,
                        $ignoreFirstLevel);
                }

                if (count($locs) > 0) {
                    return $locs;
                }
            }

        }

        $firstLevel = false;
        $locs = $this->getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl);
        $pickFullPallet = true;
        if (count($locs) == 0) {
            $locs = $this->getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl, $firstLevel);
        }


        return $locs;

    }

    public function checkLocByWvDtl($locId, $itemId, $lot)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        return $this->model->select([
            'loc_id',
            'loc_code',
            DB::raw('COALESCE(SUM(piece_remain), 0) as avail_qty'),
            DB::raw('COUNT(ctn_id) as ctns')
        ])
            ->where('whs_id', Data::getCurrentWhsId())
            ->where('loc_id', $locId)
            ->where('item_id', (int)$itemId)
//            ->where('lot', $lot)
            ->first();
    }

    public function checkPalletByWvDtl($pltId, $itemId, $lot)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model->select([
            'loc_id',
            'loc_code',
            DB::raw('COALESCE(SUM(piece_remain), 0) as avail_qty'),
            DB::raw('COUNT(ctn_id) as ctns')
        ])
            ->where('whs_id', Data::getCurrentWhsId())
            ->where('plt_id', $pltId)
            ->where('item_id', (int)$itemId)
//            ->where('lot', $lot)
            ->first()
            ;
    }

    /*
     * update for pick pallet
     *
     */

    public function getAvailableQuantityByPallet($itemID, $lot, $pltId, $whs_id)
    {
        $query = $this->getModel()
            ->where('plt_id', $pltId)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('lot', $lot);

        $qty = $query->sum('piece_remain');

        return $qty;


    }


    public function getAllCartonsPalletByWvDtl($wvDtl, $algorithm = 'FIFO', $actLocs)
    {
        $packSize = $wvDtl['pack_size'];

        $whsId = $wvDtl['whs_id'];
        $itemId = $wvDtl['item_id'];
        $lot = $wvDtl['lot'];
        $packSizes = $this->getAvailablePacksizesByPallet($whsId, $itemId, $lot,  $algorithm);

        if (!in_array($packSize, $packSizes)) {
            $packSize = array_shift($packSizes);
        } else {
            $packSizes = $this->array_remove($packSize, $packSizes);
        }

        array_unshift($packSizes, $packSize);
        $pickedcartons = [];


        $pickedQty = $actLocs['picked_qty'];
        foreach ($packSizes as $packSize) {
            $ctns = ceil($pickedQty / $packSize) + 50;
            $cartons = $this->getCartonsPalletByPack($whsId, $itemId, $lot, $actLocs['plt_id'], $packSize, $ctns,
                $algorithm);

            $qtys = array_column($cartons, 'piece_remain');
            $total = array_sum($qtys);
            $pickedcartons = array_merge($pickedcartons, $this->getPickCartons($cartons, $pickedQty));
            $pickedQty = $pickedQty - $total;

            //break if enough cartons
            if ($pickedQty <= 0) {
                break;
            }
        }


        return $pickedcartons;
    }



    public function getAvailablePacksizesByPallet($whs_id, $itemID, $lot,  $algorithm)
    {
        $query = $this->getModel()
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('lot', $lot)
            ->groupBy('ctn_pack_size');

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }


        $res = $query->get(['ctn_pack_size'])->toArray();

        return array_column($res, 'ctn_pack_size');
    }


    public function getCartonsPalletByPack($whs_id, $itemID, $lot, $plt_id, $packSize, $ctns, $algorithm)
    {
        $this->getModel()->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->where('plt_id', $plt_id)
            ->where('item_id', $itemID)
            ->where('whs_id', $whs_id)
            ->where('is_damaged', 0)
            ->where('is_ecom', 0)
            ->where('ctn_sts', 'AC')
            ->where('lot', $lot)
            ->where('ctn_pack_size', $packSize)
            ->limit($ctns)
            ->orderBy('ctn_pack_size');


        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                $query->orderBy('cartons.created_at', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        $query->orderBy('cartons.piece_remain', 'ASC');

        $res = $query->get()->toArray();

        return $res;
    }

    public function getMaxCtnNum($ctnNum)
    {
        $maxCtnNum = $this->model->where('ctn_num', 'LIKE', $ctnNum . '-%')->max('ctn_num');
        if ($maxCtnNum) {
            return $maxCtnNum;
        }
        
        return $ctnNum . '-C00';
    }

    public function _generateCtnNum()
    {
        $today_yymm = date('ym');
        $maxCarton = DB::table("cartons")->where([
            'deleted'    => 0,
            'deleted_at' => 915148800
        ])->where("ctn_num","LIKE","CTN-".$today_yymm."%")
           ->where('ctn_num', 'NOT LIKE', 'CTN-'.$today_yymm.'-C%')
            ->orderBy('ctn_id', 'desc')->first();

        if (!$maxCarton) {
            $ctnNum = "CTN-{$today_yymm}-0000001";

            return $ctnNum;
        }
        return $maxCarton['ctn_num'];
    }

    public function getNewCtnNum($ctn_num) {
        $aNum = explode("-", $ctn_num);
        $aTemp = array_keys($aNum);
        $end = end($aTemp);
        $index = (int)$aNum[$end];
        $aNum[0] = "CTN";
        $aNum[$end] = str_pad(++$index, 7, "0", STR_PAD_LEFT);
        $ctnNum = implode("-", $aNum);
        return $ctnNum;
    }

    public function getItemByAsn($attribute, $whsId) {
        return $this->getModel()
            ->join("asn_dtl","asn_dtl.asn_dtl_id", "=", "cartons.asn_dtl_id")
            ->leftJoin("pallet", "pallet.plt_id", "=", "cartons.plt_id")
            ->where("asn_dtl.asn_hdr_id", $attribute['asn_hdr_id'])
            ->where("cartons.whs_id", $whsId)
            ->where("cartons.cus_id", $attribute['cus_id'])
            ->groupBy("cartons.item_id", "cartons.plt_id")
            ->select(
                DB::raw("SUM(piece_ttl) as qty_ttl"),
                "cartons.item_id",
                "cartons.sku",
                "cartons.loc_code",
                "cartons.lpn_carton"
            )->get();

    }

    public function checkSku($attribute, $whsId) {
        return $this->getModel()
                ->join("gr_dtl", "gr_dtl.gr_dtl_id","=","cartons.gr_dtl_id")
                ->join("gr_hdr", "gr_dtl.gr_hdr_id","=","gr_hdr.gr_hdr_id")
                ->join("asn_hdr", "asn_hdr.asn_hdr_id","=","gr_hdr.asn_hdr_id")
                ->join("customer","customer.cus_id","=","cartons.cus_id")
                ->where("cartons.sku", $attribute['sku'])
                ->where("cartons.whs_id", $whsId)
                ->whereNull("cartons.loc_id")
                ->where("gr_dtl.gr_dtl_sts","RE")
                ->where("asn_hdr.asn_type","2ST")
                ->groupBy("cartons.item_id","cartons.cus_id")
                ->select(
                    "customer.cus_name","cartons.item_id" ,"customer.cus_code","customer.cus_id",DB::raw("SUM(piece_ttl) as qty_ttl")
                )
                ->get();
    }

    public function getCartonByPlt($attribute, $whsId) {
        return $this->getModel()
            ->join("customer","customer.cus_id","=","cartons.cus_id")
            ->join("pallet","pallet.plt_id","=","cartons.plt_id")
            ->where("pallet.plt_num", $attribute['plt_num'])
            ->where("cartons.whs_id", $whsId)
            ->whereNull("cartons.loc_id")
            ->groupBy("cartons.item_id")
            ->select(
                "cartons.sku", "cartons.size", "cartons.color", "cartons.ctn_pack_size", "customer.cus_name","cartons.item_id" ,"customer.cus_code","customer.cus_id",DB::raw("SUM(cartons.piece_remain) as qty_ttl")
            )
            ->get();
    }

    public function getCartonByPlt4PW($attribute, $whsId) {

//        $year = "20" . substr($attribute['plt_num'], 2, 2);
//        $m =  substr($attribute['plt_num'], 4, 2);
//
//        $start = strtotime($year . "-" . $m . "-01");
//        $end = strtotime(date('Y-m-t', strtotime($year . "-" . $m . "-01 23:59:59")));

        $palletCreatedAt = DB::table('pallet')->where('plt_num', $attribute['plt_num'])->where('whs_id', $whsId)->where('deleted', 0)->value('created_at');

        $start = strtotime(date('Y-m-01', $palletCreatedAt));
        $end = strtotime(date('Y-m-t 23:59:59', $palletCreatedAt))-18000;

        return $this->getModel()
            ->join("customer","customer.cus_id","=","cartons.cus_id")
           // ->join("pallet","pallet.plt_id","=","cartons.plt_id")
            ->where("cartons.lpn_carton", $attribute['plt_num'])
            ->where("cartons.whs_id", $whsId)
            ->where('cartons.created_at', ">=", $start)
            ->where('cartons.created_at', "<=", $end)
            ->whereNull("cartons.loc_id")
            ->groupBy("cartons.item_id")
            ->select(
                "cartons.sku", "cartons.size", "cartons.color", "cartons.ctn_pack_size", "customer.cus_name","cartons.item_id" ,"customer.cus_code","customer.cus_id",DB::raw("SUM(cartons.piece_remain) as qty_ttl")
            )
            ->get();
    }

    public function getCartonByLPNCarton($lpnCarton, $whsId)
    {
        /*$time = DB::table('pallet')->where('plt_num', $lpnCarton)->where('whs_id', $whsId)->where('deleted', 0)->value('created_at');
        $fromTime = strtotime(date('Y-m-d 00:00:00', $time));
        $toTime = $fromTime + 86400 * 31;*/
        $carton = $this->model->where('lpn_carton', $lpnCarton)
            ->where('whs_id', $whsId)
            ->where('ctn_sts', 'RG')
            ->where('deleted', 0)
            ->whereNull('cartons.loc_id')
            //->whereBetween('created_at', [$fromTime, $toTime])
            ->select(['item_id', 'cus_id'])
            ->first();

        return $carton;
    }

    public function updateCartonPartialPutaway($cartonPutawayParams) {

        // Putaway all carton with Tote
        if(!empty($cartonPutawayParams['tote'])) {
            $cartons = $this->getModel()
                ->where("plt_id", $cartonPutawayParams['tote']->plt_id)
                ->where("piece_remain", ">", 0)
                ->where("ctn_sts", "RG")
                ->update([
                    'loc_id'         => $cartonPutawayParams["location"]->loc_id,
                    'loc_code'       => $cartonPutawayParams["location"]->loc_code,
                    'loc_name'       => $cartonPutawayParams["location"]->loc_code,
                    'plt_id'         => $cartonPutawayParams["pallet_to"]->plt_id,
                    'loc_type_code'  => 'RAC',
                ]);
            DB::table("rpt_carton")
                ->where("plt_id", $cartonPutawayParams['tote']->plt_id)
                ->where("piece_remain", ">", 0)
                ->where("ctn_sts", "AC")
                ->update([
                    'loc_id'         => $cartonPutawayParams["location"]->loc_id,
                    'loc_code'       => $cartonPutawayParams["location"]->loc_code,
                    'loc_name'       => $cartonPutawayParams["location"]->loc_code,
                    'plt_id'         => $cartonPutawayParams["pallet_to"]->plt_id,
                ]);

            // Update status Tote
            $cartonPutawayParams['tote']->plt_sts = "NW";
            $cartonPutawayParams['tote']->save();
            return 0;
        }

        $cartons = $this->getModel()
            ->where("plt_id", $cartonPutawayParams['pallet_to']->plt_id)
            ->where("piece_remain", ">", 0)
            ->where("ctn_sts", "RG")
            ->update([
                'loc_id'         => $cartonPutawayParams["location"]->loc_id,
                'loc_code'       => $cartonPutawayParams["location"]->loc_code,
                'loc_name'       => $cartonPutawayParams["location"]->loc_code,
                'loc_type_code'  => 'RAC',
            ]);
        DB::table("rpt_carton")
            ->where("plt_id", $cartonPutawayParams['pallet_to']->plt_id)
            ->where("piece_remain", ">", 0)
            ->where("ctn_sts", "AC")
            ->update([
                'loc_id'         => $cartonPutawayParams["location"]->loc_id,
                'loc_code'       => $cartonPutawayParams["location"]->loc_code,
                'loc_name'       => $cartonPutawayParams["location"]->loc_code
            ]);

        return 0;
    }

    protected function putawayFullCtn($cartonPutawayParams, $carton_limit) {
        $query = $this->getModel()
            ->join("gr_dtl", "gr_dtl.gr_dtl_id","=","cartons.gr_dtl_id")
            ->join("gr_hdr", "gr_dtl.gr_hdr_id","=","gr_hdr.gr_hdr_id")
            ->join("asn_hdr", "asn_hdr.asn_hdr_id","=","gr_hdr.asn_hdr_id")
            ->where("cartons.item_id", $cartonPutawayParams['item']->item_id)
            ->where("cartons.whs_id", $cartonPutawayParams["whs_id"])
            ->where("cartons.cus_id", $cartonPutawayParams["cus_id"])
            ->whereNull("cartons.loc_id")
            ->where("gr_dtl.gr_dtl_sts","RE")
            ->where("asn_hdr.asn_type","=","2ST")
            ->whereNull("cartons.plt_id")
            ->whereRaw("cartons.piece_remain = cartons.ctn_pack_size");
//        if(!empty($cartonPutawayParams['tote'])) {
//            $query->where("cartons.plt_id", $cartonPutawayParams['tote']->plt_id);
//        } else {
//            $query->whereNull("cartons.plt_id");
//        }

        $cartons = $query->limit($carton_limit)->pluck("cartons.ctn_id");
         return  $this->getModel()->whereIn("ctn_id",$cartons)->update([
                "cartons.loc_id"        => $cartonPutawayParams["location"]->loc_id,
                "cartons.loc_code"      => $cartonPutawayParams["location"]->loc_code,
                "cartons.loc_name"      => $cartonPutawayParams["location"]->loc_code,
                "cartons.plt_id"        => $cartonPutawayParams["pallet_to"]->plt_id
            ]);
    }

    protected function getCtnRemain($cartonPutawayParams) {
        $query = $this->getModel()
            ->join("gr_dtl", "gr_dtl.gr_dtl_id","=","cartons.gr_dtl_id")
            ->join("gr_hdr", "gr_dtl.gr_hdr_id","=","gr_hdr.gr_hdr_id")
            ->join("asn_hdr", "asn_hdr.asn_hdr_id","=","gr_hdr.asn_hdr_id")
            ->where("cartons.item_id", $cartonPutawayParams['item']->item_id)
            ->where("cartons.whs_id", $cartonPutawayParams["whs_id"])
            ->where("cartons.cus_id", $cartonPutawayParams["cus_id"])
            ->whereNull("cartons.loc_id")
            ->where("gr_dtl.gr_dtl_sts","RE")
            ->where("asn_hdr.asn_type","2ST")
            ->whereNull("cartons.plt_id")
            ->whereRaw("cartons.piece_remain < cartons.ctn_pack_size");

//        if(!empty($cartonPutawayParams['tote'])) {
//            $query->where("cartons.plt_id", $cartonPutawayParams['tote']->plt_id);
//        } else {
//            $query->whereNull("cartons.plt_id");
//        }
        return $query->orderBy("cartons.piece_remain")
                    ->select("cartons.*")->get();
    }

    protected function getFirstCtnFull($cartonPutawayParams) {
        $query = $this->getModel()
                ->join("gr_dtl", "gr_dtl.gr_dtl_id","=","cartons.gr_dtl_id")
                ->join("gr_hdr", "gr_dtl.gr_hdr_id","=","gr_hdr.gr_hdr_id")
                ->join("asn_hdr", "asn_hdr.asn_hdr_id","=","gr_hdr.asn_hdr_id")
                ->where("cartons.item_id", $cartonPutawayParams['item']->item_id)
                ->where("cartons.whs_id", $cartonPutawayParams["whs_id"])
                ->where("cartons.cus_id", $cartonPutawayParams["cus_id"])
                ->whereNull("cartons.loc_id")
                ->where("gr_dtl.gr_dtl_sts","RE")
                ->where("asn_hdr.asn_type","2ST")
                ->whereNull("cartons.plt_id")
                ->whereRaw("cartons.piece_remain = cartons.ctn_pack_size");

//        if(!empty($cartonPutawayParams['tote'])) {
//            $query->where("cartons.plt_id", $cartonPutawayParams['tote']->plt_id);
//        } else {
//            $query->whereNull("cartons.plt_id");
//        }
        return $query->select("cartons.*")->first();
    }


    // Pick Cartons For Kit

    public function pickCtnByItemKit($cartonPickParams, $qty) {
        $item = $this->itemModel->getFirstWhere(["item_id" => $cartonPickParams['item_id']]);
        $full_ctn = 0;
        if($qty >= $item->pack) {
            $full_ctn = $qty / $item->pack;
        }
        if($full_ctn != 0) {
            $this->pickFullCtn($cartonPickParams, $full_ctn);
        }
        $partial_ctn = $qty % $item->pack;
        if($partial_ctn > 0) {
            $this->pickPartialCtn($cartonPickParams, $partial_ctn);
        }

        return 0;

    }

    protected function pickFullCtn($cartonPickParams, $carton_limit) {
        $query = $this->getModel()
            ->where("cartons.item_id", $cartonPickParams['item_id'])
            ->where("cartons.whs_id", $cartonPickParams["whs_id"])
            ->where("cartons.cus_id", $cartonPickParams["cus_id"])
            ->where("cartons.sts", "PD")
            ->where("cartons.odr_dtl_id", $cartonPickParams["odr_dtl_id"])
            ->whereRaw("cartons.piece_remain = cartons.ctn_pack_size")
            ->whereNotNull("expired_dt");

        $cartons = $query->orderBy("expired_dt")->limit($carton_limit)->pluck("cartons.ctn_id");
        $this->getModel()->whereIn("ctn_id",$cartons)->update([
            "piece_remain" => 0,
            "deleted" => 1
        ]);
//        $count = count($cartons);
//        if( $count < $carton_limit) {
//            $carton_limit -= $count;
//            $query = $this->getModel()
//                ->where("cartons.item_id", $cartonPickParams['item']->item_id)
//                ->where("cartons.whs_id", $cartonPickParams["whs_id"])
//                ->where("cartons.cus_id", $cartonPickParams["cus_id"])
//                ->where("cartons.sts", "AC")
//                ->whereNotNull("loc_id")
//                ->whereRaw("cartons.piece_remain = cartons.ctn_pack_size")
//                ->whereNull("expired_dt");
//
//            $cartons = $query->limit($carton_limit)->pluck("cartons.ctn_id");
//            $this->getModel()->whereIn("ctn_id",$cartons)->update([
//                "piece_remain" => 0,
//                "deleted" => 1
//            ]);
//        }
        return 0;
    }

    protected function pickPartialCtn($cartonPickParams, $qty) {

        $cartons =  $this->getCtnPickRemain($cartonPickParams);
        $pick_qty = $qty;
        foreach ($cartons as $carton) {
            if($carton->piece_remain > $pick_qty) {
                $carton->piece_remain -= $pick_qty;
                $carton->save();
                $pick_qty = 0;
                break;
            }

            if($carton->piece_remain = $pick_qty) {
                $carton->piece_remain -= $pick_qty;
                $carton->deleted = 1;
                $carton->save();
                $pick_qty = 0;
                break;
            }
            $pick_qty -= $carton->piece_remain;
            $carton->piece_remain = 0;
            $carton->deleted = 1;
            $carton->save();

        }

        if($pick_qty > 0) {
            $full_ctn = $this->getFirstPickCtnFull($cartonPickParams);
            $full_ctn->piece_remain -= $pick_qty;
            $full_ctn->save();

            $pick_qty = 0;
        }
        return 0;
    }

    protected function getCtnPickRemain($cartonPickParams) {
        $query = $this->getModel()
            ->where("cartons.item_id", $cartonPickParams['item']->item_id)
            ->where("cartons.whs_id", $cartonPickParams["whs_id"])
            ->where("cartons.cus_id", $cartonPickParams["cus_id"])
            ->where("cartons.sts", "PD")
            ->where("cartons.odr_dtl_id", $cartonPickParams["odr_dtl_id"])
            ->whereRaw("cartons.piece_remain < cartons.ctn_pack_size");

        $cartons = $query->orderBy("cartons.piece_remain")
            ->select("cartons.*")->get();

        return $cartons;
    }

    protected function getFirstPickCtnFull($cartonPickParams) {
        $query = $this->getModel()
            ->where("cartons.item_id", $cartonPickParams['item']->item_id)
            ->where("cartons.whs_id", $cartonPickParams["whs_id"])
            ->where("cartons.cus_id", $cartonPickParams["cus_id"])
            ->where("cartons.odr_dtl_id", $cartonPickParams["odr_dtl_id"])
            ->where("cartons.sts", "PD")
            ->whereRaw("cartons.piece_remain = cartons.ctn_pack_size");

        return $query->select("cartons.*")->first();
    }

    public function checkExistedCartons($ctn_ids)
    {
        $resultCount = $this->model->whereIn('ctn_id', $ctn_ids)->count();

        return $resultCount == count($ctn_ids) ? true : false;
    }

    public function updateWhereIn(array $update, array $valueWhere, $columns)
    {
        return $this->model->whereIn($columns, $valueWhere)->update($update);
    }

    public function getCartonPickedWithPieces($whsId, $cusId, $itemId, $pieces)
    {
        return $this->model->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->where('item_id', $itemId)
            ->where('ctn_sts', 'PD')
            ->select(['ctn_id', 'plt_id', 'ctn_sts', 'loc_id', 'loc_code', 'loc_name'])
            ->orderBy('ctn_id', 'DESC')
            ->limit($pieces)
            ->get();
    }
}
