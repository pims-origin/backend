<?php
namespace App\Api\V1\Models;

use App\Utils\JWTUtil;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class OrderDtlModel extends AbstractModel
{

    protected $inventoryModel;
    /**
     * OrderDtlModel constructor.
     *
     * @param OrderDtl|null $model
     */
    public function __construct(OrderDtl $model = null, InventoriesModel $inventoriesModel)
    {
        $this->model = ($model) ?: new OrderDtl();
        $this->inventoryModel = $inventoriesModel;
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getListOrderDtlOfWavePick($wv_id)
    {
        return $this->model
            ->leftJoin('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->leftJoin('wv_dtl', 'wv_dtl.wv_id', '=', 'odr_hdr.wv_id')
            ->where('odr_dtl.wv_id', $wv_id)
            ->get();
    }

    public function pickedAll($odrId, $wvHdrId)
    {
        $query = $this->model
            ->where('wv_id', $wvHdrId)
            ->where('odr_id', $odrId)
            ->where('itm_sts', "!=", "PD")
            ->first();

        if (!empty($query)) {
            return false;
        }

        return true;
    }

    public function updatePDOdrDtl($wvHdrId)
    {
        $resutl = DB::table('odr_dtl as o')
            ->where('o.wv_id', $wvHdrId)
            ->where('o.deleted', 0)
            ->where('o.alloc_qty',
                DB::raw('(
                    SELECT SUM(oc.piece_qty)
                    FROM odr_cartons oc
                    WHERE oc.odr_dtl_id = o.odr_dtl_id
                        AND oc.deleted = 0
                )')
            )
            ->update(['o.itm_sts' => 'PD']);

        return $resutl;
    }

    public function updateOdrDtlPickedQty($item, $cartons, $tote = null)
    {
        $data = new Data();
        $userInfo = $data->getUserInfo();

        $odrDtls = $this->getOrderDtlsByWaveDtl($item['wv_id'], $item['item_id'], $item['lot']);
        $orderCartons = [];
        $odrIds = [];

        $odrSortDtls = [];

        $packs = [];
        $eventData = [];

        foreach ($odrDtls as $idx => $odrDtl) {
            /**
             * 1. Check pickedTtl >= piece_qty
             * 1.1. set all_qty = piece_qty
             * 1.2 pickedTtl = pickedTtl - piece_qty
             * 1.3 update odr dtl, all_qty = piece_qty and status = PD, back_odr = false, back_odr_qty = 0
             * 1.4 $odrDtl['backorder'] = false;
             * 2. Check pickedTtl < piece_qty
             * 2.1 update odr dtl, all_qty = pickedTtl and status = PD, back_odr_qty = piece_qty - pickedTtl,
             * back_odr = true
             * 2.2 pickedTtl = 0
             */
            $odrId = $odrDtl->odr_id;
            $odrIds[] = $odrId;

            $orderCarton = [
                'odr_hdr_id' => $odrDtl->odr_id,
                'odr_dtl_id' => $odrDtl->odr_dtl_id,
                'wv_hdr_id'  => $item['wv_id'],
                'wv_dtl_id'  => $item['wv_dtl_id'],
                'odr_num'    => $odrDtl->odr_num,
                'wv_num'     => $item['wv_num'],
                'ctnr_rfid'  => null,
                'ctn_num'    => null,
                'piece_qty'  => 0,
                'ctn_id'     => null,
                'is_storage' => 0,
                'ctn_rfid'   => null,
                'ctn_sts'    => 'PD',
                'sts'        => 'I',
                'created_at' => time(),
                'updated_at' => time(),
                'deleted'    => 0,
                'deleted_at' => '915148800',
                'created_by' => $userInfo['user_id'],
                'updated_by' => $userInfo['user_id'],
                'whs_id'     => $odrDtl->whs_id,
                'cus_id'     => $odrDtl->cus_id,
                'inner_pack' => 0,

            ];

            if ($tote) {
                $orderCarton['tote_id'] = object_get($tote, 'tote_id');
                $orderCarton['tote_num'] = object_get($tote, 'tote_num');
            }

            $remaining = $odrDtl->alloc_qty - $odrDtl->picked_qty;

            foreach ($cartons as $key => $carton) {
                if ($remaining < $carton['picked_qty']) {
                    $carton['pick_piece'] = true;
                    $odrDtl->picked_qty += $remaining;
                } else {
                    $odrDtl->picked_qty += $carton['picked_qty'];
                }
                $origRemaining = $remaining;
                $remaining -= $carton['picked_qty'];


                if ($carton['pick_piece']) {

                    $orderCarton['is_storage'] = 1;
                } else {
                    $orderCarton['inner_pack'] = $carton['inner_pack'];
                }

                $orderCarton['piece_qty'] = $remaining < 0 ? $origRemaining : $carton['picked_qty'];
                $orderCarton['ctn_id'] = $carton['ctn_id'];
                $orderCarton['ctn_num'] = $carton['ctn_num'];
                $orderCarton['ctn_rfid'] = $carton['rfid'];
                $orderCarton['item_id'] = $carton['item_id'];
                $orderCarton['sku'] = $carton['sku'];
                $orderCarton['size'] = $carton['size'];
                $orderCarton['color'] = $carton['color'];
                $orderCarton['lot'] = $carton['lot'];
                $orderCarton['upc'] = $carton['upc'];
                $orderCarton['pack'] = $carton['ctn_pack_size'];
                $orderCarton['uom_id'] = $carton['ctn_uom_id'];
                $orderCarton['uom_code'] = $carton['uom_code'];
                $orderCarton['uom_name'] = $carton['uom_name'];
                $orderCarton['cat_code'] = $carton['cat_code'];
                $orderCarton['cat_name'] = $carton['cat_name'];
                $orderCarton['spc_hdl_code'] = $carton['spc_hdl_code'];
                $orderCarton['spc_hdl_name'] = $carton['spc_hdl_name'];
                $orderCarton['loc_id'] = $carton['loc_id'];
                $orderCarton['loc_code'] = $carton['loc_code'];
                $orderCarton['length'] = $carton['length'];
                $orderCarton['width'] = $carton['width'];
                $orderCarton['height'] = $carton['height'];
                $orderCarton['weight'] = $carton['weight'];
                $orderCarton['volume'] = $carton['volume'];
                $orderCarton['plt_id'] = $carton['plt_id'];

                $pltData = DB::table('pallet')
                            ->where('plt_id', $carton['plt_id'])
                            ->first();

                if ($pltData) {
                    $orderCarton['plt_rfid'] = substr($pltData['plt_num'], 0, -6);
                }

                $orderCartons[] = $orderCarton;
                $packs[$odrDtl->odr_id][$carton['ctn_id']] = [
                    'orderCarton' => $orderCarton,
                    'carton'      => $carton,
                ];

                $eventData[$odrId]['whs_id'] = $carton['whs_id'];
                $eventData[$odrId]['cus_id'] = $carton['cus_id'];


                if ($remaining == 0) {
                    unset($cartons[$key]);
                    break;
                } elseif ($remaining < 0) {
                    $cartons[$key]['picked_qty'] = abs($remaining);
                    break;
                } else {
                    unset($cartons[$key]);
                }
            }

            if ($odrDtl->alloc_qty == $odrDtl->picked_qty) {
                $odrDtl->itm_sts = 'PD';
            } else {
                $odrDtl->itm_sts = 'PK';
            }

            $odrDtl->save();

            $odrSortDtls[$odrDtl->odr_dtl_id] = $odrDtl;
        }

        $insertBatchs = array_chunk($orderCartons, 200);

        foreach ($insertBatchs as $batch) {
            DB::table('odr_cartons')->insert($batch);
        }

        return [
            'odrDtls'   => $odrSortDtls,
            'packs'     => $packs,
            'odrIds'    => $odrIds,
            'eventData' => $eventData,
        ];
    }

    public function getOrderDtlsByWaveDtl($wv_id, $itemId, $lot)
    {
        $this->model->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);

        return $this->model
            ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->where('odr_dtl.wv_id', $wv_id)
            ->where('item_id', $itemId)
            ->whereIn('lot', [$lot, 'ANY'])
            ->select(['odr_hdr.odr_num', 'odr_dtl.*'])
            ->whereIn('itm_sts', ['PK', 'NW'])
            ->get();
    }

    public function allocateKit($odrDtl) {
        if($odrDtl->itm_sts != "NW") {
            return 0;
        }
        $qty = $odrDtl->qty;

        if($qty > 0) {
            $child_items = DB::table("item_child")->where("parent", $odrDtl->item_id)->get();
            $child_qty_need = [];

            foreach ($child_items as $child_item) {
                $inventory_child = $this->inventoryModel->getFirstWhere([
                    "item_id"   => $child_item['child'],
                    "type"      => "I"
                ]);
                if(empty($inventory_child)) {
                    return 0;
                }
                $need_qty = $qty * $child_item['pack'];

                if($need_qty > $inventory_child->in_hand_qty) {
                    return 0;
                }
                $child_qty_need[$child_item['child']] = [
                    "in_hand_qty" => $inventory_child->in_hand_qty - $need_qty,
                    "in_pick_qty" => $inventory_child->in_pick_qty + $need_qty,
                ];
            }

            foreach ($child_qty_need as $key => $value) {
                $this->inventoryModel->getModel()->where("item_id", $key)->update($value);
            }
            $odrDtl->itm_sts = "AL";
            $odrDtl->save();
        }
        return 0;
    }

    public function getOrderDetail($attributes)
    {
        $whsId = array_get($attributes, 'whs_id');
        $cusId = array_get($attributes, 'cus_id');
        $odrId = array_get($attributes, 'odr_id');
        $limit = array_get($attributes, 'limit') ? array_get($attributes, 'limit') : 20;
        return $this->model->join('odr_hdr', 'odr_hdr.odr_id', 'odr_dtl.odr_id')->where([
            'odr_dtl.whs_id' => $whsId,
            'odr_dtl.cus_id' => $cusId,
            'odr_dtl.odr_id' => $odrId
        ])->where('odr_dtl.picked_qty', '>', 0)->select([
            'odr_dtl.odr_id',
            'odr_dtl.odr_dtl_id',
            'odr_dtl.wv_id',
            'odr_dtl.item_id',
            'odr_dtl.sku',
            'odr_dtl.qty',
            'odr_dtl.piece_qty',
            'odr_dtl.alloc_qty',
            'odr_dtl.picked_qty'
        ])->paginate($limit);
    }
}
