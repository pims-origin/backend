<?php
namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderDtl;

class OrderDtlModel extends AbstractModel
{

    /**
     * OrderDtlModel constructor.
     *
     * @param OrderDtl|null $model
     */
    public function __construct(OrderDtl $model = null)
    {
        $this->model = ($model) ?: new OrderDtl();
    }

    /**
     * @param $wv_id
     *
     * @return mixed
     */
    public function getListOrderDtlOfWavePick($wv_id)
    {
        return $this->model
            ->leftJoin('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->leftJoin('wv_dtl', 'wv_dtl.wv_id', '=', 'odr_hdr.wv_id')
            ->where('odr_dtl.wv_id', $wv_id)
            ->get();
    }

    public function pickedAll($odrId, $wvHdrId)
    {
        $query = $this->model
            ->where('wv_id', $wvHdrId)
            ->where('odr_id', $odrId)
            ->where('itm_sts', "!=", "PD")
            ->first();

        if (!empty($query)) {
            return false;
        }

        return true;
    }

    public function updatePDOdrDtl($wvHdrId)
    {
        $resutl = DB::table('odr_dtl as o')
            ->where('o.wv_id', $wvHdrId)
            ->where('o.alloc_qty',
                DB::raw('(SELECT SUM(oc.piece_qty) FROM odr_cartons oc WHERE oc.odr_dtl_id = o.odr_dtl_id)')
            )
            ->update(['o.itm_sts' => 'PD']);

        return $resutl;
    }

}
