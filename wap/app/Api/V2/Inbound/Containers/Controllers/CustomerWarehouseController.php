<?php

namespace App\Api\V2\Inbound\Containers\Controllers;

use App\Api\V2\Inbound\Models\CustomerWarehouseModel;
use App\Api\V2\Inbound\Containers\Transformers\CustomerWarehouseDefaultTransformer;
use App\Api\V2\Inbound\Containers\Transformers\CustomerWarehouseTransformer;
use App\WarehouseQualifier;
use Dingo\Api\Http\Response as Response;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use App\Api\V2\Inbound\Models\Log;
use App\MyHelper;
use Psr\Http\Message\ServerRequestInterface as Request;

class CustomerWarehouseController extends AbstractController
{
    /**
     * @var $customerWarehouseModel
     */
    protected $customerWarehouseModel;
    /**
     * @var $customerWarehouseDefaultTransformer
     */
    protected $customerWarehouseDefaultTransformer;

    public function __construct()
    {
        $this->customerWarehouseModel              = new CustomerWarehouseModel();
        $this->customerWarehouseDefaultTransformer = new CustomerWarehouseDefaultTransformer();
    }

    /**
     * @param $whsId
     * @return \Dingo\Api\Http\Response|void
     */
    public function loadBy(
        $whsId, Request $request
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id']= $whsId;

        /*
         * start logs
         */

        $url = "/whs/$whsId/cus-list";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'LBY',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Load list of users'
        ]);
        /*
         * end logs
         */

        try {
            $meta = $this->customerWarehouseModel->searchCustomerByWarehouse(
                $input, array_get($input, 'limit'));

            Log::respond($request, $whsId, [
                'evt_code' => 'LBY',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'response_data' => $this->response->collection($meta, $this->customerWarehouseDefaultTransformer)
            ]);
            return $this->response()->collection($meta, $this->customerWarehouseDefaultTransformer);
            //return new Response()->paginator($meta, $this->customerWarehouseDefaultTransformer);

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return new Response($this->getResponseData(), 200, [], null);
        }
    }
}
