<?php

namespace Seldat\Wms2\Models;

class Commodity extends BaseSoftModel
{
    /**
     * @var string
     */
    protected $table = 'commodity';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cmd_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cmd_name',
        'cmd_des',
        'commodity_nmfc',
        'cmd_cls',
    ];
}
