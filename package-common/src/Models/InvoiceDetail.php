<?php

namespace Seldat\Wms2\Models;

class InvoiceDetail extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice_dtls';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'inv_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'inv_num',
        'chg_code_id',
        'chg_cd_desc',
        'chg_cd_qty',
        'chg_uom_id',
        'chg_cd_price',
        'chg_cd_cur',
        'chg_cd_amt',
        'create_by',
        'update_by'
    ];

    public function invoiceHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceHeader', 'inv_num', 'inv_num');
    }

    public function chargeCode()
    {
        return $this->belongsTo(__NAMESPACE__ . '\ChargeCode', 'chg_code_id', 'chg_code_id');
    }

    public function systemUom()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemUom', 'chg_uom_id', 'sys_uom_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }
}


