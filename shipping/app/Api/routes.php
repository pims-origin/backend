<?php
$app->get('/', function () use ($app) {
    return $app->version();
});
// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
    $middleware[] = 'setWarehouseTimezone';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () {
            return ['Seldat API V1'];
        });
        
        // Thirt Party
        $api->get('/cus/{cusId:[0-9]+}/third-party', [
            'action' => 'viewThirdParty',
            'uses'   => 'ThirdPartyController@search'
        ]);
        $api->post('/cus/{cusId:[0-9]+}/third-party', [
            'action' => 'createThirdParty',
            'uses'   => 'ThirdPartyController@store'
        ]);
        $api->put('/cus/{cusId:[0-9]+}/third-party/{tpId:[0-9]+}', [
            'action' => 'editThirdParty',
            'uses'   => 'ThirdPartyController@update'
        ]);
        $api->get('/cus/{cusId:[0-9]+}/third-party/{tpId:[0-9]+}', [
            'action' => 'viewThirdParty',
            'uses'   => 'ThirdPartyController@show'
        ]);
    });
});
