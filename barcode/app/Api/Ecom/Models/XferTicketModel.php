<?php

namespace App\Api\Ecom\Models;

use Seldat\Wms2\Models\XferTicket;

class XferTicketModel extends AbstractModel
{
    public function __construct(XferTicket $model = null)
    {
        $this->model = ($model) ?: new XferTicket();
    }
}
