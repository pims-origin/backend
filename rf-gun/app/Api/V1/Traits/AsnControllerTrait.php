<?php

namespace App\Api\V1\Traits;

use Psr\Http\Message\ServerRequestInterface as Request;

trait AsnControllerTrait
{
    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function dashboard(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        
        try {
            $dashboard = $this->asnHdrModel->dashboard($input);

            return ['data' => $dashboard[0]];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}