<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\DeliveryService;


class DeliveryServiceTransformer extends TransformerAbstract
{
    /**
     * @param DeliveryNote $deliveryNote
     *
     * @return array
     */
    public function transform(DeliveryService $deliveryService)
    {
        return [
            'key' => $deliveryService->ds_key,
            'value' => $deliveryService->ds_value
        ];
    }
}
