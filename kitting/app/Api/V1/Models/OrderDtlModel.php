<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use App\InventorySummary;
use App\OrderDtl;
use App\Wms2\UserInfo\Data;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderDtlModel
 *
 * @package App\Api\V1\Models
 */
class OrderDtlModel extends AbstractModel
{
    const LOT_ANY = 'ANY';
    public function __construct(OrderDtl $model = null)
    {
        $this->model = ($model) ?: new OrderDtl();
    }
    /**
     * @param $odrDtlIds
     *
     * @return mixed
     */
    public function deleteDtls($odrDtlIds)
    {
        return $this->model
            ->whereIn('odr_dtl_id', $odrDtlIds)
            ->forceDelete();
    }

    public function updateOrdDtl($odrHdr, $odrDtl)
    {
        $where = [
            'whs_id' => $odrHdr->whs_id,
            'cus_id' => $odrHdr->cus_id,
            'item_id' => $odrDtl->item_id,
        ];
        //select inv_sum by item_id
        $invItems = $this->getInvSumByOdrDtl($where, $odrDtl['lot'], 'FIFO', $odrDtl->pack);

        $allQty = $reqQty = $odrDtl['piece_qty'];
        $availQty = 0;
        $isLast = count($invItems) === 1;
        $lotIndex = 0;
        foreach ($invItems as $invItem) {
            $lotIndex++;
            if ($lotIndex == count($invItems)) {
                $isLast = true;
            }
            if ($invItem->avail < $allQty) {
                $availQty += $invItem->avail;
                $this->createNewOdrDtl($invItem, $odrDtl, $invItem->avail, $isLast);
                $allQty = $this->updateInvSum($invItem, $allQty);

            } else {
                $this->updateInvSum($invItem, $allQty);
                $this->createNewOdrDtl($invItem, $odrDtl, $allQty, $isLast);
                break;
            }
        }

        if ($this->isLotAny($odrDtl->lot)) {
            $odrDtl->delete();
        }
    }

    public function getInvSumByOdrDtl($where, $lot, $algorithm, $packSize)
    {


        if (!$this->isLotAny($lot)) {
            $where['lot'] = $lot;
        }
        $query = DB::table('cartons')->where($where)->where('piece_remain', '>', 0)->where('ctn_sts', 'AC')->where('ctn_pack_size', $packSize);
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        };


        $items = $query->groupBy('item_id', 'lot')->select('lot')->get();
        $lots= array_pluck($items, 'lot');

        $invItems = InventorySummary::where($where)
            ->where('avail', '>', 0)
            ->get();

        $sortInvSums = [];
        foreach ($lots as $lot){
            foreach ($invItems as $key=>$value) {
                if($lot == $value->lot){
                    $sortInvSums[] = $value;
                    unset($invItems[$key]);
                    break;
                }
            }
        }

        return $sortInvSums;
    }

    public function isLotAny($lot)
    {
        $lot = strtoupper($lot);
        if ($lot === 'ANY') {
            return true;
        }

        return false;
    }

    private function createNewOdrDtl($invSum, $odrDtl, $allQty, $isOne = false)
    {
        $newOdrDlt = $odrDtl;
        $lot = $odrDtl->lot;
        if ($this->isLotAny($lot) && $isOne == false) {
            $newOdrDlt = $odrDtl->replicate();
        }

        $newOdrDlt->lot = $invSum->lot;

        if ($this->isLotAny($lot) && $isOne == false) {
            return $newOdrDlt->push();
        } else {
            //$odrDtl->piece_qty = $piece_qty;
            return $newOdrDlt->save();
        }
    }


    private function updateInvSum($invSum, $allQty)
    {
        if ($invSum->avail < $allQty) {
            $allQty = $allQty - $invSum->avail;
            $invSum->allocated_qty = $invSum->allocated_qty + $invSum->avail;
            $invSum->avail = 0;

        } else {
            $invSum->allocated_qty = $invSum->allocated_qty + $allQty;
            $invSum->avail = $invSum->avail - $allQty;
            $allQty = 0;
        }
        $invSum->save();

        return $allQty;
    }

    /**
     * @param $odr_ids
     *
     * @return mixed
     */
    public function getListOrderDtlInfo($odr_ids)
    {
        return $this->model
            ->whereIn('odr_id', $odr_ids)
            ->where('alloc_qty', '>', 0)
            ->get();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function updateOrtDtl($data)
    {
        return $this->model->where('odr_id', '=', $data['odr_id'])
            ->update([
                'wv_id' => $data['wv_id'],
                'itm_sts' => 'PK'
            ]);
    }

    public function getKittingItems($orderHdrId) {
        return $this->model->where('odr_id', $orderHdrId)
            ->where('is_kitting', 1)
            ->get();
    }

    public function updateOdrDtlPickedQty($item, $cartons)
    {
        $data = new Data();
        $userInfo = $data->getUserInfo();

        $odrDtls = $this->getOrderDtlsByWaveDtl($item['wv_id'], $item['item_id'], $item['lot']);
        $pickedTtl = $item['picked_ttl'];
        $orderCartons = [];
        $odrIds = [];
        $discrepancyTtl = 0;

        $odrSortDtls = [];

        $packs = [];
        $eventData = [];

        foreach ($odrDtls as $odrDtl) {
            /**
             * 1. Check pickedTtl >= piece_qty
             * 1.1. set all_qty = piece_qty
             * 1.2 pickedTtl = pickedTtl - piece_qty
             * 1.3 update odr dtl, all_qty = piece_qty and status = PD, back_odr = false, back_odr_qty = 0
             * 1.4 $odrDtl['backorder'] = false;
             * 2. Check pickedTtl < piece_qty
             * 2.1 update odr dtl, all_qty = pickedTtl and status = PD, back_odr_qty = piece_qty - pickedTtl,
             * back_odr = true
             * 2.2 pickedTtl = 0
             */
            $odrId = $odrDtl->odr_id;
            $odrIds[] = $odrId;
            $allQty  = 0;

            if (!array_key_exists($odrId, $eventData)) {
                $eventData[$odrId] = [
                    'whs_id' => $odrDtl->whs_id,
                    'cus_id' => $odrDtl->cus_id,
                    'evt_code' => Status::getByKey("EVENT", "ORDER-PICKED"),
                    'info' => sprintf(Status::getByKey("EVENT-INFO", "ORDER-PICKED"), $odrDtl->odr_num),
                    'owner' => $odrDtl->odr_num,
                    'trans_num' => $odrDtl->odr_num,
                    'created_at' => time(),
                    'created_by' => JWTUtil::getPayloadValue('jti') ?: 1
                ];
            }


            $orderCarton = [
                'odr_hdr_id' => $odrDtl->odr_id,
                'odr_dtl_id' => $odrDtl->odr_dtl_id,
                'wv_hdr_id'  => $item['wv_id'],
                'wv_dtl_id'  => $item['wv_dtl_id'],
                'odr_num'    => $odrDtl->odr_num,
                'wv_num'     => $item['wv_num'],
                'ctnr_rfid'  => null,
                'ctn_num'    => null,
                'piece_qty'  => 0,
                'ctn_id'     => null,
                'is_storage' => 0,
                'ctn_rfid'   => null,
                'ctn_sts'    => 'PD',
                'sts'        => 'I',
                'created_at' => time(),
                'updated_at' => time(),
                'deleted'    => 0,
                'deleted_at' => '915148800',
                'created_by' => $userInfo['user_id'],
                'updated_by' => $userInfo['user_id'],
            ];

            $remaining = $odrDtl->alloc_qty;

            foreach ($cartons as $key => $carton) {
                $remaining -= $carton['picked_qty'];
                $allQty += $carton['picked_qty'];
                if ($carton['pick_piece']) {
                    $orderCarton['is_storage'] = 1;
                }

                $orderCarton['piece_qty'] = $carton['picked_qty'];
                $orderCarton['ctn_id'] = $carton['ctn_id'];
                $orderCarton['ctn_num'] = $carton['ctn_num'];
                $orderCarton['ctn_rfid'] = $carton['rfid'];
                $orderCarton['item_id'] = $carton['item_id'];
                $orderCarton['sku'] = $carton['sku'];
                $orderCarton['size'] = $carton['size'];
                $orderCarton['color'] = $carton['color'];
                $orderCarton['lot'] = $carton['lot'];
                $orderCarton['upc'] = $carton['upc'];
                $orderCarton['pack'] = $carton['ctn_pack_size'];
                $orderCarton['uom_id'] = $carton['ctn_uom_id'];
                $orderCarton['uom_code'] = $carton['uom_code'];
                $orderCarton['uom_name'] = $carton['uom_name'];
                $orderCarton['whs_id'] = $carton['whs_id'];
                $orderCarton['cus_id'] = $carton['cus_id'];
                $orderCartons[] = $orderCarton;
                $packs[$odrDtl->odr_id][$carton['ctn_id']] = [
                    'orderCarton' => $orderCarton,
                    'carton'      => $carton,
                ];

                $eventData[$odrId]['whs_id'] = $carton['whs_id'];
                $eventData[$odrId]['cus_id'] = $carton['cus_id'];

                unset($cartons[$key]);
                if ($remaining <= 0) {
                    break;
                }
            }

            if ($pickedTtl >= $odrDtl->alloc_qty) {
                $odrDtl->back_odr = false;
                $odrDtl->back_odr_qty = 0;
                $pickedTtl -= $odrDtl->piece_qty;
            } else {
                $discrepancyQty = $odrDtl->alloc_qty - $pickedTtl;
                $discrepancyTtl += $discrepancyQty;
                $odrDtl->alloc_qty = $allQty;
                $odrDtl->back_odr = true;
                //$odrDtl->back_odr_qty = abs($pickedTtl);
                $odrDtl->back_odr_qty  =  $odrDtl->piece_qty - $allQty;
                $pickedTtl = 0;
            }

            $odrDtl->picked_qty = $odrDtl->alloc_qty;
            $odrDtl->itm_sts = 'PD';
            $odrDtl->save();

            $odrSortDtls[$odrDtl->odr_dtl_id] = $odrDtl;


        }

        $orderCartonsData = serialize($orderCartons);
        DB::table('wv_queue')->insert([
            'wv_id' => $item['wv_id'],
            'type' => 'UP',
            'data' => $orderCartonsData,
            'queue_sts' => 'NW'
        ]);

        return [
            'odrDtls'        => $odrSortDtls,
            'packs'          => $packs,
            'odrIds'         => $odrIds,
            'discrepancyTtl' => $discrepancyTtl,
            'eventData'      => $eventData,
        ];
    }

    public function getOrderDtlsByWaveDtl($wv_id, $itemId, $lot)
    {
        return $this->model
            ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->where('odr_dtl.wv_id', $wv_id)
            ->where('item_id', $itemId)
            ->whereIn('lot', [$lot, self::LOT_ANY])
            ->where('itm_sts', '<>', 'CC')
            ->select(['odr_hdr.odr_num', 'odr_dtl.*'])
            ->get();
    }

    public function sortOrderDtlByOrderID($odrDtlList)
    {
        $orders = [];
        foreach ($odrDtlList as $item) {
            foreach ($item as $id => $odrDtl) {
                $orders[$odrDtl->odr_id][$id] = $odrDtl;
            }
        }

        return $orders;
    }
}
