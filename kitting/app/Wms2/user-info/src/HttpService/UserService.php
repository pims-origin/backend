<?php

namespace App\Wms2\UserInfo\HttpService;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class UserService
{
    protected $_client;
    public function __construct($auth)
    {
        $api = env('API_AUTHENTICATION');
        $this->_client = new Client([
            'verify'   => false,
            'base_uri' => $api,
            'headers' => [
                'Authorization' => $auth
            ],
        ]);
    }
    
    public function getUserCache()
    {
        $uri = "users/user-cache";
    
        try {
            $response = $this->_client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            throw new \Exception('Connection error');
        }
    }
}