<?php

namespace App\Modules\Orders\Eloquents;

class OdrDtl extends BaseSoftModel
{
    protected $table = 'odr_dtl';

    protected $primaryKey = 'odr_dtl_id';

    protected $fillable = [
        'wv_id',
        'odr_id',
        'whs_id',
        'cus_id',
        'item_id',
        'uom_id',
        'color',
        'sku',
        'size',
        'lot',
        'pack',
        'des',
        'qty',
        'piece_qty',
        'itm_sts',
        'sts',
        'back_odr',
        'alloc_qty',
        'special_hdl',
        'back_odr_qty',
        'cus_upc',
        'ship_track_id',
        'packed_qty',
        'picked_qty',
        'allow_dmg',
        'dmg_qty',
        'uom_code',
        'prod_line',
        'cmp'
    ];

    public function odrHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\OdrHdr', 'odr_id', 'odr_id');
    }

    public function odrDtlMeta()
    {
        return $this->hasOne(__NAMESPACE__ . '\OdrDtlMeta', 'odr_dtl_id');
    }

    public function sysUom()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemUom', 'sys_uom_id', 'uom_id');
    }
}
