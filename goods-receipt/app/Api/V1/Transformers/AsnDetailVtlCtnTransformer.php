<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Utils\Status;

class AsnDetailVtlCtnTransformer extends TransformerAbstract
{

    /**
     * @param AsnDtl $asnDtl
     *
     * @return array
     */
    public function transform(AsnDtl $asnDtl)
    {
        return [
            //asn_dtl
            'asn_hdr_id'      => object_get($asnDtl, 'asn_hdr_id', ''),
            'asn_dtl_id'      => object_get($asnDtl, 'asn_dtl_id', ''),
            'item_id'         => object_get($asnDtl, 'item_id', ''),
            'asn_dtl_sku'     => object_get($asnDtl, 'asn_dtl_sku', ''),
            'asn_dtl_size'    => object_get($asnDtl, 'asn_dtl_size', ''),
            'asn_dtl_color'   => object_get($asnDtl, 'asn_dtl_color', ''),
            'asn_dtl_lot'     => object_get($asnDtl, 'asn_dtl_lot', ''),
            'asn_dtl_pack'    => object_get($asnDtl, 'asn_dtl_pack', ''),
            'asn_dtl_cus_upc' => object_get($asnDtl, 'asn_dtl_cus_upc', ''),
            //vtlCtn
            'plt_rfid'        => object_get($asnDtl, 'virtualCarton.plt_rfid', ''),
            'vtl_ctn_id'      => object_get($asnDtl, 'virtualCarton.vtl_ctn_id', ''),
            'ctn_rfid'        => object_get($asnDtl, 'virtualCarton.ctn_rfid', ''),
            'created_at'      => date("m/d/Y h:i:s", strtotime(object_get($asnDtl, 'virtualCarton.created_at', ''))),
            'updated_at'      => date("m/d/Y", strtotime(object_get($asnDtl, 'virtualCarton.updated_at', ''))),
            'updated_by'      => trim(object_get($asnDtl, "virtualCarton.updatedUser.first_name", null) . " " .
                object_get($asnDtl, "virtualCarton.updatedUser.last_name", null)),
            'created_by'      => trim(object_get($asnDtl, "virtualCarton.createdUser.first_name", null) . " " .
                object_get($asnDtl, "virtualCarton.createdUser.last_name", null)),
            'vtl_ctn_sts'     => Status::getByKey("VIRTUAL-CARTON-STATUS",
                object_get($asnDtl, 'virtualCarton.vtl_ctn_sts', '')),

        ];
    }
}
