<?php

namespace App\Modules\Items\Eloquents;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'cus_id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_name',
        'cus_status',
        'cus_code',
        'cus_country_id',
        'cus_state_id',
        'cus_city_name',
        'cus_postal_code',
        'cus_billing_account',
        'cus_des'
    ];

    public function warehouse()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\WareHouse', 'customer_warehouse', 'cus_id', 'whs_id');
    }

    public function customerAddress()
    {
        return $this->hasMany(__NAMESPACE__ . '\CustomerAddress', 'cus_add_cus_id', 'cus_id');
    }

    public function customerDefaultContact()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerContact', 'cus_ctt_cus_id', 'cus_id');
    }

    public function customerContact()
    {
        return $this->hasMany(__NAMESPACE__ . '\CustomerContact', 'cus_ctt_cus_id', 'cus_id');
    }

    public function systemCountry()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemCountry', 'cus_country_id', 'sys_country_id');
    }

    public function systemState()
    {
        return $this->belongsTo(__NAMESPACE__ . '\SystemState', 'cus_state_id', 'sys_state_id');
    }

    public function customerWarehouse()
    {
        return $this->belongsTo(__NAMESPACE__ . '\CustomerWarehouse', 'cus_id', 'cus_id');
    }

    public function users()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\User', 'cus_ex_user', 'cus_id', 'user_id');
    }

}
