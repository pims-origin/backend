<?php

namespace App\Modules\Orders\Eloquents;

class InventorySummary extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invt_smr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'inv_sum_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'item_id',
        'cus_id',
        'whs_id',
        'color',
        'size',
        'lot',
        'lot',
        'ttl',
        'allocated_qty',
        'picked_qty',
        'dmg_qty',
        'sku',
        'avail',
        'upc',
        'crs_doc_qty',
        'ecom_qty',
        'back_qty',
        'lock_qty'

    ];
}