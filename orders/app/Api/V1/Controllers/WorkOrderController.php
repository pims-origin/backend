<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ChargeCodeModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InvoiceCostModel;
use App\Api\V1\Models\InvoiceCostModelModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\WorkOrderModel;
use App\Api\V1\Models\WorkOrderDtlModel;
use App\Api\V1\Models\WorkOrderSkuModel;
use App\Api\V1\Models\CustomerChargeDetailModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Transformers\ChargeCodeTransformer;
use App\Api\V1\Transformers\WorkOrderDtlTransformer;
use App\Api\V1\Transformers\WorkOrderViewTransformer;
use App\Api\V1\Validators\WorkOrderHdrValidator;
use App\Api\V1\Validators\WorkOrderSkuValidator;
use Illuminate\Support\Facades\DB;
use mPDF;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WorkOrderDtl;
use Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Seldat\Wms2\Utils\Helper;
use Swagger\Annotations as SWG;
use Psr\Http\Message\ServerRequestInterface as Request;

class WorkOrderController extends AbstractController
{
    public function __construct(
        OrderHdrModel $orderHdrModel,
        CustomerChargeDetailModel $customerChargeDetailModel,
        CustomerModel $customerModel,
        ChargeCodeModel $chargeCodeModel,
        WorkOrderModel $workOrderModel,
        WorkOrderDtlModel $workOrderDtlModel,
        UserModel $userModel,
        WorkOrderSkuValidator $workOrderSkuValidator,
        ItemModel $itemModel,
        WorkOrderSkuModel $workOrderSkuModel,
        OrderDtlModel $orderDtlModel
    ) {

        $this->orderHdrModel = $orderHdrModel;
        $this->customerChargeDetailModel = $customerChargeDetailModel;
        $this->invoiceCostModel = new InvoiceCostModel();
        $this->customerModel = $customerModel;
        $this->chargeCodeModel = $chargeCodeModel;
        $this->workOrderModel = $workOrderModel;
        $this->workOrderDtlModel = $workOrderDtlModel;
        $this->eventTrackingModel = new EventTrackingModel(new EventTracking());
        $this->userModel = $userModel;
        $this->workOrderSkuValidator = $workOrderSkuValidator;
        $this->itemModel = $itemModel;
        $this->workOrderSkuModel = $workOrderSkuModel;
        $this->orderDtlModel = $orderDtlModel;
    }

    /**
     * @param $customerId
     * @param ChargeCodeTransformer $chargeCodeTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getChargeCode(
        $customerId,
        ChargeCodeTransformer $chargeCodeTransformer
    ) {
        try {
            //Check existing of CustomerId: The {0} is not existed!
            $orderHdr = $this->customerModel->getFirstBy('cus_id', $customerId);
            if (empty($orderHdr)) {
                throw new \Exception(Message::get("BM017", "Customer"));
            }

            //get info according to CustomerId
            $cusChaDtlInfo = $this->invoiceCostModel->findWhere(
                ['cus_id' => $customerId],
                ['customer']
            );

            // Get Charge code list
            if (!empty($cusChaDtlInfo)) {
                $arrChargeCodeIds = array_pluck(json_decode($cusChaDtlInfo, true), 'chg_code_id');
                if ($ChargeCodeInfo = $this->chargeCodeModel->getChargeCodeById($arrChargeCodeIds)
                ) {
                    return $this->response->collection($ChargeCodeInfo, $chargeCodeTransformer);
                }

            } else {
                throw new \Exception(Message::get("BM017", "Charge Code"));
            }

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param WorkOrderDtlTransformer $workOrderDtlTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function workOrderList(
        Request $request,
        WorkOrderDtlTransformer $workOrderDtlTransformer
    ) {
        $input = $request->getQueryParams();

        try {
            // get list WO
            $workOrderDtl = $this->workOrderModel->search($input,
                ['WorkOrderDtl', 'customer', 'createdBy'],
                array_get($input, 'limit'));

            return $this->response->paginator($workOrderDtl, $workOrderDtlTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show(
        $workOrderId,
        WorkOrderViewTransformer $workOrderViewTransformer
    ) {
        //$qty = $rs[$item_id];

        try {
            $workOdrHdr = $this->workOrderModel->getFirstBy('wo_hdr_id', $workOrderId, ['orderHdr']);
            if (empty($workOdrHdr)) {
                throw new \Exception(Message::get("BM017", "Work Order"));
            }
            $vail_qty = $this->orderHdrModel->getCmpValue($workOdrHdr->cus_id, $workOdrHdr->odr_hdr_num);

            $isSku = ($workOdrHdr->type) ? true : false;

            $workOrderDtl = $this->workOrderDtlModel->allBy('wo_hdr_id', $workOrderId, ["chargeCode"])->toArray();

            $items = [];

            if ($isSku) {
                // Group By Charge Code
                foreach ($workOrderDtl as $detail) {
                    $items[$detail['chg_code_id']]['chg_code_id'] = $detail['chg_code_id'];
                    $items[$detail['chg_code_id']]['skus'][] = [
                        'item_id'   => $detail['item_id'],
                        'sku'       => $detail['sku'],
                        'lot'       => $detail['lot'],
                        'size'      => $detail['size'],
                        'color'     => $detail['color'],
                        'qty'       => $vail_qty[$detail['item_id']],
                        'wo_qty'    => $detail['qty'],
                        'wo_dtl_id' => $detail['wo_dtl_id']
                    ];
                }

            } else {
                // Group By Item
                foreach ($workOrderDtl as $key => $detail) {

                    $items[$detail['item_id']]['item_id'] = $detail['item_id'];
                    $items[$detail['item_id']]['lot'] = $detail['lot'];

                    $items[$detail['item_id']]['chargecodes'][] = [
                        'chg_code_id'  => $detail['chg_code_id'],
                        'chg_code'     => $detail['chg_code'],
                        'qty'          => array_get($vail_qty, $detail['item_id']),
                        'wo_qty'       => $detail['qty'],
                        'chg_code_des' => array_get($detail, 'charge_code.chg_code_des', null),
                        'chg_uom_id'   => array_get($detail, 'charge_code.chg_uom_id', null),
                        'chg_type_id'  => array_get($detail, 'charge_code.chg_type_id', null),
                        'wo_dtl_id'    => $detail['wo_dtl_id'],
                    ];

                }
            }

            $workOdrHdr['details'] = array_values($items);

            return $this->response->item($workOdrHdr, $workOrderViewTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                $e->getMessage() . " " . $e->getLine()
            //SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $workOrderId
     */
    public function printWorkOrder(
        $workOrderId
    ) {
        try {
            //Get work order information
            $workOdrHdr = $this->workOrderModel->getFirstBy('wo_hdr_id', $workOrderId, ['orderHdr']);
            if (empty($workOdrHdr)) {
                throw new \Exception(Message::get("BM017", "Work Order"));
            }
            $vail_qty = $this->orderHdrModel->getCmpValue($workOdrHdr->cus_id, $workOdrHdr->odr_hdr_num);

            $isSku = ($workOdrHdr->type) ? true : false;

            $workOrderDtl = $this->workOrderDtlModel->allBy('wo_hdr_id', $workOrderId, ["chargeCode"])->toArray();

            $items = [];
            if ($isSku) {
                // Group By Charge Code
                foreach ($workOrderDtl as $detail) {
                    $items[$detail['chg_code_id']]['chg_code_id'] = $detail['chg_code_id'];
                    //get more charge code
                    $chargeCodeInfo = $this->chargeCodeModel->getChargeCodeAccordingToId($items[$detail['chg_code_id']]['chg_code_id']);
                    $items[$detail['chg_code_id']]['chg_code_name'] = object_get($chargeCodeInfo, 'chg_code_name', '');
                    $items[$detail['chg_code_id']]['chg_code'] = object_get($chargeCodeInfo, 'chg_code', '');
                    $items[$detail['chg_code_id']]['chg_code_des'] = object_get($chargeCodeInfo, 'chg_code_des', '');
                    $items[$detail['chg_code_id']]['chg_uom_id'] = object_get($chargeCodeInfo, 'chg_uom_id', '');
                    $items[$detail['chg_code_id']]['chg_type_id'] = object_get($chargeCodeInfo, 'chg_type_id', '');

                    $items[$detail['chg_code_id']]['skus'][] = [
                        'item_id'   => $detail['item_id'],
                        'sku'       => $detail['sku'],
                        'size'      => $detail['size'],
                        'color'     => $detail['color'],
                        'qty'       => $vail_qty[$detail['item_id']],
                        'wo_qty'    => $detail['qty'],
                        'wo_dtl_id' => $detail['wo_dtl_id'],
                        'cus_upc'   => $detail['cus_upc'],
                        'lot'       => $detail['lot']
                    ];
                }

            } else {
                // Group By Item
                foreach ($workOrderDtl as $detail) {
                    //get more sku
                    $items[$detail['item_id']]['item_id'] = $detail['item_id'];
                    $items[$detail['item_id']]['sku'] = $detail['sku'];
                    $items[$detail['item_id']]['size'] = $detail['size'];
                    $items[$detail['item_id']]['color'] = $detail['color'];
                    $items[$detail['item_id']]['cus_upc'] = $detail['cus_upc'];
                    $items[$detail['item_id']]['lot'] = $detail['lot'];

                    $items[$detail['item_id']]['chargecodes'][] = [
                        'chg_code_id'  => $detail['chg_code_id'],
                        'chg_code'     => $detail['chg_code'],
                        'qty'          => $vail_qty[$detail['item_id']],
                        'wo_qty'       => $detail['qty'],
                        'chg_code_des' => array_get($detail, 'charge_code.chg_code_des', null),
                        'chg_uom_id'   => array_get($detail, 'charge_code.chg_uom_id', null),
                        'chg_type_id'  => array_get($detail, 'charge_code.chg_type_id', null),
                        'wo_dtl_id'    => $detail['wo_dtl_id'],
                    ];
                }
            }

            $workOdrHdr['details'] = array_values($items);

            $odrNum = object_get($workOdrHdr, 'odr_hdr_num', '');
            $cusId = object_get($workOdrHdr, 'cus_id', '');
            $woHdrNum = object_get($workOdrHdr, 'wo_hdr_num', '');
            $createdBy = object_get($workOdrHdr, 'created_by', '');

            $username = $this->userModel->getUserNameById($createdBy);
            $cusInfo = $this->customerModel->getCusById($cusId)->toArray();

            $userInfo = new \Wms2\UserInfo\Data();
            $userInfo = $userInfo->getUserInfo();
            $userId = $userInfo['user_id']; //user_id,username,first_name,last_name
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            //type =1 ? SKU : ChargeCode(0)
            $type = object_get($workOdrHdr, 'type', '');

            // get Order Information: orderInfo ? orderInfo : null
            $orderInfo = '';
            if ($odrNum) {
                $orderInfo = $this->getOrderInfo($cusId, $odrNum);
            }

            $this->printPdfData($workOdrHdr->toArray(), $orderInfo, $woHdrNum, $type, $odrNum, $cusInfo, $username,
                $printedBy);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'Print work order', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $workOdrHdr
     * @param $orderInfo
     * @param $woHdrNum
     * @param $type
     * @param $odrNum
     * @param $cusInfo
     * @param $username
     *
     * @throws \MpdfException
     */
    private function printPdfData($workOdrHdr, $orderInfo, $woHdrNum, $type, $odrNum, $cusInfo, $username, $printedBy)
    {
        $pdf = new Mpdf();
        $html = (string)view('printWorkOrderTemplate', [
            'workOdrHdr' => $workOdrHdr,
            'orderInfo'  => $orderInfo,
            'type'       => $type,
            'odrNum'     => $odrNum,
            'cusInfo'    => $cusInfo,
            'username'   => $username,
            'printedBy'  => $printedBy
        ]);

        $pdf->WriteHTML($html);
        //$dir = storage_path("WorkOrder/$whsId");
        //$pdf->Output("$dir/$woHdrNum.pdf", 'F');
        $pdf->Output("$woHdrNum.pdf", "D");
    }

    protected function getOrderInfo($cusId, $odrNum)
    {
        $odrHdr = $this->orderHdrModel->getOrderInfo($cusId, $odrNum);

        if (empty($odrHdr)) {
            $this->response->errorBadRequest(Message::get('BM017', 'Order number'));
        }

        $odrInfo = [
            'odr_type'    => Status::getByKey("Order-Type", $odrHdr->odr_type),
            'odr_sts'     => Status::getByKey("Order-Status", $odrHdr->odr_sts),
            'ref_code'    => $odrHdr->ref_cod,
            'rush_odr'    => empty($odrHdr->rush_odr) ? 0 : $odrHdr->rush_odr,
            'csr'         => $odrHdr->first_name . ' ' . $odrHdr->last_name,
            'cus_odr_num' => $odrHdr->cus_odr_num,
            'cus_po'      => $odrHdr->cus_po,
        ];

        return $odrInfo;
    }

    public function getOrderInfoSku(Request $request, $cusId, $odrNum = null)
    {
        $isUpdate = filter_var(array_get($request->getQueryParams(), 'update', false), FILTER_VALIDATE_BOOLEAN);
        $odrInfo = [];
        if ($odrNum) {
            if (!$isUpdate) {
                //check order already wo
                $chk = $this->workOrderModel->getFirstBy('odr_hdr_num', $odrNum);
                if ($chk) {
                    return $this->response->errorBadRequest(Message::get('BM128', $chk->wo_hdr_num));
                }
            }

            $odrInfo = $this->getOrderInfo($cusId, $odrNum);
        }
        $rs = $this->orderHdrModel->getSku($cusId, $odrNum);

        return [
            'order_info' => $odrInfo,
            'skus'       => $rs
        ];
    }

    public function getLikeSkuByOrderNum(Request $request, $cusId, $odrNum)
    {
        $input = $request->getQueryParams();
        $searchSku = array_get($input, 'sku', null);
        $skus = [];

        if ($odrNum) {
            $skus = $this->workOrderSkuModel->getLikeSkuByOrderNum($cusId, $odrNum, $searchSku);
        }
        
        return [
            'data' => $skus
        ];
    }

    public function listNewSku(Request $request, $workOrderId, $cusId, $odrNum)
    {
        //Valid WO exist
        $workOdrHdr = $this->workOrderModel->getFirstBy('wo_hdr_id', $workOrderId, ['orderHdr']);
        if (empty($workOdrHdr)) {
            throw new \Exception(Message::get("BM017", "Work Order"));
        }

        $listItem = $this->workOrderSkuModel->getLikeSkuByOrderNum($cusId, $odrNum);

        $data = [];
        if ($listItem) {
            foreach ($listItem as $key => &$item) {
                $data[$key] = $item;
                $itemWoSku = $this->workOrderSkuModel->getFirstByOldIdAndWOId($item['item_id'], $workOrderId);
                $data[$key]['wo_sku_id'] = array_get($itemWoSku, 'wo_sku_id', null);
                $data[$key]['new_item_id'] = array_get($itemWoSku, 'item_id', null);
                $data[$key]['new_sku'] = array_get($itemWoSku, 'sku', null);
                $data[$key]['new_description'] = array_get($itemWoSku, 'description', null);
                $data[$key]['new_pack'] = array_get($itemWoSku, 'pack', null);
                $data[$key]['new_uom_code'] = array_get($itemWoSku, 'uom_code', null);
                $data[$key]['new_uom_name'] = array_get($itemWoSku, 'uom_name', null);
                $data[$key]['new_uom_id'] = array_get($itemWoSku, 'uom_id', null);
            }
        }

        return [
            'data' => $data
        ];
    }

    public function storeNewSku(Request $request)
    {
        return $this->upsertNewSku($request);
    }

    public function updateNewSku(Request $request, $workOrderSkuId = null)
    {
        return $this->upsertNewSku($request, $workOrderSkuId);
    }

    protected function upsertNewSku(Request $request, $workOrderSkuId = null)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        //Validation
        $this->workOrderSkuValidator->validate($input);

        //Valid order exists
        $odrId = array_get($input, 'odr_hdr_id', null);
        if (!($order = $this->orderHdrModel->getFirstBy('odr_id', $odrId))) {
            return $this->response->errorBadRequest((Message::get('BM081', 'Order')));
        }

        //Get current whs
        $usrInfo = new \Wms2\UserInfo\Data();
        $curWhs = $usrInfo->getCurrentWhs();

        $cusId = array_get($input, 'cus_id');
        $woHdrId = array_get($input, 'wo_hdr_id');

        try {
            DB::beginTransaction();

            $input['size'] = isset($input['size']) ? $this->normalizeNA($input['size']) : 'NA';
            $input['color'] = isset($input['color']) ? $this->normalizeNA($input['color']) : 'NA';

            //Valid old item is exist in item
            $resultItemOld = $this->itemModel->getFirstBy('item_id', $input['old_item_id']);
            if (! $resultItemOld) {
                return $this->response->errorBadRequest(Message::get('BM095', 'SKU ' . $input['old_sku'], 'Item Master'));
            }

            //Valid pack size input
            if ($input['pack'] == $resultItemOld['pack']) {
                return $this->response->errorBadRequest('Pack size must different with old pack size');
            }

            //Process Work Order
            if ($workOrderSkuId) {//Update WOSku
                $itemWoSku = $this->workOrderSkuModel->getFirstWhere([
                    'old_item_id' => $input['old_item_id'],
                    'item_id' => $input['item_id'],
                    'wo_sku_id' => $workOrderSkuId
                ]);

                if ($itemWoSku) {
                    $dataItem = [
                        'item_id'      => $input['item_id'],
                        'pack'         => $input['pack'],
                        'description'  => $input['description'],
                        'uom_code'     => $input['uom_code'],
                        'uom_name'     => $input['uom_name']
                    ];

                    //set uom id
                    $resultUom = SystemUom::where('sys_uom_code', $input['uom_code'])->first();
                    $dataItem['uom_id'] = intval($resultUom['sys_uom_id']);

                    //Create new item
                    $item = $this->itemModel->update($dataItem);
                } else {
                    //Valid new item exist
                    $item = $this->validAndCreateNewItem($input, $resultItemOld, $cusId);

                    //Create WorkOrderSku
                    $dataWOSku = [
                        'wo_sku_id'    => intval($workOrderSkuId),
                        'item_id'      => intval($item['item_id'])
                    ];

                    $this->workOrderSkuModel->update($dataWOSku);
                }
            } else {//Create WOSku

                //Valid new item exist
                $item = $this->validAndCreateNewItem($input, $resultItemOld, $cusId);

                //Create WorkOrderSku
                $dataWOSku = [
                    'wo_hdr_id'    => intval($woHdrId),
                    'odr_hdr_id'   => intval($odrId),
                    'odr_dtl_id'   => intval($input['odr_dtl_id']),
                    'whs_id'       => intval($curWhs),
                    'cus_id'       => intval($cusId),
                    'old_item_id'  => intval($input['old_item_id']),
                    'item_id'      => intval($item['item_id']),
                ];

                $this->workOrderSkuModel->create($dataWOSku);
            }

            DB::commit();
            $results = [
                'message'   => 'Update successfully!',
                'wo_hdr_id' => $woHdrId,
            ];

            return ['data' => $results];

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        DB::rollBack();
        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    protected function validAndCreateNewItem($input, $oldItem, $cusId)
    {
        $resultItem = $this->itemModel->getFirstWhere([
            'cus_id' => intval($cusId),
            'sku' => $input['sku'],
            'size' => $input['size'],
            'color' => $input['color']
            // 'pack' => $input['pack'],
            // 'uom_code' => $input['uom_code']
        ]);

        if ($resultItem) {
            return $this->response->errorBadRequest(Message::get('BM095', 'SKU ' . $input['sku'], 'Item Master'));
        }

        $dataItem = [
            'cus_id'       => intval($cusId),
            'sku'          => $input['sku'],
            'pack'         => $input['pack'],
            'description'  => $input['description'],
            'uom_code'     => $input['uom_code'],
            'uom_name'     => $input['uom_name'],
            'size'         => $oldItem['size'],
            'color'        => $oldItem['color'],
            'length'       => $oldItem['length'],
            'width'        => $oldItem['width'],
            'height'       => $oldItem['height'],
            'weight'       => $oldItem['weight'],
            'cus_upc'      => $oldItem['upc'],
            'volume'       => $oldItem['volume'],
            'condition'    => $oldItem['condition'],
            'cube'         => $oldItem['cube'],
            'status'       => config('constants.item_status.ACTIVE')
        ];

        //set uom id
        $resultUom = SystemUom::where('sys_uom_code', $input['uom_code'])->first();
        $dataItem['uom_id'] = intval($resultUom['sys_uom_id']);

        //set item code
        $dataItem['item_code'] = $this->itemModel->generateItemCode($cusId);

        //Create new item
        return $this->itemModel->create($dataItem);
    }

    private function normalizeNA($str)
    {
        $str = trim($str);
        if (strtoupper($str) === 'NA' || empty($str)) {
            return 'NA';
        }

        return $str;
    }

    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    public function update(Request $request)
    {
        return $this->upsert($request, 'update');
    }

    protected function upsert($request, $status = 'create')
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $validator = new WorkOrderHdrValidator();
        $validator->validate($input, $status);

        $usrInfo = new \Wms2\UserInfo\Data();
        $curWhs = $usrInfo->getCurrentWhs();

        $odrNum = '';
        if (!empty($input['odr_num'])) {
            $odrNum = $input['odr_num'];
            $order = $this->orderHdrModel->getFirstBy('odr_num', $odrNum);
            if (!$order) {
                return $this->response->errorBadRequest('Order number invalid');
            }
        }

        $woNum = $status === "create" ? $this->workOrderModel->generateWoNum($odrNum) : false;
        $getItem = function ($arr) {
            if (!isset($arr['item_id'])) {
                throw new \Error('item_id is required');
            }
            $itemModel = new ItemModel();
            $item = $itemModel->getFirstBy('item_id', $arr['item_id']);

            if (!$item) {
                throw new \Error('item_id ' . $arr['item_id'] . ' not existed');
            }

            return $item;
        };

        //create status
        $input_woSts = '';
        if (isset($input['wo_sts'])) {
            $input_woSts = $input['wo_sts'];
        }
        $woSts = Status::getByValue('NEW', "Order-Status");
        if ($input_woSts == "FN") {
            $woSts = $input_woSts;
        }

        if ($woNum) {
            //Create wo_hdr
            $params = [
                'whs_id'      => $curWhs,
                'cus_id'      => $input['cus_id'],
                'wo_hdr_num'  => $woNum,
                'wo_ex_notes' => $input['wo_ex_notes'],
                'wo_in_notes' => $input['wo_in_notes'],
                'type'        => $input['type'],
                'sts'         => 'i',
                'wo_sts'      => $woSts
            ];
        } else {
            //update wo_hdr
            $params = [
                'wo_hdr_id'   => $input['wo_hdr_id'],
                'wo_ex_notes' => $input['wo_ex_notes'],
                'wo_in_notes' => $input['wo_in_notes'],
                'wo_sts'      => Status::getByValue($input['wo_sts'], "WO_STATUS"),
            ];
        }

        if (isset($order)) {
            $params['odr_hdr_id'] = $order->odr_id;
            $params['odr_hdr_num'] = $order->odr_num;

            //get data for validate
            $cpmValue = $this->orderHdrModel->getCmpValue($input['cus_id'], $odrNum);
        }

        try {
            DB::beginTransaction();
            //save wo hdr
            if (($woHdrObj = $this->workOrderModel->$status($params))) {
                $woHdrId = object_get($woHdrObj, 'wo_hdr_id', 0);

                $woDtl = [];
                $woType = $woHdrObj->type === 0 ? "chargecodes" : "skus";
                $chgCodes = [];
                foreach ($input['details'] as $detail) {
                    if ($woHdrObj->type === 0) {
                        $item = $getItem($detail);
                    } else {
                        $chgCodeId = $detail['chg_code_id'];
                        if (isset($chgCodes[$chgCodeId])) {
                            $chgCode = $chgCodes[$chgCodeId];
                        } else {
                            $chgCode = $this->chargeCodeModel->byId($chgCodeId)->chg_code;
                            $chgCodes[$chgCodeId] = $chgCode;
                        }
                    }

                    foreach ($detail[$woType] as $subDetail) {
                        //check
                        if (!isset($subDetail['wo_qty'])) {
                            $subDetail['wo_qty'] = 0;
                        }

                        if ($woHdrObj->type === 0) {
                            $chgCodeId = $subDetail['chg_code_id'];
                            if (isset($chgCodes[$chgCodeId])) {
                                $chgCode = $chgCodes[$chgCodeId];
                            } else {
                                $chgCode = $this->chargeCodeModel->byId($chgCodeId)->chg_code;
                                $chgCodes[$chgCodeId] = $chgCode;
                            }
                        } else {
                            $item = $getItem($subDetail);
                        }
                        //if (isset($cpmValue) && !$this->orderHdrModel->validatWoDtl($cpmValue, $item->item_id,
                        //        $subDetail['wo_qty'])
                        //) {
                        //    throw new \Error(Message::get('VR012', $item->sku . ' qty', 'available qty'));
                        //}

                        $lot = array_get($subDetail, 'lot', 'NA');
                        if ($woType == "chargecodes" && isset($detail['value_selected'])) {
                            $lot = isset($detail['value_selected']['lot']) ? $detail['value_selected']['lot'] : "NA";
                        }

                        $data = [
                            'whs_id'      => $curWhs,
                            'cus_id'      => $input['cus_id'],
                            'item_id'     => $item->item_id,
                            'lot'         => $lot,
                            'sku'         => $item->sku,
                            'size'        => $item->size,
                            'color'       => $item->color,
                            'cus_upc'     => $item->cus_upc,
                            'wo_hdr_num'  => $woNum ?: $woHdrObj->wo_hdr_num,
                            'qty'         => empty($subDetail['wo_qty']) ? 0 : $subDetail['wo_qty'],
                            'chg_code'    => $chgCode,
                            'chg_code_id' => $chgCodeId,
                            'sts'         => 'i'
                        ];

                        //process by order
                        if (isset($order)) {
                            $data['odr_hdr_id'] = $order->odr_id;
                        }

                        //chk update or new wo_dtl
                        if (isset($subDetail['wo_dtl_id'])) {
                            $itm = $this->workOrderDtlModel->getFirstBy('wo_dtl_id', $subDetail['wo_dtl_id']);

                        } else {
                            $itm = new WorkOrderDtl();
                        }

                        if ($itm) {
                            $woDtl[] = $itm->getModel()->fill($data);
                        }
                    }
                }
                //save wo detail
                $woDtlArr = $woHdrObj->WorkOrderDtl()->saveMany($woDtl);

                if ($woNum) {
                    //update wo number
                    $this->workOrderModel->updateWoNum($woNum);
                } else {
                    foreach ($woDtlArr as $rw) {
                        $updatIds[] = $rw->wo_dtl_id;
                    }

                    //delete waste data
                    if (isset($updatIds)) {
                        $this->workOrderDtlModel->getModel()
                            ->where('wo_hdr_id', $woHdrObj->wo_hdr_id)
                            ->whereNotIn('wo_dtl_id', $updatIds)
                            ->delete();
                    }
                }

                // Insert Event Tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create([
                    'whs_id'    => $curWhs,
                    'cus_id'    => $input['cus_id'],
                    'owner'     => $woNum,
                    'evt_code'  => Status::getByKey("event", "Work-Order-Created"),
                    'trans_num' => $woNum,
                    'info'      => sprintf(Status::getByKey("event-info", "Work-Order-Created"), $woNum)
                ]);

                DB::commit();
                $results = [
                    'message'   => Message::get('BM129', ucfirst($status)),
                    'wo_hdr_id' => $woHdrId,
                ];

                return ['data' => $results];
            }
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                $e->getMessage() . " " . $e->getLine()
            //SystemBug::writeSysBugs($e, SysBug::API_WORK_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage() . " ");
        }
    }

    /**
     * @param $workOrderId
     *
     * @return array|void
     */
    public function ChangeStatus($workOrderId)
    {
        $inputParam = [
            'wo_hdr_id' => $workOrderId,
            'wo_sts'    => 'FN'
        ];

        try {
            $workOrder = $this->workOrderModel->getFirstWhere(['wo_hdr_id' => $workOrderId]);
            if (empty($workOrder)) {
                throw new \Exception(Message::get("BM017", "Work Order"));
            }

            $this->workOrderModel->update($inputParam);

            return ["data" => "successful"];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }


}
