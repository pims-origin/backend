<?php


namespace App\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DeleteSoftDataCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'delete:soft-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Soft Data';

    public function handle()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		$tables = DB::select('SHOW TABLES');
//		$this->output->progressStart(count($tables));
		foreach($tables as $table) {
			$tbName = array_values($table)[0];
			$this->info("Table: $tbName");
			if(Schema::connection("mysql")->hasColumns($tbName,['deleted', 'deleted_at'])) {
				$count = DB::table($tbName)->where('deleted',1)->count();
				$this->info("$count records");
				DB::table($tbName)->where('deleted',1)->delete();
			}
//			sleep(0.3);
//			$this->output->progressAdvance();
		}
//		$this->output->progressFinish();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');
	}
}
