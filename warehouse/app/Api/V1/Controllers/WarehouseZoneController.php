<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\WarehouseZoneModel;
use App\Api\V1\Transformers\WarehouseZoneTransformer;
use App\Api\V1\Validators\DeleteMassZoneValidator;
use App\Api\V1\Validators\WarehouseZoneValidator;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\V1\Models\LocationModel;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\SystemBug;

class WarehouseZoneController extends AbstractController
{
    /*
     * @var $warehouseZoneModel
     * */
    protected $warehouseZoneModel;
    /*
     * @var $warehouseZoneModel
     * */
    protected $warehouseZoneValidator;
    /*
     * @var $warehouseZoneTransformer
     * */
    protected $warehouseZoneTransformer;
    /*
     * @var $delMassZoneValidator
     * */
    protected $delMassZoneValidator;

    public function __construct()
    {
        parent::__construct();

        $this->warehouseZoneModel = new WarehouseZoneModel();
        $this->warehouseZoneValidator = new WarehouseZoneValidator();
        $this->warehouseZoneTransformer = new WarehouseZoneTransformer();
        $this->delMassZoneValidator = new DeleteMassZoneValidator();
    }

    public function store(
        $warehouseId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseZoneValidator->validate($input);

        $params = [
            'zone_name'        => $input['zone_name'],
            'zone_code'        => $input['zone_code'],
            'zone_whs_id'      => $warehouseId,
            'zone_type_id'     => $input['zone_type_id'],
            'zone_min_count'   => !isset($input['zone_min_count']) || $input['zone_min_count'] === "" ? null :
                $input['zone_min_count'],
            'zone_max_count'   => !isset($input['zone_max_count']) || $input['zone_max_count'] == "" ? null :
                $input['zone_max_count'],
            'zone_description' => array_get($input, 'zone_description', ''),
            'zone_color'       => array_get($input, 'zone_color', ''),
        ];
        try {
            // check duplicate code
            if ($this->warehouseZoneModel->loadBy([
                'zone_code'   => $params['zone_code'],
                'zone_whs_id' => $warehouseId
            ])->first()
            ) {
                return $this->response->errorBadRequest(Message::get("BM006", "Zone Code"));
            }

            // check duplicate code
            if ($this->warehouseZoneModel->loadBy([
                'zone_name'   => $params['zone_name'],
                'zone_whs_id' => $warehouseId
            ])->first()
            ) {
                return $this->response->errorBadRequest(Message::get("BM006", "Zone Name"));
            }

            if ($warehouseZone = $this->warehouseZoneModel->create($params)) {
                return $this->response->item($warehouseZone,
                    $this->warehouseZoneTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function zoneImport(
        $warehouseId,
        Request $request
    ) {
        $hashKey = md5($request->getUploadedFiles()['file']->getClientFilename());
        $file     = $request->getUploadedFiles()['file'];
        $fileType = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
        $file->moveTo(storage_path($hashKey . ".$fileType"));
        $filePath = storage_path($hashKey . ".$fileType");
        $reader = ReaderFactory::create($fileType);
        $reader->open($filePath);
        $fieldsName = [
            'zone_name',
            'zone_code',
            'zone_type_id',
            'zone_min_count',
            'zone_max_count',
            'zone_description',
            'zone_color',
        ];
        DB::beginTransaction();
        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $index => $row) {

                if($index==1){
                    continue;
                }
                foreach ($fieldsName as $idx => $fieldName) {
                    if($fieldName == "zone_code") {
                        $key_zone_code = $idx;
                        $rs[$fieldName] = !empty($row[$idx]) ? $row[$idx] . "-". $warehouseId  : null;
                    } else {
                        $rs[$fieldName] = !empty($row[$idx]) ? $row[$idx] : null;
                    }
                }
                $rs['zone_whs_id']= $warehouseId;
                // validation
                $this->warehouseZoneValidator->validate($rs);

                // check duplicate code
                if ($this->warehouseZoneModel->loadBy([
                    'zone_code'   => $rs['zone_code'],
                    'zone_whs_id' => $warehouseId
                ])->first()
                ) {
                    return $this->response->errorBadRequest(Message::get("BM006", "Zone Code"));
                }

                // check duplicate code
                if ($this->warehouseZoneModel->loadBy([
                    'zone_name'   => $rs['zone_name'],
                    'zone_whs_id' => $warehouseId
                ])->first()
                ) {
                    return $this->response->errorBadRequest(Message::get("BM006", "Zone Name"));
                }

                $this->warehouseZoneModel->refreshModel();
                $data = $this->warehouseZoneModel->create($rs);
                $zone_id = $data->zone_id;
                $cus_id = DB::table('customer')
                    ->where('cus_code', $row[$key_zone_code])
                    ->value('cus_id');
                DB::table('customer_zone')->insert(
                    [
                        'cus_id' => $cus_id,
                        'deleted_at' => 915148800,
                        'zone_id'=>$zone_id,
                        'created_at' => time(),
                        'updated_at' => time(),
                        'created_by' => JWTUtil::getPayloadValue('jti') ?? 0,
                        'updated_by' => JWTUtil::getPayloadValue('jti') ?? 0,
                        'deleted'   => 0

                        ]
                );

            }
            // Only process one sheet
            break;
        }
        DB::commit();

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function show($warehouseId, $zoneId)
    {
        $warehouseZone = $this->warehouseZoneModel->getZone(
            $warehouseId,
            $zoneId,
            ['customerZone.customer']
        );

        if ($warehouseZone) {
            $warehouseZone['customers'] = object_get($warehouseZone, 'customerZone.customer');

            return $this->response->item($warehouseZone, $this->warehouseZoneTransformer);
        } else {
            return $this->response->noContent();
        }
    }

    public function update(
        $warehouseId,
        $zoneId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseZoneValidator->validate($input);

        $params = [
            'zone_id'          => $zoneId,
            'zone_name'        => $input['zone_name'],
            'zone_code'        => $input['zone_code'],
            'zone_whs_id'      => $warehouseId,
            'zone_type_id'     => $input['zone_type_id'],
            'zone_min_count'   => !isset($input['zone_min_count']) || $input['zone_min_count'] === "" ? null :
                $input['zone_min_count'],
            'zone_max_count'   => !isset($input['zone_max_count']) || $input['zone_max_count'] == "" ? null :
                $input['zone_max_count'],
            'zone_description' => array_get($input, 'zone_description', ''),
            'zone_color'       => array_get($input, 'zone_color', ''),
        ];
        try {
            // check duplicate code
            if (($zone = $this->warehouseZoneModel->loadBy([
                    'zone_code'   => $params['zone_code'],
                    'zone_whs_id' => $warehouseId
                ])->first())
                && $zone->zone_id != $zoneId
            ) {
                return $this->response->errorBadRequest(Message::get("BM006", "Zone Code"));
            }

            // check duplicate code
            if (($zone = $this->warehouseZoneModel->loadBy([
                    'zone_name'   => $params['zone_name'],
                    'zone_whs_id' => $warehouseId
                ])->first())
                && $zone->zone_id != $zoneId
            ) {
                return $this->response->errorBadRequest(Message::get("BM006", "Zone Name"));
            }

            if ($warehouseZone = $this->warehouseZoneModel->update($params)) {
                return $this->response->item($warehouseZone,
                    $this->warehouseZoneTransformer)->setStatusCode(IlluminateResponse::HTTP_OK);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function destroy($warehouseId, $zoneId)
    {
        try {
            // Check Zone Type exist in Zone
            $locationModel = new LocationModel();
            $location = $locationModel->loadBy(['loc_zone_id' => $zoneId])->first();
            if (!empty($location)) {
                return $this->response
                    ->errorBadRequest(Message::get("BM003", "Zone"));
            }

            if ($this->warehouseZoneModel->deleteWarehouseZone($warehouseId, $zoneId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM017", "Zone Selected"));
    }

    public function search($warehouseId, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $warehouses = $this->warehouseZoneModel->search($warehouseId, $input, ['zoneType', 'customerZone'],
                array_get($input, 'limit', config('constants.PAGING_LIMIT')));
            if (!empty($warehouses)) {
                foreach ($warehouses as $key => $warehouse) {
                    if (!empty($warehouse->customerZone->customer)) {
                        $warehouses[$key]['customers'] = $warehouse->customerZone->customer;
                    }
                }
            }

            return $this->response->paginator($warehouses, $this->warehouseZoneTransformer);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function deleteMass($warehouseId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->delMassZoneValidator->validate($input);
        $input['zone_id'] = is_array($input['zone_id']) ? $input['zone_id'] : [$input['zone_id']];

        try {
            // Check Exist Zone in Location
            $locationModel = new LocationModel();
            $locations = $locationModel->loadByZoneIds($input['zone_id'])->first();
            if (!empty($locations)) {
                return $this->response
                    ->errorBadRequest(Message::get("BM003", "Zone"));
            }

            $this->warehouseZoneModel->deleteMassZone($warehouseId, $input['zone_id']);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function loadBy(Request $request)
    {
        $wantedFields = [
            'zone_id',
            'zone_name',
            'zone_code'
        ];

        return $this->_loadBy($request, $wantedFields, $this->warehouseZoneModel, $this->warehouseZoneTransformer);
    }

    /**
     * Free Zone
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function freeZone(
        Request $request
    ) {
        $input = $request->getQueryParams();
        $whs_id = array_get($input, 'whs_id');

        try {
            $zones = $this->warehouseZoneModel->freeZone($whs_id, ['zoneType', 'warehouse'],
                array_get($input, 'limit'));

            return $this->response->paginator($zones, $this->warehouseZoneTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $warehouseId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function export($warehouseId, Request $request)
    {
        $input = $request->getQueryParams();
        $type = strtolower(array_get($input, 'type', 'csv'));

        try {
            if (!in_array($type, ['csv', 'pdf'])) {
                throw new \Exception(Message::get("VR005", "(.csv|.pdf)"));
            }

            $warehouses = $this->warehouseZoneModel->search($warehouseId, $input, [
                'customerZone.customer',
                'zoneType'
            ], array_get($input, 'limit', 999999999));

            $dataTitle = [
                'Zone Code'                    => "zone_code",
                'Zone Name'                    => "zone_name",
                'Zone Type'                    => "zoneType.zone_type_name",
                'Min Count'                    => "zone_min_count",
                'Max Count'                    => "zone_max_count",
                'Number of Locations'          => "zone_num_of_loc",
                'Number of Inactive Locations' => "zone_num_of_inactive_loc",
                'Customer'                     => "customerZone.customer.cus_name",
                'Description'                  => "zone_description",
            ];

            if (SelExport::setDataList($warehouses, $dataTitle)) {
                SelExport::exportList("Export_Zone_List_" . date("Y-m-d", time()), $type);
            }

            return $this->response->noContent();
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
