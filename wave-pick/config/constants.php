<?php

return [
    'qualifier' =>  [
        'ORDER-STATUS'      => 'OSF',
        'WAVEPICK-STATUS'   => 'WSF',
        'ASN_STATUS'        => 'ASF',
        'GR_STATUS'         => 'GSF'
    ]
];