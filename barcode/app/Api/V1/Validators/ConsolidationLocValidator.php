<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 22/July/2016
 * Time: 11:05
 */

namespace App\Api\V1\Validators;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\PalletModel;
use Dingo\Api\Exception\UnknownVersionException;
use Seldat\Wms2\Utils\Message;

class ConsolidationLocValidator extends AbstractValidator
{
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;


    /**
     * ConsolidationPltValidator constructor.
     */
    public function __construct(
        PalletModel $palletModel,
        LocationModel $locationModel,
        CartonModel $cartonModel
    ) {
        $this->palletModel = $palletModel;
        $this->locationModel = $locationModel;
        $this->cartonModel = $cartonModel;

    }


    protected function rules()
    {
        return [
            'lpn_from' => 'required',
            'lpn_to'   => 'required',
        ];
    }

    public function check($fromLocId, $toLocId, $ctn_ids)
    {

        // Load loction (Current location id) to get name..
        $locationInfo = $this->locationModel->checkWhere(['loc_id' => $fromLocId]);

        if (empty($locationInfo)) {
            throw new \Exception(Message::get("BM017", "Current Location"));
        }

        // Load loction (new location id) to get name..
        $newLocationInfo = $this->locationModel->getFirstWhere(['loc_id' => $toLocId]);

        //Check new location is not existed.'
        if (empty($newLocationInfo)) {
            throw new \Exception(Message::get("BM017", "New Location"));
        }

        //check New location status
        $this->checkPalletStatus(object_get($newLocationInfo, 'loc_sts_code', null), "New Location");

        // check exist of carton(s)
        $checkCartons = $this->cartonModel->checkExistedCartons($ctn_ids);
        if ($checkCartons == false) {
            throw new \Exception(Message::get("BM017", "carton(s)"));
        }

        return $newLocationInfo;
    }


    /**
     * @param $loc_sts_code
     * @param $msgLocation
     *
     * @throws \Exception
     */
    public function checkPalletStatus($loc_sts_code, $msgLocation)
    {
        switch ($loc_sts_code) {
            case config('constants.INACTIVE'):
                throw new UnknownVersionException($msgLocation . ' is not active.', null, 1);

            case config('constants.LOCKED'):
                throw new UnknownVersionException($msgLocation . ' is being locked.', null, 2);

            case config('constants.RESERVED'):
                throw new UnknownVersionException($msgLocation . ' is being reserved.', null, 3);

            default:
                break;
        }
    }
}
