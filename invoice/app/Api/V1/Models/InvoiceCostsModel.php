<?php

namespace App\Api\V1\Models;

use App\Invoice;

use Seldat\Wms2\Models\InvoiceCost;

use Seldat\Wms2\Utils\SelArr;

use Illuminate\Support\Facades\DB;

class InvoiceCostsModel extends AbstractModel
{
    protected $model;

    public function __construct(
        InvoiceCost $model = NULL
    )
    {
        $this->model = ($model) ?: new InvoiceCost();
    }

    /*
    ****************************************************************************
    */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        $query = $this->make($with);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'cus_id',
                'whs_id',
                'chg_code_id',
                'chg_type_id',
                'sys_uom_id',
            ],
            'attributes' => $attributes,
            'tablePreface' => [
                'chg_code_id' => 'charge_code',
                'chg_type_id' => 'charge_type',
            ],
            'join' => [
                'chg_code_id' => [
                    'charge_code' => [
                        [
                            'table' => 'charge_code',
                            'one' => 'charge_code.chg_code_id',
                            'operator' => '=',
                            'two' => 'invoice_cost.chg_code_id'
                        ],
                    ],
                ],
                'chg_type_id' => [
                    'charge_type' => [
                        [
                            'table' => 'charge_code AS type_charge_code',
                            'one' => 'type_charge_code.chg_code_id',
                            'operator' => '=',
                            'two' => 'invoice_cost.chg_code_id'
                        ],
                        [
                            'table' => 'charge_type',
                            'one' => 'charge_type.chg_type_id',
                            'operator' => '=',
                            'two' => 'type_charge_code.chg_type_id'
                        ],
                    ],
                ],
                'sys_uom_id' => [
                    'system_uom' => [
                        [
                            'table' => 'charge_code AS system_charge_code',
                            'one' => 'system_charge_code.chg_code_id',
                            'operator' => '=',
                            'two' => 'invoice_cost.chg_code_id'
                        ],
                        [
                            'table' => 'system_uom',
                            'one' => 'system_uom.sys_uom_id',
                            'operator' => '=',
                            'two' => 'system_charge_code.chg_uom_id'
                        ],
                    ],
                ],
            ],
        ]);

        $query->where('chg_cd_price', '!=', 0);

        $this->sortBuilder($query, $searchAttr);

        $models = $query->paginate($limit);

        return $models;
    }

    /*
    ****************************************************************************
    */

    function checkChagreCode($data)
    {
        $result = DB::Table('charge_code')
            ->select('chg_code')
            ->where('deleted', 0)
            ->where('chg_code_id', $data['chg_code_id'])
            ->first();

        $charge_codes = (array)$result;

        $charge_code = $charge_codes['chg_code'];

        $measures = Invoice::$charge_codes_measures;

        $measure = array_get($measures, $charge_code);

        if (! $measure) {
            return TRUE;
        }

        unset($measures[$charge_code]);

        $keys = array_flip($measures);

        $mirror_charge_code = array_get($keys, $measure);

        if (! $mirror_charge_code) {
            return TRUE;
        }

        $used_code = DB::Table('invoice_cost AS ic')
            ->select('chg_code')
            ->join('charge_code AS cc', 'cc.chg_code_id', '=', 'ic.chg_code_id')
            ->where('ic.deleted', 0)
            ->where('cus_id', $data['cus_id'])
            ->where('whs_id', $data['whs_id'])
            ->where('chg_cd_cur', $data['chg_cd_cur'])
            ->where('ic.chg_cd_price', '>', 0)    
            ->where('cc.chg_code', $mirror_charge_code)
            ->first();

        return ! (array)$used_code;
    }

    /*
    ****************************************************************************
    */

    function updateCosts($data)
    {
        $unique = [
            'cus_id' => $data['cus_id'],
            'whs_id' => $data['whs_id'],
            'chg_code_id' => $data['chg_code_id'],
            'chg_cd_cur' => $data['chg_cd_cur'],
        ];

        // before updating restore deleted entry to avoid "duplicate keys" error
        $exists = $this->model
            ->withTrashed()
            ->where($unique)
            ->first();

        if ($exists) {
            $exists->restore();
        }

        $insert['chg_cd_price'] = $data['chg_cd_price'];

        $this->model->updateOrCreate($unique, $insert);

        return $this->model;
    }

    /*
    ****************************************************************************
    */

}

