<?php
namespace App;

use App\Api\V1\Models\BaseInvoiceModel;
use Seldat\Wms2\Utils\Database\Eloquent\SoftDeletes;

class OrderCarton extends BaseInvoiceModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'odr_cartons';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'odr_ctn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'odr_ctn_id',
        'odr_hdr_id',
        'odr_dtl_id',
        'wv_hdr_id',
        'wv_dtl_id',
        'odr_num',
        'wv_num',
        'ctnr_rfid',
        'ctn_num',
        'piece_qty',
        'ship_dt',
        'ctn_id',
        'is_storage',
        'ctn_rfid',
        'ctn_sts',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'upc',
        'pack',
        'uom_id',
        'uom_code',
        'uom_name',
        'loc_id',
        'loc_code',
        'length',
        'width',
        'height',
        'weight',
        'volume',
        'plt_id',
        'plt_rfid',
    ];

    /**
     * carton status
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne(__NAMESPACE__ . '\CartonStatus', 'ctn_sts', 'status');
    }

    /**
     * @return mixed
     */
    public function orderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_hdr_id');
    }

    /**
     * @return mixed
     */
    public function carton()
    {
        return $this->hasOne(__NAMESPACE__ . '\Carton', 'ctn_id', 'ctn_id');
    }

    /**
     * @return mixed
     */
    public function location()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_id', 'loc_id');
    }

    /**
     * @return mixed
     */
    public function pallet()
    {
        return $this->hasOne(__NAMESPACE__ . '\Pallet', 'plt_id', 'plt_id');
    }

}
