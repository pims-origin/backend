<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 08-Aug-16
 * Time: 14:21
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\SystemMeasurementModel;
use App\Api\V1\Transformers\SystemMeasurementTransformer;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class SystemMeasurementController
 *
 * @package App\Api\V1\Controllers
 */
class SystemMeasurementController extends AbstractController
{

    /**
     * @var SystemMeasurementModel
     */
    protected $systemMeasurementModel;
    /**
     * @var SystemMeasurementTransformer
     */
    protected $systemMeasurementTransformer;

    /**
     * SystemMeasurementController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->systemMeasurementModel = new SystemMeasurementModel();
        $this->systemMeasurementTransformer = new SystemMeasurementTransformer();
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(IRequest $request)
    {
        $input = $request->getQueryParams();

        try {
            $systemMeasurement = $this->systemMeasurementModel->search($input, [], array_get($input, 'limit'));

            return $this->response->paginator($systemMeasurement, $this->systemMeasurementTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $systemMeasurementCode
     *
     * @return \Dingo\Api\Http\Response
     */
    public function show($systemMeasurementCode)
    {
        $SystemMeasurement = $this->systemMeasurementModel->getFirstBy('sys_mea_code', $systemMeasurementCode);
        if ($SystemMeasurement) {
            return $this->response->item($SystemMeasurement, $this->systemMeasurementTransformer);
        } else {
            return $this->response->noContent();
        }
    }
}
