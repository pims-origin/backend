<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthorizationService;
use App\Api\V1\Models\BaseService;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\GoodsReceiptDetail;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Seldat\Wms2\Utils\Message;
use Illuminate\Support\Facades\DB;
use Dingo\Api\Routing\Helpers;
use Laravel\Lumen\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;
use Illuminate\Http\Response as LumenResponse;
use Seldat\Wms2\Models\Storages;
use Seldat\Wms2\Utils\Status;

abstract class AbstractController extends Controller
{
    use Helpers;
    protected $userId;
    protected $model;
    protected $transformer;

    public function __construct(
        IRequest $request,
        BaseService $service = null
    )
    {
        $token = $request->getHeader('Authorization');
        if(empty($token[0])) {
            throw new UnauthorizedHttpException('Unauthorized');
        }
        $result = $service->getUserIdByToken($token[0]);

        if (! $result) {
            throw new UnauthorizedHttpException('Unauthorized');
        }

        $this->userId = $result;
    }
    /**
     * Check permission for every action request
     *
     * @param IRequest $request
     * @param $permission
     * @param array $data
     */
    public function checkPermission(IRequest $request, $permission, $data = [])
    {
        $token = $request->getHeader('Authorization');
        if(empty($token[0])) {
            throw new UnauthorizedHttpException('Token is empty');
        }
        $client = new AuthorizationService($request);
        $client->check($permission, $data);
    }

    /**
     * @param IResponse $response
     * @return LumenResponse
     */
    protected function convertResponse(IResponse $response)
    {
        $luResponse = new LumenResponse($response->getBody(), $response->getStatusCode(), []);
        return $luResponse;
    }

    /*
    ****************************************************************************
    */

    public function getParsePostParams($data)
    {
        $arrayParams = [
            'sort',
            'compare_operator',
        ];

        $input = $sort = [];

        foreach ($data as $datum) {

            $values = explode('=', $datum);

            $key = $values[0];

            array_shift($values);

            $value = implode('', $values);

            foreach ($arrayParams as $arrayParam) {

                $length = strlen($arrayParam);

                if (substr($key, 0, $length) == $arrayParam) {

                    $open = strpos($key, '[');
                    $close = strpos($key, ']');

                    if ($open !== FALSE && $close !== FALSE) {

                        $key = substr($key, $open + 1, $close - $open - 1);

                        $input[$arrayParam][$key] = $value;
                    }

                    continue 2;
                }
            }

            $input[$key] = $value;
        }

        return $input;
    }

    /*
    ****************************************************************************
    */

    public function index(
        IRequest $request,
        $data = [],
        Storages $storages
    )
    {
        $model = array_get($data, 'model', NULL);

        $method = array_get($data, 'method', NULL);
        $with = array_get($data, 'with', []);

        $input = $request->getQueryParams();

        $limit = array_get($input, 'limit', PAGING_LIMIT);

        $searchModel = $model ?: $this->model;

        try {

            //Export CSV
            if (!empty($input['export']) && $input['export'] == 1) {
                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $results = $method && method_exists($searchModel, $method) ?
                    $searchModel->$method($input, $with, $limit) :
                    $searchModel->search($input, $with, $limit);
                //if (!$results) {
                //    $this->response->errorBadRequest(Message::get('VR003'));
                //}
                
                $storageinfos = [];
                if ($results) {
                    foreach ($results as $receivingReport) {
                        $packSize = array_get($receivingReport, 'asn_dtl_pack', 0);
                        $grpack = array_get($receivingReport,'pack', 0 );

                        $grHDRID = array_get($receivingReport,'gr_hdr_id','');
                        $gr_item_ttl = GoodsReceiptDetail::where('gr_hdr_id', $grHDRID)
                            ->count('item_id');

                        $gr_ctn_ttl = GoodsReceiptDetail::where('gr_hdr_id', $grHDRID)
                            ->sum('gr_dtl.gr_dtl_act_ctn_ttl', '-', 'gr_dtl.gr_dtl_is_dmg');

                        $asnHDRID = array_get($receivingReport,'asn_hdr_id','');
                        $asn_item_ttl = AsnDtl::where('asn_hdr_id', $asnHDRID)
                            ->count('item_id');

                        $asndtlid = array_get($receivingReport, 'asn_dtl_id');
                        $asn_ctn_ttl = AsnDtl::where('asn_dtl_id', $asndtlid)
                            ->sum('asn_dtl_ctn_ttl');


                        $asnUserFName= array_get($receivingReport, 'first_name');
                        $asnUserLName= array_get($receivingReport, 'last_name');
                        $asnFullName= $asnUserFName . ' ' . $asnUserLName;

                        $grUserFName= array_get($receivingReport, 'first_name');
                        $grUserLName= array_get($receivingReport, 'last_name');
                        $grFullName= $grUserFName . ' ' . $grUserLName;

                        $asnDate = array_get($receivingReport, 'asn_dtl.created_at', null);
                        $asnDate = $asnDate ? date('m-d-Y', $asnDate) : '';

                        $grDate = array_get($receivingReport, 'goods_receipt.created_at', null);
                        $grDate = $grDate ? date('m-d-Y', $grDate) : '';
                        
                        $storageinfos[] = [
                            'whs_name'       => array_get($receivingReport, 'whs_name', ''),
                            'cus_name'       => array_get($receivingReport, 'cus_name', ''),
                            'ctnr_num'       => array_get($receivingReport, 'ctnr_num', ''),

                            'asn_number'     => array_get($receivingReport, 'asn_hdr_num'),
                            'gr_hdr_num'     => array_get($receivingReport, 'gr_hdr_num', ''),

                            'asn_created_at' => $asnDate,
                            'asn_full_name'  => $asnFullName,
                            'asn_ttl_sku'    => $asn_item_ttl,
                            'asn_ttl_ctn'    => $asn_ctn_ttl,
                            'asn_ttl_pcs'    => $asn_ctn_ttl * $packSize,

                            'gr_created_at'  => $grDate,
                            'gr_full_name'   => $grFullName,

                            'gr_ttl_sku'     => $gr_item_ttl,
                            'gr_ttl_ctn'     => $gr_ctn_ttl,
                            'gr_ttl_pcs'     => $gr_ctn_ttl * $grpack,
                            "gr_dtl_plt_ttl" => array_get($receivingReport, 'gr_hdr_plt_ttl', 0),

                            'cube'=> round(($gr_ctn_ttl * (array_get($receivingReport, 'asn_dtl.item.length', 0) *
                                        array_get($receivingReport, 'asn_dtl.item.width', 0) *
                                        array_get($receivingReport, 'asn_dtl.item.height', '')))/1728 , 2),

                        ];
                    }
                }
                
                $title = [
                    'whs_name'                      => 'Warehouse',
                    'cus_name'                      => 'CUST',
                    'ctnr_num'                      => 'CNTR',
                    'asn_number'                    => 'ASN NUM',
                    'gr_hdr_num'                    => 'RCV NUM',
                    'asn_created_at'                => 'ASN DT',
                    'asn_full_name'                 => 'ASN Created By',
                    'asn_ttl_sku'                   => 'TTL ASN SKU',
                    'asn_ttl_ctn'                   => 'TTL ASN CTN',
                    'asn_ttl_pcs'                   => 'TTL ASN PCS',
                    'gr_created_at'                 => 'GR DT',
                    'gr_full_name'                  => 'Created By',
                    'gr_ttl_sku'                    => 'TTL GR SKU',
                    'gr_ttl_ctn'                    => 'TTL GR CTNS',
                    'gr_ttl_pcs'                    => 'TTL GR PCS',
                    'gr_dtl_plt_ttl'                => 'TTL GR PLT',
                    
                    'cube'                          => 'Cube',

                ];

                // $filePath = storage_path() . "/Report_PBPM_{$warehouse->whs_name}.csv";

                $today = time();
                $filePath = "Report_Storages_{$today}";
                $this->saveFile($title, $storageinfos, $filePath);
                die;
            }

            $results = $method && method_exists($searchModel, $method) ?
                $searchModel->$method($input, $with, $limit) :
                $searchModel->search($input, $with, $limit);

            if ($results) {

                $autocomplete = array_get($input, 'autocomplete', NULL);

                return $autocomplete ?
                    array_column($results->toArray(), $autocomplete) :
                    $this->response->paginator($results, $this->transformer);
            } else {
                $this->response->errorBadRequest(Message::get('VR003'));
            }

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    } 

    /*
    ****************************************************************************
    */




    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->openToBrowser($filePath); // stream data directly to the browser

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();
    }


}
