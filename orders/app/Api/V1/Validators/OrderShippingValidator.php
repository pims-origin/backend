<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:28
 */

namespace App\Api\V1\Validators;

/**
 * Class OrderShippingValidator
 *
 * @package App\Api\V1\Validators
 */
class OrderShippingValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'ship_by_dt' => 'integer',
            'ship_dt'    => 'integer'
        ];
    }
}
