<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\Api\V1\Transformers\RFGunDetailTransformer;
use App\Api\V1\Transformers\RFGunTransformer;
use App\Api\V1\Validators\RFGunValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use App\Api\V1\Models\LocationModel;
use Illuminate\Http\Response as IlluminateResponse;


/**
 * Class PalletController
 *
 * @package App\Api\V1\Controllers
 */
class PalletController extends AbstractController
{

    protected $userId;
    /**
     * @var PalletSuggestLocationModel
     */
    protected $palletSuggestLocationModel;

    /**
     * @var PalletModel
     */
    protected $palletModel;

    protected $cartonModel;

    protected $locationModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var GoodReceiptHdrModel
     */
    protected $goodReceiptHdrModel;

    /**
     * PalletController constructor.
     *
     * @param PalletModel $palletModel
     * @param PalletSuggestLocationModel $palletSuggestLocationModel
     */
    public function __construct(
        PalletModel $palletModel,
        PalletSuggestLocationModel $palletSuggestLocationModel,
        LocationModel $locationModel,
        CartonModel $cartonModel,
        EventTrackingModel $eventTrackingModel
    ) {
        $this->palletModel = $palletModel;
        $this->palletSuggestLocationModel = $palletSuggestLocationModel;
        $this->locationModel = $locationModel;
        $this->cartonModel = $cartonModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->goodReceiptHdrModel = new GoodsReceiptModel();

        $this->userId = JWTUtil::getPayloadValue('jti') ? JWTUtil::getPayloadValue('jti') : 0;
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param RFGunTransformer $rfGunTransformer
     * @param RFGunValidator $rfGunValidator
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search($whsId, Request $request, RFGunTransformer $rfGunTransformer, RFGunValidator $rfGunValidator)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;
        $input['putter'] = $this->userId;

        $rfGunValidator->validate($input);

        try {
            $allPallets = $this->palletSuggestLocationModel->show($input)->toArray();
            $all = [];
            if (!empty($allPallets)) {
                foreach ($allPallets as $allPallet) {

                    if (!empty($allPallet['actual'])) {
                        if (empty($all[$allPallet['gr_hdr_id']]['complete'])) {
                            $all[$allPallet['gr_hdr_id']]['complete'] = 0;
                        }
                        $all[$allPallet['gr_hdr_id']]['complete']++;
                    } else {
                        if (empty($all[$allPallet['gr_hdr_id']]['not-complete'])) {
                            $all[$allPallet['gr_hdr_id']]['not-complete'] = 0;
                        }
                        $all[$allPallet['gr_hdr_id']]['not-complete']++;
                    }
                }
            }

            $pallets = $this->palletSuggestLocationModel->search($input, ['pallet']);

            if (!empty($pallets)) {
                foreach ($pallets as $key => $pallet) {
                    $pallet->total = array_get($all, "{$pallet->gr_hdr_id}.complete", 0) +
                        array_get($all, "{$pallet->gr_hdr_id}.not-complete", 0);
                    $pallet->actual = array_get($all, "{$pallet->gr_hdr_id}.complete", 0);

                    $pallets[$key] = $pallet;
                }
            }

            return $this->response->collection($pallets, $rfGunTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function show($whsId, $grHdrId, RFGunDetailTransformer $rfGunDetailTransformer)
    {
        try {
            $pallets = $this->palletSuggestLocationModel->show([
                'gr_hdr_id' => $grHdrId,
                'whs_id'    => $whsId,
                'putter'    => $this->userId
            ]);

            return $this->response->collection($pallets, $rfGunDetailTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function updatePalletToLocation($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        $code = array_get($input, 'codes', null);

        if (empty($code)) {
            return $this->response->errorBadRequest("Please input codes values");
        }

        if (count($code) % 3 != 0) {
            return $this->response->errorBadRequest("Format codes input is incorrect.");
        }
        $arrSlip = array_chunk($code, 3);

        $arrUpdate = [];
        //validate data
        foreach ($arrSlip as $item) {
            if ($item[0] != $item[2]) {
                $msg = sprintf("Location start %s is not same with %s", $item[0], $item[2]);

                return $this->response->errorBadRequest($msg);
            }
            $objLoc = $this->locationModel->getLocationByLocCode($item[0], $whsId);

            if (is_null($objLoc)) {
                $msg = sprintf("Location code %s is not existed or in using.", $item[0]);

                return $this->response->errorBadRequest($msg);
            }

            $objPlt = $this->palletModel->getPalletByPalletNum($item[1]);
            if (is_null($objPlt)) {
                $msg = sprintf("Pallet number %s is not existed or put to RACK.", $item[1]);

                return $this->response->errorBadRequest($msg);
            }

            //push data to array Update
            $arrUpdate[] = [
                'plt_num'  => $objPlt->plt_num,
                'whs_id'   => $objPlt->whs_id,
                'cus_id'   => $objPlt->cus_id,
                'plt_id'   => $objPlt->plt_id,
                'loc_id'   => $objLoc->loc_id,
                'loc_code' => $objLoc->loc_code,
                'loc_name' => $objLoc->loc_alternative_name,
            ];
        }

        //update loc id to pallet
        try {
            // start transaction
            DB::beginTransaction();
            foreach ($arrUpdate as $item) {
                $this->palletModel->updateWhere(
                    [
                        'loc_id'   => $item['loc_id'],
                        'loc_code' => $item['loc_code'],
                        'loc_name' => $item['loc_name'],
                    ],
                    [
                        'plt_id' => $item['plt_id'],
                    ]);

                $this->cartonModel->updateWhere(
                    [
                        'loc_id'        => $item['loc_id'],
                        'loc_code'      => $item['loc_code'],
                        'loc_type_code' => 'RAC',
                        'loc_name'      => $item['loc_name'],
                    ],
                    [
                        'plt_id' => $item['plt_id'],
                    ]);


                $event = [
                    'whs_id'    => $item['whs_id'],
                    'cus_id'    => $item['cus_id'],
                    'owner'     => $item['loc_code'],
                    'evt_code'  => 'RFP',
                    'trans_num' => 'RFG',
                    'info'      => sprintf("RF Gun -  Put Pallet %s on %s", $item['plt_num'],$item['loc_code']),
                ];

                //  Evt tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create($event);
            }


            DB::commit();

            //respond
            $msg = sprintf("%d pallets has been put on Rack successfully!", count($arrUpdate));

            return $this->response->noContent()
                ->setContent(['data' => ['message' => $msg]])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getSuggestLocation($whsId, $grDtlId)
    {
        //update loc id to pallet
        try {
            $hdrData = $this->palletSuggestLocationModel->getHdrPutAwayByHdrDtlID($grDtlId);

            $dtlData = $this->palletSuggestLocationModel->getDtlPutAwayByHdrDtlID($grDtlId);

            $returnData = [];
            foreach ($dtlData as $item) {
                $returnData[] = $item->loc_code;
                $returnData[] = $item->plt_num;
                $returnData[] = $item->loc_code;
            }

            $sku_size_color = $hdrData[0]['sku'];
            $size = !empty($hdrData[0]['size']) ? strtoupper($hdrData[0]['size']) : '';
            if ($size != 'NA') {
                $sku_size_color .= '-'.$hdrData[0]['size'];
            }
            $color = !empty($hdrData[0]['color']) ? strtoupper($hdrData[0]['color']) : '';
            if ($color != 'NA') {
                $sku_size_color .= '-'.$hdrData[0]['color'];
            }
            $hdrData[0]['sku'] = $sku_size_color;

            $data = [
                'hdr' => $hdrData,
                'dtl' => $returnData,
            ];

            return $data;
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function putawayDashboard($whsId, Request $request)
    {
        $input = $request->getQueryParams();
        $day = $input['day'];
        $input['whs_id'] = $whsId;
        $input['putter'] = $this->userId;
        try {

            $result = $this->goodReceiptHdrModel->grDashBoard($whsId, $this->userId, $day);


            /*$allPallets = $this->palletSuggestLocationModel->show($input, [], $day)->toArray();
            $all = [];
            if (!empty($allPallets)) {
                foreach ($allPallets as $allPallet) {

                    if (!empty($allPallet['actual'])) {
                        if (empty($all[$allPallet['gr_hdr_id']]['complete'])) {
                            $all[$allPallet['gr_hdr_id']]['complete'] = 0;
                        }
                        $all[$allPallet['gr_hdr_id']]['complete']++;
                    } else {
                        if (empty($all[$allPallet['gr_hdr_id']]['not-complete'])) {
                            $all[$allPallet['gr_hdr_id']]['not-complete'] = 0;
                        }
                        $all[$allPallet['gr_hdr_id']]['not-complete']++;
                    }
                }
            }

            $pallets = $this->palletSuggestLocationModel->search($input, ['pallet']);
            $result = ["putaway_total" => 0, "putting" => 0, "putReady" => 0];
            if (!empty($pallets)) {
                foreach ($pallets as $key => $pallet) {
                    $total = array_get($all, "{$pallet->gr_hdr_id}.complete", 0) +
                        array_get($all, "{$pallet->gr_hdr_id}.not-complete", 0);
                    $actual = array_get($all, "{$pallet->gr_hdr_id}.complete", 0);
                    $result["putaway_total"] = $result["putaway_total"] + 1;
                    $result["putting"] = $result["putting"] + ($total > $actual) ? 1 : 0;
                    $result['putReady'] = $result['putReady'] + ($total == $actual) ? 1 : 0;

                }
            }*/

            return $this->response->array($result);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


}

