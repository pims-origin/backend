<?php

namespace App\Api\Inventory\Transformers;

use App\Api\Inventory\Models\BlockDtlModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\BlockDtl;

class BlockDtlTransformer extends TransformerAbstract
{
    /**
     * @param BlockDtl $blockDtl
     *
     * @return array
     */
    public function transform(BlockDtl $blockDtl)
    {
        return [
            'block_dtl_id'  => $blockDtl->block_dtl_id,
            'block_hdr_id'  => $blockDtl->block_hdr_id,
            'whs_id'        => $blockDtl->whs_id,
            'cus_id'        => $blockDtl->cus_id,
            'zone_name'     => object_get($blockDtl, 'location.zone.zone_name'),
            'item_id'       => $blockDtl->item_id,
            'upc'           => object_get($blockDtl, 'itemInfo.cus_upc'),
            'sku'           => $blockDtl->sku,
            'size'          => $blockDtl->size,
            'color'         => $blockDtl->color,
            'loc_id'        => $blockDtl->loc_id,
            'loc_name'      => $blockDtl->loc_name,
            'block_dtl_sts' => $blockDtl->block_dtl_sts,
            'created_by'    => $blockDtl->created_by,
            'updated_by'    => $blockDtl->updated_by,
            'created_at'    => $blockDtl->created_at,
            'updated_at'    => $blockDtl->updated_at,
            'sts'           => $blockDtl->sts,
            'deleted'       => $blockDtl->deleted,
            'deleted_at'    => $blockDtl->deleted_at,

            'whs_name'           => object_get($blockDtl, 'warehouse.whs_name'),
            'cus_name'           => object_get($blockDtl, 'customer.cus_name'),
            'block_dtl_sts_name' => BlockDtlModel::$arrCycleDtlStatus[$blockDtl->block_dtl_sts],
        ];
    }
}
