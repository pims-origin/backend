<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Clients\SampleClient;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\SampleModel;
use App\Api\V1\Transformers\SampleTransformer;
use App\Api\V1\Validators\SampleValidator;
use Psr\Http\Message\ServerRequestInterface as Request;

class SampleController extends AbstractController
{

    /**
     * @var SampleModel
     */
    protected $model;

    /**
     * @var SampleValidator
     */
    protected $validator;

    /**
     * @var SampleClient
     */
    protected $client;

    /**
     * SampleController constructor.
     * @param SampleModel $model
     * @param SampleValidator $validator
     * @param SampleClient $client
     */
    public function __construct(AsnHdrModel $model, SampleValidator $validator, SampleClient $client)
    {
        $this->model = $model;
        $this->validator = $validator;
        $this->client = $client;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $input = $request->getParsedBody();

        $this->validator->validate($input);

        $data = $this->model->get();

        return $this->response->item($data, new SampleTransformer);
    }


    /**
     * @return mixed
     */
    public function index()
    {
       dd($this->model->loadBy([],['asnDtl']));

        return $this->response->array($response);
    }
}
