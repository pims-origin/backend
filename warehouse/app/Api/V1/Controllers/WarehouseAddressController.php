<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MenuFavoriteModel;
use App\Api\V1\Models\WarehouseAddressModel;
use App\Api\V1\Models\MenuModel;
use App\Api\V1\Transformers\WarehouseAddressTransformer;
use App\Api\V1\Validators\WarehouseAddressValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class WarehouseAddressController extends AbstractController
{
    /**
     * @var WarehouseAddressModel
     */
    protected $warehouseAddressModel;

    /**
     * @var $warehouseAddressTransformer
     */
    protected $warehouseAddressTransformer;

    /**
     * @var $warehouseAddressValidator
     */
    protected $warehouseAddressValidator;

    /**
     * WarehouseAddressController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseAddressModel = new WarehouseAddressModel();
        $this->warehouseAddressTransformer = new WarehouseAddressTransformer();
        $this->warehouseAddressValidator = new WarehouseAddressValidator();
    }

    /**
     * @SWG\Get(
     *     path="/v1/warehouses/{warehouseId}/addresses",
     *     summary="List All Warehouse Address",
     *     description="List All Warehouse Address",
     *     operationId="listWarehouseAddress",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array all Warehouse Address",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_add_line_1": "ffffffffff",
     *                     "whs_add_line_2": "ffffffffff",
     *                     "whs_add_country_code": "eeeeeeeeee",
     *                     "whs_add_city_name": "dddddd",
     *                     "whs_add_state_code": "ccccccccccc",
     *                     "whs_add_postal_code": "bbbbbbbbbb",
     *                     "whs_add_whs_id": 0,
     *                     "whs_add_type": "aaaaaaaaaaa",
     *                     "created_at": {
     *                     "date": "2016-05-26 04:29:18.000000",
     *                     "timezone_type": 3,
     *                     "timezone": "UTC"
     *                     },
     *                         "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": null
     *                 },
     *                 {
     *                     "whs_add_line_1": "aaaaaaaaaa",
     *                     "whs_add_line_2": "bbbbbbbbbb",
     *                     "whs_add_country_code": "cccccccccc",
     *                     "whs_add_city_name": "dddddd",
     *                     "whs_add_state_code": "eeeeeeeeeeee",
     *                     "whs_add_postal_code": "ffffffffffff",
     *                     "whs_add_whs_id": 0,
     *                     "whs_add_type": "ggggggggggggggg",
     *                     "created_at": {
     *                     "date": "2016-05-26 04:29:18.000000",
     *                     "timezone_type": 3,
     *                     "timezone": "UTC"
     *                     },
     *                         "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": "2016-05-26 11:28:02"
     *                 }
     *             }
     *         },
     *     ),
     * )
     *
     * @param int $warehouseId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($warehouseId)
    {
        $warehouseAddresss = $this->warehouseAddressModel->allBy('whs_add_whs_id', $warehouseId, ['systemState']);

        return $this->response->collection($warehouseAddresss, $this->warehouseAddressTransformer);
    }

    /**
     * @SWG\Get(
     *     path="/v1/warehouses/{warehouseId}/addresses/{addressId}",
     *     summary="Read Warehouse Address",
     *     description="View Detail Warehouse Address",
     *     operationId="showWarehouseAddress",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="addressId",
     *         description="Warehouse Address Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Warehouse Address",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_add_line_1": "ffffffffff",
     *                     "whs_add_line_2": "ffffffffff",
     *                     "whs_add_country_code": "eeeeeeeeee",
     *                     "whs_add_city_name": "dddddd",
     *                     "whs_add_state_code": "ccccccccccc",
     *                     "whs_add_postal_code": "bbbbbbbbbb",
     *                     "whs_add_whs_id": 0,
     *                     "whs_add_type": "aaaaaaaaaaa",
     *                     "created_at": {
     *                     "date": "2016-05-26 04:29:18.000000",
     *                     "timezone_type": 3,
     *                     "timezone": "UTC"
     *                     },
     *                         "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": null
     *                 },
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Warehouse Address Detail
     *
     * @param int $warehouseId
     * @param int $addressId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_add_id
     */
    public function show($warehouseId, $addressId)
    {
        $warehouseAddress = $this->warehouseAddressModel->loadBy([
            'whs_add_id'     => $addressId,
            'whs_add_whs_id' => $warehouseId
        ]);

        return $this->response->item($warehouseAddress, $this->warehouseAddressTransformer);
    }

    /**
     * @SWG\Post(
     *     path="/warehouses/{warehouseId}/addresses",
     *     summary="Create Warehouse Address",
     *     description="Create Warehouse Address",
     *     operationId="createWarehouseAddress",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_line_1",
     *         description="Warehouse Address's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_line_2",
     *         description="Warehouse Address's Description",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_country_code",
     *         description="Warehouse Address's Country Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_city_name",
     *         description="Warehouse Address's City Name",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_state_code",
     *         description="Warehouse Address's State Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_postal_code",
     *         description="Warehouse Address's Postal Code",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_whs_id",
     *         description="Warehouse Address's Warehouse Id",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_type",
     *         description="Warehouse Address's Type",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Warehouse Address",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_add_line_1": "ffffffffff",
     *                     "whs_add_line_2": "ffffssssss",
     *                     "whs_add_country_code": "eeeeeeeeee",
     *                     "whs_add_city_name": "dddddd",
     *                     "whs_add_state_code": "ccccccccccc",
     *                     "whs_add_postal_code": "bbbbbbbbbb",
     *                     "whs_add_whs_id": 0,
     *                     "whs_add_type": "aaaaaaaaaaa",
     *                     "created_at": {
     *                     "date": "2016-05-26 04:29:18.000000",
     *                     "timezone_type": 3,
     *                     "timezone": "UTC"
     *                     },
     *                         "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": null
     *                 },
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * Warehouse Address Create
     *
     * @param int $warehouseId
     * @param Request $request
     *
     * @return mixed
     */
    public function store(
        $warehouseId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseAddressValidator->validate($input);

        $params = [
            'whs_add_line_1'      => $input['whs_add_line_1'],
            'whs_add_line_2'      => !empty($input['whs_add_line_2']) ? $input['whs_add_line_2'] : '',
            'whs_add_city_name'   => $input['whs_add_city_name'],
            'whs_add_state_id'    => $input['whs_add_state_id'],
            'whs_add_postal_code' => $input['whs_add_postal_code'],
            'whs_add_whs_id'      => $warehouseId,
            'whs_add_type'        => $input['whs_add_type'],
        ];

        try {
            if ($warehouseAddress = $this->warehouseAddressModel->create($params)) {
                return $this->response->item($warehouseAddress,
                    $this->warehouseAddressTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Put(
     *     path="/v1/warehouses/{warehouseId}/addresses/{addressId}",
     *     summary="Update Warehouse Address",
     *     description="Update Warehouse Address",
     *     operationId="updateWarehouseAddress",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="addressId",
     *         description="Warehouse Address Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_line_1",
     *         description="Warehouse Address's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_line_2",
     *         description="Warehouse Address's Description",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_country_code",
     *         description="Warehouse Address's Country Code",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_city_name",
     *         description="Warehouse Address's City Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_state_code",
     *         description="Warehouse Address's State Code",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_postal_code",
     *         description="Warehouse Address's Postal Code",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_whs_id",
     *         description="Warehouse Address's Warehouse Id",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="whs_add_type",
     *         description="Warehouse Address's Type",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Warehouse Address",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 ref="#/definitions/data"
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "whs_add_line_1": "ffffffffff",
     *                     "whs_add_line_2": "ffffssssss",
     *                     "whs_add_country_code": "eeeeeeeeee",
     *                     "whs_add_city_name": "dddddd",
     *                     "whs_add_state_code": "ccccccccccc",
     *                     "whs_add_postal_code": "bbbbbbbbbb",
     *                     "whs_add_whs_id": 0,
     *                     "whs_add_type": "aaaaaaaaaaa",
     *                     "created_at": {
     *                     "date": "2016-05-26 04:29:18.000000",
     *                     "timezone_type": 3,
     *                     "timezone": "UTC"
     *                     },
     *                         "updated_at": {
     *                         "date": "-0001-11-30 00:00:00.000000",
     *                         "timezone_type": 3,
     *                         "timezone": "UTC"
     *                     },
     *                     "deleted_at": null
     *                 },
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     *
     * @param int $warehouseId
     * @param int $addressId
     * @param Request $request
     *
     * @return mixed
     * @internal param int $whs_add_id
     */
    public function update(
        $warehouseId,
        $addressId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->warehouseAddressValidator->validate($input);

        $params = [
            'whs_add_id'          => $addressId,
            'whs_add_line_1'      => $input['whs_add_line_1'],
            'whs_add_line_2'      => !empty($input['whs_add_line_2']) ? $input['whs_add_line_2'] : '',
            'whs_add_city_name'   => $input['whs_add_city_name'],
            'whs_add_state_id'    => $input['whs_add_state_id'],
            'whs_add_postal_code' => $input['whs_add_postal_code'],
            'whs_add_whs_id'      => $warehouseId,
            'whs_add_type'        => $input['whs_add_type'],
        ];

        try {
            if ($warehouseAddress = $this->warehouseAddressModel->update($params)) {
                return $this->response->item($warehouseAddress, $this->warehouseAddressTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @SWG\Delete(
     *     path="/v1/warehouses/{warehouseId}/addresses/{addressId}",
     *     summary="Delete Warehouse Address",
     *     description="Delete a Warehouse Address",
     *     operationId="addressId",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="warehouseId",
     *         description="Warehouse Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="path",
     *         name="addressId",
     *         description="Warehouse Address Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Warehouse Address was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param int $warehouseId
     * @param int $addressId
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param int $whs_add_id
     * @internal param MenuModel $menuModel
     * @internal param MenuFavoriteModel $menuFavoriteModel
     *
     */
    public function destroy($warehouseId, $addressId)
    {
        try {
            if ($this->warehouseAddressModel->deleteWarehouseAddress($warehouseId, $addressId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM017", "Warehouse Address"));
    }
}
