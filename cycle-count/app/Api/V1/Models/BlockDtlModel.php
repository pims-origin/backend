<?php

namespace App\Api\V1\Models;

use App\Api\V1\Services\CycleCountService;
use Illuminate\Validation\Validator;
use phpDocumentor\Reflection\Location;
use phpDocumentor\Reflection\Types\Null_;
use Seldat\Wms2\Models\BlockDtl;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Illuminate\Support\Facades\DB;

class BlockDtlModel extends AbstractModel
{
    public $errors = [];

    protected $arrNumberFields = [
        'block_hdr_id',
        'block_dtl_id'
    ];

    protected $arrStringFields = [
        'sku',
        'size',
        'color'
    ];

    const STATUS_ITEM_BLOCKED = 'LK';
    const STATUS_ITEM_ACCEPTED = 'AC';

    public static $arrCycleDtlStatus = [
        self::STATUS_ITEM_BLOCKED => 'Blocked',
        self::STATUS_ITEM_ACCEPTED => 'Accepted'
    ];

    /**
     * BlockDtlModel constructor.
     * @param BlockDtl $model
     */
    public function __construct(BlockDtl $model)
    {
        $this->model = $model;
    }

    /**
     * @param $blockHdrId
     * @param null $blockDtlId
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @return mixed
     */

    public function search($blockHdrId, $blockDtlId = null, $attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->where('block_hdr_id', $blockHdrId);

        if ($blockDtlId) {
            $query->where('block_dtl_id', $blockDtlId);
        }

        if (!empty($attributes)) {

            foreach ($attributes as $key => $value) {
                if (in_array($key, $this->arrNumberFields)) {
                    $query->where($key, $value);
                } elseif (in_array($key, $this->arrStringFields)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $this->model->filterData($query, true, true);
        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $model
     * @param $data
     * @return mixed
     */

    public static function unblockItemDtl($model, $data)
    {
        return $model->save();
    }
}

