<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 08-Aug-16
 * Time: 14:21
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\SystemUomModel;
use App\Api\V1\Transformers\SystemUomTransformer;
use App\Api\V1\Validators\SystemUomValidator;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class SystemUomController
 *
 * @package App\Api\V1\Controllers
 */
class SystemUomController extends AbstractController
{

    /**
     * @var $systemUomModel
     */
    protected $systemUomModel;

    /**
     * @var $systemUomTransformer
     */
    protected $systemUomTransformer;

    /**
     * @var $systemUomValidator
     */
    protected $systemUomValidator;

    /**
     * @var $systemUomDeleteMassValidator
     */
    protected $systemUomDeleteMassValidator;

    /**
     * SystemUomController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));

        $this->systemUomModel = new SystemUomModel();
        $this->systemUomTransformer = new SystemUomTransformer();
        $this->systemUomValidator = new SystemUomValidator();
    }

    /**
     * @return \Dingo\Api\Http\Response|void
     */
    public function index()
    {
        try {
            $systemUom = $this->systemUomModel->all();

            return $this->response->collection($systemUom, $this->systemUomTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->systemUomValidator->validate($input);

        $params = [
            'sys_uom_code' => $input['sys_uom_code'],
            'sys_uom_des'  => !empty($input['sys_uom_des']) ? $input['sys_uom_des'] : '',
            'sys_uom_name' => $input['sys_uom_name'],
        ];

        try {
            // check duplicate Uom Code
            $countUomCode = $this->systemUomModel->checkWhere([
                'sys_uom_code' => $params['sys_uom_code']
            ]);
            if (!empty($countUomCode)) {
                return $this->response->errorBadRequest(Message::get("BM006", "System Uom Code"));
            }

            // check duplicate Uom Name
            $countUomName = $this->systemUomModel->checkWhere([
                'sys_uom_name' => $params['sys_uom_name']
            ]);
            if (!empty($countUomName)) {
                return $this->response->errorBadRequest(Message::get("BM006", "System Uom Name"));
            }

            if ($systemUom = $this->systemUomModel->create($params)) {
                return $this->response->item($systemUom,
                    $this->systemUomTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $systemUomId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($systemUomId)
    {
        try {
            $systemUom = $this->systemUomModel->getFirstBy('sys_uom_id', $systemUomId);

            return $this->response->item($systemUom, $this->systemUomTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $systemUomId
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update($systemUomId, IRequest $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['sys_uom_id'] = $systemUomId;
        // validation
        $this->systemUomValidator->validate($input);

        $params = [
            'sys_uom_id'   => $systemUomId,
            'sys_uom_code' => $input['sys_uom_code'],
            'sys_uom_des'  => !empty($input['sys_uom_des']) ? $input['sys_uom_des'] : '',
            'sys_uom_name' => $input['sys_uom_name'],
        ];

        try {
            // Check not exist
            if (!$this->systemUomModel->checkWhere(['sys_uom_id' => $systemUomId])) {
                return $this->response->errorBadRequest(Message::get("BM017", "System Uom"));
            }

            // check duplicate Uom Code
            $systemUom = $this->systemUomModel->getFirstWhere([
                'sys_uom_code' => $params['sys_uom_code']
            ]);
            if (!empty($systemUom) && $systemUom->sys_uom_id != $systemUomId) {
                return $this->response->errorBadRequest(Message::get("BM006", "System Uom Code"));
            }

            // check duplicate Uom Name
            $systemUom = $this->systemUomModel->getFirstWhere([
                'sys_uom_name' => $params['sys_uom_name']
            ]);
            if (!empty($systemUom) && $systemUom->sys_uom_id != $systemUomId) {
                return $this->response->errorBadRequest(Message::get("BM006", "System Uom Name"));
            }

            if ($systemUom = $this->systemUomModel->update($params)) {
                return $this->response->item($systemUom, $this->systemUomTransformer);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
