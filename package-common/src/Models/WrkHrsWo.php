<?php

namespace Seldat\Wms2\Models;

class WrkHrsWo extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wrk_hrs_wo';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'wo_hdr_id',
        'dt',
        'type',
        'amount',
        'cat',
        'inv_sts'
    ];

    
    public function WorkOrderHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\WorkOrderHdr', 'wo_hdr_id', 'wo_hdr_id');
    }
    
    public function invoiceHeader()
    {
        return $this->belongsTo(__NAMESPACE__ . '\InvoiceHeader', 'inv_id', 'inv_id');
    }
}

