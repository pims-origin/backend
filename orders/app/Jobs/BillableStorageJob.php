<?php

namespace App\Jobs;

use DB;

class BillableStorageJob extends IMSJob
{
    protected $odrId;

    public function __construct($odrId)
    {
        $this->odrId = $odrId;
    }

    public function handle()
    {
        $odrHdr = DB::table('odr_hdr')
            ->where('odr_id', $this->odrId)
            ->first();

        try {
            $token = $this->login();

            $endpoint = env('IMS_BILLABLE_STORAGE');

            // Check IMS Configuration
            if(empty($endpoint)) {
                $this->writeLogment('IMS Billable Storage ' . $odrHdr['odr_num'], 'IMS Billable Storage Endpoint is not define');
                return false;
            }

            $data = DB::table('billable_hdr')
                ->join('billable_dtl', 'billable_dtl.ba_id', '=', 'billable_hdr.ba_id')
                ->leftJoin('cus_sac', 'cus_sac.sac_id', '=', 'billable_dtl.sac_id')
                ->where('billable_hdr.ba_type', 'OUB')
                ->where('billable_hdr.deleted', 0)
                ->where('billable_hdr.whs_id', $odrHdr['whs_id'])
                ->where('billable_hdr.ref_num', $odrHdr['odr_num'])
                ->select([
                    'billable_hdr.cus_id',
                    'billable_hdr.whs_id',
                    'billable_dtl.sac_name',
                    'billable_dtl.ref_num',
                    'billable_dtl.qty',
                    'billable_dtl.ba_type',
                    'billable_dtl.description',
                    'cus_sac.unit_uom'
                ])
                ->groupBy('billable_dtl.cus_id')
                ->groupBy('billable_dtl.sac')
                ->get();

            if(count($data) == 0) {
                $this->writeLogment('IMS Billable Storage ' . $odrHdr['odr_num'], 'No data available');
                return false;
            }

            $maked = $this->transformMake($data);
            $data = ['data' => $maked];

            // Call cURL to IMS
            $header = [
                "Authorization: Bearer " . $token,
                "Content-Type: application/json"
            ];

            $result = $this->call($endpoint, 'POST', json_encode($data), $header);

            $this->writeLogment('IMS Billable Storage ' . $odrHdr['odr_num'], 'URL: '. $endpoint .' --- Request: ' . json_encode($data) . ' --- Response: ' . $result);

        } catch (\Exception $e) {
            $this->writeLogment('IMS Billable Storage ' . $odrHdr['odr_num'], $e->getMessage());
        }
    }

    public function transformMake($data) {
        $result = [];
        foreach($data as $key => $val) {
            $result[$key] = [
                "cus_id"        => array_get($val, 'cus_id', null),
                "whs_id"        => array_get($val, 'whs_id', null),
                "sac_name"      => array_get($val, 'sac_name', null),
                "ref_num"       => array_get($val, 'ref_num', null),
                "total_qty"     => array_get($val, 'qty', null),
                "ba_type"       => array_get($val, 'ba_type', null),
                "unit_uom"      => array_get($val, 'unit_uom', null),
                "description"   => array_get($val, 'description', null)
            ];
        }

        return $result;
    }
}
