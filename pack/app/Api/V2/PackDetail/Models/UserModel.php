<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 1-Sep-16
 * Time: 09:25
 */

namespace App\Api\V2\PackDetail\Models;

use Seldat\Wms2\Models\User;


/**
 * Class UserModel
 *
 * @package App\Api\V1\Models
 */
class UserModel extends AbstractModel
{
    /**
     * @param User $model
     */
    public function __construct(User $model = null)
    {
        $this->model = ($model) ?: new User();
    }


}
