<?php

namespace App\Modules\Orders\Models;

use App\Modules\Orders\Eloquents\OdrDtl;
use Illuminate\Support\Facades\DB;
use App\Helpers\Status;

class OrderDtlModel extends AbstractModel
{
    public function __construct(OdrDtl $model = null)
    {
        $this->model = ($model) ?: new OdrDtl();
    }

    /**
     * @param $orderHdrId
     * @param $itemIds
     * @param array $with
     *
     * @return mixed
     */
    public function loadByOrdIds($orderHdrIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('odr_id', $orderHdrIds);

        $models = $query->get();

        return $models;
    }

    public function revertOrder($odrObj, $isNew)
    {
        $odrId = $odrObj->odr_id;
        $odrType = $odrObj->odr_type;
        $whsId = $odrObj->whs_id;

        $odrStsNotUpdate = [
            Status::getByValue('Picking'        , 'Order-Status'),
            Status::getByValue('Picked'         , 'Order-Status'),
            Status::getByValue('Partial Picked' , 'Order-Status'),
            Status::getByValue('Packing'        , 'Order-Status'),
            Status::getByValue('Partial Packing', 'Order-Status'),
            Status::getByValue('Packed'         , 'Order-Status'),
            Status::getByValue('Partial Packed' , 'Order-Status'),
            Status::getByValue('Staging'        , 'Order-Status'),
            Status::getByValue('Partial Staging', 'Order-Status'),
            Status::getByValue('Shipped'        , 'Order-Status'),
            Status::getByValue('Partial Shipped', 'Order-Status'),
            Status::getByValue('Hold'           , 'Order-Status')
        ];

        if (in_array($odrObj->odr_sts, $odrStsNotUpdate)) {
            return true;
        }

        // WMS2-4404 When order hasn't been picked, return Order's allocated to Inventory summary 's allocated. In others case, will return when wavepick update or cartons consolidate.
        $invSumrModel = new InventorySummaryModel();
        $return = $invSumrModel->updateAllocatedOrderInventory($whsId, $odrId);

        return $return;
    }
}
