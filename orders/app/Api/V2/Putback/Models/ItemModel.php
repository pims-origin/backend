<?php

namespace App\Api\V2\Putback\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\InventorySummary;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use stdClass;
use Wms2\UserInfo\Data;

class ItemModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    protected $cartonModel;

    protected $carton;

    public function __construct()
    {
        $this->model = new Item();
        $this->inventorySummaryModel = new InventorySummaryModel();
        $this->cartonModel = new CartonModel();
        $this->carton = new Carton();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (isset($attributes['cus_id'])) {
            $query = $query->where('cus_id', $attributes['cus_id']);
        }

        $arrLike = ['color', 'sku', 'size', 'lot'];
        $arrEqual = [
            'item_id',
            'cus_upc',
        ];
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query = $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual)) {
                    $query = $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        $models = $query->paginate($limit);
        $data = $models;

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $whs = array_get($userInfo, 'current_whs', 0);
        $itemIdArr = [];

        foreach ($data as $key => $item) {
            $avail = $this->inventorySummaryModel->getInvSumbyOdrDlt($item['item_id'],'ANY', $whs);
            $itemIdArr[$item['item_id']] = $item['item_id'];
            // Select avail in carton
           // $avail = $this->cartonModel->getAvailItem($item['item_id']);
           // $avail = $avail - intval(object_get($invtSum, 'allocated_qty', 0)) ;
            $models[$key]->avail = $avail;
           // $models[$key]->allocated_qty = object_get($invtSum, 'allocated_qty', null);
            //$models[$key]->dmg_qty = object_get($invtSum, 'dmg_qty', null);
        }
        $this->rebuildPackSizeArr($models, $itemIdArr, $attributes);

        return $models;
    }

    /**
     * Rebuild pack size to array on page create order
     *
     * @param LengthAwarePaginator $models    Pointer
     * @param Array                $itemIdArr Array item id
     */
    private function rebuildPackSizeArr(&$models, $itemIdArr, $attributes)
    {
        $itemIdArr = array_values($itemIdArr);
        $cartonsModel = new CartonModel();
        $packObj = $cartonsModel->getCartonsByItemId($itemIdArr);
        $packArr = [];
        $lotArr = [];

        $avail = $this->inventorySummaryModel->getAvailBySku($attributes['sku']);
        $availArr = [];
        foreach($avail as $itemLot) {
            $key = "{$itemLot['item_id']}-{$itemLot['lot']}";
            $availArr[$key] = $itemLot['available'];
        }

        foreach ($packObj as $key => $value) {
            $packSize = $value['ctn_pack_size'];
            $packArr[$value['item_id']][$packSize] = $packSize;
            $lotUni = "{$value['lot']}-{$value['ctn_pack_size']}";
            $tmpObj = new stdClass();
            $tmpObj->pack_size = $value['ctn_pack_size'];
            $tmpObj->sku = $value['sku'];
            $lotArr[$value['item_id']][$value['lot']][$lotUni] = $tmpObj;
        }

        foreach ($models as $idx => $val) {
            //$models[$idx]->pack_arr = $packArr[$val['item_id']];
            if (isset($packArr[$val['item_id']])) {
                $arr = array_values($packArr[$val['item_id']]);
                $arrItem = [];
                foreach($arr as $value) {
                    $obj = new stdClass();
                    $obj->pack_size = $value;
                    $arrItem[] = $obj;
                }

                $models[$idx]->pack =$arrItem;
            } else {
                $obj = new \stdClass();
                $obj->pack_size = $val['pack'];
                $tmp = [$obj];
                $models[$idx]->pack = $tmp;
            }

            if(isset($lotArr[$val['item_id']])) {
                $lot = $lotArr[$val['item_id']];
                $lotArr2 = [];
                foreach($lot as $lotName => $packSize) {
                    $tmp = new \stdClass();
                    $tmp->lot = $lotName;
                    $tmp->pack = array_values($packSize);
                    $cKey = "{$val['item_id']}-{$lotName}";
                    $tmp->avail = isset($availArr[$cKey])?$availArr[$cKey]:0;
                    $lotArr2[] = $tmp;
                }

                $models[$idx]->lot =$lotArr2;

            }

        }

    }

    public function fetchColumn($columns = [], $where = "", $params = [], $upperCase = false)
    {
        $tblName = $this->getTable();
        $colNames = implode(', ', $columns) . ",uom_id,lot";

        $query = "
            SELECT $colNames
            FROM $tblName
        ";
        if (!empty($where)) {
            $query .= " WHERE $where";
        }

        $result = [];

        $db = $this->model->getConnection()->getPdo();

        $statement = $db->prepare($query);
        $statement->execute($params);

        $colKey = $columns;
        end($columns);
        $endKey = key($columns);
        unset($colKey[$endKey]);

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $rowRight = $row;
            $rowLeft = array_splice($rowRight, 0, (count($rowRight) - 2));
            end($rowLeft);
            $end = key($rowLeft);
            $val = [
                $columns[$endKey] => $row[$columns[$endKey]],
                'uom_id'          => $row['uom_id'],
                'lot'             => $row['lot'],
            ];
            unset($rowLeft[$end]);
            $key = implode("-", $rowLeft);

            if ($upperCase) {
                $key = strtoupper($key);
            }

            $result[$key] = $val;
        }

        return $result;
    }

    public static function generateItemCode($cusId, $number = 1)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $cusCode = Customer::where('cus_id', $cusId)
            ->value('cus_code');

        if (empty($cusCode)) {
            throw new HttpException(403, 'This customer has customer_code invalid');
        }

        $key = '91' . $cusCode;

        $seq = DB::table('seq')
            ->where([
                'tbl_name' => 'item',
                'key'      => $key
            ])
            ->value('seq');

        if (empty($seq)) {
            $seq = $key . '000001';
            //update
            DB::table('seq')
                ->insert([
                    'tbl_name' => 'item',
                    'key'      => $key,
                    'seq'      => $seq
                ]);
        } else {
            $seq++;
            //update
            DB::table('seq')
                ->where([
                    'tbl_name' => 'item',
                    'key'      => $key
                ])
                ->update(['seq' => $seq]);
        }

        return $seq;
    }
}
