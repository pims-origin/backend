<?php

namespace App\Api\V2\Inbound\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\User;


/**
 * Class UserModel
 *
 * @package App\Api\V2\Inbound\Models
 */
class UserModel extends AbstractModel
{
    /**
     * @param User $model
     */
    public function __construct(User $model = null)
    {
        $this->model = ($model) ?: new User();
    }

    public function getListUserOfWH($whsID) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        return $this->model
            ->join('user_whs', 'user_whs.user_id', '=','users.user_id')
            ->where('user_whs.whs_id', $whsID)
            ->get();
    }

}
