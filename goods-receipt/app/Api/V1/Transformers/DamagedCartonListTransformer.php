<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\Pallet;

class DamagedCartonListTransformer extends TransformerAbstract
{
    public function transform(GoodsReceipt $goodsReceipt)
    {
        $details = object_get($goodsReceipt, 'details', null);

        return [
            'gr_hdr_id'      => object_get($goodsReceipt, 'gr_hdr_id', null),
            'gr_hdr_num'     => object_get($goodsReceipt, 'gr_hdr_num', null),
            'whs_id'         => object_get($goodsReceipt, 'whs_id', null),
            'cus_id'         => object_get($goodsReceipt, 'cus_id', null),
            // damagedCartons of Goods Receipt Details
            'damagedCartons' => $details,
        ];
    }
}

