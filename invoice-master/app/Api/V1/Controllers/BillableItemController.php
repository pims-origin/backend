<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\NABillModel;
use App\Api\V1\Models\OutboundBillModel;
use App\SysBug;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use App\Api\V1\Models\GoodsReceiptBillModel;
use App\Api\V1\Models\StorageBillModel;
use App\Api\V1\Models\TransferBillModel;
use App\Api\V1\Models\BillableItemModel;
use App\Api\V1\Transformers\BillableTransformer;
use App\Api\V1\Traits\PutBackBillableTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Api\V1\Models\BillableHdrModel;
use App\Api\V1\Models\BillableDtlModel;
use GuzzleHttp\Client;

class BillableItemController extends AbstractController
{
    use PutBackBillableTrait;

    protected $billableItemModel;
    protected $transformer;
    protected $billableItemHdrModel;
    protected $billableItemDtlModel;

    /**
     * Init.
     *
     */
    public function __construct()
    {
        $this->billableItemModel    = new BillableItemModel();
        $this->transformer          = new BillableTransformer();
        $this->billableItemHdrModel = new BillableHdrModel();
        $this->billableItemDtlModel = new BillableDtlModel();
    }

    /**
     * @SWG\Post(
     *     path="/billableitem/goodsreceipt",
     *     tags={"Billable Items"},
     *     summary="Create Billable Item From Goods Receipt",
     *     description="Create Billable Item",
     *     operationId="createBillableitemGoodsReceipt",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    /**
     * Store a newly created data in billable.
     *
     * @param  IRequest $request
     * @return Response
     */
    public function storeGoodReceipt(IRequest $request)
    {
        $input = $request->getParsedBody();
        $goodsReceiptBillModel = new GoodsReceiptBillModel(
            $input['cus_id'],
            $input['whs_id'],
            $input['gr_hdr_id']
        );
        try {
            $goodsReceiptBillModel->saveBillableItem();
            return $this->response->created(Null, '{}');
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/billableitem/outbound",
     *     tags={"Billable Items"},
     *     summary="Create Billable Item From Order",
     *     description="Create Billable Item",
     *     operationId="createBillableitemOutbound",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    /**
     * Store new Billable for Outbound
     * @param IRequest $request
     * @return mixed
     * */
    public function storeOutbound(IRequest $request) {
        try {
            DB::beginTransaction();
            $input = $request->getParsedBody();
            $orderHdrId = $input['odr_id'];
            $outboundBillModel = new OutboundBillModel($orderHdrId);
            $outboundBillModel->storeBillable();
            DB::commit();
            return $this->response->created();
        } catch (\Exception $e) {
            DB::rollback();
            SysBug::writeSysBug($e->getMessage(), 'invoice-master(Billable Outbound)', __FUNCTION__);
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    /**
     * Store new Billable for NA
     * @param IRequest $request
     * @return mixed
     * */
    public function storeNA(IRequest $request) {
        try {
            DB::beginTransaction();
            $input = $request->getParsedBody();

            $naBillModel = new NABillModel(
                $input
            );

            $naBillModel->saveBillableItem();
            DB::commit();
            return ['data' => ['messsage' => 'Billable Create Successful!!']];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function autocompleteSac(IRequest $request) {
        try {
            DB::beginTransaction();

            $input = $request->getParsedBody();
//            if(empty($input['key'])) {
//                return $this->response->created(Null, '{}');
//            }

            $naBillModel = new NABillModel(
                $input
            );

            if(empty($input['limit'])) {
                $input['limit'] = 20;
            }

            $charges = $naBillModel->getChargeCodeListByKey($input['key'],$input['limit']);
            $uom_names = config("constants.unit_uom_name");

            foreach ($charges as $k => $v) {
                $charges[$k]->unit_uom_name = $uom_names[$v->unit_uom];
            }
            DB::commit();
            return ['data' => $charges];
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/billableitem/search",
     *     tags={"Billable Items"},
     *     summary="Search Billable Item",
     *     description="Search Billable Item",
     *     operationId="getBillalbeitem",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cus_id",
     *         in="query",
     *         description="ID of Customer",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="whs_id",
     *         in="query",
     *         description="ID of Warehouse",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="types[]",
     *         in="query",
     *         description="Type of Charge Code (INB, STR, OUB, PB, TRA, WO)",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="endDate",
     *         in="query",
     *         description="End Date of Filter (Y-m-d)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Limit",
     *         type="integer",
     *         default="20",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page",
     *         type="integer",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Billable Item",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    public function search(IRequest $request)
    {
        $params = $request->getQueryParams();

        try {
            // get list SAC
            $billableItems = $this->billableItemModel->search($params,['header', 'orderHdr'], array_get($params, 'limit'));

            return $this->response->paginator($billableItems, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/billableitem/put-back",
     *     tags={"Billable Items"},
     *     summary="Create Billable Item From Putback",
     *     description="Create Billable Item",
     *     operationId="createBillableitemPutpack",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    public function createBillableItemViaPutPack(Request $request)
    {
        try {
            if($this->proccessBillable($request)){
                return Response(['data'=>'success'],200);
            }
            Log::error('Can\'t created billable '.$request->get('return_id'));
            return $this->response->errorBadRequest('Can\'t created billable '.$request->get('return_id'));
        }catch(\Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/billableitem/storage",
     *     tags={"Billable Items"},
     *     summary="Create Billable Item From Storage",
     *     description="Create Billable Item",
     *     operationId="createBillableitemStorage",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    public function storeStorage(IRequest $request)
    {
        $input = $request->getParsedBody();

        $storageBillModel = new StorageBillModel(
            $input['cus_id'],
            $input['whs_id'],
            $input['billable_date'],
            $input['billable_date']
        );

        $billAbleMonth = (int)date("m", strtotime($input['billable_date']));
        $currentMonth = (int)date("m");
        if($billAbleMonth === $currentMonth){
            // Call API Report => /inventory/update-report/{whs_id}/{cus_id}?month=10
            $client = new Client();
            $version = "v1";
            $url = sprintf('/%s/inventory/update-report/%s/%s?month=%s', $version, $input['whs_id'], $input['cus_id'], $input['billable_date']);
            $authorization = $request->getHeader('Authorization');

            $response = $client->request('GET', env("API_REPORT"). $url,
                [
                    'headers'     => ['Authorization' => $authorization],
                    'form_params' => []
                ]
            );
            Log::info('Call inventory/update-report with result: ' . $response->getStatusCode());
        }

        if (!$storageBillModel->isValidTimeRange()) {
            return Response(['error'=>'Time range is not valid.'],500);
        }

        try {
            $storageBillModel->saveBillableItem();
            return $this->response->created(Null, '{}');
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getTimeRange(IRequest $request)
    {
        $input = $request->getQueryParams();
        $storageBillModel = new StorageBillModel(
            $input['cus_id'],
            $input['whs_id']
        );

        $data['start_date'] = date('m/d/Y', $storageBillModel->getStartDate());
        $data['end_date'] = date('m/d/Y', strtotime(' -1 day'));

        return $data;
    }

    /**
     * @SWG\Post(
     *     path="/billableitem/transfer",
     *     tags={"Billable Items"},
     *     summary="Create Billable Item From Xfer",
     *     description="Create Billable Item",
     *     operationId="createBillableitemTransfer",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         description="Body Data",
     *         required=true,
     *         @SWG\Schema(ref="")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    public function storeTransfer(IRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->getParsedBody();
            $xferHdrId = $input['xfer_hdr_id'];
            $transferBillModel = new TransferBillModel($xferHdrId);
            $transferBillModel->storeBillable();
            DB::commit();
            return $this->response->created();
        } catch (\Exception $e) {
            DB::rollback();
            SysBug::writeSysBug($e->getMessage(), 'invoice-master(Billable Outbound)', __FUNCTION__);
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/billableitem/search/refnum/{refNum}",
     *     tags={"Billable Items"},
     *     summary="Search Billable Item By Ref Num",
     *     description="Search Billable Item By Ref Num",
     *     operationId="getBillalbeitemByRefNum",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="refNum",
     *         in="path",
     *         description="Ref Num of Item -e.g: GDR-1802-00026-01",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Limit",
     *         type="integer",
     *         default="20",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page",
     *         type="integer",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Bilable Item",
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="unexpected error",
     *     ),
     *     security={{
     *         "api_key":{}
     *     }}
     * )
     */
    public function searchByRefNum($refNum, IRequest $request)
    {
        $params = $request->getQueryParams();

        try {
            // get list SAC
            $billableItems = $this->billableItemModel->searchByRefNum($params,[], array_get($params, 'limit'), $refNum);

            return $this->response->paginator($billableItems, $this->transformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Delete Multiple BillableItem
     * @param ServerRequestInterface $request
     * @return mixed
     * */
    public function deleteMulti(IRequest $request)
    {
        $input = $request->getParsedBody();
        $dtlIds = $input['ids'];

        if (!empty($ids))
        {
            if ($this->validateBillableItemToDelete($ids))
            {
                throw new \Exception("Can't delete billable item. Billable item has created invoice!");
            }
        }

        try {
            $hdrIds = $this->billableItemDtlModel->getModel()->whereIn('ba_dtl_id', $input['ids'])->pluck('ba_id')->toArray();
            DB::beginTransaction();
            $this->billableItemDtlModel->forcedeleteMultiByBillableItemHdrIds($dtlIds);
            foreach ($hdrIds as $hdrId) {
                $checkHdrId = $this->billableItemDtlModel->getModel()->where('ba_id', $hdrId)->first();
                if (!$checkHdrId) {
                    $this->billableItemHdrModel->forcedeleteMultiByBillableItemHdrIds($hdrId);
                }
            }

            DB::commit();

            return $this->response->accepted(null, ['msg' => 'Deleted successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function validateBillableItemToDelete($ids) {
        $check = DB::table('inv_dtl')
            ->where('deleted', 0)
            ->whereIn('ba_dtl_id', $ids)
            ->first();

        return !empty($check) ? true : false;
    }

    public function deleteStorageBillableByMonth(IRequest $request)
    {
        $input = $request->getParsedBody();
        $input['types'] = ['STR'];

        $cusId = array_get($input, 'cus_id');
        $whsId = array_get($input, 'whs_id');
        $date = array_get($input, 'billable_date');
        $date = str_replace('-', '/', $date);

        if ( !$cusId || !$date || !$whsId ) {
            $msg = 'Missing parameters';
            $this->response->errorBadRequest($msg);
        }

        $billableItems = $this->billableItemModel->search($input);
        $billableItems = $billableItems->filter(function($item) use ($date){
            return str_contains($item->description, [$date]);
        });

        $baDtlIds = $billableItems->pluck('ba_dtl_id')->all();
        $affectedRows = $this->billableItemModel->forceDelete($baDtlIds);

        $msg = sprintf('%s billable item(s) was deleted', $affectedRows);
        return $this->response->accepted(null, ['msg' => $msg]);
    }
}
