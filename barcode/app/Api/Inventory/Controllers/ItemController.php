<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */


namespace App\Api\Inventory\Controllers;

use App\Api\Inventory\Models\ItemModel;
use App\Api\Inventory\Transformers\ItemTransformer;
use Dingo\Api\Routing\Route;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class ItemController extends AbstractController
{
    protected $itemModel;
    protected $model;
    protected $itemTransformer;


    public function __construct(
        ItemModel $itemModel
    ) {
        $this->model = $itemModel;
        $this->itemTransformer = new ItemTransformer();
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response|void
     */

    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $item = $this->model->search($input, ['customer', 'carton'], array_get($input, 'limit', 10));

            return $this->response->paginator($item, $this->itemTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Laravel\Lumen\Http\ResponseFactory|string|\Symfony\Component\HttpFoundation\Response|void
     */
    public function listItems(Request $request)
    {
        $input = $request->getQueryParams();

        $result = [];
        try {

            if (! isset($input['whs_id'])) {
                return $this->response->error("Warehouse Id empty", IlluminateResponse::HTTP_NOT_ACCEPTABLE);
            }

            $whsId = $input['whs_id'];
            $item_code  = isset($input['term']) ? $input['term'] : NULL;

            $data = $this->model->searchByWarehouse($whsId, $item_code);

            if ($data) {

                foreach ($data as $item) {
                    $result['data'][] = $item;
                }
                return response()->json($result);
            }
            return response(Message::get('BM067', 'Data'), 404);
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }
}
