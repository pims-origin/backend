<?php

namespace App\Api\Shipping\Models;

use \Seldat\Wms2\Models\OrderHdrMeta;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class OrderHdrMetaModel extends AbstractModel
{
    /**
     * @param OrderHdrMeta $model
     */
    public function __construct(OrderHdrMeta $model = null)
    {
        $this->model = ($model) ?: new OrderHdrMeta();
    }
    
    public function updateShippingLocation($odrObj, $location)
    {
        $odrId  = object_get($odrObj, 'odr_id');
        $odrNum = object_get($odrObj, 'odr_num');

        $value = [
            'odr_id'   => $odrId,
            'odr_num'  => $odrNum,
            'loc_id'   => object_get($location, 'loc_id'),
            'loc_code' => object_get($location, 'loc_code'),
            'loc_name' => object_get($location, 'loc_name'),
        ];

        $data = [
            'odr_id'    => $odrId,
            'qualifier' => 'SPL',
            'value'     => json_encode([$value]),
        ];

        return $this->model->create($data);
    }
}