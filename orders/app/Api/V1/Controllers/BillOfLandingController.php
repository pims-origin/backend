<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ChargeCodeModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\CommodityModel;
use App\Api\V1\Models\WorkOrderModel;
use App\Api\V1\Models\WorkOrderDtlModel;
use App\Api\V1\Models\CustomerChargeDetailModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Transformers\CommodityTransformer;
use App\Api\V1\Transformers\WorkOrderDtlTransformer;
use App\Api\V1\Transformers\WorkOrderViewTransformer;
use App\Api\V1\Validators\WorkOrderHdrValidator;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Models\WorkOrderDtl;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Psr\Http\Message\ServerRequestInterface as Request;

class BillOfLandingController extends AbstractController
{
    public function __construct(
        OrderHdrModel $orderHdrModel,
        CustomerChargeDetailModel $customerChargeDetailModel,
        CustomerModel $customerModel,
        ChargeCodeModel $chargeCodeModel,
        WorkOrderModel $workOrderModel,
        WorkOrderDtlModel $workOrderDtlModel,
        CommodityModel $commodityModel
    ) {

        $this->orderHdrModel = $orderHdrModel;
        $this->customerChargeDetailModel = $customerChargeDetailModel;
        $this->customerModel = $customerModel;
        $this->chargeCodeModel = $chargeCodeModel;
        $this->workOrderModel = $workOrderModel;
        $this->workOrderDtlModel = $workOrderDtlModel;
        $this->commodityModel = $commodityModel;
        $this->eventTrackingModel = new EventTrackingModel(new EventTracking());
    }

    /**
     * @param CommodityTransformer $commodityTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getCommodityList(CommodityTransformer $commodityTransformer)
    {
        try {
            $commondityInfo = $this->commodityModel->all();

            return $this->response->item($commondityInfo, $commodityTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }


    public function showStatusOrDropDown(
        Request $request
    ) {
        // get data from HTTP
        $objects = $request->getQueryParams();

        //objects:ship-ready,delivery-service,trailer-load,freight-counted,freight-charge-terms,fee-terms,shipment-method...
        $types = Status::get($objects['objects']);
        $data = [];
        if (!empty($types) && is_array($types)) {
            foreach ($types as $key => $value) {
                $data[] = [
                    "key"   => $key,
                    "value" => $value,
                ];
            }
        }

        return ['data' => $data];
    }


}
