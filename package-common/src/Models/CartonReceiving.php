<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace Seldat\Wms2\Models;

class CartonReceiving extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cartons_rev';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ctn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'plt_id',
        'asn_dtl_id',
        'gr_dtl_id',
        'whs_id',
        'cus_id',
        'ctn_num',
        'rfid',
        'ctn_sts',
        'ctn_uom_id',
        'loc_id',
        'loc_code',
        'loc_name',
        'loc_type_code',
        'is_damaged',
        'piece_remain',
        'piece_ttl',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted',
        'deleted_at',
        'is_ecom',
        'gr_hdr_id',
        'gr_dt',
        'picked_dt',
        'storage_duration',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'ctn_pack_size',
        'po',
        'expired_dt',
        'uom_code',
        'uom_name',
        'upc',
        'ctnr_id',
        'ctnr_num',
        'length',
        'width',
        'height',
        'weight',
        'cube',
        'volume',
        'return_id',
        'des',
        'shipped_dt',
        'rev_at'
    ];

    /**
     * carton status
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne(__NAMESPACE__ . '\CartonStatus', 'ctn_sts', 'status');
    }

    /** good receipt detail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goodReceiptDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptDetail', 'gr_dtl_id', 'gr_dtl_id');
    }

    /**
     *  item
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AsnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }

    /**
     *  pallet
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pallet()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Pallet', 'plt_id', 'plt_id');
    }

    /**
     *  location
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Location', 'loc_id', 'loc_id');
    }

    public function damageCarton()
    {
        return $this->hasOne(__NAMESPACE__ . '\DamageCarton', 'ctn_id', 'ctn_id');
    }

    /**
     *  pallet
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }
    
    public function cycleDiscrepancy()
    {
        return $this->hasMany(__NAMESPACE__ . '\CycleDiscrepancy', 'ctn_id', 'ctn_id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'updated_by', 'user_id');
    }

}
