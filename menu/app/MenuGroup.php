<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 11:05 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Utils\Database\Eloquent\SoftDeletes;

/**
 * @property mixed menu_group_id
 * @property mixed name
 * @property mixed description
 */
class MenuGroup extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menu_group';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'menu_group_id';

    /**
     * @var array
     */
    protected $fillable = [
        'menu_group_id',
        'name',
        'description',
    ];

    public function menus()
    {
        return $this->hasMany('App\Menu', 'menu_group_id', 'menu_group_id');
    }
}
