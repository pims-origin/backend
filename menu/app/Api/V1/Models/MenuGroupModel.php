<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/20/16
 * Time: 11:08 AM
 */

namespace App\Api\V1\Models;

use App\MenuGroup;

class MenuGroupModel extends AbstractModel
{
    /**
     * @var QueryBuilder
     */
    protected $model;

    /**
     * EloquentMenuGroup constructor.
     *
     * @param MenuGroup $model
     */
    public function __construct(MenuGroup $model = null)
    {
        $this->model = ($model) ?: new MenuGroup();
    }

    /**
     * Delete Menu Group, and then also delete menu and favorite menu
     *
     * @param int $menuGroupId
     * @param MenuModel $menuModel
     * @param MenuFavoriteModel $menuFavoriteModel
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteWithMenuAndFavorite($menuGroupId, MenuModel $menuModel, MenuFavoriteModel $menuFavoriteModel)
    {
        try {
            if ($this->deleteById($menuGroupId)) {

                $menuIds = $menuModel->listMenuIdsByMenuGroupId($menuGroupId);
                // delete favorite menu
                $menuFavoriteModel->deleteByMenuIds($menuIds);
                // delete menu
                $menuModel->deleteById($menuIds);

                return true;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }

        return false;
    }

    public function getByName($name)
    {
        return $this->getFirstBy('name', $name);
    }
}