<?php

namespace App\Api\V1\Transformers;
use Seldat\Wms2\Models\Storages;
use Seldat\Wms2\Utils\Status;
use League\Fractal\TransformerAbstract;

class StoragesTransformer extends TransformerAbstract
{
    public function transform(Storages $storages)
    {

        $values = $storages->toArray();
        $totalQty = $storages->sum_piece_remain;

        $ctn_pack_size = array_get($values, 'ctn_pack_size', NULL);
        
        $percarton =   round(($storages->length*$storages->width*$storages->height/config('constants.CUBE_CONVERT_2_FEET')),2);

        return [
            'whs_name'     => object_get($storages, 'Warehouse.whs_name', ''),
            'cus_name'     => $storages->cus_name,
            'sku'          => $storages->sku,
            'lot'          => $storages->lot,
            'ctn_sts'      => Status::getByValue($storages->ctn_sts, 'CTN_STATUS'),
            'upc'          => $storages->upc,
            'color'        => $storages->color,
            'size'         => $storages->size,
            'length'       => round($storages->length, 2),
            'width'        => round($storages->width, 2),
            'height'       => round($storages->height, 2),
            'carton_cube'  => $percarton,
            'weight'       => round($storages->weight, 1),
            'loc_name'     => $storages->loc_code,
            'sys_mea_name' => object_get($storages, 'AsnDtl.AsnHdr.SystemMeasurement.sys_mea_name', ''),
            'pack_size'    => $storages->ctn_pack_size,
            'UOM'          => $storages->uom_name,
            'ctns'         => round(($totalQty/$storages->ctn_pack_size),2),
            'qty'          => $totalQty,
            'total_CU'     => round(($percarton * $totalQty/$storages->ctn_pack_size),2),
        ]; 

    }

    /*
    ****************************************************************************
    */

}
