<?php

// Bypass middleware when testing
    $middleware = ['trimInput', 'setWarehouseTimezone'];
    if (env('APP_ENV') != 'testing') {
        $middleware[] = 'verifySecret';
        $middleware[] = 'authorize';
    }

$api->version('v2', ['middleware' => $middleware], function ($api) {
    // Assign Carton to Pallet
    $api->group(['prefix' => '/v2/putback', 'namespace' => 'App\Api\V2\Putback\AssignPallet\Controllers'], function ($api) {

        // Create carton and assign to pallet
        $api->post('/pal-assign/{returnId:[0-9]+}', [
            'action' => 'assignPalletPutback',
            'uses'   => 'ReturnHdrController@assignPallet'
        ]);

        $api->get('/{returnId:[0-9]+}/get-items', [
            'action' => 'assignPalletPutback',
            'uses'   => 'ReturnHdrController@getItem'
        ]);

        $api->get('/pal-assign/check-plt-rfid', [
            'action' => 'assignPalletPutback',
            'uses'   => 'ReturnHdrController@validationPalletRfid'
        ]);

        $api->get('/return-order-details/{returnId:[0-9]+}', [
            'action' => 'cancelOrder',
            'uses'   => 'ReturnOrderController@ReturnOrdersDetail'
        ]);

        $api->get('/{returnId:[0-9]+}', [
            'action' => 'updatePutback',
            'uses'   => 'ReturnOrderController@getUpdatePutBack'
        ]);

        $api->put('/{returnId:[0-9]+}', [
            'action' => 'updatePutback',
            'uses'   => 'ReturnOrderController@updatePutBack'
        ]);

        $api->get('/location-suggestion/cus-id/{cusId:[0-9]+}/pallet-type/{pltType}/spc-hdl-code/{spcHdlCode}', [
            'action' => 'updatePutback',
            'uses'   => 'ReturnOrderController@locationSuggestion'
        ]);

        $api->get('/{returnId:[0-9]+}/print', [
            'action' => 'assignPalletPutback',
            'uses'   => 'ReturnHdrController@printPutBack'
        ]);
    });
    /**
     * Created by PhpStorm.
     * User: vinhpham
     * Date: 9/26/17
     * Time: 11:16 AM
     */

    $api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}', 'namespace' => 'App\Api\V2\BOL\Controllers'],
        function ($api) {
        $api->get('/auto-bol/{orderId:[0-9]+}',
            [    'action' => 'viewOrder', 'uses' => 'BOLController@store']);
    });

    // Order
    $api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/order', 'namespace' => 'App\Api\V2\Order'], function ($api) {
        // cancel order
        $api->get('/cancel',
            [
                'action' => "cancelOrder",
                'uses'   => 'CancelOrder\Controllers\OrderHdrController@cancel'
            ]
        );

        $api->get('/cancel-rma',
            [
                'action' => "cancelOrder",
                'uses'   => 'CancelOrder\Controllers\OrderHdrController@cancelRMA'
            ]
        );

        // order label link
        $api->get('/{odrId:[0-9]+}/label-link',
            [
                'action' => "viewOrder",
                'uses'   => 'GetLabelLink\Controllers\LabelLinkController@getList'
            ]
        );

        // auto assign cartons to order
        $api->get('/wave/{wvId:[0-9]+}/auto-assign-cartons',
            [
                'action' => "viewOrder",
                'uses'   => 'AutoAssignCarton\Controllers\AutoAssignCartonController@autoAssign'
            ]
        );
    });

    // Bill of Ladings
    $api->group(['prefix' => '/v2/whs/{whsId:[0-9]+}/bol', 'namespace' => 'App\Api\V2\BillOfLadings'], function ($api) {
        $api->post('/create',
            [
                'action' => 'createBOL',
                'uses'   => 'CreateBOL\Controllers\BOLController@store'
            ]
        );

        // get master pallet list
        $api->get('/master-pallet-list/{shipId:[0-9]+}',
            [
                'action' => 'createBOL',
                'uses'   => 'MasterPalletList\Controllers\MasterPalletController@getList'
            ]
        );

        // update BOL
        $api->put('/{shipId:[0-9]+}',
            [
                'action' => 'editBOL',
                'uses'   => 'UpdateBOL\Controllers\BOLController@update'
            ]
        );

        $api->get('/{shipId:[0-9]+}/print',
            [
                'action' => 'viewBOL',
                'uses'   => 'PrintBOL\Controllers\BOLController@printBOL'
            ]
        );
    });

    $api->group(['prefix' => '/v2', 'namespace' => 'App\Api\V2\BillOfLadings'], function ($api) {
        $api->put('/upload-dms',[
            'action'    => 'editBOL',
            'uses'      => 'CreateBOL\Controllers\BOLController@uploadToDMS'
        ]);
    });

});
