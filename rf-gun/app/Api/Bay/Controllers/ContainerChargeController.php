<?php

namespace App\Api\Bay\Controllers;

use App\Api\Bay\Models\BayModel;
use App\Api\Bay\Models\ContainerChargeModel;
use App\Api\Bay\Models\ContainerModel;
use App\Api\Bay\Models\CustomerModel;
use App\Api\Bay\Transformers\ContainerChargeTransformer;
use App\Api\Bay\Validators\ContainerChargeReportValidator;
use App\Api\Bay\Validators\ContainerChargeValidator;
use Illuminate\Support\Facades\DB;
use Dingo\Api\Http\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Illuminate\Http\Response as IlluminateResponse;
use Wms2\UserInfo\Data;


/**
 * Class ContainerChargeController
 *
 * @package App\Api\Bay\Controllers
 */
class ContainerChargeController extends AbstractController
{

    protected $userId;
    /**
     * @var BayModel
     */
    protected $bayModel;
    /**
     * @var ContainerChargeModel
     */
    protected $ctnrChargeModel;
    /**
     * @var CustomerModel
     */
    protected $customerModel;
    /**
     * @var ContainerModel
     */
    protected $containerModel;

    /**
     * ContainerChargeController constructor.
     *
     * @param BayModel $bayModel
     * @param ContainerChargeModel $ctnrChargeModel
     * @param CustomerModel $customerModel
     * @param ContainerModel $containerModel
     */
    public function __construct(
        BayModel $bayModel,
        ContainerChargeModel $ctnrChargeModel,
        CustomerModel $customerModel,
        ContainerModel $containerModel
    ) {
        $this->bayModel = $bayModel;
        $this->ctnrChargeModel = $ctnrChargeModel;
        $this->customerModel = $customerModel;
        $this->containerModel = $containerModel;
        $this->userId = JWTUtil::getPayloadValue('jti') ? JWTUtil::getPayloadValue('jti') : 0;
    }

    /**
     * @param Request $request
     * @param ContainerChargeTransformer $ctnrChargeTransformer
     * @param ContainerChargeValidator $ctnrChargeValidator
     *
     * @return mixed
     */
    public function search(
        Request $request,
        ContainerChargeTransformer $ctnrChargeTransformer,
        ContainerChargeValidator $ctnrChargeValidator
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $ctnrChargeValidator->validate($input);

        try {

            $ctnrCharges = $this->ctnrChargeModel->search($input, [], array_get($input, 'limit', null));

            return $this->response->collection($ctnrCharges, $ctnrChargeTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $ccId
     * @param ContainerChargeTransformer $ctnrChargeTransformer
     *
     * @return mixed
     */
    public function show($ccId, ContainerChargeTransformer $ctnrChargeTransformer)
    {
        try {
            $ctnrCharges = $this->ctnrChargeModel->getFirstWhere([
                'cc_id' => $ccId,
                'cc_sts' => 'O'
            ]);

            if (!$ctnrCharges) {
                throw new \Exception(Message::get("BM017", "Container Charge"));
            }

            return $this->response->item($ctnrCharges, $ctnrChargeTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    public function repairData(){

    }


    /**
     * @param Request $request
     * @param ContainerChargeValidator $ctnrChargeValidator
     *
     * @return mixed
     * @throws \Exception
     */
    public function store(
        $bayId,
        Request $request,
        ContainerChargeValidator $ctnrChargeValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $input['bay_id'] = $bayId;
        $ctnrChargeValidator->validate($input);

        $bay = $this->bayModel->getBayByIds($bayId);
        if (empty($bay->bay_id)) {
            throw new \Exception(Message::get("BM017", "Bay"));
        }

        $customer = $this->customerModel->getCusByIds($input['cus_id']);
        if (empty($customer->cus_id)) {
            throw new \Exception(Message::get("BM017", "Customer"));
        }

        $ctnr = $this->containerModel->getCtnrByIds($input['ctnr_id']);
        if (empty($ctnr->ctnr_id)) {
            throw new \Exception(Message::get("BM017", "Container"));
        }

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        try {
            DB::beginTransaction();

            $params = [
                'start'     => date('Y-m-d H:i:m', strtotime($input['start'])),
                'temp'      => array_get($input, 'temp', null),
                'cc_sts'    => array_get($input, 'cc_sts', 'O'),
                'duration'  => (int)array_get($input, 'duration', 0),
                'bay_id'    => $bay->bay_id,
                'bay_name'  => $bay->bay_name,
                'cus_id'    => $customer->cus_id,
                'cus_name'  => $customer->cus_name,
                'ctnr_id'   => $ctnr->ctnr_id,
                'ctnr_num'  => $ctnr->ctnr_num,
                'whs_id'    => $currentWH
            ];

            $bay_sts = 'U';
            if (!empty($input['end'])) {
                $params['end'] = date('Y-m-d H:i:m', strtotime($input['end']));
            }

            $ctnrCharge = $this->ctnrChargeModel->create($params);

            if (array_get($input, 'cc_sts', null) === 'C') {
                $bay_sts = 'A';
            }
            $this->bayModel->refreshModel();
            $this->bayModel->updateWhere([
                'bay_sts' => $bay_sts
            ], ['bay_id' => $bayId]);

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'cc_id' => $ctnrCharge->cc_id
                ]
            ])->setStatusCode(Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $bayId
     * @param Request $request
     * @param BayValidator $bayValidator
     *
     * @return mixed
     */
    public function update(
        $ccId,
        Request $request,
        ContainerChargeValidator $ctnrChargeValidator
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        $ctnCharges = $this->ctnrChargeModel->getFirstWhere([
            'cc_id' => $ccId,
            'cc_sts' => 'O'
        ]);

        if (!$ctnCharges) {
            throw new \Exception(Message::get("BM017", "Container Charge"));
        }

        $input['bay_id'] = $ctnCharges->bay_id;
        $ctnrChargeValidator->validate($input);

        $bay = $this->bayModel->getBayByIds($ctnCharges->bay_id);
        if (empty($bay->bay_id)) {
            throw new \Exception(Message::get("BM017", "Bay"));
        }

        $customer = $this->customerModel->getCusByIds($input['cus_id']);
        if (empty($customer->cus_id)) {
            throw new \Exception(Message::get("BM017", "Customer"));
        }

        $ctnr = $this->containerModel->getCtnrByIds($input['ctnr_id']);
        if (empty($ctnr->ctnr_id)) {
            throw new \Exception(Message::get("BM017", "Container"));
        }

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $paramsUpdate = [
            'start'     => date('Y-m-d H:i:m', strtotime($input['start'])),
            'temp'      => array_get($input, 'temp', null),
            'cc_sts'    => array_get($input, 'cc_sts', 'O'),
            'duration'  => (int)array_get($input, 'duration', 0),
            'bay_id'    => $bay->bay_id,
            'bay_name'  => $bay->bay_name,
            'cus_id'    => $customer->cus_id,
            'cus_name'  => $customer->cus_name,
            'ctnr_id'   => $ctnr->ctnr_id,
            'ctnr_num'  => $ctnr->ctnr_num,
            'whs_id'    => $currentWH
        ];

        if (!empty($input['end'])) {
            $paramsUpdate['end'] = date('Y-m-d H:i:m', strtotime($input['end']));
        }

        try {
            DB::beginTransaction();

            // Update Bay Model
            $this->ctnrChargeModel->refreshModel();
            $this->ctnrChargeModel->updateWhere($paramsUpdate,['cc_id' => $ccId]);

            if (array_get($input, 'cc_sts', null) === 'C') {
                $this->bayModel->refreshModel();
                $this->bayModel->updateWhere([
                    'bay_sts' => 'A'
                ], ['bay_id' => $ctnCharges->bay_id]);

            }

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK',
                'data'   => [
                    'cc_id' => $ccId
                ]
            ])->setStatusCode(Response::HTTP_OK);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function reportContainer(
        Request $request,
        ContainerChargeTransformer $ctnrChargeTransformer,
        ContainerChargeReportValidator $ctnrChargeReportValidator
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        $ctnrChargeReportValidator->validate($input);

        try {

            $ctnrCharges = $this->ctnrChargeModel->report($input, [
                'bay',
                'customer',
                'warehouse'
            ], array_get($input, 'limit', 20));

            return $this->response->paginator($ctnrCharges, $ctnrChargeTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_RF_GUN, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}

