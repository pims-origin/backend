<?php
namespace tests\codeception\backend\api;

use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use tests\codeception\backend\ApiTester;

class UserCest
{
    protected $_user_id,
        $_token,
        $_user_id2,
        $_user_id3,
        $_user_id4;

    public function _before(ApiTester $I)
    {
        //$I->getDbh()->beginTransaction();
        $this->_user_id = $I->haveInDatabase('users', [
            'first_name' => 'fi12T',
            'last_name'  => 'l2sT',
            'email'      => 'test@codecept.com',
            'password'   => '$2y$13$dWhcDzI0Ry.D4sCxME0L3evfLqsjlpt.Ip0MHKoVP.qwECqrExqOa', //Seldat123
            'status'     => 'AC',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->_user_id2 = $I->haveInDatabase('users', [
            'first_name' => 'fi12T_2',
            'last_name'  => 'l2sT_2',
            'email'      => 'test2@codecept.com',
            'password'   => '$2y$13$dWhcDzI0Ry.D4sCxME0L3evfLqsjlpt.Ip0MHKoVP.qwECqrExqOa', //Seldat123
            'status'     => 'AC',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->_user_id3 = $I->haveInDatabase('users', [
            'first_name' => 'fi12T_3',
            'last_name'  => 'l2sT_3',
            'email'      => 'test3@codecept.com',
            'password'   => '$2y$13$dWhcDzI0Ry.D4sCxME0L3evfLqsjlpt.Ip0MHKoVP.qwECqrExqOa', //Seldat123
            'status'     => 'AC',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->_user_id4 = $I->haveInDatabase('users', [
            'first_name' => 'fi12T_4',
            'last_name'  => 'l2sT_4',
            'email'      => 'test4@codecept.com',
            'password'   => '$2y$13$dWhcDzI0Ry.D4sCxME0L3evfLqsjlpt.Ip0MHKoVP.qwECqrExqOa', //Seldat123
            'status'     => 'AC',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        //create warehouse
        $this->_whs_id = $I->haveInDatabase('warehouse', [
            'whs_name'       => 'testWarehouse',
            'whs_status'     => 'AC',
            'whs_short_name' => 'warehouseTestCreate',
            'whs_code'       => 'w71a271ouse',
            'whs_country_id' => '2',
            'whs_state_id'   => '1',
            'whs_city_name'  => 'Alabama',
        ]);
        //$this->_name1 = $I->grabFromDatabase('authority', 'name', ['name' => $name1]);

        //create customer
        $this->_cus_id = $I->haveInDatabase('customer', [
            'cus_name'            => 'testCustomer',
            'cus_status'          => 'AC',
            'cus_code'            => 'cu6T01113r',
            'cus_country_id'      => '2',
            'cus_state_id'        => '1',
            'cus_city_name'       => 'Alabama',
            'cus_postal_code'     => '123456',
            'cus_billing_account' => 'b1ll1119T3s7',
            'cus_des'             => 'DesTest',
        ]);

        //create user warehouse
        $I->haveInDatabase('user_whs', [
            'user_id' => $this->_user_id,
            'whs_id'  => $this->_whs_id
        ]);

        //create user customer
        $I->haveInDatabase('cus_in_user', [
            'cus_id'  => $this->_cus_id,
            'user_id' => $this->_user_id,
            'whs_id'  => $this->_whs_id
        ]);

    }

    public function _after(ApiTester $I)
    {
        //$I->getDbh()->rollBack();
        //$I->deleteFromDatabase('users', ['user_id' => $this->_user_id]);
    }

    /**
     * Test login - ok
     *
     * @param ApiTester $I
     *
     * @return string
     */
    public function test_login_ok(ApiTester $I)
    {
        $I->sendPOST(
            "users/login",
            ['email' => 'test@codecept.com', 'password' => 'Seldat123']
        );
        $I->seeResponseCodeIs(200);

        $res = $I->grabResponse(); //view response
        return $res;
        //var_dump($this->_user_id);
        //$this->_token = \GuzzleHttp\json_decode($res)->data->token;
    }

    /**
     * Test login  - password not right - fail
     *
     * @param ApiTester $I
     *
     * @return string
     */
    public function test_login_passwordNotRight_Fail(ApiTester $I)
    {
        $I->sendPOST(
            "users/login",
            ['email' => 'test@codecept.com', 'password' => 'Seldat1234']
        );
        $I->seeResponseCodeIs(400);
    }

    /**
     * Test login  - password not right - fail
     *
     * @param ApiTester $I
     *
     * @return string
     */
    public function test_login_emailNotRight_Fail(ApiTester $I)
    {
        $I->sendPOST(
            "users/login",
            ['email' => 'test@codecept.coms', 'password' => 'Seldat123']
        );
        $I->seeResponseCodeIs(400);
    }

    /**
     * Test get token - ok
     *
     * @param ApiTester $I
     */
    public function test_getToken_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/token");
        $I->seeResponseCodeIs(200);
    }

    /**
     * Test logout - ok
     *
     * @param ApiTester $I
     */
    public function test_logout_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/logout");
        $I->seeResponseCodeIs(200);
    }

    /**
     * Test logout - fail
     *
     * @param ApiTester $I
     */
    public function test_logout_Fail(ApiTester $I)
    {
        $res = self::test_login_ok($I);
        $this->_token = \GuzzleHttp\json_decode($res)->data->token;

        $I->amBearerAuthenticated($this->_token . "-f");
        $I->sendPOST("users/logout");
        $I->seeResponseCodeIs(401);
    }

    /**
     * Test get info - ok
     *
     * @param ApiTester $I
     */
    public function test_info_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendGET("users/info/" . $this->_user_id);
        $I->seeResponseCodeIs(200);
    }

    /**
     * Test get user by id - ok
     *
     * @param ApiTester $I
     */
    public function test_getUserById_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/get-user-by-ids", [
            'user_ids' => $this->_user_id
        ]);
        $I->seeResponseCodeIs(200);
    }

    /**
     * Test get user by id - fail
     *
     * @param ApiTester $I
     */
    public function test_getUserById_fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/get-user-by-ids", [
            'user_ids' => '9999999999999999999'
        ]);
        $I->seeResponseCodeIs(400);
    }

    /**
     * Test update password - ok
     *
     * @param ApiTester $I
     */
    public function test_updatePassword_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/update-password", [
            'user_id'             => $this->_user_id,
            'new_password'        => '123456aA',
            'repeat_new_password' => '123456aA'
        ]);
        $I->seeResponseCodeIs(200);
    }

    /**
     * Test update password - repeat new password not right - fail
     *
     * @param ApiTester $I
     */
    public function test_updatePassword_repeatPassNotRight_fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/update-password", [
            'user_id'             => $this->_user_id,
            'new_password'        => '123456aA',
            'repeat_new_password' => '123456aAa'
        ]);
        $I->seeResponseCodeIs(422);
    }

    /**
     * Test update password - userId not right - fail
     *
     * @param ApiTester $I
     */
    public function test_updatePassword_userIdNotRight_Fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/update-password", [
            'user_id'             => '999999',
            'new_password'        => '123456aA',
            'repeat_new_password' => '123456aA'
        ]);
        $I->seeResponseCodeIs(400);
    }

    /**
     * Test change password - ok
     *
     * @param ApiTester $I
     */
    public function test_changePassword_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/change-password", [
            'user_id'             => $this->_user_id,
            'old_password'        => 'Seldat123',
            'new_password'        => '123456aA',
            'repeat_new_password' => '123456aA'
        ]);
        $I->seeResponseCodeIs(200);
    }

    /**
     * Test change password - old password not right - fail
     *
     * @param ApiTester $I
     */
    public function test_changePassword_oldPassNotRight_fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/change-password", [
            'user_id'             => $this->_user_id,
            'old_password'        => 'Sheldat1vgcfc23ahdfh',
            'new_password'        => '123456aA',
            'repeat_new_password' => '123456aA'
        ]);
        $I->seeResponseCodeIs(422);
    }

    /**
     * Test change password - new password not right - fail
     *
     * @param ApiTester $I
     */
    public function test_changePassword_newPasswordNotRight_Fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/change-password", [
            'user_id'             => $this->_user_id,
            'old_password'        => 'Seldat123',
            'new_password'        => '123456aAadfd',
            'repeat_new_password' => '123456aA'
        ]);
        $I->seeResponseCodeIs(422);
    }

    /**
     * Test index - ok
     *
     * @param ApiTester $I
     */
    public function test_index_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendGET("users");
        $I->seeResponseCodeIs(200);
    }

    /**
     * Test view - ok
     *
     * @param ApiTester $I
     */
    public function test_view_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendGET("users/" . $this->_user_id);
        $I->seeResponseCodeIs(200);
    }

    /**
     * Test view - fail
     *
     * @param ApiTester $I
     */
    public function test_view_fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendGET("users/9999999999999");
        $I->seeResponseCodeIs(404);
    }

    /**
     * Test delete multiple - ok
     *
     * @param ApiTester $I
     */
    public function test_deleteMultiple_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDELETE("users/multiple", [
            'user_ids' => $this->_user_id2 . ',' . $this->_user_id3 . ',' . $this->_user_id4
        ]);

        $I->seeResponseCodeIs(200);
    }

    /**
     * Test update - ok
     *
     * @param ApiTester $I
     */
    public function test_update_ok(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPUT("users/update/" . $this->_user_id, [
            'email'           => 'testupdate@codecept.com',
            'status'          => 'AC',
            'first_name'      => 'test_first_name',
            'last_name'       => 'test_last_name',
            'phone'           => '0987654321',
            'status'          => 'AC',
            'off_loc'         => '3/2 street',
            'dept'            => 'district 14',
            'phone'           => '09812345678',
            'phone_extend'    => '123',
            'mobile'          => '0987654321',
            'user_warehouses' => [
                [
                    'whs_id' => $this->_whs_id
                ]
            ],
            'user_customers'  => [
                [
                    'whs_id' => $this->_whs_id,
                    'cus_id' => $this->_cus_id
                ]
            ],
            'user_roles'      => [
                'role_name' => 'Admin'
            ],
        ]);

        $I->seeResponseCodeIs(200);
    }

    /**
     * Test update - fail
     *
     * @param ApiTester $I
     */
    public function test_update_emailNotRight_fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPUT("users/update/" . $this->_user_id, [
            'email'           => 'testupdate@codecept',
            'status'          => 'AC',
            'first_name'      => 'test_first_name',
            'last_name'       => 'test_last_name',
            'phone'           => '0987654321',
            'status'          => 'AC',
            'off_loc'         => '3/2 street',
            'dept'            => 'district 14',
            'phone'           => '09812345678',
            'phone_extend'    => '123',
            'mobile'          => '0987654321',
            'user_warehouses' => [
                [
                    'whs_id' => $this->_whs_id
                ]
            ],
            'user_customers'  => [
                [
                    'whs_id' => $this->_whs_id,
                    'cus_id' => $this->_cus_id
                ]
            ],
            'user_roles'      => [
                'role_name' => 'Admin'
            ],
        ]);

        $I->seeResponseCodeIs(400);
    }

    /**
     * Test update status not right
     *
     * @param ApiTester $I
     */
    public function test_update_statusNotRight_Fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPUT("users/update/" . $this->_user_id, [
            'email'           => 'testupdate@codecept.com',
            'status'          => 'AAAAACC',
            'first_name'      => 'test_first_name',
            'last_name'       => 'test_last_name',
            'phone'           => '0987654321',
            'status'          => 'dfsdfsdfAC',
            'off_loc'         => '3/2 street',
            'dept'            => 'district 14',
            'phone'           => '09812345678',
            'phone_extend'    => '123',
            'mobile'          => '0987654321',
            'user_warehouses' => [
                [
                    'whs_id' => $this->_whs_id
                ]
            ],
            'user_customers'  => [
                [
                    'whs_id' => $this->_whs_id,
                    'cus_id' => $this->_cus_id
                ]
            ],
            'user_roles'      => [
                'role_name' => 'Admin'
            ],
        ]);

        $I->seeResponseCodeIs(400);
    }

    /**
     * Common function: User Login
     *
     * @param ApiTester $I
     */
    public function userLogin(ApiTester $I)
    {
        $res = self::test_login_ok($I);
        $this->_token = \GuzzleHttp\json_decode($res)->data->token;

        $I->amBearerAuthenticated($this->_token);
    }

    public function test_create_ok(ApiTester $I)
    {
        self::userLogin($I);

        $I->sendPOST("users/signup", [
            'first_name'      => 'f1rs1 112111e',
            'last_name'       => 'L2s1 112111e',
            'email'           => 'testcreate@codecept.com',
            'status'          => 'AC',
            'off_loc'         => '3/2 street',
            'dept'            => 'district 14',
            'phone'           => '09812345678',
            'phone_extend'    => '123',
            'mobile'          => '0987654321',
            'user_warehouses' => [
                [
                    'whs_id' => $this->_whs_id
                ]
            ],
            'user_customers'  => [
                [
                    'whs_id' => $this->_whs_id,
                    'cus_id' => $this->_cus_id
                ]
            ],
            'user_roles'      => [
                'role_name' => 'Admin'
            ],
        ]);
        $I->seeResponseCodeIs(201);
        $I->deleteFromDatabase('users', ['email' => 'testcreate@codecept.com']);
    }

    /**
     * Test create - fail
     *
     * @param ApiTester $I
     */
    public function test_create_emailNotRight_fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/signup", [
            'email'           => 'testupdate@codecept',
            'status'          => 'AC',
            'first_name'      => 'test_first_name',
            'last_name'       => 'test_last_name',
            'phone'           => '0987654321',
            'status'          => 'AC',
            'off_loc'         => '3/2 street',
            'dept'            => 'district 14',
            'phone'           => '09812345678',
            'phone_extend'    => '123',
            'mobile'          => '0987654321',
            'user_warehouses' => [
                [
                    'whs_id' => $this->_whs_id
                ]
            ],
            'user_customers'  => [
                [
                    'whs_id' => $this->_whs_id,
                    'cus_id' => $this->_cus_id
                ]
            ],
            'user_roles'      => [
                'role_name' => 'Admin'
            ],
        ]);

        $I->seeResponseCodeIs(400);
    }

    /**
     * Test create status not right
     *
     * @param ApiTester $I
     */
    public function test_create_statusNotRight_Fail(ApiTester $I)
    {
        self::userLogin($I);
        $I->sendPOST("users/signup", [
            'email'           => 'testupdate@codecept.com',
            'status'          => 'AAAAACC',
            'first_name'      => 'test_first_name',
            'last_name'       => 'test_last_name',
            'phone'           => '0987654321',
            'status'          => 'dfsdfsdfAC',
            'off_loc'         => '3/2 street',
            'dept'            => 'district 14',
            'phone'           => '09812345678',
            'phone_extend'    => '123',
            'mobile'          => '0987654321',
            'user_warehouses' => [
                [
                    'whs_id' => $this->_whs_id
                ]
            ],
            'user_customers'  => [
                [
                    'whs_id' => $this->_whs_id,
                    'cus_id' => $this->_cus_id
                ]
            ],
            'user_roles'      => [
                'role_name' => 'Admin'
            ],
        ]);

        $I->seeResponseCodeIs(400);
    }
}
