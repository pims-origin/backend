<?php
namespace App\Api\V1\Models;

use Psr\Http\Message\ServerRequestInterface as IRequest;

class ItemService extends BaseService
{
    /**
     * ItemService constructor.
     * @param IRequest $request
     * @param string $baseUrl
     */
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_ITEM_MASTER');
        parent::__construct($request, $api);
    }

    /**
     * @param $queryStr
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getList($queryStr)
    {
        $uri = $this->getUri();
        $uri = sprintf('%s?%s', $uri, $queryStr);
        return $this->client->get($uri);
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDetail($id)
    {
        $uri = $this->getUri() . $id;
        return $this->client->get($uri);
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getItemById($id)
    {
        $uri = $this->getUri() . $id . '/detail';
        return $this->client->get($uri);
    }

    /**
     * @param $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create($params)
    {
        $uri = $this->getUri();

        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    /**
     * @param $params
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function edit($params, $id)
    {
        $uri = $this->getUri() . $id;

        $options = [
            'form_params' => $params
        ];

        return $this->client->put($uri, $options);
    }

    /**
     * @return string
     */
    protected function getUri()
    {
        return env('API_ITEM_MASTER') . 'items/';
    }

    /**
     * @param $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function check($params)
    {
        $uri = $this->getUri().'check-items';

        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    /**
     * @param $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function import($params)
    {
        try {
            $uri = $this->getUri().'import';
            $ext = !empty($params['file']) ? $_FILES['file']['name'] : "";
            $options = [
                [
                    'name'     => 'cus_id',
                    'contents' => empty($params['cus_id']) ? "" : $params['cus_id']
                ],
                [
                    'name'     => 'file',
                    'contents' => empty($params['file']) ? null : fopen($params['file']->getPathName(), 'rb')
                ],
                [
                    'name'     => 'ext',
                    'contents' => $ext
                ]
            ];

            return $this->client->post($uri, ['multipart' => $options]);

        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            return false;
        }
    }

    //Item packing process
    public function createItemPacking($params)
    {
        $uri = 'item-packing';

        $options = [
            'form_params' => $params
        ];

        return $this->client->post($uri, $options);
    }

    public function showItemPacking($itemId)
    {
        $uri = 'item-packing/' . $itemId;

        return $this->client->get($uri);
    }

    /**
     * [export description]
     * @param  [type] $queryStr [description]
     * @return [type]           [description]
     */
    public function export($queryStr)
    {
        $uri = env('API_ITEM_MASTER') . 'item-export-excel';
        $uri = sprintf('%s?%s', $uri, $queryStr);
        return $this->client->get($uri);
    }


    public function getItemUom()
    {
        $uri = 'item-uom';

        return $this->client->get($uri);
    }

    public function getChargeBy()
    {
        $uri = 'item-charge-by';

        return $this->client->get($uri);
    }

}
