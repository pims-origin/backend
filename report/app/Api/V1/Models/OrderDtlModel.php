<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\SelArr;

class OrderDtlModel extends AbstractModel
{
    public function __construct(OrderDtl $model = NULL)
    {
        $this->model = ($model) ?: new OrderDtl();
    }

    /*
    ****************************************************************************
    */

    public function searchOnlineOrders($attributes=[], $with=[], $limit=PAGING_LIMIT)
    {
        $query = $this->make($with)
            ->join('odr_hdr', 'odr_hdr.odr_id', '=', 'odr_dtl.odr_id')
            ->where('odr_dtl.whs_id', $attributes['whs_id'])
            ->where('is_ecom', 1);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $attributes['compare_operator']['cus_odr_num'] = 'like';
        $attributes['compare_operator']['odr_num'] = 'like';
        $attributes['compare_operator']['cus_po'] = 'like';
        $attributes['compare_operator']['sku'] = 'like';

        $this->addSearches($query, [
            'searchAttr' => $searchAttr,
            'searches' => [
                'cus_id',
                'sku',
                'cus_odr_num',
                'odr_num',
                'cus_po',
                'odr_req_dt',
            ],
            'attributes' => $attributes,
            'tablePreface' => [
                'cus_id' => NULL,
            ],
        ]);

        return $this->runSearch([
            'query' => $query,
            'attributes' => $attributes,
            'searchAttr' => $searchAttr,
            'limit' => $limit,
        ]);
    }

    /*
    ***************************************************************************
    */

}
