<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackHdrModel;
use Dingo\Api\Exception\UnknownVersionException;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;

/**
 * Class PackHdrController
 *
 * @package App\Api\V1\Controllers
 */
class PackHdrController extends AbstractController
{
    protected $packHdrModel;
    protected $orderDtlModel;
    protected $cartonModel;
    protected $inventorySummaryModel;

    public function __construct() {
        $this->packHdrModel = new PackHdrModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->cartonModel = new CartonModel();
        $this->inventorySummaryModel = new InventorySummaryModel();
    }

    public function autoStore($orderHdrId) {
        $odrHdrModel = new OrderHdrModel();
        $odrHdr = $odrHdrModel->getFirstBy('odr_id', $orderHdrId);
        if(!$odrHdr) {
            throw new UnknownVersionException("The Order {$orderHdrId} is not existed!");
        }

        if($odrHdr->odr_sts != 'PD') {
            throw new UnknownVersionException("Only Picked Orders can be Packed!");
        }

        $odrDtls = $this->orderDtlModel->allBy('odr_id', $orderHdrId, ['item', 'systemUom']);
        $kittingOdrDtls = $odrDtls->filter(function($value, $key) {
            return $value->is_kitting == 1;
        });
        $pickingOdrDtls = $odrDtls->filter(function($value, $key) {
            return $value->is_kitting == 0;
        });
        try {
            DB::beginTransaction();

            $this->cartonModel->cloneKittingCartons($kittingOdrDtls);

            $this->cartonModel->updatePickingCartons($pickingOdrDtls);

            $this->inventorySummaryModel->updatePickingInventory($pickingOdrDtls);

            $this->packHdrModel->updatePackedStatus($orderHdrId);

            DB::commit();

            return $this->response->noContent()->setContent([
                'status' => 'OK'
            ])->setStatusCode
            (Response::HTTP_CREATED);

        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
