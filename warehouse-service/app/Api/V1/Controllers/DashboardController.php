<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\DashboardService;
use Psr\Http\Message\ServerRequestInterface as IRequest;


class DashboardController extends AbstractController
{
    /**
     * @var DashboardService
     */
    protected $service;

    /**
     * DashboardController constructor.
     *
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new DashboardService($request);
    }


    public function asnDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->asnDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function odrDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->odrDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function inprocessDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->inprocessDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function asnAdminDashboard(IRequest $request)
    {

        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->asnAdminDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function OccupancyPCTDashboard(IRequest $request)
    {

        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->OccupancyPCTDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function occupancyPCTDashboardStatus(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->occupancyPCTDashboardStatus($queryStr);
        $response = $this->convertResponse($result);
        return $response;
    }

    public function odrAdminDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->odrAdminDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function inprocessAdminDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->inprocessAdminDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function putawayDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->putawayDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function productAdminDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->productAdminDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }
    public function putawayAdminDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->putawayAdminDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function wavepickDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->wavepickDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function goodReceiptDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->goodReceiptDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

	public function putawayReceivedDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->putawayReceivedDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function zoneCapaticyAdminDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result   = $this->service->zoneCapaticyAdminDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function wavePickStatisticDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->wavePickStatisticDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function roomAdminDashboard(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->roomAdminDashboard($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    public function dashboardAlertBar(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->dashboardAlertBar($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }
}