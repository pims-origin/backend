<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\CycleCountNotification;

class CycleCountNotificationModel extends AbstractModel
{
    /**
     * @param CycleCountNotification $model
     */
    public function __construct(CycleCountNotification $model = null)
    {
        $this->model = ($model) ?: new CycleCountNotification();
    }

    public function getForDashboard($cus_ids, $whsId)
    {
        $result = $this->model
            ->where('deleted', 0)
            ->whereIn('cus_id', $cus_ids)
            ->where('whs_id', $whsId)
            ->where('cc_ntf_sts', 'NW')
            ->count();
        return $result;
    }
}