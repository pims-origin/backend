<?php
/**
 *
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\EventTrackingModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\JWTUtil;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\DB;


class LocationController extends AbstractController
{
    protected $userId;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    public function __construct(
        EventTrackingModel $eventTrackingModel
    )
    {
        $this->userId = JWTUtil::getPayloadValue('jti') ? JWTUtil::getPayloadValue('jti') : 0;
        $this->eventTrackingModel = $eventTrackingModel;
    }

    public function relocation($whsId, Request $request)
    {
        $input = $request->getParsedBody();
        $input = $input['codes'];
        $totalArr = count($input) / 2;
        if ($totalArr !== intval($totalArr)) {
            return $this->response->errorBadRequest('From location not map with to location');
        }

        try {
            DB::beginTransaction();
            for ($i = 0; $i < $totalArr * 2; $i = $i + 2) {
                $fromLoc = $input[$i];
                $toLoc = $input[$i + 1];

                $rs = \DB::table('location')
                    ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
                    ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                    ->whereIn('location.loc_code', [$fromLoc, $toLoc])
                    ->where('location.loc_whs_id', $whsId)
                    ->where('location.loc_sts_code', '=', 'AC')
                    ->where('location.deleted', '=', 0)
                    ->where('loc_type.loc_type_code', 'RAC')
                    ->get(['location.loc_id', 'location.loc_code', 'pallet.plt_id', 'pallet.cus_id']);

                if (count($rs) != 2) {
                    return $this->response->errorBadRequest("$fromLoc , $toLoc Invalid");
                }

                $fromId = $toId = null;
                $cus_id = null;
                foreach ($rs as $rw) {
                    $cus_id = $cus_id ? $cus_id : $rw['cus_id'];
                    if ($rw['loc_code'] === $fromLoc) {
                        $fromId = $rw['loc_id'];
                        if (empty($rw['plt_id'])) {
                            return $this->response->errorBadRequest("From location: '$fromLoc' has to not empty");
                        }
                    }

                    if ($rw['loc_code'] === $toLoc) {
                        $toId = $rw['loc_id'];
                        if (!empty($rw['plt_id'])) {
                            return $this->response->errorBadRequest("To location: '$toLoc' has to empty");
                        }
                    }
                }

                \DB::table('pallet')->where('loc_id', $fromId)->update([
                    'loc_id'   => $toId,
                    'loc_code' => $fromLoc,
                    'loc_name' => $fromLoc
                ]);
                \DB::table('cartons')->where('loc_id', $fromId)->update([
                    'loc_id'   => $toId,
                    'loc_code' => $toLoc,
                    'loc_name' => $toLoc
                ]);

                $event = [
                    'whs_id'    => $whsId,
                    'cus_id'    => $cus_id,
                    'owner'     => $fromLoc,
                    'evt_code'  => 'RFR',
                    'trans_num' => 'RFG',
                    'info'      => sprintf("RF Gun - Update Relocate from %s to %s", $fromLoc, $toLoc),
                ];

                //  Evt tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create($event);

                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return ['data' => "Relocate successfully"];

    }
}