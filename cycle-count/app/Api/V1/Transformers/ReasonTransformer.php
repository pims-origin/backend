<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\BlockRsn;

class ReasonTransformer extends TransformerAbstract
{
    /**
     * @param BlockRsn $reason
     * @return array
     */

    public function transform(BlockRsn $reason)
    {
        return [
            'block_rsn_id'    => $reason->block_rsn_id,
            'block_rsn_name'  => $reason->block_rsn_name,
            'whs_id'          => $reason->whs_id,
            'whs_name'        => object_get($reason, 'warehouse.whs_name'),
            'reason_num'      => $reason->reason_num,
            'block_rsn_des'   => $reason->block_rsn_des,
            'create_by'       =>
                object_get($reason, 'createdBy.first_name'). ' ' . object_get($reason, 'createdBy.last_name'),
            'created_at'      => $reason->created_at
        ];

    }
}
