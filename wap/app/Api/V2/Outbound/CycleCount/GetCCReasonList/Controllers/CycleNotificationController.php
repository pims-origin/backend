<?php

namespace App\Api\V2\Outbound\CycleCount\GetCCReasonList\Controllers;

use App\Api\V2\Outbound\CycleCount\GetCCReasonList\Models\CycleCountNotificationModel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Support\Collection;
use Illuminate\Http\Response as IlluminateResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Api\V1\Models\Log;

class CycleNotificationController extends AbstractController
{

    /**
     * @var CycleCountNotificationModel
     */
    protected $cycleCountNotificationModel;

    /**
     * CycleNotificationController constructor.
     *
     */
    public function __construct() {
        $this->cycleCountNotificationModel = new CycleCountNotificationModel();
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function getReasonDropDown($whsId, Request $request) {
        $input = $request->getQueryParams();

        /*
         * start logs
         */

        $url = "/v2/whs/{$whsId}/ob/cycle-count-reason";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code' => 'CCR',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get cycle count reason list'
        ]);

        /*
         * end logs
         */

        try {
            $reasonDropDownInfo = $this->cycleCountNotificationModel->getReasonDropDown($input, array_get($input, 'limit'));

            $msg = [];
            $msg['status'] = true;
            $msg['iat'] = time();

            $msg['messages'] = [[
                'status_code' => 1,
                'msg' => "Successfully!"
            ]];
            $msg['data']   = $reasonDropDownInfo;

            return $msg;

        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return $this->_responseErrorMessage($e->getMessage());
        }

    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }

}
