<?php

namespace Seldat\Wms2\Models;

class Invt extends BaseModel
{
    protected $table = 'inventory';

    protected $primaryKey = 'id';

    protected $fillable = [
        'whs_id',
        'item_id',
        'cus_id',
        'whs_code',
        'cus_code',
        'type',
        'upc',
        'des',
        'sku',
        'size',
        'color',
        'pack',
        'uom',
        'in_hand_qty',
        'in_pick_qty',
        'total_qty',
        'created_at',
        'updated_at',

    ];

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

}
