<?php

namespace App\Api\V1\Models;

use App\OrderCarton;
use Illuminate\Support\Facades\DB;

class OrderCartonModel extends AbstractModel
{

    protected $model;

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    public function getCartonByOdrDtl(array $odrDtlId)
    {

        $result = DB::table('odr_cartons as oc')
                         ->join("cartons as c", "oc.ctn_id", "=", "c.ctn_id")
                        ->select("oc.*", "c.width", "c.height", "c.length", 'c.whs_id', 'c.cus_id')
                        ->whereIn('oc.odr_dtl_id', $odrDtlId)
                        ->where('oc.ctn_sts', 'PD')
                        ->orderBy("oc.item_id")->get();
        return $result;
    }

    public function checkPickPiece($odrDtlIds)
    {
        $query = $this->make([]);

        $count = $query->whereIn('odr_dtl_id', $odrDtlIds)->whereRaw('`pack` <> `piece_qty`')->select('odr_ctn_id')
            ->count();

        return $count == 0;
    }
}
