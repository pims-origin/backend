<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Inventory;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class InventoryModel extends AbstractModel
{
    /**
     * @var GoodsReceiptDetailModel
     */
    protected $grDtlModel;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new Inventory();
        $this->grDtlModel = new GoodsReceiptDetailModel();
    }

    public function updateInvent($goodsReceiptId, $whsId) {
        $dataInventoryNotExsit =  DB::select(DB::raw("SELECT * FROM (
                                                SELECT
                                                w.whs_id ,
                                                i.`item_id`,
                                                i.`cus_id`,
                                                w.`whs_code`,
                                                c.`cus_code`,
                                                'I' AS `type`,
                                                i.cus_upc,
                                                i.`description`,
                                                i.`sku`,
                                                i.`size`,
                                                i.`color`,
                                                i.`pack`,
                                                'PC' AS `uom`,
                                                0 AS `in_hand_qty` ,
                                                0 AS `in_pick_qty` ,
                                                i.`created_at` ,
                                                i.`updated_at`
                                                FROM
                                                item i
                                                JOIN customer c ON i.cus_id = c.cus_id
                                                JOIN warehouse w ON w.whs_id = $whsId
                                                LEFT JOIN inventory iv ON iv.whs_id = w.whs_id AND iv.item_id = i.item_id
                                                WHERE iv.item_id IS NULL) as inven"));


        if ($dataInventoryNotExsit) {
            DB::select(DB::raw("INSERT INTO inventory
                                                                    (`whs_id` ,
                                                                    `item_id`,
                                                                    `cus_id`,
                                                                    `whs_code`,
                                                                    `cus_code`,
                                                                    `type`,
                                                                    `upc`,
                                                                    `des`,
                                                                    `sku`,
                                                                    `size`,
                                                                    `color`,
                                                                    `pack`,
                                                                    `uom`,
                                                                    `in_hand_qty` ,
                                                                    `in_pick_qty` ,
                                                                    `created_at` ,
                                                                    `updated_at`)
                                                                    (SELECT
                                                                    w.whs_id ,
                                                                    i.`item_id`,
                                                                    i.`cus_id`,
                                                                    w.`whs_code`,
                                                                    c.`cus_code`,
                                                                    'I' AS `type`,
                                                                    i.cus_upc,
                                                                    i.`description`,
                                                                    i.`sku`,
                                                                    i.`size`,
                                                                    i.`color`,
                                                                    i.`pack`,
                                                                    'PC' AS `uom`,
                                                                    0 AS `in_hand_qty` ,
                                                                    0 AS `in_pick_qty` ,
                                                                    i.`created_at` ,
                                                                    i.`updated_at`
                                                                    FROM
                                                                    item i
                                                                    JOIN customer c ON i.cus_id = c.cus_id
                                                                    JOIN warehouse w ON w.whs_id = $whsId
                                                                    LEFT JOIN inventory iv ON iv.whs_id = w.whs_id AND iv.item_id = i.item_id
                                                                    WHERE iv.item_id IS NULL)"));
        }

        $dataInventoryExsit =  DB::select(DB::raw("SELECT * FROM (
                                                SELECT
                                                w.whs_id ,
                                                i.`item_id`,
                                                i.`cus_id`,
                                                w.`whs_code`,
                                                c.`cus_code`,
                                                'I' AS `type`,
                                                i.cus_upc,
                                                i.`description`,
                                                i.`sku`,
                                                i.`size`,
                                                i.`color`,
                                                i.`pack`,
                                                'PC' AS `uom`,
                                                0 AS `in_hand_qty` ,
                                                0 AS `in_pick_qty` ,
                                                i.`created_at` ,
                                                i.`updated_at`
                                                FROM
                                                item i
                                                JOIN customer c ON i.cus_id = c.cus_id
                                                JOIN warehouse w ON w.whs_id = $whsId
                                                LEFT JOIN inventory iv ON iv.whs_id = w.whs_id AND iv.item_id = i.item_id
                                                WHERE iv.item_id IS NOT NULL) as inven"));

        if ($dataInventoryExsit){
            DB::select(DB::raw("UPDATE inventory s
                                                JOIN (
                                                SELECT
                                                    h.whs_id,
                                                    d.item_id,
                                                    SUM( d.gr_dtl_act_ctn_ttl * d.pack) AS qty
                                                FROM
                                                gr_dtl d
                                                JOIN gr_hdr h ON h.gr_hdr_id = d.gr_hdr_id
                                                    WHERE h.gr_hdr_id = $goodsReceiptId
                                                                AND h.gr_sts = 'RE'
                                                                AND d.gr_dtl_sts = 'RE'
                                                                AND d.deleted = 0
                                                                AND d.gr_dtl_act_ctn_ttl > 0
                                                GROUP BY h.whs_id, d.item_id) AS t
                                                ON t.whs_id = s.whs_id AND s.item_id = t.item_id
                                                SET s.in_hand_qty = s.in_hand_qty + t.qty
                                                where s.type = 'I'
                                                "));
        }

    }
}
