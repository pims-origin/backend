<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Shipment;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class ShipmentModel
 *
 * @package App\Api\V1\Models
 */
class ShipmentModel extends AbstractModel
{
    /**
     * @var Shipment
     */
    protected $model;

    /**
     * ShipmentModel constructor.
     *
     * @param Shipment|null $model
     */
    public function __construct(Shipment $model = null)
    {
        $this->model = ($model) ?: new Shipment();
    }

    /**
     * @param $orderShipmentId
     *
     * @return mixed
     */
    public function deleteShipment($orderShipmentId)
    {
        return $this->model
            ->where('ord_shipping_id', $orderShipmentId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrLike = [
            'ship_to_cus_name',
            'ship_to_addr',
            'ship_to_city',
            'ship_to_state',
            'ship_to_zip',
            'ship_to_country'
        ];

        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $orderHdrId
     * @param $itemIds
     * @param array $with
     *
     * @return mixed
     */
    public function loadByShpIds($shpMntIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('ship_id', $shpMntIds);
        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }

    public function getBOLList($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $arrEqual = [
            'cus_id'   => array_get($attributes, 'cus_id', null),
            'bo_num'   => array_get($attributes, 'bol_num', null),
            'bo_label' => array_get($attributes, 'bol_label', null)
        ];

        $arrChangeKeySorts = [
            'ship_sts_name' => 'ship_sts',
            'ship_to'       => 'ship_to_addr_1',
            'bol_label'     => 'bo_label',
            'bol_num'       => 'bo_num'
        ];

        $arrOdrIds = array_get($attributes, 'full_add', null);
        $ids = explode(',', $arrOdrIds);
        foreach ($attributes as $key => $value) {
            switch ($key) {
                case 'cus_id':
                    $query->where('cus_id', $arrEqual['cus_id']);
                    break;
                case 'bol_num':
                    $query->where('bo_num', 'like', '%' . $arrEqual['bo_num'] . '%');
                    break;
                case 'bol_label':
                    $query->where('bo_label', 'like', '%' . $arrEqual['bo_label'] . '%');
                    break;
                case 'full_add':
                    $query->whereHas('odr_hdr', function ($query) use ($ids) {
                        $query->whereIn('odr_id', $ids);
                    });
                    break;
                default:
                    break;
            }
        }

        //  Change key sort
        if (!empty($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $value) {
                if (array_key_exists($key, $arrChangeKeySorts)) {
                    unset($attributes['sort'][$key]);
                    $attributes['sort'][$arrChangeKeySorts[$key]] = $value;
                }
            }
        }

        $this->model->filterData($query, true);

        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @return string
     */
    public function getBOLNum()
    {
        $order = $this->model
            ->orderBy('ship_id', 'desc')
            ->first();
        $index = 0;
        $boNum = object_get($order, "bo_num", null);

        if ($boNum) {
            $temp = explode("-", $boNum);
            $index = (int)end($temp);
        }

        return 'BOL-' . (date("ym", time())) . "-" . str_pad(++$index, 5, "0", STR_PAD_LEFT);
    }

    /**
     * @param $shipId
     *
     * @return mixed
     */
    public function getBolLpn($shipId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = Shipment::select(
            [
                'shipment.cus_id',
                'customer.cus_name',
                'shipment.ship_to_name',
                'shipment.whs_id',
                'shipment.bo_num',
                'shipment.ship_id',
            ]
        )
            ->join('customer', 'shipment.cus_id', '=', 'customer.cus_id')
            ->where('shipment.ship_id', $shipId)
            ->where('shipment.deleted', 0)
            ->distinct();

        $result = $query->first();

        return $result;
    }


    /**
     * @param $shipId
     *
     * @return mixed
     */
    public function getOrderDetailInfos($shipId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('odr_hdr')
            ->select(
                [
                    'odr_hdr.odr_id',
                    'odr_hdr.odr_num',
                    'odr_dtl.item_id',
                    'odr_dtl.cus_upc',
                    'odr_dtl.sku',
                    'odr_dtl.size',
                    'odr_dtl.color',
                    'odr_dtl.lot',
                    'odr_dtl.uom_id',
                    'odr_dtl.uom_code',
                    'odr_dtl.qty',
                    'odr_dtl.pack',
                    'odr_dtl.piece_qty',

                ]
            )
            ->join('odr_dtl', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_hdr.ship_id', $shipId)
            ->where('odr_hdr.deleted', 0)
            ->where('odr_dtl.deleted', 0);

        $result = $query->get();

        return $result;
    }

    /**
     * @param $shipId
     *
     * @return mixed
     */
    public function getOrderInfos($shipId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('odr_hdr')
            ->select(
                [
                    'odr_hdr.odr_id',
                    'odr_hdr.odr_num',
                    DB::raw("COUNT(odr_dtl.odr_id) AS ord_no"),
                ]
            )
            ->join('odr_dtl', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_hdr.ship_id', $shipId)
            ->where('odr_hdr.deleted', 0)
            ->where('odr_dtl.deleted', 0)
            ->groupBy('odr_hdr.odr_id');

        $result = $query->get();

        return $result;
    }


}
