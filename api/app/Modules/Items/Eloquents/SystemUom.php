<?php

namespace App\Modules\Items\Eloquents;

use Illuminate\Database\Eloquent\Model;

class SystemUom extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_uom';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_uom_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sys_uom_id',
        'level_id',
        'sys_uom_code',
        'sys_uom_name',
        'sys_uom_des',
        'sys_uom_type',
        'created_by',
        'updated_by',
    ];
}
