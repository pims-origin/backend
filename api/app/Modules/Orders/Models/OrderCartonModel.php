<?php

namespace App\Modules\Orders\Models;

use Illuminate\Support\Facades\DB;
use App\Modules\Orders\Eloquents\OrderCarton;

class OrderCartonModel extends AbstractModel
{

    protected $model;

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    /**
     * @param $data
     */
    public function getOrderCarton($data)
    {
        return $this->model
            ->where('odr_hdr_id', $data['odr_hdr_id'])
            ->first();
    }

    /**
     * @param $odr_id
     *
     * @return mixed
     */
    public function loadAllByOdrId($odr_id)
    {
        return $this->model
            ->select([DB::raw('sum(piece_qty) as piece_ttl'), DB::raw('count(odr_ctn_id) as ctn_ttl')])
            ->where('odr_hdr_id', $odr_id)
            ->groupBy('odr_hdr_id')
            ->first();
    }

    /**
     * @param $odr_dtl_id
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function loadCartonToAssign($odr_dtl_id, $with = [], $limit = null)
    {
        $query = $this->make($with)
            ->where('ctn_sts', 'RT')
            ->select(['odr_ctn_id', 'ctn_id'])
            ->where('odr_dtl_id', $odr_dtl_id);

        if (!empty($limit)) {
            $query->take($limit);
        }

        return $query->get();
    }

}
