<?php

namespace App;

use App\Api\V1\Models\BaseInvoiceModel;

class Sac extends BaseInvoiceModel
{
    protected $table = 'sac';
    protected $primaryKey = 'sac_id';
    protected $fillable = [
        'sac',
        'sac_name',
        'description',
        'type',
        'block',
        'block_uom',
        'range_from',
        'range_to',
        'range_uom',
        'price',
        'unit_uom',
        'sac_period',
    ];
    protected $visible = [
        'sac_id',
        'sac',
        'sac_name',
        'description',
        'type',
        'block',
        'period_uom',
        'range_from',
        'range_to',
        'range_uom',
        'price',
        'unit_uom',
        'sac_period'
    ];
}
