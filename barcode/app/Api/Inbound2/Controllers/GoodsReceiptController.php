<?php

namespace App\Api\Inbound2\Controllers;

use App\Api\Inbound2\Models\AsnDtlModel;
use App\Api\Inbound2\Models\CartonModel;
use App\Api\Inbound2\Services\GoodsReceiptService;

use App\Api\Inventory\Models\PalletModel;
use App\Api\V1\Models\CustomerConfigModel;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\AsnMeta;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\DamageCarton;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\VirtualCartonSummary;
use App\Api\Inbound2\Models\GoodsReceiptModel;
use App\Api\V1\Models\GoodsReceiptModel as V1GoodsReceiptModel;
use Seldat\Wms2\Utils\Status;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Wms2\UserInfo\Data;
use App\Api\Inbound2\Traits\GoodsReceiptTrait;

class GoodsReceiptController extends AbstractController
{

    use GoodsReceiptTrait;

    const MAX_ACTUAL_TTL = 20000;

    public function listGrs($whsId)
    {
        return (new V1GoodsReceiptModel())->getReceivingList()->toArray();
    }

    public function skuList($whsId, $asnHdrId, $cntrId)
    {
        return (new AsnDtlModel())->getListItem($asnHdrId, $cntrId);
    }

    public function complete(Request $request, $whsId)
    {
        $input = $request->getParsedBody();
        $actCtns = $input['act_ctn_ttl'] ?: 0;
        $asnDtlId = $input['asn_dtl_id'] ?: 0;
        $ctnrId = $input['ctnr_id'] ?: 0;

        $asnDtl = AsnDtl::where('asn_dtl_id', $asnDtlId)
            ->whereIn('asn_dtl_sts', ['NW', 'RG'])
            ->first();

        if (empty($asnDtl)) {
            return $this->response->errorBadRequest('ASN detail invalid');
        }

        \DB::setFetchMode(\PDO::FETCH_ASSOC);
        $asnHdr = AsnHdr::where('asn_hdr_id', $asnDtl->asn_hdr_id)->first();
        try {
            DB::beginTransaction();
            $asnDtl->asn_dtl_sts = 'RG';
            $asnDtl->save();

            //create virtual carton sum
            $vtlCtnSum = [
                'asn_hdr_id'      => $asnDtl->asn_hdr_id,
                'asn_dtl_id'      => $asnDtl->asn_dtl_id,
                'item_id'         => $asnDtl->item_id,
                'ctnr_id'         => $asnDtl->ctnr_id,
                'cus_id'          => $asnHdr->cus_id,
                'whs_id'          => $asnHdr->whs_id,
                'discrepancy'     => $actCtns - $asnDtl->asn_dtl_ctn_ttl,
                'act_ctns'        => $actCtns,
                'exp_ctns'        => $asnDtl->asn_dtl_ctn_ttl,
                'vtl_ctn_sum_sts' => 'RG'
            ];

            $vtlCtnSumObj = new VirtualCartonSummary();
            $vtlCtnSumObj->fill($vtlCtnSum);
            $vtlCtnSumObj->save();

            //update asnHdr sts RG
            $asnHdr->asn_sts = 'RG';
            $asnHdr->save();

            DB::commit();

            //check asn complete all
            $notComplete = AsnDtl::where('asn_dtl_sts', 'NW')
                ->where('asn_hdr_id', $asnDtl->asn_hdr_id)
                ->where('ctnr_id', $ctnrId)
                ->count();
            if (!$notComplete) {
                //call process
                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $asnDtls = VirtualCartonSummary::where('asn_hdr_id', $asnDtl->asn_hdr_id)
                    ->where('ctnr_id', $ctnrId)
                    ->get(['asn_dtl_id', 'exp_ctns AS gr_dtl_act_ctn_ttl']);

                $grData = [
                    'ctnr_id'    => $asnDtl->ctnr_id,
                    'asn_hdr_id' => $asnHdr->asn_hdr_id,
                    'details'    => $asnDtls->toArray()
                ];

                (new GoodsReceiptService($request))->createGr($grData);
            }

            return ['data' => 'succesful'];
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }

    private function getWhsInfo($whsId)
    {
        foreach (Data::getInstance()->getUserInfo()['user_warehouses'] as $whs) {
            if ($whs['whs_id'] == $whsId) {
                return $whs;
            }
        }
    }

    /**
     * url: inbound/{whsId}/scan-carton/asn-dtl/{asnDtlId}
     * @throws \Exception
     * @internal param lpn string $code
     * @internal param integer $dmg_ttl
     * @internal param integer $act_ctn_ttl
     * @internal param decimal $weight
     * @internal param integer expired_date
     * @internal param string $lot
     *
     * @return mix
     */
    public function scanCartonAndPalletByKg($whsId, $asnDtlId, Request $request)
    {
        $whsInfo = $this->getWhsInfo($whsId);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $input = $request->getParsedBody();

        $ctnTtl = array_get($input, 'act_ctn_ttl', null); //this case is ttl kg, not null
        $weight = array_get($input, 'weight', null); //this case is ttl kg, null
        $dmgCtns = array_get($input, 'dmg_ctns', null);
        $isFull = array_get($input, 'is_full', 0);
        $isKg = false; // OUM is kg

        $dmgTtl = $dmgTtl2 = array_get($input, 'dmg_ttl', 0);
        // $dmgTtl = $dmgTtl2 = 0;
        // if ($dmgCtns) {
        //     foreach ($dmgCtns as $dmgCtn) {
        //         if (array_get($dmgCtn, 'dmg_id') > 0) {
        //             $dmgTtl++;
        //         }
        //     }
        // }
        // $dmgTtl2 = $dmgTtl;
        // $input['dmg_ttl'] = $dmgTtl;

        $rfid = array_get($input, 'lpn', null);
        $arrDmgCartonId = [];
        $expiredDate = array_get($input, 'expired_date', null);
        $lot = trim(array_get($input, 'lot', null));

        if ($expiredDate) {
            $expiredDate = strtotime(\DateTime::createFromFormat('ymd', $expiredDate)->format('Y-m-d'));
            if ($expiredDate < time()) {
                return $this->response->errorBadRequest("Expiration date is earlier than today's date");
            }
        }

        if ((int)$ctnTtl <= 0) {
            $msg = sprintf('Carton total must be greater than or equal one');

            return $this->response->errorBadRequest($msg);
        }

        if ((int)$ctnTtl >= self::MAX_ACTUAL_TTL) {
            $msg = sprintf('Actual total must be not greater than max total %d', self::MAX_ACTUAL_TTL);

            return $this->response->errorBadRequest($msg);
        }

        if ($dmgTtl > (int)$ctnTtl) {
            $msg = sprintf('Damaged total %d must be not greater than %d actual total', $dmgTtl, (int)$ctnTtl);

            return $this->response->errorBadRequest($msg);
        }

        $asnDtl = AsnDtl::where('asn_dtl_id', $asnDtlId)->first();
        if (empty($asnDtl)) {
            return $this->response->errorBadRequest('Asn dtl not existed');
        }

        if ($asnDtl->asn_dtl_sts == 'CC') {
            return $this->response->errorBadRequest('This item is cancelled');
        }

        //cuong, check UOM is KG
        if ($asnDtl->uom_code == 'KG') {
            $isKg = true;

            if ($asnDtl->asn_dtl_pack > 1) {
                return $this->response->errorBadRequest('UOM is Kg only process for pack size 1.');
            }

            if (empty($weight)) {
                return $this->response->errorBadRequest('UOM is Kg only process for weight greater than 0.');
            }
        } else {
            $ctnTtl = (int)$ctnTtl;
        }

        $asnHdr = AsnHdr::where('asn_hdr_id', $asnDtl->asn_hdr_id)->first();

        if(empty($asnHdr)){
            return $this->response->errorBadRequest('The ASN is not existed.');
        }

        //get gr hdr
        $grHdr = GoodsReceipt::where([
            'asn_hdr_id' => $asnHdr->asn_hdr_id,
            'ctnr_id'    => $asnDtl->ctnr_id
        ])->first();

        $grObj = new GoodsReceiptModel();
        //$grObj->validateLPNFormat($rfid, $whsInfo);
        $grObj->validateLPNPalFormat($rfid, $whsInfo);
        $nwPltNum = $grObj->validateRfidExisted($rfid, $grHdr->gr_hdr_id ?? -1);

        try {
            DB::beginTransaction();

            $userId = Data::getCurrentUserId();
            $expiredDate = !empty($expiredDate) ? $expiredDate : $asnDtl->expired_dt;
            if ($asnDtl->asn_dtl_sts == 'NW') {
                $asnDtl->asn_dtl_sts = 'RG';
                $asnDtl->save();

            }

            // Asn Meta By Qualifier
            $createdAt = AsnMeta::where('qualifier', 'GAD')
                ->where('asn_hdr_id', $asnHdr->asn_hdr_id)
                ->select('value')
                ->first();
            $createdAt = object_get($createdAt, 'value', time());

            if (!$grHdr) {
                $grSeq = GoodsReceipt::where('asn_hdr_id', $asnHdr->asn_hdr_id)->count();
                $grSeq++;
                $asnHdrNum = $asnHdr->asn_hdr_num;
                $grNum = str_replace(['ASN', 'RMA'], 'GDR', $asnHdrNum);
                $grNum = $grNum . '-' . str_pad($grSeq, 2, '0', STR_PAD_LEFT);


                $grHdr = new GoodsReceipt();
                $arr = [
                    "ctnr_id"       => $asnDtl->ctnr_id,
                    "asn_hdr_id"    => $asnDtl->asn_hdr_id,
                    "gr_hdr_seq"    => $grSeq,
                    //"gr_hdr_ept_dt" => $asnHdr->asn_hdr_ept_dt,
                    "gr_hdr_ept_dt" => $asnDtl->asn_dtl_ept_dt,
                    "gr_hdr_num"    => $grNum,
                    "whs_id"        => $asnHdr->whs_id,
                    "cus_id"        => $asnHdr->cus_id,
                    "gr_in_note"    => "",
                    "gr_ex_note"    => "",
                    "ctnr_num"      => $asnDtl->ctnr_num,
                    "gr_sts"        => "RG",
                    "ref_code"      => $asnHdr->asn_hdr_ref,
                    "created_from"  => 'GUN',
                    "created_at"    => $createdAt,
                    "updated_at"    => $createdAt,
                    "created_by"    => $userId,
                    "updated_by"    => $userId,
                    "deleted"       => 0,
                    "deleted_at"    => 915148800
                ];
                $grHdr->fill($arr)->save();

                //event tracking
                $evtGRCreated = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($grHdr, 'gr_hdr_num', null),
                    'evt_code'   => 'WGN',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - New Goods Receipt %s Created', $grHdr->gr_hdr_num),
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                EventTracking::insert($evtGRCreated);
            } else {
                $grNum = $grHdr->gr_hdr_num;

                if ($grHdr->created_from != 'GUN') {
                    return $this->response->errorBadRequest("This Goods Receipt can't process by GUN.");
                }

                if (!in_array($grHdr->gr_sts, ['NW', 'RG'])) {
                    return $this->response->errorBadRequest("This Goods Receipt was processed.");
                }

                $evtGRUpdate = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($grHdr, 'gr_hdr_num', null),
                    'evt_code'   => 'WGU',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - Goods Receipt Update %d cartons, %d damaged cartons',
                        ($isKg) ? 1 : $ctnTtl,
                        $dmgTtl),
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                EventTracking::insert($evtGRUpdate);
            }

            if ($asnHdr->asn_sts == 'NW') {
                $asnHdr->asn_sts = 'RG';
                $asnHdr->save();

                $evtAsnCreated = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($asnHdr, 'asn_hdr_num', null),
                    'evt_code'   => 'WAR',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - New Goods Receipt %s Created', $grHdr->gr_hdr_num),
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                EventTracking::insert($evtAsnCreated);
            }

            //create gr dtl
            $grDtl = GoodsReceiptDetail::where([
                'asn_dtl_id' => $asnDtlId,
                'gr_hdr_id'  => $grHdr->gr_hdr_id,
                'item_id'    => $asnDtl->item_id,
                'lot'        => $asnDtl->asn_dtl_lot,
            ])->first();
            if (!$grDtl) {
                $grDtl = new GoodsReceiptDetail();
                $arr = [
                    "asn_dtl_id"         => $asnDtlId,
                    "gr_hdr_id"          => $grHdr->gr_hdr_id,
                    "gr_dtl_ept_ctn_ttl" => $asnDtl->asn_dtl_ctn_ttl,
                    "gr_dtl_act_ctn_ttl" => $ctnTtl,
                    "gr_dtl_ept_qty_ttl" => ($isKg) ? $asnDtl->asn_dtl_qty_ttl :
                        $asnDtl->asn_dtl_ctn_ttl * $asnDtl->asn_dtl_pack, //new field

                    "gr_dtl_disc_qty" => ($isKg) ? $weight - $asnDtl->asn_dtl_qty_ttl : $ctnTtl *
                        $asnDtl->asn_dtl_pack - ($asnDtl->asn_dtl_ctn_ttl * $asnDtl->asn_dtl_pack), //new field

                    "gr_dtl_act_qty_ttl" => ($isKg) ? $weight : $ctnTtl * $asnDtl->asn_dtl_pack,//new field
                    "gr_dtl_des"         => $asnDtl->asn_dtl_des,
                    "gr_dtl_disc"        => $ctnTtl - $asnDtl->asn_dtl_ctn_ttl,
                    "gr_dtl_plt_ttl"     => 1,
                    "gr_dtl_is_dmg"      => $input['dmg_ttl'] > 0 ? 1 : 0,
                    "gr_dtl_dmg_ttl"     => $input['dmg_ttl'] > 0 ? $input['dmg_ttl'] : 0,
                    "cat_code"           => $asnDtl->cat_code,
                    "cat_name"           => $asnDtl->cat_name,
                    "gr_dtl_sts"         => "RG",
                    "item_id"            => $asnDtl->item_id,
                    "pack"               => $asnDtl->asn_dtl_pack,
                    "sku"                => $asnDtl->asn_dtl_sku,
                    "color"              => $asnDtl->asn_dtl_color,
                    "size"               => $asnDtl->asn_dtl_size,
                    "lot"                => $asnDtl->asn_dtl_lot,
                    "po"                 => $asnDtl->asn_dtl_po,
                    "uom_code"           => $asnDtl->uom_code,
                    "uom_name"           => $asnDtl->uom_name,
                    "uom_id"             => $asnDtl->uom_id,
                    "expired_dt"         => $expiredDate,
                    "upc"                => $asnDtl->asn_dtl_cus_upc,
                    "ctnr_id"            => $asnDtl->ctnr_id,
                    "ctnr_num"           => $asnDtl->ctnr_num,
                    "length"             => $asnDtl->asn_dtl_length,
                    "width"              => $asnDtl->asn_dtl_width,
                    "height"             => $asnDtl->asn_dtl_height,
                    "weight"             => ($isKg) ? $weight : $asnDtl->asn_dtl_weight,
                    "cube"               => $asnDtl->asn_dtl_cube,
                    "volume"             => $asnDtl->asn_dtl_volume,
                    "ucc128"             => $asnDtl->ucc128,
                    "crs_doc"            => 0, //consider
                    "spc_hdl_code"       => $asnDtl->spc_hdl_code,
                    "spc_hdl_name"       => $asnDtl->spc_hdl_name,
                    "created_at"         => $createdAt,
                    "updated_at"         => $createdAt,
                    "created_by"         => $userId,
                    "updated_by"         => $userId,
                    "deleted"            => 0,
                    "deleted_at"         => 915148800
                ];
                $grDtl->fill($arr)->save();

            } else {
                $grDtl->gr_dtl_act_ctn_ttl += $ctnTtl;
                if ($isKg) {
                    $grDtl->weight += $weight;
                    $grDtl->gr_dtl_act_qty_ttl += $weight;
                } else {
                    $grDtl->gr_dtl_act_qty_ttl += $ctnTtl * $asnDtl->asn_dtl_pack;
                }

                $grDtl->gr_dtl_plt_ttl += 1;
                $grDtl->gr_dtl_is_dmg = $input['dmg_ttl'] > 0 ? 1 : $grDtl->gr_dtl_is_dmg;
                $grDtl->gr_dtl_dmg_ttl += $input['dmg_ttl'];
                $grDtl->gr_dtl_disc = $grDtl->gr_dtl_act_ctn_ttl - $asnDtl->asn_dtl_ctn_ttl;
                $grDtl->gr_dtl_disc_qty = $grDtl->gr_dtl_act_qty_ttl - $grDtl->gr_dtl_ept_qty_ttl;
                $grDtl->save();
            }

            $lot = empty($lot) ? $asnDtl->asn_dtl_lot : $lot;
            $palletID = null;
            if (is_object($nwPltNum)) {
                if ($isKg) {
                    return $this->response->errorBadRequest('Kg not allow mix sku');
                }

                //if ($grDtl->spc_hdl_code != $nwPltNum->spc_hdl_code) {
                //    return $this->response->errorBadRequest("Pallet's temperature don't match with item.");
                //}

                $palletID = $nwPltNum->plt_id;

                //process scan pallet many times
                $nwPltNum->ctn_ttl += $ctnTtl;
                $nwPltNum->init_ctn_ttl += $ctnTtl;
                $nwPltNum->init_piece_ttl += $grDtl->pack * $ctnTtl;
                $nwPltNum->dmg_ttl += $input['dmg_ttl'];
                if ($grDtl->item_id != $nwPltNum->item_id || $lot != $nwPltNum->lot) {
                    $nwPltNum->mixed_sku = 1;
                    $nwPltNum->sku = $nwPltNum->size = $nwPltNum->color = '';
                }
                $nwPltNum->save();

            } else {
                //add plt_num
                $plt = [
                    "cus_id"           => $asnHdr->cus_id,
                    "whs_id"           => $asnHdr->whs_id,
                    "plt_num"          => !empty($cus_pid) ? $rfid . "-" . $cus_pid : ++$nwPltNum,
                   // "cus_pid"          => !empty($cus_pid) ? $cus_pid : null,
                    "rfid"             => $rfid,
                    "gr_hdr_id"        => $grHdr->gr_hdr_id,
                    "gr_dtl_id"        => $grDtl->gr_dtl_id,
                    "ctn_ttl"          => ($isKg) ? 1 : $input['act_ctn_ttl'],
                    "init_ctn_ttl"     => $ctnTtl,
                    "dmg_ttl"          => $input['dmg_ttl'],
                    "expired_date"     => !empty($expiredDate) ? $expiredDate : $asnDtl->expired_dt,
                    "storage_duration" => 0,
                    "plt_sts"          => 'NW',
                    "is_movement"      => 0,
                    "item_id"          => $grDtl->item_id,
                    "sku"              => $grDtl->sku,
                    "size"             => $grDtl->size,
                    "color"            => $grDtl->color,
                    "pack"             => ($isKg) ? $ctnTtl : $grDtl->pack,
                    "lot"              => $lot,
                    "is_full"          => $isFull,
                    "uom"              => ($isKg) ? 'PL' : null,
                    "weight"           => ($isKg) ? $weight : null,
                    "init_piece_ttl"   => ($isKg) ? $weight : $grDtl->pack * $ctnTtl,
                    "created_at"       => $createdAt,
                    "updated_at"       => $createdAt,
                    "created_by"       => $userId,
                    "updated_by"       => $userId,
                    "deleted"          => 0,
                    "deleted_at"       => 915148800,
                    "spc_hdl_code"     => $grDtl->spc_hdl_code
                ];

                $palletID = DB::table('pallet')->insertGetId($plt);
            }


            //create cartons
            $ctnNumPrefix = str_replace('GDR', 'CTN', $grHdr->gr_hdr_num) . "-";
            $maxCtnNum = DB::table('cartons')->where('ctn_num', 'LIKE', $ctnNumPrefix . '%')->max('ctn_num');
            $maxCtnNum = $maxCtnNum ?: $ctnNumPrefix . '0000';
            $explodeMax = explode('-', $maxCtnNum);
            $countMax = count($explodeMax);
            $lastString = end($explodeMax);
            if (strlen($lastString) < 7) {
                $lastString = str_pad(object_get($grHdr, 'whs_id', 2), 7 - strlen($lastString), "0", STR_PAD_LEFT) . $lastString;
                $explodeMax[$countMax-1] = $lastString;
            }
            $maxCtnNum = implode('-', $explodeMax);

            $cartonParams = [];
            $cartonDamages = [];

            $ctnSts = 'RG';
            if ($isKg) {
                $cartonParams = [
                    'asn_dtl_id'    => $asnDtlId,
                    'gr_dtl_id'     => $grDtl->gr_dtl_id,
                    'gr_hdr_id'     => $grDtl->gr_hdr_id,
                    'is_damaged'    => 0,
                    'item_id'       => $grDtl->item_id,
                    'whs_id'        => $whsId,
                    'cus_id'        => $grHdr->cus_id,
                    'ctn_num'       => ++$maxCtnNum,
                    'ctn_sts'       => $ctnSts,
                    'ctn_uom_id'    => $asnDtl->uom_id,
                    'uom_code'      => $asnDtl->uom_code,
                    'uom_name'      => $asnDtl->uom_name,
                    'ctn_pack_size' => 1,
                    // 'piece_remain'  => round($weight), // this case is weight
                    'piece_remain'  => $weight, // this case is weight
                    // 'piece_ttl'     => round($weight), // this case is weight
                    'piece_ttl'     => $weight, // this case is weight
                    'gr_dt'         => $createdAt,
                    'sku'           => $asnDtl->asn_dtl_sku,
                    'size'          => $asnDtl->asn_dtl_size,
                    'color'         => $asnDtl->asn_dtl_color,
                    'lot'           => $lot,
                    'po'            => $asnDtl->asn_dtl_po,
                    'upc'           => $asnDtl->asn_dtl_cus_upc,
                    'ctnr_id'       => $asnDtl->ctnr_id,
                    'ctnr_num'      => $asnDtl->ctnr_num,
                    'length'        => $asnDtl->asn_dtl_length,
                    'width'         => $asnDtl->asn_dtl_width,
                    'height'        => $asnDtl->asn_dtl_height,
                    'weight'        => $weight,
                    'volume'        => $asnDtl->asn_dtl_volume,
                    'cube'          => $asnDtl->asn_dtl_cube,
                    'expired_dt'    => $expiredDate,
                    'ucc128'        => $asnDtl->ucc128,
                    'des'           => $asnDtl->asn_dtl_des,
                    'cat_code'      => $asnDtl->cat_code,
                    'cat_name'      => $asnDtl->cat_name,
                    'spc_hdl_code'  => $asnDtl->spc_hdl_code,
                    'spc_hdl_name'  => $asnDtl->spc_hdl_name,
                    'plt_id'        => $palletID,
                    'inner_pack'    => $ctnTtl,
                    'loc_id'        => null,
                    'loc_code'      => null,
                    'loc_name'      => null,
                    "created_at"    => time(),
                    "updated_at"    => time(),
                    "created_by"    => $userId,
                    "updated_by"    => $userId,
                    "deleted"       => 0,
                    "deleted_at"    => 915148800
                ];
            } else {
                for ($i = 0; $i < $ctnTtl; $i++) {
                    $cartonParam = [
                        'asn_dtl_id'    => $asnDtlId,
                        'gr_dtl_id'     => $grDtl->gr_dtl_id,
                        'gr_hdr_id'     => $grDtl->gr_hdr_id,
                        'is_damaged'    => $dmgTtl-- > 0 ? 1 : 0,
                        'item_id'       => $grDtl->item_id,
                        'whs_id'        => $whsId,
                        'cus_id'        => $grHdr->cus_id,
                        'ctn_num'       => ++$maxCtnNum,
                        'ctn_sts'       => $ctnSts,
                        'ctn_uom_id'    => $asnDtl->uom_id,
                        'uom_code'      => $asnDtl->uom_code,
                        'uom_name'      => $asnDtl->uom_name,
                        'ctn_pack_size' => $asnDtl->asn_dtl_pack,
                        'piece_remain'  => $asnDtl->asn_dtl_pack,
                        'piece_ttl'     => $asnDtl->asn_dtl_pack,
                        'gr_dt'         => $createdAt,
                        'sku'           => $asnDtl->asn_dtl_sku,
                        'size'          => $asnDtl->asn_dtl_size,
                        'color'         => $asnDtl->asn_dtl_color,
                        'lot'           => $lot,
                        'po'            => $asnDtl->asn_dtl_po,
                        'upc'           => $asnDtl->asn_dtl_cus_upc,
                        'ctnr_id'       => $asnDtl->ctnr_id,
                        'ctnr_num'      => $asnDtl->ctnr_num,
                        'length'        => $asnDtl->asn_dtl_length,
                        'width'         => $asnDtl->asn_dtl_width,
                        'height'        => $asnDtl->asn_dtl_height,
                        'weight'        => $asnDtl->asn_dtl_weight,
                        'volume'        => $asnDtl->asn_dtl_volume,
                        'cube'          => $asnDtl->asn_dtl_cube,
                        'expired_dt'    => $expiredDate,
                        'ucc128'        => $asnDtl->ucc128,
                        'des'           => $asnDtl->asn_dtl_des,
                        'cat_code'      => $asnDtl->cat_code,
                        'cat_name'      => $asnDtl->cat_name,
                        'spc_hdl_code'  => $asnDtl->spc_hdl_code,
                        'spc_hdl_name'  => $asnDtl->spc_hdl_name,
                        'plt_id'        => $palletID,
                        'loc_id'        => null,
                        'loc_code'      => null,
                        'loc_name'      => null,
                        "created_at"    => time(),
                        "updated_at"    => time(),
                        "created_by"    => $userId,
                        "updated_by"    => $userId,
                        "deleted"       => 0,
                        "deleted_at"    => 915148800
                    ];

                    if ($cartonParam['is_damaged'] == 1) {
                        $cartonDamages[] = $cartonParam;
                    } else {
                        $cartonParams[] = $cartonParam;
                    }
                }
            }

            if (!empty($cartonParams)) {
                $insertBatchs = array_chunk($cartonParams, 200);
                foreach ($insertBatchs as $batch) {
                    DB::table('cartons')->insert($batch);
                // Carton::insert($batch);
                }
            }

            //add dmg cartons and dmg carton note
            if (!empty($cartonDamages)) {
                foreach ($cartonDamages as $idx => $cartonDamage) {
                    $cartonDamageId = Carton::insertGetId($cartonDamage);
                    $dmgData = [
                        "ctn_id"     => $cartonDamageId,
                        "dmg_id"     => array_get($dmgCtns, 'dmg_id') ?? 1,
                        "dmg_note"   => array_get($dmgCtns, 'note'),
                        "created_at" => time(),
                        "updated_at" => time(),
                        "created_by" => $userId,
                        "updated_by" => $userId,
                        "deleted"    => 0,
                        "deleted_at" => 915148800
                    ];
                    DB::table('damage_carton')->insert($dmgData);
                    $arrDmgCartonId[] = $cartonDamageId;

                }
            }

            //sprintf('GUN - %d  cartons  to pallet  %s', ($isKg)? 1 : $ctnTtl, $rfid)
            $info = sprintf('GUN - %d  cartons  to pallet  %s', $ctnTtl, $rfid);
            if ($isKg) {
                $info = sprintf('GUN - %d carton and %s KG  to pallet  %s', $ctnTtl, $weight, $rfid);
            }

            $evtCartonCreated = [
                'whs_id'     => $whsId,
                'cus_id'     => $grHdr->cus_id,
                'owner'      => $grNum,
                'evt_code'   => 'WGS',
                'trans_num'  => $asnDtl->ctnr_num,
                'info'       => $info,
                'created_at' => time(),
                'created_by' => $userId,
            ];

            EventTracking::insert($evtCartonCreated);

            //create invertory sumary
            //$grObj->createInventory($grHdr->whs_id, $grHdr->cus_id, $grDtl, $lot);

            DB::commit();

            foreach ($arrDmgCartonId as $index => $ctnId) {
                if (isset($dmgCtns[$index]['image_base64'])) {
                    $name = uniqid("damaged_image_") . '.png';
                    $pathFile = storage_path("app") . '/' . $name;
                    $client = new Client();
                    $res = $client->post(
                        env('API_GOODSRECEIPT') . "damage-carton/{$ctnId}/upload/file", [
                            RequestOptions::MULTIPART   => [
                                [
                                    'name'     => 'file',
                                    'filename' => $name,
                                    'contents' => base64_decode($dmgCtns[$index]['image_base64'])
                                ]
                            ],
                            RequestOptions::HEADERS     => [
                                'Authorization' => $request->getHeader('Authorization')[0],
                                //'Content-Type' => 'multipart/form-data;'
                            ],
                            RequestOptions::HTTP_ERRORS => false
                        ]
                    );
                }
            }

            return [
                'message' => sprintf('`%s` and %d cartons has been created in %s', $rfid, ($isKg) ? 1 : $ctnTtl, $grNum)
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getGoodsReceiptDetails($whsId, $grId, Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        /*
         * get special handling from gr dtl
         * Get 4 locations with spc hdl
         */
        $sqlTtl = '(SELECT COUNT(pallet.plt_id)
                           FROM pallet
                           WHERE pallet.gr_hdr_id = gr_hdr.gr_hdr_id
                           AND pallet.ctn_ttl > 0
                     ) AS pallet_total';
        $sqlScan = '(SELECT COUNT(pallet.plt_id)
                           FROM pallet
                           WHERE pallet.gr_hdr_id = gr_hdr.gr_hdr_id
                                AND pallet.loc_id IS NOT NULL
                                AND pallet.ctn_ttl > 0
                     ) AS pallet_scan';

        $grInfo = DB::table('gr_hdr')
            ->select('gr_hdr.gr_hdr_num', 'gr_hdr.gr_hdr_id', DB::raw($sqlTtl), DB::raw($sqlScan))
            ->where(['gr_hdr.gr_hdr_id' => $grId, 'gr_hdr.whs_id' => $whsId])
            ->where('gr_hdr.deleted', 0)
            ->where('gr_hdr.deleted_at', 915148800)
            ->first();

        return ['data' => $grInfo];
    }

    public function getSuggestionLocations($whsId, Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        /*
         * get special handling from gr dtl
         * Get 4 locations with spc hdl
         */
        $input = $request->getQueryParams();
        $lpn = array_get($input, 'lpn', null);

        if (!$lpn) {
            return $this->response->errorBadRequest('LPN is required!');
        }

        $palletInfo = DB::table('pallet')
            ->join('gr_dtl', 'gr_dtl.gr_dtl_id', '=', 'pallet.gr_dtl_id')
            ->select([
                'gr_dtl.spc_hdl_code',
                'pallet.cus_id',
                'pallet.ctn_ttl',
                'pallet.init_ctn_ttl',
                'pallet.loc_id',
                'pallet.whs_id',
                'pallet.is_full',
                //'pallet.odr_id',
                'pallet.loc_code'
            ])
            ->where('pallet.whs_id', $whsId)
            ->where('pallet.rfid', $lpn)
            ->where('pallet.ctn_ttl', '>', 0)
            ->where('pallet.deleted', 0)
            ->where('pallet.deleted_at', 915148800)
            ->orderBy('pallet.plt_id', 'DESC')
            ->first();

        /*
         * ctn_ttl  = 0, pallet no cartons or picked
         * loc_id  IS NOT NULL, Put away already!
         */
        if (empty($palletInfo)) {
            $msg = sprintf('%s is not existed!', $lpn);

            return $this->response->errorBadRequest($msg);
        }

        list($whsCode, $pltType) = explode('-', $lpn);

        if (!empty($palletInfo['loc_id'])) {
            $msg = sprintf('%s on %s already!', $lpn, $palletInfo['loc_code']);

            return $this->response->errorBadRequest($msg);
        }

        $is_Xdock = false;
        if(!empty($palletInfo['odr_id'])) {
            $is_Xdock = true;
        }
        $isFull = $palletInfo['is_full'] && $palletInfo['init_ctn_ttl'] == $palletInfo['ctn_ttl'];

        // check cus_id exists
        $is_putaway = DB::table('pallet')
                        ->where('cus_id', '=', $palletInfo['cus_id'])
                        ->whereNotNull('loc_code')
                        ->first();
        $limit = $is_putaway ? 2 : 1;
        $locs = $this->getLocationsByCustomer($palletInfo, false, $pltType, $isFull,$is_Xdock,$limit);

        if (!$locs || count($locs) == 0) {
//            $cusConfig = new CustomerConfigModel();
//            $isDynZone = $cusConfig->isDynamicZone($palletInfo['whs_id'], $palletInfo['cus_id']);
//            if ($isDynZone) {

                $locs = $this->getLocationsByCustomer($palletInfo, true, $pltType, $isFull,$is_Xdock, $limit);

//            }
        }

        $suggestion = [];
        if (count($locs) > 0) {
            foreach ($locs as $key => $loc) {
                //$data = $this->getParentLoc($loc['parent_id']);
                //$data ? $data[0]['loc_code'] : null;
                $suggestion[] = $loc['loc_code'];
            }
        }

//        if(!empty($locs[0]['is_block_stack']) && $locs[0]['is_block_stack'] === 1) {
//            if(!empty($locs[0]['parent_id']) && $locs[0]['parent_id'] > 0) {
//                $locs = $this->getParentLoc($locs[0]['parent_id']);
//            }
//        }
//        $loc = [];
//        if(!empty($locs['0']['loc_code'])) {
//            $loc[] = $locs['0']['loc_code'];
//        }

        return [
            'locations' => $suggestion
        ];

    }

    private function getLocationsByCustomer(array $attributes, $isDynZone, $pltType, $isFull, $is_Xdock = false ,$limit = 2)
    {
        $query = DB::table('location');
        $query->select([
//            'location.loc_id',
            'location.parent_id',
            'location.is_block_stack',
            'l.loc_code'
//            'location.loc_code'
        ])
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('location.loc_sts_code', '=', 'AC')
            ->whereRaw(
                '0 = (SELECT COUNT(pallet.loc_id) FROM pallet WHERE pallet.loc_id = location.loc_id AND pallet.loc_id IS NOT NULL)'
            )
            ->where('location.loc_whs_id', $attributes['whs_id'])
            ->where('location.loc_cus_id', $attributes['cus_id'])
//            ->where('location.spc_hdl_code', $attributes['spc_hdl_code'])
            ->where('location.deleted', 0);
            //->where('location.plt_type', $pltType);
        if($is_Xdock === true) {
            $query->where('loc_type.loc_type_code', '=', 'XDK');
        } else {
            $query->where('loc_type.loc_type_code', '=', 'RAC');
        }

        //Dynamic zone select locations
        if ($isDynZone) {
            $query->whereNull('location.loc_zone_id');
        } else {
            $query->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
                ->where('customer_zone.cus_id', $attributes['cus_id']);
            $query->join('location as l', 'l.loc_id', '=', 'location.parent_id');
            $query->whereNotExists(function ($query) use ($attributes) {
                $query->select(DB::raw(1))
                    ->from('pallet')
                    ->whereRaw('l.loc_code = pallet.loc_code and pallet.cus_id <> ' . $attributes['cus_id'])
                    ->groupBy('pallet.loc_code')
                    ->havingRaw('COUNT(pallet.cus_id) >= ?', [2]);
            });
        }

        //$query->orderBy('location.aisle', 'ASC');
        $query->orderBy('location.level', 'ASC');
        $query->orderBy('location.row', 'ASC');
        $query->orderBy('location.bin', 'ASC');

        /*//Full pallet suggestion
        if ($isFull) {
            $query->orderBy(DB::Raw("location.loc_code REGEXP '([A])[1-2]$', location.loc_code"), 'ASC');
        } else {
            $query->orderBy(DB::Raw("location.loc_code REGEXP '([B-Z])[1-2]$', location.loc_code"), 'ASC');
        }*/


        $query->groupBy('location.loc_id');

        $locs = $query->take($limit)->distinct()->get()->toArray();

        if (count($locs)) {
            $locs = $this->sortDeepLocationPutaway($locs);
        }
        return $locs;
    }

    public function putAwayList($whsId)
    {
        $result = (new GoodsReceiptModel())->getPutAwayList($whsId);

        return $result;
    }

    private function sortDeepLocationPutaway($data)
    {
        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i + 1])) {
                $next = $data[$i + 1]['loc_code'];
                $cur = $data[$i]['loc_code'];
                $curloc = substr($cur, 0, strlen($cur) - 1);
                $nextLoc = substr($next, 0, strlen($next) - 1);
                if ($curloc == $nextLoc) {
                    if (substr($cur, -1, 1) < substr($next, -1, 1)) {
                        $next = $data[$i + 1];
                        $current = $data[$i];
                        $data[$i + 1] = $current;
                        $data[$i] = $next;
                        $i = $i + 1;
                    }

                }

            }
        }

        return $data;
    }

    /*
    *  Get Location in Dynamic Zone
    */

    private function getParentLoc($loc_id) {
        $query = DB::table('location');
        $query->select([
            'location.loc_id',
            'location.loc_code'
        ])->where("loc_id", $loc_id)
            ->where("deleted",0);


        $locs[] = $query->first();
        return $locs;
    }

    /*
     *  End get Virtual Location
     */

}