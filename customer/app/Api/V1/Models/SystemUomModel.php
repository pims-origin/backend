<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use App\Api\V1\Traits\ChargeCodeSysUomService;
use \Seldat\Wms2\Models\SystemUom;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class SystemUomModel extends AbstractModel
{
    use ChargeCodeSysUomService;

    /**
     * SystemUomModel constructor.
     *
     * @param SystemUom|null $model
     */
    public function __construct(SystemUom $model = null)
    {
        $this->model = ($model) ?: new SystemUom();
    }

    /**
     * @param int $char_uom_id
     *
     * @return int
     */
    public function deleteSystemUom($char_uom_id)
    {
        return $this->model
            ->where('sys_uom_id', $char_uom_id)
            ->delete();
    }

    public function deleteMassSystemUom(array $char_uom_ids)
    {
        return $this->model
            ->whereIn('sys_uom_id', $char_uom_ids)
            ->delete();
    }

    /**
     * Search SystemUom
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        return $this->_search($attributes, $with, $limit, ['sys_uom_code', 'sys_uom_name']);
    }
}
