<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 26-Jul-2016
 * Time: 3:08
 */


namespace App\Api\Inbound2\Controllers;

use App\Api\Inbound2\Models\DamageTypeModel;
use App\Api\Inbound2\Transformers\DamageTypeTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;

class DamageTypeController extends AbstractController
{
    /**
     * @var damageTypeModel
     */
    protected $damageTypeModel;

    /**
     * DamageTypeController constructor.
     *
     * @param DamageTypeModel $dmgTypeModel
     */
    public function __construct(DamageTypeModel $dmgTypeModel)
    {
        $this->damageTypeModel = $dmgTypeModel;
    }

    public function search(Request $request, DamageTypeTransformer $dmgTypeTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dmgType = $this->damageTypeModel->search($input, [], array_get($input, 'limit', 20));

            return $this->response->paginator($dmgType, $dmgTypeTransformer);
        } catch (\PDOException $e) {
            SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__);
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
