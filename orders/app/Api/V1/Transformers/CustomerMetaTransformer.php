<?php

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\CustomerMeta;
use League\Fractal\TransformerAbstract;

class CustomerMetaTransformer extends TransformerAbstract
{
    public function transform(CustomerMeta $customerMeta)
    {
        return $customerMeta->val_json_decode;
    }
}