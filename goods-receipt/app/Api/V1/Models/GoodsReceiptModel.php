<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class GoodsReceiptModel extends AbstractModel
{
    const DAS_LIMIT = 2;
    const ROLE = ['admin', 'manager'];

    /**
     * GoodsReceiptModel constructor.
     *
     * @param GoodsReceipt|null $model
     */
    public function __construct(GoodsReceipt $model = null)
    {
        $this->model = ($model) ?: new GoodsReceipt();
    }

    /**
     * @param int $goodsReceiptId
     *
     * @return int
     */
    public function deleteGoodsReceipt($goodsReceiptId)
    {
        return $this->model
            ->where('gr_hdr_id', $goodsReceiptId)
            ->delete();
    }

    /**
     * @param array $goodsReceiptIds
     *
     * @return mixed
     */
    public function deleteMassGoodsReceipt(array $goodsReceiptIds)
    {
        return $this->model
            ->whereIn('gr_hdr_id', $goodsReceiptIds)
            ->delete();
    }

    /**
     * @return bool
     */
    public function checkPermissionAdminManager($userId)
    {
        //  Role Amin & Manager are get pull data
        $roles = DB::table('user_role')
            ->select('item_name')
            ->where('user_id', $userId)
            ->get();

        $flag = false;
        foreach ($roles as $role) {
            if (in_array(strtolower($role['item_name']), self::ROLE)) {
                $flag = true;
                break;
            }
        }

        return $flag;
    }

    /**
     * Search GoodsReceipt
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = 20, $export = false)
    {
        $query = $this->make($with)
            ->select([
                'gr_hdr.*'
            ]);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (isset($attributes['asn_type'])) {
            $query->whereHas('asnHdr', function ($query) use ($attributes) {
                $query->where('asn_type', $attributes['asn_type']);
            });
        }

        if (isset($attributes['created_from'])) {
            $query->where('gr_hdr.created_from', $attributes['created_from']);
        }

        if (isset($attributes['gr_hdr_id'])) {
            $query->where('gr_hdr.gr_hdr_id', $attributes['gr_hdr_id']);
        }

        if (isset($attributes['gr_hdr_num'])) {
            $query->where('gr_hdr.gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['gr_hdr_num']) . "%");
        }
        // Search Customer
        if (isset($attributes['cus_id'])) {
            $query->where('gr_hdr.cus_id', $attributes['cus_id']);
        }
        // Search whs_id
        if (isset($attributes['whs_id'])) {
            $query->where('gr_hdr.whs_id', $attributes['whs_id']);
        }

        // Search gr_status
        if (isset($attributes['gr_sts'])) {
            $attributes['gr_sts'] = is_array($attributes['gr_sts']) ? $attributes['gr_sts'] : [$attributes['gr_sts']];
            $query->where(function($query) use($attributes){
                foreach($attributes['gr_sts'] as $index => $status){
                    if($index === 0){
                        $query->where('gr_hdr.gr_sts', $status);
                    }else{
                        $query->orWhere('gr_hdr.gr_sts', $status);
                    }
                }
            });
        }

        // Get putaway
        if (isset($attributes['putaway'])) {
            $query->where('gr_hdr.putaway', $attributes['putaway']);
        }

        // search dashboard putaway
        if (isset($attributes['dashboard_putaway'])) {
            if ($attributes['dashboard_putaway'] == 'putting') {
                $query->where('gr_hdr.putaway', 0);
                $query->whereHas('pallets', function ($query) {
                    $query->whereRaw('gr_hdr_id IS NOT NULL');
                });
            }
            if ($attributes['dashboard_putaway'] == 'already') {
                $query->where('gr_hdr.putaway', 1);
            }
            if ($attributes['dashboard_putaway'] == 'putaway') {
                $query->where('gr_hdr.putaway', 0);
                $query->doesntHave('pallets');
            }
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('gr_hdr.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        // search item
        $query->whereHas('asnHdr', function ($query) use ($attributes) {
            if (isset($attributes['asn_hdr_ref'])) {
                $query->where('asn_hdr_ref', 'like', "%" . SelStr::escapeLike($attributes['asn_hdr_ref']) . "%");
            }

        });

        // search item
        $query->whereHas('goodsReceiptDetail', function ($query) use ($attributes) {
            if (isset($attributes['sku'])) {
                $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
            }
        });

        // Putaway list
        if (in_array('palletSuggestLocation', $with)) {
            $query->whereHas('palletSuggestLocation', function ($query) use ($attributes) {
            });
        }

        if (!empty($attributes['putter'])) {
            $query->whereHas("putterUser", function ($q) use ($attributes) {
                $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['putter']) . "%");
                $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['putter']) . "%");
                //$q->orWhere(
                //    DB::raw("concat(first_name, ' ', last_name)"),
                //    'like',
                //    "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['putter'])) . "%"
                //);
            });
        }

        if (isset($attributes['gr_hdr_act_dt_from'])) {
            $act_from_date = $attributes['gr_hdr_act_dt_from'];
            $from_date = strtotime($act_from_date);
            $query->where('gr_hdr_act_dt', '>=', $from_date);
        }

        if (isset($attributes['gr_hdr_act_dt_to'])) {
            $act_to_date = $attributes['gr_hdr_act_dt_to'];
            $to_date = strtotime($act_to_date);
            $query->where('gr_hdr_act_dt', '<=', $to_date);
        }

        if (isset($attributes['day'])) {
            $day = $attributes['day'];
            $limitday = (!empty($day) && is_numeric($day)) ? $day : 0;
            $query->where('gr_hdr.updated_at', '>=',
                DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $limitday . ' DAY)'));
        }

        // search item
        // Get
        $this->sortBuilder($query, $attributes);

        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);

        // if (!$this->checkPermissionAdminManager($userId)
        //     && isset($attributes['putter_id'])
        // ) {
        //     $query->where('putter', $userId);
        // }

        //----Export
        $query->leftJoin('gr_dtl as gt', 'gt.gr_hdr_id', '=', 'gr_hdr.gr_hdr_id')
            ->addSelect([
                DB::raw("
                sum(gt.gr_dtl_act_ctn_ttl) AS gr_act_ctn_ttl,
                count(gt.gr_dtl_id) AS item_ttl
            ")
            ]);
        $query->groupBy('gr_hdr.gr_hdr_id');
        //----/Export

        $this->model->filterData($query, true);

        //$models = $query->paginate($limit);
        if ($export) {
            $models = $query->get();
        } else {
            $models = $query->paginate($limit);
        }

        return $models;
    }


    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function getGrHdrById($grHdrIds)
    {
        $grHdrIds = is_array($grHdrIds) ? $grHdrIds : [$grHdrIds];

        $rows = $this->model
            ->whereIn('gr_hdr_id', $grHdrIds)
            ->get();

        return $rows;
    }

    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function getGoodsReceiptById($grHdrId)
    {
        $rows = $this->model
            ->where('gr_hdr_id', $grHdrId)
            ->first();

        return $rows;
    }

    /**
     * @return mixed
     */
    public function grDashBoard($day)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', 0);
        $currentWH = array_get($userInfo, 'current_whs', 0);

        $limit = (!empty($day) && is_numeric($day)) ? $day : self::DAS_LIMIT;


        $inprocess = DB::table('gr_hdr')
            ->select(DB::raw('COUNT(1) as inprocess'))
            ->where('whs_id', $currentWH)
            ->where('gr_sts', Status::getByKey("GR_STATUS", "RECEIVING"))
            ->where('updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('created_by', $userId)
                    ->orWhere('updated_by', $userId);
            })
            ->first();

        $received = DB::table('gr_hdr as g')
            ->select(DB::raw("COUNT(1) as received"))
            ->join('pal_sug_loc as psl', 'psl.gr_hdr_id', '=', 'g.gr_hdr_id')
            ->where('g.whs_id', $currentWH)
            ->where('g.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'))
            ->where(function ($query) use ($userId) {
                $query->where('g.created_by', $userId)
                    ->orWhere('g.updated_by', $userId);
            })
            ->whereNotNull('g.putter')
            ->where(DB::raw('(SELECT COUNT(plt_id) FROM pallet WHERE g.gr_hdr_id = pallet.gr_hdr_id AND pallet.loc_id IS NULL)'),
                '>', 0)
            ->where('g.gr_sts', Status::getByKey("GR_STATUS", "RECEIVED"))
            ->groupBy('g.gr_hdr_id')
            ->first();

        return [
            'ttl_gr_in_process'     => !empty($inprocess['inprocess']) ? $inprocess['inprocess'] : 0,
            'ttl_gr_ready_put_away' => !empty($received['received']) ? $received['received'] : 0
        ];
    }

    public function getGRByCurrentUser($goodsReceiptId)
    {
        $whs_id = Data::getCurrentWhsId();
        $currentCusIds = Data::getCurrentCusIds();

        return $this->getModel()
            ->where('gr_hdr_id', $goodsReceiptId)
            ->where('gr_hdr.whs_id', $whs_id)
            ->whereIn('gr_hdr.cus_id', $currentCusIds)
            ->first();
    }

    public function updateGRRecevied($grId, $inNote, $exNote, $actGrDt=null,$putawayDt=null)
    {
        $updateData=[
            'gr_hdr.gr_sts'     => 'RE',
            'gr_hdr.putaway'    => 1,
            'gr_hdr.gr_in_note' => $inNote,
            'gr_hdr.gr_ex_note' => $exNote,
        ];
        if(!empty($actGrDt))
            $updateData['gr_hdr_act_dt']= strtotime($actGrDt);
        if(!empty($putawayDt))
            $updateData['putaway_dt']= strtotime($putawayDt);
        $result = $this->model
            ->where(['gr_hdr.gr_hdr_id' => $grId])
            ->update($updateData);

        GoodsReceiptDetail::where('gr_hdr_id', $grId)
            ->where('gr_dtl_sts', '!=', 'CC')
            ->update(['gr_dtl_sts' => 'RE'])
        ;

        return $result;

    }

    public function updateContainerInfo ($grHdrId, $input) {
        $input = SelArr::removeNullOrEmptyString($input);
        $result = $this->model
            ->where(['gr_hdr.gr_hdr_id' => $grHdrId])
            ->update([
                'gr_hdr.gr_type'        => array_get($input, 'gr_type'),
                'gr_hdr.pallet_ttl'     => array_get($input, 'pallet_ttl'),
                'gr_hdr.cube'           => $this->calculateCube($input),
                'gr_hdr.ctnr_size'      => array_get($input, 'ctnr_size'),
            ]);

        return $result;
    }

    /**
     * @param $input
     * @return mixed
     */
    private function calculateCube($input) {
        $volume = array_get($input, 'volume');
        $volumeUnit = array_get($input, 'volume_unit');
        if (!$volume || !$volumeUnit) {
            return NULL;
        }
        if ($volumeUnit == 'volume') {
            return round($volume / 1728, 2);
        } else {
            return $volume;
        }
    }
}