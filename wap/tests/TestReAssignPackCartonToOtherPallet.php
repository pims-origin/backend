<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\AsnDtlModel;
use Dingo\Api\Routing\Helpers;
use App\Api\V1\Transformers\AsnListsCanSortV1Transformer;
use Seldat\Wms2\Models\VirtualCarton;
use Seldat\Wms2\Models as SM;
use Tymon\JWTAuth\Facades\JWTAuth;
use Namshi\JOSE\JWS;

class TestScanPalletManyTime extends TestCase
{

    //use DatabaseTransactions;
    use Helpers;

    private $whsId = 1;
    private $cusId = 31;
    private $palletId = 1;
    private $DB_ROLLBACK = true;
    public static $_caseCreateVTCarton = [];
    private $_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0ODkwMjMxMzIsImV4cCI6MTQ4OTEwOTUzMiwibmFtZSI6IkRhbyBUcmFuIiwidXNlcm5hbWUiOiJjc3JkdHIiLCJqdGkiOjc3fQ.mgAVy8ULskl6BAR-hcdl9FgJOt1_8PLSAVHCJg8w6P4";

    public function setUp()
    {
        parent::setUp();

        $token = str_replace('Bearer ', '', $this->_token);
        JWTAuth::setToken($token);
    }

    /**
     * Test Scan pallet many time with adding more carton
     */
    public function testReassignPackCarton()
    {
        $pallet1 = $this->createOutPallet();
        $pallet2 = $this->createOutPallet();
        $packCartonNum = $this->createPackCarton();

        $url = "/outbound/{$this->whsId}/pallet/assign-cartons";
        echo "[RURL] $url\n";
        $param = [
            'pallet' => $pallet1->plt_num,
            'packs' => $packCartonNum->pack_hdr_num
        ];
        $res = $this->call('PUT', $url, $param, [], [], ['HTTP_Authorization' => $this->_token]);
        $content = $res->content();
        $jsonObj = json_decode($content);
        $this->assertEquals(1, $jsonObj->status);
        echo "[DATA] {$content}\n";
        $this->refreshApplication();
        $param['pallet'] = $pallet2->plt_num;
        $res = $this->call('PUT', $url, $param, [], [], ['HTTP_Authorization' => $this->_token]);
        $content2 = $res->content();
        $jsonObj2 = json_decode($content2);
        echo "[DATA] {$content2}\n";
        $this->assertEquals(false, $jsonObj2->status);
    }

    private function createOutPallet()
    {
        $pltNum = strtoupper(uniqid('LPNORD-'));

        $outPallet = new SM\OutPallet();
        $outPallet->cus_id = 5;
        $outPallet->whs_id = 1;
        $outPallet->plt_num = $pltNum;
        $outPallet->ctn_ttl = 1;
        $outPallet->is_movement = 0;
        $outPallet->out_plt_sts = 'AC';
        $outPallet->created_by = 1;
        $outPallet->updated_by = 1;
        $outPallet->deleted = 0;

        if ($outPallet->save()) {
            return $outPallet;
        }

        return null;
    }

    public function createPackCarton()
    {
        $packHdrNum = strtoupper(uniqid("CTN-"));
        $carton = new SM\PackHdr();
        $carton->pack_hdr_num = $packHdrNum;
        $carton->seq = 1;
        $carton->cnt_id = 1024;
        $carton->whs_id = 1;
        $carton->cus_id = 5;
        $carton->carrier_name = '';
        $carton->sku_ttl = 1;
        $carton->piece_ttl = 50;
        $carton->pack_sts = 'NW';
        $carton->carrier_name = '';
        $carton->sts = 'I';
        $carton->ship_to_name = '';
        $carton->pack_type = 'CT';
        $carton->width = '5.00000';
        $carton->height = '5.00000';
        $carton->length = '5.00000';
        $carton->pack_ref_id = '4';
        $carton->is_print = '4';
        $carton->pack_dt_checksum = '262';
        $carton->odr_hdr_id = 4;
        $carton->save();
        if ($carton) {
            return $carton;
        }
        return null;
    }

}
