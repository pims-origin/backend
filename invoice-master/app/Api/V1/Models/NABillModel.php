<?php

namespace App\Api\V1\Models;

use App\Api\V1\Models\BillableHdrModel;
use App\Api\V1\Models\BillableDtlModel;
use App\Wms2\UserInfo\Data;
use Seldat\Wms2\Models\Item;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

class NABillModel extends AbstractModel
{
    protected $attributes;
    protected $whsId;
    protected $type;
    protected $quantity;
    protected $amount;
    protected $billableHdr;
    protected $billableDtl;
    protected $grHdrObj;

    public function __construct($attributes)
    {
        $this->attributes = $attributes;
//        $this->whsId = $whsId;
//        $this->refNum = $this->getRefNum();
//        $this->refNum = "";
        $this->type = 'NA'; // NA
        $this->billableHdr = new BillableHdrModel();
        $this->billableDtl = new BillableDtlModel();
    }

    public function getChargeCodeBySac($sac_code)
    {
        $items = DB::table('cus_sac')
            ->select(DB::raw('sac_id, sac, sac_name, range_from, range_to, unit_uom, price, discount, ROUND((price * (100 - discount)/100), 2) AS discount_price, block, range_uom, LOCATE("DOMESTIC", description) as domestic'))
            ->where([
                ['cus_id', $this->attributes['cus_id']],
                ['whs_id', $this->attributes['whs_id']],
                ['type', $this->type],
                ['deleted', 0],
            ])
            ->where("sac",$sac_code)
            ->first();

        return $items;
    }

    public function generate()
    {

        $userId = Data::getCurrentUserId();
        $detail_param = [];
        $header_param = [];
        DB::beginTransaction();
        foreach ($this->attributes['items'] as $key => $value) {
            $qty = $value['qty'];
            $qty = round($qty, 2);
            $charge = $this->getChargeCodeBySac($value['sac_code']);
            if(empty($charge)) {
                $msg = sprintf("Sac code %s is not exit in this customer",$value['sac_code']);
                return $this->response->errorBadRequest($msg);
            }
            if ($qty > 0) {
                $amount = round($qty * $charge->price *(100 + $value['discount']) / 100,2);
                $detail_param = [
                    'ba_id' => '',
                    'cus_id' => $this->attributes['cus_id'],
                    'whs_id' => $this->attributes['whs_id'],
                    'ba_type' => $this->type,
                    'sac_id' => $charge->sac_id,
                    'sac' => $charge->sac,
                    'sac_name' => $charge->sac_name,
                    'ref_num' => $value['ref_num'],
                    'qty' => $qty,
                    'unit_price' => $charge->price,
                    'discount' => ($value['discount'] != null)? $value['discount']: 0,
                    'amount' => $amount,
                    'description' => $value['description'],
                    "created_at"    => time(),
                    "updated_at"    => time(),
                    "created_by"    => $userId,
                    "updated_by"    => $userId,
                    "deleted"       => 0,
                    "deleted_at"    => 915148800
                ];

                $header_param = [
                    'cus_id' => $this->attributes['cus_id'],
                    'whs_id' => $this->attributes['whs_id'],
                    'ref_num' => $value['ref_num'],
                    'ba_from' => time(),
                    'ba_to' => time(),
                    'ba_ttl' => $qty,
                    'amount_ttl' => $amount,
                    'ba_type' => $this->type,
                    'ba_period' => 'DAY',
                ];

                $header = $this->billableHdr->create($header_param);
                $detail_param['ba_id'] = $header->ba_id;
                DB::table('billable_dtl')->insert($detail_param);
            }
        }
        DB::commit();
        return 0;
    }

//    public function prepareHeaderData($value)
//    {
//        # billable header
//        return [
//            'cus_id' => $this->attributes['cus_id'],
//            'whs_id' => $this->attributes['whs_id'],
//            'ref_num' => $value['ref_num'],
//            'ba_from' => time(),
//            'ba_to' => time(),
//            'ba_ttl' => $this->quantity,
//            'amount_ttl' => $this->amount,
//            'ba_type' => $this->type,
//            'ba_period' => 'DAY',
//        ];
//    }

    public function saveBillableItem()
    {
        $detail = $this->generate();
        return 0;
    }

    public function getChargeCodeListByKey($key,$limit = 20)
    {
        $items = DB::table('cus_sac')
            ->select(DB::raw('sac_id, sac, sac_name, range_from, range_to, unit_uom, price, discount, ROUND((price * (100 - discount)/100), 2) AS discount_price, block, range_uom, LOCATE("DOMESTIC", description) as domestic'))
            ->where([
                ['cus_id', $this->attributes['cus_id']],
                ['whs_id', $this->attributes['whs_id']],
                ['type', $this->type],
                ['deleted', 0],
            ])
            ->where("sac","like",'%'.$key.'%')
            ->limit($limit)
            ->get()->toArray();

        return $items;
    }

}
