<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Status;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;

/**
 * Class OrderStatusController
 *
 * @package App\Api\V1\Controllers
 */
class OrderStatusController extends AbstractController
{
    protected $statusType = [
        'csr'               => ['NW'],
        'allocate'          => ['NW', 'AL', 'PAL'],
        'wave-pick'         => ['PK', 'PPK', 'PD', 'PIP', 'AL', 'PAL'],
        'packing'           => ['PD', 'PIP', 'PN', 'PPA', 'PA', 'PAP'],
        'pallet-assignment' => ['PA', 'PAP'],
//        'shipping'          => ['RS', 'SS','SH', 'ST', 'PSH'],
        'shipping'          => ['RS', 'SS','SH'],
        'cancel-order'      => ['CC', 'PCC', 'RMA']
    ];

    /**
     * @param Request $request
     *
     * @return array|null|string
     */
    public function show(Request $request)
    {
        $input = $request->getQueryParams();
        $inputType = array_get($input, 'type', '');
        if (!$inputType){
            return $this->response->errorBadRequest('Type is required');
        }

        $data = [];
        $types = Status::get("Order-status");
        $userId = Data::getCurrentUserId();
        $userMeta = DB::table('user_meta')
                            ->where('user_id', $userId)
                            ->where('qualifier', 'OSF')
                            ->first();

        if (!empty($types) && is_array($types)) {
            foreach ($types as $key => $value) {
                if (!empty($input['type']) && !empty($this->statusType[$input['type']]) &&
                    !in_array($key, $this->statusType[$input['type']])
                ) {
                    continue;
                }
                if($key == 'ST' || $key == 'PSH'){
                    continue;
                }
                $data[] = [
                    "key"   => $key,
                    "value" => $value,
                    "checked" => false
                ];
            }
        }
        if ($userMeta && $inputType ){
            $userValue = collect(json_decode($userMeta['value']));
            $userValue = $userValue->get($inputType, []);
            if ($userValue){
                $userValue = collect($userValue)->keyBy('key');
            }
            $data = collect($data)->keyBy('key');
            $data = $data->merge($userValue)->values();
        }
        return ['data' => $data];
    }
}
