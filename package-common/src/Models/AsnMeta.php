<?php

namespace Seldat\Wms2\Models;

class AsnMeta extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'asn_meta';

    protected $primaryKey = 'qualifier';

    /**
     * @var array
     */
    protected $fillable = [
        'qualifier',
        'asn_hdr_id',
        'value',
    ];


}
