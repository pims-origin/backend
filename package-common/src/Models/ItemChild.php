<?php

namespace Seldat\Wms2\Models;


class ItemChild extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'item_child';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;

    public $incrementing = false;

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'parent',
        'child',
        'type',
        'origin',
    ];

    public function parentItem()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'parent', 'item_id');
    }

//    public function childItem()
//    {
//        return $this->belongsTo(__NAMESPACE__ . '\ItemCat', 'child', 'item_id');
//    }
//
//    public function originItem()
//    {
//        return $this->belongsTo(__NAMESPACE__ . '\ItemCat', 'origin', 'item_id');
//    }
}
