<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Models;


use App\CustomObject;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\Profiler;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use Psr\Http\Message\ServerRequestInterface as Request;

class LocationModel extends AbstractModel
{
    protected $warehouseZone;

    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        parent::__construct($model);
    }

    /**
     * @param int $warehouseId
     * @param int $locationId
     *
     * @return int
     */
    public function deleteLocation($warehouseId, $locationId)
    {
        return $this->model
            ->where('loc_id', $locationId)
            ->where('loc_whs_id', $warehouseId)
            ->delete();
    }

    /**
     * @param int $warehouseId
     * @param array $locationIds
     *
     * @return mixed
     */
    public function deleteMassLocation($warehouseId, array $locationIds)
    {
        return $this->model
            ->whereIn('loc_id', $locationIds)
            ->where('loc_whs_id', $warehouseId)
            ->delete();
    }

    /**
     * @param $zoneId
     * @param array $locationId
     *
     * @return mixed
     */
    public function removeZoneId($zoneId, array $locationId)
    {
        return $this->model->where('loc_zone_id', $zoneId)->whereIn('loc_id',
            $locationId)->orWhereIn('parent_id', $locationId)->update(['loc_zone_id' => null]);
    }

    /**
     * Search
     *
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool|false $ignoreLocTypeCode
     * @param array $except
     * @param boolean $hasPallet
     *
     * @return mixed
     */
    public function search(
        $attributes = [],
        $with = [],
        $limit = null,
        $ignoreLocTypeCode = false,
        $except = [],
        $hasPallet = null
    ) {
        $query = $this->make($with);
        $query->select([
            'location.loc_id',
            'location.loc_code',
            'location.loc_alternative_name',
            'location.loc_type_id',
            'location.loc_width',
            'location.loc_length',
            'location.loc_height',
            'location.loc_available_capacity',
            'location.loc_weight_capacity',
            'location.loc_min_count',
            'location.loc_max_count',
            'location.loc_zone_id',
            'location.loc_whs_id',
            'location.loc_sts_code',
            'location.rfid',
            'location.spc_hdl_code',
            'location.spc_hdl_name',
            'customer.cus_name',

            'location.row',
            'location.aisle',
            'location.level',
        ]);
        $query->leftJoin('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id');
        $query->leftJoin('customer', 'customer.cus_id', '=', 'customer_zone.cus_id');

        $query->where('location.loc_whs_id', $attributes['warehouseId']);
        $query->whereNull('location.parent_id');
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes['rfid'])) {
            $query->where('location.rfid', 'like', '%' . $attributes['rfid'] . '%');
        }

        if (!empty($attributes['loc_code'])) {
            $query->where(function ($q1) use ($attributes) {
                $attributes['loc_code'] = is_array($attributes['loc_code'])
                    ? $attributes['loc_code']
                    : [$attributes['loc_code']];
                foreach ($attributes['loc_code'] as $code) {
                    $code = SelStr::escapeLike($code);
                    $q1->orWhere('location.loc_code', 'like', "%" . $code . "%");
                    $prefix_loc = strtoupper(substr($code,  0, 3));
                    if (in_array($prefix_loc, ['BIN', 'ROW'])) {
                        $q1->orWhere('location.loc_code', 'like', "%" . str_replace($prefix_loc, $prefix_loc . ' ', $code) . "%");
                    }
                }
            });
        }

        if ($hasPallet !== null) {
            $query->leftJoin('pallet', 'pallet.loc_id', '=', 'location.loc_id');
            if (intval($hasPallet) === 1) {
                $query->whereNotNull('pallet.plt_id');
            } else {
                $query->whereNull('pallet.plt_id');
            }
        }

        if (!empty($attributes['loc_alternative_name'])) {
            $query->where(function ($q) use ($attributes) {
                $attributes['loc_alternative_name'] = is_array($attributes['loc_alternative_name'])
                    ? $attributes['loc_alternative_name']
                    : [$attributes['loc_alternative_name']];
                foreach ($attributes['loc_alternative_name'] as $name) {
                    $q->orWhere('location.loc_alternative_name', 'like', "%" . SelStr::escapeLike($name) . "%");
                }
            });
        }

        if (!empty($attributes['loc_type_id']) ||
            !empty($attributes['zone_type_id']) ||
            !empty($attributes['loc_zone_id'])
        ) {
            if (!empty($attributes['loc_type_id'])) {
                $query->whereIn('location.loc_type_id', $attributes['loc_type_id']);
            }

            if (!empty($attributes['loc_zone_id']) || !empty($attributes['zone_type_id'])) {

                $attributes['loc_zone_id'] = !empty($attributes['loc_zone_id']) ? $attributes['loc_zone_id'] : [];

                if (!empty($attributes['loc_zone_id'][0]) && $attributes['loc_zone_id'][0] == 'empty') {
                    $query->where(function ($q) {
                        $q->orWhereNull('location.loc_zone_id');
                        $q->orWhere('location.loc_zone_id', 0);
                    });
                } else {
                    if (!empty($attributes['zone_type_id'])) {
                        $warehouseZoneModel = new WarehouseZoneModel();
                        $warehouseZone = $warehouseZoneModel
                            ->getAllZoneTypeIds($attributes['zone_type_id'])
                            ->toArray();
                        $zoneIds = array_pluck($warehouseZone, 'zone_id', 'zone_id');

                        if (!empty($attributes['loc_zone_id'])) {
                            $zoneIds = array_intersect($zoneIds, $attributes['loc_zone_id']);
                        }

                        $attributes['loc_zone_id'] = $zoneIds;
                    }

                    $query->whereIn('location.loc_zone_id', $attributes['loc_zone_id']);
                }
            }
        }

        if ($ignoreLocTypeCode) {
            if (!is_array($ignoreLocTypeCode)) {
                $ignoreLocTypeCode = [$ignoreLocTypeCode];
            }
            $query->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
                ->whereNotIn('loc_type.loc_type_code', $ignoreLocTypeCode);

        }

        if (!empty($attributes['loc_type_code'])) {
            if (!$ignoreLocTypeCode) {
                $query->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id');
            }
            $query->where('loc_type.loc_type_code', $attributes['loc_type_code']);
        }
        if (!empty($attributes['spc_hdl_code'])) {
            $query->where('location.spc_hdl_code', $attributes['spc_hdl_code']);
        }

        if (isset($attributes['item_id']) && !empty($attributes['item_id'])) {
            $query->join('cartons', 'cartons.loc_id', '=', 'location.loc_id');
            $query->where('cartons.item_id', '=', $attributes['item_id']);
        }

        if (!empty($attributes['status'])) {
            $query->where('location.loc_sts_code', $attributes['status']);
        }

        if (!empty($except) && is_array($except)) {
            $exceptIds = [];
            foreach ($except as $table => $fieldSelect) {
                try {
                    $temp = DB::table($table)
                        ->select($fieldSelect)
                        ->whereNotNull($fieldSelect)
                        ->where(['location.deleted' => 0, 'location.deleted_at' => 915148800])
                        ->get();
                    $locIds = array_pluck($temp, $fieldSelect);
                    $exceptIds = array_merge($exceptIds, $locIds);
                } catch (\PDOException $e) {
                    continue;
                }
            }

            if (!empty($exceptIds)) {
                $exceptIds = array_values(array_unique($exceptIds));
                $query->whereNotIn('location.loc_id', $exceptIds);
            }
        }

        if (!empty($attributes['aisle'])) {
            $query->where('location.aisle', $attributes['aisle']);
        }
        if (!empty($attributes['level'])) {
            $query->where('location.level', $attributes['level']);
        }
        if (!empty($attributes['row'])) {
            $query->where('location.row', $attributes['row']);
        }

        $query->groupBy('location.loc_id');

        $this->model->filterData($query);

        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * Search
     *
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool|false $ignoreLocTypeCode
     * @param array $except
     * @param boolean $hasPallet
     *
     * @return mixed
     */
    public function getLocationsByCustomer($attributes = [], $with = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query = $this->make($with);
        $query->select([
            'location.loc_id',
            'location.loc_code',
            'location.loc_alternative_name',
            'location.loc_sts_code'
        ])
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('loc_type.loc_type_code', '!=', 'ECO')
            ->whereNotIn('location.loc_sts_code', ['LK', 'IA'])
            ->whereRaw('NOT EXISTS (SELECT pallet.loc_id FROM pallet WHERE pallet.loc_id = location.loc_id AND pallet.loc_id IS NOT NULL)')
            ->where('loc_whs_id', $attributes['whs_id'])
            ->where('customer_zone.cus_id', $attributes['cus_id']);

        if (!empty($attributes['loc_code'])) {
            $query->where('location.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");
        }

        //#6091 task
        $query = $query->orderBy('location.aisle', 'ASC')
            ->orderBy('location.level', 'ASC')
            ->orderBy('location.row', 'ASC')
            ->orderBy('location.bin', 'ASC');

        $query->groupBy('location.loc_id');
        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    public function allExceptZone($warehouseId, $params = [], $with = [], $limit = 20)
    {
        $columns = [
            "location.loc_id",
            "location.loc_code",
            "location.loc_alternative_name",
            "location.loc_available_capacity",
            "location.loc_length",
            "location.loc_width",
            "location.loc_height",
            "location.loc_weight_capacity",
            "location.loc_min_count",
            "location.loc_max_count",
            "loc_type.loc_type_name",
            "loc_status.loc_sts_name AS loc_sts_code_name",
        ];

        $query = $this->make($with);
        //$query->select($columns);
        $query->select(DB::RAW("count(*)"));

        $query->where('location.loc_whs_id', $warehouseId);
        $query->whereNull("location.loc_zone_id");

        if (!empty($params['loc_code'])) {
            $query->where('location.loc_code', 'LIKE', $params['loc_code'] . '%');
        }

        if (!empty($params['zone_id'])) {
            $query->orWhere('loc_zone_id', $params['zone_id']);
        }

        $this->model->filterData($query);

        $total = $this->naturalExec($query->toSql(), $query->getBindings(), \PDO::FETCH_COLUMN)[0];
        $totalPages = ceil($total / $limit);
        $curPage = $params['page'] ? $params['page'] : 1;

        $query->select($columns)
            ->leftJoin('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
            ->leftJoin('loc_status', 'location.loc_sts_code', '=', 'loc_status.loc_sts_code')
            ->whereNull('parent_id')
            ->orderBy('location.loc_id', 'DESC')
            ->limit($limit)
            ->offset(($curPage - 1) * $limit);

        $rs = $this->naturalExec($query->toSql(), $query->getBindings());
        $meta = [
            "pagination" => [
                "total"        => (int)$total,
                "count"        => (int)$limit,
                "per_page"     => (int)$limit,
                "current_page" => (int)$curPage,
                "total_pages"  => (int)$totalPages
            ]
        ];

        return [
            'data' => $rs,
            'meta' => $meta
        ];
    }


    public function loadByZone($warehouseId, $zoneId, $input, $with = [])
    {
        $limit = $input['limit'] ?? 20;

        $query = $this->make($with);
        $query->where('loc_whs_id', $warehouseId);
        $query->where('loc_zone_id', $zoneId);
        $query->whereNull('parent_id');

        if (!empty($input['loc_code'])) {
            $query->where('loc_code', 'LIKE', $input['loc_code'] . '%');
        }

        $this->model->filterData($query);
        $models = $query->orderBy('loc_id', 'DESC')
                        ->paginate($limit);

        if ($models) {
            $arrLocIds = array_pluck($models, 'loc_id', null);
            $arrLocExistCartons = array_pluck(Carton::select('item_id', 'loc_id')
                ->whereIn('loc_id', $arrLocIds)
                ->where('whs_id', $warehouseId)
                ->groupBy('loc_id')
                ->get(), 'item_id', 'loc_id');

            foreach ($models as $model) {
                $model->has_item = !empty($arrLocExistCartons[$model->loc_id])
                    ? 1 : 0;
            }
        }

        return $models;
    }

    public function getLocationByWarehouse($warehouseId)
    {
        $query = $this->make();
        $this->model->filterData($query);
        $query->where('loc_whs_id', $warehouseId);
        $models = $query->get();

        return $models;
    }

    public function loadByZoneIds($zoneIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('loc_zone_id', $zoneIds);
        $this->model->filterData($query);
        // Get
        $models = $query->get();

        return $models;
    }

    public function findWhereIn($warehouseId, $arrLocIds, $with = [])
    {
        $query = $this->make($with);
        $query->whereIn('loc_id', $arrLocIds)
            ->where('loc_whs_id', $warehouseId);
        $this->model->filterData($query);
        // Get
        $models = $query->get();

        return $models;
    }

    public function loadWhsLayout($whsId, $with = [], $input = [])
    {
        $query = $this->make($with)
            ->where('loc_whs_id', $whsId)
            ->orderBy('aisle')
            ->orderBy('row');
        //->orderBy('loc_alternative_name');
        // Get
        $models = $query->get();

        return $models;
    }

    public function loadWhsLayoutV2($whsId,  $input = [], $aisleList = [], $cusId = null)
    {
        $query = $this->model
            ->where('location.loc_whs_id', $whsId)
            ->whereNotNull('location.aisle')
            ->whereNull('location.parent_id')
            ->whereIn('location.aisle', $aisleList);

        if (!empty($input['loc_code'])) {
            $query->where('location.loc_code', 'LIKE', $input['loc_code'] . '%');
        }
        if (!empty($input['spc_hdl_code'])) {
            $query->where('location.spc_hdl_code', $input['spc_hdl_code'] );
        }

        if (!empty($input['sku'])) {
            $sku = $input['sku'];
            $query->whereHas('cartons', function ($query) use ($sku) {
                $query->where('sku', 'LIKE', $sku . '%');
            });
        }

        if ($cusId) {
            $query->whereHas('cartons', function ($query) use ($cusId) {
                $query->where('cus_id', '=', $cusId);
            });
        }

        if (!empty($input['loc_type_code'])) {
            $loc_type_code = $input['loc_type_code'];
            $query->whereHas('locationType', function ($query) use ($loc_type_code) {
                $query->where('location.loc_type_code', $loc_type_code);
            });
        }

        if (!empty($input['type'])) {
            if ($input['type'] == 'zone' && !empty($input['code'])) {
                $code = $input['code'];
                $query->whereHas('zone', function ($query) use ($code) {
                    $query->where('zone_code', $code);
                });
            }
        }
        // Get
        $models = $query->get();

        return $models;
    }

    public function getCustomerInWarehouseLayout($whsId)
    {
        $layouts = DB::table('cartons')
            ->select(['location.loc_id', 'location.loc_code', 'cartons.cus_id', 'customer_color.cl_code', 'customer.cus_name', 'customer.cus_code'])
            ->join('location', 'location.loc_id', '=','cartons.loc_id')
            ->leftJoin('customer_color', 'customer_color.cus_id', '=', 'cartons.cus_id')
            ->leftJoin('customer', 'customer.cus_id', '=', 'customer_color.cus_id')
            ->where('cartons.whs_id', $whsId)
            ->whereNotNull('cartons.loc_code')
            ->whereNull('location.parent_id')
            ->groupBy('cartons.loc_code', 'cartons.cus_id')
            ->get();

        return $layouts;
    }

    public function getCustomerInWarehouseLayout2($whsId, $aisleList, $cusId)
    {
        $result = [];
        $locationList = DB::table('location')->where('deleted', 0)->whereIn('aisle', $aisleList)->whereNull('parent_id')->pluck('loc_id');
        foreach (array_chunk($locationList, 200) as $locIds) {
            $query = DB::table(DB::raw('cartons FORCE INDEX(loc_id)'))
                ->select(['cartons.loc_id', 'cartons.loc_code', 'cartons.cus_id'])
                ->where('cartons.whs_id', $whsId)
                ->where('cartons.deleted', 0)
                ->whereIn('ctn_sts', ['AC', 'RG', 'LK'])
                ->whereNotNull('cartons.loc_id')
                ->whereIn('cartons.loc_id', $locIds)
                ->groupBy('cartons.loc_id', 'cartons.cus_id');
            if ($cusId) {
                $query = $query->where('cartons.cus_id', $cusId);
            }
            array_push($result, $query->get());
        }
        $data = [];
        foreach ($result as $value) {
            foreach ($value as $val) {
                $data[] = $val;
            }
        }
        return $data;
    }

    public function getCustomerInWarehouseLayout3($locations,$whsId)
    {
        $locationIds=collect($locations)->pluck('loc_id');

		$query = DB::table(DB::raw('cartons FORCE INDEX(loc_id)'))
        ->select(['cartons.loc_id',  'cartons.cus_id',DB::raw('COUNT(DISTINCT cartons.cus_id) AS cnt')])
		->join('location','location.loc_id','=','cartons.loc_id')
        ->where('cartons.deleted', 0)
        ->where('cartons.whs_id',$whsId)
        ->whereIn('cartons.ctn_sts', ['AC', 'RG', 'LK'])
        ->whereIn('cartons.loc_id', $locationIds)
        ->groupBy('cartons.loc_id');
		
        $data=$query->get();
        $data=collect($data)->keyBy('loc_id');
        //Profiler::getQueryLog(false);
        return $data;
    }

    public function getCustomerColor()
    {
        $layouts = DB::table('customer')
            ->where('customer.deleted', 0)
            ->where('customer_color.deleted', 0)
            ->leftJoin('customer_color', 'customer_color.cus_id', '=', 'customer.cus_id')
            ->select(['customer_color.cl_code', 'customer.cus_name', 'customer.cus_code', 'customer.cus_id'])
            ->get();
        $layouts=collect($layouts)->keyBy('cus_id');

        return $layouts;
    }

    public function loadCustomerOrZoneForWarehouseLayout($whsId, $with = [])
    {
        $query = $this->make($with)
            ->where('loc_whs_id', $whsId)
            ->whereNotNull('aisle')
            ->orderBy('aisle');

        // Get
        $models = $query->get();

        return $models;
    }

    public function loadAisleListPagination($whsId, $input,$mode)
    {
        $query = $this->model
            ->where('location.loc_whs_id', $whsId)
            ->whereNotNull('location.aisle')
            ->where('location.deleted',0)
            ->whereNull('location.parent_id');

        if (!empty($input['loc_code'])) {
            $query->where('location.loc_code', 'LIKE', $input['loc_code'] . '%');
        }
        if (!empty($input['spc_hdl_code'])) {
            $query->where('location.spc_hdl_code', $input['spc_hdl_code'] );
        }

        if (!empty($input['sku']) || !empty($input['cus_id'])) {
            $query->join('cartons', 'cartons.loc_id','=','location.loc_id');
            if(!empty($input['sku']))
                $query->where('cartons.sku', 'LIKE', $input['sku'] . '%');
            if(!empty($input['cus_id']))
                $query->where('cartons.cus_id', '=',$input['cus_id']);
        }

        if (!empty($input['aisle']) ) {
            $query->whereIn('location.aisle', $input['aisle']);
        }

        /*if (!empty($input['loc_type_code'])) {
            $loc_type_code = $input['loc_type_code'];
            $query->whereHas('locationType', function ($query) use ($loc_type_code) {
                $query->where('loc_type_code', $loc_type_code);
            });
        }

        if (!empty($input['type'])) {
            if ($input['type'] == 'zone' && !empty($input['code'])) {
                $code = $input['code'];
                $query->whereHas('zone', function ($query) use ($code) {
                    $query->where('zone_code', $code);
                });
            }
        }*/
        if($mode=='count')
            return $query->select(DB::raw('COUNT(DISTINCT location.aisle) AS cnt'))->first()['cnt']??0;
        else if($mode=='getAisle') {
            $query->select('location.aisle')->groupBy('location.aisle');
            $page = $input['page'] ?? 1;
            $limit = $input['limit'] ?? 20;
            $query->limit($limit);
            $query->offset($page * $limit - $limit);
            return array_pluck($query->get()->toArray(),'aisle');
        } else if($mode=='getLoc'){
            $data=$query->get()->toArray();
            //Profiler::getQueryLog(false);
            return $data;
        }
    }

    public function loadWhsLayoutByWhsId($whsId)
    {
        $query = $this->model
            ->select([
                'customer.cus_id',
                'location.loc_id',
                'location.loc_zone_id',
                'customer.cus_code',
                'customer.cus_name',
                'loc_type.loc_type_code',
                'location.loc_alternative_name',
                DB::raw('pallet.loc_id AS pallet'),
                'customer_color.cl_name',
                'customer_color.cl_code',
                'zone.zone_code',
                'zone.zone_name',
                'zone.zone_color'
            ])
            ->leftJoin('pallet', 'pallet.loc_id', '=', 'location.loc_id')
            ->leftJoin('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->join('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->join('customer', 'customer.cus_id', '=', 'customer_zone.cus_id')
            ->join('customer_color', 'customer.cus_id', '=', 'customer_color.cus_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('location.loc_whs_id', $whsId)
            ->orderBy('loc_alternative_name');
        $models = $query->get();

        return $models;
    }

    public function readWhsLayout($whsId, $zone, $with = [])
    {
        $query = $this->make($with)
            ->where('loc_whs_id', $whsId)
            ->where('loc_alternative_name', 'like', SelStr::escapeLike($zone) . "-%")
            ->orderBy('loc_alternative_name');
        // Get
        $models = $query->get();

        return $models;
    }

    /**
     * @param $locIds
     * @param $zoneId
     * @param $whsId
     *
     * @return mixed
     */
    public function updateLocToZone($locIds, $zoneId, $whsId)
    {
        return $this->model->whereIn('loc_id', $locIds)->orWhereIn('parent_id', $locIds)->update([
            'loc_whs_id'  => $whsId,
            'loc_zone_id' => $zoneId
        ]);
    }

    public function isRfid(&$rfid, $whsId)
    {
        $rfid = trim(strtoupper($rfid));
        $rfid = str_replace(" ", "", $rfid);
        $rfid = str_replace(".", "", $rfid);
        $rfid = str_replace(",", "", $rfid);

        if (strlen($rfid) != 24) {
            return false;
        }

        $rfFirst = substr($rfid, 0, 8);
        $rfMiddle = substr($rfid, 8, 4);

        // First
        if ($rfFirst !== "DDDDDDDD") {
            return false;
        }

        // Middle
        if (!is_numeric($rfMiddle)) {
            return false;
        }

        if (empty((int)$rfMiddle) || (int)$rfMiddle != $whsId) {
            return false;
        }

        return true;
    }

    /**
     * @param array $input
     *
     * @return array
     */
    public function OccupancyPCTDashboard($input = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($input);

        $locSts = [
            'AC' => [Status::getByKey("LOCATION_STATUS", 'ACTIVE')],
            'IA' => [Status::getByKey("LOCATION_STATUS", 'INACTIVE')],
            'LK' => [Status::getByKey("LOCATION_STATUS", 'LOCKED')],
        ];

        foreach ($locSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $locSts[$key] = "'" . implode("', '", $item) . "'";
        }

        $predis = Data::getInstance();
        $currentWH = $predis::getCurrentWhsId();
        $cusIds = $predis->getCustomersByWhs($currentWH);

        $query = DB::table('location')
            ->select(DB::raw("
                count(DISTINCT location.loc_id) as total,
                COALESCE(COUNT(DISTINCT CASE WHEN loc_sts_code = {$locSts['AC']}
                    and pl.loc_id is null AND loc_zone_id IS NOT NULL
                    THEN location.loc_id ELSE NULL END), 0) AS AVAILABLE,
                COALESCE(COUNT(DISTINCT CASE WHEN loc_sts_code = {$locSts['IA']}
                    AND loc_zone_id IS NOT NULL
                    THEN location.loc_id ELSE NULL END), 0) AS INACTIVE,
                COALESCE(COUNT(DISTINCT CASE WHEN loc_sts_code = {$locSts['LK']}
                    AND loc_zone_id IS NOT NULL
                    THEN location.loc_id ELSE null END), 0) AS LOCKED,
                COALESCE(COUNT(DISTINCT CASE WHEN loc_zone_id IS NULL
                    THEN location.loc_id ELSE NULL END), 0) AS UNZONE
            "))

            ->where('location.deleted', 0)
            ->where('location.loc_whs_id', $currentWH)
            ->whereNull('location.parent_id')
            ->leftJoin('pallet as pl', 'pl.loc_id', '=', 'location.loc_id');

        if (isset($attributes['loc_type'])) {
            $query->leftJoin('loc_type as lt', 'lt.loc_type_id', '=', 'location.loc_type_id')
                ->where('location.loc_type_id', $attributes['loc_type']);
        }

        $model = $query->first();

        //Count pallet
        $modelPallet = $this->countPalletNotShipping($currentWH, $cusIds, array_get($attributes, 'loc_type'));

        //Reassign count pallet if type = shipping ~ 7
        if (isset($attributes['loc_type']) && $attributes['loc_type'] == 7) {
            $modelPallet = $this->countPalletShipping($currentWH, $cusIds, $attributes['loc_type']);
        }

        if (!isset($attributes['loc_type'])) {
            $modelPalletTmp = $this->countPalletShipping($currentWH, $cusIds);
            if (empty($modelPallet['total_pallet'])) {
                $modelPallet['total_pallet'] = array_get($modelPalletTmp, 'total_pallet', 0);
            } else {
                $modelPallet['total_pallet'] += array_get($modelPalletTmp, 'total_pallet', 0);
            }
        }

        $total = !empty($model['total']) ? $model['total'] : 0;
        $available = !empty($model['AVAILABLE']) ? $model['AVAILABLE'] : 0;
        $unzone = !empty($model['UNZONE']) ? $model['UNZONE'] : 0;
        $inactive = !empty($model['INACTIVE']) ? $model['INACTIVE'] : 0;
        $locked = !empty($model['LOCKED']) ? $model['LOCKED'] : 0;
        $totalPallet = !empty($modelPallet['total_pallet']) ? $modelPallet['total_pallet'] : 0;

        return [
            'total_loc'            => intval($total),
            'avail_loc'            => intval($available),
            'unzone_loc'           => intval($unzone),
            'inactive_loc'         => intval($inactive),
            'locked_loc'           => intval($locked),
            'total_pallet'         => intval($totalPallet),
            'percent_avail_loc'    => $total != 0 ? floatval(($available / $total) * 100) : 0,
            'percent_unzone_loc'   => $total != 0 ? floatval(($unzone / $total) * 100) : 0,
            'percent_inactive_loc' => $total != 0 ? floatval(($inactive / $total) * 100) : 0,
            'percent_locked_loc'   => $total != 0 ? floatval(($locked / $total) * 100) : 0,
            'percent_total_pallet' => $total != 0 ? floatval(($totalPallet / $total) * 100) : 0,
        ];
    }

    public function occupancyPCTDashboardByCus($input = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($input);

        $locSts = [
            'AC' => [Status::getByKey("LOCATION_STATUS", 'ACTIVE')],
            'IA' => [Status::getByKey("LOCATION_STATUS", 'INACTIVE')],
            'LK' => [Status::getByKey("LOCATION_STATUS", 'LOCKED')],
        ];

        foreach ($locSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $locSts[$key] = "'" . implode("', '", $item) . "'";
        }

        $predis = Data::getInstance();
        $currentWH = $predis::getCurrentWhsId();
        $cusId = [!empty($attributes['cus_id']) ? $attributes['cus_id'] : null];

        $query = DB::table('location')
            ->select(DB::raw("
                count(DISTINCT location.loc_id) as total,
                COALESCE(COUNT(DISTINCT CASE WHEN loc_sts_code = {$locSts['AC']}
                    AND pl.loc_id is null 
                    AND loc_zone_id IS NOT NULL
                    THEN location.loc_id ELSE NULL END), 0) AS AVAILABLE,
                COALESCE(COUNT(DISTINCT CASE WHEN loc_sts_code = {$locSts['IA']}
                    AND loc_zone_id IS NOT NULL
                    THEN location.loc_id ELSE NULL END), 0) AS INACTIVE,
                COALESCE(COUNT(DISTINCT CASE WHEN loc_sts_code = {$locSts['LK']}
                    AND loc_zone_id IS NOT NULL
                    THEN location.loc_id ELSE NULL END), 0) AS LOCKED,
                COALESCE(COUNT(DISTINCT CASE WHEN loc_zone_id IS NULL
                    THEN location.loc_id ELSE NULL END), 0) AS UNZONE
            "))
            ->where('location.deleted', 0)
            ->leftJoin('pallet as pl', 'pl.loc_id', '=', 'location.loc_id');

        if (isset($attributes['cus_id'])) {
            $query->leftJoin('customer_warehouse as cw', 'cw.whs_id', '=', 'location.loc_whs_id')
            ->where('cw.cus_id', $attributes['cus_id']);
        }

        $model = $query->first();

        //Count pallet
        $modelPallet = $this->countPalletNotShipping($currentWH, $cusId, array_get($attributes, 'loc_type'));

        //Reassign count pallet if type = shipping ~ 7
        if (isset($attributes['loc_type']) && $attributes['loc_type'] == 7) {
            $modelPallet = $this->countPalletShipping($currentWH, $cusId, $attributes['loc_type']);
        }

        if (!isset($attributes['loc_type'])) {
            $modelPalletTmp = $this->countPalletShipping($currentWH, $cusId);
            if (empty($modelPallet['total_pallet'])) {
                $modelPallet['total_pallet'] = array_get($modelPalletTmp, 'total_pallet', 0);
            } else {
                $modelPallet['total_pallet'] += array_get($modelPalletTmp, 'total_pallet', 0);
            }
        }

        $total = !empty($model['total']) ? $model['total'] : 0;
        $available = !empty($model['AVAILABLE']) ? $model['AVAILABLE'] : 0;
        $unzone = !empty($model['UNZONE']) ? $model['UNZONE'] : 0;
        $inactive = !empty($model['INACTIVE']) ? $model['INACTIVE'] : 0;
        $locked = !empty($model['LOCKED']) ? $model['LOCKED'] : 0;
        $totalPallet = !empty($modelPallet['total_pallet']) ? $modelPallet['total_pallet'] : 0;

        return [
            'total_loc'            => intval($total),
            'avail_loc'            => intval($available),
            'unzone_loc'           => intval($unzone),
            'inactive_loc'         => intval($inactive),
            'locked_loc'           => intval($locked),
            'total_pallet'         => intval($totalPallet),
            'percent_avail_loc'    => $total != 0 ? floatval(($available / $total) * 100) : 0,
            'percent_unzone_loc'   => $total != 0 ? floatval(($unzone / $total) * 100) : 0,
            'percent_inactive_loc' => $total != 0 ? floatval(($inactive / $total) * 100) : 0,
            'percent_locked_loc'   => $total != 0 ? floatval(($locked / $total) * 100) : 0,
            'percent_total_pallet' => $total != 0 ? floatval(($totalPallet / $total) * 100) : 0,
        ];
    }

    public function occupancyPCTDashboardStatus($input = [])
    {
        $attributes = SelArr::removeNullOrEmptyString($input);
        $predis = Data::getInstance();
        $currentWH = $predis::getCurrentWhsId();
        $inputStatusCode = array_get($attributes, 'loc_sts_code', '');

        $query = DB::table('location')
            ->select(
                'loc_type.loc_type_name',
                'loc_type.loc_type_id',
                DB::raw('count(location.loc_type_id) as quantity')
            )
            ->where('location.loc_whs_id', $currentWH)
            ->where('location.deleted', 0)
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id');

        if ($this->isLocStatusOccupied($inputStatusCode)) {
            $query->join('pallet', 'pallet.loc_id', '=', 'location.loc_id');
        }

        if ($this->isLocStatusUnzone($inputStatusCode)) {
            $query->whereNull('loc_zone_id');
        }

        if ($this->isLocStatusValid($inputStatusCode)) {
            $query->where('loc_sts_code', $attributes['loc_sts_code'])
                ->whereNotNull('loc_zone_id');
        }

        $query->groupBy('location.loc_type_id');

        $locations = $query->get();
        $locations = collect($locations)->keyBy('loc_type_name');

        $locTypes = DB::table('loc_type')->get();
        foreach ($locTypes as $type) {
            $typeName = $type['loc_type_name'];
            if (!isset($locations[$typeName])) {
                $locations[$typeName] = [
                    'loc_type_id'   => $type['loc_type_id'],
                    'loc_type_name' => $typeName,
                    'quantity'      => 0
                ];
            }
        }

        $total = $locations->reduce(function ($carry, $item) {
            return $carry + $item['quantity'];
        }, 0);

        $locations = $locations->map(function ($item, $key) use ($total) {
            $percent = $total != 0 ? round(($item['quantity'] / $total * 100), 2) : 0;

            return [
                'loc_type_name' => $item['loc_type_name'],
                'loc_type_id'   => $item['loc_type_id'],
                'quantity'      => $item['quantity'],
                'percent'       => $percent
            ];
        })->sortBy('loc_type_id')->values();


        $locSts = [
            Status::getByKey("LOCATION_STATUS", 'ACTIVE')   => 'Available Locations',
            'Unzone'                                        => 'Un-zone Locations',
            Status::getByKey("LOCATION_STATUS", 'INACTIVE') => 'Inactive Locations',
            Status::getByKey("LOCATION_STATUS", 'LOCKED')   => 'Locked Locations',
            'Occupied'                                      => 'Occupied Locations'
        ];

        $data['items'] = $locations->toArray();
        $data['status'] = $locSts;
        $data['total'] = $total;

        return $data;
    }

    private function isLocStatusOccupied($status)
    {
        return $status && $status == 'Occupied';
    }

    private function isLocStatusUnzone($status)
    {
        return $status && $status == 'Unzone';
    }

    private function isLocStatusValid($status)
    {
        return $status && $status != 'Unzone' && $status != 'Occupied';
    }

    public function getZoneByCustomer($cus_ids, $currentWH) {
        $query = DB::table('zone as z')
            ->join('customer_zone as cz', "z.zone_id", "=", "cz.zone_id")
            ->where('cz.deleted', 0)
            ->where('z.dynamic', 0)
            ->whereIn('cz.cus_id', $cus_ids)
            ->where('z.zone_whs_id', $currentWH)->select('z.zone_id')->distinct()->get();
        return array_column($query, 'zone_id');

    }
    public function getAllLocationHasZone($cusId)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis::getCurrentCusIds();

        $query = $this->model
            ->whereNull('location.parent_id')
            ->where('location.loc_whs_id', $currentWH);


        if ($cusId) {
            $cusIds[] = $cusId;
            $query->whereIn('loc_zone_id', $this->getZoneByCustomer($cusIds, $currentWH));
        } else {
            $query->whereIn('loc_zone_id', $this->getZoneByCustomer($cus_ids, $currentWH));
        }
        return $query->selectRaw('count(1) as total')->get();
    }

    public function countLocationUsed($cusId)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis::getCurrentCusIds();

        $query = DB::table('pallet')
            ->leftJoin('location', 'location.loc_id', '=', 'pallet.loc_id')
            ->leftJoin('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->where('zone.dynamic', 0)

            ->whereNotNull('pallet.loc_id')
            ->where('whs_id', $currentWH)
            ->where('pallet.deleted', 0);

        if ($cusId) {
            $query->where('cus_id', $cusId);
            $cusIds[] = $cusId;
            $query->whereIn('zone.zone_id', $this->getZoneByCustomer($cusIds, $currentWH));
        } else {
            $query->whereIn('cus_id', $cus_ids);
            $query->whereIn('zone.zone_id', $this->getZoneByCustomer($cus_ids, $currentWH));
        }

        return $query->count();
    }

    /**
     * @param $zoneId
     *
     * @return mixed
     */
    public function countLocationByZone($zoneId)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return DB::table('location')
            ->select(DB::raw('count(*) as used'))
           // ->leftJoin('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
           // ->where('zone.dynamic', 0)
            ->whereNull('parent_id')
            ->where('loc_zone_id', '=', $zoneId)
            ->where('loc_whs_id', $currentWH)
            ->where('location.deleted', 0)
            ->get();
    }

    /**
     * @param $zoneId
     *
     * @return mixed
     */
    public function getLocationListByZoneId($zoneId)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return $this->model
            ->leftJoin('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
            ->where('zone.dynamic', 0)
            ->where('loc_zone_id', '=', $zoneId)
            ->where('loc_whs_id', $currentWH)
            ->whereNull('parent_id')
            ->where('location.deleted', 0)
            ->lists('loc_id');
    }

    /**
     * @param $whsId
     * @param $aisle
     * @param $section
     * @param $level
     * @param $bin
     *
     * @return mixed
     */
    public function checkUniqueAisleSectionLevelBin($whsId, $aisle, $section, $level, $bin)
    {
        $query = DB::Table('location')
            ->wherenull('location.parent_id')
            ->where('location.aisle', $aisle)
            ->where('location.row', $section)
            ->where('location.level', $level)
            ->whereNull('parent_id')
            ->where('location.bin', $bin)
            ->where('location.loc_whs_id', $whsId)
            ->where('location.deleted', 0);
        $model = $query->first();

        return $model;
    }

    /**
     * @param $ids
     * @param $cus_id
     *
     * @return mixed
     */
    public function countLocationByIds($ids, $cus_id = null)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis::getCurrentCusIds();
        $query = DB::table('pallet')
            ->where('whs_id', $currentWH)
            ->where('deleted', 0)
            ->whereIn('loc_id', $ids);


        if ($cus_id) {
            $query->where('cus_id', $cus_id);
        } else {
            $query->whereIn('cus_id', $cus_ids);
        }

        return $query->count();
    }

    public function roomReport($input, &$total, &$used, $start, $limit = 10)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $total = DB::table('location')
            ->where('loc_whs_id', $currentWH)
            ->where('deleted', 0)
            ->count();
        $used = DB::table('pallet')
            ->where('whs_id', $currentWH)
            ->whereIn('cus_id', $cus_ids)
            ->where('deleted', 0)
            ->whereNotNull('loc_id')
            ->count();

        $allLocationSql = DB::table('location')
            ->where('location.loc_whs_id', $currentWH)
            ->where('loc_whs_id', $currentWH)
            ->where('location.deleted', 0)
            ->selectRaw("room, count(room) as total, 0 as used")
            ->groupBy('room');

        $totalRoom = count($allLocationSql->get());

        $allLocation = $allLocationSql->take($limit)->skip($start)->get();

        $availLocation = DB::table('pallet')
            ->selectRaw("location.room, 0 as total, count(location.room) as used")
            ->leftJoin('location', 'location.loc_id', '=', 'pallet.loc_id')
            ->groupBy('location.room')
            ->where('pallet.whs_id', $currentWH)
            ->where('pallet.cus_id', $cus_ids)
            ->where('pallet.deleted', 0)
            ->whereNotNull('pallet.loc_id')
            ->get();

        foreach ($allLocation as $key => $row) {
            $allLocation[$key]['used'] = $this->getLocUse($availLocation, $row['room']);
            $allLocation[$key]['percent'] = (!empty($allLocation[$key]['total'])) ? ($allLocation[$key]['used'] * 100) / $allLocation[$key]['total'] : 0;
            $allLocation[$key] = new CustomObject($allLocation[$key]);
        }

        $allLocation = collect($allLocation)->sortByDesc('percent')->all();

        $itemsForCurrentPage = array_slice($allLocation, $start, $limit, true);

        $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, $totalRoom, $limit, $start + 1);

        return $result;
    }

    public function getLocUse($data, $room)
    {
        foreach ($data as $row) {
            if ($row['room'] == $room) {
                return $row['used'];
            }
        }

        return 0;
    }

    public function loadLocByIdAndWhsId($locationId, $warehouseId)
    {
        return $this->model->where('loc_id', $locationId)
            ->select(['location.*', 'customer.cus_name'])
            ->where('loc_whs_id', $warehouseId)
            ->leftJoin('customer_zone', 'customer_zone.zone_id', '=', 'location.loc_zone_id')
            ->leftJoin('customer', 'customer.cus_id', '=', 'customer_zone.cus_id')
            ->first();
    }

    private function countPalletShipping($whsId, $cusIds, $locTypeId = null)
    {
        $queryPallet = DB::table('out_pallet')
            ->select(DB::raw("
                count(DISTINCT out_pallet.loc_id) as total_pallet
            "))
            ->where('out_pallet.deleted', 0)
            ->where('out_pallet.out_plt_sts', 'AC');

        $queryPallet->Join('location as l', 'l.loc_id', '=', 'out_pallet.loc_id')
            ->where('out_pallet.deleted', 0)
            ->where('out_pallet.whs_id', $whsId)
            ->whereIn('out_pallet.cus_id', $cusIds);

        if ($locTypeId) {
            $queryPallet->leftJoin('loc_type as lt', 'lt.loc_type_id', '=', 'l.loc_type_id')
                ->where('l.loc_type_id', $locTypeId);
        }

        return $queryPallet->first();
    }

    private function countPalletNotShipping($whsId, $cusIds, $locTypeId = null)
    {
        $queryPallet = DB::table('pallet')
            ->select(DB::raw("
                count(DISTINCT pallet.loc_id) as total_pallet
            "))
            ->where('pallet.deleted', 0);

        $queryPallet->Join('location as l', 'l.loc_id', '=', 'pallet.loc_id')
            ->where('pallet.deleted', 0)
            ->where('pallet.whs_id', $whsId)
            ->whereIn('pallet.cus_id', $cusIds);

        if ($locTypeId) {
            $queryPallet->leftJoin('loc_type as lt', 'lt.loc_type_id', '=', 'l.loc_type_id')
                ->where('l.loc_type_id', $locTypeId);
        }

        return $queryPallet->first();
    }
}
