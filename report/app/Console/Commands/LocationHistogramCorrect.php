<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LocationHistogramCorrect extends AbstractCommand
{
    protected $signature = 'location-histogram-correct';
    protected $description = 'Location Histogram Correct successfully.';
    protected $date_correct = '';

    public function __construct()
    {
        parent::__construct();
        $this->date_correct = env('LOC_HIS_DATE_CORRECT') ? env('LOC_HIS_DATE_CORRECT') : date('Y-m-d');
    }

    public function handle()
    {
        try {
            $this->logSuccess('start-location-histogram-correct');
            DB::beginTransaction();

            $start_date = $this->date_correct;

            // Empty Loc His Data
            DB::table('loc_his')->where('his_dt', '>=', $start_date)->delete();

            $loc_rac = DB::table('loc_type')->select('loc_type_id')->where('loc_type_code', 'RAC')->first();

            // Full Location
            $locationTtl = DB::table('location')
                ->select([
                    DB::raw('COUNT(1) as loc_ttl'),
                    'loc_whs_id'
                ])
                ->where('deleted', 0)
                ->where('loc_type_id', $loc_rac['loc_type_id'])
                ->groupBy('loc_whs_id')->get();
            $locationByWhs = array_pluck($locationTtl, 'loc_ttl', 'loc_whs_id');

            $now = date('Y-m-d');
            $arrInsert = [];
            if ($start_date) {
                while ($start_date <= $now) {
                    foreach ($locationByWhs as $whsId => $ttl) {
                        $used_ttl = DB::table('pallet_storage')
                            ->select([
                                DB::raw('COUNT(DISTINCT(pallet_storage.loc_id)) as used_ttl')
                            ])
                            ->whereRaw(DB::raw("
                                DATE(FROM_UNIXTIME(pallet_storage.start_date)) <= '$start_date'
                                AND DATE(FROM_UNIXTIME(pallet_storage.end_date)) >= '$start_date'
                                AND whs_id = $whsId
                            "))->pluck('used_ttl');

                        $loc_used_ttl = array_get($used_ttl, 0, 0);
                        $arrInsert[] = [
                            'whs_id' => $whsId,
                            'loc_used_ttl' => $loc_used_ttl,
                            'loc_avai_ttl' => $ttl - $loc_used_ttl,
                            'loc_ttl' => $ttl,
                            'his_dt' => $start_date,
                            'created_at' => time()
                        ];
                    }
                    $start_date = date('Y-m-d', strtotime($start_date . " +1 days"));
                }

                // Insert Loc_his
                if ($arrInsert) {
                    foreach (array_chunk($arrInsert, 1000) as $locs) {
                        DB::table('loc_his')->insert($locs);
                    }
                }

            }
            DB::commit();

            $this->logSuccess('end-location-histogram-correct');

        } catch (\PDOException $e) {
            $this->logPdo($e->getMessage());
            DB::rollBack();

        } catch (\Exception $e) {
            DB::rollBack();
        }

    }
}
