<?php
namespace App\Http\Middleware\Customers;

use Illuminate\Database\Eloquent\Model;

class LogsResponseTime extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logs_response_time';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'rpt_id';

    public  $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
        'rpt_url',
        'user_id',
        'rpt_start',
        'rpt_end',
        'rpt_time'
    ];
}
