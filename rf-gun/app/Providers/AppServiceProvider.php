<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        Validator::extend('greater_than_zero', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $min_value = 0;
            return $value > $min_value;
        });

        Validator::replacer('greater_than_zero', function($message, $attribute, $rule, $parameters) {
            return str_replace('_', ' ' , 'The '. $attribute .' must be greater than 0');
        });

        Validator::extend('greater_equal_than_zero', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $min_value = 0;
            return $value >= $min_value;
        });

        Validator::replacer('greater_equal_than_zero', function($message, $attribute, $rule, $parameters) {
            return str_replace('_', ' ' , 'The '. $attribute .' must be greater or equal than 0');
        });
    }
}
