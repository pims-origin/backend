<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Location;

class LocationTransformer extends TransformerAbstract
{

    /**
     * @param Location $loc
     *
     * @return array
     */
    public function transform(Location $loc)
    {
        return [
            'loc_id'       => $loc->loc_id,
            'loc_code'     => $loc->loc_code,
            'loc_sts_code' => $loc->loc_sts_code
        ];
    }
}
