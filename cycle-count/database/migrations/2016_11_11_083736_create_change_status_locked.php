<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeStatusLocked extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("UPDATE ctn_status
                        SET `status` = 'LK',
                        ctn_sts_name = 'Locked',
                        ctn_sts_des = 'Carton locked'
                        WHERE `status` = 'CL'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
