﻿<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>BOL LPNs List pdf</title>
    <style type="text/css">

        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 36px;
            font-weight: 100;
            margin: 0;
        }

        h2 {
            color: #282828;
            text-transform: uppercase;
            float: left;
            width: 100%;
            margin: 5px 0;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 14px;
            font-family: "Calibri";
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            border: 1px solid #ddd;
            padding: 20px;
            margin: 0 auto;

        }

        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #a9a9a9;
            font-size: 19px;
        }

        .table-style tbody tr td b {
            font-size: 20px;
            font-weight: 600;
            color: #000;
            margin-bottom: 50px;
        }

        .table-style tbody tr td {
            line-height: 38px;
        }

        .table-style2 {
            border: 0px solid #a9a9a9;
            margin: 15px 0;
            line-height: 30px;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            text-transform: capitalize;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 0px dashed #d5d5d5;
            border-bottom: 0px solid #c0c0c0;
            color: #595959;
            line-height: 36px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }

        .table-style2 tbody tr:nth-child(2n) td {
            background-color: #ffffff;
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }

        footer {
            border-top: 1px solid #ddd;
            margin-top: 50px;
            padding-top: 10px;
        }

        .barcodeborder {
            background-color: #0F6;
            border: 1px solid #ddd;
            width: 350px;
            float: left;
            text-align: center;
            padding: 15px;
        }

        .table-style3 {
            border: 1px solid #a9a9a9;
            margin: 15px 0;
        }

        .table-style3 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            /*text-transform: capitalize;*/
            line-height: 20px;
        }

        .table-style3 tbody tr td {
            padding-left: 5px;
            border-right: 1px dashed #d5d5d5;
            border-bottom: 1px dashed #d5d5d5;
            color: #595959;
            line-height: 30px;
        }

        .table-style3 tbody tr td:last-child {
            border-right: none;
        }

        .table-style3 tbody tr td.title {
            font-weight: 600;
            color: #364150;
        }

        .table-style3 tbody tr:nth-child(2n) td {
            background-color: #e3f1f2;
            padding-left: 5px;
            padding-right: 5px;
        }

        .table-style3 tbody tr:last-child td {
            border-bottom: none;

        }

    </style>
</head>
<body>
<table style="width: 100%; border-bottom:1px solid #ddd;margin-bottom: 0px" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td width="30%" style="vertical-align: middle">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALEAAABqCAYAAADz0LApAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpENkJCMTgzQzI4NjFFNzExQjMxMENFQUQ2QUFGRDlGOSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxMkY0Mzg2MjYxMkQxMUU3QTdCQ0VFMkZDRENFQjU4OSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxMkY0Mzg2MTYxMkQxMUU3QTdCQ0VFMkZDRENFQjU4OSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IFdpbmRvd3MiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpENkJCMTgzQzI4NjFFNzExQjMxMENFQUQ2QUFGRDlGOSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpENkJCMTgzQzI4NjFFNzExQjMxMENFQUQ2QUFGRDlGOSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PkfXjFgAAB2xSURBVHja7F0HmFTVFT5v2s7OLFtZtrBUQWkWBERABVFjwYaoWBBBRMXyJbHFGGNEP2MiSEBAIoJBIaKiRAEVhagoSlWUjiBlG8v2Or3knPvusDOz8960ZcPu3vN9x8V5dd7777n/+e+5dySv1wvChLVmkwSIhQkQCxMmQCxMmACxMAFiAWJhAsTChAkQCxMmQCxMgFiYMAFiYcIEiIUJEyAWJkAsTJgAsTBhAsTChMUM4oyrZ4fdyeLRw7CkfFjacwXUewxRX8SoccEuSxbceugOSNQ4xVMPsgZ8pqOTf4VFPVZCnTuhpS5rRO+Jfib3Pujd0Tuip6L7bkRCd6HXo1eiF6EfQt+DfhD9MPqJlnxeZ87cF/D/OgGh9hW00C9GH4M+CL0/enYc57OjE6J+Qv8WfQ16aUt/KQHitm969PPQL0e/Gb0vemIznTuBn5t8Eo/SazmYt6AfFyAWFq+NRZ/MI6+mBa7XGX0Kd4rQy9DfRi88lRfViPfcJo2owvvcr/s/vWeK+C+ib+CgNgsQC4vEKFFbyPnpLadJT0v3tAj9C/TbBYiFqdkE9PXoU5uR8zanDUdfij4PZAVEgFjYSUtDn80B0uM0v1ct+kM8Ko8QIBZGNgB9FfpvW9l9D0RfjX6fAHH7tmEcCBe14h7kdZ78aQWI25+N5PShexv4Lk+jvxwPkIVO3PrsCvTl6Blt6Ds9ykH8e/Soi3lEJG59HHhJGwOwz4jXvwDy0LgAcRs1Gg17Cz23DX/Hp0AeGBEgboNGNQqkr57fxr8nUYoZ6BcIELcyIxKolTygR/cqR6gb28njoDLQ13jPI0DcWkyH0C13mqHIkQw6BHKQXYj+53b2SKj245VI+bEA8WlgNFFgU31XWFXdB5I0joBNHMDadvhYxkGEo3oCxKcJnaAIbJDcwXTifvRr2m0HBfB39KRmA7GEj1cDsc/H00hiLl+U1pNz4fZsw3lDDov2aehnozeE2EZTTWYgMXE5vDo2147A7AWJurnfoWehOxWCC2XUVAw9C90dNHcsD/1BkOd5uf0+p1kINJfrH+gVIc57KciF3g4IFMVN6D+ir0R/At0QtJ0aK02leRW9pBkeLt3/w/w6wfdfyq9Tzz+j4eHb+fWDjb4/FcOsDrFtIn++7d0IxO+AyiwRHQfENJWTFCBnW0acbVnFeXB/p61Q4zbaQJ6ScmuYG/gTAt5NWfdfi0f5Jy0EgD8oHPMxuiXE5xkcHAMUjruLg/mPKvdDoKBSRU8cD5W6N6qPvVJhez76Yj8QT+ANVs1WK9yrMIDeHMjPqdEJQvmHKic5Wcjs8p4kFPRnSZiLH0N/24yJyvKKcyDfkcpkJLSu6PcoHEPRaia6NcS20SoA3gbyLAazQsTz2W3NwDHvUAGw7zv49wLhpKJ03wMlTuxH2ZppSg/2nW4XeOwN4LHVgdtaCx5yWz14nRiLolqyAftkD/J2hxXPZwEP/XVY5POEpJoS+9zrssv74zXpHjzWOvmvvZ7dl9dp5/chqSV5OrVIbOXd99U8kgXbUPQhyVr7ttfKLoQxaQcgW1cHdq+OCrA3gnIV1Tq8rUIC8X5rJtS4EiFNZ/FFzEyFY1bxcwabnke0UObh9++IQJIx8UbyU4wgOScGnipFcM5M7K3KDtoyiKr5KNu3nKrErkxIEoKlAaSERDB1Hwr69C6g7ZDBQOWqKgZ7ySFwnDgEksEEklYXBtASO47MkNMbJD3SQ2wcoNGApDGAo+yIDGZJc/LaXocNPHiMPj0XvSvozCmgMaeDxpAIHic2gPpqcFuqwVlZgF7ErqExhszjaEmBS9C/VAIx2Sb0dxUiJHXjD+ItTbZ6dPBO+bnwZM43YPOCE9/OfC6DBL+oOgKWUXLBPmsn2NrQBcxa9gDSVCgIcfIZCtsu540slNHL/iCKV3sW+gL08Qq0RY1G/BOiLzwvCrOdps1fk6RxvLWobAiYNC54NHsj1HkMmyT5u42KFcNuSy0kdukPmTf8Ccxn4WvSBLYHJwK5+rtlULV+IXg9dpB0BkUAE+i0iSmQNRbP1f8yBL6RgZgaQM2W96F05fNyIyAkYLR2Y6Q1ZJ0BqReOh6RzrgRDZveQ56dewlVxDBoObobabR+C5dA21qAkvdF/NwPPEb5UUyc8/MVaFb4FvfD+Wtzt46q+cMLZAfkta7WUlPwcYn/q2veSIvFNXXfYacmGBMnl6xbOUeHC2xTucRqPxqHsXYXkUs2u5T1CNPZHnqRFa5siiNRP4NNMoWf0cXVfAjBgAHB6ZYnJHQuFoK7b3GcE5D2wBMx9L2kCYNa9peVC5rVPQtbtf2PxH8Nm6GiOXb4GI2/u3a9C8tBbQJtE0dQEmsRkaNj3FQOw1423ieDzIr3wuByQcdn90O3RjyDjykcgIedMxQZCgNV3QrCPuBPyHlwGWbc8DxpTKjbAmuBdR3EqqiqxbUf/WuGpkBrxmAE5LXHbJeXnQ6rWSiyokicx/kaqwlziQlaPHuaeGAYpbF8pnSsaoYyowFKFbVdxD2VHQF7jIBajxLJPhPteBrHPnvic32e4aDw7QeM20qjd1CM3gcWrJ45Mazj8NWoOjF24Li0Hcu6cBbrUnLBHpAy9GdJG38t4a0CnygBswQjcAXImzJIbg5/V714PJe/8AXmxjYGUGo4OG0bne+ZDp3HPga5DdFPpqGGkjZwMXR9ZDimDrmO82Y/idAOFwY9gnXiByjVuQh+cgrTgo+p+sNuaxbo+tH/zjPxkZMTL/pyms8HC0iGkZPg04jv4ywplJI99pRClHleJwoviSIB6yI1N8dw+S0Z/CWKfcl7KOXs4m4T+AOUQm+q7wG+PXUfRmBo/9tNsQZIIzcu4afrFE5GHNs0p7SUHwVl2tMnnaaMmgx67fDlJ8wMwctTcu+dCh/MC8+G6H1dD8b8eBFddBWgSzAzA+o7dIG/qItx3TOg7Iy5eWwrOigJw1ZSwxDCUJeT2gZxJ8yDp7N+Au6Ga3YsfrUwIB+L1PHKEbLCk0ZJMVuJIgjXVfcDtZSevQv8v34fu6t+UZR+2pcO62l7Ac0469iGVJz9XQVWg7vtihWMIvG/GmbpfznVlta6eps8MifM6pOR8FsF+f0PQTqKh5x0NufCjpTN00NhdXnkBlPURQRi7cl1KFgLp6iDu6YSyT2fCsZevgSMzroHq75cHJkfJWZDUbxRSAZtMRwjACSYE8BzkwJcGAvinT6F42aN4LRcDMF1T0ukhe/yLkJDXNE7ZC/dA+SczIX/u7XD079fA0ZfHMM+fPQ5KVzwL1sPbmiSVklaPPckMSOw9lKkZvIcYFkp6DAYxgZBmztoUntEteKkMk8bJEjxPY9fzNtdYNqP/kIgR+svansiFc33R+gaVrnsjH6QIZVNUpJXmGrh4WkVhmRJGQ4/U6jgdKQ6zH0WZBVrJ+5gNk+j7joxFIOcidbOVeGV58ONIQKzv1AO0KYHvumL9AihfPRNoFVSPpQ7KVr0EjpKDQRGwL0gaHYuqWuSlufe8xpK4gGQRIykB0l1bxl+5h0XUzGt+hxz8kiYNp3z13yF/zq1QtmYmgnU747oejPYk9dkK90LFl29AwWsToeiNe8F+/EDA8dqkDKQxr7AehfcQvULJrKGGndcqZYFceL5T4prgPlsmKx/kycs69IUYqR359lSYh1w4TddAe+pVkihKWuYrNJrzOYUJZQc5jWkOI5pAXbYxBN14AZqv+Ibu+T6V5NlndB8zjBrXdTWeBLgX+fH2BorIjgqM0jRoskcdxV6MjknMT2qQCJjaLStYdJO0CSBhhKVu2lndtE2RJKdNzkBeiwDu21QY0WBS13nKQug09hkwIH2gBmHuNQRSRwYKWwS6kuVPQtknrzCQa81p7J6Y8oBJJjUW4sDapDRseE6o/XENFC6YCLaC3YENK6sXpI++DxsEU1B968qFBTHZHFCe6zQNI0XHUlcSvFk2GMwau0/gfxwP+JToBnFgq1fvi9NjVCjBdyqJGb2wVIVtn0YQ1aIx6i+fDXouNEKUDc1rn6A/EmZAhvWm+CwXICceSEvpUqJHETlFa6PvfLdaHkDduquyAOp2fAINe79C/xrqfl4rZ/uk3Tqt4K4rB0NmDzBk9Q7UOPd9jRzYDLkT54DpzOGhz4/gM2T3go5jHocuDy3Dc/SAlBETGCD9rXztq1D93XIGXqYpA4SAlJc1OrpnirqOymIGfFdQ4+owcAyjSBzIfSMF8UYFuQs4LbirAyZ462vOgG/qegL9G29nV4LkriUt+amCK9noniRTgSdCkXE/Ka4+xOcX8JelpCcvjgFA1jC68FN+kf9hrksqmU2FcoWzxTzChzPKyv6VKLm61mFEnorUYjMmfNjz/UAJoNL1JUMiGzgofuu32EVPhcKF90LJu08z7ZaAQKBLHXYbUoX5TGLzWQ1G6vo9/4XO9y9uQguUzON1I3XpyXTgYA5ctWEJRl5T4+BHBAmp1tiBUY6y1TPY/Z7k63jf5v6j5URQajpQpnQFC3/YStH4Hg14051eLayoHMAAa8RkrsGjhweO3Mi0YUruQF7MbrjCOYjP/kdh2yMqXHgF+q4Yu/N5YZI4ir7Xg3oROn2xl+Pk46RWrI5gv3PxBbxI+nGlywQvFY9kAYNH9PlKdIKAQ102/fUi0LTmZMi5azZ0+90HTLvNnTwfjHmN1NJdVwYVa+dA1rjpkDTgiqZfGLfb8neCveSXk6N2LCoc2gr6lGymYPhb9ffvIs2oCh6wiAjIGgQy9Ry2wsBXnNRnJGhoVNHtMkUKYp90tllhGz2BK82Y4H1Y1R+eyL+aFXbvtXZCAPegbs83wvWoyvlnK1ACGlG7QuGYygjlqlBGavuMMIMPZ3O9Wk3gXMrVBmMcILbwnmZdBPuSNHmXWeuAg/aOsODEUBYgJFk12agsumrYAAcNYkj6RDB2PYdJYASSYDvxwV/A1HsYpF4c2PnYCndD8ZvT4Oism6Bg3h1Q8OptcOwfY6EU93dVHwfb0Z+QPwcGRhdSlfrd67BHMEdZl+GjQwZsANVQvzdwyMKQcyajHB6X3RgNiBs40BR1TbxFDWbO8EHVAPh9/hiYXnQZpxaMDQ9RkaYOcUVD6aVlqUThnTECx8gHYogq1IbRhZXsOI/W9mZI+Kq4+hFuIITe0QyMqUzdeb54NBy2p0GCxlXFv0t1ZEEudOGe5eBm5MIbIP2ywBWlGg58AwXzJ0DNtv8gxy6UFQXk1faCvVD55UI4Nmss1O5YDQmZgaPwjuP7wVVzgvHc2AyxozVAw+7/svOQwkFO3FqX0olohiYaEAPv8pQiF1WVjWRCudYKH1X1gyJHin+55Z0qXHgxhK4PpeGlySoy1cI4QENhIZEPrLwQ4zmeAbk6r7nW2i0AeeTQEWY/atTPIpC19Psnr5dewJ6zVx7yf0W9CWgYBSAq4G6oIt0rqOt/B0y9LgxI8uzF++H40seQRlSwIWaiBUzZwChJnFuDgKJBDldtBWgSAyO7q74KA7AnrtekweTSVrSXNZRjM69nXvT6PXi9clarEWzhVgCycqUiVM0AHfskKQwYeR1BPygzIkxiVKPw+WPoXRS2reIAjMd8wvYrvJe4NYpjZzbD4IpS75Ibptcjo5LYj3XgeY8o225LFvQyVlA+MpcnpANDfmEEn8daD0WLp7GBCVIlEnsOQh8M+tQc5LWboeO1TwRw6sov5oGz/Bhoadg4FCUgRYHJdRpWVxFIoJ0x0YhAfdmFPDsZOpx7ldyAJAkcJ34FW/E+po5EC2Kyb3j33yvENorElMquD5GYqfUnk0CuY/YHc3dQX4T5vWYEDj3lv4Fck5EcYVL4Mpw6m8cDxfgw+92qlTzvlTqTYFn5eTCz66dQ7U6s4br+QJk1uANAJCd4ch0wacOWmi1IFb4FDYKDCnjoc9J7TyoOLhvYTxyWI543TDhwezA6lgUB0B0niGUZUJ/XHzrd1Kh6klxY+/PnEKqyNRL9g7r9fypso+75oSB+SJpwuMJzktDGBn02BZRXt1kHEQ67RmE7OD0IVyXm4vuVnUIQu3mvdijMfoNJetPQOL7O5qMUZB/45EOKWhLV+NJff3mLFAvM7ikaa01pjBqQXkyRjuhB47e1g8dWK4NfDcW8noAVtAd87oo/vnhcYD4rcBCVaj4oyuN9eWIBMXAu+ovCthv8Ejgdj8IdIjjnY37ckp7iOIX97LyrtZ6iCBhOc57N9exTbfkRyG40t2+EEanb5vqurFY7QZYyt2H0O0DdcMdrH4O8+xajvwF5DyyGpAGXstkXgejjMhxxXOSf/hKZ1yvJibkUNmBS3MZoHiivua01jA7467zRmAcbhTYlG5IH3xCQlFoObgIv9SgajS1WEFNSNVfl64z1E+gvjfCcA/wiNq1u01dhPxrV++IUAYfe6F9UFAKSGF+ClrPNoD7/j97XWUbJCdvr8+CALdOnx9N/dhNwEnsMBhNGMVOfkWA68yJWEQYup4Kk7AUtRWI/ELNh4bCzPOiKbmwERkjsHkjFXTVloEWaQg2E6jj8KtAiA7GtDlKHjcdEs5G90vC49ciPoKFCfK/XGiuIyejnnH5Q2DaRU4HJEN3vMVBCQhVuSrW6Ts5FXacQODRo8acQ16jlVKmyBUFcBuGHpDMpUho0LtD7r1MhSUVe7IZJlgpIelJyZM04FDVAkCVk9w7QjilRIxXDw+a9qWDYUg3Jg24AY5dzAhIy69EfIf3SKdBp3F/Y/zO6ESGQ6brJg66HjCsCCx7rtn0E7voKX1JXEQ+Iq1W6VaoxoEGIe6J8aaN4hO+nsJ3mwn3VAuBZHkJ5mN8Maki0Zowg2Q4NLUnSEogdRXsDPjb3uxT0GV15OWOjQENDuFTTkDLijkA56ugOtn/6JRP5RM56ptOyCaL4l46jAQ1z34tZEZD/jBHSiJ0VhQyIKUNvgezbXmRRnQDv9fUGBGgGagl8iShrOAhSc79LIOe2lwIalb14H1R+tdh/ZsjBeEBMthKU5//fqiKPlSlEUwL/XQpKhpeDy9FCACJasYX/e2ML0wifnQvhi/SPE2uldUBo2F9qfFpdJK0B6vZ8GVBsrkvOZGBjo10NNQxQpFKQOpF5/R/A1Ht4wCOv2/4RpAy5EbLGvwRZt7wACZ16UftgxTckq+nTOmOkvRc6T31DluD8ddOtHyJ9OQsMuXLVbeqICWx6lKnnBaDRkdRXw0bjPJZa9m9WlIQgTsg6AzJvfBry7l8C2uROjZEZgX3i/Wex0ZQhiBN8tOmnWCQ2f6PsmX5n4bkojiEi/jDvmi+J4jgqOXyrBQFUwpPNN3hyWtfCAE4OodgEGzXoXRb2g+aHYKCpGGxeHVeJvOcTZ7Tm74TqjW9D+ujGhXNoVgZxzIb9G8BRkQ96pBhU5GPsEliaSxVv1mM/Q6eb5debNnISJJ9/LdhLf2XlnJrEFNCn5wUUDp28sdLDUL35fcgeH9j2aTi7yyPLwY49BGnStqJ9bDoTVctR8ZCp27mQkDegSYOgianFS38Pll++Y3PueAdEye/OeEHsUyqmQuRLb37PaYguShC/0cJ81HevN6ooMafC6LnQklUvQPh1eakEcytF4X7GMuiWUAVlLjNFY1pWoTub8o7RsuLzuZDYbSAkntF4OpqsSa6oI2K0O/H+M5BywU1slofPCFymMHPlKLqXvPsUS+pqvl8Oph7ngy49r7G7NyRiwjmIuYxQD+fpSty4Gko/+DM07FrPRwRPMijqKYubA8THOV+MZAIjhf9Z/N8f8ZsYGsFx9LvAS/8P3bm3GQDs5QnpzVx9cSskrHZOHYiCkSiaHsG5vyZqRlWDVGfsaqQTNEjCSCPpvlSoTlEsdxKCuXv4dbkpGSx550k23EzDytGYrWAXnFg5HSPmZtB1yMBo/w0UvHY3ZF77OCSdp7DKggqAqRy04rPZYDn8A2hNKcGb14WSWmP94RlKgmhw4oww+5E09in/N4mVNNz7HoRXISlJrILWa8T/rwDlOpBYrB658JxatxEGmwvgwazNQHXGIC8scr2fbgZSghmclYVQuGASpF9+H6QOnwBac9P5BcSdG3avh/LP5jAAU9QtXzMDnOX5eNw0tlaEYnSqLYWabSuhcv3r4MR/k6zG8InAs584BIVvToOkvpdA6kV3MUqhsCgKvxEX0oz9ULXhTaj9YRWbFaJtpBD+wXOjUlcWi5GO8xnnumr8bUHQnazh0fhCleOoKGYttG6TILqFWcLrpyBNr3KZdg4yF8GiHishXWelBR71kpyA5gZ3BjTTgiZ7lv3nr1CzZSXy4qvA2LkfmyJEBUHEYSnqWQ5tZQMfvkIeSqCqNy5jNb00QTTprItBl5rNlAxSGCjZoomddTu/YGCnyO8DsK8RUTE8qQ71e76C+n0bkHufDaZeQ8GY158VuBO9oCIhUkycyNEb9n+LvpHxbgnBLqsTTUSYjaAwohnPT4D9kysLKVHIY9QVvB0GxBSpi0CYbzkrqHUnPJeis82Z120VXJhUALQcAgLYwFdguknxeB1VnqWCs+wIi7Zy0Y6eKQK0vBQNLdMwdOBBGhZNSfaq2bwCareuZKAkjZbJYaR8IJglQwLbT2lQhM2j42C05e9igxV0PwRgdg80YRUjLi11xfbHa2iotwh9PurZ/g0KA0HxgHgPB+QjCtuJ04aaevQuVwHOUJDiFkB8q1a2CfN4JeS9RhtG4Bm/STk4/c6Mn+CqlF8I0GD16DIleTj8jkgoOhXF08gcDd9SBCTtVqs3cIVVuUpNm2hgpZtU1MOGkilxJKmL8WYpgkIfebvcULzy9elcLpfcV0l87TWJ34fy+WhE9XO1zDgeo8VLqPIsOH2ludfLFI6p4onhrBDbqLUdjrMbV6phNjQzXTCEuY4u1hNjlK3USZ7PxqXvfm1M6oHvRyf/Ck6PBspdpm64/SpJVocGRZdrylE2oCgIIgAhKxzSKJ8z2uvHdq7VoDKnMV4Q7+SR88GgOyGepjbj4C0uZQ3gUZe+WaQr5YRLqI6GALLE5anmWq7eyc9nDTqnxCUgL2+sRXxfN//Mxf+6ec5Ax9PwdgXvhY46vNpfMnWWn2d2XWO6qMOx4Yka1+gTziQqJOgtyfUlae2sU6LnOTecRhmvUbH4Kg5Gib+k3WGOqeR8Opu/WC0HfX6c95LPZa3gftK3UrytmR5sEZfQtArXoUpx+hmrdzhQnfz5+Fa4d/DPmoxGkvrwaM5GuDlj98CD1swlNW7QSNCubREPTKcUxBRJtscIuPxm/sIEin0t8GDpOvvD7FMCUc6I9oEVIy9UOs07EO3T8bPp7RjA1NO/Gm4n8etJp5E5vRrINdTA8KRjbDCDv8Dv2vEjeQZC/3aLAPHpDOIcfT1TIWhZXE6xnm+nj4NG5yIaLxAgPo2Mfq9jiLkQqMBHaqTaNOo5r509il9BHkhzChC3MtNKXpiSud23ZK6/UdH+9+2lQwJ5ksQvkTd+YaeVWTx6peR5Ko9Qbd2eA3mZLhAgboXmZj+xpiio0ZSNu0F99aLWblR+OzN6GibstDD6xdWHszZBz4QKVmKpYKRU0KLf9jb4CKgcgSZOOASIW6nRqvsd9Q1gYL/ToWo0gEI1EyVt6Osv43TJGcvBOgGf08dc6nTC32iuI410LoHWPwz9L5DLFmIeTRWRuPUaDfXTnLx9rfT+iRLRlKwpEGc5gABx67YNIP/O9Put7L6peIpqZ2ixtbiLsgSIW7/RTBiaY0e/eFrZCu6Xyirpp9dWNNcJBYjbjtEqnzQz8+PT9P5oahFNhqA19w4054kFiNuWbQW5RJS48pbT5J4qOfelNatnxapACBC3LyPVgpZHGAXy8O0P/6f7oEkBtEbJbzj3LT1VFxISW9s1yviplJOmfA3h0Zl+U7DzKbwmrZpEPx1Hk31pcfb9LfFFBYjbvlE97lruNBeSZr6QokHLWdJ0p3h/QIcGXah4fRPn4zta+gsKELcvKwd5hjo5rVVBvzZzLgdzHw5ymp/oW52TAE7TqnxzAim60wz2Yxy4e3i0PXgquG6kJnm9XvFqhbVqEyAWJkAsTJgAsTBhAsTCBIgFiIUJEAsTJkAsTJgAsTABYvEUhAkQCxMmQCxMmACxMAFiYcIEiIUJEyAWJixm+58AAwBHKKuQjvsn6AAAAABJRU5ErkJggg==">
        </td>
        <td class="text-right">
            <h2>BOL LPNs</h2>
        </td>
    </tr>
    </tbody>
</table>

<!--order list-->
<table class="table-style2" cellpadding="0" cellspacing="0" width="100%">
    <tbody>

    @if ($shipmentInfoArr)
        @for($i=1; $i<=$LPNNo; $i++)
            <tr>
                <td style="text-align: center; padding: 0px; width: 30%; ">&nbsp;

                </td>
                <td style="text-align: center; padding: 4px; width: 40%; ">
                    {{$cusName}}
                </td>

                <td style="text-align: center; padding: 0px; width: 30%; ">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: center; padding: 0px; width: 30%; ">&nbsp;

                </td>
                <td style="text-align: center; padding: 0px; width: 40%;">
                    <?php
                    $whsId = array_get($shipmentInfoArr, "whs_id", '0');
                    $licensePlate = substr(array_get($shipmentInfoArr, "bo_num", null), 0, 14);
                    $licensePlate = $licensePlate . "-" . str_pad($i, 4, "0", STR_PAD_LEFT);
                    $licensePlate = str_replace("BOL", "MPL", $licensePlate);
                    ?>

                    <barcode height="1.5" size="0.9"
                             code="{{$licensePlate}}"
                             type="C128A"/>
                    <br>
                    {{$licensePlate}}
                    <br>
                    Ship to: {{array_get($shipmentInfoArr, 'ship_to_name', '')}}
                </td>

                <td style="text-align: center; padding: 0px; width: 30%; ">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: center; padding: 0px; width: 30%; ">&nbsp;

                </td>
                <td style="text-align: center; padding: 0px; width: 40%; ">
                    <barcode height="1.5" size="0.9" code="{{array_get($shipmentInfoArr, 'bo_num', '')}}" type="C128A"/>
                    <br>
                    {{array_get($shipmentInfoArr, 'bo_num', '')}}
                </td>

                <td style="text-align: center; padding: 0px; width: 30%; ">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="aqua">
                <td style="text-align: center; padding: 0px; width: 30%; ">&nbsp;

                </td>
                <td style="text-align: center; padding: 0px; width: 40%; ">
                    {{--PO: {{array_get($shipmentInfoArr, 'cus_po', '')}}--}}
                    {{--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--}}
                    {{--Ship Date: {{date('m/d/Y', array_get($shipmentInfoArr,'ship_by_dt', ''))}}--}}
                    &nbsp;
                </td>

                <td style="text-align: center; padding: 0px; width: 30%; ">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center; padding: 0px; line-height: 0px;">&nbsp;

                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center; padding: 17px; border-top: 1px dashed #ddd; line-height:
                0px;">&nbsp;

                </td>
            </tr>
        @endfor
    @endif


    </tbody>
</table>
<!--/Order list-->


</body>
</html>
