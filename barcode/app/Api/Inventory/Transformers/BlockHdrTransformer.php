<?php

namespace App\Api\Inventory\Transformers;

use App\Api\Inventory\Models\BlockHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\BlockHdr;

class BlockHdrTransformer extends TransformerAbstract
{
    /**
     * @param BlockHdr $blockHdr
     * @return array
     */
    public function transform(BlockHdr $blockHdr)
    {
        return [
            'block_hdr_id' => $blockHdr->block_hdr_id,
            'block_rsn_id' => $blockHdr->block_rsn_id,
            'block_rsn_name' => object_get($blockHdr, 'blockReason.block_rsn_name'),
            'block_num' => $blockHdr->block_num,
            'whs_id' => $blockHdr->whs_id,
            'block_type' => $blockHdr->block_type,
            'block_type_name' => BlockHdrModel::$arrBlockType[$blockHdr->block_type],
            'block_detail' => $blockHdr->block_detail,
            'block_sts' => $blockHdr->block_sts,
            'sts' => $blockHdr->sts,
            'created_by' => $blockHdr->created_by,
            'updated_by' => $blockHdr->updated_by,
            'created_at' => $blockHdr->created_at,
            'updated_at' => $blockHdr->updated_at,
            'whs_name' => object_get($blockHdr, 'warehouse.whs_name'),
            'create_by_name' => object_get($blockHdr, 'createdBy.first_name')
                . ' ' . object_get($blockHdr, 'createdBy.last_name'),
            'block_sts_name' => BlockHdrModel::$arrBlockStatus[$blockHdr->block_sts]
        ];
    }
}
