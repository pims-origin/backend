<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//include realpath(__DIR__ . '/..') . '\app\Modules/Ecom/Orders/routes.php';

$modules = config("module.modules");
foreach ($modules as $key => $values) {

    while (list(,$module) = each($values)) {
        $route = realpath(__DIR__ . '/..') . "/app/Modules/" . $module . '/routes.php';

        if(file_exists($route)) {
            include $route;
        }
    }

}

/*$router->get('/', function () use ($router) {
    return $router->app->version();
});*/
