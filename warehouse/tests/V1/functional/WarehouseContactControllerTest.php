<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 30-May-16
 * Time: 09:08
 */

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Models\WarehouseContactModel;
use Illuminate\Http\Response as IlluminateResponse;

class WarehouseContactControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $warehouseId;

    protected $warehouseContactParams = [
        'whs_con_fname'      => 'fname functional test',
        'whs_con_lname'      => 'lname functional test',
        'whs_con_email'      => 'email@email.com',
        'whs_con_phone'      => '1111',
        'whs_con_mobile'     => 'mobile from functional test',
        'whs_con_ext'        => 'ext from functional test',
        'whs_con_position'   => 'position functional test',
        'whs_con_whs_id'     => 1,
        'whs_con_department' => 'department from functional test',
    ];

    /**
     * WarehouseContactControllerCest constructor.
     */
    public function __construct()
    {

    }

    public function setUp()
    {
        parent::setUp();

        $warehouse = factory(App\Warehouse::class)->create();
        $this->warehouseId = $warehouse->whs_id;
        $this->warehouseContactParams['whs_con_whs_id'] = $this->warehouseId;
    }

    private function getEndPoint($warehouseContactId = null)
    {
        return "/v1/warehouses/{$this->warehouseId}/contacts" . ($warehouseContactId ? '/' . $warehouseContactId : '');
    }

    public function testList_Ok()
    {
        // make data
        factory(App\WarehouseContact::class, 3)->create(['whs_con_whs_id' => $this->warehouseId]);

        $uri = $this->getEndPoint();
        $response = $this->call('GET', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
        $this->assertCount(3, $responseData['data']);
    }

    public function testShow_Ok()
    {
        // make data
        $contact = factory(App\WarehouseContact::class)->create();
        $warehouseContactId = $contact->whs_con_id;

        $uri = $this->getEndPoint($warehouseContactId);

        $this->call('GET', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $uri = $this->getEndPoint();

        $response = $this->call('POST', $uri, $this->warehouseContactParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testUpdate_Ok()
    {
        // make data
        $contact = factory(App\WarehouseContact::class)->create();
        $warehouseContactId = $contact->whs_con_id;

        $uri = $this->getEndPoint($warehouseContactId);

        $response = $this->call('PUT', $uri, $this->warehouseContactParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testUpdate_WithZero_Fail()
    {
        $uri = $this->getEndPoint(99999);

        $this->call('PUT', $uri, $this->warehouseContactParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDelete_Ok()
    {
        // make data
        $contact = factory(App\WarehouseContact::class)->create(['whs_con_whs_id' => $this->warehouseId]);
        $warehouseContactId = $contact->whs_con_id;

        $uri = $this->getEndPoint($warehouseContactId);

        $this->call('DELETE', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        $this->seeInDatabase((new App\WarehouseContact())->getTable(),
            ['whs_con_id' => $contact->whs_con_id, 'deleted' => 1]);
    }

    public function testDelete_NotExit_Fail()
    {
        $uri = $this->getEndPoint(99999);

        $this->call('DELETE', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testsaveMassAndDeleteUnused_Ok()
    {
        // make data
        $contacts = factory(App\WarehouseContact::class, 2)->create(['whs_con_whs_id' => $this->warehouseId]);

        $uri = "/v1/warehouses/{$this->warehouseId}/contacts/save-mass-and-delete-unused";

        $params = [
            'whs_con_id'         => [
                '',
                $contacts[0]->whs_con_id,
                ''
            ],
            'whs_con_fname'      => [
                'fname1',
                'fname2',
                'fname3'
            ],
            'whs_con_lname'      => [
                'lname1',
                'lname2',
                'lname3'
            ],
            'whs_con_email'      => [
                'foo@gmail.com',
                'fab@gmail.com',
                'far@gmail.com'
            ],
            'whs_con_phone'      => [
                '11111',
                '22222',
                '33333'
            ],
            'whs_con_mobile'     => [
                '11111',
                '22222',
                '33333'
            ],
            'whs_con_ext'        => [
                '11111',
                '22222',
                '33333'
            ],
            'whs_con_position'   => [
                'A',
                'B',
                'C'
            ],
            'whs_con_department' => [
                '',
                'A',
                ''
            ],
        ];

        $response = $this->call('POST', $uri, $params);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        $tableName = (new App\WarehouseContact())->getTable();
        $this->seeInDatabase($tableName, ['whs_con_id' => $contacts[0]->whs_con_id, 'deleted' => 0]);
        $this->seeInDatabase($tableName, ['whs_con_id' => $contacts[1]->whs_con_id, 'deleted' => 1]);
        $this->seeInDatabase($tableName, ['whs_con_fname' => 'fname1', 'deleted' => 0]);
        $this->seeInDatabase($tableName, ['whs_con_fname' => 'fname2', 'deleted' => 0]);
        $this->seeInDatabase($tableName, ['whs_con_fname' => 'fname3', 'deleted' => 0]);

    }
}