<?php

namespace App\Modules\Customers\Eloquents;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{

    protected $table = 'warehouse';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'whs_id';
    /**
     * @var array
     */
    protected $fillable = [
        'whs_name',
        'whs_status',
        'whs_short_name',
        'whs_code',
        'whs_country_id',
        'whs_state_id',
        'whs_city_name'
    ];
}
