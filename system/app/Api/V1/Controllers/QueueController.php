<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 06-Jun-16
 * Time: 08:36
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\RabbitMQService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Symfony\Component\HttpKernel\Exception as HttpException;

class QueueController extends AbstractController
{
    /**
     * @var RabbitMQService
     */
    protected $rabbitMQService;

    /**
     * QueueController constructor.
     *
     */
    public function __construct(RabbitMQService $rabbitMQService)
    {
        $this->rabbitMQService = $rabbitMQService;
    }

    public function send(Request $request)
    {
        $params = $request->getParsedBody();
        $this->rabbitMQService->init(\GuzzleHttp\json_encode($params));
        $this->rabbitMQService->send();
    }

}
