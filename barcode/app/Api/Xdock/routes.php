<?php

//Xdock
$api->get('asnsV1', ['action' => "goodsReceipt", 'uses' => 'AsnController@listAsn']);
$api->get('cus/{cusId:[0-9]+}/asns/{asnID:[0-9]+}/containers/{ctnID:[0-9]+}',
    ['action' => "goodsReceipt", 'uses' => 'AsnController@loadASNDtl']);
$api->get('asn-detail2/{asnDtlId:[0-9]+}', ['action' => "goodsReceipt", 'uses' => 'AsnController@getAsnDetail2']);

$api->post('asn-dtl/{asnDtlId:[0-9]+}',
    ['action' => 'goodsReceipt', 'uses' => 'GoodsReceiptController@xdockScanCartonAndLocation']);
