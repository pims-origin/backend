<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MenuFavoriteModel;
use App\Api\V1\Models\MenuGroupModel;
use App\Api\V1\Models\MenuModel;
use App\Api\V1\Transformers\MenuTransformer;
use App\Api\V1\Validators\MenuValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Dingo\Api\Exception\ValidationHttpException;

/**
 * Class MenuController
 *
 * @package App\Api\V1\Controllers
 */
class MenuController extends AbstractController
{
    /**
     * @var $menuModel
     */
    protected $menuModel;
    /**
     * @var $menuTransformer
     */
    protected $menuTransformer;
    /**
     * @var $menuValidator
     */
    protected $menuValidator;
    /**
     * @var $menuGroupModel
     */
    protected $menuGroupModel;
    /**
     * @var $menuFavoriteModel
     */
    protected $menuFavoriteModel;

    public function __construct()
    {
        parent::__construct();

        $this->menuModel = new MenuModel();
        $this->menuTransformer = new MenuTransformer();
        $this->menuValidator = new MenuValidator();
        $this->menuFavoriteModel = new MenuFavoriteModel();
        $this->menuGroupModel = new MenuGroupModel();
    }

    /**
     * @SWG\Get(
     *     tags={"Menu"},
     *     path="/v1/menus/{menuId}",
     *     summary="Read Menu",
     *     description="View Detail Menu",
     *     operationId="GET_showMenu",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="menuId",
     *         description="Menu Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return item of Menu",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 description="Menu Fields",
     *                 properties={
     *                     @SWG\Property(property="menu_id", type="integer"),
     *                     @SWG\Property(property="menu_group_id", type="integer"),
     *                     @SWG\Property(property="name", type="string"),
     *                     @SWG\Property(property="description", type="string"),
     *                     @SWG\Property(property="url", type="string"),
     *                     @SWG\Property(property="target", type="string"),
     *                     @SWG\Property(property="icon_class", type="string"),
     *                     @SWG\Property(property="permission_name", type="string"),
     *                     @SWG\Property(property="display_order", type="integer"),
     *                 }
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_id": 1,
     *                     "menu_group_id": 1,
     *                     "name": "Header Menu",
     *                     "description": "This is example from Swagger",
     *                     "url": "#",
     *                     "target": "_blank",
     *                     "icon_class": "",
     *                     "permission_name": "viewMenu",
     *                     "display_order": 1,
     *                 }
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     */
    /**
     * Menu Detail
     *
     * @param $menuId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($menuId)
    {
        try {
            $menu = $this->menuModel->byId($menuId);

            return $this->response->item($menu, $this->menuTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     tags={"Menu"},
     *     path="/v1/menus",
     *     summary="Create Menu",
     *     description="Create Menu",
     *     operationId="POST_store",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="formData",
     *         name="menu_group_id",
     *         description="Menu's group ID",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         description="Menu's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="description",
     *         description="Menu's Description",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="url",
     *         description="Menu's url",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="target",
     *         description="Menu's target",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="icon_class",
     *         description="Menu's icon class",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="permission_name",
     *         description="Menu's permission name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="display_order",
     *         description="Menu's display order",
     *         required=false,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Return item of Menu",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 description="Menu Fields",
     *                 properties={
     *                     @SWG\Property(property="menu_id", type="integer"),
     *                     @SWG\Property(property="menu_group_id", type="integer"),
     *                     @SWG\Property(property="name", type="string"),
     *                     @SWG\Property(property="description", type="string"),
     *                     @SWG\Property(property="url", type="string"),
     *                     @SWG\Property(property="target", type="string"),
     *                     @SWG\Property(property="icon_class", type="string"),
     *                     @SWG\Property(property="permission_name", type="string"),
     *                     @SWG\Property(property="display_order", type="integer"),
     *                 }
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_group_id": 1,
     *                     "name": "Header Menu",
     *                     "description": "This is example from Swagger",
     *                     "url": "#",
     *                     "target": "_blank",
     *                     "icon_class": "",
     *                     "permission_name": "viewMenu",
     *                     "display_order": 0,
     *                 }
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     */
    /**
     * Menu Create
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->menuValidator->validate($input);

        $params = [
            'menu_group_id'   => trim(array_get($input, 'menu_group_id')),
            'name'            => trim(array_get($input, 'name', '')),
            'description'     => trim(array_get($input, 'description', '')),
            'url'             => trim(array_get($input, 'url', '')),
            'target'          => trim(array_get($input, 'target', '')),
            'icon_class'      => trim(array_get($input, 'icon_class', '')),
            'permission_name' => trim(array_get($input, 'permission_name', '')),
            'display_order'   => trim(array_get($input, 'display_order', 0)),
        ];

        try {
            if ($menu = $this->menuModel->create($params)) {
                return $this->response->item($menu,
                    $this->menuTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (ValidationHttpException $e) {
            return $this->response->error(implode("\n", $e->getErrors()->all()),
                IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    /**
     * @SWG\Put(
     *     tags={"Menu"},
     *     path="/v1/menus/{menuId}",
     *     summary="Update Menu",
     *     description="Update Menu",
     *     operationId="PUT_update",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="menuId",
     *         description="Menu Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="menu_group_id",
     *         description="Menu's group ID",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         description="Menu's Name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="description",
     *         description="Menu's Description",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="url",
     *         description="Menu's url",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="target",
     *         description="Menu's target",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="icon_class",
     *         description="Menu's icon class",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="permission_name",
     *         description="Menu's permission name",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="display_order",
     *         description="Menu's display order",
     *         required=false,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return item of Menu",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 description="Menu Fields",
     *                 properties={
     *                     @SWG\Property(property="menu_id", type="integer"),
     *                     @SWG\Property(property="menu_group_id", type="integer"),
     *                     @SWG\Property(property="name", type="string"),
     *                     @SWG\Property(property="description", type="string"),
     *                     @SWG\Property(property="url", type="string"),
     *                     @SWG\Property(property="target", type="string"),
     *                     @SWG\Property(property="icon_class", type="string"),
     *                     @SWG\Property(property="permission_name", type="string"),
     *                     @SWG\Property(property="display_order", type="integer"),
     *                 }
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_group_id": 1,
     *                     "name": "Header Menu",
     *                     "description": "This is example from Swagger",
     *                     "url": "#",
     *                     "target": "_blank",
     *                     "icon_class": "",
     *                     "permission_name": "viewMenu",
     *                     "display_order": 0,
     *                 }
     *             }
     *         },
     *     ),
     *     @SWG\Response(response=400, description="Show Error message"),
     * )
     */
    /**
     * @param int $menuId
     * @param Request $request
     *
     * @return mixed
     */
    public function update($menuId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->menuValidator->validate($input);

        $params = [
            'menu_id'         => $menuId,
            'menu_group_id'   => trim(array_get($input, 'menu_group_id')),
            'name'            => trim(array_get($input, 'name', '')),
            'description'     => trim(array_get($input, 'description', '')),
            'url'             => trim(array_get($input, 'url', '')),
            'target'          => trim(array_get($input, 'target', '')),
            'icon_class'      => trim(array_get($input, 'icon_class', '')),
            'permission_name' => trim(array_get($input, 'permission_name', '')),
            'display_order'   => trim(array_get($input, 'display_order', 0)),
        ];

        try {
            if ($menu = $this->menuModel->update($params)) {
                return $this->response->item($menu, $this->menuTransformer);
            }
        } catch (ValidationHttpException $e) {
            return $this->response->error(implode("\n", $e->getErrors()->all()),
                IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    /**
     * @SWG\Delete(
     *     tags={"Menu"},
     *     path="/v1/menus/{menuId}",
     *     summary="Delete Menu",
     *     description="Delete a Menu",
     *     operationId="DELETE_destroy",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="menuId",
     *         description="Menu Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Menu was deleted",
     *     ),
     *     @SWG\Response(response=400, description="Some Error in message"),
     * )
     *
     * @param int $menuId
     * @param MenuFavoriteModel $menuFavoriteModel
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($menuId, MenuFavoriteModel $menuFavoriteModel)
    {
        try {
            if ($this->menuModel->destroyMenu($menuId, $menuFavoriteModel)) {
                return $this->response->noContent();
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Cannot Delete this item');
    }

    /**
     * @SWG\Post(
     *     tags={"Menu"},
     *     path="/v1/groups/{menuGroupId}/list_menu",
     *     summary="Create Mass Menu belongs to Group Id",
     *     description="Create Mass Menu belongs to Group Id",
     *     operationId="storeGroup",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="path",
     *         name="menuGroupId",
     *         description="Menu Group Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="param",
     *         description="input json",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Return Status",
     *     ),
     * )
     *
     */
    /**
     * Store Menu List by Group Id
     *
     * @param int $menuGroupId
     * @param Request $request
     * @param MenuValidator $menuValidator
     * @param MenuFavoriteModel $menuFavoriteModel
     *
     * @return \Dingo\Api\Http\Response
     *
     */
    public function storeMass(
        $menuGroupId,
        Request $request
    ) {
        $input = $request->getParsedBody();

        $input = json_decode($input['param'], true);

        try {
            if ($this->menuModel->saveMass($menuGroupId, $input, $this->menuValidator, $this->menuFavoriteModel)) {
                return $this->response->noContent();
            }
        } catch (ValidationHttpException $e) {
            return $this->response->error(implode("\n", $e->getErrors()->all()),
                IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Have some thing wrong');
    }

    /**
     * @SWG\Get(
     *     tags={"Menu"},
     *     path="/v1/menu-list",
     *     summary="Menu List By Menu Group ID or Menu Group Name",
     *     description="Read Menu by Group ID or Menu Group Name",
     *     operationId="showGroup",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         in="query",
     *         name="menu_group_id",
     *         description="Either Menu Group Id or Menu Group Name is required",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="query",
     *         name="menu_group_name",
     *         description="Either Menu Group Id or Menu Group Name is required",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Menu List Group",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 description="Menu Item",
     *                 properties={
     *                     @SWG\Property(property="menu_id", type="integer"),
     *                     @SWG\Property(property="name", type="string"),
     *                     @SWG\Property(property="description", type="string"),
     *                     @SWG\Property(property="url", type="string"),
     *                     @SWG\Property(property="target", type="string"),
     *                     @SWG\Property(property="display_order", type="integer"),
     *                     @SWG\Property(property="icon_class", type="string"),
     *                     @SWG\Property(property="permission_name", type="string"),
     *                     @SWG\Property(property="nodes", type="object", description="Menu Item Node",
     *                         properties={
     *                             @SWG\Property(property="menu_id", type="integer"),
     *                             @SWG\Property(property="name", type="string"),
     *                             @SWG\Property(property="description", type="string"),
     *                             @SWG\Property(property="url", type="string"),
     *                             @SWG\Property(property="target", type="string"),
     *                             @SWG\Property(property="display_order", type="integer"),
     *                             @SWG\Property(property="icon_class", type="string"),
     *                             @SWG\Property(property="permission_name", type="string"),
     *                             @SWG\Property(property="nodes", type="object"),
     *                        },
     *                     )
     *                 },
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_id": 1,
     *                     "name": "Product Page",
     *                     "description": "This is product page from Swagger",
     *                     "url": "http://wms2.seldat.com",
     *                     "target": "Target",
     *                     "display_order": 1,
     *                     "icon_class": "ico",
     *                     "permission_name": "viewMenuList",
     *                     "nodes": {},
     *                 }
     *             }
     *         },
     *     ),
     * )
     */
    /**
     * List all Menus in a group
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function menuList(Request $request)
    {
        // get input from request
        $queryParams = $request->getQueryParams();

        $menuGroupId = array_get($queryParams, 'menu_group_id', 0);
        $menuGroupName = array_get($queryParams, 'menu_group_name', '');

        // error
        if (!$menuGroupId && !$menuGroupName) {
            return $this->response->errorBadRequest('At least fill menu_group_id or menu_group_name');
        }

        // no Menu Group ID, only fill Menu Group Name
        if (!$menuGroupId && $menuGroupName) {
            if (!$menuGroup = $this->menuGroupModel->getByName($menuGroupName)) {
                return $this->response->errorNotFound('Menu Group Name does not exist');
            }

            $menuGroupId = $menuGroup->menu_group_id;
        }

        $menus = $this->menuModel->getHierarchyMenuByMenuGroupId($menuGroupId);

        return $this->response->array(['data' => $menus]);
    }

    /**
     * @SWG\Post(
     *     tags={"Menu"},
     *     path="/v1/menu-list",
     *     summary="Menu List By (Menu Group ID or Menu Group Name) and Permission",
     *     description="Read Menu by (Menu Group ID or Menu Group Name) and permission",
     *     operationId="menuListByPermissions",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         in="query",
     *         name="menu_group_id",
     *         description="Either Menu Group Id or Menu Group Name is required",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         in="query",
     *         name="menu_group_name",
     *         description="Either Menu Group Id or Menu Group Name is required",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="permission",
     *         description="separate by comma",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Return array of Menu List Group",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 description="Menu Item",
     *                 properties={
     *                     @SWG\Property(property="menu_id", type="integer"),
     *                     @SWG\Property(property="name", type="string"),
     *                     @SWG\Property(property="description", type="string"),
     *                     @SWG\Property(property="url", type="string"),
     *                     @SWG\Property(property="target", type="string"),
     *                     @SWG\Property(property="display_order", type="integer"),
     *                     @SWG\Property(property="icon_class", type="string"),
     *                     @SWG\Property(property="permission_name", type="string"),
     *                     @SWG\Property(property="nodes", type="object", description="Menu Item Node",
     *                         properties={
     *                             @SWG\Property(property="menu_id", type="integer"),
     *                             @SWG\Property(property="name", type="string"),
     *                             @SWG\Property(property="description", type="string"),
     *                             @SWG\Property(property="url", type="string"),
     *                             @SWG\Property(property="target", type="string"),
     *                             @SWG\Property(property="display_order", type="integer"),
     *                             @SWG\Property(property="icon_class", type="string"),
     *                             @SWG\Property(property="permission_name", type="string"),
     *                             @SWG\Property(property="nodes", type="object"),
     *                        },
     *                     )
     *                 },
     *             ),
     *         ),
     *         examples={
     *             "data": {
     *                 {
     *                     "menu_id": 1,
     *                     "name": "Product Page",
     *                     "description": "This is product page from Swagger",
     *                     "url": "http://wms2.seldat.com",
     *                     "target": "Target",
     *                     "display_order": 1,
     *                     "icon_class": "ico",
     *                     "permission_name": "viewMenuList",
     *                     "nodes": {}
     *                 }
     *             }
     *         }
     *     )
     * )
     */
    /**
     * List all Menus in a group by Permission Names
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function menuListByPermissions(Request $request)
    {
        $queryParams = $request->getQueryParams();
        $input = $request->getParsedBody();

        $menuGroupId = array_get($queryParams, 'menu_group_id', 0);
        $menuGroupName = array_get($queryParams, 'menu_group_name', '');
        $permissionNames = (isset($input['permission']) && !empty($input['permission']))
            ? explode(',', array_get($input, 'permission'))
            : [];

        // error
        if (!$menuGroupId && !$menuGroupName) {
            return $this->response->errorBadRequest('At least fill menu_group_id or menu_group_name');
        }

        // error
        if (empty($permissionNames)) {
            return $this->response->errorBadRequest('At least fill permission name');
        }

        // no Menu Group ID, only fill Menu Group Name
        if (!$menuGroupId && $menuGroupName) {
            if (!$menuGroup = $this->menuGroupModel->getByName($menuGroupName)) {
                return $this->response->errorNotFound('Menu Group Name does not exist');
            }

            $menuGroupId = $menuGroup->menu_group_id;
        }

        $menus = $this->menuModel->getHierarchyMenuByPermissionName($menuGroupId, $permissionNames);

        return $this->response->array(['data' => $menus]);
    }
}
