<?php

namespace App\Api\V1\Validators;


class EdiValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'edi_trans' => 'required',
            'edi_name'  => 'required',
            'des'       => 'required',
            'edi_sts'   => 'required',
        ];
    }
}
