<?php

namespace Seldat\Wms2\Models;

/**
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property mixed loc_type_id
 * @property mixed loc_type_name
 * @property mixed loc_type_code
 * @property mixed loc_type_desc
 */
class LocationType extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loc_type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'loc_type_id';

    /**
     * @var array
     */
    protected $fillable = [
        'loc_type_name',
        'loc_type_code',
        'loc_type_desc',
    ];

    public function location()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_type_id', 'loc_type_id');
    }
}
