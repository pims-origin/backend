<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V4\Inbound\Pallet\ScanPallet\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackDtl;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class PackDtlModel
 *
 * @package App\Api\V3\Inbound\Models
 */
class PackDtlModel extends AbstractModel
{
    /**
     * PackDtlModel constructor.
     *
     * @param PackDtl|null $model
     */
    public function __construct(PackDtl $model = null)
    {
        $this->model = ($model) ?: new PackDtl();
    }
}
