<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Models\SummaryModel;
use League\Fractal\TransformerAbstract;

class InvoiceListTransformer extends TransformerAbstract
{
    public $fromDate = NULL;
    public $toDate = NULL;

    public function transform($invoiceList)
    {
        $statusCode = $invoiceList->inv_sts;
        $invoiceNumber = $invoiceList->inv_num;

        $summary = new SummaryModel();

        $amt = INVOICE_STATUSES[$statusCode] == 'Open' ?
                $summary->getAmt([
                    'clientID' => $invoiceList->cus_id,
                    'whsID' => $invoiceList->whs_id,
                    'fromDate' => $this->fromDate,
                    'toDate' => $this->toDate,
                    'curCode' => $invoiceList->inv_cur,
                ]) : $invoiceList->inv_amt;

        $headerInfo = $summary->getInvoiceDetails($invoiceNumber);

        return [
            'inv_sts' => INVOICE_STATUSES[$statusCode],
            'cus_name' => $invoiceList->cus_name,
            'inv_num' => $invoiceList->inv_num,
            'inv_dt' => $headerInfo['invoiceDate'],
            'cncl_inv_num' => $headerInfo['cancellationNumber'],
            'cur_code' => $invoiceList->inv_cur,
            'amt' => $amt,
            'pmnt_rcv_dt' => $headerInfo['paymentDate'],
            'check_num' => $headerInfo['checkNumber'],
            'invoice_number' => $invoiceList->invoice_number,
            'cus_id' => $invoiceList->cus_id,
        ];
    }

    /*
    ****************************************************************************
    */

}
