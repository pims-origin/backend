<?php

namespace App\Api\Ecom\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\XferDtl;
use Symfony\Component\HttpKernel\Exception\HttpException;

class XferDetailTransformer extends TransformerAbstract
{
    public function transform(XferDtl $xferDtl)
    {
        $sku_size_color = array_get($xferDtl, 'sku');

        if (!empty(array_get($xferDtl, 'size')) && array_get($xferDtl, 'size') != 'NA') {
            $sku_size_color .= '-'.array_get($xferDtl, 'size');
        }

        if (!empty(array_get($xferDtl, 'color')) && array_get($xferDtl, 'color') != 'NA') {
            $sku_size_color .= '-'.array_get($xferDtl, 'color');
        }

        $is_last = false;
        $act_qty = 0;
        $remain_qty = 0;

        $xferDtl = [];

        for ($i = 0; $i < count($xferTicket->xferDtl); $i++) {
            $act_qty += $xferTicket->xferDtl{$i}->act_piece_qty ?? 0;
            $remain_qty += $xferTicket->xferDtl{$i}->allocated_qty ?? 0;
            if ($xferTicket->xferDtl{$i}->xfer_dtl_sts != 'CO' && empty($xferDtl)) {
                $avail = DB::table('cartons')
                    ->where('item_id', object_get($xferHdr, 'item_id'))
                    ->where('loc_id', $xferTicket->xferDtl{$i}->req_loc_id)
                    ->where('cus_id', object_get($xferHdr, 'cus_id'))
                    ->where('deleted', 0)
                    ->where('ctn_sts', 'AC')
                    ->whereRaw('(is_ecom is null or is_ecom = 0)')
                    ->sum('piece_remain');

                if ($i == count($xferTicket->xferDtl) - 1) {
                    $is_last = true;
                }

                $xferDtl = [
                    'xfer_dtl_id'   => $xferTicket->xferDtl{$i}->xfer_dtl_id,
                    'to_loc_id'     => $xferTicket->xferDtl{$i}->mez_loc_id,
                    'to_loc_code'   => $xferTicket->xferDtl{$i}->mez_loc,
                    'from_loc_id'   => $xferTicket->xferDtl{$i}->req_loc_id,
                    'from_loc_code' => $xferTicket->xferDtl{$i}->req_loc,
                    'avail_qty'     => $avail,
                    'is_last'       => $is_last,
                    'cus_id'        => $xferTicket->xferDtl{$i}->cus_id
                ];
            }
        }

        if (empty($xferDtl)) {
            throw new HttpException(404, 'Xfer Detail is not exists or completed!');
        }


        return [
            'xfer_ticket_id'  => object_get($xferTicket, 'xfer_ticket_id'),
            'xfer_ticket_num' => object_get($xferTicket, 'xfer_ticket_num'),
            'xfer_dtl_id'     => $xferDtl['xfer_dtl_id'],
            'item_id'         => object_get($xferHdr, 'item_id'),
            'sku'             => $sku_size_color,
            'cus_upc'         => object_get($xferHdr, 'cus_upc'),
            'ucc128'          => Item::generateUpc128(object_get($xferHdr, 'cus_id'), object_get($xferHdr, 'item_id')),
            'to_loc_id'       => $xferDtl['to_loc_id'],
            'to_loc_code'     => $xferDtl['to_loc_code'],
            'qty'             => $act_qty. '/' . $remain_qty,
            'from_loc_id'     => $xferDtl['from_loc_id'],
            'from_loc_code'   => $xferDtl['from_loc_code'],
            'avail_qty'       => $xferDtl['avail_qty'],
            'is_last'         => $xferDtl['is_last'],
            'cus_id'          => $xferDtl['cus_id']
        ];
    }
}
