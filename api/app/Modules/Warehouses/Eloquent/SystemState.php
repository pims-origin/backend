<?php

namespace App\Modules\Warehouses\Eloquent;

use Illuminate\Database\Eloquent\Model;

class SystemState extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_state';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sys_state_id';

    /**
     * @var array
     */
    protected $fillable = [
        'sys_state_code',
        'sys_state_name',
        'sys_state_country_id',
        'sys_state_desc'
    ];
}
