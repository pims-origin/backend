<?php

namespace App\Api\V1\Transformers;

use App\Sac;
use League\Fractal\TransformerAbstract;

class SacTransformer extends TransformerAbstract
{

    /**
     * ps:Show SAC
     *
     * @param Sac $sac
     *
     * @return array
     */
    public function transform(Sac $sac)
    {
        return [
            "sac_id"         => $sac->sac_id,
            "sac"            => $sac->sac,
            "sac_name"       => $sac->sac_name,
            "description"    => $sac->description,
            "type"           => $sac->type,
            "type_name"      => $sac->type_name,
            "range_from"     => $sac->range_from,
            "range_to"       => $sac->range_to,
            "range_uom"      => $sac->range_uom,
            "range_uom_name" => $sac->range_uom_name,
            "period_uom"     => $sac->period_uom,
            "period_uom_name"=> $sac->period_uom_name,
            "block"          => $sac->block,
            "unit_uom"       => $sac->unit_uom,
            "unit_uom_name"  => $sac->unit_uom_name,
            "sac_period"     => $sac->sac_period,
            "price"          => $sac->price,
            'base_price'     => $sac->price
        ];
    }
}
