<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\BaseService;
use App\Api\V1\Models\WarehouseService;
use App\Api\V1\Models\ZoneService;
use App\Api\V1\Traits\ZoneLocController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;
use GuzzleHttp\Psr7\Response;


class ZoneController extends AbstractController
{
    use ZoneLocController;
    /**
     * @var MenuService
     */
    protected $service;

    /**
     * MenuController constructor.
     *
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        parent::__construct($request, new AuthenticationService($request));
        $this->service = new ZoneService($request);
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     * @param $zoneId
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function update(IRequest $request, $warehouseId, $zoneId)
    {
        $input = $request->getParsedBody();
        $result = $this->service->edit($input, $warehouseId, $zoneId);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     *
     * @return \Illuminate\Http\Response
     */
    public function search(IRequest $request, $warehouseId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->search($warehouseId, $queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     *
     * @return \Illuminate\Http\Response
     */
    public function viewWHSearch(IRequest $request, $warehouseId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->search($warehouseId, $queryStr, 'wh-');
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     * @param $zoneId
     *
     * @return \Illuminate\Http\Response
     */
    public function getLocations(IRequest $request, $warehouseId, $zoneId)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->getLocations($warehouseId, $zoneId, $queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     * @param $zoneId
     *
     * @return \Illuminate\Http\Response|IResponse
     */
    public function addLocations(IRequest $request, $warehouseId, $zoneId)
    {
        $input = $request->getParsedBody();

        $result = $this->service->addlocations($input, $warehouseId, $zoneId);

        if ($result instanceof Response) {
            $response = $this->convertResponse($result);

            return $response;
        }

        return $result;
    }

    /**
     * @param IRequest $request
     * @param $warehouseId
     *
     * @return \Illuminate\Http\Response
     */
    public function freeZones(IRequest $request)
    {
        $queryStr = $request->getUri()->getQuery();
        $result = $this->service->freeZones($queryStr);
        $response = $this->convertResponse($result);

        return $response;
    }
}
