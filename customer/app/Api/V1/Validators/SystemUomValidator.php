<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:58
 */

namespace App\Api\V1\Validators;


class SystemUomValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'sys_uom_code' => 'required',
            'sys_uom_name' => 'required',
        ];
    }
}