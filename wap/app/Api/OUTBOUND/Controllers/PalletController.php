<?php
/**
 * Created by PhpStorm.
 *
 * Date: loc_id-July-16
 * Time: 10:00
 */

namespace App\Api\OUTBOUND\Controllers;

use App\Api\OUTBOUND\Models\LocationModel;
use App\Api\OUTBOUND\Models\PackHdrModel;
use App\Api\OUTBOUND\Validators\PutBackPallet;
use App\Api\OUTBOUND\Validators\PutBackPalletValidator;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\OUTBOUND\Models\EventTrackingModel;
use App\Api\OUTBOUND\Models\OrderCartonModel;
use App\Api\OUTBOUND\Models\OrderDtlModel;
use App\Api\OUTBOUND\Models\OrderHdrModel;
use App\Api\OUTBOUND\Models\PalletModel;
use App\Api\OUTBOUND\Models\WavePickDtlModel;
use App\Api\OUTBOUND\Models\WavePickHdrModel;
use App\Api\OUTBOUND\Models\InventorySummaryModel;
use App\Api\OUTBOUND\Models\OutPalletModel;
use App\Api\V1\Models\Log;
use App\Api\V1\Models\WaveDtlLocModel;
use App\libraries\RFIDValidate;
use App\Api\V1\Validators\OutPalletValidator;
use App\MessageCode;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\OUTBOUND\Models\CartonModel;
use Seldat\Wms2\Utils\Status;
use Illuminate\Http\Response as IlluminateResponse;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use TCPDF;

/**
 * Class PalletController
 *
 * @package App\Api\V1\Controllers
 */
class PalletController extends AbstractController
{
    protected $palletModel;
    protected $virtualCartonModel;
    protected $orderDtlModel;
    protected $scanPallet;
    protected $locationModel;
    protected $locationStatusDetailModel;
    protected $cartonModel;
    protected $asnDtlModel;
    protected $eventTrackingModel;
    protected $goodsReceiptModel;
    protected $goodsReceiptDetailModel;
    protected $vtlCtnModel;
    protected $orderCartonModel;
    protected $vtlCtnSumModel;
    protected $asnHdrModel;
    protected $wvDtlModel;
    protected $waveHdrModel;
    protected $orderHdrModel;
    protected $itemModel;
    protected $packHdrModel;
    protected $packDtlModel;
    protected $InventorySummaryModel;
    protected $outPalletModel;


    public function __construct(
        CartonModel $cartonModel
    ) {
        $this->palletModel = new PalletModel();
        $this->cartonModel = $cartonModel;
        $this->goodsReceiptModel = new GoodsReceiptModel();
        $this->wvDtlModel = new WavePickDtlModel();
        $this->orderDtlModel = new OrderDtlModel();
        $this->orderCartonModel = new OrderCartonModel();
        $this->orderHdrModel = new OrderHdrModel();
        $this->waveHdrModel = new WavePickHdrModel();
        $this->InventorySummaryModel = new InventorySummaryModel();
        $this->outPalletModel = new OutPalletModel();
        $this->eventTrackingModel = new EventTrackingModel();
        $this->packHdrModel = new PackHdrModel();
        $this->locationModel = new LocationModel();
    }

    public function updateWavePick(Request $request, $whsId, $wv_id)
    {
        set_time_limit(0);
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/$whsId/wave/{$wv_id}/update";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'UWP',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Update wave pick'
        ]);

        /*
         * end logs
         */

        $codes = array_get($input, 'code', null);

        if (!$codes) {
            $msg = "Please check input params.";
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $palletRFID = array_get($codes, 'pallet', null);
        //$pallet = array_get($codes, 'pallet', null);
        $cartons = array_get($codes, 'ctns', null);

        //check invalid code carton
        $ctnRfidInValid = [];
        $errorMsg = '';
        if (!empty($cartons)) {
            foreach ($cartons as $item) {
                $ctnRFIDValid = new RFIDValidate($item, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $item;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }
            }
        } else {
            $msg = 'Ctns array is required!';
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {
            $data = [
                'data'    => $ctnRfidInValid,
                'message' => $errorMsg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        if ($palletRFID) {
            $pltRFIDValid = new RFIDValidate($palletRFID, RFIDValidate::TYPE_PALLET, $whsId);
            //validate pallet RFID
            if (!$pltRFIDValid->validate()) {
                $data = [
                    'data'    => null,
                    'message' => $pltRFIDValid->error,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }

        // check valid wave pick
        $wvDtlData = $this->wvDtlModel->getModel()
            ->where([
                'wv_dtl.wv_id'  => $wv_id,
                'wv_dtl.whs_id' => $whsId,
            ])
            ->join('wv_hdr', 'wv_hdr.wv_id', '=', 'wv_dtl.wv_id')
            ->whereIn('wv_hdr.wv_sts'    , ['PK', 'NW'])
            ->whereIn('wv_dtl.wv_dtl_sts', ['PK', 'NW'])
            ->get();

        // Check wave pick detail existed
        if (empty($wvDtlData) || count($wvDtlData) == 0) {
            $msg = "Wave pick doesn't exist.";
            $wvDtlData = $this->wvDtlModel->getModel()
                ->where([
                    'wv_id'  => $wv_id,
                    'whs_id' => $whsId,
                ])
                ->first();

            if ($wvDtlData) {
                $msg = "Wave pick already completed or canceled.";
            } else {
                $wvDtlData = $this->wvDtlModel->getModel()
                    ->where([
                        'wv_id' => $wv_id,
                    ])
                    ->first();

                if ($wvDtlData) {
                    $msg = "Wave pick doesn't belong to warehouse.";
                }
            }

            $data = [
                'data'    => $wv_id,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            \DB::beginTransaction();

            //get carton list by carton RFID
            $ctnLists = $this->getCartonListByRFIDs($cartons);

            if (count($ctnLists) != count($cartons)) {

                //validate carton RFID
                foreach ($cartons as $carton) {
                    $cartonByRFIDInfo = $this->cartonModel->getCartonByCtnRFID($carton);
                    //check carton RFID is existed or not
                    if (!$cartonByRFIDInfo) {
                        $msg = sprintf("Carton RFID %s is not existed.", $carton);
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }

                    //check carton RFID should be ACTIVE
                    if ($cartonByRFIDInfo->ctn_sts != 'AC') {
                        $msg = sprintf("Status of carton RFID %s is %s, it should be ACTIVE.", $carton,
                            Status::getByValue($cartonByRFIDInfo->ctn_sts, "CTN_STATUS"));
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }

                    //check carton RFID with piece remain must > 0
                    if ($cartonByRFIDInfo->piece_remain < 1) {
                        $msg = sprintf("Piece remain of carton RFID %s should greater than zero.", $carton);
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }

                    //check carton RFID is damaged or not
                    if ($cartonByRFIDInfo->is_damaged == 1) {
                        $msg = sprintf("Carton RFID %s has damaged.", $carton);
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }

                    //check is_ecom of carton RFID
                    if ($cartonByRFIDInfo->is_ecom == 1) {
                        $msg = sprintf("Carton RFID %s is only for eCommerce Order.", $carton);
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }

                    //check carton RFID must belong to a location
                    if (!$cartonByRFIDInfo->loc_id) {
                        $msg = sprintf("Carton RFID %s is not belong to any location.", $carton);
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }

                    //check carton RFID must belong to a pallet
                    if (!$cartonByRFIDInfo->plt_id) {
                        $msg = sprintf("Carton RFID %s is not belong to any pallet.", $carton);
                        $data = [
                            'data'    => null,
                            'message' => $msg,
                            'status'  => false,
                        ];

                        return new Response($data, 200, [], null);
                    }

                }

                $trueRFIDs = array_pluck($ctnLists, 'rfid', null);
                $wrongRFIDS = array_diff($cartons, $trueRFIDs);
                $msg = "At least 1 carton rfid doesn't exist";
                $data = [
                    'data'    => $wrongRFIDS,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
            // cartons with Active status on a LOCKED location
            $locIds = array_pluck($ctnLists, 'loc_id');
            $notActiveLocs = $this->locationModel->getNotActiveLocByIds($locIds);
            if ($notActiveLocs) {
                $locRfidNotActive = array_pluck($notActiveLocs, 'rfid');
                $msg = sprintf("Cannot update wavepick for cartons on locations that are not active: %s.", implode(', ', $locRfidNotActive));
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
            $dataWaveByCartons = [
                'whs_id'   => $whsId,
                'cartons'  => $cartons,
                'wv_id'    => $wv_id,
                'ctnLists' => $ctnLists,
            ];

            $wvSts = $this->updateWavePickByCarton($dataWaveByCartons, $wvDtlData);

            $wvDtlData = $wvDtlData->first();
            if ($wvSts) {
                $event = [
                    'whs_id'    => $whsId,
                    'cus_id'    => object_get($wvDtlData, 'cus_id'),
                    'owner'     => object_get($wvDtlData, 'wv_num'),
                    'evt_code'  => 'WPC',
                    'trans_num' => object_get($wvDtlData, 'wv_num'),
                    'info'      => "Wave Pick completed",
                ];

                //  Evt tracking
                $this->eventTrackingModel->refreshModel();
                $this->eventTrackingModel->create($event);
            }

            \DB::commit();

            $msg = "Update wave pick successfully";
            $data = [
                'message' => $msg,
                'data'    => [
                    'picked' => $wvSts,
                ],
                'status'  => true,
            ];

            //WMS2-4274: [Outbound][Update wave pick] - No message display when scanning over expected carton for the order
            $wvDtlTotalCarton = $this->wvDtlModel->getDetailTotalCartons($whsId, $wv_id);
            $data['data']['wavepick_detail'] = $wvDtlTotalCarton;

            return new Response($data, 200, [], null);

        } catch (\Exception $e) {
            \DB::rollback();
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    private function getWvDtlByVvID($wv_id, $whs_id)
    {
        // Get wave pick details info
        $dataWaveDtlInfo = $this->wvDtlModel->getModel()
            ->where([
                'wv_id'  => $wv_id,
                'whs_id' => $whs_id,
            ])
            ->whereIn('wv_dtl_sts', ['PK', 'NW'])
            ->get();

        // Check wave pick detail existed
        if (empty($dataWaveDtlInfo) || count($dataWaveDtlInfo) == 0) {
            $msg = "Wave pick details don't exist.";
            $dataWaveDtlInfo = $this->wvDtlModel->getModel()
                ->where([
                    'wv_id'  => $wv_id,
                    'whs_id' => $whs_id,
                ])
                ->first();

            if ($dataWaveDtlInfo) {
                $msg = "Wave pick details already completed or canceled.";
            } else {
                $dataWaveDtlInfo = $this->wvDtlModel->getModel()
                    ->where([
                        'wv_id' => $wv_id,
                    ])
                    ->first();

                if ($dataWaveDtlInfo) {
                    $msg = "Wave pick details don't belong to warehouse.";
                }
            }

            throw  new \Exception($msg);
        }

        return $dataWaveDtlInfo;
    }

    private function getCartonListByRFIDs($cartons)
    {
        //get carton list by carton RFID
        $ctnLists = $this->cartonModel->getActiveCartonByCtnRFID($cartons)->toArray();

        if (is_null($ctnLists)) {
            $msg = Message::get("BM155");

            throw new \Exception($msg);

        }

        return $ctnLists;
    }

    private function updateWavePickByCarton($data, $dataWaveDtlInfo)
    {
        $whs_id = $data['whs_id'];
        $wv_id = $data['wv_id'];
        //$palletRFID = $data['pallet'];
        $cartons = $data['cartons'];
        $ctnLists = $data['ctnLists'];

        // Check active & existed carton
        $dataCheckActive = [
            'ctn-rfid' => $cartons,
            'ctnLists' => $ctnLists
        ];
        $this->checkActiveCarton($dataCheckActive);

        // Get wave pick details info
        // $dataWaveDtlInfo = $this->getWvDtlByVvID($wv_id, $whs_id);

        //check carton Item ID = WV_dtl ID
        $wvDtlCodes = [];

        foreach ($dataWaveDtlInfo as $wvDtl) {
            $wvDtlCodes[] = ($wvDtl['item_id'] . '@^' . $wvDtl['lot']);
        }

        $wvDtlCodesAll = $wvDtlCodes;
        $arrCartonProcessed = []; //cartons which are picked
        $inventorySummaryModel = new InventorySummaryModel();

        $wvSts = false;
        // \DB::beginTransaction();
        $invtValues = [];
        foreach ($ctnLists as $key => $ctn) {
            $invtValues[] = [
                'whs_id'     => $ctn['whs_id'],
                'item_id'    => $ctn['item_id'],
                'lot'        => $ctn['lot'],
                'picked_qty' => $ctn['piece_remain']
            ];
        }
        foreach ($dataWaveDtlInfo as $wvDtl) {
            $pickedQTY = 0;
            $cartonItemID = null;
            $wvDtlCode = ($wvDtl['item_id'] . '@^' . $wvDtl['lot']);

            $locIds = [];

            foreach ($ctnLists as $key => $ctn) {
                $cartonCode = ($ctn['item_id'] . '@^' . $ctn['lot']);

                //check cartons QTY is enough to wave pick or not
                $ctn_Qty_wvDtl = $wvDtl['ctn_qty'];
                $wvDtlID = $wvDtl['wv_dtl_id'];
                $wvNum = $wvDtl['wv_num'];
                $ctn_Qty_odr_ctn = $this->orderCartonModel->countOrderCTByWvDtlId($wvDtlID);
                if ($ctn_Qty_wvDtl == $ctn_Qty_odr_ctn) {
                    $msg = sprintf(
                        'Had already received enough carton!'
                    );
                    throw new \Exception($msg);
                }

                if (!in_array($cartonCode, $wvDtlCodesAll)) {
                    $msg = sprintf(
                        'The carton rfid %s is not matched with wave'
                        . ' pick: %s', $ctn['rfid'], $wvDtl['wv_num']
                    );
                    throw new \Exception($msg);
                }

                if ($cartonCode == $wvDtlCode) {
                    if ($wvDtl['pack_size'] != $ctn['ctn_pack_size']) {
                        $msg = sprintf(
                            'The carton rfid %s pack size %d is not same pack size %d in wave pick details of %s!',
                            $ctn['rfid'], $ctn['ctn_pack_size'], $wvDtl['pack_size'], $wvDtl['wv_num']

                        );
                        throw new \Exception($msg);
                    }
                    $arrCartonProcessed[$key] = $ctn;
                    $pickedQTY += $ctn['piece_remain'];
                }


                $locIds[] = $ctn['loc_id'];
            }

            if (empty($arrCartonProcessed)) {
                continue;
            }

            //remove wave detail item which processed
            $wvDtlCodes = $this->removeItemFromArray($wvDtlCodes, $cartonCode);

            $actPicked = $wvDtl['act_piece_qty'] + $pickedQTY;

            if ($actPicked > $wvDtl['piece_qty']) {
                $msg = sprintf('Actual picked QTY (%d) must be not greater than wave pick QTY (%d)', $actPicked,
                    $wvDtl['piece_qty']);
                if ($pickedQTY > $wvDtl['piece_qty']) {
                    $msg = sprintf('Picked QTY (%d) must be not greater than wave pick QTY (%d)', $pickedQTY,
                        $wvDtl['piece_qty']);
                }
                throw new \Exception($msg);
            }

            $wvDtlSts = Status::getByValue("Picking", "WAVEPICK-DETAIL-STATUS");
            if ($actPicked == $wvDtl['piece_qty']) {
                $wvDtlSts = Status::getByValue("Picked", "WAVEPICK-DETAIL-STATUS");
            }

            // Update wave pick detail
            $dataUpdateWvDtl = [
                'act_piece_qty' => $actPicked,
                'wv_dtl_id'     => $wvDtl['wv_dtl_id'],
                'wv_dtl_sts'    => $wvDtlSts,
            ];

            //update wave pick detail
            $this->wvDtlModel->updateWvDtlPickCtn($dataUpdateWvDtl);

            $this->waveHdrModel->updateWhere([
                'wv_sts' => 'PK'
            ], [
                'wv_id' => $wv_id
            ]);

            //insert order carton
            $this->orderCartonModel->insertOrderCartons($arrCartonProcessed, $wvDtl);

            //update table carton to pick, pick_date, status, storage_duration
            $this->updateCartonInfoToPicked($arrCartonProcessed);

            //update wave header status
            $wvSts = $this->waveHdrModel->updatePicked($wv_id);

            // update invetory summary

            //update pallet ctn_ttl
            // update pallet zero date
            $locIds = array_unique($locIds);
            $this->palletModel->updatePalletCtnTtl($locIds);
            $this->palletModel->updateZeroPallet($locIds);

            /**
             * 1. sort cartons by wave details id
             * 2. sort cartons by locations
             * 3. calculate picked qty and actual location
             * 4. Get wv_dtl_loc
             * 5. if existed, update or create
             */
            $wdtLoc = $this->cartonModel->updateWvDtlLoc($arrCartonProcessed);

            if ($wdtLoc) {
                $this->insertWaveDtlLoc($wdtLoc, $wvDtl);
            }

            //remove array cartons which is processed from cartonList
            foreach ($arrCartonProcessed as $key => $value) {
                unset($ctnLists[$key]);
            }
            unset($arrCartonProcessed);
        }

        foreach ($invtValues as $intUpdate) {
            $allocatedSql = sprintf('`allocated_qty` - %d', $intUpdate['picked_qty']);
            $pickedSql = sprintf('`picked_qty` + %d', $intUpdate['picked_qty']);
            //check allocated qty
            $ivt = $inventorySummaryModel->getFirstWhere(
                [
                    'item_id' => $intUpdate['item_id'],
                    'whs_id'  => $intUpdate['whs_id'],
                    'lot'     => $intUpdate['lot'],
                ]
            );

            if ((int)$ivt->allocated_qty < $intUpdate['picked_qty']) {
                $msg = "Number of cartons in wavepick updating must be not greater than the number of remaining cartons need to be updated";
                throw new \Exception($msg);
            }

            $inventorySummaryModel->updateWhere([
                'allocated_qty' => DB::raw($allocatedSql),
                'picked_qty'    => DB::raw($pickedSql),
            ], [
                'item_id' => $intUpdate['item_id'],
                'whs_id'  => $intUpdate['whs_id'],
                'lot'     => $intUpdate['lot'],
            ]);
        }

        // \DB::commit();

        return $wvSts;
    }

    public function insertWaveDtlLoc($wdtLoc, $wvDtl)
    {
        $wvDtlLoc = (new WaveDtlLocModel())->getModel()->where('wv_dtl_id', $wvDtl->wv_dtl_id)->first();

        if ($wvDtlLoc) {
            if ($wvDtlLoc->act_loc_ids) {
                $actLocs = json_decode($wvDtlLoc->act_loc_ids, true);
                $actLocs = array_merge($actLocs, $wdtLoc);
                $wvDtlLoc->act_loc_ids = json_encode($actLocs);
            } else {
                $wvDtlLoc->act_loc_ids = json_encode($wdtLoc);
            }

            return $wvDtlLoc->update();
        }

        return;

    }

    private function removeItemFromArray($arr, $val)
    {
        unset($arr[array_search($val, $arr)]);

        return array_values($arr);
    }

    private function checkActiveCarton($data)
    {
        $ctnRFID = $data['ctn-rfid'];
        $ctnLists = $data['ctnLists'];

        $tempCtns = array_map(function ($e) {
            return [
                'ctn_id' => $e['ctn_id'],
                'rfid'   => $e['rfid'],
            ];
        }, $ctnLists);

        $allCtns = array_pluck($tempCtns, null, "rfid");

        foreach ($ctnRFID as $key => $value) {
            if (!isset($allCtns[$value])) {
                $msg = "Carton " . $value . " is not active or existing";
                throw new \Exception($msg);
            }
        }
    }

    /**
     * @param $wvHdrId
     *
     * @return bool|mixed
     * @deprecated  Don't use this
     */
    private function updateWaveHeaderPicked($wvHdrId)
    {

        $allDetail = DB::table($this->wvDtlModel->getTable())
            ->select(['wv_dtl_sts'])
            ->where(['wv_id' => $wvHdrId])
            ->get();

        if (!empty($allDetail)) {
            $picked = true;
            foreach ($allDetail as $detail) {
                if ($detail['wv_dtl_sts'] != Status::getByValue('Picked', 'WAVE_DETAIL_STATUS')) {
                    $picked = false;
                    break;
                }
            }

            if ($picked) {
                return $this->waveHdrModel->update(
                    [
                        'wv_id'  => $wvHdrId,
                        'wv_sts' => Status::getByValue('Completed', 'WAVE_HDR_STATUS')
                    ]
                );
            } else {
                $this->waveHdrModel->update(
                    [
                        'wv_id'  => $wvHdrId,
                        'wv_sts' => Status::getByValue('Picking', 'WAVE_HDR_STATUS')
                    ]);
            }

            return $picked;
        }

        return false;
    }


    private function updateCartonInfoToPicked($cartonList)
    {
        foreach ($cartonList as $ctn) {
            $created_at = array_get($ctn, 'created_at', 0);

            // Calculate storage_duration
            if (is_string($created_at) || is_int($created_at)) {
                $created_at = (int)$created_at;
            } else {
                $created_at = $created_at->timestamp;
            }

            $storageDate = date("Y-m-d", $created_at);

            $pickedDate = date("Y-m-d");

            $dateDiff = (int)round(abs(strtotime($storageDate) - strtotime($pickedDate)) / 86400);

            if ($dateDiff == 0) {
                $storageDuration = 1;
            } else {
                $storageDuration = $dateDiff;
            }

            $dataCartonUpdate = [
                'ctn_sts'          => Status::getByKey('CTN_STATUS', 'PICKED'),
                'picked_dt'        => time(),
                'storage_duration' => $storageDuration,
                'loc_id'           => null,
                'loc_type_code'    => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'plt_id'           => null,
            ];

            $this->cartonModel->updateWhere($dataCartonUpdate, [
                'ctn_id' => $ctn['ctn_id']
            ]);

            $this->updatePalletPick($ctn);
        }
    }

    private function updatePalletPick($ctn)
    {
        // Update ctn_ttl in pallet table
        $plt_id = array_get($ctn, 'plt_id', null);

        $palletInfo = $this->palletModel->getFirstWhere(['plt_id' => $plt_id]);

        $this->palletModel->updateWhere([
            'ctn_ttl'    => array_get($palletInfo, 'ctn_ttl', null) - 1,
            'updated_at' => time(),
        ], [
            'plt_id' => $plt_id
        ]);

        //update total of carton on pallet
        if (0 == $palletInfo['ctn_ttl']) {
            $this->palletModel->updatePallet($palletInfo);
        }
    }

    public function putPalletShippingLane($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $plt_num = array_get($input, 'plt_num', null);
        $loc_rfid = array_get($input, 'loc_rfid', null);
        /*
         * start logs
         */

        $url = "/$whsId/shipping-lane/put-pallet";
        $owner = $plt_num;
        $transaction = $loc_rfid;
        Log:: info($request, $whsId, [
            'evt_code'     => 'PSL',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Pallet shipping Lane'
        ]);

        /*
         * end logs
         */


        $checkPlt = $this->outPalletModel->getFirstBy('plt_num', $plt_num);
        $checkLoc = $this->locationModel->getLocShippingLane($whsId, $loc_rfid);

        if (empty($checkPlt)) {
            $msg = sprintf("The pallet num %s doesn't exist", $plt_num);
            throw new \Exception($msg);
        }
        if (empty($checkLoc)) {
            $msg = sprintf("The location rfid %s doesn't exist", $loc_rfid);
            throw new \Exception($msg);
        }

        try {

            // start transaction
            DB::beginTransaction();

            // update location in out_pallet
            $dataPlt = [
                'loc_id'     => $checkLoc->loc_id,
                'loc_name'   => $checkLoc->loc_alternative_name,
                'loc_code'   => $checkLoc->loc_code,
                'updated_at' => time()
            ];

            $this->outPalletModel->updateWhere($dataPlt, ['plt_id' => $checkPlt->plt_id]);

            DB::commit();

            $msg = [];
            $msg['status'] = true;
            $msg['message'] = sprintf("Pallet number %s updated successfully on location %s!", $plt_num, $loc_rfid);

            return $this->response->noContent()
                ->setContent(['data' => $msg])
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function assignCartonToPallet($whsId, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $palletCode = array_get($input, 'pallet', null);
        $packs = array_get($input, 'packs', null);

        /*
         * start logs
         */

        $url = "/$whsId/pallet/assign-cartons";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'ACP',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Assign carton to pallet'
        ]);

        /*
         * end logs
         */

        if (empty($packs) || empty($palletCode)) {
            $msg = sprintf("Pallet and packs field are required");

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        /**
         * WMS2-3891: [WAP API - OUTBOUND] Able to assign packed cartons to invalid out-pallet
         */
        if (!OutPalletValidator::validate($palletCode)) {
            $msg = OutPalletValidator::ERROR_MSG;

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }
        // Check exists Order hdl num
        $pltNumArr = explode('-', $palletCode);
        if (count($pltNumArr) < 4) {
            $msg = sprintf("The pallet num %s is invalid", $palletCode);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }
        $odrNum = "ORD-" . $pltNumArr[1] . "-" . $pltNumArr[2];
        $order = $this->orderHdrModel->getFirstWhere(['odr_num' => $odrNum]);
        if (!$order) {
            $msg = sprintf("No order has been found for the pallet number %s", $palletCode);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        } else {
            $odrSts = object_get($order, 'odr_sts', '');
            if ($odrSts == 'CC') {
                $msg = sprintf('Unable to assign cartons to out pallet for canceled Order!');
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
        }

        // check pallet num
        $pallet = $this->outPalletModel->getFirstWhere([
            'whs_id'  => $whsId,
            'plt_num' => $palletCode,
        ]);

        if ($pallet && $pallet->out_plt_sts != 'AC') {
            $msg = sprintf("The pallet num %s is %s and not active", $palletCode, $pallet->out_plt_sts);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        // Check duplicate Pack hdl num
        $uniquePacks = array_unique($packs);
        if (count($uniquePacks) != count($packs)) {
            $duplicatePack = array_unique(array_diff_assoc($packs, $uniquePacks));
            $msg = 'Duplicate Pack: ' . implode(', ', $duplicatePack);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        $ctnRfidInValid = [];
        $errorMsg = '';
        foreach ($packs as $key => $pack) {
            //check invalid carton rfid when auto packs
            $checkRfid = substr($pack, 0, 3);
            if ($checkRfid !== "CTN") {
                $ctnRFIDValid = new RFIDValidate($pack, RFIDValidate::TYPE_CARTON, $whsId);
                if (!$ctnRFIDValid->validate()) {
                    $ctnRfidInValid[] = $pack;
                    if ('' == $errorMsg) {
                        $errorMsg = $ctnRFIDValid->error;
                    }
                }

                $ctnObj = $this->cartonModel->getFirstWhere(['rfid' => $pack]);
                $ctnId = object_get($ctnObj, 'ctn_id', null);
                $packHdr = $this->packHdrModel->getFirstWhere(['cnt_id' => $ctnId]);

                $packs[$key] = object_get($packHdr, 'pack_hdr_num', null);
            }
        }

        //if has errors, return all error data and message
        if ($ctnRfidInValid) {

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $errorMsg,
                    "status_code" => -1,
                ],
                "data"    => $ctnRfidInValid
            ];
        }

        try {
            // start transaction
            DB::beginTransaction();

            // update location in out_pallet
            $ttlCtn = 0;
            $odrId = $order->odr_id;
            $odrNum = $order->odr_num;

            $sts = false;

            foreach ((array)$packs as $packCode) {
                $packHdr = null;
                if (is_numeric(strpos($packCode, 'CTN'))) {
                    $packHdr = $this->packHdrModel->getFirstWhere([
                        'pack_hdr_num' => $packCode,
                        'whs_id'       => $whsId,
                    ]);
                } else {
                    $packHdr = $this->packHdrModel->getFirstWhere([
                        'pack_hdr_num' => $packCode,
                        'whs_id'       => $whsId,
                    ]);
                }

                if (!$packHdr) {
                    $msg = sprintf("Packed Carton %s is not found!", $packCode);

                    return [
                        "iat"     => time(),
                        "status"  => false,
                        "message" => [
                            "msg"         => $msg,
                            "status_code" => -1,
                        ],
                        "data"    => []
                    ];
                }

                if ($packHdr->out_plt_id) {
                    $pltId = object_get($pallet, 'plt_id', -1);
                    if ($packHdr->out_plt_id != $pltId) {
                        $msg = sprintf("The carton %s has been assigned other pallet already ", $packCode);

                        return [
                            "iat"     => time(),
                            "status"  => false,
                            "message" => [
                                "msg"         => $msg,
                                "status_code" => -1,
                            ],
                            "data"    => []
                        ];
                    }

                }


                // if (is_numeric(strpos($palletCode, 'LPN'))) {
                //     $res = (new OrderHdrModel())->isLPNinOrder($packHdr->odr_hdr_id, $palletCode, $odrNum);
                //     if (!$res) {
                //         $msg = sprintf("%s is not existed", $palletCode);

                //         return [
                //             "iat"     => time(),
                //             "status"  => false,
                //             "message" => [
                //                 "msg"         => $msg,
                //                 "status_code" => -1,
                //             ],
                //             "data"    =>  []
                //         ];
                //     }
                // }

                if ($odrId != $packHdr->odr_hdr_id) {
                    $msg = sprintf("Please scan cartons the same as order", $odrNum);

                    return [
                        "iat"     => time(),
                        "status"  => false,
                        "message" => [
                            "msg"         => $msg,
                            "status_code" => -1,
                        ],
                        "data"    => []
                    ];
                }
                if (!$pallet) {
                    $pallet = $this->outPalletModel->createNewPallet($packHdr, $palletCode, 0);
                }

                //Update Pack Status after asigned Pallet
                $packHdr->out_plt_id = $pallet->plt_id;
                $packHdr->pack_sts = 'AS';
                $packHdr->save();
                $ttlCtn++;

                $sts = $ttlCtn;
            }

            $pallet->ctn_ttl += $ttlCtn;
            $pallet->save();

            $staging = $this->orderHdrModel->updateOrderStaging($odrId);

            if ($odrId) {
                $odrObj = $this->orderHdrModel->getFirstWhere(
                    [
                        'odr_id' => $odrId
                    ]
                );
                $this->eventTrackingModel->eventTracking([
                    'whs_id'    => $whsId,
                    'cus_id'    => object_get($odrObj, 'cus_id', null),
                    'owner'     => object_get($odrObj, 'odr_num', null),
                    'evt_code'  => 'WOP',
                    'trans_num' => object_get($odrObj, 'odr_num', null),
                    'info'      => "WAP - Assigning Carton(s) to Pallet"
                ]);
            }

            DB::commit();

            $msg = sprintf("%d carton(s) assigned to pallet %s successfully!", count($packs),
                $palletCode);

            return [
                "iat"     => time(),
                "status"  => true,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => 1,
                ],
                "data"    => []
            ];

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $e->getMessage(),
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $whsId
     * @param $pltNum
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Exception
     */
    public function scannedPallet($whsId, $pltNum, Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        /*
         * start logs
         */

        $url = "/whs/$whsId/scan-pallet/$pltNum";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'SCP',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Scanned pallet'
        ]);

        /*
         * end logs
         */

        // check pallet num
        $outPallet = $this->outPalletModel->getFirstWhere([
            'whs_id'  => $whsId,
            'plt_num' => $pltNum,
        ]);

        if ($outPallet && $outPallet->out_plt_sts != 'AC') {
            $msg = sprintf("The pallet num %s is %s and not active", $pltNum, $pallet->out_plt_sts);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        // Check exists Order hdl num
        $odrNum = substr(str_replace("LPN", "ORD", $pltNum), 0, -5);
        $order = $this->orderHdrModel->getFirstWhere(['odr_num' => $odrNum]);
        if (!$order) {
            $msg = sprintf("No order has been found for the pallet num %s", $pltNum);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        $odrId = $order->odr_id;
        $cusId = $order->cus_id;

        $packTtl = $this->packHdrModel->packTotal($whsId, $odrId);
        if ($packTtl == 0) {
            $msg = sprintf("No cartons have been packed on the order %s yet.", $odrNum);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => $msg,
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        try {
            // start transaction
            DB::beginTransaction();

            $scnTtl = 0;

            if (!$outPallet) {
                $outPallet = $this->outPalletModel->createOutPallet($whsId, $cusId, $pltNum);
                $scnTtl = $this->packHdrModel->scannedTotal($whsId, $outPallet->plt_id);
            } else {
                $scnTtl = $this->packHdrModel->scannedTotal($whsId, $outPallet->plt_id);
            }

            DB::commit();

            return [
                "iat"     => time(),
                "status"  => true,
                "message" => [
                    "msg"         => "Successfully",
                    "status_code" => 1,
                ],
                "data"    => [
                    'odr_hdr_num' => $odrNum,
                    'pack_ttl'    => $packTtl,
                    'scn_ttl'     => count($scnTtl),
                ]
            ];

        } catch (\Exception $e) {
            DB::rollback();

            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code'      => 'ERR',
                'owner'         => $owner,
                'transaction'   => $transaction,
                'url_endpoint'  => $url,
                'message'       => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);

            return [
                "iat"     => time(),
                "status"  => false,
                "message" => [
                    "msg"         => "Not successfully",
                    "status_code" => -1,
                ],
                "data"    => []
            ];
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function updatePalletMove($whsId, $palletRFID, Request $request)
    {
        $input = $request->getParsedBody();
        /*
         * start logs
         */

        $url = "/whs/$whsId/pallet/$palletRFID/update-pallet-movement";
        $owner = $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'UPM',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Update pallet move'
        ]);
        /*
         * end logs
         */

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($palletRFID, RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $pltRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $pltObj = $this->palletModel->getPalletMoveWhereRFID($palletRFID, $whsId);
        if (!$pltObj) {
            $msg = sprintf("Pallet %s is not exited", $palletRFID);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        if ($pltObj->is_movement == 1) {
            $msg = sprintf("%s is already moved.", $palletRFID);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        if ($pltObj->loc_sts_code != "AC") {
            $loc_status = Status::getByValue($pltObj->loc_sts_code, 'LOCATION_STATUS');
            $msg = sprintf("Location's status of pallet RFID %s is %s", $palletRFID, $loc_status);
            $data = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        $pltObj->is_movement = 1;
        $pltObj->save();
        $result = $pltObj->save();
        if ($result) {
            $msg = sprintf("Pallet %s is movement successfully", $palletRFID);
            $data = [
                'data'    => $palletRFID,
                'message' => $msg,
                'status'  => true,
            ];

            return new Response($data, 200, [], null);
        } else {
            $msg = sprintf("Pallet %s move from RAC failed", $palletRFID);
            $data = [
                'data'    => $palletRFID,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response
     */
    public function updatePalletPutBack($whsId, Request $request)
    {
        $input = $request->getParsedBody();

        //validate
        //(new PutBackPalletValidator())->validate($input);
        $requireFieldsValidate = (new PutBackPalletValidator())->validateRequireFields($input);
        if ($requireFieldsValidate) {
            $msg = $requireFieldsValidate;
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }


        $palletRFID = $input['pallet_rfid'];
        $locRFID = $input['loc_rfid'];

        /*
         * start logs
         */

        $url = "outbound/whs/$whsId/update-pallet-put-back";
        $owner = "";
        $transaction = "";
        Log:: info($request, $whsId, [
            'evt_code'     => 'PPB',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Update pallet put back'
        ]);
        /*
         * end logs
         */

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($palletRFID, RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $pltRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        //validate location RFID
        $locRFIDValid = new RFIDValidate($locRFID, RFIDValidate::TYPE_LOCATION, $whsId);
        if (!$locRFIDValid->validate()) {
            $data = [
                'data'    => null,
                'message' => $locRFIDValid->error,
                'status'  => false,
            ];

            return new Response($data, 200, [], null);
        }

        try {
            //get pallet by RFID and check exist
            $pltObj = $this->palletModel->checkPalletPutBackWhereRFID($palletRFID, $whsId);
            if (!$pltObj) {
                $msg = sprintf("Pallet RFID %s is not exited.", $palletRFID);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //check ctn_ttl (>0)
            if (!$pltObj->ctn_ttl) {
                $msg = sprintf("There is no carton on %s", $palletRFID);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //check movement (is_movement=1)
            if (!$pltObj->is_movement) {
                $msg = sprintf("Pallet RFID %s is not movement", $palletRFID);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //check pallet is belong to anny location
            if (!$pltObj->loc_id) {
                $msg = sprintf("Pallet RFID %s is not belong to any location.", $palletRFID);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            if ($pltObj->whs_id != $whsId) {
                $msg = sprintf("%s is not belong to this warehouse.", $palletRFID);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            //get loc by RFID and check exist
            $locObj = $this->locationModel->getLocIdByRFID($locRFID);
            if (!$locObj) {
                $msg = sprintf("Location %s doesn't exist.", $locRFID);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }
            if ($locObj->loc_sts_code != "AC") {
                $msg = sprintf("Location %s is not activate.", $locRFID);
                $data = [
                    'data'    => null,
                    'message' => $msg,
                    'status'  => false,
                ];

                return new Response($data, 200, [], null);
            }

            $arrLoc = $locObj->toArray();

            //check pallet put back to old RAC, only update is_movement = 0
            if ($pltObj->loc_id == $arrLoc['loc_id']) {
                $pltObj->is_movement = 0;
                $pltObj->save();
                $skus = $this->cartonModel->getCartonsByPltId($whsId, $pltObj->plt_id);
                $msg = sprintf("Put back pallet %s to loction %s successfully.", $palletRFID, $pltObj->loc_code);
                $data = [
                    'data'    => [
                        'pallet' => $pltObj,
                        'skus'   => $skus
                    ],
                    'message' => $msg,
                    'status'  => true,
                ];

                return new Response($data, 200, [], null);
            } else { //pallet put back to new RAC

                //check loc is contained pallet or not
                $palletHasLoc = $this->palletModel->getLocationIsContainPallet($locObj->loc_id);
                // Get ctn_ttl by pallet
                $pltId = $palletHasLoc->plt_id ?? $pltObj->plt_id;
                $skus = $this->cartonModel->getCartonsByPltId($whsId, $pltId);
                //if loc is not contain pallet
                if ($palletHasLoc) {
                    $msg = sprintf("Location %s has pallet %s", $locRFID, $palletHasLoc['rfid']);
                    $data = [
                        'data'    => [
                            'pallet' => $palletHasLoc,
                            'skus'   => $skus
                        ],
                        'message' => $msg,
                        'status'  => false,
                    ];

                    return new Response($data, 200, [], null);
                }
                \DB::beginTransaction();
                //update old pallet location is not in use
                $result = $this->palletModel->updatePalletPutBackNewLoc($pltObj->plt_id, $arrLoc);
                //update carton location
                if ($result) {
                    $result = $this->cartonModel->updateCartonPutBackNewLoc($pltObj->plt_id, $arrLoc);
                }

                if ($result) {
                    \DB::commit();
                    $msg = sprintf("Put back pallet %s to loction %s successfully.", $palletRFID, $arrLoc['loc_code']);
                    $data = [
                        'data'    => [
                            'pallet' => $palletHasLoc,
                            'skus'   => $skus
                        ],
                        'message' => $msg,
                        'status'  => true,
                    ];

                    return new Response($data, 200, [], null);
                } else {
                    DB::rollback();
                    $msg = sprintf("On pallet %s has no carton.", $palletRFID);
                    $data = [
                        'data'    => null,
                        'message' => $msg,
                        'status'  => false,
                    ];

                    return new Response($data, 200, [], null);
                }
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
