<?php

namespace App\Api\V2\PackDetail\ManualPack\Validators;

use Dingo\Api\Exception\UnknownVersionException;

/**
 * Class PackHdrValidator
 *
 * @package App\Api\V1\Validators
 */
class PackDtlValidator extends AbstractValidator
{
    /**
     * @return array
     */
    protected function rules()
    {
        return [
            'pack_hdr_id'         => 'required|exists:pack_hdr,pack_hdr_id',
            'is_new_sku'          => 'required',
            'items'               => 'required|array',
            'items.*.pack_dtl_id' => 'required|integer|exists:pack_dtl,pack_dtl_id',
            'items.*.itm_id'      => 'required|integer|exists:item,item_id',
            'items.*.piece_qty'   => 'integer',
        ];
    }


}
