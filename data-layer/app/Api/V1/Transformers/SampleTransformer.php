<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;

class SampleTransformer extends TransformerAbstract
{

    /**
     * @param $sample
     *
     * @return array
     */
    public function transform($sample)
    {
        return [
            'header' => $sample->title,
            'body'   => $sample->description
        ];
    }
}
