<?php
/**
 * Created by PhpStorm.
 * User: Phuc
 * Date: 3/8/2017
 * Time: 9:32 AM
 */

namespace App\Api\Inbound\Models;

use Illuminate\Database\Eloquent\Model;

class AsnHdr extends Model
{
    protected $table = 'asn_hdr';
    protected $primaryKey = 'asn_hdr_id';
}