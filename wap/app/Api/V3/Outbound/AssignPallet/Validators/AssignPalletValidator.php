<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 22/July/2016
 * Time: 11:05
 */

namespace App\Api\V3\Outbound\AssignPallet\Validators;

class AssignPalletValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_code' => 'required',
            'pallet' => 'required',
        ];
    }
}
