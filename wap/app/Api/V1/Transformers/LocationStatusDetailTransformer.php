<?php
/**
 * Created by PhpStorm.
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Transformers;

use App\LocationStatusDetail;
use League\Fractal\TransformerAbstract;

class LocationStatusDetailTransformer extends TransformerAbstract
{
    /**
     * @param $result
     *
     * @return array
     */
    public function transform($result)
    {
        return [
            'loc_sts_dtl_loc_id'    => object_get($result, 'loc_sts_dtl_loc_id', ''),
            'loc_sts_dtl_sts_code'  => object_get($result, 'loc_sts_dtl_sts_code', ''),
            'loc_sts_dtl_from_date' => object_get($result, 'loc_sts_dtl_from_date', '')
        ];
    }
}
