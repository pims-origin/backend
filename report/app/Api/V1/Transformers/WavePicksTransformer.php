<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Utils\Status;

class WavePicksTransformer extends TransformerAbstract
{

    public function transform($wavePick)
    {
        return [
            'wv_dtl_id' => $wavePick->wv_dtl_id,
            'wv_num' => $wavePick->wv_num,
            'cus_name' => object_get($wavePick, 'customer.cus_name', ''),
            'ctn_qty' => $wavePick->ctn_qty,
            'pack_size' => $wavePick->pack_size,
            'piece_qty' => $wavePick->piece_qty,
            'act_piece_qty' => $wavePick->act_piece_qty,
            'primary_loc' => $wavePick->primary_loc,
            'bu_loc_1' => $wavePick->bu_loc_1,
            'bu_loc_2' => $wavePick->bu_loc_2,
            'bu_loc_3' => $wavePick->bu_loc_3,
            'bu_loc_4' => $wavePick->bu_loc_4,
            'bu_loc_5' => $wavePick->bu_loc_5,
            //'act_loc' => $wavePick->act_loc_id,
            'act_loc'=>object_get($wavePick,'actloc.loc_code',''),
            'cus_upc' => object_get($wavePick, 'item.cus_upc', ''),
            'sku' => $wavePick->sku,
            'size' => $wavePick->size,
            'color' => $wavePick->color,
            'wv_dtl_sts' => Status::getByKey("WAVEPICK-STATUS", $wavePick->wv_dtl_sts),
            'lot'=>$wavePick->lot
        ];
    }

    /*
    ****************************************************************************
    */

}

