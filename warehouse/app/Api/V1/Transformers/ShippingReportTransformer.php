<?php
/**
 * Created by PhpStorm.
 * User: PCDell76_ThienNguyen
 * Date: 2/10/2017
 * Time: 7:19 PM
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\Status;

class ShippingReportTransformer extends TransformerAbstract
{
    public function transform(OrderDtl $odrDtl)
    {
        return [
            'cus_name'              => object_get($odrDtl, 'cus_name', ''),
            'cus_code'              => object_get($odrDtl, 'cus_code', ''),
            'odr_num'               => object_get($odrDtl, 'odr_num', ''),
            'odr_type_code'         => object_get($odrDtl, 'odr_type', ''),
            'odr_type'              => Status::getByKey("ORDER-TYPE", object_get($odrDtl, 'odr_type', '')),
            'item_id'               => object_get($odrDtl, 'item_id', ''),
            'sku'                   => object_get($odrDtl, 'sku', ''),
            'size'                  => object_get($odrDtl, 'size', ''),
            'color'                 => object_get($odrDtl, 'color', ''),
            'lot'                   => object_get($odrDtl, 'lot', ''),
            'upc'                   => object_get($odrDtl, 'upc', ''),
            'piece_qty'             => object_get($odrDtl, 'piece_qty', ''),
            'ship_dt'               => $this->dateFormat(object_get($odrDtl, 'shipped_dt', '')),
            'ship_by'               => object_get($odrDtl, 'first_name', '') . ' ' . object_get($odrDtl, 'last_name', ''),
            'pack'                  => object_get($odrDtl, 'pack', ''),
            'shipping_ctns'         => round(object_get($odrDtl, 'piece_qty', '') / object_get($odrDtl, 'pack', '')),
            'cus_odr_num'           => object_get($odrDtl, 'cus_odr_num', ''),
            'cus_po'                => object_get($odrDtl, 'cus_po', ''),
            'ship_to_name'          => object_get($odrDtl, 'ship_to_name', ''),
            'carrier'               => object_get($odrDtl, 'carrier', ''),
            'ship_to_add_1'         => object_get($odrDtl, 'ship_to_add_1', ''),
            'ship_to_state'         => object_get($odrDtl, 'ship_to_state', ''),
            'sys_state_name'        => object_get($odrDtl, 'sys_state_name', ''),
            'ship_to_city'          => object_get($odrDtl, 'ship_to_city', ''),
            'ship_to_zip'           => object_get($odrDtl, 'ship_to_zip', ''),

            'length' => object_get($odrDtl, 'length', ''),
            'width'  => object_get($odrDtl, 'width', ''),
            'height' => object_get($odrDtl, 'height', ''),
            'cube'   => object_get($odrDtl, 'cube', ''),

            'act_cancel_dt'         => (empty(object_get($odrDtl, 'act_cancel_dt', ''))) ? '' :
                date("m/d/Y", object_get($odrDtl, 'act_cancel_dt', '')),
        ];
    }

    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date('m/d/Y H:i', $date) : null;
    }
}