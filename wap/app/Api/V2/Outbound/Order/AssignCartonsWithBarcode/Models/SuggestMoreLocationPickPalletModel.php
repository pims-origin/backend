<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Outbound\Order\AssignCartonsWithBarcode\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class SuggestMoreLocationPickPalletModel extends AbstractModel
{
    const PICK_CARTON = 'CT';
    const PICK_PIECE = 'PC';
    const PICK_PALLET = 'PL';

    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    public function getFullPallet($whsId, $itemId, $lot)
    {
        $query = DB::table('cartons')
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->select([
                DB::raw('pallet.*')
            ])
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->where('pallet.ctn_ttl', '>', 0)
            ->whereNull('pallet.rfid')
            ->whereRaw('pallet.init_ctn_ttl = pallet.ctn_ttl')
            ->orderBy('pallet.init_ctn_ttl', 'DESC');

        //var_dump($query->getBindings());exit($query->toSql());
        return $query->first();
    }

    public function sortDeepLocationPicking($data)
    {
        for ($i = 0; $i < count($data); $i++) {

            if (isset($data[$i + 1])) {

                $next = $data[$i + 1]['loc_code'];
                $cur = $data[$i]['loc_code'];
                $curloc = substr($cur, 0, strlen($cur) - 1);
                $nextLoc = substr($next, 0, strlen($next) - 1);
                if ($curloc == $nextLoc) {
                    if (substr($cur, -1, 1) > substr($next, -1, 1)) {
                        $next = $data[$i + 1];
                        $current = $data[$i];
                        $data[$i + 1] = $current;
                        $data[$i] = $next;
                        $i = $i + 1;
                    }

                }

            }
        }

        return $data;
    }

    public function getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take, $IgnorefirstLevel = true, $rfidFlag = 0)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $query = DB::table('cartons')
            ->join('pallet', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->join('location', 'location.loc_id', '=', 'cartons.loc_id')
            ->select([
                'pallet.loc_id',
                'pallet.loc_code',
                'pallet.rfid',
                DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS ctns')
            ])
            ->where('location.loc_sts_code', 'AC')
            ->where('cartons.deleted', 0)
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.whs_id', $whsId)
            ->where('pallet.deleted', 0)
            ->whereNull('pallet.rfid')
            ->whereRaw('pallet.init_ctn_ttl = pallet.ctn_ttl')
            ->groupBy('pallet.loc_id');

        if ($IgnorefirstLevel) {
            //Remove A = Level 1
            $query->whereRaw("pallet.loc_code REGEXP '([B-Z])[1-2]$' ");
        }

        if ($rfidFlag == 1) {
            $query->whereNotNull('cartons.rfid');
        } else {
            $query->whereNull('cartons.rfid');
        }

        $query->orderBy('pallet.ctn_ttl', 'DESC');
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                // $query->orderBy('cartons.gr_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        };
        $query->orderBy(DB::Raw("pallet.loc_code"), 'DESC');

        return $query->limit($take)->skip(0)->get();
    }

    public function getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl, $firstLevel = true, $rfidFlag = 0)
    {
        $query = DB::table('pallet')
            ->select([
                'pallet.loc_id',
                'pallet.loc_code',
                'pallet.rfid',
                DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS ctns')
            ])
            ->join('cartons', 'cartons.plt_id', '=', 'pallet.plt_id')
            ->leftJoin('location', 'pallet.loc_id', '=', 'location.loc_id')
            ->where([
                'cartons.ctn_sts' => 'AC',
                'cartons.is_damaged' => 0,
                'cartons.deleted' =>0,
            ])
            ->where('pallet.plt_sts', 'AC')
            ->whereRaw("
                IF(location.loc_id, cartons.loc_type_code = 'RAC', true)
                AND IF(location.loc_id, location.loc_sts_code = 'AC', true)
                AND pallet.rfid IS NULL
            ")
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.lot', $lot)
            ->where('cartons.whs_id', $whsId)
            ->groupBy('pallet.plt_id')
        ;

        if ($rfidFlag == 1) {
            $query->whereNotNull('cartons.rfid');
        } else {
            $query->whereNull('cartons.rfid');
        }

        if ($pltTtl < 1 && $firstLevel) {
            $query->whereRaw("pallet.loc_code REGEXP '([A])[1-2]$' ");
            $query->whereRaw('pallet.init_ctn_ttl != pallet.ctn_ttl');

        }

        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.gr_dt', 'DESC');
                break;
            case "FEFO":
                $query->orderBy(DB::Raw('cartons.expired_dt = 0, cartons.expired_dt'), 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.gr_dt', 'ASC');
                break;
        }


        if ($pltTtl >= 1) {
            $query->orderBy("pallet.ctn_ttl", 'DESC');
            $query->orderBy(DB::Raw("pallet.loc_code"), 'DESC');
        } else {
            $query->orderBy("pallet.ctn_ttl", 'ASC');
            $query->orderBy(DB::Raw("pallet.loc_code"), 'ASC');
        }

        return $query->limit($take)->skip(0)->get();
    }

    /**
     * @param array $locIds
     * @param int $itemId
     *
     * @return mixed
     */
    public function getMoreSugLocByWvDtl($wvDtl, $pickFullPallet, $take = 8, $rfidFlag = 0)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $customerConfigModel = new CustomerConfigModel();
        $whsId = Data::getCurrentWhsId();
        $itemId = $wvDtl->item_id;
        $lot = $wvDtl->lot;
        $cusId = $wvDtl->cus_id;
        $pickQty = $wvDtl->piece_qty - $wvDtl->act_piece_qty;
        $pickedCTNS = ceil($pickQty / $wvDtl->pack_size);

        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cusId);

        $fullPallet = $this->getFullPallet($whsId, $itemId, $lot);
        $pltTtl = 0;


        if ($fullPallet) {
            $pltTtl = intval($pickedCTNS / $fullPallet['ctn_ttl']);
        }

        if ($pickFullPallet) {
            if ($pltTtl >= 1) {
                $ignoreFirstLevel = true;
                $locs = $this->getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take,
                    $ignoreFirstLevel,
                    $rfidFlag
                );
                if (count($locs) == 0) {
                    $locs = $this->getPalletSuggestionLocations($whsId, $itemId, $lot, $algorithm, $take,
                        $ignoreFirstLevel,
                        $rfidFlag
                    );
                }

                if (count($locs) > 0) {
                    return $locs;
                }
            }

        }

        $firstLevel = false;
        $locs = $this->getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl, $firstLevel, $rfidFlag);
        $firstLevel = true;
        if (count($locs) == 0) {
            $locs = $this->getSuggestionByCarton($whsId, $itemId, $lot, $algorithm, $take, $pltTtl, $firstLevel, $rfidFlag);
        }

        return $locs;

    }

    public function pickPallet($wvDtl, $rfidFlag = 0, $take = 8)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('odr_dtl_allocation')
            ->join('pallet', 'pallet.plt_id', '=', 'odr_dtl_allocation.plt_id')
            ->join('cartons', 'pallet.plt_id', '=', 'cartons.plt_id')
            ->select([
                'odr_dtl_allocation.loc_id',
                'odr_dtl_allocation.loc_code',
                DB::raw('odr_dtl_allocation.plt_rfid AS rfid'),
                'pallet.pack AS ctns',
                DB::raw('odr_dtl_allocation.qty AS avail_qty'),
                'odr_dtl_allocation.qty',
                'odr_dtl_allocation.wv_dtl_id'
            ])
            ->where('pallet.plt_sts', 'AL')
            ->whereNull('pallet.rfid')
            ->where('wv_dtl_id', $wvDtl->wv_dtl_id);

        if ($rfidFlag == 1) {
            $query->whereNotNull('cartons.rfid');
        } else {
            $query->whereNull('cartons.rfid');
        }

        $result = $query->limit($take)->get();

        return $result;
    }

    public function getSugPltByWvDtl($wvDtl, $rfidFlag = 0, $take = 8)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('pallet')
            ->join('cartons', 'cartons.plt_id', '=', 'pallet.plt_id')
            ->select([
                "pallet.loc_id",
                "pallet.loc_code",
                "pallet.rfid",
                DB::raw('SUM(cartons.piece_remain) AS avail_qty'),
                DB::raw('COUNT(cartons.ctn_id) AS ctns')
            ])
            ->whereIn('pallet.plt_sts', ['NW', 'AC'])
            ->whereNull('pallet.loc_id')
            ->whereNull('pallet.rfid')
            ->where([
                'cartons.item_id' => $wvDtl->item_id,
                'cartons.lot'     => $wvDtl->lot,
                'cartons.deleted' => 0,
                'cartons.ctn_sts' => 'AC',
                'pallet.cus_id'   => $wvDtl->cus_id,
            ])
            ->groupBy('pallet.plt_id');

        if ($rfidFlag == 1) {
            $query->whereNotNull('cartons.rfid');
        } else {
            $query->whereNull('cartons.rfid');
        }

        $result = $query->limit($take)->skip(0)->get();

        return $result;
    }

}
