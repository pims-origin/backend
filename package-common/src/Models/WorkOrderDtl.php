<?php

namespace Seldat\Wms2\Models;

/**
 * Class WorkOrderDtl
 *
 * @package Seldat\Wms2\Models
 */
class WorkOrderDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wo_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wo_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'wo_hdr_id',
        'whs_id',
        'cus_id',
        'odr_hdr_id',
        'item_id',
        'sku',
        'size',
        'color',
        'cus_upc',
        'wo_hdr_num',
        'qty',
        'chg_code_id',
        'chg_code',
        'sts',
        'lot'
    ];

    public function WorkOrderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\WorkOrderHdr', 'wo_hdr_id', 'wo_hdr_id');
    }

    public function orderHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\OrderHdr', 'odr_id', 'odr_hdr_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function user()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function chargeCode()
    {
        return $this->hasOne(__NAMESPACE__ . '\ChargeCode', 'chg_code_id', 'chg_code_id');
    }
}