<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Outbound\Carton\PiecesRemainFromCarton\Models;

use App\MessageCode;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WavepickDtl;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class WavepickDtlModel extends AbstractModel
{
    /**
     * WavepickDtlModel constructor.
     * @param WavepickDtl|null $model
     */
    public function __construct(WavepickDtl $model = null)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $this->model = ($model) ?: new WavepickDtl();
    }

}
