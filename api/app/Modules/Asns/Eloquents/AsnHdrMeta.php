<?php

namespace App\Modules\Asns\Eloquents;

class AsnHdrMeta extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'asn_meta';

    protected $primaryKey = 'asn_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'qualifier',
        'asn_hdr_id',
        'value'
    ];
}
