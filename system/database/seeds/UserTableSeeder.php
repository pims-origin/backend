<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'           => 'Admin Admin',
            'email'          => 'admin@admin.com',
            'password'       => app('hash')->make('123'),
            'remember_token' => str_random(10),
        ]);
    }
}
