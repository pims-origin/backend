<?php
namespace App\Api\V1\Models;

use App\ResetPassword;
use App\User;
use Carbon\Carbon;

class UserModel extends AbstractModel
{
    /**
     * UserModel constructor.
     */
    public function __construct()
    {
        $this->model = new User();
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getUserByIds($userId)
    {
        return User::where('user_id', $userId)->first();
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getUserByUsername($username)
    {
        return User::where('username', $username)->first();
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    /**
     * @param $user
     * @return static
     */
    public function makeResetPasswordToken($user)
    {
        $condition = [
            'user_id' => $user->user_id
        ];
        $token = randomToken();

        ResetPassword::updateOrCreate($condition, [
            'token' => randomToken(),
            'user_id' => $user->user_id,
            'expired_at' => Carbon::now()->addDay()
        ]);

        return $token;
    }

    /**
     * @param $token
     * @return bool
     */
    public function validateTokenByEmail($token)
    {
        $now = Carbon::now();

        $data = ResetPassword::where('token', $token)
            ->where('expired_at', '>=', $now)
            ->first();

        return $data;
    }

    /**
     * @param $userId
     * @param $password
     * @return bool|int
     */
    public function updatePassword($userId, $password)
    {
        $result = User::where('user_id', $userId)->update([
            'password' => app('hash')->make($password)
        ]);

        return $result;
    }

    /**
     * @param $userId
     */
    public function updateExpiredTime($userId)
    {
        $condition = [
            'user_id' => $userId
        ];

        ResetPassword::updateOrCreate($condition, [
            'expired_at' => Carbon::now()
        ]);
    }
}
