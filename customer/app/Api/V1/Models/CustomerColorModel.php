<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 16-Nov-16
 * Time: 12:01
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\CustomerColor;

class CustomerColorModel extends AbstractModel
{
    /**
     * CustomerColorModel constructor.
     *
     * @param CustomerColor|null $model
     */
    public function __construct(CustomerColor $model = null)
    {
        $this->model = ($model) ?: new CustomerColor();
    }

    public function getAllCustomer()
    {
        $query = $this->make(['customer'])
            ->select(['cl_id', 'cl_name', 'cl_code', 'cus_id'])
            ->whereNotNull('cus_id')
            ->get();

        return $query;
    }
}
