<?php

$middleware = ['app', 'user', 'cus', 'whs'];

$router->group([
        'prefix'     => '/rest/wms360/seldat/v1',
        'middleware' => $middleware,
        'namespace'  => 'App\Modules\GoodsReceipts\Controllers'
    ], function () use ($router) {

    // Get list goods-receipt of customer
    $router->get('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/goods-receipts', [
        'uses' => 'GoodsReceiptController@index'
    ]);

    // Get detail goods-receipt of customer});
    $router->get('/warehouses/{whsId:[0-9]+}/customers/{cusId:[0-9]+}/goods-receipts/{grHdrId:[0-9]+}', [
        'uses' => 'GoodsReceiptController@read'
    ]);

});