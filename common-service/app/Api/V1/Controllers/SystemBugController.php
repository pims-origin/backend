<?php
/**
 * Created by PhpStorm.
 * User: chicu
 * Date: 9/14/2016
 * Time: 4:02 PM
 */
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\SystemBugs;
use App\Api\V1\Models\SystemBugsModel;
use Illuminate\Support\Facades\Input;
use Laravel\Lumen\Routing\Controller;

class SystemBugController extends Controller {

    public function getSysBugs () {
        $option = Input::all();
        $sysBug = new SystemBugsModel();
        return $sysBug->getSysBugs($option) ;
    }
}