<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use App\OrderHdr;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderDtlTransformer
 *
 * @package App\Api\V1\Transformers
 */
class PutBackTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $kittingOdrDtl = $orderHdr->details()->where('is_kitting', 1)->get();

        $pickedTotal = 0;
        foreach ($kittingOdrDtl as $item) {
            $pickedTotal += $item->picked_qty;
        }
        return [
            // Customer
            'cus_id'               => $orderHdr->cus_id,
            'cus_name'             => object_get($orderHdr, 'customer.cus_name', null),
            'ref_cod'              => $orderHdr->ref_cod,
            'odr_sts'              => Status::getByKey("Order-status", $orderHdr->odr_sts),
            'odr_sts_name'         => $orderHdr->odr_sts,
            'odr_num'              => $orderHdr->odr_num,
            'picked_ttl'           => $pickedTotal,
            'data'                 => $kittingOdrDtl->toArray(),
        ];
    }
}
