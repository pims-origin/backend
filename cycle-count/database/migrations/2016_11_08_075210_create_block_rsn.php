<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockRsn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('block_rsn')) {
            Schema::create('block_rsn', function (Blueprint $table) {
                $table->increments('block_rsn_id');
                $table->string('block_rsn_name', 50);
                $table->text('block_rsn_des')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by');
                $table->integer('created_at');
                $table->integer('updated_at');
                $table->char('sts', 1);
                $table->tinyInteger('deleted');
                $table->integer('deleted_at');
                $table->unique('block_rsn_name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('block_rsn')) {
            Schema::drop('block_rsn');
        }
    }
}
