<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\Putback\Models;

use Seldat\Wms2\Models\PutBack;

/**
 * Class PutBackModel
 *
 * @package App\Api\V1\Models
 */
class PutBackModel extends AbstractModel
{
    /**
     * PutBackModel constructor.
     *
     * @param PutBack|null $model
     */
    public function __construct(PutBack $model = null)
    {
        $this->model = ($model) ?: new PutBack();
    }
}
