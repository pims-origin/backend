<?php

use App\MenuGroup;
use App\Menu;
use App\MenuFavorite;
use Illuminate\Http\Response as IlluminateResponse;

class MenuControllerCest
{
//    protected $endpoint = '/v1';
//
//    public function storeMass_Ok(ApiTester $I, MenuGroup $menuGroupEntity)
//    {
//        $paramMenuGroup = ['name' => 'group 1', 'description' => 'description'];
//        $menuGroupId = $I->haveRecord($menuGroupEntity->getTable(), $paramMenuGroup);
//
//        $uri = $this->endpoint . '/groups/' . $menuGroupId .'/list_menu';
//        $params['param'] = [
//            [
//                "name"          => "Menu item1",
//                "description"   => "Menu item1",
//                "url"           => "http://menu1",
//                "target"        => "_blank",
//                "nodes"         => [
//                    [
//                        "name"          => "Menu item1.1",
//                        "description"   => "Menu item1.1",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ],
//                    [
//                        "name"          => "Menu item1.2",
//                        "description"   => "Menu item1.2",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ]
//                ]
//            ],
//            [
//                "name"          => "Menu item2",
//                "description"   => "Menu item2",
//                "url"           => "http://menu2",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item3",
//                "description"   => "Menu item3",
//                "url"           => "http://menu3",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item4",
//                "description"   => "Menu item4",
//                "url"           => "http://menu4",
//                "target"        => "_blank",
//                "nodes"         => []
//            ]
//        ];
//
//        $params['param'] = json_encode($params['param']);
//
//        $I->sendPOST($uri, $params);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//    }
//
//    public function storeMass_NotHasMenuGroup_Error(ApiTester $I)
//    {
//        $menuGroupId = 2;
//        $uri = $this->endpoint . '/groups/' . $menuGroupId .'/list_menu';
//        $params['param'] = [
//            [
//                "name"          => "Menu item1",
//                "description"   => "Menu item1",
//                "url"           => "http://menu1",
//                "target"        => "_blank",
//                "nodes"         => [
//                    [
//                        "name"          => "Menu item1.1",
//                        "description"   => "Menu item1.1",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ],
//                    [
//                        "name"          => "Menu item1.2",
//                        "description"   => "Menu item1.2",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ]
//                ]
//            ],
//            [
//                "name"          => "Menu item2",
//                "description"   => "Menu item2",
//                "url"           => "http://menu2",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item3",
//                "description"   => "Menu item3",
//                "url"           => "http://menu3",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item4",
//                "description"   => "Menu item4",
//                "url"           => "http://menu4",
//                "target"        => "_blank",
//                "nodes"         => []
//            ]
//        ];
//
//        $params['param'] = json_encode($params['param']);
//
//        $I->sendPOST($uri, $params);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_BAD_REQUEST);
//    }
//
//    // can test them la van tao duoc cac menu phia tren, khong bi loi
//    public function storeMass_NotExistingMenuId_Error(ApiTester $I)
//    {
//        $menuGroupId = 2;
//        $uri = $this->endpoint . '/groups/' . $menuGroupId .'/list_menu';
//        $params['param'] = [
//            [
//                "name"          => "Menu item1",
//                "description"   => "Menu item1",
//                "url"           => "http://menu1",
//                "target"        => "_blank",
//                "nodes"         => [
//                    [
//                        "menu_id"       => 3,
//                        "name"          => "Menu item1.1",
//                        "description"   => "Menu item1.1",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ],
//                    [
//                        "name"          => "Menu item1.2",
//                        "description"   => "Menu item1.2",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ]
//                ]
//            ],
//            [
//                "name"          => "Menu item2",
//                "description"   => "Menu item2",
//                "url"           => "http://menu2",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item3",
//                "description"   => "Menu item3",
//                "url"           => "http://menu3",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item4",
//                "description"   => "Menu item4",
//                "url"           => "http://menu4",
//                "target"        => "_blank",
//                "nodes"         => []
//            ]
//        ];
//
//        $params['param'] = json_encode($params['param']);
//
//        $I->sendPOST($uri, $params);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_BAD_REQUEST);
//    }
//
//    // can test them la van tao duoc cac menu phia tren, khong bi loi
//    public function storeMass_NotName_Error(ApiTester $I)
//    {
//        $menuGroupId = 2;
//        $uri = $this->endpoint . '/groups/' . $menuGroupId .'/list_menu';
//        $params['param'] = [
//            [
//                "name"          => "Menu item1",
//                "description"   => "Menu item1",
//                "url"           => "http://menu1",
//                "target"        => "_blank",
//                "nodes"         => [
//                    [
//                        "name"          => "",
//                        "description"   => "Menu item1.1",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ],
//                    [
//                        "name"          => "Menu item1.2",
//                        "description"   => "Menu item1.2",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ]
//                ]
//            ],
//            [
//                "name"          => "Menu item2",
//                "description"   => "Menu item2",
//                "url"           => "http://menu2",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item3",
//                "description"   => "Menu item3",
//                "url"           => "http://menu3",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item4",
//                "description"   => "Menu item4",
//                "url"           => "http://menu4",
//                "target"        => "_blank",
//                "nodes"         => []
//            ]
//        ];
//
//        $params['param'] = json_encode($params['param']);
//
//        $I->sendPOST($uri, $params);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_BAD_REQUEST);
//    }
//
//    public function storeMass_DeleteFavorite_Ok(ApiTester $I, MenuGroup $menuGroupEntity, Menu $menuEntity, MenuFavorite $menuFavoriteEntity)
//    {
//        $paramMenuGroup = ['name' => 'group 1', 'description' => 'description'];
//        $menuGroupId = $I->haveRecord($menuGroupEntity->getTable(), $paramMenuGroup);
//
//        $menuIds = [];
//        for ($i = 0; $i < 10; $i++) {
//            $paramMenu = [
//                "name"          => "Menu item" . $i,
//                "description"   => "Menu item",
//                "url"           => "http://menu",
//                "target"        => "_blank",
//                "menu_group_id" => $menuGroupId,
//            ];
//            $menuIds[] = $I->haveRecord($menuEntity->getTable(), $paramMenu);
//        }
//
//        for ($i = 0; $i < 3; $i++) {
//            $param = [
//                'menu_id'       => $menuIds[$i],
//                'user_id'       => 1,
//                'display_order' => $i + 1
//            ];
//
//            $I->haveRecordNoPrimary($menuFavoriteEntity->getTable(), $param);
//        }
//
//        $uri = $this->endpoint . '/groups/' . $menuGroupId .'/list_menu';
//
//        $params = [];
//        $params['param'] = [
//            [
//                "menu_id"       => $menuIds[0],
//                "name"          => "Menu item1",
//                "description"   => "Menu item1",
//                "url"           => "http://menu1",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item2",
//                "description"   => "Menu item2",
//                "url"           => "http://menu2",
//                "target"        => "_blank",
//                "nodes"         => []
//            ]
//        ];
//
//        $params['param'] = json_encode($params['param']);
//
//        $I->sendPOST($uri, $params);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//
//        $I->seeRecord($menuFavoriteEntity->getTable(), ['menu_id' => $menuIds[0], 'user_id' => 1]);
//        $I->dontSeeRecord($menuFavoriteEntity->getTable(), ['menu_id' => $menuIds[1], 'user_id' => 1]);
//        $I->dontSeeRecord($menuFavoriteEntity->getTable(), ['menu_id' => $menuIds[2], 'user_id' => 1]);
//    }
//
//    public function showGroup_Ok(ApiTester $I, MenuGroup $menuGroupEntity)
//    {
//        $paramMenuGroup = ['name' => 'group 1', 'description' => 'description'];
//        $menuGroupId = $I->haveRecord($menuGroupEntity->getTable(), $paramMenuGroup);
//
//        $uri = $this->endpoint . '/groups/' . $menuGroupId .'/list_menu';
//
//        $params['param'] = [
//            [
//                "name"          => "Menu item1",
//                "description"   => "Menu item1",
//                "url"           => "http://menu1",
//                "target"        => "_blank",
//                "nodes"         => [
//                    [
//                        "name"          => "Menu item1.1",
//                        "description"   => "Menu item1.1",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ],
//                    [
//                        "name"          => "Menu item1.2",
//                        "description"   => "Menu item1.2",
//                        "url"           => "http://",
//                        "target"        => "_blank",
//                        "nodes"         => []
//                    ]
//                ]
//            ],
//            [
//                "name"          => "Menu item2",
//                "description"   => "Menu item2",
//                "url"           => "http://menu2",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item3",
//                "description"   => "Menu item3",
//                "url"           => "http://menu3",
//                "target"        => "_blank",
//                "nodes"         => []
//            ],
//            [
//                "name"          => "Menu item4",
//                "description"   => "Menu item4",
//                "url"           => "http://menu4",
//                "target"        => "_blank",
//                "nodes"         => []
//            ]
//        ];
//
//        $params['param'] = json_encode($params['param']);
//
//        $I->sendPOST($uri, $params);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//
//        $I->sendGET($uri);
//        $I->seeResponseCodeIs(IlluminateResponse::HTTP_OK);
//
//        $I->seeResponseIsJson();
//
//        $I->seeResponseContainsJson(
//            [
//                [
//                    "name"          => "Menu item1",
//                    "description"   => "Menu item1",
//                    "url"           => "http://menu1",
//                    "target"        => "_blank",
//                    "display_order" => 1,
//                ]
//            ]
//        );
//
//        $I->seeResponseContainsJson(
//            [
//                [
//                    "nodes" => [
//                        0 => [
//                            "name" => "Menu item1.1",
//                            "display_order" => 1,
//                        ],
//                        1 => [
//                            "name" => "Menu item1.2",
//                            "display_order" => 2,
//                        ]
//                    ],
//                ]
//            ]
//        );
//
//        $I->dontseeResponseContainsJson(
//            [
//                [
//                    "nodes" => [
//                        [
//                            "name" => "Menu item2",
//                        ]
//                    ],
//                ]
//            ]
//        );
//    }
}